.class Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;
.super Ljava/lang/Object;
.source "XTWiFiLocationProvider.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;


# direct methods
.method constructor <init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 438
    iput-object p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 10

    #@0
    .prologue
    const/16 v8, 0x66

    #@2
    const/4 v7, 0x0

    #@3
    .line 443
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@5
    invoke-static {v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$400(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@8
    move-result-object v3

    #@9
    sget-object v4, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;->IS_TRACKING_WAIT_FOR_TBF:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@b
    if-eq v3, v4, :cond_32

    #@d
    .line 448
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@f
    invoke-static {v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$000(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Lcom/qualcomm/lib/location/log/LocLog;

    #@12
    move-result-object v3

    #@13
    const-string v4, "XTWiFiLP"

    #@15
    new-instance v5, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v6, "timeoutNextFixSessionDue triggered at wrong state "

    #@1c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v5

    #@20
    iget-object v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@22
    invoke-static {v6}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$400(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@25
    move-result-object v6

    #@26
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v5

    #@2a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v5

    #@2e
    invoke-virtual {v3, v4, v5}, Lcom/qualcomm/lib/location/log/LocLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@31
    .line 563
    :goto_31
    return-void

    #@32
    .line 452
    :cond_32
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@34
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@37
    move-result-wide v4

    #@38
    invoke-static {v3, v4, v5}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$502(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;J)J

    #@3b
    .line 455
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@3d
    sget-object v4, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;->IS_TRACKING_WAIT_FOR_FINAL_FIX:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@3f
    invoke-static {v3, v4}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$402(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;)Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@42
    .line 458
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@44
    invoke-static {v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$100(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Landroid/os/Handler;

    #@47
    move-result-object v3

    #@48
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@4a
    invoke-static {v4}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$600(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Ljava/lang/Runnable;

    #@4d
    move-result-object v4

    #@4e
    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@51
    .line 459
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@53
    invoke-static {v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$100(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Landroid/os/Handler;

    #@56
    move-result-object v3

    #@57
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@59
    invoke-static {v4}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$600(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Ljava/lang/Runnable;

    #@5c
    move-result-object v4

    #@5d
    iget-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@5f
    invoke-static {v5}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$700(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)J

    #@62
    move-result-wide v5

    #@63
    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@66
    .line 463
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@68
    invoke-static {v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$800(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Z

    #@6b
    move-result v3

    #@6c
    if-nez v3, :cond_bd

    #@6e
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@70
    invoke-static {v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$900(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Z

    #@73
    move-result v3

    #@74
    if-eqz v3, :cond_bd

    #@76
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@78
    invoke-static {v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$1000(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;

    #@7b
    move-result-object v3

    #@7c
    if-eqz v3, :cond_bd

    #@7e
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@80
    invoke-static {v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$1100(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Z

    #@83
    move-result v3

    #@84
    if-nez v3, :cond_bd

    #@86
    .line 465
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@88
    invoke-static {v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$1000(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;

    #@8b
    move-result-object v3

    #@8c
    invoke-virtual {v3}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->triggerZppIfNotAlreadyRunning()Z

    #@8f
    move-result v3

    #@90
    if-eqz v3, :cond_1c2

    #@92
    .line 467
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@94
    sget-object v4, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;->IS_TRACKING_WAIT_FOR_ZPP_FIX:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@96
    invoke-static {v3, v4}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$402(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;)Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@99
    .line 470
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@9b
    invoke-static {v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$100(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Landroid/os/Handler;

    #@9e
    move-result-object v3

    #@9f
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@a1
    invoke-static {v4}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$1200(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Ljava/lang/Runnable;

    #@a4
    move-result-object v4

    #@a5
    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@a8
    .line 471
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@aa
    invoke-static {v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$100(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Landroid/os/Handler;

    #@ad
    move-result-object v3

    #@ae
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@b0
    invoke-static {v4}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$1200(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Ljava/lang/Runnable;

    #@b3
    move-result-object v4

    #@b4
    iget-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@b6
    invoke-static {v5}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$1300(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)J

    #@b9
    move-result-wide v5

    #@ba
    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@bd
    .line 488
    :cond_bd
    :goto_bd
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@bf
    invoke-static {v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$1404(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)I

    #@c2
    .line 489
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@c4
    invoke-static {v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$1400(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)I

    #@c7
    move-result v3

    #@c8
    const/16 v4, 0x3e8

    #@ca
    if-le v3, v4, :cond_d2

    #@cc
    .line 491
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@ce
    const/4 v4, 0x1

    #@cf
    invoke-static {v3, v4}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$1402(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;I)I

    #@d2
    .line 493
    :cond_d2
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@d4
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@d6
    invoke-static {v4}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$1400(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)I

    #@d9
    move-result v4

    #@da
    invoke-static {v3, v4}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$1502(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;I)I

    #@dd
    .line 495
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@df
    invoke-static {v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$800(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Z

    #@e2
    move-result v3

    #@e3
    if-eqz v3, :cond_150

    #@e5
    .line 497
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@e7
    sget-object v4, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;->IS_TRACKING_WAIT_FOR_ZPP_FIX:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@e9
    invoke-static {v3, v4}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$402(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;)Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@ec
    .line 504
    :try_start_ec
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@ee
    invoke-static {v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$1600(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@f1
    move-result-object v3

    #@f2
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->getOutputStream()Ljava/io/OutputStream;

    #@f5
    move-result-object v1

    #@f6
    .line 505
    .local v1, os:Ljava/io/OutputStream;
    new-instance v2, Lcom/qualcomm/lib/location/mq_client/OutPostcard;

    #@f8
    invoke-direct {v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;-><init>()V

    #@fb
    .line 506
    .local v2, out:Lcom/qualcomm/lib/location/mq_client/OutPostcard;
    invoke-virtual {v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->init()V

    #@fe
    .line 507
    const-string v3, "TO"

    #@100
    const-string v4, "XTWWAN-PE"

    #@102
    invoke-virtual {v2, v3, v4}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@105
    .line 508
    const-string v3, "FROM"

    #@107
    const-string v4, "XTWiFiLP"

    #@109
    invoke-virtual {v2, v3, v4}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@10c
    .line 509
    const-string v3, "REQ"

    #@10e
    const-string v4, "POSITION"

    #@110
    invoke-virtual {v2, v3, v4}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@113
    .line 510
    const-string v3, "TX-ID"

    #@115
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@117
    invoke-static {v4}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$1500(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)I

    #@11a
    move-result v4

    #@11b
    int-to-long v4, v4

    #@11c
    invoke-virtual {v2, v3, v4, v5}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addInt32(Ljava/lang/String;J)V

    #@11f
    .line 511
    invoke-virtual {v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->seal()V

    #@122
    .line 512
    invoke-virtual {v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->getEncodedBuffer()[B

    #@125
    move-result-object v3

    #@126
    invoke-virtual {v1, v3}, Ljava/io/OutputStream;->write([B)V

    #@129
    .line 513
    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V
    :try_end_12c
    .catch Ljava/lang/Exception; {:try_start_ec .. :try_end_12c} :catch_1d6

    #@12c
    .line 525
    .end local v1           #os:Ljava/io/OutputStream;
    .end local v2           #out:Lcom/qualcomm/lib/location/mq_client/OutPostcard;
    :goto_12c
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@12e
    invoke-static {v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$100(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Landroid/os/Handler;

    #@131
    move-result-object v3

    #@132
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@134
    invoke-static {v4}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$1200(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Ljava/lang/Runnable;

    #@137
    move-result-object v4

    #@138
    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@13b
    .line 526
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@13d
    invoke-static {v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$100(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Landroid/os/Handler;

    #@140
    move-result-object v3

    #@141
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@143
    invoke-static {v4}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$1200(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Ljava/lang/Runnable;

    #@146
    move-result-object v4

    #@147
    iget-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@149
    invoke-static {v5}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$1300(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)J

    #@14c
    move-result-wide v5

    #@14d
    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@150
    .line 538
    :cond_150
    :try_start_150
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@152
    invoke-static {v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$1600(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@155
    move-result-object v3

    #@156
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->getOutputStream()Ljava/io/OutputStream;

    #@159
    move-result-object v1

    #@15a
    .line 539
    .restart local v1       #os:Ljava/io/OutputStream;
    new-instance v2, Lcom/qualcomm/lib/location/mq_client/OutPostcard;

    #@15c
    invoke-direct {v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;-><init>()V

    #@15f
    .line 540
    .restart local v2       #out:Lcom/qualcomm/lib/location/mq_client/OutPostcard;
    invoke-virtual {v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->init()V

    #@162
    .line 541
    const-string v3, "TO"

    #@164
    const-string v4, "XTWiFi-PE"

    #@166
    invoke-virtual {v2, v3, v4}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@169
    .line 542
    const-string v3, "FROM"

    #@16b
    const-string v4, "XTWiFiLP"

    #@16d
    invoke-virtual {v2, v3, v4}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@170
    .line 543
    const-string v3, "REQ"

    #@172
    const-string v4, "POSITION"

    #@174
    invoke-virtual {v2, v3, v4}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@177
    .line 544
    const-string v3, "TX-ID"

    #@179
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@17b
    invoke-static {v4}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$1500(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)I

    #@17e
    move-result v4

    #@17f
    int-to-long v4, v4

    #@180
    invoke-virtual {v2, v3, v4, v5}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addInt32(Ljava/lang/String;J)V

    #@183
    .line 546
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@185
    invoke-static {v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$1700(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Z

    #@188
    move-result v3

    #@189
    if-eqz v3, :cond_191

    #@18b
    .line 548
    const-string v3, "SERVER-ACCESS-ALLOWED"

    #@18d
    const/4 v4, 0x0

    #@18e
    invoke-virtual {v2, v3, v4}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addBool(Ljava/lang/String;Z)V

    #@191
    .line 551
    :cond_191
    invoke-virtual {v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->seal()V

    #@194
    .line 552
    invoke-virtual {v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->getEncodedBuffer()[B

    #@197
    move-result-object v3

    #@198
    invoke-virtual {v1, v3}, Ljava/io/OutputStream;->write([B)V

    #@19b
    .line 553
    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V
    :try_end_19e
    .catch Ljava/lang/Exception; {:try_start_150 .. :try_end_19e} :catch_1a0

    #@19e
    goto/16 :goto_31

    #@1a0
    .line 555
    .end local v1           #os:Ljava/io/OutputStream;
    .end local v2           #out:Lcom/qualcomm/lib/location/mq_client/OutPostcard;
    :catch_1a0
    move-exception v0

    #@1a1
    .line 557
    .local v0, e:Ljava/lang/Exception;
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@1a3
    invoke-static {v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$200(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Lcom/qualcomm/services/location/xtwifi/NlpPublicStatus;

    #@1a6
    move-result-object v3

    #@1a7
    invoke-virtual {v3, v7}, Lcom/qualcomm/services/location/xtwifi/NlpPublicStatus;->setStatus(I)V

    #@1aa
    .line 558
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@1ac
    invoke-static {v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$000(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Lcom/qualcomm/lib/location/log/LocLog;

    #@1af
    move-result-object v3

    #@1b0
    const-string v4, "XTWiFiLP"

    #@1b2
    const-string v5, "cannot send POSITION request message to XTWiFi-PE"

    #@1b4
    invoke-virtual {v3, v4, v5}, Lcom/qualcomm/lib/location/log/LocLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@1b7
    .line 561
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@1b9
    invoke-static {v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$100(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Landroid/os/Handler;

    #@1bc
    move-result-object v3

    #@1bd
    invoke-virtual {v3, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@1c0
    goto/16 :goto_31

    #@1c2
    .line 476
    .end local v0           #e:Ljava/lang/Exception;
    :cond_1c2
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@1c4
    invoke-static {v3, v7}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$902(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;Z)Z

    #@1c7
    .line 477
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@1c9
    invoke-static {v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$000(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Lcom/qualcomm/lib/location/log/LocLog;

    #@1cc
    move-result-object v3

    #@1cd
    const-string v4, "XTWiFiLP"

    #@1cf
    const-string v5, "timeoutNextFixSessionDue cannot trigger ZPP fix. disable ZPP"

    #@1d1
    invoke-virtual {v3, v4, v5}, Lcom/qualcomm/lib/location/log/LocLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@1d4
    goto/16 :goto_bd

    #@1d6
    .line 515
    :catch_1d6
    move-exception v0

    #@1d7
    .line 517
    .restart local v0       #e:Ljava/lang/Exception;
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@1d9
    invoke-static {v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$200(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Lcom/qualcomm/services/location/xtwifi/NlpPublicStatus;

    #@1dc
    move-result-object v3

    #@1dd
    invoke-virtual {v3, v7}, Lcom/qualcomm/services/location/xtwifi/NlpPublicStatus;->setStatus(I)V

    #@1e0
    .line 518
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@1e2
    invoke-static {v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$000(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Lcom/qualcomm/lib/location/log/LocLog;

    #@1e5
    move-result-object v3

    #@1e6
    const-string v4, "XTWiFiLP"

    #@1e8
    const-string v5, "cannot send POSITION request message to XTWWAN-PE"

    #@1ea
    invoke-virtual {v3, v4, v5}, Lcom/qualcomm/lib/location/log/LocLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@1ed
    .line 521
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@1ef
    invoke-static {v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$100(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Landroid/os/Handler;

    #@1f2
    move-result-object v3

    #@1f3
    invoke-virtual {v3, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@1f6
    goto/16 :goto_12c
.end method
