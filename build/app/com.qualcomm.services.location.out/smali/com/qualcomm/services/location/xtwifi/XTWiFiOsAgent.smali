.class public Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;
.super Landroid/app/Service;
.source "XTWiFiOsAgent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread;,
        Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$SystemSettingsObserver;,
        Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$GlobalSettingsObserver;,
        Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$SecureSettingsObserver;,
        Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$PassiveLocationListener;,
        Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$TimedLocationListener;,
        Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$UpdateSubscriber;,
        Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$RilListener;,
        Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$ApiLevel17;
    }
.end annotation


# static fields
.field public static final ENH_LOCATION_SERVICES_ENABLED:Ljava/lang/String; = "enhLocationServices_on"

.field private static final MSG_IPC_LOOP_EXCEPTION:I = 0x66

.field private static final MSG_NEW_IPC_MESSAGE:I = 0x65

.field private static final MSG_TRY_CONNECT:I = 0x64

.field private static final TAG:Ljava/lang/String; = "XTWiFiOS"

.field private static final WWAN_LOC_RIL_DEREGISTER_CELL_UPDATE:I = 0x3

.field private static final WWAN_LOC_RIL_REGISTER_CELL_UPDATE:I = 0x2

.field private static final WWAN_LOC_RIL_REQUEST_SERVICE_INFO:I = 0x4

.field private static final WWAN_LOC_RIL_REQUEST_VERSION:I = 0x1

.field private static mCDMAHomeCarrier:Ljava/lang/String; = null

.field private static final mIpcName:Ljava/lang/String; = "OS-Agent"


# instance fields
.field private agentInitialized:Ljava/util/concurrent/CountDownLatch;

.field private agentThread:Ljava/lang/Thread;

.field private current_req_tx_id:I

.field private globalSettingsObserver:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$GlobalSettingsObserver;

.field private gnssRequestTimeout:Ljava/lang/Runnable;

.field private is_ENH_enabled:Z

.field private is_GPS_enabled:Z

.field private is_NLP_enabled:Z

.field private is_WIFI_on:Z

.field private is_airplane_mode_on:Z

.field private is_connected:Z

.field private is_our_gnss_request_running:Z

.field private final llog:Lcom/qualcomm/lib/location/log/LocLog;

.field private mActiveNetworkInfo:Landroid/net/NetworkInfo;

.field private mAgentHandler:Landroid/os/Handler;

.field private mAllowedProviders:Ljava/lang/String;

.field private final mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mConfigMaxAcceptableHepe:F

.field private mConfigTimeOutForGnssFix:J

.field private mConnectivityManager:Landroid/net/ConnectivityManager;

.field private mContext:Landroid/content/Context;

.field private mCurrentServiceState:I

.field private mGlobalSettings:Landroid/content/ContentQueryMap;

.field private mLastKnownLocation:Landroid/location/Location;

.field private mLocationManager:Landroid/location/LocationManager;

.field private mMaxAcceptableHepe:F

.field mPassiveListener:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$PassiveLocationListener;

.field private mRegisteredClientList:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mResolver:Landroid/content/ContentResolver;

.field private mRilListener:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$RilListener;

.field private mSecureSettings:Landroid/content/ContentQueryMap;

.field private mStatusUpdateSubscriber:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$UpdateSubscriber;",
            ">;"
        }
    .end annotation
.end field

.field private mSystemSettings:Landroid/content/ContentQueryMap;

.field private mTelMgr:Landroid/telephony/TelephonyManager;

.field private mTimeOutForGnssFix:J

.field mTimedListener:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$TimedLocationListener;

.field private mWifiManager:Landroid/net/wifi/WifiManager;

.field private m_api_level_17:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$ApiLevel17;

.field private m_connectRetryCounter:I

.field m_ipcClient:Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

.field private m_mutex:Ljava/lang/Object;

.field private next_req_tx_id:I

.field private secureSettingsObserver:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$SecureSettingsObserver;

.field private systemSettingsObserver:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$SystemSettingsObserver;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 119
    const-string v0, ""

    #@2
    sput-object v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mCDMAHomeCarrier:Ljava/lang/String;

    #@4
    return-void
.end method

.method public constructor <init>()V
    .registers 7

    #@0
    .prologue
    const/high16 v5, 0x447a

    #@2
    const/4 v4, 0x1

    #@3
    const/4 v3, 0x0

    #@4
    const/4 v2, 0x0

    #@5
    .line 370
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    #@8
    .line 91
    new-instance v0, Lcom/qualcomm/lib/location/log/LocLog;

    #@a
    const/4 v1, 0x3

    #@b
    invoke-direct {v0, v1}, Lcom/qualcomm/lib/location/log/LocLog;-><init>(I)V

    #@e
    iput-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@10
    .line 102
    const/4 v0, -0x1

    #@11
    iput v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->current_req_tx_id:I

    #@13
    .line 103
    iput v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->next_req_tx_id:I

    #@15
    .line 104
    iput v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mCurrentServiceState:I

    #@17
    .line 106
    iput v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->m_connectRetryCounter:I

    #@19
    .line 107
    iput-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->m_ipcClient:Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@1b
    .line 110
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    #@1d
    invoke-direct {v0, v4}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    #@20
    iput-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->agentInitialized:Ljava/util/concurrent/CountDownLatch;

    #@22
    .line 117
    iput-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mRilListener:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$RilListener;

    #@24
    .line 120
    new-instance v0, Ljava/lang/Object;

    #@26
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@29
    iput-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->m_mutex:Ljava/lang/Object;

    #@2b
    .line 155
    new-instance v0, Ljava/util/HashMap;

    #@2d
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@30
    iput-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mRegisteredClientList:Ljava/util/Map;

    #@32
    .line 205
    new-instance v0, Ljava/util/HashMap;

    #@34
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@37
    iput-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mStatusUpdateSubscriber:Ljava/util/Map;

    #@39
    .line 208
    iput-boolean v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->is_our_gnss_request_running:Z

    #@3b
    .line 209
    iput v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mConfigMaxAcceptableHepe:F

    #@3d
    .line 210
    iput v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mMaxAcceptableHepe:F

    #@3f
    .line 211
    const-wide/32 v0, 0xea60

    #@42
    iput-wide v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mConfigTimeOutForGnssFix:J

    #@44
    .line 212
    const-wide/16 v0, -0x1

    #@46
    iput-wide v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mTimeOutForGnssFix:J

    #@48
    .line 219
    new-instance v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$SecureSettingsObserver;

    #@4a
    invoke-direct {v0, p0, v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$SecureSettingsObserver;-><init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$1;)V

    #@4d
    iput-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->secureSettingsObserver:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$SecureSettingsObserver;

    #@4f
    .line 220
    new-instance v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$SystemSettingsObserver;

    #@51
    invoke-direct {v0, p0, v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$SystemSettingsObserver;-><init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$1;)V

    #@54
    iput-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->systemSettingsObserver:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$SystemSettingsObserver;

    #@56
    .line 221
    new-instance v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$GlobalSettingsObserver;

    #@58
    invoke-direct {v0, p0, v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$GlobalSettingsObserver;-><init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$1;)V

    #@5b
    iput-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->globalSettingsObserver:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$GlobalSettingsObserver;

    #@5d
    .line 223
    new-instance v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$TimedLocationListener;

    #@5f
    invoke-direct {v0, p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$TimedLocationListener;-><init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)V

    #@62
    iput-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mTimedListener:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$TimedLocationListener;

    #@64
    .line 224
    new-instance v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$PassiveLocationListener;

    #@66
    invoke-direct {v0, p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$PassiveLocationListener;-><init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)V

    #@69
    iput-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mPassiveListener:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$PassiveLocationListener;

    #@6b
    .line 226
    iput-boolean v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->is_airplane_mode_on:Z

    #@6d
    .line 227
    iput-boolean v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->is_WIFI_on:Z

    #@6f
    .line 228
    iput-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mAllowedProviders:Ljava/lang/String;

    #@71
    .line 229
    iput-boolean v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->is_GPS_enabled:Z

    #@73
    .line 230
    iput-boolean v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->is_NLP_enabled:Z

    #@75
    .line 231
    iput-boolean v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->is_ENH_enabled:Z

    #@77
    .line 232
    iput-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mLastKnownLocation:Landroid/location/Location;

    #@79
    .line 233
    iput-boolean v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->is_connected:Z

    #@7b
    .line 237
    iput-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mActiveNetworkInfo:Landroid/net/NetworkInfo;

    #@7d
    .line 1427
    new-instance v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$1;

    #@7f
    invoke-direct {v0, p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$1;-><init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)V

    #@82
    iput-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@84
    .line 1526
    new-instance v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$2;

    #@86
    invoke-direct {v0, p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$2;-><init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)V

    #@89
    iput-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->gnssRequestTimeout:Ljava/lang/Runnable;

    #@8b
    .line 371
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@8d
    const-string v1, "XTWiFiOS"

    #@8f
    const-string v2, "Constructor"

    #@91
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@94
    .line 373
    invoke-direct {p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->setupNewApiIfNecessary()V

    #@97
    .line 374
    return-void
.end method

.method private CacheCDMACarrier()V
    .registers 5

    #@0
    .prologue
    .line 1143
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mTelMgr:Landroid/telephony/TelephonyManager;

    #@2
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    #@5
    move-result v0

    #@6
    const/4 v1, 0x2

    #@7
    if-eq v0, v1, :cond_a

    #@9
    .line 1167
    :cond_9
    :goto_9
    return-void

    #@a
    .line 1146
    :cond_a
    sget-object v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mCDMAHomeCarrier:Ljava/lang/String;

    #@c
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_9

    #@12
    .line 1149
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@14
    const-string v1, "XTWiFiOS"

    #@16
    new-instance v2, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v3, "CacheCDMACarrier: ServiceState: "

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    iget v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mCurrentServiceState:I

    #@23
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    const-string v3, "Roaming: "

    #@29
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mTelMgr:Landroid/telephony/TelephonyManager;

    #@2f
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    #@32
    move-result v3

    #@33
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v2

    #@3b
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@3e
    .line 1150
    iget v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mCurrentServiceState:I

    #@40
    if-nez v0, :cond_9

    #@42
    .line 1152
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mTelMgr:Landroid/telephony/TelephonyManager;

    #@44
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    #@47
    move-result v0

    #@48
    if-nez v0, :cond_90

    #@4a
    .line 1154
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mTelMgr:Landroid/telephony/TelephonyManager;

    #@4c
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    #@4f
    move-result-object v0

    #@50
    sput-object v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mCDMAHomeCarrier:Ljava/lang/String;

    #@52
    .line 1155
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@54
    const-string v1, "XTWiFiOS"

    #@56
    new-instance v2, Ljava/lang/StringBuilder;

    #@58
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5b
    const-string v3, "Operator name = "

    #@5d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v2

    #@61
    sget-object v3, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mCDMAHomeCarrier:Ljava/lang/String;

    #@63
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v2

    #@67
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6a
    move-result-object v2

    #@6b
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@6e
    .line 1156
    sget-object v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mCDMAHomeCarrier:Ljava/lang/String;

    #@70
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    #@73
    move-result v0

    #@74
    if-nez v0, :cond_9

    #@76
    .line 1161
    sget-object v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mCDMAHomeCarrier:Ljava/lang/String;

    #@78
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@7b
    move-result-object v0

    #@7c
    const-string v1, "indicator"

    #@7e
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@81
    move-result v0

    #@82
    if-nez v0, :cond_8c

    #@84
    sget-object v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mCDMAHomeCarrier:Ljava/lang/String;

    #@86
    invoke-direct {p0, v0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->isNumeric(Ljava/lang/String;)Z

    #@89
    move-result v0

    #@8a
    if-eqz v0, :cond_90

    #@8c
    .line 1162
    :cond_8c
    const-string v0, ""

    #@8e
    sput-object v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mCDMAHomeCarrier:Ljava/lang/String;

    #@90
    .line 1164
    :cond_90
    sget-object v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mCDMAHomeCarrier:Ljava/lang/String;

    #@92
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    #@95
    move-result v0

    #@96
    if-nez v0, :cond_9

    #@98
    .line 1165
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@9a
    const-string v1, "XTWiFiOS"

    #@9c
    new-instance v2, Ljava/lang/StringBuilder;

    #@9e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a1
    const-string v3, "Got CDMA Carrier Name = "

    #@a3
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v2

    #@a7
    sget-object v3, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mCDMAHomeCarrier:Ljava/lang/String;

    #@a9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v2

    #@ad
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b0
    move-result-object v2

    #@b1
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@b4
    goto/16 :goto_9
.end method

.method private FillCellUpdateInfo(Ljava/lang/String;ILandroid/telephony/CellLocation;Z)V
    .registers 42
    .parameter "str_from"
    .parameter "tx_id"
    .parameter "location"
    .parameter "update"

    #@0
    .prologue
    .line 934
    if-eqz p4, :cond_f

    #@2
    :try_start_2
    move-object/from16 v0, p0

    #@4
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mRegisteredClientList:Ljava/util/Map;

    #@6
    move-object/from16 v33, v0

    #@8
    invoke-interface/range {v33 .. v33}, Ljava/util/Map;->isEmpty()Z

    #@b
    move-result v33

    #@c
    if-eqz v33, :cond_f

    #@e
    .line 1138
    :cond_e
    :goto_e
    return-void

    #@f
    .line 938
    :cond_f
    new-instance v23, Lcom/qualcomm/lib/location/mq_client/OutPostcard;

    #@11
    invoke-direct/range {v23 .. v23}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;-><init>()V

    #@14
    .line 939
    .local v23, out:Lcom/qualcomm/lib/location/mq_client/OutPostcard;
    invoke-virtual/range {v23 .. v23}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->init()V

    #@17
    .line 941
    move-object/from16 v0, p0

    #@19
    iget v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mCurrentServiceState:I

    #@1b
    move/from16 v33, v0

    #@1d
    const/16 v34, 0x1

    #@1f
    move/from16 v0, v33

    #@21
    move/from16 v1, v34

    #@23
    if-eq v0, v1, :cond_41

    #@25
    move-object/from16 v0, p0

    #@27
    iget v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mCurrentServiceState:I

    #@29
    move/from16 v33, v0

    #@2b
    const/16 v34, 0x3

    #@2d
    move/from16 v0, v33

    #@2f
    move/from16 v1, v34

    #@31
    if-eq v0, v1, :cond_41

    #@33
    move-object/from16 v0, p0

    #@35
    iget v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mCurrentServiceState:I

    #@37
    move/from16 v33, v0

    #@39
    const/16 v34, 0x2

    #@3b
    move/from16 v0, v33

    #@3d
    move/from16 v1, v34

    #@3f
    if-ne v0, v1, :cond_11f

    #@41
    .line 945
    :cond_41
    const-string v33, "NETWORK-STATUS"

    #@43
    const-string v34, "OOO"

    #@45
    move-object/from16 v0, v23

    #@47
    move-object/from16 v1, v33

    #@49
    move-object/from16 v2, v34

    #@4b
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@4e
    .line 1091
    :goto_4e
    invoke-virtual/range {v23 .. v23}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->seal()V

    #@51
    .line 1093
    if-eqz p4, :cond_4d9

    #@53
    .line 1094
    move-object/from16 v0, p0

    #@55
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mRegisteredClientList:Ljava/util/Map;

    #@57
    move-object/from16 v33, v0

    #@59
    invoke-interface/range {v33 .. v33}, Ljava/util/Map;->keySet()Ljava/util/Set;

    #@5c
    move-result-object v33

    #@5d
    invoke-interface/range {v33 .. v33}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@60
    move-result-object v17

    #@61
    .line 1096
    .local v17, iter:Ljava/util/Iterator;
    :goto_61
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    #@64
    move-result v33

    #@65
    if-eqz v33, :cond_e

    #@67
    .line 1097
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@6a
    move-result-object v28

    #@6b
    check-cast v28, Ljava/lang/String;

    #@6d
    .line 1098
    .local v28, strlistener:Ljava/lang/String;
    move-object/from16 v0, p0

    #@6f
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mRegisteredClientList:Ljava/util/Map;

    #@71
    move-object/from16 v33, v0

    #@73
    move-object/from16 v0, v33

    #@75
    move-object/from16 v1, v28

    #@77
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@7a
    move-result-object v32

    #@7b
    check-cast v32, Ljava/lang/Integer;

    #@7d
    .line 1100
    .local v32, txid:Ljava/lang/Integer;
    move-object/from16 v0, p0

    #@7f
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@81
    move-object/from16 v33, v0

    #@83
    const-string v34, "XTWiFiOS"

    #@85
    new-instance v35, Ljava/lang/StringBuilder;

    #@87
    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    #@8a
    const-string v36, "Send cell info to client "

    #@8c
    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v35

    #@90
    move-object/from16 v0, v35

    #@92
    move-object/from16 v1, v28

    #@94
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v35

    #@98
    const-string v36, "with txid = "

    #@9a
    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v35

    #@9e
    move-object/from16 v0, v35

    #@a0
    move-object/from16 v1, v32

    #@a2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v35

    #@a6
    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a9
    move-result-object v35

    #@aa
    invoke-virtual/range {v33 .. v35}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@ad
    .line 1102
    new-instance v8, Lcom/qualcomm/lib/location/mq_client/OutPostcard;

    #@af
    invoke-direct {v8}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;-><init>()V

    #@b2
    .line 1103
    .local v8, card_for_listener:Lcom/qualcomm/lib/location/mq_client/OutPostcard;
    invoke-virtual {v8}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->init()V

    #@b5
    .line 1104
    const-string v33, "TO"

    #@b7
    move-object/from16 v0, v33

    #@b9
    move-object/from16 v1, v28

    #@bb
    invoke-virtual {v8, v0, v1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@be
    .line 1105
    const-string v33, "FROM"

    #@c0
    const-string v34, "OS-Agent"

    #@c2
    move-object/from16 v0, v33

    #@c4
    move-object/from16 v1, v34

    #@c6
    invoke-virtual {v8, v0, v1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@c9
    .line 1106
    const-string v33, "TX-ID"

    #@cb
    invoke-virtual/range {v32 .. v32}, Ljava/lang/Integer;->intValue()I

    #@ce
    move-result v34

    #@cf
    move/from16 v0, v34

    #@d1
    int-to-long v0, v0

    #@d2
    move-wide/from16 v34, v0

    #@d4
    move-object/from16 v0, v33

    #@d6
    move-wide/from16 v1, v34

    #@d8
    invoke-virtual {v8, v0, v1, v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addInt32(Ljava/lang/String;J)V

    #@db
    .line 1107
    const-string v33, "INFO"

    #@dd
    const-string v34, "RIL_UPDATE"

    #@df
    move-object/from16 v0, v33

    #@e1
    move-object/from16 v1, v34

    #@e3
    invoke-virtual {v8, v0, v1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@e6
    .line 1108
    const-string v33, "RIL-CELL-UPDATE"

    #@e8
    move-object/from16 v0, v33

    #@ea
    move-object/from16 v1, v23

    #@ec
    invoke-virtual {v8, v0, v1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addCard(Ljava/lang/String;Lcom/qualcomm/lib/location/mq_client/OutPostcard;)V

    #@ef
    .line 1110
    invoke-virtual {v8}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->seal()V

    #@f2
    .line 1111
    move-object/from16 v0, p0

    #@f4
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->m_ipcClient:Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@f6
    move-object/from16 v33, v0

    #@f8
    invoke-virtual/range {v33 .. v33}, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->getOutputStream()Ljava/io/OutputStream;

    #@fb
    move-result-object v22

    #@fc
    .line 1112
    .local v22, os:Ljava/io/OutputStream;
    invoke-virtual {v8}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->getEncodedBuffer()[B

    #@ff
    move-result-object v33

    #@100
    move-object/from16 v0, v22

    #@102
    move-object/from16 v1, v33

    #@104
    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    #@107
    .line 1113
    invoke-virtual/range {v22 .. v22}, Ljava/io/OutputStream;->flush()V
    :try_end_10a
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_10a} :catch_10c

    #@10a
    goto/16 :goto_61

    #@10c
    .line 1133
    .end local v8           #card_for_listener:Lcom/qualcomm/lib/location/mq_client/OutPostcard;
    .end local v17           #iter:Ljava/util/Iterator;
    .end local v22           #os:Ljava/io/OutputStream;
    .end local v23           #out:Lcom/qualcomm/lib/location/mq_client/OutPostcard;
    .end local v28           #strlistener:Ljava/lang/String;
    .end local v32           #txid:Ljava/lang/Integer;
    :catch_10c
    move-exception v12

    #@10d
    .line 1135
    .local v12, e:Ljava/lang/Exception;
    move-object/from16 v0, p0

    #@10f
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@111
    move-object/from16 v33, v0

    #@113
    const-string v34, "XTWiFiOS"

    #@115
    const-string v35, "Cannot generate RIL Cell Update Information"

    #@117
    invoke-virtual/range {v33 .. v35}, Lcom/qualcomm/lib/location/log/LocLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@11a
    .line 1136
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V

    #@11d
    goto/16 :goto_e

    #@11f
    .line 950
    .end local v12           #e:Ljava/lang/Exception;
    .restart local v23       #out:Lcom/qualcomm/lib/location/mq_client/OutPostcard;
    :cond_11f
    :try_start_11f
    move-object/from16 v0, p0

    #@121
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mTelMgr:Landroid/telephony/TelephonyManager;

    #@123
    move-object/from16 v33, v0

    #@125
    invoke-virtual/range {v33 .. v33}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    #@128
    move-result v20

    #@129
    .line 951
    .local v20, nw_status:Z
    const-string v34, "NETWORK-STATUS"

    #@12b
    if-eqz v20, :cond_1cf

    #@12d
    const-string v33, "ROAMING"

    #@12f
    :goto_12f
    move-object/from16 v0, v23

    #@131
    move-object/from16 v1, v34

    #@133
    move-object/from16 v2, v33

    #@135
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@138
    .line 952
    move-object/from16 v0, p0

    #@13a
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@13c
    move-object/from16 v33, v0

    #@13e
    const-string v34, "XTWiFiOS"

    #@140
    new-instance v35, Ljava/lang/StringBuilder;

    #@142
    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    #@145
    const-string v36, "Network Status: "

    #@147
    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14a
    move-result-object v35

    #@14b
    move-object/from16 v0, v35

    #@14d
    move/from16 v1, v20

    #@14f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@152
    move-result-object v35

    #@153
    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@156
    move-result-object v35

    #@157
    invoke-virtual/range {v33 .. v35}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@15a
    .line 956
    move-object/from16 v0, p3

    #@15c
    instance-of v0, v0, Landroid/telephony/gsm/GsmCellLocation;

    #@15e
    move/from16 v33, v0

    #@160
    if-eqz v33, :cond_348

    #@162
    .line 957
    move-object/from16 v0, p3

    #@164
    check-cast v0, Landroid/telephony/gsm/GsmCellLocation;

    #@166
    move-object v13, v0

    #@167
    .line 958
    .local v13, gsmCell:Landroid/telephony/gsm/GsmCellLocation;
    const/16 v24, -0x1

    #@169
    .line 959
    .local v24, psc:I
    const/4 v10, -0x1

    #@16a
    .line 960
    .local v10, cid:I
    const/16 v18, -0x1

    #@16c
    .line 961
    .local v18, lac:I
    const/4 v14, 0x0

    #@16d
    .line 962
    .local v14, iMcc:I
    const/4 v15, 0x0

    #@16e
    .line 964
    .local v15, iMnc:I
    const-string v25, ""

    #@170
    .line 965
    .local v25, rilTechType:Ljava/lang/String;
    move-object/from16 v0, p0

    #@172
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mTelMgr:Landroid/telephony/TelephonyManager;

    #@174
    move-object/from16 v33, v0

    #@176
    invoke-virtual/range {v33 .. v33}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    #@179
    move-result v21

    #@17a
    .line 966
    .local v21, nwtype:I
    packed-switch v21, :pswitch_data_564

    #@17d
    .line 999
    :goto_17d
    :pswitch_17d
    invoke-virtual {v13}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    #@180
    move-result v10

    #@181
    .line 1000
    invoke-virtual {v13}, Landroid/telephony/gsm/GsmCellLocation;->getLac()I

    #@184
    move-result v18

    #@185
    .line 1005
    const/16 v33, -0x1

    #@187
    move/from16 v0, v33

    #@189
    if-ne v10, v0, :cond_221

    #@18b
    .line 1007
    move-object/from16 v0, p0

    #@18d
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@18f
    move-object/from16 v33, v0

    #@191
    const-string v34, "XTWiFiOS"

    #@193
    new-instance v35, Ljava/lang/StringBuilder;

    #@195
    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    #@198
    const-string v36, "RIL-TECH-TYPE = "

    #@19a
    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19d
    move-result-object v35

    #@19e
    move-object/from16 v0, v35

    #@1a0
    move-object/from16 v1, v25

    #@1a2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a5
    move-result-object v35

    #@1a6
    const-string v36, " CID = "

    #@1a8
    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ab
    move-result-object v35

    #@1ac
    move-object/from16 v0, v35

    #@1ae
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b1
    move-result-object v35

    #@1b2
    const-string v36, " LAC = "

    #@1b4
    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b7
    move-result-object v35

    #@1b8
    move-object/from16 v0, v35

    #@1ba
    move/from16 v1, v18

    #@1bc
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1bf
    move-result-object v35

    #@1c0
    const-string v36, " ignoring, cell change"

    #@1c2
    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c5
    move-result-object v35

    #@1c6
    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c9
    move-result-object v35

    #@1ca
    invoke-virtual/range {v33 .. v35}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@1cd
    goto/16 :goto_e

    #@1cf
    .line 951
    .end local v10           #cid:I
    .end local v13           #gsmCell:Landroid/telephony/gsm/GsmCellLocation;
    .end local v14           #iMcc:I
    .end local v15           #iMnc:I
    .end local v18           #lac:I
    .end local v21           #nwtype:I
    .end local v24           #psc:I
    .end local v25           #rilTechType:Ljava/lang/String;
    :cond_1cf
    const-string v33, "HOME"

    #@1d1
    goto/16 :goto_12f

    #@1d3
    .line 970
    .restart local v10       #cid:I
    .restart local v13       #gsmCell:Landroid/telephony/gsm/GsmCellLocation;
    .restart local v14       #iMcc:I
    .restart local v15       #iMnc:I
    .restart local v18       #lac:I
    .restart local v21       #nwtype:I
    .restart local v24       #psc:I
    .restart local v25       #rilTechType:Ljava/lang/String;
    :pswitch_1d3
    const-string v33, "RIL-TECH-TYPE"

    #@1d5
    const-string v34, "GSM"

    #@1d7
    move-object/from16 v0, v23

    #@1d9
    move-object/from16 v1, v33

    #@1db
    move-object/from16 v2, v34

    #@1dd
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@1e0
    .line 971
    const-string v25, "GSM"

    #@1e2
    .line 972
    goto :goto_17d

    #@1e3
    .line 979
    :pswitch_1e3
    const-string v33, "RIL-TECH-TYPE"

    #@1e5
    const-string v34, "WCDMA"

    #@1e7
    move-object/from16 v0, v23

    #@1e9
    move-object/from16 v1, v33

    #@1eb
    move-object/from16 v2, v34

    #@1ed
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@1f0
    .line 980
    const-string v25, "WCDMA"

    #@1f2
    .line 981
    goto :goto_17d

    #@1f3
    .line 983
    :pswitch_1f3
    invoke-virtual {v13}, Landroid/telephony/gsm/GsmCellLocation;->getPsc()I

    #@1f6
    move-result v24

    #@1f7
    .line 984
    const/16 v33, -0x1

    #@1f9
    move/from16 v0, v24

    #@1fb
    move/from16 v1, v33

    #@1fd
    if-ne v0, v1, :cond_210

    #@1ff
    .line 986
    const-string v33, "RIL-TECH-TYPE"

    #@201
    const-string v34, "GSM"

    #@203
    move-object/from16 v0, v23

    #@205
    move-object/from16 v1, v33

    #@207
    move-object/from16 v2, v34

    #@209
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@20c
    .line 987
    const-string v25, "GSM"

    #@20e
    goto/16 :goto_17d

    #@210
    .line 991
    :cond_210
    const-string v33, "RIL-TECH-TYPE"

    #@212
    const-string v34, "WCDMA"

    #@214
    move-object/from16 v0, v23

    #@216
    move-object/from16 v1, v33

    #@218
    move-object/from16 v2, v34

    #@21a
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@21d
    .line 992
    const-string v25, "WCDMA"

    #@21f
    .line 994
    goto/16 :goto_17d

    #@221
    .line 1010
    :cond_221
    const-string v33, "GSM"

    #@223
    move-object/from16 v0, v25

    #@225
    move-object/from16 v1, v33

    #@227
    if-ne v0, v1, :cond_275

    #@229
    const/16 v33, -0x1

    #@22b
    move/from16 v0, v18

    #@22d
    move/from16 v1, v33

    #@22f
    if-ne v0, v1, :cond_275

    #@231
    .line 1012
    move-object/from16 v0, p0

    #@233
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@235
    move-object/from16 v33, v0

    #@237
    const-string v34, "XTWiFiOS"

    #@239
    new-instance v35, Ljava/lang/StringBuilder;

    #@23b
    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    #@23e
    const-string v36, "RIL-TECH-TYPE = "

    #@240
    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@243
    move-result-object v35

    #@244
    move-object/from16 v0, v35

    #@246
    move-object/from16 v1, v25

    #@248
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24b
    move-result-object v35

    #@24c
    const-string v36, " CID = "

    #@24e
    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@251
    move-result-object v35

    #@252
    move-object/from16 v0, v35

    #@254
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@257
    move-result-object v35

    #@258
    const-string v36, " LAC = "

    #@25a
    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25d
    move-result-object v35

    #@25e
    move-object/from16 v0, v35

    #@260
    move/from16 v1, v18

    #@262
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@265
    move-result-object v35

    #@266
    const-string v36, " ignoring, cell change"

    #@268
    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26b
    move-result-object v35

    #@26c
    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26f
    move-result-object v35

    #@270
    invoke-virtual/range {v33 .. v35}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@273
    goto/16 :goto_e

    #@275
    .line 1016
    :cond_275
    const-string v33, "CELL-ID"

    #@277
    int-to-long v0, v10

    #@278
    move-wide/from16 v34, v0

    #@27a
    move-object/from16 v0, v23

    #@27c
    move-object/from16 v1, v33

    #@27e
    move-wide/from16 v2, v34

    #@280
    invoke-virtual {v0, v1, v2, v3}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addUInt32(Ljava/lang/String;J)V

    #@283
    .line 1017
    const-string v33, "LOCATION-AREA-CODE"

    #@285
    move-object/from16 v0, v23

    #@287
    move-object/from16 v1, v33

    #@289
    move/from16 v2, v18

    #@28b
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addUInt16(Ljava/lang/String;I)V

    #@28e
    .line 1020
    move-object/from16 v0, p0

    #@290
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mTelMgr:Landroid/telephony/TelephonyManager;

    #@292
    move-object/from16 v33, v0

    #@294
    invoke-virtual/range {v33 .. v33}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    #@297
    move-result-object v27

    #@298
    .line 1021
    .local v27, strNetworkOperator:Ljava/lang/String;
    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->isEmpty()Z

    #@29b
    move-result v33

    #@29c
    if-nez v33, :cond_2d6

    #@29e
    .line 1022
    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@2a1
    move-result v33

    #@2a2
    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->length()I

    #@2a5
    move-result v34

    #@2a6
    move-object/from16 v0, p0

    #@2a8
    move/from16 v1, v33

    #@2aa
    move/from16 v2, v34

    #@2ac
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->getMcc(II)I

    #@2af
    move-result v14

    #@2b0
    .line 1023
    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@2b3
    move-result v33

    #@2b4
    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->length()I

    #@2b7
    move-result v34

    #@2b8
    move-object/from16 v0, p0

    #@2ba
    move/from16 v1, v33

    #@2bc
    move/from16 v2, v34

    #@2be
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->getMnc(II)I

    #@2c1
    move-result v15

    #@2c2
    .line 1025
    if-eqz v14, :cond_2cd

    #@2c4
    .line 1026
    const-string v33, "MOBILE-COUNTRY-CODE"

    #@2c6
    move-object/from16 v0, v23

    #@2c8
    move-object/from16 v1, v33

    #@2ca
    invoke-virtual {v0, v1, v14}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addUInt16(Ljava/lang/String;I)V

    #@2cd
    .line 1028
    :cond_2cd
    const-string v33, "MOBILE-NETWORK-CODE"

    #@2cf
    move-object/from16 v0, v23

    #@2d1
    move-object/from16 v1, v33

    #@2d3
    invoke-virtual {v0, v1, v15}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addUInt16(Ljava/lang/String;I)V

    #@2d6
    .line 1031
    :cond_2d6
    move-object/from16 v0, p0

    #@2d8
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@2da
    move-object/from16 v33, v0

    #@2dc
    const-string v34, "XTWiFiOS"

    #@2de
    new-instance v35, Ljava/lang/StringBuilder;

    #@2e0
    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    #@2e3
    const-string v36, "NWTYPE = "

    #@2e5
    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e8
    move-result-object v35

    #@2e9
    move-object/from16 v0, v35

    #@2eb
    move/from16 v1, v21

    #@2ed
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f0
    move-result-object v35

    #@2f1
    const-string v36, " PSC = "

    #@2f3
    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f6
    move-result-object v35

    #@2f7
    move-object/from16 v0, v35

    #@2f9
    move/from16 v1, v24

    #@2fb
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2fe
    move-result-object v35

    #@2ff
    const-string v36, "CID = "

    #@301
    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@304
    move-result-object v35

    #@305
    move-object/from16 v0, v35

    #@307
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30a
    move-result-object v35

    #@30b
    const-string v36, " LAC = "

    #@30d
    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@310
    move-result-object v35

    #@311
    move-object/from16 v0, v35

    #@313
    move/from16 v1, v18

    #@315
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@318
    move-result-object v35

    #@319
    const-string v36, " Network Operator = "

    #@31b
    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31e
    move-result-object v35

    #@31f
    move-object/from16 v0, v35

    #@321
    move-object/from16 v1, v27

    #@323
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@326
    move-result-object v35

    #@327
    const-string v36, " Network Mcc = "

    #@329
    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32c
    move-result-object v35

    #@32d
    move-object/from16 v0, v35

    #@32f
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@332
    move-result-object v35

    #@333
    const-string v36, " Network Mnc = "

    #@335
    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@338
    move-result-object v35

    #@339
    move-object/from16 v0, v35

    #@33b
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@33e
    move-result-object v35

    #@33f
    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@342
    move-result-object v35

    #@343
    invoke-virtual/range {v33 .. v35}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@346
    goto/16 :goto_4e

    #@348
    .line 1034
    .end local v10           #cid:I
    .end local v13           #gsmCell:Landroid/telephony/gsm/GsmCellLocation;
    .end local v14           #iMcc:I
    .end local v15           #iMnc:I
    .end local v18           #lac:I
    .end local v21           #nwtype:I
    .end local v24           #psc:I
    .end local v25           #rilTechType:Ljava/lang/String;
    .end local v27           #strNetworkOperator:Ljava/lang/String;
    :cond_348
    move-object/from16 v0, p3

    #@34a
    instance-of v0, v0, Landroid/telephony/cdma/CdmaCellLocation;

    #@34c
    move/from16 v33, v0

    #@34e
    if-eqz v33, :cond_4ca

    #@350
    .line 1035
    const-string v33, "RIL-TECH-TYPE"

    #@352
    const-string v34, "CDMA"

    #@354
    move-object/from16 v0, v23

    #@356
    move-object/from16 v1, v33

    #@358
    move-object/from16 v2, v34

    #@35a
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@35d
    .line 1037
    move-object/from16 v0, p3

    #@35f
    check-cast v0, Landroid/telephony/cdma/CdmaCellLocation;

    #@361
    move-object v9, v0

    #@362
    .line 1038
    .local v9, cdmaCell:Landroid/telephony/cdma/CdmaCellLocation;
    const/16 v26, -0x1

    #@364
    .local v26, sid:I
    const/16 v19, -0x1

    #@366
    .local v19, nid:I
    const/4 v5, -0x1

    #@367
    .local v5, bsid:I
    const/4 v6, -0x1

    #@368
    .local v6, bslat:I
    const/4 v7, -0x1

    #@369
    .local v7, bslong:I
    const/4 v14, 0x0

    #@36a
    .line 1040
    .restart local v14       #iMcc:I
    invoke-virtual {v9}, Landroid/telephony/cdma/CdmaCellLocation;->getSystemId()I

    #@36d
    move-result v26

    #@36e
    .line 1041
    invoke-virtual {v9}, Landroid/telephony/cdma/CdmaCellLocation;->getNetworkId()I

    #@371
    move-result v19

    #@372
    .line 1042
    invoke-virtual {v9}, Landroid/telephony/cdma/CdmaCellLocation;->getBaseStationId()I

    #@375
    move-result v5

    #@376
    .line 1043
    invoke-virtual {v9}, Landroid/telephony/cdma/CdmaCellLocation;->getBaseStationLatitude()I

    #@379
    move-result v6

    #@37a
    .line 1044
    invoke-virtual {v9}, Landroid/telephony/cdma/CdmaCellLocation;->getBaseStationLongitude()I

    #@37d
    move-result v7

    #@37e
    .line 1046
    const/16 v33, -0x1

    #@380
    move/from16 v0, v26

    #@382
    move/from16 v1, v33

    #@384
    if-eq v0, v1, :cond_394

    #@386
    const/16 v33, -0x1

    #@388
    move/from16 v0, v19

    #@38a
    move/from16 v1, v33

    #@38c
    if-eq v0, v1, :cond_394

    #@38e
    const/16 v33, -0x1

    #@390
    move/from16 v0, v33

    #@392
    if-ne v5, v0, :cond_3d8

    #@394
    .line 1048
    :cond_394
    move-object/from16 v0, p0

    #@396
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@398
    move-object/from16 v33, v0

    #@39a
    const-string v34, "XTWiFiOS"

    #@39c
    new-instance v35, Ljava/lang/StringBuilder;

    #@39e
    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    #@3a1
    const-string v36, "RIL-TECH-TYPE = CDMA SID = "

    #@3a3
    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a6
    move-result-object v35

    #@3a7
    move-object/from16 v0, v35

    #@3a9
    move/from16 v1, v26

    #@3ab
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3ae
    move-result-object v35

    #@3af
    const-string v36, " NID = "

    #@3b1
    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b4
    move-result-object v35

    #@3b5
    move-object/from16 v0, v35

    #@3b7
    move/from16 v1, v19

    #@3b9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3bc
    move-result-object v35

    #@3bd
    const-string v36, " BSID = "

    #@3bf
    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c2
    move-result-object v35

    #@3c3
    move-object/from16 v0, v35

    #@3c5
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3c8
    move-result-object v35

    #@3c9
    const-string v36, " ignoring, cell change"

    #@3cb
    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3ce
    move-result-object v35

    #@3cf
    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d2
    move-result-object v35

    #@3d3
    invoke-virtual/range {v33 .. v35}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@3d6
    goto/16 :goto_e

    #@3d8
    .line 1052
    :cond_3d8
    const-string v33, "SYSTEM-ID"

    #@3da
    move-object/from16 v0, v23

    #@3dc
    move-object/from16 v1, v33

    #@3de
    move/from16 v2, v26

    #@3e0
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addUInt16(Ljava/lang/String;I)V

    #@3e3
    .line 1053
    const-string v33, "NETWORK-ID"

    #@3e5
    move-object/from16 v0, v23

    #@3e7
    move-object/from16 v1, v33

    #@3e9
    move/from16 v2, v19

    #@3eb
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addUInt16(Ljava/lang/String;I)V

    #@3ee
    .line 1054
    const-string v33, "BASE-STATION-ID"

    #@3f0
    int-to-long v0, v5

    #@3f1
    move-wide/from16 v34, v0

    #@3f3
    move-object/from16 v0, v23

    #@3f5
    move-object/from16 v1, v33

    #@3f7
    move-wide/from16 v2, v34

    #@3f9
    invoke-virtual {v0, v1, v2, v3}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addUInt32(Ljava/lang/String;J)V

    #@3fc
    .line 1056
    const v33, 0x7fffffff

    #@3ff
    move/from16 v0, v33

    #@401
    if-eq v6, v0, :cond_411

    #@403
    .line 1057
    const-string v33, "BASE-STATION-LATITUDE"

    #@405
    int-to-long v0, v6

    #@406
    move-wide/from16 v34, v0

    #@408
    move-object/from16 v0, v23

    #@40a
    move-object/from16 v1, v33

    #@40c
    move-wide/from16 v2, v34

    #@40e
    invoke-virtual {v0, v1, v2, v3}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addInt32(Ljava/lang/String;J)V

    #@411
    .line 1058
    :cond_411
    const v33, 0x7fffffff

    #@414
    move/from16 v0, v33

    #@416
    if-eq v7, v0, :cond_426

    #@418
    .line 1059
    const-string v33, "BASE-STATION-LONGITUDE"

    #@41a
    int-to-long v0, v7

    #@41b
    move-wide/from16 v34, v0

    #@41d
    move-object/from16 v0, v23

    #@41f
    move-object/from16 v1, v33

    #@421
    move-wide/from16 v2, v34

    #@423
    invoke-virtual {v0, v1, v2, v3}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addInt32(Ljava/lang/String;J)V

    #@426
    .line 1062
    :cond_426
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    #@429
    move-result-object v31

    #@42a
    .line 1063
    .local v31, timezone:Ljava/util/TimeZone;
    new-instance v11, Ljava/util/Date;

    #@42c
    invoke-direct {v11}, Ljava/util/Date;-><init>()V

    #@42f
    .line 1064
    .local v11, datenow:Ljava/util/Date;
    move-object/from16 v0, v31

    #@431
    invoke-virtual {v0, v11}, Ljava/util/TimeZone;->inDaylightTime(Ljava/util/Date;)Z

    #@434
    move-result v16

    #@435
    .line 1066
    .local v16, inDST:Z
    invoke-virtual {v11}, Ljava/util/Date;->getTime()J

    #@438
    move-result-wide v29

    #@439
    .line 1067
    .local v29, timenow:J
    move-object/from16 v0, v31

    #@43b
    move-wide/from16 v1, v29

    #@43d
    invoke-virtual {v0, v1, v2}, Ljava/util/TimeZone;->getOffset(J)I

    #@440
    move-result v4

    #@441
    .line 1069
    .local v4, UTCTimeOffset:I
    const-string v33, "IN-DAY-LIGHT-SAVING"

    #@443
    move-object/from16 v0, v23

    #@445
    move-object/from16 v1, v33

    #@447
    move/from16 v2, v16

    #@449
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addBool(Ljava/lang/String;Z)V

    #@44c
    .line 1070
    const-string v33, "TIME-ZONE-OFFSET"

    #@44e
    int-to-long v0, v4

    #@44f
    move-wide/from16 v34, v0

    #@451
    move-object/from16 v0, v23

    #@453
    move-object/from16 v1, v33

    #@455
    move-wide/from16 v2, v34

    #@457
    invoke-virtual {v0, v1, v2, v3}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addInt32(Ljava/lang/String;J)V

    #@45a
    .line 1081
    move-object/from16 v0, p0

    #@45c
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@45e
    move-object/from16 v33, v0

    #@460
    const-string v34, "XTWiFiOS"

    #@462
    new-instance v35, Ljava/lang/StringBuilder;

    #@464
    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    #@467
    const-string v36, "CDMA SID = "

    #@469
    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46c
    move-result-object v35

    #@46d
    move-object/from16 v0, v35

    #@46f
    move/from16 v1, v26

    #@471
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@474
    move-result-object v35

    #@475
    const-string v36, " NID = "

    #@477
    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47a
    move-result-object v35

    #@47b
    move-object/from16 v0, v35

    #@47d
    move/from16 v1, v19

    #@47f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@482
    move-result-object v35

    #@483
    const-string v36, " BSID = "

    #@485
    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@488
    move-result-object v35

    #@489
    move-object/from16 v0, v35

    #@48b
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@48e
    move-result-object v35

    #@48f
    const-string v36, " BSLAT = "

    #@491
    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@494
    move-result-object v35

    #@495
    move-object/from16 v0, v35

    #@497
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@49a
    move-result-object v35

    #@49b
    const-string v36, " BSLONG = "

    #@49d
    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a0
    move-result-object v35

    #@4a1
    move-object/from16 v0, v35

    #@4a3
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4a6
    move-result-object v35

    #@4a7
    const-string v36, "DST = "

    #@4a9
    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4ac
    move-result-object v35

    #@4ad
    move-object/from16 v0, v35

    #@4af
    move/from16 v1, v16

    #@4b1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4b4
    move-result-object v35

    #@4b5
    const-string v36, "TimeOffset from UTC = "

    #@4b7
    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4ba
    move-result-object v35

    #@4bb
    move-object/from16 v0, v35

    #@4bd
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4c0
    move-result-object v35

    #@4c1
    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c4
    move-result-object v35

    #@4c5
    invoke-virtual/range {v33 .. v35}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@4c8
    goto/16 :goto_4e

    #@4ca
    .line 1086
    .end local v4           #UTCTimeOffset:I
    .end local v5           #bsid:I
    .end local v6           #bslat:I
    .end local v7           #bslong:I
    .end local v9           #cdmaCell:Landroid/telephony/cdma/CdmaCellLocation;
    .end local v11           #datenow:Ljava/util/Date;
    .end local v14           #iMcc:I
    .end local v16           #inDST:Z
    .end local v19           #nid:I
    .end local v26           #sid:I
    .end local v29           #timenow:J
    .end local v31           #timezone:Ljava/util/TimeZone;
    :cond_4ca
    move-object/from16 v0, p0

    #@4cc
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@4ce
    move-object/from16 v33, v0

    #@4d0
    const-string v34, "XTWiFiOS"

    #@4d2
    const-string v35, "Unknown cell location type"

    #@4d4
    invoke-virtual/range {v33 .. v35}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@4d7
    goto/16 :goto_4e

    #@4d9
    .line 1117
    .end local v20           #nw_status:Z
    :cond_4d9
    move-object/from16 v0, p0

    #@4db
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@4dd
    move-object/from16 v33, v0

    #@4df
    const-string v34, "XTWiFiOS"

    #@4e1
    new-instance v35, Ljava/lang/StringBuilder;

    #@4e3
    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    #@4e6
    const-string v36, "Send cell update resp to client "

    #@4e8
    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4eb
    move-result-object v35

    #@4ec
    move-object/from16 v0, v35

    #@4ee
    move-object/from16 v1, p1

    #@4f0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f3
    move-result-object v35

    #@4f4
    const-string v36, "with txid = "

    #@4f6
    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f9
    move-result-object v35

    #@4fa
    move-object/from16 v0, v35

    #@4fc
    move/from16 v1, p2

    #@4fe
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@501
    move-result-object v35

    #@502
    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@505
    move-result-object v35

    #@506
    invoke-virtual/range {v33 .. v35}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@509
    .line 1119
    new-instance v8, Lcom/qualcomm/lib/location/mq_client/OutPostcard;

    #@50b
    invoke-direct {v8}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;-><init>()V

    #@50e
    .line 1120
    .restart local v8       #card_for_listener:Lcom/qualcomm/lib/location/mq_client/OutPostcard;
    invoke-virtual {v8}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->init()V

    #@511
    .line 1121
    const-string v33, "TO"

    #@513
    move-object/from16 v0, v33

    #@515
    move-object/from16 v1, p1

    #@517
    invoke-virtual {v8, v0, v1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@51a
    .line 1122
    const-string v33, "FROM"

    #@51c
    const-string v34, "OS-Agent"

    #@51e
    move-object/from16 v0, v33

    #@520
    move-object/from16 v1, v34

    #@522
    invoke-virtual {v8, v0, v1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@525
    .line 1123
    const-string v33, "TX-ID"

    #@527
    move/from16 v0, p2

    #@529
    int-to-long v0, v0

    #@52a
    move-wide/from16 v34, v0

    #@52c
    move-object/from16 v0, v33

    #@52e
    move-wide/from16 v1, v34

    #@530
    invoke-virtual {v8, v0, v1, v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addInt32(Ljava/lang/String;J)V

    #@533
    .line 1124
    const-string v33, "RESP"

    #@535
    const-string v34, "RIL_UPDATE"

    #@537
    move-object/from16 v0, v33

    #@539
    move-object/from16 v1, v34

    #@53b
    invoke-virtual {v8, v0, v1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@53e
    .line 1125
    const-string v33, "RIL-CELL-UPDATE"

    #@540
    move-object/from16 v0, v33

    #@542
    move-object/from16 v1, v23

    #@544
    invoke-virtual {v8, v0, v1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addCard(Ljava/lang/String;Lcom/qualcomm/lib/location/mq_client/OutPostcard;)V

    #@547
    .line 1127
    invoke-virtual {v8}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->seal()V

    #@54a
    .line 1128
    move-object/from16 v0, p0

    #@54c
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->m_ipcClient:Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@54e
    move-object/from16 v33, v0

    #@550
    invoke-virtual/range {v33 .. v33}, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->getOutputStream()Ljava/io/OutputStream;

    #@553
    move-result-object v22

    #@554
    .line 1129
    .restart local v22       #os:Ljava/io/OutputStream;
    invoke-virtual {v8}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->getEncodedBuffer()[B

    #@557
    move-result-object v33

    #@558
    move-object/from16 v0, v22

    #@55a
    move-object/from16 v1, v33

    #@55c
    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    #@55f
    .line 1130
    invoke-virtual/range {v22 .. v22}, Ljava/io/OutputStream;->flush()V
    :try_end_562
    .catch Ljava/lang/Exception; {:try_start_11f .. :try_end_562} :catch_10c

    #@562
    goto/16 :goto_e

    #@564
    .line 966
    :pswitch_data_564
    .packed-switch 0x0
        :pswitch_1f3
        :pswitch_1d3
        :pswitch_1d3
        :pswitch_1e3
        :pswitch_17d
        :pswitch_17d
        :pswitch_17d
        :pswitch_17d
        :pswitch_1e3
        :pswitch_1e3
        :pswitch_1e3
        :pswitch_17d
        :pswitch_17d
        :pswitch_17d
        :pswitch_17d
        :pswitch_1e3
        :pswitch_17d
        :pswitch_1e3
    .end packed-switch
.end method

.method private FillServiceInfo(Ljava/lang/String;I)V
    .registers 16
    .parameter "str_from"
    .parameter "tx_id"

    #@0
    .prologue
    .line 1186
    const/4 v2, 0x0

    #@1
    .local v2, iMcc:I
    const/4 v3, 0x0

    #@2
    .line 1187
    .local v3, iMnc:I
    :try_start_2
    new-instance v6, Lcom/qualcomm/lib/location/mq_client/OutPostcard;

    #@4
    invoke-direct {v6}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;-><init>()V

    #@7
    .line 1188
    .local v6, out:Lcom/qualcomm/lib/location/mq_client/OutPostcard;
    invoke-virtual {v6}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->init()V

    #@a
    .line 1194
    iget v9, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mCurrentServiceState:I

    #@c
    const/4 v10, 0x1

    #@d
    if-eq v9, v10, :cond_19

    #@f
    iget v9, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mCurrentServiceState:I

    #@11
    const/4 v10, 0x3

    #@12
    if-eq v9, v10, :cond_19

    #@14
    iget v9, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mCurrentServiceState:I

    #@16
    const/4 v10, 0x2

    #@17
    if-ne v9, v10, :cond_72

    #@19
    .line 1197
    :cond_19
    iget-object v9, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@1b
    const-string v10, "XTWiFiOS"

    #@1d
    new-instance v11, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v12, "Service State = "

    #@24
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v11

    #@28
    iget v12, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mCurrentServiceState:I

    #@2a
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v11

    #@2e
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v11

    #@32
    invoke-virtual {v9, v10, v11}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@35
    .line 1241
    :goto_35
    new-instance v0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;

    #@37
    invoke-direct {v0}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;-><init>()V

    #@3a
    .line 1242
    .local v0, card_for_requester:Lcom/qualcomm/lib/location/mq_client/OutPostcard;
    invoke-virtual {v0}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->init()V

    #@3d
    .line 1243
    const-string v9, "TO"

    #@3f
    invoke-virtual {v0, v9, p1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@42
    .line 1244
    const-string v9, "FROM"

    #@44
    const-string v10, "OS-Agent"

    #@46
    invoke-virtual {v0, v9, v10}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@49
    .line 1245
    const-string v9, "TX-ID"

    #@4b
    int-to-long v10, p2

    #@4c
    invoke-virtual {v0, v9, v10, v11}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addInt32(Ljava/lang/String;J)V

    #@4f
    .line 1246
    const-string v9, "RESP"

    #@51
    const-string v10, "RIL_UPDATE"

    #@53
    invoke-virtual {v0, v9, v10}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@56
    .line 1248
    invoke-virtual {v6}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->seal()V

    #@59
    .line 1249
    const-string v9, "RIL-SERVICE-INFO"

    #@5b
    invoke-virtual {v0, v9, v6}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addCard(Ljava/lang/String;Lcom/qualcomm/lib/location/mq_client/OutPostcard;)V

    #@5e
    .line 1251
    invoke-virtual {v0}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->seal()V

    #@61
    .line 1252
    iget-object v9, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->m_ipcClient:Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@63
    invoke-virtual {v9}, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->getOutputStream()Ljava/io/OutputStream;

    #@66
    move-result-object v5

    #@67
    .line 1253
    .local v5, os:Ljava/io/OutputStream;
    invoke-virtual {v0}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->getEncodedBuffer()[B

    #@6a
    move-result-object v9

    #@6b
    invoke-virtual {v5, v9}, Ljava/io/OutputStream;->write([B)V

    #@6e
    .line 1254
    invoke-virtual {v5}, Ljava/io/OutputStream;->flush()V

    #@71
    .line 1261
    .end local v0           #card_for_requester:Lcom/qualcomm/lib/location/mq_client/OutPostcard;
    .end local v5           #os:Ljava/io/OutputStream;
    .end local v6           #out:Lcom/qualcomm/lib/location/mq_client/OutPostcard;
    :goto_71
    return-void

    #@72
    .line 1200
    .restart local v6       #out:Lcom/qualcomm/lib/location/mq_client/OutPostcard;
    :cond_72
    iget-object v9, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mTelMgr:Landroid/telephony/TelephonyManager;

    #@74
    invoke-virtual {v9}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    #@77
    move-result v4

    #@78
    .line 1201
    .local v4, nw_status:Z
    iget-object v9, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@7a
    const-string v10, "XTWiFiOS"

    #@7c
    new-instance v11, Ljava/lang/StringBuilder;

    #@7e
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@81
    const-string v12, "Network Status: "

    #@83
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v11

    #@87
    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v11

    #@8b
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8e
    move-result-object v11

    #@8f
    invoke-virtual {v9, v10, v11}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@92
    .line 1203
    iget-object v9, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mTelMgr:Landroid/telephony/TelephonyManager;

    #@94
    invoke-virtual {v9}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    #@97
    move-result v7

    #@98
    .line 1205
    .local v7, phoneType:I
    const/4 v9, 0x2

    #@99
    if-ne v7, v9, :cond_f1

    #@9b
    .line 1206
    const-string v9, "CARRIER-AIRINTERFACE-TYPE"

    #@9d
    const-string v10, "CDMA"

    #@9f
    invoke-virtual {v6, v9, v10}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@a2
    .line 1207
    iget-object v9, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@a4
    const-string v10, "XTWiFiOS"

    #@a6
    const-string v11, "Home carrier network is CDMA"

    #@a8
    invoke-virtual {v9, v10, v11}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@ab
    .line 1211
    sget-object v9, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mCDMAHomeCarrier:Ljava/lang/String;

    #@ad
    invoke-virtual {v9}, Ljava/lang/String;->isEmpty()Z

    #@b0
    move-result v9

    #@b1
    if-nez v9, :cond_e6

    #@b3
    .line 1213
    const-string v9, "HOME-CARRIER-NAME"

    #@b5
    sget-object v10, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mCDMAHomeCarrier:Ljava/lang/String;

    #@b7
    invoke-virtual {v6, v9, v10}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@ba
    .line 1214
    iget-object v9, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@bc
    const-string v10, "XTWiFiOS"

    #@be
    new-instance v11, Ljava/lang/StringBuilder;

    #@c0
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@c3
    const-string v12, "CDMA Home Carrier = "

    #@c5
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v11

    #@c9
    sget-object v12, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mCDMAHomeCarrier:Ljava/lang/String;

    #@cb
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v11

    #@cf
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d2
    move-result-object v11

    #@d3
    invoke-virtual {v9, v10, v11}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_d6
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_d6} :catch_d8

    #@d6
    goto/16 :goto_35

    #@d8
    .line 1256
    .end local v4           #nw_status:Z
    .end local v6           #out:Lcom/qualcomm/lib/location/mq_client/OutPostcard;
    .end local v7           #phoneType:I
    :catch_d8
    move-exception v1

    #@d9
    .line 1258
    .local v1, e:Ljava/lang/Exception;
    iget-object v9, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@db
    const-string v10, "XTWiFiOS"

    #@dd
    const-string v11, "Cannot generate RIL Service Information"

    #@df
    invoke-virtual {v9, v10, v11}, Lcom/qualcomm/lib/location/log/LocLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@e2
    .line 1259
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    #@e5
    goto :goto_71

    #@e6
    .line 1217
    .end local v1           #e:Ljava/lang/Exception;
    .restart local v4       #nw_status:Z
    .restart local v6       #out:Lcom/qualcomm/lib/location/mq_client/OutPostcard;
    .restart local v7       #phoneType:I
    :cond_e6
    :try_start_e6
    iget-object v9, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@e8
    const-string v10, "XTWiFiOS"

    #@ea
    const-string v11, "Unknown CDMA Home Carrier"

    #@ec
    invoke-virtual {v9, v10, v11}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@ef
    goto/16 :goto_35

    #@f1
    .line 1219
    :cond_f1
    const/4 v9, 0x1

    #@f2
    if-ne v7, v9, :cond_164

    #@f4
    .line 1220
    const-string v9, "CARRIER-AIRINTERFACE-TYPE"

    #@f6
    const-string v10, "GSM"

    #@f8
    invoke-virtual {v6, v9, v10}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@fb
    .line 1221
    iget-object v9, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@fd
    const-string v10, "XTWiFiOS"

    #@ff
    const-string v11, "Home carrier is G/W/L"

    #@101
    invoke-virtual {v9, v10, v11}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@104
    .line 1223
    iget-object v9, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mTelMgr:Landroid/telephony/TelephonyManager;

    #@106
    invoke-virtual {v9}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    #@109
    move-result-object v8

    #@10a
    .line 1224
    .local v8, strSimOperator:Ljava/lang/String;
    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    #@10d
    move-result v9

    #@10e
    if-nez v9, :cond_128

    #@110
    .line 1225
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@113
    move-result v9

    #@114
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    #@117
    move-result v10

    #@118
    invoke-direct {p0, v9, v10}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->getMcc(II)I

    #@11b
    move-result v2

    #@11c
    .line 1226
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@11f
    move-result v9

    #@120
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    #@123
    move-result v10

    #@124
    invoke-direct {p0, v9, v10}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->getMnc(II)I

    #@127
    move-result v3

    #@128
    .line 1229
    :cond_128
    if-eqz v2, :cond_12f

    #@12a
    .line 1230
    const-string v9, "MOBILE-COUNTRY-CODE"

    #@12c
    invoke-virtual {v6, v9, v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addUInt16(Ljava/lang/String;I)V

    #@12f
    .line 1232
    :cond_12f
    const-string v9, "MOBILE-NETWORK-CODE"

    #@131
    invoke-virtual {v6, v9, v3}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addUInt16(Ljava/lang/String;I)V

    #@134
    .line 1233
    iget-object v9, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@136
    const-string v10, "XTWiFiOS"

    #@138
    new-instance v11, Ljava/lang/StringBuilder;

    #@13a
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@13d
    const-string v12, "Operator = "

    #@13f
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@142
    move-result-object v11

    #@143
    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@146
    move-result-object v11

    #@147
    const-string v12, " Carrier Mcc = "

    #@149
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14c
    move-result-object v11

    #@14d
    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@150
    move-result-object v11

    #@151
    const-string v12, " Carrier Mnc = "

    #@153
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@156
    move-result-object v11

    #@157
    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15a
    move-result-object v11

    #@15b
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15e
    move-result-object v11

    #@15f
    invoke-virtual {v9, v10, v11}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@162
    goto/16 :goto_35

    #@164
    .line 1236
    .end local v8           #strSimOperator:Ljava/lang/String;
    :cond_164
    iget-object v9, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@166
    const-string v10, "XTWiFiOS"

    #@168
    new-instance v11, Ljava/lang/StringBuilder;

    #@16a
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@16d
    const-string v12, "Connot determine or unsupported phone type: Phone Type = "

    #@16f
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@172
    move-result-object v11

    #@173
    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@176
    move-result-object v11

    #@177
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17a
    move-result-object v11

    #@17b
    invoke-virtual {v9, v10, v11}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_17e
    .catch Ljava/lang/Exception; {:try_start_e6 .. :try_end_17e} :catch_d8

    #@17e
    goto/16 :goto_35
.end method

.method static synthetic access$000(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Landroid/content/ContentResolver;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 86
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mResolver:Landroid/content/ContentResolver;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Landroid/content/ContentQueryMap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 86
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mSecureSettings:Landroid/content/ContentQueryMap;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;Ljava/lang/String;ILandroid/telephony/CellLocation;Z)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 86
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->FillCellUpdateInfo(Ljava/lang/String;ILandroid/telephony/CellLocation;Z)V

    #@3
    return-void
.end method

.method static synthetic access$102(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;Landroid/content/ContentQueryMap;)Landroid/content/ContentQueryMap;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 86
    iput-object p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mSecureSettings:Landroid/content/ContentQueryMap;

    #@2
    return-object p1
.end method

.method static synthetic access$1100(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 86
    iget v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mCurrentServiceState:I

    #@2
    return v0
.end method

.method static synthetic access$1102(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 86
    iput p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mCurrentServiceState:I

    #@2
    return p1
.end method

.method static synthetic access$1200(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Landroid/telephony/TelephonyManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 86
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mTelMgr:Landroid/telephony/TelephonyManager;

    #@2
    return-object v0
.end method

.method static synthetic access$1800(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 86
    iget v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mMaxAcceptableHepe:F

    #@2
    return v0
.end method

.method static synthetic access$1900(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Landroid/location/LocationManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 86
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mLocationManager:Landroid/location/LocationManager;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 86
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mAgentHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$2000(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Ljava/lang/Runnable;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 86
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->gnssRequestTimeout:Ljava/lang/Runnable;

    #@2
    return-object v0
.end method

.method static synthetic access$202(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;Landroid/os/Handler;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 86
    iput-object p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mAgentHandler:Landroid/os/Handler;

    #@2
    return-object p1
.end method

.method static synthetic access$2102(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 86
    iput-boolean p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->is_our_gnss_request_running:Z

    #@2
    return p1
.end method

.method static synthetic access$2202(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;Landroid/location/Location;)Landroid/location/Location;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 86
    iput-object p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mLastKnownLocation:Landroid/location/Location;

    #@2
    return-object p1
.end method

.method static synthetic access$2300(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 86
    invoke-direct {p0, p1}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->triggerStatusUpdate(Z)V

    #@3
    return-void
.end method

.method static synthetic access$2402(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 86
    iput-boolean p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->is_connected:Z

    #@2
    return p1
.end method

.method static synthetic access$2500(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 86
    invoke-direct {p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->handleTryConnect()V

    #@3
    return-void
.end method

.method static synthetic access$2600(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;Lcom/qualcomm/lib/location/mq_client/InPostcard;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 86
    invoke-direct {p0, p1}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->handleNewIpcMessage(Lcom/qualcomm/lib/location/mq_client/InPostcard;)V

    #@3
    return-void
.end method

.method static synthetic access$2700(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Ljava/util/concurrent/CountDownLatch;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 86
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->agentInitialized:Ljava/util/concurrent/CountDownLatch;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$SecureSettingsObserver;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 86
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->secureSettingsObserver:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$SecureSettingsObserver;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Landroid/content/ContentQueryMap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 86
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mGlobalSettings:Landroid/content/ContentQueryMap;

    #@2
    return-object v0
.end method

.method static synthetic access$402(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;Landroid/content/ContentQueryMap;)Landroid/content/ContentQueryMap;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 86
    iput-object p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mGlobalSettings:Landroid/content/ContentQueryMap;

    #@2
    return-object p1
.end method

.method static synthetic access$500(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$GlobalSettingsObserver;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 86
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->globalSettingsObserver:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$GlobalSettingsObserver;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Lcom/qualcomm/lib/location/log/LocLog;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 86
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@2
    return-object v0
.end method

.method static synthetic access$800(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 86
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->m_mutex:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$900(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 86
    invoke-direct {p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->CacheCDMACarrier()V

    #@3
    return-void
.end method

.method private fillStatusUpdateMessage(Lcom/qualcomm/lib/location/mq_client/OutPostcard;)V
    .registers 9
    .parameter "out"

    #@0
    .prologue
    .line 682
    :try_start_0
    const-string v4, "ALLOWED_PROVIDERS"

    #@2
    iget-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mAllowedProviders:Ljava/lang/String;

    #@4
    invoke-virtual {p1, v4, v5}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 683
    const-string v4, "IS_AIRPLANE_MODE_ON"

    #@9
    iget-boolean v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->is_airplane_mode_on:Z

    #@b
    invoke-virtual {p1, v4, v5}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addBool(Ljava/lang/String;Z)V

    #@e
    .line 684
    const-string v4, "IS_WIFI_HARDWARE_ON"

    #@10
    iget-boolean v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->is_WIFI_on:Z

    #@12
    invoke-virtual {p1, v4, v5}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addBool(Ljava/lang/String;Z)V

    #@15
    .line 685
    const-string v4, "IS_GPS_PROVIDER_ENABLED"

    #@17
    iget-boolean v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->is_GPS_enabled:Z

    #@19
    invoke-virtual {p1, v4, v5}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addBool(Ljava/lang/String;Z)V

    #@1c
    .line 686
    const-string v4, "IS_NETWORK_PROVIDER_ENABLED"

    #@1e
    iget-boolean v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->is_NLP_enabled:Z

    #@20
    invoke-virtual {p1, v4, v5}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addBool(Ljava/lang/String;Z)V

    #@23
    .line 687
    const-string v4, "IS_QUALCOMM_ENHANCED_PROVIDER_ENABLED"

    #@25
    iget-boolean v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->is_ENH_enabled:Z

    #@27
    invoke-virtual {p1, v4, v5}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addBool(Ljava/lang/String;Z)V

    #@2a
    .line 688
    const-string v4, "IS_CONNECTED"

    #@2c
    iget-boolean v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->is_connected:Z

    #@2e
    invoke-virtual {p1, v4, v5}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addBool(Ljava/lang/String;Z)V

    #@31
    .line 690
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mLastKnownLocation:Landroid/location/Location;

    #@33
    if-eqz v4, :cond_7c

    #@35
    .line 692
    new-instance v1, Lcom/qualcomm/lib/location/mq_client/OutPostcard;

    #@37
    invoke-direct {v1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;-><init>()V

    #@3a
    .line 693
    .local v1, last_fix:Lcom/qualcomm/lib/location/mq_client/OutPostcard;
    invoke-virtual {v1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->init()V

    #@3d
    .line 694
    const-string v4, "SOURCE"

    #@3f
    iget-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mLastKnownLocation:Landroid/location/Location;

    #@41
    invoke-virtual {v5}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    #@44
    move-result-object v5

    #@45
    invoke-virtual {v1, v4, v5}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@48
    .line 695
    const-string v4, "LATITUDE_DEG"

    #@4a
    iget-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mLastKnownLocation:Landroid/location/Location;

    #@4c
    invoke-virtual {v5}, Landroid/location/Location;->getLatitude()D

    #@4f
    move-result-wide v5

    #@50
    invoke-virtual {v1, v4, v5, v6}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addDouble(Ljava/lang/String;D)V

    #@53
    .line 696
    const-string v4, "LONGITUDE_DEG"

    #@55
    iget-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mLastKnownLocation:Landroid/location/Location;

    #@57
    invoke-virtual {v5}, Landroid/location/Location;->getLongitude()D

    #@5a
    move-result-wide v5

    #@5b
    invoke-virtual {v1, v4, v5, v6}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addDouble(Ljava/lang/String;D)V

    #@5e
    .line 697
    const-string v4, "HOR_UNC_M"

    #@60
    iget-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mLastKnownLocation:Landroid/location/Location;

    #@62
    invoke-virtual {v5}, Landroid/location/Location;->getAccuracy()F

    #@65
    move-result v5

    #@66
    invoke-virtual {v1, v4, v5}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addFloat(Ljava/lang/String;F)V

    #@69
    .line 698
    const-string v4, "TIME_STAMP"

    #@6b
    iget-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mLastKnownLocation:Landroid/location/Location;

    #@6d
    invoke-virtual {v5}, Landroid/location/Location;->getTime()J

    #@70
    move-result-wide v5

    #@71
    invoke-virtual {v1, v4, v5, v6}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addInt64(Ljava/lang/String;J)V

    #@74
    .line 699
    invoke-virtual {v1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->seal()V

    #@77
    .line 700
    const-string v4, "LAST_KNOWN_FIX"

    #@79
    invoke-virtual {p1, v4, v1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addCard(Ljava/lang/String;Lcom/qualcomm/lib/location/mq_client/OutPostcard;)V

    #@7c
    .line 703
    .end local v1           #last_fix:Lcom/qualcomm/lib/location/mq_client/OutPostcard;
    :cond_7c
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mActiveNetworkInfo:Landroid/net/NetworkInfo;

    #@7e
    if-eqz v4, :cond_ea

    #@80
    .line 705
    new-instance v2, Lcom/qualcomm/lib/location/mq_client/OutPostcard;

    #@82
    invoke-direct {v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;-><init>()V

    #@85
    .line 706
    .local v2, network_info:Lcom/qualcomm/lib/location/mq_client/OutPostcard;
    invoke-virtual {v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->init()V

    #@88
    .line 708
    const/16 v3, 0x12c

    #@8a
    .line 709
    .local v3, type:I
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mActiveNetworkInfo:Landroid/net/NetworkInfo;

    #@8c
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getType()I

    #@8f
    move-result v4

    #@90
    packed-switch v4, :pswitch_data_114

    #@93
    .line 746
    :pswitch_93
    const/16 v3, 0x12c

    #@95
    .line 749
    :goto_95
    const-string v4, "TYPE"

    #@97
    int-to-long v5, v3

    #@98
    invoke-virtual {v2, v4, v5, v6}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addInt32(Ljava/lang/String;J)V

    #@9b
    .line 751
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mActiveNetworkInfo:Landroid/net/NetworkInfo;

    #@9d
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    #@a0
    move-result-object v4

    #@a1
    if-eqz v4, :cond_ae

    #@a3
    .line 753
    const-string v4, "TYPE_NAME"

    #@a5
    iget-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mActiveNetworkInfo:Landroid/net/NetworkInfo;

    #@a7
    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    #@aa
    move-result-object v5

    #@ab
    invoke-virtual {v2, v4, v5}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@ae
    .line 755
    :cond_ae
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mActiveNetworkInfo:Landroid/net/NetworkInfo;

    #@b0
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    #@b3
    move-result-object v4

    #@b4
    if-eqz v4, :cond_c1

    #@b6
    .line 757
    const-string v4, "SUB_TYPE_NAME"

    #@b8
    iget-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mActiveNetworkInfo:Landroid/net/NetworkInfo;

    #@ba
    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    #@bd
    move-result-object v5

    #@be
    invoke-virtual {v2, v4, v5}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@c1
    .line 759
    :cond_c1
    const-string v4, "IS_AVAILABLE"

    #@c3
    iget-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mActiveNetworkInfo:Landroid/net/NetworkInfo;

    #@c5
    invoke-virtual {v5}, Landroid/net/NetworkInfo;->isAvailable()Z

    #@c8
    move-result v5

    #@c9
    invoke-virtual {v2, v4, v5}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addBool(Ljava/lang/String;Z)V

    #@cc
    .line 760
    const-string v4, "IS_CONNECTED"

    #@ce
    iget-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mActiveNetworkInfo:Landroid/net/NetworkInfo;

    #@d0
    invoke-virtual {v5}, Landroid/net/NetworkInfo;->isConnected()Z

    #@d3
    move-result v5

    #@d4
    invoke-virtual {v2, v4, v5}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addBool(Ljava/lang/String;Z)V

    #@d7
    .line 761
    const-string v4, "IS_ROAMING"

    #@d9
    iget-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mActiveNetworkInfo:Landroid/net/NetworkInfo;

    #@db
    invoke-virtual {v5}, Landroid/net/NetworkInfo;->isRoaming()Z

    #@de
    move-result v5

    #@df
    invoke-virtual {v2, v4, v5}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addBool(Ljava/lang/String;Z)V

    #@e2
    .line 762
    invoke-virtual {v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->seal()V

    #@e5
    .line 763
    const-string v4, "ACTIVE_NETWORK_INFO"

    #@e7
    invoke-virtual {p1, v4, v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addCard(Ljava/lang/String;Lcom/qualcomm/lib/location/mq_client/OutPostcard;)V
    :try_end_ea
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_ea} :catch_106

    #@ea
    .line 771
    .end local v2           #network_info:Lcom/qualcomm/lib/location/mq_client/OutPostcard;
    .end local v3           #type:I
    :cond_ea
    :goto_ea
    return-void

    #@eb
    .line 713
    .restart local v2       #network_info:Lcom/qualcomm/lib/location/mq_client/OutPostcard;
    .restart local v3       #type:I
    :pswitch_eb
    const/16 v3, 0x64

    #@ed
    .line 714
    goto :goto_95

    #@ee
    .line 716
    :pswitch_ee
    const/16 v3, 0x65

    #@f0
    .line 717
    goto :goto_95

    #@f1
    .line 719
    :pswitch_f1
    const/16 v3, 0x66

    #@f3
    .line 720
    goto :goto_95

    #@f4
    .line 724
    :pswitch_f4
    const/16 v3, 0xc9

    #@f6
    .line 725
    goto :goto_95

    #@f7
    .line 727
    :pswitch_f7
    const/16 v3, 0xca

    #@f9
    .line 728
    goto :goto_95

    #@fa
    .line 730
    :pswitch_fa
    const/16 v3, 0xcb

    #@fc
    .line 731
    goto :goto_95

    #@fd
    .line 733
    :pswitch_fd
    const/16 v3, 0xcc

    #@ff
    .line 734
    goto :goto_95

    #@100
    .line 736
    :pswitch_100
    const/16 v3, 0xcd

    #@102
    .line 737
    goto :goto_95

    #@103
    .line 739
    :pswitch_103
    const/16 v3, 0xdc

    #@105
    .line 740
    goto :goto_95

    #@106
    .line 766
    .end local v2           #network_info:Lcom/qualcomm/lib/location/mq_client/OutPostcard;
    .end local v3           #type:I
    :catch_106
    move-exception v0

    #@107
    .line 768
    .local v0, e:Ljava/io/IOException;
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@109
    const-string v5, "XTWiFiOS"

    #@10b
    const-string v6, "Cannot fill status update message"

    #@10d
    invoke-virtual {v4, v5, v6}, Lcom/qualcomm/lib/location/log/LocLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@110
    .line 769
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@113
    goto :goto_ea

    #@114
    .line 709
    :pswitch_data_114
    .packed-switch 0x0
        :pswitch_f4
        :pswitch_eb
        :pswitch_fd
        :pswitch_100
        :pswitch_f7
        :pswitch_fa
        :pswitch_103
        :pswitch_f1
        :pswitch_93
        :pswitch_ee
    .end packed-switch
.end method

.method private getMcc(II)I
    .registers 8
    .parameter "mncmccCombo"
    .parameter "digits"

    #@0
    .prologue
    .line 1275
    const/4 v0, 0x0

    #@1
    .line 1276
    .local v0, mcc:I
    const/4 v1, 0x6

    #@2
    if-ne p2, v1, :cond_21

    #@4
    .line 1277
    div-int/lit16 v0, p1, 0x3e8

    #@6
    .line 1280
    :cond_6
    :goto_6
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@8
    const-string v2, "XTWiFiOS"

    #@a
    new-instance v3, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v4, "getMcc() - "

    #@11
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v3

    #@15
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v1, v2, v3}, Lcom/qualcomm/lib/location/log/LocLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@20
    .line 1281
    return v0

    #@21
    .line 1278
    :cond_21
    const/4 v1, 0x5

    #@22
    if-ne p2, v1, :cond_6

    #@24
    .line 1279
    div-int/lit8 v0, p1, 0x64

    #@26
    goto :goto_6
.end method

.method private getMnc(II)I
    .registers 8
    .parameter "mncmccCombo"
    .parameter "digits"

    #@0
    .prologue
    .line 1264
    const/4 v0, 0x0

    #@1
    .line 1265
    .local v0, mnc:I
    const/4 v1, 0x6

    #@2
    if-ne p2, v1, :cond_21

    #@4
    .line 1266
    rem-int/lit16 v0, p1, 0x3e8

    #@6
    .line 1270
    :cond_6
    :goto_6
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@8
    const-string v2, "XTWiFiOS"

    #@a
    new-instance v3, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v4, "getMnc() - "

    #@11
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v3

    #@15
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v1, v2, v3}, Lcom/qualcomm/lib/location/log/LocLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@20
    .line 1271
    return v0

    #@21
    .line 1267
    :cond_21
    const/4 v1, 0x5

    #@22
    if-ne p2, v1, :cond_6

    #@24
    .line 1268
    rem-int/lit8 v0, p1, 0x64

    #@26
    goto :goto_6
.end method

.method private handleNewIpcMessage(Lcom/qualcomm/lib/location/mq_client/InPostcard;)V
    .registers 14
    .parameter "in_card"

    #@0
    .prologue
    .line 1313
    const/4 v0, 0x0

    #@1
    .line 1316
    .local v0, close_tx:Z
    :try_start_1
    const-string v8, "FROM"

    #@3
    invoke-virtual {p1, v8}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v2

    #@7
    .line 1317
    .local v2, msg_from:Ljava/lang/String;
    const-string v8, "TO"

    #@9
    invoke-virtual {p1, v8}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@c
    move-result-object v6

    #@d
    .line 1318
    .local v6, msg_to:Ljava/lang/String;
    const-string v8, "RESP"

    #@f
    const/4 v9, 0x0

    #@10
    invoke-virtual {p1, v8, v9}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->getStringDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@13
    move-result-object v5

    #@14
    .line 1319
    .local v5, msg_resp:Ljava/lang/String;
    const-string v8, "REQ"

    #@16
    const/4 v9, 0x0

    #@17
    invoke-virtual {p1, v8, v9}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->getStringDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1a
    move-result-object v4

    #@1b
    .line 1320
    .local v4, msg_req:Ljava/lang/String;
    const-string v8, "INFO"

    #@1d
    const/4 v9, 0x0

    #@1e
    invoke-virtual {p1, v8, v9}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->getStringDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@21
    move-result-object v3

    #@22
    .line 1321
    .local v3, msg_info:Ljava/lang/String;
    const-string v8, "TX-ID"

    #@24
    invoke-virtual {p1, v8}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->getInt32(Ljava/lang/String;)I

    #@27
    move-result v7

    #@28
    .line 1323
    .local v7, msg_tx_id:I
    iget-object v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@2a
    const-string v9, "XTWiFiOS"

    #@2c
    new-instance v10, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v11, "FROM: "

    #@33
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v10

    #@37
    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v10

    #@3b
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v10

    #@3f
    invoke-virtual {v8, v9, v10}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@42
    .line 1324
    iget-object v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@44
    const-string v9, "XTWiFiOS"

    #@46
    new-instance v10, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v11, "TO: "

    #@4d
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v10

    #@51
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v10

    #@55
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v10

    #@59
    invoke-virtual {v8, v9, v10}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@5c
    .line 1325
    iget-object v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@5e
    const-string v9, "XTWiFiOS"

    #@60
    new-instance v10, Ljava/lang/StringBuilder;

    #@62
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@65
    const-string v11, "TX-ID: "

    #@67
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v10

    #@6b
    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v10

    #@6f
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@72
    move-result-object v10

    #@73
    invoke-virtual {v8, v9, v10}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@76
    .line 1327
    if-eqz v4, :cond_f9

    #@78
    .line 1329
    iget-object v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@7a
    const-string v9, "XTWiFiOS"

    #@7c
    new-instance v10, Ljava/lang/StringBuilder;

    #@7e
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@81
    const-string v11, "REQ: "

    #@83
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v10

    #@87
    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v10

    #@8b
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8e
    move-result-object v10

    #@8f
    invoke-virtual {v8, v9, v10}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@92
    .line 1331
    const-string v8, "TRIGGER-SINGLE-GNSS-FIX"

    #@94
    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@97
    move-result v8

    #@98
    if-eqz v8, :cond_9e

    #@9a
    .line 1333
    invoke-direct {p0, p1}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->handleTriggerSingleGnssFix(Lcom/qualcomm/lib/location/mq_client/InPostcard;)V

    #@9d
    .line 1390
    .end local v2           #msg_from:Ljava/lang/String;
    .end local v3           #msg_info:Ljava/lang/String;
    .end local v4           #msg_req:Ljava/lang/String;
    .end local v5           #msg_resp:Ljava/lang/String;
    .end local v6           #msg_to:Ljava/lang/String;
    .end local v7           #msg_tx_id:I
    :cond_9d
    :goto_9d
    return-void

    #@9e
    .line 1335
    .restart local v2       #msg_from:Ljava/lang/String;
    .restart local v3       #msg_info:Ljava/lang/String;
    .restart local v4       #msg_req:Ljava/lang/String;
    .restart local v5       #msg_resp:Ljava/lang/String;
    .restart local v6       #msg_to:Ljava/lang/String;
    .restart local v7       #msg_tx_id:I
    :cond_9e
    const-string v8, "SET-ALARM-WITH-WAKELOCK"

    #@a0
    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a3
    move-result v8

    #@a4
    if-nez v8, :cond_9d

    #@a6
    .line 1339
    const-string v8, "SUBSCRIBE-OS-STATUS-UPDATE-NOTIFICATION"

    #@a8
    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ab
    move-result v8

    #@ac
    if-eqz v8, :cond_c0

    #@ae
    .line 1341
    invoke-direct {p0, p1, v2, v7}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->handleSubscribeNotification(Lcom/qualcomm/lib/location/mq_client/InPostcard;Ljava/lang/String;I)V
    :try_end_b1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_b1} :catch_b2
    .catch Ljava/util/NoSuchElementException; {:try_start_1 .. :try_end_b1} :catch_cc

    #@b1
    goto :goto_9d

    #@b2
    .line 1381
    .end local v2           #msg_from:Ljava/lang/String;
    .end local v3           #msg_info:Ljava/lang/String;
    .end local v4           #msg_req:Ljava/lang/String;
    .end local v5           #msg_resp:Ljava/lang/String;
    .end local v6           #msg_to:Ljava/lang/String;
    .end local v7           #msg_tx_id:I
    :catch_b2
    move-exception v1

    #@b3
    .line 1383
    .local v1, e:Ljava/io/IOException;
    iget-object v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@b5
    const-string v9, "XTWiFiOS"

    #@b7
    const-string v10, "cannot receive IPC message"

    #@b9
    invoke-virtual {v8, v9, v10}, Lcom/qualcomm/lib/location/log/LocLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@bc
    .line 1384
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@bf
    goto :goto_9d

    #@c0
    .line 1343
    .end local v1           #e:Ljava/io/IOException;
    .restart local v2       #msg_from:Ljava/lang/String;
    .restart local v3       #msg_info:Ljava/lang/String;
    .restart local v4       #msg_req:Ljava/lang/String;
    .restart local v5       #msg_resp:Ljava/lang/String;
    .restart local v6       #msg_to:Ljava/lang/String;
    .restart local v7       #msg_tx_id:I
    :cond_c0
    :try_start_c0
    const-string v8, "QUERY-OS-STATUS"

    #@c2
    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c5
    move-result v8

    #@c6
    if-eqz v8, :cond_d7

    #@c8
    .line 1345
    invoke-direct {p0, p1, v2, v7}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->handleQueryStatus(Lcom/qualcomm/lib/location/mq_client/InPostcard;Ljava/lang/String;I)V
    :try_end_cb
    .catch Ljava/io/IOException; {:try_start_c0 .. :try_end_cb} :catch_b2
    .catch Ljava/util/NoSuchElementException; {:try_start_c0 .. :try_end_cb} :catch_cc

    #@cb
    goto :goto_9d

    #@cc
    .line 1386
    .end local v2           #msg_from:Ljava/lang/String;
    .end local v3           #msg_info:Ljava/lang/String;
    .end local v4           #msg_req:Ljava/lang/String;
    .end local v5           #msg_resp:Ljava/lang/String;
    .end local v6           #msg_to:Ljava/lang/String;
    .end local v7           #msg_tx_id:I
    :catch_cc
    move-exception v1

    #@cd
    .line 1388
    .local v1, e:Ljava/util/NoSuchElementException;
    iget-object v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@cf
    const-string v9, "XTWiFiOS"

    #@d1
    const-string v10, "no such element in post card"

    #@d3
    invoke-virtual {v8, v9, v10}, Lcom/qualcomm/lib/location/log/LocLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@d6
    goto :goto_9d

    #@d7
    .line 1347
    .end local v1           #e:Ljava/util/NoSuchElementException;
    .restart local v2       #msg_from:Ljava/lang/String;
    .restart local v3       #msg_info:Ljava/lang/String;
    .restart local v4       #msg_req:Ljava/lang/String;
    .restart local v5       #msg_resp:Ljava/lang/String;
    .restart local v6       #msg_to:Ljava/lang/String;
    .restart local v7       #msg_tx_id:I
    :cond_d7
    :try_start_d7
    const-string v8, "UNSUBSCRIBE-OS-STATUS-UPDATE-NOTIFICATION"

    #@d9
    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@dc
    move-result v8

    #@dd
    if-eqz v8, :cond_e3

    #@df
    .line 1349
    invoke-direct {p0, p1, v2, v7}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->handleUnSubscribeNotification(Lcom/qualcomm/lib/location/mq_client/InPostcard;Ljava/lang/String;I)V

    #@e2
    goto :goto_9d

    #@e3
    .line 1351
    :cond_e3
    const-string v8, "RIL_REQUEST"

    #@e5
    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e8
    move-result v8

    #@e9
    if-eqz v8, :cond_ef

    #@eb
    .line 1353
    invoke-direct {p0, p1, v2, v7}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->handleRILMessage(Lcom/qualcomm/lib/location/mq_client/InPostcard;Ljava/lang/String;I)V

    #@ee
    goto :goto_9d

    #@ef
    .line 1357
    :cond_ef
    iget-object v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@f1
    const-string v9, "XTWiFiOS"

    #@f3
    const-string v10, "Unknown request"

    #@f5
    invoke-virtual {v8, v9, v10}, Lcom/qualcomm/lib/location/log/LocLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@f8
    goto :goto_9d

    #@f9
    .line 1360
    :cond_f9
    if-eqz v5, :cond_116

    #@fb
    .line 1362
    iget-object v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@fd
    const-string v9, "XTWiFiOS"

    #@ff
    new-instance v10, Ljava/lang/StringBuilder;

    #@101
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@104
    const-string v11, "RESP: "

    #@106
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@109
    move-result-object v10

    #@10a
    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10d
    move-result-object v10

    #@10e
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@111
    move-result-object v10

    #@112
    invoke-virtual {v8, v9, v10}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@115
    goto :goto_9d

    #@116
    .line 1364
    :cond_116
    if-eqz v3, :cond_145

    #@118
    .line 1366
    iget-object v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@11a
    const-string v9, "XTWiFiOS"

    #@11c
    new-instance v10, Ljava/lang/StringBuilder;

    #@11e
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@121
    const-string v11, "INFO: "

    #@123
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@126
    move-result-object v10

    #@127
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12a
    move-result-object v10

    #@12b
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12e
    move-result-object v10

    #@12f
    invoke-virtual {v8, v9, v10}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@132
    .line 1368
    const-string v8, "WAKELOCK-HANDLER-COMPLETED"

    #@134
    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@137
    move-result v8

    #@138
    if-nez v8, :cond_9d

    #@13a
    .line 1373
    iget-object v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@13c
    const-string v9, "XTWiFiOS"

    #@13e
    const-string v10, "Unknown info notification"

    #@140
    invoke-virtual {v8, v9, v10}, Lcom/qualcomm/lib/location/log/LocLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@143
    goto/16 :goto_9d

    #@145
    .line 1378
    :cond_145
    iget-object v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@147
    const-string v9, "XTWiFiOS"

    #@149
    const-string v10, "Not sure what message it is"

    #@14b
    invoke-virtual {v8, v9, v10}, Lcom/qualcomm/lib/location/log/LocLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_14e
    .catch Ljava/io/IOException; {:try_start_d7 .. :try_end_14e} :catch_b2
    .catch Ljava/util/NoSuchElementException; {:try_start_d7 .. :try_end_14e} :catch_cc

    #@14e
    goto/16 :goto_9d
.end method

.method private handleQueryStatus(Lcom/qualcomm/lib/location/mq_client/InPostcard;Ljava/lang/String;I)V
    .registers 10
    .parameter "in_card"
    .parameter "str_from"
    .parameter "tx_id"

    #@0
    .prologue
    .line 1288
    :try_start_0
    invoke-direct {p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->updateStatus()V

    #@3
    .line 1290
    new-instance v2, Lcom/qualcomm/lib/location/mq_client/OutPostcard;

    #@5
    invoke-direct {v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;-><init>()V

    #@8
    .line 1291
    .local v2, out:Lcom/qualcomm/lib/location/mq_client/OutPostcard;
    invoke-virtual {v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->init()V

    #@b
    .line 1292
    const-string v3, "TO"

    #@d
    invoke-virtual {v2, v3, p2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@10
    .line 1293
    const-string v3, "FROM"

    #@12
    const-string v4, "OS-Agent"

    #@14
    invoke-virtual {v2, v3, v4}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@17
    .line 1294
    const-string v3, "RESP"

    #@19
    const-string v4, "QUERY-OS-STATUS"

    #@1b
    invoke-virtual {v2, v3, v4}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@1e
    .line 1295
    const-string v3, "TX-ID"

    #@20
    int-to-long v4, p3

    #@21
    invoke-virtual {v2, v3, v4, v5}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addInt32(Ljava/lang/String;J)V

    #@24
    .line 1297
    invoke-direct {p0, v2}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->fillStatusUpdateMessage(Lcom/qualcomm/lib/location/mq_client/OutPostcard;)V

    #@27
    .line 1299
    invoke-virtual {v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->seal()V

    #@2a
    .line 1300
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->m_ipcClient:Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@2c
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->getOutputStream()Ljava/io/OutputStream;

    #@2f
    move-result-object v1

    #@30
    .line 1301
    .local v1, os:Ljava/io/OutputStream;
    invoke-virtual {v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->getEncodedBuffer()[B

    #@33
    move-result-object v3

    #@34
    invoke-virtual {v1, v3}, Ljava/io/OutputStream;->write([B)V

    #@37
    .line 1302
    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V
    :try_end_3a
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_3a} :catch_3b

    #@3a
    .line 1309
    .end local v1           #os:Ljava/io/OutputStream;
    .end local v2           #out:Lcom/qualcomm/lib/location/mq_client/OutPostcard;
    :goto_3a
    return-void

    #@3b
    .line 1304
    :catch_3b
    move-exception v0

    #@3c
    .line 1306
    .local v0, e:Ljava/lang/Exception;
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@3e
    const-string v4, "XTWiFiOS"

    #@40
    const-string v5, "Cannot generate status update 1"

    #@42
    invoke-virtual {v3, v4, v5}, Lcom/qualcomm/lib/location/log/LocLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@45
    .line 1307
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@48
    goto :goto_3a
.end method

.method private handleRILMessage(Lcom/qualcomm/lib/location/mq_client/InPostcard;Ljava/lang/String;I)V
    .registers 13
    .parameter "in_card"
    .parameter "str_from"
    .parameter "tx_id"

    #@0
    .prologue
    .line 862
    const/4 v2, -0x1

    #@1
    .line 864
    .local v2, reqtype:I
    iget-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->m_mutex:Ljava/lang/Object;

    #@3
    monitor-enter v5

    #@4
    .line 868
    :try_start_4
    const-string v4, "ReqType"

    #@6
    invoke-virtual {p1, v4}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->getInt32(Ljava/lang/String;)I
    :try_end_9
    .catchall {:try_start_4 .. :try_end_9} :catchall_34
    .catch Ljava/util/NoSuchElementException; {:try_start_4 .. :try_end_9} :catch_29
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_9} :catch_135

    #@9
    move-result v2

    #@a
    .line 879
    :goto_a
    packed-switch v2, :pswitch_data_138

    #@d
    .line 924
    :try_start_d
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@f
    const-string v6, "XTWiFiOS"

    #@11
    new-instance v7, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v8, "Unknown request type = "

    #@18
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v7

    #@1c
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v7

    #@20
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v7

    #@24
    invoke-virtual {v4, v6, v7}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@27
    .line 926
    :cond_27
    :goto_27
    monitor-exit v5

    #@28
    .line 927
    return-void

    #@29
    .line 870
    :catch_29
    move-exception v0

    #@2a
    .line 872
    .local v0, e:Ljava/util/NoSuchElementException;
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@2c
    const-string v6, "XTWiFiOS"

    #@2e
    const-string v7, "No ReqType attribute"

    #@30
    invoke-virtual {v4, v6, v7}, Lcom/qualcomm/lib/location/log/LocLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@33
    goto :goto_a

    #@34
    .line 926
    .end local v0           #e:Ljava/util/NoSuchElementException;
    :catchall_34
    move-exception v4

    #@35
    monitor-exit v5
    :try_end_36
    .catchall {:try_start_d .. :try_end_36} :catchall_34

    #@36
    throw v4

    #@37
    .line 882
    :pswitch_37
    :try_start_37
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@39
    const-string v6, "XTWiFiOS"

    #@3b
    const-string v7, "LOC_RIL_REQUEST_VERSION"

    #@3d
    invoke-virtual {v4, v6, v7}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@40
    goto :goto_27

    #@41
    .line 885
    :pswitch_41
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@43
    const-string v6, "XTWiFiOS"

    #@45
    new-instance v7, Ljava/lang/StringBuilder;

    #@47
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@4a
    const-string v8, "LOC_RIL_REQUEST_SERVICE_INFO Service State = "

    #@4c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v7

    #@50
    iget v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mCurrentServiceState:I

    #@52
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@55
    move-result-object v7

    #@56
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v7

    #@5a
    invoke-virtual {v4, v6, v7}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@5d
    .line 886
    invoke-direct {p0, p2, p3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->FillServiceInfo(Ljava/lang/String;I)V

    #@60
    goto :goto_27

    #@61
    .line 889
    :pswitch_61
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@63
    const-string v6, "XTWiFiOS"

    #@65
    new-instance v7, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    const-string v8, "LOC_RIL_REGISTER_CELL_UPDATE Service State = "

    #@6c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v7

    #@70
    iget v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mCurrentServiceState:I

    #@72
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@75
    move-result-object v7

    #@76
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object v7

    #@7a
    invoke-virtual {v4, v6, v7}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@7d
    .line 891
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mTelMgr:Landroid/telephony/TelephonyManager;

    #@7f
    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getCellLocation()Landroid/telephony/CellLocation;

    #@82
    move-result-object v4

    #@83
    const/4 v6, 0x0

    #@84
    invoke-direct {p0, p2, p3, v4, v6}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->FillCellUpdateInfo(Ljava/lang/String;ILandroid/telephony/CellLocation;Z)V

    #@87
    .line 894
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mRegisteredClientList:Ljava/util/Map;

    #@89
    if-eqz v4, :cond_c5

    #@8b
    .line 896
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mRegisteredClientList:Ljava/util/Map;

    #@8d
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@90
    move-result-object v6

    #@91
    invoke-interface {v4, p2, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@94
    move-result-object v1

    #@95
    check-cast v1, Ljava/lang/Integer;

    #@97
    .line 897
    .local v1, prevtx_id:Ljava/lang/Integer;
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@99
    const-string v6, "XTWiFiOS"

    #@9b
    new-instance v7, Ljava/lang/StringBuilder;

    #@9d
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@a0
    const-string v8, "Registered Client "

    #@a2
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v7

    #@a6
    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v7

    #@aa
    const-string v8, " with tx_id = "

    #@ac
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af
    move-result-object v7

    #@b0
    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v7

    #@b4
    const-string v8, " old tx_id = "

    #@b6
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v7

    #@ba
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v7

    #@be
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c1
    move-result-object v7

    #@c2
    invoke-virtual {v4, v6, v7}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@c5
    .line 900
    .end local v1           #prevtx_id:Ljava/lang/Integer;
    :cond_c5
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mRegisteredClientList:Ljava/util/Map;

    #@c7
    invoke-interface {v4}, Ljava/util/Map;->isEmpty()Z

    #@ca
    move-result v4

    #@cb
    if-nez v4, :cond_27

    #@cd
    .line 902
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@cf
    const-string v6, "XTWiFiOS"

    #@d1
    const-string v7, "Start listening for phone location change events"

    #@d3
    invoke-virtual {v4, v6, v7}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@d6
    .line 903
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mTelMgr:Landroid/telephony/TelephonyManager;

    #@d8
    iget-object v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mRilListener:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$RilListener;

    #@da
    const/16 v7, 0x11

    #@dc
    invoke-virtual {v4, v6, v7}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    #@df
    goto/16 :goto_27

    #@e1
    .line 909
    :pswitch_e1
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mRegisteredClientList:Ljava/util/Map;

    #@e3
    if-eqz v4, :cond_111

    #@e5
    .line 911
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mRegisteredClientList:Ljava/util/Map;

    #@e7
    invoke-interface {v4, p2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@ea
    move-result-object v3

    #@eb
    check-cast v3, Ljava/lang/Integer;

    #@ed
    .line 912
    .local v3, txid:Ljava/lang/Integer;
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@ef
    const-string v6, "XTWiFiOS"

    #@f1
    new-instance v7, Ljava/lang/StringBuilder;

    #@f3
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@f6
    const-string v8, "Remove client from list "

    #@f8
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fb
    move-result-object v7

    #@fc
    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v7

    #@100
    const-string v8, " with txid = "

    #@102
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@105
    move-result-object v7

    #@106
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@109
    move-result-object v7

    #@10a
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10d
    move-result-object v7

    #@10e
    invoke-virtual {v4, v6, v7}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@111
    .line 915
    .end local v3           #txid:Ljava/lang/Integer;
    :cond_111
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mRegisteredClientList:Ljava/util/Map;

    #@113
    invoke-interface {v4}, Ljava/util/Map;->isEmpty()Z

    #@116
    move-result v4

    #@117
    if-eqz v4, :cond_12a

    #@119
    .line 917
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@11b
    const-string v6, "XTWiFiOS"

    #@11d
    const-string v7, "Deregister from listening to phone location change events, listen only for service state changes"

    #@11f
    invoke-virtual {v4, v6, v7}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@122
    .line 918
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mTelMgr:Landroid/telephony/TelephonyManager;

    #@124
    iget-object v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mRilListener:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$RilListener;

    #@126
    const/4 v7, 0x1

    #@127
    invoke-virtual {v4, v6, v7}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    #@12a
    .line 921
    :cond_12a
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@12c
    const-string v6, "XTWiFiOS"

    #@12e
    const-string v7, "LOC_RIL_DEREGISTER_CELL_UPDATE"

    #@130
    invoke-virtual {v4, v6, v7}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_133
    .catchall {:try_start_37 .. :try_end_133} :catchall_34

    #@133
    goto/16 :goto_27

    #@135
    .line 874
    :catch_135
    move-exception v4

    #@136
    goto/16 :goto_a

    #@138
    .line 879
    :pswitch_data_138
    .packed-switch 0x1
        :pswitch_37
        :pswitch_61
        :pswitch_e1
        :pswitch_41
    .end packed-switch
.end method

.method private handleSubscribeNotification(Lcom/qualcomm/lib/location/mq_client/InPostcard;Ljava/lang/String;I)V
    .registers 12
    .parameter "in_card"
    .parameter "str_from"
    .parameter "tx_id"

    #@0
    .prologue
    .line 777
    const/4 v1, 0x0

    #@1
    .line 780
    .local v1, need_location_update:Z
    :try_start_1
    const-string v5, "NEED_LOCATION_UPDATE"

    #@3
    invoke-virtual {p1, v5}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->getBool(Ljava/lang/String;)Z
    :try_end_6
    .catch Ljava/util/NoSuchElementException; {:try_start_1 .. :try_end_6} :catch_87
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_6} :catch_72

    #@6
    move-result v1

    #@7
    .line 786
    :goto_7
    :try_start_7
    new-instance v4, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$UpdateSubscriber;

    #@9
    invoke-direct {v4, p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$UpdateSubscriber;-><init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)V

    #@c
    .line 787
    .local v4, subscriber:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$UpdateSubscriber;
    iput-object p2, v4, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$UpdateSubscriber;->ipc_name:Ljava/lang/String;

    #@e
    .line 788
    iput-boolean v1, v4, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$UpdateSubscriber;->need_location_update:Z

    #@10
    .line 791
    iget-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mStatusUpdateSubscriber:Ljava/util/Map;

    #@12
    invoke-interface {v5, p2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@15
    .line 793
    invoke-direct {p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->updateStatus()V

    #@18
    .line 795
    new-instance v3, Lcom/qualcomm/lib/location/mq_client/OutPostcard;

    #@1a
    invoke-direct {v3}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;-><init>()V

    #@1d
    .line 796
    .local v3, out:Lcom/qualcomm/lib/location/mq_client/OutPostcard;
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->init()V

    #@20
    .line 797
    const-string v5, "TO"

    #@22
    invoke-virtual {v3, v5, p2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@25
    .line 798
    const-string v5, "FROM"

    #@27
    const-string v6, "OS-Agent"

    #@29
    invoke-virtual {v3, v5, v6}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@2c
    .line 799
    const-string v5, "RESP"

    #@2e
    const-string v6, "SUBSCRIBE-OS-STATUS-UPDATE-NOTIFICATION"

    #@30
    invoke-virtual {v3, v5, v6}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@33
    .line 800
    const-string v5, "TX-ID"

    #@35
    int-to-long v6, p3

    #@36
    invoke-virtual {v3, v5, v6, v7}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addInt32(Ljava/lang/String;J)V

    #@39
    .line 801
    sget-object v5, Landroid/os/Build;->MODEL:Ljava/lang/String;

    #@3b
    const-string v6, "unknown"

    #@3d
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@40
    move-result v5

    #@41
    if-nez v5, :cond_4a

    #@43
    .line 803
    const-string v5, "MODEL"

    #@45
    sget-object v6, Landroid/os/Build;->MODEL:Ljava/lang/String;

    #@47
    invoke-virtual {v3, v5, v6}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@4a
    .line 805
    :cond_4a
    sget-object v5, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    #@4c
    const-string v6, "unknown"

    #@4e
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@51
    move-result v5

    #@52
    if-nez v5, :cond_5b

    #@54
    .line 807
    const-string v5, "MANUFACTURER"

    #@56
    sget-object v6, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    #@58
    invoke-virtual {v3, v5, v6}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@5b
    .line 810
    :cond_5b
    invoke-direct {p0, v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->fillStatusUpdateMessage(Lcom/qualcomm/lib/location/mq_client/OutPostcard;)V

    #@5e
    .line 812
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->seal()V

    #@61
    .line 814
    iget-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->m_ipcClient:Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@63
    invoke-virtual {v5}, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->getOutputStream()Ljava/io/OutputStream;

    #@66
    move-result-object v2

    #@67
    .line 815
    .local v2, os:Ljava/io/OutputStream;
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->getEncodedBuffer()[B

    #@6a
    move-result-object v5

    #@6b
    invoke-virtual {v2, v5}, Ljava/io/OutputStream;->write([B)V

    #@6e
    .line 816
    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V
    :try_end_71
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_71} :catch_72

    #@71
    .line 825
    .end local v2           #os:Ljava/io/OutputStream;
    .end local v3           #out:Lcom/qualcomm/lib/location/mq_client/OutPostcard;
    .end local v4           #subscriber:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$UpdateSubscriber;
    :goto_71
    return-void

    #@72
    .line 818
    :catch_72
    move-exception v0

    #@73
    .line 820
    .local v0, e:Ljava/lang/Exception;
    iget-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@75
    const-string v6, "XTWiFiOS"

    #@77
    const-string v7, "Cannot generate status update 1"

    #@79
    invoke-virtual {v5, v6, v7}, Lcom/qualcomm/lib/location/log/LocLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@7c
    .line 821
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@7f
    .line 823
    iget-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mAgentHandler:Landroid/os/Handler;

    #@81
    const/16 v6, 0x66

    #@83
    invoke-virtual {v5, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@86
    goto :goto_71

    #@87
    .line 782
    .end local v0           #e:Ljava/lang/Exception;
    :catch_87
    move-exception v5

    #@88
    goto/16 :goto_7
.end method

.method private handleTriggerSingleGnssFix(Lcom/qualcomm/lib/location/mq_client/InPostcard;)V
    .registers 15
    .parameter "in_card"

    #@0
    .prologue
    .line 495
    :try_start_0
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mLocationManager:Landroid/location/LocationManager;

    #@2
    if-eqz v0, :cond_9d

    #@4
    .line 497
    iget v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mConfigMaxAcceptableHepe:F
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_6} :catch_8f

    #@6
    .line 500
    .local v8, max_acceptable_hepe:F
    :try_start_6
    const-string v0, "MAX_ACCEPTABLE_HEPE"

    #@8
    invoke-virtual {p1, v0}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->getFloat(Ljava/lang/String;)F
    :try_end_b
    .catch Ljava/util/NoSuchElementException; {:try_start_6 .. :try_end_b} :catch_b0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_b} :catch_ad
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_b} :catch_8f

    #@b
    move-result v8

    #@c
    .line 511
    :goto_c
    :try_start_c
    iget-wide v11, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mConfigTimeOutForGnssFix:J
    :try_end_e
    .catch Ljava/lang/RuntimeException; {:try_start_c .. :try_end_e} :catch_8f

    #@e
    .line 514
    .local v11, timeout_relative_msec:J
    :try_start_e
    const-string v0, "TIMEOUT_SEC"

    #@10
    invoke-virtual {p1, v0}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->getInt32(Ljava/lang/String;)I
    :try_end_13
    .catch Ljava/util/NoSuchElementException; {:try_start_e .. :try_end_13} :catch_aa
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_13} :catch_a7
    .catch Ljava/lang/RuntimeException; {:try_start_e .. :try_end_13} :catch_8f

    #@13
    move-result v0

    #@14
    int-to-long v0, v0

    #@15
    const-wide/16 v2, 0x3e8

    #@17
    mul-long v11, v0, v2

    #@19
    .line 525
    :goto_19
    :try_start_19
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@1c
    move-result-wide v0

    #@1d
    add-long v9, v0, v11

    #@1f
    .line 527
    .local v9, timeout_absolute_msec:J
    iget-boolean v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->is_our_gnss_request_running:Z

    #@21
    if-eqz v0, :cond_75

    #@23
    .line 529
    iget v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mMaxAcceptableHepe:F

    #@25
    cmpg-float v0, v8, v0

    #@27
    if-gez v0, :cond_2b

    #@29
    .line 532
    iput v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mMaxAcceptableHepe:F

    #@2b
    .line 535
    :cond_2b
    iget-wide v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mTimeOutForGnssFix:J

    #@2d
    cmp-long v0, v0, v9

    #@2f
    if-gez v0, :cond_33

    #@31
    .line 538
    iput-wide v9, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mTimeOutForGnssFix:J

    #@33
    .line 549
    :cond_33
    :goto_33
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@35
    const-string v1, "XTWiFiOS"

    #@37
    new-instance v2, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v3, "set max HEPE acceptable to "

    #@3e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v2

    #@42
    iget v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mMaxAcceptableHepe:F

    #@44
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@47
    move-result-object v2

    #@48
    const-string v3, " meters, with timeout in "

    #@4a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v2

    #@4e
    iget-wide v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mTimeOutForGnssFix:J

    #@50
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@53
    move-result-wide v5

    #@54
    sub-long/2addr v3, v5

    #@55
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@58
    move-result-object v2

    #@59
    const-string v3, " msec"

    #@5b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v2

    #@5f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v2

    #@63
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@66
    .line 552
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mAgentHandler:Landroid/os/Handler;

    #@68
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->gnssRequestTimeout:Ljava/lang/Runnable;

    #@6a
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@6d
    .line 553
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mAgentHandler:Landroid/os/Handler;

    #@6f
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->gnssRequestTimeout:Ljava/lang/Runnable;

    #@71
    invoke-virtual {v0, v1, v11, v12}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@74
    .line 565
    .end local v8           #max_acceptable_hepe:F
    .end local v9           #timeout_absolute_msec:J
    .end local v11           #timeout_relative_msec:J
    :goto_74
    return-void

    #@75
    .line 543
    .restart local v8       #max_acceptable_hepe:F
    .restart local v9       #timeout_absolute_msec:J
    .restart local v11       #timeout_relative_msec:J
    :cond_75
    const/4 v0, 0x1

    #@76
    iput-boolean v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->is_our_gnss_request_running:Z

    #@78
    .line 544
    iput v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mMaxAcceptableHepe:F

    #@7a
    .line 545
    iput-wide v9, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mTimeOutForGnssFix:J

    #@7c
    .line 546
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mLocationManager:Landroid/location/LocationManager;

    #@7e
    const-string v1, "gps"

    #@80
    const-wide/16 v2, 0x0

    #@82
    const/4 v4, 0x0

    #@83
    iget-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mTimedListener:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$TimedLocationListener;

    #@85
    iget-object v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mAgentHandler:Landroid/os/Handler;

    #@87
    invoke-virtual {v6}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    #@8a
    move-result-object v6

    #@8b
    invoke-virtual/range {v0 .. v6}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V
    :try_end_8e
    .catch Ljava/lang/RuntimeException; {:try_start_19 .. :try_end_8e} :catch_8f

    #@8e
    goto :goto_33

    #@8f
    .line 560
    .end local v8           #max_acceptable_hepe:F
    .end local v9           #timeout_absolute_msec:J
    .end local v11           #timeout_relative_msec:J
    :catch_8f
    move-exception v7

    #@90
    .line 562
    .local v7, e:Ljava/lang/RuntimeException;
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@92
    const-string v1, "XTWiFiOS"

    #@94
    const-string v2, "Cannot request for single update"

    #@96
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@99
    .line 563
    invoke-virtual {v7}, Ljava/lang/RuntimeException;->printStackTrace()V

    #@9c
    goto :goto_74

    #@9d
    .line 557
    .end local v7           #e:Ljava/lang/RuntimeException;
    :cond_9d
    :try_start_9d
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@9f
    const-string v1, "XTWiFiOS"

    #@a1
    const-string v2, "Location manager service is not available"

    #@a3
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a6
    .catch Ljava/lang/RuntimeException; {:try_start_9d .. :try_end_a6} :catch_8f

    #@a6
    goto :goto_74

    #@a7
    .line 520
    .restart local v8       #max_acceptable_hepe:F
    .restart local v11       #timeout_relative_msec:J
    :catch_a7
    move-exception v0

    #@a8
    goto/16 :goto_19

    #@aa
    .line 516
    :catch_aa
    move-exception v0

    #@ab
    goto/16 :goto_19

    #@ad
    .line 506
    .end local v11           #timeout_relative_msec:J
    :catch_ad
    move-exception v0

    #@ae
    goto/16 :goto_c

    #@b0
    .line 502
    :catch_b0
    move-exception v0

    #@b1
    goto/16 :goto_c
.end method

.method private handleTryConnect()V
    .registers 9

    #@0
    .prologue
    const/16 v7, 0x66

    #@2
    .line 378
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->m_ipcClient:Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@4
    if-nez v3, :cond_12

    #@6
    .line 380
    new-instance v3, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@8
    const-string v4, "/data/misc/location/mq/location-mq-s"

    #@a
    invoke-direct {v3, v4}, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;-><init>(Ljava/lang/String;)V

    #@d
    iput-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->m_ipcClient:Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@f
    .line 381
    const/4 v3, 0x0

    #@10
    iput v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->m_connectRetryCounter:I

    #@12
    .line 384
    :cond_12
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->m_ipcClient:Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@14
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->connect()Z

    #@17
    move-result v3

    #@18
    if-nez v3, :cond_56

    #@1a
    .line 386
    iget v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->m_connectRetryCounter:I

    #@1c
    add-int/lit8 v3, v3, 0x1

    #@1e
    iput v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->m_connectRetryCounter:I

    #@20
    .line 387
    iget v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->m_connectRetryCounter:I

    #@22
    const/16 v4, 0x14

    #@24
    if-ge v3, v4, :cond_4c

    #@26
    .line 389
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@28
    const-string v4, "XTWiFiOS"

    #@2a
    new-instance v5, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v6, "connection failure. count "

    #@31
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v5

    #@35
    iget v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->m_connectRetryCounter:I

    #@37
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v5

    #@3b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v5

    #@3f
    invoke-virtual {v3, v4, v5}, Lcom/qualcomm/lib/location/log/LocLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@42
    .line 390
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mAgentHandler:Landroid/os/Handler;

    #@44
    const/16 v4, 0x64

    #@46
    const-wide/16 v5, 0x1388

    #@48
    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@4b
    .line 422
    :goto_4b
    return-void

    #@4c
    .line 394
    :cond_4c
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@4e
    const-string v4, "XTWiFiOS"

    #@50
    const-string v5, "connection failure. abort"

    #@52
    invoke-virtual {v3, v4, v5}, Lcom/qualcomm/lib/location/log/LocLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@55
    goto :goto_4b

    #@56
    .line 401
    :cond_56
    :try_start_56
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->m_ipcClient:Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@58
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mAgentHandler:Landroid/os/Handler;

    #@5a
    const/16 v5, 0x65

    #@5c
    const/16 v6, 0x66

    #@5e
    invoke-virtual {v3, v4, v5, v6}, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->startReceiverThread(Landroid/os/Handler;II)V

    #@61
    .line 405
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->m_ipcClient:Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@63
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->getOutputStream()Ljava/io/OutputStream;

    #@66
    move-result-object v1

    #@67
    .line 406
    .local v1, os:Ljava/io/OutputStream;
    new-instance v2, Lcom/qualcomm/lib/location/mq_client/OutPostcard;

    #@69
    invoke-direct {v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;-><init>()V

    #@6c
    .line 407
    .local v2, out:Lcom/qualcomm/lib/location/mq_client/OutPostcard;
    invoke-virtual {v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->init()V

    #@6f
    .line 408
    const-string v3, "TO"

    #@71
    const-string v4, "SERVER"

    #@73
    invoke-virtual {v2, v3, v4}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@76
    .line 409
    const-string v3, "FROM"

    #@78
    const-string v4, "OS-Agent"

    #@7a
    invoke-virtual {v2, v3, v4}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@7d
    .line 410
    const-string v3, "REQ"

    #@7f
    const-string v4, "REGISTER"

    #@81
    invoke-virtual {v2, v3, v4}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@84
    .line 411
    invoke-virtual {v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->seal()V

    #@87
    .line 412
    invoke-virtual {v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->getEncodedBuffer()[B

    #@8a
    move-result-object v3

    #@8b
    invoke-virtual {v1, v3}, Ljava/io/OutputStream;->write([B)V

    #@8e
    .line 413
    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V
    :try_end_91
    .catch Ljava/lang/Exception; {:try_start_56 .. :try_end_91} :catch_92

    #@91
    goto :goto_4b

    #@92
    .line 415
    .end local v1           #os:Ljava/io/OutputStream;
    .end local v2           #out:Lcom/qualcomm/lib/location/mq_client/OutPostcard;
    :catch_92
    move-exception v0

    #@93
    .line 417
    .local v0, e:Ljava/lang/Exception;
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@95
    const-string v4, "XTWiFiOS"

    #@97
    const-string v5, "cannot send register message"

    #@99
    invoke-virtual {v3, v4, v5}, Lcom/qualcomm/lib/location/log/LocLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@9c
    .line 419
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mAgentHandler:Landroid/os/Handler;

    #@9e
    invoke-virtual {v3, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@a1
    goto :goto_4b
.end method

.method private handleUnSubscribeNotification(Lcom/qualcomm/lib/location/mq_client/InPostcard;Ljava/lang/String;I)V
    .registers 11
    .parameter "in_card"
    .parameter "str_from"
    .parameter "tx_id"

    #@0
    .prologue
    .line 831
    :try_start_0
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mStatusUpdateSubscriber:Ljava/util/Map;

    #@2
    invoke-interface {v3, p2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v3

    #@6
    if-eqz v3, :cond_5d

    #@8
    .line 833
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@a
    const-string v4, "XTWiFiOS"

    #@c
    new-instance v5, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v6, "Client ["

    #@13
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v5

    #@17
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v5

    #@1b
    const-string v6, "] just unsubscribed from status update"

    #@1d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v5

    #@21
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v5

    #@25
    invoke-virtual {v3, v4, v5}, Lcom/qualcomm/lib/location/log/LocLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@28
    .line 840
    :goto_28
    new-instance v2, Lcom/qualcomm/lib/location/mq_client/OutPostcard;

    #@2a
    invoke-direct {v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;-><init>()V

    #@2d
    .line 841
    .local v2, out:Lcom/qualcomm/lib/location/mq_client/OutPostcard;
    invoke-virtual {v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->init()V

    #@30
    .line 842
    const-string v3, "TO"

    #@32
    invoke-virtual {v2, v3, p2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@35
    .line 843
    const-string v3, "FROM"

    #@37
    const-string v4, "OS-Agent"

    #@39
    invoke-virtual {v2, v3, v4}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@3c
    .line 844
    const-string v3, "RESP"

    #@3e
    const-string v4, "UNSUBSCRIBE-OS-STATUS-UPDATE-NOTIFICATION"

    #@40
    invoke-virtual {v2, v3, v4}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@43
    .line 845
    const-string v3, "TX-ID"

    #@45
    int-to-long v4, p3

    #@46
    invoke-virtual {v2, v3, v4, v5}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addInt32(Ljava/lang/String;J)V

    #@49
    .line 847
    invoke-virtual {v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->seal()V

    #@4c
    .line 849
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->m_ipcClient:Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@4e
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->getOutputStream()Ljava/io/OutputStream;

    #@51
    move-result-object v1

    #@52
    .line 850
    .local v1, os:Ljava/io/OutputStream;
    invoke-virtual {v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->getEncodedBuffer()[B

    #@55
    move-result-object v3

    #@56
    invoke-virtual {v1, v3}, Ljava/io/OutputStream;->write([B)V

    #@59
    .line 851
    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V

    #@5c
    .line 858
    .end local v1           #os:Ljava/io/OutputStream;
    .end local v2           #out:Lcom/qualcomm/lib/location/mq_client/OutPostcard;
    :goto_5c
    return-void

    #@5d
    .line 837
    :cond_5d
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@5f
    const-string v4, "XTWiFiOS"

    #@61
    new-instance v5, Ljava/lang/StringBuilder;

    #@63
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@66
    const-string v6, "Client ["

    #@68
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v5

    #@6c
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v5

    #@70
    const-string v6, "] just tried to unsubscribe from status update, but it\'s not a subscriber yet"

    #@72
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v5

    #@76
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object v5

    #@7a
    invoke-virtual {v3, v4, v5}, Lcom/qualcomm/lib/location/log/LocLog;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7d
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_7d} :catch_7e

    #@7d
    goto :goto_28

    #@7e
    .line 853
    :catch_7e
    move-exception v0

    #@7f
    .line 855
    .local v0, e:Ljava/lang/Exception;
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@81
    const-string v4, "XTWiFiOS"

    #@83
    const-string v5, "Cannot generate response to unsubscribtion request"

    #@85
    invoke-virtual {v3, v4, v5}, Lcom/qualcomm/lib/location/log/LocLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@88
    .line 856
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@8b
    goto :goto_5c
.end method

.method private installObservers()V
    .registers 14

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v12, 0x2

    #@2
    const/4 v11, 0x0

    #@3
    const/4 v10, 0x1

    #@4
    .line 1499
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->m_api_level_17:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$ApiLevel17;

    #@6
    if-eqz v0, :cond_4a

    #@8
    .line 1501
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->m_api_level_17:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$ApiLevel17;

    #@a
    invoke-virtual {v0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$ApiLevel17;->installObservers()V

    #@d
    .line 1518
    :goto_d
    new-instance v6, Landroid/content/IntentFilter;

    #@f
    invoke-direct {v6}, Landroid/content/IntentFilter;-><init>()V

    #@12
    .line 1519
    .local v6, intentFilter:Landroid/content/IntentFilter;
    const-string v0, "android.net.conn.CONNECTIVITY_CHANGE"

    #@14
    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@17
    .line 1520
    const-string v0, "android.net.wifi.WIFI_STATE_CHANGED"

    #@19
    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1c
    .line 1521
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mContext:Landroid/content/Context;

    #@1e
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@20
    const-string v2, "android.permission.BIND_DEVICE_ADMIN"

    #@22
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mAgentHandler:Landroid/os/Handler;

    #@24
    invoke-virtual {v0, v1, v6, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@27
    .line 1523
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@29
    const-string v1, "XTWiFiOS"

    #@2b
    new-instance v2, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v3, "installing observers. thread id "

    #@32
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v2

    #@36
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v3}, Ljava/lang/Thread;->getId()J

    #@3d
    move-result-wide v3

    #@3e
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@41
    move-result-object v2

    #@42
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v2

    #@46
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@49
    .line 1524
    return-void

    #@4a
    .line 1505
    .end local v6           #intentFilter:Landroid/content/IntentFilter;
    :cond_4a
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mResolver:Landroid/content/ContentResolver;

    #@4c
    sget-object v1, Landroid/provider/Settings$Secure;->CONTENT_URI:Landroid/net/Uri;

    #@4e
    new-array v2, v12, [Ljava/lang/String;

    #@50
    const-string v3, "name"

    #@52
    aput-object v3, v2, v11

    #@54
    const-string v3, "value"

    #@56
    aput-object v3, v2, v10

    #@58
    const-string v3, "(name=?) or (name=?) "

    #@5a
    new-array v4, v10, [Ljava/lang/String;

    #@5c
    const-string v9, "location_providers_allowed"

    #@5e
    aput-object v9, v4, v11

    #@60
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@63
    move-result-object v7

    #@64
    .line 1508
    .local v7, secureSettingsCursor:Landroid/database/Cursor;
    new-instance v0, Landroid/content/ContentQueryMap;

    #@66
    const-string v1, "name"

    #@68
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mAgentHandler:Landroid/os/Handler;

    #@6a
    invoke-direct {v0, v7, v1, v10, v2}, Landroid/content/ContentQueryMap;-><init>(Landroid/database/Cursor;Ljava/lang/String;ZLandroid/os/Handler;)V

    #@6d
    iput-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mSecureSettings:Landroid/content/ContentQueryMap;

    #@6f
    .line 1509
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mSecureSettings:Landroid/content/ContentQueryMap;

    #@71
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->secureSettingsObserver:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$SecureSettingsObserver;

    #@73
    invoke-virtual {v0, v1}, Landroid/content/ContentQueryMap;->addObserver(Ljava/util/Observer;)V

    #@76
    .line 1511
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mResolver:Landroid/content/ContentResolver;

    #@78
    sget-object v1, Landroid/provider/Settings$System;->CONTENT_URI:Landroid/net/Uri;

    #@7a
    new-array v2, v12, [Ljava/lang/String;

    #@7c
    const-string v3, "name"

    #@7e
    aput-object v3, v2, v11

    #@80
    const-string v3, "value"

    #@82
    aput-object v3, v2, v10

    #@84
    const-string v3, "(name=?) or (name=?) "

    #@86
    new-array v4, v10, [Ljava/lang/String;

    #@88
    const-string v9, "airplane_mode_on"

    #@8a
    aput-object v9, v4, v11

    #@8c
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@8f
    move-result-object v8

    #@90
    .line 1514
    .local v8, systemSettingsCursor:Landroid/database/Cursor;
    new-instance v0, Landroid/content/ContentQueryMap;

    #@92
    const-string v1, "name"

    #@94
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mAgentHandler:Landroid/os/Handler;

    #@96
    invoke-direct {v0, v8, v1, v10, v2}, Landroid/content/ContentQueryMap;-><init>(Landroid/database/Cursor;Ljava/lang/String;ZLandroid/os/Handler;)V

    #@99
    iput-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mSystemSettings:Landroid/content/ContentQueryMap;

    #@9b
    .line 1515
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mSystemSettings:Landroid/content/ContentQueryMap;

    #@9d
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->systemSettingsObserver:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$SystemSettingsObserver;

    #@9f
    invoke-virtual {v0, v1}, Landroid/content/ContentQueryMap;->addObserver(Ljava/util/Observer;)V

    #@a2
    goto/16 :goto_d
.end method

.method private isNumeric(Ljava/lang/String;)Z
    .registers 4
    .parameter "strToCheck"

    #@0
    .prologue
    .line 1173
    :try_start_0
    invoke-static {p1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_3} :catch_5

    #@3
    .line 1180
    const/4 v1, 0x1

    #@4
    :goto_4
    return v1

    #@5
    .line 1175
    :catch_5
    move-exception v0

    #@6
    .line 1177
    .local v0, nfe:Ljava/lang/NumberFormatException;
    const/4 v1, 0x0

    #@7
    goto :goto_4
.end method

.method private setupNewApiIfNecessary()V
    .registers 3

    #@0
    .prologue
    .line 150
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    #@2
    const/16 v1, 0x11

    #@4
    if-lt v0, v1, :cond_e

    #@6
    .line 152
    new-instance v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$ApiLevel17;

    #@8
    const/4 v1, 0x0

    #@9
    invoke-direct {v0, p0, v1}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$ApiLevel17;-><init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$1;)V

    #@c
    iput-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->m_api_level_17:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$ApiLevel17;

    #@e
    .line 154
    :cond_e
    return-void
.end method

.method private triggerStatusUpdate(Z)V
    .registers 10
    .parameter "location_update_event"

    #@0
    .prologue
    .line 1450
    iget-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mStatusUpdateSubscriber:Ljava/util/Map;

    #@2
    invoke-interface {v5}, Ljava/util/Map;->isEmpty()Z

    #@5
    move-result v5

    #@6
    if-eqz v5, :cond_9

    #@8
    .line 1494
    :cond_8
    return-void

    #@9
    .line 1456
    :cond_9
    invoke-direct {p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->updateStatus()V

    #@c
    .line 1459
    iget-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mStatusUpdateSubscriber:Ljava/util/Map;

    #@e
    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    #@11
    move-result-object v5

    #@12
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@15
    move-result-object v1

    #@16
    .local v1, i$:Ljava/util/Iterator;
    :cond_16
    :goto_16
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@19
    move-result v5

    #@1a
    if-eqz v5, :cond_8

    #@1c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1f
    move-result-object v4

    #@20
    check-cast v4, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$UpdateSubscriber;

    #@22
    .line 1461
    .local v4, subscriber:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$UpdateSubscriber;
    if-eqz p1, :cond_28

    #@24
    iget-boolean v5, v4, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$UpdateSubscriber;->need_location_update:Z

    #@26
    if-eqz v5, :cond_16

    #@28
    .line 1469
    :cond_28
    :try_start_28
    new-instance v3, Lcom/qualcomm/lib/location/mq_client/OutPostcard;

    #@2a
    invoke-direct {v3}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;-><init>()V

    #@2d
    .line 1470
    .local v3, out:Lcom/qualcomm/lib/location/mq_client/OutPostcard;
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->init()V

    #@30
    .line 1471
    const-string v5, "TO"

    #@32
    iget-object v6, v4, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$UpdateSubscriber;->ipc_name:Ljava/lang/String;

    #@34
    invoke-virtual {v3, v5, v6}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@37
    .line 1472
    const-string v5, "FROM"

    #@39
    const-string v6, "OS-Agent"

    #@3b
    invoke-virtual {v3, v5, v6}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@3e
    .line 1473
    const-string v5, "INFO"

    #@40
    const-string v6, "OS-STATUS-UPDATE"

    #@42
    invoke-virtual {v3, v5, v6}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@45
    .line 1474
    const-string v5, "TX-ID"

    #@47
    iget v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->next_req_tx_id:I

    #@49
    int-to-long v6, v6

    #@4a
    invoke-virtual {v3, v5, v6, v7}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addInt32(Ljava/lang/String;J)V

    #@4d
    .line 1475
    iget v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->next_req_tx_id:I

    #@4f
    add-int/lit8 v5, v5, 0x1

    #@51
    iput v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->next_req_tx_id:I

    #@53
    .line 1476
    iget v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->next_req_tx_id:I

    #@55
    const/16 v6, 0x3e8

    #@57
    if-le v5, v6, :cond_5c

    #@59
    .line 1478
    const/4 v5, 0x1

    #@5a
    iput v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->next_req_tx_id:I

    #@5c
    .line 1481
    :cond_5c
    invoke-direct {p0, v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->fillStatusUpdateMessage(Lcom/qualcomm/lib/location/mq_client/OutPostcard;)V

    #@5f
    .line 1483
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->seal()V

    #@62
    .line 1484
    iget-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->m_ipcClient:Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@64
    invoke-virtual {v5}, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->getOutputStream()Ljava/io/OutputStream;

    #@67
    move-result-object v2

    #@68
    .line 1485
    .local v2, os:Ljava/io/OutputStream;
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->getEncodedBuffer()[B

    #@6b
    move-result-object v5

    #@6c
    invoke-virtual {v2, v5}, Ljava/io/OutputStream;->write([B)V

    #@6f
    .line 1486
    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V
    :try_end_72
    .catch Ljava/lang/Exception; {:try_start_28 .. :try_end_72} :catch_73

    #@72
    goto :goto_16

    #@73
    .line 1488
    .end local v2           #os:Ljava/io/OutputStream;
    .end local v3           #out:Lcom/qualcomm/lib/location/mq_client/OutPostcard;
    :catch_73
    move-exception v0

    #@74
    .line 1490
    .local v0, e:Ljava/lang/Exception;
    iget-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@76
    const-string v6, "XTWiFiOS"

    #@78
    const-string v7, "Cannot generate status update notification"

    #@7a
    invoke-virtual {v5, v6, v7}, Lcom/qualcomm/lib/location/log/LocLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@7d
    .line 1491
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@80
    goto :goto_16
.end method

.method private updateStatus()V
    .registers 13

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    .line 569
    iget-object v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mResolver:Landroid/content/ContentResolver;

    #@4
    const-string v9, "location_providers_allowed"

    #@6
    invoke-static {v8, v9}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v8

    #@a
    iput-object v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mAllowedProviders:Ljava/lang/String;

    #@c
    .line 571
    iget-object v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mAllowedProviders:Ljava/lang/String;

    #@e
    if-eqz v8, :cond_a3

    #@10
    .line 573
    iget-object v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mAllowedProviders:Ljava/lang/String;

    #@12
    const-string v9, ","

    #@14
    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@17
    move-result-object v0

    #@18
    .line 575
    .local v0, allowed_providers_array:[Ljava/lang/String;
    iput-boolean v7, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->is_GPS_enabled:Z

    #@1a
    .line 576
    iput-boolean v7, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->is_NLP_enabled:Z

    #@1c
    .line 577
    iput-boolean v7, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->is_ENH_enabled:Z

    #@1e
    .line 579
    if-eqz v0, :cond_73

    #@20
    .line 581
    move-object v1, v0

    #@21
    .local v1, arr$:[Ljava/lang/String;
    array-length v4, v1

    #@22
    .local v4, len$:I
    const/4 v3, 0x0

    #@23
    .local v3, i$:I
    :goto_23
    if-ge v3, v4, :cond_3f

    #@25
    aget-object v5, v1, v3

    #@27
    .line 583
    .local v5, name:Ljava/lang/String;
    const-string v8, "gps"

    #@29
    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2c
    move-result v8

    #@2d
    if-eqz v8, :cond_34

    #@2f
    .line 585
    iput-boolean v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->is_GPS_enabled:Z

    #@31
    .line 581
    :cond_31
    :goto_31
    add-int/lit8 v3, v3, 0x1

    #@33
    goto :goto_23

    #@34
    .line 587
    :cond_34
    const-string v8, "network"

    #@36
    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@39
    move-result v8

    #@3a
    if-eqz v8, :cond_31

    #@3c
    .line 589
    iput-boolean v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->is_NLP_enabled:Z

    #@3e
    goto :goto_31

    #@3f
    .line 593
    .end local v5           #name:Ljava/lang/String;
    :cond_3f
    iget-object v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@41
    const-string v9, "XTWiFiOS"

    #@43
    new-instance v10, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v11, "Allowed location providers: ["

    #@4a
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v10

    #@4e
    iget-object v11, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mAllowedProviders:Ljava/lang/String;

    #@50
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v10

    #@54
    const-string v11, "] GPS Enabled: "

    #@56
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v10

    #@5a
    iget-boolean v11, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->is_GPS_enabled:Z

    #@5c
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v10

    #@60
    const-string v11, ", NLP enabled: "

    #@62
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v10

    #@66
    iget-boolean v11, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->is_NLP_enabled:Z

    #@68
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v10

    #@6c
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6f
    move-result-object v10

    #@70
    invoke-virtual {v8, v9, v10}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@73
    .line 597
    .end local v1           #arr$:[Ljava/lang/String;
    .end local v3           #i$:I
    .end local v4           #len$:I
    :cond_73
    iget-object v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mResolver:Landroid/content/ContentResolver;

    #@75
    const-string v9, "enhLocationServices_on"

    #@77
    invoke-static {v8, v9}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@7a
    move-result-object v2

    #@7b
    .line 599
    .local v2, enhanced_location_services:Ljava/lang/String;
    if-eqz v2, :cond_143

    #@7d
    .line 601
    const-string v8, "1"

    #@7f
    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@82
    move-result v8

    #@83
    if-eqz v8, :cond_87

    #@85
    .line 603
    iput-boolean v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->is_ENH_enabled:Z

    #@87
    .line 610
    :cond_87
    :goto_87
    iget-object v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@89
    const-string v9, "XTWiFiOS"

    #@8b
    new-instance v10, Ljava/lang/StringBuilder;

    #@8d
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@90
    const-string v11, "Enhanced location services: "

    #@92
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v10

    #@96
    iget-boolean v11, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->is_ENH_enabled:Z

    #@98
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v10

    #@9c
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9f
    move-result-object v10

    #@a0
    invoke-virtual {v8, v9, v10}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@a3
    .line 613
    .end local v0           #allowed_providers_array:[Ljava/lang/String;
    .end local v2           #enhanced_location_services:Ljava/lang/String;
    :cond_a3
    iget-object v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@a5
    invoke-virtual {v8}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    #@a8
    move-result v8

    #@a9
    iput-boolean v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->is_WIFI_on:Z

    #@ab
    .line 616
    :try_start_ab
    iget-object v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->m_api_level_17:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$ApiLevel17;

    #@ad
    if-eqz v8, :cond_161

    #@af
    .line 618
    iget-object v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->m_api_level_17:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$ApiLevel17;

    #@b1
    invoke-virtual {v6}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$ApiLevel17;->getIsAirplaneModeOn()Z

    #@b4
    move-result v6

    #@b5
    iput-boolean v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->is_airplane_mode_on:Z
    :try_end_b7
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_ab .. :try_end_b7} :catch_16f

    #@b7
    .line 647
    :goto_b7
    iget-object v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@b9
    const-string v7, "XTWiFiOS"

    #@bb
    new-instance v8, Ljava/lang/StringBuilder;

    #@bd
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@c0
    const-string v9, "is airplane mode ON: "

    #@c2
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v8

    #@c6
    iget-boolean v9, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->is_airplane_mode_on:Z

    #@c8
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@cb
    move-result-object v8

    #@cc
    const-string v9, " is_WIFI_on: "

    #@ce
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v8

    #@d2
    iget-boolean v9, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->is_WIFI_on:Z

    #@d4
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v8

    #@d8
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@db
    move-result-object v8

    #@dc
    invoke-virtual {v6, v7, v8}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@df
    .line 649
    iget-boolean v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->is_connected:Z

    #@e1
    if-eqz v6, :cond_174

    #@e3
    .line 651
    iget-object v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@e5
    const-string v7, "XTWiFiOS"

    #@e7
    const-string v8, "we have connectivity"

    #@e9
    invoke-virtual {v6, v7, v8}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@ec
    .line 658
    :goto_ec
    iget-object v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mConnectivityManager:Landroid/net/ConnectivityManager;

    #@ee
    if-eqz v6, :cond_189

    #@f0
    .line 660
    iget-object v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mConnectivityManager:Landroid/net/ConnectivityManager;

    #@f2
    invoke-virtual {v6}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    #@f5
    move-result-object v6

    #@f6
    iput-object v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mActiveNetworkInfo:Landroid/net/NetworkInfo;

    #@f8
    .line 662
    iget-object v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mActiveNetworkInfo:Landroid/net/NetworkInfo;

    #@fa
    if-eqz v6, :cond_17f

    #@fc
    .line 664
    iget-object v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@fe
    const-string v7, "XTWiFiOS"

    #@100
    new-instance v8, Ljava/lang/StringBuilder;

    #@102
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@105
    const-string v9, "Active network type ["

    #@107
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10a
    move-result-object v8

    #@10b
    iget-object v9, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mActiveNetworkInfo:Landroid/net/NetworkInfo;

    #@10d
    invoke-virtual {v9}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    #@110
    move-result-object v9

    #@111
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@114
    move-result-object v8

    #@115
    const-string v9, "], availibility ["

    #@117
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11a
    move-result-object v8

    #@11b
    iget-object v9, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mActiveNetworkInfo:Landroid/net/NetworkInfo;

    #@11d
    invoke-virtual {v9}, Landroid/net/NetworkInfo;->isAvailable()Z

    #@120
    move-result v9

    #@121
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@124
    move-result-object v8

    #@125
    const-string v9, "], connected ["

    #@127
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12a
    move-result-object v8

    #@12b
    iget-object v9, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mActiveNetworkInfo:Landroid/net/NetworkInfo;

    #@12d
    invoke-virtual {v9}, Landroid/net/NetworkInfo;->isConnected()Z

    #@130
    move-result v9

    #@131
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@134
    move-result-object v8

    #@135
    const-string v9, "]"

    #@137
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13a
    move-result-object v8

    #@13b
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13e
    move-result-object v8

    #@13f
    invoke-virtual {v6, v7, v8}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@142
    .line 676
    :goto_142
    return-void

    #@143
    .line 608
    .restart local v0       #allowed_providers_array:[Ljava/lang/String;
    .restart local v2       #enhanced_location_services:Ljava/lang/String;
    :cond_143
    iget-object v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@145
    const-string v9, "XTWiFiOS"

    #@147
    new-instance v10, Ljava/lang/StringBuilder;

    #@149
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@14c
    const-string v11, "No entry in secure settings for enhanced location services: "

    #@14e
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@151
    move-result-object v10

    #@152
    iget-boolean v11, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->is_ENH_enabled:Z

    #@154
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@157
    move-result-object v10

    #@158
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15b
    move-result-object v10

    #@15c
    invoke-virtual {v8, v9, v10}, Lcom/qualcomm/lib/location/log/LocLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@15f
    goto/16 :goto_87

    #@161
    .line 622
    .end local v0           #allowed_providers_array:[Ljava/lang/String;
    .end local v2           #enhanced_location_services:Ljava/lang/String;
    :cond_161
    :try_start_161
    iget-object v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mResolver:Landroid/content/ContentResolver;

    #@163
    const-string v9, "airplane_mode_on"

    #@165
    invoke-static {v8, v9}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    #@168
    move-result v8

    #@169
    if-ne v8, v6, :cond_172

    #@16b
    :goto_16b
    iput-boolean v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->is_airplane_mode_on:Z
    :try_end_16d
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_161 .. :try_end_16d} :catch_16f

    #@16d
    goto/16 :goto_b7

    #@16f
    .line 625
    :catch_16f
    move-exception v6

    #@170
    goto/16 :goto_b7

    #@172
    :cond_172
    move v6, v7

    #@173
    .line 622
    goto :goto_16b

    #@174
    .line 655
    :cond_174
    iget-object v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@176
    const-string v7, "XTWiFiOS"

    #@178
    const-string v8, "we DO NOT have connectivity"

    #@17a
    invoke-virtual {v6, v7, v8}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@17d
    goto/16 :goto_ec

    #@17f
    .line 669
    :cond_17f
    iget-object v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@181
    const-string v7, "XTWiFiOS"

    #@183
    const-string v8, "Active network info is not available"

    #@185
    invoke-virtual {v6, v7, v8}, Lcom/qualcomm/lib/location/log/LocLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@188
    goto :goto_142

    #@189
    .line 674
    :cond_189
    iget-object v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@18b
    const-string v7, "XTWiFiOS"

    #@18d
    const-string v8, "Connectivity manager service is not available"

    #@18f
    invoke-virtual {v6, v7, v8}, Lcom/qualcomm/lib/location/log/LocLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@192
    goto :goto_142
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 5
    .parameter "intent"

    #@0
    .prologue
    .line 365
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@2
    const-string v1, "XTWiFiOS"

    #@4
    const-string v2, "onBind"

    #@6
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 366
    const/4 v0, 0x0

    #@a
    return-object v0
.end method

.method public onCreate()V
    .registers 10

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 241
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@3
    const-string v1, "XTWiFiOS"

    #@5
    const-string v2, "onCreate"

    #@7
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 243
    const-string v0, "ro.qc.sdk.izat.premium_enabled"

    #@c
    const-string v1, "0"

    #@e
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@11
    move-result-object v8

    #@12
    .line 244
    .local v8, iZat_enabled:Ljava/lang/String;
    const-string v0, "1"

    #@14
    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v0

    #@18
    if-nez v0, :cond_d3

    #@1a
    .line 249
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@1c
    const-string v1, "XTWiFiOS"

    #@1e
    const-string v2, "iZat is not enabled. Continue initialization"

    #@20
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@23
    .line 256
    :goto_23
    invoke-virtual {p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->getApplicationContext()Landroid/content/Context;

    #@26
    move-result-object v0

    #@27
    iput-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mContext:Landroid/content/Context;

    #@29
    .line 257
    invoke-virtual {p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->getContentResolver()Landroid/content/ContentResolver;

    #@2c
    move-result-object v0

    #@2d
    iput-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mResolver:Landroid/content/ContentResolver;

    #@2f
    .line 261
    :try_start_2f
    const-string v0, "location"

    #@31
    invoke-virtual {p0, v0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@34
    move-result-object v0

    #@35
    check-cast v0, Landroid/location/LocationManager;

    #@37
    iput-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mLocationManager:Landroid/location/LocationManager;

    #@39
    .line 262
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mLocationManager:Landroid/location/LocationManager;

    #@3b
    if-nez v0, :cond_46

    #@3d
    .line 264
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@3f
    const-string v1, "XTWiFiOS"

    #@41
    const-string v2, "Unable to get LOCATION_SERVICE"

    #@43
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@46
    .line 267
    :cond_46
    const-string v0, "connectivity"

    #@48
    invoke-virtual {p0, v0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@4b
    move-result-object v0

    #@4c
    check-cast v0, Landroid/net/ConnectivityManager;

    #@4e
    iput-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mConnectivityManager:Landroid/net/ConnectivityManager;

    #@50
    .line 268
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mConnectivityManager:Landroid/net/ConnectivityManager;

    #@52
    if-nez v0, :cond_5d

    #@54
    .line 270
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@56
    const-string v1, "XTWiFiOS"

    #@58
    const-string v2, "Unable to get CONNECTIVITY_SERVICE"

    #@5a
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@5d
    .line 273
    :cond_5d
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mContext:Landroid/content/Context;

    #@5f
    const-string v1, "wifi"

    #@61
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@64
    move-result-object v0

    #@65
    check-cast v0, Landroid/net/wifi/WifiManager;

    #@67
    iput-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@69
    .line 274
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@6b
    if-nez v0, :cond_76

    #@6d
    .line 276
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@6f
    const-string v1, "XTWiFiOS"

    #@71
    const-string v2, "Unable to get WIFI_SERVICE"

    #@73
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@76
    .line 278
    :cond_76
    const-string v0, "phone"

    #@78
    invoke-virtual {p0, v0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@7b
    move-result-object v0

    #@7c
    check-cast v0, Landroid/telephony/TelephonyManager;

    #@7e
    iput-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mTelMgr:Landroid/telephony/TelephonyManager;

    #@80
    .line 279
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mTelMgr:Landroid/telephony/TelephonyManager;

    #@82
    if-nez v0, :cond_8d

    #@84
    .line 281
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@86
    const-string v1, "XTWiFiOS"

    #@88
    const-string v2, "Unable to get TELEPHONY_SERVICE"

    #@8a
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@8d
    .line 284
    :cond_8d
    new-instance v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$RilListener;

    #@8f
    const/4 v1, 0x0

    #@90
    invoke-direct {v0, p0, v1}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$RilListener;-><init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$1;)V

    #@93
    iput-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mRilListener:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$RilListener;

    #@95
    .line 287
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mTelMgr:Landroid/telephony/TelephonyManager;

    #@97
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mRilListener:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$RilListener;

    #@99
    const/4 v2, 0x1

    #@9a
    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V
    :try_end_9d
    .catch Ljava/lang/RuntimeException; {:try_start_2f .. :try_end_9d} :catch_de

    #@9d
    .line 311
    :goto_9d
    new-instance v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread;

    #@9f
    invoke-direct {v0, p0, v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread;-><init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$1;)V

    #@a2
    iput-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->agentThread:Ljava/lang/Thread;

    #@a4
    .line 312
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->agentThread:Ljava/lang/Thread;

    #@a6
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    #@a9
    .line 317
    :goto_a9
    :try_start_a9
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->agentInitialized:Ljava/util/concurrent/CountDownLatch;

    #@ab
    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_ae
    .catch Ljava/lang/InterruptedException; {:try_start_a9 .. :try_end_ae} :catch_ec

    #@ae
    .line 328
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mAgentHandler:Landroid/os/Handler;

    #@b0
    if-eqz v0, :cond_105

    #@b2
    .line 331
    invoke-direct {p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->installObservers()V

    #@b5
    .line 333
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mLocationManager:Landroid/location/LocationManager;

    #@b7
    if-eqz v0, :cond_cb

    #@b9
    .line 337
    :try_start_b9
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mLocationManager:Landroid/location/LocationManager;

    #@bb
    const-string v1, "passive"

    #@bd
    const-wide/16 v2, 0x0

    #@bf
    const/4 v4, 0x0

    #@c0
    iget-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mPassiveListener:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$PassiveLocationListener;

    #@c2
    iget-object v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mAgentHandler:Landroid/os/Handler;

    #@c4
    invoke-virtual {v6}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    #@c7
    move-result-object v6

    #@c8
    invoke-virtual/range {v0 .. v6}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V
    :try_end_cb
    .catch Ljava/lang/RuntimeException; {:try_start_b9 .. :try_end_cb} :catch_f7

    #@cb
    .line 348
    :cond_cb
    :goto_cb
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mAgentHandler:Landroid/os/Handler;

    #@cd
    const/16 v1, 0x64

    #@cf
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@d2
    .line 354
    :goto_d2
    return-void

    #@d3
    .line 253
    :cond_d3
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@d5
    const-string v1, "XTWiFiOS"

    #@d7
    const-string v2, "iZat is enabled. Continue initialization"

    #@d9
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@dc
    goto/16 :goto_23

    #@de
    .line 289
    :catch_de
    move-exception v7

    #@df
    .line 291
    .local v7, e:Ljava/lang/RuntimeException;
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@e1
    const-string v1, "XTWiFiOS"

    #@e3
    const-string v2, "cannot acquire location/connectivity/telephony service or failed to initialize RilListener"

    #@e5
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@e8
    .line 292
    invoke-virtual {v7}, Ljava/lang/RuntimeException;->printStackTrace()V

    #@eb
    goto :goto_9d

    #@ec
    .line 320
    .end local v7           #e:Ljava/lang/RuntimeException;
    :catch_ec
    move-exception v7

    #@ed
    .line 324
    .local v7, e:Ljava/lang/InterruptedException;
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@ef
    const-string v1, "XTWiFiOS"

    #@f1
    const-string v2, "Constructor interrupted, retry"

    #@f3
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@f6
    goto :goto_a9

    #@f7
    .line 340
    .end local v7           #e:Ljava/lang/InterruptedException;
    :catch_f7
    move-exception v7

    #@f8
    .line 342
    .local v7, e:Ljava/lang/RuntimeException;
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@fa
    const-string v1, "XTWiFiOS"

    #@fc
    const-string v2, "Cannot request for passive location update"

    #@fe
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@101
    .line 343
    invoke-virtual {v7}, Ljava/lang/RuntimeException;->printStackTrace()V

    #@104
    goto :goto_cb

    #@105
    .line 352
    .end local v7           #e:Ljava/lang/RuntimeException;
    :cond_105
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@107
    const-string v1, "XTWiFiOS"

    #@109
    const-string v2, "OSAgent handler is null"

    #@10b
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@10e
    goto :goto_d2
.end method

.method public onDestroy()V
    .registers 4

    #@0
    .prologue
    .line 358
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@2
    const-string v1, "XTWiFiOS"

    #@4
    const-string v2, "onDestroy"

    #@6
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 359
    return-void
.end method
