.class public Lcom/qualcomm/services/location/GeoFenceKeeper;
.super Ljava/lang/Object;
.source "GeoFenceKeeper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/services/location/GeoFenceKeeper$BreachEvent;,
        Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;,
        Lcom/qualcomm/services/location/GeoFenceKeeper$MyHandler;
    }
.end annotation


# static fields
.field private static final MSGS:[Ljava/lang/String; = null

.field private static final MSG_ADD:I = 0x0

.field private static final MSG_BREACH:I = 0x4

.field private static final MSG_EXPIRE:I = 0x3

.field private static final MSG_REMOVE:I = 0x1

.field private static final MSG_REMOVE_APP:I = 0x2

.field private static final MSG_SSR:I = 0x5

.field private static final ON_TARGET_DEBUG:Z = true

.field private static final TAG:Ljava/lang/String; = "GeoFenceKeeper"

.field private static final TEXT_ACTION_CHECK_GEOFENCES:Ljava/lang/String; = "com.qualcomm.services.location.geofence.checkgeofences"

.field private static final TEXT_ACTION_POS_INJ:Ljava/lang/String; = "com.qualcomm.services.location.geofence.breach"

.field private static final TIME_UNIT_IN_MS:I = 0x10000

.field private static final VERBOSE_DBG:Z = true

.field private static mGeoFenceEnumerater:I


# instance fields
.field private mAllGeoFences:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/app/PendingIntent;",
            "Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private final mHandler:Lcom/qualcomm/services/location/GeoFenceKeeper$MyHandler;

.field private mRandom:Ljava/util/Random;

.field private mRunningGeoFences:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;",
            ">;"
        }
    .end annotation
.end field

.field private final mTestPosInjRcvr:Landroid/content/BroadcastReceiver;

.field private mTickingGeoFences:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/HashSet",
            "<",
            "Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 39
    const/4 v0, 0x6

    #@2
    new-array v0, v0, [Ljava/lang/String;

    #@4
    const/4 v1, 0x0

    #@5
    const-string v2, "MSG_ADD"

    #@7
    aput-object v2, v0, v1

    #@9
    const-string v1, "MSG_REMOVE"

    #@b
    aput-object v1, v0, v3

    #@d
    const/4 v1, 0x2

    #@e
    const-string v2, "MSG_REMOVE_APP"

    #@10
    aput-object v2, v0, v1

    #@12
    const/4 v1, 0x3

    #@13
    const-string v2, "MSG_EXPIRE"

    #@15
    aput-object v2, v0, v1

    #@17
    const/4 v1, 0x4

    #@18
    const-string v2, "MSG_BREACH"

    #@1a
    aput-object v2, v0, v1

    #@1c
    const/4 v1, 0x5

    #@1d
    const-string v2, "MSG_SSR"

    #@1f
    aput-object v2, v0, v1

    #@21
    sput-object v0, Lcom/qualcomm/services/location/GeoFenceKeeper;->MSGS:[Ljava/lang/String;

    #@23
    .line 54
    sput v3, Lcom/qualcomm/services/location/GeoFenceKeeper;->mGeoFenceEnumerater:I

    #@25
    .line 355
    const-string v0, "geofence"

    #@27
    #invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@2a
    .line 356
    invoke-static {}, Lcom/qualcomm/services/location/GeoFenceKeeper;->classInit()V

    #@2d
    .line 357
    return-void
.end method

.method public constructor <init>(Landroid/os/Looper;Landroid/content/Context;)V
    .registers 4
    .parameter "looper"
    .parameter "context"

    #@0
    .prologue
    .line 62
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 366
    new-instance v0, Lcom/qualcomm/services/location/GeoFenceKeeper$1;

    #@5
    invoke-direct {v0, p0}, Lcom/qualcomm/services/location/GeoFenceKeeper$1;-><init>(Lcom/qualcomm/services/location/GeoFenceKeeper;)V

    #@8
    iput-object v0, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mTestPosInjRcvr:Landroid/content/BroadcastReceiver;

    #@a
    .line 63
    new-instance v0, Lcom/qualcomm/services/location/GeoFenceKeeper$MyHandler;

    #@c
    invoke-direct {v0, p0, p1}, Lcom/qualcomm/services/location/GeoFenceKeeper$MyHandler;-><init>(Lcom/qualcomm/services/location/GeoFenceKeeper;Landroid/os/Looper;)V

    #@f
    iput-object v0, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mHandler:Lcom/qualcomm/services/location/GeoFenceKeeper$MyHandler;

    #@11
    .line 64
    iput-object p2, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mContext:Landroid/content/Context;

    #@13
    .line 65
    new-instance v0, Ljava/util/HashMap;

    #@15
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@18
    iput-object v0, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mAllGeoFences:Ljava/util/HashMap;

    #@1a
    .line 66
    new-instance v0, Ljava/util/HashMap;

    #@1c
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@1f
    iput-object v0, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mRunningGeoFences:Ljava/util/HashMap;

    #@21
    .line 67
    new-instance v0, Ljava/util/HashMap;

    #@23
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@26
    iput-object v0, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mTickingGeoFences:Ljava/util/HashMap;

    #@28
    .line 69
    invoke-direct {p0}, Lcom/qualcomm/services/location/GeoFenceKeeper;->ssrestart()V

    #@2b
    .line 71
    invoke-direct {p0}, Lcom/qualcomm/services/location/GeoFenceKeeper;->setTestEnv()V

    #@2e
    .line 72
    return-void
.end method

.method static synthetic access$100()[Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 36
    sget-object v0, Lcom/qualcomm/services/location/GeoFenceKeeper;->MSGS:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$1500(Lcom/qualcomm/services/location/GeoFenceKeeper;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Lcom/qualcomm/services/location/GeoFenceKeeper;->handleBreachEvent(II)V

    #@3
    return-void
.end method

.method static synthetic access$1600(Lcom/qualcomm/services/location/GeoFenceKeeper;)Ljava/util/HashMap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 36
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mAllGeoFences:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method static synthetic access$1700(Lcom/qualcomm/services/location/GeoFenceKeeper;)Ljava/util/Random;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 36
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mRandom:Ljava/util/Random;

    #@2
    return-object v0
.end method

.method static synthetic access$1800(Lcom/qualcomm/services/location/GeoFenceKeeper;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/qualcomm/services/location/GeoFenceKeeper;->testBreach(I)V

    #@3
    return-void
.end method

.method static synthetic access$1900(Lcom/qualcomm/services/location/GeoFenceKeeper;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 36
    invoke-direct {p0}, Lcom/qualcomm/services/location/GeoFenceKeeper;->debugCheckData()V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/qualcomm/services/location/GeoFenceKeeper;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/qualcomm/services/location/GeoFenceKeeper;->handleAddGeoFence(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/qualcomm/services/location/GeoFenceKeeper;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/qualcomm/services/location/GeoFenceKeeper;->handleRemoveGeoFence(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/qualcomm/services/location/GeoFenceKeeper;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/qualcomm/services/location/GeoFenceKeeper;->handleRemoveGeoFenceApp(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$500(Lcom/qualcomm/services/location/GeoFenceKeeper;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/qualcomm/services/location/GeoFenceKeeper;->handleExpiredGeoFences(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$600(Lcom/qualcomm/services/location/GeoFenceKeeper;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/qualcomm/services/location/GeoFenceKeeper;->handleBreachEvent(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$700(Lcom/qualcomm/services/location/GeoFenceKeeper;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/qualcomm/services/location/GeoFenceKeeper;->handleSSR(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$800(J)J
    .registers 4
    .parameter "x0"

    #@0
    .prologue
    .line 36
    invoke-static {p0, p1}, Lcom/qualcomm/services/location/GeoFenceKeeper;->upTimeInTimeUnit(J)J

    #@3
    move-result-wide v0

    #@4
    return-wide v0
.end method

.method static synthetic access$908()I
    .registers 2

    #@0
    .prologue
    .line 36
    sget v0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mGeoFenceEnumerater:I

    #@2
    add-int/lit8 v1, v0, 0x1

    #@4
    sput v1, Lcom/qualcomm/services/location/GeoFenceKeeper;->mGeoFenceEnumerater:I

    #@6
    return v0
.end method

.method private breachEvent(IIDD)V
    .registers 10
    .parameter "engID"
    .parameter "whichWay"
    .parameter "lat"
    .parameter "lon"

    #@0
    .prologue
    .line 100
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mHandler:Lcom/qualcomm/services/location/GeoFenceKeeper$MyHandler;

    #@2
    iget-object v1, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mHandler:Lcom/qualcomm/services/location/GeoFenceKeeper$MyHandler;

    #@4
    const/4 v2, 0x4

    #@5
    invoke-static {v1, v2, p1, p2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v0, v1}, Lcom/qualcomm/services/location/GeoFenceKeeper$MyHandler;->sendMessage(Landroid/os/Message;)Z

    #@c
    .line 101
    return-void
.end method

.method private static classInit()V
	.registers 0
	return-void
.end method

.method private debugCheckData()V
    .registers 16

    #@0
    .prologue
    .line 414
    const-string v13, "GeoFenceKeeper"

    #@2
    const-string v14, "debugCheckData: number checking..."

    #@4
    invoke-static {v13, v14}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 416
    iget-object v13, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mAllGeoFences:Ljava/util/HashMap;

    #@9
    invoke-virtual {v13}, Ljava/util/HashMap;->size()I

    #@c
    move-result v1

    #@d
    .line 417
    .local v1, allSize:I
    iget-object v13, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mRunningGeoFences:Ljava/util/HashMap;

    #@f
    invoke-virtual {v13}, Ljava/util/HashMap;->size()I

    #@12
    move-result v10

    #@13
    .line 419
    .local v10, runningSize:I
    if-eq v1, v10, :cond_38

    #@15
    .line 420
    new-instance v13, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v14, "debugCheckData size check failed.. allSize - "

    #@1c
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v13

    #@20
    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23
    move-result-object v13

    #@24
    const-string v14, " runningSize - "

    #@26
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v13

    #@2a
    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v13

    #@2e
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v8

    #@32
    .line 422
    .local v8, msg:Ljava/lang/String;
    new-instance v13, Ljava/lang/Error;

    #@34
    invoke-direct {v13, v8}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    #@37
    throw v13

    #@38
    .line 425
    .end local v8           #msg:Ljava/lang/String;
    :cond_38
    const-string v13, "GeoFenceKeeper"

    #@3a
    const-string v14, "debugCheckData: mRunningGeoFences KEY checking..."

    #@3c
    invoke-static {v13, v14}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    .line 426
    iget-object v13, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mRunningGeoFences:Ljava/util/HashMap;

    #@41
    invoke-virtual {v13}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@44
    move-result-object v13

    #@45
    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@48
    move-result-object v7

    #@49
    .local v7, i$:Ljava/util/Iterator;
    :cond_49
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@4c
    move-result v13

    #@4d
    if-eqz v13, :cond_8e

    #@4f
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@52
    move-result-object v5

    #@53
    check-cast v5, Ljava/util/Map$Entry;

    #@55
    .line 427
    .local v5, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;>;"
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@58
    move-result-object v14

    #@59
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@5c
    move-result-object v13

    #@5d
    check-cast v13, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;

    #@5f
    invoke-static {v13}, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->access$1300(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;)Ljava/lang/Integer;

    #@62
    move-result-object v13

    #@63
    if-eq v14, v13, :cond_49

    #@65
    .line 428
    new-instance v13, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    const-string v14, "debugCheckData mRunningGeoFences KEY check failed..  mismatched mEngID - "

    #@6c
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v13

    #@70
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@73
    move-result-object v14

    #@74
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v13

    #@78
    const-string v14, " mRunningGeoFences "

    #@7a
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v13

    #@7e
    iget-object v14, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mRunningGeoFences:Ljava/util/HashMap;

    #@80
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v13

    #@84
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@87
    move-result-object v8

    #@88
    .line 431
    .restart local v8       #msg:Ljava/lang/String;
    new-instance v13, Ljava/lang/Error;

    #@8a
    invoke-direct {v13, v8}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    #@8d
    throw v13

    #@8e
    .line 434
    .end local v5           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;>;"
    .end local v8           #msg:Ljava/lang/String;
    :cond_8e
    const-string v13, "GeoFenceKeeper"

    #@90
    const-string v14, "debugCheckData: mAllGeoFences KEY checking..."

    #@92
    invoke-static {v13, v14}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@95
    .line 435
    iget-object v13, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mAllGeoFences:Ljava/util/HashMap;

    #@97
    invoke-virtual {v13}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@9a
    move-result-object v13

    #@9b
    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@9e
    move-result-object v7

    #@9f
    :cond_9f
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@a2
    move-result v13

    #@a3
    if-eqz v13, :cond_ec

    #@a5
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@a8
    move-result-object v4

    #@a9
    check-cast v4, Ljava/util/Map$Entry;

    #@ab
    .line 436
    .local v4, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Landroid/app/PendingIntent;Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;>;"
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@ae
    move-result-object v13

    #@af
    check-cast v13, Landroid/app/PendingIntent;

    #@b1
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@b4
    move-result-object v14

    #@b5
    check-cast v14, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;

    #@b7
    invoke-static {v14}, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->access$1000(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;)Landroid/location/GeoFenceParams;

    #@ba
    move-result-object v14

    #@bb
    iget-object v14, v14, Landroid/location/GeoFenceParams;->mIntent:Landroid/app/PendingIntent;

    #@bd
    invoke-virtual {v13, v14}, Landroid/app/PendingIntent;->equals(Ljava/lang/Object;)Z

    #@c0
    move-result v13

    #@c1
    if-nez v13, :cond_9f

    #@c3
    .line 437
    new-instance v13, Ljava/lang/StringBuilder;

    #@c5
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@c8
    const-string v14, "debugCheckData mAllGeoFences KEY check failed..  mismatched PendingIntent - "

    #@ca
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v13

    #@ce
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@d1
    move-result-object v14

    #@d2
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v13

    #@d6
    const-string v14, " mAllGeoFences - "

    #@d8
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@db
    move-result-object v13

    #@dc
    iget-object v14, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mAllGeoFences:Ljava/util/HashMap;

    #@de
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e1
    move-result-object v13

    #@e2
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e5
    move-result-object v8

    #@e6
    .line 440
    .restart local v8       #msg:Ljava/lang/String;
    new-instance v13, Ljava/lang/Error;

    #@e8
    invoke-direct {v13, v8}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    #@eb
    throw v13

    #@ec
    .line 444
    .end local v4           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Landroid/app/PendingIntent;Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;>;"
    .end local v8           #msg:Ljava/lang/String;
    :cond_ec
    const-string v13, "GeoFenceKeeper"

    #@ee
    const-string v14, "debugCheckData: cross reference checking..."

    #@f0
    invoke-static {v13, v14}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@f3
    .line 445
    const/4 v12, 0x0

    #@f4
    .line 446
    .local v12, tickingSize:I
    iget-object v13, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mAllGeoFences:Ljava/util/HashMap;

    #@f6
    invoke-virtual {v13}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@f9
    move-result-object v0

    #@fa
    .line 447
    .local v0, allFences:Ljava/util/Collection;
    iget-object v13, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mRunningGeoFences:Ljava/util/HashMap;

    #@fc
    invoke-virtual {v13}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@ff
    move-result-object v9

    #@100
    .line 448
    .local v9, runningFences:Ljava/util/Collection;
    const-wide/16 v13, 0x0

    #@102
    invoke-static {v13, v14}, Lcom/qualcomm/services/location/GeoFenceKeeper;->upTimeInTimeUnit(J)J

    #@105
    move-result-wide v2

    #@106
    .line 450
    .local v2, curStamp:J
    iget-object v13, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mTickingGeoFences:Ljava/util/HashMap;

    #@108
    invoke-virtual {v13}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@10b
    move-result-object v13

    #@10c
    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@10f
    move-result-object v7

    #@110
    :goto_110
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@113
    move-result v13

    #@114
    if-eqz v13, :cond_1e0

    #@116
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@119
    move-result-object v11

    #@11a
    check-cast v11, Ljava/util/Map$Entry;

    #@11c
    .line 452
    .local v11, tge:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/util/HashSet<Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;>;>;"
    invoke-interface {v11}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@11f
    move-result-object v13

    #@120
    check-cast v13, Ljava/lang/Long;

    #@122
    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    #@125
    move-result-wide v13

    #@126
    cmp-long v13, v2, v13

    #@128
    if-lez v13, :cond_15d

    #@12a
    .line 453
    new-instance v13, Ljava/lang/StringBuilder;

    #@12c
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@12f
    const-string v14, "debugCheckData timestamp check failed.. curStamp - "

    #@131
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@134
    move-result-object v13

    #@135
    invoke-virtual {v13, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@138
    move-result-object v13

    #@139
    const-string v14, " tge.ts "

    #@13b
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13e
    move-result-object v13

    #@13f
    invoke-interface {v11}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@142
    move-result-object v14

    #@143
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@146
    move-result-object v13

    #@147
    const-string v14, " mTickingGeoFences "

    #@149
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14c
    move-result-object v13

    #@14d
    iget-object v14, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mTickingGeoFences:Ljava/util/HashMap;

    #@14f
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@152
    move-result-object v13

    #@153
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@156
    move-result-object v8

    #@157
    .line 456
    .restart local v8       #msg:Ljava/lang/String;
    new-instance v13, Ljava/lang/Error;

    #@159
    invoke-direct {v13, v8}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    #@15c
    throw v13

    #@15d
    .line 458
    .end local v8           #msg:Ljava/lang/String;
    :cond_15d
    invoke-interface {v11}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@160
    move-result-object v6

    #@161
    check-cast v6, Ljava/util/Collection;

    #@163
    .line 459
    .local v6, fences:Ljava/util/Collection;
    invoke-interface {v0, v6}, Ljava/util/Collection;->containsAll(Ljava/util/Collection;)Z

    #@166
    move-result v13

    #@167
    if-nez v13, :cond_19e

    #@169
    .line 460
    new-instance v13, Ljava/lang/StringBuilder;

    #@16b
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@16e
    const-string v14, "debugCheckData cross check failed.. not in mAllGeoFences, tickgroup - "

    #@170
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@173
    move-result-object v13

    #@174
    invoke-interface {v11}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@177
    move-result-object v14

    #@178
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@17b
    move-result-object v13

    #@17c
    const-string v14, " mTickingGeoFences - "

    #@17e
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@181
    move-result-object v13

    #@182
    iget-object v14, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mTickingGeoFences:Ljava/util/HashMap;

    #@184
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@187
    move-result-object v13

    #@188
    const-string v14, "mAllGeoFences - "

    #@18a
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18d
    move-result-object v13

    #@18e
    iget-object v14, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mAllGeoFences:Ljava/util/HashMap;

    #@190
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@193
    move-result-object v13

    #@194
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@197
    move-result-object v8

    #@198
    .line 464
    .restart local v8       #msg:Ljava/lang/String;
    new-instance v13, Ljava/lang/Error;

    #@19a
    invoke-direct {v13, v8}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    #@19d
    throw v13

    #@19e
    .line 466
    .end local v8           #msg:Ljava/lang/String;
    :cond_19e
    invoke-interface {v9, v6}, Ljava/util/Collection;->containsAll(Ljava/util/Collection;)Z

    #@1a1
    move-result v13

    #@1a2
    if-nez v13, :cond_1d9

    #@1a4
    .line 467
    new-instance v13, Ljava/lang/StringBuilder;

    #@1a6
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@1a9
    const-string v14, "debugCheckData cross check failed.. not in mRunningGeoFences, tickgroup - "

    #@1ab
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ae
    move-result-object v13

    #@1af
    invoke-interface {v11}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@1b2
    move-result-object v14

    #@1b3
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1b6
    move-result-object v13

    #@1b7
    const-string v14, " mTickingGeoFences - "

    #@1b9
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bc
    move-result-object v13

    #@1bd
    iget-object v14, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mTickingGeoFences:Ljava/util/HashMap;

    #@1bf
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c2
    move-result-object v13

    #@1c3
    const-string v14, " mRunningGeoFences - "

    #@1c5
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c8
    move-result-object v13

    #@1c9
    iget-object v14, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mRunningGeoFences:Ljava/util/HashMap;

    #@1cb
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1ce
    move-result-object v13

    #@1cf
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d2
    move-result-object v8

    #@1d3
    .line 471
    .restart local v8       #msg:Ljava/lang/String;
    new-instance v13, Ljava/lang/Error;

    #@1d5
    invoke-direct {v13, v8}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    #@1d8
    throw v13

    #@1d9
    .line 473
    .end local v8           #msg:Ljava/lang/String;
    :cond_1d9
    invoke-interface {v6}, Ljava/util/Collection;->size()I

    #@1dc
    move-result v13

    #@1dd
    add-int/2addr v12, v13

    #@1de
    .line 474
    goto/16 :goto_110

    #@1e0
    .line 476
    .end local v6           #fences:Ljava/util/Collection;
    .end local v11           #tge:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/util/HashSet<Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;>;>;"
    :cond_1e0
    const-string v13, "GeoFenceKeeper"

    #@1e2
    const-string v14, "debugCheckData: tickingSize checking..."

    #@1e4
    invoke-static {v13, v14}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1e7
    .line 477
    if-ge v1, v12, :cond_20c

    #@1e9
    .line 478
    new-instance v13, Ljava/lang/StringBuilder;

    #@1eb
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@1ee
    const-string v14, "debugCheckData size check failed.. allSize - "

    #@1f0
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f3
    move-result-object v13

    #@1f4
    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f7
    move-result-object v13

    #@1f8
    const-string v14, " tickingSize - "

    #@1fa
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1fd
    move-result-object v13

    #@1fe
    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@201
    move-result-object v13

    #@202
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@205
    move-result-object v8

    #@206
    .line 480
    .restart local v8       #msg:Ljava/lang/String;
    new-instance v13, Ljava/lang/Error;

    #@208
    invoke-direct {v13, v8}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    #@20b
    throw v13

    #@20c
    .line 483
    .end local v8           #msg:Ljava/lang/String;
    :cond_20c
    const-string v13, "GeoFenceKeeper"

    #@20e
    const-string v14, "debugCheckData check passed!!!"

    #@210
    invoke-static {v13, v14}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@213
    .line 485
    return-void
.end method

.method private handleAddGeoFence(Landroid/os/Message;)V
    .registers 5
    .parameter "m"

    #@0
    .prologue
    .line 166
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2
    check-cast v0, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;

    #@4
    .line 169
    .local v0, fenceData:Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;
    iget-object v1, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mAllGeoFences:Ljava/util/HashMap;

    #@6
    invoke-static {v0}, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->access$1000(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;)Landroid/location/GeoFenceParams;

    #@9
    move-result-object v2

    #@a
    iget-object v2, v2, Landroid/location/GeoFenceParams;->mIntent:Landroid/app/PendingIntent;

    #@c
    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;

    #@12
    invoke-direct {p0, v1}, Lcom/qualcomm/services/location/GeoFenceKeeper;->removeGeoFence(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;)V

    #@15
    .line 170
    invoke-direct {p0, v0}, Lcom/qualcomm/services/location/GeoFenceKeeper;->handleCleanAddGeoFence(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;)V

    #@18
    .line 171
    return-void
.end method

.method private handleBreachEvent(II)V
    .registers 11
    .parameter "id"
    .parameter "direction"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 258
    iget-object v6, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mRunningGeoFences:Ljava/util/HashMap;

    #@4
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@7
    move-result-object v7

    #@8
    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@b
    move-result-object v2

    #@c
    check-cast v2, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;

    #@e
    .line 259
    .local v2, geofence:Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;
    if-eqz v2, :cond_2a

    #@10
    .line 260
    invoke-static {v2}, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->access$1000(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;)Landroid/location/GeoFenceParams;

    #@13
    move-result-object v6

    #@14
    iget-object v3, v6, Landroid/location/GeoFenceParams;->mIntent:Landroid/app/PendingIntent;

    #@16
    .line 261
    .local v3, intent:Landroid/app/PendingIntent;
    new-instance v1, Landroid/content/Intent;

    #@18
    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    #@1b
    .line 262
    .local v1, enteredIntent:Landroid/content/Intent;
    const-string v6, "entering"

    #@1d
    add-int/lit8 v7, p2, 0x1

    #@1f
    if-ne v7, v4, :cond_2b

    #@21
    :goto_21
    invoke-virtual {v1, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@24
    .line 265
    :try_start_24
    iget-object v4, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mContext:Landroid/content/Context;

    #@26
    const/4 v5, 0x0

    #@27
    invoke-virtual {v3, v4, v5, v1}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_2a
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_24 .. :try_end_2a} :catch_2d

    #@2a
    .line 271
    .end local v1           #enteredIntent:Landroid/content/Intent;
    .end local v3           #intent:Landroid/app/PendingIntent;
    :cond_2a
    :goto_2a
    return-void

    #@2b
    .restart local v1       #enteredIntent:Landroid/content/Intent;
    .restart local v3       #intent:Landroid/app/PendingIntent;
    :cond_2b
    move v4, v5

    #@2c
    .line 262
    goto :goto_21

    #@2d
    .line 266
    :catch_2d
    move-exception v0

    #@2e
    .line 268
    .local v0, e:Landroid/app/PendingIntent$CanceledException;
    invoke-virtual {p0, v3}, Lcom/qualcomm/services/location/GeoFenceKeeper;->removeGeoFence(Landroid/app/PendingIntent;)V

    #@31
    goto :goto_2a
.end method

.method private handleBreachEvent(Landroid/os/Message;)V
    .registers 4
    .parameter "m"

    #@0
    .prologue
    .line 255
    iget v0, p1, Landroid/os/Message;->arg1:I

    #@2
    iget v1, p1, Landroid/os/Message;->arg2:I

    #@4
    invoke-direct {p0, v0, v1}, Lcom/qualcomm/services/location/GeoFenceKeeper;->handleBreachEvent(II)V

    #@7
    .line 256
    return-void
.end method

.method private handleCleanAddGeoFence(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;)V
    .registers 15
    .parameter "fenceData"

    #@0
    .prologue
    .line 173
    invoke-static {p1}, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->access$1100(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;)Ljava/lang/Long;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    #@7
    move-result-wide v9

    #@8
    .line 174
    .local v9, lifeSpan:J
    const-string v0, "GeoFenceKeeper"

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "handleAddGeoFence lifeSpan - "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 175
    const-wide/16 v0, -0x1

    #@22
    cmp-long v0, v9, v0

    #@24
    if-eqz v0, :cond_55

    #@26
    .line 176
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mTickingGeoFences:Ljava/util/HashMap;

    #@28
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@2b
    move-result-object v1

    #@2c
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@2f
    move-result-object v12

    #@30
    check-cast v12, Ljava/util/HashSet;

    #@32
    .line 177
    .local v12, tickingGroup:Ljava/util/HashSet;,"Ljava/util/HashSet<Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;>;"
    if-nez v12, :cond_52

    #@34
    .line 179
    new-instance v12, Ljava/util/HashSet;

    #@36
    .end local v12           #tickingGroup:Ljava/util/HashSet;,"Ljava/util/HashSet<Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;>;"
    invoke-direct {v12}, Ljava/util/HashSet;-><init>()V

    #@39
    .line 180
    .restart local v12       #tickingGroup:Ljava/util/HashSet;,"Ljava/util/HashSet<Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;>;"
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mTickingGeoFences:Ljava/util/HashMap;

    #@3b
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@3e
    move-result-object v1

    #@3f
    invoke-virtual {v0, v1, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@42
    .line 182
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mHandler:Lcom/qualcomm/services/location/GeoFenceKeeper$MyHandler;

    #@44
    const/4 v1, 0x3

    #@45
    invoke-static {v0, v1, v12}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    #@48
    move-result-object v11

    #@49
    .line 184
    .local v11, message:Landroid/os/Message;
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mHandler:Lcom/qualcomm/services/location/GeoFenceKeeper$MyHandler;

    #@4b
    invoke-static {v9, v10}, Lcom/qualcomm/services/location/GeoFenceKeeper;->timeFromNowInMs(J)J

    #@4e
    move-result-wide v1

    #@4f
    invoke-virtual {v0, v11, v1, v2}, Lcom/qualcomm/services/location/GeoFenceKeeper$MyHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@52
    .line 187
    .end local v11           #message:Landroid/os/Message;
    :cond_52
    invoke-virtual {v12, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@55
    .line 190
    .end local v12           #tickingGroup:Ljava/util/HashSet;,"Ljava/util/HashSet<Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;>;"
    :cond_55
    invoke-static {p1}, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->access$1200(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;)Ljava/lang/Integer;

    #@58
    move-result-object v0

    #@59
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@5c
    move-result v1

    #@5d
    invoke-static {p1}, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->access$1000(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;)Landroid/location/GeoFenceParams;

    #@60
    move-result-object v0

    #@61
    iget-wide v2, v0, Landroid/location/GeoFenceParams;->mLatitude:D

    #@63
    invoke-static {p1}, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->access$1000(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;)Landroid/location/GeoFenceParams;

    #@66
    move-result-object v0

    #@67
    iget-wide v4, v0, Landroid/location/GeoFenceParams;->mLongitude:D

    #@69
    invoke-static {p1}, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->access$1000(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;)Landroid/location/GeoFenceParams;

    #@6c
    move-result-object v0

    #@6d
    iget v6, v0, Landroid/location/GeoFenceParams;->mRadius:F

    #@6f
    move-object v0, p0

    #@70
    invoke-direct/range {v0 .. v6}, Lcom/qualcomm/services/location/GeoFenceKeeper;->startGeoFence(IDDF)J

    #@73
    move-result-wide v7

    #@74
    .line 193
    .local v7, engID:J
    const-wide/16 v0, -0x1

    #@76
    cmp-long v0, v7, v0

    #@78
    if-nez v0, :cond_87

    #@7a
    .line 194
    const-string v0, "GeoFenceKeeper"

    #@7c
    const-string v1, "startGeoFence failed"

    #@7e
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@81
    .line 200
    :goto_81
    const-string v0, "AddGeoFenceHandler"

    #@83
    invoke-direct {p0, v0}, Lcom/qualcomm/services/location/GeoFenceKeeper;->logMe(Ljava/lang/String;)V

    #@86
    .line 201
    return-void

    #@87
    .line 196
    :cond_87
    long-to-int v0, v7

    #@88
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8b
    move-result-object v0

    #@8c
    invoke-static {p1, v0}, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->access$1302(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;Ljava/lang/Integer;)Ljava/lang/Integer;

    #@8f
    .line 197
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mRunningGeoFences:Ljava/util/HashMap;

    #@91
    invoke-static {p1}, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->access$1300(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;)Ljava/lang/Integer;

    #@94
    move-result-object v1

    #@95
    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@98
    goto :goto_81
.end method

.method private handleExpiredGeoFences(Landroid/os/Message;)V
    .registers 11
    .parameter "m"

    #@0
    .prologue
    .line 229
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2
    check-cast v5, Ljava/util/HashSet;

    #@4
    .line 231
    .local v5, tg:Ljava/util/HashSet;,"Ljava/util/HashSet<Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;>;"
    invoke-virtual {v5}, Ljava/util/HashSet;->isEmpty()Z

    #@7
    move-result v7

    #@8
    if-nez v7, :cond_67

    #@a
    .line 232
    invoke-virtual {v5}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@d
    move-result-object v7

    #@e
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@11
    move-result-object v7

    #@12
    check-cast v7, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;

    #@14
    invoke-static {v7}, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->access$1100(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;)Ljava/lang/Long;

    #@17
    move-result-object v7

    #@18
    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    #@1b
    move-result-wide v0

    #@1c
    .line 233
    .local v0, groupID:J
    iget-object v7, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mTickingGeoFences:Ljava/util/HashMap;

    #@1e
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@21
    move-result-object v8

    #@22
    invoke-virtual {v7, v8}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@25
    move-result-object v6

    #@26
    check-cast v6, Ljava/util/HashSet;

    #@28
    .line 235
    .local v6, tickingGroup:Ljava/util/HashSet;,"Ljava/util/HashSet<Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;>;"
    if-eqz v6, :cond_51

    #@2a
    .line 236
    move-object v4, v6

    #@2b
    .line 237
    .local v4, members:Ljava/util/HashSet;,"Ljava/util/HashSet<Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;>;"
    invoke-virtual {v4}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@2e
    move-result-object v2

    #@2f
    .local v2, i$:Ljava/util/Iterator;
    :goto_2f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@32
    move-result v7

    #@33
    if-eqz v7, :cond_4e

    #@35
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@38
    move-result-object v3

    #@39
    check-cast v3, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;

    #@3b
    .line 238
    .local v3, member:Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;
    iget-object v7, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mAllGeoFences:Ljava/util/HashMap;

    #@3d
    invoke-static {v3}, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->access$1000(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;)Landroid/location/GeoFenceParams;

    #@40
    move-result-object v8

    #@41
    iget-object v8, v8, Landroid/location/GeoFenceParams;->mIntent:Landroid/app/PendingIntent;

    #@43
    invoke-virtual {v7, v8}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@46
    move-result-object v7

    #@47
    check-cast v7, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;

    #@49
    const/4 v8, 0x1

    #@4a
    invoke-direct {p0, v7, v8}, Lcom/qualcomm/services/location/GeoFenceKeeper;->removeGeoFenceCommon(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;Z)Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;

    #@4d
    goto :goto_2f

    #@4e
    .line 241
    .end local v3           #member:Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;
    :cond_4e
    invoke-virtual {v4}, Ljava/util/HashSet;->clear()V

    #@51
    .line 243
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v4           #members:Ljava/util/HashSet;,"Ljava/util/HashSet<Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;>;"
    :cond_51
    new-instance v7, Ljava/lang/StringBuilder;

    #@53
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@56
    const-string v8, "TickingGroup groupID -"

    #@58
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v7

    #@5c
    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v7

    #@60
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v7

    #@64
    invoke-direct {p0, v7}, Lcom/qualcomm/services/location/GeoFenceKeeper;->logMe(Ljava/lang/String;)V

    #@67
    .line 245
    .end local v0           #groupID:J
    .end local v6           #tickingGroup:Ljava/util/HashSet;,"Ljava/util/HashSet<Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;>;"
    :cond_67
    return-void
.end method

.method private handleRemoveGeoFence(Landroid/os/Message;)V
    .registers 4
    .parameter "m"

    #@0
    .prologue
    .line 204
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2
    check-cast v0, Landroid/app/PendingIntent;

    #@4
    .line 205
    .local v0, intent:Landroid/app/PendingIntent;
    iget-object v1, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mAllGeoFences:Ljava/util/HashMap;

    #@6
    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v1

    #@a
    check-cast v1, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;

    #@c
    invoke-direct {p0, v1}, Lcom/qualcomm/services/location/GeoFenceKeeper;->removeGeoFence(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;)V

    #@f
    .line 206
    const-string v1, "RemoveGeoFenceHandler"

    #@11
    invoke-direct {p0, v1}, Lcom/qualcomm/services/location/GeoFenceKeeper;->logMe(Ljava/lang/String;)V

    #@14
    .line 207
    return-void
.end method

.method private handleRemoveGeoFenceApp(Landroid/os/Message;)V
    .registers 9
    .parameter "m"

    #@0
    .prologue
    .line 210
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@2
    .line 211
    .local v4, uid:I
    const/4 v3, 0x0

    #@3
    .line 212
    .local v3, removedFences:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    iget-object v5, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mAllGeoFences:Ljava/util/HashMap;

    #@5
    invoke-virtual {v5}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@8
    move-result-object v5

    #@9
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@c
    move-result-object v2

    #@d
    .local v2, i$:Ljava/util/Iterator;
    :cond_d
    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@10
    move-result v5

    #@11
    if-eqz v5, :cond_36

    #@13
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@16
    move-result-object v0

    #@17
    check-cast v0, Ljava/util/Map$Entry;

    #@19
    .line 213
    .local v0, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Landroid/app/PendingIntent;Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@1c
    move-result-object v5

    #@1d
    check-cast v5, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;

    #@1f
    invoke-static {v5}, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->access$1000(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;)Landroid/location/GeoFenceParams;

    #@22
    move-result-object v5

    #@23
    iget v5, v5, Landroid/location/GeoFenceParams;->mUid:I

    #@25
    if-ne v5, v4, :cond_d

    #@27
    .line 214
    if-nez v3, :cond_2e

    #@29
    .line 215
    new-instance v3, Ljava/util/ArrayList;

    #@2b
    .end local v3           #removedFences:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@2e
    .line 217
    .restart local v3       #removedFences:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    :cond_2e
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@31
    move-result-object v5

    #@32
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@35
    goto :goto_d

    #@36
    .line 220
    .end local v0           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Landroid/app/PendingIntent;Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;>;"
    :cond_36
    if-eqz v3, :cond_68

    #@38
    .line 221
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@3b
    move-result v5

    #@3c
    add-int/lit8 v1, v5, -0x1

    #@3e
    .local v1, i:I
    :goto_3e
    if-ltz v1, :cond_52

    #@40
    .line 222
    iget-object v5, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mAllGeoFences:Ljava/util/HashMap;

    #@42
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@45
    move-result-object v6

    #@46
    invoke-virtual {v5, v6}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@49
    move-result-object v5

    #@4a
    check-cast v5, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;

    #@4c
    invoke-direct {p0, v5}, Lcom/qualcomm/services/location/GeoFenceKeeper;->removeGeoFence(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;)V

    #@4f
    .line 221
    add-int/lit8 v1, v1, -0x1

    #@51
    goto :goto_3e

    #@52
    .line 224
    :cond_52
    new-instance v5, Ljava/lang/StringBuilder;

    #@54
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@57
    const-string v6, "RemoveGeoFenceUserHandler uid -"

    #@59
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v5

    #@5d
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@60
    move-result-object v5

    #@61
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v5

    #@65
    invoke-direct {p0, v5}, Lcom/qualcomm/services/location/GeoFenceKeeper;->logMe(Ljava/lang/String;)V

    #@68
    .line 226
    .end local v1           #i:I
    :cond_68
    return-void
.end method

.method private handleSSR(Landroid/os/Message;)V
    .registers 5
    .parameter "m"

    #@0
    .prologue
    .line 248
    invoke-direct {p0}, Lcom/qualcomm/services/location/GeoFenceKeeper;->nativeInit()V

    #@3
    .line 249
    iget-object v2, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mAllGeoFences:Ljava/util/HashMap;

    #@5
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@8
    move-result-object v2

    #@9
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@c
    move-result-object v1

    #@d
    .local v1, i$:Ljava/util/Iterator;
    :goto_d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@10
    move-result v2

    #@11
    if-eqz v2, :cond_1d

    #@13
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@16
    move-result-object v0

    #@17
    check-cast v0, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;

    #@19
    .line 250
    .local v0, gfd:Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;
    invoke-direct {p0, v0}, Lcom/qualcomm/services/location/GeoFenceKeeper;->handleCleanAddGeoFence(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;)V

    #@1c
    goto :goto_d

    #@1d
    .line 252
    .end local v0           #gfd:Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;
    :cond_1d
    return-void
.end method

.method private logMe(Ljava/lang/String;)V
    .registers 5
    .parameter "where"

    #@0
    .prologue
    .line 325
    const-string v0, "GeoFenceKeeper"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "logMe "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 326
    const-string v0, "GeoFenceKeeper"

    #@1a
    new-instance v1, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v2, "mAllGeoFences -"

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    iget-object v2, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mAllGeoFences:Ljava/util/HashMap;

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v1

    #@2f
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 327
    const-string v0, "GeoFenceKeeper"

    #@34
    new-instance v1, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v2, "mRunningGeoFences -"

    #@3b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v1

    #@3f
    iget-object v2, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mRunningGeoFences:Ljava/util/HashMap;

    #@41
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v1

    #@45
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v1

    #@49
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    .line 328
    const-string v0, "GeoFenceKeeper"

    #@4e
    new-instance v1, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    const-string v2, "mTickingGeoFences -"

    #@55
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v1

    #@59
    iget-object v2, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mTickingGeoFences:Ljava/util/HashMap;

    #@5b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v1

    #@5f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v1

    #@63
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@66
    .line 330
    return-void
.end method

.method private nativeDestroy()V
	.registers 1
	return-void
.end method

.method private nativeInit()V
	.registers 1
	return-void
.end method

.method private removeGeoFence(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;)V
    .registers 8
    .parameter "gfd"

    #@0
    .prologue
    .line 290
    const/4 v2, 0x0

    #@1
    invoke-direct {p0, p1, v2}, Lcom/qualcomm/services/location/GeoFenceKeeper;->removeGeoFenceCommon(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;Z)Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;

    #@4
    move-result-object v0

    #@5
    .line 291
    .local v0, fenceData:Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;
    if-eqz v0, :cond_3b

    #@7
    invoke-static {v0}, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->access$1100(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;)Ljava/lang/Long;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    #@e
    move-result-wide v2

    #@f
    const-wide/16 v4, -0x1

    #@11
    cmp-long v2, v2, v4

    #@13
    if-eqz v2, :cond_3b

    #@15
    .line 293
    iget-object v2, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mTickingGeoFences:Ljava/util/HashMap;

    #@17
    invoke-static {v0}, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->access$1100(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;)Ljava/lang/Long;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1e
    move-result-object v1

    #@1f
    check-cast v1, Ljava/util/HashSet;

    #@21
    .line 295
    .local v1, tickingGroup:Ljava/util/HashSet;,"Ljava/util/HashSet<Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;>;"
    if-eqz v1, :cond_3b

    #@23
    .line 296
    invoke-virtual {v1, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@26
    .line 297
    invoke-virtual {v1}, Ljava/util/HashSet;->isEmpty()Z

    #@29
    move-result v2

    #@2a
    if-eqz v2, :cond_3b

    #@2c
    .line 298
    iget-object v2, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mHandler:Lcom/qualcomm/services/location/GeoFenceKeeper$MyHandler;

    #@2e
    const/4 v3, 0x3

    #@2f
    invoke-virtual {v2, v3, v1}, Lcom/qualcomm/services/location/GeoFenceKeeper$MyHandler;->removeMessages(ILjava/lang/Object;)V

    #@32
    .line 299
    iget-object v2, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mTickingGeoFences:Ljava/util/HashMap;

    #@34
    invoke-static {v0}, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->access$1100(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;)Ljava/lang/Long;

    #@37
    move-result-object v3

    #@38
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@3b
    .line 303
    .end local v1           #tickingGroup:Ljava/util/HashSet;,"Ljava/util/HashSet<Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;>;"
    :cond_3b
    return-void
.end method

.method private removeGeoFenceCommon(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;Z)Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;
    .registers 5
    .parameter "fenceData"
    .parameter "expired"

    #@0
    .prologue
    .line 274
    if-eqz p1, :cond_1c

    #@2
    .line 275
    invoke-static {p1}, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->access$1300(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;)Ljava/lang/Integer;

    #@5
    move-result-object v0

    #@6
    if-nez v0, :cond_1d

    #@8
    .line 276
    const-string v0, "GeoFenceKeeper"

    #@a
    const-string v1, "mEngID null, invalid data?"

    #@c
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 284
    :goto_f
    invoke-static {p1}, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->access$1400(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;)Lcom/qualcomm/services/location/GeoFenceServlet;

    #@12
    move-result-object v0

    #@13
    invoke-static {p1}, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->access$1000(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;)Landroid/location/GeoFenceParams;

    #@16
    move-result-object v1

    #@17
    iget-object v1, v1, Landroid/location/GeoFenceParams;->mIntent:Landroid/app/PendingIntent;

    #@19
    invoke-virtual {v0, v1, p2}, Lcom/qualcomm/services/location/GeoFenceServlet;->onGeoFenceDone(Landroid/app/PendingIntent;Z)V

    #@1c
    .line 286
    :cond_1c
    return-object p1

    #@1d
    .line 278
    :cond_1d
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mRunningGeoFences:Ljava/util/HashMap;

    #@1f
    invoke-static {p1}, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->access$1300(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;)Ljava/lang/Integer;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@26
    .line 281
    invoke-static {p1}, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->access$1300(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;)Ljava/lang/Integer;

    #@29
    move-result-object v0

    #@2a
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@2d
    move-result v0

    #@2e
    invoke-direct {p0, v0}, Lcom/qualcomm/services/location/GeoFenceKeeper;->stopGenFence(I)V

    #@31
    goto :goto_f
.end method

.method private setTestEnv()V
    .registers 4

    #@0
    .prologue
    .line 390
    new-instance v1, Ljava/util/Random;

    #@2
    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    #@5
    iput-object v1, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mRandom:Ljava/util/Random;

    #@7
    .line 391
    new-instance v0, Landroid/content/IntentFilter;

    #@9
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@c
    .line 392
    .local v0, intentFilter:Landroid/content/IntentFilter;
    const-string v1, "com.qualcomm.services.location.geofence.breach"

    #@e
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@11
    .line 393
    const-string v1, "com.qualcomm.services.location.geofence.checkgeofences"

    #@13
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@16
    .line 394
    iget-object v1, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mContext:Landroid/content/Context;

    #@18
    iget-object v2, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mTestPosInjRcvr:Landroid/content/BroadcastReceiver;

    #@1a
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@1d
    .line 396
    return-void
.end method

.method private ssrestart()V
    .registers 4

    #@0
    .prologue
    .line 104
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mHandler:Lcom/qualcomm/services/location/GeoFenceKeeper$MyHandler;

    #@2
    iget-object v1, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mHandler:Lcom/qualcomm/services/location/GeoFenceKeeper$MyHandler;

    #@4
    const/4 v2, 0x5

    #@5
    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v0, v1}, Lcom/qualcomm/services/location/GeoFenceKeeper$MyHandler;->sendMessage(Landroid/os/Message;)Z

    #@c
    .line 105
    return-void
.end method

.method private startGeoFence(IDDF)J
	.registers 8
	const/4 v0, 0x0
	return v0
.end method

.method private stopGenFence(I)V
	.registers 2
	return-void
.end method

.method private testBreach(I)V
	.registers 2
	return-void
.end method

.method private static timeFromNowInMs(J)J
    .registers 6
    .parameter "upTimeInTimeUnit"

    #@0
    .prologue
    .line 343
    const/high16 v0, 0x1

    #@2
    shl-long v0, p0, v0

    #@4
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@7
    move-result-wide v2

    #@8
    sub-long/2addr v0, v2

    #@9
    return-wide v0
.end method

.method private unsetTestEnv()V
    .registers 3

    #@0
    .prologue
    .line 399
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mContext:Landroid/content/Context;

    #@2
    iget-object v1, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mTestPosInjRcvr:Landroid/content/BroadcastReceiver;

    #@4
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@7
    .line 401
    return-void
.end method

.method private static upTimeInTimeUnit(J)J
    .registers 5
    .parameter "timeFromNowInMs"

    #@0
    .prologue
    .line 336
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@3
    move-result-wide v0

    #@4
    add-long/2addr v0, p0

    #@5
    const/high16 v2, 0x1

    #@7
    shr-long/2addr v0, v2

    #@8
    return-wide v0
.end method


# virtual methods
.method public addGeoFence(Lcom/qualcomm/services/location/GeoFenceServlet;Landroid/location/GeoFenceParams;)Z
    .registers 10
    .parameter "client"
    .parameter "fenceParams"

    #@0
    .prologue
    .line 75
    const/4 v1, 0x0

    #@1
    .line 76
    .local v1, isSet:Z
    iget v3, p2, Landroid/location/GeoFenceParams;->mRadius:F

    #@3
    const/4 v4, 0x0

    #@4
    cmpl-float v3, v3, v4

    #@6
    if-ltz v3, :cond_2c

    #@8
    iget-wide v3, p2, Landroid/location/GeoFenceParams;->mExpiration:J

    #@a
    const-wide/16 v5, 0x0

    #@c
    cmp-long v3, v3, v5

    #@e
    if-gtz v3, :cond_18

    #@10
    iget-wide v3, p2, Landroid/location/GeoFenceParams;->mExpiration:J

    #@12
    const-wide/16 v5, -0x1

    #@14
    cmp-long v3, v3, v5

    #@16
    if-nez v3, :cond_2c

    #@18
    .line 79
    :cond_18
    new-instance v0, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;

    #@1a
    const/4 v3, 0x0

    #@1b
    invoke-direct {v0, p0, p1, p2, v3}, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;-><init>(Lcom/qualcomm/services/location/GeoFenceKeeper;Lcom/qualcomm/services/location/GeoFenceServlet;Landroid/location/GeoFenceParams;Lcom/qualcomm/services/location/GeoFenceKeeper$1;)V

    #@1e
    .line 80
    .local v0, fenceData:Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;
    iget-object v3, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mHandler:Lcom/qualcomm/services/location/GeoFenceKeeper$MyHandler;

    #@20
    const/4 v4, 0x0

    #@21
    invoke-static {v3, v4, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    #@24
    move-result-object v2

    #@25
    .line 81
    .local v2, message:Landroid/os/Message;
    iget-object v3, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mHandler:Lcom/qualcomm/services/location/GeoFenceKeeper$MyHandler;

    #@27
    invoke-virtual {v3, v2}, Lcom/qualcomm/services/location/GeoFenceKeeper$MyHandler;->sendMessage(Landroid/os/Message;)Z

    #@2a
    .line 82
    const/4 v1, 0x1

    #@2b
    .line 87
    .end local v0           #fenceData:Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;
    .end local v2           #message:Landroid/os/Message;
    :goto_2b
    return v1

    #@2c
    .line 84
    :cond_2c
    const-string v3, "GeoFenceKeeper"

    #@2e
    new-instance v4, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v5, "bad values: radius - "

    #@35
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v4

    #@39
    iget v5, p2, Landroid/location/GeoFenceParams;->mRadius:F

    #@3b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v4

    #@3f
    const-string v5, " fenceParams.mExpiration - "

    #@41
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v4

    #@45
    iget-wide v5, p2, Landroid/location/GeoFenceParams;->mExpiration:J

    #@47
    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v4

    #@4b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v4

    #@4f
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    goto :goto_2b
.end method

.method public debugCheckGeoFences()V
    .registers 3

    #@0
    .prologue
    .line 404
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mHandler:Lcom/qualcomm/services/location/GeoFenceKeeper$MyHandler;

    #@2
    new-instance v1, Lcom/qualcomm/services/location/GeoFenceKeeper$2;

    #@4
    invoke-direct {v1, p0}, Lcom/qualcomm/services/location/GeoFenceKeeper$2;-><init>(Lcom/qualcomm/services/location/GeoFenceKeeper;)V

    #@7
    invoke-virtual {v0, v1}, Lcom/qualcomm/services/location/GeoFenceKeeper$MyHandler;->post(Ljava/lang/Runnable;)Z

    #@a
    .line 410
    return-void
.end method

.method public removeGeoFence(Landroid/app/PendingIntent;)V
    .registers 5
    .parameter "intent"

    #@0
    .prologue
    .line 91
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mHandler:Lcom/qualcomm/services/location/GeoFenceKeeper$MyHandler;

    #@2
    iget-object v1, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mHandler:Lcom/qualcomm/services/location/GeoFenceKeeper$MyHandler;

    #@4
    const/4 v2, 0x1

    #@5
    invoke-static {v1, v2, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v0, v1}, Lcom/qualcomm/services/location/GeoFenceKeeper$MyHandler;->sendMessage(Landroid/os/Message;)Z

    #@c
    .line 92
    return-void
.end method

.method public removeGeoFenceApp(I)V
    .registers 6
    .parameter "uid"

    #@0
    .prologue
    .line 95
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mHandler:Lcom/qualcomm/services/location/GeoFenceKeeper$MyHandler;

    #@2
    iget-object v1, p0, Lcom/qualcomm/services/location/GeoFenceKeeper;->mHandler:Lcom/qualcomm/services/location/GeoFenceKeeper$MyHandler;

    #@4
    const/4 v2, 0x1

    #@5
    const/4 v3, 0x0

    #@6
    invoke-static {v1, v2, p1, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v0, v1}, Lcom/qualcomm/services/location/GeoFenceKeeper$MyHandler;->sendMessage(Landroid/os/Message;)Z

    #@d
    .line 96
    return-void
.end method
