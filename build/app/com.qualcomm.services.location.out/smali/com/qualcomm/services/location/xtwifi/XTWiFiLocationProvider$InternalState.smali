.class final enum Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;
.super Ljava/lang/Enum;
.source "XTWiFiLocationProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "InternalState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

.field public static final enum IS_DISABLED:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

.field public static final enum IS_IDLE:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

.field public static final enum IS_TRACKING_WAIT_FOR_FINAL_FIX:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

.field public static final enum IS_TRACKING_WAIT_FOR_TBF:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

.field public static final enum IS_TRACKING_WAIT_FOR_ZPP_FIX:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    #@0
    .prologue
    const/4 v6, 0x4

    #@1
    const/4 v5, 0x3

    #@2
    const/4 v4, 0x2

    #@3
    const/4 v3, 0x1

    #@4
    const/4 v2, 0x0

    #@5
    .line 130
    new-instance v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@7
    const-string v1, "IS_DISABLED"

    #@9
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;-><init>(Ljava/lang/String;I)V

    #@c
    sput-object v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;->IS_DISABLED:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@e
    new-instance v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@10
    const-string v1, "IS_IDLE"

    #@12
    invoke-direct {v0, v1, v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;-><init>(Ljava/lang/String;I)V

    #@15
    sput-object v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;->IS_IDLE:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@17
    new-instance v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@19
    const-string v1, "IS_TRACKING_WAIT_FOR_TBF"

    #@1b
    invoke-direct {v0, v1, v4}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;-><init>(Ljava/lang/String;I)V

    #@1e
    sput-object v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;->IS_TRACKING_WAIT_FOR_TBF:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@20
    new-instance v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@22
    const-string v1, "IS_TRACKING_WAIT_FOR_ZPP_FIX"

    #@24
    invoke-direct {v0, v1, v5}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;-><init>(Ljava/lang/String;I)V

    #@27
    sput-object v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;->IS_TRACKING_WAIT_FOR_ZPP_FIX:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@29
    new-instance v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@2b
    const-string v1, "IS_TRACKING_WAIT_FOR_FINAL_FIX"

    #@2d
    invoke-direct {v0, v1, v6}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;-><init>(Ljava/lang/String;I)V

    #@30
    sput-object v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;->IS_TRACKING_WAIT_FOR_FINAL_FIX:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@32
    .line 128
    const/4 v0, 0x5

    #@33
    new-array v0, v0, [Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@35
    sget-object v1, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;->IS_DISABLED:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@37
    aput-object v1, v0, v2

    #@39
    sget-object v1, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;->IS_IDLE:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@3b
    aput-object v1, v0, v3

    #@3d
    sget-object v1, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;->IS_TRACKING_WAIT_FOR_TBF:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@3f
    aput-object v1, v0, v4

    #@41
    sget-object v1, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;->IS_TRACKING_WAIT_FOR_ZPP_FIX:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@43
    aput-object v1, v0, v5

    #@45
    sget-object v1, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;->IS_TRACKING_WAIT_FOR_FINAL_FIX:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@47
    aput-object v1, v0, v6

    #@49
    sput-object v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;->$VALUES:[Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@4b
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 128
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 128
    const-class v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;
    .registers 1

    #@0
    .prologue
    .line 128
    sget-object v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;->$VALUES:[Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@2
    invoke-virtual {v0}, [Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@8
    return-object v0
.end method
