.class Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$2;
.super Ljava/lang/Object;
.source "XTWiFiOsAgent.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;


# direct methods
.method constructor <init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1527
    iput-object p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 4

    #@0
    .prologue
    .line 1530
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@2
    invoke-static {v0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$1900(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Landroid/location/LocationManager;

    #@5
    move-result-object v0

    #@6
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@8
    iget-object v1, v1, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mTimedListener:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$TimedLocationListener;

    #@a
    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    #@d
    .line 1531
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@f
    const/4 v1, 0x0

    #@10
    invoke-static {v0, v1}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$2102(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;Z)Z

    #@13
    .line 1532
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$2;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@15
    invoke-static {v0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$700(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Lcom/qualcomm/lib/location/log/LocLog;

    #@18
    move-result-object v0

    #@19
    const-string v1, "XTWiFiOS"

    #@1b
    const-string v2, "GNSS fix request timed out, stop."

    #@1d
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@20
    .line 1533
    return-void
.end method
