.class final Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$RilListener;
.super Landroid/telephony/PhoneStateListener;
.source "XTWiFiOsAgent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "RilListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;


# direct methods
.method private constructor <init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 158
    iput-object p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$RilListener;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@2
    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 158
    invoke-direct {p0, p1}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$RilListener;-><init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)V

    #@3
    return-void
.end method


# virtual methods
.method public onCellLocationChanged(Landroid/telephony/CellLocation;)V
    .registers 8
    .parameter "location"

    #@0
    .prologue
    .line 165
    :try_start_0
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$RilListener;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@2
    invoke-static {v1}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$700(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Lcom/qualcomm/lib/location/log/LocLog;

    #@5
    move-result-object v1

    #@6
    const-string v2, "XTWiFiOS"

    #@8
    new-instance v3, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v4, "Dump Cell Location:"

    #@f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v1, v2, v3}, Lcom/qualcomm/lib/location/log/LocLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_22
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_22} :catch_38

    #@22
    .line 172
    :goto_22
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$RilListener;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@24
    invoke-static {v1}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$800(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Ljava/lang/Object;

    #@27
    move-result-object v2

    #@28
    monitor-enter v2

    #@29
    .line 174
    :try_start_29
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$RilListener;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@2b
    invoke-static {v1}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$900(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)V

    #@2e
    .line 175
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$RilListener;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@30
    const/4 v3, 0x0

    #@31
    const/4 v4, 0x0

    #@32
    const/4 v5, 0x1

    #@33
    invoke-static {v1, v3, v4, p1, v5}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$1000(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;Ljava/lang/String;ILandroid/telephony/CellLocation;Z)V

    #@36
    .line 176
    monitor-exit v2
    :try_end_37
    .catchall {:try_start_29 .. :try_end_37} :catchall_47

    #@37
    .line 177
    return-void

    #@38
    .line 167
    :catch_38
    move-exception v0

    #@39
    .line 169
    .local v0, e:Ljava/lang/NullPointerException;
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$RilListener;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@3b
    invoke-static {v1}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$700(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Lcom/qualcomm/lib/location/log/LocLog;

    #@3e
    move-result-object v1

    #@3f
    const-string v2, "XTWiFiOS"

    #@41
    const-string v3, "Null pointer for CellLocation"

    #@43
    invoke-virtual {v1, v2, v3}, Lcom/qualcomm/lib/location/log/LocLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@46
    goto :goto_22

    #@47
    .line 176
    .end local v0           #e:Ljava/lang/NullPointerException;
    :catchall_47
    move-exception v1

    #@48
    :try_start_48
    monitor-exit v2
    :try_end_49
    .catchall {:try_start_48 .. :try_end_49} :catchall_47

    #@49
    throw v1
.end method

.method public onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .registers 8
    .parameter "serviceState"

    #@0
    .prologue
    .line 182
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$RilListener;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@2
    invoke-static {v0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$700(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Lcom/qualcomm/lib/location/log/LocLog;

    #@5
    move-result-object v0

    #@6
    const-string v1, "XTWiFiOS"

    #@8
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v3, "CurrentServiceState = "

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$RilListener;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@15
    invoke-static {v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$1100(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)I

    #@18
    move-result v3

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    const-string v3, " NewServiceState = "

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getState()I

    #@26
    move-result v3

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@32
    .line 184
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$RilListener;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@34
    invoke-static {v0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$1100(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)I

    #@37
    move-result v0

    #@38
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getState()I

    #@3b
    move-result v1

    #@3c
    if-eq v0, v1, :cond_66

    #@3e
    .line 189
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$RilListener;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@40
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getState()I

    #@43
    move-result v1

    #@44
    invoke-static {v0, v1}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$1102(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;I)I

    #@47
    .line 191
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$RilListener;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@49
    invoke-static {v0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$800(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Ljava/lang/Object;

    #@4c
    move-result-object v1

    #@4d
    monitor-enter v1

    #@4e
    .line 193
    :try_start_4e
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$RilListener;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@50
    const/4 v2, 0x0

    #@51
    const/4 v3, 0x0

    #@52
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$RilListener;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@54
    invoke-static {v4}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$1200(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Landroid/telephony/TelephonyManager;

    #@57
    move-result-object v4

    #@58
    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getCellLocation()Landroid/telephony/CellLocation;

    #@5b
    move-result-object v4

    #@5c
    const/4 v5, 0x1

    #@5d
    invoke-static {v0, v2, v3, v4, v5}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$1000(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;Ljava/lang/String;ILandroid/telephony/CellLocation;Z)V

    #@60
    .line 194
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$RilListener;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@62
    invoke-static {v0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$900(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)V

    #@65
    .line 195
    monitor-exit v1

    #@66
    .line 197
    :cond_66
    return-void

    #@67
    .line 195
    :catchall_67
    move-exception v0

    #@68
    monitor-exit v1
    :try_end_69
    .catchall {:try_start_4e .. :try_end_69} :catchall_67

    #@69
    throw v0
.end method
