.class public Lcom/qualcomm/services/location/xtwifi/NlpPublicStatus;
.super Ljava/lang/Object;
.source "NlpPublicStatus.java"


# instance fields
.field private m_publicStatus:I

.field private m_publicStatusUpdateTime:J


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 29
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 30
    const/4 v0, 0x1

    #@4
    invoke-virtual {p0, v0}, Lcom/qualcomm/services/location/xtwifi/NlpPublicStatus;->setStatus(I)V

    #@7
    .line 31
    return-void
.end method


# virtual methods
.method public declared-synchronized getStatus()I
    .registers 2

    #@0
    .prologue
    .line 41
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Lcom/qualcomm/services/location/xtwifi/NlpPublicStatus;->m_publicStatus:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getStatusUpdateTime()J
    .registers 3

    #@0
    .prologue
    .line 46
    monitor-enter p0

    #@1
    :try_start_1
    iget-wide v0, p0, Lcom/qualcomm/services/location/xtwifi/NlpPublicStatus;->m_publicStatusUpdateTime:J
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return-wide v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized setStatus(I)V
    .registers 4
    .parameter "status"

    #@0
    .prologue
    .line 35
    monitor-enter p0

    #@1
    :try_start_1
    iput p1, p0, Lcom/qualcomm/services/location/xtwifi/NlpPublicStatus;->m_publicStatus:I

    #@3
    .line 36
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@6
    move-result-wide v0

    #@7
    iput-wide v0, p0, Lcom/qualcomm/services/location/xtwifi/NlpPublicStatus;->m_publicStatusUpdateTime:J
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_b

    #@9
    .line 37
    monitor-exit p0

    #@a
    return-void

    #@b
    .line 35
    :catchall_b
    move-exception v0

    #@c
    monitor-exit p0

    #@d
    throw v0
.end method
