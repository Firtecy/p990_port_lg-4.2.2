.class public Lcom/qualcomm/services/location/LocationAlarmService;
.super Landroid/app/Service;
.source "LocationAlarmService.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field public static final LOG_TAG:Ljava/lang/String; = "LAlarm"


# instance fields
.field private mAlarmList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mAlarmManager:Landroid/app/AlarmManager;

.field private mAlarmReceiver:Lcom/qualcomm/services/location/LocationAlarmReceiver;

.field private mIntent:Landroid/content/Intent;

.field private mIntentFilter:Landroid/content/IntentFilter;

.field private mPendingIntent:Landroid/app/PendingIntent;

.field private mReg:Z

.field private message:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 38
    const-string v0, "alarmservice_jni"

    #@2
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@5
    .line 39
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 24
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    #@3
    return-void
.end method

.method private native native_deregister()Z
.end method

.method private native native_register()Z
.end method

.method private native native_wait_for_alarm()I
.end method


# virtual methods
.method public addAlarm(I)V
    .registers 15
    .parameter "duration"

    #@0
    .prologue
    .line 67
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@3
    move-result-wide v6

    #@4
    .line 68
    .local v6, startTime:J
    int-to-long v8, p1

    #@5
    add-long v0, v6, v8

    #@7
    .line 69
    .local v0, alarmTime:J
    const/4 v3, 0x0

    #@8
    .line 71
    .local v3, i:I
    new-instance v8, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v9, "Alarm Added at "

    #@f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v8

    #@13
    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@16
    move-result-object v9

    #@17
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v8

    #@1b
    const-string v9, " for "

    #@1d
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v8

    #@21
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@24
    move-result-object v9

    #@25
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v8

    #@29
    const-string v9, " ms."

    #@2b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v8

    #@2f
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v5

    #@33
    .line 73
    .local v5, logMsg:Ljava/lang/String;
    const-string v8, "LAlarm"

    #@35
    invoke-static {v8, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 74
    monitor-enter p0

    #@39
    .line 75
    :try_start_39
    iget-object v8, p0, Lcom/qualcomm/services/location/LocationAlarmService;->mAlarmList:Ljava/util/LinkedList;

    #@3b
    invoke-virtual {v8}, Ljava/util/LinkedList;->listIterator()Ljava/util/ListIterator;

    #@3e
    move-result-object v4

    #@3f
    .line 76
    .local v4, itr:Ljava/util/ListIterator;,"Ljava/util/ListIterator<Ljava/lang/Long;>;"
    const/4 v2, 0x0

    #@40
    .line 77
    .local v2, e:Ljava/lang/Long;
    :goto_40
    invoke-interface {v4}, Ljava/util/ListIterator;->hasNext()Z

    #@43
    move-result v8

    #@44
    if-eqz v8, :cond_5e

    #@46
    .line 78
    invoke-interface {v4}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    #@49
    move-result-object v2

    #@4a
    .end local v2           #e:Ljava/lang/Long;
    check-cast v2, Ljava/lang/Long;

    #@4c
    .line 79
    .restart local v2       #e:Ljava/lang/Long;
    add-int/lit8 v3, v3, 0x1

    #@4e
    .line 81
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    #@51
    move-result-wide v8

    #@52
    cmp-long v8, v8, v0

    #@54
    if-lez v8, :cond_b3

    #@56
    .line 82
    invoke-interface {v4}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    #@59
    move-result-object v2

    #@5a
    .end local v2           #e:Ljava/lang/Long;
    check-cast v2, Ljava/lang/Long;

    #@5c
    .line 83
    .restart local v2       #e:Ljava/lang/Long;
    add-int/lit8 v3, v3, -0x1

    #@5e
    .line 88
    :cond_5e
    if-eqz v2, :cond_6b

    #@60
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    #@63
    move-result-wide v8

    #@64
    sub-long/2addr v8, v0

    #@65
    const-wide/16 v10, 0x14

    #@67
    cmp-long v8, v8, v10

    #@69
    if-lez v8, :cond_78

    #@6b
    .line 89
    :cond_6b
    new-instance v8, Ljava/lang/Long;

    #@6d
    invoke-direct {v8, v0, v1}, Ljava/lang/Long;-><init>(J)V

    #@70
    invoke-interface {v4, v8}, Ljava/util/ListIterator;->add(Ljava/lang/Object;)V

    #@73
    .line 91
    if-nez v3, :cond_78

    #@75
    .line 92
    invoke-virtual {p0, v0, v1}, Lcom/qualcomm/services/location/LocationAlarmService;->setAlarm(J)V

    #@78
    .line 95
    :cond_78
    monitor-exit p0
    :try_end_79
    .catchall {:try_start_39 .. :try_end_79} :catchall_b5

    #@79
    .line 96
    const-string v9, "LAlarm"

    #@7b
    new-instance v8, Ljava/lang/StringBuilder;

    #@7d
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@80
    const-string v10, "Number of Alarms: "

    #@82
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v8

    #@86
    iget-object v10, p0, Lcom/qualcomm/services/location/LocationAlarmService;->mAlarmList:Ljava/util/LinkedList;

    #@88
    invoke-virtual {v10}, Ljava/util/LinkedList;->size()I

    #@8b
    move-result v10

    #@8c
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v8

    #@90
    const-string v10, ". Shortest: "

    #@92
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v10

    #@96
    iget-object v8, p0, Lcom/qualcomm/services/location/LocationAlarmService;->mAlarmList:Ljava/util/LinkedList;

    #@98
    invoke-virtual {v8}, Ljava/util/LinkedList;->peek()Ljava/lang/Object;

    #@9b
    move-result-object v8

    #@9c
    check-cast v8, Ljava/lang/Long;

    #@9e
    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    #@a1
    move-result-wide v11

    #@a2
    sub-long/2addr v11, v6

    #@a3
    invoke-static {v11, v12}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@a6
    move-result-object v8

    #@a7
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v8

    #@ab
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ae
    move-result-object v8

    #@af
    invoke-static {v9, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b2
    .line 98
    return-void

    #@b3
    .line 86
    :cond_b3
    const/4 v2, 0x0

    #@b4
    goto :goto_40

    #@b5
    .line 95
    .end local v2           #e:Ljava/lang/Long;
    .end local v4           #itr:Ljava/util/ListIterator;,"Ljava/util/ListIterator<Ljava/lang/Long;>;"
    :catchall_b5
    move-exception v8

    #@b6
    :try_start_b6
    monitor-exit p0
    :try_end_b7
    .catchall {:try_start_b6 .. :try_end_b7} :catchall_b5

    #@b7
    throw v8
.end method

.method public deregister()V
    .registers 2

    #@0
    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/qualcomm/services/location/LocationAlarmService;->mReg:Z

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 62
    invoke-direct {p0}, Lcom/qualcomm/services/location/LocationAlarmService;->native_deregister()Z

    #@7
    .line 64
    :cond_7
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 4
    .parameter "intent"

    #@0
    .prologue
    .line 177
    const-string v0, "LAlarm"

    #@2
    const-string v1, "Service bind."

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 178
    const/4 v0, 0x0

    #@8
    return-object v0
.end method

.method public onCreate()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 144
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    #@4
    .line 145
    const-string v0, "LAlarm"

    #@6
    const-string v1, "Service created."

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 146
    const-string v0, "alarm"

    #@d
    invoke-virtual {p0, v0}, Lcom/qualcomm/services/location/LocationAlarmService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@10
    move-result-object v0

    #@11
    check-cast v0, Landroid/app/AlarmManager;

    #@13
    iput-object v0, p0, Lcom/qualcomm/services/location/LocationAlarmService;->mAlarmManager:Landroid/app/AlarmManager;

    #@15
    .line 147
    new-instance v0, Lcom/qualcomm/services/location/LocationAlarmReceiver;

    #@17
    invoke-direct {v0}, Lcom/qualcomm/services/location/LocationAlarmReceiver;-><init>()V

    #@1a
    iput-object v0, p0, Lcom/qualcomm/services/location/LocationAlarmService;->mAlarmReceiver:Lcom/qualcomm/services/location/LocationAlarmReceiver;

    #@1c
    .line 148
    new-instance v0, Landroid/content/IntentFilter;

    #@1e
    sget-object v1, Lcom/qualcomm/services/location/LocationAlarmReceiver;->ACTION:Ljava/lang/String;

    #@20
    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@23
    iput-object v0, p0, Lcom/qualcomm/services/location/LocationAlarmService;->mIntentFilter:Landroid/content/IntentFilter;

    #@25
    .line 149
    iget-object v0, p0, Lcom/qualcomm/services/location/LocationAlarmService;->mAlarmReceiver:Lcom/qualcomm/services/location/LocationAlarmReceiver;

    #@27
    iget-object v1, p0, Lcom/qualcomm/services/location/LocationAlarmService;->mIntentFilter:Landroid/content/IntentFilter;

    #@29
    invoke-virtual {p0, v0, v1}, Lcom/qualcomm/services/location/LocationAlarmService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@2c
    .line 150
    new-instance v0, Landroid/content/Intent;

    #@2e
    sget-object v1, Lcom/qualcomm/services/location/LocationAlarmReceiver;->ACTION:Ljava/lang/String;

    #@30
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@33
    iput-object v0, p0, Lcom/qualcomm/services/location/LocationAlarmService;->mIntent:Landroid/content/Intent;

    #@35
    .line 151
    iget-object v0, p0, Lcom/qualcomm/services/location/LocationAlarmService;->mIntent:Landroid/content/Intent;

    #@37
    invoke-static {p0, v2, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@3a
    move-result-object v0

    #@3b
    iput-object v0, p0, Lcom/qualcomm/services/location/LocationAlarmService;->mPendingIntent:Landroid/app/PendingIntent;

    #@3d
    .line 153
    new-instance v0, Ljava/util/LinkedList;

    #@3f
    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    #@42
    iput-object v0, p0, Lcom/qualcomm/services/location/LocationAlarmService;->mAlarmList:Ljava/util/LinkedList;

    #@44
    .line 155
    new-instance v0, Ljava/lang/Thread;

    #@46
    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    #@49
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    #@4c
    .line 156
    return-void
.end method

.method public onDestroy()V
    .registers 3

    #@0
    .prologue
    .line 169
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    #@3
    .line 170
    iget-object v0, p0, Lcom/qualcomm/services/location/LocationAlarmService;->mAlarmReceiver:Lcom/qualcomm/services/location/LocationAlarmReceiver;

    #@5
    invoke-virtual {p0, v0}, Lcom/qualcomm/services/location/LocationAlarmService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@8
    .line 171
    invoke-virtual {p0}, Lcom/qualcomm/services/location/LocationAlarmService;->deregister()V

    #@b
    .line 172
    const-string v0, "LAlarm"

    #@d
    const-string v1, "Service destroyed."

    #@f
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 173
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .registers 7
    .parameter "intent"
    .parameter "flags"
    .parameter "startId"

    #@0
    .prologue
    .line 161
    const-string v0, "LAlarm"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Service started flags "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, " startId "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 165
    const/4 v0, 0x1

    #@23
    return v0
.end method

.method public register()Z
    .registers 5

    #@0
    .prologue
    .line 43
    const/4 v1, 0x0

    #@1
    .local v1, i:I
    :goto_1
    const/16 v2, 0x14

    #@3
    if-ge v1, v2, :cond_10

    #@5
    .line 45
    invoke-direct {p0}, Lcom/qualcomm/services/location/LocationAlarmService;->native_register()Z

    #@8
    move-result v2

    #@9
    iput-boolean v2, p0, Lcom/qualcomm/services/location/LocationAlarmService;->mReg:Z

    #@b
    .line 46
    iget-boolean v2, p0, Lcom/qualcomm/services/location/LocationAlarmService;->mReg:Z

    #@d
    const/4 v3, 0x1

    #@e
    if-ne v2, v3, :cond_13

    #@10
    .line 56
    :cond_10
    iget-boolean v2, p0, Lcom/qualcomm/services/location/LocationAlarmService;->mReg:Z

    #@12
    return v2

    #@13
    .line 50
    :cond_13
    const-wide/16 v2, 0x3e8

    #@15
    :try_start_15
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_18
    .catch Ljava/lang/InterruptedException; {:try_start_15 .. :try_end_18} :catch_1b

    #@18
    .line 43
    :goto_18
    add-int/lit8 v1, v1, 0x1

    #@1a
    goto :goto_1

    #@1b
    .line 51
    :catch_1b
    move-exception v0

    #@1c
    .line 52
    .local v0, e:Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    #@1f
    goto :goto_18
.end method

.method public run()V
    .registers 4

    #@0
    .prologue
    .line 129
    const-string v1, "LAlarm"

    #@2
    const-string v2, "Thread created."

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 130
    invoke-virtual {p0}, Lcom/qualcomm/services/location/LocationAlarmService;->register()Z

    #@a
    move-result v1

    #@b
    const/4 v2, 0x1

    #@c
    if-ne v1, v2, :cond_18

    #@e
    .line 134
    :cond_e
    :goto_e
    invoke-direct {p0}, Lcom/qualcomm/services/location/LocationAlarmService;->native_wait_for_alarm()I

    #@11
    move-result v0

    #@12
    .line 135
    .local v0, duration:I
    if-lez v0, :cond_e

    #@14
    .line 136
    invoke-virtual {p0, v0}, Lcom/qualcomm/services/location/LocationAlarmService;->addAlarm(I)V

    #@17
    goto :goto_e

    #@18
    .line 140
    .end local v0           #duration:I
    :cond_18
    return-void
.end method

.method public setAlarm(J)V
    .registers 9
    .parameter "alarmAt"

    #@0
    .prologue
    .line 100
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@3
    move-result-wide v0

    #@4
    .line 102
    .local v0, currentTime:J
    new-instance v3, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v4, "Alarm Set at "

    #@b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v3

    #@f
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@12
    move-result-object v4

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    const-string v4, " for "

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    sub-long v4, p1, v0

    #@1f
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@22
    move-result-object v4

    #@23
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    const-string v4, " ms."

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v2

    #@31
    .line 104
    .local v2, logMsg:Ljava/lang/String;
    const-string v3, "LAlarm"

    #@33
    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 105
    iget-object v3, p0, Lcom/qualcomm/services/location/LocationAlarmService;->mAlarmManager:Landroid/app/AlarmManager;

    #@38
    const/4 v4, 0x0

    #@39
    iget-object v5, p0, Lcom/qualcomm/services/location/LocationAlarmService;->mPendingIntent:Landroid/app/PendingIntent;

    #@3b
    invoke-virtual {v3, v4, p1, p2, v5}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    #@3e
    .line 106
    return-void
.end method

.method public setNextAlarm()V
    .registers 7

    #@0
    .prologue
    .line 109
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@3
    move-result-wide v0

    #@4
    .line 110
    .local v0, currentTime:J
    monitor-enter p0

    #@5
    .line 111
    :try_start_5
    iget-object v4, p0, Lcom/qualcomm/services/location/LocationAlarmService;->mAlarmList:Ljava/util/LinkedList;

    #@7
    invoke-virtual {v4}, Ljava/util/LinkedList;->listIterator()Ljava/util/ListIterator;

    #@a
    move-result-object v3

    #@b
    .line 112
    .local v3, itr:Ljava/util/ListIterator;,"Ljava/util/ListIterator<Ljava/lang/Long;>;"
    :goto_b
    invoke-interface {v3}, Ljava/util/ListIterator;->hasNext()Z

    #@e
    move-result v4

    #@f
    if-eqz v4, :cond_26

    #@11
    .line 113
    invoke-interface {v3}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v2

    #@15
    check-cast v2, Ljava/lang/Long;

    #@17
    .line 115
    .local v2, e:Ljava/lang/Long;
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    #@1a
    move-result-wide v4

    #@1b
    cmp-long v4, v4, v0

    #@1d
    if-gtz v4, :cond_26

    #@1f
    .line 116
    invoke-interface {v3}, Ljava/util/ListIterator;->remove()V

    #@22
    goto :goto_b

    #@23
    .line 125
    .end local v2           #e:Ljava/lang/Long;
    .end local v3           #itr:Ljava/util/ListIterator;,"Ljava/util/ListIterator<Ljava/lang/Long;>;"
    :catchall_23
    move-exception v4

    #@24
    monitor-exit p0
    :try_end_25
    .catchall {:try_start_5 .. :try_end_25} :catchall_23

    #@25
    throw v4

    #@26
    .line 122
    .restart local v3       #itr:Ljava/util/ListIterator;,"Ljava/util/ListIterator<Ljava/lang/Long;>;"
    :cond_26
    :try_start_26
    iget-object v4, p0, Lcom/qualcomm/services/location/LocationAlarmService;->mAlarmList:Ljava/util/LinkedList;

    #@28
    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    #@2b
    move-result v4

    #@2c
    if-nez v4, :cond_3d

    #@2e
    .line 123
    iget-object v4, p0, Lcom/qualcomm/services/location/LocationAlarmService;->mAlarmList:Ljava/util/LinkedList;

    #@30
    invoke-virtual {v4}, Ljava/util/LinkedList;->peek()Ljava/lang/Object;

    #@33
    move-result-object v4

    #@34
    check-cast v4, Ljava/lang/Long;

    #@36
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    #@39
    move-result-wide v4

    #@3a
    invoke-virtual {p0, v4, v5}, Lcom/qualcomm/services/location/LocationAlarmService;->setAlarm(J)V

    #@3d
    .line 125
    :cond_3d
    monitor-exit p0
    :try_end_3e
    .catchall {:try_start_26 .. :try_end_3e} :catchall_23

    #@3e
    .line 126
    return-void
.end method
