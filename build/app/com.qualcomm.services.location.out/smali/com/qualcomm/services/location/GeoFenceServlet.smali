.class public Lcom/qualcomm/services/location/GeoFenceServlet;
.super Ljava/lang/Object;
.source "GeoFenceServlet.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# static fields
.field private static final TAG:Ljava/lang/String; = "GeoClientRemoteHandle"

.field private static final mAllClients:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/location/IGeoFenceListener;",
            "Lcom/qualcomm/services/location/GeoFenceServlet;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mClient:Landroid/location/IGeoFenceListener;

.field private final mGeoFenceIDs:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Landroid/app/PendingIntent;",
            ">;"
        }
    .end annotation
.end field

.field private final mGeoFenceKeeper:Lcom/qualcomm/services/location/GeoFenceKeeper;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 29
    new-instance v0, Ljava/util/HashMap;

    #@2
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@5
    sput-object v0, Lcom/qualcomm/services/location/GeoFenceServlet;->mAllClients:Ljava/util/HashMap;

    #@7
    return-void
.end method

.method private constructor <init>(Landroid/os/IBinder;Lcom/qualcomm/services/location/GeoFenceKeeper;)V
    .registers 6
    .parameter "client"
    .parameter "keeper"

    #@0
    .prologue
    .line 45
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 46
    invoke-static {p1}, Landroid/location/IGeoFenceListener$Stub;->asInterface(Landroid/os/IBinder;)Landroid/location/IGeoFenceListener;

    #@6
    move-result-object v1

    #@7
    iput-object v1, p0, Lcom/qualcomm/services/location/GeoFenceServlet;->mClient:Landroid/location/IGeoFenceListener;

    #@9
    .line 47
    new-instance v1, Ljava/util/HashSet;

    #@b
    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    #@e
    iput-object v1, p0, Lcom/qualcomm/services/location/GeoFenceServlet;->mGeoFenceIDs:Ljava/util/HashSet;

    #@10
    .line 48
    iput-object p2, p0, Lcom/qualcomm/services/location/GeoFenceServlet;->mGeoFenceKeeper:Lcom/qualcomm/services/location/GeoFenceKeeper;

    #@12
    .line 50
    :try_start_12
    iget-object v1, p0, Lcom/qualcomm/services/location/GeoFenceServlet;->mClient:Landroid/location/IGeoFenceListener;

    #@14
    invoke-interface {v1}, Landroid/location/IGeoFenceListener;->asBinder()Landroid/os/IBinder;

    #@17
    move-result-object v1

    #@18
    const/4 v2, 0x0

    #@19
    invoke-interface {v1, p0, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_1c
    .catch Landroid/os/RemoteException; {:try_start_12 .. :try_end_1c} :catch_1d

    #@1c
    .line 55
    :goto_1c
    return-void

    #@1d
    .line 51
    :catch_1d
    move-exception v0

    #@1e
    .line 53
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@21
    goto :goto_1c
.end method

.method static declared-synchronized getGeoFenceServlet(Landroid/os/IBinder;Lcom/qualcomm/services/location/GeoFenceKeeper;)Lcom/qualcomm/services/location/GeoFenceServlet;
    .registers 6
    .parameter "client"
    .parameter "keeper"

    #@0
    .prologue
    .line 33
    const-class v2, Lcom/qualcomm/services/location/GeoFenceServlet;

    #@2
    monitor-enter v2

    #@3
    :try_start_3
    sget-object v1, Lcom/qualcomm/services/location/GeoFenceServlet;->mAllClients:Ljava/util/HashMap;

    #@5
    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Lcom/qualcomm/services/location/GeoFenceServlet;

    #@b
    .line 34
    .local v0, clientHandle:Lcom/qualcomm/services/location/GeoFenceServlet;
    if-nez v0, :cond_19

    #@d
    .line 35
    new-instance v0, Lcom/qualcomm/services/location/GeoFenceServlet;

    #@f
    .end local v0           #clientHandle:Lcom/qualcomm/services/location/GeoFenceServlet;
    invoke-direct {v0, p0, p1}, Lcom/qualcomm/services/location/GeoFenceServlet;-><init>(Landroid/os/IBinder;Lcom/qualcomm/services/location/GeoFenceKeeper;)V

    #@12
    .line 36
    .restart local v0       #clientHandle:Lcom/qualcomm/services/location/GeoFenceServlet;
    sget-object v1, Lcom/qualcomm/services/location/GeoFenceServlet;->mAllClients:Ljava/util/HashMap;

    #@14
    iget-object v3, v0, Lcom/qualcomm/services/location/GeoFenceServlet;->mClient:Landroid/location/IGeoFenceListener;

    #@16
    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_19
    .catchall {:try_start_3 .. :try_end_19} :catchall_1b

    #@19
    .line 38
    :cond_19
    monitor-exit v2

    #@1a
    return-object v0

    #@1b
    .line 33
    .end local v0           #clientHandle:Lcom/qualcomm/services/location/GeoFenceServlet;
    :catchall_1b
    move-exception v1

    #@1c
    monitor-exit v2

    #@1d
    throw v1
.end method


# virtual methods
.method addGeoFence(Landroid/location/GeoFenceParams;)Z
    .registers 4
    .parameter "fence"

    #@0
    .prologue
    .line 70
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceServlet;->mGeoFenceIDs:Ljava/util/HashSet;

    #@2
    iget-object v1, p1, Landroid/location/GeoFenceParams;->mIntent:Landroid/app/PendingIntent;

    #@4
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@7
    .line 71
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceServlet;->mGeoFenceKeeper:Lcom/qualcomm/services/location/GeoFenceKeeper;

    #@9
    invoke-virtual {v0, p0, p1}, Lcom/qualcomm/services/location/GeoFenceKeeper;->addGeoFence(Lcom/qualcomm/services/location/GeoFenceServlet;Landroid/location/GeoFenceParams;)Z

    #@c
    move-result v0

    #@d
    return v0
.end method

.method public binderDied()V
    .registers 5

    #@0
    .prologue
    .line 59
    const-string v2, "GeoClientRemoteHandle"

    #@2
    const-string v3, "binderDied"

    #@4
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 60
    iget-object v2, p0, Lcom/qualcomm/services/location/GeoFenceServlet;->mGeoFenceIDs:Ljava/util/HashSet;

    #@9
    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@c
    move-result-object v1

    #@d
    .local v1, i$:Ljava/util/Iterator;
    :goto_d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@10
    move-result v2

    #@11
    if-eqz v2, :cond_1f

    #@13
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@16
    move-result-object v0

    #@17
    check-cast v0, Landroid/app/PendingIntent;

    #@19
    .line 61
    .local v0, fence:Landroid/app/PendingIntent;
    iget-object v2, p0, Lcom/qualcomm/services/location/GeoFenceServlet;->mGeoFenceKeeper:Lcom/qualcomm/services/location/GeoFenceKeeper;

    #@1b
    invoke-virtual {v2, v0}, Lcom/qualcomm/services/location/GeoFenceKeeper;->removeGeoFence(Landroid/app/PendingIntent;)V

    #@1e
    goto :goto_d

    #@1f
    .line 63
    .end local v0           #fence:Landroid/app/PendingIntent;
    :cond_1f
    iget-object v2, p0, Lcom/qualcomm/services/location/GeoFenceServlet;->mGeoFenceIDs:Ljava/util/HashSet;

    #@21
    invoke-virtual {v2}, Ljava/util/HashSet;->clear()V

    #@24
    .line 64
    sget-object v3, Lcom/qualcomm/services/location/GeoFenceServlet;->mAllClients:Ljava/util/HashMap;

    #@26
    monitor-enter v3

    #@27
    .line 65
    :try_start_27
    sget-object v2, Lcom/qualcomm/services/location/GeoFenceServlet;->mAllClients:Ljava/util/HashMap;

    #@29
    invoke-virtual {v2, p0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@2c
    .line 66
    monitor-exit v3

    #@2d
    .line 67
    return-void

    #@2e
    .line 66
    :catchall_2e
    move-exception v2

    #@2f
    monitor-exit v3
    :try_end_30
    .catchall {:try_start_27 .. :try_end_30} :catchall_2e

    #@30
    throw v2
.end method

.method onGeoFenceDone(Landroid/app/PendingIntent;Z)V
    .registers 5
    .parameter "fenceID"
    .parameter "expired"

    #@0
    .prologue
    .line 79
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceServlet;->mGeoFenceIDs:Ljava/util/HashSet;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@5
    .line 80
    if-eqz p2, :cond_c

    #@7
    .line 82
    :try_start_7
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceServlet;->mClient:Landroid/location/IGeoFenceListener;

    #@9
    invoke-interface {v0, p1}, Landroid/location/IGeoFenceListener;->geoFenceExpired(Landroid/app/PendingIntent;)V
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_c} :catch_2d

    #@c
    .line 88
    :cond_c
    :goto_c
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceServlet;->mGeoFenceIDs:Ljava/util/HashSet;

    #@e
    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_27

    #@14
    .line 90
    :try_start_14
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceServlet;->mClient:Landroid/location/IGeoFenceListener;

    #@16
    invoke-interface {v0}, Landroid/location/IGeoFenceListener;->asBinder()Landroid/os/IBinder;

    #@19
    move-result-object v0

    #@1a
    const/4 v1, 0x0

    #@1b
    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    #@1e
    .line 91
    sget-object v1, Lcom/qualcomm/services/location/GeoFenceServlet;->mAllClients:Ljava/util/HashMap;

    #@20
    monitor-enter v1
    :try_end_21
    .catch Ljava/util/NoSuchElementException; {:try_start_14 .. :try_end_21} :catch_2b

    #@21
    .line 92
    :try_start_21
    sget-object v0, Lcom/qualcomm/services/location/GeoFenceServlet;->mAllClients:Ljava/util/HashMap;

    #@23
    invoke-virtual {v0, p0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@26
    .line 93
    monitor-exit v1

    #@27
    .line 97
    :cond_27
    :goto_27
    return-void

    #@28
    .line 93
    :catchall_28
    move-exception v0

    #@29
    monitor-exit v1
    :try_end_2a
    .catchall {:try_start_21 .. :try_end_2a} :catchall_28

    #@2a
    :try_start_2a
    throw v0
    :try_end_2b
    .catch Ljava/util/NoSuchElementException; {:try_start_2a .. :try_end_2b} :catch_2b

    #@2b
    .line 94
    :catch_2b
    move-exception v0

    #@2c
    goto :goto_27

    #@2d
    .line 83
    :catch_2d
    move-exception v0

    #@2e
    goto :goto_c
.end method

.method removeGeoFence(Landroid/app/PendingIntent;)V
    .registers 3
    .parameter "fenceID"

    #@0
    .prologue
    .line 75
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceServlet;->mGeoFenceKeeper:Lcom/qualcomm/services/location/GeoFenceKeeper;

    #@2
    invoke-virtual {v0, p1}, Lcom/qualcomm/services/location/GeoFenceKeeper;->removeGeoFence(Landroid/app/PendingIntent;)V

    #@5
    .line 76
    return-void
.end method
