.class Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread$1;
.super Landroid/os/Handler;
.source "XTWiFiLocationProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;


# direct methods
.method constructor <init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1379
    iput-object p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 9
    .parameter "msg"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 1385
    :try_start_1
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    #@4
    .line 1386
    iget v1, p1, Landroid/os/Message;->what:I

    #@6
    .line 1387
    .local v1, message:I
    sparse-switch v1, :sswitch_data_144

    #@9
    .line 1455
    .end local v1           #message:I
    :cond_9
    :goto_9
    return-void

    #@a
    .line 1390
    .restart local v1       #message:I
    :sswitch_a
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@c
    if-ne v3, v2, :cond_2a

    #@e
    .line 1392
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;

    #@10
    iget-object v2, v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@12
    invoke-virtual {v2}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->handleEnableNlp()V
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_15} :catch_16

    #@15
    goto :goto_9

    #@16
    .line 1450
    .end local v1           #message:I
    :catch_16
    move-exception v0

    #@17
    .line 1452
    .local v0, e:Ljava/lang/Exception;
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;

    #@19
    iget-object v2, v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@1b
    invoke-static {v2}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$000(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Lcom/qualcomm/lib/location/log/LocLog;

    #@1e
    move-result-object v2

    #@1f
    const-string v3, "XTWiFiLP"

    #@21
    const-string v4, "Unchaught exception in message handler"

    #@23
    invoke-virtual {v2, v3, v4}, Lcom/qualcomm/lib/location/log/LocLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@26
    .line 1453
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@29
    goto :goto_9

    #@2a
    .line 1396
    .end local v0           #e:Ljava/lang/Exception;
    .restart local v1       #message:I
    :cond_2a
    :try_start_2a
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;

    #@2c
    iget-object v2, v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@2e
    invoke-virtual {v2}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->handleDisableNlp()V

    #@31
    goto :goto_9

    #@32
    .line 1400
    :sswitch_32
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;

    #@34
    iget-object v3, v3, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@36
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@38
    if-ne v4, v2, :cond_3e

    #@3a
    :goto_3a
    invoke-virtual {v3, v2}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->handleEnableLocationTracking(Z)V

    #@3d
    goto :goto_9

    #@3e
    :cond_3e
    const/4 v2, 0x0

    #@3f
    goto :goto_3a

    #@40
    .line 1403
    :sswitch_40
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;

    #@42
    iget-object v2, v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@44
    invoke-static {v2}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$2800(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)V

    #@47
    goto :goto_9

    #@48
    .line 1406
    :sswitch_48
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;

    #@4a
    iget-object v2, v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@4c
    invoke-static {v2}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$000(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Lcom/qualcomm/lib/location/log/LocLog;

    #@4f
    move-result-object v2

    #@50
    const-string v3, "XTWiFiLP"

    #@52
    const-string v4, "IPC receiver thread ended, remove local client"

    #@54
    invoke-virtual {v2, v3, v4}, Lcom/qualcomm/lib/location/log/LocLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@57
    .line 1407
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;

    #@59
    iget-object v2, v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@5b
    invoke-static {v2}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$1600(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@5e
    move-result-object v2

    #@5f
    if-eqz v2, :cond_74

    #@61
    .line 1412
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;

    #@63
    iget-object v2, v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@65
    invoke-static {v2}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$1600(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@68
    move-result-object v2

    #@69
    invoke-virtual {v2}, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->disconnect()Z

    #@6c
    .line 1413
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;

    #@6e
    iget-object v2, v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@70
    const/4 v3, 0x0

    #@71
    invoke-static {v2, v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$1602(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;)Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@74
    .line 1416
    :cond_74
    const/16 v2, 0x66

    #@76
    invoke-virtual {p0, v2}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread$1;->removeMessages(I)V

    #@79
    .line 1417
    const/16 v2, 0x64

    #@7b
    invoke-virtual {p0, v2}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread$1;->removeMessages(I)V

    #@7e
    .line 1419
    const/16 v2, 0x64

    #@80
    invoke-virtual {p0, v2}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread$1;->sendEmptyMessage(I)Z

    #@83
    goto :goto_9

    #@84
    .line 1422
    :sswitch_84
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;

    #@86
    iget-object v3, v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@88
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@8a
    check-cast v2, Lcom/qualcomm/lib/location/mq_client/InPostcard;

    #@8c
    invoke-static {v3, v2}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$2900(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;Lcom/qualcomm/lib/location/mq_client/InPostcard;)V

    #@8f
    goto/16 :goto_9

    #@91
    .line 1425
    :sswitch_91
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;

    #@93
    iget-object v3, v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@95
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@97
    check-cast v2, Ljava/lang/Long;

    #@99
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    #@9c
    move-result-wide v4

    #@9d
    invoke-static {v3, v4, v5}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$3002(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;J)J

    #@a0
    .line 1426
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;

    #@a2
    iget-object v2, v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@a4
    invoke-static {v2}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$3000(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)J

    #@a7
    move-result-wide v2

    #@a8
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;

    #@aa
    iget-object v4, v4, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@ac
    invoke-static {v4}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$2300(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)J

    #@af
    move-result-wide v4

    #@b0
    cmp-long v2, v2, v4

    #@b2
    if-gez v2, :cond_c3

    #@b4
    .line 1428
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;

    #@b6
    iget-object v2, v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@b8
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;

    #@ba
    iget-object v3, v3, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@bc
    invoke-static {v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$2300(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)J

    #@bf
    move-result-wide v3

    #@c0
    invoke-static {v2, v3, v4}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$3002(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;J)J

    #@c3
    .line 1430
    :cond_c3
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;

    #@c5
    iget-object v2, v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@c7
    invoke-static {v2}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$000(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Lcom/qualcomm/lib/location/log/LocLog;

    #@ca
    move-result-object v2

    #@cb
    const-string v3, "XTWiFiLP"

    #@cd
    new-instance v4, Ljava/lang/StringBuilder;

    #@cf
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@d2
    const-string v5, "handleMessage TBF set to "

    #@d4
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v4

    #@d8
    iget-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;

    #@da
    iget-object v5, v5, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@dc
    invoke-static {v5}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$3000(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)J

    #@df
    move-result-wide v5

    #@e0
    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@e3
    move-result-object v4

    #@e4
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e7
    move-result-object v4

    #@e8
    invoke-virtual {v2, v3, v4}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@eb
    .line 1433
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;

    #@ed
    iget-object v2, v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@ef
    invoke-static {v2}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$400(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@f2
    move-result-object v2

    #@f3
    sget-object v3, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;->IS_TRACKING_WAIT_FOR_TBF:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@f5
    if-ne v2, v3, :cond_9

    #@f7
    .line 1436
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;

    #@f9
    iget-object v2, v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@fb
    invoke-static {v2}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$100(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Landroid/os/Handler;

    #@fe
    move-result-object v2

    #@ff
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;

    #@101
    iget-object v3, v3, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@103
    invoke-static {v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$3100(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Ljava/lang/Runnable;

    #@106
    move-result-object v3

    #@107
    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@10a
    .line 1437
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;

    #@10c
    iget-object v2, v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@10e
    invoke-static {v2}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$100(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Landroid/os/Handler;

    #@111
    move-result-object v2

    #@112
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;

    #@114
    iget-object v3, v3, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@116
    invoke-static {v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$3100(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Ljava/lang/Runnable;

    #@119
    move-result-object v3

    #@11a
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;

    #@11c
    iget-object v4, v4, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@11e
    invoke-static {v4}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$3000(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)J

    #@121
    move-result-wide v4

    #@122
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@125
    goto/16 :goto_9

    #@127
    .line 1442
    :sswitch_127
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;

    #@129
    iget-object v2, v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@12b
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@12e
    move-result-wide v3

    #@12f
    invoke-static {v2, v3, v4}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$3202(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;J)J

    #@132
    .line 1443
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;

    #@134
    iget-object v2, v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@136
    invoke-static {v2}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$1800(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)V

    #@139
    goto/16 :goto_9

    #@13b
    .line 1446
    :sswitch_13b
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;

    #@13d
    iget-object v2, v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@13f
    invoke-static {v2}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$3300(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)V
    :try_end_142
    .catch Ljava/lang/Exception; {:try_start_2a .. :try_end_142} :catch_16

    #@142
    goto/16 :goto_9

    #@144
    .line 1387
    :sswitch_data_144
    .sparse-switch
        0x1 -> :sswitch_a
        0x2 -> :sswitch_32
        0x7 -> :sswitch_91
        0x8 -> :sswitch_127
        0x9 -> :sswitch_13b
        0x64 -> :sswitch_40
        0x65 -> :sswitch_84
        0x66 -> :sswitch_48
    .end sparse-switch
.end method
