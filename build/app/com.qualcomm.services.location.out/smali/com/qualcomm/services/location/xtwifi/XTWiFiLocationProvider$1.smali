.class Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$1;
.super Lcom/android/internal/location/ILocationProvider$Stub;
.source "XTWiFiLocationProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final m_properties:Lcom/android/internal/location/ProviderProperties;

.field final synthetic this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;


# direct methods
.method constructor <init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)V
    .registers 12
    .parameter

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 203
    iput-object p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$1;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@4
    invoke-direct {p0}, Lcom/android/internal/location/ILocationProvider$Stub;-><init>()V

    #@7
    .line 204
    new-instance v0, Lcom/android/internal/location/ProviderProperties;

    #@9
    const/4 v9, 0x2

    #@a
    move v3, v2

    #@b
    move v4, v2

    #@c
    move v5, v2

    #@d
    move v6, v2

    #@e
    move v7, v2

    #@f
    move v8, v1

    #@10
    invoke-direct/range {v0 .. v9}, Lcom/android/internal/location/ProviderProperties;-><init>(ZZZZZZZII)V

    #@13
    iput-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$1;->m_properties:Lcom/android/internal/location/ProviderProperties;

    #@15
    return-void
.end method


# virtual methods
.method public disable()V
    .registers 5

    #@0
    .prologue
    .line 216
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$1;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@2
    invoke-static {v1}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$000(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Lcom/qualcomm/lib/location/log/LocLog;

    #@5
    move-result-object v1

    #@6
    const-string v2, "XTWiFiLP"

    #@8
    const-string v3, "Request to disable"

    #@a
    invoke-virtual {v1, v2, v3}, Lcom/qualcomm/lib/location/log/LocLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$1;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@f
    invoke-static {v1}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$100(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Landroid/os/Handler;

    #@12
    move-result-object v1

    #@13
    const/4 v2, 0x1

    #@14
    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@17
    move-result-object v0

    #@18
    .line 217
    .local v0, nlpMsg:Landroid/os/Message;
    const/4 v1, 0x0

    #@19
    iput v1, v0, Landroid/os/Message;->arg1:I

    #@1b
    .line 218
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$1;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@1d
    invoke-static {v1}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$100(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Landroid/os/Handler;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@24
    .line 219
    return-void
.end method

.method public enable()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 209
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$1;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@3
    invoke-static {v1}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$000(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Lcom/qualcomm/lib/location/log/LocLog;

    #@6
    move-result-object v1

    #@7
    const-string v2, "XTWiFiLP"

    #@9
    const-string v3, "Request to enable"

    #@b
    invoke-virtual {v1, v2, v3}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@e
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$1;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@10
    invoke-static {v1}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$100(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Landroid/os/Handler;

    #@13
    move-result-object v1

    #@14
    invoke-static {v1, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@17
    move-result-object v0

    #@18
    .line 210
    .local v0, nlpMsg:Landroid/os/Message;
    iput v4, v0, Landroid/os/Message;->arg1:I

    #@1a
    .line 211
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$1;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@1c
    invoke-static {v1}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$100(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Landroid/os/Handler;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@23
    .line 212
    return-void
.end method

.method public getProperties()Lcom/android/internal/location/ProviderProperties;
    .registers 2

    #@0
    .prologue
    .line 234
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$1;->m_properties:Lcom/android/internal/location/ProviderProperties;

    #@2
    return-object v0
.end method

.method public getStatus(Landroid/os/Bundle;)I
    .registers 7
    .parameter "extras"

    #@0
    .prologue
    .line 239
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$1;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@2
    invoke-static {v1}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$200(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Lcom/qualcomm/services/location/xtwifi/NlpPublicStatus;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1}, Lcom/qualcomm/services/location/xtwifi/NlpPublicStatus;->getStatus()I

    #@9
    move-result v0

    #@a
    .line 240
    .local v0, status:I
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$1;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@c
    invoke-static {v1}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$000(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Lcom/qualcomm/lib/location/log/LocLog;

    #@f
    move-result-object v1

    #@10
    const-string v2, "XTWiFiLP"

    #@12
    new-instance v3, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v4, "getStatus: "

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    invoke-virtual {v1, v2, v3}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@28
    .line 241
    return v0
.end method

.method public getStatusUpdateTime()J
    .registers 3

    #@0
    .prologue
    .line 246
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$1;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@2
    invoke-static {v0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$200(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Lcom/qualcomm/services/location/xtwifi/NlpPublicStatus;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Lcom/qualcomm/services/location/xtwifi/NlpPublicStatus;->getStatusUpdateTime()J

    #@9
    move-result-wide v0

    #@a
    return-wide v0
.end method

.method public sendExtraCommand(Ljava/lang/String;Landroid/os/Bundle;)Z
    .registers 4
    .parameter "command"
    .parameter "extras"

    #@0
    .prologue
    .line 252
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public setRequest(Lcom/android/internal/location/ProviderRequest;Landroid/os/WorkSource;)V
    .registers 10
    .parameter "request"
    .parameter "ws"

    #@0
    .prologue
    .line 222
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$1;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@2
    invoke-static {v2}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$000(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Lcom/qualcomm/lib/location/log/LocLog;

    #@5
    move-result-object v2

    #@6
    const-string v3, "XTWiFiLP"

    #@8
    new-instance v4, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v5, "setRequest interval: "

    #@f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v4

    #@13
    iget-wide v5, p1, Lcom/android/internal/location/ProviderRequest;->interval:J

    #@15
    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@18
    move-result-object v4

    #@19
    const-string v5, "reportLocation: "

    #@1b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v4

    #@1f
    iget-boolean v5, p1, Lcom/android/internal/location/ProviderRequest;->reportLocation:Z

    #@21
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@24
    move-result-object v4

    #@25
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v4

    #@29
    invoke-virtual {v2, v3, v4}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@2c
    .line 224
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$1;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@2e
    invoke-static {v2}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$100(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Landroid/os/Handler;

    #@31
    move-result-object v2

    #@32
    const/4 v3, 0x7

    #@33
    invoke-static {v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@36
    move-result-object v1

    #@37
    .line 225
    .local v1, msgSetTime:Landroid/os/Message;
    new-instance v2, Ljava/lang/Long;

    #@39
    iget-wide v3, p1, Lcom/android/internal/location/ProviderRequest;->interval:J

    #@3b
    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    #@3e
    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@40
    .line 226
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$1;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@42
    invoke-static {v2}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$100(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Landroid/os/Handler;

    #@45
    move-result-object v2

    #@46
    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@49
    .line 228
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$1;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@4b
    invoke-static {v2}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$100(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Landroid/os/Handler;

    #@4e
    move-result-object v2

    #@4f
    const/4 v3, 0x2

    #@50
    invoke-static {v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@53
    move-result-object v0

    #@54
    .line 229
    .local v0, msgEnableTracking:Landroid/os/Message;
    iget-boolean v2, p1, Lcom/android/internal/location/ProviderRequest;->reportLocation:Z

    #@56
    if-eqz v2, :cond_65

    #@58
    const/4 v2, 0x1

    #@59
    :goto_59
    iput v2, v0, Landroid/os/Message;->arg1:I

    #@5b
    .line 230
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$1;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@5d
    invoke-static {v2}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$100(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Landroid/os/Handler;

    #@60
    move-result-object v2

    #@61
    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@64
    .line 231
    return-void

    #@65
    .line 229
    :cond_65
    const/4 v2, 0x0

    #@66
    goto :goto_59
.end method
