.class Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread$1;
.super Landroid/os/Handler;
.source "ZppAdaptor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;


# direct methods
.method constructor <init>(Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 323
    iput-object p1, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 27
    .parameter "msg"

    #@0
    .prologue
    .line 327
    invoke-super/range {p0 .. p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    #@3
    .line 328
    move-object/from16 v0, p1

    #@5
    iget v0, v0, Landroid/os/Message;->what:I

    #@7
    move/from16 v23, v0

    #@9
    .line 329
    .local v23, message:I
    packed-switch v23, :pswitch_data_140

    #@c
    .line 402
    :goto_c
    return-void

    #@d
    .line 333
    :pswitch_d
    const/4 v2, 0x0

    #@e
    .line 345
    .local v2, is_pos_valid:Z
    move-object/from16 v0, p1

    #@10
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@12
    move-object/from16 v22, v0

    #@14
    check-cast v22, Landroid/os/Bundle;

    #@16
    .line 346
    .local v22, bundle:Landroid/os/Bundle;
    if-eqz v22, :cond_c3

    #@18
    .line 348
    const-string v1, "BUNDLE_SOURCE_TYPE"

    #@1a
    move-object/from16 v0, v22

    #@1c
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@1f
    move-result v24

    #@20
    .line 349
    .local v24, srctype:I
    const-string v1, "BUNDLE_IS_POS_VALID"

    #@22
    move-object/from16 v0, v22

    #@24
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@27
    move-result v2

    #@28
    .line 350
    const-string v1, "BUNDLE_LAT_DEG"

    #@2a
    move-object/from16 v0, v22

    #@2c
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    #@2f
    move-result-wide v3

    #@30
    .line 351
    .local v3, lat_deg:D
    const-string v1, "BUNDLE_LON_DEG"

    #@32
    move-object/from16 v0, v22

    #@34
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    #@37
    move-result-wide v5

    #@38
    .line 352
    .local v5, lon_deg:D
    const-string v1, "BUNDLE_UNC_M"

    #@3a
    move-object/from16 v0, v22

    #@3c
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    #@3f
    move-result v7

    #@40
    .line 353
    .local v7, unc_m:F
    const-string v1, "BUNDLE_VERTICAL_UNCERTAINITY"

    #@42
    move-object/from16 v0, v22

    #@44
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    #@47
    move-result v8

    #@48
    .line 354
    .local v8, vertical_uncertainity:F
    const-string v1, "BUNDLE_MAC_ARRAY"

    #@4a
    move-object/from16 v0, v22

    #@4c
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    #@4f
    move-result-object v19

    #@50
    .line 355
    .local v19, mac_array:[I
    const-string v1, "BUNDLE_RSSI_ARRAY"

    #@52
    move-object/from16 v0, v22

    #@54
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    #@57
    move-result-object v20

    #@58
    .line 356
    .local v20, rssi_array:[I
    const-string v1, "BUNDLE_CHANNEL_ARRAY"

    #@5a
    move-object/from16 v0, v22

    #@5c
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    #@5f
    move-result-object v21

    #@60
    .line 357
    .local v21, channel_array:[I
    const-string v1, "BUNDLE_ALTITUDE_WRT_ELLIPSOID"

    #@62
    move-object/from16 v0, v22

    #@64
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    #@67
    move-result v11

    #@68
    .line 358
    .local v11, altitude_wrt_ellipsoid:F
    const-string v1, "BUNDLE_AGEOFFIX"

    #@6a
    move-object/from16 v0, v22

    #@6c
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    #@6f
    move-result-wide v9

    #@70
    .line 360
    .local v9, ageOfFix:J
    move-object/from16 v0, p0

    #@72
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;

    #@74
    iget-object v1, v1, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;->this$0:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;

    #@76
    const/4 v12, 0x1

    #@77
    invoke-static {v1, v12}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->access$300(Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;Z)Z

    #@7a
    .line 363
    move-object/from16 v0, p0

    #@7c
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;

    #@7e
    iget-object v1, v1, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;->this$0:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;

    #@80
    invoke-static {v1}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->access$100(Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;)Lcom/qualcomm/lib/location/log/LocLog;

    #@83
    move-result-object v1

    #@84
    const-string v12, "XTWiFiZpp"

    #@86
    const-string v13, "calling injection"

    #@88
    invoke-virtual {v1, v12, v13}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@8b
    .line 364
    move-object/from16 v0, p0

    #@8d
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;

    #@8f
    iget-object v1, v1, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;->this$0:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;

    #@91
    invoke-static {v1}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->access$400(Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;)V

    #@94
    .line 365
    const/4 v1, 0x1

    #@95
    move/from16 v0, v24

    #@97
    if-ne v0, v1, :cond_ae

    #@99
    .line 367
    move-object/from16 v0, p0

    #@9b
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;

    #@9d
    iget-object v1, v1, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;->this$0:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;

    #@9f
    invoke-static/range {v1 .. v11}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->access$500(Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;ZDDFFJF)I

    #@a2
    .line 373
    :cond_a2
    :goto_a2
    move-object/from16 v0, p0

    #@a4
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;

    #@a6
    iget-object v1, v1, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;->this$0:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;

    #@a8
    const/4 v12, 0x0

    #@a9
    invoke-static {v1, v12}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->access$300(Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;Z)Z

    #@ac
    goto/16 :goto_c

    #@ae
    .line 369
    :cond_ae
    const/4 v1, 0x3

    #@af
    move/from16 v0, v24

    #@b1
    if-ne v0, v1, :cond_a2

    #@b3
    .line 371
    move-object/from16 v0, p0

    #@b5
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;

    #@b7
    iget-object v12, v1, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;->this$0:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;

    #@b9
    move v13, v2

    #@ba
    move-wide v14, v3

    #@bb
    move-wide/from16 v16, v5

    #@bd
    move/from16 v18, v7

    #@bf
    invoke-static/range {v12 .. v21}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->access$600(Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;ZDDF[I[I[I)I

    #@c2
    goto :goto_a2

    #@c3
    .line 377
    .end local v3           #lat_deg:D
    .end local v5           #lon_deg:D
    .end local v7           #unc_m:F
    .end local v8           #vertical_uncertainity:F
    .end local v9           #ageOfFix:J
    .end local v11           #altitude_wrt_ellipsoid:F
    .end local v19           #mac_array:[I
    .end local v20           #rssi_array:[I
    .end local v21           #channel_array:[I
    .end local v24           #srctype:I
    :cond_c3
    move-object/from16 v0, p0

    #@c5
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;

    #@c7
    iget-object v1, v1, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;->this$0:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;

    #@c9
    invoke-static {v1}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->access$100(Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;)Lcom/qualcomm/lib/location/log/LocLog;

    #@cc
    move-result-object v1

    #@cd
    const-string v12, "XTWiFiZpp"

    #@cf
    const-string v13, "Invalid injection parameters"

    #@d1
    invoke-virtual {v1, v12, v13}, Lcom/qualcomm/lib/location/log/LocLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@d4
    goto/16 :goto_c

    #@d6
    .line 383
    .end local v2           #is_pos_valid:Z
    .end local v22           #bundle:Landroid/os/Bundle;
    :pswitch_d6
    move-object/from16 v0, p0

    #@d8
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;

    #@da
    iget-object v1, v1, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;->this$0:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;

    #@dc
    const/4 v12, 0x1

    #@dd
    invoke-static {v1, v12}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->access$300(Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;Z)Z

    #@e0
    .line 386
    move-object/from16 v0, p0

    #@e2
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;

    #@e4
    iget-object v1, v1, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;->this$0:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;

    #@e6
    invoke-static {v1}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->access$100(Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;)Lcom/qualcomm/lib/location/log/LocLog;

    #@e9
    move-result-object v1

    #@ea
    const-string v12, "XTWiFiZpp"

    #@ec
    const-string v13, "calling ZPP"

    #@ee
    invoke-virtual {v1, v12, v13}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@f1
    .line 387
    move-object/from16 v0, p0

    #@f3
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;

    #@f5
    iget-object v1, v1, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;->this$0:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;

    #@f7
    invoke-static {v1}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->access$400(Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;)V

    #@fa
    .line 388
    move-object/from16 v0, p0

    #@fc
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;

    #@fe
    iget-object v1, v1, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;->this$0:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;

    #@100
    const/4 v12, 0x1

    #@101
    invoke-static {v1, v12}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->access$700(Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;I)I

    #@104
    move-result v1

    #@105
    if-nez v1, :cond_134

    #@107
    .line 390
    move-object/from16 v0, p0

    #@109
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;

    #@10b
    iget-object v1, v1, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;->this$0:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;

    #@10d
    const/4 v12, 0x1

    #@10e
    invoke-static {v1, v12}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->access$802(Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;Z)Z

    #@111
    .line 396
    :goto_111
    move-object/from16 v0, p0

    #@113
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;

    #@115
    iget-object v1, v1, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;->this$0:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;

    #@117
    const/4 v12, 0x0

    #@118
    invoke-static {v1, v12}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->access$300(Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;Z)Z

    #@11b
    .line 399
    move-object/from16 v0, p0

    #@11d
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;

    #@11f
    iget-object v1, v1, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;->this$0:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;

    #@121
    invoke-static {v1}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->access$1000(Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;)Landroid/os/Handler;

    #@124
    move-result-object v1

    #@125
    move-object/from16 v0, p0

    #@127
    iget-object v12, v0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;

    #@129
    iget-object v12, v12, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;->this$0:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;

    #@12b
    invoke-static {v12}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->access$900(Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;)I

    #@12e
    move-result v12

    #@12f
    invoke-virtual {v1, v12}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@132
    goto/16 :goto_c

    #@134
    .line 394
    :cond_134
    move-object/from16 v0, p0

    #@136
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;

    #@138
    iget-object v1, v1, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;->this$0:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;

    #@13a
    const/4 v12, 0x0

    #@13b
    invoke-static {v1, v12}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->access$802(Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;Z)Z

    #@13e
    goto :goto_111

    #@13f
    .line 329
    nop

    #@140
    :pswitch_data_140
    .packed-switch 0x1
        :pswitch_d6
        :pswitch_d
    .end packed-switch
.end method
