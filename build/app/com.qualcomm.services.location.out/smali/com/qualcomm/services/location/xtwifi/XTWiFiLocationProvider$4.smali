.class Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$4;
.super Ljava/lang/Object;
.source "XTWiFiLocationProvider.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;


# direct methods
.method constructor <init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 615
    iput-object p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$4;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 5

    #@0
    .prologue
    .line 620
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$4;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@2
    invoke-static {v0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$000(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Lcom/qualcomm/lib/location/log/LocLog;

    #@5
    move-result-object v0

    #@6
    const-string v1, "XTWiFiLP"

    #@8
    const-string v2, "timeoutFixSession fix session timed out"

    #@a
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    .line 622
    sget-object v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$7;->$SwitchMap$com$qualcomm$services$location$xtwifi$XTWiFiLocationProvider$InternalState:[I

    #@f
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$4;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@11
    invoke-static {v1}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$400(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;->ordinal()I

    #@18
    move-result v1

    #@19
    aget v0, v0, v1

    #@1b
    packed-switch v0, :pswitch_data_64

    #@1e
    .line 638
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$4;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@20
    invoke-static {v0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$000(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Lcom/qualcomm/lib/location/log/LocLog;

    #@23
    move-result-object v0

    #@24
    const-string v1, "XTWiFiLP"

    #@26
    new-instance v2, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v3, "timeoutFixSession fired at wrong state "

    #@2d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$4;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@33
    invoke-static {v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$400(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@36
    move-result-object v3

    #@37
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v2

    #@3b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@42
    .line 644
    :goto_42
    return-void

    #@43
    .line 625
    :pswitch_43
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$4;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@45
    invoke-static {v0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$000(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Lcom/qualcomm/lib/location/log/LocLog;

    #@48
    move-result-object v0

    #@49
    const-string v1, "XTWiFiLP"

    #@4b
    const-string v2, "timeoutFixSession fired before ZPP timeout?"

    #@4d
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@50
    .line 643
    :goto_50
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$4;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@52
    invoke-static {v0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$2100(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)V

    #@55
    goto :goto_42

    #@56
    .line 633
    :pswitch_56
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$4;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@58
    sget-object v1, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;->XFS_FINAL:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@5a
    invoke-static {v0, v1}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$1902(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;)Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@5d
    .line 634
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$4;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@5f
    invoke-static {v0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$2000(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)V

    #@62
    goto :goto_50

    #@63
    .line 622
    nop

    #@64
    :pswitch_data_64
    .packed-switch 0x1
        :pswitch_43
        :pswitch_56
    .end packed-switch
.end method
