.class final Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread;
.super Ljava/lang/Thread;
.source "XTWiFiOsAgent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "OsAgentThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;


# direct methods
.method private constructor <init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1536
    iput-object p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@2
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1536
    invoke-direct {p0, p1}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread;-><init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)V

    #@3
    return-void
.end method


# virtual methods
.method public run()V
    .registers 9

    #@0
    .prologue
    .line 1541
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    #@3
    .line 1543
    new-instance v0, Ljava/util/Properties;

    #@5
    invoke-direct {v0}, Ljava/util/Properties;-><init>()V

    #@8
    .line 1544
    .local v0, config:Ljava/util/Properties;
    const/4 v2, 0x0

    #@9
    .line 1547
    .local v2, inFileStream:Ljava/io/FileInputStream;
    :try_start_9
    new-instance v3, Ljava/io/FileInputStream;

    #@b
    const-string v5, "/system/etc/xtwifi.conf"

    #@d
    invoke-direct {v3, v5}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_10
    .catchall {:try_start_9 .. :try_end_10} :catchall_8f
    .catch Ljava/io/FileNotFoundException; {:try_start_9 .. :try_end_10} :catch_62
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_10} :catch_71
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_10} :catch_80

    #@10
    .line 1548
    .end local v2           #inFileStream:Ljava/io/FileInputStream;
    .local v3, inFileStream:Ljava/io/FileInputStream;
    :try_start_10
    invoke-virtual {v0, v3}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    #@13
    .line 1552
    const-string v5, "DEBUG_GLOBAL_LOG_LEVEL"

    #@15
    const/4 v6, 0x3

    #@16
    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@19
    move-result-object v6

    #@1a
    invoke-virtual {v0, v5, v6}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1d
    move-result-object v5

    #@1e
    invoke-static {v5}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    #@21
    move-result-object v5

    #@22
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    #@25
    move-result v4

    #@26
    .line 1553
    .local v4, log_level:I
    iget-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@28
    invoke-static {v5}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$700(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Lcom/qualcomm/lib/location/log/LocLog;

    #@2b
    move-result-object v5

    #@2c
    invoke-virtual {v5, v4}, Lcom/qualcomm/lib/location/log/LocLog;->setLevel(I)V
    :try_end_2f
    .catchall {:try_start_10 .. :try_end_2f} :catchall_9b
    .catch Ljava/io/FileNotFoundException; {:try_start_10 .. :try_end_2f} :catch_a4
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_2f} :catch_a1
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_2f} :catch_9e

    #@2f
    .line 1571
    if-eqz v3, :cond_34

    #@31
    .line 1572
    :try_start_31
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_34
    .catch Ljava/io/IOException; {:try_start_31 .. :try_end_34} :catch_5c

    #@34
    :cond_34
    move-object v2, v3

    #@35
    .line 1580
    .end local v3           #inFileStream:Ljava/io/FileInputStream;
    .end local v4           #log_level:I
    .restart local v2       #inFileStream:Ljava/io/FileInputStream;
    :cond_35
    :goto_35
    invoke-static {}, Landroid/os/Looper;->prepare()V

    #@38
    .line 1582
    iget-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@3a
    invoke-static {v5}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$700(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Lcom/qualcomm/lib/location/log/LocLog;

    #@3d
    move-result-object v5

    #@3e
    const-string v6, "XTWiFiOS"

    #@40
    const-string v7, "entering Looper"

    #@42
    invoke-virtual {v5, v6, v7}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@45
    .line 1584
    iget-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@47
    new-instance v6, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread$1;

    #@49
    invoke-direct {v6, p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread$1;-><init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread;)V

    #@4c
    invoke-static {v5, v6}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$202(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;Landroid/os/Handler;)Landroid/os/Handler;

    #@4f
    .line 1627
    iget-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@51
    invoke-static {v5}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$2700(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Ljava/util/concurrent/CountDownLatch;

    #@54
    move-result-object v5

    #@55
    invoke-virtual {v5}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    #@58
    .line 1628
    invoke-static {}, Landroid/os/Looper;->loop()V

    #@5b
    .line 1629
    return-void

    #@5c
    .line 1574
    .end local v2           #inFileStream:Ljava/io/FileInputStream;
    .restart local v3       #inFileStream:Ljava/io/FileInputStream;
    .restart local v4       #log_level:I
    :catch_5c
    move-exception v1

    #@5d
    .line 1576
    .local v1, e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@60
    move-object v2, v3

    #@61
    .line 1578
    .end local v3           #inFileStream:Ljava/io/FileInputStream;
    .restart local v2       #inFileStream:Ljava/io/FileInputStream;
    goto :goto_35

    #@62
    .line 1555
    .end local v1           #e:Ljava/io/IOException;
    .end local v4           #log_level:I
    :catch_62
    move-exception v1

    #@63
    .line 1557
    .local v1, e:Ljava/io/FileNotFoundException;
    :goto_63
    :try_start_63
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_66
    .catchall {:try_start_63 .. :try_end_66} :catchall_8f

    #@66
    .line 1571
    if-eqz v2, :cond_35

    #@68
    .line 1572
    :try_start_68
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_6b
    .catch Ljava/io/IOException; {:try_start_68 .. :try_end_6b} :catch_6c

    #@6b
    goto :goto_35

    #@6c
    .line 1574
    :catch_6c
    move-exception v1

    #@6d
    .line 1576
    .local v1, e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@70
    goto :goto_35

    #@71
    .line 1559
    .end local v1           #e:Ljava/io/IOException;
    :catch_71
    move-exception v1

    #@72
    .line 1561
    .restart local v1       #e:Ljava/io/IOException;
    :goto_72
    :try_start_72
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_75
    .catchall {:try_start_72 .. :try_end_75} :catchall_8f

    #@75
    .line 1571
    if-eqz v2, :cond_35

    #@77
    .line 1572
    :try_start_77
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_7a
    .catch Ljava/io/IOException; {:try_start_77 .. :try_end_7a} :catch_7b

    #@7a
    goto :goto_35

    #@7b
    .line 1574
    :catch_7b
    move-exception v1

    #@7c
    .line 1576
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@7f
    goto :goto_35

    #@80
    .line 1563
    .end local v1           #e:Ljava/io/IOException;
    :catch_80
    move-exception v1

    #@81
    .line 1565
    .local v1, e:Ljava/lang/Exception;
    :goto_81
    :try_start_81
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_84
    .catchall {:try_start_81 .. :try_end_84} :catchall_8f

    #@84
    .line 1571
    if-eqz v2, :cond_35

    #@86
    .line 1572
    :try_start_86
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_89
    .catch Ljava/io/IOException; {:try_start_86 .. :try_end_89} :catch_8a

    #@89
    goto :goto_35

    #@8a
    .line 1574
    :catch_8a
    move-exception v1

    #@8b
    .line 1576
    .local v1, e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@8e
    goto :goto_35

    #@8f
    .line 1569
    .end local v1           #e:Ljava/io/IOException;
    :catchall_8f
    move-exception v5

    #@90
    .line 1571
    :goto_90
    if-eqz v2, :cond_95

    #@92
    .line 1572
    :try_start_92
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_95
    .catch Ljava/io/IOException; {:try_start_92 .. :try_end_95} :catch_96

    #@95
    .line 1577
    :cond_95
    :goto_95
    throw v5

    #@96
    .line 1574
    :catch_96
    move-exception v1

    #@97
    .line 1576
    .restart local v1       #e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@9a
    goto :goto_95

    #@9b
    .line 1569
    .end local v1           #e:Ljava/io/IOException;
    .end local v2           #inFileStream:Ljava/io/FileInputStream;
    .restart local v3       #inFileStream:Ljava/io/FileInputStream;
    :catchall_9b
    move-exception v5

    #@9c
    move-object v2, v3

    #@9d
    .end local v3           #inFileStream:Ljava/io/FileInputStream;
    .restart local v2       #inFileStream:Ljava/io/FileInputStream;
    goto :goto_90

    #@9e
    .line 1563
    .end local v2           #inFileStream:Ljava/io/FileInputStream;
    .restart local v3       #inFileStream:Ljava/io/FileInputStream;
    :catch_9e
    move-exception v1

    #@9f
    move-object v2, v3

    #@a0
    .end local v3           #inFileStream:Ljava/io/FileInputStream;
    .restart local v2       #inFileStream:Ljava/io/FileInputStream;
    goto :goto_81

    #@a1
    .line 1559
    .end local v2           #inFileStream:Ljava/io/FileInputStream;
    .restart local v3       #inFileStream:Ljava/io/FileInputStream;
    :catch_a1
    move-exception v1

    #@a2
    move-object v2, v3

    #@a3
    .end local v3           #inFileStream:Ljava/io/FileInputStream;
    .restart local v2       #inFileStream:Ljava/io/FileInputStream;
    goto :goto_72

    #@a4
    .line 1555
    .end local v2           #inFileStream:Ljava/io/FileInputStream;
    .restart local v3       #inFileStream:Ljava/io/FileInputStream;
    :catch_a4
    move-exception v1

    #@a5
    move-object v2, v3

    #@a6
    .end local v3           #inFileStream:Ljava/io/FileInputStream;
    .restart local v2       #inFileStream:Ljava/io/FileInputStream;
    goto :goto_63
.end method
