.class final enum Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;
.super Ljava/lang/Enum;
.source "XTWiFiLocationProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "XtwifiFixState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

.field public static final enum XFS_FINAL:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

.field public static final enum XFS_IDLE_OR_WAITING:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

.field public static final enum XFS_PRELIMINARY:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 135
    new-instance v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@5
    const-string v1, "XFS_IDLE_OR_WAITING"

    #@7
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;-><init>(Ljava/lang/String;I)V

    #@a
    sput-object v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;->XFS_IDLE_OR_WAITING:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@c
    new-instance v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@e
    const-string v1, "XFS_PRELIMINARY"

    #@10
    invoke-direct {v0, v1, v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;-><init>(Ljava/lang/String;I)V

    #@13
    sput-object v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;->XFS_PRELIMINARY:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@15
    new-instance v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@17
    const-string v1, "XFS_FINAL"

    #@19
    invoke-direct {v0, v1, v4}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;-><init>(Ljava/lang/String;I)V

    #@1c
    sput-object v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;->XFS_FINAL:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@1e
    .line 133
    const/4 v0, 0x3

    #@1f
    new-array v0, v0, [Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@21
    sget-object v1, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;->XFS_IDLE_OR_WAITING:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@23
    aput-object v1, v0, v2

    #@25
    sget-object v1, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;->XFS_PRELIMINARY:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@27
    aput-object v1, v0, v3

    #@29
    sget-object v1, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;->XFS_FINAL:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@2b
    aput-object v1, v0, v4

    #@2d
    sput-object v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;->$VALUES:[Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@2f
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 133
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 133
    const-class v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;
    .registers 1

    #@0
    .prologue
    .line 133
    sget-object v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;->$VALUES:[Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@2
    invoke-virtual {v0}, [Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@8
    return-object v0
.end method
