.class public Lcom/qualcomm/services/location/GeoFenceService;
.super Landroid/app/Service;
.source "GeoFenceService.java"


# static fields
.field private static final LOCAL_LOG:Z = true

.field private static final TAG:Ljava/lang/String; = "GeoFenceService"


# instance fields
.field private mGeoFenceKeeper:Lcom/qualcomm/services/location/GeoFenceKeeper;

.field private final mGeoFencer:Landroid/location/IGeoFencer$Stub;

.field private mKeeperThread:Landroid/os/HandlerThread;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 24
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    #@3
    .line 31
    new-instance v0, Lcom/qualcomm/services/location/GeoFenceService$1;

    #@5
    invoke-direct {v0, p0}, Lcom/qualcomm/services/location/GeoFenceService$1;-><init>(Lcom/qualcomm/services/location/GeoFenceService;)V

    #@8
    iput-object v0, p0, Lcom/qualcomm/services/location/GeoFenceService;->mGeoFencer:Landroid/location/IGeoFencer$Stub;

    #@a
    return-void
.end method

.method static synthetic access$000(Lcom/qualcomm/services/location/GeoFenceService;)Landroid/os/HandlerThread;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 24
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceService;->mKeeperThread:Landroid/os/HandlerThread;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/qualcomm/services/location/GeoFenceService;)Lcom/qualcomm/services/location/GeoFenceKeeper;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 24
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceService;->mGeoFenceKeeper:Lcom/qualcomm/services/location/GeoFenceKeeper;

    #@2
    return-object v0
.end method

.method private log(Ljava/lang/String;)V
    .registers 3
    .parameter "s"

    #@0
    .prologue
    .line 112
    const-string v0, "GeoFenceService"

    #@2
    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 114
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 3
    .parameter "arg0"

    #@0
    .prologue
    .line 91
    const-string v0, "onBind"

    #@2
    invoke-direct {p0, v0}, Lcom/qualcomm/services/location/GeoFenceService;->log(Ljava/lang/String;)V

    #@5
    .line 92
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceService;->mGeoFencer:Landroid/location/IGeoFencer$Stub;

    #@7
    return-object v0
.end method

.method public onCreate()V
    .registers 3

    #@0
    .prologue
    .line 83
    const-string v0, "onCreate"

    #@2
    invoke-direct {p0, v0}, Lcom/qualcomm/services/location/GeoFenceService;->log(Ljava/lang/String;)V

    #@5
    .line 84
    new-instance v0, Landroid/os/HandlerThread;

    #@7
    const-string v1, "GeoFenceService"

    #@9
    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@c
    iput-object v0, p0, Lcom/qualcomm/services/location/GeoFenceService;->mKeeperThread:Landroid/os/HandlerThread;

    #@e
    .line 85
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceService;->mKeeperThread:Landroid/os/HandlerThread;

    #@10
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    #@13
    .line 86
    new-instance v0, Lcom/qualcomm/services/location/GeoFenceKeeper;

    #@15
    iget-object v1, p0, Lcom/qualcomm/services/location/GeoFenceService;->mKeeperThread:Landroid/os/HandlerThread;

    #@17
    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@1a
    move-result-object v1

    #@1b
    invoke-direct {v0, v1, p0}, Lcom/qualcomm/services/location/GeoFenceKeeper;-><init>(Landroid/os/Looper;Landroid/content/Context;)V

    #@1e
    iput-object v0, p0, Lcom/qualcomm/services/location/GeoFenceService;->mGeoFenceKeeper:Lcom/qualcomm/services/location/GeoFenceKeeper;

    #@20
    .line 87
    return-void
.end method

.method public onDestroy()V
    .registers 3

    #@0
    .prologue
    .line 97
    const-string v0, "GeoFenceService"

    #@2
    const-string v1, "onDestroy"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 98
    iget-object v1, p0, Lcom/qualcomm/services/location/GeoFenceService;->mKeeperThread:Landroid/os/HandlerThread;

    #@9
    monitor-enter v1

    #@a
    .line 99
    :try_start_a
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceService;->mKeeperThread:Landroid/os/HandlerThread;

    #@c
    if-eqz v0, :cond_19

    #@e
    .line 100
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceService;->mKeeperThread:Landroid/os/HandlerThread;

    #@10
    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    #@13
    .line 101
    const/4 v0, 0x0

    #@14
    iput-object v0, p0, Lcom/qualcomm/services/location/GeoFenceService;->mKeeperThread:Landroid/os/HandlerThread;

    #@16
    .line 102
    const/4 v0, 0x0

    #@17
    iput-object v0, p0, Lcom/qualcomm/services/location/GeoFenceService;->mGeoFenceKeeper:Lcom/qualcomm/services/location/GeoFenceKeeper;

    #@19
    .line 104
    :cond_19
    monitor-exit v1

    #@1a
    .line 108
    return-void

    #@1b
    .line 104
    :catchall_1b
    move-exception v0

    #@1c
    monitor-exit v1
    :try_end_1d
    .catchall {:try_start_a .. :try_end_1d} :catchall_1b

    #@1d
    throw v0
.end method
