.class public Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;
.super Ljava/lang/Object;
.source "ZppAdaptor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$1;,
        Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;
    }
.end annotation


# static fields
.field private static final BUNDLE_AGEOFFIX:Ljava/lang/String; = "BUNDLE_AGEOFFIX"

.field private static final BUNDLE_ALTITUDE_WRT_ELLIPSOID:Ljava/lang/String; = "BUNDLE_ALTITUDE_WRT_ELLIPSOID"

.field private static final BUNDLE_CHANNEL_ARRAY:Ljava/lang/String; = "BUNDLE_CHANNEL_ARRAY"

.field private static final BUNDLE_IS_POS_VALID:Ljava/lang/String; = "BUNDLE_IS_POS_VALID"

.field private static final BUNDLE_LAT_DEG:Ljava/lang/String; = "BUNDLE_LAT_DEG"

.field private static final BUNDLE_LON_DEG:Ljava/lang/String; = "BUNDLE_LON_DEG"

.field private static final BUNDLE_MAC_ARRAY:Ljava/lang/String; = "BUNDLE_MAC_ARRAY"

.field private static final BUNDLE_RSSI_ARRAY:Ljava/lang/String; = "BUNDLE_RSSI_ARRAY"

.field private static final BUNDLE_SOURCE_TYPE:Ljava/lang/String; = "BUNDLE_SOURCE_TYPE"

.field private static final BUNDLE_UNC_M:Ljava/lang/String; = "BUNDLE_UNC_M"

.field private static final BUNDLE_VERTICAL_UNCERTAINITY:Ljava/lang/String; = "BUNDLE_VERTICAL_UNCERTAINITY"

.field private static final INJECT_SRC_CELLID:I = 0x1

.field private static final INJECT_SRC_WIFI:I = 0x3

.field private static final MSG_GET_ZPP:I = 0x1

.field private static final MSG_INJECT_FIX:I = 0x2

.field private static final TAG:Ljava/lang/String; = "XTWiFiZpp"

.field public static final TECH_SOURCE_BITMASK:Ljava/lang/String; = "TECH_SOURCE_BITMASK"

.field public static final TECH_SOURCE_CELLID:I = 0x2

.field public static final TECH_SOURCE_INJECTED_COARSE_POSITION:I = 0x20

.field public static final TECH_SOURCE_REF_LOCATION:I = 0x10

.field public static final TECH_SOURCE_SATELLITE:I = 0x1

.field public static final TECH_SOURCE_SENSORS:I = 0x8

.field public static final TECH_SOURCE_WIFI:I = 0x4


# instance fields
.field private is_zpp_connected:Z

.field private final llog:Lcom/qualcomm/lib/location/log/LocLog;

.field private m_handler:Landroid/os/Handler;

.field private m_handlerNlp:Landroid/os/Handler;

.field private m_handlerThread:Ljava/lang/Thread;

.field private m_handlerThreadInitialized:Ljava/util/concurrent/CountDownLatch;

.field private m_is_zpp_running:Z

.field private m_lastKnownLocation:Landroid/location/Location;

.field private m_locationExtra:Landroid/os/Bundle;

.field private m_log_level:I

.field private m_msgNlpZppFinished:I

.field private m_mutex:Ljava/lang/Object;

.field private m_was_last_zpp_fix_valid:Z

.field private m_was_last_zpp_successful:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 36
    const-string v0, "xtwifi_zpp_adaptor"

    #@2
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@5
    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;II)V
    .registers 8
    .parameter "nlpHandler"
    .parameter "msgZppFinished"
    .parameter "log_level"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 121
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 39
    new-instance v1, Lcom/qualcomm/lib/location/log/LocLog;

    #@6
    const/4 v2, 0x3

    #@7
    invoke-direct {v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;-><init>(I)V

    #@a
    iput-object v1, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@c
    .line 64
    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    #@e
    const/4 v2, 0x1

    #@f
    invoke-direct {v1, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    #@12
    iput-object v1, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_handlerThreadInitialized:Ljava/util/concurrent/CountDownLatch;

    #@14
    .line 68
    new-instance v1, Ljava/lang/Object;

    #@16
    invoke-direct/range {v1 .. v1}, Ljava/lang/Object;-><init>()V

    #@19
    iput-object v1, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_mutex:Ljava/lang/Object;

    #@1b
    .line 69
    iput-boolean v3, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_is_zpp_running:Z

    #@1d
    .line 70
    iput-boolean v3, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_was_last_zpp_successful:Z

    #@1f
    .line 71
    iput-boolean v3, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_was_last_zpp_fix_valid:Z

    #@21
    .line 72
    new-instance v1, Landroid/location/Location;

    #@23
    const-string v2, "network"

    #@25
    invoke-direct {v1, v2}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    #@28
    iput-object v1, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_lastKnownLocation:Landroid/location/Location;

    #@2a
    .line 73
    new-instance v1, Landroid/os/Bundle;

    #@2c
    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    #@2f
    iput-object v1, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_locationExtra:Landroid/os/Bundle;

    #@31
    .line 77
    iput-boolean v3, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->is_zpp_connected:Z

    #@33
    .line 122
    iput p3, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_log_level:I

    #@35
    .line 123
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@37
    iget v2, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_log_level:I

    #@39
    invoke-virtual {v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->setLevel(I)V

    #@3c
    .line 125
    iput-object p1, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_handlerNlp:Landroid/os/Handler;

    #@3e
    .line 126
    iput p2, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_msgNlpZppFinished:I

    #@40
    .line 128
    iput-boolean v3, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->is_zpp_connected:Z

    #@42
    .line 129
    invoke-direct {p0}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->_tryConnectZpp()V

    #@45
    .line 131
    new-instance v1, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;

    #@47
    const/4 v2, 0x0

    #@48
    invoke-direct {v1, p0, v2}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;-><init>(Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$1;)V

    #@4b
    iput-object v1, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_handlerThread:Ljava/lang/Thread;

    #@4d
    .line 132
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_handlerThread:Ljava/lang/Thread;

    #@4f
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    #@52
    .line 137
    :goto_52
    :try_start_52
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_handlerThreadInitialized:Ljava/util/concurrent/CountDownLatch;

    #@54
    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_57
    .catch Ljava/lang/InterruptedException; {:try_start_52 .. :try_end_57} :catch_58

    #@57
    .line 147
    return-void

    #@58
    .line 140
    :catch_58
    move-exception v0

    #@59
    .line 144
    .local v0, e:Ljava/lang/InterruptedException;
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@5b
    const-string v2, "XTWiFiZpp"

    #@5d
    const-string v3, "Constructor interrupted, retry"

    #@5f
    invoke-virtual {v1, v2, v3}, Lcom/qualcomm/lib/location/log/LocLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@62
    goto :goto_52
.end method

.method private _setZppRunning(Z)Z
    .registers 4
    .parameter "running"

    #@0
    .prologue
    .line 159
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_mutex:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 161
    :try_start_3
    iput-boolean p1, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_is_zpp_running:Z

    #@5
    monitor-exit v1

    #@6
    return p1

    #@7
    .line 162
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method private _tryConnectZpp()V
    .registers 4

    #@0
    .prologue
    .line 95
    iget-boolean v0, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->is_zpp_connected:Z

    #@2
    if-nez v0, :cond_2b

    #@4
    .line 97
    iget v0, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_log_level:I

    #@6
    invoke-direct {p0, v0}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->init(I)I

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_1e

    #@c
    .line 99
    invoke-direct {p0}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->connect()I

    #@f
    move-result v0

    #@10
    if-nez v0, :cond_2c

    #@12
    .line 101
    const/4 v0, 0x1

    #@13
    iput-boolean v0, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->is_zpp_connected:Z

    #@15
    .line 102
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@17
    const-string v1, "XTWiFiZpp"

    #@19
    const-string v2, "ZPP adaptor connected"

    #@1b
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@1e
    .line 109
    :cond_1e
    :goto_1e
    iget-boolean v0, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->is_zpp_connected:Z

    #@20
    if-nez v0, :cond_2b

    #@22
    .line 111
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@24
    const-string v1, "XTWiFiZpp"

    #@26
    const-string v2, "ZPP adaptor NOT connected"

    #@28
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@2b
    .line 118
    :cond_2b
    return-void

    #@2c
    .line 106
    :cond_2c
    invoke-direct {p0}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->deinit()I

    #@2f
    goto :goto_1e
.end method

.method static synthetic access$100(Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;)Lcom/qualcomm/lib/location/log/LocLog;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 32
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 32
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_handlerNlp:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$1100(Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;)Ljava/util/concurrent/CountDownLatch;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 32
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_handlerThreadInitialized:Ljava/util/concurrent/CountDownLatch;

    #@2
    return-object v0
.end method

.method static synthetic access$202(Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;Landroid/os/Handler;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 32
    iput-object p1, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_handler:Landroid/os/Handler;

    #@2
    return-object p1
.end method

.method static synthetic access$300(Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;Z)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->_setZppRunning(Z)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$400(Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 32
    invoke-direct {p0}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->_tryConnectZpp()V

    #@3
    return-void
.end method

.method static synthetic access$500(Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;ZDDFFJF)I
    .registers 12
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"
    .parameter "x6"
    .parameter "x7"

    #@0
    .prologue
    .line 32
    invoke-direct/range {p0 .. p10}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->sendWwanFix(ZDDFFJF)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$600(Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;ZDDF[I[I[I)I
    .registers 11
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"
    .parameter "x6"
    .parameter "x7"

    #@0
    .prologue
    .line 32
    invoke-direct/range {p0 .. p9}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->sendWiFiFix(ZDDF[I[I[I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$700(Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;I)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->getZppFix(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$802(Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_was_last_zpp_successful:Z

    #@2
    return p1
.end method

.method static synthetic access$900(Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 32
    iget v0, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_msgNlpZppFinished:I

    #@2
    return v0
.end method

.method private native connect()I
.end method

.method private native deinit()I
.end method

.method private native getZppFix(I)I
.end method

.method private native init(I)I
.end method

.method private native sendWiFiFix(ZDDF[I[I[I)I
.end method

.method private native sendWwanFix(ZDDFFJF)I
.end method


# virtual methods
.method public getLastKnownLocation(Landroid/location/Location;)Z
    .registers 4
    .parameter "copy"

    #@0
    .prologue
    .line 248
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_mutex:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 251
    :try_start_3
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_lastKnownLocation:Landroid/location/Location;

    #@5
    invoke-virtual {p1, v0}, Landroid/location/Location;->set(Landroid/location/Location;)V

    #@8
    .line 252
    iget-boolean v0, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_was_last_zpp_successful:Z

    #@a
    if-eqz v0, :cond_13

    #@c
    iget-boolean v0, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_was_last_zpp_fix_valid:Z

    #@e
    if-eqz v0, :cond_13

    #@10
    const/4 v0, 0x1

    #@11
    :goto_11
    monitor-exit v1

    #@12
    return v0

    #@13
    :cond_13
    const/4 v0, 0x0

    #@14
    goto :goto_11

    #@15
    .line 253
    :catchall_15
    move-exception v0

    #@16
    monitor-exit v1
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_15

    #@17
    throw v0
.end method

.method public injectFix(IZDDFFJF[I[I[I)Z
    .registers 24
    .parameter "src"
    .parameter "is_pos_valid"
    .parameter "lat_deg"
    .parameter "lon_deg"
    .parameter "unc_m"
    .parameter "vertical_uncertainity"
    .parameter "ageOfFix"
    .parameter "altitude_wrt_ellipsoid"
    .parameter "mac_array"
    .parameter "rssi_array"
    .parameter "channel_array"

    #@0
    .prologue
    .line 201
    iget-object v5, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_mutex:Ljava/lang/Object;

    #@2
    monitor-enter v5

    #@3
    .line 203
    :try_start_3
    new-instance v2, Landroid/os/Bundle;

    #@5
    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    #@8
    .line 204
    .local v2, bundle:Landroid/os/Bundle;
    const-string v4, "BUNDLE_SOURCE_TYPE"

    #@a
    invoke-virtual {v2, v4, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@d
    .line 205
    const-string v4, "BUNDLE_IS_POS_VALID"

    #@f
    invoke-virtual {v2, v4, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@12
    .line 206
    const-string v4, "BUNDLE_LAT_DEG"

    #@14
    invoke-virtual {v2, v4, p3, p4}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    #@17
    .line 207
    const-string v4, "BUNDLE_LON_DEG"

    #@19
    invoke-virtual {v2, v4, p5, p6}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    #@1c
    .line 208
    const-string v4, "BUNDLE_UNC_M"

    #@1e
    move/from16 v0, p7

    #@20
    invoke-virtual {v2, v4, v0}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    #@23
    .line 209
    const-string v4, "BUNDLE_VERTICAL_UNCERTAINITY"

    #@25
    move/from16 v0, p8

    #@27
    invoke-virtual {v2, v4, v0}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    #@2a
    .line 210
    const-string v4, "BUNDLE_AGEOFFIX"

    #@2c
    move-wide/from16 v0, p9

    #@2e
    invoke-virtual {v2, v4, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@31
    .line 211
    const-string v4, "BUNDLE_ALTITUDE_WRT_ELLIPSOID"

    #@33
    move/from16 v0, p11

    #@35
    invoke-virtual {v2, v4, v0}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    #@38
    .line 212
    const-string v4, "BUNDLE_MAC_ARRAY"

    #@3a
    move-object/from16 v0, p12

    #@3c
    invoke-virtual {v2, v4, v0}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    #@3f
    .line 213
    const-string v4, "BUNDLE_RSSI_ARRAY"

    #@41
    move-object/from16 v0, p13

    #@43
    invoke-virtual {v2, v4, v0}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    #@46
    .line 214
    const-string v4, "BUNDLE_CHANNEL_ARRAY"

    #@48
    move-object/from16 v0, p14

    #@4a
    invoke-virtual {v2, v4, v0}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    #@4d
    .line 215
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_handler:Landroid/os/Handler;

    #@4f
    const/4 v6, 0x2

    #@50
    const/4 v7, 0x0

    #@51
    const/4 v8, 0x0

    #@52
    invoke-virtual {v4, v6, v7, v8, v2}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@55
    move-result-object v3

    #@56
    .line 216
    .local v3, msg:Landroid/os/Message;
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_handler:Landroid/os/Handler;

    #@58
    invoke-virtual {v4, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@5b
    move-result v4

    #@5c
    monitor-exit v5

    #@5d
    return v4

    #@5e
    .line 217
    .end local v2           #bundle:Landroid/os/Bundle;
    .end local v3           #msg:Landroid/os/Message;
    :catchall_5e
    move-exception v4

    #@5f
    monitor-exit v5
    :try_end_60
    .catchall {:try_start_3 .. :try_end_60} :catchall_5e

    #@60
    throw v4
.end method

.method public injectFixIfNotAlreadyRunning(ZDDF[I[I[I)Z
    .registers 18
    .parameter "is_pos_valid"
    .parameter "lat_deg"
    .parameter "lon_deg"
    .parameter "unc_m"
    .parameter "mac_array"
    .parameter "rssi_array"
    .parameter "channel_array"

    #@0
    .prologue
    .line 223
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_mutex:Ljava/lang/Object;

    #@2
    monitor-enter v4

    #@3
    .line 225
    :try_start_3
    iget-boolean v3, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_is_zpp_running:Z

    #@5
    if-nez v3, :cond_44

    #@7
    .line 227
    new-instance v1, Landroid/os/Bundle;

    #@9
    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    #@c
    .line 228
    .local v1, bundle:Landroid/os/Bundle;
    const-string v3, "BUNDLE_IS_POS_VALID"

    #@e
    invoke-virtual {v1, v3, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@11
    .line 229
    const-string v3, "BUNDLE_LAT_DEG"

    #@13
    invoke-virtual {v1, v3, p2, p3}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    #@16
    .line 230
    const-string v3, "BUNDLE_LON_DEG"

    #@18
    invoke-virtual {v1, v3, p4, p5}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    #@1b
    .line 231
    const-string v3, "BUNDLE_UNC_M"

    #@1d
    invoke-virtual {v1, v3, p6}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    #@20
    .line 232
    const-string v3, "BUNDLE_MAC_ARRAY"

    #@22
    invoke-virtual {v1, v3, p7}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    #@25
    .line 233
    const-string v3, "BUNDLE_RSSI_ARRAY"

    #@27
    move-object/from16 v0, p8

    #@29
    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    #@2c
    .line 234
    const-string v3, "BUNDLE_CHANNEL_ARRAY"

    #@2e
    move-object/from16 v0, p9

    #@30
    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    #@33
    .line 235
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_handler:Landroid/os/Handler;

    #@35
    const/4 v5, 0x2

    #@36
    const/4 v6, 0x0

    #@37
    const/4 v7, 0x0

    #@38
    invoke-virtual {v3, v5, v6, v7, v1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@3b
    move-result-object v2

    #@3c
    .line 236
    .local v2, msg:Landroid/os/Message;
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_handler:Landroid/os/Handler;

    #@3e
    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@41
    move-result v3

    #@42
    monitor-exit v4

    #@43
    .line 241
    .end local v1           #bundle:Landroid/os/Bundle;
    .end local v2           #msg:Landroid/os/Message;
    :goto_43
    return v3

    #@44
    .line 240
    :cond_44
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@46
    const-string v5, "XTWiFiZpp"

    #@48
    const-string v6, "ZPP fix is still running. skip injection"

    #@4a
    invoke-virtual {v3, v5, v6}, Lcom/qualcomm/lib/location/log/LocLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@4d
    .line 241
    const/4 v3, 0x1

    #@4e
    monitor-exit v4

    #@4f
    goto :goto_43

    #@50
    .line 243
    :catchall_50
    move-exception v3

    #@51
    monitor-exit v4
    :try_end_52
    .catchall {:try_start_3 .. :try_end_52} :catchall_50

    #@52
    throw v3
.end method

.method public isZppRunning()Z
    .registers 3

    #@0
    .prologue
    .line 151
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_mutex:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 153
    :try_start_3
    iget-boolean v0, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_is_zpp_running:Z

    #@5
    monitor-exit v1

    #@6
    return v0

    #@7
    .line 154
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public location_report(ZIZDZDZFZJ)V
    .registers 20
    .parameter "is_source_valid"
    .parameter "source_bitmask"
    .parameter "is_latitude_valid"
    .parameter "latitude"
    .parameter "is_longitude_valid"
    .parameter "longitude"
    .parameter "is_hor_unc_valid"
    .parameter "hor_unc"
    .parameter "is_timestamp_valid"
    .parameter "timestamp"

    #@0
    .prologue
    .line 260
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@2
    const-string v3, "XTWiFiZpp"

    #@4
    new-instance v4, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v5, "location report callback 1: source validity: "

    #@b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v4

    #@f
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@12
    move-result-object v4

    #@13
    const-string v5, ", latitude validity: "

    #@15
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v4

    #@19
    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v4

    #@1d
    const-string v5, ", longitude validity: "

    #@1f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v4

    #@23
    invoke-virtual {v4, p6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@26
    move-result-object v4

    #@27
    const-string v5, ", unc validity: "

    #@29
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v4, p9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@30
    move-result-object v4

    #@31
    const-string v5, ", timestamp validity: "

    #@33
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v4

    #@37
    move/from16 v0, p11

    #@39
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v4

    #@3d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v4

    #@41
    invoke-virtual {v2, v3, v4}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@44
    .line 263
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@46
    const-string v3, "XTWiFiZpp"

    #@48
    new-instance v4, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v5, "location report callback 2: source "

    #@4f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v4

    #@53
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@56
    move-result-object v4

    #@57
    const-string v5, ", ("

    #@59
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v4

    #@5d
    invoke-virtual {v4, p4, p5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    #@60
    move-result-object v4

    #@61
    const-string v5, ", "

    #@63
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v4

    #@67
    invoke-virtual {v4, p7, p8}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v4

    #@6b
    const-string v5, "), unc "

    #@6d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v4

    #@71
    move/from16 v0, p10

    #@73
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@76
    move-result-object v4

    #@77
    const-string v5, ", timestamp "

    #@79
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v4

    #@7d
    move-wide/from16 v0, p12

    #@7f
    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@82
    move-result-object v4

    #@83
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@86
    move-result-object v4

    #@87
    invoke-virtual {v2, v3, v4}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@8a
    .line 266
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_mutex:Ljava/lang/Object;

    #@8c
    monitor-enter v3

    #@8d
    .line 270
    const/4 v2, 0x0

    #@8e
    :try_start_8e
    iput-boolean v2, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_was_last_zpp_fix_valid:Z

    #@90
    .line 271
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_lastKnownLocation:Landroid/location/Location;

    #@92
    invoke-virtual {v2}, Landroid/location/Location;->reset()V

    #@95
    .line 272
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_locationExtra:Landroid/os/Bundle;

    #@97
    invoke-virtual {v2}, Landroid/os/Bundle;->clear()V

    #@9a
    .line 274
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_lastKnownLocation:Landroid/location/Location;

    #@9c
    const-string v4, "network"

    #@9e
    invoke-virtual {v2, v4}, Landroid/location/Location;->setProvider(Ljava/lang/String;)V

    #@a1
    .line 277
    if-eqz p1, :cond_aa

    #@a3
    .line 279
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_locationExtra:Landroid/os/Bundle;

    #@a5
    const-string v4, "TECH_SOURCE_BITMASK"

    #@a7
    invoke-virtual {v2, v4, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@aa
    .line 282
    :cond_aa
    if-nez p11, :cond_ae

    #@ac
    .line 285
    const-wide/16 p12, 0x0

    #@ae
    .line 288
    :cond_ae
    if-eqz p3, :cond_f1

    #@b0
    if-eqz p6, :cond_f1

    #@b2
    if-eqz p9, :cond_f1

    #@b4
    .line 290
    const-wide/16 v4, 0x0

    #@b6
    cmpl-double v2, p4, v4

    #@b8
    if-nez v2, :cond_cb

    #@ba
    const-wide/16 v4, 0x0

    #@bc
    cmpl-double v2, p7, v4

    #@be
    if-nez v2, :cond_cb

    #@c0
    .line 293
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@c2
    const-string v4, "XTWiFiZpp"

    #@c4
    const-string v5, "ZPP fix contains lat/lon: (0,0), ignore"

    #@c6
    invoke-virtual {v2, v4, v5}, Lcom/qualcomm/lib/location/log/LocLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@c9
    .line 309
    :goto_c9
    monitor-exit v3

    #@ca
    .line 310
    return-void

    #@cb
    .line 297
    :cond_cb
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_lastKnownLocation:Landroid/location/Location;

    #@cd
    invoke-virtual {v2, p4, p5}, Landroid/location/Location;->setLatitude(D)V

    #@d0
    .line 298
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_lastKnownLocation:Landroid/location/Location;

    #@d2
    invoke-virtual {v2, p7, p8}, Landroid/location/Location;->setLongitude(D)V

    #@d5
    .line 299
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_lastKnownLocation:Landroid/location/Location;

    #@d7
    move/from16 v0, p10

    #@d9
    invoke-virtual {v2, v0}, Landroid/location/Location;->setAccuracy(F)V

    #@dc
    .line 300
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_lastKnownLocation:Landroid/location/Location;

    #@de
    move-wide/from16 v0, p12

    #@e0
    invoke-virtual {v2, v0, v1}, Landroid/location/Location;->setTime(J)V

    #@e3
    .line 301
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_lastKnownLocation:Landroid/location/Location;

    #@e5
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_locationExtra:Landroid/os/Bundle;

    #@e7
    invoke-virtual {v2, v4}, Landroid/location/Location;->setExtras(Landroid/os/Bundle;)V

    #@ea
    .line 302
    const/4 v2, 0x1

    #@eb
    iput-boolean v2, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_was_last_zpp_fix_valid:Z

    #@ed
    goto :goto_c9

    #@ee
    .line 309
    :catchall_ee
    move-exception v2

    #@ef
    monitor-exit v3
    :try_end_f0
    .catchall {:try_start_8e .. :try_end_f0} :catchall_ee

    #@f0
    throw v2

    #@f1
    .line 307
    :cond_f1
    :try_start_f1
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@f3
    const-string v4, "XTWiFiZpp"

    #@f5
    const-string v5, "ZPP fix is invalid"

    #@f7
    invoke-virtual {v2, v4, v5}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_fa
    .catchall {:try_start_f1 .. :try_end_fa} :catchall_ee

    #@fa
    goto :goto_c9
.end method

.method public triggerZppIfNotAlreadyRunning()Z
    .registers 6

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 167
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_mutex:Ljava/lang/Object;

    #@3
    monitor-enter v1

    #@4
    .line 169
    :try_start_4
    iget-boolean v2, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_is_zpp_running:Z

    #@6
    if-nez v2, :cond_11

    #@8
    .line 171
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->m_handler:Landroid/os/Handler;

    #@a
    const/4 v2, 0x1

    #@b
    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@e
    move-result v0

    #@f
    monitor-exit v1

    #@10
    .line 176
    :goto_10
    return v0

    #@11
    .line 175
    :cond_11
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@13
    const-string v3, "XTWiFiZpp"

    #@15
    const-string v4, "ZPP fix is still running. skip triggering"

    #@17
    invoke-virtual {v2, v3, v4}, Lcom/qualcomm/lib/location/log/LocLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@1a
    .line 176
    monitor-exit v1

    #@1b
    goto :goto_10

    #@1c
    .line 178
    :catchall_1c
    move-exception v0

    #@1d
    monitor-exit v1
    :try_end_1e
    .catchall {:try_start_4 .. :try_end_1e} :catchall_1c

    #@1e
    throw v0
.end method
