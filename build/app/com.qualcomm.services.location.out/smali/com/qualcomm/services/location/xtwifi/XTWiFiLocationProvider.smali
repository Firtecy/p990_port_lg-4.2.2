.class public Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;
.super Landroid/app/Service;
.source "XTWiFiLocationProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$7;,
        Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;,
        Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;,
        Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;
    }
.end annotation


# static fields
.field private static final BUNDLE_ALTITUDE_WRT_ELLIPSOID:Ljava/lang/String; = "BUNDLE_ALTITUDE_WRT_ELLIPSOID"

.field private static final BUNDLE_VERTICAL_UNCERTAINITY:Ljava/lang/String; = "BUNDLE_VERTICAL_UNCERTAINITY"

.field public static final CONFIG_FILE:Ljava/lang/String; = "/system/etc/xtwifi.conf"

.field public static final CONNECT_RETRY_LIMIT:I = 0x14

.field private static final INJECT_SRC_CELLID:I = 0x1

.field private static final INJECT_SRC_WIFI:I = 0x3

.field public static final INVALID_TX_ID:I = -0x1

.field public static final MAX_VALID_TX_ID:I = 0x3e8

.field public static final MIN_VALID_TX_ID:I = 0x1

.field public static final MQ_SOCKET:Ljava/lang/String; = "/data/misc/location/mq/location-mq-s"

.field private static final MSG_ENABLE:I = 0x1

.field private static final MSG_ENABLE_TRACKING:I = 0x2

.field private static final MSG_IPC_EXCEPTION:I = 0x66

.field private static final MSG_NEW_IPC_MESSAGE:I = 0x65

.field private static final MSG_SET_MIN_TIME:I = 0x7

.field private static final MSG_TRY_CONNECT:I = 0x64

.field private static final MSG_WIFI_SCAN_RESULT_AVAILABLE:I = 0x9

.field private static final MSG_ZPP_FINISHED:I = 0x8

.field private static final TAG:Ljava/lang/String; = "XTWiFiLP"


# instance fields
.field private final llog:Lcom/qualcomm/lib/location/log/LocLog;

.field private final m_FreeRideLocationListener:Landroid/location/LocationListener;

.field private m_LocationManager:Landroid/location/ILocationManager;

.field private final m_WiFiScanResultReceiver:Landroid/content/BroadcastReceiver;

.field private m_connectRetryCounter:I

.field private m_currentTimeBetweenFix:J

.field private m_current_gtp_req_tx_id:I

.field private m_got_xtwwan_response:Z

.field private m_handler:Landroid/os/Handler;

.field private m_handlerThread:Ljava/lang/Thread;

.field private m_handlerThreadInitialized:Ljava/util/concurrent/CountDownLatch;

.field private m_has_reported_anything_in_current_fix_session:Z

.field private m_internalState:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

.field private m_ipcClient:Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

.field private m_isEnabledFixReqOnFreeWiFiScan:Z

.field private m_isFreeRideListenerInstalled:Z

.field private m_isXTWWANEnabled:Z

.field private m_isZppEnabled:Z

.field private m_is_xtwifi_fix_with_lat_lon:Z

.field private m_is_xtwwan_fix_with_lat_lon:Z

.field private m_lastKnownXtwifiFix:Landroid/location/Location;

.field private m_lastKnownXtwifiFix_channel_array:[I

.field private m_lastKnownXtwifiFix_mac_array:[I

.field private m_lastKnownXtwifiFix_rssi_array:[I

.field private m_lastKnownZppFix:Landroid/location/Location;

.field private m_log_level:I

.field private m_maxAgeForZppFix:J

.field private m_maxAgeForZppTransaction:J

.field private m_maxTimeBetweenWiFiScanAndRequest:J

.field private m_minTimeBetweenFix:J

.field private m_minTimeBetweenFixOnFreeWiFiScan:J

.field private m_next_gtp_req_tx_id:I

.field private m_publicLocationManager:Landroid/location/LocationManager;

.field private m_publicStatus:Lcom/qualcomm/services/location/xtwifi/NlpPublicStatus;

.field private final m_stubBinder:Lcom/android/internal/location/ILocationProvider$Stub;

.field private m_timeoutFinalFix:J

.field private m_timeoutZppFix:J

.field private m_timestampLastZppReport:J

.field private m_timestamp_last_fix_request_in_current_tracking_session:J

.field private m_timestamp_last_fix_request_on_free_wifi_scan:J

.field private m_timestamp_last_wifi_scan_result:J

.field private m_xtwifi_tx_state:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

.field private m_zppAdaptor:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;

.field private timeoutFixSession:Ljava/lang/Runnable;

.field private timeoutNextFixSessionDue:Ljava/lang/Runnable;

.field private timeoutZppFix:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .registers 10

    #@0
    .prologue
    const/4 v3, 0x3

    #@1
    const-wide/16 v7, 0x0

    #@3
    const/4 v6, 0x1

    #@4
    const/4 v5, 0x0

    #@5
    const/4 v4, 0x0

    #@6
    .line 332
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    #@9
    .line 68
    new-instance v2, Lcom/qualcomm/lib/location/log/LocLog;

    #@b
    invoke-direct {v2, v3}, Lcom/qualcomm/lib/location/log/LocLog;-><init>(I)V

    #@e
    iput-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@10
    .line 93
    iput v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_log_level:I

    #@12
    .line 95
    new-instance v2, Ljava/util/concurrent/CountDownLatch;

    #@14
    invoke-direct {v2, v6}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    #@17
    iput-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_handlerThreadInitialized:Ljava/util/concurrent/CountDownLatch;

    #@19
    .line 100
    iput v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_connectRetryCounter:I

    #@1b
    .line 101
    iput-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_ipcClient:Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@1d
    .line 103
    iput-boolean v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_isZppEnabled:Z

    #@1f
    .line 104
    iput-boolean v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_isXTWWANEnabled:Z

    #@21
    .line 107
    const-wide/16 v2, 0xbb8

    #@23
    iput-wide v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_currentTimeBetweenFix:J

    #@25
    .line 108
    const-wide/16 v2, 0x3e8

    #@27
    iput-wide v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_minTimeBetweenFix:J

    #@29
    .line 109
    iput-wide v7, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_maxAgeForZppTransaction:J

    #@2b
    .line 110
    const-wide/16 v2, 0x1388

    #@2d
    iput-wide v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_maxAgeForZppFix:J

    #@2f
    .line 111
    const-wide/16 v2, 0x1388

    #@31
    iput-wide v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_timeoutZppFix:J

    #@33
    .line 112
    const-wide/32 v2, 0xafc8

    #@36
    iput-wide v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_timeoutFinalFix:J

    #@38
    .line 115
    iput-boolean v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_isEnabledFixReqOnFreeWiFiScan:Z

    #@3a
    .line 116
    const-wide/16 v2, 0x4e20

    #@3c
    iput-wide v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_minTimeBetweenFixOnFreeWiFiScan:J

    #@3e
    .line 117
    iput-wide v7, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_timestamp_last_fix_request_on_free_wifi_scan:J

    #@40
    .line 118
    iput-boolean v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_isFreeRideListenerInstalled:Z

    #@42
    .line 119
    const-wide/16 v2, 0x7d0

    #@44
    iput-wide v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_maxTimeBetweenWiFiScanAndRequest:J

    #@46
    .line 120
    iput-wide v7, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_timestamp_last_wifi_scan_result:J

    #@48
    .line 139
    sget-object v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;->IS_IDLE:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@4a
    iput-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_internalState:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@4c
    .line 141
    iput v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_next_gtp_req_tx_id:I

    #@4e
    .line 142
    const-wide/16 v2, -0x1

    #@50
    iput-wide v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_timestamp_last_fix_request_in_current_tracking_session:J

    #@52
    .line 145
    const/4 v2, -0x1

    #@53
    iput v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_current_gtp_req_tx_id:I

    #@55
    .line 147
    iput-boolean v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_is_xtwifi_fix_with_lat_lon:Z

    #@57
    .line 148
    iput-boolean v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_is_xtwwan_fix_with_lat_lon:Z

    #@59
    .line 149
    iput-boolean v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_got_xtwwan_response:Z

    #@5b
    .line 150
    iput-boolean v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_has_reported_anything_in_current_fix_session:Z

    #@5d
    .line 151
    new-instance v2, Landroid/location/Location;

    #@5f
    const-string v3, "network"

    #@61
    invoke-direct {v2, v3}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    #@64
    iput-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix:Landroid/location/Location;

    #@66
    .line 152
    iput-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix_mac_array:[I

    #@68
    .line 153
    iput-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix_rssi_array:[I

    #@6a
    .line 154
    iput-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix_channel_array:[I

    #@6c
    .line 157
    new-instance v2, Landroid/location/Location;

    #@6e
    const-string v3, "network"

    #@70
    invoke-direct {v2, v3}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    #@73
    iput-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownZppFix:Landroid/location/Location;

    #@75
    .line 158
    const-wide/16 v2, -0x1

    #@77
    iput-wide v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_timestampLastZppReport:J

    #@79
    .line 160
    new-instance v2, Lcom/qualcomm/services/location/xtwifi/NlpPublicStatus;

    #@7b
    invoke-direct {v2}, Lcom/qualcomm/services/location/xtwifi/NlpPublicStatus;-><init>()V

    #@7e
    iput-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_publicStatus:Lcom/qualcomm/services/location/xtwifi/NlpPublicStatus;

    #@80
    .line 202
    new-instance v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$1;

    #@82
    invoke-direct {v2, p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$1;-><init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)V

    #@85
    iput-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_stubBinder:Lcom/android/internal/location/ILocationProvider$Stub;

    #@87
    .line 437
    new-instance v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;

    #@89
    invoke-direct {v2, p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$2;-><init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)V

    #@8c
    iput-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->timeoutNextFixSessionDue:Ljava/lang/Runnable;

    #@8e
    .line 599
    new-instance v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$3;

    #@90
    invoke-direct {v2, p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$3;-><init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)V

    #@93
    iput-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->timeoutZppFix:Ljava/lang/Runnable;

    #@95
    .line 614
    new-instance v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$4;

    #@97
    invoke-direct {v2, p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$4;-><init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)V

    #@9a
    iput-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->timeoutFixSession:Ljava/lang/Runnable;

    #@9c
    .line 947
    new-instance v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$5;

    #@9e
    invoke-direct {v2, p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$5;-><init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)V

    #@a1
    iput-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_WiFiScanResultReceiver:Landroid/content/BroadcastReceiver;

    #@a3
    .line 957
    new-instance v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$6;

    #@a5
    invoke-direct {v2, p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$6;-><init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)V

    #@a8
    iput-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_FreeRideLocationListener:Landroid/location/LocationListener;

    #@aa
    .line 333
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@ac
    const-string v3, "XTWiFiLP"

    #@ae
    const-string v4, "Constructor"

    #@b0
    invoke-virtual {v2, v3, v4}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@b3
    .line 336
    const-string v2, "location"

    #@b5
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@b8
    move-result-object v1

    #@b9
    .line 337
    .local v1, locManBinder:Landroid/os/IBinder;
    invoke-static {v1}, Landroid/location/ILocationManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/location/ILocationManager;

    #@bc
    move-result-object v2

    #@bd
    iput-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_LocationManager:Landroid/location/ILocationManager;

    #@bf
    .line 338
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_LocationManager:Landroid/location/ILocationManager;

    #@c1
    if-nez v2, :cond_cc

    #@c3
    .line 340
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@c5
    const-string v3, "XTWiFiLP"

    #@c7
    const-string v4, "Unable to get private LOCATION_SERVICE"

    #@c9
    invoke-virtual {v2, v3, v4}, Lcom/qualcomm/lib/location/log/LocLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@cc
    .line 343
    :cond_cc
    new-instance v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;

    #@ce
    invoke-direct {v2, p0, v5}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;-><init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$1;)V

    #@d1
    iput-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_handlerThread:Ljava/lang/Thread;

    #@d3
    .line 344
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_handlerThread:Ljava/lang/Thread;

    #@d5
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    #@d8
    .line 349
    :goto_d8
    :try_start_d8
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_handlerThreadInitialized:Ljava/util/concurrent/CountDownLatch;

    #@da
    invoke-virtual {v2}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_dd
    .catch Ljava/lang/InterruptedException; {:try_start_d8 .. :try_end_dd} :catch_de

    #@dd
    .line 359
    return-void

    #@de
    .line 352
    :catch_de
    move-exception v0

    #@df
    .line 356
    .local v0, e:Ljava/lang/InterruptedException;
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@e1
    const-string v3, "XTWiFiLP"

    #@e3
    const-string v4, "Constructor interrupted, retry"

    #@e5
    invoke-virtual {v2, v3, v4}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@e8
    goto :goto_d8
.end method

.method static synthetic access$000(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Lcom/qualcomm/lib/location/log/LocLog;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_handler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_zppAdaptor:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;

    #@2
    return-object v0
.end method

.method static synthetic access$102(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;Landroid/os/Handler;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    iput-object p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_handler:Landroid/os/Handler;

    #@2
    return-object p1
.end method

.method static synthetic access$1100(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    invoke-direct {p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->isZppRequestSentRecently()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1200(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Ljava/lang/Runnable;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->timeoutZppFix:Ljava/lang/Runnable;

    #@2
    return-object v0
.end method

.method static synthetic access$1300(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-wide v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_timeoutZppFix:J

    #@2
    return-wide v0
.end method

.method static synthetic access$1302(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;J)J
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    iput-wide p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_timeoutZppFix:J

    #@2
    return-wide p1
.end method

.method static synthetic access$1400(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_next_gtp_req_tx_id:I

    #@2
    return v0
.end method

.method static synthetic access$1402(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    iput p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_next_gtp_req_tx_id:I

    #@2
    return p1
.end method

.method static synthetic access$1404(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_next_gtp_req_tx_id:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    iput v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_next_gtp_req_tx_id:I

    #@6
    return v0
.end method

.method static synthetic access$1500(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_current_gtp_req_tx_id:I

    #@2
    return v0
.end method

.method static synthetic access$1502(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    iput p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_current_gtp_req_tx_id:I

    #@2
    return p1
.end method

.method static synthetic access$1600(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_ipcClient:Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@2
    return-object v0
.end method

.method static synthetic access$1602(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;)Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    iput-object p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_ipcClient:Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@2
    return-object p1
.end method

.method static synthetic access$1700(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    invoke-direct {p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->shouldSendFreeRideRequest()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1800(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 62
    invoke-direct {p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->handleZppFix()V

    #@3
    return-void
.end method

.method static synthetic access$1902(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;)Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    iput-object p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_xtwifi_tx_state:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@2
    return-object p1
.end method

.method static synthetic access$200(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Lcom/qualcomm/services/location/xtwifi/NlpPublicStatus;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_publicStatus:Lcom/qualcomm/services/location/xtwifi/NlpPublicStatus;

    #@2
    return-object v0
.end method

.method static synthetic access$2000(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 62
    invoke-direct {p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->handleFixReportFromXtwifi()V

    #@3
    return-void
.end method

.method static synthetic access$2100(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 62
    invoke-direct {p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->reInstallTbfTimer()V

    #@3
    return-void
.end method

.method static synthetic access$2200(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_log_level:I

    #@2
    return v0
.end method

.method static synthetic access$2202(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    iput p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_log_level:I

    #@2
    return p1
.end method

.method static synthetic access$2300(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-wide v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_minTimeBetweenFix:J

    #@2
    return-wide v0
.end method

.method static synthetic access$2302(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;J)J
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    iput-wide p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_minTimeBetweenFix:J

    #@2
    return-wide p1
.end method

.method static synthetic access$2400(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-wide v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_maxAgeForZppTransaction:J

    #@2
    return-wide v0
.end method

.method static synthetic access$2402(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;J)J
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    iput-wide p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_maxAgeForZppTransaction:J

    #@2
    return-wide p1
.end method

.method static synthetic access$2500(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_isEnabledFixReqOnFreeWiFiScan:Z

    #@2
    return v0
.end method

.method static synthetic access$2502(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_isEnabledFixReqOnFreeWiFiScan:Z

    #@2
    return p1
.end method

.method static synthetic access$2600(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-wide v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_minTimeBetweenFixOnFreeWiFiScan:J

    #@2
    return-wide v0
.end method

.method static synthetic access$2602(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;J)J
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    iput-wide p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_minTimeBetweenFixOnFreeWiFiScan:J

    #@2
    return-wide p1
.end method

.method static synthetic access$2700(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-wide v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_maxTimeBetweenWiFiScanAndRequest:J

    #@2
    return-wide v0
.end method

.method static synthetic access$2702(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;J)J
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    iput-wide p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_maxTimeBetweenWiFiScanAndRequest:J

    #@2
    return-wide p1
.end method

.method static synthetic access$2800(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 62
    invoke-direct {p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->handleTryConnect()V

    #@3
    return-void
.end method

.method static synthetic access$2900(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;Lcom/qualcomm/lib/location/mq_client/InPostcard;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->handleNewIpcMessage(Lcom/qualcomm/lib/location/mq_client/InPostcard;)V

    #@3
    return-void
.end method

.method static synthetic access$3000(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-wide v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_currentTimeBetweenFix:J

    #@2
    return-wide v0
.end method

.method static synthetic access$3002(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;J)J
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    iput-wide p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_currentTimeBetweenFix:J

    #@2
    return-wide p1
.end method

.method static synthetic access$3100(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Ljava/lang/Runnable;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->timeoutNextFixSessionDue:Ljava/lang/Runnable;

    #@2
    return-object v0
.end method

.method static synthetic access$3202(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;J)J
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    iput-wide p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_timestampLastZppReport:J

    #@2
    return-wide p1
.end method

.method static synthetic access$3300(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 62
    invoke-direct {p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->handleWiFiScanResult()V

    #@3
    return-void
.end method

.method static synthetic access$3400(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Ljava/util/concurrent/CountDownLatch;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_handlerThreadInitialized:Ljava/util/concurrent/CountDownLatch;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_internalState:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@2
    return-object v0
.end method

.method static synthetic access$402(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;)Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    iput-object p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_internalState:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@2
    return-object p1
.end method

.method static synthetic access$502(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;J)J
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    iput-wide p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_timestamp_last_fix_request_in_current_tracking_session:J

    #@2
    return-wide p1
.end method

.method static synthetic access$600(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Ljava/lang/Runnable;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->timeoutFixSession:Ljava/lang/Runnable;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-wide v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_timeoutFinalFix:J

    #@2
    return-wide v0
.end method

.method static synthetic access$702(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;J)J
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    iput-wide p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_timeoutFinalFix:J

    #@2
    return-wide p1
.end method

.method static synthetic access$800(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_isXTWWANEnabled:Z

    #@2
    return v0
.end method

.method static synthetic access$802(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_isXTWWANEnabled:Z

    #@2
    return p1
.end method

.method static synthetic access$900(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_isZppEnabled:Z

    #@2
    return v0
.end method

.method static synthetic access$902(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_isZppEnabled:Z

    #@2
    return p1
.end method

.method private handleFixReportFromXtwifi()V
    .registers 22

    #@0
    .prologue
    .line 880
    move-object/from16 v0, p0

    #@2
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_internalState:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@4
    sget-object v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;->IS_TRACKING_WAIT_FOR_FINAL_FIX:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@6
    if-eq v1, v2, :cond_29

    #@8
    .line 882
    move-object/from16 v0, p0

    #@a
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@c
    const-string v2, "XTWiFiLP"

    #@e
    new-instance v3, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v4, "handleFixReportFromXtwifi called in wrong state "

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    move-object/from16 v0, p0

    #@1b
    iget-object v4, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_internalState:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@1d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    invoke-virtual {v1, v2, v3}, Lcom/qualcomm/lib/location/log/LocLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@28
    .line 945
    :goto_28
    return-void

    #@29
    .line 886
    :cond_29
    invoke-direct/range {p0 .. p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->verifyWithZppAndReportXtwifiFix()V

    #@2c
    .line 888
    move-object/from16 v0, p0

    #@2e
    iget-boolean v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_has_reported_anything_in_current_fix_session:Z

    #@30
    if-nez v1, :cond_145

    #@32
    move-object/from16 v0, p0

    #@34
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_xtwifi_tx_state:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@36
    sget-object v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;->XFS_FINAL:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@38
    if-ne v1, v2, :cond_145

    #@3a
    invoke-direct/range {p0 .. p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->isZppFixGood()Z

    #@3d
    move-result v1

    #@3e
    if-eqz v1, :cond_145

    #@40
    .line 896
    :try_start_40
    move-object/from16 v0, p0

    #@42
    iget-boolean v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_isXTWWANEnabled:Z

    #@44
    if-eqz v1, :cond_bb

    #@46
    move-object/from16 v0, p0

    #@48
    iget-boolean v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_is_xtwwan_fix_with_lat_lon:Z

    #@4a
    if-eqz v1, :cond_bb

    #@4c
    move-object/from16 v0, p0

    #@4e
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_zppAdaptor:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;

    #@50
    if-eqz v1, :cond_bb

    #@52
    .line 897
    move-object/from16 v0, p0

    #@54
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@56
    const-string v2, "XTWiFiLP"

    #@58
    const-string v3, "Injecting XTWWAN fix"

    #@5a
    invoke-virtual {v1, v2, v3}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@5d
    .line 901
    const v12, -0x3c068000

    #@60
    .line 902
    .local v12, altitude_wrt_ellipsod:F
    const/4 v9, 0x0

    #@61
    .line 904
    .local v9, vertical_uncertainity:F
    const/4 v1, 0x0

    #@62
    new-array v13, v1, [I

    #@64
    .line 905
    .local v13, EMPTY_ARRAY:[I
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    #@67
    move-result-wide v1

    #@68
    move-object/from16 v0, p0

    #@6a
    iget-object v3, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownZppFix:Landroid/location/Location;

    #@6c
    invoke-virtual {v3}, Landroid/location/Location;->getElapsedRealtimeNanos()J

    #@6f
    move-result-wide v3

    #@70
    sub-long v16, v1, v3

    #@72
    .line 907
    .local v16, ageOfFix:J
    move-object/from16 v0, p0

    #@74
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownZppFix:Landroid/location/Location;

    #@76
    invoke-virtual {v1}, Landroid/location/Location;->getExtras()Landroid/os/Bundle;

    #@79
    move-result-object v18

    #@7a
    .line 908
    .local v18, bundle:Landroid/os/Bundle;
    if-eqz v18, :cond_90

    #@7c
    .line 910
    const-string v1, "BUNDLE_ALTITUDE_WRT_ELLIPSOID"

    #@7e
    const v2, -0x3c068000

    #@81
    move-object/from16 v0, v18

    #@83
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    #@86
    move-result v12

    #@87
    .line 911
    const-string v1, "BUNDLE_VERTICAL_UNCERTAINITY"

    #@89
    const/4 v2, 0x0

    #@8a
    move-object/from16 v0, v18

    #@8c
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    #@8f
    move-result v9

    #@90
    .line 914
    :cond_90
    move-object/from16 v0, p0

    #@92
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_zppAdaptor:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;

    #@94
    const/4 v2, 0x1

    #@95
    move-object/from16 v0, p0

    #@97
    iget-boolean v3, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_is_xtwwan_fix_with_lat_lon:Z

    #@99
    move-object/from16 v0, p0

    #@9b
    iget-object v4, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownZppFix:Landroid/location/Location;

    #@9d
    invoke-virtual {v4}, Landroid/location/Location;->getLatitude()D

    #@a0
    move-result-wide v4

    #@a1
    move-object/from16 v0, p0

    #@a3
    iget-object v6, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownZppFix:Landroid/location/Location;

    #@a5
    invoke-virtual {v6}, Landroid/location/Location;->getLongitude()D

    #@a8
    move-result-wide v6

    #@a9
    move-object/from16 v0, p0

    #@ab
    iget-object v8, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownZppFix:Landroid/location/Location;

    #@ad
    invoke-virtual {v8}, Landroid/location/Location;->getAccuracy()F

    #@b0
    move-result v8

    #@b1
    const-wide/32 v10, 0xf4240

    #@b4
    div-long v10, v16, v10

    #@b6
    move-object v14, v13

    #@b7
    move-object v15, v13

    #@b8
    invoke-virtual/range {v1 .. v15}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->injectFix(IZDDFFJF[I[I[I)Z

    #@bb
    .line 920
    .end local v9           #vertical_uncertainity:F
    .end local v12           #altitude_wrt_ellipsod:F
    .end local v13           #EMPTY_ARRAY:[I
    .end local v16           #ageOfFix:J
    .end local v18           #bundle:Landroid/os/Bundle;
    :cond_bb
    move-object/from16 v0, p0

    #@bd
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@bf
    const-string v2, "XTWiFiLP"

    #@c1
    new-instance v3, Ljava/lang/StringBuilder;

    #@c3
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@c6
    const-string v4, "Reporting ZPP fix ("

    #@c8
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cb
    move-result-object v3

    #@cc
    move-object/from16 v0, p0

    #@ce
    iget-object v4, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownZppFix:Landroid/location/Location;

    #@d0
    invoke-virtual {v4}, Landroid/location/Location;->getLatitude()D

    #@d3
    move-result-wide v4

    #@d4
    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v3

    #@d8
    const-string v4, ", "

    #@da
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v3

    #@de
    move-object/from16 v0, p0

    #@e0
    iget-object v4, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownZppFix:Landroid/location/Location;

    #@e2
    invoke-virtual {v4}, Landroid/location/Location;->getLongitude()D

    #@e5
    move-result-wide v4

    #@e6
    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    #@e9
    move-result-object v3

    #@ea
    const-string v4, "), unc "

    #@ec
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ef
    move-result-object v3

    #@f0
    move-object/from16 v0, p0

    #@f2
    iget-object v4, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownZppFix:Landroid/location/Location;

    #@f4
    invoke-virtual {v4}, Landroid/location/Location;->getAccuracy()F

    #@f7
    move-result v4

    #@f8
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@fb
    move-result-object v3

    #@fc
    const-string v4, " time (ms) "

    #@fe
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@101
    move-result-object v3

    #@102
    move-object/from16 v0, p0

    #@104
    iget-object v4, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownZppFix:Landroid/location/Location;

    #@106
    invoke-virtual {v4}, Landroid/location/Location;->getElapsedRealtimeNanos()J

    #@109
    move-result-wide v4

    #@10a
    const-wide/32 v6, 0xf4240

    #@10d
    div-long/2addr v4, v6

    #@10e
    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@111
    move-result-object v3

    #@112
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@115
    move-result-object v3

    #@116
    invoke-virtual {v1, v2, v3}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@119
    .line 924
    new-instance v20, Landroid/location/Location;

    #@11b
    move-object/from16 v0, p0

    #@11d
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix:Landroid/location/Location;

    #@11f
    move-object/from16 v0, v20

    #@121
    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    #@124
    .line 925
    .local v20, fuzzifiedLocation:Landroid/location/Location;
    move-object/from16 v0, p0

    #@126
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownZppFix:Landroid/location/Location;

    #@128
    const-string v2, "noGPSLocation"

    #@12a
    move-object/from16 v0, v20

    #@12c
    invoke-virtual {v1, v2, v0}, Landroid/location/Location;->setExtraLocation(Ljava/lang/String;Landroid/location/Location;)V

    #@12f
    .line 927
    move-object/from16 v0, p0

    #@131
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_LocationManager:Landroid/location/ILocationManager;

    #@133
    new-instance v2, Landroid/location/Location;

    #@135
    move-object/from16 v0, p0

    #@137
    iget-object v3, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownZppFix:Landroid/location/Location;

    #@139
    invoke-direct {v2, v3}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    #@13c
    const/4 v3, 0x0

    #@13d
    invoke-interface {v1, v2, v3}, Landroid/location/ILocationManager;->reportLocation(Landroid/location/Location;Z)V
    :try_end_140
    .catch Landroid/os/RemoteException; {:try_start_40 .. :try_end_140} :catch_15b

    #@140
    .line 934
    .end local v20           #fuzzifiedLocation:Landroid/location/Location;
    :goto_140
    const/4 v1, 0x1

    #@141
    move-object/from16 v0, p0

    #@143
    iput-boolean v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_has_reported_anything_in_current_fix_session:Z

    #@145
    .line 937
    :cond_145
    move-object/from16 v0, p0

    #@147
    iget-boolean v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_has_reported_anything_in_current_fix_session:Z

    #@149
    if-nez v1, :cond_156

    #@14b
    .line 939
    move-object/from16 v0, p0

    #@14d
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@14f
    const-string v2, "XTWiFiLP"

    #@151
    const-string v3, "Reporting Nothing"

    #@153
    invoke-virtual {v1, v2, v3}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@156
    .line 944
    :cond_156
    invoke-direct/range {p0 .. p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->removeLocationUpdateListenerForWifi()V

    #@159
    goto/16 :goto_28

    #@15b
    .line 929
    :catch_15b
    move-exception v19

    #@15c
    .line 931
    .local v19, e:Landroid/os/RemoteException;
    move-object/from16 v0, p0

    #@15e
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@160
    const-string v2, "XTWiFiLP"

    #@162
    const-string v3, "Exception in reporting location to LocationManagerService (1)"

    #@164
    invoke-virtual {v1, v2, v3}, Lcom/qualcomm/lib/location/log/LocLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@167
    goto :goto_140
.end method

.method private handleNewIpcMessage(Lcom/qualcomm/lib/location/mq_client/InPostcard;)V
    .registers 26
    .parameter "in_card"

    #@0
    .prologue
    .line 1088
    :try_start_0
    const-string v20, "FROM"

    #@2
    move-object/from16 v0, p1

    #@4
    move-object/from16 v1, v20

    #@6
    invoke-virtual {v0, v1}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v15

    #@a
    .line 1090
    .local v15, msg_from:Ljava/lang/String;
    const-string v20, "RESP"

    #@c
    move-object/from16 v0, p1

    #@e
    move-object/from16 v1, v20

    #@10
    invoke-virtual {v0, v1}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@13
    move-result-object v16

    #@14
    .line 1091
    .local v16, msg_resp:Ljava/lang/String;
    const-string v20, "TX-ID"

    #@16
    move-object/from16 v0, p1

    #@18
    move-object/from16 v1, v20

    #@1a
    invoke-virtual {v0, v1}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->getInt32(Ljava/lang/String;)I

    #@1d
    move-result v17

    #@1e
    .line 1092
    .local v17, msg_tx_id:I
    const-string v20, "PRELIMINARY-FIX"

    #@20
    const/16 v21, 0x0

    #@22
    move-object/from16 v0, p1

    #@24
    move-object/from16 v1, v20

    #@26
    move/from16 v2, v21

    #@28
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->getBoolDefault(Ljava/lang/String;Z)Z

    #@2b
    move-result v9

    #@2c
    .line 1096
    .local v9, is_preliminary_fix:Z
    move-object/from16 v0, p0

    #@2e
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@30
    move-object/from16 v20, v0

    #@32
    const-string v21, "XTWiFiLP"

    #@34
    new-instance v22, Ljava/lang/StringBuilder;

    #@36
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v23, "FROM: "

    #@3b
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v22

    #@3f
    move-object/from16 v0, v22

    #@41
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v22

    #@45
    const-string v23, "RESP:"

    #@47
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v22

    #@4b
    move-object/from16 v0, v22

    #@4d
    move-object/from16 v1, v16

    #@4f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v22

    #@53
    const-string v23, ", TX-ID:"

    #@55
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v22

    #@59
    move-object/from16 v0, v22

    #@5b
    move/from16 v1, v17

    #@5d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@60
    move-result-object v22

    #@61
    const-string v23, ", Preliminary: "

    #@63
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v22

    #@67
    move-object/from16 v0, v22

    #@69
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v22

    #@6d
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@70
    move-result-object v22

    #@71
    invoke-virtual/range {v20 .. v22}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@74
    .line 1098
    const-string v20, "POSITION"

    #@76
    move-object/from16 v0, v16

    #@78
    move-object/from16 v1, v20

    #@7a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@7d
    move-result v20

    #@7e
    if-nez v20, :cond_a3

    #@80
    .line 1100
    move-object/from16 v0, p0

    #@82
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@84
    move-object/from16 v20, v0

    #@86
    const-string v21, "XTWiFiLP"

    #@88
    new-instance v22, Ljava/lang/StringBuilder;

    #@8a
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@8d
    const-string v23, "handleNewIpcMessage unknown response msg: "

    #@8f
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v22

    #@93
    move-object/from16 v0, v22

    #@95
    move-object/from16 v1, v16

    #@97
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v22

    #@9b
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9e
    move-result-object v22

    #@9f
    invoke-virtual/range {v20 .. v22}, Lcom/qualcomm/lib/location/log/LocLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@a2
    .line 1294
    .end local v9           #is_preliminary_fix:Z
    .end local v15           #msg_from:Ljava/lang/String;
    .end local v16           #msg_resp:Ljava/lang/String;
    .end local v17           #msg_tx_id:I
    :cond_a2
    :goto_a2
    return-void

    #@a3
    .line 1104
    .restart local v9       #is_preliminary_fix:Z
    .restart local v15       #msg_from:Ljava/lang/String;
    .restart local v16       #msg_resp:Ljava/lang/String;
    .restart local v17       #msg_tx_id:I
    :cond_a3
    move-object/from16 v0, p0

    #@a5
    iget v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_current_gtp_req_tx_id:I

    #@a7
    move/from16 v20, v0

    #@a9
    move/from16 v0, v17

    #@ab
    move/from16 v1, v20

    #@ad
    if-eq v0, v1, :cond_f1

    #@af
    .line 1107
    move-object/from16 v0, p0

    #@b1
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@b3
    move-object/from16 v20, v0

    #@b5
    const-string v21, "XTWiFiLP"

    #@b7
    new-instance v22, Ljava/lang/StringBuilder;

    #@b9
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@bc
    const-string v23, "handleNewIpcMessage TX ID mismatch. was expecting "

    #@be
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v22

    #@c2
    move-object/from16 v0, p0

    #@c4
    iget v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_current_gtp_req_tx_id:I

    #@c6
    move/from16 v23, v0

    #@c8
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@cb
    move-result-object v22

    #@cc
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cf
    move-result-object v22

    #@d0
    invoke-virtual/range {v20 .. v22}, Lcom/qualcomm/lib/location/log/LocLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_d3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_d3} :catch_d4
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_d3} :catch_128

    #@d3
    goto :goto_a2

    #@d4
    .line 1282
    .end local v9           #is_preliminary_fix:Z
    .end local v15           #msg_from:Ljava/lang/String;
    .end local v16           #msg_resp:Ljava/lang/String;
    .end local v17           #msg_tx_id:I
    :catch_d4
    move-exception v6

    #@d5
    .line 1284
    .local v6, e:Ljava/io/IOException;
    move-object/from16 v0, p0

    #@d7
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@d9
    move-object/from16 v20, v0

    #@db
    const-string v21, "XTWiFiLP"

    #@dd
    const-string v22, "handleNewIpcMessage cannot receive IPC message"

    #@df
    invoke-virtual/range {v20 .. v22}, Lcom/qualcomm/lib/location/log/LocLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@e2
    .line 1285
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    #@e5
    .line 1288
    move-object/from16 v0, p0

    #@e7
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_handler:Landroid/os/Handler;

    #@e9
    move-object/from16 v20, v0

    #@eb
    const/16 v21, 0x66

    #@ed
    invoke-virtual/range {v20 .. v21}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@f0
    goto :goto_a2

    #@f1
    .line 1111
    .end local v6           #e:Ljava/io/IOException;
    .restart local v9       #is_preliminary_fix:Z
    .restart local v15       #msg_from:Ljava/lang/String;
    .restart local v16       #msg_resp:Ljava/lang/String;
    .restart local v17       #msg_tx_id:I
    :cond_f1
    :try_start_f1
    sget-object v20, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$7;->$SwitchMap$com$qualcomm$services$location$xtwifi$XTWiFiLocationProvider$InternalState:[I

    #@f3
    move-object/from16 v0, p0

    #@f5
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_internalState:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@f7
    move-object/from16 v21, v0

    #@f9
    invoke-virtual/range {v21 .. v21}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;->ordinal()I

    #@fc
    move-result v21

    #@fd
    aget v20, v20, v21

    #@ff
    packed-switch v20, :pswitch_data_404

    #@102
    .line 1118
    move-object/from16 v0, p0

    #@104
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@106
    move-object/from16 v20, v0

    #@108
    const-string v21, "XTWiFiLP"

    #@10a
    new-instance v22, Ljava/lang/StringBuilder;

    #@10c
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@10f
    const-string v23, "handleNewIpcMessage fired at wrong state "

    #@111
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@114
    move-result-object v22

    #@115
    move-object/from16 v0, p0

    #@117
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_internalState:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@119
    move-object/from16 v23, v0

    #@11b
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@11e
    move-result-object v22

    #@11f
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@122
    move-result-object v22

    #@123
    invoke-virtual/range {v20 .. v22}, Lcom/qualcomm/lib/location/log/LocLog;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_126
    .catch Ljava/io/IOException; {:try_start_f1 .. :try_end_126} :catch_d4
    .catch Ljava/util/NoSuchElementException; {:try_start_f1 .. :try_end_126} :catch_128

    #@126
    goto/16 :goto_a2

    #@128
    .line 1290
    .end local v9           #is_preliminary_fix:Z
    .end local v15           #msg_from:Ljava/lang/String;
    .end local v16           #msg_resp:Ljava/lang/String;
    .end local v17           #msg_tx_id:I
    :catch_128
    move-exception v6

    #@129
    .line 1292
    .local v6, e:Ljava/util/NoSuchElementException;
    move-object/from16 v0, p0

    #@12b
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@12d
    move-object/from16 v20, v0

    #@12f
    const-string v21, "XTWiFiLP"

    #@131
    const-string v22, "handleNewIpcMessage missing information in IPC message"

    #@133
    invoke-virtual/range {v20 .. v22}, Lcom/qualcomm/lib/location/log/LocLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@136
    goto/16 :goto_a2

    #@138
    .line 1122
    .end local v6           #e:Ljava/util/NoSuchElementException;
    .restart local v9       #is_preliminary_fix:Z
    .restart local v15       #msg_from:Ljava/lang/String;
    .restart local v16       #msg_resp:Ljava/lang/String;
    .restart local v17       #msg_tx_id:I
    :pswitch_138
    :try_start_138
    const-string v20, "XTWiFi-PE"

    #@13a
    move-object/from16 v0, v20

    #@13c
    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13f
    move-result v20

    #@140
    if-eqz v20, :cond_2ca

    #@142
    .line 1124
    if-eqz v9, :cond_260

    #@144
    .line 1128
    move-object/from16 v0, p0

    #@146
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_xtwifi_tx_state:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@148
    move-object/from16 v20, v0

    #@14a
    sget-object v21, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;->XFS_IDLE_OR_WAITING:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@14c
    move-object/from16 v0, v20

    #@14e
    move-object/from16 v1, v21

    #@150
    if-eq v0, v1, :cond_176

    #@152
    .line 1130
    move-object/from16 v0, p0

    #@154
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@156
    move-object/from16 v20, v0

    #@158
    const-string v21, "XTWiFiLP"

    #@15a
    new-instance v22, Ljava/lang/StringBuilder;

    #@15c
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@15f
    const-string v23, "handleNewIpcMessage Receive preliminary fix at wrong state "

    #@161
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@164
    move-result-object v22

    #@165
    move-object/from16 v0, p0

    #@167
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_xtwifi_tx_state:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@169
    move-object/from16 v23, v0

    #@16b
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16e
    move-result-object v22

    #@16f
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@172
    move-result-object v22

    #@173
    invoke-virtual/range {v20 .. v22}, Lcom/qualcomm/lib/location/log/LocLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@176
    .line 1133
    :cond_176
    sget-object v20, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;->XFS_PRELIMINARY:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@178
    move-object/from16 v0, v20

    #@17a
    move-object/from16 v1, p0

    #@17c
    iput-object v0, v1, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_xtwifi_tx_state:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;
    :try_end_17e
    .catch Ljava/io/IOException; {:try_start_138 .. :try_end_17e} :catch_d4
    .catch Ljava/util/NoSuchElementException; {:try_start_138 .. :try_end_17e} :catch_128

    #@17e
    .line 1149
    :goto_17e
    :try_start_17e
    const-string v20, "LATITUDE_DEG"

    #@180
    move-object/from16 v0, p1

    #@182
    move-object/from16 v1, v20

    #@184
    invoke-virtual {v0, v1}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->getDouble(Ljava/lang/String;)D

    #@187
    move-result-wide v10

    #@188
    .line 1150
    .local v10, lat_deg:D
    const-string v20, "LONGITUDE_DEG"

    #@18a
    move-object/from16 v0, p1

    #@18c
    move-object/from16 v1, v20

    #@18e
    invoke-virtual {v0, v1}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->getDouble(Ljava/lang/String;)D

    #@191
    move-result-wide v12

    #@192
    .line 1151
    .local v12, lon_deg:D
    const-string v20, "HOR_UNC_M"

    #@194
    move-object/from16 v0, p1

    #@196
    move-object/from16 v1, v20

    #@198
    invoke-virtual {v0, v1}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->getFloat(Ljava/lang/String;)F

    #@19b
    move-result v20

    #@19c
    move/from16 v0, v20

    #@19e
    float-to-double v7, v0

    #@19f
    .line 1153
    .local v7, hor_unc_meter:D
    move-object/from16 v0, p0

    #@1a1
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix:Landroid/location/Location;

    #@1a3
    move-object/from16 v20, v0

    #@1a5
    invoke-virtual/range {v20 .. v20}, Landroid/location/Location;->reset()V

    #@1a8
    .line 1154
    move-object/from16 v0, p0

    #@1aa
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix:Landroid/location/Location;

    #@1ac
    move-object/from16 v20, v0

    #@1ae
    const-string v21, "network"

    #@1b0
    invoke-virtual/range {v20 .. v21}, Landroid/location/Location;->setProvider(Ljava/lang/String;)V

    #@1b3
    .line 1155
    move-object/from16 v0, p0

    #@1b5
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix:Landroid/location/Location;

    #@1b7
    move-object/from16 v20, v0

    #@1b9
    move-object/from16 v0, v20

    #@1bb
    invoke-virtual {v0, v10, v11}, Landroid/location/Location;->setLatitude(D)V

    #@1be
    .line 1156
    move-object/from16 v0, p0

    #@1c0
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix:Landroid/location/Location;

    #@1c2
    move-object/from16 v20, v0

    #@1c4
    move-object/from16 v0, v20

    #@1c6
    invoke-virtual {v0, v12, v13}, Landroid/location/Location;->setLongitude(D)V

    #@1c9
    .line 1157
    move-object/from16 v0, p0

    #@1cb
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix:Landroid/location/Location;

    #@1cd
    move-object/from16 v20, v0

    #@1cf
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@1d2
    move-result-wide v21

    #@1d3
    invoke-virtual/range {v20 .. v22}, Landroid/location/Location;->setTime(J)V

    #@1d6
    .line 1158
    move-object/from16 v0, p0

    #@1d8
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix:Landroid/location/Location;

    #@1da
    move-object/from16 v20, v0

    #@1dc
    double-to-float v0, v7

    #@1dd
    move/from16 v21, v0

    #@1df
    invoke-virtual/range {v20 .. v21}, Landroid/location/Location;->setAccuracy(F)V

    #@1e2
    .line 1161
    move-object/from16 v0, p0

    #@1e4
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix:Landroid/location/Location;

    #@1e6
    move-object/from16 v20, v0

    #@1e8
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    #@1eb
    move-result-wide v21

    #@1ec
    invoke-virtual/range {v20 .. v22}, Landroid/location/Location;->setElapsedRealtimeNanos(J)V

    #@1ef
    .line 1169
    const/16 v20, 0x1

    #@1f1
    move/from16 v0, v20

    #@1f3
    move-object/from16 v1, p0

    #@1f5
    iput-boolean v0, v1, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_is_xtwifi_fix_with_lat_lon:Z
    :try_end_1f7
    .catch Ljava/util/NoSuchElementException; {:try_start_17e .. :try_end_1f7} :catch_2aa
    .catch Ljava/io/IOException; {:try_start_17e .. :try_end_1f7} :catch_d4

    #@1f7
    .line 1183
    .end local v7           #hor_unc_meter:D
    .end local v10           #lat_deg:D
    .end local v12           #lon_deg:D
    :goto_1f7
    const/16 v20, 0x0

    #@1f9
    :try_start_1f9
    move-object/from16 v0, v20

    #@1fb
    move-object/from16 v1, p0

    #@1fd
    iput-object v0, v1, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix_mac_array:[I

    #@1ff
    .line 1184
    const/16 v20, 0x0

    #@201
    move-object/from16 v0, v20

    #@203
    move-object/from16 v1, p0

    #@205
    iput-object v0, v1, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix_rssi_array:[I

    #@207
    .line 1185
    const/16 v20, 0x0

    #@209
    move-object/from16 v0, v20

    #@20b
    move-object/from16 v1, p0

    #@20d
    iput-object v0, v1, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix_channel_array:[I

    #@20f
    .line 1189
    const-string v20, "SCAN_RESULT_MAC_ADDRESS"

    #@211
    move-object/from16 v0, p1

    #@213
    move-object/from16 v1, v20

    #@215
    invoke-virtual {v0, v1}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->getArrayUInt8(Ljava/lang/String;)[I

    #@218
    move-result-object v14

    #@219
    .line 1190
    .local v14, mac_int_array:[I
    const-string v20, "SCAN_RESULT_RSSI"

    #@21b
    move-object/from16 v0, p1

    #@21d
    move-object/from16 v1, v20

    #@21f
    invoke-virtual {v0, v1}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->getArrayInt16(Ljava/lang/String;)[I

    #@222
    move-result-object v18

    #@223
    .line 1191
    .local v18, rssi_array:[I
    const-string v20, "SCAN_RESULT_CHANNEL"

    #@225
    move-object/from16 v0, p1

    #@227
    move-object/from16 v1, v20

    #@229
    invoke-virtual {v0, v1}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->getArrayInt16(Ljava/lang/String;)[I

    #@22c
    move-result-object v5

    #@22d
    .line 1193
    .local v5, channel_array:[I
    move-object/from16 v0, p0

    #@22f
    iput-object v14, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix_mac_array:[I

    #@231
    .line 1194
    move-object/from16 v0, v18

    #@233
    move-object/from16 v1, p0

    #@235
    iput-object v0, v1, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix_rssi_array:[I

    #@237
    .line 1195
    move-object/from16 v0, p0

    #@239
    iput-object v5, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix_channel_array:[I
    :try_end_23b
    .catch Ljava/util/NoSuchElementException; {:try_start_1f9 .. :try_end_23b} :catch_2ba
    .catch Ljava/io/IOException; {:try_start_1f9 .. :try_end_23b} :catch_d4

    #@23b
    .line 1257
    .end local v5           #channel_array:[I
    .end local v14           #mac_int_array:[I
    .end local v18           #rssi_array:[I
    :cond_23b
    :goto_23b
    :try_start_23b
    move-object/from16 v0, p0

    #@23d
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_internalState:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@23f
    move-object/from16 v20, v0

    #@241
    sget-object v21, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;->IS_TRACKING_WAIT_FOR_ZPP_FIX:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@243
    move-object/from16 v0, v20

    #@245
    move-object/from16 v1, v21

    #@247
    if-ne v0, v1, :cond_3ed

    #@249
    .line 1259
    move-object/from16 v0, p0

    #@24b
    iget-boolean v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_got_xtwwan_response:Z

    #@24d
    move/from16 v20, v0

    #@24f
    if-nez v20, :cond_3e8

    #@251
    .line 1261
    move-object/from16 v0, p0

    #@253
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@255
    move-object/from16 v20, v0

    #@257
    const-string v21, "XTWiFiLP"

    #@259
    const-string v22, "handleNewIpcMessage Wait for XTWWAN / Zpp fix to come"

    #@25b
    invoke-virtual/range {v20 .. v22}, Lcom/qualcomm/lib/location/log/LocLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@25e
    goto/16 :goto_a2

    #@260
    .line 1138
    :cond_260
    move-object/from16 v0, p0

    #@262
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_xtwifi_tx_state:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@264
    move-object/from16 v20, v0

    #@266
    sget-object v21, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;->XFS_IDLE_OR_WAITING:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@268
    move-object/from16 v0, v20

    #@26a
    move-object/from16 v1, v21

    #@26c
    if-eq v0, v1, :cond_2a0

    #@26e
    move-object/from16 v0, p0

    #@270
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_xtwifi_tx_state:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@272
    move-object/from16 v20, v0

    #@274
    sget-object v21, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;->XFS_PRELIMINARY:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@276
    move-object/from16 v0, v20

    #@278
    move-object/from16 v1, v21

    #@27a
    if-eq v0, v1, :cond_2a0

    #@27c
    .line 1141
    move-object/from16 v0, p0

    #@27e
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@280
    move-object/from16 v20, v0

    #@282
    const-string v21, "XTWiFiLP"

    #@284
    new-instance v22, Ljava/lang/StringBuilder;

    #@286
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@289
    const-string v23, "handleNewIpcMessage Receive final fix at wrong state "

    #@28b
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28e
    move-result-object v22

    #@28f
    move-object/from16 v0, p0

    #@291
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_xtwifi_tx_state:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@293
    move-object/from16 v23, v0

    #@295
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@298
    move-result-object v22

    #@299
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29c
    move-result-object v22

    #@29d
    invoke-virtual/range {v20 .. v22}, Lcom/qualcomm/lib/location/log/LocLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@2a0
    .line 1144
    :cond_2a0
    sget-object v20, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;->XFS_FINAL:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@2a2
    move-object/from16 v0, v20

    #@2a4
    move-object/from16 v1, p0

    #@2a6
    iput-object v0, v1, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_xtwifi_tx_state:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@2a8
    goto/16 :goto_17e

    #@2aa
    .line 1171
    :catch_2aa
    move-exception v6

    #@2ab
    .line 1177
    .restart local v6       #e:Ljava/util/NoSuchElementException;
    move-object/from16 v0, p0

    #@2ad
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@2af
    move-object/from16 v20, v0

    #@2b1
    const-string v21, "XTWiFiLP"

    #@2b3
    const-string v22, "No fix from XTWiFi-PE"

    #@2b5
    invoke-virtual/range {v20 .. v22}, Lcom/qualcomm/lib/location/log/LocLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@2b8
    goto/16 :goto_1f7

    #@2ba
    .line 1197
    .end local v6           #e:Ljava/util/NoSuchElementException;
    :catch_2ba
    move-exception v6

    #@2bb
    .line 1200
    .restart local v6       #e:Ljava/util/NoSuchElementException;
    move-object/from16 v0, p0

    #@2bd
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@2bf
    move-object/from16 v20, v0

    #@2c1
    const-string v21, "XTWiFiLP"

    #@2c3
    const-string v22, "No WiFi scan result from XTWiFi-PE"

    #@2c5
    invoke-virtual/range {v20 .. v22}, Lcom/qualcomm/lib/location/log/LocLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@2c8
    goto/16 :goto_23b

    #@2ca
    .line 1203
    .end local v6           #e:Ljava/util/NoSuchElementException;
    :cond_2ca
    const-string v20, "XTWWAN-PE"

    #@2cc
    move-object/from16 v0, v20

    #@2ce
    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2d1
    move-result v20

    #@2d2
    if-eqz v20, :cond_23b

    #@2d4
    .line 1205
    const/16 v20, 0x1

    #@2d6
    move/from16 v0, v20

    #@2d8
    move-object/from16 v1, p0

    #@2da
    iput-boolean v0, v1, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_got_xtwwan_response:Z
    :try_end_2dc
    .catch Ljava/io/IOException; {:try_start_23b .. :try_end_2dc} :catch_d4
    .catch Ljava/util/NoSuchElementException; {:try_start_23b .. :try_end_2dc} :catch_128

    #@2dc
    .line 1208
    :try_start_2dc
    move-object/from16 v0, p0

    #@2de
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_internalState:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@2e0
    move-object/from16 v20, v0

    #@2e2
    sget-object v21, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;->IS_TRACKING_WAIT_FOR_ZPP_FIX:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@2e4
    move-object/from16 v0, v20

    #@2e6
    move-object/from16 v1, v21

    #@2e8
    if-ne v0, v1, :cond_3b3

    #@2ea
    .line 1210
    const-string v20, "LATITUDE_DEG"

    #@2ec
    move-object/from16 v0, p1

    #@2ee
    move-object/from16 v1, v20

    #@2f0
    invoke-virtual {v0, v1}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->getDouble(Ljava/lang/String;)D

    #@2f3
    move-result-wide v10

    #@2f4
    .line 1211
    .restart local v10       #lat_deg:D
    const-string v20, "LONGITUDE_DEG"

    #@2f6
    move-object/from16 v0, p1

    #@2f8
    move-object/from16 v1, v20

    #@2fa
    invoke-virtual {v0, v1}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->getDouble(Ljava/lang/String;)D

    #@2fd
    move-result-wide v12

    #@2fe
    .line 1212
    .restart local v12       #lon_deg:D
    const-string v20, "HOR_UNC_M"

    #@300
    move-object/from16 v0, p1

    #@302
    move-object/from16 v1, v20

    #@304
    invoke-virtual {v0, v1}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->getFloat(Ljava/lang/String;)F

    #@307
    move-result v7

    #@308
    .line 1214
    .local v7, hor_unc_meter:F
    move-object/from16 v0, p0

    #@30a
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownZppFix:Landroid/location/Location;

    #@30c
    move-object/from16 v20, v0

    #@30e
    invoke-virtual/range {v20 .. v20}, Landroid/location/Location;->reset()V

    #@311
    .line 1215
    move-object/from16 v0, p0

    #@313
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownZppFix:Landroid/location/Location;

    #@315
    move-object/from16 v20, v0

    #@317
    const-string v21, "network"

    #@319
    invoke-virtual/range {v20 .. v21}, Landroid/location/Location;->setProvider(Ljava/lang/String;)V

    #@31c
    .line 1216
    move-object/from16 v0, p0

    #@31e
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownZppFix:Landroid/location/Location;

    #@320
    move-object/from16 v20, v0

    #@322
    move-object/from16 v0, v20

    #@324
    invoke-virtual {v0, v10, v11}, Landroid/location/Location;->setLatitude(D)V

    #@327
    .line 1217
    move-object/from16 v0, p0

    #@329
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownZppFix:Landroid/location/Location;

    #@32b
    move-object/from16 v20, v0

    #@32d
    move-object/from16 v0, v20

    #@32f
    invoke-virtual {v0, v12, v13}, Landroid/location/Location;->setLongitude(D)V

    #@332
    .line 1218
    move-object/from16 v0, p0

    #@334
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownZppFix:Landroid/location/Location;

    #@336
    move-object/from16 v20, v0

    #@338
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@33b
    move-result-wide v21

    #@33c
    invoke-virtual/range {v20 .. v22}, Landroid/location/Location;->setTime(J)V

    #@33f
    .line 1219
    move-object/from16 v0, p0

    #@341
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownZppFix:Landroid/location/Location;

    #@343
    move-object/from16 v20, v0

    #@345
    move-object/from16 v0, v20

    #@347
    invoke-virtual {v0, v7}, Landroid/location/Location;->setAccuracy(F)V

    #@34a
    .line 1222
    move-object/from16 v0, p0

    #@34c
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownZppFix:Landroid/location/Location;

    #@34e
    move-object/from16 v20, v0

    #@350
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    #@353
    move-result-wide v21

    #@354
    invoke-virtual/range {v20 .. v22}, Landroid/location/Location;->setElapsedRealtimeNanos(J)V

    #@357
    .line 1223
    const/16 v20, 0x1

    #@359
    move/from16 v0, v20

    #@35b
    move-object/from16 v1, p0

    #@35d
    iput-boolean v0, v1, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_is_xtwwan_fix_with_lat_lon:Z
    :try_end_35f
    .catch Ljava/util/NoSuchElementException; {:try_start_2dc .. :try_end_35f} :catch_3d8
    .catch Ljava/io/IOException; {:try_start_2dc .. :try_end_35f} :catch_d4

    #@35f
    .line 1233
    .end local v7           #hor_unc_meter:F
    .end local v10           #lat_deg:D
    .end local v12           #lon_deg:D
    :goto_35f
    :try_start_35f
    move-object/from16 v0, p0

    #@361
    iget-boolean v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_is_xtwwan_fix_with_lat_lon:Z

    #@363
    move/from16 v20, v0
    :try_end_365
    .catch Ljava/io/IOException; {:try_start_35f .. :try_end_365} :catch_d4
    .catch Ljava/util/NoSuchElementException; {:try_start_35f .. :try_end_365} :catch_128

    #@365
    if-eqz v20, :cond_23b

    #@367
    .line 1238
    :try_start_367
    const-string v20, "VER_UNC_M"

    #@369
    move-object/from16 v0, p1

    #@36b
    move-object/from16 v1, v20

    #@36d
    invoke-virtual {v0, v1}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->getFloat(Ljava/lang/String;)F

    #@370
    move-result v19

    #@371
    .line 1239
    .local v19, vert_unc_meter:F
    const-string v20, "ALT_ELP_M"

    #@373
    move-object/from16 v0, p1

    #@375
    move-object/from16 v1, v20

    #@377
    invoke-virtual {v0, v1}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->getFloat(Ljava/lang/String;)F

    #@37a
    move-result v3

    #@37b
    .line 1241
    .local v3, altitude_wrt_ellipsoid:F
    new-instance v4, Landroid/os/Bundle;

    #@37d
    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    #@380
    .line 1242
    .local v4, bundle:Landroid/os/Bundle;
    const/16 v20, 0x0

    #@382
    cmpl-float v20, v3, v20

    #@384
    if-eqz v20, :cond_23b

    #@386
    .line 1244
    const-string v20, "BUNDLE_VERTICAL_UNCERTAINITY"

    #@388
    move-object/from16 v0, v20

    #@38a
    move/from16 v1, v19

    #@38c
    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    #@38f
    .line 1245
    const-string v20, "BUNDLE_ALTITUDE_WRT_ELLIPSOID"

    #@391
    move-object/from16 v0, v20

    #@393
    invoke-virtual {v4, v0, v3}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    #@396
    .line 1246
    move-object/from16 v0, p0

    #@398
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownZppFix:Landroid/location/Location;

    #@39a
    move-object/from16 v20, v0

    #@39c
    move-object/from16 v0, v20

    #@39e
    invoke-virtual {v0, v4}, Landroid/location/Location;->setExtras(Landroid/os/Bundle;)V
    :try_end_3a1
    .catch Ljava/util/NoSuchElementException; {:try_start_367 .. :try_end_3a1} :catch_3a3
    .catch Ljava/io/IOException; {:try_start_367 .. :try_end_3a1} :catch_d4

    #@3a1
    goto/16 :goto_23b

    #@3a3
    .line 1249
    .end local v3           #altitude_wrt_ellipsoid:F
    .end local v4           #bundle:Landroid/os/Bundle;
    .end local v19           #vert_unc_meter:F
    :catch_3a3
    move-exception v6

    #@3a4
    .line 1251
    .restart local v6       #e:Ljava/util/NoSuchElementException;
    :try_start_3a4
    move-object/from16 v0, p0

    #@3a6
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@3a8
    move-object/from16 v20, v0

    #@3aa
    const-string v21, "XTWiFiLP"

    #@3ac
    const-string v22, "Exception in altitude/ vertical uncertainity for XTWWAN fix"

    #@3ae
    invoke-virtual/range {v20 .. v22}, Lcom/qualcomm/lib/location/log/LocLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3b1
    .catch Ljava/io/IOException; {:try_start_3a4 .. :try_end_3b1} :catch_d4
    .catch Ljava/util/NoSuchElementException; {:try_start_3a4 .. :try_end_3b1} :catch_128

    #@3b1
    goto/16 :goto_23b

    #@3b3
    .line 1226
    .end local v6           #e:Ljava/util/NoSuchElementException;
    :cond_3b3
    :try_start_3b3
    move-object/from16 v0, p0

    #@3b5
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@3b7
    move-object/from16 v20, v0

    #@3b9
    const-string v21, "XTWiFiLP"

    #@3bb
    new-instance v22, Ljava/lang/StringBuilder;

    #@3bd
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@3c0
    const-string v23, "got XTWWAN fix in wrong state "

    #@3c2
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c5
    move-result-object v22

    #@3c6
    move-object/from16 v0, p0

    #@3c8
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_internalState:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@3ca
    move-object/from16 v23, v0

    #@3cc
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3cf
    move-result-object v22

    #@3d0
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d3
    move-result-object v22

    #@3d4
    invoke-virtual/range {v20 .. v22}, Lcom/qualcomm/lib/location/log/LocLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3d7
    .catch Ljava/util/NoSuchElementException; {:try_start_3b3 .. :try_end_3d7} :catch_3d8
    .catch Ljava/io/IOException; {:try_start_3b3 .. :try_end_3d7} :catch_d4

    #@3d7
    goto :goto_35f

    #@3d8
    .line 1228
    :catch_3d8
    move-exception v6

    #@3d9
    .line 1230
    .restart local v6       #e:Ljava/util/NoSuchElementException;
    :try_start_3d9
    move-object/from16 v0, p0

    #@3db
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@3dd
    move-object/from16 v20, v0

    #@3df
    const-string v21, "XTWiFiLP"

    #@3e1
    const-string v22, "Exception in getting lat/long/accurcy for XTWWAN fix"

    #@3e3
    invoke-virtual/range {v20 .. v22}, Lcom/qualcomm/lib/location/log/LocLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@3e6
    goto/16 :goto_35f

    #@3e8
    .line 1266
    .end local v6           #e:Ljava/util/NoSuchElementException;
    :cond_3e8
    invoke-direct/range {p0 .. p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->handleZppFix()V

    #@3eb
    goto/16 :goto_a2

    #@3ed
    .line 1272
    :cond_3ed
    invoke-direct/range {p0 .. p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->handleFixReportFromXtwifi()V

    #@3f0
    .line 1275
    move-object/from16 v0, p0

    #@3f2
    iget-object v0, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_xtwifi_tx_state:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@3f4
    move-object/from16 v20, v0

    #@3f6
    sget-object v21, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;->XFS_FINAL:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@3f8
    move-object/from16 v0, v20

    #@3fa
    move-object/from16 v1, v21

    #@3fc
    if-ne v0, v1, :cond_a2

    #@3fe
    .line 1278
    invoke-direct/range {p0 .. p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->reInstallTbfTimer()V
    :try_end_401
    .catch Ljava/io/IOException; {:try_start_3d9 .. :try_end_401} :catch_d4
    .catch Ljava/util/NoSuchElementException; {:try_start_3d9 .. :try_end_401} :catch_128

    #@401
    goto/16 :goto_a2

    #@403
    .line 1111
    nop

    #@404
    :pswitch_data_404
    .packed-switch 0x1
        :pswitch_138
        :pswitch_138
    .end packed-switch
.end method

.method private handleTryConnect()V
    .registers 10

    #@0
    .prologue
    const/16 v8, 0x66

    #@2
    const/4 v7, 0x0

    #@3
    .line 363
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_ipcClient:Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@5
    if-nez v3, :cond_12

    #@7
    .line 365
    new-instance v3, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@9
    const-string v4, "/data/misc/location/mq/location-mq-s"

    #@b
    invoke-direct {v3, v4}, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;-><init>(Ljava/lang/String;)V

    #@e
    iput-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_ipcClient:Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@10
    .line 366
    iput v7, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_connectRetryCounter:I

    #@12
    .line 369
    :cond_12
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_ipcClient:Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@14
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->connect()Z

    #@17
    move-result v3

    #@18
    if-nez v3, :cond_5b

    #@1a
    .line 371
    iget v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_connectRetryCounter:I

    #@1c
    add-int/lit8 v3, v3, 0x1

    #@1e
    iput v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_connectRetryCounter:I

    #@20
    .line 372
    iget v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_connectRetryCounter:I

    #@22
    const/16 v4, 0x14

    #@24
    if-ge v3, v4, :cond_4c

    #@26
    .line 374
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@28
    const-string v4, "XTWiFiLP"

    #@2a
    new-instance v5, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v6, "connection failure. count "

    #@31
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v5

    #@35
    iget v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_connectRetryCounter:I

    #@37
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v5

    #@3b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v5

    #@3f
    invoke-virtual {v3, v4, v5}, Lcom/qualcomm/lib/location/log/LocLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@42
    .line 375
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_handler:Landroid/os/Handler;

    #@44
    const/16 v4, 0x64

    #@46
    const-wide/16 v5, 0x1388

    #@48
    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@4b
    .line 435
    :cond_4b
    :goto_4b
    return-void

    #@4c
    .line 379
    :cond_4c
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@4e
    const-string v4, "XTWiFiLP"

    #@50
    const-string v5, "connection failure. abort"

    #@52
    invoke-virtual {v3, v4, v5}, Lcom/qualcomm/lib/location/log/LocLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@55
    .line 380
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_publicStatus:Lcom/qualcomm/services/location/xtwifi/NlpPublicStatus;

    #@57
    invoke-virtual {v3, v7}, Lcom/qualcomm/services/location/xtwifi/NlpPublicStatus;->setStatus(I)V

    #@5a
    goto :goto_4b

    #@5b
    .line 385
    :cond_5b
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_zppAdaptor:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;

    #@5d
    if-nez v3, :cond_79

    #@5f
    iget-boolean v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_isZppEnabled:Z

    #@61
    if-eqz v3, :cond_79

    #@63
    .line 389
    :try_start_63
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@65
    const-string v4, "XTWiFiLP"

    #@67
    const-string v5, "Initializing ZPP adaptor"

    #@69
    invoke-virtual {v3, v4, v5}, Lcom/qualcomm/lib/location/log/LocLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@6c
    .line 390
    new-instance v3, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;

    #@6e
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_handler:Landroid/os/Handler;

    #@70
    const/16 v5, 0x8

    #@72
    iget v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_log_level:I

    #@74
    invoke-direct {v3, v4, v5, v6}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;-><init>(Landroid/os/Handler;II)V

    #@77
    iput-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_zppAdaptor:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;
    :try_end_79
    .catch Ljava/lang/LinkageError; {:try_start_63 .. :try_end_79} :catch_ef

    #@79
    .line 402
    :cond_79
    :goto_79
    :try_start_79
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_ipcClient:Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@7b
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_handler:Landroid/os/Handler;

    #@7d
    const/16 v5, 0x65

    #@7f
    const/16 v6, 0x66

    #@81
    invoke-virtual {v3, v4, v5, v6}, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->startReceiverThread(Landroid/os/Handler;II)V

    #@84
    .line 406
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_ipcClient:Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@86
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->getOutputStream()Ljava/io/OutputStream;

    #@89
    move-result-object v1

    #@8a
    .line 407
    .local v1, os:Ljava/io/OutputStream;
    new-instance v2, Lcom/qualcomm/lib/location/mq_client/OutPostcard;

    #@8c
    invoke-direct {v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;-><init>()V

    #@8f
    .line 408
    .local v2, out:Lcom/qualcomm/lib/location/mq_client/OutPostcard;
    invoke-virtual {v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->init()V

    #@92
    .line 409
    const-string v3, "TO"

    #@94
    const-string v4, "SERVER"

    #@96
    invoke-virtual {v2, v3, v4}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@99
    .line 410
    const-string v3, "FROM"

    #@9b
    const-string v4, "XTWiFiLP"

    #@9d
    invoke-virtual {v2, v3, v4}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@a0
    .line 411
    const-string v3, "REQ"

    #@a2
    const-string v4, "REGISTER"

    #@a4
    invoke-virtual {v2, v3, v4}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addString(Ljava/lang/String;Ljava/lang/String;)V

    #@a7
    .line 412
    invoke-virtual {v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->seal()V

    #@aa
    .line 413
    invoke-virtual {v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->getEncodedBuffer()[B

    #@ad
    move-result-object v3

    #@ae
    invoke-virtual {v1, v3}, Ljava/io/OutputStream;->write([B)V

    #@b1
    .line 414
    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V

    #@b4
    .line 416
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_publicStatus:Lcom/qualcomm/services/location/xtwifi/NlpPublicStatus;

    #@b6
    const/4 v4, 0x2

    #@b7
    invoke-virtual {v3, v4}, Lcom/qualcomm/services/location/xtwifi/NlpPublicStatus;->setStatus(I)V

    #@ba
    .line 418
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_internalState:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@bc
    sget-object v4, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;->IS_DISABLED:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@be
    if-eq v3, v4, :cond_4b

    #@c0
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_internalState:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@c2
    sget-object v4, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;->IS_IDLE:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@c4
    if-eq v3, v4, :cond_4b

    #@c6
    .line 422
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@c8
    const-string v4, "XTWiFiLP"

    #@ca
    const-string v5, "Restart tracking session after re-connection"

    #@cc
    invoke-virtual {v3, v4, v5}, Lcom/qualcomm/lib/location/log/LocLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@cf
    .line 423
    const/4 v3, 0x0

    #@d0
    invoke-virtual {p0, v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->handleEnableLocationTracking(Z)V

    #@d3
    .line 424
    const/4 v3, 0x1

    #@d4
    invoke-virtual {p0, v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->handleEnableLocationTracking(Z)V
    :try_end_d7
    .catch Ljava/lang/Exception; {:try_start_79 .. :try_end_d7} :catch_d9

    #@d7
    goto/16 :goto_4b

    #@d9
    .line 427
    .end local v1           #os:Ljava/io/OutputStream;
    .end local v2           #out:Lcom/qualcomm/lib/location/mq_client/OutPostcard;
    :catch_d9
    move-exception v0

    #@da
    .line 429
    .local v0, e:Ljava/lang/Exception;
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_publicStatus:Lcom/qualcomm/services/location/xtwifi/NlpPublicStatus;

    #@dc
    invoke-virtual {v3, v7}, Lcom/qualcomm/services/location/xtwifi/NlpPublicStatus;->setStatus(I)V

    #@df
    .line 430
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@e1
    const-string v4, "XTWiFiLP"

    #@e3
    const-string v5, "cannot send register message"

    #@e5
    invoke-virtual {v3, v4, v5}, Lcom/qualcomm/lib/location/log/LocLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@e8
    .line 432
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_handler:Landroid/os/Handler;

    #@ea
    invoke-virtual {v3, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@ed
    goto/16 :goto_4b

    #@ef
    .line 392
    .end local v0           #e:Ljava/lang/Exception;
    :catch_ef
    move-exception v0

    #@f0
    .line 394
    .local v0, e:Ljava/lang/LinkageError;
    invoke-virtual {v0}, Ljava/lang/LinkageError;->printStackTrace()V

    #@f3
    .line 395
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@f5
    const-string v4, "XTWiFiLP"

    #@f7
    const-string v5, "ZPP adaptor not found. disabling ZPP."

    #@f9
    invoke-virtual {v3, v4, v5}, Lcom/qualcomm/lib/location/log/LocLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@fc
    .line 396
    iput-boolean v7, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_isZppEnabled:Z

    #@fe
    goto/16 :goto_79
.end method

.method private handleWiFiScanResult()V
    .registers 11

    #@0
    .prologue
    const-wide/16 v3, 0x0

    #@2
    .line 1028
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@5
    move-result-wide v8

    #@6
    .line 1029
    .local v8, now:J
    iput-wide v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_timestamp_last_wifi_scan_result:J

    #@8
    .line 1031
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@a
    const-string v1, "XTWiFiLP"

    #@c
    const-string v2, "handleWiFiScanResult: wifi scan result is available"

    #@e
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 1033
    iget-boolean v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_isEnabledFixReqOnFreeWiFiScan:Z

    #@13
    if-nez v0, :cond_16

    #@15
    .line 1082
    :goto_15
    return-void

    #@16
    .line 1039
    :cond_16
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_publicLocationManager:Landroid/location/LocationManager;

    #@18
    if-nez v0, :cond_24

    #@1a
    .line 1041
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@1c
    const-string v1, "XTWiFiLP"

    #@1e
    const-string v2, "no access to public location manager"

    #@20
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@23
    goto :goto_15

    #@24
    .line 1046
    :cond_24
    iget-boolean v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_isFreeRideListenerInstalled:Z

    #@26
    if-eqz v0, :cond_32

    #@28
    .line 1048
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@2a
    const-string v1, "XTWiFiLP"

    #@2c
    const-string v2, "handleWiFiScanResult: free fix request session is already active, ignore"

    #@2e
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@31
    goto :goto_15

    #@32
    .line 1054
    :cond_32
    iget-wide v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_timestamp_last_fix_request_on_free_wifi_scan:J

    #@34
    cmp-long v0, v0, v3

    #@36
    if-eqz v0, :cond_52

    #@38
    .line 1056
    iget-wide v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_timestamp_last_fix_request_on_free_wifi_scan:J

    #@3a
    cmp-long v0, v8, v0

    #@3c
    if-lez v0, :cond_52

    #@3e
    .line 1058
    iget-wide v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_timestamp_last_fix_request_on_free_wifi_scan:J

    #@40
    sub-long v0, v8, v0

    #@42
    iget-wide v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_minTimeBetweenFixOnFreeWiFiScan:J

    #@44
    cmp-long v0, v0, v2

    #@46
    if-gez v0, :cond_52

    #@48
    .line 1060
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@4a
    const-string v1, "XTWiFiLP"

    #@4c
    const-string v2, "handleWiFiScanResult: free fix request was sent out not too long ago, ignore"

    #@4e
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@51
    goto :goto_15

    #@52
    .line 1069
    :cond_52
    iput-wide v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_timestamp_last_fix_request_on_free_wifi_scan:J

    #@54
    .line 1074
    :try_start_54
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@56
    const-string v1, "XTWiFiLP"

    #@58
    const-string v2, "handleWiFiScanResult: requesting fast tracking session for NLP"

    #@5a
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@5d
    .line 1075
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_publicLocationManager:Landroid/location/LocationManager;

    #@5f
    const-string v1, "network"

    #@61
    const-wide/16 v2, 0x0

    #@63
    const/4 v4, 0x0

    #@64
    iget-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_FreeRideLocationListener:Landroid/location/LocationListener;

    #@66
    iget-object v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_handler:Landroid/os/Handler;

    #@68
    invoke-virtual {v6}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    #@6b
    move-result-object v6

    #@6c
    invoke-virtual/range {v0 .. v6}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V

    #@6f
    .line 1076
    const/4 v0, 0x1

    #@70
    iput-boolean v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_isFreeRideListenerInstalled:Z
    :try_end_72
    .catch Ljava/lang/IllegalArgumentException; {:try_start_54 .. :try_end_72} :catch_73

    #@72
    goto :goto_15

    #@73
    .line 1078
    :catch_73
    move-exception v7

    #@74
    .line 1080
    .local v7, e:Ljava/lang/IllegalArgumentException;
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@76
    const-string v1, "XTWiFiLP"

    #@78
    const-string v2, "handleWiFiScanResult: Network Location Provider is not enabled."

    #@7a
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7d
    goto :goto_15
.end method

.method private handleZppFix()V
    .registers 5

    #@0
    .prologue
    .line 568
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_handler:Landroid/os/Handler;

    #@2
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->timeoutZppFix:Ljava/lang/Runnable;

    #@4
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@7
    .line 569
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_internalState:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@9
    sget-object v1, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;->IS_TRACKING_WAIT_FOR_ZPP_FIX:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@b
    if-ne v0, v1, :cond_34

    #@d
    .line 571
    sget-object v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;->IS_TRACKING_WAIT_FOR_FINAL_FIX:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@f
    iput-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_internalState:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@11
    .line 573
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_xtwifi_tx_state:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@13
    sget-object v1, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;->XFS_PRELIMINARY:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@15
    if-eq v0, v1, :cond_1d

    #@17
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_xtwifi_tx_state:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@19
    sget-object v1, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;->XFS_FINAL:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@1b
    if-ne v0, v1, :cond_2a

    #@1d
    .line 577
    :cond_1d
    invoke-direct {p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->handleFixReportFromXtwifi()V

    #@20
    .line 580
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_xtwifi_tx_state:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@22
    sget-object v1, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;->XFS_FINAL:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@24
    if-ne v0, v1, :cond_29

    #@26
    .line 583
    invoke-direct {p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->reInstallTbfTimer()V

    #@29
    .line 597
    :cond_29
    :goto_29
    return-void

    #@2a
    .line 589
    :cond_2a
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@2c
    const-string v1, "XTWiFiLP"

    #@2e
    const-string v2, "handleZppFix keep waiting for XTWiFi final fix"

    #@30
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@33
    goto :goto_29

    #@34
    .line 595
    :cond_34
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@36
    const-string v1, "XTWiFiLP"

    #@38
    new-instance v2, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v3, "handleZppFix fired in wrong state "

    #@3f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v2

    #@43
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_internalState:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@45
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v2

    #@49
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v2

    #@4d
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@50
    goto :goto_29
.end method

.method private isZppFixGood()Z
    .registers 7

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 729
    iget-boolean v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_isZppEnabled:Z

    #@3
    if-nez v2, :cond_33

    #@5
    iget-boolean v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_isXTWWANEnabled:Z

    #@7
    if-nez v2, :cond_33

    #@9
    .line 732
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@b
    const-string v3, "XTWiFiLP"

    #@d
    new-instance v4, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v5, "isZppFixGood ZPP Enabled = "

    #@14
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v4

    #@18
    iget-boolean v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_isZppEnabled:Z

    #@1a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v4

    #@1e
    const-string v5, " XTWWAN Enabled = "

    #@20
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v4

    #@24
    iget-boolean v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_isXTWWANEnabled:Z

    #@26
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@29
    move-result-object v4

    #@2a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v4

    #@2e
    invoke-virtual {v2, v3, v4}, Lcom/qualcomm/lib/location/log/LocLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@31
    move v0, v1

    #@32
    .line 778
    :goto_32
    return v0

    #@33
    .line 736
    :cond_33
    iget-boolean v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_isXTWWANEnabled:Z

    #@35
    if-eqz v2, :cond_3a

    #@37
    .line 737
    iget-boolean v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_is_xtwwan_fix_with_lat_lon:Z

    #@39
    goto :goto_32

    #@3a
    .line 739
    :cond_3a
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownZppFix:Landroid/location/Location;

    #@3c
    invoke-virtual {v2}, Landroid/location/Location;->reset()V

    #@3f
    .line 740
    const/4 v0, 0x0

    #@40
    .line 741
    .local v0, is_zpp_fix_valid:Z
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_zppAdaptor:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;

    #@42
    if-eqz v2, :cond_4c

    #@44
    .line 743
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_zppAdaptor:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;

    #@46
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownZppFix:Landroid/location/Location;

    #@48
    invoke-virtual {v2, v3}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->getLastKnownLocation(Landroid/location/Location;)Z

    #@4b
    move-result v0

    #@4c
    .line 746
    :cond_4c
    if-nez v0, :cond_59

    #@4e
    .line 749
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@50
    const-string v3, "XTWiFiLP"

    #@52
    const-string v4, "isZppFixGood ZPP adaptor null or it says the fix is not valid"

    #@54
    invoke-virtual {v2, v3, v4}, Lcom/qualcomm/lib/location/log/LocLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@57
    move v0, v1

    #@58
    .line 750
    goto :goto_32

    #@59
    .line 753
    :cond_59
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownZppFix:Landroid/location/Location;

    #@5b
    const-string v2, "network"

    #@5d
    invoke-virtual {v1, v2}, Landroid/location/Location;->setProvider(Ljava/lang/String;)V

    #@60
    .line 754
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownZppFix:Landroid/location/Location;

    #@62
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    #@65
    move-result-wide v2

    #@66
    invoke-virtual {v1, v2, v3}, Landroid/location/Location;->setElapsedRealtimeNanos(J)V

    #@69
    goto :goto_32
.end method

.method private isZppRequestSentRecently()Z
    .registers 10

    #@0
    .prologue
    const-wide/16 v7, 0x0

    #@2
    const/4 v4, 0x0

    #@3
    .line 696
    iget-boolean v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_isZppEnabled:Z

    #@5
    if-nez v5, :cond_8

    #@7
    .line 723
    :cond_7
    :goto_7
    return v4

    #@8
    .line 702
    :cond_8
    iget-wide v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_maxAgeForZppTransaction:J

    #@a
    cmp-long v5, v5, v7

    #@c
    if-eqz v5, :cond_7

    #@e
    .line 707
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@11
    move-result-wide v2

    #@12
    .line 708
    .local v2, now:J
    const-wide/16 v0, -0x1

    #@14
    .line 710
    .local v0, age_ZPP_tx:J
    iget-wide v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_timestampLastZppReport:J

    #@16
    cmp-long v5, v5, v7

    #@18
    if-lez v5, :cond_24

    #@1a
    iget-wide v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_timestampLastZppReport:J

    #@1c
    cmp-long v5, v5, v2

    #@1e
    if-gez v5, :cond_24

    #@20
    .line 713
    iget-wide v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_timestampLastZppReport:J

    #@22
    sub-long v0, v2, v5

    #@24
    .line 716
    :cond_24
    cmp-long v5, v0, v7

    #@26
    if-lez v5, :cond_7

    #@28
    iget-wide v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_maxAgeForZppTransaction:J

    #@2a
    cmp-long v5, v0, v5

    #@2c
    if-gez v5, :cond_7

    #@2e
    .line 719
    const/4 v4, 0x1

    #@2f
    goto :goto_7
.end method

.method private reInstallTbfTimer()V
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 649
    sget-object v4, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;->IS_TRACKING_WAIT_FOR_TBF:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@3
    iput-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_internalState:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@5
    .line 652
    const/4 v4, -0x1

    #@6
    iput v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_current_gtp_req_tx_id:I

    #@8
    .line 653
    sget-object v4, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;->XFS_IDLE_OR_WAITING:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@a
    iput-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_xtwifi_tx_state:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@c
    .line 654
    iput-boolean v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_is_xtwifi_fix_with_lat_lon:Z

    #@e
    .line 655
    iput-boolean v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_is_xtwwan_fix_with_lat_lon:Z

    #@10
    .line 656
    iput-boolean v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_got_xtwwan_response:Z

    #@12
    .line 658
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@15
    move-result-wide v0

    #@16
    .line 659
    .local v0, now:J
    const-wide/16 v2, 0x0

    #@18
    .line 661
    .local v2, time_to_next_fix:J
    iget-wide v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_timestamp_last_fix_request_in_current_tracking_session:J

    #@1a
    const-wide/16 v6, 0x0

    #@1c
    cmp-long v4, v4, v6

    #@1e
    if-ltz v4, :cond_26

    #@20
    iget-wide v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_timestamp_last_fix_request_in_current_tracking_session:J

    #@22
    cmp-long v4, v4, v0

    #@24
    if-lez v4, :cond_61

    #@26
    .line 667
    :cond_26
    const-wide/16 v2, 0x0

    #@28
    .line 683
    :goto_28
    iput-boolean v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_has_reported_anything_in_current_fix_session:Z

    #@2a
    .line 686
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_handler:Landroid/os/Handler;

    #@2c
    iget-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->timeoutZppFix:Ljava/lang/Runnable;

    #@2e
    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@31
    .line 687
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_handler:Landroid/os/Handler;

    #@33
    iget-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->timeoutFixSession:Ljava/lang/Runnable;

    #@35
    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@38
    .line 688
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_handler:Landroid/os/Handler;

    #@3a
    iget-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->timeoutNextFixSessionDue:Ljava/lang/Runnable;

    #@3c
    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@3f
    .line 689
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_handler:Landroid/os/Handler;

    #@41
    iget-object v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->timeoutNextFixSessionDue:Ljava/lang/Runnable;

    #@43
    invoke-virtual {v4, v5, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@46
    .line 691
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@48
    const-string v5, "XTWiFiLP"

    #@4a
    new-instance v6, Ljava/lang/StringBuilder;

    #@4c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@4f
    const-string v7, "reInstallTbfTimer set timer to next fix: "

    #@51
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v6

    #@55
    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@58
    move-result-object v6

    #@59
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v6

    #@5d
    invoke-virtual {v4, v5, v6}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@60
    .line 692
    return-void

    #@61
    .line 669
    :cond_61
    iget-boolean v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_has_reported_anything_in_current_fix_session:Z

    #@63
    if-eqz v4, :cond_68

    #@65
    .line 671
    iget-wide v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_currentTimeBetweenFix:J

    #@67
    goto :goto_28

    #@68
    .line 679
    :cond_68
    iget-wide v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_currentTimeBetweenFix:J

    #@6a
    goto :goto_28
.end method

.method private removeLocationUpdateListenerForWifi()V
    .registers 4

    #@0
    .prologue
    .line 1016
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_publicLocationManager:Landroid/location/LocationManager;

    #@2
    if-eqz v0, :cond_8

    #@4
    iget-boolean v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_isFreeRideListenerInstalled:Z

    #@6
    if-nez v0, :cond_9

    #@8
    .line 1024
    :cond_8
    :goto_8
    return-void

    #@9
    .line 1021
    :cond_9
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@b
    const-string v1, "XTWiFiLP"

    #@d
    const-string v2, "removeLocationUpdateListenerForWifi: removing location listener"

    #@f
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@12
    .line 1022
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_publicLocationManager:Landroid/location/LocationManager;

    #@14
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_FreeRideLocationListener:Landroid/location/LocationListener;

    #@16
    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    #@19
    .line 1023
    const/4 v0, 0x0

    #@1a
    iput-boolean v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_isFreeRideListenerInstalled:Z

    #@1c
    goto :goto_8
.end method

.method private shouldSendFreeRideRequest()Z
    .registers 8

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 982
    iget-boolean v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_isEnabledFixReqOnFreeWiFiScan:Z

    #@3
    if-eqz v3, :cond_11

    #@5
    iget-boolean v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_isFreeRideListenerInstalled:Z

    #@7
    if-eqz v3, :cond_11

    #@9
    const-wide/16 v3, 0x0

    #@b
    iget-wide v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_timestamp_last_fix_request_on_free_wifi_scan:J

    #@d
    cmp-long v3, v3, v5

    #@f
    if-nez v3, :cond_12

    #@11
    .line 1011
    :cond_11
    :goto_11
    return v2

    #@12
    .line 988
    :cond_12
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@15
    move-result-wide v0

    #@16
    .line 989
    .local v0, now:J
    iget-wide v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_timestamp_last_wifi_scan_result:J

    #@18
    cmp-long v3, v0, v3

    #@1a
    if-lez v3, :cond_28

    #@1c
    .line 991
    iget-wide v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_timestamp_last_wifi_scan_result:J

    #@1e
    sub-long v3, v0, v3

    #@20
    iget-wide v5, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_maxTimeBetweenWiFiScanAndRequest:J

    #@22
    cmp-long v3, v3, v5

    #@24
    if-gez v3, :cond_28

    #@26
    .line 997
    const/4 v2, 0x1

    #@27
    goto :goto_11

    #@28
    .line 1010
    :cond_28
    invoke-direct {p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->removeLocationUpdateListenerForWifi()V

    #@2b
    goto :goto_11
.end method

.method private verifyWithZppAndReportXtwifiFix()V
    .registers 22

    #@0
    .prologue
    .line 783
    const/16 v19, 0x1

    #@2
    .line 785
    .local v19, should_report:Z
    move-object/from16 v0, p0

    #@4
    iget-boolean v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_is_xtwifi_fix_with_lat_lon:Z

    #@6
    if-nez v1, :cond_152

    #@8
    .line 787
    const/16 v19, 0x0

    #@a
    .line 789
    const/16 v20, 0x0

    #@c
    .line 790
    .local v20, wifi_injected:Z
    move-object/from16 v0, p0

    #@e
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix_mac_array:[I

    #@10
    if-eqz v1, :cond_6b

    #@12
    .line 792
    move-object/from16 v0, p0

    #@14
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix_mac_array:[I

    #@16
    array-length v1, v1

    #@17
    if-lez v1, :cond_6b

    #@19
    .line 795
    move-object/from16 v0, p0

    #@1b
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@1d
    const-string v2, "XTWiFiLP"

    #@1f
    const-string v3, "Injecting Wifi scan result without fix information"

    #@21
    invoke-virtual {v1, v2, v3}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@24
    .line 796
    move-object/from16 v0, p0

    #@26
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_zppAdaptor:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;

    #@28
    const/4 v2, 0x3

    #@29
    move-object/from16 v0, p0

    #@2b
    iget-boolean v3, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_is_xtwifi_fix_with_lat_lon:Z

    #@2d
    move-object/from16 v0, p0

    #@2f
    iget-object v4, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix:Landroid/location/Location;

    #@31
    invoke-virtual {v4}, Landroid/location/Location;->getLatitude()D

    #@34
    move-result-wide v4

    #@35
    move-object/from16 v0, p0

    #@37
    iget-object v6, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix:Landroid/location/Location;

    #@39
    invoke-virtual {v6}, Landroid/location/Location;->getLongitude()D

    #@3c
    move-result-wide v6

    #@3d
    move-object/from16 v0, p0

    #@3f
    iget-object v8, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix:Landroid/location/Location;

    #@41
    invoke-virtual {v8}, Landroid/location/Location;->getAccuracy()F

    #@44
    move-result v8

    #@45
    const/high16 v9, -0x4080

    #@47
    const-wide/16 v10, 0x0

    #@49
    const/high16 v12, -0x4080

    #@4b
    move-object/from16 v0, p0

    #@4d
    iget-object v13, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix_mac_array:[I

    #@4f
    move-object/from16 v0, p0

    #@51
    iget-object v14, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix_rssi_array:[I

    #@53
    move-object/from16 v0, p0

    #@55
    iget-object v15, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix_channel_array:[I

    #@57
    invoke-virtual/range {v1 .. v15}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->injectFix(IZDDFFJF[I[I[I)Z

    #@5a
    .line 800
    const/16 v20, 0x1

    #@5c
    .line 803
    const/4 v1, 0x0

    #@5d
    move-object/from16 v0, p0

    #@5f
    iput-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix_mac_array:[I

    #@61
    .line 804
    const/4 v1, 0x0

    #@62
    move-object/from16 v0, p0

    #@64
    iput-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix_rssi_array:[I

    #@66
    .line 805
    const/4 v1, 0x0

    #@67
    move-object/from16 v0, p0

    #@69
    iput-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix_channel_array:[I

    #@6b
    .line 808
    :cond_6b
    if-nez v20, :cond_78

    #@6d
    .line 810
    move-object/from16 v0, p0

    #@6f
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@71
    const-string v2, "XTWiFiLP"

    #@73
    const-string v3, "Injecting nothing as we do not have fix, nor WiFi scan result"

    #@75
    invoke-virtual {v1, v2, v3}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@78
    .line 842
    .end local v20           #wifi_injected:Z
    :cond_78
    :goto_78
    if-eqz v19, :cond_151

    #@7a
    .line 846
    :try_start_7a
    move-object/from16 v0, p0

    #@7c
    iget-boolean v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_isXTWWANEnabled:Z

    #@7e
    if-eqz v1, :cond_c7

    #@80
    move-object/from16 v0, p0

    #@82
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_zppAdaptor:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;

    #@84
    if-eqz v1, :cond_c7

    #@86
    .line 848
    move-object/from16 v0, p0

    #@88
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@8a
    const-string v2, "XTWiFiLP"

    #@8c
    const-string v3, "Injecting XTWiFi fix (with validation)"

    #@8e
    invoke-virtual {v1, v2, v3}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@91
    .line 849
    move-object/from16 v0, p0

    #@93
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_zppAdaptor:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;

    #@95
    const/4 v2, 0x3

    #@96
    move-object/from16 v0, p0

    #@98
    iget-boolean v3, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_is_xtwifi_fix_with_lat_lon:Z

    #@9a
    move-object/from16 v0, p0

    #@9c
    iget-object v4, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix:Landroid/location/Location;

    #@9e
    invoke-virtual {v4}, Landroid/location/Location;->getLatitude()D

    #@a1
    move-result-wide v4

    #@a2
    move-object/from16 v0, p0

    #@a4
    iget-object v6, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix:Landroid/location/Location;

    #@a6
    invoke-virtual {v6}, Landroid/location/Location;->getLongitude()D

    #@a9
    move-result-wide v6

    #@aa
    move-object/from16 v0, p0

    #@ac
    iget-object v8, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix:Landroid/location/Location;

    #@ae
    invoke-virtual {v8}, Landroid/location/Location;->getAccuracy()F

    #@b1
    move-result v8

    #@b2
    const/high16 v9, -0x4080

    #@b4
    const-wide/16 v10, 0x0

    #@b6
    const/high16 v12, -0x4080

    #@b8
    move-object/from16 v0, p0

    #@ba
    iget-object v13, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix_mac_array:[I

    #@bc
    move-object/from16 v0, p0

    #@be
    iget-object v14, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix_rssi_array:[I

    #@c0
    move-object/from16 v0, p0

    #@c2
    iget-object v15, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix_channel_array:[I

    #@c4
    invoke-virtual/range {v1 .. v15}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->injectFix(IZDDFFJF[I[I[I)Z

    #@c7
    .line 855
    :cond_c7
    move-object/from16 v0, p0

    #@c9
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@cb
    const-string v2, "XTWiFiLP"

    #@cd
    new-instance v3, Ljava/lang/StringBuilder;

    #@cf
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@d2
    const-string v4, "Reporting XTWiFi fix ("

    #@d4
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v3

    #@d8
    move-object/from16 v0, p0

    #@da
    iget-object v4, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix:Landroid/location/Location;

    #@dc
    invoke-virtual {v4}, Landroid/location/Location;->getLatitude()D

    #@df
    move-result-wide v4

    #@e0
    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    #@e3
    move-result-object v3

    #@e4
    const-string v4, ", "

    #@e6
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e9
    move-result-object v3

    #@ea
    move-object/from16 v0, p0

    #@ec
    iget-object v4, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix:Landroid/location/Location;

    #@ee
    invoke-virtual {v4}, Landroid/location/Location;->getLongitude()D

    #@f1
    move-result-wide v4

    #@f2
    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    #@f5
    move-result-object v3

    #@f6
    const-string v4, "), unc "

    #@f8
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fb
    move-result-object v3

    #@fc
    move-object/from16 v0, p0

    #@fe
    iget-object v4, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix:Landroid/location/Location;

    #@100
    invoke-virtual {v4}, Landroid/location/Location;->getAccuracy()F

    #@103
    move-result v4

    #@104
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@107
    move-result-object v3

    #@108
    const-string v4, " time (ms) "

    #@10a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10d
    move-result-object v3

    #@10e
    move-object/from16 v0, p0

    #@110
    iget-object v4, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix:Landroid/location/Location;

    #@112
    invoke-virtual {v4}, Landroid/location/Location;->getElapsedRealtimeNanos()J

    #@115
    move-result-wide v4

    #@116
    const-wide/32 v6, 0xf4240

    #@119
    div-long/2addr v4, v6

    #@11a
    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@11d
    move-result-object v3

    #@11e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@121
    move-result-object v3

    #@122
    invoke-virtual {v1, v2, v3}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@125
    .line 860
    new-instance v18, Landroid/location/Location;

    #@127
    move-object/from16 v0, p0

    #@129
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix:Landroid/location/Location;

    #@12b
    move-object/from16 v0, v18

    #@12d
    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    #@130
    .line 861
    .local v18, fuzzifiedLocation:Landroid/location/Location;
    move-object/from16 v0, p0

    #@132
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix:Landroid/location/Location;

    #@134
    const-string v2, "noGPSLocation"

    #@136
    move-object/from16 v0, v18

    #@138
    invoke-virtual {v1, v2, v0}, Landroid/location/Location;->setExtraLocation(Ljava/lang/String;Landroid/location/Location;)V

    #@13b
    .line 863
    move-object/from16 v0, p0

    #@13d
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_LocationManager:Landroid/location/ILocationManager;

    #@13f
    new-instance v2, Landroid/location/Location;

    #@141
    move-object/from16 v0, p0

    #@143
    iget-object v3, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix:Landroid/location/Location;

    #@145
    invoke-direct {v2, v3}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    #@148
    const/4 v3, 0x0

    #@149
    invoke-interface {v1, v2, v3}, Landroid/location/ILocationManager;->reportLocation(Landroid/location/Location;Z)V
    :try_end_14c
    .catch Landroid/os/RemoteException; {:try_start_7a .. :try_end_14c} :catch_206

    #@14c
    .line 871
    .end local v18           #fuzzifiedLocation:Landroid/location/Location;
    :goto_14c
    const/4 v1, 0x1

    #@14d
    move-object/from16 v0, p0

    #@14f
    iput-boolean v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_has_reported_anything_in_current_fix_session:Z

    #@151
    .line 873
    :cond_151
    return-void

    #@152
    .line 815
    :cond_152
    move-object/from16 v0, p0

    #@154
    iget-boolean v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_isXTWWANEnabled:Z

    #@156
    if-nez v1, :cond_1ae

    #@158
    move-object/from16 v0, p0

    #@15a
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_zppAdaptor:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;

    #@15c
    if-eqz v1, :cond_1ae

    #@15e
    .line 817
    move-object/from16 v0, p0

    #@160
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@162
    const-string v2, "XTWiFiLP"

    #@164
    const-string v3, "Injecting XTWiFi fix (without validation)"

    #@166
    invoke-virtual {v1, v2, v3}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@169
    .line 818
    move-object/from16 v0, p0

    #@16b
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_zppAdaptor:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;

    #@16d
    const/4 v2, 0x3

    #@16e
    move-object/from16 v0, p0

    #@170
    iget-boolean v3, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_is_xtwifi_fix_with_lat_lon:Z

    #@172
    move-object/from16 v0, p0

    #@174
    iget-object v4, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix:Landroid/location/Location;

    #@176
    invoke-virtual {v4}, Landroid/location/Location;->getLatitude()D

    #@179
    move-result-wide v4

    #@17a
    move-object/from16 v0, p0

    #@17c
    iget-object v6, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix:Landroid/location/Location;

    #@17e
    invoke-virtual {v6}, Landroid/location/Location;->getLongitude()D

    #@181
    move-result-wide v6

    #@182
    move-object/from16 v0, p0

    #@184
    iget-object v8, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix:Landroid/location/Location;

    #@186
    invoke-virtual {v8}, Landroid/location/Location;->getAccuracy()F

    #@189
    move-result v8

    #@18a
    const/high16 v9, -0x4080

    #@18c
    const-wide/16 v10, 0x0

    #@18e
    const/high16 v12, -0x4080

    #@190
    move-object/from16 v0, p0

    #@192
    iget-object v13, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix_mac_array:[I

    #@194
    move-object/from16 v0, p0

    #@196
    iget-object v14, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix_rssi_array:[I

    #@198
    move-object/from16 v0, p0

    #@19a
    iget-object v15, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix_channel_array:[I

    #@19c
    invoke-virtual/range {v1 .. v15}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->injectFix(IZDDFFJF[I[I[I)Z

    #@19f
    .line 824
    const/4 v1, 0x0

    #@1a0
    move-object/from16 v0, p0

    #@1a2
    iput-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix_mac_array:[I

    #@1a4
    .line 825
    const/4 v1, 0x0

    #@1a5
    move-object/from16 v0, p0

    #@1a7
    iput-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix_rssi_array:[I

    #@1a9
    .line 826
    const/4 v1, 0x0

    #@1aa
    move-object/from16 v0, p0

    #@1ac
    iput-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix_channel_array:[I

    #@1ae
    .line 829
    :cond_1ae
    invoke-direct/range {p0 .. p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->isZppFixGood()Z

    #@1b1
    move-result v1

    #@1b2
    if-eqz v1, :cond_78

    #@1b4
    .line 833
    move-object/from16 v0, p0

    #@1b6
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownZppFix:Landroid/location/Location;

    #@1b8
    move-object/from16 v0, p0

    #@1ba
    iget-object v2, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownXtwifiFix:Landroid/location/Location;

    #@1bc
    invoke-virtual {v1, v2}, Landroid/location/Location;->distanceTo(Landroid/location/Location;)F

    #@1bf
    move-result v16

    #@1c0
    .line 834
    .local v16, distance:F
    move-object/from16 v0, p0

    #@1c2
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@1c4
    const-string v2, "XTWiFiLP"

    #@1c6
    new-instance v3, Ljava/lang/StringBuilder;

    #@1c8
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1cb
    const-string v4, "Distance between ZPP and XTWiFi fixes: "

    #@1cd
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d0
    move-result-object v3

    #@1d1
    move/from16 v0, v16

    #@1d3
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@1d6
    move-result-object v3

    #@1d7
    const-string v4, " m, while accuracy of ZPP fix is "

    #@1d9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1dc
    move-result-object v3

    #@1dd
    move-object/from16 v0, p0

    #@1df
    iget-object v4, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownZppFix:Landroid/location/Location;

    #@1e1
    invoke-virtual {v4}, Landroid/location/Location;->getAccuracy()F

    #@1e4
    move-result v4

    #@1e5
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@1e8
    move-result-object v3

    #@1e9
    const-string v4, " m"

    #@1eb
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ee
    move-result-object v3

    #@1ef
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f2
    move-result-object v3

    #@1f3
    invoke-virtual {v1, v2, v3}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@1f6
    .line 835
    move-object/from16 v0, p0

    #@1f8
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_lastKnownZppFix:Landroid/location/Location;

    #@1fa
    invoke-virtual {v1}, Landroid/location/Location;->getAccuracy()F

    #@1fd
    move-result v1

    #@1fe
    cmpl-float v1, v16, v1

    #@200
    if-lez v1, :cond_78

    #@202
    .line 837
    const/16 v19, 0x0

    #@204
    goto/16 :goto_78

    #@206
    .line 866
    .end local v16           #distance:F
    :catch_206
    move-exception v17

    #@207
    .line 868
    .local v17, e:Landroid/os/RemoteException;
    move-object/from16 v0, p0

    #@209
    iget-object v1, v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@20b
    const-string v2, "XTWiFiLP"

    #@20d
    const-string v3, "Exception in reporting location to LocationManagerService (2)"

    #@20f
    invoke-virtual {v1, v2, v3}, Lcom/qualcomm/lib/location/log/LocLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@212
    goto/16 :goto_14c
.end method


# virtual methods
.method public handleDisableNlp()V
    .registers 4

    #@0
    .prologue
    .line 268
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@2
    const-string v1, "XTWiFiLP"

    #@4
    const-string v2, "handleDisable"

    #@6
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 269
    const/4 v0, 0x0

    #@a
    invoke-virtual {p0, v0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->handleEnableLocationTracking(Z)V

    #@d
    .line 271
    sget-object v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;->IS_DISABLED:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@f
    iput-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_internalState:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@11
    .line 272
    return-void
.end method

.method public handleEnableLocationTracking(Z)V
    .registers 7
    .parameter "enable"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 276
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@3
    const-string v1, "XTWiFiLP"

    #@5
    new-instance v2, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v3, "handleEnableLocationTracking ["

    #@c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    const-string v3, "]"

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@21
    .line 281
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_handler:Landroid/os/Handler;

    #@23
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->timeoutZppFix:Ljava/lang/Runnable;

    #@25
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@28
    .line 282
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_handler:Landroid/os/Handler;

    #@2a
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->timeoutFixSession:Ljava/lang/Runnable;

    #@2c
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@2f
    .line 283
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_handler:Landroid/os/Handler;

    #@31
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->timeoutNextFixSessionDue:Ljava/lang/Runnable;

    #@33
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@36
    .line 286
    const/4 v0, -0x1

    #@37
    iput v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_current_gtp_req_tx_id:I

    #@39
    .line 287
    sget-object v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;->XFS_IDLE_OR_WAITING:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@3b
    iput-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_xtwifi_tx_state:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$XtwifiFixState;

    #@3d
    .line 288
    iput-boolean v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_is_xtwifi_fix_with_lat_lon:Z

    #@3f
    .line 289
    iput-boolean v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_is_xtwwan_fix_with_lat_lon:Z

    #@41
    .line 290
    iput-boolean v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_got_xtwwan_response:Z

    #@43
    .line 291
    iput-boolean v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_has_reported_anything_in_current_fix_session:Z

    #@45
    .line 297
    const-wide/16 v0, -0x1

    #@47
    iput-wide v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_timestamp_last_fix_request_in_current_tracking_session:J

    #@49
    .line 299
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_internalState:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@4b
    sget-object v1, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;->IS_DISABLED:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@4d
    if-ne v0, v1, :cond_59

    #@4f
    .line 302
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@51
    const-string v1, "XTWiFiLP"

    #@53
    const-string v2, "handleEnableLocationTracking already in disabled state. ignore request"

    #@55
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@58
    .line 329
    :goto_58
    return-void

    #@59
    .line 306
    :cond_59
    if-eqz p1, :cond_7e

    #@5b
    .line 308
    const/4 v0, 0x2

    #@5c
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_publicStatus:Lcom/qualcomm/services/location/xtwifi/NlpPublicStatus;

    #@5e
    invoke-virtual {v1}, Lcom/qualcomm/services/location/xtwifi/NlpPublicStatus;->getStatus()I

    #@61
    move-result v1

    #@62
    if-ne v0, v1, :cond_70

    #@64
    .line 311
    sget-object v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;->IS_TRACKING_WAIT_FOR_TBF:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@66
    iput-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_internalState:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@68
    .line 312
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_handler:Landroid/os/Handler;

    #@6a
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->timeoutNextFixSessionDue:Ljava/lang/Runnable;

    #@6c
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@6f
    goto :goto_58

    #@70
    .line 318
    :cond_70
    sget-object v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;->IS_IDLE:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@72
    iput-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_internalState:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@74
    .line 319
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@76
    const-string v1, "XTWiFiLP"

    #@78
    const-string v2, "handleEnableLocationTracking not in available state. ignore request"

    #@7a
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@7d
    goto :goto_58

    #@7e
    .line 326
    :cond_7e
    sget-object v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;->IS_IDLE:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@80
    iput-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_internalState:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@82
    goto :goto_58
.end method

.method public handleEnableNlp()V
    .registers 4

    #@0
    .prologue
    .line 258
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@2
    const-string v1, "XTWiFiLP"

    #@4
    const-string v2, "handleEnable"

    #@6
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 259
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_internalState:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@b
    sget-object v1, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;->IS_DISABLED:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@d
    if-ne v0, v1, :cond_13

    #@f
    .line 262
    sget-object v0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;->IS_IDLE:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@11
    iput-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_internalState:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$InternalState;

    #@13
    .line 264
    :cond_13
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 5
    .parameter "intent"

    #@0
    .prologue
    .line 165
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@2
    const-string v1, "XTWiFiLP"

    #@4
    const-string v2, "onBind"

    #@6
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 166
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_stubBinder:Lcom/android/internal/location/ILocationProvider$Stub;

    #@b
    return-object v0
.end method

.method public onCreate()V
    .registers 5

    #@0
    .prologue
    .line 171
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@2
    const-string v1, "XTWiFiLP"

    #@4
    const-string v2, "onCreate"

    #@6
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 176
    const-string v0, "location"

    #@b
    invoke-virtual {p0, v0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Landroid/location/LocationManager;

    #@11
    iput-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_publicLocationManager:Landroid/location/LocationManager;

    #@13
    .line 177
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_publicLocationManager:Landroid/location/LocationManager;

    #@15
    if-nez v0, :cond_20

    #@17
    .line 179
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@19
    const-string v1, "XTWiFiLP"

    #@1b
    const-string v2, "Unable to get public LOCATION_SERVICE"

    #@1d
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@20
    .line 182
    :cond_20
    iget-boolean v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_isEnabledFixReqOnFreeWiFiScan:Z

    #@22
    if-eqz v0, :cond_3e

    #@24
    .line 184
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@26
    const-string v1, "XTWiFiLP"

    #@28
    const-string v2, "Installing wifi scan result receiver"

    #@2a
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@2d
    .line 187
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_WiFiScanResultReceiver:Landroid/content/BroadcastReceiver;

    #@2f
    new-instance v1, Landroid/content/IntentFilter;

    #@31
    const-string v2, "android.net.wifi.SCAN_RESULTS"

    #@33
    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@36
    const-string v2, "android.permission.BIND_DEVICE_ADMIN"

    #@38
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->m_handler:Landroid/os/Handler;

    #@3a
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@3d
    .line 195
    :goto_3d
    return-void

    #@3e
    .line 193
    :cond_3e
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@40
    const-string v1, "XTWiFiLP"

    #@42
    const-string v2, "Skip installing wifi scan result receiver"

    #@44
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@47
    goto :goto_3d
.end method

.method public onDestroy()V
    .registers 4

    #@0
    .prologue
    .line 199
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->llog:Lcom/qualcomm/lib/location/log/LocLog;

    #@2
    const-string v1, "XTWiFiLP"

    #@4
    const-string v2, "onDestroy"

    #@6
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 200
    return-void
.end method
