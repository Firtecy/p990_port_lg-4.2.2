.class Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$TimedLocationListener;
.super Ljava/lang/Object;
.source "XTWiFiOsAgent.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TimedLocationListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;


# direct methods
.method constructor <init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 424
    iput-object p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$TimedLocationListener;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .registers 6
    .parameter "location"

    #@0
    .prologue
    .line 428
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    #@3
    move-result v0

    #@4
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$TimedLocationListener;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@6
    invoke-static {v1}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$1800(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)F

    #@9
    move-result v1

    #@a
    cmpg-float v0, v0, v1

    #@c
    if-gez v0, :cond_5a

    #@e
    .line 431
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$TimedLocationListener;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@10
    invoke-static {v0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$1900(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Landroid/location/LocationManager;

    #@13
    move-result-object v0

    #@14
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$TimedLocationListener;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@16
    iget-object v1, v1, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->mTimedListener:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$TimedLocationListener;

    #@18
    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    #@1b
    .line 432
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$TimedLocationListener;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@1d
    invoke-static {v0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$200(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Landroid/os/Handler;

    #@20
    move-result-object v0

    #@21
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$TimedLocationListener;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@23
    invoke-static {v1}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$2000(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Ljava/lang/Runnable;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@2a
    .line 433
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$TimedLocationListener;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@2c
    const/4 v1, 0x0

    #@2d
    invoke-static {v0, v1}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$2102(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;Z)Z

    #@30
    .line 434
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$TimedLocationListener;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@32
    invoke-static {v0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$700(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Lcom/qualcomm/lib/location/log/LocLog;

    #@35
    move-result-object v0

    #@36
    const-string v1, "XTWiFiOS"

    #@38
    new-instance v2, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v3, "Accuracy threshold ( "

    #@3f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v2

    #@43
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$TimedLocationListener;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@45
    invoke-static {v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$1800(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)F

    #@48
    move-result v3

    #@49
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v2

    #@4d
    const-string v3, " m ) has been met, stop."

    #@4f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v2

    #@53
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v2

    #@57
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@5a
    .line 436
    :cond_5a
    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .registers 2
    .parameter "provider"

    #@0
    .prologue
    .line 440
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .registers 2
    .parameter "provider"

    #@0
    .prologue
    .line 444
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .registers 4
    .parameter "provider"
    .parameter "status"
    .parameter "extras"

    #@0
    .prologue
    .line 448
    return-void
.end method
