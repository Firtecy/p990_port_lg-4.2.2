.class Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$ApiLevel17;
.super Ljava/lang/Object;
.source "XTWiFiOsAgent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ApiLevel17"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;


# direct methods
.method private constructor <init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 125
    iput-object p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$ApiLevel17;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 125
    invoke-direct {p0, p1}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$ApiLevel17;-><init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)V

    #@3
    return-void
.end method


# virtual methods
.method public getIsAirplaneModeOn()Z
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/provider/Settings$SettingNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 129
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$ApiLevel17;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@3
    invoke-static {v1}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$000(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Landroid/content/ContentResolver;

    #@6
    move-result-object v1

    #@7
    const-string v2, "airplane_mode_on"

    #@9
    invoke-static {v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    #@c
    move-result v1

    #@d
    if-ne v1, v0, :cond_10

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method

.method public installObservers()V
    .registers 13

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v11, 0x2

    #@2
    const/4 v10, 0x0

    #@3
    const/4 v9, 0x1

    #@4
    .line 134
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$ApiLevel17;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@6
    invoke-static {v0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$000(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Landroid/content/ContentResolver;

    #@9
    move-result-object v0

    #@a
    sget-object v1, Landroid/provider/Settings$Secure;->CONTENT_URI:Landroid/net/Uri;

    #@c
    new-array v2, v11, [Ljava/lang/String;

    #@e
    const-string v3, "name"

    #@10
    aput-object v3, v2, v10

    #@12
    const-string v3, "value"

    #@14
    aput-object v3, v2, v9

    #@16
    const-string v3, "(name=?) "

    #@18
    new-array v4, v9, [Ljava/lang/String;

    #@1a
    const-string v8, "location_providers_allowed"

    #@1c
    aput-object v8, v4, v10

    #@1e
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@21
    move-result-object v7

    #@22
    .line 137
    .local v7, secureSettingsCursor:Landroid/database/Cursor;
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$ApiLevel17;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@24
    new-instance v1, Landroid/content/ContentQueryMap;

    #@26
    const-string v2, "name"

    #@28
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$ApiLevel17;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@2a
    invoke-static {v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$200(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Landroid/os/Handler;

    #@2d
    move-result-object v3

    #@2e
    invoke-direct {v1, v7, v2, v9, v3}, Landroid/content/ContentQueryMap;-><init>(Landroid/database/Cursor;Ljava/lang/String;ZLandroid/os/Handler;)V

    #@31
    invoke-static {v0, v1}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$102(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;Landroid/content/ContentQueryMap;)Landroid/content/ContentQueryMap;

    #@34
    .line 138
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$ApiLevel17;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@36
    invoke-static {v0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$100(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Landroid/content/ContentQueryMap;

    #@39
    move-result-object v0

    #@3a
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$ApiLevel17;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@3c
    invoke-static {v1}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$300(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$SecureSettingsObserver;

    #@3f
    move-result-object v1

    #@40
    invoke-virtual {v0, v1}, Landroid/content/ContentQueryMap;->addObserver(Ljava/util/Observer;)V

    #@43
    .line 140
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$ApiLevel17;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@45
    invoke-static {v0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$000(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Landroid/content/ContentResolver;

    #@48
    move-result-object v0

    #@49
    sget-object v1, Landroid/provider/Settings$Global;->CONTENT_URI:Landroid/net/Uri;

    #@4b
    new-array v2, v11, [Ljava/lang/String;

    #@4d
    const-string v3, "name"

    #@4f
    aput-object v3, v2, v10

    #@51
    const-string v3, "value"

    #@53
    aput-object v3, v2, v9

    #@55
    const-string v3, "(name=?) or (name=?)"

    #@57
    new-array v4, v9, [Ljava/lang/String;

    #@59
    const-string v8, "airplane_mode_on"

    #@5b
    aput-object v8, v4, v10

    #@5d
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@60
    move-result-object v6

    #@61
    .line 143
    .local v6, globalSettingsCursor:Landroid/database/Cursor;
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$ApiLevel17;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@63
    new-instance v1, Landroid/content/ContentQueryMap;

    #@65
    const-string v2, "name"

    #@67
    iget-object v3, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$ApiLevel17;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@69
    invoke-static {v3}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$200(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Landroid/os/Handler;

    #@6c
    move-result-object v3

    #@6d
    invoke-direct {v1, v6, v2, v9, v3}, Landroid/content/ContentQueryMap;-><init>(Landroid/database/Cursor;Ljava/lang/String;ZLandroid/os/Handler;)V

    #@70
    invoke-static {v0, v1}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$402(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;Landroid/content/ContentQueryMap;)Landroid/content/ContentQueryMap;

    #@73
    .line 144
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$ApiLevel17;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@75
    invoke-static {v0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$400(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Landroid/content/ContentQueryMap;

    #@78
    move-result-object v0

    #@79
    iget-object v1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$ApiLevel17;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@7b
    invoke-static {v1}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$500(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$GlobalSettingsObserver;

    #@7e
    move-result-object v1

    #@7f
    invoke-virtual {v0, v1}, Landroid/content/ContentQueryMap;->addObserver(Ljava/util/Observer;)V

    #@82
    .line 145
    return-void
.end method
