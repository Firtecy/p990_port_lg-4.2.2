.class final Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;
.super Ljava/lang/Thread;
.source "XTWiFiLocationProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "NetworkLocationProviderThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;


# direct methods
.method private constructor <init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1296
    iput-object p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@2
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1296
    invoke-direct {p0, p1}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;-><init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)V

    #@3
    return-void
.end method


# virtual methods
.method public run()V
    .registers 11

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 1301
    const/4 v2, 0x0

    #@3
    .line 1302
    .local v2, inFileStream:Ljava/io/FileInputStream;
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    #@6
    .line 1304
    new-instance v0, Ljava/util/Properties;

    #@8
    invoke-direct {v0}, Ljava/util/Properties;-><init>()V

    #@b
    .line 1307
    .local v0, config:Ljava/util/Properties;
    :try_start_b
    new-instance v3, Ljava/io/FileInputStream;

    #@d
    const-string v6, "/system/etc/xtwifi.conf"

    #@f
    invoke-direct {v3, v6}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_12
    .catchall {:try_start_b .. :try_end_12} :catchall_1b5
    .catch Ljava/io/FileNotFoundException; {:try_start_b .. :try_end_12} :catch_188
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_12} :catch_197
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_12} :catch_1a6

    #@12
    .line 1308
    .end local v2           #inFileStream:Ljava/io/FileInputStream;
    .local v3, inFileStream:Ljava/io/FileInputStream;
    :try_start_12
    invoke-virtual {v0, v3}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    #@15
    .line 1312
    iget-object v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@17
    const-string v7, "DEBUG_GLOBAL_LOG_LEVEL"

    #@19
    iget-object v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@1b
    invoke-static {v8}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$2200(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)I

    #@1e
    move-result v8

    #@1f
    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@22
    move-result-object v8

    #@23
    invoke-virtual {v0, v7, v8}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@26
    move-result-object v7

    #@27
    invoke-static {v7}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    #@2a
    move-result-object v7

    #@2b
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    #@2e
    move-result v7

    #@2f
    invoke-static {v6, v7}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$2202(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;I)I

    #@32
    .line 1313
    iget-object v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@34
    invoke-static {v6}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$000(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Lcom/qualcomm/lib/location/log/LocLog;

    #@37
    move-result-object v6

    #@38
    iget-object v7, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@3a
    invoke-static {v7}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$2200(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)I

    #@3d
    move-result v7

    #@3e
    invoke-virtual {v6, v7}, Lcom/qualcomm/lib/location/log/LocLog;->setLevel(I)V

    #@41
    .line 1315
    iget-object v7, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@43
    const-string v8, "XTRAT_WWAN_LITE_FEATURE_CONTROL"

    #@45
    iget-object v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@47
    invoke-static {v6}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$800(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Z

    #@4a
    move-result v6

    #@4b
    if-eqz v6, :cond_16e

    #@4d
    const-string v6, "1"

    #@4f
    :goto_4f
    invoke-virtual {v0, v8, v6}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@52
    move-result-object v6

    #@53
    invoke-static {v6}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    #@56
    move-result-object v6

    #@57
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    #@5a
    move-result v6

    #@5b
    if-ne v6, v4, :cond_172

    #@5d
    move v6, v4

    #@5e
    :goto_5e
    invoke-static {v7, v6}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$802(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;Z)Z

    #@61
    .line 1316
    iget-object v7, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@63
    const-string v8, "ENABLE_NLP_ZPP_ADAPTOR"

    #@65
    iget-object v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@67
    invoke-static {v6}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$900(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Z

    #@6a
    move-result v6

    #@6b
    if-eqz v6, :cond_175

    #@6d
    const-string v6, "1"

    #@6f
    :goto_6f
    invoke-virtual {v0, v8, v6}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@72
    move-result-object v6

    #@73
    invoke-static {v6}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    #@76
    move-result-object v6

    #@77
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    #@7a
    move-result v6

    #@7b
    if-ne v6, v4, :cond_179

    #@7d
    move v6, v4

    #@7e
    :goto_7e
    invoke-static {v7, v6}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$902(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;Z)Z

    #@81
    .line 1317
    iget-object v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@83
    const-string v7, "TIME_MSEC_NLP_MIN_TIME_BETWEEN_FIX"

    #@85
    iget-object v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@87
    invoke-static {v8}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$2300(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)J

    #@8a
    move-result-wide v8

    #@8b
    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@8e
    move-result-object v8

    #@8f
    invoke-virtual {v0, v7, v8}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@92
    move-result-object v7

    #@93
    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@96
    move-result-wide v7

    #@97
    invoke-static {v6, v7, v8}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$2302(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;J)J

    #@9a
    .line 1319
    iget-object v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@9c
    const-string v7, "TIMEOUT_MSEC_NLP_ZPP_FIX"

    #@9e
    iget-object v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@a0
    invoke-static {v8}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$1300(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)J

    #@a3
    move-result-wide v8

    #@a4
    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@a7
    move-result-object v8

    #@a8
    invoke-virtual {v0, v7, v8}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@ab
    move-result-object v7

    #@ac
    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@af
    move-result-wide v7

    #@b0
    invoke-static {v6, v7, v8}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$1302(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;J)J

    #@b3
    .line 1320
    iget-object v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@b5
    const-string v7, "TIMEOUT_MSEC_NLP_XTWIFI_FINAL_FIX"

    #@b7
    iget-object v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@b9
    invoke-static {v8}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$700(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)J

    #@bc
    move-result-wide v8

    #@bd
    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@c0
    move-result-object v8

    #@c1
    invoke-virtual {v0, v7, v8}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@c4
    move-result-object v7

    #@c5
    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@c8
    move-result-wide v7

    #@c9
    invoke-static {v6, v7, v8}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$702(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;J)J

    #@cc
    .line 1322
    iget-object v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@ce
    const-string v7, "TIME_MSEC_NLP_MAX_AGE_FOR_ZPP_TRANSACTION"

    #@d0
    iget-object v8, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@d2
    invoke-static {v8}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$2400(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)J

    #@d5
    move-result-wide v8

    #@d6
    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@d9
    move-result-object v8

    #@da
    invoke-virtual {v0, v7, v8}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@dd
    move-result-object v7

    #@de
    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@e1
    move-result-wide v7

    #@e2
    invoke-static {v6, v7, v8}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$2402(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;J)J

    #@e5
    .line 1343
    iget-object v7, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@e7
    const-string v8, "ENABLE_NLP_FIX_REQUEST_ON_FREE_WIFI_SCAN"

    #@e9
    iget-object v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@eb
    invoke-static {v6}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$2500(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Z

    #@ee
    move-result v6

    #@ef
    if-eqz v6, :cond_17c

    #@f1
    const-string v6, "1"

    #@f3
    :goto_f3
    invoke-virtual {v0, v8, v6}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@f6
    move-result-object v6

    #@f7
    invoke-static {v6}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    #@fa
    move-result-object v6

    #@fb
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    #@fe
    move-result v6

    #@ff
    if-ne v6, v4, :cond_180

    #@101
    :goto_101
    invoke-static {v7, v4}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$2502(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;Z)Z

    #@104
    .line 1344
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@106
    const-string v5, "TIME_MSEC_NLP_MIN_TIME_BETWEEN_FIX_REQ_ON_FREE_WIFI"

    #@108
    iget-object v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@10a
    invoke-static {v6}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$2600(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)J

    #@10d
    move-result-wide v6

    #@10e
    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@111
    move-result-object v6

    #@112
    invoke-virtual {v0, v5, v6}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@115
    move-result-object v5

    #@116
    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@119
    move-result-wide v5

    #@11a
    invoke-static {v4, v5, v6}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$2602(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;J)J

    #@11d
    .line 1346
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@11f
    const-string v5, "TIME_MSEC_NLP_MAX_TIME_BETWEEN_WIFI_SCAN_AND_FREE_RIDE_REQ"

    #@121
    iget-object v6, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@123
    invoke-static {v6}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$2700(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)J

    #@126
    move-result-wide v6

    #@127
    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@12a
    move-result-object v6

    #@12b
    invoke-virtual {v0, v5, v6}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@12e
    move-result-object v5

    #@12f
    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@132
    move-result-wide v5

    #@133
    invoke-static {v4, v5, v6}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$2702(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;J)J
    :try_end_136
    .catchall {:try_start_12 .. :try_end_136} :catchall_1c1
    .catch Ljava/io/FileNotFoundException; {:try_start_12 .. :try_end_136} :catch_1ca
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_136} :catch_1c7
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_136} :catch_1c4

    #@136
    .line 1365
    if-eqz v3, :cond_13b

    #@138
    .line 1366
    :try_start_138
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_13b
    .catch Ljava/io/IOException; {:try_start_138 .. :try_end_13b} :catch_182

    #@13b
    :cond_13b
    move-object v2, v3

    #@13c
    .line 1374
    .end local v3           #inFileStream:Ljava/io/FileInputStream;
    .restart local v2       #inFileStream:Ljava/io/FileInputStream;
    :cond_13c
    :goto_13c
    invoke-static {}, Landroid/os/Looper;->prepare()V

    #@13f
    .line 1376
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@141
    invoke-static {v4}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$000(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Lcom/qualcomm/lib/location/log/LocLog;

    #@144
    move-result-object v4

    #@145
    const-string v5, "XTWiFiLP"

    #@147
    const-string v6, "entering Looper"

    #@149
    invoke-virtual {v4, v5, v6}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@14c
    .line 1378
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@14e
    new-instance v5, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread$1;

    #@150
    invoke-direct {v5, p0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread$1;-><init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;)V

    #@153
    invoke-static {v4, v5}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$102(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;Landroid/os/Handler;)Landroid/os/Handler;

    #@156
    .line 1460
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@158
    invoke-static {v4}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$100(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Landroid/os/Handler;

    #@15b
    move-result-object v4

    #@15c
    const/16 v5, 0x64

    #@15e
    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@161
    .line 1462
    iget-object v4, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider$NetworkLocationProviderThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;

    #@163
    invoke-static {v4}, Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;->access$3400(Lcom/qualcomm/services/location/xtwifi/XTWiFiLocationProvider;)Ljava/util/concurrent/CountDownLatch;

    #@166
    move-result-object v4

    #@167
    invoke-virtual {v4}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    #@16a
    .line 1463
    invoke-static {}, Landroid/os/Looper;->loop()V

    #@16d
    .line 1464
    return-void

    #@16e
    .line 1315
    .end local v2           #inFileStream:Ljava/io/FileInputStream;
    .restart local v3       #inFileStream:Ljava/io/FileInputStream;
    :cond_16e
    :try_start_16e
    const-string v6, "0"

    #@170
    goto/16 :goto_4f

    #@172
    :cond_172
    move v6, v5

    #@173
    goto/16 :goto_5e

    #@175
    .line 1316
    :cond_175
    const-string v6, "0"

    #@177
    goto/16 :goto_6f

    #@179
    :cond_179
    move v6, v5

    #@17a
    goto/16 :goto_7e

    #@17c
    .line 1343
    :cond_17c
    const-string v6, "0"
    :try_end_17e
    .catchall {:try_start_16e .. :try_end_17e} :catchall_1c1
    .catch Ljava/io/FileNotFoundException; {:try_start_16e .. :try_end_17e} :catch_1ca
    .catch Ljava/io/IOException; {:try_start_16e .. :try_end_17e} :catch_1c7
    .catch Ljava/lang/Exception; {:try_start_16e .. :try_end_17e} :catch_1c4

    #@17e
    goto/16 :goto_f3

    #@180
    :cond_180
    move v4, v5

    #@181
    goto :goto_101

    #@182
    .line 1368
    :catch_182
    move-exception v1

    #@183
    .line 1370
    .local v1, e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@186
    move-object v2, v3

    #@187
    .line 1372
    .end local v3           #inFileStream:Ljava/io/FileInputStream;
    .restart local v2       #inFileStream:Ljava/io/FileInputStream;
    goto :goto_13c

    #@188
    .line 1349
    .end local v1           #e:Ljava/io/IOException;
    :catch_188
    move-exception v1

    #@189
    .line 1351
    .local v1, e:Ljava/io/FileNotFoundException;
    :goto_189
    :try_start_189
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_18c
    .catchall {:try_start_189 .. :try_end_18c} :catchall_1b5

    #@18c
    .line 1365
    if-eqz v2, :cond_13c

    #@18e
    .line 1366
    :try_start_18e
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_191
    .catch Ljava/io/IOException; {:try_start_18e .. :try_end_191} :catch_192

    #@191
    goto :goto_13c

    #@192
    .line 1368
    :catch_192
    move-exception v1

    #@193
    .line 1370
    .local v1, e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@196
    goto :goto_13c

    #@197
    .line 1353
    .end local v1           #e:Ljava/io/IOException;
    :catch_197
    move-exception v1

    #@198
    .line 1355
    .restart local v1       #e:Ljava/io/IOException;
    :goto_198
    :try_start_198
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_19b
    .catchall {:try_start_198 .. :try_end_19b} :catchall_1b5

    #@19b
    .line 1365
    if-eqz v2, :cond_13c

    #@19d
    .line 1366
    :try_start_19d
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_1a0
    .catch Ljava/io/IOException; {:try_start_19d .. :try_end_1a0} :catch_1a1

    #@1a0
    goto :goto_13c

    #@1a1
    .line 1368
    :catch_1a1
    move-exception v1

    #@1a2
    .line 1370
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@1a5
    goto :goto_13c

    #@1a6
    .line 1357
    .end local v1           #e:Ljava/io/IOException;
    :catch_1a6
    move-exception v1

    #@1a7
    .line 1359
    .local v1, e:Ljava/lang/Exception;
    :goto_1a7
    :try_start_1a7
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1aa
    .catchall {:try_start_1a7 .. :try_end_1aa} :catchall_1b5

    #@1aa
    .line 1365
    if-eqz v2, :cond_13c

    #@1ac
    .line 1366
    :try_start_1ac
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_1af
    .catch Ljava/io/IOException; {:try_start_1ac .. :try_end_1af} :catch_1b0

    #@1af
    goto :goto_13c

    #@1b0
    .line 1368
    :catch_1b0
    move-exception v1

    #@1b1
    .line 1370
    .local v1, e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@1b4
    goto :goto_13c

    #@1b5
    .line 1363
    .end local v1           #e:Ljava/io/IOException;
    :catchall_1b5
    move-exception v4

    #@1b6
    .line 1365
    :goto_1b6
    if-eqz v2, :cond_1bb

    #@1b8
    .line 1366
    :try_start_1b8
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_1bb
    .catch Ljava/io/IOException; {:try_start_1b8 .. :try_end_1bb} :catch_1bc

    #@1bb
    .line 1371
    :cond_1bb
    :goto_1bb
    throw v4

    #@1bc
    .line 1368
    :catch_1bc
    move-exception v1

    #@1bd
    .line 1370
    .restart local v1       #e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@1c0
    goto :goto_1bb

    #@1c1
    .line 1363
    .end local v1           #e:Ljava/io/IOException;
    .end local v2           #inFileStream:Ljava/io/FileInputStream;
    .restart local v3       #inFileStream:Ljava/io/FileInputStream;
    :catchall_1c1
    move-exception v4

    #@1c2
    move-object v2, v3

    #@1c3
    .end local v3           #inFileStream:Ljava/io/FileInputStream;
    .restart local v2       #inFileStream:Ljava/io/FileInputStream;
    goto :goto_1b6

    #@1c4
    .line 1357
    .end local v2           #inFileStream:Ljava/io/FileInputStream;
    .restart local v3       #inFileStream:Ljava/io/FileInputStream;
    :catch_1c4
    move-exception v1

    #@1c5
    move-object v2, v3

    #@1c6
    .end local v3           #inFileStream:Ljava/io/FileInputStream;
    .restart local v2       #inFileStream:Ljava/io/FileInputStream;
    goto :goto_1a7

    #@1c7
    .line 1353
    .end local v2           #inFileStream:Ljava/io/FileInputStream;
    .restart local v3       #inFileStream:Ljava/io/FileInputStream;
    :catch_1c7
    move-exception v1

    #@1c8
    move-object v2, v3

    #@1c9
    .end local v3           #inFileStream:Ljava/io/FileInputStream;
    .restart local v2       #inFileStream:Ljava/io/FileInputStream;
    goto :goto_198

    #@1ca
    .line 1349
    .end local v2           #inFileStream:Ljava/io/FileInputStream;
    .restart local v3       #inFileStream:Ljava/io/FileInputStream;
    :catch_1ca
    move-exception v1

    #@1cb
    move-object v2, v3

    #@1cc
    .end local v3           #inFileStream:Ljava/io/FileInputStream;
    .restart local v2       #inFileStream:Ljava/io/FileInputStream;
    goto :goto_189
.end method
