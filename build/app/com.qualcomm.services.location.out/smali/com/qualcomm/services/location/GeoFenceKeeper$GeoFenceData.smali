.class Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;
.super Ljava/lang/Object;
.source "GeoFenceKeeper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/services/location/GeoFenceKeeper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GeoFenceData"
.end annotation


# instance fields
.field private final mActTransID:Ljava/lang/Integer;

.field private final mClient:Lcom/qualcomm/services/location/GeoFenceServlet;

.field private mEngID:Ljava/lang/Integer;

.field private final mLifeInTimeUnit:Ljava/lang/Long;

.field private final mParams:Landroid/location/GeoFenceParams;

.field final synthetic this$0:Lcom/qualcomm/services/location/GeoFenceKeeper;


# direct methods
.method private constructor <init>(Lcom/qualcomm/services/location/GeoFenceKeeper;Lcom/qualcomm/services/location/GeoFenceServlet;Landroid/location/GeoFenceParams;)V
    .registers 8
    .parameter
    .parameter "client"
    .parameter "params"

    #@0
    .prologue
    const-wide/16 v2, -0x1

    #@2
    .line 150
    iput-object p1, p0, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->this$0:Lcom/qualcomm/services/location/GeoFenceKeeper;

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    .line 151
    iput-object p2, p0, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->mClient:Lcom/qualcomm/services/location/GeoFenceServlet;

    #@9
    .line 152
    iput-object p3, p0, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->mParams:Landroid/location/GeoFenceParams;

    #@b
    .line 153
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->mParams:Landroid/location/GeoFenceParams;

    #@d
    iget-wide v0, v0, Landroid/location/GeoFenceParams;->mExpiration:J

    #@f
    cmp-long v0, v2, v0

    #@11
    if-nez v0, :cond_2d

    #@13
    .line 154
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@16
    move-result-object v0

    #@17
    iput-object v0, p0, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->mLifeInTimeUnit:Ljava/lang/Long;

    #@19
    .line 158
    :goto_19
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@1c
    move-result-object v1

    #@1d
    monitor-enter v1

    #@1e
    .line 159
    :try_start_1e
    invoke-static {}, Lcom/qualcomm/services/location/GeoFenceKeeper;->access$908()I

    #@21
    move-result v0

    #@22
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@25
    move-result-object v0

    #@26
    iput-object v0, p0, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->mActTransID:Ljava/lang/Integer;

    #@28
    .line 160
    monitor-exit v1
    :try_end_29
    .catchall {:try_start_1e .. :try_end_29} :catchall_3c

    #@29
    .line 161
    const/4 v0, 0x0

    #@2a
    iput-object v0, p0, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->mEngID:Ljava/lang/Integer;

    #@2c
    .line 162
    return-void

    #@2d
    .line 156
    :cond_2d
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->mParams:Landroid/location/GeoFenceParams;

    #@2f
    iget-wide v0, v0, Landroid/location/GeoFenceParams;->mExpiration:J

    #@31
    invoke-static {v0, v1}, Lcom/qualcomm/services/location/GeoFenceKeeper;->access$800(J)J

    #@34
    move-result-wide v0

    #@35
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@38
    move-result-object v0

    #@39
    iput-object v0, p0, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->mLifeInTimeUnit:Ljava/lang/Long;

    #@3b
    goto :goto_19

    #@3c
    .line 160
    :catchall_3c
    move-exception v0

    #@3d
    :try_start_3d
    monitor-exit v1
    :try_end_3e
    .catchall {:try_start_3d .. :try_end_3e} :catchall_3c

    #@3e
    throw v0
.end method

.method synthetic constructor <init>(Lcom/qualcomm/services/location/GeoFenceKeeper;Lcom/qualcomm/services/location/GeoFenceServlet;Landroid/location/GeoFenceParams;Lcom/qualcomm/services/location/GeoFenceKeeper$1;)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 143
    invoke-direct {p0, p1, p2, p3}, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;-><init>(Lcom/qualcomm/services/location/GeoFenceKeeper;Lcom/qualcomm/services/location/GeoFenceServlet;Landroid/location/GeoFenceParams;)V

    #@3
    return-void
.end method

.method static synthetic access$1000(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;)Landroid/location/GeoFenceParams;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 143
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->mParams:Landroid/location/GeoFenceParams;

    #@2
    return-object v0
.end method

.method static synthetic access$1100(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;)Ljava/lang/Long;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 143
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->mLifeInTimeUnit:Ljava/lang/Long;

    #@2
    return-object v0
.end method

.method static synthetic access$1200(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;)Ljava/lang/Integer;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 143
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->mActTransID:Ljava/lang/Integer;

    #@2
    return-object v0
.end method

.method static synthetic access$1300(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;)Ljava/lang/Integer;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 143
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->mEngID:Ljava/lang/Integer;

    #@2
    return-object v0
.end method

.method static synthetic access$1302(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;Ljava/lang/Integer;)Ljava/lang/Integer;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 143
    iput-object p1, p0, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->mEngID:Ljava/lang/Integer;

    #@2
    return-object p1
.end method

.method static synthetic access$1400(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;)Lcom/qualcomm/services/location/GeoFenceServlet;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 143
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->mClient:Lcom/qualcomm/services/location/GeoFenceServlet;

    #@2
    return-object v0
.end method
