.class final Lcom/qualcomm/services/location/GeoFenceKeeper$MyHandler;
.super Landroid/os/Handler;
.source "GeoFenceKeeper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/services/location/GeoFenceKeeper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MyHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/services/location/GeoFenceKeeper;


# direct methods
.method constructor <init>(Lcom/qualcomm/services/location/GeoFenceKeeper;Landroid/os/Looper;)V
    .registers 3
    .parameter
    .parameter "looper"

    #@0
    .prologue
    .line 108
    iput-object p1, p0, Lcom/qualcomm/services/location/GeoFenceKeeper$MyHandler;->this$0:Lcom/qualcomm/services/location/GeoFenceKeeper;

    #@2
    .line 109
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 110
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 6
    .parameter "msg"

    #@0
    .prologue
    .line 113
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    .line 114
    .local v0, message:I
    const-string v2, "GeoFenceKeeper"

    #@4
    new-instance v1, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v3, "handleMessage what - "

    #@b
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v3

    #@f
    invoke-static {}, Lcom/qualcomm/services/location/GeoFenceKeeper;->access$100()[Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    array-length v1, v1

    #@14
    if-ge v0, v1, :cond_43

    #@16
    invoke-static {}, Lcom/qualcomm/services/location/GeoFenceKeeper;->access$100()[Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    aget-object v1, v1, v0

    #@1c
    :goto_1c
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 118
    packed-switch v0, :pswitch_data_6c

    #@2a
    .line 138
    const-string v1, "GeoFenceKeeper"

    #@2c
    new-instance v2, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v3, "known message "

    #@33
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v2

    #@3b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v2

    #@3f
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    .line 140
    :goto_42
    return-void

    #@43
    .line 114
    :cond_43
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@46
    move-result-object v1

    #@47
    goto :goto_1c

    #@48
    .line 120
    :pswitch_48
    iget-object v1, p0, Lcom/qualcomm/services/location/GeoFenceKeeper$MyHandler;->this$0:Lcom/qualcomm/services/location/GeoFenceKeeper;

    #@4a
    invoke-static {v1, p1}, Lcom/qualcomm/services/location/GeoFenceKeeper;->access$200(Lcom/qualcomm/services/location/GeoFenceKeeper;Landroid/os/Message;)V

    #@4d
    goto :goto_42

    #@4e
    .line 123
    :pswitch_4e
    iget-object v1, p0, Lcom/qualcomm/services/location/GeoFenceKeeper$MyHandler;->this$0:Lcom/qualcomm/services/location/GeoFenceKeeper;

    #@50
    invoke-static {v1, p1}, Lcom/qualcomm/services/location/GeoFenceKeeper;->access$300(Lcom/qualcomm/services/location/GeoFenceKeeper;Landroid/os/Message;)V

    #@53
    goto :goto_42

    #@54
    .line 126
    :pswitch_54
    iget-object v1, p0, Lcom/qualcomm/services/location/GeoFenceKeeper$MyHandler;->this$0:Lcom/qualcomm/services/location/GeoFenceKeeper;

    #@56
    invoke-static {v1, p1}, Lcom/qualcomm/services/location/GeoFenceKeeper;->access$400(Lcom/qualcomm/services/location/GeoFenceKeeper;Landroid/os/Message;)V

    #@59
    goto :goto_42

    #@5a
    .line 129
    :pswitch_5a
    iget-object v1, p0, Lcom/qualcomm/services/location/GeoFenceKeeper$MyHandler;->this$0:Lcom/qualcomm/services/location/GeoFenceKeeper;

    #@5c
    invoke-static {v1, p1}, Lcom/qualcomm/services/location/GeoFenceKeeper;->access$500(Lcom/qualcomm/services/location/GeoFenceKeeper;Landroid/os/Message;)V

    #@5f
    goto :goto_42

    #@60
    .line 132
    :pswitch_60
    iget-object v1, p0, Lcom/qualcomm/services/location/GeoFenceKeeper$MyHandler;->this$0:Lcom/qualcomm/services/location/GeoFenceKeeper;

    #@62
    invoke-static {v1, p1}, Lcom/qualcomm/services/location/GeoFenceKeeper;->access$600(Lcom/qualcomm/services/location/GeoFenceKeeper;Landroid/os/Message;)V

    #@65
    goto :goto_42

    #@66
    .line 135
    :pswitch_66
    iget-object v1, p0, Lcom/qualcomm/services/location/GeoFenceKeeper$MyHandler;->this$0:Lcom/qualcomm/services/location/GeoFenceKeeper;

    #@68
    invoke-static {v1, p1}, Lcom/qualcomm/services/location/GeoFenceKeeper;->access$700(Lcom/qualcomm/services/location/GeoFenceKeeper;Landroid/os/Message;)V

    #@6b
    goto :goto_42

    #@6c
    .line 118
    :pswitch_data_6c
    .packed-switch 0x0
        :pswitch_48
        :pswitch_4e
        :pswitch_54
        :pswitch_5a
        :pswitch_60
        :pswitch_66
    .end packed-switch
.end method
