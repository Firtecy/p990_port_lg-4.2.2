.class public Lcom/qualcomm/services/location/LocationAlarmReceiver;
.super Landroid/content/BroadcastReceiver;
.source "LocationAlarmReceiver.java"


# static fields
.field public static ACTION:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 22
    const-string v0, "com.qualcomm.services.location.ALARM"

    #@2
    sput-object v0, Lcom/qualcomm/services/location/LocationAlarmReceiver;->ACTION:Ljava/lang/String;

    #@4
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 21
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 10
    .parameter "ctxt"
    .parameter "intent"

    #@0
    .prologue
    .line 26
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@3
    move-result-wide v2

    #@4
    .local v2, rtcTime:J
    move-object v5, p1

    #@5
    .line 27
    check-cast v5, Lcom/qualcomm/services/location/LocationAlarmService;

    #@7
    invoke-virtual {v5}, Lcom/qualcomm/services/location/LocationAlarmService;->setNextAlarm()V

    #@a
    .line 28
    new-instance v5, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v6, "Alarm worked at "

    #@11
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v5

    #@15
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@18
    move-result-object v6

    #@19
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v5

    #@1d
    const-string v6, "."

    #@1f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v5

    #@23
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v0

    #@27
    .line 29
    .local v0, message:Ljava/lang/String;
    const-string v5, "LAlarm"

    #@29
    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 30
    const-string v5, "power"

    #@2e
    invoke-virtual {p1, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@31
    move-result-object v1

    #@32
    check-cast v1, Landroid/os/PowerManager;

    #@34
    .line 31
    .local v1, pm:Landroid/os/PowerManager;
    const/4 v5, 0x1

    #@35
    sget-object v6, Lcom/qualcomm/services/location/LocationAlarmReceiver;->ACTION:Ljava/lang/String;

    #@37
    invoke-virtual {v1, v5, v6}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@3a
    move-result-object v4

    #@3b
    .line 33
    .local v4, wl:Landroid/os/PowerManager$WakeLock;
    const-wide/16 v5, 0xa

    #@3d
    invoke-virtual {v4, v5, v6}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    #@40
    .line 34
    return-void
.end method
