.class final Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;
.super Ljava/lang/Thread;
.source "ZppAdaptor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ZppAdaptorThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;


# direct methods
.method private constructor <init>(Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 312
    iput-object p1, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;->this$0:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;

    #@2
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 312
    invoke-direct {p0, p1}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;-><init>(Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;)V

    #@3
    return-void
.end method


# virtual methods
.method public run()V
    .registers 4

    #@0
    .prologue
    .line 317
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    #@3
    .line 318
    invoke-static {}, Landroid/os/Looper;->prepare()V

    #@6
    .line 320
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;->this$0:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;

    #@8
    invoke-static {v0}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->access$100(Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;)Lcom/qualcomm/lib/location/log/LocLog;

    #@b
    move-result-object v0

    #@c
    const-string v1, "XTWiFiZpp"

    #@e
    const-string v2, "entering Looper"

    #@10
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@13
    .line 322
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;->this$0:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;

    #@15
    new-instance v1, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread$1;

    #@17
    invoke-direct {v1, p0}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread$1;-><init>(Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;)V

    #@1a
    invoke-static {v0, v1}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->access$202(Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;Landroid/os/Handler;)Landroid/os/Handler;

    #@1d
    .line 404
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor$ZppAdaptorThread;->this$0:Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;

    #@1f
    invoke-static {v0}, Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;->access$1100(Lcom/qualcomm/services/location/xtwifi/ZppAdaptor;)Ljava/util/concurrent/CountDownLatch;

    #@22
    move-result-object v0

    #@23
    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    #@26
    .line 405
    invoke-static {}, Landroid/os/Looper;->loop()V

    #@29
    .line 406
    return-void
.end method
