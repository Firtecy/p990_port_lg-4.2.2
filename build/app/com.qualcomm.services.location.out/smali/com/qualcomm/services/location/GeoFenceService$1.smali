.class Lcom/qualcomm/services/location/GeoFenceService$1;
.super Landroid/location/IGeoFencer$Stub;
.source "GeoFenceService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/services/location/GeoFenceService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/services/location/GeoFenceService;


# direct methods
.method constructor <init>(Lcom/qualcomm/services/location/GeoFenceService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 31
    iput-object p1, p0, Lcom/qualcomm/services/location/GeoFenceService$1;->this$0:Lcom/qualcomm/services/location/GeoFenceService;

    #@2
    invoke-direct {p0}, Landroid/location/IGeoFencer$Stub;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public clearGeoFence(Landroid/os/IBinder;Landroid/app/PendingIntent;)V
    .registers 8
    .parameter "who"
    .parameter "fence"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 50
    iget-object v1, p0, Lcom/qualcomm/services/location/GeoFenceService$1;->this$0:Lcom/qualcomm/services/location/GeoFenceService;

    #@2
    invoke-static {v1}, Lcom/qualcomm/services/location/GeoFenceService;->access$000(Lcom/qualcomm/services/location/GeoFenceService;)Landroid/os/HandlerThread;

    #@5
    move-result-object v2

    #@6
    monitor-enter v2

    #@7
    .line 51
    :try_start_7
    iget-object v1, p0, Lcom/qualcomm/services/location/GeoFenceService$1;->this$0:Lcom/qualcomm/services/location/GeoFenceService;

    #@9
    invoke-static {v1}, Lcom/qualcomm/services/location/GeoFenceService;->access$000(Lcom/qualcomm/services/location/GeoFenceService;)Landroid/os/HandlerThread;

    #@c
    move-result-object v1

    #@d
    if-eqz v1, :cond_38

    #@f
    .line 52
    iget-object v1, p0, Lcom/qualcomm/services/location/GeoFenceService$1;->this$0:Lcom/qualcomm/services/location/GeoFenceService;

    #@11
    invoke-static {v1}, Lcom/qualcomm/services/location/GeoFenceService;->access$100(Lcom/qualcomm/services/location/GeoFenceService;)Lcom/qualcomm/services/location/GeoFenceKeeper;

    #@14
    move-result-object v1

    #@15
    invoke-static {p1, v1}, Lcom/qualcomm/services/location/GeoFenceServlet;->getGeoFenceServlet(Landroid/os/IBinder;Lcom/qualcomm/services/location/GeoFenceKeeper;)Lcom/qualcomm/services/location/GeoFenceServlet;

    #@18
    move-result-object v0

    #@19
    .line 54
    .local v0, servlet:Lcom/qualcomm/services/location/GeoFenceServlet;
    const-string v1, "GeoFenceService"

    #@1b
    new-instance v3, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v4, "clearGeoFence "

    #@22
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {p2}, Landroid/app/PendingIntent;->toString()Ljava/lang/String;

    #@29
    move-result-object v4

    #@2a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v3

    #@2e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v3

    #@32
    invoke-static {v1, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    .line 55
    invoke-virtual {v0, p2}, Lcom/qualcomm/services/location/GeoFenceServlet;->removeGeoFence(Landroid/app/PendingIntent;)V

    #@38
    .line 57
    .end local v0           #servlet:Lcom/qualcomm/services/location/GeoFenceServlet;
    :cond_38
    monitor-exit v2

    #@39
    .line 58
    return-void

    #@3a
    .line 57
    :catchall_3a
    move-exception v1

    #@3b
    monitor-exit v2
    :try_end_3c
    .catchall {:try_start_7 .. :try_end_3c} :catchall_3a

    #@3c
    throw v1
.end method

.method public clearGeoFenceUser(I)V
    .registers 6
    .parameter "uid"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceService$1;->this$0:Lcom/qualcomm/services/location/GeoFenceService;

    #@2
    invoke-static {v0}, Lcom/qualcomm/services/location/GeoFenceService;->access$000(Lcom/qualcomm/services/location/GeoFenceService;)Landroid/os/HandlerThread;

    #@5
    move-result-object v1

    #@6
    monitor-enter v1

    #@7
    .line 64
    :try_start_7
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceService$1;->this$0:Lcom/qualcomm/services/location/GeoFenceService;

    #@9
    invoke-static {v0}, Lcom/qualcomm/services/location/GeoFenceService;->access$000(Lcom/qualcomm/services/location/GeoFenceService;)Landroid/os/HandlerThread;

    #@c
    move-result-object v0

    #@d
    if-eqz v0, :cond_30

    #@f
    .line 65
    const-string v0, "GeoFenceService"

    #@11
    new-instance v2, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v3, "clearGeoFence uid - "

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 66
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceService$1;->this$0:Lcom/qualcomm/services/location/GeoFenceService;

    #@29
    invoke-static {v0}, Lcom/qualcomm/services/location/GeoFenceService;->access$100(Lcom/qualcomm/services/location/GeoFenceService;)Lcom/qualcomm/services/location/GeoFenceKeeper;

    #@2c
    move-result-object v0

    #@2d
    invoke-virtual {v0, p1}, Lcom/qualcomm/services/location/GeoFenceKeeper;->removeGeoFenceApp(I)V

    #@30
    .line 68
    :cond_30
    monitor-exit v1

    #@31
    .line 69
    return-void

    #@32
    .line 68
    :catchall_32
    move-exception v0

    #@33
    monitor-exit v1
    :try_end_34
    .catchall {:try_start_7 .. :try_end_34} :catchall_32

    #@34
    throw v0
.end method

.method public debugCheckGeoFences()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 73
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceService$1;->this$0:Lcom/qualcomm/services/location/GeoFenceService;

    #@2
    invoke-static {v0}, Lcom/qualcomm/services/location/GeoFenceService;->access$000(Lcom/qualcomm/services/location/GeoFenceService;)Landroid/os/HandlerThread;

    #@5
    move-result-object v1

    #@6
    monitor-enter v1

    #@7
    .line 74
    :try_start_7
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceService$1;->this$0:Lcom/qualcomm/services/location/GeoFenceService;

    #@9
    invoke-static {v0}, Lcom/qualcomm/services/location/GeoFenceService;->access$000(Lcom/qualcomm/services/location/GeoFenceService;)Landroid/os/HandlerThread;

    #@c
    move-result-object v0

    #@d
    if-eqz v0, :cond_18

    #@f
    .line 75
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceService$1;->this$0:Lcom/qualcomm/services/location/GeoFenceService;

    #@11
    invoke-static {v0}, Lcom/qualcomm/services/location/GeoFenceService;->access$100(Lcom/qualcomm/services/location/GeoFenceService;)Lcom/qualcomm/services/location/GeoFenceKeeper;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0}, Lcom/qualcomm/services/location/GeoFenceKeeper;->debugCheckGeoFences()V

    #@18
    .line 77
    :cond_18
    monitor-exit v1

    #@19
    .line 78
    return-void

    #@1a
    .line 77
    :catchall_1a
    move-exception v0

    #@1b
    monitor-exit v1
    :try_end_1c
    .catchall {:try_start_7 .. :try_end_1c} :catchall_1a

    #@1c
    throw v0
.end method

.method public setGeoFence(Landroid/os/IBinder;Landroid/location/GeoFenceParams;)Z
    .registers 9
    .parameter "who"
    .parameter "params"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 35
    const/4 v0, 0x0

    #@1
    .line 36
    .local v0, isSet:Z
    iget-object v2, p0, Lcom/qualcomm/services/location/GeoFenceService$1;->this$0:Lcom/qualcomm/services/location/GeoFenceService;

    #@3
    invoke-static {v2}, Lcom/qualcomm/services/location/GeoFenceService;->access$000(Lcom/qualcomm/services/location/GeoFenceService;)Landroid/os/HandlerThread;

    #@6
    move-result-object v3

    #@7
    monitor-enter v3

    #@8
    .line 37
    :try_start_8
    iget-object v2, p0, Lcom/qualcomm/services/location/GeoFenceService$1;->this$0:Lcom/qualcomm/services/location/GeoFenceService;

    #@a
    invoke-static {v2}, Lcom/qualcomm/services/location/GeoFenceService;->access$000(Lcom/qualcomm/services/location/GeoFenceService;)Landroid/os/HandlerThread;

    #@d
    move-result-object v2

    #@e
    if-eqz v2, :cond_3a

    #@10
    .line 38
    iget-object v2, p0, Lcom/qualcomm/services/location/GeoFenceService$1;->this$0:Lcom/qualcomm/services/location/GeoFenceService;

    #@12
    invoke-static {v2}, Lcom/qualcomm/services/location/GeoFenceService;->access$100(Lcom/qualcomm/services/location/GeoFenceService;)Lcom/qualcomm/services/location/GeoFenceKeeper;

    #@15
    move-result-object v2

    #@16
    invoke-static {p1, v2}, Lcom/qualcomm/services/location/GeoFenceServlet;->getGeoFenceServlet(Landroid/os/IBinder;Lcom/qualcomm/services/location/GeoFenceKeeper;)Lcom/qualcomm/services/location/GeoFenceServlet;

    #@19
    move-result-object v1

    #@1a
    .line 40
    .local v1, servlet:Lcom/qualcomm/services/location/GeoFenceServlet;
    const-string v2, "GeoFenceService"

    #@1c
    new-instance v4, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v5, "setGeoFence "

    #@23
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v4

    #@27
    invoke-virtual {p2}, Landroid/location/GeoFenceParams;->toString()Ljava/lang/String;

    #@2a
    move-result-object v5

    #@2b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v4

    #@2f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v4

    #@33
    invoke-static {v2, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 41
    invoke-virtual {v1, p2}, Lcom/qualcomm/services/location/GeoFenceServlet;->addGeoFence(Landroid/location/GeoFenceParams;)Z

    #@39
    move-result v0

    #@3a
    .line 43
    .end local v1           #servlet:Lcom/qualcomm/services/location/GeoFenceServlet;
    :cond_3a
    monitor-exit v3

    #@3b
    .line 44
    return v0

    #@3c
    .line 43
    :catchall_3c
    move-exception v2

    #@3d
    monitor-exit v3
    :try_end_3e
    .catchall {:try_start_8 .. :try_end_3e} :catchall_3c

    #@3e
    throw v2
.end method
