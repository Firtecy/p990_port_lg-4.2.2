.class Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread$1;
.super Landroid/os/Handler;
.source "XTWiFiOsAgent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread;


# direct methods
.method constructor <init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1585
    iput-object p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 7
    .parameter "msg"

    #@0
    .prologue
    .line 1591
    :try_start_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    #@3
    .line 1592
    iget v1, p1, Landroid/os/Message;->what:I

    #@5
    .line 1593
    .local v1, message:I
    packed-switch v1, :pswitch_data_68

    #@8
    .line 1624
    .end local v1           #message:I
    :goto_8
    return-void

    #@9
    .line 1596
    .restart local v1       #message:I
    :pswitch_9
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread;

    #@b
    iget-object v2, v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@d
    invoke-static {v2}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$2500(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_10} :catch_11

    #@10
    goto :goto_8

    #@11
    .line 1619
    .end local v1           #message:I
    :catch_11
    move-exception v0

    #@12
    .line 1621
    .local v0, e:Ljava/lang/Exception;
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread;

    #@14
    iget-object v2, v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@16
    invoke-static {v2}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$700(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Lcom/qualcomm/lib/location/log/LocLog;

    #@19
    move-result-object v2

    #@1a
    const-string v3, "XTWiFiOS"

    #@1c
    const-string v4, "Unchaught exception in message handler"

    #@1e
    invoke-virtual {v2, v3, v4}, Lcom/qualcomm/lib/location/log/LocLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@21
    .line 1622
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@24
    goto :goto_8

    #@25
    .line 1599
    .end local v0           #e:Ljava/lang/Exception;
    .restart local v1       #message:I
    :pswitch_25
    :try_start_25
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread;

    #@27
    iget-object v2, v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@29
    invoke-static {v2}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$700(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Lcom/qualcomm/lib/location/log/LocLog;

    #@2c
    move-result-object v2

    #@2d
    const-string v3, "XTWiFiOS"

    #@2f
    const-string v4, "IPC receiver thread ended, remove local client"

    #@31
    invoke-virtual {v2, v3, v4}, Lcom/qualcomm/lib/location/log/LocLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@34
    .line 1600
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread;

    #@36
    iget-object v2, v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@38
    iget-object v2, v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->m_ipcClient:Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@3a
    if-eqz v2, :cond_4c

    #@3c
    .line 1605
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread;

    #@3e
    iget-object v2, v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@40
    iget-object v2, v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->m_ipcClient:Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@42
    invoke-virtual {v2}, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->disconnect()Z

    #@45
    .line 1606
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread;

    #@47
    iget-object v2, v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@49
    const/4 v3, 0x0

    #@4a
    iput-object v3, v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->m_ipcClient:Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@4c
    .line 1609
    :cond_4c
    const/16 v2, 0x66

    #@4e
    invoke-virtual {p0, v2}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread$1;->removeMessages(I)V

    #@51
    .line 1610
    const/16 v2, 0x64

    #@53
    invoke-virtual {p0, v2}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread$1;->removeMessages(I)V

    #@56
    .line 1612
    const/16 v2, 0x64

    #@58
    invoke-virtual {p0, v2}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread$1;->sendEmptyMessage(I)Z

    #@5b
    goto :goto_8

    #@5c
    .line 1615
    :pswitch_5c
    iget-object v2, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread$1;->this$1:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread;

    #@5e
    iget-object v3, v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$OsAgentThread;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@60
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@62
    check-cast v2, Lcom/qualcomm/lib/location/mq_client/InPostcard;

    #@64
    invoke-static {v3, v2}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$2600(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;Lcom/qualcomm/lib/location/mq_client/InPostcard;)V
    :try_end_67
    .catch Ljava/lang/Exception; {:try_start_25 .. :try_end_67} :catch_11

    #@67
    goto :goto_8

    #@68
    .line 1593
    :pswitch_data_68
    .packed-switch 0x64
        :pswitch_9
        :pswitch_5c
        :pswitch_25
    .end packed-switch
.end method
