.class Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$PassiveLocationListener;
.super Ljava/lang/Object;
.source "XTWiFiOsAgent.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PassiveLocationListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;


# direct methods
.method constructor <init>(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 451
    iput-object p1, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$PassiveLocationListener;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .registers 5
    .parameter "location"

    #@0
    .prologue
    .line 463
    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    const-string v1, "network"

    #@6
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_2f

    #@c
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    #@f
    move-result v0

    #@10
    const/high16 v1, 0x42c8

    #@12
    cmpg-float v0, v0, v1

    #@14
    if-gez v0, :cond_2f

    #@16
    .line 465
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$PassiveLocationListener;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@18
    invoke-static {v0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$700(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Lcom/qualcomm/lib/location/log/LocLog;

    #@1b
    move-result-object v0

    #@1c
    const-string v1, "XTWiFiOS"

    #@1e
    const-string v2, "reporting passive location report to clients"

    #@20
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@23
    .line 466
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$PassiveLocationListener;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@25
    invoke-static {v0, p1}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$2202(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;Landroid/location/Location;)Landroid/location/Location;

    #@28
    .line 467
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$PassiveLocationListener;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@2a
    const/4 v1, 0x1

    #@2b
    invoke-static {v0, v1}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$2300(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;Z)V

    #@2e
    .line 473
    :goto_2e
    return-void

    #@2f
    .line 471
    :cond_2f
    iget-object v0, p0, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent$PassiveLocationListener;->this$0:Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@31
    invoke-static {v0}, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;->access$700(Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;)Lcom/qualcomm/lib/location/log/LocLog;

    #@34
    move-result-object v0

    #@35
    const-string v1, "XTWiFiOS"

    #@37
    const-string v2, "filter out passive location report"

    #@39
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/log/LocLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@3c
    goto :goto_2e
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .registers 2
    .parameter "provider"

    #@0
    .prologue
    .line 478
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .registers 2
    .parameter "provider"

    #@0
    .prologue
    .line 483
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .registers 4
    .parameter "provider"
    .parameter "status"
    .parameter "extras"

    #@0
    .prologue
    .line 488
    return-void
.end method
