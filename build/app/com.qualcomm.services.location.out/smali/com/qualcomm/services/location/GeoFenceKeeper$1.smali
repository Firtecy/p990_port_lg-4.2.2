.class Lcom/qualcomm/services/location/GeoFenceKeeper$1;
.super Landroid/content/BroadcastReceiver;
.source "GeoFenceKeeper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/services/location/GeoFenceKeeper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/services/location/GeoFenceKeeper;


# direct methods
.method constructor <init>(Lcom/qualcomm/services/location/GeoFenceKeeper;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 366
    iput-object p1, p0, Lcom/qualcomm/services/location/GeoFenceKeeper$1;->this$0:Lcom/qualcomm/services/location/GeoFenceKeeper;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 12
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 369
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 370
    .local v0, action:Ljava/lang/String;
    iget-object v6, p0, Lcom/qualcomm/services/location/GeoFenceKeeper$1;->this$0:Lcom/qualcomm/services/location/GeoFenceKeeper;

    #@6
    invoke-static {v6}, Lcom/qualcomm/services/location/GeoFenceKeeper;->access$1600(Lcom/qualcomm/services/location/GeoFenceKeeper;)Ljava/util/HashMap;

    #@9
    move-result-object v6

    #@a
    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    #@d
    move-result v4

    #@e
    .line 371
    .local v4, size:I
    if-nez v4, :cond_18

    #@10
    .line 372
    const-string v6, "GeoFenceKeeper"

    #@12
    const-string v7, "no active GeoFences"

    #@14
    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    .line 386
    :cond_17
    :goto_17
    return-void

    #@18
    .line 373
    :cond_18
    const-string v6, "com.qualcomm.services.location.geofence.breach"

    #@1a
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v6

    #@1e
    if-eqz v6, :cond_7a

    #@20
    .line 374
    iget-object v6, p0, Lcom/qualcomm/services/location/GeoFenceKeeper$1;->this$0:Lcom/qualcomm/services/location/GeoFenceKeeper;

    #@22
    invoke-static {v6}, Lcom/qualcomm/services/location/GeoFenceKeeper;->access$1700(Lcom/qualcomm/services/location/GeoFenceKeeper;)Ljava/util/Random;

    #@25
    move-result-object v6

    #@26
    invoke-virtual {v6, v4}, Ljava/util/Random;->nextInt(I)I

    #@29
    move-result v3

    #@2a
    .line 375
    .local v3, rand:I
    const-string v6, "GeoFenceKeeper"

    #@2c
    new-instance v7, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v8, "action=com.qualcomm.services.location.geofence.breach, "

    #@33
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v7

    #@37
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v7

    #@3b
    const-string v8, " fences, breach "

    #@3d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v7

    #@41
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@44
    move-result-object v7

    #@45
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v7

    #@49
    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    .line 376
    iget-object v6, p0, Lcom/qualcomm/services/location/GeoFenceKeeper$1;->this$0:Lcom/qualcomm/services/location/GeoFenceKeeper;

    #@4e
    invoke-static {v6}, Lcom/qualcomm/services/location/GeoFenceKeeper;->access$1600(Lcom/qualcomm/services/location/GeoFenceKeeper;)Ljava/util/HashMap;

    #@51
    move-result-object v6

    #@52
    invoke-virtual {v6}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@55
    move-result-object v6

    #@56
    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@59
    move-result-object v5

    #@5a
    .line 377
    .local v5, values:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;>;"
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@5d
    move-result-object v1

    #@5e
    check-cast v1, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;

    #@60
    .line 379
    .local v1, geoFenceData:Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;
    const/4 v2, 0x0

    #@61
    .local v2, i:I
    :goto_61
    if-ge v2, v3, :cond_6c

    #@63
    .line 380
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@66
    move-result-object v1

    #@67
    .end local v1           #geoFenceData:Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;
    check-cast v1, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;

    #@69
    .line 379
    .restart local v1       #geoFenceData:Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;
    add-int/lit8 v2, v2, 0x1

    #@6b
    goto :goto_61

    #@6c
    .line 382
    :cond_6c
    iget-object v6, p0, Lcom/qualcomm/services/location/GeoFenceKeeper$1;->this$0:Lcom/qualcomm/services/location/GeoFenceKeeper;

    #@6e
    invoke-static {v1}, Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;->access$1300(Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;)Ljava/lang/Integer;

    #@71
    move-result-object v7

    #@72
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    #@75
    move-result v7

    #@76
    invoke-static {v6, v7}, Lcom/qualcomm/services/location/GeoFenceKeeper;->access$1800(Lcom/qualcomm/services/location/GeoFenceKeeper;I)V

    #@79
    goto :goto_17

    #@7a
    .line 383
    .end local v1           #geoFenceData:Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;
    .end local v2           #i:I
    .end local v3           #rand:I
    .end local v5           #values:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/qualcomm/services/location/GeoFenceKeeper$GeoFenceData;>;"
    :cond_7a
    const-string v6, "com.qualcomm.services.location.geofence.checkgeofences"

    #@7c
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7f
    move-result v6

    #@80
    if-eqz v6, :cond_17

    #@82
    .line 384
    iget-object v6, p0, Lcom/qualcomm/services/location/GeoFenceKeeper$1;->this$0:Lcom/qualcomm/services/location/GeoFenceKeeper;

    #@84
    invoke-static {v6}, Lcom/qualcomm/services/location/GeoFenceKeeper;->access$1900(Lcom/qualcomm/services/location/GeoFenceKeeper;)V

    #@87
    goto :goto_17
.end method
