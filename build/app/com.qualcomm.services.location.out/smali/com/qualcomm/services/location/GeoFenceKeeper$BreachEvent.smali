.class final Lcom/qualcomm/services/location/GeoFenceKeeper$BreachEvent;
.super Ljava/lang/Object;
.source "GeoFenceKeeper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/services/location/GeoFenceKeeper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "BreachEvent"
.end annotation


# instance fields
.field private final mDirection:I

.field private final mID:I

.field private final mLat:D

.field private final mLon:D

.field final synthetic this$0:Lcom/qualcomm/services/location/GeoFenceKeeper;


# direct methods
.method constructor <init>(Lcom/qualcomm/services/location/GeoFenceKeeper;IIDD)V
    .registers 8
    .parameter
    .parameter "id"
    .parameter "whichWay"
    .parameter "lat"
    .parameter "lon"

    #@0
    .prologue
    .line 310
    iput-object p1, p0, Lcom/qualcomm/services/location/GeoFenceKeeper$BreachEvent;->this$0:Lcom/qualcomm/services/location/GeoFenceKeeper;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 311
    iput p2, p0, Lcom/qualcomm/services/location/GeoFenceKeeper$BreachEvent;->mID:I

    #@7
    .line 312
    iput p3, p0, Lcom/qualcomm/services/location/GeoFenceKeeper$BreachEvent;->mDirection:I

    #@9
    .line 313
    iput-wide p4, p0, Lcom/qualcomm/services/location/GeoFenceKeeper$BreachEvent;->mLat:D

    #@b
    .line 314
    iput-wide p6, p0, Lcom/qualcomm/services/location/GeoFenceKeeper$BreachEvent;->mLon:D

    #@d
    .line 315
    return-void
.end method


# virtual methods
.method public run()V
    .registers 4

    #@0
    .prologue
    .line 319
    iget-object v0, p0, Lcom/qualcomm/services/location/GeoFenceKeeper$BreachEvent;->this$0:Lcom/qualcomm/services/location/GeoFenceKeeper;

    #@2
    iget v1, p0, Lcom/qualcomm/services/location/GeoFenceKeeper$BreachEvent;->mID:I

    #@4
    iget v2, p0, Lcom/qualcomm/services/location/GeoFenceKeeper$BreachEvent;->mDirection:I

    #@6
    invoke-static {v0, v1, v2}, Lcom/qualcomm/services/location/GeoFenceKeeper;->access$1500(Lcom/qualcomm/services/location/GeoFenceKeeper;II)V

    #@9
    .line 320
    return-void
.end method
