.class public Lcom/qualcomm/services/location/LocationServicesBootReceiver;
.super Landroid/content/BroadcastReceiver;
.source "LocationServicesBootReceiver.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 20
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 6
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 28
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 29
    .local v0, intentAction:Ljava/lang/String;
    if-eqz v0, :cond_22

    #@6
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_22

    #@e
    .line 31
    new-instance v1, Landroid/content/Intent;

    #@10
    const-class v2, Lcom/qualcomm/services/location/LocationAlarmService;

    #@12
    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    #@15
    invoke-virtual {p1, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    #@18
    .line 32
    new-instance v1, Landroid/content/Intent;

    #@1a
    const-class v2, Lcom/qualcomm/services/location/xtwifi/XTWiFiOsAgent;

    #@1c
    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    #@1f
    invoke-virtual {p1, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    #@22
    .line 34
    :cond_22
    return-void
.end method
