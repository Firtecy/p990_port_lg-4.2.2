.class public Lcom/qualcomm/lib/location/mq_client/InPostcard;
.super Ljava/lang/Object;
.source "InPostcard.java"


# static fields
.field public static final FtArrayInt64:I = 0x1e

.field public static final FtArrayUInt64:I = 0x1f

.field protected static final TAG:Ljava/lang/String; = "InPostcard"


# instance fields
.field protected buf:[B


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .registers 8
    .parameter "is"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 33
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 34
    new-instance v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;

    #@5
    invoke-direct {v2, p1}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;-><init>(Ljava/io/InputStream;)V

    #@8
    .line 35
    .local v2, in:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;
    invoke-virtual {v2}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readUInt32()J

    #@b
    move-result-wide v0

    #@c
    .line 36
    .local v0, card_size:J
    const-wide/32 v4, 0x7fffffff

    #@f
    cmp-long v4, v0, v4

    #@11
    if-lez v4, :cond_1b

    #@13
    .line 38
    new-instance v4, Ljava/io/IOException;

    #@15
    const-string v5, "memory error"

    #@17
    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v4

    #@1b
    .line 41
    :cond_1b
    long-to-int v4, v0

    #@1c
    new-array v4, v4, [B

    #@1e
    iput-object v4, p0, Lcom/qualcomm/lib/location/mq_client/InPostcard;->buf:[B

    #@20
    .line 42
    iget-object v4, p0, Lcom/qualcomm/lib/location/mq_client/InPostcard;->buf:[B

    #@22
    invoke-virtual {p1, v4}, Ljava/io/InputStream;->read([B)I

    #@25
    move-result v3

    #@26
    .line 43
    .local v3, read_length:I
    int-to-long v4, v3

    #@27
    cmp-long v4, v4, v0

    #@29
    if-eqz v4, :cond_33

    #@2b
    .line 45
    new-instance v4, Ljava/io/IOException;

    #@2d
    const-string v5, "read error"

    #@2f
    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@32
    throw v4

    #@33
    .line 47
    :cond_33
    return-void
.end method


# virtual methods
.method public getArrayBool(Ljava/lang/String;)[Z
    .registers 15
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const-wide/16 v11, 0x1

    #@2
    .line 160
    const/16 v7, 0x26

    #@4
    const/4 v8, 0x0

    #@5
    invoke-virtual {p0, p1, v7, v8}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->lookup(Ljava/lang/String;II)Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;

    #@8
    move-result-object v3

    #@9
    .line 161
    .local v3, data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;
    if-eqz v3, :cond_2f

    #@b
    .line 163
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readUInt32()J

    #@e
    move-result-wide v1

    #@f
    .line 164
    .local v1, blob_length:J
    const/4 v6, 0x1

    #@10
    .line 165
    .local v6, size_of_element:I
    const-wide/16 v7, 0x0

    #@12
    rem-long v9, v1, v11

    #@14
    cmp-long v7, v7, v9

    #@16
    if-eqz v7, :cond_1e

    #@18
    .line 167
    new-instance v7, Ljava/io/IOException;

    #@1a
    invoke-direct {v7, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v7

    #@1e
    .line 169
    :cond_1e
    div-long v7, v1, v11

    #@20
    long-to-int v5, v7

    #@21
    .line 170
    .local v5, num_element:I
    new-array v0, v5, [Z

    #@23
    .line 171
    .local v0, array:[Z
    const/4 v4, 0x0

    #@24
    .local v4, i:I
    :goto_24
    if-ge v4, v5, :cond_35

    #@26
    .line 173
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readBoolean()Z

    #@29
    move-result v7

    #@2a
    aput-boolean v7, v0, v4

    #@2c
    .line 171
    add-int/lit8 v4, v4, 0x1

    #@2e
    goto :goto_24

    #@2f
    .line 179
    .end local v0           #array:[Z
    .end local v1           #blob_length:J
    .end local v4           #i:I
    .end local v5           #num_element:I
    .end local v6           #size_of_element:I
    :cond_2f
    new-instance v7, Ljava/util/NoSuchElementException;

    #@31
    invoke-direct {v7, p1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    #@34
    throw v7

    #@35
    .line 175
    .restart local v0       #array:[Z
    .restart local v1       #blob_length:J
    .restart local v4       #i:I
    .restart local v5       #num_element:I
    .restart local v6       #size_of_element:I
    :cond_35
    return-object v0
.end method

.method public getArrayDouble(Ljava/lang/String;)[D
    .registers 15
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const-wide/16 v11, 0x8

    #@2
    .line 360
    const/16 v7, 0x27

    #@4
    const/4 v8, 0x0

    #@5
    invoke-virtual {p0, p1, v7, v8}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->lookup(Ljava/lang/String;II)Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;

    #@8
    move-result-object v3

    #@9
    .line 361
    .local v3, data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;
    if-eqz v3, :cond_30

    #@b
    .line 363
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readUInt32()J

    #@e
    move-result-wide v1

    #@f
    .line 364
    .local v1, blob_length:J
    const/16 v6, 0x8

    #@11
    .line 365
    .local v6, size_of_element:I
    const-wide/16 v7, 0x0

    #@13
    rem-long v9, v1, v11

    #@15
    cmp-long v7, v7, v9

    #@17
    if-eqz v7, :cond_1f

    #@19
    .line 367
    new-instance v7, Ljava/io/IOException;

    #@1b
    invoke-direct {v7, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v7

    #@1f
    .line 369
    :cond_1f
    div-long v7, v1, v11

    #@21
    long-to-int v5, v7

    #@22
    .line 370
    .local v5, num_element:I
    new-array v0, v5, [D

    #@24
    .line 371
    .local v0, array:[D
    const/4 v4, 0x0

    #@25
    .local v4, i:I
    :goto_25
    if-ge v4, v5, :cond_36

    #@27
    .line 373
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readDouble()D

    #@2a
    move-result-wide v7

    #@2b
    aput-wide v7, v0, v4

    #@2d
    .line 371
    add-int/lit8 v4, v4, 0x1

    #@2f
    goto :goto_25

    #@30
    .line 379
    .end local v0           #array:[D
    .end local v1           #blob_length:J
    .end local v4           #i:I
    .end local v5           #num_element:I
    .end local v6           #size_of_element:I
    :cond_30
    new-instance v7, Ljava/util/NoSuchElementException;

    #@32
    invoke-direct {v7, p1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    #@35
    throw v7

    #@36
    .line 375
    .restart local v0       #array:[D
    .restart local v1       #blob_length:J
    .restart local v4       #i:I
    .restart local v5       #num_element:I
    .restart local v6       #size_of_element:I
    :cond_36
    return-object v0
.end method

.method public getArrayFloat(Ljava/lang/String;)[F
    .registers 15
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const-wide/16 v11, 0x4

    #@2
    .line 335
    const/16 v7, 0x28

    #@4
    const/4 v8, 0x0

    #@5
    invoke-virtual {p0, p1, v7, v8}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->lookup(Ljava/lang/String;II)Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;

    #@8
    move-result-object v3

    #@9
    .line 336
    .local v3, data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;
    if-eqz v3, :cond_2f

    #@b
    .line 338
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readUInt32()J

    #@e
    move-result-wide v1

    #@f
    .line 339
    .local v1, blob_length:J
    const/4 v6, 0x4

    #@10
    .line 340
    .local v6, size_of_element:I
    const-wide/16 v7, 0x0

    #@12
    rem-long v9, v1, v11

    #@14
    cmp-long v7, v7, v9

    #@16
    if-eqz v7, :cond_1e

    #@18
    .line 342
    new-instance v7, Ljava/io/IOException;

    #@1a
    invoke-direct {v7, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v7

    #@1e
    .line 344
    :cond_1e
    div-long v7, v1, v11

    #@20
    long-to-int v5, v7

    #@21
    .line 345
    .local v5, num_element:I
    new-array v0, v5, [F

    #@23
    .line 346
    .local v0, array:[F
    const/4 v4, 0x0

    #@24
    .local v4, i:I
    :goto_24
    if-ge v4, v5, :cond_35

    #@26
    .line 348
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readFloat()F

    #@29
    move-result v7

    #@2a
    aput v7, v0, v4

    #@2c
    .line 346
    add-int/lit8 v4, v4, 0x1

    #@2e
    goto :goto_24

    #@2f
    .line 354
    .end local v0           #array:[F
    .end local v1           #blob_length:J
    .end local v4           #i:I
    .end local v5           #num_element:I
    .end local v6           #size_of_element:I
    :cond_2f
    new-instance v7, Ljava/util/NoSuchElementException;

    #@31
    invoke-direct {v7, p1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    #@34
    throw v7

    #@35
    .line 350
    .restart local v0       #array:[F
    .restart local v1       #blob_length:J
    .restart local v4       #i:I
    .restart local v5       #num_element:I
    .restart local v6       #size_of_element:I
    :cond_35
    return-object v0
.end method

.method public getArrayInt16(Ljava/lang/String;)[I
    .registers 15
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const-wide/16 v11, 0x2

    #@2
    .line 235
    const/16 v7, 0x22

    #@4
    const/4 v8, 0x0

    #@5
    invoke-virtual {p0, p1, v7, v8}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->lookup(Ljava/lang/String;II)Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;

    #@8
    move-result-object v3

    #@9
    .line 236
    .local v3, data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;
    if-eqz v3, :cond_2f

    #@b
    .line 238
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readUInt32()J

    #@e
    move-result-wide v1

    #@f
    .line 239
    .local v1, blob_length:J
    const/4 v6, 0x2

    #@10
    .line 240
    .local v6, size_of_element:I
    const-wide/16 v7, 0x0

    #@12
    rem-long v9, v1, v11

    #@14
    cmp-long v7, v7, v9

    #@16
    if-eqz v7, :cond_1e

    #@18
    .line 242
    new-instance v7, Ljava/io/IOException;

    #@1a
    invoke-direct {v7, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v7

    #@1e
    .line 244
    :cond_1e
    div-long v7, v1, v11

    #@20
    long-to-int v5, v7

    #@21
    .line 245
    .local v5, num_element:I
    new-array v0, v5, [I

    #@23
    .line 246
    .local v0, array:[I
    const/4 v4, 0x0

    #@24
    .local v4, i:I
    :goto_24
    if-ge v4, v5, :cond_35

    #@26
    .line 248
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readInt16()I

    #@29
    move-result v7

    #@2a
    aput v7, v0, v4

    #@2c
    .line 246
    add-int/lit8 v4, v4, 0x1

    #@2e
    goto :goto_24

    #@2f
    .line 254
    .end local v0           #array:[I
    .end local v1           #blob_length:J
    .end local v4           #i:I
    .end local v5           #num_element:I
    .end local v6           #size_of_element:I
    :cond_2f
    new-instance v7, Ljava/util/NoSuchElementException;

    #@31
    invoke-direct {v7, p1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    #@34
    throw v7

    #@35
    .line 250
    .restart local v0       #array:[I
    .restart local v1       #blob_length:J
    .restart local v4       #i:I
    .restart local v5       #num_element:I
    .restart local v6       #size_of_element:I
    :cond_35
    return-object v0
.end method

.method public getArrayInt32(Ljava/lang/String;)[I
    .registers 15
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const-wide/16 v11, 0x4

    #@2
    .line 285
    const/16 v7, 0x20

    #@4
    const/4 v8, 0x0

    #@5
    invoke-virtual {p0, p1, v7, v8}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->lookup(Ljava/lang/String;II)Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;

    #@8
    move-result-object v3

    #@9
    .line 286
    .local v3, data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;
    if-eqz v3, :cond_2f

    #@b
    .line 288
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readUInt32()J

    #@e
    move-result-wide v1

    #@f
    .line 289
    .local v1, blob_length:J
    const/4 v6, 0x4

    #@10
    .line 290
    .local v6, size_of_element:I
    const-wide/16 v7, 0x0

    #@12
    rem-long v9, v1, v11

    #@14
    cmp-long v7, v7, v9

    #@16
    if-eqz v7, :cond_1e

    #@18
    .line 292
    new-instance v7, Ljava/io/IOException;

    #@1a
    invoke-direct {v7, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v7

    #@1e
    .line 294
    :cond_1e
    div-long v7, v1, v11

    #@20
    long-to-int v5, v7

    #@21
    .line 295
    .local v5, num_element:I
    new-array v0, v5, [I

    #@23
    .line 296
    .local v0, array:[I
    const/4 v4, 0x0

    #@24
    .local v4, i:I
    :goto_24
    if-ge v4, v5, :cond_35

    #@26
    .line 298
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readInt32()I

    #@29
    move-result v7

    #@2a
    aput v7, v0, v4

    #@2c
    .line 296
    add-int/lit8 v4, v4, 0x1

    #@2e
    goto :goto_24

    #@2f
    .line 304
    .end local v0           #array:[I
    .end local v1           #blob_length:J
    .end local v4           #i:I
    .end local v5           #num_element:I
    .end local v6           #size_of_element:I
    :cond_2f
    new-instance v7, Ljava/util/NoSuchElementException;

    #@31
    invoke-direct {v7, p1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    #@34
    throw v7

    #@35
    .line 300
    .restart local v0       #array:[I
    .restart local v1       #blob_length:J
    .restart local v4       #i:I
    .restart local v5       #num_element:I
    .restart local v6       #size_of_element:I
    :cond_35
    return-object v0
.end method

.method public getArrayInt64(Ljava/lang/String;)[J
    .registers 15
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const-wide/16 v11, 0x8

    #@2
    .line 385
    const/16 v7, 0x1e

    #@4
    const/4 v8, 0x0

    #@5
    invoke-virtual {p0, p1, v7, v8}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->lookup(Ljava/lang/String;II)Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;

    #@8
    move-result-object v3

    #@9
    .line 386
    .local v3, data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;
    if-eqz v3, :cond_30

    #@b
    .line 388
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readUInt32()J

    #@e
    move-result-wide v1

    #@f
    .line 389
    .local v1, blob_length:J
    const/16 v6, 0x8

    #@11
    .line 390
    .local v6, size_of_element:I
    const-wide/16 v7, 0x0

    #@13
    rem-long v9, v1, v11

    #@15
    cmp-long v7, v7, v9

    #@17
    if-eqz v7, :cond_1f

    #@19
    .line 392
    new-instance v7, Ljava/io/IOException;

    #@1b
    invoke-direct {v7, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v7

    #@1f
    .line 394
    :cond_1f
    div-long v7, v1, v11

    #@21
    long-to-int v5, v7

    #@22
    .line 395
    .local v5, num_element:I
    new-array v0, v5, [J

    #@24
    .line 396
    .local v0, array:[J
    const/4 v4, 0x0

    #@25
    .local v4, i:I
    :goto_25
    if-ge v4, v5, :cond_36

    #@27
    .line 398
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readInt64()J

    #@2a
    move-result-wide v7

    #@2b
    aput-wide v7, v0, v4

    #@2d
    .line 396
    add-int/lit8 v4, v4, 0x1

    #@2f
    goto :goto_25

    #@30
    .line 404
    .end local v0           #array:[J
    .end local v1           #blob_length:J
    .end local v4           #i:I
    .end local v5           #num_element:I
    .end local v6           #size_of_element:I
    :cond_30
    new-instance v7, Ljava/util/NoSuchElementException;

    #@32
    invoke-direct {v7, p1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    #@35
    throw v7

    #@36
    .line 400
    .restart local v0       #array:[J
    .restart local v1       #blob_length:J
    .restart local v4       #i:I
    .restart local v5       #num_element:I
    .restart local v6       #size_of_element:I
    :cond_36
    return-object v0
.end method

.method public getArrayInt8(Ljava/lang/String;)[I
    .registers 15
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const-wide/16 v11, 0x1

    #@2
    .line 185
    const/16 v7, 0x24

    #@4
    const/4 v8, 0x0

    #@5
    invoke-virtual {p0, p1, v7, v8}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->lookup(Ljava/lang/String;II)Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;

    #@8
    move-result-object v3

    #@9
    .line 186
    .local v3, data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;
    if-eqz v3, :cond_2f

    #@b
    .line 188
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readUInt32()J

    #@e
    move-result-wide v1

    #@f
    .line 189
    .local v1, blob_length:J
    const/4 v6, 0x1

    #@10
    .line 190
    .local v6, size_of_element:I
    const-wide/16 v7, 0x0

    #@12
    rem-long v9, v1, v11

    #@14
    cmp-long v7, v7, v9

    #@16
    if-eqz v7, :cond_1e

    #@18
    .line 192
    new-instance v7, Ljava/io/IOException;

    #@1a
    invoke-direct {v7, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v7

    #@1e
    .line 194
    :cond_1e
    div-long v7, v1, v11

    #@20
    long-to-int v5, v7

    #@21
    .line 195
    .local v5, num_element:I
    new-array v0, v5, [I

    #@23
    .line 196
    .local v0, array:[I
    const/4 v4, 0x0

    #@24
    .local v4, i:I
    :goto_24
    if-ge v4, v5, :cond_35

    #@26
    .line 198
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readInt8()I

    #@29
    move-result v7

    #@2a
    aput v7, v0, v4

    #@2c
    .line 196
    add-int/lit8 v4, v4, 0x1

    #@2e
    goto :goto_24

    #@2f
    .line 204
    .end local v0           #array:[I
    .end local v1           #blob_length:J
    .end local v4           #i:I
    .end local v5           #num_element:I
    .end local v6           #size_of_element:I
    :cond_2f
    new-instance v7, Ljava/util/NoSuchElementException;

    #@31
    invoke-direct {v7, p1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    #@34
    throw v7

    #@35
    .line 200
    .restart local v0       #array:[I
    .restart local v1       #blob_length:J
    .restart local v4       #i:I
    .restart local v5       #num_element:I
    .restart local v6       #size_of_element:I
    :cond_35
    return-object v0
.end method

.method public getArrayUInt16(Ljava/lang/String;)[I
    .registers 15
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const-wide/16 v11, 0x2

    #@2
    .line 260
    const/16 v7, 0x23

    #@4
    const/4 v8, 0x0

    #@5
    invoke-virtual {p0, p1, v7, v8}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->lookup(Ljava/lang/String;II)Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;

    #@8
    move-result-object v3

    #@9
    .line 261
    .local v3, data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;
    if-eqz v3, :cond_2f

    #@b
    .line 263
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readUInt32()J

    #@e
    move-result-wide v1

    #@f
    .line 264
    .local v1, blob_length:J
    const/4 v6, 0x2

    #@10
    .line 265
    .local v6, size_of_element:I
    const-wide/16 v7, 0x0

    #@12
    rem-long v9, v1, v11

    #@14
    cmp-long v7, v7, v9

    #@16
    if-eqz v7, :cond_1e

    #@18
    .line 267
    new-instance v7, Ljava/io/IOException;

    #@1a
    invoke-direct {v7, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v7

    #@1e
    .line 269
    :cond_1e
    div-long v7, v1, v11

    #@20
    long-to-int v5, v7

    #@21
    .line 270
    .local v5, num_element:I
    new-array v0, v5, [I

    #@23
    .line 271
    .local v0, array:[I
    const/4 v4, 0x0

    #@24
    .local v4, i:I
    :goto_24
    if-ge v4, v5, :cond_35

    #@26
    .line 273
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readUInt16()I

    #@29
    move-result v7

    #@2a
    aput v7, v0, v4

    #@2c
    .line 271
    add-int/lit8 v4, v4, 0x1

    #@2e
    goto :goto_24

    #@2f
    .line 279
    .end local v0           #array:[I
    .end local v1           #blob_length:J
    .end local v4           #i:I
    .end local v5           #num_element:I
    .end local v6           #size_of_element:I
    :cond_2f
    new-instance v7, Ljava/util/NoSuchElementException;

    #@31
    invoke-direct {v7, p1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    #@34
    throw v7

    #@35
    .line 275
    .restart local v0       #array:[I
    .restart local v1       #blob_length:J
    .restart local v4       #i:I
    .restart local v5       #num_element:I
    .restart local v6       #size_of_element:I
    :cond_35
    return-object v0
.end method

.method public getArrayUInt32(Ljava/lang/String;)[J
    .registers 15
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const-wide/16 v11, 0x4

    #@2
    .line 310
    const/16 v7, 0x21

    #@4
    const/4 v8, 0x0

    #@5
    invoke-virtual {p0, p1, v7, v8}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->lookup(Ljava/lang/String;II)Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;

    #@8
    move-result-object v3

    #@9
    .line 311
    .local v3, data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;
    if-eqz v3, :cond_2f

    #@b
    .line 313
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readUInt32()J

    #@e
    move-result-wide v1

    #@f
    .line 314
    .local v1, blob_length:J
    const/4 v6, 0x4

    #@10
    .line 315
    .local v6, size_of_element:I
    const-wide/16 v7, 0x0

    #@12
    rem-long v9, v1, v11

    #@14
    cmp-long v7, v7, v9

    #@16
    if-eqz v7, :cond_1e

    #@18
    .line 317
    new-instance v7, Ljava/io/IOException;

    #@1a
    invoke-direct {v7, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v7

    #@1e
    .line 319
    :cond_1e
    div-long v7, v1, v11

    #@20
    long-to-int v5, v7

    #@21
    .line 320
    .local v5, num_element:I
    new-array v0, v5, [J

    #@23
    .line 321
    .local v0, array:[J
    const/4 v4, 0x0

    #@24
    .local v4, i:I
    :goto_24
    if-ge v4, v5, :cond_35

    #@26
    .line 323
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readUInt32()J

    #@29
    move-result-wide v7

    #@2a
    aput-wide v7, v0, v4

    #@2c
    .line 321
    add-int/lit8 v4, v4, 0x1

    #@2e
    goto :goto_24

    #@2f
    .line 329
    .end local v0           #array:[J
    .end local v1           #blob_length:J
    .end local v4           #i:I
    .end local v5           #num_element:I
    .end local v6           #size_of_element:I
    :cond_2f
    new-instance v7, Ljava/util/NoSuchElementException;

    #@31
    invoke-direct {v7, p1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    #@34
    throw v7

    #@35
    .line 325
    .restart local v0       #array:[J
    .restart local v1       #blob_length:J
    .restart local v4       #i:I
    .restart local v5       #num_element:I
    .restart local v6       #size_of_element:I
    :cond_35
    return-object v0
.end method

.method public getArrayUInt64(Ljava/lang/String;)[Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;
    .registers 15
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const-wide/16 v11, 0x8

    #@2
    .line 410
    const/16 v7, 0x1f

    #@4
    const/4 v8, 0x0

    #@5
    invoke-virtual {p0, p1, v7, v8}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->lookup(Ljava/lang/String;II)Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;

    #@8
    move-result-object v3

    #@9
    .line 411
    .local v3, data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;
    if-eqz v3, :cond_30

    #@b
    .line 413
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readUInt32()J

    #@e
    move-result-wide v1

    #@f
    .line 414
    .local v1, blob_length:J
    const/16 v6, 0x8

    #@11
    .line 415
    .local v6, size_of_element:I
    const-wide/16 v7, 0x0

    #@13
    rem-long v9, v1, v11

    #@15
    cmp-long v7, v7, v9

    #@17
    if-eqz v7, :cond_1f

    #@19
    .line 417
    new-instance v7, Ljava/io/IOException;

    #@1b
    invoke-direct {v7, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v7

    #@1f
    .line 419
    :cond_1f
    div-long v7, v1, v11

    #@21
    long-to-int v5, v7

    #@22
    .line 420
    .local v5, num_element:I
    new-array v0, v5, [Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;

    #@24
    .line 421
    .local v0, array:[Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;
    const/4 v4, 0x0

    #@25
    .local v4, i:I
    :goto_25
    if-ge v4, v5, :cond_36

    #@27
    .line 423
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readUInt64()Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;

    #@2a
    move-result-object v7

    #@2b
    aput-object v7, v0, v4

    #@2d
    .line 421
    add-int/lit8 v4, v4, 0x1

    #@2f
    goto :goto_25

    #@30
    .line 429
    .end local v0           #array:[Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;
    .end local v1           #blob_length:J
    .end local v4           #i:I
    .end local v5           #num_element:I
    .end local v6           #size_of_element:I
    :cond_30
    new-instance v7, Ljava/util/NoSuchElementException;

    #@32
    invoke-direct {v7, p1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    #@35
    throw v7

    #@36
    .line 425
    .restart local v0       #array:[Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;
    .restart local v1       #blob_length:J
    .restart local v4       #i:I
    .restart local v5       #num_element:I
    .restart local v6       #size_of_element:I
    :cond_36
    return-object v0
.end method

.method public getArrayUInt8(Ljava/lang/String;)[I
    .registers 15
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const-wide/16 v11, 0x1

    #@2
    .line 210
    const/16 v7, 0x25

    #@4
    const/4 v8, 0x0

    #@5
    invoke-virtual {p0, p1, v7, v8}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->lookup(Ljava/lang/String;II)Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;

    #@8
    move-result-object v3

    #@9
    .line 211
    .local v3, data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;
    if-eqz v3, :cond_2f

    #@b
    .line 213
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readUInt32()J

    #@e
    move-result-wide v1

    #@f
    .line 214
    .local v1, blob_length:J
    const/4 v6, 0x1

    #@10
    .line 215
    .local v6, size_of_element:I
    const-wide/16 v7, 0x0

    #@12
    rem-long v9, v1, v11

    #@14
    cmp-long v7, v7, v9

    #@16
    if-eqz v7, :cond_1e

    #@18
    .line 217
    new-instance v7, Ljava/io/IOException;

    #@1a
    invoke-direct {v7, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v7

    #@1e
    .line 219
    :cond_1e
    div-long v7, v1, v11

    #@20
    long-to-int v5, v7

    #@21
    .line 220
    .local v5, num_element:I
    new-array v0, v5, [I

    #@23
    .line 221
    .local v0, array:[I
    const/4 v4, 0x0

    #@24
    .local v4, i:I
    :goto_24
    if-ge v4, v5, :cond_35

    #@26
    .line 223
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readUInt8()I

    #@29
    move-result v7

    #@2a
    aput v7, v0, v4

    #@2c
    .line 221
    add-int/lit8 v4, v4, 0x1

    #@2e
    goto :goto_24

    #@2f
    .line 229
    .end local v0           #array:[I
    .end local v1           #blob_length:J
    .end local v4           #i:I
    .end local v5           #num_element:I
    .end local v6           #size_of_element:I
    :cond_2f
    new-instance v7, Ljava/util/NoSuchElementException;

    #@31
    invoke-direct {v7, p1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    #@34
    throw v7

    #@35
    .line 225
    .restart local v0       #array:[I
    .restart local v1       #blob_length:J
    .restart local v4       #i:I
    .restart local v5       #num_element:I
    .restart local v6       #size_of_element:I
    :cond_35
    return-object v0
.end method

.method public getBool(Ljava/lang/String;)Z
    .registers 5
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 557
    const/16 v1, 0x12

    #@2
    const/4 v2, 0x0

    #@3
    invoke-virtual {p0, p1, v1, v2}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->lookup(Ljava/lang/String;II)Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;

    #@6
    move-result-object v0

    #@7
    .line 558
    .local v0, data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;
    if-eqz v0, :cond_e

    #@9
    .line 560
    invoke-virtual {v0}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readBoolean()Z

    #@c
    move-result v1

    #@d
    return v1

    #@e
    .line 564
    :cond_e
    new-instance v1, Ljava/util/NoSuchElementException;

    #@10
    invoke-direct {v1, p1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    #@13
    throw v1
.end method

.method public getBoolDefault(Ljava/lang/String;Z)Z
    .registers 4
    .parameter "name"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 572
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->getBool(Ljava/lang/String;)Z
    :try_end_3
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_3} :catch_5

    #@3
    move-result p2

    #@4
    .line 576
    .end local p2
    :goto_4
    return p2

    #@5
    .line 574
    .restart local p2
    :catch_5
    move-exception v0

    #@6
    .line 576
    .local v0, e:Ljava/util/NoSuchElementException;
    goto :goto_4
.end method

.method public getCard(Ljava/lang/String;I)Lcom/qualcomm/lib/location/mq_client/InPostcard;
    .registers 5
    .parameter "name"
    .parameter "index"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 455
    const/4 v1, 0x1

    #@1
    invoke-virtual {p0, p1, v1, p2}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->lookup(Ljava/lang/String;II)Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;

    #@4
    move-result-object v0

    #@5
    .line 456
    .local v0, data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;
    if-eqz v0, :cond_d

    #@7
    .line 458
    new-instance v1, Lcom/qualcomm/lib/location/mq_client/InPostcard;

    #@9
    invoke-direct {v1, v0}, Lcom/qualcomm/lib/location/mq_client/InPostcard;-><init>(Ljava/io/InputStream;)V

    #@c
    return-object v1

    #@d
    .line 462
    :cond_d
    new-instance v1, Ljava/util/NoSuchElementException;

    #@f
    invoke-direct {v1, p1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    #@12
    throw v1
.end method

.method public getDouble(Ljava/lang/String;)D
    .registers 5
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 507
    const/16 v1, 0x15

    #@2
    const/4 v2, 0x0

    #@3
    invoke-virtual {p0, p1, v1, v2}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->lookup(Ljava/lang/String;II)Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;

    #@6
    move-result-object v0

    #@7
    .line 508
    .local v0, data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;
    if-eqz v0, :cond_e

    #@9
    .line 510
    invoke-virtual {v0}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readDouble()D

    #@c
    move-result-wide v1

    #@d
    return-wide v1

    #@e
    .line 514
    :cond_e
    new-instance v1, Ljava/util/NoSuchElementException;

    #@10
    invoke-direct {v1, p1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    #@13
    throw v1
.end method

.method public getDoubleDefault(Ljava/lang/String;D)D
    .registers 5
    .parameter "name"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 522
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->getDouble(Ljava/lang/String;)D
    :try_end_3
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_3} :catch_5

    #@3
    move-result-wide p2

    #@4
    .line 526
    .end local p2
    :goto_4
    return-wide p2

    #@5
    .line 524
    .restart local p2
    :catch_5
    move-exception v0

    #@6
    .line 526
    .local v0, e:Ljava/util/NoSuchElementException;
    goto :goto_4
.end method

.method public getFloat(Ljava/lang/String;)F
    .registers 5
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 532
    const/16 v1, 0x16

    #@2
    const/4 v2, 0x0

    #@3
    invoke-virtual {p0, p1, v1, v2}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->lookup(Ljava/lang/String;II)Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;

    #@6
    move-result-object v0

    #@7
    .line 533
    .local v0, data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;
    if-eqz v0, :cond_e

    #@9
    .line 535
    invoke-virtual {v0}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readFloat()F

    #@c
    move-result v1

    #@d
    return v1

    #@e
    .line 539
    :cond_e
    new-instance v1, Ljava/util/NoSuchElementException;

    #@10
    invoke-direct {v1, p1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    #@13
    throw v1
.end method

.method public getFloatDefault(Ljava/lang/String;F)D
    .registers 6
    .parameter "name"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 547
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->getFloat(Ljava/lang/String;)F
    :try_end_3
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_3} :catch_6

    #@3
    move-result v1

    #@4
    float-to-double v1, v1

    #@5
    .line 551
    :goto_5
    return-wide v1

    #@6
    .line 549
    :catch_6
    move-exception v0

    #@7
    .line 551
    .local v0, e:Ljava/util/NoSuchElementException;
    float-to-double v1, p2

    #@8
    goto :goto_5
.end method

.method public getInt16(Ljava/lang/String;)I
    .registers 5
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 657
    const/16 v1, 0xe

    #@2
    const/4 v2, 0x0

    #@3
    invoke-virtual {p0, p1, v1, v2}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->lookup(Ljava/lang/String;II)Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;

    #@6
    move-result-object v0

    #@7
    .line 658
    .local v0, data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;
    if-eqz v0, :cond_e

    #@9
    .line 660
    invoke-virtual {v0}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readInt16()I

    #@c
    move-result v1

    #@d
    return v1

    #@e
    .line 664
    :cond_e
    new-instance v1, Ljava/util/NoSuchElementException;

    #@10
    invoke-direct {v1, p1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    #@13
    throw v1
.end method

.method public getInt16Default(Ljava/lang/String;I)I
    .registers 4
    .parameter "name"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 672
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->getInt16(Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_3} :catch_5

    #@3
    move-result p2

    #@4
    .line 676
    .end local p2
    :goto_4
    return p2

    #@5
    .line 674
    .restart local p2
    :catch_5
    move-exception v0

    #@6
    .line 676
    .local v0, e:Ljava/util/NoSuchElementException;
    goto :goto_4
.end method

.method public getInt32(Ljava/lang/String;)I
    .registers 5
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 482
    const/16 v1, 0xc

    #@2
    const/4 v2, 0x0

    #@3
    invoke-virtual {p0, p1, v1, v2}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->lookup(Ljava/lang/String;II)Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;

    #@6
    move-result-object v0

    #@7
    .line 483
    .local v0, data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;
    if-eqz v0, :cond_e

    #@9
    .line 485
    invoke-virtual {v0}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readInt32()I

    #@c
    move-result v1

    #@d
    return v1

    #@e
    .line 489
    :cond_e
    new-instance v1, Ljava/util/NoSuchElementException;

    #@10
    invoke-direct {v1, p1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    #@13
    throw v1
.end method

.method public getInt32Default(Ljava/lang/String;I)I
    .registers 4
    .parameter "name"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 497
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->getInt32(Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_3} :catch_5

    #@3
    move-result p2

    #@4
    .line 501
    .end local p2
    :goto_4
    return p2

    #@5
    .line 499
    .restart local p2
    :catch_5
    move-exception v0

    #@6
    .line 501
    .local v0, e:Ljava/util/NoSuchElementException;
    goto :goto_4
.end method

.method public getInt64(Ljava/lang/String;)J
    .registers 5
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 707
    const/16 v1, 0xa

    #@2
    const/4 v2, 0x0

    #@3
    invoke-virtual {p0, p1, v1, v2}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->lookup(Ljava/lang/String;II)Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;

    #@6
    move-result-object v0

    #@7
    .line 708
    .local v0, data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;
    if-eqz v0, :cond_e

    #@9
    .line 710
    invoke-virtual {v0}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readInt64()J

    #@c
    move-result-wide v1

    #@d
    return-wide v1

    #@e
    .line 714
    :cond_e
    new-instance v1, Ljava/util/NoSuchElementException;

    #@10
    invoke-direct {v1, p1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    #@13
    throw v1
.end method

.method public getInt64Default(Ljava/lang/String;J)J
    .registers 5
    .parameter "name"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 722
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->getInt64(Ljava/lang/String;)J
    :try_end_3
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_3} :catch_5

    #@3
    move-result-wide p2

    #@4
    .line 726
    .end local p2
    :goto_4
    return-wide p2

    #@5
    .line 724
    .restart local p2
    :catch_5
    move-exception v0

    #@6
    .line 726
    .local v0, e:Ljava/util/NoSuchElementException;
    goto :goto_4
.end method

.method public getInt8(Ljava/lang/String;)I
    .registers 5
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 607
    const/16 v1, 0x10

    #@2
    const/4 v2, 0x0

    #@3
    invoke-virtual {p0, p1, v1, v2}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->lookup(Ljava/lang/String;II)Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;

    #@6
    move-result-object v0

    #@7
    .line 608
    .local v0, data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;
    if-eqz v0, :cond_e

    #@9
    .line 610
    invoke-virtual {v0}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readInt8()I

    #@c
    move-result v1

    #@d
    return v1

    #@e
    .line 614
    :cond_e
    new-instance v1, Ljava/util/NoSuchElementException;

    #@10
    invoke-direct {v1, p1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    #@13
    throw v1
.end method

.method public getInt8Default(Ljava/lang/String;I)I
    .registers 4
    .parameter "name"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 622
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->getInt8(Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_3} :catch_5

    #@3
    move-result p2

    #@4
    .line 626
    .end local p2
    :goto_4
    return p2

    #@5
    .line 624
    .restart local p2
    :catch_5
    move-exception v0

    #@6
    .line 626
    .local v0, e:Ljava/util/NoSuchElementException;
    goto :goto_4
.end method

.method public getString(Ljava/lang/String;)Ljava/lang/String;
    .registers 8
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 436
    const/16 v4, 0x13

    #@2
    const/4 v5, 0x0

    #@3
    invoke-virtual {p0, p1, v4, v5}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->lookup(Ljava/lang/String;II)Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;

    #@6
    move-result-object v0

    #@7
    .line 437
    .local v0, data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;
    if-eqz v0, :cond_24

    #@9
    .line 439
    invoke-virtual {v0}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readUInt32()J

    #@c
    move-result-wide v2

    #@d
    .line 440
    .local v2, string_length:J
    invoke-virtual {v0}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readCstr()Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    .line 441
    .local v1, str:Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@14
    move-result v4

    #@15
    add-int/lit8 v4, v4, 0x1

    #@17
    int-to-long v4, v4

    #@18
    cmp-long v4, v4, v2

    #@1a
    if-eqz v4, :cond_2a

    #@1c
    .line 443
    new-instance v4, Ljava/io/IOException;

    #@1e
    const-string v5, "invalid string length"

    #@20
    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@23
    throw v4

    #@24
    .line 449
    .end local v1           #str:Ljava/lang/String;
    .end local v2           #string_length:J
    :cond_24
    new-instance v4, Ljava/util/NoSuchElementException;

    #@26
    invoke-direct {v4, p1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    #@29
    throw v4

    #@2a
    .line 445
    .restart local v1       #str:Ljava/lang/String;
    .restart local v2       #string_length:J
    :cond_2a
    return-object v1
.end method

.method public getStringDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "name"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 471
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_3
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_3} :catch_5

    #@3
    move-result-object v1

    #@4
    .line 477
    .local v1, result:Ljava/lang/String;
    :goto_4
    return-object v1

    #@5
    .line 473
    .end local v1           #result:Ljava/lang/String;
    :catch_5
    move-exception v0

    #@6
    .line 475
    .local v0, e:Ljava/util/NoSuchElementException;
    move-object v1, p2

    #@7
    .restart local v1       #result:Ljava/lang/String;
    goto :goto_4
.end method

.method public getUInt16(Ljava/lang/String;)I
    .registers 5
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 632
    const/16 v1, 0xf

    #@2
    const/4 v2, 0x0

    #@3
    invoke-virtual {p0, p1, v1, v2}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->lookup(Ljava/lang/String;II)Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;

    #@6
    move-result-object v0

    #@7
    .line 633
    .local v0, data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;
    if-eqz v0, :cond_e

    #@9
    .line 635
    invoke-virtual {v0}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readUInt16()I

    #@c
    move-result v1

    #@d
    return v1

    #@e
    .line 639
    :cond_e
    new-instance v1, Ljava/util/NoSuchElementException;

    #@10
    invoke-direct {v1, p1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    #@13
    throw v1
.end method

.method public getUInt16Default(Ljava/lang/String;I)I
    .registers 4
    .parameter "name"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 647
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->getUInt16(Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_3} :catch_5

    #@3
    move-result p2

    #@4
    .line 651
    .end local p2
    :goto_4
    return p2

    #@5
    .line 649
    .restart local p2
    :catch_5
    move-exception v0

    #@6
    .line 651
    .local v0, e:Ljava/util/NoSuchElementException;
    goto :goto_4
.end method

.method public getUInt32(Ljava/lang/String;)J
    .registers 5
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 682
    const/16 v1, 0xd

    #@2
    const/4 v2, 0x0

    #@3
    invoke-virtual {p0, p1, v1, v2}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->lookup(Ljava/lang/String;II)Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;

    #@6
    move-result-object v0

    #@7
    .line 683
    .local v0, data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;
    if-eqz v0, :cond_e

    #@9
    .line 685
    invoke-virtual {v0}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readUInt32()J

    #@c
    move-result-wide v1

    #@d
    return-wide v1

    #@e
    .line 689
    :cond_e
    new-instance v1, Ljava/util/NoSuchElementException;

    #@10
    invoke-direct {v1, p1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    #@13
    throw v1
.end method

.method public getUInt32Default(Ljava/lang/String;I)J
    .registers 6
    .parameter "name"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 697
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->getUInt32(Ljava/lang/String;)J
    :try_end_3
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_3} :catch_5

    #@3
    move-result-wide v1

    #@4
    .line 701
    :goto_4
    return-wide v1

    #@5
    .line 699
    :catch_5
    move-exception v0

    #@6
    .line 701
    .local v0, e:Ljava/util/NoSuchElementException;
    int-to-long v1, p2

    #@7
    goto :goto_4
.end method

.method public getUInt64(Ljava/lang/String;)Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;
    .registers 5
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 732
    const/16 v1, 0xb

    #@2
    const/4 v2, 0x0

    #@3
    invoke-virtual {p0, p1, v1, v2}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->lookup(Ljava/lang/String;II)Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;

    #@6
    move-result-object v0

    #@7
    .line 733
    .local v0, data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;
    if-eqz v0, :cond_e

    #@9
    .line 735
    invoke-virtual {v0}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readUInt64()Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;

    #@c
    move-result-object v1

    #@d
    return-object v1

    #@e
    .line 739
    :cond_e
    new-instance v1, Ljava/util/NoSuchElementException;

    #@10
    invoke-direct {v1, p1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    #@13
    throw v1
.end method

.method public getUInt64Default(Ljava/lang/String;Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;)Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;
    .registers 4
    .parameter "name"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 748
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->getUInt64(Ljava/lang/String;)Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;
    :try_end_3
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_3} :catch_5

    #@3
    move-result-object p2

    #@4
    .line 752
    .end local p2
    :goto_4
    return-object p2

    #@5
    .line 750
    .restart local p2
    :catch_5
    move-exception v0

    #@6
    .line 752
    .local v0, e:Ljava/util/NoSuchElementException;
    goto :goto_4
.end method

.method public getUInt8(Ljava/lang/String;)I
    .registers 5
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 582
    const/16 v1, 0x11

    #@2
    const/4 v2, 0x0

    #@3
    invoke-virtual {p0, p1, v1, v2}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->lookup(Ljava/lang/String;II)Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;

    #@6
    move-result-object v0

    #@7
    .line 583
    .local v0, data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;
    if-eqz v0, :cond_e

    #@9
    .line 585
    invoke-virtual {v0}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readUInt8()I

    #@c
    move-result v1

    #@d
    return v1

    #@e
    .line 589
    :cond_e
    new-instance v1, Ljava/util/NoSuchElementException;

    #@10
    invoke-direct {v1, p1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    #@13
    throw v1
.end method

.method public getUInt8Default(Ljava/lang/String;I)I
    .registers 4
    .parameter "name"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 597
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->getUInt8(Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_3} :catch_5

    #@3
    move-result p2

    #@4
    .line 601
    .end local p2
    :goto_4
    return p2

    #@5
    .line 599
    .restart local p2
    :catch_5
    move-exception v0

    #@6
    .line 601
    .local v0, e:Ljava/util/NoSuchElementException;
    goto :goto_4
.end method

.method public hasField(Ljava/lang/String;II)Z
    .registers 5
    .parameter "name"
    .parameter "type"
    .parameter "index"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 51
    invoke-virtual {p0, p1, p2, p3}, Lcom/qualcomm/lib/location/mq_client/InPostcard;->lookup(Ljava/lang/String;II)Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;

    #@3
    move-result-object v0

    #@4
    if-eqz v0, :cond_8

    #@6
    .line 53
    const/4 v0, 0x1

    #@7
    .line 55
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method protected lookup(Ljava/lang/String;II)Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;
    .registers 16
    .parameter "name"
    .parameter "type"
    .parameter "index"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 60
    new-instance v4, Ljava/io/ByteArrayInputStream;

    #@2
    iget-object v8, p0, Lcom/qualcomm/lib/location/mq_client/InPostcard;->buf:[B

    #@4
    invoke-direct {v4, v8}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    #@7
    .line 61
    .local v4, in:Ljava/io/ByteArrayInputStream;
    new-instance v3, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;

    #@9
    invoke-direct {v3, v4}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;-><init>(Ljava/io/InputStream;)V

    #@c
    .line 63
    .local v3, data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;
    const/4 v2, 0x0

    #@d
    .line 66
    .local v2, count:I
    :cond_d
    :goto_d
    const/4 v8, 0x1

    #@e
    invoke-virtual {v4}, Ljava/io/ByteArrayInputStream;->available()I

    #@11
    move-result v9

    #@12
    if-ne v8, v9, :cond_16

    #@14
    .line 152
    const/4 v3, 0x0

    #@15
    .end local v3           #data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;
    :cond_15
    return-object v3

    #@16
    .line 72
    .restart local v3       #data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;
    :cond_16
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readUInt16()I

    #@19
    move-result v7

    #@1a
    .line 73
    .local v7, record_type:I
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readUInt16()I

    #@1d
    move-result v6

    #@1e
    .line 74
    .local v6, record_name_length_including_zero:I
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readCstr()Ljava/lang/String;

    #@21
    move-result-object v5

    #@22
    .line 76
    .local v5, record_name:Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@25
    move-result v8

    #@26
    add-int/lit8 v8, v8, 0x1

    #@28
    if-eq v6, v8, :cond_66

    #@2a
    .line 78
    const-string v8, "InPostcard"

    #@2c
    new-instance v9, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v10, "type "

    #@33
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v9

    #@37
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v9

    #@3b
    const-string v10, " name ["

    #@3d
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v9

    #@41
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v9

    #@45
    const-string v10, "] real string length "

    #@47
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v9

    #@4b
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@4e
    move-result v10

    #@4f
    add-int/lit8 v10, v10, 0x1

    #@51
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@54
    move-result-object v9

    #@55
    const-string v10, " but got "

    #@57
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v9

    #@5b
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v9

    #@5f
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v9

    #@63
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@66
    .line 83
    :cond_66
    if-ne v7, p2, :cond_72

    #@68
    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6b
    move-result v8

    #@6c
    if-eqz v8, :cond_72

    #@6e
    .line 85
    if-eq v2, p3, :cond_15

    #@70
    .line 90
    add-int/lit8 v2, v2, 0x1

    #@72
    .line 93
    :cond_72
    packed-switch v7, :pswitch_data_f8

    #@75
    .line 147
    :pswitch_75
    const-string v8, "InPostcard"

    #@77
    new-instance v9, Ljava/lang/StringBuilder;

    #@79
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@7c
    const-string v10, "unknown data type "

    #@7e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v9

    #@82
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@85
    move-result-object v9

    #@86
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@89
    move-result-object v9

    #@8a
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8d
    goto :goto_d

    #@8e
    .line 98
    :pswitch_8e
    const-wide/16 v8, 0x1

    #@90
    const-wide/16 v10, 0x1

    #@92
    invoke-virtual {v3, v10, v11}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->skip(J)J

    #@95
    move-result-wide v10

    #@96
    cmp-long v8, v8, v10

    #@98
    if-eqz v8, :cond_d

    #@9a
    .line 100
    const-string v8, "InPostcard"

    #@9c
    const-string v9, "skip failed 1"

    #@9e
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a1
    goto/16 :goto_d

    #@a3
    .line 105
    :pswitch_a3
    const-wide/16 v8, 0x2

    #@a5
    const-wide/16 v10, 0x2

    #@a7
    invoke-virtual {v3, v10, v11}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->skip(J)J

    #@aa
    move-result-wide v10

    #@ab
    cmp-long v8, v8, v10

    #@ad
    if-eqz v8, :cond_d

    #@af
    .line 107
    const-string v8, "InPostcard"

    #@b1
    const-string v9, "skip failed 2"

    #@b3
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b6
    goto/16 :goto_d

    #@b8
    .line 113
    :pswitch_b8
    const-wide/16 v8, 0x4

    #@ba
    const-wide/16 v10, 0x4

    #@bc
    invoke-virtual {v3, v10, v11}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->skip(J)J

    #@bf
    move-result-wide v10

    #@c0
    cmp-long v8, v8, v10

    #@c2
    if-eqz v8, :cond_d

    #@c4
    .line 115
    const-string v8, "InPostcard"

    #@c6
    const-string v9, "skip failed 4"

    #@c8
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@cb
    goto/16 :goto_d

    #@cd
    .line 121
    :pswitch_cd
    const-wide/16 v8, 0x8

    #@cf
    const-wide/16 v10, 0x8

    #@d1
    invoke-virtual {v3, v10, v11}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->skip(J)J

    #@d4
    move-result-wide v10

    #@d5
    cmp-long v8, v8, v10

    #@d7
    if-eqz v8, :cond_d

    #@d9
    .line 123
    const-string v8, "InPostcard"

    #@db
    const-string v9, "skip failed 8"

    #@dd
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e0
    goto/16 :goto_d

    #@e2
    .line 140
    :pswitch_e2
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->readUInt32()J

    #@e5
    move-result-wide v0

    #@e6
    .line 141
    .local v0, blob_length:J
    invoke-virtual {v3, v0, v1}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->skip(J)J

    #@e9
    move-result-wide v8

    #@ea
    cmp-long v8, v0, v8

    #@ec
    if-eqz v8, :cond_d

    #@ee
    .line 143
    const-string v8, "InPostcard"

    #@f0
    const-string v9, "skip failed multiple"

    #@f2
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f5
    goto/16 :goto_d

    #@f7
    .line 93
    nop

    #@f8
    :pswitch_data_f8
    .packed-switch 0x1
        :pswitch_e2
        :pswitch_75
        :pswitch_75
        :pswitch_75
        :pswitch_75
        :pswitch_75
        :pswitch_75
        :pswitch_75
        :pswitch_75
        :pswitch_cd
        :pswitch_cd
        :pswitch_b8
        :pswitch_b8
        :pswitch_a3
        :pswitch_a3
        :pswitch_8e
        :pswitch_8e
        :pswitch_8e
        :pswitch_e2
        :pswitch_e2
        :pswitch_cd
        :pswitch_b8
        :pswitch_75
        :pswitch_75
        :pswitch_75
        :pswitch_75
        :pswitch_75
        :pswitch_75
        :pswitch_75
        :pswitch_e2
        :pswitch_e2
        :pswitch_e2
        :pswitch_e2
        :pswitch_e2
        :pswitch_e2
        :pswitch_e2
        :pswitch_e2
        :pswitch_e2
        :pswitch_e2
        :pswitch_e2
    .end packed-switch
.end method
