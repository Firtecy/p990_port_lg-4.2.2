.class final enum Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;
.super Ljava/lang/Enum;
.source "OutPostcard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/lib/location/mq_client/OutPostcard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "CARD_STATE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;

.field public static final enum CS_ADD:Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;

.field public static final enum CS_PRE_INIT:Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;

.field public static final enum CS_SEALED:Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 54
    new-instance v0, Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;

    #@5
    const-string v1, "CS_PRE_INIT"

    #@7
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;-><init>(Ljava/lang/String;I)V

    #@a
    sput-object v0, Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;->CS_PRE_INIT:Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;

    #@c
    new-instance v0, Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;

    #@e
    const-string v1, "CS_ADD"

    #@10
    invoke-direct {v0, v1, v3}, Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;-><init>(Ljava/lang/String;I)V

    #@13
    sput-object v0, Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;->CS_ADD:Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;

    #@15
    new-instance v0, Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;

    #@17
    const-string v1, "CS_SEALED"

    #@19
    invoke-direct {v0, v1, v4}, Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;-><init>(Ljava/lang/String;I)V

    #@1c
    sput-object v0, Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;->CS_SEALED:Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;

    #@1e
    .line 52
    const/4 v0, 0x3

    #@1f
    new-array v0, v0, [Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;

    #@21
    sget-object v1, Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;->CS_PRE_INIT:Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;

    #@23
    aput-object v1, v0, v2

    #@25
    sget-object v1, Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;->CS_ADD:Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;

    #@27
    aput-object v1, v0, v3

    #@29
    sget-object v1, Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;->CS_SEALED:Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;

    #@2b
    aput-object v1, v0, v4

    #@2d
    sput-object v0, Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;->$VALUES:[Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;

    #@2f
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 52
    const-class v0, Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;
    .registers 1

    #@0
    .prologue
    .line 52
    sget-object v0, Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;->$VALUES:[Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;

    #@2
    invoke-virtual {v0}, [Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;

    #@8
    return-object v0
.end method
