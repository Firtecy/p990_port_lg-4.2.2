.class public Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;
.super Ljava/lang/Object;
.source "MessageQueueClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/lib/location/mq_client/MessageQueueClient$ReceiverThread;
    }
.end annotation


# static fields
.field protected static final TAG:Ljava/lang/String; = "MQClient"


# instance fields
.field handler:Landroid/os/Handler;

.field id_exception:I

.field id_new_msg:I

.field receiver_thread:Ljava/lang/Thread;

.field protected sock:Landroid/net/LocalSocket;

.field protected sock_name:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 3
    .parameter "sock_name"

    #@0
    .prologue
    .line 41
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 34
    new-instance v0, Landroid/net/LocalSocket;

    #@5
    invoke-direct {v0}, Landroid/net/LocalSocket;-><init>()V

    #@8
    iput-object v0, p0, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->sock:Landroid/net/LocalSocket;

    #@a
    .line 42
    iput-object p1, p0, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->sock_name:Ljava/lang/String;

    #@c
    .line 43
    return-void
.end method


# virtual methods
.method public connect()Z
    .registers 6

    #@0
    .prologue
    .line 69
    :try_start_0
    iget-object v1, p0, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->sock:Landroid/net/LocalSocket;

    #@2
    new-instance v2, Landroid/net/LocalSocketAddress;

    #@4
    iget-object v3, p0, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->sock_name:Ljava/lang/String;

    #@6
    sget-object v4, Landroid/net/LocalSocketAddress$Namespace;->FILESYSTEM:Landroid/net/LocalSocketAddress$Namespace;

    #@8
    invoke-direct {v2, v3, v4}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;Landroid/net/LocalSocketAddress$Namespace;)V

    #@b
    invoke-virtual {v1, v2}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_e} :catch_15

    #@e
    .line 75
    :goto_e
    iget-object v1, p0, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->sock:Landroid/net/LocalSocket;

    #@10
    invoke-virtual {v1}, Landroid/net/LocalSocket;->isConnected()Z

    #@13
    move-result v1

    #@14
    return v1

    #@15
    .line 71
    :catch_15
    move-exception v0

    #@16
    .line 73
    .local v0, e:Ljava/io/IOException;
    const-string v1, "MQClient"

    #@18
    new-instance v2, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v3, "unable to connect to ["

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    iget-object v3, p0, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->sock_name:Ljava/lang/String;

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    const-string v3, "]:["

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    #@32
    move-result-object v3

    #@33
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    const-string v3, "]"

    #@39
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v2

    #@3d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v2

    #@41
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    goto :goto_e
.end method

.method public disconnect()Z
    .registers 4

    #@0
    .prologue
    .line 49
    :try_start_0
    iget-object v1, p0, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->sock:Landroid/net/LocalSocket;

    #@2
    invoke-virtual {v1}, Landroid/net/LocalSocket;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    .line 57
    const/4 v1, 0x1

    #@6
    :goto_6
    return v1

    #@7
    .line 51
    :catch_7
    move-exception v0

    #@8
    .line 53
    .local v0, e:Ljava/io/IOException;
    const-string v1, "MQClient"

    #@a
    const-string v2, "unable to close socket"

    #@c
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 54
    const/4 v1, 0x0

    #@10
    goto :goto_6
.end method

.method public getOutputStream()Ljava/io/OutputStream;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->isConnected()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 82
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->sock:Landroid/net/LocalSocket;

    #@8
    invoke-virtual {v0}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    #@b
    move-result-object v0

    #@c
    .line 86
    :goto_c
    return-object v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method

.method public isConnected()Z
    .registers 2

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->sock:Landroid/net/LocalSocket;

    #@2
    invoke-virtual {v0}, Landroid/net/LocalSocket;->isConnected()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public startReceiverThread(Landroid/os/Handler;II)V
    .registers 6
    .parameter "handler"
    .parameter "id_new_msg"
    .parameter "id_exception"

    #@0
    .prologue
    .line 118
    iput-object p1, p0, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->handler:Landroid/os/Handler;

    #@2
    .line 119
    iput p2, p0, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->id_new_msg:I

    #@4
    .line 120
    iput p3, p0, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->id_exception:I

    #@6
    .line 122
    new-instance v0, Ljava/lang/Thread;

    #@8
    new-instance v1, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient$ReceiverThread;

    #@a
    invoke-direct {v1, p0}, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient$ReceiverThread;-><init>(Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;)V

    #@d
    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    #@10
    iput-object v0, p0, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->receiver_thread:Ljava/lang/Thread;

    #@12
    .line 123
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->receiver_thread:Ljava/lang/Thread;

    #@14
    const/4 v1, 0x1

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setDaemon(Z)V

    #@18
    .line 124
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->receiver_thread:Ljava/lang/Thread;

    #@1a
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    #@1d
    .line 125
    return-void
.end method
