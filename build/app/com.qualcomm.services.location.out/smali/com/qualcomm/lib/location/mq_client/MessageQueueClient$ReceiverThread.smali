.class public Lcom/qualcomm/lib/location/mq_client/MessageQueueClient$ReceiverThread;
.super Ljava/lang/Object;
.source "MessageQueueClient.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "ReceiverThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;


# direct methods
.method protected constructor <init>(Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 90
    iput-object p1, p0, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient$ReceiverThread;->this$0:Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 6

    #@0
    .prologue
    .line 101
    :goto_0
    :try_start_0
    new-instance v0, Lcom/qualcomm/lib/location/mq_client/InPostcard;

    #@2
    iget-object v3, p0, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient$ReceiverThread;->this$0:Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@4
    iget-object v3, v3, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->sock:Landroid/net/LocalSocket;

    #@6
    invoke-virtual {v3}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;

    #@9
    move-result-object v3

    #@a
    invoke-direct {v0, v3}, Lcom/qualcomm/lib/location/mq_client/InPostcard;-><init>(Ljava/io/InputStream;)V

    #@d
    .line 103
    .local v0, card:Lcom/qualcomm/lib/location/mq_client/InPostcard;
    iget-object v3, p0, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient$ReceiverThread;->this$0:Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@f
    iget-object v3, v3, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->handler:Landroid/os/Handler;

    #@11
    iget-object v4, p0, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient$ReceiverThread;->this$0:Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@13
    iget v4, v4, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->id_new_msg:I

    #@15
    invoke-virtual {v3, v4, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@18
    move-result-object v2

    #@19
    .line 104
    .local v2, msg:Landroid/os/Message;
    iget-object v3, p0, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient$ReceiverThread;->this$0:Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@1b
    iget-object v3, v3, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->handler:Landroid/os/Handler;

    #@1d
    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_20
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_20} :catch_21

    #@20
    goto :goto_0

    #@21
    .line 107
    .end local v0           #card:Lcom/qualcomm/lib/location/mq_client/InPostcard;
    .end local v2           #msg:Landroid/os/Message;
    :catch_21
    move-exception v1

    #@22
    .line 109
    .local v1, e:Ljava/io/IOException;
    iget-object v3, p0, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient$ReceiverThread;->this$0:Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@24
    iget-object v3, v3, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->handler:Landroid/os/Handler;

    #@26
    iget-object v4, p0, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient$ReceiverThread;->this$0:Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;

    #@28
    iget v4, v4, Lcom/qualcomm/lib/location/mq_client/MessageQueueClient;->id_exception:I

    #@2a
    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@2d
    .line 110
    const-string v3, "MQClient"

    #@2f
    const-string v4, "leaving receiver thread"

    #@31
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 112
    return-void
.end method
