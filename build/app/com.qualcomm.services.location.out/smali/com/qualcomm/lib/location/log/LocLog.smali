.class public Lcom/qualcomm/lib/location/log/LocLog;
.super Ljava/lang/Object;
.source "LocLog.java"


# static fields
.field public static final LOG_ALL:I = 0x64

.field public static final LOG_OFF:I = 0x0

.field public static final LOG_VERBOSE_LEVEL_DEBUG:I = 0x4

.field public static final LOG_VERBOSE_LEVEL_ERROR:I = 0x1

.field public static final LOG_VERBOSE_LEVEL_INFO:I = 0x3

.field public static final LOG_VERBOSE_LEVEL_VERBOSE:I = 0x5

.field public static final LOG_VERBOSE_LEVEL_WARNING:I = 0x2


# instance fields
.field private log_level:I


# direct methods
.method public constructor <init>(I)V
    .registers 2
    .parameter "level"

    #@0
    .prologue
    .line 36
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 37
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/log/LocLog;->setLevel(I)V

    #@6
    .line 38
    return-void
.end method


# virtual methods
.method public declared-synchronized d(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "tag"
    .parameter "msg"

    #@0
    .prologue
    .line 55
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Lcom/qualcomm/lib/location/log/LocLog;->log_level:I

    #@3
    const/4 v1, 0x4

    #@4
    if-lt v0, v1, :cond_9

    #@6
    .line 57
    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_b

    #@9
    .line 59
    :cond_9
    monitor-exit p0

    #@a
    return-void

    #@b
    .line 55
    :catchall_b
    move-exception v0

    #@c
    monitor-exit p0

    #@d
    throw v0
.end method

.method public declared-synchronized e(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "tag"
    .parameter "msg"

    #@0
    .prologue
    .line 79
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Lcom/qualcomm/lib/location/log/LocLog;->log_level:I

    #@3
    const/4 v1, 0x1

    #@4
    if-lt v0, v1, :cond_9

    #@6
    .line 81
    invoke-static {p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_b

    #@9
    .line 83
    :cond_9
    monitor-exit p0

    #@a
    return-void

    #@b
    .line 79
    :catchall_b
    move-exception v0

    #@c
    monitor-exit p0

    #@d
    throw v0
.end method

.method public declared-synchronized i(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "tag"
    .parameter "msg"

    #@0
    .prologue
    .line 63
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Lcom/qualcomm/lib/location/log/LocLog;->log_level:I

    #@3
    const/4 v1, 0x3

    #@4
    if-lt v0, v1, :cond_9

    #@6
    .line 65
    invoke-static {p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_b

    #@9
    .line 67
    :cond_9
    monitor-exit p0

    #@a
    return-void

    #@b
    .line 63
    :catchall_b
    move-exception v0

    #@c
    monitor-exit p0

    #@d
    throw v0
.end method

.method public declared-synchronized setLevel(I)V
    .registers 3
    .parameter "level"

    #@0
    .prologue
    .line 42
    monitor-enter p0

    #@1
    :try_start_1
    iput p1, p0, Lcom/qualcomm/lib/location/log/LocLog;->log_level:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    .line 43
    monitor-exit p0

    #@4
    return-void

    #@5
    .line 42
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized v(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "tag"
    .parameter "msg"

    #@0
    .prologue
    .line 47
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Lcom/qualcomm/lib/location/log/LocLog;->log_level:I

    #@3
    const/4 v1, 0x5

    #@4
    if-lt v0, v1, :cond_9

    #@6
    .line 49
    invoke-static {p1, p2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_b

    #@9
    .line 51
    :cond_9
    monitor-exit p0

    #@a
    return-void

    #@b
    .line 47
    :catchall_b
    move-exception v0

    #@c
    monitor-exit p0

    #@d
    throw v0
.end method

.method public declared-synchronized w(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "tag"
    .parameter "msg"

    #@0
    .prologue
    .line 71
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Lcom/qualcomm/lib/location/log/LocLog;->log_level:I

    #@3
    const/4 v1, 0x2

    #@4
    if-lt v0, v1, :cond_9

    #@6
    .line 73
    invoke-static {p1, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_b

    #@9
    .line 75
    :cond_9
    monitor-exit p0

    #@a
    return-void

    #@b
    .line 71
    :catchall_b
    move-exception v0

    #@c
    monitor-exit p0

    #@d
    throw v0
.end method
