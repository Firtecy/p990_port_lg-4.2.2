.class public Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;
.super Ljava/io/FilterInputStream;
.source "NativeOrderDataInputStream.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;
    }
.end annotation


# instance fields
.field protected native_is_big_endian:Z


# direct methods
.method protected constructor <init>(Ljava/io/InputStream;)V
    .registers 4
    .parameter "in"

    #@0
    .prologue
    .line 37
    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    #@3
    .line 39
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    #@6
    move-result-object v0

    #@7
    sget-object v1, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_13

    #@f
    .line 41
    const/4 v0, 0x1

    #@10
    iput-boolean v0, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->native_is_big_endian:Z

    #@12
    .line 47
    :goto_12
    return-void

    #@13
    .line 45
    :cond_13
    const/4 v0, 0x0

    #@14
    iput-boolean v0, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->native_is_big_endian:Z

    #@16
    goto :goto_12
.end method


# virtual methods
.method public readBoolean()Z
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 52
    iget-object v2, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->in:Ljava/io/InputStream;

    #@3
    invoke-virtual {v2}, Ljava/io/InputStream;->read()I

    #@6
    move-result v0

    #@7
    .line 53
    .local v0, result:I
    if-gez v0, :cond_f

    #@9
    .line 55
    new-instance v1, Ljava/io/EOFException;

    #@b
    invoke-direct {v1}, Ljava/io/EOFException;-><init>()V

    #@e
    throw v1

    #@f
    .line 57
    :cond_f
    if-ne v1, v0, :cond_12

    #@11
    .line 63
    :goto_11
    return v1

    #@12
    .line 61
    :cond_12
    if-nez v0, :cond_16

    #@14
    .line 63
    const/4 v1, 0x0

    #@15
    goto :goto_11

    #@16
    .line 67
    :cond_16
    new-instance v1, Ljava/io/IOException;

    #@18
    const-string v2, "invalid value"

    #@1a
    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v1
.end method

.method public readCstr()Ljava/lang/String;
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 379
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    #@2
    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    #@5
    .line 381
    .local v0, array:Ljava/io/ByteArrayOutputStream;
    :goto_5
    iget-object v2, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->in:Ljava/io/InputStream;

    #@7
    invoke-virtual {v2}, Ljava/io/InputStream;->read()I

    #@a
    move-result v1

    #@b
    .local v1, t:I
    if-eqz v1, :cond_19

    #@d
    .line 383
    if-gez v1, :cond_15

    #@f
    .line 385
    new-instance v2, Ljava/io/EOFException;

    #@11
    invoke-direct {v2}, Ljava/io/EOFException;-><init>()V

    #@14
    throw v2

    #@15
    .line 387
    :cond_15
    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@18
    goto :goto_5

    #@19
    .line 390
    :cond_19
    new-instance v2, Ljava/lang/String;

    #@1b
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@1e
    move-result-object v3

    #@1f
    const-string v4, "UTF-8"

    #@21
    invoke-direct {v2, v3, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    #@24
    return-object v2
.end method

.method public readDouble()D
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 339
    const/16 v4, 0x8

    #@2
    new-array v0, v4, [B

    #@4
    .line 340
    .local v0, b:[B
    const/4 v2, 0x0

    #@5
    .local v2, i:I
    :goto_5
    array-length v4, v0

    #@6
    if-ge v2, v4, :cond_1c

    #@8
    .line 342
    iget-object v4, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->in:Ljava/io/InputStream;

    #@a
    invoke-virtual {v4}, Ljava/io/InputStream;->read()I

    #@d
    move-result v3

    #@e
    .line 343
    .local v3, t:I
    if-gez v3, :cond_16

    #@10
    .line 345
    new-instance v4, Ljava/io/EOFException;

    #@12
    invoke-direct {v4}, Ljava/io/EOFException;-><init>()V

    #@15
    throw v4

    #@16
    .line 347
    :cond_16
    int-to-byte v4, v3

    #@17
    aput-byte v4, v0, v2

    #@19
    .line 340
    add-int/lit8 v2, v2, 0x1

    #@1b
    goto :goto_5

    #@1c
    .line 352
    .end local v3           #t:I
    :cond_1c
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    #@1f
    move-result-object v1

    #@20
    .line 353
    .local v1, bb:Ljava/nio/ByteBuffer;
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    #@23
    move-result-object v4

    #@24
    invoke-virtual {v1, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    #@27
    .line 354
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getDouble()D

    #@2a
    move-result-wide v4

    #@2b
    return-wide v4
.end method

.method public readFloat()F
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 359
    const/4 v4, 0x4

    #@1
    new-array v0, v4, [B

    #@3
    .line 360
    .local v0, b:[B
    const/4 v2, 0x0

    #@4
    .local v2, i:I
    :goto_4
    array-length v4, v0

    #@5
    if-ge v2, v4, :cond_1b

    #@7
    .line 362
    iget-object v4, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->in:Ljava/io/InputStream;

    #@9
    invoke-virtual {v4}, Ljava/io/InputStream;->read()I

    #@c
    move-result v3

    #@d
    .line 363
    .local v3, t:I
    if-gez v3, :cond_15

    #@f
    .line 365
    new-instance v4, Ljava/io/EOFException;

    #@11
    invoke-direct {v4}, Ljava/io/EOFException;-><init>()V

    #@14
    throw v4

    #@15
    .line 367
    :cond_15
    int-to-byte v4, v3

    #@16
    aput-byte v4, v0, v2

    #@18
    .line 360
    add-int/lit8 v2, v2, 0x1

    #@1a
    goto :goto_4

    #@1b
    .line 372
    .end local v3           #t:I
    :cond_1b
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    #@1e
    move-result-object v1

    #@1f
    .line 373
    .local v1, bb:Ljava/nio/ByteBuffer;
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    #@22
    move-result-object v4

    #@23
    invoke-virtual {v1, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    #@26
    .line 374
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getFloat()F

    #@29
    move-result v4

    #@2a
    return v4
.end method

.method public readInt16()I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 142
    iget-object v3, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->in:Ljava/io/InputStream;

    #@2
    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    #@5
    move-result v0

    #@6
    .line 143
    .local v0, b0:I
    if-gez v0, :cond_e

    #@8
    .line 145
    new-instance v3, Ljava/io/EOFException;

    #@a
    invoke-direct {v3}, Ljava/io/EOFException;-><init>()V

    #@d
    throw v3

    #@e
    .line 147
    :cond_e
    iget-object v3, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->in:Ljava/io/InputStream;

    #@10
    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    #@13
    move-result v1

    #@14
    .line 148
    .local v1, b1:I
    if-gez v1, :cond_1c

    #@16
    .line 150
    new-instance v3, Ljava/io/EOFException;

    #@18
    invoke-direct {v3}, Ljava/io/EOFException;-><init>()V

    #@1b
    throw v3

    #@1c
    .line 155
    :cond_1c
    iget-boolean v3, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->native_is_big_endian:Z

    #@1e
    if-eqz v3, :cond_28

    #@20
    .line 157
    int-to-short v2, v0

    #@21
    .line 158
    .local v2, value:S
    shl-int/lit8 v3, v2, 0x8

    #@23
    int-to-short v2, v3

    #@24
    .line 159
    or-int v3, v2, v1

    #@26
    int-to-short v2, v3

    #@27
    .line 167
    :goto_27
    return v2

    #@28
    .line 163
    .end local v2           #value:S
    :cond_28
    int-to-short v2, v1

    #@29
    .line 164
    .restart local v2       #value:S
    shl-int/lit8 v3, v2, 0x8

    #@2b
    int-to-short v2, v3

    #@2c
    .line 165
    or-int v3, v2, v0

    #@2e
    int-to-short v2, v3

    #@2f
    goto :goto_27
.end method

.method public readInt32()I
    .registers 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v7, 0x3

    #@1
    const/4 v6, 0x2

    #@2
    const/4 v5, 0x1

    #@3
    const/4 v4, 0x0

    #@4
    .line 210
    const/4 v3, 0x4

    #@5
    new-array v0, v3, [I

    #@7
    .line 211
    .local v0, b:[I
    const/4 v1, 0x0

    #@8
    .local v1, i:I
    :goto_8
    array-length v3, v0

    #@9
    if-ge v1, v3, :cond_20

    #@b
    .line 213
    iget-object v3, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->in:Ljava/io/InputStream;

    #@d
    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    #@10
    move-result v3

    #@11
    aput v3, v0, v1

    #@13
    .line 214
    aget v3, v0, v1

    #@15
    if-gez v3, :cond_1d

    #@17
    .line 216
    new-instance v3, Ljava/io/EOFException;

    #@19
    invoke-direct {v3}, Ljava/io/EOFException;-><init>()V

    #@1c
    throw v3

    #@1d
    .line 211
    :cond_1d
    add-int/lit8 v1, v1, 0x1

    #@1f
    goto :goto_8

    #@20
    .line 222
    :cond_20
    iget-boolean v3, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->native_is_big_endian:Z

    #@22
    if-eqz v3, :cond_36

    #@24
    .line 224
    aget v2, v0, v4

    #@26
    .line 225
    .local v2, value:I
    shl-int/lit8 v2, v2, 0x8

    #@28
    .line 226
    aget v3, v0, v5

    #@2a
    or-int/2addr v2, v3

    #@2b
    .line 227
    shl-int/lit8 v2, v2, 0x8

    #@2d
    .line 228
    aget v3, v0, v6

    #@2f
    or-int/2addr v2, v3

    #@30
    .line 229
    shl-int/lit8 v2, v2, 0x8

    #@32
    .line 230
    aget v3, v0, v7

    #@34
    or-int/2addr v2, v3

    #@35
    .line 242
    :goto_35
    return v2

    #@36
    .line 234
    .end local v2           #value:I
    :cond_36
    aget v2, v0, v7

    #@38
    .line 235
    .restart local v2       #value:I
    shl-int/lit8 v2, v2, 0x8

    #@3a
    .line 236
    aget v3, v0, v6

    #@3c
    or-int/2addr v2, v3

    #@3d
    .line 237
    shl-int/lit8 v2, v2, 0x8

    #@3f
    .line 238
    aget v3, v0, v5

    #@41
    or-int/2addr v2, v3

    #@42
    .line 239
    shl-int/lit8 v2, v2, 0x8

    #@44
    .line 240
    aget v3, v0, v4

    #@46
    or-int/2addr v2, v3

    #@47
    goto :goto_35
.end method

.method public readInt64()J
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 319
    const/16 v4, 0x8

    #@2
    new-array v0, v4, [B

    #@4
    .line 320
    .local v0, b:[B
    const/4 v2, 0x0

    #@5
    .local v2, i:I
    :goto_5
    array-length v4, v0

    #@6
    if-ge v2, v4, :cond_1c

    #@8
    .line 322
    iget-object v4, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->in:Ljava/io/InputStream;

    #@a
    invoke-virtual {v4}, Ljava/io/InputStream;->read()I

    #@d
    move-result v3

    #@e
    .line 323
    .local v3, t:I
    if-gez v3, :cond_16

    #@10
    .line 325
    new-instance v4, Ljava/io/EOFException;

    #@12
    invoke-direct {v4}, Ljava/io/EOFException;-><init>()V

    #@15
    throw v4

    #@16
    .line 327
    :cond_16
    int-to-byte v4, v3

    #@17
    aput-byte v4, v0, v2

    #@19
    .line 320
    add-int/lit8 v2, v2, 0x1

    #@1b
    goto :goto_5

    #@1c
    .line 332
    .end local v3           #t:I
    :cond_1c
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    #@1f
    move-result-object v1

    #@20
    .line 333
    .local v1, bb:Ljava/nio/ByteBuffer;
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    #@23
    move-result-object v4

    #@24
    invoke-virtual {v1, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    #@27
    .line 334
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getLong()J

    #@2a
    move-result-wide v4

    #@2b
    return-wide v4
.end method

.method public readInt8()I
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 92
    iget-object v1, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->in:Ljava/io/InputStream;

    #@2
    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    #@5
    move-result v0

    #@6
    .line 93
    .local v0, result:I
    if-gez v0, :cond_e

    #@8
    .line 95
    new-instance v1, Ljava/io/EOFException;

    #@a
    invoke-direct {v1}, Ljava/io/EOFException;-><init>()V

    #@d
    throw v1

    #@e
    .line 97
    :cond_e
    const/16 v1, -0x80

    #@10
    if-lt v0, v1, :cond_16

    #@12
    const/16 v1, 0x7f

    #@14
    if-le v0, v1, :cond_1e

    #@16
    .line 99
    :cond_16
    new-instance v1, Ljava/io/IOException;

    #@18
    const-string v2, "invalid value"

    #@1a
    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v1

    #@1e
    .line 103
    :cond_1e
    return v0
.end method

.method public readUInt16()I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 110
    iget-object v3, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->in:Ljava/io/InputStream;

    #@2
    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    #@5
    move-result v0

    #@6
    .line 111
    .local v0, b0:I
    if-gez v0, :cond_e

    #@8
    .line 113
    new-instance v3, Ljava/io/EOFException;

    #@a
    invoke-direct {v3}, Ljava/io/EOFException;-><init>()V

    #@d
    throw v3

    #@e
    .line 115
    :cond_e
    iget-object v3, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->in:Ljava/io/InputStream;

    #@10
    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    #@13
    move-result v1

    #@14
    .line 116
    .local v1, b1:I
    if-gez v1, :cond_1c

    #@16
    .line 118
    new-instance v3, Ljava/io/EOFException;

    #@18
    invoke-direct {v3}, Ljava/io/EOFException;-><init>()V

    #@1b
    throw v3

    #@1c
    .line 124
    :cond_1c
    iget-boolean v3, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->native_is_big_endian:Z

    #@1e
    if-eqz v3, :cond_25

    #@20
    .line 126
    move v2, v0

    #@21
    .line 127
    .local v2, value:I
    shl-int/lit8 v2, v2, 0x8

    #@23
    .line 128
    or-int/2addr v2, v1

    #@24
    .line 136
    :goto_24
    return v2

    #@25
    .line 132
    .end local v2           #value:I
    :cond_25
    move v2, v1

    #@26
    .line 133
    .restart local v2       #value:I
    shl-int/lit8 v2, v2, 0x8

    #@28
    .line 134
    or-int/2addr v2, v0

    #@29
    goto :goto_24
.end method

.method public readUInt32()J
    .registers 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v10, 0x3

    #@1
    const/4 v9, 0x2

    #@2
    const/4 v8, 0x1

    #@3
    const/4 v7, 0x0

    #@4
    const/16 v6, 0x8

    #@6
    .line 172
    const/4 v4, 0x4

    #@7
    new-array v0, v4, [I

    #@9
    .line 173
    .local v0, b:[I
    const/4 v1, 0x0

    #@a
    .local v1, i:I
    :goto_a
    array-length v4, v0

    #@b
    if-ge v1, v4, :cond_22

    #@d
    .line 175
    iget-object v4, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->in:Ljava/io/InputStream;

    #@f
    invoke-virtual {v4}, Ljava/io/InputStream;->read()I

    #@12
    move-result v4

    #@13
    aput v4, v0, v1

    #@15
    .line 176
    aget v4, v0, v1

    #@17
    if-gez v4, :cond_1f

    #@19
    .line 178
    new-instance v4, Ljava/io/EOFException;

    #@1b
    invoke-direct {v4}, Ljava/io/EOFException;-><init>()V

    #@1e
    throw v4

    #@1f
    .line 173
    :cond_1f
    add-int/lit8 v1, v1, 0x1

    #@21
    goto :goto_a

    #@22
    .line 185
    :cond_22
    iget-boolean v4, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->native_is_big_endian:Z

    #@24
    if-eqz v4, :cond_39

    #@26
    .line 187
    aget v4, v0, v7

    #@28
    int-to-long v2, v4

    #@29
    .line 188
    .local v2, value:J
    shl-long/2addr v2, v6

    #@2a
    .line 189
    aget v4, v0, v8

    #@2c
    int-to-long v4, v4

    #@2d
    or-long/2addr v2, v4

    #@2e
    .line 190
    shl-long/2addr v2, v6

    #@2f
    .line 191
    aget v4, v0, v9

    #@31
    int-to-long v4, v4

    #@32
    or-long/2addr v2, v4

    #@33
    .line 192
    shl-long/2addr v2, v6

    #@34
    .line 193
    aget v4, v0, v10

    #@36
    int-to-long v4, v4

    #@37
    or-long/2addr v2, v4

    #@38
    .line 205
    :goto_38
    return-wide v2

    #@39
    .line 197
    .end local v2           #value:J
    :cond_39
    aget v4, v0, v10

    #@3b
    int-to-long v2, v4

    #@3c
    .line 198
    .restart local v2       #value:J
    shl-long/2addr v2, v6

    #@3d
    .line 199
    aget v4, v0, v9

    #@3f
    int-to-long v4, v4

    #@40
    or-long/2addr v2, v4

    #@41
    .line 200
    shl-long/2addr v2, v6

    #@42
    .line 201
    aget v4, v0, v8

    #@44
    int-to-long v4, v4

    #@45
    or-long/2addr v2, v4

    #@46
    .line 202
    shl-long/2addr v2, v6

    #@47
    .line 203
    aget v4, v0, v7

    #@49
    int-to-long v4, v4

    #@4a
    or-long/2addr v2, v4

    #@4b
    goto :goto_38
.end method

.method public readUInt64()Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;
    .registers 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v11, 0x2

    #@1
    const/4 v10, 0x7

    #@2
    const/4 v9, 0x1

    #@3
    const/4 v8, 0x0

    #@4
    const/16 v7, 0x8

    #@6
    .line 253
    new-array v0, v7, [I

    #@8
    .line 254
    .local v0, b:[I
    const/4 v1, 0x0

    #@9
    .local v1, i:I
    :goto_9
    array-length v3, v0

    #@a
    if-ge v1, v3, :cond_21

    #@c
    .line 256
    iget-object v3, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->in:Ljava/io/InputStream;

    #@e
    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    #@11
    move-result v3

    #@12
    aput v3, v0, v1

    #@14
    .line 257
    aget v3, v0, v1

    #@16
    if-gez v3, :cond_1e

    #@18
    .line 259
    new-instance v3, Ljava/io/EOFException;

    #@1a
    invoke-direct {v3}, Ljava/io/EOFException;-><init>()V

    #@1d
    throw v3

    #@1e
    .line 254
    :cond_1e
    add-int/lit8 v1, v1, 0x1

    #@20
    goto :goto_9

    #@21
    .line 263
    :cond_21
    new-instance v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;

    #@23
    invoke-direct {v2, p0}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;-><init>(Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;)V

    #@26
    .line 264
    .local v2, result:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;
    const-wide/16 v3, 0x0

    #@28
    iput-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@2a
    .line 265
    iput-boolean v8, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->highest_bit_set:Z

    #@2c
    .line 266
    iget-boolean v3, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->native_is_big_endian:Z

    #@2e
    if-eqz v3, :cond_a3

    #@30
    .line 268
    aget v3, v0, v8

    #@32
    and-int/lit16 v3, v3, 0x80

    #@34
    if-eqz v3, :cond_3e

    #@36
    .line 270
    iput-boolean v9, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->highest_bit_set:Z

    #@38
    .line 271
    aget v3, v0, v8

    #@3a
    xor-int/lit16 v3, v3, 0x80

    #@3c
    aput v3, v0, v8

    #@3e
    .line 274
    :cond_3e
    aget v3, v0, v8

    #@40
    int-to-long v3, v3

    #@41
    iput-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@43
    .line 275
    iget-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@45
    shl-long/2addr v3, v7

    #@46
    iput-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@48
    .line 276
    iget-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@4a
    aget v5, v0, v9

    #@4c
    int-to-long v5, v5

    #@4d
    or-long/2addr v3, v5

    #@4e
    iput-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@50
    .line 277
    iget-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@52
    shl-long/2addr v3, v7

    #@53
    iput-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@55
    .line 278
    iget-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@57
    aget v5, v0, v11

    #@59
    int-to-long v5, v5

    #@5a
    or-long/2addr v3, v5

    #@5b
    iput-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@5d
    .line 279
    iget-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@5f
    shl-long/2addr v3, v7

    #@60
    iput-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@62
    .line 280
    iget-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@64
    const/4 v5, 0x3

    #@65
    aget v5, v0, v5

    #@67
    int-to-long v5, v5

    #@68
    or-long/2addr v3, v5

    #@69
    iput-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@6b
    .line 281
    iget-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@6d
    shl-long/2addr v3, v7

    #@6e
    iput-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@70
    .line 282
    iget-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@72
    const/4 v5, 0x4

    #@73
    aget v5, v0, v5

    #@75
    int-to-long v5, v5

    #@76
    or-long/2addr v3, v5

    #@77
    iput-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@79
    .line 283
    iget-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@7b
    shl-long/2addr v3, v7

    #@7c
    iput-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@7e
    .line 284
    iget-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@80
    const/4 v5, 0x5

    #@81
    aget v5, v0, v5

    #@83
    int-to-long v5, v5

    #@84
    or-long/2addr v3, v5

    #@85
    iput-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@87
    .line 285
    iget-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@89
    shl-long/2addr v3, v7

    #@8a
    iput-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@8c
    .line 286
    iget-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@8e
    const/4 v5, 0x6

    #@8f
    aget v5, v0, v5

    #@91
    int-to-long v5, v5

    #@92
    or-long/2addr v3, v5

    #@93
    iput-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@95
    .line 287
    iget-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@97
    shl-long/2addr v3, v7

    #@98
    iput-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@9a
    .line 288
    iget-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@9c
    aget v5, v0, v10

    #@9e
    int-to-long v5, v5

    #@9f
    or-long/2addr v3, v5

    #@a0
    iput-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@a2
    .line 314
    :goto_a2
    return-object v2

    #@a3
    .line 292
    :cond_a3
    aget v3, v0, v10

    #@a5
    and-int/lit16 v3, v3, 0x80

    #@a7
    if-eqz v3, :cond_b1

    #@a9
    .line 294
    iput-boolean v9, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->highest_bit_set:Z

    #@ab
    .line 295
    aget v3, v0, v10

    #@ad
    xor-int/lit16 v3, v3, 0x80

    #@af
    aput v3, v0, v10

    #@b1
    .line 298
    :cond_b1
    aget v3, v0, v10

    #@b3
    int-to-long v3, v3

    #@b4
    iput-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@b6
    .line 299
    iget-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@b8
    shl-long/2addr v3, v7

    #@b9
    iput-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@bb
    .line 300
    iget-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@bd
    const/4 v5, 0x6

    #@be
    aget v5, v0, v5

    #@c0
    int-to-long v5, v5

    #@c1
    or-long/2addr v3, v5

    #@c2
    iput-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@c4
    .line 301
    iget-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@c6
    shl-long/2addr v3, v7

    #@c7
    iput-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@c9
    .line 302
    iget-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@cb
    const/4 v5, 0x5

    #@cc
    aget v5, v0, v5

    #@ce
    int-to-long v5, v5

    #@cf
    or-long/2addr v3, v5

    #@d0
    iput-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@d2
    .line 303
    iget-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@d4
    shl-long/2addr v3, v7

    #@d5
    iput-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@d7
    .line 304
    iget-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@d9
    const/4 v5, 0x4

    #@da
    aget v5, v0, v5

    #@dc
    int-to-long v5, v5

    #@dd
    or-long/2addr v3, v5

    #@de
    iput-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@e0
    .line 305
    iget-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@e2
    shl-long/2addr v3, v7

    #@e3
    iput-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@e5
    .line 306
    iget-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@e7
    const/4 v5, 0x3

    #@e8
    aget v5, v0, v5

    #@ea
    int-to-long v5, v5

    #@eb
    or-long/2addr v3, v5

    #@ec
    iput-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@ee
    .line 307
    iget-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@f0
    shl-long/2addr v3, v7

    #@f1
    iput-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@f3
    .line 308
    iget-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@f5
    aget v5, v0, v11

    #@f7
    int-to-long v5, v5

    #@f8
    or-long/2addr v3, v5

    #@f9
    iput-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@fb
    .line 309
    iget-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@fd
    shl-long/2addr v3, v7

    #@fe
    iput-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@100
    .line 310
    iget-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@102
    aget v5, v0, v9

    #@104
    int-to-long v5, v5

    #@105
    or-long/2addr v3, v5

    #@106
    iput-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@108
    .line 311
    iget-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@10a
    shl-long/2addr v3, v7

    #@10b
    iput-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@10d
    .line 312
    iget-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@10f
    aget v5, v0, v8

    #@111
    int-to-long v5, v5

    #@112
    or-long/2addr v3, v5

    #@113
    iput-wide v3, v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream$Uint64;->value:J

    #@115
    goto :goto_a2
.end method

.method public readUInt8()I
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 74
    iget-object v1, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataInputStream;->in:Ljava/io/InputStream;

    #@2
    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    #@5
    move-result v0

    #@6
    .line 75
    .local v0, result:I
    if-gez v0, :cond_e

    #@8
    .line 77
    new-instance v1, Ljava/io/EOFException;

    #@a
    invoke-direct {v1}, Ljava/io/EOFException;-><init>()V

    #@d
    throw v1

    #@e
    .line 79
    :cond_e
    if-ltz v0, :cond_14

    #@10
    const/16 v1, 0xff

    #@12
    if-le v0, v1, :cond_1c

    #@14
    .line 81
    :cond_14
    new-instance v1, Ljava/io/IOException;

    #@16
    const-string v2, "invalid value"

    #@18
    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v1

    #@1c
    .line 85
    :cond_1c
    return v0
.end method
