.class public Lcom/qualcomm/lib/location/mq_client/OutPostcard;
.super Ljava/lang/Object;
.source "OutPostcard.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;
    }
.end annotation


# static fields
.field public static final FtArrayBool:I = 0x26

.field public static final FtArrayDouble:I = 0x27

.field public static final FtArrayFloat:I = 0x28

.field public static final FtArrayInt16:I = 0x22

.field public static final FtArrayInt32:I = 0x20

.field public static final FtArrayInt64:I = 0x1e

.field public static final FtArrayInt8:I = 0x24

.field public static final FtArrayUInt16:I = 0x23

.field public static final FtArrayUInt32:I = 0x21

.field public static final FtArrayUInt64:I = 0x1f

.field public static final FtArrayUInt8:I = 0x25

.field public static final FtBlob:I = 0x14

.field public static final FtBool:I = 0x12

.field public static final FtCard:I = 0x1

.field public static final FtDouble:I = 0x15

.field public static final FtFloat:I = 0x16

.field public static final FtInt16:I = 0xe

.field public static final FtInt32:I = 0xc

.field public static final FtInt64:I = 0xa

.field public static final FtInt8:I = 0x10

.field public static final FtString:I = 0x13

.field public static final FtUInt16:I = 0xf

.field public static final FtUInt32:I = 0xd

.field public static final FtUInt64:I = 0xb

.field public static final FtUInt8:I = 0x11

.field public static final FtUnknown:I


# instance fields
.field private buf:Ljava/io/ByteArrayOutputStream;

.field private data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

.field private m_state:Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;

.field private sealed_card:[B


# direct methods
.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    .line 63
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 57
    sget-object v0, Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;->CS_PRE_INIT:Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;

    #@5
    iput-object v0, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->m_state:Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;

    #@7
    .line 59
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    #@9
    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    #@c
    iput-object v0, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->buf:Ljava/io/ByteArrayOutputStream;

    #@e
    .line 60
    new-instance v0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@10
    iget-object v1, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->buf:Ljava/io/ByteArrayOutputStream;

    #@12
    invoke-direct {v0, v1}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@15
    iput-object v0, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@17
    .line 64
    return-void
.end method


# virtual methods
.method public addArrayBool(Ljava/lang/String;[Z)V
    .registers 10
    .parameter "name"
    .parameter "array"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 227
    iget-object v4, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@2
    const/16 v5, 0x26

    #@4
    invoke-virtual {v4, v5}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt16(I)V

    #@7
    .line 228
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addName(Ljava/lang/String;)V

    #@a
    .line 229
    iget-object v4, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@c
    array-length v5, p2

    #@d
    mul-int/lit8 v5, v5, 0x1

    #@f
    int-to-long v5, v5

    #@10
    invoke-virtual {v4, v5, v6}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt32(J)V

    #@13
    .line 230
    move-object v0, p2

    #@14
    .local v0, arr$:[Z
    array-length v3, v0

    #@15
    .local v3, len$:I
    const/4 v2, 0x0

    #@16
    .local v2, i$:I
    :goto_16
    if-ge v2, v3, :cond_22

    #@18
    aget-boolean v1, v0, v2

    #@1a
    .line 232
    .local v1, flag:Z
    iget-object v4, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@1c
    invoke-virtual {v4, v1}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeBoolean(Z)V

    #@1f
    .line 230
    add-int/lit8 v2, v2, 0x1

    #@21
    goto :goto_16

    #@22
    .line 234
    .end local v1           #flag:Z
    :cond_22
    return-void
.end method

.method public addArrayDouble(Ljava/lang/String;[D)V
    .registers 11
    .parameter "name"
    .parameter "array"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 304
    iget-object v5, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@2
    const/16 v6, 0x27

    #@4
    invoke-virtual {v5, v6}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt16(I)V

    #@7
    .line 305
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addName(Ljava/lang/String;)V

    #@a
    .line 306
    iget-object v5, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@c
    array-length v6, p2

    #@d
    mul-int/lit8 v6, v6, 0x8

    #@f
    int-to-long v6, v6

    #@10
    invoke-virtual {v5, v6, v7}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt32(J)V

    #@13
    .line 307
    move-object v0, p2

    #@14
    .local v0, arr$:[D
    array-length v2, v0

    #@15
    .local v2, len$:I
    const/4 v1, 0x0

    #@16
    .local v1, i$:I
    :goto_16
    if-ge v1, v2, :cond_22

    #@18
    aget-wide v3, v0, v1

    #@1a
    .line 309
    .local v3, v:D
    iget-object v5, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@1c
    invoke-virtual {v5, v3, v4}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeDouble(D)V

    #@1f
    .line 307
    add-int/lit8 v1, v1, 0x1

    #@21
    goto :goto_16

    #@22
    .line 311
    .end local v3           #v:D
    :cond_22
    return-void
.end method

.method public addArrayFloat(Ljava/lang/String;[F)V
    .registers 10
    .parameter "name"
    .parameter "array"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 315
    iget-object v4, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@2
    const/16 v5, 0x28

    #@4
    invoke-virtual {v4, v5}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt16(I)V

    #@7
    .line 316
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addName(Ljava/lang/String;)V

    #@a
    .line 317
    iget-object v4, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@c
    array-length v5, p2

    #@d
    mul-int/lit8 v5, v5, 0x4

    #@f
    int-to-long v5, v5

    #@10
    invoke-virtual {v4, v5, v6}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt32(J)V

    #@13
    .line 318
    move-object v0, p2

    #@14
    .local v0, arr$:[F
    array-length v2, v0

    #@15
    .local v2, len$:I
    const/4 v1, 0x0

    #@16
    .local v1, i$:I
    :goto_16
    if-ge v1, v2, :cond_22

    #@18
    aget v3, v0, v1

    #@1a
    .line 320
    .local v3, v:F
    iget-object v4, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@1c
    invoke-virtual {v4, v3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeFloat(F)V

    #@1f
    .line 318
    add-int/lit8 v1, v1, 0x1

    #@21
    goto :goto_16

    #@22
    .line 322
    .end local v3           #v:F
    :cond_22
    return-void
.end method

.method public addArrayInt16(Ljava/lang/String;[I)V
    .registers 10
    .parameter "name"
    .parameter "array"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 238
    iget-object v4, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@2
    const/16 v5, 0x22

    #@4
    invoke-virtual {v4, v5}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt16(I)V

    #@7
    .line 239
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addName(Ljava/lang/String;)V

    #@a
    .line 240
    iget-object v4, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@c
    array-length v5, p2

    #@d
    mul-int/lit8 v5, v5, 0x2

    #@f
    int-to-long v5, v5

    #@10
    invoke-virtual {v4, v5, v6}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt32(J)V

    #@13
    .line 241
    move-object v0, p2

    #@14
    .local v0, arr$:[I
    array-length v3, v0

    #@15
    .local v3, len$:I
    const/4 v2, 0x0

    #@16
    .local v2, i$:I
    :goto_16
    if-ge v2, v3, :cond_22

    #@18
    aget v1, v0, v2

    #@1a
    .line 243
    .local v1, i:I
    iget-object v4, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@1c
    invoke-virtual {v4, v1}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeInt16(I)V

    #@1f
    .line 241
    add-int/lit8 v2, v2, 0x1

    #@21
    goto :goto_16

    #@22
    .line 245
    .end local v1           #i:I
    :cond_22
    return-void
.end method

.method public addArrayInt32(Ljava/lang/String;[I)V
    .registers 10
    .parameter "name"
    .parameter "array"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 260
    iget-object v4, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@2
    const/16 v5, 0x20

    #@4
    invoke-virtual {v4, v5}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt16(I)V

    #@7
    .line 261
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addName(Ljava/lang/String;)V

    #@a
    .line 262
    iget-object v4, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@c
    array-length v5, p2

    #@d
    mul-int/lit8 v5, v5, 0x4

    #@f
    int-to-long v5, v5

    #@10
    invoke-virtual {v4, v5, v6}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt32(J)V

    #@13
    .line 263
    move-object v0, p2

    #@14
    .local v0, arr$:[I
    array-length v3, v0

    #@15
    .local v3, len$:I
    const/4 v2, 0x0

    #@16
    .local v2, i$:I
    :goto_16
    if-ge v2, v3, :cond_23

    #@18
    aget v1, v0, v2

    #@1a
    .line 265
    .local v1, i:I
    iget-object v4, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@1c
    int-to-long v5, v1

    #@1d
    invoke-virtual {v4, v5, v6}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeInt32(J)V

    #@20
    .line 263
    add-int/lit8 v2, v2, 0x1

    #@22
    goto :goto_16

    #@23
    .line 267
    .end local v1           #i:I
    :cond_23
    return-void
.end method

.method public addArrayInt64(Ljava/lang/String;[J)V
    .registers 11
    .parameter "name"
    .parameter "array"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 282
    iget-object v5, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@2
    const/16 v6, 0x1e

    #@4
    invoke-virtual {v5, v6}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt16(I)V

    #@7
    .line 283
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addName(Ljava/lang/String;)V

    #@a
    .line 284
    iget-object v5, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@c
    array-length v6, p2

    #@d
    mul-int/lit8 v6, v6, 0x8

    #@f
    int-to-long v6, v6

    #@10
    invoke-virtual {v5, v6, v7}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt32(J)V

    #@13
    .line 285
    move-object v0, p2

    #@14
    .local v0, arr$:[J
    array-length v4, v0

    #@15
    .local v4, len$:I
    const/4 v3, 0x0

    #@16
    .local v3, i$:I
    :goto_16
    if-ge v3, v4, :cond_22

    #@18
    aget-wide v1, v0, v3

    #@1a
    .line 287
    .local v1, i:J
    iget-object v5, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@1c
    invoke-virtual {v5, v1, v2}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeInt64(J)V

    #@1f
    .line 285
    add-int/lit8 v3, v3, 0x1

    #@21
    goto :goto_16

    #@22
    .line 289
    .end local v1           #i:J
    :cond_22
    return-void
.end method

.method public addArrayInt8(Ljava/lang/String;[I)V
    .registers 10
    .parameter "name"
    .parameter "array"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 205
    iget-object v4, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@2
    const/16 v5, 0x24

    #@4
    invoke-virtual {v4, v5}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt16(I)V

    #@7
    .line 206
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addName(Ljava/lang/String;)V

    #@a
    .line 207
    iget-object v4, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@c
    array-length v5, p2

    #@d
    mul-int/lit8 v5, v5, 0x1

    #@f
    int-to-long v5, v5

    #@10
    invoke-virtual {v4, v5, v6}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt32(J)V

    #@13
    .line 208
    move-object v0, p2

    #@14
    .local v0, arr$:[I
    array-length v3, v0

    #@15
    .local v3, len$:I
    const/4 v2, 0x0

    #@16
    .local v2, i$:I
    :goto_16
    if-ge v2, v3, :cond_22

    #@18
    aget v1, v0, v2

    #@1a
    .line 210
    .local v1, i:I
    iget-object v4, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@1c
    invoke-virtual {v4, v1}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeInt8(I)V

    #@1f
    .line 208
    add-int/lit8 v2, v2, 0x1

    #@21
    goto :goto_16

    #@22
    .line 212
    .end local v1           #i:I
    :cond_22
    return-void
.end method

.method public addArrayUInt16(Ljava/lang/String;[I)V
    .registers 10
    .parameter "name"
    .parameter "array"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 249
    iget-object v4, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@2
    const/16 v5, 0x23

    #@4
    invoke-virtual {v4, v5}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt16(I)V

    #@7
    .line 250
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addName(Ljava/lang/String;)V

    #@a
    .line 251
    iget-object v4, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@c
    array-length v5, p2

    #@d
    mul-int/lit8 v5, v5, 0x2

    #@f
    int-to-long v5, v5

    #@10
    invoke-virtual {v4, v5, v6}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt32(J)V

    #@13
    .line 252
    move-object v0, p2

    #@14
    .local v0, arr$:[I
    array-length v3, v0

    #@15
    .local v3, len$:I
    const/4 v2, 0x0

    #@16
    .local v2, i$:I
    :goto_16
    if-ge v2, v3, :cond_22

    #@18
    aget v1, v0, v2

    #@1a
    .line 254
    .local v1, i:I
    iget-object v4, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@1c
    invoke-virtual {v4, v1}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt16(I)V

    #@1f
    .line 252
    add-int/lit8 v2, v2, 0x1

    #@21
    goto :goto_16

    #@22
    .line 256
    .end local v1           #i:I
    :cond_22
    return-void
.end method

.method public addArrayUInt32(Ljava/lang/String;[J)V
    .registers 11
    .parameter "name"
    .parameter "array"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 271
    iget-object v5, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@2
    const/16 v6, 0x21

    #@4
    invoke-virtual {v5, v6}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt16(I)V

    #@7
    .line 272
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addName(Ljava/lang/String;)V

    #@a
    .line 273
    iget-object v5, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@c
    array-length v6, p2

    #@d
    mul-int/lit8 v6, v6, 0x4

    #@f
    int-to-long v6, v6

    #@10
    invoke-virtual {v5, v6, v7}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt32(J)V

    #@13
    .line 274
    move-object v0, p2

    #@14
    .local v0, arr$:[J
    array-length v4, v0

    #@15
    .local v4, len$:I
    const/4 v3, 0x0

    #@16
    .local v3, i$:I
    :goto_16
    if-ge v3, v4, :cond_22

    #@18
    aget-wide v1, v0, v3

    #@1a
    .line 276
    .local v1, i:J
    iget-object v5, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@1c
    invoke-virtual {v5, v1, v2}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt32(J)V

    #@1f
    .line 274
    add-int/lit8 v3, v3, 0x1

    #@21
    goto :goto_16

    #@22
    .line 278
    .end local v1           #i:J
    :cond_22
    return-void
.end method

.method public addArrayUInt64(Ljava/lang/String;[J[Z)V
    .registers 9
    .parameter "name"
    .parameter "array"
    .parameter "most_significant_bit"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 293
    iget-object v1, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@2
    const/16 v2, 0x1f

    #@4
    invoke-virtual {v1, v2}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt16(I)V

    #@7
    .line 294
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addName(Ljava/lang/String;)V

    #@a
    .line 295
    iget-object v1, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@c
    array-length v2, p2

    #@d
    mul-int/lit8 v2, v2, 0x8

    #@f
    int-to-long v2, v2

    #@10
    invoke-virtual {v1, v2, v3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt32(J)V

    #@13
    .line 296
    const/4 v0, 0x0

    #@14
    .local v0, i:I
    :goto_14
    array-length v1, p2

    #@15
    if-ge v0, v1, :cond_23

    #@17
    .line 298
    iget-object v1, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@19
    aget-wide v2, p2, v0

    #@1b
    aget-boolean v4, p3, v0

    #@1d
    invoke-virtual {v1, v2, v3, v4}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt64(JZ)V

    #@20
    .line 296
    add-int/lit8 v0, v0, 0x1

    #@22
    goto :goto_14

    #@23
    .line 300
    :cond_23
    return-void
.end method

.method public addArrayUInt8(Ljava/lang/String;[I)V
    .registers 10
    .parameter "name"
    .parameter "array"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 216
    iget-object v4, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@2
    const/16 v5, 0x25

    #@4
    invoke-virtual {v4, v5}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt16(I)V

    #@7
    .line 217
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addName(Ljava/lang/String;)V

    #@a
    .line 218
    iget-object v4, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@c
    array-length v5, p2

    #@d
    mul-int/lit8 v5, v5, 0x1

    #@f
    int-to-long v5, v5

    #@10
    invoke-virtual {v4, v5, v6}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt32(J)V

    #@13
    .line 219
    move-object v0, p2

    #@14
    .local v0, arr$:[I
    array-length v3, v0

    #@15
    .local v3, len$:I
    const/4 v2, 0x0

    #@16
    .local v2, i$:I
    :goto_16
    if-ge v2, v3, :cond_22

    #@18
    aget v1, v0, v2

    #@1a
    .line 221
    .local v1, i:I
    iget-object v4, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@1c
    invoke-virtual {v4, v1}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt8(I)V

    #@1f
    .line 219
    add-int/lit8 v2, v2, 0x1

    #@21
    goto :goto_16

    #@22
    .line 223
    .end local v1           #i:I
    :cond_22
    return-void
.end method

.method public addBlob(Ljava/lang/String;[B)V
    .registers 6
    .parameter "name"
    .parameter "blob"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 197
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@2
    const/16 v1, 0x14

    #@4
    invoke-virtual {v0, v1}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt16(I)V

    #@7
    .line 198
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addName(Ljava/lang/String;)V

    #@a
    .line 199
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@c
    array-length v1, p2

    #@d
    int-to-long v1, v1

    #@e
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt32(J)V

    #@11
    .line 200
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@13
    invoke-virtual {v0, p2}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->write([B)V

    #@16
    .line 201
    return-void
.end method

.method public addBool(Ljava/lang/String;Z)V
    .registers 5
    .parameter "name"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 112
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@2
    const/16 v1, 0x12

    #@4
    invoke-virtual {v0, v1}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt16(I)V

    #@7
    .line 113
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addName(Ljava/lang/String;)V

    #@a
    .line 114
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@c
    invoke-virtual {v0, p2}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeBoolean(Z)V

    #@f
    .line 115
    return-void
.end method

.method public addCard(Ljava/lang/String;Lcom/qualcomm/lib/location/mq_client/OutPostcard;)V
    .registers 5
    .parameter "name"
    .parameter "card"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 326
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-virtual {v0, v1}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt16(I)V

    #@6
    .line 327
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addName(Ljava/lang/String;)V

    #@9
    .line 328
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@b
    invoke-virtual {p2}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->getEncodedBuffer()[B

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v0, v1}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->write([B)V

    #@12
    .line 329
    return-void
.end method

.method public addDouble(Ljava/lang/String;D)V
    .registers 6
    .parameter "name"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 183
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@2
    const/16 v1, 0x15

    #@4
    invoke-virtual {v0, v1}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt16(I)V

    #@7
    .line 184
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addName(Ljava/lang/String;)V

    #@a
    .line 185
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@c
    invoke-virtual {v0, p2, p3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeDouble(D)V

    #@f
    .line 186
    return-void
.end method

.method public addFloat(Ljava/lang/String;F)V
    .registers 5
    .parameter "name"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 190
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@2
    const/16 v1, 0x16

    #@4
    invoke-virtual {v0, v1}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt16(I)V

    #@7
    .line 191
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addName(Ljava/lang/String;)V

    #@a
    .line 192
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@c
    invoke-virtual {v0, p2}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeFloat(F)V

    #@f
    .line 193
    return-void
.end method

.method public addInt16(Ljava/lang/String;I)V
    .registers 5
    .parameter "name"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 140
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@2
    const/16 v1, 0xe

    #@4
    invoke-virtual {v0, v1}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt16(I)V

    #@7
    .line 141
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addName(Ljava/lang/String;)V

    #@a
    .line 142
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@c
    invoke-virtual {v0, p2}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeInt16(I)V

    #@f
    .line 143
    return-void
.end method

.method public addInt32(Ljava/lang/String;J)V
    .registers 6
    .parameter "name"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 154
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@2
    const/16 v1, 0xc

    #@4
    invoke-virtual {v0, v1}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt16(I)V

    #@7
    .line 155
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addName(Ljava/lang/String;)V

    #@a
    .line 156
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@c
    invoke-virtual {v0, p2, p3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeInt32(J)V

    #@f
    .line 157
    return-void
.end method

.method public addInt64(Ljava/lang/String;J)V
    .registers 6
    .parameter "name"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 168
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@2
    const/16 v1, 0xa

    #@4
    invoke-virtual {v0, v1}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt16(I)V

    #@7
    .line 169
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addName(Ljava/lang/String;)V

    #@a
    .line 170
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@c
    invoke-virtual {v0, p2, p3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeInt64(J)V

    #@f
    .line 171
    return-void
.end method

.method public addInt8(Ljava/lang/String;I)V
    .registers 5
    .parameter "name"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 126
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@2
    const/16 v1, 0x10

    #@4
    invoke-virtual {v0, v1}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt16(I)V

    #@7
    .line 127
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addName(Ljava/lang/String;)V

    #@a
    .line 128
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@c
    invoke-virtual {v0, p2}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeInt8(I)V

    #@f
    .line 129
    return-void
.end method

.method protected addName(Ljava/lang/String;)V
    .registers 4
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 106
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@5
    move-result v1

    #@6
    add-int/lit8 v1, v1, 0x1

    #@8
    invoke-virtual {v0, v1}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt16(I)V

    #@b
    .line 107
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@d
    invoke-virtual {v0, p1}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeCstr(Ljava/lang/String;)V

    #@10
    .line 108
    return-void
.end method

.method public addString(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "name"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 175
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@2
    const/16 v1, 0x13

    #@4
    invoke-virtual {v0, v1}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt16(I)V

    #@7
    .line 176
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addName(Ljava/lang/String;)V

    #@a
    .line 177
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@c
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    #@f
    move-result v1

    #@10
    add-int/lit8 v1, v1, 0x1

    #@12
    int-to-long v1, v1

    #@13
    invoke-virtual {v0, v1, v2}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt32(J)V

    #@16
    .line 178
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@18
    invoke-virtual {v0, p2}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeCstr(Ljava/lang/String;)V

    #@1b
    .line 179
    return-void
.end method

.method public addUInt16(Ljava/lang/String;I)V
    .registers 5
    .parameter "name"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 133
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@2
    const/16 v1, 0xf

    #@4
    invoke-virtual {v0, v1}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt16(I)V

    #@7
    .line 134
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addName(Ljava/lang/String;)V

    #@a
    .line 135
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@c
    invoke-virtual {v0, p2}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt16(I)V

    #@f
    .line 136
    return-void
.end method

.method public addUInt32(Ljava/lang/String;J)V
    .registers 6
    .parameter "name"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 147
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@2
    const/16 v1, 0xd

    #@4
    invoke-virtual {v0, v1}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt16(I)V

    #@7
    .line 148
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addName(Ljava/lang/String;)V

    #@a
    .line 149
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@c
    invoke-virtual {v0, p2, p3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt32(J)V

    #@f
    .line 150
    return-void
.end method

.method public addUInt64(Ljava/lang/String;JZ)V
    .registers 7
    .parameter "name"
    .parameter "value"
    .parameter "highest_bit_set"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 161
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@2
    const/16 v1, 0xb

    #@4
    invoke-virtual {v0, v1}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt16(I)V

    #@7
    .line 162
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addName(Ljava/lang/String;)V

    #@a
    .line 163
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@c
    invoke-virtual {v0, p2, p3, p4}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt64(JZ)V

    #@f
    .line 164
    return-void
.end method

.method public addUInt8(Ljava/lang/String;I)V
    .registers 5
    .parameter "name"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 119
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@2
    const/16 v1, 0x11

    #@4
    invoke-virtual {v0, v1}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt16(I)V

    #@7
    .line 120
    invoke-virtual {p0, p1}, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->addName(Ljava/lang/String;)V

    #@a
    .line 121
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@c
    invoke-virtual {v0, p2}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt8(I)V

    #@f
    .line 122
    return-void
.end method

.method public getEncodedBuffer()[B
    .registers 3

    #@0
    .prologue
    .line 97
    sget-object v0, Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;->CS_SEALED:Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;

    #@2
    iget-object v1, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->m_state:Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;

    #@4
    if-eq v0, v1, :cond_c

    #@6
    .line 99
    new-instance v0, Ljava/lang/IllegalStateException;

    #@8
    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    #@b
    throw v0

    #@c
    .line 101
    :cond_c
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->sealed_card:[B

    #@e
    return-object v0
.end method

.method public init()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 68
    sget-object v0, Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;->CS_PRE_INIT:Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;

    #@2
    iget-object v1, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->m_state:Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;

    #@4
    if-eq v0, v1, :cond_c

    #@6
    .line 70
    new-instance v0, Ljava/lang/IllegalStateException;

    #@8
    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    #@b
    throw v0

    #@c
    .line 72
    :cond_c
    sget-object v0, Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;->CS_ADD:Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;

    #@e
    iput-object v0, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->m_state:Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;

    #@10
    .line 73
    return-void
.end method

.method public seal()V
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 77
    sget-object v3, Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;->CS_ADD:Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;

    #@3
    iget-object v4, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->m_state:Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;

    #@5
    if-eq v3, v4, :cond_d

    #@7
    .line 79
    new-instance v3, Ljava/lang/IllegalStateException;

    #@9
    invoke-direct {v3}, Ljava/lang/IllegalStateException;-><init>()V

    #@c
    throw v3

    #@d
    .line 83
    :cond_d
    iget-object v3, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@f
    const/4 v4, 0x0

    #@10
    invoke-virtual {v3, v4}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->write(I)V

    #@13
    .line 84
    iget-object v3, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@15
    invoke-virtual {v3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->size()I

    #@18
    move-result v1

    #@19
    .line 85
    .local v1, card_length:I
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    #@1b
    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    #@1e
    .line 86
    .local v0, buf2:Ljava/io/ByteArrayOutputStream;
    new-instance v2, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@20
    invoke-direct {v2, v0}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@23
    .line 87
    .local v2, data2:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;
    int-to-long v3, v1

    #@24
    invoke-virtual {v2, v3, v4}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt32(J)V

    #@27
    .line 88
    iget-object v3, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->buf:Ljava/io/ByteArrayOutputStream;

    #@29
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v2, v3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->write([B)V

    #@30
    .line 89
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@33
    move-result-object v3

    #@34
    iput-object v3, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->sealed_card:[B

    #@36
    .line 90
    iput-object v5, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->buf:Ljava/io/ByteArrayOutputStream;

    #@38
    .line 91
    iput-object v5, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->data:Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;

    #@3a
    .line 92
    sget-object v3, Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;->CS_SEALED:Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;

    #@3c
    iput-object v3, p0, Lcom/qualcomm/lib/location/mq_client/OutPostcard;->m_state:Lcom/qualcomm/lib/location/mq_client/OutPostcard$CARD_STATE;

    #@3e
    .line 93
    return-void
.end method
