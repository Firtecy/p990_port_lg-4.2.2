.class public Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;
.super Ljava/io/FilterOutputStream;
.source "NativeOrderDataOutputStream.java"


# instance fields
.field protected bbDouble:Ljava/nio/ByteBuffer;

.field protected bbFloat:Ljava/nio/ByteBuffer;

.field protected native_is_big_endian:Z

.field protected written:I


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;)V
    .registers 5
    .parameter "arg0"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 43
    invoke-direct {p0, p1}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@4
    .line 36
    iput v2, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->written:I

    #@6
    .line 38
    const/4 v0, 0x4

    #@7
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->bbFloat:Ljava/nio/ByteBuffer;

    #@d
    .line 39
    const/16 v0, 0x8

    #@f
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    #@12
    move-result-object v0

    #@13
    iput-object v0, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->bbDouble:Ljava/nio/ByteBuffer;

    #@15
    .line 45
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    #@18
    move-result-object v0

    #@19
    sget-object v1, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@1e
    move-result v0

    #@1f
    if-eqz v0, :cond_37

    #@21
    .line 47
    const/4 v0, 0x1

    #@22
    iput-boolean v0, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->native_is_big_endian:Z

    #@24
    .line 54
    :goto_24
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->bbFloat:Ljava/nio/ByteBuffer;

    #@26
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    #@2d
    .line 55
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->bbDouble:Ljava/nio/ByteBuffer;

    #@2f
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    #@36
    .line 56
    return-void

    #@37
    .line 51
    :cond_37
    iput-boolean v2, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->native_is_big_endian:Z

    #@39
    goto :goto_24
.end method


# virtual methods
.method public size()I
    .registers 2

    #@0
    .prologue
    .line 60
    iget v0, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->written:I

    #@2
    return v0
.end method

.method public write(I)V
    .registers 3
    .parameter "b"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 65
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->out:Ljava/io/OutputStream;

    #@2
    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    #@5
    .line 66
    iget v0, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->written:I

    #@7
    add-int/lit8 v0, v0, 0x1

    #@9
    iput v0, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->written:I

    #@b
    .line 67
    return-void
.end method

.method public write([B)V
    .registers 4
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 71
    const/4 v0, 0x0

    #@1
    array-length v1, p1

    #@2
    invoke-virtual {p0, p1, v0, v1}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->write([BII)V

    #@5
    .line 72
    return-void
.end method

.method public write([BII)V
    .registers 5
    .parameter "data"
    .parameter "offset"
    .parameter "length"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 76
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->out:Ljava/io/OutputStream;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    #@5
    .line 77
    iget v0, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->written:I

    #@7
    add-int/2addr v0, p3

    #@8
    iput v0, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->written:I

    #@a
    .line 78
    return-void
.end method

.method public writeBoolean(Z)V
    .registers 3
    .parameter "val"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 82
    if-eqz p1, :cond_7

    #@2
    .line 84
    const/4 v0, 0x1

    #@3
    invoke-virtual {p0, v0}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->write(I)V

    #@6
    .line 90
    :goto_6
    return-void

    #@7
    .line 88
    :cond_7
    const/4 v0, 0x0

    #@8
    invoke-virtual {p0, v0}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->write(I)V

    #@b
    goto :goto_6
.end method

.method public writeCstr(Ljava/lang/String;)V
    .registers 4
    .parameter "str"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 274
    const-string v1, "UTF-8"

    #@2
    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    #@5
    move-result-object v0

    #@6
    .line 275
    .local v0, bytes:[B
    invoke-virtual {p0, v0}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->write([B)V

    #@9
    .line 276
    const/4 v1, 0x0

    #@a
    invoke-virtual {p0, v1}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->write(I)V

    #@d
    .line 277
    return-void
.end method

.method public writeDouble(D)V
    .registers 4
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 260
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->bbDouble:Ljava/nio/ByteBuffer;

    #@2
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    #@5
    .line 261
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->bbDouble:Ljava/nio/ByteBuffer;

    #@7
    invoke-virtual {v0, p1, p2}, Ljava/nio/ByteBuffer;->putDouble(D)Ljava/nio/ByteBuffer;

    #@a
    .line 262
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->bbDouble:Ljava/nio/ByteBuffer;

    #@c
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    #@f
    move-result-object v0

    #@10
    invoke-virtual {p0, v0}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->write([B)V

    #@13
    .line 263
    return-void
.end method

.method public writeFloat(F)V
    .registers 3
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 267
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->bbFloat:Ljava/nio/ByteBuffer;

    #@2
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    #@5
    .line 268
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->bbFloat:Ljava/nio/ByteBuffer;

    #@7
    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    #@a
    .line 269
    iget-object v0, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->bbFloat:Ljava/nio/ByteBuffer;

    #@c
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    #@f
    move-result-object v0

    #@10
    invoke-virtual {p0, v0}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->write([B)V

    #@13
    .line 270
    return-void
.end method

.method public writeInt16(I)V
    .registers 6
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 134
    const/16 v2, -0x8000

    #@2
    if-lt p1, v2, :cond_8

    #@4
    const/16 v2, 0x7fff

    #@6
    if-le p1, v2, :cond_10

    #@8
    .line 136
    :cond_8
    new-instance v2, Ljava/io/IOException;

    #@a
    const-string v3, "value is not in expected range"

    #@c
    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@f
    throw v2

    #@10
    .line 138
    :cond_10
    and-int/lit16 v2, p1, 0xff

    #@12
    int-to-byte v1, v2

    #@13
    .line 139
    .local v1, lo:B
    shr-int/lit8 v2, p1, 0x8

    #@15
    and-int/lit16 v2, v2, 0xff

    #@17
    int-to-byte v0, v2

    #@18
    .line 140
    .local v0, hi:B
    iget-boolean v2, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->native_is_big_endian:Z

    #@1a
    if-eqz v2, :cond_23

    #@1c
    .line 142
    invoke-virtual {p0, v0}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->write(I)V

    #@1f
    .line 143
    invoke-virtual {p0, v1}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->write(I)V

    #@22
    .line 150
    :goto_22
    return-void

    #@23
    .line 147
    :cond_23
    invoke-virtual {p0, v1}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->write(I)V

    #@26
    .line 148
    invoke-virtual {p0, v0}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->write(I)V

    #@29
    goto :goto_22
.end method

.method public writeInt32(J)V
    .registers 13
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v9, 0x2

    #@1
    const/4 v8, 0x1

    #@2
    const/4 v7, 0x0

    #@3
    const/16 v6, 0x8

    #@5
    const-wide/16 v4, 0xff

    #@7
    .line 185
    const-wide/32 v1, -0x80000000

    #@a
    cmp-long v1, p1, v1

    #@c
    if-ltz v1, :cond_15

    #@e
    const-wide/32 v1, 0x7fffffff

    #@11
    cmp-long v1, p1, v1

    #@13
    if-lez v1, :cond_1d

    #@15
    .line 187
    :cond_15
    new-instance v1, Ljava/io/IOException;

    #@17
    const-string v2, "value is not in expected range"

    #@19
    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@1c
    throw v1

    #@1d
    .line 189
    :cond_1d
    const/4 v1, 0x4

    #@1e
    new-array v0, v1, [B

    #@20
    .line 191
    .local v0, array:[B
    iget-boolean v1, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->native_is_big_endian:Z

    #@22
    if-eqz v1, :cond_44

    #@24
    .line 193
    const/4 v1, 0x3

    #@25
    and-long v2, p1, v4

    #@27
    long-to-int v2, v2

    #@28
    int-to-byte v2, v2

    #@29
    aput-byte v2, v0, v1

    #@2b
    .line 194
    shr-long/2addr p1, v6

    #@2c
    .line 195
    and-long v1, p1, v4

    #@2e
    long-to-int v1, v1

    #@2f
    int-to-byte v1, v1

    #@30
    aput-byte v1, v0, v9

    #@32
    .line 196
    shr-long/2addr p1, v6

    #@33
    .line 197
    and-long v1, p1, v4

    #@35
    long-to-int v1, v1

    #@36
    int-to-byte v1, v1

    #@37
    aput-byte v1, v0, v8

    #@39
    .line 198
    shr-long/2addr p1, v6

    #@3a
    .line 199
    and-long v1, p1, v4

    #@3c
    long-to-int v1, v1

    #@3d
    int-to-byte v1, v1

    #@3e
    aput-byte v1, v0, v7

    #@40
    .line 211
    :goto_40
    invoke-virtual {p0, v0}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->write([B)V

    #@43
    .line 212
    return-void

    #@44
    .line 203
    :cond_44
    and-long v1, p1, v4

    #@46
    long-to-int v1, v1

    #@47
    int-to-byte v1, v1

    #@48
    aput-byte v1, v0, v7

    #@4a
    .line 204
    shr-long/2addr p1, v6

    #@4b
    .line 205
    and-long v1, p1, v4

    #@4d
    long-to-int v1, v1

    #@4e
    int-to-byte v1, v1

    #@4f
    aput-byte v1, v0, v8

    #@51
    .line 206
    shr-long/2addr p1, v6

    #@52
    .line 207
    and-long v1, p1, v4

    #@54
    long-to-int v1, v1

    #@55
    int-to-byte v1, v1

    #@56
    aput-byte v1, v0, v9

    #@58
    .line 208
    shr-long/2addr p1, v6

    #@59
    .line 209
    const/4 v1, 0x3

    #@5a
    and-long v2, p1, v4

    #@5c
    long-to-int v2, v2

    #@5d
    int-to-byte v2, v2

    #@5e
    aput-byte v2, v0, v1

    #@60
    goto :goto_40
.end method

.method public writeInt64(J)V
    .registers 10
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const-wide v5, 0xffffffffL

    #@5
    .line 216
    and-long v2, p1, v5

    #@7
    .line 217
    .local v2, lo:J
    const/16 v4, 0x20

    #@9
    ushr-long/2addr p1, v4

    #@a
    .line 218
    and-long v0, p1, v5

    #@c
    .line 219
    .local v0, hi:J
    iget-boolean v4, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->native_is_big_endian:Z

    #@e
    if-eqz v4, :cond_17

    #@10
    .line 221
    invoke-virtual {p0, v0, v1}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt32(J)V

    #@13
    .line 222
    invoke-virtual {p0, v2, v3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt32(J)V

    #@16
    .line 229
    :goto_16
    return-void

    #@17
    .line 226
    :cond_17
    invoke-virtual {p0, v2, v3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt32(J)V

    #@1a
    .line 227
    invoke-virtual {p0, v0, v1}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt32(J)V

    #@1d
    goto :goto_16
.end method

.method public writeInt8(I)V
    .registers 5
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 104
    const/16 v1, -0x80

    #@2
    if-lt p1, v1, :cond_8

    #@4
    const/16 v1, 0x7f

    #@6
    if-le p1, v1, :cond_10

    #@8
    .line 106
    :cond_8
    new-instance v1, Ljava/io/IOException;

    #@a
    const-string v2, "value is not in expected range"

    #@c
    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@f
    throw v1

    #@10
    .line 108
    :cond_10
    and-int/lit16 v1, p1, 0xff

    #@12
    int-to-byte v0, v1

    #@13
    .line 109
    .local v0, lo:B
    invoke-virtual {p0, v0}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->write(I)V

    #@16
    .line 110
    return-void
.end method

.method public writeUInt16(I)V
    .registers 6
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 114
    if-ltz p1, :cond_7

    #@2
    const v2, 0xffff

    #@5
    if-le p1, v2, :cond_f

    #@7
    .line 116
    :cond_7
    new-instance v2, Ljava/io/IOException;

    #@9
    const-string v3, "value is not in expected range"

    #@b
    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@e
    throw v2

    #@f
    .line 118
    :cond_f
    and-int/lit16 v2, p1, 0xff

    #@11
    int-to-byte v1, v2

    #@12
    .line 119
    .local v1, lo:B
    shr-int/lit8 v2, p1, 0x8

    #@14
    and-int/lit16 v2, v2, 0xff

    #@16
    int-to-byte v0, v2

    #@17
    .line 120
    .local v0, hi:B
    iget-boolean v2, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->native_is_big_endian:Z

    #@19
    if-eqz v2, :cond_22

    #@1b
    .line 122
    invoke-virtual {p0, v0}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->write(I)V

    #@1e
    .line 123
    invoke-virtual {p0, v1}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->write(I)V

    #@21
    .line 130
    :goto_21
    return-void

    #@22
    .line 127
    :cond_22
    invoke-virtual {p0, v1}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->write(I)V

    #@25
    .line 128
    invoke-virtual {p0, v0}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->write(I)V

    #@28
    goto :goto_21
.end method

.method public writeUInt32(J)V
    .registers 13
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v9, 0x2

    #@1
    const/4 v8, 0x1

    #@2
    const/4 v7, 0x0

    #@3
    const/16 v6, 0x8

    #@5
    const-wide/16 v4, 0xff

    #@7
    .line 154
    const-wide/16 v1, 0x0

    #@9
    cmp-long v1, p1, v1

    #@b
    if-ltz v1, :cond_16

    #@d
    const-wide v1, 0xffffffffL

    #@12
    cmp-long v1, p1, v1

    #@14
    if-lez v1, :cond_1e

    #@16
    .line 156
    :cond_16
    new-instance v1, Ljava/io/IOException;

    #@18
    const-string v2, "value is not in expected range"

    #@1a
    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v1

    #@1e
    .line 158
    :cond_1e
    const/4 v1, 0x4

    #@1f
    new-array v0, v1, [B

    #@21
    .line 160
    .local v0, array:[B
    iget-boolean v1, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->native_is_big_endian:Z

    #@23
    if-eqz v1, :cond_45

    #@25
    .line 162
    const/4 v1, 0x3

    #@26
    and-long v2, p1, v4

    #@28
    long-to-int v2, v2

    #@29
    int-to-byte v2, v2

    #@2a
    aput-byte v2, v0, v1

    #@2c
    .line 163
    shr-long/2addr p1, v6

    #@2d
    .line 164
    and-long v1, p1, v4

    #@2f
    long-to-int v1, v1

    #@30
    int-to-byte v1, v1

    #@31
    aput-byte v1, v0, v9

    #@33
    .line 165
    shr-long/2addr p1, v6

    #@34
    .line 166
    and-long v1, p1, v4

    #@36
    long-to-int v1, v1

    #@37
    int-to-byte v1, v1

    #@38
    aput-byte v1, v0, v8

    #@3a
    .line 167
    shr-long/2addr p1, v6

    #@3b
    .line 168
    and-long v1, p1, v4

    #@3d
    long-to-int v1, v1

    #@3e
    int-to-byte v1, v1

    #@3f
    aput-byte v1, v0, v7

    #@41
    .line 180
    :goto_41
    invoke-virtual {p0, v0}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->write([B)V

    #@44
    .line 181
    return-void

    #@45
    .line 172
    :cond_45
    and-long v1, p1, v4

    #@47
    long-to-int v1, v1

    #@48
    int-to-byte v1, v1

    #@49
    aput-byte v1, v0, v7

    #@4b
    .line 173
    shr-long/2addr p1, v6

    #@4c
    .line 174
    and-long v1, p1, v4

    #@4e
    long-to-int v1, v1

    #@4f
    int-to-byte v1, v1

    #@50
    aput-byte v1, v0, v8

    #@52
    .line 175
    shr-long/2addr p1, v6

    #@53
    .line 176
    and-long v1, p1, v4

    #@55
    long-to-int v1, v1

    #@56
    int-to-byte v1, v1

    #@57
    aput-byte v1, v0, v9

    #@59
    .line 177
    shr-long/2addr p1, v6

    #@5a
    .line 178
    const/4 v1, 0x3

    #@5b
    and-long v2, p1, v4

    #@5d
    long-to-int v2, v2

    #@5e
    int-to-byte v2, v2

    #@5f
    aput-byte v2, v0, v1

    #@61
    goto :goto_41
.end method

.method public writeUInt64(JZ)V
    .registers 12
    .parameter "value"
    .parameter "highest_bit_set"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const-wide v6, 0xffffffffL

    #@5
    .line 235
    const-wide/16 v4, 0x0

    #@7
    cmp-long v4, p1, v4

    #@9
    if-gez v4, :cond_13

    #@b
    .line 237
    new-instance v4, Ljava/io/IOException;

    #@d
    const-string v5, "value is not in expected range"

    #@f
    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@12
    throw v4

    #@13
    .line 239
    :cond_13
    and-long v2, p1, v6

    #@15
    .line 240
    .local v2, lo:J
    const/16 v4, 0x20

    #@17
    ushr-long/2addr p1, v4

    #@18
    .line 241
    and-long v0, p1, v6

    #@1a
    .line 242
    .local v0, hi:J
    if-eqz p3, :cond_20

    #@1c
    .line 244
    const-wide/32 v4, -0x80000000

    #@1f
    or-long/2addr v0, v4

    #@20
    .line 246
    :cond_20
    iget-boolean v4, p0, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->native_is_big_endian:Z

    #@22
    if-eqz v4, :cond_2b

    #@24
    .line 248
    invoke-virtual {p0, v0, v1}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt32(J)V

    #@27
    .line 249
    invoke-virtual {p0, v2, v3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt32(J)V

    #@2a
    .line 256
    :goto_2a
    return-void

    #@2b
    .line 253
    :cond_2b
    invoke-virtual {p0, v2, v3}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt32(J)V

    #@2e
    .line 254
    invoke-virtual {p0, v0, v1}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->writeUInt32(J)V

    #@31
    goto :goto_2a
.end method

.method public writeUInt8(I)V
    .registers 5
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 94
    if-ltz p1, :cond_6

    #@2
    const/16 v1, 0xff

    #@4
    if-le p1, v1, :cond_e

    #@6
    .line 96
    :cond_6
    new-instance v1, Ljava/io/IOException;

    #@8
    const-string v2, "value is not in expected range"

    #@a
    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@d
    throw v1

    #@e
    .line 98
    :cond_e
    and-int/lit16 v1, p1, 0xff

    #@10
    int-to-byte v0, v1

    #@11
    .line 99
    .local v0, lo:B
    invoke-virtual {p0, v0}, Lcom/qualcomm/lib/location/mq_client/NativeOrderDataOutputStream;->write(I)V

    #@14
    .line 100
    return-void
.end method
