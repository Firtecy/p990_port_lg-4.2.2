.class final enum Lcom/qualcomm/wfd/WfdMode;
.super Ljava/lang/Enum;
.source "ExtendedRemoteDisplay.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/wfd/WfdMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/wfd/WfdMode;

.field public static final enum INVALID:Lcom/qualcomm/wfd/WfdMode;

.field public static final enum PAUSED:Lcom/qualcomm/wfd/WfdMode;

.field public static final enum PLAYING:Lcom/qualcomm/wfd/WfdMode;

.field public static final enum STANDING_BY:Lcom/qualcomm/wfd/WfdMode;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    #@0
    .prologue
    const/4 v5, 0x3

    #@1
    const/4 v4, 0x2

    #@2
    const/4 v3, 0x1

    #@3
    const/4 v2, 0x0

    #@4
    .line 68
    new-instance v0, Lcom/qualcomm/wfd/WfdMode;

    #@6
    const-string v1, "PLAYING"

    #@8
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdMode;-><init>(Ljava/lang/String;I)V

    #@b
    sput-object v0, Lcom/qualcomm/wfd/WfdMode;->PLAYING:Lcom/qualcomm/wfd/WfdMode;

    #@d
    .line 69
    new-instance v0, Lcom/qualcomm/wfd/WfdMode;

    #@f
    const-string v1, "PAUSED"

    #@11
    invoke-direct {v0, v1, v3}, Lcom/qualcomm/wfd/WfdMode;-><init>(Ljava/lang/String;I)V

    #@14
    sput-object v0, Lcom/qualcomm/wfd/WfdMode;->PAUSED:Lcom/qualcomm/wfd/WfdMode;

    #@16
    .line 70
    new-instance v0, Lcom/qualcomm/wfd/WfdMode;

    #@18
    const-string v1, "STANDING_BY"

    #@1a
    invoke-direct {v0, v1, v4}, Lcom/qualcomm/wfd/WfdMode;-><init>(Ljava/lang/String;I)V

    #@1d
    sput-object v0, Lcom/qualcomm/wfd/WfdMode;->STANDING_BY:Lcom/qualcomm/wfd/WfdMode;

    #@1f
    .line 71
    new-instance v0, Lcom/qualcomm/wfd/WfdMode;

    #@21
    const-string v1, "INVALID"

    #@23
    invoke-direct {v0, v1, v5}, Lcom/qualcomm/wfd/WfdMode;-><init>(Ljava/lang/String;I)V

    #@26
    sput-object v0, Lcom/qualcomm/wfd/WfdMode;->INVALID:Lcom/qualcomm/wfd/WfdMode;

    #@28
    .line 67
    const/4 v0, 0x4

    #@29
    new-array v0, v0, [Lcom/qualcomm/wfd/WfdMode;

    #@2b
    sget-object v1, Lcom/qualcomm/wfd/WfdMode;->PLAYING:Lcom/qualcomm/wfd/WfdMode;

    #@2d
    aput-object v1, v0, v2

    #@2f
    sget-object v1, Lcom/qualcomm/wfd/WfdMode;->PAUSED:Lcom/qualcomm/wfd/WfdMode;

    #@31
    aput-object v1, v0, v3

    #@33
    sget-object v1, Lcom/qualcomm/wfd/WfdMode;->STANDING_BY:Lcom/qualcomm/wfd/WfdMode;

    #@35
    aput-object v1, v0, v4

    #@37
    sget-object v1, Lcom/qualcomm/wfd/WfdMode;->INVALID:Lcom/qualcomm/wfd/WfdMode;

    #@39
    aput-object v1, v0, v5

    #@3b
    sput-object v0, Lcom/qualcomm/wfd/WfdMode;->$VALUES:[Lcom/qualcomm/wfd/WfdMode;

    #@3d
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/wfd/WfdMode;
    .registers 2
    .parameter

    #@0
    .prologue
    .line 67
    const-class v0, Lcom/qualcomm/wfd/WfdMode;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/qualcomm/wfd/WfdMode;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/qualcomm/wfd/WfdMode;
    .registers 1

    #@0
    .prologue
    .line 67
    sget-object v0, Lcom/qualcomm/wfd/WfdMode;->$VALUES:[Lcom/qualcomm/wfd/WfdMode;

    #@2
    invoke-virtual {v0}, [Lcom/qualcomm/wfd/WfdMode;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/qualcomm/wfd/WfdMode;

    #@8
    return-object v0
.end method
