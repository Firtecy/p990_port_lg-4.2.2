.class Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;
.super Landroid/os/Handler;
.source "ExtendedRemoteDisplay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/ExtendedRemoteDisplay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ExtendedRemoteDisplayEventHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;


# direct methods
.method constructor <init>(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 515
    iput-object p1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 15
    .parameter "msg"

    #@0
    .prologue
    const/4 v12, 0x1

    #@1
    const/4 v11, 0x0

    #@2
    .line 519
    const-string v8, "ExtendedRemoteDisplay"

    #@4
    new-instance v9, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v10, "Event handler received: "

    #@b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v9

    #@f
    iget v10, p1, Landroid/os/Message;->what:I

    #@11
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v9

    #@15
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v9

    #@19
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 521
    iget v8, p1, Landroid/os/Message;->what:I

    #@1e
    packed-switch v8, :pswitch_data_28e

    #@21
    .line 676
    :pswitch_21
    const-string v8, "ExtendedRemoteDisplay"

    #@23
    new-instance v9, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v10, "Unknown event received: "

    #@2a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v9

    #@2e
    iget v10, p1, Landroid/os/Message;->what:I

    #@30
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@33
    move-result-object v9

    #@34
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v9

    #@38
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 678
    :cond_3b
    :goto_3b
    return-void

    #@3c
    .line 524
    :pswitch_3c
    iget-object v8, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@3e
    sget-object v9, Lcom/qualcomm/wfd/WFDState;->PLAY:Lcom/qualcomm/wfd/WFDState;

    #@40
    invoke-static {v8, v9}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$202(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Lcom/qualcomm/wfd/WFDState;)Lcom/qualcomm/wfd/WFDState;

    #@43
    .line 525
    const-string v8, "ExtendedRemoteDisplay"

    #@45
    const-string v9, "WFDService in PLAY state"

    #@47
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    goto :goto_3b

    #@4b
    .line 530
    :pswitch_4b
    const-string v8, "ExtendedRemoteDisplay"

    #@4d
    const-string v9, "WFDService in PAUSE state"

    #@4f
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    goto :goto_3b

    #@53
    .line 535
    :pswitch_53
    const-string v8, "ExtendedRemoteDisplay"

    #@55
    const-string v9, "WFDService in STANDBY state"

    #@57
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5a
    goto :goto_3b

    #@5b
    .line 540
    :pswitch_5b
    iget-object v8, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@5d
    new-instance v9, Lcom/qualcomm/wfd/WfdActionListenerImpl;

    #@5f
    iget-object v10, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@61
    invoke-static {v10}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$400(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;

    #@64
    move-result-object v10

    #@65
    invoke-direct {v9, v10}, Lcom/qualcomm/wfd/WfdActionListenerImpl;-><init>(Landroid/os/Handler;)V

    #@68
    invoke-static {v8, v9}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$302(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Lcom/qualcomm/wfd/service/IWfdActionListener;)Lcom/qualcomm/wfd/service/IWfdActionListener;

    #@6b
    .line 542
    iget-object v8, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@6d
    sget-object v9, Lcom/qualcomm/wfd/WFDState;->BOUND:Lcom/qualcomm/wfd/WFDState;

    #@6f
    invoke-static {v8, v9}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$202(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Lcom/qualcomm/wfd/WFDState;)Lcom/qualcomm/wfd/WFDState;

    #@72
    .line 545
    :try_start_72
    invoke-static {}, Lcom/qualcomm/wfd/ServiceUtil;->getInstance()Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@75
    move-result-object v8

    #@76
    const/4 v9, 0x0

    #@77
    invoke-interface {v8, v9}, Lcom/qualcomm/wfd/service/ISessionManagerService;->setDeviceType(I)I

    #@7a
    move-result v5

    #@7b
    .line 547
    .local v5, setDeviceTypeRet:I
    if-nez v5, :cond_ef

    #@7d
    .line 548
    const-string v8, "ExtendedRemoteDisplay"

    #@7f
    const-string v9, "mWfdService.setDeviceType called."

    #@81
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@84
    .line 556
    :goto_84
    invoke-static {}, Lcom/qualcomm/wfd/ServiceUtil;->getInstance()Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@87
    move-result-object v8

    #@88
    invoke-interface {v8}, Lcom/qualcomm/wfd/service/ISessionManagerService;->getStatus()Lcom/qualcomm/wfd/WfdStatus;

    #@8b
    move-result-object v6

    #@8c
    .line 559
    .local v6, wfdStatus:Lcom/qualcomm/wfd/WfdStatus;
    const-string v8, "ExtendedRemoteDisplay"

    #@8e
    new-instance v9, Ljava/lang/StringBuilder;

    #@90
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@93
    const-string v10, "wfdStatus.state= "

    #@95
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v9

    #@99
    iget v10, v6, Lcom/qualcomm/wfd/WfdStatus;->state:I

    #@9b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v9

    #@9f
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a2
    move-result-object v9

    #@a3
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a6
    .line 561
    iget v8, v6, Lcom/qualcomm/wfd/WfdStatus;->state:I

    #@a8
    sget-object v9, Lcom/qualcomm/wfd/WfdEnums$SessionState;->INVALID:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@aa
    invoke-virtual {v9}, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ordinal()I

    #@ad
    move-result v9

    #@ae
    if-eq v8, v9, :cond_ba

    #@b0
    iget v8, v6, Lcom/qualcomm/wfd/WfdStatus;->state:I

    #@b2
    sget-object v9, Lcom/qualcomm/wfd/WfdEnums$SessionState;->INITIALIZED:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@b4
    invoke-virtual {v9}, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ordinal()I

    #@b7
    move-result v9

    #@b8
    if-ne v8, v9, :cond_c1

    #@ba
    .line 565
    :cond_ba
    const-string v8, "ExtendedRemoteDisplay"

    #@bc
    const-string v9, "wfdStatus.state is INVALID or INITIALIZED"

    #@be
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c1
    .line 569
    :cond_c1
    iget-object v8, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@c3
    invoke-static {v8}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$500(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Lcom/qualcomm/wfd/WfdDevice;

    #@c6
    move-result-object v8

    #@c7
    if-eqz v8, :cond_3b

    #@c9
    .line 570
    invoke-static {}, Lcom/qualcomm/wfd/ServiceUtil;->getInstance()Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@cc
    move-result-object v8

    #@cd
    iget-object v9, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@cf
    invoke-static {v9}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$300(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Lcom/qualcomm/wfd/service/IWfdActionListener;

    #@d2
    move-result-object v9

    #@d3
    iget-object v10, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@d5
    invoke-static {v10}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$500(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Lcom/qualcomm/wfd/WfdDevice;

    #@d8
    move-result-object v10

    #@d9
    invoke-interface {v8, v9, v10}, Lcom/qualcomm/wfd/service/ISessionManagerService;->init(Lcom/qualcomm/wfd/service/IWfdActionListener;Lcom/qualcomm/wfd/WfdDevice;)I

    #@dc
    .line 573
    iget-object v8, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@de
    sget-object v9, Lcom/qualcomm/wfd/WFDState;->INITIALIZING:Lcom/qualcomm/wfd/WFDState;

    #@e0
    invoke-static {v8, v9}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$202(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Lcom/qualcomm/wfd/WFDState;)Lcom/qualcomm/wfd/WFDState;
    :try_end_e3
    .catch Landroid/os/RemoteException; {:try_start_72 .. :try_end_e3} :catch_e5

    #@e3
    goto/16 :goto_3b

    #@e5
    .line 577
    .end local v5           #setDeviceTypeRet:I
    .end local v6           #wfdStatus:Lcom/qualcomm/wfd/WfdStatus;
    :catch_e5
    move-exception v1

    #@e6
    .line 578
    .local v1, e:Landroid/os/RemoteException;
    const-string v8, "ExtendedRemoteDisplay"

    #@e8
    const-string v9, "WfdService init() failed"

    #@ea
    invoke-static {v8, v9, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@ed
    goto/16 :goto_3b

    #@ef
    .line 550
    .end local v1           #e:Landroid/os/RemoteException;
    .restart local v5       #setDeviceTypeRet:I
    :cond_ef
    :try_start_ef
    const-string v8, "ExtendedRemoteDisplay"

    #@f1
    new-instance v9, Ljava/lang/StringBuilder;

    #@f3
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@f6
    const-string v10, "mWfdService.setDeviceType failed error code: "

    #@f8
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fb
    move-result-object v9

    #@fc
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v9

    #@100
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@103
    move-result-object v9

    #@104
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_107
    .catch Landroid/os/RemoteException; {:try_start_ef .. :try_end_107} :catch_e5

    #@107
    goto/16 :goto_84

    #@109
    .line 585
    .end local v5           #setDeviceTypeRet:I
    :pswitch_109
    :try_start_109
    iget-object v8, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@10b
    sget-object v9, Lcom/qualcomm/wfd/WFDState;->TEARDOWN:Lcom/qualcomm/wfd/WFDState;

    #@10d
    invoke-static {v8, v9}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$202(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Lcom/qualcomm/wfd/WFDState;)Lcom/qualcomm/wfd/WFDState;

    #@110
    .line 586
    iget-object v8, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@112
    invoke-static {v8}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$600(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)V

    #@115
    .line 587
    invoke-static {}, Lcom/qualcomm/wfd/ServiceUtil;->getInstance()Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@118
    move-result-object v8

    #@119
    invoke-interface {v8}, Lcom/qualcomm/wfd/service/ISessionManagerService;->deinit()I

    #@11c
    .line 588
    iget-object v8, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@11e
    sget-object v9, Lcom/qualcomm/wfd/WFDState;->DEINIT:Lcom/qualcomm/wfd/WFDState;

    #@120
    invoke-static {v8, v9}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$202(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Lcom/qualcomm/wfd/WFDState;)Lcom/qualcomm/wfd/WFDState;

    #@123
    .line 589
    iget-object v8, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@125
    invoke-static {v8}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$700(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)I

    #@128
    move-result v8

    #@129
    if-eqz v8, :cond_14d

    #@12b
    .line 593
    iget-object v8, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@12d
    iget-object v9, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@12f
    invoke-static {v9}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$700(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)I

    #@132
    move-result v9

    #@133
    iget-object v10, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@135
    invoke-static {v10}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$800(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Landroid/view/Surface;

    #@138
    move-result-object v10

    #@139
    invoke-static {v8, v9, v10}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$900(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;ILandroid/view/Surface;)I

    #@13c
    .line 594
    iget-object v8, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@13e
    iget-object v9, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@140
    invoke-static {v9}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$700(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)I

    #@143
    move-result v9

    #@144
    invoke-static {v8, v9}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1000(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;I)V

    #@147
    .line 595
    iget-object v8, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@149
    const/4 v9, 0x0

    #@14a
    invoke-static {v8, v9}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$702(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;I)I

    #@14d
    .line 597
    :cond_14d
    const-string v8, "ExtendedRemoteDisplay"

    #@14f
    const-string v9, "Unbind the WFD service"

    #@151
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@154
    .line 598
    iget-object v8, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@156
    invoke-static {v8}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1100(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Landroid/content/Context;

    #@159
    move-result-object v8

    #@15a
    invoke-static {v8}, Lcom/qualcomm/wfd/ServiceUtil;->unbindService(Landroid/content/Context;)V

    #@15d
    .line 599
    const-string v8, "persist.sys.wfd.virtual"

    #@15f
    const-string v9, "0"

    #@161
    invoke-static {v8, v9}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_164
    .catch Landroid/os/RemoteException; {:try_start_109 .. :try_end_164} :catch_166

    #@164
    goto/16 :goto_3b

    #@166
    .line 600
    :catch_166
    move-exception v1

    #@167
    .line 601
    .restart local v1       #e:Landroid/os/RemoteException;
    const-string v8, "ExtendedRemoteDisplay"

    #@169
    const-string v9, "EventHandler:Remote exception when calling deinit()"

    #@16b
    invoke-static {v8, v9, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@16e
    goto/16 :goto_3b

    #@170
    .line 610
    .end local v1           #e:Landroid/os/RemoteException;
    :pswitch_170
    iget-object v8, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@172
    sget-object v9, Lcom/qualcomm/wfd/WFDState;->PLAYING:Lcom/qualcomm/wfd/WFDState;

    #@174
    invoke-static {v8, v9}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$202(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Lcom/qualcomm/wfd/WFDState;)Lcom/qualcomm/wfd/WFDState;

    #@177
    .line 611
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@17a
    move-result-object v0

    #@17b
    .line 612
    .local v0, b:Landroid/os/Bundle;
    const-string v8, "width"

    #@17d
    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@180
    move-result-object v8

    #@181
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@184
    move-result v7

    #@185
    .line 613
    .local v7, width:I
    const-string v8, "height"

    #@187
    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@18a
    move-result-object v8

    #@18b
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@18e
    move-result v2

    #@18f
    .line 620
    .local v2, height:I
    const-string v8, "hdcp"

    #@191
    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@194
    move-result-object v8

    #@195
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@198
    move-result v4

    #@199
    .line 622
    .local v4, secure:I
    const-string v8, "ExtendedRemoteDisplay"

    #@19b
    new-instance v9, Ljava/lang/StringBuilder;

    #@19d
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@1a0
    const-string v10, "MM Stream Started Height, width and secure:  "

    #@1a2
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a5
    move-result-object v9

    #@1a6
    const-string v10, "width"

    #@1a8
    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@1ab
    move-result-object v10

    #@1ac
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1af
    move-result-object v9

    #@1b0
    const-string v10, " "

    #@1b2
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b5
    move-result-object v9

    #@1b6
    const-string v10, "height"

    #@1b8
    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@1bb
    move-result-object v10

    #@1bc
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bf
    move-result-object v9

    #@1c0
    const-string v10, " "

    #@1c2
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c5
    move-result-object v9

    #@1c6
    const-string v10, "hdcp"

    #@1c8
    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@1cb
    move-result-object v10

    #@1cc
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cf
    move-result-object v9

    #@1d0
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d3
    move-result-object v9

    #@1d4
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d7
    .line 627
    iget-object v8, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@1d9
    invoke-static {v8}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$700(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)I

    #@1dc
    move-result v8

    #@1dd
    if-nez v8, :cond_22e

    #@1df
    .line 628
    const-string v8, "ExtendedRemoteDisplay"

    #@1e1
    const-string v9, "Create Native object"

    #@1e3
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e6
    .line 629
    iget-object v8, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@1e8
    iget-object v9, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@1ea
    invoke-static {v9}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1200(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)I

    #@1ed
    move-result v9

    #@1ee
    invoke-static {v8, v9}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$702(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;I)I

    #@1f1
    .line 630
    iget-object v8, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@1f3
    invoke-static {v8}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$700(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)I

    #@1f6
    move-result v8

    #@1f7
    if-nez v8, :cond_20f

    #@1f9
    .line 631
    const-string v8, "ExtendedRemoteDisplay"

    #@1fb
    const-string v9, "ExtendedRemoteDisplay Failed to get Native Object"

    #@1fd
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@200
    .line 637
    :goto_200
    if-eqz v4, :cond_221

    #@202
    .line 638
    iget-object v8, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@204
    iget-object v9, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@206
    invoke-static {v9}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$800(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Landroid/view/Surface;

    #@209
    move-result-object v9

    #@20a
    invoke-static {v8, v9, v7, v2, v12}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1400(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Landroid/view/Surface;III)V

    #@20d
    goto/16 :goto_3b

    #@20f
    .line 634
    :cond_20f
    iget-object v8, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@211
    iget-object v9, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@213
    iget-object v10, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@215
    invoke-static {v10}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$700(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)I

    #@218
    move-result v10

    #@219
    invoke-static {v9, v10, v7, v2}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1300(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;III)Landroid/view/Surface;

    #@21c
    move-result-object v9

    #@21d
    invoke-static {v8, v9}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$802(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Landroid/view/Surface;)Landroid/view/Surface;

    #@220
    goto :goto_200

    #@221
    .line 641
    :cond_221
    iget-object v8, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@223
    iget-object v9, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@225
    invoke-static {v9}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$800(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Landroid/view/Surface;

    #@228
    move-result-object v9

    #@229
    invoke-static {v8, v9, v7, v2, v11}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1400(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Landroid/view/Surface;III)V

    #@22c
    goto/16 :goto_3b

    #@22e
    .line 644
    :cond_22e
    const-string v8, "ExtendedRemoteDisplay"

    #@230
    const-string v9, "ExtendedRemoteDisplay Not honor stream start"

    #@232
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@235
    goto/16 :goto_3b

    #@237
    .line 652
    .end local v0           #b:Landroid/os/Bundle;
    .end local v2           #height:I
    .end local v4           #secure:I
    .end local v7           #width:I
    :pswitch_237
    :try_start_237
    iget-object v8, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@239
    sget-object v9, Lcom/qualcomm/wfd/WFDState;->ESTABLISHING:Lcom/qualcomm/wfd/WFDState;

    #@23b
    invoke-static {v8, v9}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$202(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Lcom/qualcomm/wfd/WFDState;)Lcom/qualcomm/wfd/WFDState;

    #@23e
    .line 653
    invoke-static {}, Lcom/qualcomm/wfd/ServiceUtil;->getInstance()Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@241
    move-result-object v8

    #@242
    iget-object v9, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@244
    invoke-static {v9}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1500(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Lcom/qualcomm/wfd/WfdDevice;

    #@247
    move-result-object v9

    #@248
    invoke-interface {v8, v9}, Lcom/qualcomm/wfd/service/ISessionManagerService;->startWfdSession(Lcom/qualcomm/wfd/WfdDevice;)I

    #@24b
    move-result v3

    #@24c
    .line 655
    .local v3, ret:I
    const-string v8, "ExtendedRemoteDisplay"

    #@24e
    new-instance v9, Ljava/lang/StringBuilder;

    #@250
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@253
    const-string v10, "EventHandler:startSession-initiated"

    #@255
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@258
    move-result-object v9

    #@259
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25c
    move-result-object v9

    #@25d
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@260
    move-result-object v9

    #@261
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_264
    .catch Landroid/os/RemoteException; {:try_start_237 .. :try_end_264} :catch_266

    #@264
    goto/16 :goto_3b

    #@266
    .line 657
    .end local v3           #ret:I
    :catch_266
    move-exception v1

    #@267
    .line 658
    .restart local v1       #e:Landroid/os/RemoteException;
    const-string v8, "ExtendedRemoteDisplay"

    #@269
    const-string v9, "EventHandler: startSession- failed"

    #@26b
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@26e
    .line 659
    iget-object v8, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@270
    invoke-static {v8, v12}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1600(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;I)V

    #@273
    goto/16 :goto_3b

    #@275
    .line 665
    .end local v1           #e:Landroid/os/RemoteException;
    :pswitch_275
    iget-object v8, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@277
    sget-object v9, Lcom/qualcomm/wfd/WFDState;->ESTABLISHED:Lcom/qualcomm/wfd/WFDState;

    #@279
    invoke-static {v8, v9}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$202(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Lcom/qualcomm/wfd/WFDState;)Lcom/qualcomm/wfd/WFDState;

    #@27c
    .line 666
    const-string v8, "ExtendedRemoteDisplay"

    #@27e
    const-string v9, "EventHandler: startSession- completed"

    #@280
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@283
    goto/16 :goto_3b

    #@285
    .line 671
    :pswitch_285
    const-string v8, "ExtendedRemoteDisplay"

    #@287
    const-string v9, "EventHandler: uibcActionCompleted- completed"

    #@289
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28c
    goto/16 :goto_3b

    #@28e
    .line 521
    :pswitch_data_28e
    .packed-switch 0x0
        :pswitch_3c
        :pswitch_4b
        :pswitch_53
        :pswitch_285
        :pswitch_109
        :pswitch_21
        :pswitch_21
        :pswitch_5b
        :pswitch_275
        :pswitch_237
        :pswitch_170
    .end packed-switch
.end method
