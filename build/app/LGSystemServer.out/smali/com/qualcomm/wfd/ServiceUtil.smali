.class Lcom/qualcomm/wfd/ServiceUtil;
.super Ljava/lang/Object;
.source "ExtendedRemoteDisplay.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/wfd/ServiceUtil$ServiceFailedToBindException;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ExtendedRemoteDisplay.ServiceUtil"

.field private static eventHandler:Landroid/os/Handler;

.field protected static mConnection:Landroid/content/ServiceConnection;

.field private static mServiceAlreadyBound:Z

.field private static uniqueInstance:Lcom/qualcomm/wfd/service/ISessionManagerService;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 111
    sput-object v1, Lcom/qualcomm/wfd/ServiceUtil;->uniqueInstance:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@3
    .line 112
    const/4 v0, 0x0

    #@4
    sput-boolean v0, Lcom/qualcomm/wfd/ServiceUtil;->mServiceAlreadyBound:Z

    #@6
    .line 113
    sput-object v1, Lcom/qualcomm/wfd/ServiceUtil;->eventHandler:Landroid/os/Handler;

    #@8
    .line 167
    new-instance v0, Lcom/qualcomm/wfd/ServiceUtil$1;

    #@a
    invoke-direct {v0}, Lcom/qualcomm/wfd/ServiceUtil$1;-><init>()V

    #@d
    sput-object v0, Lcom/qualcomm/wfd/ServiceUtil;->mConnection:Landroid/content/ServiceConnection;

    #@f
    return-void
.end method

.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 108
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 158
    return-void
.end method

.method static synthetic access$102(Z)Z
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 108
    sput-boolean p0, Lcom/qualcomm/wfd/ServiceUtil;->mServiceAlreadyBound:Z

    #@2
    return p0
.end method

.method static synthetic access$202(Lcom/qualcomm/wfd/service/ISessionManagerService;)Lcom/qualcomm/wfd/service/ISessionManagerService;
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 108
    sput-object p0, Lcom/qualcomm/wfd/ServiceUtil;->uniqueInstance:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@2
    return-object p0
.end method

.method static synthetic access$300()Landroid/os/Handler;
    .registers 1

    #@0
    .prologue
    .line 108
    sget-object v0, Lcom/qualcomm/wfd/ServiceUtil;->eventHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method public static bindService(Landroid/content/Context;Landroid/os/Handler;)V
    .registers 6
    .parameter "context"
    .parameter "inEventHandler"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/qualcomm/wfd/ServiceUtil$ServiceFailedToBindException;
        }
    .end annotation

    #@0
    .prologue
    .line 121
    sget-boolean v1, Lcom/qualcomm/wfd/ServiceUtil;->mServiceAlreadyBound:Z

    #@2
    if-eqz v1, :cond_8

    #@4
    sget-object v1, Lcom/qualcomm/wfd/ServiceUtil;->uniqueInstance:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@6
    if-nez v1, :cond_31

    #@8
    .line 122
    :cond_8
    const-string v1, "ExtendedRemoteDisplay.ServiceUtil"

    #@a
    const-string v2, "bindService- !AlreadyBound||uniqueInstance=null"

    #@c
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 125
    new-instance v0, Landroid/content/Intent;

    #@11
    const-string v1, "com.qualcomm.wfd.service.WfdService"

    #@13
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@16
    .line 127
    .local v0, serviceIntent:Landroid/content/Intent;
    sput-object p1, Lcom/qualcomm/wfd/ServiceUtil;->eventHandler:Landroid/os/Handler;

    #@18
    .line 128
    sget-object v1, Lcom/qualcomm/wfd/ServiceUtil;->mConnection:Landroid/content/ServiceConnection;

    #@1a
    const/4 v2, 0x1

    #@1b
    invoke-virtual {p0, v0, v1, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    #@1e
    move-result v1

    #@1f
    if-nez v1, :cond_31

    #@21
    .line 130
    const-string v1, "ExtendedRemoteDisplay.ServiceUtil"

    #@23
    const-string v2, "Failed to connect to Provider service"

    #@25
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 131
    new-instance v1, Lcom/qualcomm/wfd/ServiceUtil$ServiceFailedToBindException;

    #@2a
    const-string v2, "Failed to connect to Provider service"

    #@2c
    const/4 v3, 0x0

    #@2d
    invoke-direct {v1, v2, v3}, Lcom/qualcomm/wfd/ServiceUtil$ServiceFailedToBindException;-><init>(Ljava/lang/String;Lcom/qualcomm/wfd/ServiceUtil$1;)V

    #@30
    throw v1

    #@31
    .line 135
    .end local v0           #serviceIntent:Landroid/content/Intent;
    :cond_31
    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/qualcomm/wfd/service/ISessionManagerService;
    .registers 5

    #@0
    .prologue
    .line 147
    const-class v1, Lcom/qualcomm/wfd/ServiceUtil;

    #@2
    monitor-enter v1

    #@3
    :goto_3
    :try_start_3
    sget-object v0, Lcom/qualcomm/wfd/ServiceUtil;->uniqueInstance:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@5
    if-nez v0, :cond_31

    #@7
    .line 148
    const-string v0, "ExtendedRemoteDisplay.ServiceUtil"

    #@9
    const-string v2, "Waiting for service to bind ..."

    #@b
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_2e

    #@e
    .line 150
    :try_start_e
    const-class v0, Lcom/qualcomm/wfd/ServiceUtil;

    #@10
    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_13
    .catchall {:try_start_e .. :try_end_13} :catchall_2e
    .catch Ljava/lang/InterruptedException; {:try_start_e .. :try_end_13} :catch_14

    #@13
    goto :goto_3

    #@14
    .line 151
    :catch_14
    move-exception v0

    #@15
    .line 152
    :try_start_15
    const-string v2, "ExtendedRemoteDisplay.ServiceUtil"

    #@17
    new-instance v3, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v4, "InterruptedException: "

    #@1e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v0

    #@26
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v0

    #@2a
    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2d
    .catchall {:try_start_15 .. :try_end_2d} :catchall_2e

    #@2d
    goto :goto_3

    #@2e
    .line 147
    :catchall_2e
    move-exception v0

    #@2f
    monitor-exit v1

    #@30
    throw v0

    #@31
    .line 155
    :cond_31
    :try_start_31
    sget-object v0, Lcom/qualcomm/wfd/ServiceUtil;->uniqueInstance:Lcom/qualcomm/wfd/service/ISessionManagerService;
    :try_end_33
    .catchall {:try_start_31 .. :try_end_33} :catchall_2e

    #@33
    monitor-exit v1

    #@34
    return-object v0
.end method

.method protected static getmServiceAlreadyBound()Z
    .registers 1

    #@0
    .prologue
    .line 116
    sget-boolean v0, Lcom/qualcomm/wfd/ServiceUtil;->mServiceAlreadyBound:Z

    #@2
    return v0
.end method

.method public static unbindService(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 138
    const/4 v1, 0x0

    #@1
    sput-boolean v1, Lcom/qualcomm/wfd/ServiceUtil;->mServiceAlreadyBound:Z

    #@3
    .line 140
    :try_start_3
    sget-object v1, Lcom/qualcomm/wfd/ServiceUtil;->mConnection:Landroid/content/ServiceConnection;

    #@5
    invoke-virtual {p0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_8
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_8} :catch_9

    #@8
    .line 144
    :goto_8
    return-void

    #@9
    .line 141
    :catch_9
    move-exception v0

    #@a
    .line 142
    .local v0, e:Ljava/lang/IllegalArgumentException;
    const-string v1, "ExtendedRemoteDisplay.ServiceUtil"

    #@c
    new-instance v2, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v3, "IllegalArgumentException: "

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    goto :goto_8
.end method
