.class public final enum Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;
.super Ljava/lang/Enum;
.source "WfdEnums.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/WfdEnums;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CoupledSinkStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;

.field public static final enum COUPLED:Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;

.field public static final enum NOT_COUPLED:Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;

.field public static final enum TEARDOWN_COUPLING:Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;


# instance fields
.field private code:I


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 539
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;

    #@5
    const-string v1, "NOT_COUPLED"

    #@7
    invoke-direct {v0, v1, v2, v2}, Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;-><init>(Ljava/lang/String;II)V

    #@a
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;->NOT_COUPLED:Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;

    #@c
    .line 540
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;

    #@e
    const-string v1, "COUPLED"

    #@10
    invoke-direct {v0, v1, v3, v3}, Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;-><init>(Ljava/lang/String;II)V

    #@13
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;->COUPLED:Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;

    #@15
    .line 541
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;

    #@17
    const-string v1, "TEARDOWN_COUPLING"

    #@19
    invoke-direct {v0, v1, v4, v4}, Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;-><init>(Ljava/lang/String;II)V

    #@1c
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;->TEARDOWN_COUPLING:Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;

    #@1e
    .line 538
    const/4 v0, 0x3

    #@1f
    new-array v0, v0, [Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;

    #@21
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;->NOT_COUPLED:Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;

    #@23
    aput-object v1, v0, v2

    #@25
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;->COUPLED:Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;

    #@27
    aput-object v1, v0, v3

    #@29
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;->TEARDOWN_COUPLING:Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;

    #@2b
    aput-object v1, v0, v4

    #@2d
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;

    #@2f
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter "c"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 545
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    .line 546
    iput p3, p0, Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;->code:I

    #@5
    .line 547
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;
    .registers 2
    .parameter

    #@0
    .prologue
    .line 538
    const-class v0, Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;
    .registers 1

    #@0
    .prologue
    .line 538
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;

    #@2
    invoke-virtual {v0}, [Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;

    #@8
    return-object v0
.end method


# virtual methods
.method public getCode()I
    .registers 2

    #@0
    .prologue
    .line 550
    iget v0, p0, Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;->code:I

    #@2
    return v0
.end method
