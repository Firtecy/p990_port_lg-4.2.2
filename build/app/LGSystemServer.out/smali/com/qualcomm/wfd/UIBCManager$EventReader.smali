.class Lcom/qualcomm/wfd/UIBCManager$EventReader;
.super Lcom/qualcomm/wfd/UIBCManager$EventQueue;
.source "UIBCManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/UIBCManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "EventReader"
.end annotation


# instance fields
.field public running:Z

.field final synthetic this$0:Lcom/qualcomm/wfd/UIBCManager;


# direct methods
.method constructor <init>(Lcom/qualcomm/wfd/UIBCManager;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 158
    iput-object p1, p0, Lcom/qualcomm/wfd/UIBCManager$EventReader;->this$0:Lcom/qualcomm/wfd/UIBCManager;

    #@2
    const/4 v0, 0x0

    #@3
    invoke-direct {p0, p1, v0}, Lcom/qualcomm/wfd/UIBCManager$EventQueue;-><init>(Lcom/qualcomm/wfd/UIBCManager;Lcom/qualcomm/wfd/UIBCManager$1;)V

    #@6
    .line 159
    const/4 v0, 0x1

    #@7
    iput-boolean v0, p0, Lcom/qualcomm/wfd/UIBCManager$EventReader;->running:Z

    #@9
    return-void
.end method


# virtual methods
.method public run()V
    .registers 4

    #@0
    .prologue
    .line 162
    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/qualcomm/wfd/UIBCManager$EventReader;->running:Z

    #@2
    if-eqz v1, :cond_26

    #@4
    .line 163
    invoke-virtual {p0}, Lcom/qualcomm/wfd/UIBCManager$EventReader;->getNextEvent()Landroid/view/InputEvent;

    #@7
    move-result-object v0

    #@8
    .line 164
    .local v0, ev:Landroid/view/InputEvent;
    if-eqz v0, :cond_0

    #@a
    .line 165
    instance-of v1, v0, Landroid/view/MotionEvent;

    #@c
    if-eqz v1, :cond_14

    #@e
    .line 167
    check-cast v0, Landroid/view/MotionEvent;

    #@10
    .end local v0           #ev:Landroid/view/InputEvent;
    invoke-static {v0}, Lcom/qualcomm/wfd/WFDNative;->sendUIBCMotionEvent(Landroid/view/MotionEvent;)V

    #@13
    goto :goto_0

    #@14
    .line 168
    .restart local v0       #ev:Landroid/view/InputEvent;
    :cond_14
    instance-of v1, v0, Landroid/view/KeyEvent;

    #@16
    if-eqz v1, :cond_1e

    #@18
    .line 169
    check-cast v0, Landroid/view/KeyEvent;

    #@1a
    .end local v0           #ev:Landroid/view/InputEvent;
    invoke-static {v0}, Lcom/qualcomm/wfd/WFDNative;->sendUIBCKeyEvent(Landroid/view/KeyEvent;)V

    #@1d
    goto :goto_0

    #@1e
    .line 171
    .restart local v0       #ev:Landroid/view/InputEvent;
    :cond_1e
    const-string v1, "UIBCManager"

    #@20
    const-string v2, "***** Unknown event received"

    #@22
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    goto :goto_0

    #@26
    .line 178
    .end local v0           #ev:Landroid/view/InputEvent;
    :cond_26
    return-void
.end method
