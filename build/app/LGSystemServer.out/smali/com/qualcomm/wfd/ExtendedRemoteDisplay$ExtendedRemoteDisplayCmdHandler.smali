.class Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;
.super Landroid/os/Handler;
.source "ExtendedRemoteDisplay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/ExtendedRemoteDisplay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ExtendedRemoteDisplayCmdHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;


# direct methods
.method public constructor <init>(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Landroid/os/Looper;)V
    .registers 3
    .parameter
    .parameter "looper"

    #@0
    .prologue
    .line 687
    iput-object p1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@2
    .line 688
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 689
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 6
    .parameter "msg"

    #@0
    .prologue
    .line 693
    const-string v1, "ExtendedRemoteDisplay"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "Cmd handler received: "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    iget v3, p1, Landroid/os/Message;->what:I

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 695
    iget v1, p1, Landroid/os/Message;->what:I

    #@1c
    packed-switch v1, :pswitch_data_12a

    #@1f
    .line 751
    const-string v1, "ExtendedRemoteDisplay"

    #@21
    new-instance v2, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v3, "Unknown cmd received: "

    #@28
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v2

    #@2c
    iget v3, p1, Landroid/os/Message;->what:I

    #@2e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@31
    move-result-object v2

    #@32
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v2

    #@36
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    .line 753
    :goto_39
    return-void

    #@3a
    .line 698
    :pswitch_3a
    const-string v1, "ExtendedRemoteDisplay"

    #@3c
    const-string v2, "Started......"

    #@3e
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    .line 700
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@43
    iget-object v2, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@45
    invoke-static {v2}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1700(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Ljava/lang/String;

    #@48
    move-result-object v2

    #@49
    invoke-static {v1, v2}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1800(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Ljava/lang/String;)V

    #@4c
    .line 701
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@4e
    iget-object v2, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@50
    invoke-static {v2}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1700(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Ljava/lang/String;

    #@53
    move-result-object v2

    #@54
    invoke-static {v1, v2}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1900(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Ljava/lang/String;)V

    #@57
    .line 703
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@59
    invoke-static {v1}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$500(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Lcom/qualcomm/wfd/WfdDevice;

    #@5c
    move-result-object v1

    #@5d
    if-eqz v1, :cond_67

    #@5f
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@61
    invoke-static {v1}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1500(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Lcom/qualcomm/wfd/WfdDevice;

    #@64
    move-result-object v1

    #@65
    if-nez v1, :cond_8c

    #@67
    .line 704
    :cond_67
    new-instance v1, Ljava/lang/IllegalStateException;

    #@69
    new-instance v2, Ljava/lang/StringBuilder;

    #@6b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@6e
    const-string v3, "Could not start listening for remote display connection on \""

    #@70
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v2

    #@74
    iget-object v3, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@76
    invoke-static {v3}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1700(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Ljava/lang/String;

    #@79
    move-result-object v3

    #@7a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v2

    #@7e
    const-string v3, "\""

    #@80
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v2

    #@84
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@87
    move-result-object v2

    #@88
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@8b
    throw v1

    #@8c
    .line 710
    :cond_8c
    :try_start_8c
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@8e
    sget-object v2, Lcom/qualcomm/wfd/WFDState;->BINDING:Lcom/qualcomm/wfd/WFDState;

    #@90
    invoke-static {v1, v2}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$202(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Lcom/qualcomm/wfd/WFDState;)Lcom/qualcomm/wfd/WFDState;

    #@93
    .line 711
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@95
    invoke-static {v1}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1100(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Landroid/content/Context;

    #@98
    move-result-object v1

    #@99
    iget-object v2, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@9b
    invoke-static {v2}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$400(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;

    #@9e
    move-result-object v2

    #@9f
    invoke-static {v1, v2}, Lcom/qualcomm/wfd/ServiceUtil;->bindService(Landroid/content/Context;Landroid/os/Handler;)V
    :try_end_a2
    .catch Lcom/qualcomm/wfd/ServiceUtil$ServiceFailedToBindException; {:try_start_8c .. :try_end_a2} :catch_a3

    #@a2
    goto :goto_39

    #@a3
    .line 712
    :catch_a3
    move-exception v0

    #@a4
    .line 713
    .local v0, e:Lcom/qualcomm/wfd/ServiceUtil$ServiceFailedToBindException;
    const-string v1, "ExtendedRemoteDisplay"

    #@a6
    const-string v2, "ServiceFailedToBindException received"

    #@a8
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ab
    goto :goto_39

    #@ac
    .line 721
    .end local v0           #e:Lcom/qualcomm/wfd/ServiceUtil$ServiceFailedToBindException;
    :pswitch_ac
    const-string v1, "ExtendedRemoteDisplay"

    #@ae
    const-string v2, "Ending........."

    #@b0
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b3
    .line 722
    const-string v1, "ExtendedRemoteDisplay"

    #@b5
    const-string v2, "Notify Display Disconnected"

    #@b7
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ba
    .line 723
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@bc
    invoke-static {v1}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$600(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)V

    #@bf
    .line 724
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@c1
    invoke-static {v1}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$200(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Lcom/qualcomm/wfd/WFDState;

    #@c4
    move-result-object v1

    #@c5
    sget-object v2, Lcom/qualcomm/wfd/WFDState;->DEINIT:Lcom/qualcomm/wfd/WFDState;

    #@c7
    if-ne v1, v2, :cond_d2

    #@c9
    .line 725
    const-string v1, "persist.sys.wfd.virtual"

    #@cb
    const-string v2, "0"

    #@cd
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@d0
    goto/16 :goto_39

    #@d2
    .line 729
    :cond_d2
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@d4
    invoke-static {v1}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$200(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Lcom/qualcomm/wfd/WFDState;

    #@d7
    move-result-object v1

    #@d8
    sget-object v2, Lcom/qualcomm/wfd/WFDState;->INITIALIZED:Lcom/qualcomm/wfd/WFDState;

    #@da
    if-eq v1, v2, :cond_e6

    #@dc
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@de
    invoke-static {v1}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$200(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Lcom/qualcomm/wfd/WFDState;

    #@e1
    move-result-object v1

    #@e2
    sget-object v2, Lcom/qualcomm/wfd/WFDState;->INITIALIZING:Lcom/qualcomm/wfd/WFDState;

    #@e4
    if-ne v1, v2, :cond_110

    #@e6
    .line 732
    :cond_e6
    :try_start_e6
    invoke-static {}, Lcom/qualcomm/wfd/ServiceUtil;->getInstance()Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@e9
    move-result-object v1

    #@ea
    invoke-interface {v1}, Lcom/qualcomm/wfd/service/ISessionManagerService;->deinit()I

    #@ed
    .line 733
    const-string v1, "ExtendedRemoteDisplay"

    #@ef
    const-string v2, "Unbind the WFD service"

    #@f1
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f4
    .line 734
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@f6
    invoke-static {v1}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1100(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Landroid/content/Context;

    #@f9
    move-result-object v1

    #@fa
    invoke-static {v1}, Lcom/qualcomm/wfd/ServiceUtil;->unbindService(Landroid/content/Context;)V

    #@fd
    .line 735
    const-string v1, "persist.sys.wfd.virtual"

    #@ff
    const-string v2, "0"

    #@101
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_104
    .catch Landroid/os/RemoteException; {:try_start_e6 .. :try_end_104} :catch_106

    #@104
    goto/16 :goto_39

    #@106
    .line 736
    :catch_106
    move-exception v0

    #@107
    .line 737
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "ExtendedRemoteDisplay"

    #@109
    const-string v2, "RemoteException in deInit"

    #@10b
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@10e
    goto/16 :goto_39

    #@110
    .line 741
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_110
    const-string v1, "ExtendedRemoteDisplay"

    #@112
    const-string v2, "Teardown WFD Session"

    #@114
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@117
    .line 743
    :try_start_117
    invoke-static {}, Lcom/qualcomm/wfd/ServiceUtil;->getInstance()Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@11a
    move-result-object v1

    #@11b
    invoke-interface {v1}, Lcom/qualcomm/wfd/service/ISessionManagerService;->teardown()I
    :try_end_11e
    .catch Landroid/os/RemoteException; {:try_start_117 .. :try_end_11e} :catch_120

    #@11e
    goto/16 :goto_39

    #@120
    .line 744
    :catch_120
    move-exception v0

    #@121
    .line 745
    .restart local v0       #e:Landroid/os/RemoteException;
    const-string v1, "ExtendedRemoteDisplay"

    #@123
    const-string v2, "RemoteException in teardown"

    #@125
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@128
    goto/16 :goto_39

    #@12a
    .line 695
    :pswitch_data_12a
    .packed-switch 0x0
        :pswitch_3a
        :pswitch_ac
    .end packed-switch
.end method
