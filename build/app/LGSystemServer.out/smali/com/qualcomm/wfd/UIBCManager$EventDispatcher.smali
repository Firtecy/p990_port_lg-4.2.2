.class Lcom/qualcomm/wfd/UIBCManager$EventDispatcher;
.super Lcom/qualcomm/wfd/UIBCManager$EventQueue;
.source "UIBCManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/UIBCManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "EventDispatcher"
.end annotation


# instance fields
.field public running:Z

.field final synthetic this$0:Lcom/qualcomm/wfd/UIBCManager;


# direct methods
.method constructor <init>(Lcom/qualcomm/wfd/UIBCManager;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 188
    iput-object p1, p0, Lcom/qualcomm/wfd/UIBCManager$EventDispatcher;->this$0:Lcom/qualcomm/wfd/UIBCManager;

    #@2
    const/4 v0, 0x0

    #@3
    invoke-direct {p0, p1, v0}, Lcom/qualcomm/wfd/UIBCManager$EventQueue;-><init>(Lcom/qualcomm/wfd/UIBCManager;Lcom/qualcomm/wfd/UIBCManager$1;)V

    #@6
    .line 189
    const/4 v0, 0x1

    #@7
    iput-boolean v0, p0, Lcom/qualcomm/wfd/UIBCManager$EventDispatcher;->running:Z

    #@9
    return-void
.end method


# virtual methods
.method public run()V
    .registers 9

    #@0
    .prologue
    .line 192
    :cond_0
    :goto_0
    iget-boolean v5, p0, Lcom/qualcomm/wfd/UIBCManager$EventDispatcher;->running:Z

    #@2
    if-eqz v5, :cond_9e

    #@4
    .line 193
    invoke-virtual {p0}, Lcom/qualcomm/wfd/UIBCManager$EventDispatcher;->getNextEvent()Landroid/view/InputEvent;

    #@7
    move-result-object v1

    #@8
    .line 194
    .local v1, ev:Landroid/view/InputEvent;
    if-eqz v1, :cond_0

    #@a
    .line 195
    const/4 v2, 0x0

    #@b
    .line 196
    .local v2, injectSuccess:Z
    instance-of v5, v1, Landroid/view/MotionEvent;

    #@d
    if-eqz v5, :cond_66

    #@f
    move-object v4, v1

    #@10
    .line 198
    check-cast v4, Landroid/view/MotionEvent;

    #@12
    .line 200
    .local v4, me:Landroid/view/MotionEvent;
    :try_start_12
    invoke-virtual {v4}, Landroid/view/MotionEvent;->getSource()I

    #@15
    move-result v5

    #@16
    const/16 v6, 0x1002

    #@18
    if-ne v5, v6, :cond_40

    #@1a
    .line 201
    iget-object v5, p0, Lcom/qualcomm/wfd/UIBCManager$EventDispatcher;->this$0:Lcom/qualcomm/wfd/UIBCManager;

    #@1c
    invoke-static {v5}, Lcom/qualcomm/wfd/UIBCManager;->access$100(Lcom/qualcomm/wfd/UIBCManager;)Lcom/qualcomm/compat/VersionedInputManager;

    #@1f
    move-result-object v5

    #@20
    const/4 v6, 0x0

    #@21
    invoke-virtual {v5, v4, v6}, Lcom/qualcomm/compat/VersionedInputManager;->injectPointerEvent(Landroid/view/MotionEvent;Z)Z
    :try_end_24
    .catch Landroid/os/RemoteException; {:try_start_12 .. :try_end_24} :catch_5d

    #@24
    move-result v2

    #@25
    .line 223
    .end local v4           #me:Landroid/view/MotionEvent;
    :cond_25
    :goto_25
    if-nez v2, :cond_0

    #@27
    .line 224
    const-string v5, "UIBCManager"

    #@29
    new-instance v6, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v7, "Inject failed for event type with contents: "

    #@30
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v6

    #@34
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v6

    #@38
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v6

    #@3c
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    goto :goto_0

    #@40
    .line 202
    .restart local v4       #me:Landroid/view/MotionEvent;
    :cond_40
    :try_start_40
    invoke-virtual {v4}, Landroid/view/MotionEvent;->getSource()I

    #@43
    move-result v5

    #@44
    const v6, 0x10004

    #@47
    if-ne v5, v6, :cond_55

    #@49
    .line 203
    iget-object v5, p0, Lcom/qualcomm/wfd/UIBCManager$EventDispatcher;->this$0:Lcom/qualcomm/wfd/UIBCManager;

    #@4b
    invoke-static {v5}, Lcom/qualcomm/wfd/UIBCManager;->access$100(Lcom/qualcomm/wfd/UIBCManager;)Lcom/qualcomm/compat/VersionedInputManager;

    #@4e
    move-result-object v5

    #@4f
    const/4 v6, 0x0

    #@50
    invoke-virtual {v5, v4, v6}, Lcom/qualcomm/compat/VersionedInputManager;->injectTrackballEvent(Landroid/view/MotionEvent;Z)Z

    #@53
    move-result v2

    #@54
    goto :goto_25

    #@55
    .line 205
    :cond_55
    const-string v5, "UIBCManager"

    #@57
    const-string v6, "Unknown motion event"

    #@59
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5c
    .catch Landroid/os/RemoteException; {:try_start_40 .. :try_end_5c} :catch_5d

    #@5c
    goto :goto_25

    #@5d
    .line 207
    :catch_5d
    move-exception v0

    #@5e
    .line 208
    .local v0, e:Landroid/os/RemoteException;
    const-string v5, "UIBCManager"

    #@60
    const-string v6, "Exception happened when injecting event"

    #@62
    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@65
    goto :goto_25

    #@66
    .line 210
    .end local v0           #e:Landroid/os/RemoteException;
    .end local v4           #me:Landroid/view/MotionEvent;
    :cond_66
    instance-of v5, v1, Landroid/view/KeyEvent;

    #@68
    if-eqz v5, :cond_25

    #@6a
    move-object v3, v1

    #@6b
    .line 213
    check-cast v3, Landroid/view/KeyEvent;

    #@6d
    .line 214
    .local v3, ke:Landroid/view/KeyEvent;
    const-string v5, "UIBCManager"

    #@6f
    new-instance v6, Ljava/lang/StringBuilder;

    #@71
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@74
    const-string v7, "Injecting key event"

    #@76
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v6

    #@7a
    invoke-virtual {v3}, Landroid/view/KeyEvent;->getKeyCode()I

    #@7d
    move-result v7

    #@7e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@81
    move-result-object v6

    #@82
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v6

    #@86
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@89
    .line 216
    :try_start_89
    iget-object v5, p0, Lcom/qualcomm/wfd/UIBCManager$EventDispatcher;->this$0:Lcom/qualcomm/wfd/UIBCManager;

    #@8b
    invoke-static {v5}, Lcom/qualcomm/wfd/UIBCManager;->access$100(Lcom/qualcomm/wfd/UIBCManager;)Lcom/qualcomm/compat/VersionedInputManager;

    #@8e
    move-result-object v5

    #@8f
    const/4 v6, 0x0

    #@90
    invoke-virtual {v5, v3, v6}, Lcom/qualcomm/compat/VersionedInputManager;->injectKeyEvent(Landroid/view/KeyEvent;Z)Z
    :try_end_93
    .catch Landroid/os/RemoteException; {:try_start_89 .. :try_end_93} :catch_95

    #@93
    move-result v2

    #@94
    goto :goto_25

    #@95
    .line 218
    :catch_95
    move-exception v0

    #@96
    .line 220
    .restart local v0       #e:Landroid/os/RemoteException;
    const-string v5, "UIBCManager"

    #@98
    const-string v6, "Exception happened when injecting key event"

    #@9a
    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@9d
    goto :goto_25

    #@9e
    .line 228
    .end local v0           #e:Landroid/os/RemoteException;
    .end local v1           #ev:Landroid/view/InputEvent;
    .end local v2           #injectSuccess:Z
    .end local v3           #ke:Landroid/view/KeyEvent;
    :cond_9e
    return-void
.end method
