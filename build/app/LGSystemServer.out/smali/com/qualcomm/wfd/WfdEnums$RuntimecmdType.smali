.class public final enum Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;
.super Ljava/lang/Enum;
.source "WfdEnums.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/WfdEnums;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RuntimecmdType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;

.field public static final enum AUDIOPROXY_CLOSE:Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;

.field public static final enum AUDIOPROXY_OPEN:Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;

.field public static final enum UNKNOWN:Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;


# instance fields
.field private final code:I


# direct methods
.method static constructor <clinit>()V
    .registers 6

    #@0
    .prologue
    const/4 v5, 0x2

    #@1
    const/4 v4, 0x1

    #@2
    const/4 v3, 0x0

    #@3
    .line 524
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;

    #@5
    const-string v1, "UNKNOWN"

    #@7
    const/4 v2, -0x1

    #@8
    invoke-direct {v0, v1, v3, v2}, Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;-><init>(Ljava/lang/String;II)V

    #@b
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;->UNKNOWN:Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;

    #@d
    .line 525
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;

    #@f
    const-string v1, "AUDIOPROXY_OPEN"

    #@11
    invoke-direct {v0, v1, v4, v3}, Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;-><init>(Ljava/lang/String;II)V

    #@14
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;->AUDIOPROXY_OPEN:Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;

    #@16
    .line 526
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;

    #@18
    const-string v1, "AUDIOPROXY_CLOSE"

    #@1a
    invoke-direct {v0, v1, v5, v4}, Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;-><init>(Ljava/lang/String;II)V

    #@1d
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;->AUDIOPROXY_CLOSE:Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;

    #@1f
    .line 523
    const/4 v0, 0x3

    #@20
    new-array v0, v0, [Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;

    #@22
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;->UNKNOWN:Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;

    #@24
    aput-object v1, v0, v3

    #@26
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;->AUDIOPROXY_OPEN:Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;

    #@28
    aput-object v1, v0, v4

    #@2a
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;->AUDIOPROXY_CLOSE:Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;

    #@2c
    aput-object v1, v0, v5

    #@2e
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;

    #@30
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter "c"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 530
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    .line 531
    iput p3, p0, Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;->code:I

    #@5
    .line 532
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;
    .registers 2
    .parameter

    #@0
    .prologue
    .line 523
    const-class v0, Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;
    .registers 1

    #@0
    .prologue
    .line 523
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;

    #@2
    invoke-virtual {v0}, [Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;

    #@8
    return-object v0
.end method


# virtual methods
.method public getCmdType()I
    .registers 2

    #@0
    .prologue
    .line 534
    iget v0, p0, Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;->code:I

    #@2
    return v0
.end method
