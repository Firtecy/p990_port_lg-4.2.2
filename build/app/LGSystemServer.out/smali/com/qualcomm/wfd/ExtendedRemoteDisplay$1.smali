.class Lcom/qualcomm/wfd/ExtendedRemoteDisplay$1;
.super Ljava/lang/Object;
.source "ExtendedRemoteDisplay.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->notifyDisplayConnected(Landroid/view/Surface;III)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

.field final synthetic val$flags:I

.field final synthetic val$height:I

.field final synthetic val$surface:Landroid/view/Surface;

.field final synthetic val$width:I


# direct methods
.method constructor <init>(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Landroid/view/Surface;III)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 481
    iput-object p1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$1;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@2
    iput-object p2, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$1;->val$surface:Landroid/view/Surface;

    #@4
    iput p3, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$1;->val$width:I

    #@6
    iput p4, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$1;->val$height:I

    #@8
    iput p5, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$1;->val$flags:I

    #@a
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@d
    return-void
.end method


# virtual methods
.method public run()V
    .registers 6

    #@0
    .prologue
    .line 484
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$1;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-static {v0, v1}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$002(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Z)Z

    #@6
    .line 485
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$1;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@8
    invoke-static {v0}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$100(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Landroid/media/RemoteDisplay$Listener;

    #@b
    move-result-object v0

    #@c
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$1;->val$surface:Landroid/view/Surface;

    #@e
    iget v2, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$1;->val$width:I

    #@10
    iget v3, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$1;->val$height:I

    #@12
    iget v4, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$1;->val$flags:I

    #@14
    invoke-interface {v0, v1, v2, v3, v4}, Landroid/media/RemoteDisplay$Listener;->onDisplayConnected(Landroid/view/Surface;III)V

    #@17
    .line 486
    return-void
.end method
