.class public Lcom/qualcomm/wfd/WfdDevice;
.super Ljava/lang/Object;
.source "WfdDevice.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/wfd/WfdDevice;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public addressOfAP:Ljava/lang/String;

.field public capabilities:Landroid/os/Bundle;

.field public coupledSinkStatus:I

.field public deviceName:Ljava/lang/String;

.field public deviceType:I

.field public ipAddress:Ljava/lang/String;

.field public isAvailableForSession:Z

.field public macAddress:Ljava/lang/String;

.field public preferredConnectivity:I

.field public rtspPort:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 40
    new-instance v0, Lcom/qualcomm/wfd/WfdDevice$1;

    #@2
    invoke-direct {v0}, Lcom/qualcomm/wfd/WfdDevice$1;-><init>()V

    #@5
    sput-object v0, Lcom/qualcomm/wfd/WfdDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 50
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 51
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->UNKNOWN:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@5
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->getCode()I

    #@8
    move-result v0

    #@9
    iput v0, p0, Lcom/qualcomm/wfd/WfdDevice;->deviceType:I

    #@b
    .line 52
    const/4 v0, -0x1

    #@c
    iput v0, p0, Lcom/qualcomm/wfd/WfdDevice;->rtspPort:I

    #@e
    .line 53
    new-instance v0, Landroid/os/Bundle;

    #@10
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@13
    iput-object v0, p0, Lcom/qualcomm/wfd/WfdDevice;->capabilities:Landroid/os/Bundle;

    #@15
    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 2
    .parameter "in"

    #@0
    .prologue
    .line 56
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 57
    invoke-direct {p0, p1}, Lcom/qualcomm/wfd/WfdDevice;->readFromParcel(Landroid/os/Parcel;)V

    #@6
    .line 58
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 61
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    iput v0, p0, Lcom/qualcomm/wfd/WfdDevice;->deviceType:I

    #@6
    .line 62
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Lcom/qualcomm/wfd/WfdDevice;->macAddress:Ljava/lang/String;

    #@c
    .line 63
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@f
    move-result-object v0

    #@10
    iput-object v0, p0, Lcom/qualcomm/wfd/WfdDevice;->deviceName:Ljava/lang/String;

    #@12
    .line 64
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    iput-object v0, p0, Lcom/qualcomm/wfd/WfdDevice;->ipAddress:Ljava/lang/String;

    #@18
    .line 65
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v0

    #@1c
    iput v0, p0, Lcom/qualcomm/wfd/WfdDevice;->rtspPort:I

    #@1e
    .line 66
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    #@21
    move-result v0

    #@22
    if-eqz v0, :cond_40

    #@24
    const/4 v0, 0x1

    #@25
    :goto_25
    iput-boolean v0, p0, Lcom/qualcomm/wfd/WfdDevice;->isAvailableForSession:Z

    #@27
    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@2a
    move-result v0

    #@2b
    iput v0, p0, Lcom/qualcomm/wfd/WfdDevice;->preferredConnectivity:I

    #@2d
    .line 68
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@30
    move-result-object v0

    #@31
    iput-object v0, p0, Lcom/qualcomm/wfd/WfdDevice;->addressOfAP:Ljava/lang/String;

    #@33
    .line 69
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@36
    move-result v0

    #@37
    iput v0, p0, Lcom/qualcomm/wfd/WfdDevice;->coupledSinkStatus:I

    #@39
    .line 70
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    #@3c
    move-result-object v0

    #@3d
    iput-object v0, p0, Lcom/qualcomm/wfd/WfdDevice;->capabilities:Landroid/os/Bundle;

    #@3f
    .line 71
    return-void

    #@40
    .line 66
    :cond_40
    const/4 v0, 0x0

    #@41
    goto :goto_25
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 75
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 80
    iget v0, p0, Lcom/qualcomm/wfd/WfdDevice;->deviceType:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 81
    iget-object v0, p0, Lcom/qualcomm/wfd/WfdDevice;->macAddress:Ljava/lang/String;

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@a
    .line 82
    iget-object v0, p0, Lcom/qualcomm/wfd/WfdDevice;->deviceName:Ljava/lang/String;

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    .line 83
    iget-object v0, p0, Lcom/qualcomm/wfd/WfdDevice;->ipAddress:Ljava/lang/String;

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@14
    .line 84
    iget v0, p0, Lcom/qualcomm/wfd/WfdDevice;->rtspPort:I

    #@16
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 85
    iget-boolean v0, p0, Lcom/qualcomm/wfd/WfdDevice;->isAvailableForSession:Z

    #@1b
    if-eqz v0, :cond_37

    #@1d
    const/4 v0, 0x1

    #@1e
    :goto_1e
    int-to-byte v0, v0

    #@1f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    #@22
    .line 86
    iget v0, p0, Lcom/qualcomm/wfd/WfdDevice;->preferredConnectivity:I

    #@24
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@27
    .line 87
    iget-object v0, p0, Lcom/qualcomm/wfd/WfdDevice;->addressOfAP:Ljava/lang/String;

    #@29
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@2c
    .line 88
    iget v0, p0, Lcom/qualcomm/wfd/WfdDevice;->coupledSinkStatus:I

    #@2e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@31
    .line 89
    iget-object v0, p0, Lcom/qualcomm/wfd/WfdDevice;->capabilities:Landroid/os/Bundle;

    #@33
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    #@36
    .line 90
    return-void

    #@37
    .line 85
    :cond_37
    const/4 v0, 0x0

    #@38
    goto :goto_1e
.end method
