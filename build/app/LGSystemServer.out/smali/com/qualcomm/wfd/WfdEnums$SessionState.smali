.class public final enum Lcom/qualcomm/wfd/WfdEnums$SessionState;
.super Ljava/lang/Enum;
.source "WfdEnums.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/WfdEnums;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SessionState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/wfd/WfdEnums$SessionState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/wfd/WfdEnums$SessionState;

.field public static final enum ESTABLISHED:Lcom/qualcomm/wfd/WfdEnums$SessionState;

.field public static final enum IDLE:Lcom/qualcomm/wfd/WfdEnums$SessionState;

.field public static final enum INITIALIZED:Lcom/qualcomm/wfd/WfdEnums$SessionState;

.field public static final enum INVALID:Lcom/qualcomm/wfd/WfdEnums$SessionState;

.field public static final enum PAUSE:Lcom/qualcomm/wfd/WfdEnums$SessionState;

.field public static final enum PAUSING:Lcom/qualcomm/wfd/WfdEnums$SessionState;

.field public static final enum PLAY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

.field public static final enum PLAYING:Lcom/qualcomm/wfd/WfdEnums$SessionState;

.field public static final enum STANDBY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

.field public static final enum STANDING_BY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

.field public static final enum TEARDOWN:Lcom/qualcomm/wfd/WfdEnums$SessionState;

.field public static final enum TEARING_DOWN:Lcom/qualcomm/wfd/WfdEnums$SessionState;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 464
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@7
    const-string v1, "INVALID"

    #@9
    invoke-direct {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums$SessionState;-><init>(Ljava/lang/String;I)V

    #@c
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;->INVALID:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@e
    .line 465
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@10
    const-string v1, "INITIALIZED"

    #@12
    invoke-direct {v0, v1, v4}, Lcom/qualcomm/wfd/WfdEnums$SessionState;-><init>(Ljava/lang/String;I)V

    #@15
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;->INITIALIZED:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@17
    .line 466
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@19
    const-string v1, "IDLE"

    #@1b
    invoke-direct {v0, v1, v5}, Lcom/qualcomm/wfd/WfdEnums$SessionState;-><init>(Ljava/lang/String;I)V

    #@1e
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;->IDLE:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@20
    .line 467
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@22
    const-string v1, "PLAY"

    #@24
    invoke-direct {v0, v1, v6}, Lcom/qualcomm/wfd/WfdEnums$SessionState;-><init>(Ljava/lang/String;I)V

    #@27
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PLAY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@29
    .line 468
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@2b
    const-string v1, "PAUSE"

    #@2d
    invoke-direct {v0, v1, v7}, Lcom/qualcomm/wfd/WfdEnums$SessionState;-><init>(Ljava/lang/String;I)V

    #@30
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PAUSE:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@32
    .line 469
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@34
    const-string v1, "ESTABLISHED"

    #@36
    const/4 v2, 0x5

    #@37
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$SessionState;-><init>(Ljava/lang/String;I)V

    #@3a
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ESTABLISHED:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@3c
    .line 470
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@3e
    const-string v1, "TEARDOWN"

    #@40
    const/4 v2, 0x6

    #@41
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$SessionState;-><init>(Ljava/lang/String;I)V

    #@44
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;->TEARDOWN:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@46
    .line 471
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@48
    const-string v1, "PLAYING"

    #@4a
    const/4 v2, 0x7

    #@4b
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$SessionState;-><init>(Ljava/lang/String;I)V

    #@4e
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PLAYING:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@50
    .line 472
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@52
    const-string v1, "PAUSING"

    #@54
    const/16 v2, 0x8

    #@56
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$SessionState;-><init>(Ljava/lang/String;I)V

    #@59
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PAUSING:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@5b
    .line 473
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@5d
    const-string v1, "STANDBY"

    #@5f
    const/16 v2, 0x9

    #@61
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$SessionState;-><init>(Ljava/lang/String;I)V

    #@64
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;->STANDBY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@66
    .line 474
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@68
    const-string v1, "STANDING_BY"

    #@6a
    const/16 v2, 0xa

    #@6c
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$SessionState;-><init>(Ljava/lang/String;I)V

    #@6f
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;->STANDING_BY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@71
    .line 475
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@73
    const-string v1, "TEARING_DOWN"

    #@75
    const/16 v2, 0xb

    #@77
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$SessionState;-><init>(Ljava/lang/String;I)V

    #@7a
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;->TEARING_DOWN:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@7c
    .line 463
    const/16 v0, 0xc

    #@7e
    new-array v0, v0, [Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@80
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$SessionState;->INVALID:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@82
    aput-object v1, v0, v3

    #@84
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$SessionState;->INITIALIZED:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@86
    aput-object v1, v0, v4

    #@88
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$SessionState;->IDLE:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@8a
    aput-object v1, v0, v5

    #@8c
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PLAY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@8e
    aput-object v1, v0, v6

    #@90
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PAUSE:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@92
    aput-object v1, v0, v7

    #@94
    const/4 v1, 0x5

    #@95
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ESTABLISHED:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@97
    aput-object v2, v0, v1

    #@99
    const/4 v1, 0x6

    #@9a
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$SessionState;->TEARDOWN:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@9c
    aput-object v2, v0, v1

    #@9e
    const/4 v1, 0x7

    #@9f
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PLAYING:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@a1
    aput-object v2, v0, v1

    #@a3
    const/16 v1, 0x8

    #@a5
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PAUSING:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@a7
    aput-object v2, v0, v1

    #@a9
    const/16 v1, 0x9

    #@ab
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$SessionState;->STANDBY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@ad
    aput-object v2, v0, v1

    #@af
    const/16 v1, 0xa

    #@b1
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$SessionState;->STANDING_BY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@b3
    aput-object v2, v0, v1

    #@b5
    const/16 v1, 0xb

    #@b7
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$SessionState;->TEARING_DOWN:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@b9
    aput-object v2, v0, v1

    #@bb
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@bd
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 463
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/wfd/WfdEnums$SessionState;
    .registers 2
    .parameter

    #@0
    .prologue
    .line 463
    const-class v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/qualcomm/wfd/WfdEnums$SessionState;
    .registers 1

    #@0
    .prologue
    .line 463
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@2
    invoke-virtual {v0}, [Lcom/qualcomm/wfd/WfdEnums$SessionState;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@8
    return-object v0
.end method
