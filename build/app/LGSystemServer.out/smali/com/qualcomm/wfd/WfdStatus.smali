.class public Lcom/qualcomm/wfd/WfdStatus;
.super Ljava/lang/Object;
.source "WfdStatus.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/wfd/WfdStatus;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public connectedDevice:Lcom/qualcomm/wfd/WfdDevice;

.field public sessionId:I

.field public state:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 27
    new-instance v0, Lcom/qualcomm/wfd/WfdStatus$1;

    #@2
    invoke-direct {v0}, Lcom/qualcomm/wfd/WfdStatus$1;-><init>()V

    #@5
    sput-object v0, Lcom/qualcomm/wfd/WfdStatus;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 21
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 22
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;->INVALID:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@5
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ordinal()I

    #@8
    move-result v0

    #@9
    iput v0, p0, Lcom/qualcomm/wfd/WfdStatus;->state:I

    #@b
    .line 23
    const/4 v0, -0x1

    #@c
    iput v0, p0, Lcom/qualcomm/wfd/WfdStatus;->sessionId:I

    #@e
    .line 24
    const/4 v0, 0x0

    #@f
    iput-object v0, p0, Lcom/qualcomm/wfd/WfdStatus;->connectedDevice:Lcom/qualcomm/wfd/WfdDevice;

    #@11
    .line 25
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 46
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 51
    iget v0, p0, Lcom/qualcomm/wfd/WfdStatus;->state:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 52
    iget v0, p0, Lcom/qualcomm/wfd/WfdStatus;->sessionId:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 53
    iget-object v0, p0, Lcom/qualcomm/wfd/WfdStatus;->connectedDevice:Lcom/qualcomm/wfd/WfdDevice;

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    #@f
    .line 54
    return-void
.end method
