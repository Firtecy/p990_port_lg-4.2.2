.class Lcom/qualcomm/wfd/WfdActionListenerImpl;
.super Lcom/qualcomm/wfd/service/IWfdActionListener$Stub;
.source "ExtendedRemoteDisplay.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/wfd/WfdActionListenerImpl$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ExtendedRemoteDisplay.WfdActionListenerImpl"


# instance fields
.field mHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Landroid/os/Handler;)V
    .registers 2
    .parameter "handler"

    #@0
    .prologue
    .line 194
    invoke-direct {p0}, Lcom/qualcomm/wfd/service/IWfdActionListener$Stub;-><init>()V

    #@3
    .line 195
    iput-object p1, p0, Lcom/qualcomm/wfd/WfdActionListenerImpl;->mHandler:Landroid/os/Handler;

    #@5
    .line 196
    return-void
.end method


# virtual methods
.method public notify(Landroid/os/Bundle;I)V
    .registers 7
    .parameter "b"
    .parameter "sessionId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 271
    if-eqz p1, :cond_27

    #@2
    .line 272
    const-string v2, "ExtendedRemoteDisplay.WfdActionListenerImpl"

    #@4
    const-string v3, "Notify from WFDService"

    #@6
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 273
    const-string v2, "event"

    #@b
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    .line 274
    .local v0, event:Ljava/lang/String;
    const-string v2, "MMStreamStarted"

    #@11
    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@14
    move-result v2

    #@15
    if-eqz v2, :cond_27

    #@17
    .line 275
    iget-object v2, p0, Lcom/qualcomm/wfd/WfdActionListenerImpl;->mHandler:Landroid/os/Handler;

    #@19
    const/16 v3, 0xa

    #@1b
    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@1e
    move-result-object v1

    #@1f
    .line 277
    .local v1, messageEvent:Landroid/os/Message;
    invoke-virtual {v1, p1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@22
    .line 278
    iget-object v2, p0, Lcom/qualcomm/wfd/WfdActionListenerImpl;->mHandler:Landroid/os/Handler;

    #@24
    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@27
    .line 281
    .end local v0           #event:Ljava/lang/String;
    .end local v1           #messageEvent:Landroid/os/Message;
    :cond_27
    return-void
.end method

.method public notifyEvent(II)V
    .registers 3
    .parameter "event"
    .parameter "sessionId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 267
    return-void
.end method

.method public onStateUpdate(II)V
    .registers 13
    .parameter "newState"
    .parameter "sessionId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v9, 0x4

    #@1
    .line 201
    invoke-static {}, Lcom/qualcomm/wfd/WfdEnums$SessionState;->values()[Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@4
    move-result-object v7

    #@5
    aget-object v6, v7, p1

    #@7
    .line 202
    .local v6, state:Lcom/qualcomm/wfd/WfdEnums$SessionState;
    sget-object v7, Lcom/qualcomm/wfd/WfdActionListenerImpl$1;->$SwitchMap$com$qualcomm$wfd$WfdEnums$SessionState:[I

    #@9
    invoke-virtual {v6}, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ordinal()I

    #@c
    move-result v8

    #@d
    aget v7, v7, v8

    #@f
    packed-switch v7, :pswitch_data_ba

    #@12
    .line 263
    :goto_12
    return-void

    #@13
    .line 204
    :pswitch_13
    const-string v7, "ExtendedRemoteDisplay.WfdActionListenerImpl"

    #@15
    const-string v8, "WfdEnums.SessionState==INITIALIZED"

    #@17
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 205
    if-lez p2, :cond_2f

    #@1c
    .line 206
    const-string v7, "ExtendedRemoteDisplay.WfdActionListenerImpl"

    #@1e
    const-string v8, "WfdEnums.SessionState==INITIALIZED, sessionId > 0"

    #@20
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 208
    iget-object v7, p0, Lcom/qualcomm/wfd/WfdActionListenerImpl;->mHandler:Landroid/os/Handler;

    #@25
    invoke-virtual {v7, v9}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@28
    move-result-object v5

    #@29
    .line 210
    .local v5, messageTeardown:Landroid/os/Message;
    iget-object v7, p0, Lcom/qualcomm/wfd/WfdActionListenerImpl;->mHandler:Landroid/os/Handler;

    #@2b
    invoke-virtual {v7, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@2e
    goto :goto_12

    #@2f
    .line 212
    .end local v5           #messageTeardown:Landroid/os/Message;
    :cond_2f
    const-string v7, "ExtendedRemoteDisplay.WfdActionListenerImpl"

    #@31
    const-string v8, "WfdEnums.SessionState==INITIALIZED, Init callback"

    #@33
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 214
    iget-object v7, p0, Lcom/qualcomm/wfd/WfdActionListenerImpl;->mHandler:Landroid/os/Handler;

    #@38
    const/16 v8, 0x9

    #@3a
    invoke-virtual {v7, v8}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@3d
    move-result-object v1

    #@3e
    .line 216
    .local v1, messageInit:Landroid/os/Message;
    iget-object v7, p0, Lcom/qualcomm/wfd/WfdActionListenerImpl;->mHandler:Landroid/os/Handler;

    #@40
    invoke-virtual {v7, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@43
    goto :goto_12

    #@44
    .line 221
    .end local v1           #messageInit:Landroid/os/Message;
    :pswitch_44
    const-string v7, "ExtendedRemoteDisplay.WfdActionListenerImpl"

    #@46
    const-string v8, "WfdEnums.SessionState==INVALID"

    #@48
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    goto :goto_12

    #@4c
    .line 225
    :pswitch_4c
    const-string v7, "ExtendedRemoteDisplay.WfdActionListenerImpl"

    #@4e
    const-string v8, "WfdEnums.SessionState==IDLE"

    #@50
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    goto :goto_12

    #@54
    .line 229
    :pswitch_54
    const-string v7, "ExtendedRemoteDisplay.WfdActionListenerImpl"

    #@56
    const-string v8, "WfdEnums.SessionState==PLAY"

    #@58
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5b
    .line 230
    iget-object v7, p0, Lcom/qualcomm/wfd/WfdActionListenerImpl;->mHandler:Landroid/os/Handler;

    #@5d
    const/4 v8, 0x0

    #@5e
    invoke-virtual {v7, v8}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@61
    move-result-object v3

    #@62
    .line 232
    .local v3, messagePlay:Landroid/os/Message;
    iget-object v7, p0, Lcom/qualcomm/wfd/WfdActionListenerImpl;->mHandler:Landroid/os/Handler;

    #@64
    invoke-virtual {v7, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@67
    goto :goto_12

    #@68
    .line 236
    .end local v3           #messagePlay:Landroid/os/Message;
    :pswitch_68
    const-string v7, "ExtendedRemoteDisplay.WfdActionListenerImpl"

    #@6a
    const-string v8, "WfdEnums.SessionState==PAUSE"

    #@6c
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6f
    .line 237
    iget-object v7, p0, Lcom/qualcomm/wfd/WfdActionListenerImpl;->mHandler:Landroid/os/Handler;

    #@71
    const/4 v8, 0x1

    #@72
    invoke-virtual {v7, v8}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@75
    move-result-object v2

    #@76
    .line 239
    .local v2, messagePause:Landroid/os/Message;
    iget-object v7, p0, Lcom/qualcomm/wfd/WfdActionListenerImpl;->mHandler:Landroid/os/Handler;

    #@78
    invoke-virtual {v7, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@7b
    goto :goto_12

    #@7c
    .line 243
    .end local v2           #messagePause:Landroid/os/Message;
    :pswitch_7c
    const-string v7, "ExtendedRemoteDisplay.WfdActionListenerImpl"

    #@7e
    const-string v8, "WfdEnums.SessionState = STANDING_BY"

    #@80
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@83
    .line 244
    iget-object v7, p0, Lcom/qualcomm/wfd/WfdActionListenerImpl;->mHandler:Landroid/os/Handler;

    #@85
    const/4 v8, 0x2

    #@86
    invoke-virtual {v7, v8}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@89
    move-result-object v4

    #@8a
    .line 246
    .local v4, messageStandby:Landroid/os/Message;
    iget-object v7, p0, Lcom/qualcomm/wfd/WfdActionListenerImpl;->mHandler:Landroid/os/Handler;

    #@8c
    invoke-virtual {v7, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@8f
    goto :goto_12

    #@90
    .line 250
    .end local v4           #messageStandby:Landroid/os/Message;
    :pswitch_90
    const-string v7, "ExtendedRemoteDisplay.WfdActionListenerImpl"

    #@92
    const-string v8, "WfdEnums.SessionState==ESTABLISHED"

    #@94
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@97
    .line 251
    iget-object v7, p0, Lcom/qualcomm/wfd/WfdActionListenerImpl;->mHandler:Landroid/os/Handler;

    #@99
    const/16 v8, 0x8

    #@9b
    invoke-virtual {v7, v8}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@9e
    move-result-object v0

    #@9f
    .line 253
    .local v0, messageEstablishedCallback:Landroid/os/Message;
    iget-object v7, p0, Lcom/qualcomm/wfd/WfdActionListenerImpl;->mHandler:Landroid/os/Handler;

    #@a1
    invoke-virtual {v7, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@a4
    goto/16 :goto_12

    #@a6
    .line 257
    .end local v0           #messageEstablishedCallback:Landroid/os/Message;
    :pswitch_a6
    const-string v7, "ExtendedRemoteDisplay.WfdActionListenerImpl"

    #@a8
    const-string v8, "WfdEnums.SessionState==TEARDOWN"

    #@aa
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ad
    .line 258
    iget-object v7, p0, Lcom/qualcomm/wfd/WfdActionListenerImpl;->mHandler:Landroid/os/Handler;

    #@af
    invoke-virtual {v7, v9}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@b2
    move-result-object v5

    #@b3
    .line 260
    .restart local v5       #messageTeardown:Landroid/os/Message;
    iget-object v7, p0, Lcom/qualcomm/wfd/WfdActionListenerImpl;->mHandler:Landroid/os/Handler;

    #@b5
    invoke-virtual {v7, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@b8
    goto/16 :goto_12

    #@ba
    .line 202
    :pswitch_data_ba
    .packed-switch 0x1
        :pswitch_13
        :pswitch_44
        :pswitch_4c
        :pswitch_54
        :pswitch_68
        :pswitch_7c
        :pswitch_90
        :pswitch_a6
    .end packed-switch
.end method
