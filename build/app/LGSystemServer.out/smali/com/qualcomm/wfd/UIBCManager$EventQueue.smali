.class Lcom/qualcomm/wfd/UIBCManager$EventQueue;
.super Ljava/lang/Object;
.source "UIBCManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/UIBCManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EventQueue"
.end annotation


# instance fields
.field protected eventQueueStatus:Z

.field private queuedEvents:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Landroid/view/InputEvent;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/qualcomm/wfd/UIBCManager;

.field private final timeOut:I


# direct methods
.method private constructor <init>(Lcom/qualcomm/wfd/UIBCManager;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 236
    iput-object p1, p0, Lcom/qualcomm/wfd/UIBCManager$EventQueue;->this$0:Lcom/qualcomm/wfd/UIBCManager;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 238
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    #@7
    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    #@a
    iput-object v0, p0, Lcom/qualcomm/wfd/UIBCManager$EventQueue;->queuedEvents:Ljava/util/concurrent/BlockingQueue;

    #@c
    .line 239
    const/16 v0, 0x1f4

    #@e
    iput v0, p0, Lcom/qualcomm/wfd/UIBCManager$EventQueue;->timeOut:I

    #@10
    .line 240
    const/4 v0, 0x1

    #@11
    iput-boolean v0, p0, Lcom/qualcomm/wfd/UIBCManager$EventQueue;->eventQueueStatus:Z

    #@13
    return-void
.end method

.method synthetic constructor <init>(Lcom/qualcomm/wfd/UIBCManager;Lcom/qualcomm/wfd/UIBCManager$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 236
    invoke-direct {p0, p1}, Lcom/qualcomm/wfd/UIBCManager$EventQueue;-><init>(Lcom/qualcomm/wfd/UIBCManager;)V

    #@3
    return-void
.end method


# virtual methods
.method public addEvent(Landroid/view/InputEvent;)V
    .registers 7
    .parameter "ev"

    #@0
    .prologue
    .line 263
    if-eqz p1, :cond_b

    #@2
    .line 264
    :try_start_2
    iget-object v1, p0, Lcom/qualcomm/wfd/UIBCManager$EventQueue;->queuedEvents:Ljava/util/concurrent/BlockingQueue;

    #@4
    const-wide/16 v2, 0x1f4

    #@6
    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    #@8
    invoke-interface {v1, p1, v2, v3, v4}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Z
    :try_end_b
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_b} :catch_c
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_b} :catch_15

    #@b
    .line 271
    :cond_b
    :goto_b
    return-void

    #@c
    .line 266
    :catch_c
    move-exception v0

    #@d
    .line 267
    .local v0, e:Ljava/lang/InterruptedException;
    const-string v1, "UIBCManager"

    #@f
    const-string v2, "Interrupted when waiting to insert to queue"

    #@11
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@14
    goto :goto_b

    #@15
    .line 268
    .end local v0           #e:Ljava/lang/InterruptedException;
    :catch_15
    move-exception v0

    #@16
    .line 269
    .local v0, e:Ljava/lang/IllegalArgumentException;
    const-string v1, "UIBCManager"

    #@18
    const-string v2, "Illegal Argument Exception "

    #@1a
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1d
    goto :goto_b
.end method

.method public getNextEvent()Landroid/view/InputEvent;
    .registers 8

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 242
    iget-boolean v3, p0, Lcom/qualcomm/wfd/UIBCManager$EventQueue;->eventQueueStatus:Z

    #@3
    if-eqz v3, :cond_20

    #@5
    .line 245
    :try_start_5
    iget-object v3, p0, Lcom/qualcomm/wfd/UIBCManager$EventQueue;->queuedEvents:Ljava/util/concurrent/BlockingQueue;

    #@7
    const-wide/16 v4, 0x1f4

    #@9
    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    #@b
    invoke-interface {v3, v4, v5, v6}, Ljava/util/concurrent/BlockingQueue;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Landroid/view/InputEvent;
    :try_end_11
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_11} :catch_16

    #@11
    .line 246
    .local v1, queuedEvent:Landroid/view/InputEvent;
    if-eqz v1, :cond_14

    #@13
    .line 257
    .end local v1           #queuedEvent:Landroid/view/InputEvent;
    :goto_13
    return-object v1

    #@14
    .restart local v1       #queuedEvent:Landroid/view/InputEvent;
    :cond_14
    move-object v1, v2

    #@15
    .line 250
    goto :goto_13

    #@16
    .line 252
    .end local v1           #queuedEvent:Landroid/view/InputEvent;
    :catch_16
    move-exception v0

    #@17
    .line 253
    .local v0, e:Ljava/lang/InterruptedException;
    const-string v3, "UIBCManager"

    #@19
    const-string v4, "Interrupted when waiting to read from queue"

    #@1b
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1e
    move-object v1, v2

    #@1f
    .line 254
    goto :goto_13

    #@20
    .end local v0           #e:Ljava/lang/InterruptedException;
    :cond_20
    move-object v1, v2

    #@21
    .line 257
    goto :goto_13
.end method
