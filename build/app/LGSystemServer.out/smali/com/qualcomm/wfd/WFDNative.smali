.class public Lcom/qualcomm/wfd/WFDNative;
.super Ljava/lang/Object;
.source "WFDNative.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/wfd/WFDNative$WfdActionListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "WFDNative"

.field public static listener:Lcom/qualcomm/wfd/WFDNative$WfdActionListener;

.field private static localIpAddress:Ljava/lang/String;

.field private static localMacAddress:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 26
    const-string v0, "WFDNative"

    #@3
    const-string v1, "try to load libwfdrtsp.so"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 27
    const-string v0, "wfdrtsp"

    #@a
    #invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@d
    .line 28
    const-string v0, "WFDNative"

    #@f
    const-string v1, "libwfdrtsp.so loaded."

    #@11
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    .line 29
    const-string v0, "WFDNative"

    #@16
    const-string v1, "try to load libwfdnative.so"

    #@18
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 30
    const-string v0, "wfdnative"

    #@1d
    #invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@20
    .line 31
    const-string v0, "WFDNative"

    #@22
    const-string v1, "libwfdnative.so loaded."

    #@24
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 32
    invoke-static {v2, v2}, Lcom/qualcomm/wfd/WFDNative;->eventCallback(Ljava/lang/String;[Ljava/lang/Object;)V

    #@2a
    .line 80
    const-string v0, "00:00:00:00:00:00"

    #@2c
    sput-object v0, Lcom/qualcomm/wfd/WFDNative;->localMacAddress:Ljava/lang/String;

    #@2e
    .line 81
    const-string v0, ""

    #@30
    sput-object v0, Lcom/qualcomm/wfd/WFDNative;->localIpAddress:Ljava/lang/String;

    #@32
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 21
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 384
    return-void
.end method

.method public static disableUIBC(I)V
	.registers 1
	return-void
.end method

.method public static disableWfd()Z
	.registers 1
	const/4 v0, 0x1
	return v0
.end method

.method public static enableUIBC(I)V
	.registers 1
	return-void
.end method

.method public static enableWfd(Lcom/qualcomm/wfd/WfdDevice;)Z
	.registers 2
	const/4 v0, 0x1
	return v0
.end method

.method public static eventCallback(Ljava/lang/String;[Ljava/lang/Object;)V
    .registers 18
    .parameter "eventName"
    .parameter "objectArray"

    #@0
    .prologue
    .line 148
    const-string v13, "WFDNative"

    #@2
    const-string v14, "eventCallback triggered"

    #@4
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 149
    if-eqz p0, :cond_b

    #@9
    if-nez p1, :cond_13

    #@b
    .line 150
    :cond_b
    const-string v13, "WFDNative"

    #@d
    const-string v14, "No event info, ignore."

    #@f
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 372
    :cond_12
    :goto_12
    return-void

    #@13
    .line 153
    :cond_13
    move-object/from16 v0, p1

    #@15
    array-length v1, v0

    #@16
    .line 154
    .local v1, array_length:I
    const-string v13, "WFDNative"

    #@18
    new-instance v14, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v15, "CallbackEvent \""

    #@1f
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v14

    #@23
    move-object/from16 v0, p0

    #@25
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v14

    #@29
    const-string v15, "\" --- objectArray length="

    #@2b
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v14

    #@2f
    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v14

    #@33
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v14

    #@37
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 155
    const/4 v7, 0x0

    #@3b
    .local v7, i:I
    :goto_3b
    if-ge v7, v1, :cond_68

    #@3d
    .line 156
    const-string v13, "WFDNative"

    #@3f
    new-instance v14, Ljava/lang/StringBuilder;

    #@41
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@44
    const-string v15, "\tobjectArray["

    #@46
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v14

    #@4a
    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v14

    #@4e
    const-string v15, "] = "

    #@50
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v14

    #@54
    aget-object v15, p1, v7

    #@56
    invoke-virtual {v15}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@59
    move-result-object v15

    #@5a
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v14

    #@5e
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v14

    #@62
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@65
    .line 155
    add-int/lit8 v7, v7, 0x1

    #@67
    goto :goto_3b

    #@68
    .line 158
    :cond_68
    sget-object v13, Lcom/qualcomm/wfd/WFDNative;->listener:Lcom/qualcomm/wfd/WFDNative$WfdActionListener;

    #@6a
    if-nez v13, :cond_74

    #@6c
    .line 159
    const-string v13, "WFDNative"

    #@6e
    const-string v14, "Listener is destroyed, can\'t notify"

    #@70
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@73
    goto :goto_12

    #@74
    .line 162
    :cond_74
    const-string v13, "Error"

    #@76
    move-object/from16 v0, p0

    #@78
    invoke-virtual {v13, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@7b
    move-result v13

    #@7c
    if-eqz v13, :cond_bd

    #@7e
    .line 163
    move-object/from16 v0, p1

    #@80
    array-length v13, v0

    #@81
    if-lez v13, :cond_12

    #@83
    .line 164
    const-string v14, "RTSPCloseCallback"

    #@85
    const/4 v13, 0x0

    #@86
    aget-object v13, p1, v13

    #@88
    check-cast v13, Ljava/lang/String;

    #@8a
    invoke-virtual {v14, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@8d
    move-result v13

    #@8e
    if-eqz v13, :cond_9f

    #@90
    .line 165
    const-string v13, "WFDNative"

    #@92
    const-string v14, "RTSP close callback, treat as TEARDOWN start"

    #@94
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@97
    .line 166
    sget-object v13, Lcom/qualcomm/wfd/WFDNative;->listener:Lcom/qualcomm/wfd/WFDNative$WfdActionListener;

    #@99
    sget-object v14, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->TEARDOWN_START:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@9b
    const/4 v15, -0x1

    #@9c
    invoke-interface {v13, v14, v15}, Lcom/qualcomm/wfd/WFDNative$WfdActionListener;->notifyEvent(Lcom/qualcomm/wfd/WfdEnums$WfdEvent;I)V

    #@9f
    .line 168
    :cond_9f
    const-string v14, "StartSessionFail"

    #@a1
    const/4 v13, 0x0

    #@a2
    aget-object v13, p1, v13

    #@a4
    check-cast v13, Ljava/lang/String;

    #@a6
    invoke-virtual {v14, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@a9
    move-result v13

    #@aa
    if-eqz v13, :cond_12

    #@ac
    .line 169
    const-string v13, "WFDNative"

    #@ae
    const-string v14, "Start of WFD Session Failed"

    #@b0
    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b3
    .line 170
    sget-object v13, Lcom/qualcomm/wfd/WFDNative;->listener:Lcom/qualcomm/wfd/WFDNative$WfdActionListener;

    #@b5
    sget-object v14, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->START_SESSION_FAIL:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@b7
    const/4 v15, -0x1

    #@b8
    invoke-interface {v13, v14, v15}, Lcom/qualcomm/wfd/WFDNative$WfdActionListener;->notifyEvent(Lcom/qualcomm/wfd/WfdEnums$WfdEvent;I)V

    #@bb
    goto/16 :goto_12

    #@bd
    .line 174
    :cond_bd
    const-string v13, "ServiceStateChanged"

    #@bf
    move-object/from16 v0, p0

    #@c1
    invoke-virtual {v13, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@c4
    move-result v13

    #@c5
    if-eqz v13, :cond_11c

    #@c7
    .line 175
    const-string v14, "enabled"

    #@c9
    const/4 v13, 0x0

    #@ca
    aget-object v13, p1, v13

    #@cc
    check-cast v13, Ljava/lang/String;

    #@ce
    invoke-virtual {v14, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@d1
    move-result v4

    #@d2
    .line 176
    .local v4, enabled:Z
    if-eqz v4, :cond_f8

    #@d4
    .line 177
    sget-object v13, Lcom/qualcomm/wfd/WFDNative;->listener:Lcom/qualcomm/wfd/WFDNative$WfdActionListener;

    #@d6
    sget-object v14, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->WFD_SERVICE_ENABLED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@d8
    const/4 v15, 0x0

    #@d9
    invoke-interface {v13, v14, v15}, Lcom/qualcomm/wfd/WFDNative$WfdActionListener;->notifyEvent(Lcom/qualcomm/wfd/WfdEnums$WfdEvent;I)V

    #@dc
    .line 178
    const-string v13, "WFDNative"

    #@de
    new-instance v14, Ljava/lang/StringBuilder;

    #@e0
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@e3
    move-object/from16 v0, p0

    #@e5
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e8
    move-result-object v14

    #@e9
    const-string v15, " WFD_SERVICE_ENABLED"

    #@eb
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ee
    move-result-object v14

    #@ef
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f2
    move-result-object v14

    #@f3
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f6
    goto/16 :goto_12

    #@f8
    .line 180
    :cond_f8
    sget-object v13, Lcom/qualcomm/wfd/WFDNative;->listener:Lcom/qualcomm/wfd/WFDNative$WfdActionListener;

    #@fa
    sget-object v14, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->WFD_SERVICE_DISABLED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@fc
    const/4 v15, 0x0

    #@fd
    invoke-interface {v13, v14, v15}, Lcom/qualcomm/wfd/WFDNative$WfdActionListener;->notifyEvent(Lcom/qualcomm/wfd/WfdEnums$WfdEvent;I)V

    #@100
    .line 181
    const-string v13, "WFDNative"

    #@102
    new-instance v14, Ljava/lang/StringBuilder;

    #@104
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@107
    move-object/from16 v0, p0

    #@109
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10c
    move-result-object v14

    #@10d
    const-string v15, " WFD_SERVICE_DISABLED"

    #@10f
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@112
    move-result-object v14

    #@113
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@116
    move-result-object v14

    #@117
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@11a
    goto/16 :goto_12

    #@11c
    .line 184
    .end local v4           #enabled:Z
    :cond_11c
    const-string v13, "SessionStateChanged"

    #@11e
    move-object/from16 v0, p0

    #@120
    invoke-virtual {v13, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@123
    move-result v13

    #@124
    if-eqz v13, :cond_1ad

    #@126
    .line 186
    move-object/from16 v0, p1

    #@128
    array-length v13, v0

    #@129
    const/4 v14, 0x3

    #@12a
    if-ne v13, v14, :cond_12

    #@12c
    .line 187
    const/4 v13, 0x0

    #@12d
    aget-object v9, p1, v13

    #@12f
    check-cast v9, Ljava/lang/String;

    #@131
    .line 188
    .local v9, state:Ljava/lang/String;
    const/4 v13, 0x2

    #@132
    aget-object v13, p1, v13

    #@134
    check-cast v13, Ljava/lang/String;

    #@136
    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@139
    move-result v8

    #@13a
    .line 189
    .local v8, sessionId:I
    const-string v13, "STANDBY"

    #@13c
    invoke-virtual {v13, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@13f
    move-result v13

    #@140
    if-eqz v13, :cond_174

    #@142
    .line 190
    sget-object v13, Lcom/qualcomm/wfd/WFDNative;->listener:Lcom/qualcomm/wfd/WFDNative$WfdActionListener;

    #@144
    sget-object v14, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->STANDBY_START:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@146
    invoke-interface {v13, v14, v8}, Lcom/qualcomm/wfd/WFDNative$WfdActionListener;->notifyEvent(Lcom/qualcomm/wfd/WfdEnums$WfdEvent;I)V

    #@149
    .line 198
    :goto_149
    const-string v14, "WFDNative"

    #@14b
    new-instance v13, Ljava/lang/StringBuilder;

    #@14d
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@150
    const-string v15, "Event: "

    #@152
    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@155
    move-result-object v13

    #@156
    move-object/from16 v0, p0

    #@158
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15b
    move-result-object v13

    #@15c
    const-string v15, " State: "

    #@15e
    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@161
    move-result-object v15

    #@162
    const/4 v13, 0x2

    #@163
    aget-object v13, p1, v13

    #@165
    check-cast v13, Ljava/lang/String;

    #@167
    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16a
    move-result-object v13

    #@16b
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16e
    move-result-object v13

    #@16f
    invoke-static {v14, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@172
    goto/16 :goto_12

    #@174
    .line 191
    :cond_174
    const-string v13, "NEGOTIATING"

    #@176
    invoke-virtual {v13, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@179
    move-result v13

    #@17a
    if-eqz v13, :cond_184

    #@17c
    .line 192
    sget-object v13, Lcom/qualcomm/wfd/WFDNative;->listener:Lcom/qualcomm/wfd/WFDNative$WfdActionListener;

    #@17e
    sget-object v14, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ESTABLISHED:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@180
    invoke-interface {v13, v14, v8}, Lcom/qualcomm/wfd/WFDNative$WfdActionListener;->updateState(Lcom/qualcomm/wfd/WfdEnums$SessionState;I)V

    #@183
    goto :goto_149

    #@184
    .line 193
    :cond_184
    const-string v13, "STOPPED"

    #@186
    invoke-virtual {v13, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@189
    move-result v13

    #@18a
    if-eqz v13, :cond_194

    #@18c
    .line 194
    sget-object v13, Lcom/qualcomm/wfd/WFDNative;->listener:Lcom/qualcomm/wfd/WFDNative$WfdActionListener;

    #@18e
    sget-object v14, Lcom/qualcomm/wfd/WfdEnums$SessionState;->TEARDOWN:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@190
    invoke-interface {v13, v14, v8}, Lcom/qualcomm/wfd/WFDNative$WfdActionListener;->updateState(Lcom/qualcomm/wfd/WfdEnums$SessionState;I)V

    #@193
    goto :goto_149

    #@194
    .line 196
    :cond_194
    const-string v13, "WFDNative"

    #@196
    new-instance v14, Ljava/lang/StringBuilder;

    #@198
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@19b
    const-string v15, "No Session state change is required for native state "

    #@19d
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a0
    move-result-object v14

    #@1a1
    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a4
    move-result-object v14

    #@1a5
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a8
    move-result-object v14

    #@1a9
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1ac
    goto :goto_149

    #@1ad
    .line 202
    .end local v8           #sessionId:I
    .end local v9           #state:Ljava/lang/String;
    :cond_1ad
    const-string v13, "StreamControlCompleted"

    #@1af
    move-object/from16 v0, p0

    #@1b1
    invoke-virtual {v13, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1b4
    move-result v13

    #@1b5
    if-eqz v13, :cond_227

    #@1b7
    .line 203
    const-string v13, "WFDNative"

    #@1b9
    move-object/from16 v0, p0

    #@1bb
    invoke-static {v13, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1be
    .line 204
    move-object/from16 v0, p1

    #@1c0
    array-length v13, v0

    #@1c1
    const/4 v14, 0x2

    #@1c2
    if-lt v13, v14, :cond_12

    #@1c4
    .line 205
    const/4 v13, 0x1

    #@1c5
    aget-object v9, p1, v13

    #@1c7
    check-cast v9, Ljava/lang/String;

    #@1c9
    .line 206
    .restart local v9       #state:Ljava/lang/String;
    const/4 v13, 0x0

    #@1ca
    aget-object v13, p1, v13

    #@1cc
    check-cast v13, Ljava/lang/String;

    #@1ce
    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@1d1
    move-result v8

    #@1d2
    .line 207
    .restart local v8       #sessionId:I
    const-string v13, "PLAY"

    #@1d4
    invoke-virtual {v13, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1d7
    move-result v13

    #@1d8
    if-eqz v13, :cond_1e3

    #@1da
    .line 208
    sget-object v13, Lcom/qualcomm/wfd/WFDNative;->listener:Lcom/qualcomm/wfd/WFDNative$WfdActionListener;

    #@1dc
    sget-object v14, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->PLAY_START:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@1de
    invoke-interface {v13, v14, v8}, Lcom/qualcomm/wfd/WFDNative$WfdActionListener;->notifyEvent(Lcom/qualcomm/wfd/WfdEnums$WfdEvent;I)V

    #@1e1
    goto/16 :goto_12

    #@1e3
    .line 209
    :cond_1e3
    const-string v13, "PLAY_DONE"

    #@1e5
    invoke-virtual {v13, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1e8
    move-result v13

    #@1e9
    if-eqz v13, :cond_1f4

    #@1eb
    .line 210
    sget-object v13, Lcom/qualcomm/wfd/WFDNative;->listener:Lcom/qualcomm/wfd/WFDNative$WfdActionListener;

    #@1ed
    sget-object v14, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PLAY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@1ef
    invoke-interface {v13, v14, v8}, Lcom/qualcomm/wfd/WFDNative$WfdActionListener;->updateState(Lcom/qualcomm/wfd/WfdEnums$SessionState;I)V

    #@1f2
    goto/16 :goto_12

    #@1f4
    .line 211
    :cond_1f4
    const-string v13, "PAUSE"

    #@1f6
    invoke-virtual {v13, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1f9
    move-result v13

    #@1fa
    if-eqz v13, :cond_205

    #@1fc
    .line 212
    sget-object v13, Lcom/qualcomm/wfd/WFDNative;->listener:Lcom/qualcomm/wfd/WFDNative$WfdActionListener;

    #@1fe
    sget-object v14, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->PAUSE_START:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@200
    invoke-interface {v13, v14, v8}, Lcom/qualcomm/wfd/WFDNative$WfdActionListener;->notifyEvent(Lcom/qualcomm/wfd/WfdEnums$WfdEvent;I)V

    #@203
    goto/16 :goto_12

    #@205
    .line 213
    :cond_205
    const-string v13, "PAUSE_DONE"

    #@207
    invoke-virtual {v13, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@20a
    move-result v13

    #@20b
    if-eqz v13, :cond_216

    #@20d
    .line 214
    sget-object v13, Lcom/qualcomm/wfd/WFDNative;->listener:Lcom/qualcomm/wfd/WFDNative$WfdActionListener;

    #@20f
    sget-object v14, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PAUSE:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@211
    invoke-interface {v13, v14, v8}, Lcom/qualcomm/wfd/WFDNative$WfdActionListener;->updateState(Lcom/qualcomm/wfd/WfdEnums$SessionState;I)V

    #@214
    goto/16 :goto_12

    #@216
    .line 215
    :cond_216
    const-string v13, "TEARDOWN"

    #@218
    invoke-virtual {v13, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@21b
    move-result v13

    #@21c
    if-eqz v13, :cond_12

    #@21e
    .line 216
    sget-object v13, Lcom/qualcomm/wfd/WFDNative;->listener:Lcom/qualcomm/wfd/WFDNative$WfdActionListener;

    #@220
    sget-object v14, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->TEARDOWN_START:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@222
    invoke-interface {v13, v14, v8}, Lcom/qualcomm/wfd/WFDNative$WfdActionListener;->notifyEvent(Lcom/qualcomm/wfd/WfdEnums$WfdEvent;I)V

    #@225
    goto/16 :goto_12

    #@227
    .line 219
    .end local v8           #sessionId:I
    .end local v9           #state:Ljava/lang/String;
    :cond_227
    const-string v13, "UIBCControlCompleted"

    #@229
    move-object/from16 v0, p0

    #@22b
    invoke-virtual {v13, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@22e
    move-result v13

    #@22f
    if-eqz v13, :cond_2e9

    #@231
    .line 220
    move-object/from16 v0, p1

    #@233
    array-length v13, v0

    #@234
    const/4 v14, 0x2

    #@235
    if-lt v13, v14, :cond_12

    #@237
    .line 221
    const/4 v13, 0x0

    #@238
    aget-object v13, p1, v13

    #@23a
    check-cast v13, Ljava/lang/String;

    #@23c
    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@23f
    move-result v8

    #@240
    .line 222
    .restart local v8       #sessionId:I
    const/4 v13, 0x1

    #@241
    aget-object v9, p1, v13

    #@243
    check-cast v9, Ljava/lang/String;

    #@245
    .line 223
    .restart local v9       #state:Ljava/lang/String;
    const-string v13, "ENABLED"

    #@247
    invoke-virtual {v13, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@24a
    move-result v13

    #@24b
    if-eqz v13, :cond_270

    #@24d
    .line 224
    sget-object v13, Lcom/qualcomm/wfd/WFDNative;->listener:Lcom/qualcomm/wfd/WFDNative$WfdActionListener;

    #@24f
    sget-object v14, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->UIBC_ENABLED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@251
    invoke-interface {v13, v14, v8}, Lcom/qualcomm/wfd/WFDNative$WfdActionListener;->notifyEvent(Lcom/qualcomm/wfd/WfdEnums$WfdEvent;I)V

    #@254
    .line 225
    const-string v13, "WFDNative"

    #@256
    new-instance v14, Ljava/lang/StringBuilder;

    #@258
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@25b
    move-object/from16 v0, p0

    #@25d
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@260
    move-result-object v14

    #@261
    const-string v15, " ENABLED"

    #@263
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@266
    move-result-object v14

    #@267
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26a
    move-result-object v14

    #@26b
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26e
    goto/16 :goto_12

    #@270
    .line 227
    :cond_270
    const-string v13, "DISABLED"

    #@272
    invoke-virtual {v13, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@275
    move-result v13

    #@276
    if-eqz v13, :cond_29b

    #@278
    .line 230
    sget-object v13, Lcom/qualcomm/wfd/WFDNative;->listener:Lcom/qualcomm/wfd/WFDNative$WfdActionListener;

    #@27a
    sget-object v14, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->UIBC_DISABLED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@27c
    invoke-interface {v13, v14, v8}, Lcom/qualcomm/wfd/WFDNative$WfdActionListener;->notifyEvent(Lcom/qualcomm/wfd/WfdEnums$WfdEvent;I)V

    #@27f
    .line 231
    const-string v13, "WFDNative"

    #@281
    new-instance v14, Ljava/lang/StringBuilder;

    #@283
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@286
    move-object/from16 v0, p0

    #@288
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28b
    move-result-object v14

    #@28c
    const-string v15, " DISABLED"

    #@28e
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@291
    move-result-object v14

    #@292
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@295
    move-result-object v14

    #@296
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@299
    goto/16 :goto_12

    #@29b
    .line 233
    :cond_29b
    const-string v13, "SUPPORTED"

    #@29d
    invoke-virtual {v13, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@2a0
    move-result v13

    #@2a1
    if-eqz v13, :cond_2c6

    #@2a3
    .line 234
    sget-object v13, Lcom/qualcomm/wfd/WFDNative;->listener:Lcom/qualcomm/wfd/WFDNative$WfdActionListener;

    #@2a5
    sget-object v14, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->UIBC_SUPPORTED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@2a7
    invoke-interface {v13, v14, v8}, Lcom/qualcomm/wfd/WFDNative$WfdActionListener;->notifyEvent(Lcom/qualcomm/wfd/WfdEnums$WfdEvent;I)V

    #@2aa
    .line 235
    const-string v13, "WFDNative"

    #@2ac
    new-instance v14, Ljava/lang/StringBuilder;

    #@2ae
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@2b1
    move-object/from16 v0, p0

    #@2b3
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b6
    move-result-object v14

    #@2b7
    const-string v15, " SUPPORTED"

    #@2b9
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2bc
    move-result-object v14

    #@2bd
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c0
    move-result-object v14

    #@2c1
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2c4
    goto/16 :goto_12

    #@2c6
    .line 237
    :cond_2c6
    sget-object v13, Lcom/qualcomm/wfd/WFDNative;->listener:Lcom/qualcomm/wfd/WFDNative$WfdActionListener;

    #@2c8
    sget-object v14, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->UIBC_NOT_SUPPORTED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@2ca
    invoke-interface {v13, v14, v8}, Lcom/qualcomm/wfd/WFDNative$WfdActionListener;->notifyEvent(Lcom/qualcomm/wfd/WfdEnums$WfdEvent;I)V

    #@2cd
    .line 238
    const-string v13, "WFDNative"

    #@2cf
    new-instance v14, Ljava/lang/StringBuilder;

    #@2d1
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@2d4
    move-object/from16 v0, p0

    #@2d6
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d9
    move-result-object v14

    #@2da
    const-string v15, " NOT SUPPORTED"

    #@2dc
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2df
    move-result-object v14

    #@2e0
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e3
    move-result-object v14

    #@2e4
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2e7
    goto/16 :goto_12

    #@2e9
    .line 242
    .end local v8           #sessionId:I
    .end local v9           #state:Ljava/lang/String;
    :cond_2e9
    const-string v13, "MMEvent"

    #@2eb
    move-object/from16 v0, p0

    #@2ed
    invoke-virtual {v13, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@2f0
    move-result v13

    #@2f1
    if-eqz v13, :cond_3b3

    #@2f3
    .line 243
    move-object/from16 v0, p1

    #@2f5
    array-length v13, v0

    #@2f6
    const/4 v14, 0x2

    #@2f7
    if-lt v13, v14, :cond_12

    #@2f9
    .line 244
    const/4 v13, 0x0

    #@2fa
    aget-object v11, p1, v13

    #@2fc
    check-cast v11, Ljava/lang/String;

    #@2fe
    .line 245
    .local v11, var:Ljava/lang/String;
    const/4 v13, 0x1

    #@2ff
    aget-object v10, p1, v13

    #@301
    check-cast v10, Ljava/lang/String;

    #@303
    .line 246
    .local v10, value:Ljava/lang/String;
    const-string v13, "HDCP_CONNECT"

    #@305
    invoke-virtual {v13, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@308
    move-result v13

    #@309
    if-eqz v13, :cond_339

    #@30b
    .line 247
    const-string v13, "SUCCESS"

    #@30d
    invoke-virtual {v13, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@310
    move-result v13

    #@311
    if-eqz v13, :cond_31d

    #@313
    .line 248
    sget-object v13, Lcom/qualcomm/wfd/WFDNative;->listener:Lcom/qualcomm/wfd/WFDNative$WfdActionListener;

    #@315
    sget-object v14, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->HDCP_CONNECT_SUCCESS:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@317
    const/4 v15, -0x1

    #@318
    invoke-interface {v13, v14, v15}, Lcom/qualcomm/wfd/WFDNative$WfdActionListener;->notifyEvent(Lcom/qualcomm/wfd/WfdEnums$WfdEvent;I)V

    #@31b
    goto/16 :goto_12

    #@31d
    .line 249
    :cond_31d
    const-string v13, "UNSUPPORTEDBYPEER"

    #@31f
    invoke-virtual {v13, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@322
    move-result v13

    #@323
    if-eqz v13, :cond_32f

    #@325
    .line 250
    sget-object v13, Lcom/qualcomm/wfd/WFDNative;->listener:Lcom/qualcomm/wfd/WFDNative$WfdActionListener;

    #@327
    sget-object v14, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->HDCP_ENFORCE_FAIL:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@329
    const/4 v15, -0x1

    #@32a
    invoke-interface {v13, v14, v15}, Lcom/qualcomm/wfd/WFDNative$WfdActionListener;->notifyEvent(Lcom/qualcomm/wfd/WfdEnums$WfdEvent;I)V

    #@32d
    goto/16 :goto_12

    #@32f
    .line 252
    :cond_32f
    sget-object v13, Lcom/qualcomm/wfd/WFDNative;->listener:Lcom/qualcomm/wfd/WFDNative$WfdActionListener;

    #@331
    sget-object v14, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->HDCP_CONNECT_FAIL:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@333
    const/4 v15, -0x1

    #@334
    invoke-interface {v13, v14, v15}, Lcom/qualcomm/wfd/WFDNative$WfdActionListener;->notifyEvent(Lcom/qualcomm/wfd/WfdEnums$WfdEvent;I)V

    #@337
    goto/16 :goto_12

    #@339
    .line 254
    :cond_339
    const-string v13, "MMStreamStarted"

    #@33b
    invoke-virtual {v13, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@33e
    move-result v13

    #@33f
    if-eqz v13, :cond_12

    #@341
    .line 255
    const/4 v13, 0x1

    #@342
    aget-object v13, p1, v13

    #@344
    check-cast v13, Ljava/lang/String;

    #@346
    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@349
    move-result v12

    #@34a
    .line 256
    .local v12, width:I
    const/4 v13, 0x2

    #@34b
    aget-object v13, p1, v13

    #@34d
    check-cast v13, Ljava/lang/String;

    #@34f
    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@352
    move-result v6

    #@353
    .line 257
    .local v6, height:I
    const/4 v5, 0x0

    #@354
    .line 258
    .local v5, hdcp:I
    move-object/from16 v0, p1

    #@356
    array-length v13, v0

    #@357
    const/4 v14, 0x3

    #@358
    if-le v13, v14, :cond_363

    #@35a
    .line 259
    const/4 v13, 0x3

    #@35b
    aget-object v13, p1, v13

    #@35d
    check-cast v13, Ljava/lang/String;

    #@35f
    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@362
    move-result v5

    #@363
    .line 261
    :cond_363
    const-string v13, "WFDNative"

    #@365
    new-instance v14, Ljava/lang/StringBuilder;

    #@367
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@36a
    const-string v15, "MMStreamStarted"

    #@36c
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36f
    move-result-object v14

    #@370
    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@373
    move-result-object v14

    #@374
    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@377
    move-result-object v14

    #@378
    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@37b
    move-result-object v14

    #@37c
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37f
    move-result-object v14

    #@380
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@383
    .line 263
    new-instance v2, Landroid/os/Bundle;

    #@385
    const/4 v13, 0x4

    #@386
    invoke-direct {v2, v13}, Landroid/os/Bundle;-><init>(I)V

    #@389
    .line 264
    .local v2, b:Landroid/os/Bundle;
    const-string v13, "event"

    #@38b
    invoke-virtual {v2, v13, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@38e
    .line 265
    const-string v14, "width"

    #@390
    const/4 v13, 0x1

    #@391
    aget-object v13, p1, v13

    #@393
    check-cast v13, Ljava/lang/String;

    #@395
    invoke-virtual {v2, v14, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@398
    .line 266
    const-string v14, "height"

    #@39a
    const/4 v13, 0x2

    #@39b
    aget-object v13, p1, v13

    #@39d
    check-cast v13, Ljava/lang/String;

    #@39f
    invoke-virtual {v2, v14, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@3a2
    .line 267
    const-string v13, "hdcp"

    #@3a4
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@3a7
    move-result-object v14

    #@3a8
    invoke-virtual {v2, v13, v14}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@3ab
    .line 269
    sget-object v13, Lcom/qualcomm/wfd/WFDNative;->listener:Lcom/qualcomm/wfd/WFDNative$WfdActionListener;

    #@3ad
    const/4 v14, -0x1

    #@3ae
    invoke-interface {v13, v2, v14}, Lcom/qualcomm/wfd/WFDNative$WfdActionListener;->notify(Landroid/os/Bundle;I)V

    #@3b1
    goto/16 :goto_12

    #@3b3
    .line 272
    .end local v2           #b:Landroid/os/Bundle;
    .end local v5           #hdcp:I
    .end local v6           #height:I
    .end local v10           #value:Ljava/lang/String;
    .end local v11           #var:Ljava/lang/String;
    .end local v12           #width:I
    :cond_3b3
    const-string v13, "VideoEvent"

    #@3b5
    move-object/from16 v0, p0

    #@3b7
    invoke-virtual {v13, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@3ba
    move-result v13

    #@3bb
    if-eqz v13, :cond_40f

    #@3bd
    .line 273
    move-object/from16 v0, p1

    #@3bf
    array-length v13, v0

    #@3c0
    const/4 v14, 0x1

    #@3c1
    if-lt v13, v14, :cond_406

    #@3c3
    .line 274
    const/4 v13, 0x0

    #@3c4
    aget-object v3, p1, v13

    #@3c6
    check-cast v3, Ljava/lang/String;

    #@3c8
    .line 275
    .local v3, desc:Ljava/lang/String;
    const-string v13, "RuntimeError"

    #@3ca
    invoke-virtual {v13, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@3cd
    move-result v13

    #@3ce
    if-eqz v13, :cond_3da

    #@3d0
    .line 276
    sget-object v13, Lcom/qualcomm/wfd/WFDNative;->listener:Lcom/qualcomm/wfd/WFDNative$WfdActionListener;

    #@3d2
    sget-object v14, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->VIDEO_RUNTIME_ERROR:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@3d4
    const/4 v15, -0x1

    #@3d5
    invoke-interface {v13, v14, v15}, Lcom/qualcomm/wfd/WFDNative$WfdActionListener;->notifyEvent(Lcom/qualcomm/wfd/WfdEnums$WfdEvent;I)V

    #@3d8
    goto/16 :goto_12

    #@3da
    .line 277
    :cond_3da
    const-string v13, "ConfigureFailure"

    #@3dc
    invoke-virtual {v13, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@3df
    move-result v13

    #@3e0
    if-eqz v13, :cond_3ec

    #@3e2
    .line 278
    sget-object v13, Lcom/qualcomm/wfd/WFDNative;->listener:Lcom/qualcomm/wfd/WFDNative$WfdActionListener;

    #@3e4
    sget-object v14, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->VIDEO_CONFIGURE_FAILURE:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@3e6
    const/4 v15, -0x1

    #@3e7
    invoke-interface {v13, v14, v15}, Lcom/qualcomm/wfd/WFDNative$WfdActionListener;->notifyEvent(Lcom/qualcomm/wfd/WfdEnums$WfdEvent;I)V

    #@3ea
    goto/16 :goto_12

    #@3ec
    .line 280
    :cond_3ec
    const-string v13, "WFDNative"

    #@3ee
    new-instance v14, Ljava/lang/StringBuilder;

    #@3f0
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@3f3
    const-string v15, "Unknown description:"

    #@3f5
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f8
    move-result-object v14

    #@3f9
    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3fc
    move-result-object v14

    #@3fd
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@400
    move-result-object v14

    #@401
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@404
    goto/16 :goto_12

    #@406
    .line 283
    .end local v3           #desc:Ljava/lang/String;
    :cond_406
    const-string v13, "WFDNative"

    #@408
    const-string v14, "No description for VideoEvent"

    #@40a
    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@40d
    goto/16 :goto_12

    #@40f
    .line 285
    :cond_40f
    const-string v13, "AudioEvent"

    #@411
    move-object/from16 v0, p0

    #@413
    invoke-virtual {v13, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@416
    move-result v13

    #@417
    if-eqz v13, :cond_48f

    #@419
    .line 286
    move-object/from16 v0, p1

    #@41b
    array-length v13, v0

    #@41c
    const/4 v14, 0x1

    #@41d
    if-lt v13, v14, :cond_486

    #@41f
    .line 287
    const/4 v13, 0x0

    #@420
    aget-object v3, p1, v13

    #@422
    check-cast v3, Ljava/lang/String;

    #@424
    .line 288
    .restart local v3       #desc:Ljava/lang/String;
    const-string v13, "RuntimeError"

    #@426
    invoke-virtual {v13, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@429
    move-result v13

    #@42a
    if-eqz v13, :cond_436

    #@42c
    .line 289
    sget-object v13, Lcom/qualcomm/wfd/WFDNative;->listener:Lcom/qualcomm/wfd/WFDNative$WfdActionListener;

    #@42e
    sget-object v14, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->AUDIO_RUNTIME_ERROR:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@430
    const/4 v15, -0x1

    #@431
    invoke-interface {v13, v14, v15}, Lcom/qualcomm/wfd/WFDNative$WfdActionListener;->notifyEvent(Lcom/qualcomm/wfd/WfdEnums$WfdEvent;I)V

    #@434
    goto/16 :goto_12

    #@436
    .line 290
    :cond_436
    const-string v13, "ConfigureFailure"

    #@438
    invoke-virtual {v13, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@43b
    move-result v13

    #@43c
    if-eqz v13, :cond_448

    #@43e
    .line 291
    sget-object v13, Lcom/qualcomm/wfd/WFDNative;->listener:Lcom/qualcomm/wfd/WFDNative$WfdActionListener;

    #@440
    sget-object v14, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->AUDIO_CONFIGURE_FAILURE:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@442
    const/4 v15, -0x1

    #@443
    invoke-interface {v13, v14, v15}, Lcom/qualcomm/wfd/WFDNative$WfdActionListener;->notifyEvent(Lcom/qualcomm/wfd/WfdEnums$WfdEvent;I)V

    #@446
    goto/16 :goto_12

    #@448
    .line 292
    :cond_448
    const-string v13, "AudioProxyOpened"

    #@44a
    invoke-virtual {v13, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@44d
    move-result v13

    #@44e
    if-eqz v13, :cond_45a

    #@450
    .line 293
    sget-object v13, Lcom/qualcomm/wfd/WFDNative;->listener:Lcom/qualcomm/wfd/WFDNative$WfdActionListener;

    #@452
    sget-object v14, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->AUDIOPROXY_OPENED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@454
    const/4 v15, -0x1

    #@455
    invoke-interface {v13, v14, v15}, Lcom/qualcomm/wfd/WFDNative$WfdActionListener;->notifyEvent(Lcom/qualcomm/wfd/WfdEnums$WfdEvent;I)V

    #@458
    goto/16 :goto_12

    #@45a
    .line 294
    :cond_45a
    const-string v13, "AudioProxyClosed"

    #@45c
    invoke-virtual {v13, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@45f
    move-result v13

    #@460
    if-eqz v13, :cond_46c

    #@462
    .line 295
    sget-object v13, Lcom/qualcomm/wfd/WFDNative;->listener:Lcom/qualcomm/wfd/WFDNative$WfdActionListener;

    #@464
    sget-object v14, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->AUDIOPROXY_CLOSED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@466
    const/4 v15, -0x1

    #@467
    invoke-interface {v13, v14, v15}, Lcom/qualcomm/wfd/WFDNative$WfdActionListener;->notifyEvent(Lcom/qualcomm/wfd/WfdEnums$WfdEvent;I)V

    #@46a
    goto/16 :goto_12

    #@46c
    .line 297
    :cond_46c
    const-string v13, "WFDNative"

    #@46e
    new-instance v14, Ljava/lang/StringBuilder;

    #@470
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@473
    const-string v15, "Unknown description:"

    #@475
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@478
    move-result-object v14

    #@479
    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47c
    move-result-object v14

    #@47d
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@480
    move-result-object v14

    #@481
    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@484
    goto/16 :goto_12

    #@486
    .line 300
    .end local v3           #desc:Ljava/lang/String;
    :cond_486
    const-string v13, "WFDNative"

    #@488
    const-string v14, "No description for AudioEvent"

    #@48a
    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@48d
    goto/16 :goto_12

    #@48f
    .line 302
    :cond_48f
    const-string v13, "HdcpEvent"

    #@491
    move-object/from16 v0, p0

    #@493
    invoke-virtual {v13, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@496
    move-result v13

    #@497
    if-eqz v13, :cond_4d9

    #@499
    .line 303
    move-object/from16 v0, p1

    #@49b
    array-length v13, v0

    #@49c
    const/4 v14, 0x1

    #@49d
    if-lt v13, v14, :cond_4d0

    #@49f
    .line 304
    const/4 v13, 0x0

    #@4a0
    aget-object v3, p1, v13

    #@4a2
    check-cast v3, Ljava/lang/String;

    #@4a4
    .line 305
    .restart local v3       #desc:Ljava/lang/String;
    const-string v13, "RuntimeError"

    #@4a6
    invoke-virtual {v13, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@4a9
    move-result v13

    #@4aa
    if-eqz v13, :cond_4b6

    #@4ac
    .line 306
    sget-object v13, Lcom/qualcomm/wfd/WFDNative;->listener:Lcom/qualcomm/wfd/WFDNative$WfdActionListener;

    #@4ae
    sget-object v14, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->HDCP_RUNTIME_ERROR:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@4b0
    const/4 v15, -0x1

    #@4b1
    invoke-interface {v13, v14, v15}, Lcom/qualcomm/wfd/WFDNative$WfdActionListener;->notifyEvent(Lcom/qualcomm/wfd/WfdEnums$WfdEvent;I)V

    #@4b4
    goto/16 :goto_12

    #@4b6
    .line 308
    :cond_4b6
    const-string v13, "WFDNative"

    #@4b8
    new-instance v14, Ljava/lang/StringBuilder;

    #@4ba
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@4bd
    const-string v15, "Unknown description:"

    #@4bf
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c2
    move-result-object v14

    #@4c3
    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c6
    move-result-object v14

    #@4c7
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4ca
    move-result-object v14

    #@4cb
    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4ce
    goto/16 :goto_12

    #@4d0
    .line 311
    .end local v3           #desc:Ljava/lang/String;
    :cond_4d0
    const-string v13, "WFDNative"

    #@4d2
    const-string v14, "No description for AudioEvent"

    #@4d4
    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4d7
    goto/16 :goto_12

    #@4d9
    .line 314
    :cond_4d9
    const-string v13, "NetworkEvent"

    #@4db
    move-object/from16 v0, p0

    #@4dd
    invoke-virtual {v13, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@4e0
    move-result v13

    #@4e1
    if-eqz v13, :cond_5c5

    #@4e3
    .line 315
    move-object/from16 v0, p1

    #@4e5
    array-length v13, v0

    #@4e6
    const/4 v14, 0x1

    #@4e7
    if-lt v13, v14, :cond_5bc

    #@4e9
    .line 316
    const/4 v13, 0x0

    #@4ea
    aget-object v3, p1, v13

    #@4ec
    check-cast v3, Ljava/lang/String;

    #@4ee
    .line 317
    .restart local v3       #desc:Ljava/lang/String;
    const-string v13, "RuntimeError"

    #@4f0
    invoke-virtual {v13, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@4f3
    move-result v13

    #@4f4
    if-eqz v13, :cond_500

    #@4f6
    .line 318
    sget-object v13, Lcom/qualcomm/wfd/WFDNative;->listener:Lcom/qualcomm/wfd/WFDNative$WfdActionListener;

    #@4f8
    sget-object v14, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->NETWORK_RUNTIME_ERROR:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@4fa
    const/4 v15, -0x1

    #@4fb
    invoke-interface {v13, v14, v15}, Lcom/qualcomm/wfd/WFDNative$WfdActionListener;->notifyEvent(Lcom/qualcomm/wfd/WfdEnums$WfdEvent;I)V

    #@4fe
    goto/16 :goto_12

    #@500
    .line 319
    :cond_500
    const-string v13, "ConfigureFailure"

    #@502
    invoke-virtual {v13, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@505
    move-result v13

    #@506
    if-eqz v13, :cond_512

    #@508
    .line 320
    sget-object v13, Lcom/qualcomm/wfd/WFDNative;->listener:Lcom/qualcomm/wfd/WFDNative$WfdActionListener;

    #@50a
    sget-object v14, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->NETWORK_CONFIGURE_FAILURE:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@50c
    const/4 v15, -0x1

    #@50d
    invoke-interface {v13, v14, v15}, Lcom/qualcomm/wfd/WFDNative$WfdActionListener;->notifyEvent(Lcom/qualcomm/wfd/WfdEnums$WfdEvent;I)V

    #@510
    goto/16 :goto_12

    #@512
    .line 321
    :cond_512
    const-string v13, "RtpTransportNegotiationSuccess"

    #@514
    invoke-virtual {v13, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@517
    move-result v13

    #@518
    if-eqz v13, :cond_524

    #@51a
    .line 322
    sget-object v13, Lcom/qualcomm/wfd/WFDNative;->listener:Lcom/qualcomm/wfd/WFDNative$WfdActionListener;

    #@51c
    sget-object v14, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->RTP_TRANSPORT_NEGOTIATED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@51e
    const/4 v15, -0x1

    #@51f
    invoke-interface {v13, v14, v15}, Lcom/qualcomm/wfd/WFDNative$WfdActionListener;->notifyEvent(Lcom/qualcomm/wfd/WfdEnums$WfdEvent;I)V

    #@522
    goto/16 :goto_12

    #@524
    .line 323
    :cond_524
    const-string v13, "BufferingUpdate"

    #@526
    invoke-virtual {v13, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@529
    move-result v13

    #@52a
    if-eqz v13, :cond_559

    #@52c
    move-object/from16 v0, p1

    #@52e
    array-length v13, v0

    #@52f
    const/4 v14, 0x3

    #@530
    if-lt v13, v14, :cond_559

    #@532
    .line 324
    new-instance v2, Landroid/os/Bundle;

    #@534
    const/4 v13, 0x3

    #@535
    invoke-direct {v2, v13}, Landroid/os/Bundle;-><init>(I)V

    #@538
    .line 325
    .restart local v2       #b:Landroid/os/Bundle;
    const-string v13, "event"

    #@53a
    invoke-virtual {v2, v13, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@53d
    .line 326
    const-string v14, "bufferLength"

    #@53f
    const/4 v13, 0x1

    #@540
    aget-object v13, p1, v13

    #@542
    check-cast v13, Ljava/lang/String;

    #@544
    invoke-virtual {v2, v14, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@547
    .line 327
    const-string v14, "windowSize"

    #@549
    const/4 v13, 0x2

    #@54a
    aget-object v13, p1, v13

    #@54c
    check-cast v13, Ljava/lang/String;

    #@54e
    invoke-virtual {v2, v14, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@551
    .line 328
    sget-object v13, Lcom/qualcomm/wfd/WFDNative;->listener:Lcom/qualcomm/wfd/WFDNative$WfdActionListener;

    #@553
    const/4 v14, -0x1

    #@554
    invoke-interface {v13, v2, v14}, Lcom/qualcomm/wfd/WFDNative$WfdActionListener;->notify(Landroid/os/Bundle;I)V

    #@557
    goto/16 :goto_12

    #@559
    .line 329
    .end local v2           #b:Landroid/os/Bundle;
    :cond_559
    const-string v13, "PlaybackControl"

    #@55b
    invoke-virtual {v13, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@55e
    move-result v13

    #@55f
    if-eqz v13, :cond_5a2

    #@561
    .line 330
    new-instance v2, Landroid/os/Bundle;

    #@563
    const/4 v13, 0x3

    #@564
    invoke-direct {v2, v13}, Landroid/os/Bundle;-><init>(I)V

    #@567
    .line 331
    .restart local v2       #b:Landroid/os/Bundle;
    const-string v13, "event"

    #@569
    invoke-virtual {v2, v13, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@56c
    .line 332
    const-string v14, "cmd"

    #@56e
    const/4 v13, 0x1

    #@56f
    aget-object v13, p1, v13

    #@571
    check-cast v13, Ljava/lang/String;

    #@573
    invoke-virtual {v2, v14, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@576
    .line 333
    const-string v14, "status"

    #@578
    const/4 v13, 0x2

    #@579
    aget-object v13, p1, v13

    #@57b
    check-cast v13, Ljava/lang/String;

    #@57d
    invoke-virtual {v2, v14, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@580
    .line 334
    move-object/from16 v0, p1

    #@582
    array-length v13, v0

    #@583
    const/4 v14, 0x5

    #@584
    if-lt v13, v14, :cond_59a

    #@586
    .line 335
    const-string v14, "bufferLength"

    #@588
    const/4 v13, 0x3

    #@589
    aget-object v13, p1, v13

    #@58b
    check-cast v13, Ljava/lang/String;

    #@58d
    invoke-virtual {v2, v14, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@590
    .line 336
    const-string v14, "windowSize"

    #@592
    const/4 v13, 0x4

    #@593
    aget-object v13, p1, v13

    #@595
    check-cast v13, Ljava/lang/String;

    #@597
    invoke-virtual {v2, v14, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@59a
    .line 338
    :cond_59a
    sget-object v13, Lcom/qualcomm/wfd/WFDNative;->listener:Lcom/qualcomm/wfd/WFDNative$WfdActionListener;

    #@59c
    const/4 v14, -0x1

    #@59d
    invoke-interface {v13, v2, v14}, Lcom/qualcomm/wfd/WFDNative$WfdActionListener;->notify(Landroid/os/Bundle;I)V

    #@5a0
    goto/16 :goto_12

    #@5a2
    .line 341
    .end local v2           #b:Landroid/os/Bundle;
    :cond_5a2
    const-string v13, "WFDNative"

    #@5a4
    new-instance v14, Ljava/lang/StringBuilder;

    #@5a6
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@5a9
    const-string v15, "Unknown description:"

    #@5ab
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5ae
    move-result-object v14

    #@5af
    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b2
    move-result-object v14

    #@5b3
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b6
    move-result-object v14

    #@5b7
    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5ba
    goto/16 :goto_12

    #@5bc
    .line 345
    .end local v3           #desc:Ljava/lang/String;
    :cond_5bc
    const-string v13, "WFDNative"

    #@5be
    const-string v14, "No description for NetworkEvent"

    #@5c0
    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5c3
    goto/16 :goto_12

    #@5c5
    .line 348
    :cond_5c5
    const-string v13, "LgWfdEvent"

    #@5c7
    move-object/from16 v0, p0

    #@5c9
    invoke-virtual {v13, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@5cc
    move-result v13

    #@5cd
    if-eqz v13, :cond_647

    #@5cf
    .line 349
    move-object/from16 v0, p1

    #@5d1
    array-length v13, v0

    #@5d2
    const/4 v14, 0x1

    #@5d3
    if-lt v13, v14, :cond_63e

    #@5d5
    .line 350
    const/4 v13, 0x0

    #@5d6
    aget-object v3, p1, v13

    #@5d8
    check-cast v3, Ljava/lang/String;

    #@5da
    .line 351
    .restart local v3       #desc:Ljava/lang/String;
    const-string v13, "DLNA_SINK_UDN"

    #@5dc
    invoke-virtual {v13, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@5df
    move-result v13

    #@5e0
    if-eqz v13, :cond_5ff

    #@5e2
    .line 352
    new-instance v2, Landroid/os/Bundle;

    #@5e4
    const/4 v13, 0x2

    #@5e5
    invoke-direct {v2, v13}, Landroid/os/Bundle;-><init>(I)V

    #@5e8
    .line 353
    .restart local v2       #b:Landroid/os/Bundle;
    const-string v13, "event"

    #@5ea
    invoke-virtual {v2, v13, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@5ed
    .line 354
    const-string v14, "sink_udn"

    #@5ef
    const/4 v13, 0x1

    #@5f0
    aget-object v13, p1, v13

    #@5f2
    check-cast v13, Ljava/lang/String;

    #@5f4
    invoke-virtual {v2, v14, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@5f7
    .line 355
    sget-object v13, Lcom/qualcomm/wfd/WFDNative;->listener:Lcom/qualcomm/wfd/WFDNative$WfdActionListener;

    #@5f9
    const/4 v14, -0x1

    #@5fa
    invoke-interface {v13, v2, v14}, Lcom/qualcomm/wfd/WFDNative$WfdActionListener;->notify(Landroid/os/Bundle;I)V

    #@5fd
    goto/16 :goto_12

    #@5ff
    .line 356
    .end local v2           #b:Landroid/os/Bundle;
    :cond_5ff
    const-string v13, "SRC_IP_ADDR"

    #@601
    invoke-virtual {v13, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@604
    move-result v13

    #@605
    if-eqz v13, :cond_624

    #@607
    .line 357
    new-instance v2, Landroid/os/Bundle;

    #@609
    const/4 v13, 0x2

    #@60a
    invoke-direct {v2, v13}, Landroid/os/Bundle;-><init>(I)V

    #@60d
    .line 358
    .restart local v2       #b:Landroid/os/Bundle;
    const-string v13, "event"

    #@60f
    invoke-virtual {v2, v13, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@612
    .line 359
    const-string v14, "src_ip_addr"

    #@614
    const/4 v13, 0x1

    #@615
    aget-object v13, p1, v13

    #@617
    check-cast v13, Ljava/lang/String;

    #@619
    invoke-virtual {v2, v14, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@61c
    .line 360
    sget-object v13, Lcom/qualcomm/wfd/WFDNative;->listener:Lcom/qualcomm/wfd/WFDNative$WfdActionListener;

    #@61e
    const/4 v14, -0x1

    #@61f
    invoke-interface {v13, v2, v14}, Lcom/qualcomm/wfd/WFDNative$WfdActionListener;->notify(Landroid/os/Bundle;I)V

    #@622
    goto/16 :goto_12

    #@624
    .line 362
    .end local v2           #b:Landroid/os/Bundle;
    :cond_624
    const-string v13, "WFDNative"

    #@626
    new-instance v14, Ljava/lang/StringBuilder;

    #@628
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@62b
    const-string v15, "Unknown description:"

    #@62d
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@630
    move-result-object v14

    #@631
    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@634
    move-result-object v14

    #@635
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@638
    move-result-object v14

    #@639
    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@63c
    goto/16 :goto_12

    #@63e
    .line 366
    .end local v3           #desc:Ljava/lang/String;
    :cond_63e
    const-string v13, "WFDNative"

    #@640
    const-string v14, "No description for WfdDlnaEvent"

    #@642
    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@645
    goto/16 :goto_12

    #@647
    .line 370
    :cond_647
    const-string v13, "WFDNative"

    #@649
    new-instance v14, Ljava/lang/StringBuilder;

    #@64b
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@64e
    const-string v15, "Receive unrecognized event from WFDNative.cpp: "

    #@650
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@653
    move-result-object v14

    #@654
    move-object/from16 v0, p0

    #@656
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@659
    move-result-object v14

    #@65a
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65d
    move-result-object v14

    #@65e
    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@661
    goto/16 :goto_12
.end method

.method public static getIpAddress(Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "macAddr"

    #@0
    .prologue
    .line 100
    const-string v1, "0.0.0.0"

    #@2
    .line 101
    .local v1, peerIP:Ljava/lang/String;
    const/4 v0, 0x0

    #@3
    .line 103
    .local v0, devIndex:I
    const-string v2, "WFDNative"

    #@5
    const-string v3, "getIpAddress() called."

    #@7
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 104
    const-string v2, "WFDNative"

    #@c
    new-instance v3, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v4, "peerIP = "

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 105
    if-ltz v0, :cond_2b

    #@24
    .line 106
    const-string v2, "WFDNative"

    #@26
    const-string v3, "ip info in deviceList = "

    #@28
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 108
    :cond_2b
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@2e
    move-result v2

    #@2f
    if-lez v2, :cond_33

    #@31
    if-ltz v0, :cond_33

    #@33
    .line 113
    :cond_33
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@36
    move-result v2

    #@37
    if-lez v2, :cond_3a

    #@39
    .line 119
    .end local v1           #peerIP:Ljava/lang/String;
    :cond_39
    :goto_39
    return-object v1

    #@3a
    .line 115
    .restart local v1       #peerIP:Ljava/lang/String;
    :cond_3a
    if-gez v0, :cond_39

    #@3c
    .line 119
    const-string v1, ""

    #@3e
    goto :goto_39
.end method

.method public static getLinkSpeed()I
    .registers 1

    #@0
    .prologue
    .line 96
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public static getLocalIpAddress()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 88
    sget-object v0, Lcom/qualcomm/wfd/WFDNative;->localIpAddress:Ljava/lang/String;

    #@2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_c

    #@8
    .line 89
    const-string v0, "0.0.0.0"

    #@a
    sput-object v0, Lcom/qualcomm/wfd/WFDNative;->localIpAddress:Ljava/lang/String;

    #@c
    .line 91
    :cond_c
    sget-object v0, Lcom/qualcomm/wfd/WFDNative;->localIpAddress:Ljava/lang/String;

    #@e
    return-object v0
.end method

.method public static getWfdDeviceCoupleSinkStatusBitmap(Ljava/lang/String;)I
    .registers 2
    .parameter "macAddr"

    #@0
    .prologue
    .line 143
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public static getWfdDeviceInfoBitmap(Ljava/lang/String;)J
    .registers 3
    .parameter "macAddr"

    #@0
    .prologue
    .line 125
    const-wide/16 v0, 0x0

    #@2
    return-wide v0
.end method

.method public static getWfdDeviceMaxThroughput(Ljava/lang/String;)I
    .registers 2
    .parameter "macAddr"

    #@0
    .prologue
    .line 137
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public static getWfdDeviceRtspPort(Ljava/lang/String;)I
    .registers 2
    .parameter "macAddr"

    #@0
    .prologue
    .line 131
    const/16 v0, 0x356

    #@2
    return v0
.end method

.method public static initUIBC()V
	.registers 0
	return-void
.end method

.method public static negotiateRtpTransport(III)V
	.registers 3
	return-void
.end method

.method public static pause(IZ)V
	.registers 2
	return-void
.end method

.method public static pause_transit(I)V
	.registers 1
	return-void
.end method

.method public static play(IZ)V
	.registers 2
	return-void
.end method

.method public static play_transit(I)V
	.registers 1
	return-void
.end method

.method public static sendUIBCKeyEvent(Landroid/view/KeyEvent;)V
	.registers 1
	return-void
.end method

.method public static sendUIBCMotionEvent(Landroid/view/MotionEvent;)V
	.registers 1
	return-void
.end method

.method public static setAvPlaybackMode(I)Z
	.registers 1
	return-void
.end method

.method public static setBitrate(I)V
	.registers 1
	return-void
.end method

.method public static setResolution(II[I)Z
	.registers 4
	const/4 v0, 0x1
	return v0
.end method

.method public static setRtpTransport(I)V
	.registers 1
	return-void
.end method

.method public static setSurfaceProp(III)V
	.registers 3
	return-void
.end method

.method public static setVideoSurface(Landroid/view/Surface;)V
	.registers 1
	return-void
.end method

.method public static standby(I)Z
	.registers 2
	const/4 v0, 0x1
	return v0
.end method

.method public static startUIBC(Ljava/lang/Object;)V
	.registers 1
	return-void
.end method

.method public static startWfdSession(Lcom/qualcomm/wfd/WfdDevice;)V
	.registers 1
	return-void
.end method

.method public static stopUIBC()V
	.registers 0
	return-void
.end method

.method public static stopWfdSession(I)V
	.registers 1
	return-void
.end method

.method public static tcpPlaybackControl(I)V
	.registers 1
	return-void
.end method

.method public static teardown(IZ)V
	.registers 2
	return-void
.end method
