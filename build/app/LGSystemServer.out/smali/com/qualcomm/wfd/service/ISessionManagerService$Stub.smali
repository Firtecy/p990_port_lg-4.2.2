.class public abstract Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;
.super Landroid/os/Binder;
.source "ISessionManagerService.java"

# interfaces
.implements Lcom/qualcomm/wfd/service/ISessionManagerService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/service/ISessionManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/wfd/service/ISessionManagerService$Stub$Proxy;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 44
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 45
    const-string v0, "com.qualcomm.wfd.service.ISessionManagerService"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 46
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/qualcomm/wfd/service/ISessionManagerService;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 53
    if-nez p0, :cond_4

    #@2
    .line 54
    const/4 v0, 0x0

    #@3
    .line 60
    :goto_3
    return-object v0

    #@4
    .line 56
    :cond_4
    const-string v1, "com.qualcomm.wfd.service.ISessionManagerService"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 57
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 58
    check-cast v0, Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@12
    goto :goto_3

    #@13
    .line 60
    :cond_13
    new-instance v0, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 64
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 13
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    .line 68
    sparse-switch p1, :sswitch_data_290

    #@5
    .line 379
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v5

    #@9
    :goto_9
    return v5

    #@a
    .line 72
    :sswitch_a
    const-string v6, "com.qualcomm.wfd.service.ISessionManagerService"

    #@c
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 77
    :sswitch_10
    const-string v6, "com.qualcomm.wfd.service.ISessionManagerService"

    #@12
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 79
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@18
    move-result v0

    #@19
    .line 80
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->setDeviceType(I)I

    #@1c
    move-result v4

    #@1d
    .line 81
    .local v4, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@20
    .line 82
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@23
    goto :goto_9

    #@24
    .line 87
    .end local v0           #_arg0:I
    .end local v4           #_result:I
    :sswitch_24
    const-string v6, "com.qualcomm.wfd.service.ISessionManagerService"

    #@26
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@29
    .line 89
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2c
    move-result v1

    #@2d
    .line 90
    .local v1, _arg0_length:I
    if-gez v1, :cond_3e

    #@2f
    .line 91
    const/4 v0, 0x0

    #@30
    .line 96
    .local v0, _arg0:[I
    :goto_30
    invoke-virtual {p0, v0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->getSupportedTypes([I)I

    #@33
    move-result v4

    #@34
    .line 97
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@37
    .line 98
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@3a
    .line 99
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    #@3d
    goto :goto_9

    #@3e
    .line 94
    .end local v0           #_arg0:[I
    .end local v4           #_result:I
    :cond_3e
    new-array v0, v1, [I

    #@40
    .restart local v0       #_arg0:[I
    goto :goto_30

    #@41
    .line 104
    .end local v0           #_arg0:[I
    .end local v1           #_arg0_length:I
    :sswitch_41
    const-string v6, "com.qualcomm.wfd.service.ISessionManagerService"

    #@43
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@46
    .line 105
    invoke-virtual {p0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->getStatus()Lcom/qualcomm/wfd/WfdStatus;

    #@49
    move-result-object v4

    #@4a
    .line 106
    .local v4, _result:Lcom/qualcomm/wfd/WfdStatus;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4d
    .line 107
    if-eqz v4, :cond_56

    #@4f
    .line 108
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@52
    .line 109
    invoke-virtual {v4, p3, v5}, Lcom/qualcomm/wfd/WfdStatus;->writeToParcel(Landroid/os/Parcel;I)V

    #@55
    goto :goto_9

    #@56
    .line 112
    :cond_56
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@59
    goto :goto_9

    #@5a
    .line 118
    .end local v4           #_result:Lcom/qualcomm/wfd/WfdStatus;
    :sswitch_5a
    const-string v6, "com.qualcomm.wfd.service.ISessionManagerService"

    #@5c
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5f
    .line 120
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@62
    move-result-object v6

    #@63
    invoke-static {v6}, Lcom/qualcomm/wfd/service/IWfdActionListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/qualcomm/wfd/service/IWfdActionListener;

    #@66
    move-result-object v0

    #@67
    .line 122
    .local v0, _arg0:Lcom/qualcomm/wfd/service/IWfdActionListener;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@6a
    move-result v6

    #@6b
    if-eqz v6, :cond_80

    #@6d
    .line 123
    sget-object v6, Lcom/qualcomm/wfd/WfdDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@6f
    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@72
    move-result-object v2

    #@73
    check-cast v2, Lcom/qualcomm/wfd/WfdDevice;

    #@75
    .line 128
    .local v2, _arg1:Lcom/qualcomm/wfd/WfdDevice;
    :goto_75
    invoke-virtual {p0, v0, v2}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->init(Lcom/qualcomm/wfd/service/IWfdActionListener;Lcom/qualcomm/wfd/WfdDevice;)I

    #@78
    move-result v4

    #@79
    .line 129
    .local v4, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@7c
    .line 130
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@7f
    goto :goto_9

    #@80
    .line 126
    .end local v2           #_arg1:Lcom/qualcomm/wfd/WfdDevice;
    .end local v4           #_result:I
    :cond_80
    const/4 v2, 0x0

    #@81
    .restart local v2       #_arg1:Lcom/qualcomm/wfd/WfdDevice;
    goto :goto_75

    #@82
    .line 135
    .end local v0           #_arg0:Lcom/qualcomm/wfd/service/IWfdActionListener;
    .end local v2           #_arg1:Lcom/qualcomm/wfd/WfdDevice;
    :sswitch_82
    const-string v6, "com.qualcomm.wfd.service.ISessionManagerService"

    #@84
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@87
    .line 137
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@8a
    move-result-object v6

    #@8b
    invoke-static {v6}, Lcom/qualcomm/wfd/service/IWfdActionListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/qualcomm/wfd/service/IWfdActionListener;

    #@8e
    move-result-object v0

    #@8f
    .line 138
    .restart local v0       #_arg0:Lcom/qualcomm/wfd/service/IWfdActionListener;
    invoke-virtual {p0, v0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->unregisterListener(Lcom/qualcomm/wfd/service/IWfdActionListener;)I

    #@92
    move-result v4

    #@93
    .line 139
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@96
    .line 140
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@99
    goto/16 :goto_9

    #@9b
    .line 145
    .end local v0           #_arg0:Lcom/qualcomm/wfd/service/IWfdActionListener;
    .end local v4           #_result:I
    :sswitch_9b
    const-string v6, "com.qualcomm.wfd.service.ISessionManagerService"

    #@9d
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a0
    .line 146
    invoke-virtual {p0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->deinit()I

    #@a3
    move-result v4

    #@a4
    .line 147
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@a7
    .line 148
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@aa
    goto/16 :goto_9

    #@ac
    .line 153
    .end local v4           #_result:I
    :sswitch_ac
    const-string v6, "com.qualcomm.wfd.service.ISessionManagerService"

    #@ae
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b1
    .line 155
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@b4
    move-result v6

    #@b5
    if-eqz v6, :cond_cb

    #@b7
    .line 156
    sget-object v6, Lcom/qualcomm/wfd/WfdDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@b9
    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@bc
    move-result-object v0

    #@bd
    check-cast v0, Lcom/qualcomm/wfd/WfdDevice;

    #@bf
    .line 161
    .local v0, _arg0:Lcom/qualcomm/wfd/WfdDevice;
    :goto_bf
    invoke-virtual {p0, v0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->startWfdSession(Lcom/qualcomm/wfd/WfdDevice;)I

    #@c2
    move-result v4

    #@c3
    .line 162
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@c6
    .line 163
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@c9
    goto/16 :goto_9

    #@cb
    .line 159
    .end local v0           #_arg0:Lcom/qualcomm/wfd/WfdDevice;
    .end local v4           #_result:I
    :cond_cb
    const/4 v0, 0x0

    #@cc
    .restart local v0       #_arg0:Lcom/qualcomm/wfd/WfdDevice;
    goto :goto_bf

    #@cd
    .line 168
    .end local v0           #_arg0:Lcom/qualcomm/wfd/WfdDevice;
    :sswitch_cd
    const-string v6, "com.qualcomm.wfd.service.ISessionManagerService"

    #@cf
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d2
    .line 169
    invoke-virtual {p0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->stopWfdSession()I

    #@d5
    move-result v4

    #@d6
    .line 170
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@d9
    .line 171
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@dc
    goto/16 :goto_9

    #@de
    .line 176
    .end local v4           #_result:I
    :sswitch_de
    const-string v6, "com.qualcomm.wfd.service.ISessionManagerService"

    #@e0
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@e3
    .line 178
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@e6
    move-result v0

    #@e7
    .line 180
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@ea
    move-result v2

    #@eb
    .line 181
    .local v2, _arg1:I
    invoke-virtual {p0, v0, v2}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->setResolution(II)I

    #@ee
    move-result v4

    #@ef
    .line 182
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@f2
    .line 183
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@f5
    goto/16 :goto_9

    #@f7
    .line 188
    .end local v0           #_arg0:I
    .end local v2           #_arg1:I
    .end local v4           #_result:I
    :sswitch_f7
    const-string v6, "com.qualcomm.wfd.service.ISessionManagerService"

    #@f9
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@fc
    .line 190
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@ff
    move-result v0

    #@100
    .line 191
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->setBitrate(I)I

    #@103
    move-result v4

    #@104
    .line 192
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@107
    .line 193
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@10a
    goto/16 :goto_9

    #@10c
    .line 198
    .end local v0           #_arg0:I
    .end local v4           #_result:I
    :sswitch_10c
    const-string v6, "com.qualcomm.wfd.service.ISessionManagerService"

    #@10e
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@111
    .line 200
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@114
    move-result v0

    #@115
    .line 202
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@118
    move-result v2

    #@119
    .line 204
    .restart local v2       #_arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@11c
    move-result v3

    #@11d
    .line 205
    .local v3, _arg2:I
    invoke-virtual {p0, v0, v2, v3}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->setRtpTransport(III)I

    #@120
    move-result v4

    #@121
    .line 206
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@124
    .line 207
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@127
    goto/16 :goto_9

    #@129
    .line 212
    .end local v0           #_arg0:I
    .end local v2           #_arg1:I
    .end local v3           #_arg2:I
    .end local v4           #_result:I
    :sswitch_129
    const-string v6, "com.qualcomm.wfd.service.ISessionManagerService"

    #@12b
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@12e
    .line 214
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@131
    move-result v0

    #@132
    .line 215
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->tcpPlaybackControl(I)I

    #@135
    move-result v4

    #@136
    .line 216
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@139
    .line 217
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@13c
    goto/16 :goto_9

    #@13e
    .line 222
    .end local v0           #_arg0:I
    .end local v4           #_result:I
    :sswitch_13e
    const-string v6, "com.qualcomm.wfd.service.ISessionManagerService"

    #@140
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@143
    .line 224
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@146
    move-result v0

    #@147
    .line 225
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->setAvPlaybackMode(I)I

    #@14a
    move-result v4

    #@14b
    .line 226
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@14e
    .line 227
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@151
    goto/16 :goto_9

    #@153
    .line 232
    .end local v0           #_arg0:I
    .end local v4           #_result:I
    :sswitch_153
    const-string v6, "com.qualcomm.wfd.service.ISessionManagerService"

    #@155
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@158
    .line 233
    invoke-virtual {p0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->play()I

    #@15b
    move-result v4

    #@15c
    .line 234
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@15f
    .line 235
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@162
    goto/16 :goto_9

    #@164
    .line 240
    .end local v4           #_result:I
    :sswitch_164
    const-string v6, "com.qualcomm.wfd.service.ISessionManagerService"

    #@166
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@169
    .line 241
    invoke-virtual {p0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->pause()I

    #@16c
    move-result v4

    #@16d
    .line 242
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@170
    .line 243
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@173
    goto/16 :goto_9

    #@175
    .line 248
    .end local v4           #_result:I
    :sswitch_175
    const-string v6, "com.qualcomm.wfd.service.ISessionManagerService"

    #@177
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@17a
    .line 249
    invoke-virtual {p0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->play_transit()I

    #@17d
    move-result v4

    #@17e
    .line 250
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@181
    .line 251
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@184
    goto/16 :goto_9

    #@186
    .line 256
    .end local v4           #_result:I
    :sswitch_186
    const-string v6, "com.qualcomm.wfd.service.ISessionManagerService"

    #@188
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@18b
    .line 257
    invoke-virtual {p0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->pause_transit()I

    #@18e
    move-result v4

    #@18f
    .line 258
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@192
    .line 259
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@195
    goto/16 :goto_9

    #@197
    .line 264
    .end local v4           #_result:I
    :sswitch_197
    const-string v6, "com.qualcomm.wfd.service.ISessionManagerService"

    #@199
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@19c
    .line 265
    invoke-virtual {p0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->teardown()I

    #@19f
    move-result v4

    #@1a0
    .line 266
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1a3
    .line 267
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1a6
    goto/16 :goto_9

    #@1a8
    .line 272
    .end local v4           #_result:I
    :sswitch_1a8
    const-string v6, "com.qualcomm.wfd.service.ISessionManagerService"

    #@1aa
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1ad
    .line 273
    invoke-virtual {p0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->standby()I

    #@1b0
    move-result v4

    #@1b1
    .line 274
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1b4
    .line 275
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1b7
    goto/16 :goto_9

    #@1b9
    .line 280
    .end local v4           #_result:I
    :sswitch_1b9
    const-string v6, "com.qualcomm.wfd.service.ISessionManagerService"

    #@1bb
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1be
    .line 281
    invoke-virtual {p0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->startUibcSession()I

    #@1c1
    move-result v4

    #@1c2
    .line 282
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1c5
    .line 283
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1c8
    goto/16 :goto_9

    #@1ca
    .line 288
    .end local v4           #_result:I
    :sswitch_1ca
    const-string v6, "com.qualcomm.wfd.service.ISessionManagerService"

    #@1cc
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1cf
    .line 289
    invoke-virtual {p0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->stopUibcSession()I

    #@1d2
    move-result v4

    #@1d3
    .line 290
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1d6
    .line 291
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1d9
    goto/16 :goto_9

    #@1db
    .line 296
    .end local v4           #_result:I
    :sswitch_1db
    const-string v6, "com.qualcomm.wfd.service.ISessionManagerService"

    #@1dd
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1e0
    .line 298
    new-instance v0, Landroid/os/Bundle;

    #@1e2
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@1e5
    .line 299
    .local v0, _arg0:Landroid/os/Bundle;
    invoke-virtual {p0, v0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->getCommonCapabilities(Landroid/os/Bundle;)I

    #@1e8
    move-result v4

    #@1e9
    .line 300
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1ec
    .line 301
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1ef
    .line 302
    if-eqz v0, :cond_1f9

    #@1f1
    .line 303
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@1f4
    .line 304
    invoke-virtual {v0, p3, v5}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@1f7
    goto/16 :goto_9

    #@1f9
    .line 307
    :cond_1f9
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@1fc
    goto/16 :goto_9

    #@1fe
    .line 313
    .end local v0           #_arg0:Landroid/os/Bundle;
    .end local v4           #_result:I
    :sswitch_1fe
    const-string v6, "com.qualcomm.wfd.service.ISessionManagerService"

    #@200
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@203
    .line 315
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@206
    move-result v6

    #@207
    if-eqz v6, :cond_21d

    #@209
    .line 316
    sget-object v6, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@20b
    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@20e
    move-result-object v0

    #@20f
    check-cast v0, Landroid/os/Bundle;

    #@211
    .line 321
    .restart local v0       #_arg0:Landroid/os/Bundle;
    :goto_211
    invoke-virtual {p0, v0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->setNegotiatedCapabilities(Landroid/os/Bundle;)I

    #@214
    move-result v4

    #@215
    .line 322
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@218
    .line 323
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@21b
    goto/16 :goto_9

    #@21d
    .line 319
    .end local v0           #_arg0:Landroid/os/Bundle;
    .end local v4           #_result:I
    :cond_21d
    const/4 v0, 0x0

    #@21e
    .restart local v0       #_arg0:Landroid/os/Bundle;
    goto :goto_211

    #@21f
    .line 328
    .end local v0           #_arg0:Landroid/os/Bundle;
    :sswitch_21f
    const-string v6, "com.qualcomm.wfd.service.ISessionManagerService"

    #@221
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@224
    .line 329
    invoke-virtual {p0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->completeCapabilityNegotiation()I

    #@227
    move-result v4

    #@228
    .line 330
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@22b
    .line 331
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@22e
    goto/16 :goto_9

    #@230
    .line 336
    .end local v4           #_result:I
    :sswitch_230
    const-string v6, "com.qualcomm.wfd.service.ISessionManagerService"

    #@232
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@235
    .line 338
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@238
    move-result v6

    #@239
    if-eqz v6, :cond_24f

    #@23b
    .line 339
    sget-object v6, Landroid/view/Surface;->CREATOR:Landroid/os/Parcelable$Creator;

    #@23d
    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@240
    move-result-object v0

    #@241
    check-cast v0, Landroid/view/Surface;

    #@243
    .line 344
    .local v0, _arg0:Landroid/view/Surface;
    :goto_243
    invoke-virtual {p0, v0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->setSurface(Landroid/view/Surface;)I

    #@246
    move-result v4

    #@247
    .line 345
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@24a
    .line 346
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@24d
    goto/16 :goto_9

    #@24f
    .line 342
    .end local v0           #_arg0:Landroid/view/Surface;
    .end local v4           #_result:I
    :cond_24f
    const/4 v0, 0x0

    #@250
    .restart local v0       #_arg0:Landroid/view/Surface;
    goto :goto_243

    #@251
    .line 351
    .end local v0           #_arg0:Landroid/view/Surface;
    :sswitch_251
    const-string v6, "com.qualcomm.wfd.service.ISessionManagerService"

    #@253
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@256
    .line 353
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@259
    move-result v6

    #@25a
    if-eqz v6, :cond_270

    #@25c
    .line 354
    sget-object v6, Landroid/view/InputEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@25e
    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@261
    move-result-object v0

    #@262
    check-cast v0, Landroid/view/InputEvent;

    #@264
    .line 359
    .local v0, _arg0:Landroid/view/InputEvent;
    :goto_264
    invoke-virtual {p0, v0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->sendEvent(Landroid/view/InputEvent;)I

    #@267
    move-result v4

    #@268
    .line 360
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@26b
    .line 361
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@26e
    goto/16 :goto_9

    #@270
    .line 357
    .end local v0           #_arg0:Landroid/view/InputEvent;
    .end local v4           #_result:I
    :cond_270
    const/4 v0, 0x0

    #@271
    .restart local v0       #_arg0:Landroid/view/InputEvent;
    goto :goto_264

    #@272
    .line 366
    .end local v0           #_arg0:Landroid/view/InputEvent;
    :sswitch_272
    const-string v6, "com.qualcomm.wfd.service.ISessionManagerService"

    #@274
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@277
    .line 368
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@27a
    move-result v0

    #@27b
    .line 370
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@27e
    move-result v2

    #@27f
    .line 372
    .restart local v2       #_arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@282
    move-result v3

    #@283
    .line 373
    .restart local v3       #_arg2:I
    invoke-virtual {p0, v0, v2, v3}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->setSurfaceProp(III)I

    #@286
    move-result v4

    #@287
    .line 374
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@28a
    .line 375
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@28d
    goto/16 :goto_9

    #@28f
    .line 68
    nop

    #@290
    :sswitch_data_290
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_24
        0x3 -> :sswitch_41
        0x4 -> :sswitch_5a
        0x5 -> :sswitch_82
        0x6 -> :sswitch_9b
        0x7 -> :sswitch_ac
        0x8 -> :sswitch_cd
        0x9 -> :sswitch_de
        0xa -> :sswitch_f7
        0xb -> :sswitch_10c
        0xc -> :sswitch_129
        0xd -> :sswitch_13e
        0xe -> :sswitch_153
        0xf -> :sswitch_164
        0x10 -> :sswitch_175
        0x11 -> :sswitch_186
        0x12 -> :sswitch_197
        0x13 -> :sswitch_1a8
        0x14 -> :sswitch_1b9
        0x15 -> :sswitch_1ca
        0x16 -> :sswitch_1db
        0x17 -> :sswitch_1fe
        0x18 -> :sswitch_21f
        0x19 -> :sswitch_230
        0x1a -> :sswitch_251
        0x1b -> :sswitch_272
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
