.class Lcom/qualcomm/wfd/service/IWfdActionListener$Stub$Proxy;
.super Ljava/lang/Object;
.source "IWfdActionListener.java"

# interfaces
.implements Lcom/qualcomm/wfd/service/IWfdActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/service/IWfdActionListener$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 87
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 88
    iput-object p1, p0, Lcom/qualcomm/wfd/service/IWfdActionListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 89
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 92
    iget-object v0, p0, Lcom/qualcomm/wfd/service/IWfdActionListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public notify(Landroid/os/Bundle;I)V
    .registers 8
    .parameter "b"
    .parameter "sessionId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 126
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 128
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.qualcomm.wfd.service.IWfdActionListener"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 129
    if-eqz p1, :cond_22

    #@b
    .line 130
    const/4 v1, 0x1

    #@c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 131
    const/4 v1, 0x0

    #@10
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@13
    .line 136
    :goto_13
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 137
    iget-object v1, p0, Lcom/qualcomm/wfd/service/IWfdActionListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/4 v2, 0x3

    #@19
    const/4 v3, 0x0

    #@1a
    const/4 v4, 0x1

    #@1b
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1e
    .catchall {:try_start_4 .. :try_end_1e} :catchall_27

    #@1e
    .line 140
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 142
    return-void

    #@22
    .line 134
    :cond_22
    const/4 v1, 0x0

    #@23
    :try_start_23
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_26
    .catchall {:try_start_23 .. :try_end_26} :catchall_27

    #@26
    goto :goto_13

    #@27
    .line 140
    :catchall_27
    move-exception v1

    #@28
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    throw v1
.end method

.method public notifyEvent(II)V
    .registers 8
    .parameter "event"
    .parameter "sessionId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 113
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 115
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.qualcomm.wfd.service.IWfdActionListener"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 116
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 117
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 118
    iget-object v1, p0, Lcom/qualcomm/wfd/service/IWfdActionListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@11
    const/4 v2, 0x2

    #@12
    const/4 v3, 0x0

    #@13
    const/4 v4, 0x1

    #@14
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_17
    .catchall {:try_start_4 .. :try_end_17} :catchall_1b

    #@17
    .line 121
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 123
    return-void

    #@1b
    .line 121
    :catchall_1b
    move-exception v1

    #@1c
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1f
    throw v1
.end method

.method public onStateUpdate(II)V
    .registers 8
    .parameter "newState"
    .parameter "sessionId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 100
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 102
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.qualcomm.wfd.service.IWfdActionListener"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 103
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 104
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 105
    iget-object v1, p0, Lcom/qualcomm/wfd/service/IWfdActionListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@11
    const/4 v2, 0x1

    #@12
    const/4 v3, 0x0

    #@13
    const/4 v4, 0x1

    #@14
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_17
    .catchall {:try_start_4 .. :try_end_17} :catchall_1b

    #@17
    .line 108
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 110
    return-void

    #@1b
    .line 108
    :catchall_1b
    move-exception v1

    #@1c
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1f
    throw v1
.end method
