.class Lcom/qualcomm/wfd/service/ISessionManagerService$Stub$Proxy;
.super Ljava/lang/Object;
.source "ISessionManagerService.java"

# interfaces
.implements Lcom/qualcomm/wfd/service/ISessionManagerService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 385
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 386
    iput-object p1, p0, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 387
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 390
    iget-object v0, p0, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public completeCapabilityNegotiation()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1007
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1008
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1011
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.qualcomm.wfd.service.ISessionManagerService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1012
    iget-object v3, p0, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x18

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 1013
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 1014
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_23

    #@1b
    move-result v2

    #@1c
    .line 1017
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 1018
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 1020
    return v2

    #@23
    .line 1017
    .end local v2           #_result:I
    :catchall_23
    move-exception v3

    #@24
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 1018
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    throw v3
.end method

.method public deinit()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 558
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 559
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 562
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.qualcomm.wfd.service.ISessionManagerService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 563
    iget-object v3, p0, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x6

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 564
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 565
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_22

    #@1a
    move-result v2

    #@1b
    .line 568
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 569
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 571
    return v2

    #@22
    .line 568
    .end local v2           #_result:I
    :catchall_22
    move-exception v3

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 569
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v3
.end method

.method public getCommonCapabilities(Landroid/os/Bundle;)I
    .registers 8
    .parameter "capabilities"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 954
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 955
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 958
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.qualcomm.wfd.service.ISessionManagerService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 959
    iget-object v3, p0, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x16

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 960
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 961
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v2

    #@1c
    .line 962
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1f
    move-result v3

    #@20
    if-eqz v3, :cond_25

    #@22
    .line 963
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_25
    .catchall {:try_start_8 .. :try_end_25} :catchall_2c

    #@25
    .line 967
    :cond_25
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 968
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 970
    return v2

    #@2c
    .line 967
    .end local v2           #_result:I
    :catchall_2c
    move-exception v3

    #@2d
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 968
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@33
    throw v3
.end method

.method public getStatus()Lcom/qualcomm/wfd/WfdStatus;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 465
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 466
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 469
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.qualcomm.wfd.service.ISessionManagerService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 470
    iget-object v3, p0, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x3

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 471
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 472
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1a
    move-result v3

    #@1b
    if-eqz v3, :cond_2c

    #@1d
    .line 473
    sget-object v3, Lcom/qualcomm/wfd/WfdStatus;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1f
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@22
    move-result-object v2

    #@23
    check-cast v2, Lcom/qualcomm/wfd/WfdStatus;
    :try_end_25
    .catchall {:try_start_8 .. :try_end_25} :catchall_2e

    #@25
    .line 480
    .local v2, _result:Lcom/qualcomm/wfd/WfdStatus;
    :goto_25
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 481
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 483
    return-object v2

    #@2c
    .line 476
    .end local v2           #_result:Lcom/qualcomm/wfd/WfdStatus;
    :cond_2c
    const/4 v2, 0x0

    #@2d
    .restart local v2       #_result:Lcom/qualcomm/wfd/WfdStatus;
    goto :goto_25

    #@2e
    .line 480
    .end local v2           #_result:Lcom/qualcomm/wfd/WfdStatus;
    :catchall_2e
    move-exception v3

    #@2f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 481
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@35
    throw v3
.end method

.method public getSupportedTypes([I)I
    .registers 8
    .parameter "types"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 436
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 437
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 440
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.qualcomm.wfd.service.ISessionManagerService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 441
    if-nez p1, :cond_2b

    #@f
    .line 442
    const/4 v3, -0x1

    #@10
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 447
    :goto_13
    iget-object v3, p0, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/4 v4, 0x2

    #@16
    const/4 v5, 0x0

    #@17
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1a
    .line 448
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1d
    .line 449
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@20
    move-result v2

    #@21
    .line 450
    .local v2, _result:I
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->readIntArray([I)V
    :try_end_24
    .catchall {:try_start_8 .. :try_end_24} :catchall_30

    #@24
    .line 453
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 454
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 456
    return v2

    #@2b
    .line 445
    .end local v2           #_result:I
    :cond_2b
    :try_start_2b
    array-length v3, p1

    #@2c
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2f
    .catchall {:try_start_2b .. :try_end_2f} :catchall_30

    #@2f
    goto :goto_13

    #@30
    .line 453
    :catchall_30
    move-exception v3

    #@31
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@34
    .line 454
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@37
    throw v3
.end method

.method public init(Lcom/qualcomm/wfd/service/IWfdActionListener;Lcom/qualcomm/wfd/WfdDevice;)I
    .registers 9
    .parameter "listener"
    .parameter "thisDevice"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 499
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 500
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 503
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.qualcomm.wfd.service.ISessionManagerService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 504
    if-eqz p1, :cond_35

    #@f
    invoke-interface {p1}, Lcom/qualcomm/wfd/service/IWfdActionListener;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v3

    #@13
    :goto_13
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 505
    if-eqz p2, :cond_37

    #@18
    .line 506
    const/4 v3, 0x1

    #@19
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 507
    const/4 v3, 0x0

    #@1d
    invoke-virtual {p2, v0, v3}, Lcom/qualcomm/wfd/WfdDevice;->writeToParcel(Landroid/os/Parcel;I)V

    #@20
    .line 512
    :goto_20
    iget-object v3, p0, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@22
    const/4 v4, 0x4

    #@23
    const/4 v5, 0x0

    #@24
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@27
    .line 513
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@2a
    .line 514
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_2d
    .catchall {:try_start_8 .. :try_end_2d} :catchall_3c

    #@2d
    move-result v2

    #@2e
    .line 517
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 518
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@34
    .line 520
    return v2

    #@35
    .line 504
    .end local v2           #_result:I
    :cond_35
    const/4 v3, 0x0

    #@36
    goto :goto_13

    #@37
    .line 510
    :cond_37
    const/4 v3, 0x0

    #@38
    :try_start_38
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_3b
    .catchall {:try_start_38 .. :try_end_3b} :catchall_3c

    #@3b
    goto :goto_20

    #@3c
    .line 517
    :catchall_3c
    move-exception v3

    #@3d
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@40
    .line 518
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@43
    throw v3
.end method

.method public pause()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 797
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 798
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 801
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.qualcomm.wfd.service.ISessionManagerService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 802
    iget-object v3, p0, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0xf

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 803
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 804
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_23

    #@1b
    move-result v2

    #@1c
    .line 807
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 808
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 810
    return v2

    #@23
    .line 807
    .end local v2           #_result:I
    :catchall_23
    move-exception v3

    #@24
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 808
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    throw v3
.end method

.method public pause_transit()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 838
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 839
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 842
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.qualcomm.wfd.service.ISessionManagerService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 843
    iget-object v3, p0, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x11

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 844
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 845
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_23

    #@1b
    move-result v2

    #@1c
    .line 848
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 849
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 851
    return v2

    #@23
    .line 848
    .end local v2           #_result:I
    :catchall_23
    move-exception v3

    #@24
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 849
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    throw v3
.end method

.method public play()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 777
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 778
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 781
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.qualcomm.wfd.service.ISessionManagerService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 782
    iget-object v3, p0, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0xe

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 783
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 784
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_23

    #@1b
    move-result v2

    #@1c
    .line 787
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 788
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 790
    return v2

    #@23
    .line 787
    .end local v2           #_result:I
    :catchall_23
    move-exception v3

    #@24
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 788
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    throw v3
.end method

.method public play_transit()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 818
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 819
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 822
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.qualcomm.wfd.service.ISessionManagerService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 823
    iget-object v3, p0, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x10

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 824
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 825
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_23

    #@1b
    move-result v2

    #@1c
    .line 828
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 829
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 831
    return v2

    #@23
    .line 828
    .end local v2           #_result:I
    :catchall_23
    move-exception v3

    #@24
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 829
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    throw v3
.end method

.method public sendEvent(Landroid/view/InputEvent;)I
    .registers 8
    .parameter "event"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1068
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1069
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1072
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.qualcomm.wfd.service.ISessionManagerService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1073
    if-eqz p1, :cond_2d

    #@f
    .line 1074
    const/4 v3, 0x1

    #@10
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 1075
    const/4 v3, 0x0

    #@14
    invoke-virtual {p1, v0, v3}, Landroid/view/InputEvent;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 1080
    :goto_17
    iget-object v3, p0, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/16 v4, 0x1a

    #@1b
    const/4 v5, 0x0

    #@1c
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1f
    .line 1081
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@22
    .line 1082
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_25
    .catchall {:try_start_8 .. :try_end_25} :catchall_32

    #@25
    move-result v2

    #@26
    .line 1085
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 1086
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 1088
    return v2

    #@2d
    .line 1078
    .end local v2           #_result:I
    :cond_2d
    const/4 v3, 0x0

    #@2e
    :try_start_2e
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_31
    .catchall {:try_start_2e .. :try_end_31} :catchall_32

    #@31
    goto :goto_17

    #@32
    .line 1085
    :catchall_32
    move-exception v3

    #@33
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@36
    .line 1086
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@39
    throw v3
.end method

.method public setAvPlaybackMode(I)I
    .registers 8
    .parameter "mode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 754
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 755
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 758
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.qualcomm.wfd.service.ISessionManagerService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 759
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 760
    iget-object v3, p0, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v4, 0xd

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 761
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 762
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1e
    .catchall {:try_start_8 .. :try_end_1e} :catchall_26

    #@1e
    move-result v2

    #@1f
    .line 765
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 766
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 768
    return v2

    #@26
    .line 765
    .end local v2           #_result:I
    :catchall_26
    move-exception v3

    #@27
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 766
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    throw v3
.end method

.method public setBitrate(I)I
    .registers 8
    .parameter "bitrate"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 674
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 675
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 678
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.qualcomm.wfd.service.ISessionManagerService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 679
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 680
    iget-object v3, p0, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v4, 0xa

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 681
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 682
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1e
    .catchall {:try_start_8 .. :try_end_1e} :catchall_26

    #@1e
    move-result v2

    #@1f
    .line 685
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 686
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 688
    return v2

    #@26
    .line 685
    .end local v2           #_result:I
    :catchall_26
    move-exception v3

    #@27
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 686
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    throw v3
.end method

.method public setDeviceType(I)I
    .registers 8
    .parameter "type"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 409
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 410
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 413
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.qualcomm.wfd.service.ISessionManagerService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 414
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 415
    iget-object v3, p0, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v4, 0x1

    #@13
    const/4 v5, 0x0

    #@14
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 416
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1a
    .line 417
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1d
    .catchall {:try_start_8 .. :try_end_1d} :catchall_25

    #@1d
    move-result v2

    #@1e
    .line 420
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 421
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 423
    return v2

    #@25
    .line 420
    .end local v2           #_result:I
    :catchall_25
    move-exception v3

    #@26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 421
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    throw v3
.end method

.method public setNegotiatedCapabilities(Landroid/os/Bundle;)I
    .registers 8
    .parameter "capabilities"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 979
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 980
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 983
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.qualcomm.wfd.service.ISessionManagerService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 984
    if-eqz p1, :cond_2d

    #@f
    .line 985
    const/4 v3, 0x1

    #@10
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 986
    const/4 v3, 0x0

    #@14
    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 991
    :goto_17
    iget-object v3, p0, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/16 v4, 0x17

    #@1b
    const/4 v5, 0x0

    #@1c
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1f
    .line 992
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@22
    .line 993
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_25
    .catchall {:try_start_8 .. :try_end_25} :catchall_32

    #@25
    move-result v2

    #@26
    .line 996
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 997
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 999
    return v2

    #@2d
    .line 989
    .end local v2           #_result:I
    :cond_2d
    const/4 v3, 0x0

    #@2e
    :try_start_2e
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_31
    .catchall {:try_start_2e .. :try_end_31} :catchall_32

    #@31
    goto :goto_17

    #@32
    .line 996
    :catchall_32
    move-exception v3

    #@33
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@36
    .line 997
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@39
    throw v3
.end method

.method public setResolution(II)I
    .registers 9
    .parameter "formatType"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 647
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 648
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 651
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.qualcomm.wfd.service.ISessionManagerService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 652
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 653
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 654
    iget-object v3, p0, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/16 v4, 0x9

    #@17
    const/4 v5, 0x0

    #@18
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 655
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1e
    .line 656
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_21
    .catchall {:try_start_8 .. :try_end_21} :catchall_29

    #@21
    move-result v2

    #@22
    .line 659
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 660
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 662
    return v2

    #@29
    .line 659
    .end local v2           #_result:I
    :catchall_29
    move-exception v3

    #@2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 660
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    throw v3
.end method

.method public setRtpTransport(III)I
    .registers 10
    .parameter "transportType"
    .parameter "bufferLengthMs"
    .parameter "port"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 700
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 701
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 704
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.qualcomm.wfd.service.ISessionManagerService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 705
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 706
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 707
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 708
    iget-object v3, p0, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/16 v4, 0xb

    #@1a
    const/4 v5, 0x0

    #@1b
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 709
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@21
    .line 710
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_24
    .catchall {:try_start_8 .. :try_end_24} :catchall_2c

    #@24
    move-result v2

    #@25
    .line 713
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 714
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 716
    return v2

    #@2c
    .line 713
    .end local v2           #_result:I
    :catchall_2c
    move-exception v3

    #@2d
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 714
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@33
    throw v3
.end method

.method public setSurface(Landroid/view/Surface;)I
    .registers 8
    .parameter "mmSurface"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1036
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1037
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1040
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.qualcomm.wfd.service.ISessionManagerService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1041
    if-eqz p1, :cond_2d

    #@f
    .line 1042
    const/4 v3, 0x1

    #@10
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 1043
    const/4 v3, 0x0

    #@14
    invoke-virtual {p1, v0, v3}, Landroid/view/Surface;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 1048
    :goto_17
    iget-object v3, p0, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/16 v4, 0x19

    #@1b
    const/4 v5, 0x0

    #@1c
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1f
    .line 1049
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@22
    .line 1050
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_25
    .catchall {:try_start_8 .. :try_end_25} :catchall_32

    #@25
    move-result v2

    #@26
    .line 1053
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 1054
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 1056
    return v2

    #@2d
    .line 1046
    .end local v2           #_result:I
    :cond_2d
    const/4 v3, 0x0

    #@2e
    :try_start_2e
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_31
    .catchall {:try_start_2e .. :try_end_31} :catchall_32

    #@31
    goto :goto_17

    #@32
    .line 1053
    :catchall_32
    move-exception v3

    #@33
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@36
    .line 1054
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@39
    throw v3
.end method

.method public setSurfaceProp(III)I
    .registers 10
    .parameter "width"
    .parameter "height"
    .parameter "orientation"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1101
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1102
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1105
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.qualcomm.wfd.service.ISessionManagerService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1106
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 1107
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 1108
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 1109
    iget-object v3, p0, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/16 v4, 0x1b

    #@1a
    const/4 v5, 0x0

    #@1b
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 1110
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@21
    .line 1111
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_24
    .catchall {:try_start_8 .. :try_end_24} :catchall_2c

    #@24
    move-result v2

    #@25
    .line 1114
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 1115
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 1117
    return v2

    #@2c
    .line 1114
    .end local v2           #_result:I
    :catchall_2c
    move-exception v3

    #@2d
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 1115
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@33
    throw v3
.end method

.method public standby()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 880
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 881
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 884
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.qualcomm.wfd.service.ISessionManagerService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 885
    iget-object v3, p0, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x13

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 886
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 887
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_23

    #@1b
    move-result v2

    #@1c
    .line 890
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 891
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 893
    return v2

    #@23
    .line 890
    .end local v2           #_result:I
    :catchall_23
    move-exception v3

    #@24
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 891
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    throw v3
.end method

.method public startUibcSession()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 905
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 906
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 909
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.qualcomm.wfd.service.ISessionManagerService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 910
    iget-object v3, p0, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x14

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 911
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 912
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_23

    #@1b
    move-result v2

    #@1c
    .line 915
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 916
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 918
    return v2

    #@23
    .line 915
    .end local v2           #_result:I
    :catchall_23
    move-exception v3

    #@24
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 916
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    throw v3
.end method

.method public startWfdSession(Lcom/qualcomm/wfd/WfdDevice;)I
    .registers 8
    .parameter "device"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 585
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 586
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 589
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.qualcomm.wfd.service.ISessionManagerService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 590
    if-eqz p1, :cond_2c

    #@f
    .line 591
    const/4 v3, 0x1

    #@10
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 592
    const/4 v3, 0x0

    #@14
    invoke-virtual {p1, v0, v3}, Lcom/qualcomm/wfd/WfdDevice;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 597
    :goto_17
    iget-object v3, p0, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/4 v4, 0x7

    #@1a
    const/4 v5, 0x0

    #@1b
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 598
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@21
    .line 599
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_24
    .catchall {:try_start_8 .. :try_end_24} :catchall_31

    #@24
    move-result v2

    #@25
    .line 602
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 603
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 605
    return v2

    #@2c
    .line 595
    .end local v2           #_result:I
    :cond_2c
    const/4 v3, 0x0

    #@2d
    :try_start_2d
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_30
    .catchall {:try_start_2d .. :try_end_30} :catchall_31

    #@30
    goto :goto_17

    #@31
    .line 602
    :catchall_31
    move-exception v3

    #@32
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 603
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@38
    throw v3
.end method

.method public stopUibcSession()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 930
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 931
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 934
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.qualcomm.wfd.service.ISessionManagerService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 935
    iget-object v3, p0, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x15

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 936
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 937
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_23

    #@1b
    move-result v2

    #@1c
    .line 940
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 941
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 943
    return v2

    #@23
    .line 940
    .end local v2           #_result:I
    :catchall_23
    move-exception v3

    #@24
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 941
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    throw v3
.end method

.method public stopWfdSession()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 618
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 619
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 622
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.qualcomm.wfd.service.ISessionManagerService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 623
    iget-object v3, p0, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x8

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 624
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 625
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_23

    #@1b
    move-result v2

    #@1c
    .line 628
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 629
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 631
    return v2

    #@23
    .line 628
    .end local v2           #_result:I
    :catchall_23
    move-exception v3

    #@24
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 629
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    throw v3
.end method

.method public tcpPlaybackControl(I)I
    .registers 8
    .parameter "cmdType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 728
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 729
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 732
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.qualcomm.wfd.service.ISessionManagerService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 733
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 734
    iget-object v3, p0, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v4, 0xc

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 735
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 736
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1e
    .catchall {:try_start_8 .. :try_end_1e} :catchall_26

    #@1e
    move-result v2

    #@1f
    .line 739
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 740
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 742
    return v2

    #@26
    .line 739
    .end local v2           #_result:I
    :catchall_26
    move-exception v3

    #@27
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 740
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    throw v3
.end method

.method public teardown()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 860
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 861
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 864
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.qualcomm.wfd.service.ISessionManagerService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 865
    iget-object v3, p0, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x12

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 866
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 867
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_23

    #@1b
    move-result v2

    #@1c
    .line 870
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 871
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 873
    return v2

    #@23
    .line 870
    .end local v2           #_result:I
    :catchall_23
    move-exception v3

    #@24
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 871
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    throw v3
.end method

.method public unregisterListener(Lcom/qualcomm/wfd/service/IWfdActionListener;)I
    .registers 8
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 532
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 533
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 536
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.qualcomm.wfd.service.ISessionManagerService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 537
    if-eqz p1, :cond_2b

    #@f
    invoke-interface {p1}, Lcom/qualcomm/wfd/service/IWfdActionListener;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v3

    #@13
    :goto_13
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 538
    iget-object v3, p0, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/4 v4, 0x5

    #@19
    const/4 v5, 0x0

    #@1a
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1d
    .line 539
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@20
    .line 540
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_23
    .catchall {:try_start_8 .. :try_end_23} :catchall_2d

    #@23
    move-result v2

    #@24
    .line 543
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 544
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 546
    return v2

    #@2b
    .line 537
    .end local v2           #_result:I
    :cond_2b
    const/4 v3, 0x0

    #@2c
    goto :goto_13

    #@2d
    .line 543
    :catchall_2d
    move-exception v3

    #@2e
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 544
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@34
    throw v3
.end method
