.class Lcom/qualcomm/wfd/service/SessionManagerService$1;
.super Landroid/content/BroadcastReceiver;
.source "SessionManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qualcomm/wfd/service/SessionManagerService;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/wfd/service/SessionManagerService;


# direct methods
.method constructor <init>(Lcom/qualcomm/wfd/service/SessionManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 125
    iput-object p1, p0, Lcom/qualcomm/wfd/service/SessionManagerService$1;->this$0:Lcom/qualcomm/wfd/service/SessionManagerService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 25
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 127
    const-string v18, "SessionManagerService"

    #@2
    new-instance v19, Ljava/lang/StringBuilder;

    #@4
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v20, "Received intent: "

    #@9
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v19

    #@d
    const/16 v20, 0x0

    #@f
    move-object/from16 v0, p2

    #@11
    move/from16 v1, v20

    #@13
    invoke-virtual {v0, v1}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    #@16
    move-result-object v20

    #@17
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v19

    #@1b
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v19

    #@1f
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 128
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@25
    move-result-object v3

    #@26
    .line 129
    .local v3, action:Ljava/lang/String;
    invoke-static {}, Lcom/qualcomm/wfd/service/SessionManagerService;->access$000()Ljava/lang/Object;

    #@29
    move-result-object v19

    #@2a
    monitor-enter v19

    #@2b
    .line 130
    :try_start_2b
    const-string v18, "android.intent.action.HDMI_PLUGGED"

    #@2d
    move-object/from16 v0, v18

    #@2f
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@32
    move-result v18

    #@33
    if-eqz v18, :cond_60

    #@35
    .line 131
    const-string v18, "state"

    #@37
    const/16 v20, 0x0

    #@39
    move-object/from16 v0, p2

    #@3b
    move-object/from16 v1, v18

    #@3d
    move/from16 v2, v20

    #@3f
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@42
    move-result v18

    #@43
    invoke-static/range {v18 .. v18}, Lcom/qualcomm/wfd/service/SessionManagerService;->access$102(Z)Z

    #@46
    .line 133
    invoke-static {}, Lcom/qualcomm/wfd/service/SessionManagerService;->access$100()Z

    #@49
    move-result v18

    #@4a
    if-eqz v18, :cond_5e

    #@4c
    .line 134
    move-object/from16 v0, p0

    #@4e
    iget-object v0, v0, Lcom/qualcomm/wfd/service/SessionManagerService$1;->this$0:Lcom/qualcomm/wfd/service/SessionManagerService;

    #@50
    move-object/from16 v18, v0

    #@52
    invoke-virtual/range {v18 .. v18}, Lcom/qualcomm/wfd/service/SessionManagerService;->stopWfdSession()I

    #@55
    .line 135
    move-object/from16 v0, p0

    #@57
    iget-object v0, v0, Lcom/qualcomm/wfd/service/SessionManagerService$1;->this$0:Lcom/qualcomm/wfd/service/SessionManagerService;

    #@59
    move-object/from16 v18, v0

    #@5b
    invoke-virtual/range {v18 .. v18}, Lcom/qualcomm/wfd/service/SessionManagerService;->stopUibcSession()I

    #@5e
    .line 241
    :cond_5e
    :goto_5e
    monitor-exit v19

    #@5f
    .line 242
    return-void

    #@60
    .line 137
    :cond_60
    const-string v18, "android.intent.action.SCREEN_OFF"

    #@62
    move-object/from16 v0, v18

    #@64
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@67
    move-result v18

    #@68
    if-eqz v18, :cond_11f

    #@6a
    .line 138
    invoke-static {}, Lcom/qualcomm/wfd/service/SessionManagerService;->access$300()Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@6d
    move-result-object v18

    #@6e
    invoke-static/range {v18 .. v18}, Lcom/qualcomm/wfd/service/SessionManagerService;->access$202(Lcom/qualcomm/wfd/WfdEnums$SessionState;)Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@71
    .line 139
    invoke-static {}, Lcom/qualcomm/wfd/service/SessionManagerService;->access$200()Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@74
    move-result-object v18

    #@75
    sget-object v20, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PLAYING:Lcom/qualcomm/wfd/WfdEnums$SessionState;
    :try_end_77
    .catchall {:try_start_2b .. :try_end_77} :catchall_e3

    #@77
    move-object/from16 v0, v18

    #@79
    move-object/from16 v1, v20

    #@7b
    if-ne v0, v1, :cond_ac

    #@7d
    .line 141
    :try_start_7d
    const-string v18, "SessionManagerService"

    #@7f
    const-string v20, "Waiting to play"

    #@81
    move-object/from16 v0, v18

    #@83
    move-object/from16 v1, v20

    #@85
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@88
    .line 142
    invoke-static {}, Lcom/qualcomm/wfd/service/SessionManagerService;->access$000()Ljava/lang/Object;

    #@8b
    move-result-object v18

    #@8c
    const-wide/16 v20, 0x1388

    #@8e
    move-object/from16 v0, v18

    #@90
    move-wide/from16 v1, v20

    #@92
    invoke-virtual {v0, v1, v2}, Ljava/lang/Object;->wait(J)V
    :try_end_95
    .catchall {:try_start_7d .. :try_end_95} :catchall_e3
    .catch Ljava/lang/InterruptedException; {:try_start_7d .. :try_end_95} :catch_e6

    #@95
    .line 146
    :goto_95
    :try_start_95
    invoke-static {}, Lcom/qualcomm/wfd/service/SessionManagerService;->access$300()Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@98
    move-result-object v18

    #@99
    sget-object v20, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PLAY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@9b
    move-object/from16 v0, v18

    #@9d
    move-object/from16 v1, v20

    #@9f
    if-eq v0, v1, :cond_ac

    #@a1
    .line 147
    const-string v18, "SessionManagerService"

    #@a3
    const-string v20, "Session failed to move to play state"

    #@a5
    move-object/from16 v0, v18

    #@a7
    move-object/from16 v1, v20

    #@a9
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ac
    .line 150
    :cond_ac
    const/16 v18, 0x3

    #@ae
    const/16 v20, 0x0

    #@b0
    move/from16 v0, v18

    #@b2
    move/from16 v1, v20

    #@b4
    invoke-static {v0, v1}, Landroid/media/AudioSystem;->isStreamActive(II)Z

    #@b7
    move-result v18

    #@b8
    if-eqz v18, :cond_109

    #@ba
    .line 151
    move-object/from16 v0, p0

    #@bc
    iget-object v0, v0, Lcom/qualcomm/wfd/service/SessionManagerService$1;->this$0:Lcom/qualcomm/wfd/service/SessionManagerService;

    #@be
    move-object/from16 v18, v0

    #@c0
    const-string v20, "AudioStreamInSuspend"

    #@c2
    move-object/from16 v0, v18

    #@c4
    move-object/from16 v1, v20

    #@c6
    invoke-virtual {v0, v1}, Lcom/qualcomm/wfd/service/SessionManagerService;->getValue(Ljava/lang/String;)Ljava/lang/String;

    #@c9
    move-result-object v18

    #@ca
    const-string v20, "1"

    #@cc
    move-object/from16 v0, v18

    #@ce
    move-object/from16 v1, v20

    #@d0
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@d3
    move-result v18

    #@d4
    if-eqz v18, :cond_f3

    #@d6
    .line 152
    const-string v18, "SessionManagerService"

    #@d8
    const-string v20, "Audio Stream is on so not calling standby"

    #@da
    move-object/from16 v0, v18

    #@dc
    move-object/from16 v1, v20

    #@de
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e1
    goto/16 :goto_5e

    #@e3
    .line 241
    :catchall_e3
    move-exception v18

    #@e4
    monitor-exit v19
    :try_end_e5
    .catchall {:try_start_95 .. :try_end_e5} :catchall_e3

    #@e5
    throw v18

    #@e6
    .line 143
    :catch_e6
    move-exception v9

    #@e7
    .line 144
    .local v9, e:Ljava/lang/InterruptedException;
    :try_start_e7
    const-string v18, "SessionManagerService"

    #@e9
    const-string v20, "Wait for PLAY state interrupted"

    #@eb
    move-object/from16 v0, v18

    #@ed
    move-object/from16 v1, v20

    #@ef
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f2
    goto :goto_95

    #@f3
    .line 154
    .end local v9           #e:Ljava/lang/InterruptedException;
    :cond_f3
    const-string v18, "SessionManagerService"

    #@f5
    const-string v20, "Calling Standby even if Audio Stream is on"

    #@f7
    move-object/from16 v0, v18

    #@f9
    move-object/from16 v1, v20

    #@fb
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@fe
    .line 155
    move-object/from16 v0, p0

    #@100
    iget-object v0, v0, Lcom/qualcomm/wfd/service/SessionManagerService$1;->this$0:Lcom/qualcomm/wfd/service/SessionManagerService;

    #@102
    move-object/from16 v18, v0

    #@104
    invoke-virtual/range {v18 .. v18}, Lcom/qualcomm/wfd/service/SessionManagerService;->standby()I

    #@107
    goto/16 :goto_5e

    #@109
    .line 158
    :cond_109
    const-string v18, "SessionManagerService"

    #@10b
    const-string v20, "Calling Standby as no Audio Stream is playing"

    #@10d
    move-object/from16 v0, v18

    #@10f
    move-object/from16 v1, v20

    #@111
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@114
    .line 159
    move-object/from16 v0, p0

    #@116
    iget-object v0, v0, Lcom/qualcomm/wfd/service/SessionManagerService$1;->this$0:Lcom/qualcomm/wfd/service/SessionManagerService;

    #@118
    move-object/from16 v18, v0

    #@11a
    invoke-virtual/range {v18 .. v18}, Lcom/qualcomm/wfd/service/SessionManagerService;->standby()I

    #@11d
    goto/16 :goto_5e

    #@11f
    .line 161
    :cond_11f
    const-string v18, "android.intent.action.SCREEN_ON"

    #@121
    move-object/from16 v0, v18

    #@123
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@126
    move-result v18

    #@127
    if-eqz v18, :cond_1aa

    #@129
    .line 162
    invoke-static {}, Lcom/qualcomm/wfd/service/SessionManagerService;->access$200()Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@12c
    move-result-object v18

    #@12d
    if-eqz v18, :cond_5e

    #@12f
    invoke-static {}, Lcom/qualcomm/wfd/service/SessionManagerService;->access$200()Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@132
    move-result-object v18

    #@133
    sget-object v20, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PLAY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@135
    move-object/from16 v0, v18

    #@137
    move-object/from16 v1, v20

    #@139
    if-eq v0, v1, :cond_147

    #@13b
    invoke-static {}, Lcom/qualcomm/wfd/service/SessionManagerService;->access$200()Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@13e
    move-result-object v18

    #@13f
    sget-object v20, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PLAYING:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@141
    move-object/from16 v0, v18

    #@143
    move-object/from16 v1, v20

    #@145
    if-ne v0, v1, :cond_5e

    #@147
    .line 164
    :cond_147
    invoke-static {}, Lcom/qualcomm/wfd/service/SessionManagerService;->access$300()Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@14a
    move-result-object v18

    #@14b
    sget-object v20, Lcom/qualcomm/wfd/WfdEnums$SessionState;->STANDING_BY:Lcom/qualcomm/wfd/WfdEnums$SessionState;
    :try_end_14d
    .catchall {:try_start_e7 .. :try_end_14d} :catchall_e3

    #@14d
    move-object/from16 v0, v18

    #@14f
    move-object/from16 v1, v20

    #@151
    if-ne v0, v1, :cond_182

    #@153
    .line 166
    :try_start_153
    const-string v18, "SessionManagerService"

    #@155
    const-string v20, "Waiting to standby"

    #@157
    move-object/from16 v0, v18

    #@159
    move-object/from16 v1, v20

    #@15b
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@15e
    .line 167
    invoke-static {}, Lcom/qualcomm/wfd/service/SessionManagerService;->access$000()Ljava/lang/Object;

    #@161
    move-result-object v18

    #@162
    const-wide/16 v20, 0x1388

    #@164
    move-object/from16 v0, v18

    #@166
    move-wide/from16 v1, v20

    #@168
    invoke-virtual {v0, v1, v2}, Ljava/lang/Object;->wait(J)V
    :try_end_16b
    .catchall {:try_start_153 .. :try_end_16b} :catchall_e3
    .catch Ljava/lang/InterruptedException; {:try_start_153 .. :try_end_16b} :catch_19d

    #@16b
    .line 171
    :goto_16b
    :try_start_16b
    invoke-static {}, Lcom/qualcomm/wfd/service/SessionManagerService;->access$300()Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@16e
    move-result-object v18

    #@16f
    sget-object v20, Lcom/qualcomm/wfd/WfdEnums$SessionState;->STANDBY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@171
    move-object/from16 v0, v18

    #@173
    move-object/from16 v1, v20

    #@175
    if-eq v0, v1, :cond_182

    #@177
    .line 172
    const-string v18, "SessionManagerService"

    #@179
    const-string v20, "Session failed to move to standby state"

    #@17b
    move-object/from16 v0, v18

    #@17d
    move-object/from16 v1, v20

    #@17f
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@182
    .line 175
    :cond_182
    const-string v18, "SessionManagerService"

    #@184
    const-string v20, "Resume session calling play()"

    #@186
    move-object/from16 v0, v18

    #@188
    move-object/from16 v1, v20

    #@18a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18d
    .line 176
    const/16 v18, 0x0

    #@18f
    invoke-static/range {v18 .. v18}, Lcom/qualcomm/wfd/service/SessionManagerService;->access$202(Lcom/qualcomm/wfd/WfdEnums$SessionState;)Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@192
    .line 177
    move-object/from16 v0, p0

    #@194
    iget-object v0, v0, Lcom/qualcomm/wfd/service/SessionManagerService$1;->this$0:Lcom/qualcomm/wfd/service/SessionManagerService;

    #@196
    move-object/from16 v18, v0

    #@198
    invoke-virtual/range {v18 .. v18}, Lcom/qualcomm/wfd/service/SessionManagerService;->play()I

    #@19b
    goto/16 :goto_5e

    #@19d
    .line 168
    :catch_19d
    move-exception v9

    #@19e
    .line 169
    .restart local v9       #e:Ljava/lang/InterruptedException;
    const-string v18, "SessionManagerService"

    #@1a0
    const-string v20, "Wait for standby state interrupted"

    #@1a2
    move-object/from16 v0, v18

    #@1a4
    move-object/from16 v1, v20

    #@1a6
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1a9
    goto :goto_16b

    #@1aa
    .line 179
    .end local v9           #e:Ljava/lang/InterruptedException;
    :cond_1aa
    const-string v18, "android.intent.action.ACTION_SHUTDOWN"

    #@1ac
    move-object/from16 v0, v18

    #@1ae
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b1
    move-result v18

    #@1b2
    if-eqz v18, :cond_1bf

    #@1b4
    .line 180
    move-object/from16 v0, p0

    #@1b6
    iget-object v0, v0, Lcom/qualcomm/wfd/service/SessionManagerService$1;->this$0:Lcom/qualcomm/wfd/service/SessionManagerService;

    #@1b8
    move-object/from16 v18, v0

    #@1ba
    invoke-virtual/range {v18 .. v18}, Lcom/qualcomm/wfd/service/SessionManagerService;->teardown()I

    #@1bd
    goto/16 :goto_5e

    #@1bf
    .line 181
    :cond_1bf
    const-string v18, "qualcomm.intent.action.WIFI_DISPLAY_RESOLUTION"

    #@1c1
    move-object/from16 v0, v18

    #@1c3
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c6
    move-result v18

    #@1c7
    if-eqz v18, :cond_243

    #@1c9
    .line 182
    const-string v18, "format"

    #@1cb
    sget-object v20, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_CEA_RESOLUTIONS_BITMAP:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@1cd
    invoke-virtual/range {v20 .. v20}, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->ordinal()I

    #@1d0
    move-result v20

    #@1d1
    move-object/from16 v0, p2

    #@1d3
    move-object/from16 v1, v18

    #@1d5
    move/from16 v2, v20

    #@1d7
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@1da
    move-result v10

    #@1db
    .line 184
    .local v10, formatType:I
    const-string v18, "value"

    #@1dd
    const/16 v20, 0x0

    #@1df
    move-object/from16 v0, p2

    #@1e1
    move-object/from16 v1, v18

    #@1e3
    move/from16 v2, v20

    #@1e5
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@1e8
    move-result v17

    #@1e9
    .line 185
    .local v17, value:I
    const-string v18, "SessionManagerService"

    #@1eb
    new-instance v20, Ljava/lang/StringBuilder;

    #@1ed
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@1f0
    const-string v21, "Calculated formatType: "

    #@1f2
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f5
    move-result-object v20

    #@1f6
    move-object/from16 v0, v20

    #@1f8
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1fb
    move-result-object v20

    #@1fc
    const-string v21, " value: 0x"

    #@1fe
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@201
    move-result-object v20

    #@202
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@205
    move-result-object v21

    #@206
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@209
    move-result-object v20

    #@20a
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20d
    move-result-object v20

    #@20e
    move-object/from16 v0, v18

    #@210
    move-object/from16 v1, v20

    #@212
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@215
    .line 188
    move-object/from16 v0, p0

    #@217
    iget-object v0, v0, Lcom/qualcomm/wfd/service/SessionManagerService$1;->this$0:Lcom/qualcomm/wfd/service/SessionManagerService;

    #@219
    move-object/from16 v18, v0

    #@21b
    move-object/from16 v0, v18

    #@21d
    move/from16 v1, v17

    #@21f
    invoke-virtual {v0, v10, v1}, Lcom/qualcomm/wfd/service/SessionManagerService;->setResolution(II)I

    #@222
    move-result v14

    #@223
    .line 189
    .local v14, ret:I
    const-string v18, "SessionManagerService"

    #@225
    new-instance v20, Ljava/lang/StringBuilder;

    #@227
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@22a
    const-string v21, "setResoltuion returned: "

    #@22c
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22f
    move-result-object v20

    #@230
    move-object/from16 v0, v20

    #@232
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@235
    move-result-object v20

    #@236
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@239
    move-result-object v20

    #@23a
    move-object/from16 v0, v18

    #@23c
    move-object/from16 v1, v20

    #@23e
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@241
    goto/16 :goto_5e

    #@243
    .line 190
    .end local v10           #formatType:I
    .end local v14           #ret:I
    .end local v17           #value:I
    :cond_243
    const-string v18, "qualcomm.intent.action.WIFI_DISPLAY_BITRATE"

    #@245
    move-object/from16 v0, v18

    #@247
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24a
    move-result v18

    #@24b
    if-eqz v18, :cond_2a5

    #@24d
    .line 191
    const-string v18, "value"

    #@24f
    const/16 v20, 0x0

    #@251
    move-object/from16 v0, p2

    #@253
    move-object/from16 v1, v18

    #@255
    move/from16 v2, v20

    #@257
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@25a
    move-result v4

    #@25b
    .line 192
    .local v4, bitrate:I
    const-string v18, "SessionManagerService"

    #@25d
    new-instance v20, Ljava/lang/StringBuilder;

    #@25f
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@262
    const-string v21, "Calculated bitrate: "

    #@264
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@267
    move-result-object v20

    #@268
    move-object/from16 v0, v20

    #@26a
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26d
    move-result-object v20

    #@26e
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@271
    move-result-object v20

    #@272
    move-object/from16 v0, v18

    #@274
    move-object/from16 v1, v20

    #@276
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@279
    .line 193
    move-object/from16 v0, p0

    #@27b
    iget-object v0, v0, Lcom/qualcomm/wfd/service/SessionManagerService$1;->this$0:Lcom/qualcomm/wfd/service/SessionManagerService;

    #@27d
    move-object/from16 v18, v0

    #@27f
    move-object/from16 v0, v18

    #@281
    invoke-virtual {v0, v4}, Lcom/qualcomm/wfd/service/SessionManagerService;->setBitrate(I)I

    #@284
    move-result v14

    #@285
    .line 194
    .restart local v14       #ret:I
    const-string v18, "SessionManagerService"

    #@287
    new-instance v20, Ljava/lang/StringBuilder;

    #@289
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@28c
    const-string v21, "setBitrate returned: "

    #@28e
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@291
    move-result-object v20

    #@292
    move-object/from16 v0, v20

    #@294
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@297
    move-result-object v20

    #@298
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29b
    move-result-object v20

    #@29c
    move-object/from16 v0, v18

    #@29e
    move-object/from16 v1, v20

    #@2a0
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a3
    goto/16 :goto_5e

    #@2a5
    .line 195
    .end local v4           #bitrate:I
    .end local v14           #ret:I
    :cond_2a5
    const-string v18, "qualcomm.intent.action.WIFI_DISPLAY_RTP_TRANSPORT"

    #@2a7
    move-object/from16 v0, v18

    #@2a9
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2ac
    move-result v18

    #@2ad
    if-eqz v18, :cond_33f

    #@2af
    .line 196
    const-string v18, "type"

    #@2b1
    const/16 v20, -0x1

    #@2b3
    move-object/from16 v0, p2

    #@2b5
    move-object/from16 v1, v18

    #@2b7
    move/from16 v2, v20

    #@2b9
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@2bc
    move-result v16

    #@2bd
    .line 197
    .local v16, transportType:I
    const-string v18, "bufferLength"

    #@2bf
    const/16 v20, 0x0

    #@2c1
    move-object/from16 v0, p2

    #@2c3
    move-object/from16 v1, v18

    #@2c5
    move/from16 v2, v20

    #@2c7
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@2ca
    move-result v5

    #@2cb
    .line 198
    .local v5, bufferLengthMs:I
    const-string v18, "port"

    #@2cd
    const/16 v20, -0x1

    #@2cf
    move-object/from16 v0, p2

    #@2d1
    move-object/from16 v1, v18

    #@2d3
    move/from16 v2, v20

    #@2d5
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@2d8
    move-result v13

    #@2d9
    .line 199
    .local v13, port:I
    const-string v18, "SessionManagerService"

    #@2db
    new-instance v20, Ljava/lang/StringBuilder;

    #@2dd
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@2e0
    const-string v21, "Calculted type: "

    #@2e2
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e5
    move-result-object v20

    #@2e6
    move-object/from16 v0, v20

    #@2e8
    move/from16 v1, v16

    #@2ea
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2ed
    move-result-object v20

    #@2ee
    const-string v21, ", bufferLengthMs: "

    #@2f0
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f3
    move-result-object v20

    #@2f4
    move-object/from16 v0, v20

    #@2f6
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f9
    move-result-object v20

    #@2fa
    const-string v21, ", port: "

    #@2fc
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ff
    move-result-object v20

    #@300
    move-object/from16 v0, v20

    #@302
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@305
    move-result-object v20

    #@306
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@309
    move-result-object v20

    #@30a
    move-object/from16 v0, v18

    #@30c
    move-object/from16 v1, v20

    #@30e
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@311
    .line 200
    move-object/from16 v0, p0

    #@313
    iget-object v0, v0, Lcom/qualcomm/wfd/service/SessionManagerService$1;->this$0:Lcom/qualcomm/wfd/service/SessionManagerService;

    #@315
    move-object/from16 v18, v0

    #@317
    move-object/from16 v0, v18

    #@319
    move/from16 v1, v16

    #@31b
    invoke-virtual {v0, v1, v5, v13}, Lcom/qualcomm/wfd/service/SessionManagerService;->setRtpTransport(III)I

    #@31e
    move-result v14

    #@31f
    .line 201
    .restart local v14       #ret:I
    const-string v18, "SessionManagerService"

    #@321
    new-instance v20, Ljava/lang/StringBuilder;

    #@323
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@326
    const-string v21, "setRtpTransport returned: "

    #@328
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32b
    move-result-object v20

    #@32c
    move-object/from16 v0, v20

    #@32e
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@331
    move-result-object v20

    #@332
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@335
    move-result-object v20

    #@336
    move-object/from16 v0, v18

    #@338
    move-object/from16 v1, v20

    #@33a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@33d
    goto/16 :goto_5e

    #@33f
    .line 202
    .end local v5           #bufferLengthMs:I
    .end local v13           #port:I
    .end local v14           #ret:I
    .end local v16           #transportType:I
    :cond_33f
    const-string v18, "qualcomm.intent.action.WIFI_DISPLAY_TCP_PLAYBACK_CONTROL"

    #@341
    move-object/from16 v0, v18

    #@343
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@346
    move-result v18

    #@347
    if-eqz v18, :cond_3a1

    #@349
    .line 203
    const-string v18, "type"

    #@34b
    const/16 v20, -0x1

    #@34d
    move-object/from16 v0, p2

    #@34f
    move-object/from16 v1, v18

    #@351
    move/from16 v2, v20

    #@353
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@356
    move-result v6

    #@357
    .line 204
    .local v6, cmdType:I
    const-string v18, "SessionManagerService"

    #@359
    new-instance v20, Ljava/lang/StringBuilder;

    #@35b
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@35e
    const-string v21, "Calculated type:"

    #@360
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@363
    move-result-object v20

    #@364
    move-object/from16 v0, v20

    #@366
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@369
    move-result-object v20

    #@36a
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36d
    move-result-object v20

    #@36e
    move-object/from16 v0, v18

    #@370
    move-object/from16 v1, v20

    #@372
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@375
    .line 205
    move-object/from16 v0, p0

    #@377
    iget-object v0, v0, Lcom/qualcomm/wfd/service/SessionManagerService$1;->this$0:Lcom/qualcomm/wfd/service/SessionManagerService;

    #@379
    move-object/from16 v18, v0

    #@37b
    move-object/from16 v0, v18

    #@37d
    invoke-virtual {v0, v6}, Lcom/qualcomm/wfd/service/SessionManagerService;->tcpPlaybackControl(I)I

    #@380
    move-result v14

    #@381
    .line 206
    .restart local v14       #ret:I
    const-string v18, "SessionManagerService"

    #@383
    new-instance v20, Ljava/lang/StringBuilder;

    #@385
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@388
    const-string v21, "tcpPlaybackControl returned: "

    #@38a
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38d
    move-result-object v20

    #@38e
    move-object/from16 v0, v20

    #@390
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@393
    move-result-object v20

    #@394
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@397
    move-result-object v20

    #@398
    move-object/from16 v0, v18

    #@39a
    move-object/from16 v1, v20

    #@39c
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@39f
    goto/16 :goto_5e

    #@3a1
    .line 207
    .end local v6           #cmdType:I
    .end local v14           #ret:I
    :cond_3a1
    const-string v18, "android.intent.action.CONFIGURATION_CHANGED"

    #@3a3
    move-object/from16 v0, v18

    #@3a5
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3a8
    move-result v18

    #@3a9
    if-eqz v18, :cond_447

    #@3ab
    .line 208
    move-object/from16 v0, p0

    #@3ad
    iget-object v0, v0, Lcom/qualcomm/wfd/service/SessionManagerService$1;->this$0:Lcom/qualcomm/wfd/service/SessionManagerService;

    #@3af
    move-object/from16 v18, v0

    #@3b1
    invoke-static/range {v18 .. v18}, Lcom/qualcomm/wfd/service/SessionManagerService;->access$400(Lcom/qualcomm/wfd/service/SessionManagerService;)Landroid/content/Context;

    #@3b4
    move-result-object v18

    #@3b5
    move-object/from16 v0, p0

    #@3b7
    iget-object v0, v0, Lcom/qualcomm/wfd/service/SessionManagerService$1;->this$0:Lcom/qualcomm/wfd/service/SessionManagerService;

    #@3b9
    move-object/from16 v20, v0

    #@3bb
    invoke-static/range {v20 .. v20}, Lcom/qualcomm/wfd/service/SessionManagerService;->access$400(Lcom/qualcomm/wfd/service/SessionManagerService;)Landroid/content/Context;

    #@3be
    const-string v20, "window"

    #@3c0
    move-object/from16 v0, v18

    #@3c2
    move-object/from16 v1, v20

    #@3c4
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@3c7
    move-result-object v18

    #@3c8
    check-cast v18, Landroid/view/WindowManager;

    #@3ca
    invoke-interface/range {v18 .. v18}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    #@3cd
    move-result-object v8

    #@3ce
    .line 210
    .local v8, display:Landroid/view/Display;
    new-instance v15, Landroid/graphics/Point;

    #@3d0
    invoke-direct {v15}, Landroid/graphics/Point;-><init>()V

    #@3d3
    .line 211
    .local v15, size:Landroid/graphics/Point;
    invoke-virtual {v8, v15}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    #@3d6
    .line 212
    move-object/from16 v0, p0

    #@3d8
    iget-object v0, v0, Lcom/qualcomm/wfd/service/SessionManagerService$1;->this$0:Lcom/qualcomm/wfd/service/SessionManagerService;

    #@3da
    move-object/from16 v18, v0

    #@3dc
    invoke-static/range {v18 .. v18}, Lcom/qualcomm/wfd/service/SessionManagerService;->access$400(Lcom/qualcomm/wfd/service/SessionManagerService;)Landroid/content/Context;

    #@3df
    move-result-object v18

    #@3e0
    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3e3
    move-result-object v18

    #@3e4
    invoke-virtual/range {v18 .. v18}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@3e7
    move-result-object v7

    #@3e8
    .line 213
    .local v7, config:Landroid/content/res/Configuration;
    const-string v18, "SessionManagerService"

    #@3ea
    new-instance v20, Ljava/lang/StringBuilder;

    #@3ec
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@3ef
    const-string v21, "width "

    #@3f1
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f4
    move-result-object v20

    #@3f5
    iget v0, v15, Landroid/graphics/Point;->x:I

    #@3f7
    move/from16 v21, v0

    #@3f9
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3fc
    move-result-object v20

    #@3fd
    const-string v21, "height "

    #@3ff
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@402
    move-result-object v20

    #@403
    iget v0, v15, Landroid/graphics/Point;->y:I

    #@405
    move/from16 v21, v0

    #@407
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40a
    move-result-object v20

    #@40b
    const-string v21, "Orientation "

    #@40d
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@410
    move-result-object v20

    #@411
    iget v0, v7, Landroid/content/res/Configuration;->orientation:I

    #@413
    move/from16 v21, v0

    #@415
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@418
    move-result-object v20

    #@419
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41c
    move-result-object v20

    #@41d
    move-object/from16 v0, v18

    #@41f
    move-object/from16 v1, v20

    #@421
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@424
    .line 214
    invoke-static {}, Lcom/qualcomm/wfd/service/SessionManagerService;->access$500()Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@427
    move-result-object v18

    #@428
    sget-object v20, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->SOURCE:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@42a
    move-object/from16 v0, v18

    #@42c
    move-object/from16 v1, v20

    #@42e
    if-ne v0, v1, :cond_5e

    #@430
    .line 215
    iget v0, v15, Landroid/graphics/Point;->x:I

    #@432
    move/from16 v18, v0

    #@434
    iget v0, v15, Landroid/graphics/Point;->y:I

    #@436
    move/from16 v20, v0

    #@438
    iget v0, v7, Landroid/content/res/Configuration;->orientation:I

    #@43a
    move/from16 v21, v0

    #@43c
    move/from16 v0, v18

    #@43e
    move/from16 v1, v20

    #@440
    move/from16 v2, v21

    #@442
    invoke-static {v0, v1, v2}, Lcom/qualcomm/wfd/WFDNative;->setSurfaceProp(III)V

    #@445
    goto/16 :goto_5e

    #@447
    .line 217
    .end local v7           #config:Landroid/content/res/Configuration;
    .end local v8           #display:Landroid/view/Display;
    .end local v15           #size:Landroid/graphics/Point;
    :cond_447
    const-string v18, "qualcomm.intent.action.WIFI_DISPLAY_PLAYBACK_MODE"

    #@449
    move-object/from16 v0, v18

    #@44b
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@44e
    move-result v18

    #@44f
    if-eqz v18, :cond_4a9

    #@451
    .line 218
    const-string v18, "mode"

    #@453
    const/16 v20, -0x1

    #@455
    move-object/from16 v0, p2

    #@457
    move-object/from16 v1, v18

    #@459
    move/from16 v2, v20

    #@45b
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@45e
    move-result v11

    #@45f
    .line 219
    .local v11, mode:I
    const-string v18, "SessionManagerService"

    #@461
    new-instance v20, Ljava/lang/StringBuilder;

    #@463
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@466
    const-string v21, "Calculated mode:"

    #@468
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46b
    move-result-object v20

    #@46c
    move-object/from16 v0, v20

    #@46e
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@471
    move-result-object v20

    #@472
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@475
    move-result-object v20

    #@476
    move-object/from16 v0, v18

    #@478
    move-object/from16 v1, v20

    #@47a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@47d
    .line 220
    move-object/from16 v0, p0

    #@47f
    iget-object v0, v0, Lcom/qualcomm/wfd/service/SessionManagerService$1;->this$0:Lcom/qualcomm/wfd/service/SessionManagerService;

    #@481
    move-object/from16 v18, v0

    #@483
    move-object/from16 v0, v18

    #@485
    invoke-virtual {v0, v11}, Lcom/qualcomm/wfd/service/SessionManagerService;->setAvPlaybackMode(I)I

    #@488
    move-result v14

    #@489
    .line 221
    .restart local v14       #ret:I
    const-string v18, "SessionManagerService"

    #@48b
    new-instance v20, Ljava/lang/StringBuilder;

    #@48d
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@490
    const-string v21, "setAvPlaybackMode returned: "

    #@492
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@495
    move-result-object v20

    #@496
    move-object/from16 v0, v20

    #@498
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@49b
    move-result-object v20

    #@49c
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49f
    move-result-object v20

    #@4a0
    move-object/from16 v0, v18

    #@4a2
    move-object/from16 v1, v20

    #@4a4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4a7
    goto/16 :goto_5e

    #@4a9
    .line 224
    .end local v11           #mode:I
    .end local v14           #ret:I
    :cond_4a9
    const-string v18, "android.intent.action.PHONE_STATE"

    #@4ab
    move-object/from16 v0, v18

    #@4ad
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4b0
    move-result v18

    #@4b1
    if-eqz v18, :cond_5e

    #@4b3
    .line 225
    const-string v18, "state"

    #@4b5
    move-object/from16 v0, p2

    #@4b7
    move-object/from16 v1, v18

    #@4b9
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@4bc
    move-result-object v12

    #@4bd
    .line 226
    .local v12, phoneState:Ljava/lang/String;
    const-string v18, "SessionManagerService"

    #@4bf
    new-instance v20, Ljava/lang/StringBuilder;

    #@4c1
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@4c4
    const-string v21, "Received intent: "

    #@4c6
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c9
    move-result-object v20

    #@4ca
    move-object/from16 v0, v20

    #@4cc
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4cf
    move-result-object v20

    #@4d0
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d3
    move-result-object v20

    #@4d4
    move-object/from16 v0, v18

    #@4d6
    move-object/from16 v1, v20

    #@4d8
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4db
    .line 228
    sget-object v18, Landroid/telephony/TelephonyManager;->EXTRA_STATE_IDLE:Ljava/lang/String;

    #@4dd
    move-object/from16 v0, v18

    #@4df
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4e2
    move-result v18

    #@4e3
    if-eqz v18, :cond_5e

    #@4e5
    .line 229
    invoke-static {}, Lcom/qualcomm/wfd/service/SessionManagerService;->access$300()Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@4e8
    move-result-object v18

    #@4e9
    sget-object v20, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PLAY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@4eb
    move-object/from16 v0, v18

    #@4ed
    move-object/from16 v1, v20

    #@4ef
    if-eq v0, v1, :cond_4fd

    #@4f1
    invoke-static {}, Lcom/qualcomm/wfd/service/SessionManagerService;->access$300()Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@4f4
    move-result-object v18

    #@4f5
    sget-object v20, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PLAYING:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@4f7
    move-object/from16 v0, v18

    #@4f9
    move-object/from16 v1, v20

    #@4fb
    if-ne v0, v1, :cond_527

    #@4fd
    .line 230
    :cond_4fd
    invoke-static {}, Lcom/qualcomm/wfd/service/SessionManagerService;->access$600()Z

    #@500
    move-result v18

    #@501
    if-nez v18, :cond_51a

    #@503
    invoke-static {}, Lcom/qualcomm/wfd/service/SessionManagerService;->access$700()Z

    #@506
    move-result v18

    #@507
    if-nez v18, :cond_51a

    #@509
    .line 231
    move-object/from16 v0, p0

    #@50b
    iget-object v0, v0, Lcom/qualcomm/wfd/service/SessionManagerService$1;->this$0:Lcom/qualcomm/wfd/service/SessionManagerService;

    #@50d
    move-object/from16 v18, v0

    #@50f
    const/16 v20, 0x1

    #@511
    move-object/from16 v0, v18

    #@513
    move/from16 v1, v20

    #@515
    invoke-static {v0, v1}, Lcom/qualcomm/wfd/service/SessionManagerService;->access$800(Lcom/qualcomm/wfd/service/SessionManagerService;Z)V

    #@518
    goto/16 :goto_5e

    #@51a
    .line 233
    :cond_51a
    const-string v18, "SessionManagerService"

    #@51c
    const-string v20, "BT or HEADSET connected, not broadcasting intent"

    #@51e
    move-object/from16 v0, v18

    #@520
    move-object/from16 v1, v20

    #@522
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@525
    goto/16 :goto_5e

    #@527
    .line 236
    :cond_527
    const-string v18, "SessionManagerService"

    #@529
    const-string v20, "Session state is not PLAY, not broadcasting intent"

    #@52b
    move-object/from16 v0, v18

    #@52d
    move-object/from16 v1, v20

    #@52f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_532
    .catchall {:try_start_16b .. :try_end_532} :catchall_e3

    #@532
    goto/16 :goto_5e
.end method
