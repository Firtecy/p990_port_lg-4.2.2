.class public Lcom/qualcomm/wfd/service/SessionManagerService;
.super Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;
.source "SessionManagerService.java"

# interfaces
.implements Lcom/qualcomm/wfd/WFDNative$WfdActionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/wfd/service/SessionManagerService$2;
    }
.end annotation


# static fields
.field private static ActionListeners:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/qualcomm/wfd/service/IWfdActionListener;",
            ">;"
        }
    .end annotation
.end field

.field private static BeforeSecureState:Lcom/qualcomm/wfd/WfdEnums$SessionState;

.field private static BeforeStandbyState:Lcom/qualcomm/wfd/WfdEnums$SessionState;

.field private static ConnectedDevice:Lcom/qualcomm/wfd/WfdDevice;

.field private static DeviceType:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

.field private static IS_BT_CONNECTED:Z

.field private static IS_HDCP_ENABLED:Z

.field private static IS_HDMI_CABLE_CONNECTED:Z

.field private static IS_HEADSET_PLUGGED:Z

.field private static IS_RTP_TRANSPORT_NEGOTIATED:Z

.field private static IS_WFD_AUDIO_ENABLED:Z

.field private static PLAYBACK_MODE:Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;

.field private static SessionId:I

.field private static State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

.field private static final StaticLock:Ljava/lang/Object;

.field private static UIBCMgr:Lcom/qualcomm/wfd/UIBCManager;

.field private static sSurface:Landroid/view/Surface;

.field private static uibcEnabled:Z


# instance fields
.field private mContext:Landroid/content/Context;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private sConfigFile:Ljava/io/File;

.field private sDocument:Lorg/w3c/dom/Document;

.field private sDocumentBuilder:Ljavax/xml/parsers/DocumentBuilder;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 61
    new-instance v0, Ljava/lang/Object;

    #@4
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@7
    sput-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->StaticLock:Ljava/lang/Object;

    #@9
    .line 65
    sput-boolean v1, Lcom/qualcomm/wfd/service/SessionManagerService;->IS_HDMI_CABLE_CONNECTED:Z

    #@b
    .line 67
    sput-boolean v1, Lcom/qualcomm/wfd/service/SessionManagerService;->IS_BT_CONNECTED:Z

    #@d
    .line 69
    sput-boolean v1, Lcom/qualcomm/wfd/service/SessionManagerService;->IS_HEADSET_PLUGGED:Z

    #@f
    .line 71
    sput-boolean v1, Lcom/qualcomm/wfd/service/SessionManagerService;->IS_HDCP_ENABLED:Z

    #@11
    .line 73
    sput-boolean v1, Lcom/qualcomm/wfd/service/SessionManagerService;->IS_RTP_TRANSPORT_NEGOTIATED:Z

    #@13
    .line 75
    sput-boolean v1, Lcom/qualcomm/wfd/service/SessionManagerService;->IS_WFD_AUDIO_ENABLED:Z

    #@15
    .line 77
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;->AUDIO_VIDEO:Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;

    #@17
    sput-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->PLAYBACK_MODE:Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;

    #@19
    .line 79
    sput-object v2, Lcom/qualcomm/wfd/service/SessionManagerService;->ActionListeners:Landroid/os/RemoteCallbackList;

    #@1b
    .line 81
    sput-object v2, Lcom/qualcomm/wfd/service/SessionManagerService;->UIBCMgr:Lcom/qualcomm/wfd/UIBCManager;

    #@1d
    .line 83
    sput-boolean v1, Lcom/qualcomm/wfd/service/SessionManagerService;->uibcEnabled:Z

    #@1f
    .line 85
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;->INVALID:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@21
    sput-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@23
    .line 87
    sput-object v2, Lcom/qualcomm/wfd/service/SessionManagerService;->BeforeStandbyState:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@25
    .line 89
    sput-object v2, Lcom/qualcomm/wfd/service/SessionManagerService;->BeforeSecureState:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@27
    .line 91
    const/4 v0, -0x1

    #@28
    sput v0, Lcom/qualcomm/wfd/service/SessionManagerService;->SessionId:I

    #@2a
    .line 93
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->SOURCE:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@2c
    sput-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->DeviceType:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@2e
    .line 95
    sput-object v2, Lcom/qualcomm/wfd/service/SessionManagerService;->ConnectedDevice:Lcom/qualcomm/wfd/WfdDevice;

    #@30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 7
    .parameter "context"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 111
    invoke-direct {p0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;-><init>()V

    #@4
    .line 103
    iput-object v2, p0, Lcom/qualcomm/wfd/service/SessionManagerService;->sDocumentBuilder:Ljavax/xml/parsers/DocumentBuilder;

    #@6
    .line 105
    iput-object v2, p0, Lcom/qualcomm/wfd/service/SessionManagerService;->sDocument:Lorg/w3c/dom/Document;

    #@8
    .line 107
    iput-object v2, p0, Lcom/qualcomm/wfd/service/SessionManagerService;->sConfigFile:Ljava/io/File;

    #@a
    .line 112
    const-string v2, "SessionManagerService"

    #@c
    const-string v3, "SessionManagerService ctor"

    #@e
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 113
    iput-object p1, p0, Lcom/qualcomm/wfd/service/SessionManagerService;->mContext:Landroid/content/Context;

    #@13
    .line 114
    invoke-direct {p0}, Lcom/qualcomm/wfd/service/SessionManagerService;->setupXML()V

    #@16
    .line 115
    sget-object v3, Lcom/qualcomm/wfd/service/SessionManagerService;->StaticLock:Ljava/lang/Object;

    #@18
    monitor-enter v3

    #@19
    .line 116
    :try_start_19
    sget-object v2, Lcom/qualcomm/wfd/service/SessionManagerService;->ActionListeners:Landroid/os/RemoteCallbackList;

    #@1b
    if-nez v2, :cond_9c

    #@1d
    .line 117
    const-string v2, "SessionManagerService"

    #@1f
    const-string v4, "Reinitializing callback list"

    #@21
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 118
    new-instance v2, Landroid/os/RemoteCallbackList;

    #@26
    invoke-direct {v2}, Landroid/os/RemoteCallbackList;-><init>()V

    #@29
    sput-object v2, Lcom/qualcomm/wfd/service/SessionManagerService;->ActionListeners:Landroid/os/RemoteCallbackList;

    #@2b
    .line 122
    :goto_2b
    monitor-exit v3
    :try_end_2c
    .catchall {:try_start_19 .. :try_end_2c} :catchall_a4

    #@2c
    .line 123
    invoke-virtual {p0}, Lcom/qualcomm/wfd/service/SessionManagerService;->teardown()I

    #@2f
    move-result v1

    #@30
    .line 124
    .local v1, ret:I
    const-string v2, "SessionManagerService"

    #@32
    new-instance v3, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    const-string v4, "Cleanup any existing sessions. ret: "

    #@39
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v3

    #@3d
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v3

    #@41
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v3

    #@45
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 125
    new-instance v2, Lcom/qualcomm/wfd/service/SessionManagerService$1;

    #@4a
    invoke-direct {v2, p0}, Lcom/qualcomm/wfd/service/SessionManagerService$1;-><init>(Lcom/qualcomm/wfd/service/SessionManagerService;)V

    #@4d
    iput-object v2, p0, Lcom/qualcomm/wfd/service/SessionManagerService;->mReceiver:Landroid/content/BroadcastReceiver;

    #@4f
    .line 244
    new-instance v0, Landroid/content/IntentFilter;

    #@51
    const-string v2, "android.intent.action.HDMI_PLUGGED"

    #@53
    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@56
    .line 249
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v2, "qualcomm.intent.action.SECURE_START"

    #@58
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@5b
    .line 250
    const-string v2, "qualcomm.intent.action.SECURE_START_DONE"

    #@5d
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@60
    .line 251
    const-string v2, "qualcomm.intent.action.SECURE_END"

    #@62
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@65
    .line 252
    const-string v2, "qualcomm.intent.action.SECURE_END_DONE"

    #@67
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@6a
    .line 253
    const-string v2, "android.intent.action.ACTION_SHUTDOWN"

    #@6c
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@6f
    .line 254
    const-string v2, "qualcomm.intent.action.WIFI_DISPLAY_RESOLUTION"

    #@71
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@74
    .line 255
    const-string v2, "qualcomm.intent.action.WIFI_DISPLAY_BITRATE"

    #@76
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@79
    .line 256
    const-string v2, "qualcomm.intent.action.WIFI_DISPLAY_RTP_TRANSPORT"

    #@7b
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@7e
    .line 257
    const-string v2, "qualcomm.intent.action.WIFI_DISPLAY_TCP_PLAYBACK_CONTROL"

    #@80
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@83
    .line 258
    const-string v2, "android.intent.action.CONFIGURATION_CHANGED"

    #@85
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@88
    .line 259
    const-string v2, "qualcomm.intent.action.WIFI_DISPLAY_PLAYBACK_MODE"

    #@8a
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@8d
    .line 261
    const-string v2, "android.intent.action.PHONE_STATE"

    #@8f
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@92
    .line 263
    iget-object v2, p0, Lcom/qualcomm/wfd/service/SessionManagerService;->mContext:Landroid/content/Context;

    #@94
    iget-object v3, p0, Lcom/qualcomm/wfd/service/SessionManagerService;->mReceiver:Landroid/content/BroadcastReceiver;

    #@96
    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@99
    .line 264
    sput-object p0, Lcom/qualcomm/wfd/WFDNative;->listener:Lcom/qualcomm/wfd/WFDNative$WfdActionListener;

    #@9b
    .line 265
    return-void

    #@9c
    .line 120
    .end local v0           #filter:Landroid/content/IntentFilter;
    .end local v1           #ret:I
    :cond_9c
    :try_start_9c
    const-string v2, "SessionManagerService"

    #@9e
    const-string v4, "Callback list in not null"

    #@a0
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a3
    goto :goto_2b

    #@a4
    .line 122
    :catchall_a4
    move-exception v2

    #@a5
    monitor-exit v3
    :try_end_a6
    .catchall {:try_start_9c .. :try_end_a6} :catchall_a4

    #@a6
    throw v2
.end method

.method static synthetic access$000()Ljava/lang/Object;
    .registers 1

    #@0
    .prologue
    .line 56
    sget-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->StaticLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$100()Z
    .registers 1

    #@0
    .prologue
    .line 56
    sget-boolean v0, Lcom/qualcomm/wfd/service/SessionManagerService;->IS_HDMI_CABLE_CONNECTED:Z

    #@2
    return v0
.end method

.method static synthetic access$102(Z)Z
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 56
    sput-boolean p0, Lcom/qualcomm/wfd/service/SessionManagerService;->IS_HDMI_CABLE_CONNECTED:Z

    #@2
    return p0
.end method

.method static synthetic access$200()Lcom/qualcomm/wfd/WfdEnums$SessionState;
    .registers 1

    #@0
    .prologue
    .line 56
    sget-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->BeforeStandbyState:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@2
    return-object v0
.end method

.method static synthetic access$202(Lcom/qualcomm/wfd/WfdEnums$SessionState;)Lcom/qualcomm/wfd/WfdEnums$SessionState;
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 56
    sput-object p0, Lcom/qualcomm/wfd/service/SessionManagerService;->BeforeStandbyState:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@2
    return-object p0
.end method

.method static synthetic access$300()Lcom/qualcomm/wfd/WfdEnums$SessionState;
    .registers 1

    #@0
    .prologue
    .line 56
    sget-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/qualcomm/wfd/service/SessionManagerService;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 56
    iget-object v0, p0, Lcom/qualcomm/wfd/service/SessionManagerService;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$500()Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;
    .registers 1

    #@0
    .prologue
    .line 56
    sget-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->DeviceType:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@2
    return-object v0
.end method

.method static synthetic access$600()Z
    .registers 1

    #@0
    .prologue
    .line 56
    sget-boolean v0, Lcom/qualcomm/wfd/service/SessionManagerService;->IS_BT_CONNECTED:Z

    #@2
    return v0
.end method

.method static synthetic access$700()Z
    .registers 1

    #@0
    .prologue
    .line 56
    sget-boolean v0, Lcom/qualcomm/wfd/service/SessionManagerService;->IS_HEADSET_PLUGGED:Z

    #@2
    return v0
.end method

.method static synthetic access$800(Lcom/qualcomm/wfd/service/SessionManagerService;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/qualcomm/wfd/service/SessionManagerService;->broadcastWifiDisplayAudioIntent(Z)V

    #@3
    return-void
.end method

.method private broadcastWifiDisplayAudioIntent(Z)V
    .registers 8
    .parameter

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 920
    const-string v0, "SessionManagerService"

    #@4
    new-instance v1, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v2, "broadcastWifiDisplayAudioIntent() - flag: "

    #@b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 921
    sget-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->DeviceType:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@1c
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->SOURCE:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@1e
    if-eq v0, v1, :cond_28

    #@20
    .line 922
    const-string v0, "SessionManagerService"

    #@22
    const-string v1, "Abort broadcast as device type is not source"

    #@24
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 951
    :goto_27
    return-void

    #@28
    .line 925
    :cond_28
    sget-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->PLAYBACK_MODE:Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;

    #@2a
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;->VIDEO_ONLY:Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;

    #@2c
    if-ne v0, v1, :cond_36

    #@2e
    .line 926
    const-string v0, "SessionManagerService"

    #@30
    const-string v1, "Playback mode is VIDEO_ONLY, ignore audio intent broadcast"

    #@32
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    goto :goto_27

    #@36
    .line 930
    :cond_36
    if-eqz p1, :cond_70

    #@38
    .line 931
    sget-boolean v0, Lcom/qualcomm/wfd/service/SessionManagerService;->IS_WFD_AUDIO_ENABLED:Z

    #@3a
    if-nez v0, :cond_68

    #@3c
    .line 932
    new-instance v0, Landroid/content/Intent;

    #@3e
    const-string v1, "qualcomm.intent.action.WIFI_DISPLAY_AUDIO"

    #@40
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@43
    .line 933
    const-string v1, "state"

    #@45
    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@48
    .line 934
    const-string v1, "SessionManagerService"

    #@4a
    new-instance v2, Ljava/lang/StringBuilder;

    #@4c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@4f
    const-string v3, "Broadcasting WFD intent: "

    #@51
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v2

    #@55
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v2

    #@59
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v2

    #@5d
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@60
    .line 935
    sput-boolean v5, Lcom/qualcomm/wfd/service/SessionManagerService;->IS_WFD_AUDIO_ENABLED:Z

    #@62
    .line 936
    iget-object v1, p0, Lcom/qualcomm/wfd/service/SessionManagerService;->mContext:Landroid/content/Context;

    #@64
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@67
    goto :goto_27

    #@68
    .line 938
    :cond_68
    const-string v0, "SessionManagerService"

    #@6a
    const-string v1, "Ignoring WFD Audio Enabled Broadcast as already sent"

    #@6c
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6f
    goto :goto_27

    #@70
    .line 941
    :cond_70
    sget-boolean v0, Lcom/qualcomm/wfd/service/SessionManagerService;->IS_WFD_AUDIO_ENABLED:Z

    #@72
    if-eqz v0, :cond_a0

    #@74
    .line 942
    new-instance v0, Landroid/content/Intent;

    #@76
    const-string v1, "qualcomm.intent.action.WIFI_DISPLAY_AUDIO"

    #@78
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7b
    .line 943
    const-string v1, "state"

    #@7d
    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@80
    .line 944
    const-string v1, "SessionManagerService"

    #@82
    new-instance v2, Ljava/lang/StringBuilder;

    #@84
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@87
    const-string v3, "Broadcasting WFD intent: "

    #@89
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v2

    #@8d
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v2

    #@91
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v2

    #@95
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@98
    .line 945
    sput-boolean v4, Lcom/qualcomm/wfd/service/SessionManagerService;->IS_WFD_AUDIO_ENABLED:Z

    #@9a
    .line 946
    iget-object v1, p0, Lcom/qualcomm/wfd/service/SessionManagerService;->mContext:Landroid/content/Context;

    #@9c
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@9f
    goto :goto_27

    #@a0
    .line 948
    :cond_a0
    const-string v0, "SessionManagerService"

    #@a2
    const-string v1, "Ignoring WFD Audio Disabled Broadcast as already sent"

    #@a4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a7
    goto :goto_27
.end method

.method private broadcastWifiDisplayVideoEnabled(Z)V
    .registers 6
    .parameter

    #@0
    .prologue
    .line 954
    const-string v0, "SessionManagerService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "broadcastWifiDisplayVideoEnabled() - flag: "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 955
    sget-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->DeviceType:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@1a
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->SOURCE:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@1c
    if-eq v0, v1, :cond_26

    #@1e
    .line 956
    const-string v0, "SessionManagerService"

    #@20
    const-string v1, "Abort broadcast as device type is not source"

    #@22
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 972
    :goto_25
    return-void

    #@26
    .line 959
    :cond_26
    sget-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->PLAYBACK_MODE:Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;

    #@28
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;->AUDIO_ONLY:Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;

    #@2a
    if-ne v0, v1, :cond_34

    #@2c
    .line 960
    const-string v0, "SessionManagerService"

    #@2e
    const-string v1, "Playback mode is AUDIO_ONLY, ignore video intent broadcast"

    #@30
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    goto :goto_25

    #@34
    .line 963
    :cond_34
    new-instance v0, Landroid/content/Intent;

    #@36
    const-string v1, "qualcomm.intent.action.WIFI_DISPLAY_VIDEO"

    #@38
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@3b
    .line 964
    if-eqz p1, :cond_61

    #@3d
    .line 965
    const-string v1, "state"

    #@3f
    const/4 v2, 0x1

    #@40
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@43
    .line 969
    :goto_43
    const-string v1, "SessionManagerService"

    #@45
    new-instance v2, Ljava/lang/StringBuilder;

    #@47
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@4a
    const-string v3, "Broadcasting WFD video intent: "

    #@4c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v2

    #@50
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v2

    #@54
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v2

    #@58
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5b
    .line 970
    iget-object v1, p0, Lcom/qualcomm/wfd/service/SessionManagerService;->mContext:Landroid/content/Context;

    #@5d
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@60
    goto :goto_25

    #@61
    .line 967
    :cond_61
    const-string v1, "state"

    #@63
    const/4 v2, 0x0

    #@64
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@67
    goto :goto_43
.end method

.method private findNodeByName(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;
    .registers 6
    .parameter "node"
    .parameter "name"

    #@0
    .prologue
    .line 317
    if-eqz p1, :cond_22

    #@2
    if-eqz p2, :cond_22

    #@4
    .line 318
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    #@7
    move-result-object v2

    #@8
    invoke-virtual {p2, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@b
    move-result v2

    #@c
    if-eqz v2, :cond_f

    #@e
    .line 330
    .end local p1
    :goto_e
    return-object p1

    #@f
    .line 321
    .restart local p1
    :cond_f
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    #@12
    move-result-object v1

    #@13
    .local v1, n:Lorg/w3c/dom/Node;
    :goto_13
    if-eqz v1, :cond_22

    #@15
    .line 323
    invoke-direct {p0, v1, p2}, Lcom/qualcomm/wfd/service/SessionManagerService;->findNodeByName(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    #@18
    move-result-object v0

    #@19
    .line 324
    .local v0, found:Lorg/w3c/dom/Node;
    if-eqz v0, :cond_1d

    #@1b
    move-object p1, v0

    #@1c
    .line 325
    goto :goto_e

    #@1d
    .line 321
    :cond_1d
    invoke-interface {v1}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;

    #@20
    move-result-object v1

    #@21
    goto :goto_13

    #@22
    .line 330
    .end local v0           #found:Lorg/w3c/dom/Node;
    .end local v1           #n:Lorg/w3c/dom/Node;
    :cond_22
    const/4 p1, 0x0

    #@23
    goto :goto_e
.end method

.method private internalPause(Z)I
    .registers 6
    .parameter "secureFlag"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 711
    const-string v1, "SessionManagerService"

    #@3
    new-instance v2, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v3, "internalPause(): secureFlag - "

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 712
    sget-object v1, Lcom/qualcomm/wfd/service/SessionManagerService;->StaticLock:Ljava/lang/Object;

    #@1b
    monitor-enter v1

    #@1c
    .line 713
    :try_start_1c
    sget-boolean v2, Lcom/qualcomm/wfd/service/SessionManagerService;->IS_HDMI_CABLE_CONNECTED:Z

    #@1e
    if-eqz v2, :cond_28

    #@20
    .line 714
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->HDMI_CABLE_CONNECTED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@22
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@25
    move-result v0

    #@26
    monitor-exit v1

    #@27
    .line 726
    :goto_27
    return v0

    #@28
    .line 716
    :cond_28
    sget-object v2, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@2a
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PAUSING:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@2c
    if-ne v2, v3, :cond_3a

    #@2e
    .line 717
    const-string v2, "SessionManagerService"

    #@30
    const-string v3, "Already in the middle of PAUSING"

    #@32
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    .line 718
    monitor-exit v1

    #@36
    goto :goto_27

    #@37
    .line 728
    :catchall_37
    move-exception v0

    #@38
    monitor-exit v1
    :try_end_39
    .catchall {:try_start_1c .. :try_end_39} :catchall_37

    #@39
    throw v0

    #@3a
    .line 720
    :cond_3a
    :try_start_3a
    sget-object v2, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@3c
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PLAY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@3e
    if-ne v2, v3, :cond_4b

    #@40
    .line 721
    sget v2, Lcom/qualcomm/wfd/service/SessionManagerService;->SessionId:I

    #@42
    invoke-static {v2, p1}, Lcom/qualcomm/wfd/WFDNative;->pause(IZ)V

    #@45
    .line 722
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PAUSING:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@47
    sput-object v2, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@49
    .line 723
    monitor-exit v1

    #@4a
    goto :goto_27

    #@4b
    .line 725
    :cond_4b
    const-string v0, "SessionManagerService"

    #@4d
    const-string v2, "Session state is not PLAY"

    #@4f
    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 726
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->INCORRECT_STATE_FOR_OPERATION:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@54
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@57
    move-result v0

    #@58
    monitor-exit v1
    :try_end_59
    .catchall {:try_start_3a .. :try_end_59} :catchall_37

    #@59
    goto :goto_27
.end method

.method private internalPlay(Z)I
    .registers 6
    .parameter "secureFlag"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 683
    const-string v1, "SessionManagerService"

    #@3
    new-instance v2, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v3, "internalPlay(): secureFlag - "

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 684
    sget-object v1, Lcom/qualcomm/wfd/service/SessionManagerService;->StaticLock:Ljava/lang/Object;

    #@1b
    monitor-enter v1

    #@1c
    .line 685
    :try_start_1c
    sget-boolean v2, Lcom/qualcomm/wfd/service/SessionManagerService;->IS_HDMI_CABLE_CONNECTED:Z

    #@1e
    if-eqz v2, :cond_28

    #@20
    .line 686
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->HDMI_CABLE_CONNECTED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@22
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@25
    move-result v0

    #@26
    monitor-exit v1

    #@27
    .line 699
    :goto_27
    return v0

    #@28
    .line 688
    :cond_28
    sget-object v2, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@2a
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PLAYING:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@2c
    if-ne v2, v3, :cond_3a

    #@2e
    .line 689
    const-string v2, "SessionManagerService"

    #@30
    const-string v3, "Already in the middle of PLAYING"

    #@32
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    .line 690
    monitor-exit v1

    #@36
    goto :goto_27

    #@37
    .line 701
    :catchall_37
    move-exception v0

    #@38
    monitor-exit v1
    :try_end_39
    .catchall {:try_start_1c .. :try_end_39} :catchall_37

    #@39
    throw v0

    #@3a
    .line 692
    :cond_3a
    :try_start_3a
    sget-object v2, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@3c
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ESTABLISHED:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@3e
    if-eq v2, v3, :cond_4c

    #@40
    sget-object v2, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@42
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PAUSE:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@44
    if-eq v2, v3, :cond_4c

    #@46
    sget-object v2, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@48
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$SessionState;->STANDBY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@4a
    if-ne v2, v3, :cond_57

    #@4c
    .line 694
    :cond_4c
    sget v2, Lcom/qualcomm/wfd/service/SessionManagerService;->SessionId:I

    #@4e
    invoke-static {v2, p1}, Lcom/qualcomm/wfd/WFDNative;->play(IZ)V

    #@51
    .line 695
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PLAYING:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@53
    sput-object v2, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@55
    .line 696
    monitor-exit v1

    #@56
    goto :goto_27

    #@57
    .line 698
    :cond_57
    const-string v0, "SessionManagerService"

    #@59
    const-string v2, "Session state is not ESTABLISHED or PAUSE or STANDBY"

    #@5b
    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    .line 699
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->INCORRECT_STATE_FOR_OPERATION:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@60
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@63
    move-result v0

    #@64
    monitor-exit v1
    :try_end_65
    .catchall {:try_start_3a .. :try_end_65} :catchall_37

    #@65
    goto :goto_27
.end method

.method private releaseFileReferences()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 310
    const-string v0, "SessionManagerService"

    #@3
    const-string v1, "releaseFileReferences()"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 311
    iput-object v2, p0, Lcom/qualcomm/wfd/service/SessionManagerService;->sDocumentBuilder:Ljavax/xml/parsers/DocumentBuilder;

    #@a
    .line 312
    iput-object v2, p0, Lcom/qualcomm/wfd/service/SessionManagerService;->sDocument:Lorg/w3c/dom/Document;

    #@c
    .line 313
    iput-object v2, p0, Lcom/qualcomm/wfd/service/SessionManagerService;->sConfigFile:Ljava/io/File;

    #@e
    .line 314
    return-void
.end method

.method private setupXML()V
    .registers 5

    #@0
    .prologue
    .line 283
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {v1}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    #@7
    move-result-object v1

    #@8
    iput-object v1, p0, Lcom/qualcomm/wfd/service/SessionManagerService;->sDocumentBuilder:Ljavax/xml/parsers/DocumentBuilder;
    :try_end_a
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_0 .. :try_end_a} :catch_26

    #@a
    .line 289
    new-instance v1, Ljava/io/File;

    #@c
    const-string v2, "/system/etc/wfdconfig.xml"

    #@e
    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@11
    iput-object v1, p0, Lcom/qualcomm/wfd/service/SessionManagerService;->sConfigFile:Ljava/io/File;

    #@13
    .line 290
    iget-object v1, p0, Lcom/qualcomm/wfd/service/SessionManagerService;->sConfigFile:Ljava/io/File;

    #@15
    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    #@18
    move-result v1

    #@19
    if-eqz v1, :cond_3e

    #@1b
    .line 293
    :try_start_1b
    iget-object v1, p0, Lcom/qualcomm/wfd/service/SessionManagerService;->sDocumentBuilder:Ljavax/xml/parsers/DocumentBuilder;

    #@1d
    iget-object v2, p0, Lcom/qualcomm/wfd/service/SessionManagerService;->sConfigFile:Ljava/io/File;

    #@1f
    invoke-virtual {v1, v2}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/File;)Lorg/w3c/dom/Document;

    #@22
    move-result-object v1

    #@23
    iput-object v1, p0, Lcom/qualcomm/wfd/service/SessionManagerService;->sDocument:Lorg/w3c/dom/Document;
    :try_end_25
    .catch Lorg/xml/sax/SAXException; {:try_start_1b .. :try_end_25} :catch_2e
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_25} :catch_36

    #@25
    .line 307
    :goto_25
    return-void

    #@26
    .line 284
    :catch_26
    move-exception v0

    #@27
    .line 285
    .local v0, e:Ljavax/xml/parsers/ParserConfigurationException;
    invoke-virtual {v0}, Ljavax/xml/parsers/ParserConfigurationException;->printStackTrace()V

    #@2a
    .line 286
    invoke-direct {p0}, Lcom/qualcomm/wfd/service/SessionManagerService;->releaseFileReferences()V

    #@2d
    goto :goto_25

    #@2e
    .line 294
    .end local v0           #e:Ljavax/xml/parsers/ParserConfigurationException;
    :catch_2e
    move-exception v0

    #@2f
    .line 295
    .local v0, e:Lorg/xml/sax/SAXException;
    invoke-virtual {v0}, Lorg/xml/sax/SAXException;->printStackTrace()V

    #@32
    .line 296
    invoke-direct {p0}, Lcom/qualcomm/wfd/service/SessionManagerService;->releaseFileReferences()V

    #@35
    goto :goto_25

    #@36
    .line 298
    .end local v0           #e:Lorg/xml/sax/SAXException;
    :catch_36
    move-exception v0

    #@37
    .line 299
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@3a
    .line 300
    invoke-direct {p0}, Lcom/qualcomm/wfd/service/SessionManagerService;->releaseFileReferences()V

    #@3d
    goto :goto_25

    #@3e
    .line 304
    .end local v0           #e:Ljava/io/IOException;
    :cond_3e
    const-string v1, "SessionManagerService"

    #@40
    new-instance v2, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    iget-object v3, p0, Lcom/qualcomm/wfd/service/SessionManagerService;->sConfigFile:Ljava/io/File;

    #@47
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    const-string v3, " is not on the filesystem"

    #@4d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v2

    #@51
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v2

    #@55
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@58
    .line 305
    invoke-direct {p0}, Lcom/qualcomm/wfd/service/SessionManagerService;->releaseFileReferences()V

    #@5b
    goto :goto_25
.end method


# virtual methods
.method public clearVars()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1224
    sget-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->UIBCMgr:Lcom/qualcomm/wfd/UIBCManager;

    #@3
    if-eqz v0, :cond_15

    #@5
    sget-boolean v0, Lcom/qualcomm/wfd/service/SessionManagerService;->uibcEnabled:Z

    #@7
    if-eqz v0, :cond_15

    #@9
    .line 1225
    const-string v0, "SessionManagerService"

    #@b
    const-string v1, "stopping UIBC threads"

    #@d
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 1226
    sget-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->UIBCMgr:Lcom/qualcomm/wfd/UIBCManager;

    #@12
    invoke-virtual {v0}, Lcom/qualcomm/wfd/UIBCManager;->stop()Z

    #@15
    .line 1229
    :cond_15
    sput-object v2, Lcom/qualcomm/wfd/service/SessionManagerService;->UIBCMgr:Lcom/qualcomm/wfd/UIBCManager;

    #@17
    .line 1230
    const/4 v0, 0x0

    #@18
    sput-boolean v0, Lcom/qualcomm/wfd/service/SessionManagerService;->uibcEnabled:Z

    #@1a
    .line 1231
    sput-object v2, Lcom/qualcomm/wfd/service/SessionManagerService;->ConnectedDevice:Lcom/qualcomm/wfd/WfdDevice;

    #@1c
    .line 1232
    return-void
.end method

.method public completeCapabilityNegotiation()I
    .registers 3

    #@0
    .prologue
    .line 878
    const-string v0, "SessionManagerService"

    #@2
    const-string v1, "completeCapabilityNegotiation()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 879
    sget-boolean v0, Lcom/qualcomm/wfd/service/SessionManagerService;->IS_HDMI_CABLE_CONNECTED:Z

    #@9
    if-eqz v0, :cond_12

    #@b
    .line 880
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->HDMI_CABLE_CONNECTED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@d
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@10
    move-result v0

    #@11
    .line 883
    :goto_11
    return v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method public deinit()I
    .registers 8

    #@0
    .prologue
    const/4 v3, -0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 553
    const-string v4, "SessionManagerService"

    #@4
    const-string v5, "deinit()"

    #@6
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 557
    const/4 v1, 0x0

    #@a
    .line 558
    .local v1, ret:Z
    sget-object v4, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@c
    sget-object v5, Lcom/qualcomm/wfd/WfdEnums$SessionState;->INVALID:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@e
    if-eq v4, v5, :cond_4b

    #@10
    .line 559
    new-instance v0, Landroid/content/Intent;

    #@12
    const-string v4, "qualcomm.intent.action.WIFI_DISPLAY_DISABLED"

    #@14
    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@17
    .line 560
    .local v0, intent:Landroid/content/Intent;
    invoke-direct {p0, v2}, Lcom/qualcomm/wfd/service/SessionManagerService;->broadcastWifiDisplayAudioIntent(Z)V

    #@1a
    .line 561
    invoke-direct {p0, v2}, Lcom/qualcomm/wfd/service/SessionManagerService;->broadcastWifiDisplayVideoEnabled(Z)V

    #@1d
    .line 562
    sget v4, Lcom/qualcomm/wfd/service/SessionManagerService;->SessionId:I

    #@1f
    invoke-static {v4, v2}, Lcom/qualcomm/wfd/WFDNative;->teardown(IZ)V

    #@22
    .line 563
    invoke-static {}, Lcom/qualcomm/wfd/WFDNative;->disableWfd()Z

    #@25
    move-result v1

    #@26
    .line 564
    sget-object v4, Lcom/qualcomm/wfd/WfdEnums$SessionState;->INVALID:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@28
    invoke-virtual {p0, v4, v3}, Lcom/qualcomm/wfd/service/SessionManagerService;->updateState(Lcom/qualcomm/wfd/WfdEnums$SessionState;I)V

    #@2b
    .line 565
    const/4 v4, 0x0

    #@2c
    sput-object v4, Lcom/qualcomm/wfd/service/SessionManagerService;->ConnectedDevice:Lcom/qualcomm/wfd/WfdDevice;

    #@2e
    .line 566
    const-string v4, "SessionManagerService"

    #@30
    new-instance v5, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v6, "Broadcasting WFD intent: "

    #@37
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v5

    #@3b
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v5

    #@3f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v5

    #@43
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    .line 567
    iget-object v4, p0, Lcom/qualcomm/wfd/service/SessionManagerService;->mContext:Landroid/content/Context;

    #@48
    invoke-virtual {v4, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@4b
    .line 569
    .end local v0           #intent:Landroid/content/Intent;
    :cond_4b
    if-eqz v1, :cond_4e

    #@4d
    :goto_4d
    return v2

    #@4e
    :cond_4e
    move v2, v3

    #@4f
    goto :goto_4d
.end method

.method public getCommonCapabilities(Landroid/os/Bundle;)I
    .registers 4
    .parameter

    #@0
    .prologue
    .line 845
    const-string v0, "SessionManagerService"

    #@2
    const-string v1, "getCommonCapabilities()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 846
    sget-boolean v0, Lcom/qualcomm/wfd/service/SessionManagerService;->IS_HDMI_CABLE_CONNECTED:Z

    #@9
    if-eqz v0, :cond_12

    #@b
    .line 847
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->HDMI_CABLE_CONNECTED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@d
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@10
    move-result v0

    #@11
    .line 850
    :goto_11
    return v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method public getStatus()Lcom/qualcomm/wfd/WfdStatus;
    .registers 4

    #@0
    .prologue
    .line 976
    const-string v0, "SessionManagerService"

    #@2
    const-string v1, "getStatus()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 977
    sget-object v1, Lcom/qualcomm/wfd/service/SessionManagerService;->StaticLock:Ljava/lang/Object;

    #@9
    monitor-enter v1

    #@a
    .line 978
    :try_start_a
    new-instance v0, Lcom/qualcomm/wfd/WfdStatus;

    #@c
    invoke-direct {v0}, Lcom/qualcomm/wfd/WfdStatus;-><init>()V

    #@f
    .line 979
    sget v2, Lcom/qualcomm/wfd/service/SessionManagerService;->SessionId:I

    #@11
    iput v2, v0, Lcom/qualcomm/wfd/WfdStatus;->sessionId:I

    #@13
    .line 980
    sget-object v2, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@15
    invoke-virtual {v2}, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ordinal()I

    #@18
    move-result v2

    #@19
    iput v2, v0, Lcom/qualcomm/wfd/WfdStatus;->state:I

    #@1b
    .line 981
    sget-object v2, Lcom/qualcomm/wfd/service/SessionManagerService;->ConnectedDevice:Lcom/qualcomm/wfd/WfdDevice;

    #@1d
    iput-object v2, v0, Lcom/qualcomm/wfd/WfdStatus;->connectedDevice:Lcom/qualcomm/wfd/WfdDevice;

    #@1f
    .line 982
    monitor-exit v1

    #@20
    return-object v0

    #@21
    .line 983
    :catchall_21
    move-exception v0

    #@22
    monitor-exit v1
    :try_end_23
    .catchall {:try_start_a .. :try_end_23} :catchall_21

    #@23
    throw v0
.end method

.method public getSupportedTypes([I)I
    .registers 7
    .parameter

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 865
    const-string v0, "SessionManagerService"

    #@3
    const-string v2, "getSupportedTypes()"

    #@5
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 866
    const/4 v0, 0x1

    #@9
    new-array v2, v0, [Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@b
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->SOURCE:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@d
    aput-object v0, v2, v1

    #@f
    .line 869
    array-length v0, v2

    #@10
    new-array v3, v0, [I

    #@12
    move v0, v1

    #@13
    .line 870
    :goto_13
    array-length v4, v2

    #@14
    if-ge v0, v4, :cond_21

    #@16
    .line 871
    aget-object v4, v2, v0

    #@18
    invoke-virtual {v4}, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->getCode()I

    #@1b
    move-result v4

    #@1c
    aput v4, v3, v0

    #@1e
    .line 870
    add-int/lit8 v0, v0, 0x1

    #@20
    goto :goto_13

    #@21
    .line 873
    :cond_21
    return v1
.end method

.method public getValue(Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "key"

    #@0
    .prologue
    .line 334
    iget-object v1, p0, Lcom/qualcomm/wfd/service/SessionManagerService;->sDocument:Lorg/w3c/dom/Document;

    #@2
    invoke-direct {p0, v1, p1}, Lcom/qualcomm/wfd/service/SessionManagerService;->findNodeByName(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    #@5
    move-result-object v0

    #@6
    .line 335
    .local v0, value:Lorg/w3c/dom/Node;
    if-nez v0, :cond_23

    #@8
    .line 337
    const-string v1, "SessionManagerService"

    #@a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    const-string v3, "not found in config file"

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 338
    const-string v1, "Not Found"

    #@22
    .line 340
    :goto_22
    return-object v1

    #@23
    :cond_23
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    goto :goto_22
.end method

.method public init(Lcom/qualcomm/wfd/service/IWfdActionListener;Lcom/qualcomm/wfd/WfdDevice;)I
    .registers 10
    .parameter "listener"
    .parameter "thisDevice"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 524
    const-string v3, "SessionManagerService"

    #@3
    const-string v4, "init()"

    #@5
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 525
    sget-object v3, Lcom/qualcomm/wfd/service/SessionManagerService;->StaticLock:Ljava/lang/Object;

    #@a
    monitor-enter v3

    #@b
    .line 527
    :try_start_b
    sget-boolean v4, Lcom/qualcomm/wfd/service/SessionManagerService;->IS_HDMI_CABLE_CONNECTED:Z

    #@d
    if-eqz v4, :cond_17

    #@f
    .line 528
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->HDMI_CABLE_CONNECTED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@11
    invoke-virtual {v2}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@14
    move-result v2

    #@15
    monitor-exit v3

    #@16
    .line 546
    :goto_16
    return v2

    #@17
    .line 530
    :cond_17
    if-eqz p1, :cond_1e

    #@19
    .line 531
    sget-object v4, Lcom/qualcomm/wfd/service/SessionManagerService;->ActionListeners:Landroid/os/RemoteCallbackList;

    #@1b
    invoke-virtual {v4, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    #@1e
    .line 533
    :cond_1e
    sget-object v4, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@20
    sget-object v5, Lcom/qualcomm/wfd/WfdEnums$SessionState;->INVALID:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@22
    if-ne v4, v5, :cond_6e

    #@24
    .line 534
    if-nez p2, :cond_38

    #@26
    .line 535
    const-string v2, "SessionManagerService"

    #@28
    const-string v4, "WfdDevice arg can not be null"

    #@2a
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 536
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->INVALID_ARG:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@2f
    invoke-virtual {v2}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@32
    move-result v2

    #@33
    monitor-exit v3

    #@34
    goto :goto_16

    #@35
    .line 548
    :catchall_35
    move-exception v2

    #@36
    monitor-exit v3
    :try_end_37
    .catchall {:try_start_b .. :try_end_37} :catchall_35

    #@37
    throw v2

    #@38
    .line 538
    :cond_38
    :try_start_38
    new-instance v0, Landroid/content/Intent;

    #@3a
    const-string v4, "qualcomm.intent.action.WIFI_DISPLAY_ENABLED"

    #@3c
    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@3f
    .line 539
    .local v0, intent:Landroid/content/Intent;
    const/4 v4, 0x0

    #@40
    sput-object v4, Lcom/qualcomm/wfd/service/SessionManagerService;->ConnectedDevice:Lcom/qualcomm/wfd/WfdDevice;

    #@42
    .line 540
    invoke-static {p2}, Lcom/qualcomm/wfd/WFDNative;->enableWfd(Lcom/qualcomm/wfd/WfdDevice;)Z

    #@45
    move-result v1

    #@46
    .line 541
    .local v1, ret:Z
    sget-object v4, Lcom/qualcomm/wfd/WfdEnums$SessionState;->INITIALIZED:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@48
    const/4 v5, -0x1

    #@49
    invoke-virtual {p0, v4, v5}, Lcom/qualcomm/wfd/service/SessionManagerService;->updateState(Lcom/qualcomm/wfd/WfdEnums$SessionState;I)V

    #@4c
    .line 542
    const-string v4, "SessionManagerService"

    #@4e
    new-instance v5, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    const-string v6, "Broadcasting WFD intent: "

    #@55
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v5

    #@59
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v5

    #@5d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v5

    #@61
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@64
    .line 543
    iget-object v4, p0, Lcom/qualcomm/wfd/service/SessionManagerService;->mContext:Landroid/content/Context;

    #@66
    invoke-virtual {v4, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@69
    .line 544
    if-eqz v1, :cond_6c

    #@6b
    const/4 v2, 0x0

    #@6c
    :cond_6c
    monitor-exit v3

    #@6d
    goto :goto_16

    #@6e
    .line 546
    .end local v0           #intent:Landroid/content/Intent;
    .end local v1           #ret:Z
    :cond_6e
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->ALREADY_INITIALIZED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@70
    invoke-virtual {v2}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@73
    move-result v2

    #@74
    monitor-exit v3
    :try_end_75
    .catchall {:try_start_38 .. :try_end_75} :catchall_35

    #@75
    goto :goto_16
.end method

.method public notify(Landroid/os/Bundle;I)V
    .registers 9
    .parameter "b"
    .parameter "sessionId"

    #@0
    .prologue
    .line 505
    sget-object v4, Lcom/qualcomm/wfd/service/SessionManagerService;->StaticLock:Ljava/lang/Object;

    #@2
    monitor-enter v4

    #@3
    .line 507
    :try_start_3
    sget-object v3, Lcom/qualcomm/wfd/service/SessionManagerService;->ActionListeners:Landroid/os/RemoteCallbackList;

    #@5
    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_25

    #@8
    move-result v0

    #@9
    .line 508
    .local v0, N:I
    const/4 v2, 0x0

    #@a
    .local v2, i:I
    :goto_a
    if-ge v2, v0, :cond_28

    #@c
    .line 510
    :try_start_c
    sget-object v3, Lcom/qualcomm/wfd/service/SessionManagerService;->ActionListeners:Landroid/os/RemoteCallbackList;

    #@e
    invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@11
    move-result-object v3

    #@12
    check-cast v3, Lcom/qualcomm/wfd/service/IWfdActionListener;

    #@14
    sget v5, Lcom/qualcomm/wfd/service/SessionManagerService;->SessionId:I

    #@16
    invoke-interface {v3, p1, v5}, Lcom/qualcomm/wfd/service/IWfdActionListener;->notify(Landroid/os/Bundle;I)V
    :try_end_19
    .catchall {:try_start_c .. :try_end_19} :catchall_25
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_19} :catch_1c

    #@19
    .line 508
    :goto_19
    add-int/lit8 v2, v2, 0x1

    #@1b
    goto :goto_a

    #@1c
    .line 511
    :catch_1c
    move-exception v1

    #@1d
    .line 512
    .local v1, e:Landroid/os/RemoteException;
    :try_start_1d
    const-string v3, "SessionManagerService"

    #@1f
    const-string v5, "Error sending notification to client, removing listener"

    #@21
    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    goto :goto_19

    #@25
    .line 516
    .end local v0           #N:I
    .end local v1           #e:Landroid/os/RemoteException;
    .end local v2           #i:I
    :catchall_25
    move-exception v3

    #@26
    monitor-exit v4
    :try_end_27
    .catchall {:try_start_1d .. :try_end_27} :catchall_25

    #@27
    throw v3

    #@28
    .line 515
    .restart local v0       #N:I
    .restart local v2       #i:I
    :cond_28
    :try_start_28
    sget-object v3, Lcom/qualcomm/wfd/service/SessionManagerService;->ActionListeners:Landroid/os/RemoteCallbackList;

    #@2a
    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    #@2d
    .line 516
    monitor-exit v4
    :try_end_2e
    .catchall {:try_start_28 .. :try_end_2e} :catchall_25

    #@2e
    .line 517
    return-void
.end method

.method public notifyEvent(Lcom/qualcomm/wfd/WfdEnums$WfdEvent;I)V
    .registers 10
    .parameter "event"
    .parameter "sessionId"

    #@0
    .prologue
    .line 400
    const-string v3, "SessionManagerService"

    #@2
    const-string v4, "notifyEvent()"

    #@4
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 401
    sget-object v4, Lcom/qualcomm/wfd/service/SessionManagerService;->StaticLock:Ljava/lang/Object;

    #@9
    monitor-enter v4

    #@a
    .line 402
    :try_start_a
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->UIBC_ENABLED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@c
    if-ne p1, v3, :cond_ba

    #@e
    .line 403
    const-string v3, "SessionManagerService"

    #@10
    new-instance v5, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v6, "UIBC called from "

    #@17
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v5

    #@1b
    sget-object v6, Lcom/qualcomm/wfd/service/SessionManagerService;->DeviceType:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@1d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v5

    #@21
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v5

    #@25
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_28
    .catchall {:try_start_a .. :try_end_28} :catchall_83

    #@28
    .line 405
    :try_start_28
    sget-object v3, Lcom/qualcomm/wfd/service/SessionManagerService;->UIBCMgr:Lcom/qualcomm/wfd/UIBCManager;

    #@2a
    if-nez v3, :cond_39

    #@2c
    .line 406
    new-instance v3, Lcom/qualcomm/wfd/UIBCManager;

    #@2e
    sget-object v5, Lcom/qualcomm/wfd/service/SessionManagerService;->DeviceType:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@30
    invoke-virtual {v5}, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->getCode()I

    #@33
    move-result v5

    #@34
    invoke-direct {v3, v5}, Lcom/qualcomm/wfd/UIBCManager;-><init>(I)V

    #@37
    sput-object v3, Lcom/qualcomm/wfd/service/SessionManagerService;->UIBCMgr:Lcom/qualcomm/wfd/UIBCManager;
    :try_end_39
    .catchall {:try_start_28 .. :try_end_39} :catchall_83
    .catch Ljava/lang/InstantiationException; {:try_start_28 .. :try_end_39} :catch_69
    .catch Ljava/lang/IllegalAccessException; {:try_start_28 .. :try_end_39} :catch_86
    .catch Ljava/lang/ClassNotFoundException; {:try_start_28 .. :try_end_39} :catch_a0

    #@39
    .line 417
    :cond_39
    :goto_39
    :try_start_39
    sget-object v3, Lcom/qualcomm/wfd/service/SessionManagerService;->UIBCMgr:Lcom/qualcomm/wfd/UIBCManager;

    #@3b
    invoke-virtual {v3}, Lcom/qualcomm/wfd/UIBCManager;->start()Z

    #@3e
    .line 418
    const/4 v3, 0x1

    #@3f
    sput-boolean v3, Lcom/qualcomm/wfd/service/SessionManagerService;->uibcEnabled:Z

    #@41
    .line 486
    :cond_41
    :goto_41
    sget-object v3, Lcom/qualcomm/wfd/service/SessionManagerService;->ActionListeners:Landroid/os/RemoteCallbackList;

    #@43
    if-eqz v3, :cond_1fc

    #@45
    .line 487
    const-string v3, "SessionManagerService"

    #@47
    const-string v5, "Sending notifyEvent() to listeners"

    #@49
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    .line 488
    sget-object v3, Lcom/qualcomm/wfd/service/SessionManagerService;->ActionListeners:Landroid/os/RemoteCallbackList;

    #@4e
    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
    :try_end_51
    .catchall {:try_start_39 .. :try_end_51} :catchall_83

    #@51
    move-result v0

    #@52
    .line 489
    .local v0, N:I
    const/4 v2, 0x0

    #@53
    .local v2, i:I
    :goto_53
    if-ge v2, v0, :cond_1f4

    #@55
    .line 491
    :try_start_55
    sget-object v3, Lcom/qualcomm/wfd/service/SessionManagerService;->ActionListeners:Landroid/os/RemoteCallbackList;

    #@57
    invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@5a
    move-result-object v3

    #@5b
    check-cast v3, Lcom/qualcomm/wfd/service/IWfdActionListener;

    #@5d
    invoke-virtual {p1}, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->ordinal()I

    #@60
    move-result v5

    #@61
    sget v6, Lcom/qualcomm/wfd/service/SessionManagerService;->SessionId:I

    #@63
    invoke-interface {v3, v5, v6}, Lcom/qualcomm/wfd/service/IWfdActionListener;->notifyEvent(II)V
    :try_end_66
    .catchall {:try_start_55 .. :try_end_66} :catchall_83
    .catch Landroid/os/RemoteException; {:try_start_55 .. :try_end_66} :catch_1ea

    #@66
    .line 489
    :goto_66
    add-int/lit8 v2, v2, 0x1

    #@68
    goto :goto_53

    #@69
    .line 408
    .end local v0           #N:I
    .end local v2           #i:I
    :catch_69
    move-exception v1

    #@6a
    .line 409
    .local v1, e:Ljava/lang/InstantiationException;
    :try_start_6a
    const-string v3, "SessionManagerService"

    #@6c
    new-instance v5, Ljava/lang/StringBuilder;

    #@6e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@71
    const-string v6, "InstantiationException: "

    #@73
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v5

    #@77
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v5

    #@7b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e
    move-result-object v5

    #@7f
    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@82
    goto :goto_39

    #@83
    .line 500
    .end local v1           #e:Ljava/lang/InstantiationException;
    :catchall_83
    move-exception v3

    #@84
    monitor-exit v4
    :try_end_85
    .catchall {:try_start_6a .. :try_end_85} :catchall_83

    #@85
    throw v3

    #@86
    .line 411
    :catch_86
    move-exception v1

    #@87
    .line 412
    .local v1, e:Ljava/lang/IllegalAccessException;
    :try_start_87
    const-string v3, "SessionManagerService"

    #@89
    new-instance v5, Ljava/lang/StringBuilder;

    #@8b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@8e
    const-string v6, "IllegalAccessException: "

    #@90
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v5

    #@94
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v5

    #@98
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9b
    move-result-object v5

    #@9c
    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@9f
    goto :goto_39

    #@a0
    .line 414
    .end local v1           #e:Ljava/lang/IllegalAccessException;
    :catch_a0
    move-exception v1

    #@a1
    .line 415
    .local v1, e:Ljava/lang/ClassNotFoundException;
    const-string v3, "SessionManagerService"

    #@a3
    new-instance v5, Ljava/lang/StringBuilder;

    #@a5
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@a8
    const-string v6, "ClassNotFoundException: "

    #@aa
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v5

    #@ae
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v5

    #@b2
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b5
    move-result-object v5

    #@b6
    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b9
    goto :goto_39

    #@ba
    .line 419
    .end local v1           #e:Ljava/lang/ClassNotFoundException;
    :cond_ba
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->UIBC_DISABLED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;
    :try_end_bc
    .catchall {:try_start_87 .. :try_end_bc} :catchall_83

    #@bc
    if-ne p1, v3, :cond_e2

    #@be
    .line 421
    :try_start_be
    sget-object v3, Lcom/qualcomm/wfd/service/SessionManagerService;->UIBCMgr:Lcom/qualcomm/wfd/UIBCManager;

    #@c0
    invoke-virtual {v3}, Lcom/qualcomm/wfd/UIBCManager;->stop()Z
    :try_end_c3
    .catchall {:try_start_be .. :try_end_c3} :catchall_83
    .catch Ljava/lang/NullPointerException; {:try_start_be .. :try_end_c3} :catch_c8

    #@c3
    .line 425
    :goto_c3
    const/4 v3, 0x0

    #@c4
    :try_start_c4
    sput-boolean v3, Lcom/qualcomm/wfd/service/SessionManagerService;->uibcEnabled:Z

    #@c6
    goto/16 :goto_41

    #@c8
    .line 422
    :catch_c8
    move-exception v1

    #@c9
    .line 423
    .local v1, e:Ljava/lang/NullPointerException;
    const-string v3, "SessionManagerService"

    #@cb
    new-instance v5, Ljava/lang/StringBuilder;

    #@cd
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@d0
    const-string v6, "NullPointerException: "

    #@d2
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v5

    #@d6
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v5

    #@da
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@dd
    move-result-object v5

    #@de
    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e1
    goto :goto_c3

    #@e2
    .line 426
    .end local v1           #e:Ljava/lang/NullPointerException;
    :cond_e2
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->PAUSE_START:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@e4
    if-ne p1, v3, :cond_f7

    #@e6
    .line 427
    const-string v3, "SessionManagerService"

    #@e8
    const-string v5, "PAUSE start"

    #@ea
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ed
    .line 428
    const/4 v3, 0x0

    #@ee
    invoke-direct {p0, v3}, Lcom/qualcomm/wfd/service/SessionManagerService;->broadcastWifiDisplayAudioIntent(Z)V

    #@f1
    .line 429
    const/4 v3, 0x0

    #@f2
    invoke-direct {p0, v3}, Lcom/qualcomm/wfd/service/SessionManagerService;->broadcastWifiDisplayVideoEnabled(Z)V

    #@f5
    goto/16 :goto_41

    #@f7
    .line 430
    :cond_f7
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->STANDBY_START:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@f9
    if-ne p1, v3, :cond_110

    #@fb
    .line 431
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$SessionState;->STANDING_BY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@fd
    sput-object v3, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@ff
    .line 432
    const-string v3, "SessionManagerService"

    #@101
    const-string v5, "STANDBY_START"

    #@103
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@106
    .line 434
    const/4 v3, 0x0

    #@107
    invoke-direct {p0, v3}, Lcom/qualcomm/wfd/service/SessionManagerService;->broadcastWifiDisplayAudioIntent(Z)V

    #@10a
    .line 435
    const/4 v3, 0x0

    #@10b
    invoke-direct {p0, v3}, Lcom/qualcomm/wfd/service/SessionManagerService;->broadcastWifiDisplayVideoEnabled(Z)V

    #@10e
    goto/16 :goto_41

    #@110
    .line 436
    :cond_110
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->PLAY_START:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@112
    if-ne p1, v3, :cond_121

    #@114
    .line 437
    const-string v3, "SessionManagerService"

    #@116
    const-string v5, "PLAY start"

    #@118
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@11b
    .line 438
    const/4 v3, 0x1

    #@11c
    invoke-direct {p0, v3}, Lcom/qualcomm/wfd/service/SessionManagerService;->broadcastWifiDisplayVideoEnabled(Z)V

    #@11f
    goto/16 :goto_41

    #@121
    .line 439
    :cond_121
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->TEARDOWN_START:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@123
    if-ne p1, v3, :cond_136

    #@125
    .line 440
    const-string v3, "SessionManagerService"

    #@127
    const-string v5, "TEARDOWN start"

    #@129
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@12c
    .line 441
    const/4 v3, 0x0

    #@12d
    invoke-direct {p0, v3}, Lcom/qualcomm/wfd/service/SessionManagerService;->broadcastWifiDisplayAudioIntent(Z)V

    #@130
    .line 442
    const/4 v3, 0x0

    #@131
    invoke-direct {p0, v3}, Lcom/qualcomm/wfd/service/SessionManagerService;->broadcastWifiDisplayVideoEnabled(Z)V

    #@134
    goto/16 :goto_41

    #@136
    .line 443
    :cond_136
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->HDCP_CONNECT_SUCCESS:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@138
    if-ne p1, v3, :cond_146

    #@13a
    .line 444
    const-string v3, "SessionManagerService"

    #@13c
    const-string v5, "HDCP Connect Success"

    #@13e
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@141
    .line 445
    const/4 v3, 0x1

    #@142
    sput-boolean v3, Lcom/qualcomm/wfd/service/SessionManagerService;->IS_HDCP_ENABLED:Z

    #@144
    goto/16 :goto_41

    #@146
    .line 446
    :cond_146
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->HDCP_CONNECT_FAIL:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@148
    if-ne p1, v3, :cond_156

    #@14a
    .line 447
    const-string v3, "SessionManagerService"

    #@14c
    const-string v5, "HDCP Connect Fail"

    #@14e
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@151
    .line 448
    const/4 v3, 0x0

    #@152
    sput-boolean v3, Lcom/qualcomm/wfd/service/SessionManagerService;->IS_HDCP_ENABLED:Z

    #@154
    goto/16 :goto_41

    #@156
    .line 451
    :cond_156
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->NETWORK_RUNTIME_ERROR:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@158
    if-ne p1, v3, :cond_163

    #@15a
    .line 452
    const-string v3, "SessionManagerService"

    #@15c
    const-string v5, "NETWORK_RUNTIME_ERROR was ignored!"

    #@15e
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@161
    .line 453
    monitor-exit v4

    #@162
    .line 501
    :goto_162
    return-void

    #@163
    .line 457
    :cond_163
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->HDCP_RUNTIME_ERROR:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@165
    if-ne p1, v3, :cond_173

    #@167
    .line 458
    const-string v3, "SessionManagerService"

    #@169
    const-string v5, "HDCP_RUNTIME_ERROR occured!"

    #@16b
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16e
    .line 459
    const/4 v3, 0x0

    #@16f
    sput-boolean v3, Lcom/qualcomm/wfd/service/SessionManagerService;->IS_HDCP_ENABLED:Z

    #@171
    .line 460
    monitor-exit v4

    #@172
    goto :goto_162

    #@173
    .line 462
    :cond_173
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->HDCP_ENFORCE_FAIL:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@175
    if-ne p1, v3, :cond_189

    #@177
    .line 463
    const-string v3, "SessionManagerService"

    #@179
    const-string v5, "HDCP Unsupported by Peer. Fail"

    #@17b
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@17e
    .line 464
    const/4 v3, 0x0

    #@17f
    sput-boolean v3, Lcom/qualcomm/wfd/service/SessionManagerService;->IS_HDCP_ENABLED:Z

    #@181
    .line 465
    sget v3, Lcom/qualcomm/wfd/service/SessionManagerService;->SessionId:I

    #@183
    const/4 v5, 0x1

    #@184
    invoke-static {v3, v5}, Lcom/qualcomm/wfd/WFDNative;->teardown(IZ)V

    #@187
    goto/16 :goto_41

    #@189
    .line 466
    :cond_189
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->VIDEO_RUNTIME_ERROR:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@18b
    if-eq p1, v3, :cond_1a9

    #@18d
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->AUDIO_RUNTIME_ERROR:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@18f
    if-eq p1, v3, :cond_1a9

    #@191
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->HDCP_RUNTIME_ERROR:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@193
    if-eq p1, v3, :cond_1a9

    #@195
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->VIDEO_CONFIGURE_FAILURE:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@197
    if-eq p1, v3, :cond_1a9

    #@199
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->AUDIO_CONFIGURE_FAILURE:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@19b
    if-eq p1, v3, :cond_1a9

    #@19d
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->NETWORK_RUNTIME_ERROR:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@19f
    if-eq p1, v3, :cond_1a9

    #@1a1
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->NETWORK_CONFIGURE_FAILURE:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@1a3
    if-eq p1, v3, :cond_1a9

    #@1a5
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->START_SESSION_FAIL:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@1a7
    if-ne p1, v3, :cond_1c6

    #@1a9
    .line 474
    :cond_1a9
    const-string v3, "SessionManagerService"

    #@1ab
    new-instance v5, Ljava/lang/StringBuilder;

    #@1ad
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1b0
    const-string v6, "Error event received:"

    #@1b2
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b5
    move-result-object v5

    #@1b6
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1b9
    move-result-object v5

    #@1ba
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1bd
    move-result-object v5

    #@1be
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c1
    .line 475
    invoke-virtual {p0}, Lcom/qualcomm/wfd/service/SessionManagerService;->teardown()I

    #@1c4
    goto/16 :goto_41

    #@1c6
    .line 476
    :cond_1c6
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->RTP_TRANSPORT_NEGOTIATED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@1c8
    if-ne p1, v3, :cond_1d6

    #@1ca
    .line 477
    const-string v3, "SessionManagerService"

    #@1cc
    const-string v5, "RTP transport is changed successfully"

    #@1ce
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d1
    .line 478
    const/4 v3, 0x1

    #@1d2
    sput-boolean v3, Lcom/qualcomm/wfd/service/SessionManagerService;->IS_RTP_TRANSPORT_NEGOTIATED:Z

    #@1d4
    goto/16 :goto_41

    #@1d6
    .line 479
    :cond_1d6
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->AUDIOPROXY_CLOSED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@1d8
    if-ne p1, v3, :cond_1e0

    #@1da
    .line 480
    const/4 v3, 0x0

    #@1db
    invoke-direct {p0, v3}, Lcom/qualcomm/wfd/service/SessionManagerService;->broadcastWifiDisplayAudioIntent(Z)V

    #@1de
    goto/16 :goto_41

    #@1e0
    .line 481
    :cond_1e0
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->AUDIOPROXY_OPENED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@1e2
    if-ne p1, v3, :cond_41

    #@1e4
    .line 482
    const/4 v3, 0x1

    #@1e5
    invoke-direct {p0, v3}, Lcom/qualcomm/wfd/service/SessionManagerService;->broadcastWifiDisplayAudioIntent(Z)V

    #@1e8
    goto/16 :goto_41

    #@1ea
    .line 492
    .restart local v0       #N:I
    .restart local v2       #i:I
    :catch_1ea
    move-exception v1

    #@1eb
    .line 493
    .local v1, e:Landroid/os/RemoteException;
    const-string v3, "SessionManagerService"

    #@1ed
    const-string v5, "Error sending notification to client, removing listener"

    #@1ef
    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1f2
    goto/16 :goto_66

    #@1f4
    .line 496
    .end local v1           #e:Landroid/os/RemoteException;
    :cond_1f4
    sget-object v3, Lcom/qualcomm/wfd/service/SessionManagerService;->ActionListeners:Landroid/os/RemoteCallbackList;

    #@1f6
    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    #@1f9
    .line 500
    .end local v0           #N:I
    .end local v2           #i:I
    :goto_1f9
    monitor-exit v4

    #@1fa
    goto/16 :goto_162

    #@1fc
    .line 498
    :cond_1fc
    const-string v3, "SessionManagerService"

    #@1fe
    const-string v5, "notifyEvent: Remote callback list is null"

    #@200
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_203
    .catchall {:try_start_c4 .. :try_end_203} :catchall_83

    #@203
    goto :goto_1f9
.end method

.method public pause()I
    .registers 3

    #@0
    .prologue
    .line 706
    const-string v0, "SessionManagerService"

    #@2
    const-string v1, "pause()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 707
    const/4 v0, 0x0

    #@8
    invoke-direct {p0, v0}, Lcom/qualcomm/wfd/service/SessionManagerService;->internalPause(Z)I

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public pause_transit()I
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 756
    const-string v1, "SessionManagerService"

    #@3
    const-string v2, "pause_transit()"

    #@5
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 757
    sget-object v1, Lcom/qualcomm/wfd/service/SessionManagerService;->StaticLock:Ljava/lang/Object;

    #@a
    monitor-enter v1

    #@b
    .line 758
    :try_start_b
    sget-boolean v2, Lcom/qualcomm/wfd/service/SessionManagerService;->IS_HDMI_CABLE_CONNECTED:Z

    #@d
    if-eqz v2, :cond_17

    #@f
    .line 759
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->HDMI_CABLE_CONNECTED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@11
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@14
    move-result v0

    #@15
    monitor-exit v1

    #@16
    .line 771
    :goto_16
    return v0

    #@17
    .line 761
    :cond_17
    sget-object v2, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@19
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PAUSING:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@1b
    if-ne v2, v3, :cond_29

    #@1d
    .line 762
    const-string v2, "SessionManagerService"

    #@1f
    const-string v3, "Already in the middle of PAUSING"

    #@21
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 763
    monitor-exit v1

    #@25
    goto :goto_16

    #@26
    .line 773
    :catchall_26
    move-exception v0

    #@27
    monitor-exit v1
    :try_end_28
    .catchall {:try_start_b .. :try_end_28} :catchall_26

    #@28
    throw v0

    #@29
    .line 765
    :cond_29
    :try_start_29
    sget-object v2, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@2b
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PLAY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@2d
    if-ne v2, v3, :cond_3a

    #@2f
    .line 766
    sget v2, Lcom/qualcomm/wfd/service/SessionManagerService;->SessionId:I

    #@31
    invoke-static {v2}, Lcom/qualcomm/wfd/WFDNative;->pause_transit(I)V

    #@34
    .line 767
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PAUSING:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@36
    sput-object v2, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@38
    .line 768
    monitor-exit v1

    #@39
    goto :goto_16

    #@3a
    .line 770
    :cond_3a
    const-string v0, "SessionManagerService"

    #@3c
    const-string v2, "Session state is not PLAY"

    #@3e
    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    .line 771
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->INCORRECT_STATE_FOR_OPERATION:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@43
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@46
    move-result v0

    #@47
    monitor-exit v1
    :try_end_48
    .catchall {:try_start_29 .. :try_end_48} :catchall_26

    #@48
    goto :goto_16
.end method

.method public play()I
    .registers 3

    #@0
    .prologue
    .line 678
    const-string v0, "SessionManagerService"

    #@2
    const-string v1, "play()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 679
    const/4 v0, 0x0

    #@8
    invoke-direct {p0, v0}, Lcom/qualcomm/wfd/service/SessionManagerService;->internalPlay(Z)I

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public play_transit()I
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 734
    const-string v1, "SessionManagerService"

    #@3
    const-string v2, "play_transit()"

    #@5
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 735
    sget-object v1, Lcom/qualcomm/wfd/service/SessionManagerService;->StaticLock:Ljava/lang/Object;

    #@a
    monitor-enter v1

    #@b
    .line 736
    :try_start_b
    sget-boolean v2, Lcom/qualcomm/wfd/service/SessionManagerService;->IS_HDMI_CABLE_CONNECTED:Z

    #@d
    if-eqz v2, :cond_17

    #@f
    .line 737
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->HDMI_CABLE_CONNECTED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@11
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@14
    move-result v0

    #@15
    monitor-exit v1

    #@16
    .line 749
    :goto_16
    return v0

    #@17
    .line 739
    :cond_17
    sget-object v2, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@19
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PLAYING:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@1b
    if-ne v2, v3, :cond_29

    #@1d
    .line 740
    const-string v2, "SessionManagerService"

    #@1f
    const-string v3, "Already in the middle of PLAYING"

    #@21
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 741
    monitor-exit v1

    #@25
    goto :goto_16

    #@26
    .line 751
    :catchall_26
    move-exception v0

    #@27
    monitor-exit v1
    :try_end_28
    .catchall {:try_start_b .. :try_end_28} :catchall_26

    #@28
    throw v0

    #@29
    .line 743
    :cond_29
    :try_start_29
    sget-object v2, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@2b
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ESTABLISHED:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@2d
    if-eq v2, v3, :cond_35

    #@2f
    sget-object v2, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@31
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PAUSE:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@33
    if-ne v2, v3, :cond_40

    #@35
    .line 744
    :cond_35
    sget v2, Lcom/qualcomm/wfd/service/SessionManagerService;->SessionId:I

    #@37
    invoke-static {v2}, Lcom/qualcomm/wfd/WFDNative;->play_transit(I)V

    #@3a
    .line 745
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PLAYING:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@3c
    sput-object v2, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@3e
    .line 746
    monitor-exit v1

    #@3f
    goto :goto_16

    #@40
    .line 748
    :cond_40
    const-string v0, "SessionManagerService"

    #@42
    const-string v2, "Session state is not ESTABLISHED or PAUSE"

    #@44
    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    .line 749
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->INCORRECT_STATE_FOR_OPERATION:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@49
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@4c
    move-result v0

    #@4d
    monitor-exit v1
    :try_end_4e
    .catchall {:try_start_29 .. :try_end_4e} :catchall_26

    #@4e
    goto :goto_16
.end method

.method public sendEvent(Landroid/view/InputEvent;)I
    .registers 5
    .parameter

    #@0
    .prologue
    .line 1188
    sget-boolean v0, Lcom/qualcomm/wfd/service/SessionManagerService;->uibcEnabled:Z

    #@2
    const/4 v1, 0x1

    #@3
    if-ne v0, v1, :cond_37

    #@5
    .line 1190
    sget-object v1, Lcom/qualcomm/wfd/service/SessionManagerService;->StaticLock:Ljava/lang/Object;

    #@7
    monitor-enter v1

    #@8
    .line 1191
    :try_start_8
    sget-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->DeviceType:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@a
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->SOURCE:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@c
    if-ne v0, v2, :cond_1d

    #@e
    .line 1192
    const-string v0, "SessionManagerService"

    #@10
    const-string v2, "Device type is source. Ignoring sendEvent"

    #@12
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    .line 1193
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->NOT_SINK_DEVICE:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@17
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@1a
    move-result v0

    #@1b
    monitor-exit v1

    #@1c
    .line 1204
    :goto_1c
    return v0

    #@1d
    .line 1195
    :cond_1d
    monitor-exit v1
    :try_end_1e
    .catchall {:try_start_8 .. :try_end_1e} :catchall_2d

    #@1e
    .line 1196
    instance-of v0, p1, Landroid/view/KeyEvent;

    #@20
    if-nez v0, :cond_26

    #@22
    instance-of v0, p1, Landroid/view/MotionEvent;

    #@24
    if-eqz v0, :cond_30

    #@26
    .line 1197
    :cond_26
    sget-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->UIBCMgr:Lcom/qualcomm/wfd/UIBCManager;

    #@28
    invoke-virtual {v0, p1}, Lcom/qualcomm/wfd/UIBCManager;->addUIBCEvent(Landroid/view/InputEvent;)V

    #@2b
    .line 1204
    :goto_2b
    const/4 v0, 0x0

    #@2c
    goto :goto_1c

    #@2d
    .line 1195
    :catchall_2d
    move-exception v0

    #@2e
    :try_start_2e
    monitor-exit v1
    :try_end_2f
    .catchall {:try_start_2e .. :try_end_2f} :catchall_2d

    #@2f
    throw v0

    #@30
    .line 1199
    :cond_30
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->INVALID_ARG:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@32
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@35
    move-result v0

    #@36
    goto :goto_1c

    #@37
    .line 1202
    :cond_37
    const-string v0, "SessionManagerService"

    #@39
    const-string v1, "UIBC connection is not established yet. Ignoring sendEvents"

    #@3b
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    goto :goto_2b
.end method

.method public setAvPlaybackMode(I)I
    .registers 10
    .parameter

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1138
    const-string v1, "SessionManagerService"

    #@3
    new-instance v2, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v3, "setAvPlaybackMode mode: "

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 1139
    sget-object v4, Lcom/qualcomm/wfd/service/SessionManagerService;->StaticLock:Ljava/lang/Object;

    #@1b
    monitor-enter v4

    #@1c
    .line 1140
    :try_start_1c
    sget-object v1, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@1e
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$SessionState;->INVALID:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@20
    if-eq v1, v2, :cond_28

    #@22
    sget-object v1, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@24
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$SessionState;->INITIALIZED:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@26
    if-ne v1, v2, :cond_76

    #@28
    .line 1142
    :cond_28
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;->AUDIO_VIDEO:Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;

    #@2a
    .line 1143
    invoke-static {}, Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;->values()[Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;

    #@2d
    move-result-object v5

    #@2e
    array-length v6, v5

    #@2f
    move v3, v0

    #@30
    :goto_30
    if-ge v3, v6, :cond_85

    #@32
    aget-object v1, v5, v3

    #@34
    .line 1144
    invoke-virtual {v1}, Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;->ordinal()I

    #@37
    move-result v7

    #@38
    if-ne p1, v7, :cond_5d

    #@3a
    .line 1146
    const/4 v2, 0x1

    #@3b
    .line 1150
    :goto_3b
    if-nez v2, :cond_61

    #@3d
    .line 1151
    const-string v0, "SessionManagerService"

    #@3f
    new-instance v1, Ljava/lang/StringBuilder;

    #@41
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@44
    const-string v2, "Invalid AV playback mode:"

    #@46
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v1

    #@4a
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v1

    #@4e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v1

    #@52
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@55
    .line 1152
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->INVALID_ARG:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@57
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@5a
    move-result v0

    #@5b
    monitor-exit v4

    #@5c
    .line 1162
    :goto_5c
    return v0

    #@5d
    .line 1143
    :cond_5d
    add-int/lit8 v1, v3, 0x1

    #@5f
    move v3, v1

    #@60
    goto :goto_30

    #@61
    .line 1154
    :cond_61
    invoke-static {p1}, Lcom/qualcomm/wfd/WFDNative;->setAvPlaybackMode(I)Z

    #@64
    move-result v2

    #@65
    if-eqz v2, :cond_6e

    #@67
    .line 1155
    sput-object v1, Lcom/qualcomm/wfd/service/SessionManagerService;->PLAYBACK_MODE:Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;

    #@69
    .line 1156
    monitor-exit v4

    #@6a
    goto :goto_5c

    #@6b
    .line 1164
    :catchall_6b
    move-exception v0

    #@6c
    monitor-exit v4
    :try_end_6d
    .catchall {:try_start_1c .. :try_end_6d} :catchall_6b

    #@6d
    throw v0

    #@6e
    .line 1158
    :cond_6e
    :try_start_6e
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->UNKNOWN:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@70
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@73
    move-result v0

    #@74
    monitor-exit v4

    #@75
    goto :goto_5c

    #@76
    .line 1161
    :cond_76
    const-string v0, "SessionManagerService"

    #@78
    const-string v1, "Session state is not INVALID or INITIALIZED"

    #@7a
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7d
    .line 1162
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->INCORRECT_STATE_FOR_OPERATION:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@7f
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@82
    move-result v0

    #@83
    monitor-exit v4
    :try_end_84
    .catchall {:try_start_6e .. :try_end_84} :catchall_6b

    #@84
    goto :goto_5c

    #@85
    :cond_85
    move-object v1, v2

    #@86
    move v2, v0

    #@87
    goto :goto_3b
.end method

.method public setBitrate(I)I
    .registers 4
    .parameter

    #@0
    .prologue
    .line 1057
    const-string v0, "SessionManagerService"

    #@2
    const-string v1, "setBitrate()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1058
    invoke-static {p1}, Lcom/qualcomm/wfd/WFDNative;->setBitrate(I)V

    #@a
    .line 1059
    const/4 v0, 0x0

    #@b
    return v0
.end method

.method public setDeviceType(I)I
    .registers 9
    .parameter "type"

    #@0
    .prologue
    .line 659
    const-string v4, "SessionManagerService"

    #@2
    new-instance v5, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v6, "setDeviceType(): type - "

    #@9
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v5

    #@d
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v5

    #@11
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v5

    #@15
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 660
    sget-object v5, Lcom/qualcomm/wfd/service/SessionManagerService;->StaticLock:Ljava/lang/Object;

    #@1a
    monitor-enter v5

    #@1b
    .line 661
    :try_start_1b
    sget-object v4, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@1d
    sget-object v6, Lcom/qualcomm/wfd/WfdEnums$SessionState;->INVALID:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@1f
    if-eq v4, v6, :cond_27

    #@21
    sget-object v4, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@23
    sget-object v6, Lcom/qualcomm/wfd/WfdEnums$SessionState;->INITIALIZED:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@25
    if-ne v4, v6, :cond_4a

    #@27
    .line 662
    :cond_27
    invoke-static {}, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->values()[Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@2a
    move-result-object v0

    #@2b
    .local v0, arr$:[Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;
    array-length v3, v0

    #@2c
    .local v3, len$:I
    const/4 v2, 0x0

    #@2d
    .local v2, i$:I
    :goto_2d
    if-ge v2, v3, :cond_3f

    #@2f
    aget-object v1, v0, v2

    #@31
    .line 663
    .local v1, each:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;
    invoke-virtual {v1}, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->getCode()I

    #@34
    move-result v4

    #@35
    if-ne v4, p1, :cond_3c

    #@37
    .line 664
    sput-object v1, Lcom/qualcomm/wfd/service/SessionManagerService;->DeviceType:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@39
    .line 665
    const/4 v4, 0x0

    #@3a
    monitor-exit v5

    #@3b
    .line 671
    .end local v0           #arr$:[Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;
    .end local v1           #each:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;
    .end local v2           #i$:I
    .end local v3           #len$:I
    :goto_3b
    return v4

    #@3c
    .line 662
    .restart local v0       #arr$:[Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;
    .restart local v1       #each:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;
    .restart local v2       #i$:I
    .restart local v3       #len$:I
    :cond_3c
    add-int/lit8 v2, v2, 0x1

    #@3e
    goto :goto_2d

    #@3f
    .line 668
    .end local v1           #each:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;
    :cond_3f
    sget-object v4, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->INVALID_ARG:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@41
    invoke-virtual {v4}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@44
    move-result v4

    #@45
    monitor-exit v5

    #@46
    goto :goto_3b

    #@47
    .line 673
    .end local v0           #arr$:[Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;
    .end local v2           #i$:I
    .end local v3           #len$:I
    :catchall_47
    move-exception v4

    #@48
    monitor-exit v5
    :try_end_49
    .catchall {:try_start_1b .. :try_end_49} :catchall_47

    #@49
    throw v4

    #@4a
    .line 670
    :cond_4a
    :try_start_4a
    const-string v4, "SessionManagerService"

    #@4c
    const-string v6, "Session state is not INVALID or INITIALIZED"

    #@4e
    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@51
    .line 671
    sget-object v4, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->INCORRECT_STATE_FOR_OPERATION:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@53
    invoke-virtual {v4}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@56
    move-result v4

    #@57
    monitor-exit v5
    :try_end_58
    .catchall {:try_start_4a .. :try_end_58} :catchall_47

    #@58
    goto :goto_3b
.end method

.method public setNegotiatedCapabilities(Landroid/os/Bundle;)I
    .registers 4
    .parameter

    #@0
    .prologue
    .line 855
    const-string v0, "SessionManagerService"

    #@2
    const-string v1, "setNegotiatedCapabilities()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 856
    sget-boolean v0, Lcom/qualcomm/wfd/service/SessionManagerService;->IS_HDMI_CABLE_CONNECTED:Z

    #@9
    if-eqz v0, :cond_12

    #@b
    .line 857
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->HDMI_CABLE_CONNECTED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@d
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@10
    move-result v0

    #@11
    .line 860
    :goto_11
    return v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method public setResolution(II)I
    .registers 10
    .parameter
    .parameter

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1000
    const-string v1, "SessionManagerService"

    #@3
    const-string v2, "setResolution()"

    #@5
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 1001
    sget-object v1, Lcom/qualcomm/wfd/service/SessionManagerService;->StaticLock:Ljava/lang/Object;

    #@a
    monitor-enter v1

    #@b
    .line 1003
    :try_start_b
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_CEA_RESOLUTIONS_BITMAP:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@d
    invoke-virtual {v2}, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->ordinal()I

    #@10
    move-result v2

    #@11
    if-lt p1, v2, :cond_61

    #@13
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_VESA_RESOLUTIONS_BITMAP:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@15
    invoke-virtual {v2}, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->ordinal()I

    #@18
    move-result v2

    #@19
    if-gt p1, v2, :cond_61

    #@1b
    .line 1004
    invoke-static {}, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->values()[Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@1e
    move-result-object v2

    #@1f
    aget-object v2, v2, p1

    #@21
    .line 1005
    sget-object v3, Lcom/qualcomm/wfd/service/SessionManagerService$2;->$SwitchMap$com$qualcomm$wfd$WfdEnums$CapabilityType:[I

    #@23
    invoke-virtual {v2}, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->ordinal()I

    #@26
    move-result v2

    #@27
    aget v2, v3, v2

    #@29
    packed-switch v2, :pswitch_data_f0

    #@2c
    .line 1022
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->INVALID_ARG:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@2e
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@31
    move-result v0

    #@32
    monitor-exit v1

    #@33
    .line 1051
    :goto_33
    return v0

    #@34
    .line 1007
    :pswitch_34
    invoke-static {p2}, Lcom/qualcomm/wfd/WfdEnums;->isCeaResolution(I)Z

    #@37
    move-result v2

    #@38
    if-nez v2, :cond_70

    #@3a
    .line 1008
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->INVALID_ARG:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@3c
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@3f
    move-result v0

    #@40
    monitor-exit v1

    #@41
    goto :goto_33

    #@42
    .line 1052
    :catchall_42
    move-exception v0

    #@43
    monitor-exit v1
    :try_end_44
    .catchall {:try_start_b .. :try_end_44} :catchall_42

    #@44
    throw v0

    #@45
    .line 1012
    :pswitch_45
    :try_start_45
    invoke-static {p2}, Lcom/qualcomm/wfd/WfdEnums;->isHhResolution(I)Z

    #@48
    move-result v2

    #@49
    if-nez v2, :cond_70

    #@4b
    .line 1013
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->INVALID_ARG:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@4d
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@50
    move-result v0

    #@51
    monitor-exit v1

    #@52
    goto :goto_33

    #@53
    .line 1017
    :pswitch_53
    invoke-static {p2}, Lcom/qualcomm/wfd/WfdEnums;->isVesaResolution(I)Z

    #@56
    move-result v2

    #@57
    if-nez v2, :cond_70

    #@59
    .line 1018
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->INVALID_ARG:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@5b
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@5e
    move-result v0

    #@5f
    monitor-exit v1

    #@60
    goto :goto_33

    #@61
    .line 1025
    :cond_61
    const-string v0, "SessionManagerService"

    #@63
    const-string v2, "Unsupported formatType"

    #@65
    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@68
    .line 1026
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->INVALID_ARG:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@6a
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@6d
    move-result v0

    #@6e
    monitor-exit v1

    #@6f
    goto :goto_33

    #@70
    .line 1028
    :cond_70
    sget-object v2, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@72
    .line 1029
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PLAY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@74
    if-ne v2, v3, :cond_9d

    #@76
    .line 1030
    const/4 v3, 0x0

    #@77
    invoke-direct {p0, v3}, Lcom/qualcomm/wfd/service/SessionManagerService;->broadcastWifiDisplayAudioIntent(Z)V

    #@7a
    .line 1031
    const/4 v3, 0x1

    #@7b
    invoke-direct {p0, v3}, Lcom/qualcomm/wfd/service/SessionManagerService;->internalPause(Z)I

    #@7e
    move-result v3

    #@7f
    .line 1032
    const-string v4, "SessionManagerService"

    #@81
    new-instance v5, Ljava/lang/StringBuilder;

    #@83
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@86
    const-string v6, "internalPause ret = "

    #@88
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v5

    #@8c
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v3

    #@90
    const-string v5, ", continuing setResolution"

    #@92
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v3

    #@96
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@99
    move-result-object v3

    #@9a
    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9d
    .line 1034
    :cond_9d
    invoke-static {}, Lcom/qualcomm/wfd/WfdEnums;->getResParams()[I

    #@a0
    move-result-object v3

    #@a1
    .line 1035
    invoke-static {p1, p2, v3}, Lcom/qualcomm/wfd/WFDNative;->setResolution(II[I)Z

    #@a4
    move-result v3

    #@a5
    if-nez v3, :cond_ae

    #@a7
    .line 1036
    const-string v3, "SessionManagerService"

    #@a9
    const-string v4, "Setting new resolution failed!"

    #@ab
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ae
    .line 1038
    :cond_ae
    sget-object v3, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@b0
    sget-object v4, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PAUSE:Lcom/qualcomm/wfd/WfdEnums$SessionState;
    :try_end_b2
    .catchall {:try_start_45 .. :try_end_b2} :catchall_42

    #@b2
    if-eq v3, v4, :cond_bb

    #@b4
    .line 1040
    :try_start_b4
    sget-object v3, Lcom/qualcomm/wfd/service/SessionManagerService;->StaticLock:Ljava/lang/Object;

    #@b6
    const-wide/16 v4, 0x1388

    #@b8
    invoke-virtual {v3, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_bb
    .catchall {:try_start_b4 .. :try_end_bb} :catchall_42
    .catch Ljava/lang/InterruptedException; {:try_start_b4 .. :try_end_bb} :catch_df

    #@bb
    .line 1047
    :cond_bb
    :try_start_bb
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PLAY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@bd
    if-ne v2, v3, :cond_dc

    #@bf
    .line 1048
    const/4 v2, 0x1

    #@c0
    invoke-direct {p0, v2}, Lcom/qualcomm/wfd/service/SessionManagerService;->internalPlay(Z)I

    #@c3
    move-result v2

    #@c4
    .line 1049
    const-string v3, "SessionManagerService"

    #@c6
    new-instance v4, Ljava/lang/StringBuilder;

    #@c8
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@cb
    const-string v5, "internalPlay ret = "

    #@cd
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d0
    move-result-object v4

    #@d1
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d4
    move-result-object v2

    #@d5
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d8
    move-result-object v2

    #@d9
    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@dc
    .line 1051
    :cond_dc
    monitor-exit v1

    #@dd
    goto/16 :goto_33

    #@df
    .line 1041
    :catch_df
    move-exception v0

    #@e0
    .line 1042
    const-string v2, "SessionManagerService"

    #@e2
    const-string v3, "Wait for PAUSE interrupted"

    #@e4
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@e7
    .line 1043
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->UNKNOWN:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@e9
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@ec
    move-result v0

    #@ed
    monitor-exit v1
    :try_end_ee
    .catchall {:try_start_bb .. :try_end_ee} :catchall_42

    #@ee
    goto/16 :goto_33

    #@f0
    .line 1005
    :pswitch_data_f0
    .packed-switch 0x1
        :pswitch_34
        :pswitch_45
        :pswitch_53
    .end packed-switch
.end method

.method public setRtpTransport(III)I
    .registers 11
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v0, 0x0

    #@2
    .line 1064
    sget-object v3, Lcom/qualcomm/wfd/service/SessionManagerService;->StaticLock:Ljava/lang/Object;

    #@4
    monitor-enter v3

    #@5
    .line 1066
    :try_start_5
    invoke-static {}, Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;->values()[Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;

    #@8
    move-result-object v4

    #@9
    array-length v5, v4

    #@a
    move v2, v0

    #@b
    :goto_b
    if-ge v2, v5, :cond_e2

    #@d
    aget-object v6, v4, v2

    #@f
    .line 1067
    invoke-virtual {v6}, Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;->ordinal()I

    #@12
    move-result v6

    #@13
    if-ne p1, v6, :cond_37

    #@15
    .line 1072
    :goto_15
    if-nez v1, :cond_3a

    #@17
    .line 1073
    const-string v0, "SessionManagerService"

    #@19
    new-instance v1, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v2, "Invalid transport type:"

    #@20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@27
    move-result-object v1

    #@28
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v1

    #@2c
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 1074
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->INVALID_ARG:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@31
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@34
    move-result v0

    #@35
    monitor-exit v3

    #@36
    .line 1113
    :goto_36
    return v0

    #@37
    .line 1066
    :cond_37
    add-int/lit8 v2, v2, 0x1

    #@39
    goto :goto_b

    #@3a
    .line 1078
    :cond_3a
    const/4 v1, 0x0

    #@3b
    sput-boolean v1, Lcom/qualcomm/wfd/service/SessionManagerService;->IS_RTP_TRANSPORT_NEGOTIATED:Z

    #@3d
    .line 1079
    invoke-static {p1, p2, p3}, Lcom/qualcomm/wfd/WFDNative;->negotiateRtpTransport(III)V
    :try_end_40
    .catchall {:try_start_5 .. :try_end_40} :catchall_5a

    #@40
    .line 1081
    :try_start_40
    const-string v1, "SessionManagerService"

    #@42
    const-string v2, "Wait for RTSP negotiation for new RTP transport"

    #@44
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    .line 1082
    sget-object v1, Lcom/qualcomm/wfd/service/SessionManagerService;->StaticLock:Ljava/lang/Object;

    #@49
    const-wide/16 v4, 0x1388

    #@4b
    invoke-virtual {v1, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_4e
    .catchall {:try_start_40 .. :try_end_4e} :catchall_5a
    .catch Ljava/lang/InterruptedException; {:try_start_40 .. :try_end_4e} :catch_5d

    #@4e
    .line 1087
    :try_start_4e
    sget-boolean v1, Lcom/qualcomm/wfd/service/SessionManagerService;->IS_RTP_TRANSPORT_NEGOTIATED:Z

    #@50
    if-nez v1, :cond_6d

    #@52
    .line 1088
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->UNKNOWN:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@54
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@57
    move-result v0

    #@58
    monitor-exit v3

    #@59
    goto :goto_36

    #@5a
    .line 1114
    :catchall_5a
    move-exception v0

    #@5b
    monitor-exit v3
    :try_end_5c
    .catchall {:try_start_4e .. :try_end_5c} :catchall_5a

    #@5c
    throw v0

    #@5d
    .line 1083
    :catch_5d
    move-exception v0

    #@5e
    .line 1084
    :try_start_5e
    const-string v1, "SessionManagerService"

    #@60
    const-string v2, "setRtpTransport interrupted"

    #@62
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@65
    .line 1085
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->UNKNOWN:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@67
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@6a
    move-result v0

    #@6b
    monitor-exit v3

    #@6c
    goto :goto_36

    #@6d
    .line 1090
    :cond_6d
    const/4 v1, 0x0

    #@6e
    sput-boolean v1, Lcom/qualcomm/wfd/service/SessionManagerService;->IS_RTP_TRANSPORT_NEGOTIATED:Z

    #@70
    .line 1092
    sget-object v1, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@72
    .line 1093
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PLAY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@74
    if-ne v1, v2, :cond_aa

    #@76
    .line 1094
    const/4 v2, 0x0

    #@77
    invoke-direct {p0, v2}, Lcom/qualcomm/wfd/service/SessionManagerService;->broadcastWifiDisplayAudioIntent(Z)V

    #@7a
    .line 1095
    const/4 v2, 0x1

    #@7b
    invoke-direct {p0, v2}, Lcom/qualcomm/wfd/service/SessionManagerService;->internalPause(Z)I

    #@7e
    move-result v2

    #@7f
    .line 1096
    const-string v4, "SessionManagerService"

    #@81
    new-instance v5, Ljava/lang/StringBuilder;

    #@83
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@86
    const-string v6, "internalPause ret = "

    #@88
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v5

    #@8c
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v2

    #@90
    const-string v5, ", continuing setRtpTransport"

    #@92
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v2

    #@96
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@99
    move-result-object v2

    #@9a
    invoke-static {v4, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9d
    .line 1097
    sget-object v2, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@9f
    sget-object v4, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PAUSE:Lcom/qualcomm/wfd/WfdEnums$SessionState;
    :try_end_a1
    .catchall {:try_start_5e .. :try_end_a1} :catchall_5a

    #@a1
    if-eq v2, v4, :cond_aa

    #@a3
    .line 1099
    :try_start_a3
    sget-object v2, Lcom/qualcomm/wfd/service/SessionManagerService;->StaticLock:Ljava/lang/Object;

    #@a5
    const-wide/16 v4, 0x1388

    #@a7
    invoke-virtual {v2, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_aa
    .catchall {:try_start_a3 .. :try_end_aa} :catchall_5a
    .catch Ljava/lang/InterruptedException; {:try_start_a3 .. :try_end_aa} :catch_d1

    #@aa
    .line 1107
    :cond_aa
    :try_start_aa
    invoke-static {p1}, Lcom/qualcomm/wfd/WFDNative;->setRtpTransport(I)V

    #@ad
    .line 1109
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PLAY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@af
    if-ne v1, v2, :cond_ce

    #@b1
    .line 1110
    const/4 v1, 0x1

    #@b2
    invoke-direct {p0, v1}, Lcom/qualcomm/wfd/service/SessionManagerService;->internalPlay(Z)I

    #@b5
    move-result v1

    #@b6
    .line 1111
    const-string v2, "SessionManagerService"

    #@b8
    new-instance v4, Ljava/lang/StringBuilder;

    #@ba
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@bd
    const-string v5, "internalPlay ret = "

    #@bf
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v4

    #@c3
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v1

    #@c7
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ca
    move-result-object v1

    #@cb
    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ce
    .line 1113
    :cond_ce
    monitor-exit v3

    #@cf
    goto/16 :goto_36

    #@d1
    .line 1100
    :catch_d1
    move-exception v0

    #@d2
    .line 1101
    const-string v1, "SessionManagerService"

    #@d4
    const-string v2, "Wait for PAUSE interrupted"

    #@d6
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@d9
    .line 1102
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->UNKNOWN:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@db
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@de
    move-result v0

    #@df
    monitor-exit v3
    :try_end_e0
    .catchall {:try_start_aa .. :try_end_e0} :catchall_5a

    #@e0
    goto/16 :goto_36

    #@e2
    :cond_e2
    move v1, v0

    #@e3
    goto/16 :goto_15
.end method

.method public setSurface(Landroid/view/Surface;)I
    .registers 5
    .parameter

    #@0
    .prologue
    .line 1169
    sget-object v1, Lcom/qualcomm/wfd/service/SessionManagerService;->StaticLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 1170
    :try_start_3
    sget-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->DeviceType:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@5
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->SOURCE:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@7
    if-ne v0, v2, :cond_18

    #@9
    .line 1171
    const-string v0, "SessionManagerService"

    #@b
    const-string v2, "Device type is source. Ignoring setSurface"

    #@d
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 1172
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->NOT_SINK_DEVICE:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@12
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@15
    move-result v0

    #@16
    monitor-exit v1

    #@17
    .line 1182
    :goto_17
    return v0

    #@18
    .line 1174
    :cond_18
    sget-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->sSurface:Landroid/view/Surface;

    #@1a
    if-eqz v0, :cond_30

    #@1c
    .line 1175
    sget-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->sSurface:Landroid/view/Surface;

    #@1e
    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    #@21
    .line 1176
    sget-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->sSurface:Landroid/view/Surface;

    #@23
    invoke-virtual {v0}, Landroid/view/Surface;->isValid()Z

    #@26
    move-result v0

    #@27
    if-eqz v0, :cond_30

    #@29
    .line 1177
    const-string v0, "SessionManagerService"

    #@2b
    const-string v2, "Something really bad happened!"

    #@2d
    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 1180
    :cond_30
    sput-object p1, Lcom/qualcomm/wfd/service/SessionManagerService;->sSurface:Landroid/view/Surface;

    #@32
    .line 1181
    invoke-static {p1}, Lcom/qualcomm/wfd/WFDNative;->setVideoSurface(Landroid/view/Surface;)V

    #@35
    .line 1182
    const/4 v0, 0x0

    #@36
    monitor-exit v1

    #@37
    goto :goto_17

    #@38
    .line 1183
    :catchall_38
    move-exception v0

    #@39
    monitor-exit v1
    :try_end_3a
    .catchall {:try_start_3 .. :try_end_3a} :catchall_38

    #@3a
    throw v0
.end method

.method public setSurfaceProp(III)I
    .registers 7
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 1207
    sget-object v1, Lcom/qualcomm/wfd/service/SessionManagerService;->StaticLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 1208
    :try_start_3
    sget-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->DeviceType:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@5
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->PRIMARY_SINK:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@7
    if-eq v0, v2, :cond_18

    #@9
    .line 1209
    const-string v0, "SessionManagerService"

    #@b
    const-string v2, "Invalid device type "

    #@d
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 1210
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->NOT_SINK_DEVICE:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@12
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@15
    move-result v0

    #@16
    monitor-exit v1

    #@17
    .line 1214
    :goto_17
    return v0

    #@18
    .line 1212
    :cond_18
    monitor-exit v1
    :try_end_19
    .catchall {:try_start_3 .. :try_end_19} :catchall_1e

    #@19
    .line 1213
    invoke-static {p1, p2, p3}, Lcom/qualcomm/wfd/WFDNative;->setSurfaceProp(III)V

    #@1c
    .line 1214
    const/4 v0, 0x0

    #@1d
    goto :goto_17

    #@1e
    .line 1212
    :catchall_1e
    move-exception v0

    #@1f
    :try_start_1f
    monitor-exit v1
    :try_end_20
    .catchall {:try_start_1f .. :try_end_20} :catchall_1e

    #@20
    throw v0
.end method

.method public standby()I
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 819
    const-string v1, "SessionManagerService"

    #@3
    const-string v2, "standby()"

    #@5
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 820
    sget-object v1, Lcom/qualcomm/wfd/service/SessionManagerService;->StaticLock:Ljava/lang/Object;

    #@a
    monitor-enter v1

    #@b
    .line 821
    :try_start_b
    sget-boolean v2, Lcom/qualcomm/wfd/service/SessionManagerService;->IS_HDMI_CABLE_CONNECTED:Z

    #@d
    if-eqz v2, :cond_17

    #@f
    .line 822
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->HDMI_CABLE_CONNECTED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@11
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@14
    move-result v0

    #@15
    monitor-exit v1

    #@16
    .line 838
    :goto_16
    return v0

    #@17
    .line 824
    :cond_17
    sget-object v2, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@19
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$SessionState;->STANDING_BY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@1b
    if-ne v2, v3, :cond_29

    #@1d
    .line 825
    const-string v2, "SessionManagerService"

    #@1f
    const-string v3, "Already in the middle of STANDING_BY"

    #@21
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 826
    monitor-exit v1

    #@25
    goto :goto_16

    #@26
    .line 840
    :catchall_26
    move-exception v0

    #@27
    monitor-exit v1
    :try_end_28
    .catchall {:try_start_b .. :try_end_28} :catchall_26

    #@28
    throw v0

    #@29
    .line 828
    :cond_29
    :try_start_29
    sget-object v2, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@2b
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PAUSE:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@2d
    if-eq v2, v3, :cond_35

    #@2f
    sget-object v2, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@31
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PLAY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@33
    if-ne v2, v3, :cond_52

    #@35
    .line 829
    :cond_35
    sget v2, Lcom/qualcomm/wfd/service/SessionManagerService;->SessionId:I

    #@37
    invoke-static {v2}, Lcom/qualcomm/wfd/WFDNative;->standby(I)Z

    #@3a
    move-result v2

    #@3b
    if-eqz v2, :cond_43

    #@3d
    .line 830
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$SessionState;->STANDING_BY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@3f
    sput-object v2, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@41
    .line 835
    monitor-exit v1

    #@42
    goto :goto_16

    #@43
    .line 832
    :cond_43
    const-string v0, "SessionManagerService"

    #@45
    const-string v2, "Calling standby failed."

    #@47
    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    .line 833
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->UNKNOWN:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@4c
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@4f
    move-result v0

    #@50
    monitor-exit v1

    #@51
    goto :goto_16

    #@52
    .line 837
    :cond_52
    const-string v0, "SessionManagerService"

    #@54
    const-string v2, "Session state is not PAUSE or PLAY"

    #@56
    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@59
    .line 838
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->INCORRECT_STATE_FOR_OPERATION:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@5b
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@5e
    move-result v0

    #@5f
    monitor-exit v1
    :try_end_60
    .catchall {:try_start_29 .. :try_end_60} :catchall_26

    #@60
    goto :goto_16
.end method

.method public startUibcSession()I
    .registers 9

    #@0
    .prologue
    .line 605
    const-string v4, "SessionManagerService"

    #@2
    const-string v5, "startUibcSession()"

    #@4
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 606
    sget-object v5, Lcom/qualcomm/wfd/service/SessionManagerService;->StaticLock:Ljava/lang/Object;

    #@9
    monitor-enter v5

    #@a
    .line 607
    :try_start_a
    sget-boolean v4, Lcom/qualcomm/wfd/service/SessionManagerService;->IS_HDMI_CABLE_CONNECTED:Z

    #@c
    if-eqz v4, :cond_16

    #@e
    .line 608
    sget-object v4, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->HDMI_CABLE_CONNECTED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@10
    invoke-virtual {v4}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@13
    move-result v4

    #@14
    monitor-exit v5

    #@15
    .line 637
    :goto_15
    return v4

    #@16
    .line 610
    :cond_16
    sget-object v4, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@18
    sget-object v6, Lcom/qualcomm/wfd/WfdEnums$SessionState;->INVALID:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@1a
    if-ne v4, v6, :cond_27

    #@1c
    .line 611
    sget-object v4, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->NOT_INITIALIZED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@1e
    invoke-virtual {v4}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@21
    move-result v4

    #@22
    monitor-exit v5

    #@23
    goto :goto_15

    #@24
    .line 638
    :catchall_24
    move-exception v4

    #@25
    monitor-exit v5
    :try_end_26
    .catchall {:try_start_a .. :try_end_26} :catchall_24

    #@26
    throw v4

    #@27
    .line 613
    :cond_27
    :try_start_27
    sget-boolean v4, Lcom/qualcomm/wfd/service/SessionManagerService;->uibcEnabled:Z

    #@29
    if-eqz v4, :cond_33

    #@2b
    .line 614
    sget-object v4, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->UIBC_ALREADY_ENABLED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@2d
    invoke-virtual {v4}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@30
    move-result v4

    #@31
    monitor-exit v5

    #@32
    goto :goto_15

    #@33
    .line 616
    :cond_33
    sget-object v4, Lcom/qualcomm/wfd/service/SessionManagerService;->UIBCMgr:Lcom/qualcomm/wfd/UIBCManager;
    :try_end_35
    .catchall {:try_start_27 .. :try_end_35} :catchall_24

    #@35
    if-nez v4, :cond_44

    #@37
    .line 618
    :try_start_37
    new-instance v4, Lcom/qualcomm/wfd/UIBCManager;

    #@39
    sget-object v6, Lcom/qualcomm/wfd/service/SessionManagerService;->DeviceType:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@3b
    invoke-virtual {v6}, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->getCode()I

    #@3e
    move-result v6

    #@3f
    invoke-direct {v4, v6}, Lcom/qualcomm/wfd/UIBCManager;-><init>(I)V

    #@42
    sput-object v4, Lcom/qualcomm/wfd/service/SessionManagerService;->UIBCMgr:Lcom/qualcomm/wfd/UIBCManager;
    :try_end_44
    .catchall {:try_start_37 .. :try_end_44} :catchall_24
    .catch Ljava/lang/Exception; {:try_start_37 .. :try_end_44} :catch_84

    #@44
    .line 624
    :cond_44
    :try_start_44
    iget-object v4, p0, Lcom/qualcomm/wfd/service/SessionManagerService;->mContext:Landroid/content/Context;

    #@46
    iget-object v6, p0, Lcom/qualcomm/wfd/service/SessionManagerService;->mContext:Landroid/content/Context;

    #@48
    const-string v6, "window"

    #@4a
    invoke-virtual {v4, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@4d
    move-result-object v4

    #@4e
    check-cast v4, Landroid/view/WindowManager;

    #@50
    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    #@53
    move-result-object v1

    #@54
    .line 626
    .local v1, display:Landroid/view/Display;
    new-instance v3, Landroid/graphics/Point;

    #@56
    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    #@59
    .line 627
    .local v3, size:Landroid/graphics/Point;
    invoke-virtual {v1, v3}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    #@5c
    .line 628
    iget-object v4, p0, Lcom/qualcomm/wfd/service/SessionManagerService;->mContext:Landroid/content/Context;

    #@5e
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@61
    move-result-object v4

    #@62
    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@65
    move-result-object v0

    #@66
    .line 629
    .local v0, config:Landroid/content/res/Configuration;
    sget-object v4, Lcom/qualcomm/wfd/service/SessionManagerService;->DeviceType:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@68
    sget-object v6, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->SOURCE:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@6a
    if-ne v4, v6, :cond_75

    #@6c
    .line 630
    iget v4, v3, Landroid/graphics/Point;->x:I

    #@6e
    iget v6, v3, Landroid/graphics/Point;->y:I

    #@70
    iget v7, v0, Landroid/content/res/Configuration;->orientation:I

    #@72
    invoke-static {v4, v6, v7}, Lcom/qualcomm/wfd/WFDNative;->setSurfaceProp(III)V

    #@75
    .line 633
    :cond_75
    sget-object v4, Lcom/qualcomm/wfd/service/SessionManagerService;->UIBCMgr:Lcom/qualcomm/wfd/UIBCManager;

    #@77
    invoke-virtual {v4}, Lcom/qualcomm/wfd/UIBCManager;->init()Z

    #@7a
    .line 636
    sget-object v4, Lcom/qualcomm/wfd/service/SessionManagerService;->UIBCMgr:Lcom/qualcomm/wfd/UIBCManager;

    #@7c
    sget v6, Lcom/qualcomm/wfd/service/SessionManagerService;->SessionId:I

    #@7e
    invoke-virtual {v4, v6}, Lcom/qualcomm/wfd/UIBCManager;->enable(I)Z

    #@81
    .line 637
    const/4 v4, 0x0

    #@82
    monitor-exit v5

    #@83
    goto :goto_15

    #@84
    .line 619
    .end local v0           #config:Landroid/content/res/Configuration;
    .end local v1           #display:Landroid/view/Display;
    .end local v3           #size:Landroid/graphics/Point;
    :catch_84
    move-exception v2

    #@85
    .line 620
    .local v2, e:Ljava/lang/Exception;
    const-string v4, "SessionManagerService"

    #@87
    const-string v6, "Error creating UIBC manager"

    #@89
    invoke-static {v4, v6, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@8c
    .line 621
    sget-object v4, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->UNKNOWN:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@8e
    invoke-virtual {v4}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@91
    move-result v4

    #@92
    monitor-exit v5
    :try_end_93
    .catchall {:try_start_44 .. :try_end_93} :catchall_24

    #@93
    goto :goto_15
.end method

.method public startWfdSession(Lcom/qualcomm/wfd/WfdDevice;)I
    .registers 5
    .parameter "device"

    #@0
    .prologue
    .line 577
    const-string v0, "SessionManagerService"

    #@2
    const-string v1, "startWfdSession()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 578
    sget-object v1, Lcom/qualcomm/wfd/service/SessionManagerService;->StaticLock:Ljava/lang/Object;

    #@9
    monitor-enter v1

    #@a
    .line 579
    :try_start_a
    sget-boolean v0, Lcom/qualcomm/wfd/service/SessionManagerService;->IS_HDMI_CABLE_CONNECTED:Z

    #@c
    if-eqz v0, :cond_16

    #@e
    .line 580
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->HDMI_CABLE_CONNECTED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@10
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@13
    move-result v0

    #@14
    monitor-exit v1

    #@15
    .line 594
    :goto_15
    return v0

    #@16
    .line 581
    :cond_16
    sget-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@18
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$SessionState;->INVALID:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@1a
    if-ne v0, v2, :cond_27

    #@1c
    .line 582
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->NOT_INITIALIZED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@1e
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@21
    move-result v0

    #@22
    monitor-exit v1

    #@23
    goto :goto_15

    #@24
    .line 593
    :catchall_24
    move-exception v0

    #@25
    monitor-exit v1
    :try_end_26
    .catchall {:try_start_a .. :try_end_26} :catchall_24

    #@26
    throw v0

    #@27
    .line 583
    :cond_27
    :try_start_27
    sget-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@29
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$SessionState;->INITIALIZED:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@2b
    if-eq v0, v2, :cond_35

    #@2d
    .line 584
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->SESSION_IN_PROGRESS:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@2f
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@32
    move-result v0

    #@33
    monitor-exit v1

    #@34
    goto :goto_15

    #@35
    .line 585
    :cond_35
    if-nez p1, :cond_46

    #@37
    .line 586
    const-string v0, "SessionManagerService"

    #@39
    const-string v2, "Peer device is null"

    #@3b
    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 587
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->INVALID_ARG:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@40
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@43
    move-result v0

    #@44
    monitor-exit v1

    #@45
    goto :goto_15

    #@46
    .line 589
    :cond_46
    invoke-static {p1}, Lcom/qualcomm/wfd/WFDNative;->startWfdSession(Lcom/qualcomm/wfd/WfdDevice;)V

    #@49
    .line 590
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;->IDLE:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@4b
    const/4 v2, -0x1

    #@4c
    invoke-virtual {p0, v0, v2}, Lcom/qualcomm/wfd/service/SessionManagerService;->updateState(Lcom/qualcomm/wfd/WfdEnums$SessionState;I)V

    #@4f
    .line 592
    sput-object p1, Lcom/qualcomm/wfd/service/SessionManagerService;->ConnectedDevice:Lcom/qualcomm/wfd/WfdDevice;

    #@51
    .line 593
    monitor-exit v1
    :try_end_52
    .catchall {:try_start_27 .. :try_end_52} :catchall_24

    #@52
    .line 594
    const/4 v0, 0x0

    #@53
    goto :goto_15
.end method

.method public stopUibcSession()I
    .registers 4

    #@0
    .prologue
    .line 643
    const-string v0, "SessionManagerService"

    #@2
    const-string v1, "stopUibcSession"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 644
    sget-object v1, Lcom/qualcomm/wfd/service/SessionManagerService;->StaticLock:Ljava/lang/Object;

    #@9
    monitor-enter v1

    #@a
    .line 645
    :try_start_a
    sget-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->UIBCMgr:Lcom/qualcomm/wfd/UIBCManager;

    #@c
    if-eqz v0, :cond_1c

    #@e
    sget-boolean v0, Lcom/qualcomm/wfd/service/SessionManagerService;->uibcEnabled:Z

    #@10
    if-eqz v0, :cond_1c

    #@12
    .line 646
    sget-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->UIBCMgr:Lcom/qualcomm/wfd/UIBCManager;

    #@14
    sget v2, Lcom/qualcomm/wfd/service/SessionManagerService;->SessionId:I

    #@16
    invoke-virtual {v0, v2}, Lcom/qualcomm/wfd/UIBCManager;->disable(I)Z

    #@19
    .line 650
    const/4 v0, 0x0

    #@1a
    monitor-exit v1

    #@1b
    .line 652
    :goto_1b
    return v0

    #@1c
    :cond_1c
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->UIBC_NOT_ENABLED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@1e
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@21
    move-result v0

    #@22
    monitor-exit v1

    #@23
    goto :goto_1b

    #@24
    .line 654
    :catchall_24
    move-exception v0

    #@25
    monitor-exit v1
    :try_end_26
    .catchall {:try_start_a .. :try_end_26} :catchall_24

    #@26
    throw v0
.end method

.method public stopWfdSession()I
    .registers 3

    #@0
    .prologue
    .line 599
    const-string v0, "SessionManagerService"

    #@2
    const-string v1, "stopWfdSession()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 600
    invoke-virtual {p0}, Lcom/qualcomm/wfd/service/SessionManagerService;->teardown()I

    #@a
    move-result v0

    #@b
    return v0
.end method

.method public tcpPlaybackControl(I)I
    .registers 8
    .parameter

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1119
    sget-object v2, Lcom/qualcomm/wfd/service/SessionManagerService;->StaticLock:Ljava/lang/Object;

    #@3
    monitor-enter v2

    #@4
    .line 1121
    :try_start_4
    invoke-static {}, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;->values()[Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

    #@7
    move-result-object v3

    #@8
    array-length v4, v3

    #@9
    move v1, v0

    #@a
    :goto_a
    if-ge v1, v4, :cond_42

    #@c
    aget-object v5, v3, v1

    #@e
    .line 1122
    invoke-virtual {v5}, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;->ordinal()I

    #@11
    move-result v5

    #@12
    if-ne p1, v5, :cond_37

    #@14
    .line 1123
    const/4 v1, 0x1

    #@15
    .line 1127
    :goto_15
    if-nez v1, :cond_3a

    #@17
    .line 1128
    const-string v0, "SessionManagerService"

    #@19
    new-instance v1, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v3, "Invalid control cmd type:"

    #@20
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@27
    move-result-object v1

    #@28
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v1

    #@2c
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 1129
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->INVALID_ARG:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@31
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@34
    move-result v0

    #@35
    monitor-exit v2

    #@36
    .line 1132
    :goto_36
    return v0

    #@37
    .line 1121
    :cond_37
    add-int/lit8 v1, v1, 0x1

    #@39
    goto :goto_a

    #@3a
    .line 1131
    :cond_3a
    invoke-static {p1}, Lcom/qualcomm/wfd/WFDNative;->tcpPlaybackControl(I)V

    #@3d
    .line 1132
    monitor-exit v2

    #@3e
    goto :goto_36

    #@3f
    .line 1133
    :catchall_3f
    move-exception v0

    #@40
    monitor-exit v2
    :try_end_41
    .catchall {:try_start_4 .. :try_end_41} :catchall_3f

    #@41
    throw v0

    #@42
    :cond_42
    move v1, v0

    #@43
    goto :goto_15
.end method

.method public teardown()I
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 778
    const-string v0, "SessionManagerService"

    #@3
    const-string v1, "teardown()"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 779
    sget-object v1, Lcom/qualcomm/wfd/service/SessionManagerService;->StaticLock:Ljava/lang/Object;

    #@a
    monitor-enter v1

    #@b
    .line 783
    :try_start_b
    const-string v0, "SessionManagerService"

    #@d
    const-string v2, "Teardown session, broadcast intents first"

    #@f
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 784
    const/4 v0, 0x0

    #@13
    invoke-direct {p0, v0}, Lcom/qualcomm/wfd/service/SessionManagerService;->broadcastWifiDisplayAudioIntent(Z)V

    #@16
    .line 785
    const/4 v0, 0x0

    #@17
    invoke-direct {p0, v0}, Lcom/qualcomm/wfd/service/SessionManagerService;->broadcastWifiDisplayVideoEnabled(Z)V

    #@1a
    .line 786
    sget-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@1c
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$SessionState;->TEARDOWN:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@1e
    if-eq v0, v2, :cond_26

    #@20
    sget-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@22
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$SessionState;->TEARING_DOWN:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@24
    if-ne v0, v2, :cond_42

    #@26
    .line 787
    :cond_26
    const-string v0, "SessionManagerService"

    #@28
    new-instance v2, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v3, "Already in the middle of teardown. State: "

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    sget-object v3, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@35
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v2

    #@3d
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 789
    monitor-exit v1

    #@41
    .line 812
    :goto_41
    return v4

    #@42
    .line 792
    :cond_42
    sget-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@44
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$SessionState;->INVALID:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@46
    if-eq v0, v2, :cond_4e

    #@48
    sget-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@4a
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$SessionState;->INITIALIZED:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@4c
    if-ne v0, v2, :cond_5a

    #@4e
    .line 796
    :cond_4e
    const-string v0, "SessionManagerService"

    #@50
    const-string v2, "No session in progress"

    #@52
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@55
    .line 797
    monitor-exit v1

    #@56
    goto :goto_41

    #@57
    .line 814
    :catchall_57
    move-exception v0

    #@58
    monitor-exit v1
    :try_end_59
    .catchall {:try_start_b .. :try_end_59} :catchall_57

    #@59
    throw v0

    #@5a
    .line 800
    :cond_5a
    :try_start_5a
    sget-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@5c
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PLAY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@5e
    if-eq v0, v2, :cond_7e

    #@60
    sget-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@62
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PLAYING:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@64
    if-eq v0, v2, :cond_7e

    #@66
    sget-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@68
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PAUSE:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@6a
    if-eq v0, v2, :cond_7e

    #@6c
    sget-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@6e
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PAUSING:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@70
    if-eq v0, v2, :cond_7e

    #@72
    sget-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@74
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$SessionState;->STANDBY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@76
    if-eq v0, v2, :cond_7e

    #@78
    sget-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@7a
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$SessionState;->STANDING_BY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@7c
    if-ne v0, v2, :cond_91

    #@7e
    .line 803
    :cond_7e
    const-string v0, "SessionManagerService"

    #@80
    const-string v2, "Perform triggered TEARDOWN"

    #@82
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@85
    .line 804
    sget v0, Lcom/qualcomm/wfd/service/SessionManagerService;->SessionId:I

    #@87
    const/4 v2, 0x1

    #@88
    invoke-static {v0, v2}, Lcom/qualcomm/wfd/WFDNative;->teardown(IZ)V

    #@8b
    .line 805
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;->TEARING_DOWN:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@8d
    sput-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@8f
    .line 806
    monitor-exit v1

    #@90
    goto :goto_41

    #@91
    .line 808
    :cond_91
    const-string v0, "SessionManagerService"

    #@93
    const-string v2, "Session state is neither PLAY nor PAUSE"

    #@95
    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@98
    .line 809
    const-string v0, "SessionManagerService"

    #@9a
    const-string v2, "Perform local TEARDOWN without RTSP"

    #@9c
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9f
    .line 810
    sget v0, Lcom/qualcomm/wfd/service/SessionManagerService;->SessionId:I

    #@a1
    const/4 v2, 0x0

    #@a2
    invoke-static {v0, v2}, Lcom/qualcomm/wfd/WFDNative;->teardown(IZ)V

    #@a5
    .line 811
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;->TEARING_DOWN:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@a7
    sput-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@a9
    .line 812
    monitor-exit v1
    :try_end_aa
    .catchall {:try_start_5a .. :try_end_aa} :catchall_57

    #@aa
    goto :goto_41
.end method

.method public unregisterListener(Lcom/qualcomm/wfd/service/IWfdActionListener;)I
    .registers 5
    .parameter

    #@0
    .prologue
    .line 988
    const-string v0, "SessionManagerService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "unregisterListener: "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 989
    sget-object v1, Lcom/qualcomm/wfd/service/SessionManagerService;->StaticLock:Ljava/lang/Object;

    #@1a
    monitor-enter v1

    #@1b
    .line 990
    if-eqz p1, :cond_2f

    #@1d
    .line 991
    :try_start_1d
    sget-object v0, Lcom/qualcomm/wfd/service/SessionManagerService;->ActionListeners:Landroid/os/RemoteCallbackList;

    #@1f
    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    #@22
    move-result v0

    #@23
    if-eqz v0, :cond_28

    #@25
    const/4 v0, 0x0

    #@26
    :goto_26
    monitor-exit v1

    #@27
    .line 994
    :goto_27
    return v0

    #@28
    .line 991
    :cond_28
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->UNKNOWN:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@2a
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@2d
    move-result v0

    #@2e
    goto :goto_26

    #@2f
    .line 993
    :cond_2f
    const-string v0, "SessionManagerService"

    #@31
    const-string v2, "Listener to unregister is null"

    #@33
    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 994
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->INVALID_ARG:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@38
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@3b
    move-result v0

    #@3c
    monitor-exit v1

    #@3d
    goto :goto_27

    #@3e
    .line 996
    :catchall_3e
    move-exception v0

    #@3f
    monitor-exit v1
    :try_end_40
    .catchall {:try_start_1d .. :try_end_40} :catchall_3e

    #@40
    throw v0
.end method

.method public updateState(Lcom/qualcomm/wfd/WfdEnums$SessionState;I)V
    .registers 10
    .parameter "state"
    .parameter "sessionId"

    #@0
    .prologue
    .line 345
    const-string v3, "SessionManagerService"

    #@2
    const-string v4, "updateState()"

    #@4
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 346
    sget-object v4, Lcom/qualcomm/wfd/service/SessionManagerService;->StaticLock:Ljava/lang/Object;

    #@9
    monitor-enter v4

    #@a
    .line 347
    :try_start_a
    sget-object v3, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@c
    if-ne p1, v3, :cond_1b

    #@e
    sget v3, Lcom/qualcomm/wfd/service/SessionManagerService;->SessionId:I

    #@10
    if-ne p2, v3, :cond_1b

    #@12
    .line 348
    const-string v3, "SessionManagerService"

    #@14
    const-string v5, "Nothing has changed. Ignoring updateState"

    #@16
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 349
    monitor-exit v4

    #@1a
    .line 396
    :goto_1a
    return-void

    #@1b
    .line 351
    :cond_1b
    sget-object v3, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@1d
    sget-object v5, Lcom/qualcomm/wfd/WfdEnums$SessionState;->STANDING_BY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@1f
    if-ne v3, v5, :cond_99

    #@21
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PAUSE:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@23
    if-ne p1, v3, :cond_99

    #@25
    .line 353
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$SessionState;->STANDBY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@27
    sput-object v3, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@29
    .line 357
    :goto_29
    sget v3, Lcom/qualcomm/wfd/service/SessionManagerService;->SessionId:I

    #@2b
    if-eq p2, v3, :cond_9f

    #@2d
    .line 358
    const-string v3, "SessionManagerService"

    #@2f
    new-instance v5, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v6, "Session id changed from "

    #@36
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v5

    #@3a
    sget v6, Lcom/qualcomm/wfd/service/SessionManagerService;->SessionId:I

    #@3c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v5

    #@40
    const-string v6, " to "

    #@42
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v5

    #@46
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@49
    move-result-object v5

    #@4a
    const-string v6, " with state "

    #@4c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v5

    #@50
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v5

    #@54
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v5

    #@58
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5b
    .line 363
    :goto_5b
    sput p2, Lcom/qualcomm/wfd/service/SessionManagerService;->SessionId:I

    #@5d
    .line 364
    sget-object v3, Lcom/qualcomm/wfd/service/SessionManagerService;->StaticLock:Ljava/lang/Object;

    #@5f
    invoke-virtual {v3}, Ljava/lang/Object;->notifyAll()V

    #@62
    .line 366
    sget-object v3, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@64
    sget-object v5, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PAUSE:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@66
    if-ne v3, v5, :cond_c2

    #@68
    .line 367
    const-string v3, "SessionManagerService"

    #@6a
    const-string v5, "PAUSE done"

    #@6c
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6f
    .line 380
    :cond_6f
    :goto_6f
    sget-object v3, Lcom/qualcomm/wfd/service/SessionManagerService;->ActionListeners:Landroid/os/RemoteCallbackList;

    #@71
    if-eqz v3, :cond_10a

    #@73
    .line 381
    const-string v3, "SessionManagerService"

    #@75
    const-string v5, "Sending onStateUpdate() to listeners"

    #@77
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7a
    .line 382
    sget-object v3, Lcom/qualcomm/wfd/service/SessionManagerService;->ActionListeners:Landroid/os/RemoteCallbackList;

    #@7c
    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
    :try_end_7f
    .catchall {:try_start_a .. :try_end_7f} :catchall_9c

    #@7f
    move-result v0

    #@80
    .line 383
    .local v0, N:I
    const/4 v2, 0x0

    #@81
    .local v2, i:I
    :goto_81
    if-ge v2, v0, :cond_102

    #@83
    .line 385
    :try_start_83
    sget-object v3, Lcom/qualcomm/wfd/service/SessionManagerService;->ActionListeners:Landroid/os/RemoteCallbackList;

    #@85
    invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@88
    move-result-object v3

    #@89
    check-cast v3, Lcom/qualcomm/wfd/service/IWfdActionListener;

    #@8b
    sget-object v5, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@8d
    invoke-virtual {v5}, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ordinal()I

    #@90
    move-result v5

    #@91
    sget v6, Lcom/qualcomm/wfd/service/SessionManagerService;->SessionId:I

    #@93
    invoke-interface {v3, v5, v6}, Lcom/qualcomm/wfd/service/IWfdActionListener;->onStateUpdate(II)V
    :try_end_96
    .catchall {:try_start_83 .. :try_end_96} :catchall_9c
    .catch Landroid/os/RemoteException; {:try_start_83 .. :try_end_96} :catch_f9

    #@96
    .line 383
    :goto_96
    add-int/lit8 v2, v2, 0x1

    #@98
    goto :goto_81

    #@99
    .line 355
    .end local v0           #N:I
    .end local v2           #i:I
    :cond_99
    :try_start_99
    sput-object p1, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@9b
    goto :goto_29

    #@9c
    .line 395
    :catchall_9c
    move-exception v3

    #@9d
    monitor-exit v4
    :try_end_9e
    .catchall {:try_start_99 .. :try_end_9e} :catchall_9c

    #@9e
    throw v3

    #@9f
    .line 361
    :cond_9f
    :try_start_9f
    const-string v3, "SessionManagerService"

    #@a1
    new-instance v5, Ljava/lang/StringBuilder;

    #@a3
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@a6
    const-string v6, "Session id "

    #@a8
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v5

    #@ac
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@af
    move-result-object v5

    #@b0
    const-string v6, " with state "

    #@b2
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v5

    #@b6
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v5

    #@ba
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bd
    move-result-object v5

    #@be
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c1
    goto :goto_5b

    #@c2
    .line 368
    :cond_c2
    sget-object v3, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@c4
    sget-object v5, Lcom/qualcomm/wfd/WfdEnums$SessionState;->STANDBY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@c6
    if-ne v3, v5, :cond_d0

    #@c8
    .line 369
    const-string v3, "SessionManagerService"

    #@ca
    const-string v5, "STANDBY Done"

    #@cc
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@cf
    goto :goto_6f

    #@d0
    .line 370
    :cond_d0
    sget-object v3, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@d2
    sget-object v5, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PLAY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@d4
    if-ne v3, v5, :cond_de

    #@d6
    .line 371
    const-string v3, "SessionManagerService"

    #@d8
    const-string v5, "PLAY done"

    #@da
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@dd
    goto :goto_6f

    #@de
    .line 372
    :cond_de
    sget-object v3, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@e0
    sget-object v5, Lcom/qualcomm/wfd/WfdEnums$SessionState;->TEARDOWN:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@e2
    if-ne v3, v5, :cond_6f

    #@e4
    .line 373
    const-string v3, "SessionManagerService"

    #@e6
    const-string v5, "TEARDOWN done"

    #@e8
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@eb
    .line 374
    invoke-virtual {p0}, Lcom/qualcomm/wfd/service/SessionManagerService;->clearVars()V

    #@ee
    .line 375
    sget v3, Lcom/qualcomm/wfd/service/SessionManagerService;->SessionId:I

    #@f0
    invoke-static {v3}, Lcom/qualcomm/wfd/WFDNative;->stopWfdSession(I)V

    #@f3
    .line 376
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$SessionState;->INITIALIZED:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@f5
    sput-object v3, Lcom/qualcomm/wfd/service/SessionManagerService;->State:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@f7
    goto/16 :goto_6f

    #@f9
    .line 387
    .restart local v0       #N:I
    .restart local v2       #i:I
    :catch_f9
    move-exception v1

    #@fa
    .line 388
    .local v1, e:Landroid/os/RemoteException;
    const-string v3, "SessionManagerService"

    #@fc
    const-string v5, "Error sending status update to client, removing listener"

    #@fe
    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@101
    goto :goto_96

    #@102
    .line 391
    .end local v1           #e:Landroid/os/RemoteException;
    :cond_102
    sget-object v3, Lcom/qualcomm/wfd/service/SessionManagerService;->ActionListeners:Landroid/os/RemoteCallbackList;

    #@104
    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    #@107
    .line 395
    .end local v0           #N:I
    .end local v2           #i:I
    :goto_107
    monitor-exit v4

    #@108
    goto/16 :goto_1a

    #@10a
    .line 393
    :cond_10a
    const-string v3, "SessionManagerService"

    #@10c
    const-string v5, "updateState: Remote callback list is null"

    #@10e
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_111
    .catchall {:try_start_9f .. :try_end_111} :catchall_9c

    #@111
    goto :goto_107
.end method
