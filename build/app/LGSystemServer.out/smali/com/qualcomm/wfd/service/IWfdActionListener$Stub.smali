.class public abstract Lcom/qualcomm/wfd/service/IWfdActionListener$Stub;
.super Landroid/os/Binder;
.source "IWfdActionListener.java"

# interfaces
.implements Lcom/qualcomm/wfd/service/IWfdActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/service/IWfdActionListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/wfd/service/IWfdActionListener$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.qualcomm.wfd.service.IWfdActionListener"

.field static final TRANSACTION_notify:I = 0x3

.field static final TRANSACTION_notifyEvent:I = 0x2

.field static final TRANSACTION_onStateUpdate:I = 0x1


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 15
    const-string v0, "com.qualcomm.wfd.service.IWfdActionListener"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/qualcomm/wfd/service/IWfdActionListener$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/qualcomm/wfd/service/IWfdActionListener;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 23
    if-nez p0, :cond_4

    #@2
    .line 24
    const/4 v0, 0x0

    #@3
    .line 30
    :goto_3
    return-object v0

    #@4
    .line 26
    :cond_4
    const-string v1, "com.qualcomm.wfd.service.IWfdActionListener"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 27
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/qualcomm/wfd/service/IWfdActionListener;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 28
    check-cast v0, Lcom/qualcomm/wfd/service/IWfdActionListener;

    #@12
    goto :goto_3

    #@13
    .line 30
    :cond_13
    new-instance v0, Lcom/qualcomm/wfd/service/IWfdActionListener$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/qualcomm/wfd/service/IWfdActionListener$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 9
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 38
    sparse-switch p1, :sswitch_data_4e

    #@4
    .line 81
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v2

    #@8
    :goto_8
    return v2

    #@9
    .line 42
    :sswitch_9
    const-string v3, "com.qualcomm.wfd.service.IWfdActionListener"

    #@b
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 47
    :sswitch_f
    const-string v3, "com.qualcomm.wfd.service.IWfdActionListener"

    #@11
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v0

    #@18
    .line 51
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v1

    #@1c
    .line 52
    .local v1, _arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/qualcomm/wfd/service/IWfdActionListener$Stub;->onStateUpdate(II)V

    #@1f
    goto :goto_8

    #@20
    .line 57
    .end local v0           #_arg0:I
    .end local v1           #_arg1:I
    :sswitch_20
    const-string v3, "com.qualcomm.wfd.service.IWfdActionListener"

    #@22
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@25
    .line 59
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@28
    move-result v0

    #@29
    .line 61
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2c
    move-result v1

    #@2d
    .line 62
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/qualcomm/wfd/service/IWfdActionListener$Stub;->notifyEvent(II)V

    #@30
    goto :goto_8

    #@31
    .line 67
    .end local v0           #_arg0:I
    .end local v1           #_arg1:I
    :sswitch_31
    const-string v3, "com.qualcomm.wfd.service.IWfdActionListener"

    #@33
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@36
    .line 69
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@39
    move-result v3

    #@3a
    if-eqz v3, :cond_4c

    #@3c
    .line 70
    sget-object v3, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@3e
    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@41
    move-result-object v0

    #@42
    check-cast v0, Landroid/os/Bundle;

    #@44
    .line 76
    .local v0, _arg0:Landroid/os/Bundle;
    :goto_44
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@47
    move-result v1

    #@48
    .line 77
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/qualcomm/wfd/service/IWfdActionListener$Stub;->notify(Landroid/os/Bundle;I)V

    #@4b
    goto :goto_8

    #@4c
    .line 73
    .end local v0           #_arg0:Landroid/os/Bundle;
    .end local v1           #_arg1:I
    :cond_4c
    const/4 v0, 0x0

    #@4d
    .restart local v0       #_arg0:Landroid/os/Bundle;
    goto :goto_44

    #@4e
    .line 38
    :sswitch_data_4e
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_20
        0x3 -> :sswitch_31
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
