.class public final enum Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;
.super Ljava/lang/Enum;
.source "WfdEnums.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/WfdEnums;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "WFDDeviceType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

.field public static final enum PRIMARY_SINK:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

.field public static final enum SECONDARY_SINK:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

.field public static final enum SOURCE:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

.field public static final enum SOURCE_PRIMARY_SINK:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

.field public static final enum UNKNOWN:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;


# instance fields
.field private final code:I


# direct methods
.method static constructor <clinit>()V
    .registers 7

    #@0
    .prologue
    const/4 v6, 0x4

    #@1
    const/4 v5, 0x3

    #@2
    const/4 v4, 0x2

    #@3
    const/4 v3, 0x1

    #@4
    const/4 v2, 0x0

    #@5
    .line 397
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@7
    const-string v1, "SOURCE"

    #@9
    invoke-direct {v0, v1, v2, v2}, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;-><init>(Ljava/lang/String;II)V

    #@c
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->SOURCE:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@e
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@10
    const-string v1, "PRIMARY_SINK"

    #@12
    invoke-direct {v0, v1, v3, v3}, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;-><init>(Ljava/lang/String;II)V

    #@15
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->PRIMARY_SINK:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@17
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@19
    const-string v1, "SECONDARY_SINK"

    #@1b
    invoke-direct {v0, v1, v4, v4}, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;-><init>(Ljava/lang/String;II)V

    #@1e
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->SECONDARY_SINK:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@20
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@22
    const-string v1, "SOURCE_PRIMARY_SINK"

    #@24
    invoke-direct {v0, v1, v5, v5}, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;-><init>(Ljava/lang/String;II)V

    #@27
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->SOURCE_PRIMARY_SINK:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@29
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@2b
    const-string v1, "UNKNOWN"

    #@2d
    invoke-direct {v0, v1, v6, v6}, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;-><init>(Ljava/lang/String;II)V

    #@30
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->UNKNOWN:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@32
    .line 396
    const/4 v0, 0x5

    #@33
    new-array v0, v0, [Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@35
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->SOURCE:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@37
    aput-object v1, v0, v2

    #@39
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->PRIMARY_SINK:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@3b
    aput-object v1, v0, v3

    #@3d
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->SECONDARY_SINK:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@3f
    aput-object v1, v0, v4

    #@41
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->SOURCE_PRIMARY_SINK:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@43
    aput-object v1, v0, v5

    #@45
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->UNKNOWN:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@47
    aput-object v1, v0, v6

    #@49
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@4b
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter "c"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 401
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    .line 402
    iput p3, p0, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->code:I

    #@5
    .line 403
    return-void
.end method

.method public static getValue(I)Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;
    .registers 6
    .parameter "c"

    #@0
    .prologue
    .line 410
    invoke-static {}, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->values()[Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@3
    move-result-object v0

    #@4
    .local v0, arr$:[Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;
    array-length v3, v0

    #@5
    .local v3, len$:I
    const/4 v2, 0x0

    #@6
    .local v2, i$:I
    :goto_6
    if-ge v2, v3, :cond_12

    #@8
    aget-object v1, v0, v2

    #@a
    .line 411
    .local v1, e:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;
    iget v4, v1, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->code:I

    #@c
    if-ne v4, p0, :cond_f

    #@e
    .line 414
    .end local v1           #e:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;
    :goto_e
    return-object v1

    #@f
    .line 410
    .restart local v1       #e:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;
    :cond_f
    add-int/lit8 v2, v2, 0x1

    #@11
    goto :goto_6

    #@12
    .line 414
    .end local v1           #e:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;
    :cond_12
    const/4 v1, 0x0

    #@13
    goto :goto_e
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;
    .registers 2
    .parameter

    #@0
    .prologue
    .line 396
    const-class v0, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;
    .registers 1

    #@0
    .prologue
    .line 396
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@2
    invoke-virtual {v0}, [Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@8
    return-object v0
.end method


# virtual methods
.method public getCode()I
    .registers 2

    #@0
    .prologue
    .line 406
    iget v0, p0, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->code:I

    #@2
    return v0
.end method
