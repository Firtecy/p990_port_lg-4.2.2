.class public final enum Lcom/qualcomm/wfd/WfdEnums$VideoFormat;
.super Ljava/lang/Enum;
.source "WfdEnums.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/WfdEnums;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "VideoFormat"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/wfd/WfdEnums$VideoFormat;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

.field public static final enum WFD_VIDEO_3D:Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

.field public static final enum WFD_VIDEO_H264:Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

.field public static final enum WFD_VIDEO_INVALID:Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

.field public static final enum WFD_VIDEO_UNK:Lcom/qualcomm/wfd/WfdEnums$VideoFormat;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    #@0
    .prologue
    const/4 v5, 0x3

    #@1
    const/4 v4, 0x2

    #@2
    const/4 v3, 0x1

    #@3
    const/4 v2, 0x0

    #@4
    .line 457
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

    #@6
    const-string v1, "WFD_VIDEO_UNK"

    #@8
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;-><init>(Ljava/lang/String;I)V

    #@b
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;->WFD_VIDEO_UNK:Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

    #@d
    .line 458
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

    #@f
    const-string v1, "WFD_VIDEO_H264"

    #@11
    invoke-direct {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;-><init>(Ljava/lang/String;I)V

    #@14
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;->WFD_VIDEO_H264:Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

    #@16
    .line 459
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

    #@18
    const-string v1, "WFD_VIDEO_3D"

    #@1a
    invoke-direct {v0, v1, v4}, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;-><init>(Ljava/lang/String;I)V

    #@1d
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;->WFD_VIDEO_3D:Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

    #@1f
    .line 460
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

    #@21
    const-string v1, "WFD_VIDEO_INVALID"

    #@23
    invoke-direct {v0, v1, v5}, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;-><init>(Ljava/lang/String;I)V

    #@26
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;->WFD_VIDEO_INVALID:Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

    #@28
    .line 456
    const/4 v0, 0x4

    #@29
    new-array v0, v0, [Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

    #@2b
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;->WFD_VIDEO_UNK:Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

    #@2d
    aput-object v1, v0, v2

    #@2f
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;->WFD_VIDEO_H264:Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

    #@31
    aput-object v1, v0, v3

    #@33
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;->WFD_VIDEO_3D:Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

    #@35
    aput-object v1, v0, v4

    #@37
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;->WFD_VIDEO_INVALID:Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

    #@39
    aput-object v1, v0, v5

    #@3b
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

    #@3d
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 456
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/wfd/WfdEnums$VideoFormat;
    .registers 2
    .parameter

    #@0
    .prologue
    .line 456
    const-class v0, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/qualcomm/wfd/WfdEnums$VideoFormat;
    .registers 1

    #@0
    .prologue
    .line 456
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

    #@2
    invoke-virtual {v0}, [Lcom/qualcomm/wfd/WfdEnums$VideoFormat;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

    #@8
    return-object v0
.end method
