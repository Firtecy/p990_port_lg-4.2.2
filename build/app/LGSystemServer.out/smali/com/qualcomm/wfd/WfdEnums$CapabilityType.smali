.class public final enum Lcom/qualcomm/wfd/WfdEnums$CapabilityType;
.super Ljava/lang/Enum;
.source "WfdEnums.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/WfdEnums;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CapabilityType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/wfd/WfdEnums$CapabilityType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

.field public static final enum WFD_3D_VIDEO_FORMATS:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

.field public static final enum WFD_AUDIO_CODECS:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

.field public static final enum WFD_CEA_RESOLUTIONS_BITMAP:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

.field public static final enum WFD_CONTENT_PROTECTION_SUPPORTED:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

.field public static final enum WFD_COUPLED_SINK:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

.field public static final enum WFD_COUPLED_SINK_SUPPORTED_BY_SINK:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

.field public static final enum WFD_COUPLED_SINK_SUPPORTED_BY_SOURCE:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

.field public static final enum WFD_DISPLAY_EDID:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

.field public static final enum WFD_HH_RESOLUTIONS_BITMAP:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

.field public static final enum WFD_I2C:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

.field public static final enum WFD_SERVICE_DISCOVERY_SUPPORTED:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

.field public static final enum WFD_STANDBY_RESUME_CAPABILITY:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

.field public static final enum WFD_TIME_SYNC_SUPPORTED:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

.field public static final enum WFD_UIBC_SUPPORTED:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

.field public static final enum WFD_VESA_RESOLUTIONS_BITMAP:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

.field public static final enum WFD_VIDEO_FORMATS:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 422
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@7
    const-string v1, "WFD_AUDIO_CODECS"

    #@9
    invoke-direct {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;-><init>(Ljava/lang/String;I)V

    #@c
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_AUDIO_CODECS:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@e
    .line 423
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@10
    const-string v1, "WFD_VIDEO_FORMATS"

    #@12
    invoke-direct {v0, v1, v4}, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;-><init>(Ljava/lang/String;I)V

    #@15
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_VIDEO_FORMATS:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@17
    .line 424
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@19
    const-string v1, "WFD_3D_VIDEO_FORMATS"

    #@1b
    invoke-direct {v0, v1, v5}, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;-><init>(Ljava/lang/String;I)V

    #@1e
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_3D_VIDEO_FORMATS:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@20
    .line 425
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@22
    const-string v1, "WFD_CEA_RESOLUTIONS_BITMAP"

    #@24
    invoke-direct {v0, v1, v6}, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;-><init>(Ljava/lang/String;I)V

    #@27
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_CEA_RESOLUTIONS_BITMAP:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@29
    .line 426
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@2b
    const-string v1, "WFD_VESA_RESOLUTIONS_BITMAP"

    #@2d
    invoke-direct {v0, v1, v7}, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;-><init>(Ljava/lang/String;I)V

    #@30
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_VESA_RESOLUTIONS_BITMAP:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@32
    .line 427
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@34
    const-string v1, "WFD_HH_RESOLUTIONS_BITMAP"

    #@36
    const/4 v2, 0x5

    #@37
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;-><init>(Ljava/lang/String;I)V

    #@3a
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_HH_RESOLUTIONS_BITMAP:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@3c
    .line 428
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@3e
    const-string v1, "WFD_DISPLAY_EDID"

    #@40
    const/4 v2, 0x6

    #@41
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;-><init>(Ljava/lang/String;I)V

    #@44
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_DISPLAY_EDID:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@46
    .line 429
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@48
    const-string v1, "WFD_COUPLED_SINK"

    #@4a
    const/4 v2, 0x7

    #@4b
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;-><init>(Ljava/lang/String;I)V

    #@4e
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_COUPLED_SINK:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@50
    .line 430
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@52
    const-string v1, "WFD_I2C"

    #@54
    const/16 v2, 0x8

    #@56
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;-><init>(Ljava/lang/String;I)V

    #@59
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_I2C:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@5b
    .line 431
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@5d
    const-string v1, "WFD_UIBC_SUPPORTED"

    #@5f
    const/16 v2, 0x9

    #@61
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;-><init>(Ljava/lang/String;I)V

    #@64
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_UIBC_SUPPORTED:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@66
    .line 432
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@68
    const-string v1, "WFD_STANDBY_RESUME_CAPABILITY"

    #@6a
    const/16 v2, 0xa

    #@6c
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;-><init>(Ljava/lang/String;I)V

    #@6f
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_STANDBY_RESUME_CAPABILITY:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@71
    .line 433
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@73
    const-string v1, "WFD_COUPLED_SINK_SUPPORTED_BY_SOURCE"

    #@75
    const/16 v2, 0xb

    #@77
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;-><init>(Ljava/lang/String;I)V

    #@7a
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_COUPLED_SINK_SUPPORTED_BY_SOURCE:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@7c
    .line 434
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@7e
    const-string v1, "WFD_COUPLED_SINK_SUPPORTED_BY_SINK"

    #@80
    const/16 v2, 0xc

    #@82
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;-><init>(Ljava/lang/String;I)V

    #@85
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_COUPLED_SINK_SUPPORTED_BY_SINK:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@87
    .line 435
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@89
    const-string v1, "WFD_SERVICE_DISCOVERY_SUPPORTED"

    #@8b
    const/16 v2, 0xd

    #@8d
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;-><init>(Ljava/lang/String;I)V

    #@90
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_SERVICE_DISCOVERY_SUPPORTED:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@92
    .line 436
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@94
    const-string v1, "WFD_CONTENT_PROTECTION_SUPPORTED"

    #@96
    const/16 v2, 0xe

    #@98
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;-><init>(Ljava/lang/String;I)V

    #@9b
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_CONTENT_PROTECTION_SUPPORTED:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@9d
    .line 437
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@9f
    const-string v1, "WFD_TIME_SYNC_SUPPORTED"

    #@a1
    const/16 v2, 0xf

    #@a3
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;-><init>(Ljava/lang/String;I)V

    #@a6
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_TIME_SYNC_SUPPORTED:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@a8
    .line 421
    const/16 v0, 0x10

    #@aa
    new-array v0, v0, [Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@ac
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_AUDIO_CODECS:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@ae
    aput-object v1, v0, v3

    #@b0
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_VIDEO_FORMATS:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@b2
    aput-object v1, v0, v4

    #@b4
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_3D_VIDEO_FORMATS:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@b6
    aput-object v1, v0, v5

    #@b8
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_CEA_RESOLUTIONS_BITMAP:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@ba
    aput-object v1, v0, v6

    #@bc
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_VESA_RESOLUTIONS_BITMAP:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@be
    aput-object v1, v0, v7

    #@c0
    const/4 v1, 0x5

    #@c1
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_HH_RESOLUTIONS_BITMAP:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@c3
    aput-object v2, v0, v1

    #@c5
    const/4 v1, 0x6

    #@c6
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_DISPLAY_EDID:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@c8
    aput-object v2, v0, v1

    #@ca
    const/4 v1, 0x7

    #@cb
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_COUPLED_SINK:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@cd
    aput-object v2, v0, v1

    #@cf
    const/16 v1, 0x8

    #@d1
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_I2C:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@d3
    aput-object v2, v0, v1

    #@d5
    const/16 v1, 0x9

    #@d7
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_UIBC_SUPPORTED:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@d9
    aput-object v2, v0, v1

    #@db
    const/16 v1, 0xa

    #@dd
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_STANDBY_RESUME_CAPABILITY:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@df
    aput-object v2, v0, v1

    #@e1
    const/16 v1, 0xb

    #@e3
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_COUPLED_SINK_SUPPORTED_BY_SOURCE:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@e5
    aput-object v2, v0, v1

    #@e7
    const/16 v1, 0xc

    #@e9
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_COUPLED_SINK_SUPPORTED_BY_SINK:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@eb
    aput-object v2, v0, v1

    #@ed
    const/16 v1, 0xd

    #@ef
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_SERVICE_DISCOVERY_SUPPORTED:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@f1
    aput-object v2, v0, v1

    #@f3
    const/16 v1, 0xe

    #@f5
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_CONTENT_PROTECTION_SUPPORTED:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@f7
    aput-object v2, v0, v1

    #@f9
    const/16 v1, 0xf

    #@fb
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_TIME_SYNC_SUPPORTED:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@fd
    aput-object v2, v0, v1

    #@ff
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@101
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 421
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/wfd/WfdEnums$CapabilityType;
    .registers 2
    .parameter

    #@0
    .prologue
    .line 421
    const-class v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/qualcomm/wfd/WfdEnums$CapabilityType;
    .registers 1

    #@0
    .prologue
    .line 421
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@2
    invoke-virtual {v0}, [Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    #@8
    return-object v0
.end method
