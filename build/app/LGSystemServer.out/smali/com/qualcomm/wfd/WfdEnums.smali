.class public Lcom/qualcomm/wfd/WfdEnums;
.super Ljava/lang/Object;
.source "WfdEnums.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/wfd/WfdEnums$ErrorType;,
        Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;,
        Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;,
        Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;,
        Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;,
        Lcom/qualcomm/wfd/WfdEnums$PreferredConnectivity;,
        Lcom/qualcomm/wfd/WfdEnums$WfdEvent;,
        Lcom/qualcomm/wfd/WfdEnums$SessionState;,
        Lcom/qualcomm/wfd/WfdEnums$VideoFormat;,
        Lcom/qualcomm/wfd/WfdEnums$AudioCodec;,
        Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;,
        Lcom/qualcomm/wfd/WfdEnums$CapabilityType;,
        Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;
    }
.end annotation


# static fields
.field public static final ACTION_SECURE_END:Ljava/lang/String; = "qualcomm.intent.action.SECURE_END"

.field public static final ACTION_SECURE_END_DONE:Ljava/lang/String; = "qualcomm.intent.action.SECURE_END_DONE"

.field public static final ACTION_SECURE_START:Ljava/lang/String; = "qualcomm.intent.action.SECURE_START"

.field public static final ACTION_SECURE_START_DONE:Ljava/lang/String; = "qualcomm.intent.action.SECURE_START_DONE"

.field public static final ACTION_WIFI_DISPLAY_BITRATE:Ljava/lang/String; = "qualcomm.intent.action.WIFI_DISPLAY_BITRATE"

.field public static final ACTION_WIFI_DISPLAY_DISABLED:Ljava/lang/String; = "qualcomm.intent.action.WIFI_DISPLAY_DISABLED"

.field public static final ACTION_WIFI_DISPLAY_ENABLED:Ljava/lang/String; = "qualcomm.intent.action.WIFI_DISPLAY_ENABLED"

.field public static final ACTION_WIFI_DISPLAY_PLAYBACK_MODE:Ljava/lang/String; = "qualcomm.intent.action.WIFI_DISPLAY_PLAYBACK_MODE"

.field public static final ACTION_WIFI_DISPLAY_RESOLUTION:Ljava/lang/String; = "qualcomm.intent.action.WIFI_DISPLAY_RESOLUTION"

.field public static final ACTION_WIFI_DISPLAY_RTP_TRANSPORT:Ljava/lang/String; = "qualcomm.intent.action.WIFI_DISPLAY_RTP_TRANSPORT"

.field public static final ACTION_WIFI_DISPLAY_TCP_PLAYBACK_CONTROL:Ljava/lang/String; = "qualcomm.intent.action.WIFI_DISPLAY_TCP_PLAYBACK_CONTROL"

.field static final BIT0:I = 0x1

.field static final BIT1:I = 0x2

.field static final BIT10:I = 0x400

.field static final BIT11:I = 0x800

.field static final BIT12:I = 0x1000

.field static final BIT13:I = 0x2000

.field static final BIT14:I = 0x4000

.field static final BIT15:I = 0x8000

.field static final BIT16:I = 0x10000

.field static final BIT17:I = 0x20000

.field static final BIT18:I = 0x40000

.field static final BIT19:I = 0x80000

.field static final BIT2:I = 0x4

.field static final BIT20:I = 0x100000

.field static final BIT21:I = 0x200000

.field static final BIT22:I = 0x400000

.field static final BIT23:I = 0x800000

.field static final BIT24:I = 0x1000000

.field static final BIT25:I = 0x2000000

.field static final BIT26:I = 0x4000000

.field static final BIT27:I = 0x8000000

.field static final BIT28:I = 0x10000000

.field static final BIT29:I = 0x20000000

.field static final BIT3:I = 0x8

.field static final BIT30:I = 0x40000000

.field static final BIT31:I = -0x80000000

.field static final BIT4:I = 0x10

.field static final BIT5:I = 0x20

.field static final BIT6:I = 0x40

.field static final BIT7:I = 0x80

.field static final BIT8:I = 0x100

.field static final BIT9:I = 0x200

.field public static final H264_CEA_1280x720p24:I = 0x8000

.field public static final H264_CEA_1280x720p25:I = 0x400

.field public static final H264_CEA_1280x720p30:I = 0x20

.field public static final H264_CEA_1280x720p50:I = 0x800

.field public static final H264_CEA_1280x720p60:I = 0x40

.field public static final H264_CEA_1920x1080i50:I = 0x4000

.field public static final H264_CEA_1920x1080i60:I = 0x200

.field public static final H264_CEA_1920x1080p24:I = 0x10000

.field public static final H264_CEA_1920x1080p25:I = 0x1000

.field public static final H264_CEA_1920x1080p30:I = 0x80

.field public static final H264_CEA_1920x1080p50:I = 0x2000

.field public static final H264_CEA_1920x1080p60:I = 0x100

.field public static final H264_CEA_640x480p60:I = 0x1

.field public static final H264_CEA_720x480i60:I = 0x4

.field public static final H264_CEA_720x480p60:I = 0x2

.field public static final H264_CEA_720x576i50:I = 0x10

.field public static final H264_CEA_720x576p50:I = 0x8

.field public static final H264_HH_640x360p30:I = 0x40

.field public static final H264_HH_640x360p60:I = 0x80

.field public static final H264_HH_800x480p30:I = 0x1

.field public static final H264_HH_800x480p60:I = 0x2

.field public static final H264_HH_848x480p30:I = 0x400

.field public static final H264_HH_848x480p60:I = 0x800

.field public static final H264_HH_854x480p30:I = 0x4

.field public static final H264_HH_854x480p60:I = 0x8

.field public static final H264_HH_864x480p30:I = 0x10

.field public static final H264_HH_864x480p60:I = 0x20

.field public static final H264_HH_960x540p30:I = 0x100

.field public static final H264_VESA_1024x768p30:I = 0x4

.field public static final H264_VESA_1024x768p60:I = 0x8

.field public static final H264_VESA_1152x864p30:I = 0x10

.field public static final H264_VESA_1152x864p60:I = 0x20

.field public static final H264_VESA_1280x1024p30:I = 0x4000

.field public static final H264_VESA_1280x1024p60:I = 0x8000

.field public static final H264_VESA_1280x768p30:I = 0x40

.field public static final H264_VESA_1280x768p60:I = 0x80

.field public static final H264_VESA_1280x800p30:I = 0x100

.field public static final H264_VESA_1280x800p60:I = 0x200

.field public static final H264_VESA_1360x768p30:I = 0x400

.field public static final H264_VESA_1360x768p60:I = 0x800

.field public static final H264_VESA_1366x768p30:I = 0x1000

.field public static final H264_VESA_1366x768p60:I = 0x2000

.field public static final H264_VESA_1400x1050p30:I = 0x10000

.field public static final H264_VESA_1400x1050p60:I = 0x20000

.field public static final H264_VESA_1440x900p30:I = 0x40000

.field public static final H264_VESA_1440x900p60:I = 0x80000

.field public static final H264_VESA_1600x1200p30:I = 0x400000

.field public static final H264_VESA_1600x1200p60:I = 0x800000

.field public static final H264_VESA_1600x900p30:I = 0x100000

.field public static final H264_VESA_1600x900p60:I = 0x200000

.field public static final H264_VESA_1680x1024p30:I = 0x1000000

.field public static final H264_VESA_1680x1024p60:I = 0x2000000

.field public static final H264_VESA_1680x1050p30:I = 0x4000000

.field public static final H264_VESA_1680x1050p60:I = 0x8000000

.field public static final H264_VESA_1920x1200p30:I = 0x10000000

.field public static final H264_VESA_1920x1200p60:I = 0x20000000

.field public static final H264_VESA_800x600p30:I = 0x1

.field public static final H264_VESA_800x600p60:I = 0x2

.field private static resFps:I

.field private static resHeight:I

.field private static resWidth:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 68
    sput v0, Lcom/qualcomm/wfd/WfdEnums;->resWidth:I

    #@3
    sput v0, Lcom/qualcomm/wfd/WfdEnums;->resHeight:I

    #@5
    sput v0, Lcom/qualcomm/wfd/WfdEnums;->resFps:I

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 12
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 554
    return-void
.end method

.method public static getResParams()[I
    .registers 3

    #@0
    .prologue
    .line 96
    const/4 v1, 0x3

    #@1
    new-array v0, v1, [I

    #@3
    .line 97
    .local v0, resParams:[I
    const/4 v1, 0x0

    #@4
    sget v2, Lcom/qualcomm/wfd/WfdEnums;->resWidth:I

    #@6
    aput v2, v0, v1

    #@8
    .line 98
    const/4 v1, 0x1

    #@9
    sget v2, Lcom/qualcomm/wfd/WfdEnums;->resHeight:I

    #@b
    aput v2, v0, v1

    #@d
    .line 99
    const/4 v1, 0x2

    #@e
    sget v2, Lcom/qualcomm/wfd/WfdEnums;->resFps:I

    #@10
    aput v2, v0, v1

    #@12
    .line 100
    return-object v0
.end method

.method public static final isCeaResolution(I)Z
    .registers 7
    .parameter "resolution"

    #@0
    .prologue
    const/16 v5, 0x32

    #@2
    const/16 v4, 0x3c

    #@4
    const/16 v3, 0x780

    #@6
    const/16 v1, 0x438

    #@8
    const/16 v2, 0x2d0

    #@a
    .line 104
    sparse-switch p0, :sswitch_data_76

    #@d
    .line 174
    const/4 v0, 0x0

    #@e
    .line 176
    :goto_e
    return v0

    #@f
    .line 106
    :sswitch_f
    const/16 v0, 0x280

    #@11
    const/16 v1, 0x1e0

    #@13
    invoke-static {v0, v1, v4}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@16
    .line 176
    :goto_16
    const/4 v0, 0x1

    #@17
    goto :goto_e

    #@18
    .line 110
    :sswitch_18
    const/16 v0, 0x1e0

    #@1a
    invoke-static {v2, v0, v4}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@1d
    goto :goto_16

    #@1e
    .line 114
    :sswitch_1e
    const/16 v0, 0x1e0

    #@20
    invoke-static {v2, v0, v4}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@23
    goto :goto_16

    #@24
    .line 118
    :sswitch_24
    const/16 v0, 0x240

    #@26
    invoke-static {v2, v0, v5}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@29
    goto :goto_16

    #@2a
    .line 122
    :sswitch_2a
    const/16 v0, 0x240

    #@2c
    invoke-static {v2, v0, v5}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@2f
    goto :goto_16

    #@30
    .line 126
    :sswitch_30
    const/16 v0, 0x500

    #@32
    const/16 v1, 0x1e

    #@34
    invoke-static {v0, v2, v1}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@37
    goto :goto_16

    #@38
    .line 130
    :sswitch_38
    const/16 v0, 0x500

    #@3a
    invoke-static {v0, v2, v4}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@3d
    goto :goto_16

    #@3e
    .line 134
    :sswitch_3e
    const/16 v0, 0x1e

    #@40
    invoke-static {v3, v1, v0}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@43
    goto :goto_16

    #@44
    .line 138
    :sswitch_44
    invoke-static {v3, v1, v4}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@47
    goto :goto_16

    #@48
    .line 142
    :sswitch_48
    invoke-static {v3, v1, v4}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@4b
    goto :goto_16

    #@4c
    .line 146
    :sswitch_4c
    const/16 v0, 0x500

    #@4e
    const/16 v1, 0x19

    #@50
    invoke-static {v0, v2, v1}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@53
    goto :goto_16

    #@54
    .line 150
    :sswitch_54
    const/16 v0, 0x500

    #@56
    invoke-static {v0, v2, v5}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@59
    goto :goto_16

    #@5a
    .line 154
    :sswitch_5a
    const/16 v0, 0x19

    #@5c
    invoke-static {v3, v1, v0}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@5f
    goto :goto_16

    #@60
    .line 158
    :sswitch_60
    invoke-static {v3, v1, v5}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@63
    goto :goto_16

    #@64
    .line 162
    :sswitch_64
    invoke-static {v3, v1, v5}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@67
    goto :goto_16

    #@68
    .line 166
    :sswitch_68
    const/16 v0, 0x500

    #@6a
    const/16 v1, 0x18

    #@6c
    invoke-static {v0, v2, v1}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@6f
    goto :goto_16

    #@70
    .line 170
    :sswitch_70
    const/16 v0, 0x18

    #@72
    invoke-static {v3, v1, v0}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@75
    goto :goto_16

    #@76
    .line 104
    :sswitch_data_76
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_18
        0x4 -> :sswitch_1e
        0x8 -> :sswitch_24
        0x10 -> :sswitch_2a
        0x20 -> :sswitch_30
        0x40 -> :sswitch_38
        0x80 -> :sswitch_3e
        0x100 -> :sswitch_44
        0x200 -> :sswitch_48
        0x400 -> :sswitch_4c
        0x800 -> :sswitch_54
        0x1000 -> :sswitch_5a
        0x2000 -> :sswitch_60
        0x4000 -> :sswitch_64
        0x8000 -> :sswitch_68
        0x10000 -> :sswitch_70
    .end sparse-switch
.end method

.method public static final isHhResolution(I)Z
    .registers 6
    .parameter "resolution"

    #@0
    .prologue
    const/16 v4, 0x280

    #@2
    const/16 v0, 0x168

    #@4
    const/16 v3, 0x3c

    #@6
    const/16 v2, 0x1e

    #@8
    const/16 v1, 0x1e0

    #@a
    .line 353
    sparse-switch p0, :sswitch_data_44

    #@d
    .line 390
    const/4 v0, 0x0

    #@e
    .line 392
    :goto_e
    return v0

    #@f
    .line 355
    :sswitch_f
    const/16 v0, 0x320

    #@11
    invoke-static {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@14
    .line 392
    :goto_14
    const/4 v0, 0x1

    #@15
    goto :goto_e

    #@16
    .line 359
    :sswitch_16
    const/16 v0, 0x320

    #@18
    invoke-static {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@1b
    goto :goto_14

    #@1c
    .line 363
    :sswitch_1c
    const/16 v0, 0x360

    #@1e
    invoke-static {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@21
    goto :goto_14

    #@22
    .line 367
    :sswitch_22
    const/16 v0, 0x360

    #@24
    invoke-static {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@27
    goto :goto_14

    #@28
    .line 371
    :sswitch_28
    invoke-static {v4, v0, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@2b
    goto :goto_14

    #@2c
    .line 375
    :sswitch_2c
    invoke-static {v4, v0, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@2f
    goto :goto_14

    #@30
    .line 379
    :sswitch_30
    const/16 v0, 0x3c0

    #@32
    const/16 v1, 0x21c

    #@34
    invoke-static {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@37
    goto :goto_14

    #@38
    .line 383
    :sswitch_38
    const/16 v0, 0x350

    #@3a
    invoke-static {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@3d
    .line 386
    :sswitch_3d
    const/16 v0, 0x350

    #@3f
    invoke-static {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@42
    goto :goto_14

    #@43
    .line 353
    nop

    #@44
    :sswitch_data_44
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_16
        0x10 -> :sswitch_1c
        0x20 -> :sswitch_22
        0x40 -> :sswitch_28
        0x80 -> :sswitch_2c
        0x100 -> :sswitch_30
        0x400 -> :sswitch_38
        0x800 -> :sswitch_3d
    .end sparse-switch
.end method

.method public static final isVesaResolution(I)Z
    .registers 7
    .parameter "resolution"

    #@0
    .prologue
    const/16 v5, 0x500

    #@2
    const/16 v4, 0x400

    #@4
    const/16 v1, 0x300

    #@6
    const/16 v3, 0x3c

    #@8
    const/16 v2, 0x1e

    #@a
    .line 212
    sparse-switch p0, :sswitch_data_e2

    #@d
    .line 334
    const/4 v0, 0x0

    #@e
    .line 336
    :goto_e
    return v0

    #@f
    .line 214
    :sswitch_f
    const/16 v0, 0x320

    #@11
    const/16 v1, 0x258

    #@13
    invoke-static {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@16
    .line 336
    :goto_16
    const/4 v0, 0x1

    #@17
    goto :goto_e

    #@18
    .line 218
    :sswitch_18
    const/16 v0, 0x320

    #@1a
    const/16 v1, 0x258

    #@1c
    invoke-static {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@1f
    goto :goto_16

    #@20
    .line 222
    :sswitch_20
    invoke-static {v4, v1, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@23
    goto :goto_16

    #@24
    .line 226
    :sswitch_24
    invoke-static {v4, v1, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@27
    goto :goto_16

    #@28
    .line 230
    :sswitch_28
    const/16 v0, 0x480

    #@2a
    const/16 v1, 0x360

    #@2c
    invoke-static {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@2f
    goto :goto_16

    #@30
    .line 234
    :sswitch_30
    const/16 v0, 0x480

    #@32
    const/16 v1, 0x360

    #@34
    invoke-static {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@37
    goto :goto_16

    #@38
    .line 238
    :sswitch_38
    invoke-static {v5, v1, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@3b
    goto :goto_16

    #@3c
    .line 242
    :sswitch_3c
    invoke-static {v5, v1, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@3f
    goto :goto_16

    #@40
    .line 246
    :sswitch_40
    const/16 v0, 0x320

    #@42
    invoke-static {v5, v0, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@45
    goto :goto_16

    #@46
    .line 250
    :sswitch_46
    const/16 v0, 0x320

    #@48
    invoke-static {v5, v0, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@4b
    goto :goto_16

    #@4c
    .line 254
    :sswitch_4c
    const/16 v0, 0x550

    #@4e
    invoke-static {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@51
    goto :goto_16

    #@52
    .line 258
    :sswitch_52
    const/16 v0, 0x550

    #@54
    invoke-static {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@57
    goto :goto_16

    #@58
    .line 262
    :sswitch_58
    const/16 v0, 0x556

    #@5a
    invoke-static {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@5d
    goto :goto_16

    #@5e
    .line 266
    :sswitch_5e
    const/16 v0, 0x556

    #@60
    invoke-static {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@63
    goto :goto_16

    #@64
    .line 270
    :sswitch_64
    invoke-static {v5, v4, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@67
    goto :goto_16

    #@68
    .line 274
    :sswitch_68
    invoke-static {v5, v4, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@6b
    goto :goto_16

    #@6c
    .line 278
    :sswitch_6c
    const/16 v0, 0x578

    #@6e
    const/16 v1, 0x41a

    #@70
    invoke-static {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@73
    goto :goto_16

    #@74
    .line 282
    :sswitch_74
    const/16 v0, 0x578

    #@76
    const/16 v1, 0x41a

    #@78
    invoke-static {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@7b
    goto :goto_16

    #@7c
    .line 286
    :sswitch_7c
    const/16 v0, 0x5a0

    #@7e
    const/16 v1, 0x384

    #@80
    invoke-static {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@83
    goto :goto_16

    #@84
    .line 290
    :sswitch_84
    const/16 v0, 0x5a0

    #@86
    const/16 v1, 0x384

    #@88
    invoke-static {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@8b
    goto :goto_16

    #@8c
    .line 294
    :sswitch_8c
    const/16 v0, 0x640

    #@8e
    const/16 v1, 0x384

    #@90
    invoke-static {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@93
    goto :goto_16

    #@94
    .line 298
    :sswitch_94
    const/16 v0, 0x640

    #@96
    const/16 v1, 0x384

    #@98
    invoke-static {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@9b
    goto/16 :goto_16

    #@9d
    .line 302
    :sswitch_9d
    const/16 v0, 0x640

    #@9f
    const/16 v1, 0x4b0

    #@a1
    invoke-static {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@a4
    goto/16 :goto_16

    #@a6
    .line 306
    :sswitch_a6
    const/16 v0, 0x640

    #@a8
    const/16 v1, 0x4b0

    #@aa
    invoke-static {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@ad
    goto/16 :goto_16

    #@af
    .line 310
    :sswitch_af
    const/16 v0, 0x690

    #@b1
    invoke-static {v0, v4, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@b4
    goto/16 :goto_16

    #@b6
    .line 314
    :sswitch_b6
    const/16 v0, 0x690

    #@b8
    invoke-static {v0, v4, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@bb
    goto/16 :goto_16

    #@bd
    .line 318
    :sswitch_bd
    const/16 v0, 0x690

    #@bf
    const/16 v1, 0x41a

    #@c1
    invoke-static {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@c4
    goto/16 :goto_16

    #@c6
    .line 322
    :sswitch_c6
    const/16 v0, 0x690

    #@c8
    const/16 v1, 0x41a

    #@ca
    invoke-static {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@cd
    goto/16 :goto_16

    #@cf
    .line 326
    :sswitch_cf
    const/16 v0, 0x780

    #@d1
    const/16 v1, 0x4b0

    #@d3
    invoke-static {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@d6
    goto/16 :goto_16

    #@d8
    .line 330
    :sswitch_d8
    const/16 v0, 0x780

    #@da
    const/16 v1, 0x4b0

    #@dc
    invoke-static {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    #@df
    goto/16 :goto_16

    #@e1
    .line 212
    nop

    #@e2
    :sswitch_data_e2
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_18
        0x4 -> :sswitch_20
        0x8 -> :sswitch_24
        0x10 -> :sswitch_28
        0x20 -> :sswitch_30
        0x40 -> :sswitch_38
        0x80 -> :sswitch_3c
        0x100 -> :sswitch_40
        0x200 -> :sswitch_46
        0x400 -> :sswitch_4c
        0x800 -> :sswitch_52
        0x1000 -> :sswitch_58
        0x2000 -> :sswitch_5e
        0x4000 -> :sswitch_64
        0x8000 -> :sswitch_68
        0x10000 -> :sswitch_6c
        0x20000 -> :sswitch_74
        0x40000 -> :sswitch_7c
        0x80000 -> :sswitch_84
        0x100000 -> :sswitch_8c
        0x200000 -> :sswitch_94
        0x400000 -> :sswitch_9d
        0x800000 -> :sswitch_a6
        0x1000000 -> :sswitch_af
        0x2000000 -> :sswitch_b6
        0x4000000 -> :sswitch_bd
        0x8000000 -> :sswitch_c6
        0x10000000 -> :sswitch_cf
        0x20000000 -> :sswitch_d8
    .end sparse-switch
.end method

.method private static setResParams(III)V
    .registers 3
    .parameter "width"
    .parameter "height"
    .parameter "fps"

    #@0
    .prologue
    .line 90
    sput p0, Lcom/qualcomm/wfd/WfdEnums;->resWidth:I

    #@2
    .line 91
    sput p1, Lcom/qualcomm/wfd/WfdEnums;->resHeight:I

    #@4
    .line 92
    sput p2, Lcom/qualcomm/wfd/WfdEnums;->resFps:I

    #@6
    .line 93
    return-void
.end method
