.class final Lcom/qualcomm/wfd/WfdDevice$1;
.super Ljava/lang/Object;
.source "WfdDevice.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/WfdDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/qualcomm/wfd/WfdDevice;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 40
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/qualcomm/wfd/WfdDevice;
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 42
    new-instance v0, Lcom/qualcomm/wfd/WfdDevice;

    #@2
    invoke-direct {v0, p1}, Lcom/qualcomm/wfd/WfdDevice;-><init>(Landroid/os/Parcel;)V

    #@5
    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/qualcomm/wfd/WfdDevice$1;->createFromParcel(Landroid/os/Parcel;)Lcom/qualcomm/wfd/WfdDevice;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public newArray(I)[Lcom/qualcomm/wfd/WfdDevice;
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 46
    new-array v0, p1, [Lcom/qualcomm/wfd/WfdDevice;

    #@2
    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/qualcomm/wfd/WfdDevice$1;->newArray(I)[Lcom/qualcomm/wfd/WfdDevice;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
