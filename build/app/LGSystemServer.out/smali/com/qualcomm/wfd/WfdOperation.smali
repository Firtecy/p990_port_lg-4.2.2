.class final enum Lcom/qualcomm/wfd/WfdOperation;
.super Ljava/lang/Enum;
.source "ExtendedRemoteDisplay.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/wfd/WfdOperation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/wfd/WfdOperation;

.field public static final enum PAUSE:Lcom/qualcomm/wfd/WfdOperation;

.field public static final enum PLAY:Lcom/qualcomm/wfd/WfdOperation;

.field public static final enum RESUME:Lcom/qualcomm/wfd/WfdOperation;

.field public static final enum STANDBY:Lcom/qualcomm/wfd/WfdOperation;

.field public static final enum START_UIBC:Lcom/qualcomm/wfd/WfdOperation;

.field public static final enum STOP_UIBC:Lcom/qualcomm/wfd/WfdOperation;

.field public static final enum TEARDOWN:Lcom/qualcomm/wfd/WfdOperation;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 58
    new-instance v0, Lcom/qualcomm/wfd/WfdOperation;

    #@7
    const-string v1, "PLAY"

    #@9
    invoke-direct {v0, v1, v3}, Lcom/qualcomm/wfd/WfdOperation;-><init>(Ljava/lang/String;I)V

    #@c
    sput-object v0, Lcom/qualcomm/wfd/WfdOperation;->PLAY:Lcom/qualcomm/wfd/WfdOperation;

    #@e
    .line 59
    new-instance v0, Lcom/qualcomm/wfd/WfdOperation;

    #@10
    const-string v1, "PAUSE"

    #@12
    invoke-direct {v0, v1, v4}, Lcom/qualcomm/wfd/WfdOperation;-><init>(Ljava/lang/String;I)V

    #@15
    sput-object v0, Lcom/qualcomm/wfd/WfdOperation;->PAUSE:Lcom/qualcomm/wfd/WfdOperation;

    #@17
    .line 60
    new-instance v0, Lcom/qualcomm/wfd/WfdOperation;

    #@19
    const-string v1, "STANDBY"

    #@1b
    invoke-direct {v0, v1, v5}, Lcom/qualcomm/wfd/WfdOperation;-><init>(Ljava/lang/String;I)V

    #@1e
    sput-object v0, Lcom/qualcomm/wfd/WfdOperation;->STANDBY:Lcom/qualcomm/wfd/WfdOperation;

    #@20
    .line 61
    new-instance v0, Lcom/qualcomm/wfd/WfdOperation;

    #@22
    const-string v1, "RESUME"

    #@24
    invoke-direct {v0, v1, v6}, Lcom/qualcomm/wfd/WfdOperation;-><init>(Ljava/lang/String;I)V

    #@27
    sput-object v0, Lcom/qualcomm/wfd/WfdOperation;->RESUME:Lcom/qualcomm/wfd/WfdOperation;

    #@29
    .line 62
    new-instance v0, Lcom/qualcomm/wfd/WfdOperation;

    #@2b
    const-string v1, "START_UIBC"

    #@2d
    invoke-direct {v0, v1, v7}, Lcom/qualcomm/wfd/WfdOperation;-><init>(Ljava/lang/String;I)V

    #@30
    sput-object v0, Lcom/qualcomm/wfd/WfdOperation;->START_UIBC:Lcom/qualcomm/wfd/WfdOperation;

    #@32
    .line 63
    new-instance v0, Lcom/qualcomm/wfd/WfdOperation;

    #@34
    const-string v1, "STOP_UIBC"

    #@36
    const/4 v2, 0x5

    #@37
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdOperation;-><init>(Ljava/lang/String;I)V

    #@3a
    sput-object v0, Lcom/qualcomm/wfd/WfdOperation;->STOP_UIBC:Lcom/qualcomm/wfd/WfdOperation;

    #@3c
    .line 64
    new-instance v0, Lcom/qualcomm/wfd/WfdOperation;

    #@3e
    const-string v1, "TEARDOWN"

    #@40
    const/4 v2, 0x6

    #@41
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdOperation;-><init>(Ljava/lang/String;I)V

    #@44
    sput-object v0, Lcom/qualcomm/wfd/WfdOperation;->TEARDOWN:Lcom/qualcomm/wfd/WfdOperation;

    #@46
    .line 57
    const/4 v0, 0x7

    #@47
    new-array v0, v0, [Lcom/qualcomm/wfd/WfdOperation;

    #@49
    sget-object v1, Lcom/qualcomm/wfd/WfdOperation;->PLAY:Lcom/qualcomm/wfd/WfdOperation;

    #@4b
    aput-object v1, v0, v3

    #@4d
    sget-object v1, Lcom/qualcomm/wfd/WfdOperation;->PAUSE:Lcom/qualcomm/wfd/WfdOperation;

    #@4f
    aput-object v1, v0, v4

    #@51
    sget-object v1, Lcom/qualcomm/wfd/WfdOperation;->STANDBY:Lcom/qualcomm/wfd/WfdOperation;

    #@53
    aput-object v1, v0, v5

    #@55
    sget-object v1, Lcom/qualcomm/wfd/WfdOperation;->RESUME:Lcom/qualcomm/wfd/WfdOperation;

    #@57
    aput-object v1, v0, v6

    #@59
    sget-object v1, Lcom/qualcomm/wfd/WfdOperation;->START_UIBC:Lcom/qualcomm/wfd/WfdOperation;

    #@5b
    aput-object v1, v0, v7

    #@5d
    const/4 v1, 0x5

    #@5e
    sget-object v2, Lcom/qualcomm/wfd/WfdOperation;->STOP_UIBC:Lcom/qualcomm/wfd/WfdOperation;

    #@60
    aput-object v2, v0, v1

    #@62
    const/4 v1, 0x6

    #@63
    sget-object v2, Lcom/qualcomm/wfd/WfdOperation;->TEARDOWN:Lcom/qualcomm/wfd/WfdOperation;

    #@65
    aput-object v2, v0, v1

    #@67
    sput-object v0, Lcom/qualcomm/wfd/WfdOperation;->$VALUES:[Lcom/qualcomm/wfd/WfdOperation;

    #@69
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/wfd/WfdOperation;
    .registers 2
    .parameter

    #@0
    .prologue
    .line 57
    const-class v0, Lcom/qualcomm/wfd/WfdOperation;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/qualcomm/wfd/WfdOperation;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/qualcomm/wfd/WfdOperation;
    .registers 1

    #@0
    .prologue
    .line 57
    sget-object v0, Lcom/qualcomm/wfd/WfdOperation;->$VALUES:[Lcom/qualcomm/wfd/WfdOperation;

    #@2
    invoke-virtual {v0}, [Lcom/qualcomm/wfd/WfdOperation;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/qualcomm/wfd/WfdOperation;

    #@8
    return-object v0
.end method
