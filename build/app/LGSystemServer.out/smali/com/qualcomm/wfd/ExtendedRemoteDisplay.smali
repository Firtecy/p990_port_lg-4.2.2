.class public final Lcom/qualcomm/wfd/ExtendedRemoteDisplay;
.super Ljava/lang/Object;
.source "ExtendedRemoteDisplay.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;,
        Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;
    }
.end annotation


# static fields
.field public static final DISPLAY_ERROR_CONNECTION_DROPPED:I = 0x2

.field public static final DISPLAY_ERROR_UNKOWN:I = 0x1

.field public static final DISPLAY_FLAG_SECURE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "ExtendedRemoteDisplay"

.field private static final WFD_PRIMARY_SINK:I = 0x1

.field private static final WFD_SOURCE:I

.field public static mRefs:I


# instance fields
.field private mActionListener:Lcom/qualcomm/wfd/service/IWfdActionListener;

.field public mCmdHdlr:Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;

.field private mConnected:Z

.field private mContext:Landroid/content/Context;

.field private mEventHandler:Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;

.field private final mGuard:Ldalvik/system/CloseGuard;

.field private mHThread:Landroid/os/HandlerThread;

.field private mHandler:Landroid/os/Handler;

.field private mIface:Ljava/lang/String;

.field private mListener:Landroid/media/RemoteDisplay$Listener;

.field private mLocalWfdDevice:Lcom/qualcomm/wfd/WfdDevice;

.field private mLooper:Landroid/os/Looper;

.field private mPeerWfdDevice:Lcom/qualcomm/wfd/WfdDevice;

.field private mPtr:I

.field private mState:Lcom/qualcomm/wfd/WFDState;

.field private surface:Landroid/view/Surface;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 297
    const-string v0, "extendedremotedisplay"

    #@2
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@5
    .line 298
    const-string v0, "ExtendedRemoteDisplay"

    #@7
    const-string v1, "Loaded extendedremotedisplay.so"

    #@9
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 301
    sget v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mRefs:I

    #@e
    add-int/lit8 v0, v0, 0x1

    #@10
    sput v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mRefs:I

    #@12
    .line 302
    return-void
.end method

.method public constructor <init>(Landroid/media/RemoteDisplay$Listener;Landroid/os/Handler;Landroid/content/Context;)V
    .registers 6
    .parameter "listener"
    .parameter "handler"
    .parameter "context"

    #@0
    .prologue
    .line 346
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 317
    invoke-static {}, Ldalvik/system/CloseGuard;->get()Ldalvik/system/CloseGuard;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mGuard:Ldalvik/system/CloseGuard;

    #@9
    .line 336
    sget-object v0, Lcom/qualcomm/wfd/WFDState;->DEINIT:Lcom/qualcomm/wfd/WFDState;

    #@b
    iput-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mState:Lcom/qualcomm/wfd/WFDState;

    #@d
    .line 347
    iput-object p1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mListener:Landroid/media/RemoteDisplay$Listener;

    #@f
    .line 348
    iput-object p2, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mHandler:Landroid/os/Handler;

    #@11
    .line 349
    iput-object p3, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mContext:Landroid/content/Context;

    #@13
    .line 350
    new-instance v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;

    #@15
    invoke-direct {v0, p0}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;-><init>(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)V

    #@18
    iput-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mEventHandler:Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;

    #@1a
    .line 352
    new-instance v0, Landroid/os/HandlerThread;

    #@1c
    const-string v1, "ExtendedRemoteDisplay"

    #@1e
    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@21
    iput-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mHThread:Landroid/os/HandlerThread;

    #@23
    .line 353
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mHThread:Landroid/os/HandlerThread;

    #@25
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    #@28
    .line 354
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mHThread:Landroid/os/HandlerThread;

    #@2a
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@2d
    move-result-object v0

    #@2e
    iput-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mLooper:Landroid/os/Looper;

    #@30
    .line 355
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mLooper:Landroid/os/Looper;

    #@32
    if-eqz v0, :cond_3d

    #@34
    .line 356
    new-instance v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;

    #@36
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mLooper:Landroid/os/Looper;

    #@38
    invoke-direct {v0, p0, v1}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;-><init>(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Landroid/os/Looper;)V

    #@3b
    iput-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mCmdHdlr:Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;

    #@3d
    .line 358
    :cond_3d
    return-void
.end method

.method static synthetic access$000(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 291
    iget-boolean v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mConnected:Z

    #@2
    return v0
.end method

.method static synthetic access$002(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 291
    iput-boolean p1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mConnected:Z

    #@2
    return p1
.end method

.method static synthetic access$100(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Landroid/media/RemoteDisplay$Listener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 291
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mListener:Landroid/media/RemoteDisplay$Listener;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 291
    invoke-direct {p0, p1}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->destroyNativeObject(I)V

    #@3
    return-void
.end method

.method static synthetic access$1100(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 291
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1200(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 291
    invoke-direct {p0}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->getNativeObject()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1300(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;III)Landroid/view/Surface;
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 291
    invoke-direct {p0, p1, p2, p3}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->getSurface(III)Landroid/view/Surface;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$1400(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Landroid/view/Surface;III)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 291
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->notifyDisplayConnected(Landroid/view/Surface;III)V

    #@3
    return-void
.end method

.method static synthetic access$1500(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Lcom/qualcomm/wfd/WfdDevice;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 291
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mPeerWfdDevice:Lcom/qualcomm/wfd/WfdDevice;

    #@2
    return-object v0
.end method

.method static synthetic access$1600(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 291
    invoke-direct {p0, p1}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->notifyDisplayError(I)V

    #@3
    return-void
.end method

.method static synthetic access$1700(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 291
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mIface:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$1800(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 291
    invoke-direct {p0, p1}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->createLocalWFDDevice(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$1900(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 291
    invoke-direct {p0, p1}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->createPeerWFDDevice(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Lcom/qualcomm/wfd/WFDState;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 291
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mState:Lcom/qualcomm/wfd/WFDState;

    #@2
    return-object v0
.end method

.method static synthetic access$202(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Lcom/qualcomm/wfd/WFDState;)Lcom/qualcomm/wfd/WFDState;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 291
    iput-object p1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mState:Lcom/qualcomm/wfd/WFDState;

    #@2
    return-object p1
.end method

.method static synthetic access$300(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Lcom/qualcomm/wfd/service/IWfdActionListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 291
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mActionListener:Lcom/qualcomm/wfd/service/IWfdActionListener;

    #@2
    return-object v0
.end method

.method static synthetic access$302(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Lcom/qualcomm/wfd/service/IWfdActionListener;)Lcom/qualcomm/wfd/service/IWfdActionListener;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 291
    iput-object p1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mActionListener:Lcom/qualcomm/wfd/service/IWfdActionListener;

    #@2
    return-object p1
.end method

.method static synthetic access$400(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 291
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mEventHandler:Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Lcom/qualcomm/wfd/WfdDevice;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 291
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mLocalWfdDevice:Lcom/qualcomm/wfd/WfdDevice;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 291
    invoke-direct {p0}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->notifyDisplayDisconnected()V

    #@3
    return-void
.end method

.method static synthetic access$700(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 291
    iget v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mPtr:I

    #@2
    return v0
.end method

.method static synthetic access$702(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 291
    iput p1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mPtr:I

    #@2
    return p1
.end method

.method static synthetic access$800(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Landroid/view/Surface;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 291
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->surface:Landroid/view/Surface;

    #@2
    return-object v0
.end method

.method static synthetic access$802(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Landroid/view/Surface;)Landroid/view/Surface;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 291
    iput-object p1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->surface:Landroid/view/Surface;

    #@2
    return-object p1
.end method

.method static synthetic access$900(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;ILandroid/view/Surface;)I
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 291
    invoke-direct {p0, p1, p2}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->destroySurface(ILandroid/view/Surface;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private createLocalWFDDevice(Ljava/lang/String;)V
    .registers 8
    .parameter "iface"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x0

    #@2
    .line 445
    new-instance v1, Lcom/qualcomm/wfd/WfdDevice;

    #@4
    invoke-direct {v1}, Lcom/qualcomm/wfd/WfdDevice;-><init>()V

    #@7
    iput-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mLocalWfdDevice:Lcom/qualcomm/wfd/WfdDevice;

    #@9
    .line 446
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mLocalWfdDevice:Lcom/qualcomm/wfd/WfdDevice;

    #@b
    iput v4, v1, Lcom/qualcomm/wfd/WfdDevice;->deviceType:I

    #@d
    .line 447
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mLocalWfdDevice:Lcom/qualcomm/wfd/WfdDevice;

    #@f
    iput-object v5, v1, Lcom/qualcomm/wfd/WfdDevice;->macAddress:Ljava/lang/String;

    #@11
    .line 449
    if-eqz p1, :cond_6c

    #@13
    .line 450
    const-string v1, "ExtendedRemoteDisplay"

    #@15
    new-instance v2, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v3, "Create WFDDevice from"

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 451
    const/16 v1, 0x3a

    #@2d
    invoke-virtual {p1, v1, v4}, Ljava/lang/String;->indexOf(II)I

    #@30
    move-result v0

    #@31
    .line 452
    .local v0, index:I
    if-lez v0, :cond_6c

    #@33
    .line 453
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mLocalWfdDevice:Lcom/qualcomm/wfd/WfdDevice;

    #@35
    invoke-virtual {p1, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@38
    move-result-object v2

    #@39
    iput-object v2, v1, Lcom/qualcomm/wfd/WfdDevice;->ipAddress:Ljava/lang/String;

    #@3b
    .line 454
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mLocalWfdDevice:Lcom/qualcomm/wfd/WfdDevice;

    #@3d
    add-int/lit8 v2, v0, 0x1

    #@3f
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@42
    move-result v3

    #@43
    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@46
    move-result-object v2

    #@47
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@4a
    move-result v2

    #@4b
    iput v2, v1, Lcom/qualcomm/wfd/WfdDevice;->rtspPort:I

    #@4d
    .line 457
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mLocalWfdDevice:Lcom/qualcomm/wfd/WfdDevice;

    #@4f
    iget-object v1, v1, Lcom/qualcomm/wfd/WfdDevice;->ipAddress:Ljava/lang/String;

    #@51
    if-eqz v1, :cond_63

    #@53
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mLocalWfdDevice:Lcom/qualcomm/wfd/WfdDevice;

    #@55
    iget v1, v1, Lcom/qualcomm/wfd/WfdDevice;->rtspPort:I

    #@57
    const/4 v2, 0x1

    #@58
    if-lt v1, v2, :cond_63

    #@5a
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mLocalWfdDevice:Lcom/qualcomm/wfd/WfdDevice;

    #@5c
    iget v1, v1, Lcom/qualcomm/wfd/WfdDevice;->rtspPort:I

    #@5e
    const v2, 0xffff

    #@61
    if-le v1, v2, :cond_6c

    #@63
    .line 460
    :cond_63
    const-string v1, "ExtendedRemoteDisplay"

    #@65
    const-string v2, "Invalid RTSP port received or no valid IP"

    #@67
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6a
    .line 461
    iput-object v5, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mLocalWfdDevice:Lcom/qualcomm/wfd/WfdDevice;

    #@6c
    .line 465
    .end local v0           #index:I
    :cond_6c
    return-void
.end method

.method private createPeerWFDDevice(Ljava/lang/String;)V
    .registers 5
    .parameter "iface"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 471
    new-instance v0, Lcom/qualcomm/wfd/WfdDevice;

    #@3
    invoke-direct {v0}, Lcom/qualcomm/wfd/WfdDevice;-><init>()V

    #@6
    iput-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mPeerWfdDevice:Lcom/qualcomm/wfd/WfdDevice;

    #@8
    .line 472
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mPeerWfdDevice:Lcom/qualcomm/wfd/WfdDevice;

    #@a
    const/4 v1, 0x1

    #@b
    iput v1, v0, Lcom/qualcomm/wfd/WfdDevice;->deviceType:I

    #@d
    .line 473
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mPeerWfdDevice:Lcom/qualcomm/wfd/WfdDevice;

    #@f
    iput-object v2, v0, Lcom/qualcomm/wfd/WfdDevice;->macAddress:Ljava/lang/String;

    #@11
    .line 474
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mPeerWfdDevice:Lcom/qualcomm/wfd/WfdDevice;

    #@13
    iput-object v2, v0, Lcom/qualcomm/wfd/WfdDevice;->ipAddress:Ljava/lang/String;

    #@15
    .line 475
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mPeerWfdDevice:Lcom/qualcomm/wfd/WfdDevice;

    #@17
    const/4 v1, 0x0

    #@18
    iput v1, v0, Lcom/qualcomm/wfd/WfdDevice;->rtspPort:I

    #@1a
    .line 477
    return-void
.end method

.method private native destroyNativeObject(I)V
.end method

.method private native destroySurface(ILandroid/view/Surface;)I
.end method

.method private dispose(Z)V
    .registers 5
    .parameter "finalized"

    #@0
    .prologue
    .line 411
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mGuard:Ldalvik/system/CloseGuard;

    #@2
    if-eqz v1, :cond_b

    #@4
    .line 412
    if-eqz p1, :cond_18

    #@6
    .line 413
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mGuard:Ldalvik/system/CloseGuard;

    #@8
    invoke-virtual {v1}, Ldalvik/system/CloseGuard;->warnIfOpen()V

    #@b
    .line 418
    :cond_b
    :goto_b
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mCmdHdlr:Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;

    #@d
    const/4 v2, 0x1

    #@e
    invoke-virtual {v1, v2}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;->obtainMessage(I)Landroid/os/Message;

    #@11
    move-result-object v0

    #@12
    .line 420
    .local v0, messageEnd:Landroid/os/Message;
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mCmdHdlr:Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;

    #@14
    invoke-virtual {v1, v0}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;->sendMessage(Landroid/os/Message;)Z

    #@17
    .line 421
    return-void

    #@18
    .line 415
    .end local v0           #messageEnd:Landroid/os/Message;
    :cond_18
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mGuard:Ldalvik/system/CloseGuard;

    #@1a
    invoke-virtual {v1}, Ldalvik/system/CloseGuard;->close()V

    #@1d
    goto :goto_b
.end method

.method private native getNativeObject()I
.end method

.method private native getSurface(III)Landroid/view/Surface;
.end method

.method public static listen(Ljava/lang/String;Landroid/media/RemoteDisplay$Listener;Landroid/os/Handler;Landroid/content/Context;)Lcom/qualcomm/wfd/ExtendedRemoteDisplay;
    .registers 7
    .parameter "iface"
    .parameter "listener"
    .parameter "handler"
    .parameter "context"

    #@0
    .prologue
    .line 380
    if-nez p0, :cond_a

    #@2
    .line 381
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v2, "iface must not be null"

    #@6
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v1

    #@a
    .line 383
    :cond_a
    if-nez p1, :cond_14

    #@c
    .line 384
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@e
    const-string v2, "listener must not be null"

    #@10
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@13
    throw v1

    #@14
    .line 386
    :cond_14
    if-nez p2, :cond_1e

    #@16
    .line 387
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@18
    const-string v2, "handler must not be null"

    #@1a
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v1

    #@1e
    .line 390
    :cond_1e
    new-instance v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    #@20
    invoke-direct {v0, p1, p2, p3}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;-><init>(Landroid/media/RemoteDisplay$Listener;Landroid/os/Handler;Landroid/content/Context;)V

    #@23
    .line 393
    .local v0, display:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;
    iget-object v1, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mCmdHdlr:Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;

    #@25
    if-nez v1, :cond_29

    #@27
    .line 394
    const/4 v0, 0x0

    #@28
    .line 399
    .end local v0           #display:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;
    :goto_28
    return-object v0

    #@29
    .line 397
    .restart local v0       #display:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;
    :cond_29
    invoke-virtual {v0, p0}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->startListening(Ljava/lang/String;)V

    #@2c
    goto :goto_28
.end method

.method private notifyDisplayConnected(Landroid/view/Surface;III)V
    .registers 12
    .parameter "surface"
    .parameter "width"
    .parameter "height"
    .parameter "flags"

    #@0
    .prologue
    .line 481
    iget-object v6, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mHandler:Landroid/os/Handler;

    #@2
    new-instance v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$1;

    #@4
    move-object v1, p0

    #@5
    move-object v2, p1

    #@6
    move v3, p2

    #@7
    move v4, p3

    #@8
    move v5, p4

    #@9
    invoke-direct/range {v0 .. v5}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$1;-><init>(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Landroid/view/Surface;III)V

    #@c
    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@f
    .line 488
    return-void
.end method

.method private notifyDisplayDisconnected()V
    .registers 3

    #@0
    .prologue
    .line 491
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mHandler:Landroid/os/Handler;

    #@2
    new-instance v1, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$2;

    #@4
    invoke-direct {v1, p0}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$2;-><init>(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)V

    #@7
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@a
    .line 500
    return-void
.end method

.method private notifyDisplayError(I)V
    .registers 4
    .parameter "error"

    #@0
    .prologue
    .line 503
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mHandler:Landroid/os/Handler;

    #@2
    new-instance v1, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$3;

    #@4
    invoke-direct {v1, p0, p1}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$3;-><init>(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;I)V

    #@7
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@a
    .line 509
    return-void
.end method


# virtual methods
.method public dispose()V
    .registers 2

    #@0
    .prologue
    .line 406
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->dispose(Z)V

    #@4
    .line 407
    return-void
.end method

.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 363
    const/4 v0, 0x1

    #@1
    :try_start_1
    invoke-direct {p0, v0}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->dispose(Z)V
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_8

    #@4
    .line 365
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@7
    .line 367
    return-void

    #@8
    .line 365
    :catchall_8
    move-exception v0

    #@9
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@c
    throw v0
.end method

.method public startListening(Ljava/lang/String;)V
    .registers 5
    .parameter "iface"

    #@0
    .prologue
    .line 427
    iput-object p1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mIface:Ljava/lang/String;

    #@2
    .line 429
    const-string v1, "persist.sys.wfd.virtual"

    #@4
    const-string v2, "1"

    #@6
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 431
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mCmdHdlr:Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;

    #@b
    const/4 v2, 0x0

    #@c
    invoke-virtual {v1, v2}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;->obtainMessage(I)Landroid/os/Message;

    #@f
    move-result-object v0

    #@10
    .line 433
    .local v0, messageStart:Landroid/os/Message;
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mCmdHdlr:Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;

    #@12
    invoke-virtual {v1, v0}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;->sendMessage(Landroid/os/Message;)Z

    #@15
    .line 435
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mGuard:Ldalvik/system/CloseGuard;

    #@17
    const-string v2, "dispose"

    #@19
    invoke-virtual {v1, v2}, Ldalvik/system/CloseGuard;->open(Ljava/lang/String;)V

    #@1c
    .line 436
    return-void
.end method
