.class public final enum Lcom/qualcomm/wfd/WfdEnums$AudioCodec;
.super Ljava/lang/Enum;
.source "WfdEnums.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/WfdEnums;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AudioCodec"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/wfd/WfdEnums$AudioCodec;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/wfd/WfdEnums$AudioCodec;

.field public static final enum WFD_AUDIO_AAC:Lcom/qualcomm/wfd/WfdEnums$AudioCodec;

.field public static final enum WFD_AUDIO_DOLBY_DIGITAL:Lcom/qualcomm/wfd/WfdEnums$AudioCodec;

.field public static final enum WFD_AUDIO_INVALID:Lcom/qualcomm/wfd/WfdEnums$AudioCodec;

.field public static final enum WFD_AUDIO_LPCM:Lcom/qualcomm/wfd/WfdEnums$AudioCodec;

.field public static final enum WFD_AUDIO_UNK:Lcom/qualcomm/wfd/WfdEnums$AudioCodec;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    #@0
    .prologue
    const/4 v6, 0x4

    #@1
    const/4 v5, 0x3

    #@2
    const/4 v4, 0x2

    #@3
    const/4 v3, 0x1

    #@4
    const/4 v2, 0x0

    #@5
    .line 449
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$AudioCodec;

    #@7
    const-string v1, "WFD_AUDIO_UNK"

    #@9
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$AudioCodec;-><init>(Ljava/lang/String;I)V

    #@c
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$AudioCodec;->WFD_AUDIO_UNK:Lcom/qualcomm/wfd/WfdEnums$AudioCodec;

    #@e
    .line 450
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$AudioCodec;

    #@10
    const-string v1, "WFD_AUDIO_LPCM"

    #@12
    invoke-direct {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums$AudioCodec;-><init>(Ljava/lang/String;I)V

    #@15
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$AudioCodec;->WFD_AUDIO_LPCM:Lcom/qualcomm/wfd/WfdEnums$AudioCodec;

    #@17
    .line 451
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$AudioCodec;

    #@19
    const-string v1, "WFD_AUDIO_AAC"

    #@1b
    invoke-direct {v0, v1, v4}, Lcom/qualcomm/wfd/WfdEnums$AudioCodec;-><init>(Ljava/lang/String;I)V

    #@1e
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$AudioCodec;->WFD_AUDIO_AAC:Lcom/qualcomm/wfd/WfdEnums$AudioCodec;

    #@20
    .line 452
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$AudioCodec;

    #@22
    const-string v1, "WFD_AUDIO_DOLBY_DIGITAL"

    #@24
    invoke-direct {v0, v1, v5}, Lcom/qualcomm/wfd/WfdEnums$AudioCodec;-><init>(Ljava/lang/String;I)V

    #@27
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$AudioCodec;->WFD_AUDIO_DOLBY_DIGITAL:Lcom/qualcomm/wfd/WfdEnums$AudioCodec;

    #@29
    .line 453
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$AudioCodec;

    #@2b
    const-string v1, "WFD_AUDIO_INVALID"

    #@2d
    invoke-direct {v0, v1, v6}, Lcom/qualcomm/wfd/WfdEnums$AudioCodec;-><init>(Ljava/lang/String;I)V

    #@30
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$AudioCodec;->WFD_AUDIO_INVALID:Lcom/qualcomm/wfd/WfdEnums$AudioCodec;

    #@32
    .line 448
    const/4 v0, 0x5

    #@33
    new-array v0, v0, [Lcom/qualcomm/wfd/WfdEnums$AudioCodec;

    #@35
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$AudioCodec;->WFD_AUDIO_UNK:Lcom/qualcomm/wfd/WfdEnums$AudioCodec;

    #@37
    aput-object v1, v0, v2

    #@39
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$AudioCodec;->WFD_AUDIO_LPCM:Lcom/qualcomm/wfd/WfdEnums$AudioCodec;

    #@3b
    aput-object v1, v0, v3

    #@3d
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$AudioCodec;->WFD_AUDIO_AAC:Lcom/qualcomm/wfd/WfdEnums$AudioCodec;

    #@3f
    aput-object v1, v0, v4

    #@41
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$AudioCodec;->WFD_AUDIO_DOLBY_DIGITAL:Lcom/qualcomm/wfd/WfdEnums$AudioCodec;

    #@43
    aput-object v1, v0, v5

    #@45
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$AudioCodec;->WFD_AUDIO_INVALID:Lcom/qualcomm/wfd/WfdEnums$AudioCodec;

    #@47
    aput-object v1, v0, v6

    #@49
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$AudioCodec;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$AudioCodec;

    #@4b
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 448
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/wfd/WfdEnums$AudioCodec;
    .registers 2
    .parameter

    #@0
    .prologue
    .line 448
    const-class v0, Lcom/qualcomm/wfd/WfdEnums$AudioCodec;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/qualcomm/wfd/WfdEnums$AudioCodec;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/qualcomm/wfd/WfdEnums$AudioCodec;
    .registers 1

    #@0
    .prologue
    .line 448
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$AudioCodec;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$AudioCodec;

    #@2
    invoke-virtual {v0}, [Lcom/qualcomm/wfd/WfdEnums$AudioCodec;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/qualcomm/wfd/WfdEnums$AudioCodec;

    #@8
    return-object v0
.end method
