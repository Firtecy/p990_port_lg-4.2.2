.class public final enum Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;
.super Ljava/lang/Enum;
.source "WfdEnums.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/WfdEnums;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RtpTransportType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;

.field public static final enum TCP:Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;

.field public static final enum UDP:Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 512
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;

    #@4
    const-string v1, "UDP"

    #@6
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;-><init>(Ljava/lang/String;I)V

    #@9
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;->UDP:Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;

    #@b
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;

    #@d
    const-string v1, "TCP"

    #@f
    invoke-direct {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;-><init>(Ljava/lang/String;I)V

    #@12
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;->TCP:Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;

    #@14
    .line 511
    const/4 v0, 0x2

    #@15
    new-array v0, v0, [Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;

    #@17
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;->UDP:Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;

    #@19
    aput-object v1, v0, v2

    #@1b
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;->TCP:Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;

    #@1d
    aput-object v1, v0, v3

    #@1f
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;

    #@21
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 511
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;
    .registers 2
    .parameter

    #@0
    .prologue
    .line 511
    const-class v0, Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;
    .registers 1

    #@0
    .prologue
    .line 511
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;

    #@2
    invoke-virtual {v0}, [Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;

    #@8
    return-object v0
.end method
