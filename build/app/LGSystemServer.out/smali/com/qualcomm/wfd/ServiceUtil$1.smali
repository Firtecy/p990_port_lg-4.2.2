.class final Lcom/qualcomm/wfd/ServiceUtil$1;
.super Ljava/lang/Object;
.source "ExtendedRemoteDisplay.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/ServiceUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 167
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .registers 5
    .parameter
    .parameter

    #@0
    .prologue
    .line 170
    const-string v0, "ExtendedRemoteDisplay.ServiceUtil"

    #@2
    const-string v1, "Connection object created"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 171
    const/4 v0, 0x1

    #@8
    invoke-static {v0}, Lcom/qualcomm/wfd/ServiceUtil;->access$102(Z)Z

    #@b
    .line 172
    invoke-static {p2}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@e
    move-result-object v0

    #@f
    invoke-static {v0}, Lcom/qualcomm/wfd/ServiceUtil;->access$202(Lcom/qualcomm/wfd/service/ISessionManagerService;)Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@12
    .line 173
    const-class v1, Lcom/qualcomm/wfd/ServiceUtil;

    #@14
    monitor-enter v1

    #@15
    .line 174
    :try_start_15
    const-class v0, Lcom/qualcomm/wfd/ServiceUtil;

    #@17
    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    #@1a
    .line 175
    monitor-exit v1
    :try_end_1b
    .catchall {:try_start_15 .. :try_end_1b} :catchall_2c

    #@1b
    .line 176
    invoke-static {}, Lcom/qualcomm/wfd/ServiceUtil;->access$300()Landroid/os/Handler;

    #@1e
    move-result-object v0

    #@1f
    const/4 v1, 0x7

    #@20
    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@23
    move-result-object v0

    #@24
    .line 178
    invoke-static {}, Lcom/qualcomm/wfd/ServiceUtil;->access$300()Landroid/os/Handler;

    #@27
    move-result-object v1

    #@28
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@2b
    .line 179
    return-void

    #@2c
    .line 175
    :catchall_2c
    move-exception v0

    #@2d
    :try_start_2d
    monitor-exit v1
    :try_end_2e
    .catchall {:try_start_2d .. :try_end_2e} :catchall_2c

    #@2e
    throw v0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .registers 4
    .parameter "className"

    #@0
    .prologue
    .line 182
    const-string v0, "ExtendedRemoteDisplay.ServiceUtil"

    #@2
    const-string v1, "Remote service disconnected"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 183
    const/4 v0, 0x0

    #@8
    invoke-static {v0}, Lcom/qualcomm/wfd/ServiceUtil;->access$102(Z)Z

    #@b
    .line 184
    return-void
.end method
