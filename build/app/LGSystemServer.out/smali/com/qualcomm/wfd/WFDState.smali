.class final enum Lcom/qualcomm/wfd/WFDState;
.super Ljava/lang/Enum;
.source "ExtendedRemoteDisplay.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/wfd/WFDState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/wfd/WFDState;

.field public static final enum BINDING:Lcom/qualcomm/wfd/WFDState;

.field public static final enum BOUND:Lcom/qualcomm/wfd/WFDState;

.field public static final enum DEINIT:Lcom/qualcomm/wfd/WFDState;

.field public static final enum ESTABLISHED:Lcom/qualcomm/wfd/WFDState;

.field public static final enum ESTABLISHING:Lcom/qualcomm/wfd/WFDState;

.field public static final enum INITIALIZED:Lcom/qualcomm/wfd/WFDState;

.field public static final enum INITIALIZING:Lcom/qualcomm/wfd/WFDState;

.field public static final enum PLAY:Lcom/qualcomm/wfd/WFDState;

.field public static final enum PLAYING:Lcom/qualcomm/wfd/WFDState;

.field public static final enum TEARDOWN:Lcom/qualcomm/wfd/WFDState;

.field public static final enum TEARINGDOWN:Lcom/qualcomm/wfd/WFDState;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 75
    new-instance v0, Lcom/qualcomm/wfd/WFDState;

    #@7
    const-string v1, "DEINIT"

    #@9
    invoke-direct {v0, v1, v3}, Lcom/qualcomm/wfd/WFDState;-><init>(Ljava/lang/String;I)V

    #@c
    sput-object v0, Lcom/qualcomm/wfd/WFDState;->DEINIT:Lcom/qualcomm/wfd/WFDState;

    #@e
    .line 76
    new-instance v0, Lcom/qualcomm/wfd/WFDState;

    #@10
    const-string v1, "BINDING"

    #@12
    invoke-direct {v0, v1, v4}, Lcom/qualcomm/wfd/WFDState;-><init>(Ljava/lang/String;I)V

    #@15
    sput-object v0, Lcom/qualcomm/wfd/WFDState;->BINDING:Lcom/qualcomm/wfd/WFDState;

    #@17
    .line 77
    new-instance v0, Lcom/qualcomm/wfd/WFDState;

    #@19
    const-string v1, "BOUND"

    #@1b
    invoke-direct {v0, v1, v5}, Lcom/qualcomm/wfd/WFDState;-><init>(Ljava/lang/String;I)V

    #@1e
    sput-object v0, Lcom/qualcomm/wfd/WFDState;->BOUND:Lcom/qualcomm/wfd/WFDState;

    #@20
    .line 78
    new-instance v0, Lcom/qualcomm/wfd/WFDState;

    #@22
    const-string v1, "INITIALIZING"

    #@24
    invoke-direct {v0, v1, v6}, Lcom/qualcomm/wfd/WFDState;-><init>(Ljava/lang/String;I)V

    #@27
    sput-object v0, Lcom/qualcomm/wfd/WFDState;->INITIALIZING:Lcom/qualcomm/wfd/WFDState;

    #@29
    .line 79
    new-instance v0, Lcom/qualcomm/wfd/WFDState;

    #@2b
    const-string v1, "INITIALIZED"

    #@2d
    invoke-direct {v0, v1, v7}, Lcom/qualcomm/wfd/WFDState;-><init>(Ljava/lang/String;I)V

    #@30
    sput-object v0, Lcom/qualcomm/wfd/WFDState;->INITIALIZED:Lcom/qualcomm/wfd/WFDState;

    #@32
    .line 80
    new-instance v0, Lcom/qualcomm/wfd/WFDState;

    #@34
    const-string v1, "ESTABLISHING"

    #@36
    const/4 v2, 0x5

    #@37
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WFDState;-><init>(Ljava/lang/String;I)V

    #@3a
    sput-object v0, Lcom/qualcomm/wfd/WFDState;->ESTABLISHING:Lcom/qualcomm/wfd/WFDState;

    #@3c
    .line 81
    new-instance v0, Lcom/qualcomm/wfd/WFDState;

    #@3e
    const-string v1, "ESTABLISHED"

    #@40
    const/4 v2, 0x6

    #@41
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WFDState;-><init>(Ljava/lang/String;I)V

    #@44
    sput-object v0, Lcom/qualcomm/wfd/WFDState;->ESTABLISHED:Lcom/qualcomm/wfd/WFDState;

    #@46
    .line 82
    new-instance v0, Lcom/qualcomm/wfd/WFDState;

    #@48
    const-string v1, "PLAY"

    #@4a
    const/4 v2, 0x7

    #@4b
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WFDState;-><init>(Ljava/lang/String;I)V

    #@4e
    sput-object v0, Lcom/qualcomm/wfd/WFDState;->PLAY:Lcom/qualcomm/wfd/WFDState;

    #@50
    .line 83
    new-instance v0, Lcom/qualcomm/wfd/WFDState;

    #@52
    const-string v1, "PLAYING"

    #@54
    const/16 v2, 0x8

    #@56
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WFDState;-><init>(Ljava/lang/String;I)V

    #@59
    sput-object v0, Lcom/qualcomm/wfd/WFDState;->PLAYING:Lcom/qualcomm/wfd/WFDState;

    #@5b
    .line 84
    new-instance v0, Lcom/qualcomm/wfd/WFDState;

    #@5d
    const-string v1, "TEARINGDOWN"

    #@5f
    const/16 v2, 0x9

    #@61
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WFDState;-><init>(Ljava/lang/String;I)V

    #@64
    sput-object v0, Lcom/qualcomm/wfd/WFDState;->TEARINGDOWN:Lcom/qualcomm/wfd/WFDState;

    #@66
    .line 85
    new-instance v0, Lcom/qualcomm/wfd/WFDState;

    #@68
    const-string v1, "TEARDOWN"

    #@6a
    const/16 v2, 0xa

    #@6c
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WFDState;-><init>(Ljava/lang/String;I)V

    #@6f
    sput-object v0, Lcom/qualcomm/wfd/WFDState;->TEARDOWN:Lcom/qualcomm/wfd/WFDState;

    #@71
    .line 74
    const/16 v0, 0xb

    #@73
    new-array v0, v0, [Lcom/qualcomm/wfd/WFDState;

    #@75
    sget-object v1, Lcom/qualcomm/wfd/WFDState;->DEINIT:Lcom/qualcomm/wfd/WFDState;

    #@77
    aput-object v1, v0, v3

    #@79
    sget-object v1, Lcom/qualcomm/wfd/WFDState;->BINDING:Lcom/qualcomm/wfd/WFDState;

    #@7b
    aput-object v1, v0, v4

    #@7d
    sget-object v1, Lcom/qualcomm/wfd/WFDState;->BOUND:Lcom/qualcomm/wfd/WFDState;

    #@7f
    aput-object v1, v0, v5

    #@81
    sget-object v1, Lcom/qualcomm/wfd/WFDState;->INITIALIZING:Lcom/qualcomm/wfd/WFDState;

    #@83
    aput-object v1, v0, v6

    #@85
    sget-object v1, Lcom/qualcomm/wfd/WFDState;->INITIALIZED:Lcom/qualcomm/wfd/WFDState;

    #@87
    aput-object v1, v0, v7

    #@89
    const/4 v1, 0x5

    #@8a
    sget-object v2, Lcom/qualcomm/wfd/WFDState;->ESTABLISHING:Lcom/qualcomm/wfd/WFDState;

    #@8c
    aput-object v2, v0, v1

    #@8e
    const/4 v1, 0x6

    #@8f
    sget-object v2, Lcom/qualcomm/wfd/WFDState;->ESTABLISHED:Lcom/qualcomm/wfd/WFDState;

    #@91
    aput-object v2, v0, v1

    #@93
    const/4 v1, 0x7

    #@94
    sget-object v2, Lcom/qualcomm/wfd/WFDState;->PLAY:Lcom/qualcomm/wfd/WFDState;

    #@96
    aput-object v2, v0, v1

    #@98
    const/16 v1, 0x8

    #@9a
    sget-object v2, Lcom/qualcomm/wfd/WFDState;->PLAYING:Lcom/qualcomm/wfd/WFDState;

    #@9c
    aput-object v2, v0, v1

    #@9e
    const/16 v1, 0x9

    #@a0
    sget-object v2, Lcom/qualcomm/wfd/WFDState;->TEARINGDOWN:Lcom/qualcomm/wfd/WFDState;

    #@a2
    aput-object v2, v0, v1

    #@a4
    const/16 v1, 0xa

    #@a6
    sget-object v2, Lcom/qualcomm/wfd/WFDState;->TEARDOWN:Lcom/qualcomm/wfd/WFDState;

    #@a8
    aput-object v2, v0, v1

    #@aa
    sput-object v0, Lcom/qualcomm/wfd/WFDState;->$VALUES:[Lcom/qualcomm/wfd/WFDState;

    #@ac
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 74
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/wfd/WFDState;
    .registers 2
    .parameter

    #@0
    .prologue
    .line 74
    const-class v0, Lcom/qualcomm/wfd/WFDState;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/qualcomm/wfd/WFDState;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/qualcomm/wfd/WFDState;
    .registers 1

    #@0
    .prologue
    .line 74
    sget-object v0, Lcom/qualcomm/wfd/WFDState;->$VALUES:[Lcom/qualcomm/wfd/WFDState;

    #@2
    invoke-virtual {v0}, [Lcom/qualcomm/wfd/WFDState;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/qualcomm/wfd/WFDState;

    #@8
    return-object v0
.end method
