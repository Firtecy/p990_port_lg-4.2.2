.class public final enum Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;
.super Ljava/lang/Enum;
.source "WfdEnums.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/WfdEnums;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ControlCmdType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

.field public static final enum FLUSH:Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

.field public static final enum PAUSE:Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

.field public static final enum PLAY:Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

.field public static final enum STATUS:Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    #@0
    .prologue
    const/4 v5, 0x3

    #@1
    const/4 v4, 0x2

    #@2
    const/4 v3, 0x1

    #@3
    const/4 v2, 0x0

    #@4
    .line 516
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

    #@6
    const-string v1, "FLUSH"

    #@8
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;-><init>(Ljava/lang/String;I)V

    #@b
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;->FLUSH:Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

    #@d
    .line 517
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

    #@f
    const-string v1, "PLAY"

    #@11
    invoke-direct {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;-><init>(Ljava/lang/String;I)V

    #@14
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;->PLAY:Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

    #@16
    .line 518
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

    #@18
    const-string v1, "PAUSE"

    #@1a
    invoke-direct {v0, v1, v4}, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;-><init>(Ljava/lang/String;I)V

    #@1d
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;->PAUSE:Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

    #@1f
    .line 519
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

    #@21
    const-string v1, "STATUS"

    #@23
    invoke-direct {v0, v1, v5}, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;-><init>(Ljava/lang/String;I)V

    #@26
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;->STATUS:Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

    #@28
    .line 515
    const/4 v0, 0x4

    #@29
    new-array v0, v0, [Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

    #@2b
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;->FLUSH:Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

    #@2d
    aput-object v1, v0, v2

    #@2f
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;->PLAY:Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

    #@31
    aput-object v1, v0, v3

    #@33
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;->PAUSE:Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

    #@35
    aput-object v1, v0, v4

    #@37
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;->STATUS:Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

    #@39
    aput-object v1, v0, v5

    #@3b
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

    #@3d
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 515
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;
    .registers 2
    .parameter

    #@0
    .prologue
    .line 515
    const-class v0, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;
    .registers 1

    #@0
    .prologue
    .line 515
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

    #@2
    invoke-virtual {v0}, [Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

    #@8
    return-object v0
.end method
