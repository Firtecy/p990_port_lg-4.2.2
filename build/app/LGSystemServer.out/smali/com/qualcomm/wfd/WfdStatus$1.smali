.class final Lcom/qualcomm/wfd/WfdStatus$1;
.super Ljava/lang/Object;
.source "WfdStatus.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/WfdStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/qualcomm/wfd/WfdStatus;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 27
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/qualcomm/wfd/WfdStatus;
    .registers 4
    .parameter

    #@0
    .prologue
    .line 31
    new-instance v1, Lcom/qualcomm/wfd/WfdStatus;

    #@2
    invoke-direct {v1}, Lcom/qualcomm/wfd/WfdStatus;-><init>()V

    #@5
    .line 32
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@8
    move-result v0

    #@9
    iput v0, v1, Lcom/qualcomm/wfd/WfdStatus;->state:I

    #@b
    .line 33
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@e
    move-result v0

    #@f
    iput v0, v1, Lcom/qualcomm/wfd/WfdStatus;->sessionId:I

    #@11
    .line 34
    const-class v0, Lcom/qualcomm/wfd/WfdDevice;

    #@13
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    #@1a
    move-result-object v0

    #@1b
    check-cast v0, Lcom/qualcomm/wfd/WfdDevice;

    #@1d
    iput-object v0, v1, Lcom/qualcomm/wfd/WfdStatus;->connectedDevice:Lcom/qualcomm/wfd/WfdDevice;

    #@1f
    .line 35
    return-object v1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 27
    invoke-virtual {p0, p1}, Lcom/qualcomm/wfd/WfdStatus$1;->createFromParcel(Landroid/os/Parcel;)Lcom/qualcomm/wfd/WfdStatus;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public newArray(I)[Lcom/qualcomm/wfd/WfdStatus;
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 40
    new-array v0, p1, [Lcom/qualcomm/wfd/WfdStatus;

    #@2
    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 27
    invoke-virtual {p0, p1}, Lcom/qualcomm/wfd/WfdStatus$1;->newArray(I)[Lcom/qualcomm/wfd/WfdStatus;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
