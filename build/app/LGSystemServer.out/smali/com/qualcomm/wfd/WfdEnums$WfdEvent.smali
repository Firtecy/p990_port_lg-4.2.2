.class public final enum Lcom/qualcomm/wfd/WfdEnums$WfdEvent;
.super Ljava/lang/Enum;
.source "WfdEnums.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/WfdEnums;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "WfdEvent"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/wfd/WfdEnums$WfdEvent;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

.field public static final enum AUDIOPROXY_CLOSED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

.field public static final enum AUDIOPROXY_OPENED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

.field public static final enum AUDIO_CONFIGURE_FAILURE:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

.field public static final enum AUDIO_RUNTIME_ERROR:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

.field public static final enum HDCP_CONNECT_FAIL:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

.field public static final enum HDCP_CONNECT_SUCCESS:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

.field public static final enum HDCP_ENFORCE_FAIL:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

.field public static final enum HDCP_RUNTIME_ERROR:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

.field public static final enum NETWORK_CONFIGURE_FAILURE:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

.field public static final enum NETWORK_RUNTIME_ERROR:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

.field public static final enum PAUSE_START:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

.field public static final enum PLAY_START:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

.field public static final enum RTP_TRANSPORT_NEGOTIATED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

.field public static final enum STANDBY_START:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

.field public static final enum START_SESSION_FAIL:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

.field public static final enum TEARDOWN_START:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

.field public static final enum UIBC_DISABLED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

.field public static final enum UIBC_ENABLED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

.field public static final enum UIBC_NOT_SUPPORTED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

.field public static final enum UIBC_SUPPORTED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

.field public static final enum VIDEO_CONFIGURE_FAILURE:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

.field public static final enum VIDEO_RUNTIME_ERROR:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

.field public static final enum WFD_SERVICE_DISABLED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

.field public static final enum WFD_SERVICE_ENABLED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 479
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@7
    const-string v1, "WFD_SERVICE_ENABLED"

    #@9
    invoke-direct {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;-><init>(Ljava/lang/String;I)V

    #@c
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->WFD_SERVICE_ENABLED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@e
    .line 480
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@10
    const-string v1, "WFD_SERVICE_DISABLED"

    #@12
    invoke-direct {v0, v1, v4}, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;-><init>(Ljava/lang/String;I)V

    #@15
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->WFD_SERVICE_DISABLED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@17
    .line 481
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@19
    const-string v1, "UIBC_ENABLED"

    #@1b
    invoke-direct {v0, v1, v5}, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;-><init>(Ljava/lang/String;I)V

    #@1e
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->UIBC_ENABLED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@20
    .line 482
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@22
    const-string v1, "UIBC_DISABLED"

    #@24
    invoke-direct {v0, v1, v6}, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;-><init>(Ljava/lang/String;I)V

    #@27
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->UIBC_DISABLED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@29
    .line 483
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@2b
    const-string v1, "PLAY_START"

    #@2d
    invoke-direct {v0, v1, v7}, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;-><init>(Ljava/lang/String;I)V

    #@30
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->PLAY_START:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@32
    .line 484
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@34
    const-string v1, "PAUSE_START"

    #@36
    const/4 v2, 0x5

    #@37
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;-><init>(Ljava/lang/String;I)V

    #@3a
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->PAUSE_START:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@3c
    .line 485
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@3e
    const-string v1, "AUDIOPROXY_CLOSED"

    #@40
    const/4 v2, 0x6

    #@41
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;-><init>(Ljava/lang/String;I)V

    #@44
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->AUDIOPROXY_CLOSED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@46
    .line 486
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@48
    const-string v1, "AUDIOPROXY_OPENED"

    #@4a
    const/4 v2, 0x7

    #@4b
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;-><init>(Ljava/lang/String;I)V

    #@4e
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->AUDIOPROXY_OPENED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@50
    .line 487
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@52
    const-string v1, "TEARDOWN_START"

    #@54
    const/16 v2, 0x8

    #@56
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;-><init>(Ljava/lang/String;I)V

    #@59
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->TEARDOWN_START:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@5b
    .line 488
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@5d
    const-string v1, "HDCP_CONNECT_SUCCESS"

    #@5f
    const/16 v2, 0x9

    #@61
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;-><init>(Ljava/lang/String;I)V

    #@64
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->HDCP_CONNECT_SUCCESS:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@66
    .line 489
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@68
    const-string v1, "HDCP_CONNECT_FAIL"

    #@6a
    const/16 v2, 0xa

    #@6c
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;-><init>(Ljava/lang/String;I)V

    #@6f
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->HDCP_CONNECT_FAIL:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@71
    .line 490
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@73
    const-string v1, "HDCP_ENFORCE_FAIL"

    #@75
    const/16 v2, 0xb

    #@77
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;-><init>(Ljava/lang/String;I)V

    #@7a
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->HDCP_ENFORCE_FAIL:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@7c
    .line 491
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@7e
    const-string v1, "VIDEO_RUNTIME_ERROR"

    #@80
    const/16 v2, 0xc

    #@82
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;-><init>(Ljava/lang/String;I)V

    #@85
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->VIDEO_RUNTIME_ERROR:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@87
    .line 492
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@89
    const-string v1, "VIDEO_CONFIGURE_FAILURE"

    #@8b
    const/16 v2, 0xd

    #@8d
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;-><init>(Ljava/lang/String;I)V

    #@90
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->VIDEO_CONFIGURE_FAILURE:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@92
    .line 493
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@94
    const-string v1, "AUDIO_RUNTIME_ERROR"

    #@96
    const/16 v2, 0xe

    #@98
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;-><init>(Ljava/lang/String;I)V

    #@9b
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->AUDIO_RUNTIME_ERROR:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@9d
    .line 494
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@9f
    const-string v1, "HDCP_RUNTIME_ERROR"

    #@a1
    const/16 v2, 0xf

    #@a3
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;-><init>(Ljava/lang/String;I)V

    #@a6
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->HDCP_RUNTIME_ERROR:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@a8
    .line 495
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@aa
    const-string v1, "AUDIO_CONFIGURE_FAILURE"

    #@ac
    const/16 v2, 0x10

    #@ae
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;-><init>(Ljava/lang/String;I)V

    #@b1
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->AUDIO_CONFIGURE_FAILURE:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@b3
    .line 496
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@b5
    const-string v1, "NETWORK_RUNTIME_ERROR"

    #@b7
    const/16 v2, 0x11

    #@b9
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;-><init>(Ljava/lang/String;I)V

    #@bc
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->NETWORK_RUNTIME_ERROR:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@be
    .line 497
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@c0
    const-string v1, "NETWORK_CONFIGURE_FAILURE"

    #@c2
    const/16 v2, 0x12

    #@c4
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;-><init>(Ljava/lang/String;I)V

    #@c7
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->NETWORK_CONFIGURE_FAILURE:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@c9
    .line 498
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@cb
    const-string v1, "RTP_TRANSPORT_NEGOTIATED"

    #@cd
    const/16 v2, 0x13

    #@cf
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;-><init>(Ljava/lang/String;I)V

    #@d2
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->RTP_TRANSPORT_NEGOTIATED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@d4
    .line 499
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@d6
    const-string v1, "STANDBY_START"

    #@d8
    const/16 v2, 0x14

    #@da
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;-><init>(Ljava/lang/String;I)V

    #@dd
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->STANDBY_START:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@df
    .line 500
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@e1
    const-string v1, "START_SESSION_FAIL"

    #@e3
    const/16 v2, 0x15

    #@e5
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;-><init>(Ljava/lang/String;I)V

    #@e8
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->START_SESSION_FAIL:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@ea
    .line 502
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@ec
    const-string v1, "UIBC_SUPPORTED"

    #@ee
    const/16 v2, 0x16

    #@f0
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;-><init>(Ljava/lang/String;I)V

    #@f3
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->UIBC_SUPPORTED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@f5
    .line 503
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@f7
    const-string v1, "UIBC_NOT_SUPPORTED"

    #@f9
    const/16 v2, 0x17

    #@fb
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;-><init>(Ljava/lang/String;I)V

    #@fe
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->UIBC_NOT_SUPPORTED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@100
    .line 478
    const/16 v0, 0x18

    #@102
    new-array v0, v0, [Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@104
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->WFD_SERVICE_ENABLED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@106
    aput-object v1, v0, v3

    #@108
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->WFD_SERVICE_DISABLED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@10a
    aput-object v1, v0, v4

    #@10c
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->UIBC_ENABLED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@10e
    aput-object v1, v0, v5

    #@110
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->UIBC_DISABLED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@112
    aput-object v1, v0, v6

    #@114
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->PLAY_START:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@116
    aput-object v1, v0, v7

    #@118
    const/4 v1, 0x5

    #@119
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->PAUSE_START:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@11b
    aput-object v2, v0, v1

    #@11d
    const/4 v1, 0x6

    #@11e
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->AUDIOPROXY_CLOSED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@120
    aput-object v2, v0, v1

    #@122
    const/4 v1, 0x7

    #@123
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->AUDIOPROXY_OPENED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@125
    aput-object v2, v0, v1

    #@127
    const/16 v1, 0x8

    #@129
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->TEARDOWN_START:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@12b
    aput-object v2, v0, v1

    #@12d
    const/16 v1, 0x9

    #@12f
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->HDCP_CONNECT_SUCCESS:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@131
    aput-object v2, v0, v1

    #@133
    const/16 v1, 0xa

    #@135
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->HDCP_CONNECT_FAIL:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@137
    aput-object v2, v0, v1

    #@139
    const/16 v1, 0xb

    #@13b
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->HDCP_ENFORCE_FAIL:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@13d
    aput-object v2, v0, v1

    #@13f
    const/16 v1, 0xc

    #@141
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->VIDEO_RUNTIME_ERROR:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@143
    aput-object v2, v0, v1

    #@145
    const/16 v1, 0xd

    #@147
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->VIDEO_CONFIGURE_FAILURE:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@149
    aput-object v2, v0, v1

    #@14b
    const/16 v1, 0xe

    #@14d
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->AUDIO_RUNTIME_ERROR:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@14f
    aput-object v2, v0, v1

    #@151
    const/16 v1, 0xf

    #@153
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->HDCP_RUNTIME_ERROR:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@155
    aput-object v2, v0, v1

    #@157
    const/16 v1, 0x10

    #@159
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->AUDIO_CONFIGURE_FAILURE:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@15b
    aput-object v2, v0, v1

    #@15d
    const/16 v1, 0x11

    #@15f
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->NETWORK_RUNTIME_ERROR:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@161
    aput-object v2, v0, v1

    #@163
    const/16 v1, 0x12

    #@165
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->NETWORK_CONFIGURE_FAILURE:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@167
    aput-object v2, v0, v1

    #@169
    const/16 v1, 0x13

    #@16b
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->RTP_TRANSPORT_NEGOTIATED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@16d
    aput-object v2, v0, v1

    #@16f
    const/16 v1, 0x14

    #@171
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->STANDBY_START:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@173
    aput-object v2, v0, v1

    #@175
    const/16 v1, 0x15

    #@177
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->START_SESSION_FAIL:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@179
    aput-object v2, v0, v1

    #@17b
    const/16 v1, 0x16

    #@17d
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->UIBC_SUPPORTED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@17f
    aput-object v2, v0, v1

    #@181
    const/16 v1, 0x17

    #@183
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->UIBC_NOT_SUPPORTED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@185
    aput-object v2, v0, v1

    #@187
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@189
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 478
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/wfd/WfdEnums$WfdEvent;
    .registers 2
    .parameter

    #@0
    .prologue
    .line 478
    const-class v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/qualcomm/wfd/WfdEnums$WfdEvent;
    .registers 1

    #@0
    .prologue
    .line 478
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@2
    invoke-virtual {v0}, [Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@8
    return-object v0
.end method
