.class synthetic Lcom/qualcomm/wfd/WfdActionListenerImpl$1;
.super Ljava/lang/Object;
.source "ExtendedRemoteDisplay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/WfdActionListenerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$qualcomm$wfd$WfdEnums$SessionState:[I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 202
    invoke-static {}, Lcom/qualcomm/wfd/WfdEnums$SessionState;->values()[Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@3
    move-result-object v0

    #@4
    array-length v0, v0

    #@5
    new-array v0, v0, [I

    #@7
    sput-object v0, Lcom/qualcomm/wfd/WfdActionListenerImpl$1;->$SwitchMap$com$qualcomm$wfd$WfdEnums$SessionState:[I

    #@9
    :try_start_9
    sget-object v0, Lcom/qualcomm/wfd/WfdActionListenerImpl$1;->$SwitchMap$com$qualcomm$wfd$WfdEnums$SessionState:[I

    #@b
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$SessionState;->INITIALIZED:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@d
    invoke-virtual {v1}, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ordinal()I

    #@10
    move-result v1

    #@11
    const/4 v2, 0x1

    #@12
    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_14} :catch_71

    #@14
    :goto_14
    :try_start_14
    sget-object v0, Lcom/qualcomm/wfd/WfdActionListenerImpl$1;->$SwitchMap$com$qualcomm$wfd$WfdEnums$SessionState:[I

    #@16
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$SessionState;->INVALID:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@18
    invoke-virtual {v1}, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ordinal()I

    #@1b
    move-result v1

    #@1c
    const/4 v2, 0x2

    #@1d
    aput v2, v0, v1
    :try_end_1f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_1f} :catch_6f

    #@1f
    :goto_1f
    :try_start_1f
    sget-object v0, Lcom/qualcomm/wfd/WfdActionListenerImpl$1;->$SwitchMap$com$qualcomm$wfd$WfdEnums$SessionState:[I

    #@21
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$SessionState;->IDLE:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@23
    invoke-virtual {v1}, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ordinal()I

    #@26
    move-result v1

    #@27
    const/4 v2, 0x3

    #@28
    aput v2, v0, v1
    :try_end_2a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1f .. :try_end_2a} :catch_6d

    #@2a
    :goto_2a
    :try_start_2a
    sget-object v0, Lcom/qualcomm/wfd/WfdActionListenerImpl$1;->$SwitchMap$com$qualcomm$wfd$WfdEnums$SessionState:[I

    #@2c
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PLAY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@2e
    invoke-virtual {v1}, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ordinal()I

    #@31
    move-result v1

    #@32
    const/4 v2, 0x4

    #@33
    aput v2, v0, v1
    :try_end_35
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2a .. :try_end_35} :catch_6b

    #@35
    :goto_35
    :try_start_35
    sget-object v0, Lcom/qualcomm/wfd/WfdActionListenerImpl$1;->$SwitchMap$com$qualcomm$wfd$WfdEnums$SessionState:[I

    #@37
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PAUSE:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@39
    invoke-virtual {v1}, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ordinal()I

    #@3c
    move-result v1

    #@3d
    const/4 v2, 0x5

    #@3e
    aput v2, v0, v1
    :try_end_40
    .catch Ljava/lang/NoSuchFieldError; {:try_start_35 .. :try_end_40} :catch_69

    #@40
    :goto_40
    :try_start_40
    sget-object v0, Lcom/qualcomm/wfd/WfdActionListenerImpl$1;->$SwitchMap$com$qualcomm$wfd$WfdEnums$SessionState:[I

    #@42
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$SessionState;->STANDING_BY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@44
    invoke-virtual {v1}, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ordinal()I

    #@47
    move-result v1

    #@48
    const/4 v2, 0x6

    #@49
    aput v2, v0, v1
    :try_end_4b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_40 .. :try_end_4b} :catch_67

    #@4b
    :goto_4b
    :try_start_4b
    sget-object v0, Lcom/qualcomm/wfd/WfdActionListenerImpl$1;->$SwitchMap$com$qualcomm$wfd$WfdEnums$SessionState:[I

    #@4d
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ESTABLISHED:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@4f
    invoke-virtual {v1}, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ordinal()I

    #@52
    move-result v1

    #@53
    const/4 v2, 0x7

    #@54
    aput v2, v0, v1
    :try_end_56
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4b .. :try_end_56} :catch_65

    #@56
    :goto_56
    :try_start_56
    sget-object v0, Lcom/qualcomm/wfd/WfdActionListenerImpl$1;->$SwitchMap$com$qualcomm$wfd$WfdEnums$SessionState:[I

    #@58
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$SessionState;->TEARDOWN:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@5a
    invoke-virtual {v1}, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ordinal()I

    #@5d
    move-result v1

    #@5e
    const/16 v2, 0x8

    #@60
    aput v2, v0, v1
    :try_end_62
    .catch Ljava/lang/NoSuchFieldError; {:try_start_56 .. :try_end_62} :catch_63

    #@62
    :goto_62
    return-void

    #@63
    :catch_63
    move-exception v0

    #@64
    goto :goto_62

    #@65
    :catch_65
    move-exception v0

    #@66
    goto :goto_56

    #@67
    :catch_67
    move-exception v0

    #@68
    goto :goto_4b

    #@69
    :catch_69
    move-exception v0

    #@6a
    goto :goto_40

    #@6b
    :catch_6b
    move-exception v0

    #@6c
    goto :goto_35

    #@6d
    :catch_6d
    move-exception v0

    #@6e
    goto :goto_2a

    #@6f
    :catch_6f
    move-exception v0

    #@70
    goto :goto_1f

    #@71
    :catch_71
    move-exception v0

    #@72
    goto :goto_14
.end method
