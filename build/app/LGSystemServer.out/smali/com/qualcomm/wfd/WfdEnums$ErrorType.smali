.class public final enum Lcom/qualcomm/wfd/WfdEnums$ErrorType;
.super Ljava/lang/Enum;
.source "WfdEnums.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/WfdEnums;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ErrorType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/wfd/WfdEnums$ErrorType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/wfd/WfdEnums$ErrorType;

.field public static final enum ALREADY_INITIALIZED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

.field public static final enum HDMI_CABLE_CONNECTED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

.field public static final enum INCORRECT_STATE_FOR_OPERATION:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

.field public static final enum INVALID_ARG:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

.field public static final enum NOT_INITIALIZED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

.field public static final enum NOT_SINK_DEVICE:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

.field public static final enum OPERATION_TIMED_OUT:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

.field public static final enum SESSION_IN_PROGRESS:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

.field public static final enum UIBC_ALREADY_ENABLED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

.field public static final enum UIBC_NOT_ENABLED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

.field public static final enum UNKNOWN:Lcom/qualcomm/wfd/WfdEnums$ErrorType;


# instance fields
.field private final code:I


# direct methods
.method static constructor <clinit>()V
    .registers 9

    #@0
    .prologue
    const/4 v8, 0x4

    #@1
    const/4 v7, 0x3

    #@2
    const/4 v6, 0x2

    #@3
    const/4 v5, 0x1

    #@4
    const/4 v4, 0x0

    #@5
    .line 555
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@7
    const-string v1, "UNKNOWN"

    #@9
    const/4 v2, -0x1

    #@a
    invoke-direct {v0, v1, v4, v2}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;-><init>(Ljava/lang/String;II)V

    #@d
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->UNKNOWN:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@f
    .line 556
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@11
    const-string v1, "INVALID_ARG"

    #@13
    const/4 v2, -0x2

    #@14
    invoke-direct {v0, v1, v5, v2}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;-><init>(Ljava/lang/String;II)V

    #@17
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->INVALID_ARG:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@19
    .line 557
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@1b
    const-string v1, "HDMI_CABLE_CONNECTED"

    #@1d
    const/4 v2, -0x3

    #@1e
    invoke-direct {v0, v1, v6, v2}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;-><init>(Ljava/lang/String;II)V

    #@21
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->HDMI_CABLE_CONNECTED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@23
    .line 558
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@25
    const-string v1, "OPERATION_TIMED_OUT"

    #@27
    const/4 v2, -0x4

    #@28
    invoke-direct {v0, v1, v7, v2}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;-><init>(Ljava/lang/String;II)V

    #@2b
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->OPERATION_TIMED_OUT:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@2d
    .line 559
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@2f
    const-string v1, "ALREADY_INITIALIZED"

    #@31
    const/16 v2, -0xa

    #@33
    invoke-direct {v0, v1, v8, v2}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;-><init>(Ljava/lang/String;II)V

    #@36
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->ALREADY_INITIALIZED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@38
    .line 560
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@3a
    const-string v1, "NOT_INITIALIZED"

    #@3c
    const/4 v2, 0x5

    #@3d
    const/16 v3, -0xb

    #@3f
    invoke-direct {v0, v1, v2, v3}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;-><init>(Ljava/lang/String;II)V

    #@42
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->NOT_INITIALIZED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@44
    .line 561
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@46
    const-string v1, "SESSION_IN_PROGRESS"

    #@48
    const/4 v2, 0x6

    #@49
    const/16 v3, -0xc

    #@4b
    invoke-direct {v0, v1, v2, v3}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;-><init>(Ljava/lang/String;II)V

    #@4e
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->SESSION_IN_PROGRESS:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@50
    .line 562
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@52
    const-string v1, "INCORRECT_STATE_FOR_OPERATION"

    #@54
    const/4 v2, 0x7

    #@55
    const/16 v3, -0xd

    #@57
    invoke-direct {v0, v1, v2, v3}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;-><init>(Ljava/lang/String;II)V

    #@5a
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->INCORRECT_STATE_FOR_OPERATION:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@5c
    .line 563
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@5e
    const-string v1, "NOT_SINK_DEVICE"

    #@60
    const/16 v2, 0x8

    #@62
    const/16 v3, -0xe

    #@64
    invoke-direct {v0, v1, v2, v3}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;-><init>(Ljava/lang/String;II)V

    #@67
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->NOT_SINK_DEVICE:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@69
    .line 564
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@6b
    const-string v1, "UIBC_NOT_ENABLED"

    #@6d
    const/16 v2, 0x9

    #@6f
    const/16 v3, -0x14

    #@71
    invoke-direct {v0, v1, v2, v3}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;-><init>(Ljava/lang/String;II)V

    #@74
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->UIBC_NOT_ENABLED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@76
    .line 565
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@78
    const-string v1, "UIBC_ALREADY_ENABLED"

    #@7a
    const/16 v2, 0xa

    #@7c
    const/16 v3, -0x15

    #@7e
    invoke-direct {v0, v1, v2, v3}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;-><init>(Ljava/lang/String;II)V

    #@81
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->UIBC_ALREADY_ENABLED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@83
    .line 554
    const/16 v0, 0xb

    #@85
    new-array v0, v0, [Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@87
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->UNKNOWN:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@89
    aput-object v1, v0, v4

    #@8b
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->INVALID_ARG:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@8d
    aput-object v1, v0, v5

    #@8f
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->HDMI_CABLE_CONNECTED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@91
    aput-object v1, v0, v6

    #@93
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->OPERATION_TIMED_OUT:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@95
    aput-object v1, v0, v7

    #@97
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->ALREADY_INITIALIZED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@99
    aput-object v1, v0, v8

    #@9b
    const/4 v1, 0x5

    #@9c
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->NOT_INITIALIZED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@9e
    aput-object v2, v0, v1

    #@a0
    const/4 v1, 0x6

    #@a1
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->SESSION_IN_PROGRESS:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@a3
    aput-object v2, v0, v1

    #@a5
    const/4 v1, 0x7

    #@a6
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->INCORRECT_STATE_FOR_OPERATION:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@a8
    aput-object v2, v0, v1

    #@aa
    const/16 v1, 0x8

    #@ac
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->NOT_SINK_DEVICE:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@ae
    aput-object v2, v0, v1

    #@b0
    const/16 v1, 0x9

    #@b2
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->UIBC_NOT_ENABLED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@b4
    aput-object v2, v0, v1

    #@b6
    const/16 v1, 0xa

    #@b8
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->UIBC_ALREADY_ENABLED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@ba
    aput-object v2, v0, v1

    #@bc
    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@be
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter "c"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 569
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    .line 570
    iput p3, p0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->code:I

    #@5
    .line 571
    return-void
.end method

.method public static getValue(I)Lcom/qualcomm/wfd/WfdEnums$ErrorType;
    .registers 6
    .parameter "c"

    #@0
    .prologue
    .line 578
    invoke-static {}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->values()[Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@3
    move-result-object v0

    #@4
    .local v0, arr$:[Lcom/qualcomm/wfd/WfdEnums$ErrorType;
    array-length v3, v0

    #@5
    .local v3, len$:I
    const/4 v2, 0x0

    #@6
    .local v2, i$:I
    :goto_6
    if-ge v2, v3, :cond_12

    #@8
    aget-object v1, v0, v2

    #@a
    .line 579
    .local v1, e:Lcom/qualcomm/wfd/WfdEnums$ErrorType;
    iget v4, v1, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->code:I

    #@c
    if-ne v4, p0, :cond_f

    #@e
    .line 582
    .end local v1           #e:Lcom/qualcomm/wfd/WfdEnums$ErrorType;
    :goto_e
    return-object v1

    #@f
    .line 578
    .restart local v1       #e:Lcom/qualcomm/wfd/WfdEnums$ErrorType;
    :cond_f
    add-int/lit8 v2, v2, 0x1

    #@11
    goto :goto_6

    #@12
    .line 582
    .end local v1           #e:Lcom/qualcomm/wfd/WfdEnums$ErrorType;
    :cond_12
    const/4 v1, 0x0

    #@13
    goto :goto_e
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/wfd/WfdEnums$ErrorType;
    .registers 2
    .parameter

    #@0
    .prologue
    .line 554
    const-class v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/qualcomm/wfd/WfdEnums$ErrorType;
    .registers 1

    #@0
    .prologue
    .line 554
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@2
    invoke-virtual {v0}, [Lcom/qualcomm/wfd/WfdEnums$ErrorType;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@8
    return-object v0
.end method


# virtual methods
.method public getCode()I
    .registers 2

    #@0
    .prologue
    .line 574
    iget v0, p0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->code:I

    #@2
    return v0
.end method
