.class public Lcom/qualcomm/wfd/UIBCManager;
.super Ljava/lang/Object;
.source "UIBCManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/wfd/UIBCManager$1;,
        Lcom/qualcomm/wfd/UIBCManager$EventQueue;,
        Lcom/qualcomm/wfd/UIBCManager$EventDispatcher;,
        Lcom/qualcomm/wfd/UIBCManager$EventReader;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "UIBCManager"


# instance fields
.field private deviceType:I

.field private eventDispatcher:Lcom/qualcomm/wfd/UIBCManager$EventDispatcher;

.field private eventDispatcherThread:Ljava/lang/Thread;

.field private eventReader:Lcom/qualcomm/wfd/UIBCManager$EventReader;

.field private eventReaderThread:Ljava/lang/Thread;

.field private inputManager:Lcom/qualcomm/compat/VersionedInputManager;


# direct methods
.method public constructor <init>(I)V
    .registers 4
    .parameter "devType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InstantiationException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 34
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 35
    iput p1, p0, Lcom/qualcomm/wfd/UIBCManager;->deviceType:I

    #@5
    .line 36
    iget v0, p0, Lcom/qualcomm/wfd/UIBCManager;->deviceType:I

    #@7
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->SOURCE:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@9
    invoke-virtual {v1}, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->getCode()I

    #@c
    move-result v1

    #@d
    if-ne v0, v1, :cond_15

    #@f
    .line 37
    invoke-static {}, Lcom/qualcomm/compat/VersionedInputManager;->newInstance()Lcom/qualcomm/compat/VersionedInputManager;

    #@12
    move-result-object v0

    #@13
    iput-object v0, p0, Lcom/qualcomm/wfd/UIBCManager;->inputManager:Lcom/qualcomm/compat/VersionedInputManager;

    #@15
    .line 39
    :cond_15
    return-void
.end method

.method static synthetic access$100(Lcom/qualcomm/wfd/UIBCManager;)Lcom/qualcomm/compat/VersionedInputManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 26
    iget-object v0, p0, Lcom/qualcomm/wfd/UIBCManager;->inputManager:Lcom/qualcomm/compat/VersionedInputManager;

    #@2
    return-object v0
.end method


# virtual methods
.method public addUIBCEvent(Landroid/view/InputEvent;)V
    .registers 3
    .parameter "ev"

    #@0
    .prologue
    .line 119
    iget-object v0, p0, Lcom/qualcomm/wfd/UIBCManager;->eventReader:Lcom/qualcomm/wfd/UIBCManager$EventReader;

    #@2
    invoke-virtual {v0, p1}, Lcom/qualcomm/wfd/UIBCManager$EventReader;->addEvent(Landroid/view/InputEvent;)V

    #@5
    .line 120
    return-void
.end method

.method public disable(I)Z
    .registers 4
    .parameter "sessionId"

    #@0
    .prologue
    .line 79
    iget v0, p0, Lcom/qualcomm/wfd/UIBCManager;->deviceType:I

    #@2
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->SOURCE:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@4
    invoke-virtual {v1}, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->getCode()I

    #@7
    move-result v1

    #@8
    if-eq v0, v1, :cond_13

    #@a
    .line 80
    const-string v0, "UIBCManager"

    #@c
    const-string v1, "UIBC can only be disabled from WFD source."

    #@e
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 81
    const/4 v0, 0x0

    #@12
    .line 84
    :goto_12
    return v0

    #@13
    .line 83
    :cond_13
    invoke-static {p1}, Lcom/qualcomm/wfd/WFDNative;->disableUIBC(I)V

    #@16
    .line 84
    const/4 v0, 0x1

    #@17
    goto :goto_12
.end method

.method public enable(I)Z
    .registers 5
    .parameter "sessionId"

    #@0
    .prologue
    .line 62
    const-string v0, "UIBCManager"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "UIBCManager::enable "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 63
    iget v0, p0, Lcom/qualcomm/wfd/UIBCManager;->deviceType:I

    #@1a
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->SOURCE:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@1c
    invoke-virtual {v1}, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->getCode()I

    #@1f
    move-result v1

    #@20
    if-eq v0, v1, :cond_2b

    #@22
    .line 64
    const-string v0, "UIBCManager"

    #@24
    const-string v1, "UIBC can only be enabled from WFD source."

    #@26
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 65
    const/4 v0, 0x0

    #@2a
    .line 69
    :goto_2a
    return v0

    #@2b
    .line 67
    :cond_2b
    invoke-static {p1}, Lcom/qualcomm/wfd/WFDNative;->enableUIBC(I)V

    #@2e
    .line 69
    const/4 v0, 0x1

    #@2f
    goto :goto_2a
.end method

.method public init()Z
    .registers 3

    #@0
    .prologue
    .line 43
    const-string v0, "UIBCManager"

    #@2
    const-string v1, "UIBCManager::init "

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 44
    iget v0, p0, Lcom/qualcomm/wfd/UIBCManager;->deviceType:I

    #@9
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->SOURCE:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@b
    invoke-virtual {v1}, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->getCode()I

    #@e
    move-result v1

    #@f
    if-eq v0, v1, :cond_1a

    #@11
    .line 45
    const-string v0, "UIBCManager"

    #@13
    const-string v1, "UIBC can onlyl be enabled from WFD source."

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 46
    const/4 v0, 0x0

    #@19
    .line 51
    :goto_19
    return v0

    #@1a
    .line 49
    :cond_1a
    invoke-static {}, Lcom/qualcomm/wfd/WFDNative;->initUIBC()V

    #@1d
    .line 51
    const/4 v0, 0x1

    #@1e
    goto :goto_19
.end method

.method public start()Z
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 92
    iget v0, p0, Lcom/qualcomm/wfd/UIBCManager;->deviceType:I

    #@3
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->SOURCE:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@5
    invoke-virtual {v1}, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->getCode()I

    #@8
    move-result v1

    #@9
    if-ne v0, v1, :cond_2e

    #@b
    .line 94
    new-instance v0, Lcom/qualcomm/wfd/UIBCManager$EventDispatcher;

    #@d
    invoke-direct {v0, p0}, Lcom/qualcomm/wfd/UIBCManager$EventDispatcher;-><init>(Lcom/qualcomm/wfd/UIBCManager;)V

    #@10
    iput-object v0, p0, Lcom/qualcomm/wfd/UIBCManager;->eventDispatcher:Lcom/qualcomm/wfd/UIBCManager$EventDispatcher;

    #@12
    .line 95
    iget-object v0, p0, Lcom/qualcomm/wfd/UIBCManager;->eventDispatcher:Lcom/qualcomm/wfd/UIBCManager$EventDispatcher;

    #@14
    iput-boolean v2, v0, Lcom/qualcomm/wfd/UIBCManager$EventQueue;->eventQueueStatus:Z

    #@16
    .line 97
    new-instance v0, Ljava/lang/Thread;

    #@18
    iget-object v1, p0, Lcom/qualcomm/wfd/UIBCManager;->eventDispatcher:Lcom/qualcomm/wfd/UIBCManager$EventDispatcher;

    #@1a
    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    #@1d
    iput-object v0, p0, Lcom/qualcomm/wfd/UIBCManager;->eventDispatcherThread:Ljava/lang/Thread;

    #@1f
    .line 98
    iget-object v0, p0, Lcom/qualcomm/wfd/UIBCManager;->eventDispatcher:Lcom/qualcomm/wfd/UIBCManager$EventDispatcher;

    #@21
    iput-boolean v2, v0, Lcom/qualcomm/wfd/UIBCManager$EventDispatcher;->running:Z

    #@23
    .line 99
    iget-object v0, p0, Lcom/qualcomm/wfd/UIBCManager;->eventDispatcherThread:Ljava/lang/Thread;

    #@25
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    #@28
    .line 101
    iget-object v0, p0, Lcom/qualcomm/wfd/UIBCManager;->eventDispatcher:Lcom/qualcomm/wfd/UIBCManager$EventDispatcher;

    #@2a
    invoke-static {v0}, Lcom/qualcomm/wfd/WFDNative;->startUIBC(Ljava/lang/Object;)V

    #@2d
    .line 115
    :goto_2d
    return v2

    #@2e
    .line 103
    :cond_2e
    const-string v0, "UIBCManager"

    #@30
    const-string v1, "Create eventReader"

    #@32
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    .line 105
    new-instance v0, Lcom/qualcomm/wfd/UIBCManager$EventReader;

    #@37
    invoke-direct {v0, p0}, Lcom/qualcomm/wfd/UIBCManager$EventReader;-><init>(Lcom/qualcomm/wfd/UIBCManager;)V

    #@3a
    iput-object v0, p0, Lcom/qualcomm/wfd/UIBCManager;->eventReader:Lcom/qualcomm/wfd/UIBCManager$EventReader;

    #@3c
    .line 106
    iget-object v0, p0, Lcom/qualcomm/wfd/UIBCManager;->eventReader:Lcom/qualcomm/wfd/UIBCManager$EventReader;

    #@3e
    iput-boolean v2, v0, Lcom/qualcomm/wfd/UIBCManager$EventQueue;->eventQueueStatus:Z

    #@40
    .line 107
    const-string v0, "UIBCManager"

    #@42
    const-string v1, "calling WFDNative.startUIBC()"

    #@44
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    .line 109
    iget-object v0, p0, Lcom/qualcomm/wfd/UIBCManager;->eventReader:Lcom/qualcomm/wfd/UIBCManager$EventReader;

    #@49
    invoke-static {v0}, Lcom/qualcomm/wfd/WFDNative;->startUIBC(Ljava/lang/Object;)V

    #@4c
    .line 111
    new-instance v0, Ljava/lang/Thread;

    #@4e
    iget-object v1, p0, Lcom/qualcomm/wfd/UIBCManager;->eventReader:Lcom/qualcomm/wfd/UIBCManager$EventReader;

    #@50
    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    #@53
    iput-object v0, p0, Lcom/qualcomm/wfd/UIBCManager;->eventReaderThread:Ljava/lang/Thread;

    #@55
    .line 112
    iget-object v0, p0, Lcom/qualcomm/wfd/UIBCManager;->eventReader:Lcom/qualcomm/wfd/UIBCManager$EventReader;

    #@57
    iput-boolean v2, v0, Lcom/qualcomm/wfd/UIBCManager$EventReader;->running:Z

    #@59
    .line 113
    iget-object v0, p0, Lcom/qualcomm/wfd/UIBCManager;->eventReaderThread:Ljava/lang/Thread;

    #@5b
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    #@5e
    goto :goto_2d
.end method

.method public stop()Z
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 128
    iget v1, p0, Lcom/qualcomm/wfd/UIBCManager;->deviceType:I

    #@3
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->SOURCE:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@5
    invoke-virtual {v2}, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->getCode()I

    #@8
    move-result v2

    #@9
    if-ne v1, v2, :cond_26

    #@b
    .line 130
    invoke-static {}, Lcom/qualcomm/wfd/WFDNative;->stopUIBC()V

    #@e
    .line 131
    iget-object v1, p0, Lcom/qualcomm/wfd/UIBCManager;->eventDispatcher:Lcom/qualcomm/wfd/UIBCManager$EventDispatcher;

    #@10
    iput-boolean v3, v1, Lcom/qualcomm/wfd/UIBCManager$EventDispatcher;->running:Z

    #@12
    .line 132
    iget-object v1, p0, Lcom/qualcomm/wfd/UIBCManager;->eventDispatcher:Lcom/qualcomm/wfd/UIBCManager$EventDispatcher;

    #@14
    iput-boolean v3, v1, Lcom/qualcomm/wfd/UIBCManager$EventQueue;->eventQueueStatus:Z

    #@16
    .line 134
    :try_start_16
    iget-object v1, p0, Lcom/qualcomm/wfd/UIBCManager;->eventDispatcherThread:Ljava/lang/Thread;

    #@18
    invoke-virtual {v1}, Ljava/lang/Thread;->join()V
    :try_end_1b
    .catch Ljava/lang/InterruptedException; {:try_start_16 .. :try_end_1b} :catch_1d

    #@1b
    .line 149
    :goto_1b
    const/4 v1, 0x1

    #@1c
    return v1

    #@1d
    .line 135
    :catch_1d
    move-exception v0

    #@1e
    .line 136
    .local v0, e:Ljava/lang/InterruptedException;
    const-string v1, "UIBCManager"

    #@20
    const-string v2, "Error joining event dispatcher thread"

    #@22
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@25
    goto :goto_1b

    #@26
    .line 140
    .end local v0           #e:Ljava/lang/InterruptedException;
    :cond_26
    iget-object v1, p0, Lcom/qualcomm/wfd/UIBCManager;->eventReader:Lcom/qualcomm/wfd/UIBCManager$EventReader;

    #@28
    iput-boolean v3, v1, Lcom/qualcomm/wfd/UIBCManager$EventReader;->running:Z

    #@2a
    .line 141
    iget-object v1, p0, Lcom/qualcomm/wfd/UIBCManager;->eventReader:Lcom/qualcomm/wfd/UIBCManager$EventReader;

    #@2c
    iput-boolean v3, v1, Lcom/qualcomm/wfd/UIBCManager$EventQueue;->eventQueueStatus:Z

    #@2e
    .line 143
    :try_start_2e
    iget-object v1, p0, Lcom/qualcomm/wfd/UIBCManager;->eventReaderThread:Ljava/lang/Thread;

    #@30
    invoke-virtual {v1}, Ljava/lang/Thread;->join()V
    :try_end_33
    .catch Ljava/lang/InterruptedException; {:try_start_2e .. :try_end_33} :catch_37

    #@33
    .line 147
    :goto_33
    invoke-static {}, Lcom/qualcomm/wfd/WFDNative;->stopUIBC()V

    #@36
    goto :goto_1b

    #@37
    .line 144
    :catch_37
    move-exception v0

    #@38
    .line 145
    .restart local v0       #e:Ljava/lang/InterruptedException;
    const-string v1, "UIBCManager"

    #@3a
    const-string v2, "Error joining reader thread"

    #@3c
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3f
    goto :goto_33
.end method
