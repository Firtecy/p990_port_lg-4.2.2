.class public final enum Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;
.super Ljava/lang/Enum;
.source "SensorTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/sensors/sensortest/SensorTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DataType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;

.field public static final enum PRIMARY:Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;

.field public static final enum SECONDARY:Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 155
    new-instance v0, Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;

    #@4
    const-string v1, "PRIMARY"

    #@6
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;-><init>(Ljava/lang/String;I)V

    #@9
    sput-object v0, Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;->PRIMARY:Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;

    #@b
    new-instance v0, Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;

    #@d
    const-string v1, "SECONDARY"

    #@f
    invoke-direct {v0, v1, v3}, Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;-><init>(Ljava/lang/String;I)V

    #@12
    sput-object v0, Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;->SECONDARY:Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;

    #@14
    const/4 v0, 0x2

    #@15
    new-array v0, v0, [Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;

    #@17
    sget-object v1, Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;->PRIMARY:Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;

    #@19
    aput-object v1, v0, v2

    #@1b
    sget-object v1, Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;->SECONDARY:Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;

    #@1d
    aput-object v1, v0, v3

    #@1f
    sput-object v0, Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;->$VALUES:[Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;

    #@21
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 155
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;
    .registers 2
    .parameter

    #@0
    .prologue
    .line 155
    const-class v0, Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;
    .registers 1

    #@0
    .prologue
    .line 155
    sget-object v0, Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;->$VALUES:[Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;

    #@2
    invoke-virtual {v0}, [Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;

    #@8
    return-object v0
.end method
