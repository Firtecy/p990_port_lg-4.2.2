.class public final enum Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;
.super Ljava/lang/Enum;
.source "SensorTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/sensors/sensortest/SensorTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TestType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;

.field public static final enum CONNECTIVITY:Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;

.field public static final enum IRQ:Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;

.field public static final enum OEMTEST:Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;

.field public static final enum SELFTEST:Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;

.field public static final enum SELFTEST_HW:Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;

.field public static final enum SELFTEST_SW:Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 163
    new-instance v0, Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;

    #@7
    const-string v1, "SELFTEST"

    #@9
    invoke-direct {v0, v1, v3}, Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;-><init>(Ljava/lang/String;I)V

    #@c
    sput-object v0, Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;->SELFTEST:Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;

    #@e
    new-instance v0, Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;

    #@10
    const-string v1, "IRQ"

    #@12
    invoke-direct {v0, v1, v4}, Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;-><init>(Ljava/lang/String;I)V

    #@15
    sput-object v0, Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;->IRQ:Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;

    #@17
    new-instance v0, Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;

    #@19
    const-string v1, "CONNECTIVITY"

    #@1b
    invoke-direct {v0, v1, v5}, Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;-><init>(Ljava/lang/String;I)V

    #@1e
    sput-object v0, Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;->CONNECTIVITY:Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;

    #@20
    new-instance v0, Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;

    #@22
    const-string v1, "SELFTEST_HW"

    #@24
    invoke-direct {v0, v1, v6}, Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;-><init>(Ljava/lang/String;I)V

    #@27
    sput-object v0, Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;->SELFTEST_HW:Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;

    #@29
    new-instance v0, Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;

    #@2b
    const-string v1, "SELFTEST_SW"

    #@2d
    invoke-direct {v0, v1, v7}, Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;-><init>(Ljava/lang/String;I)V

    #@30
    sput-object v0, Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;->SELFTEST_SW:Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;

    #@32
    new-instance v0, Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;

    #@34
    const-string v1, "OEMTEST"

    #@36
    const/4 v2, 0x5

    #@37
    invoke-direct {v0, v1, v2}, Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;-><init>(Ljava/lang/String;I)V

    #@3a
    sput-object v0, Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;->OEMTEST:Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;

    #@3c
    const/4 v0, 0x6

    #@3d
    new-array v0, v0, [Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;

    #@3f
    sget-object v1, Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;->SELFTEST:Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;

    #@41
    aput-object v1, v0, v3

    #@43
    sget-object v1, Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;->IRQ:Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;

    #@45
    aput-object v1, v0, v4

    #@47
    sget-object v1, Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;->CONNECTIVITY:Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;

    #@49
    aput-object v1, v0, v5

    #@4b
    sget-object v1, Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;->SELFTEST_HW:Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;

    #@4d
    aput-object v1, v0, v6

    #@4f
    sget-object v1, Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;->SELFTEST_SW:Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;

    #@51
    aput-object v1, v0, v7

    #@53
    const/4 v1, 0x5

    #@54
    sget-object v2, Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;->OEMTEST:Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;

    #@56
    aput-object v2, v0, v1

    #@58
    sput-object v0, Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;->$VALUES:[Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;

    #@5a
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 163
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;
    .registers 2
    .parameter

    #@0
    .prologue
    .line 163
    const-class v0, Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;
    .registers 1

    #@0
    .prologue
    .line 163
    sget-object v0, Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;->$VALUES:[Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;

    #@2
    invoke-virtual {v0}, [Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;

    #@8
    return-object v0
.end method
