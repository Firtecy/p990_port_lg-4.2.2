.class public Lcom/qualcomm/sensors/sensortest/SensorID;
.super Ljava/lang/Object;
.source "SensorID.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;
    }
.end annotation


# instance fields
.field private sensorCount_:I

.field private sensorID_:I

.field private sensorType_:Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;


# direct methods
.method public constructor <init>(Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;I)V
    .registers 5
    .parameter "sensorType"
    .parameter "sensorCount"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 83
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 84
    const/16 v0, 0x9

    #@5
    if-gt p2, v0, :cond_9

    #@7
    if-gez p2, :cond_11

    #@9
    .line 85
    :cond_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@b
    const-string v1, "Range Exception: the value of sensor count must be between 0 and 9, inclusive"

    #@d
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@10
    throw v0

    #@11
    .line 86
    :cond_11
    if-nez p1, :cond_1b

    #@13
    .line 87
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@15
    const-string v1, "Range Exception: sensor type must not be null"

    #@17
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v0

    #@1b
    .line 89
    :cond_1b
    iput p2, p0, Lcom/qualcomm/sensors/sensortest/SensorID;->sensorCount_:I

    #@1d
    .line 90
    iput-object p1, p0, Lcom/qualcomm/sensors/sensortest/SensorID;->sensorType_:Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;

    #@1f
    .line 91
    invoke-virtual {p1}, Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;->typeValue()I

    #@22
    move-result v0

    #@23
    add-int/2addr v0, p2

    #@24
    iput v0, p0, Lcom/qualcomm/sensors/sensortest/SensorID;->sensorID_:I

    #@26
    .line 92
    return-void
.end method


# virtual methods
.method public getSensorID()I
    .registers 2

    #@0
    .prologue
    .line 98
    iget v0, p0, Lcom/qualcomm/sensors/sensortest/SensorID;->sensorID_:I

    #@2
    return v0
.end method
