.class public Lcom/qualcomm/sensors/sensortest/SensorTest;
.super Ljava/lang/Object;
.source "SensorTest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;,
        Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SensorTest"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 31
    const-string v0, "sensor_test"

    #@2
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@5
    .line 32
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 22
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method private static native getNativeRawDataMode()I
.end method

.method public static declared-synchronized getRawDataMode()Z
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 136
    const-class v2, Lcom/qualcomm/sensors/sensortest/SensorTest;

    #@3
    monitor-enter v2

    #@4
    :try_start_4
    invoke-static {}, Lcom/qualcomm/sensors/sensortest/SensorTest;->getNativeRawDataMode()I
    :try_end_7
    .catchall {:try_start_4 .. :try_end_7} :catchall_e

    #@7
    move-result v0

    #@8
    .line 139
    .local v0, enabled:I
    if-ne v0, v1, :cond_c

    #@a
    :goto_a
    monitor-exit v2

    #@b
    return v1

    #@c
    :cond_c
    const/4 v1, 0x0

    #@d
    goto :goto_a

    #@e
    .line 136
    .end local v0           #enabled:I
    :catchall_e
    move-exception v1

    #@f
    monitor-exit v2

    #@10
    throw v1
.end method

.method private static native runNativeSensorTest(III)I
.end method

.method private static native runNativeSensorTest(IIIZZ)I
.end method

.method public static runSensorTest(Lcom/qualcomm/sensors/sensortest/SensorID;Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;)I
    .registers 4
    .parameter "sensorID"
    .parameter "dataType"
    .parameter "testType"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 75
    invoke-static {p0, p1, p2, v0, v0}, Lcom/qualcomm/sensors/sensortest/SensorTest;->runSensorTest(Lcom/qualcomm/sensors/sensortest/SensorID;Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;ZZ)I

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public static declared-synchronized runSensorTest(Lcom/qualcomm/sensors/sensortest/SensorID;Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;ZZ)I
    .registers 10
    .parameter "sensorID"
    .parameter "dataType"
    .parameter "testType"
    .parameter "saveToRegistry"
    .parameter "applyCalNow"

    #@0
    .prologue
    .line 58
    const-class v2, Lcom/qualcomm/sensors/sensortest/SensorTest;

    #@2
    monitor-enter v2

    #@3
    if-nez p0, :cond_f

    #@5
    .line 59
    :try_start_5
    const-string v1, "SensorTest"

    #@7
    const-string v3, "SensorID must not be NULL"

    #@9
    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c
    .catchall {:try_start_5 .. :try_end_c} :catchall_20

    #@c
    .line 60
    const/4 v0, -0x2

    #@d
    .line 65
    :goto_d
    monitor-exit v2

    #@e
    return v0

    #@f
    .line 63
    :cond_f
    :try_start_f
    invoke-virtual {p0}, Lcom/qualcomm/sensors/sensortest/SensorID;->getSensorID()I

    #@12
    move-result v1

    #@13
    invoke-virtual {p1}, Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;->ordinal()I

    #@16
    move-result v3

    #@17
    invoke-virtual {p2}, Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;->ordinal()I

    #@1a
    move-result v4

    #@1b
    invoke-static {v1, v3, v4, p3, p4}, Lcom/qualcomm/sensors/sensortest/SensorTest;->runNativeSensorTest(IIIZZ)I
    :try_end_1e
    .catchall {:try_start_f .. :try_end_1e} :catchall_20

    #@1e
    move-result v0

    #@1f
    .line 65
    .local v0, nativeTestResult:I
    goto :goto_d

    #@20
    .line 58
    .end local v0           #nativeTestResult:I
    :catchall_20
    move-exception v1

    #@21
    monitor-exit v2

    #@22
    throw v1
.end method

.method private static native setNativeRawDataMode(Z)I
.end method

.method public static declared-synchronized setRawDataMode(Z)V
    .registers 6
    .parameter "enabled"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    #@0
    .prologue
    .line 114
    const-class v2, Lcom/qualcomm/sensors/sensortest/SensorTest;

    #@2
    monitor-enter v2

    #@3
    :try_start_3
    invoke-static {p0}, Lcom/qualcomm/sensors/sensortest/SensorTest;->setNativeRawDataMode(Z)I

    #@6
    move-result v0

    #@7
    .line 117
    .local v0, result:I
    if-eqz v0, :cond_25

    #@9
    .line 118
    new-instance v1, Ljava/lang/Exception;

    #@b
    new-instance v3, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v4, "Unknown error occurred within native code: "

    #@12
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v3

    #@1e
    invoke-direct {v1, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    #@21
    throw v1
    :try_end_22
    .catchall {:try_start_3 .. :try_end_22} :catchall_22

    #@22
    .line 114
    .end local v0           #result:I
    :catchall_22
    move-exception v1

    #@23
    monitor-exit v2

    #@24
    throw v1

    #@25
    .line 120
    .restart local v0       #result:I
    :cond_25
    monitor-exit v2

    #@26
    return-void
.end method
