.class public final enum Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;
.super Ljava/lang/Enum;
.source "SensorID.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/sensors/sensortest/SensorID;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SensorType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;

.field public static final enum ACCEL:Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;

.field public static final enum GYRO:Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;

.field public static final enum MAG:Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;

.field public static final enum PRESSURE:Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;

.field public static final enum PROX_LIGHT:Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;


# instance fields
.field private final typeValue:I


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 28
    new-instance v0, Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;

    #@7
    const-string v1, "ACCEL"

    #@9
    invoke-direct {v0, v1, v3, v3}, Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;-><init>(Ljava/lang/String;II)V

    #@c
    sput-object v0, Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;->ACCEL:Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;

    #@e
    .line 29
    new-instance v0, Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;

    #@10
    const-string v1, "GYRO"

    #@12
    const/16 v2, 0xa

    #@14
    invoke-direct {v0, v1, v4, v2}, Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;-><init>(Ljava/lang/String;II)V

    #@17
    sput-object v0, Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;->GYRO:Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;

    #@19
    .line 30
    new-instance v0, Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;

    #@1b
    const-string v1, "MAG"

    #@1d
    const/16 v2, 0x14

    #@1f
    invoke-direct {v0, v1, v5, v2}, Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;-><init>(Ljava/lang/String;II)V

    #@22
    sput-object v0, Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;->MAG:Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;

    #@24
    .line 31
    new-instance v0, Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;

    #@26
    const-string v1, "PRESSURE"

    #@28
    const/16 v2, 0x1e

    #@2a
    invoke-direct {v0, v1, v6, v2}, Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;-><init>(Ljava/lang/String;II)V

    #@2d
    sput-object v0, Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;->PRESSURE:Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;

    #@2f
    .line 32
    new-instance v0, Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;

    #@31
    const-string v1, "PROX_LIGHT"

    #@33
    const/16 v2, 0x28

    #@35
    invoke-direct {v0, v1, v7, v2}, Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;-><init>(Ljava/lang/String;II)V

    #@38
    sput-object v0, Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;->PROX_LIGHT:Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;

    #@3a
    .line 27
    const/4 v0, 0x5

    #@3b
    new-array v0, v0, [Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;

    #@3d
    sget-object v1, Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;->ACCEL:Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;

    #@3f
    aput-object v1, v0, v3

    #@41
    sget-object v1, Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;->GYRO:Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;

    #@43
    aput-object v1, v0, v4

    #@45
    sget-object v1, Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;->MAG:Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;

    #@47
    aput-object v1, v0, v5

    #@49
    sget-object v1, Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;->PRESSURE:Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;

    #@4b
    aput-object v1, v0, v6

    #@4d
    sget-object v1, Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;->PROX_LIGHT:Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;

    #@4f
    aput-object v1, v0, v7

    #@51
    sput-object v0, Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;->$VALUES:[Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;

    #@53
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter "typeValue"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    .line 37
    iput p3, p0, Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;->typeValue:I

    #@5
    .line 38
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;
    .registers 2
    .parameter

    #@0
    .prologue
    .line 27
    const-class v0, Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;
    .registers 1

    #@0
    .prologue
    .line 27
    sget-object v0, Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;->$VALUES:[Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;

    #@2
    invoke-virtual {v0}, [Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;

    #@8
    return-object v0
.end method


# virtual methods
.method public typeValue()I
    .registers 2

    #@0
    .prologue
    .line 40
    iget v0, p0, Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;->typeValue:I

    #@2
    return v0
.end method
