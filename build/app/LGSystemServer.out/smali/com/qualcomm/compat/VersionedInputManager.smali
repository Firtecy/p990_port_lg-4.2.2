.class public abstract Lcom/qualcomm/compat/VersionedInputManager;
.super Ljava/lang/Object;
.source "VersionedInputManager.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 16
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static newInstance()Lcom/qualcomm/compat/VersionedInputManager;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InstantiationException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 33
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    #@2
    const/16 v2, 0xf

    #@4
    if-gt v1, v2, :cond_1a

    #@6
    .line 34
    const-string v1, "VersionedInputManager"

    #@8
    const-string v2, "Instantiating ICS input manager"

    #@a
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 35
    const-string v1, "com.qualcomm.compat.IcsInputManager"

    #@f
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    #@12
    move-result-object v0

    #@13
    .line 42
    .local v0, vim:Ljava/lang/Class;,"Ljava/lang/Class<Lcom/qualcomm/compat/VersionedInputManager;>;"
    :goto_13
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    #@16
    move-result-object v1

    #@17
    check-cast v1, Lcom/qualcomm/compat/VersionedInputManager;

    #@19
    return-object v1

    #@1a
    .line 38
    .end local v0           #vim:Ljava/lang/Class;,"Ljava/lang/Class<Lcom/qualcomm/compat/VersionedInputManager;>;"
    :cond_1a
    const-string v1, "VersionedInputManager"

    #@1c
    const-string v2, "Instantiating JB input manager"

    #@1e
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 39
    const-string v1, "com.qualcomm.compat.JbInputManager"

    #@23
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    #@26
    move-result-object v0

    #@27
    .restart local v0       #vim:Ljava/lang/Class;,"Ljava/lang/Class<Lcom/qualcomm/compat/VersionedInputManager;>;"
    goto :goto_13
.end method


# virtual methods
.method public abstract injectKeyEvent(Landroid/view/KeyEvent;Z)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract injectPointerEvent(Landroid/view/MotionEvent;Z)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract injectTrackballEvent(Landroid/view/MotionEvent;Z)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
