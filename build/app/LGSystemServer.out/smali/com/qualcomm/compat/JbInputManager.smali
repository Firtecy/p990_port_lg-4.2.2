.class public Lcom/qualcomm/compat/JbInputManager;
.super Lcom/qualcomm/compat/VersionedInputManager;
.source "JbInputManager.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Lcom/qualcomm/compat/VersionedInputManager;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public injectKeyEvent(Landroid/view/KeyEvent;Z)Z
    .registers 5
    .parameter "ke"
    .parameter "sync"

    #@0
    .prologue
    .line 30
    invoke-static {}, Landroid/hardware/input/InputManager;->getInstance()Landroid/hardware/input/InputManager;

    #@3
    move-result-object v0

    #@4
    const/4 v1, 0x1

    #@5
    invoke-virtual {v0, p1, v1}, Landroid/hardware/input/InputManager;->injectInputEvent(Landroid/view/InputEvent;I)Z

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public injectPointerEvent(Landroid/view/MotionEvent;Z)Z
    .registers 5
    .parameter "me"
    .parameter "sync"

    #@0
    .prologue
    .line 18
    invoke-static {}, Landroid/hardware/input/InputManager;->getInstance()Landroid/hardware/input/InputManager;

    #@3
    move-result-object v0

    #@4
    const/4 v1, 0x1

    #@5
    invoke-virtual {v0, p1, v1}, Landroid/hardware/input/InputManager;->injectInputEvent(Landroid/view/InputEvent;I)Z

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public injectTrackballEvent(Landroid/view/MotionEvent;Z)Z
    .registers 5
    .parameter "me"
    .parameter "sync"

    #@0
    .prologue
    .line 24
    invoke-static {}, Landroid/hardware/input/InputManager;->getInstance()Landroid/hardware/input/InputManager;

    #@3
    move-result-object v0

    #@4
    const/4 v1, 0x1

    #@5
    invoke-virtual {v0, p1, v1}, Landroid/hardware/input/InputManager;->injectInputEvent(Landroid/view/InputEvent;I)Z

    #@8
    move-result v0

    #@9
    return v0
.end method
