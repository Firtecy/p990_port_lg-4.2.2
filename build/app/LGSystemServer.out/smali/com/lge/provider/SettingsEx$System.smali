.class public final Lcom/lge/provider/SettingsEx$System;
.super Landroid/provider/Settings$NameValueTable;
.source "SettingsEx.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/provider/SettingsEx;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "System"
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final DEFAULT_RINGTONE_VOIPCALL:Landroid/net/Uri;

.field public static final SETTINGS_EX_TO_BACKUP:[Ljava/lang/String;

.field public static final USER_PREFERRED_SUBS:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 165
    const-string v0, "content://settings/system"

    #@5
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@8
    move-result-object v0

    #@9
    sput-object v0, Lcom/lge/provider/SettingsEx$System;->CONTENT_URI:Landroid/net/Uri;

    #@b
    .line 339
    new-array v0, v4, [Ljava/lang/String;

    #@d
    const-string v1, "user_preferred_sub1"

    #@f
    aput-object v1, v0, v2

    #@11
    const-string v1, "user_preferred_sub2"

    #@13
    aput-object v1, v0, v3

    #@15
    sput-object v0, Lcom/lge/provider/SettingsEx$System;->USER_PREFERRED_SUBS:[Ljava/lang/String;

    #@17
    .line 1566
    const/16 v0, 0x1b

    #@19
    new-array v0, v0, [Ljava/lang/String;

    #@1b
    const-string v1, "text_link"

    #@1d
    aput-object v1, v0, v2

    #@1f
    const-string v1, "battery_condition_alarm"

    #@21
    aput-object v1, v0, v3

    #@23
    const-string v1, "battery_condition"

    #@25
    aput-object v1, v0, v4

    #@27
    const/4 v1, 0x3

    #@28
    const-string v2, "com.lge.settings.wifi.wifiOffloadingNotifyMe"

    #@2a
    aput-object v2, v0, v1

    #@2c
    const/4 v1, 0x4

    #@2d
    const-string v2, "wifi_networks_available_auto_connect"

    #@2f
    aput-object v2, v0, v1

    #@31
    const/4 v1, 0x5

    #@32
    const-string v2, "start_ip"

    #@34
    aput-object v2, v0, v1

    #@36
    const/4 v1, 0x6

    #@37
    const-string v2, "end_ip"

    #@39
    aput-object v2, v0, v1

    #@3b
    const/4 v1, 0x7

    #@3c
    const-string v2, "mask"

    #@3e
    aput-object v2, v0, v1

    #@40
    const/16 v1, 0x8

    #@42
    const-string v2, "prefix_length"

    #@44
    aput-object v2, v0, v1

    #@46
    const/16 v1, 0x9

    #@48
    const-string v2, "gateway"

    #@4a
    aput-object v2, v0, v1

    #@4c
    const/16 v1, 0xa

    #@4e
    const-string v2, "dns_server"

    #@50
    aput-object v2, v0, v1

    #@52
    const/16 v1, 0xb

    #@54
    const-string v2, "cdma_dns_server_1"

    #@56
    aput-object v2, v0, v1

    #@58
    const/16 v1, 0xc

    #@5a
    const-string v2, "cdma_dns_server_2"

    #@5c
    aput-object v2, v0, v1

    #@5e
    const/16 v1, 0xd

    #@60
    const-string v2, "dhcp_server"

    #@62
    aput-object v2, v0, v1

    #@64
    const/16 v1, 0xe

    #@66
    const-string v2, "wifi_auto_connect"

    #@68
    aput-object v2, v0, v1

    #@6a
    const/16 v1, 0xf

    #@6c
    const-string v2, "ssid_update_done"

    #@6e
    aput-object v2, v0, v1

    #@70
    const/16 v1, 0x10

    #@72
    const-string v2, "wifi_ssid_visibility"

    #@74
    aput-object v2, v0, v1

    #@76
    const/16 v1, 0x11

    #@78
    const-string v2, "wifi_ap_current_max_client"

    #@7a
    aput-object v2, v0, v1

    #@7c
    const/16 v1, 0x12

    #@7e
    const-string v2, "tether_entitlement_check_state"

    #@80
    aput-object v2, v0, v1

    #@82
    const/16 v1, 0x13

    #@84
    const-string v2, "tether_entitlement_check_type"

    #@86
    aput-object v2, v0, v1

    #@88
    const/16 v1, 0x14

    #@8a
    const-string v2, "tether_entitlement_check_interval"

    #@8c
    aput-object v2, v0, v1

    #@8e
    const/16 v1, 0x15

    #@90
    const-string v2, "wifi_networks_available_notification_settings"

    #@92
    aput-object v2, v0, v1

    #@94
    const/16 v1, 0x16

    #@96
    const-string v2, "otksl_count"

    #@98
    aput-object v2, v0, v1

    #@9a
    const/16 v1, 0x17

    #@9c
    const-string v2, "mhs_usage_time"

    #@9e
    aput-object v2, v0, v1

    #@a0
    const/16 v1, 0x18

    #@a2
    const-string v2, "wifi_rssi_based_op"

    #@a4
    aput-object v2, v0, v1

    #@a6
    const/16 v1, 0x19

    #@a8
    const-string v2, "xdivert_status"

    #@aa
    aput-object v2, v0, v1

    #@ac
    const/16 v1, 0x1a

    #@ae
    const-string v2, "roaming_auto_dial"

    #@b0
    aput-object v2, v0, v1

    #@b2
    sput-object v0, Lcom/lge/provider/SettingsEx$System;->SETTINGS_EX_TO_BACKUP:[Ljava/lang/String;

    #@b4
    .line 1664
    const-string v0, "ringtone_voipcall"

    #@b6
    invoke-static {v0}, Lcom/lge/provider/SettingsEx$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@b9
    move-result-object v0

    #@ba
    sput-object v0, Lcom/lge/provider/SettingsEx$System;->DEFAULT_RINGTONE_VOIPCALL:Landroid/net/Uri;

    #@bc
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 85
    invoke-direct {p0}, Landroid/provider/Settings$NameValueTable;-><init>()V

    #@3
    return-void
.end method

.method public static getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I
    .registers 4
    .parameter "cr"
    .parameter "name"
    .parameter "def"

    #@0
    .prologue
    .line 105
    invoke-static {p0, p1, p2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static getUriFor(Ljava/lang/String;)Landroid/net/Uri;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 101
    invoke-static {p0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public static putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    .registers 7
    .parameter "cr"
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 117
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@3
    move-result v0

    #@4
    .line 118
    .local v0, pid:I
    const-string v1, "LGSettingsProvider"

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "putInt() : name="

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    const-string v3, ", value="

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    const-string v3, ", pid="

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v2

    #@2d
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 119
    invoke-static {p0, p1, p2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@33
    move-result v1

    #@34
    return v1
.end method
