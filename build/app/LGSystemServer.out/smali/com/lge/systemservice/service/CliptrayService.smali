.class public Lcom/lge/systemservice/service/CliptrayService;
.super Landroid/app/Service;
.source "CliptrayService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;,
        Lcom/lge/systemservice/service/CliptrayService$mCueBtnView;,
        Lcom/lge/systemservice/service/CliptrayService$cliptrayEventReceiver;
    }
.end annotation


# instance fields
.field private Lookscreenflag:Z

.field bind:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService$Stub;

.field private clipManager:Landroid/content/ClipboardManager;

.field private clipboard:Landroid/view/View;

.field private clipboardHeight:I

.field private clipboardWidth:I

.field private ctWindow:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

.field private eventReceiver:Lcom/lge/systemservice/service/CliptrayService$cliptrayEventReceiver;

.field handler:Landroid/os/Handler;

.field isOpenCliptray:Z

.field private mClipCue:Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;

.field private mCliptrayConnected:Z

.field private mOnDeleteMode:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnDeleteMode;

.field private mOnNoContent:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnNoContent;

.field private mOnPasteInterface:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnPasteInterface;

.field private final pasteListener:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/lge/systemservice/core/cliptraymanager/IOnPasteListener;",
            ">;"
        }
    .end annotation
.end field

.field private preorientation:I

.field private prevLocale:Ljava/lang/String;

.field private wm:Landroid/view/WindowManager;

.field private wmParams:Landroid/view/WindowManager$LayoutParams;


# direct methods
.method public constructor <init>()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 48
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    #@5
    .line 71
    iput-object v2, p0, Lcom/lge/systemservice/service/CliptrayService;->clipboard:Landroid/view/View;

    #@7
    .line 72
    iput-object v2, p0, Lcom/lge/systemservice/service/CliptrayService;->ctWindow:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@9
    .line 73
    new-instance v0, Landroid/os/RemoteCallbackList;

    #@b
    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    #@e
    iput-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->pasteListener:Landroid/os/RemoteCallbackList;

    #@10
    .line 78
    iput-boolean v1, p0, Lcom/lge/systemservice/service/CliptrayService;->Lookscreenflag:Z

    #@12
    .line 79
    iput-boolean v1, p0, Lcom/lge/systemservice/service/CliptrayService;->isOpenCliptray:Z

    #@14
    .line 87
    iput-object v2, p0, Lcom/lge/systemservice/service/CliptrayService;->mClipCue:Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;

    #@16
    .line 88
    iput-boolean v1, p0, Lcom/lge/systemservice/service/CliptrayService;->mCliptrayConnected:Z

    #@18
    .line 123
    new-instance v0, Lcom/lge/systemservice/service/CliptrayService$1;

    #@1a
    invoke-direct {v0, p0}, Lcom/lge/systemservice/service/CliptrayService$1;-><init>(Lcom/lge/systemservice/service/CliptrayService;)V

    #@1d
    iput-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->bind:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService$Stub;

    #@1f
    .line 235
    new-instance v0, Lcom/lge/systemservice/service/CliptrayService$2;

    #@21
    invoke-direct {v0, p0}, Lcom/lge/systemservice/service/CliptrayService$2;-><init>(Lcom/lge/systemservice/service/CliptrayService;)V

    #@24
    iput-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->handler:Landroid/os/Handler;

    #@26
    .line 382
    new-instance v0, Lcom/lge/systemservice/service/CliptrayService$3;

    #@28
    invoke-direct {v0, p0}, Lcom/lge/systemservice/service/CliptrayService$3;-><init>(Lcom/lge/systemservice/service/CliptrayService;)V

    #@2b
    iput-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->mOnPasteInterface:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnPasteInterface;

    #@2d
    .line 405
    new-instance v0, Lcom/lge/systemservice/service/CliptrayService$4;

    #@2f
    invoke-direct {v0, p0}, Lcom/lge/systemservice/service/CliptrayService$4;-><init>(Lcom/lge/systemservice/service/CliptrayService;)V

    #@32
    iput-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->mOnDeleteMode:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnDeleteMode;

    #@34
    .line 413
    new-instance v0, Lcom/lge/systemservice/service/CliptrayService$5;

    #@36
    invoke-direct {v0, p0}, Lcom/lge/systemservice/service/CliptrayService$5;-><init>(Lcom/lge/systemservice/service/CliptrayService;)V

    #@39
    iput-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->mOnNoContent:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnNoContent;

    #@3b
    .line 582
    return-void
.end method

.method static synthetic access$000(Lcom/lge/systemservice/service/CliptrayService;)Landroid/os/RemoteCallbackList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->pasteListener:Landroid/os/RemoteCallbackList;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/lge/systemservice/service/CliptrayService;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->clipboard:Landroid/view/View;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Lcom/lge/systemservice/service/CliptrayService;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/lge/systemservice/service/CliptrayService;->changeViewMode(Z)V

    #@3
    return-void
.end method

.method static synthetic access$1100(Lcom/lge/systemservice/service/CliptrayService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 48
    invoke-direct {p0}, Lcom/lge/systemservice/service/CliptrayService;->showDecodeErr()V

    #@3
    return-void
.end method

.method static synthetic access$1200(Lcom/lge/systemservice/service/CliptrayService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 48
    invoke-direct {p0}, Lcom/lge/systemservice/service/CliptrayService;->showCliptrayCopied()V

    #@3
    return-void
.end method

.method static synthetic access$1400(Lcom/lge/systemservice/service/CliptrayService;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->prevLocale:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$1402(Lcom/lge/systemservice/service/CliptrayService;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 48
    iput-object p1, p0, Lcom/lge/systemservice/service/CliptrayService;->prevLocale:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$1500(Lcom/lge/systemservice/service/CliptrayService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    iget v0, p0, Lcom/lge/systemservice/service/CliptrayService;->preorientation:I

    #@2
    return v0
.end method

.method static synthetic access$1502(Lcom/lge/systemservice/service/CliptrayService;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 48
    iput p1, p0, Lcom/lge/systemservice/service/CliptrayService;->preorientation:I

    #@2
    return p1
.end method

.method static synthetic access$1800(Lcom/lge/systemservice/service/CliptrayService;)Landroid/view/WindowManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->wm:Landroid/view/WindowManager;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/lge/systemservice/service/CliptrayService;)Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->mClipCue:Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/lge/systemservice/service/CliptrayService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/lge/systemservice/service/CliptrayService;->mCliptrayConnected:Z

    #@2
    return v0
.end method

.method static synthetic access$302(Lcom/lge/systemservice/service/CliptrayService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 48
    iput-boolean p1, p0, Lcom/lge/systemservice/service/CliptrayService;->mCliptrayConnected:Z

    #@2
    return p1
.end method

.method static synthetic access$400(Lcom/lge/systemservice/service/CliptrayService;)Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->ctWindow:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Lcom/lge/systemservice/service/CliptrayService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 48
    invoke-direct {p0}, Lcom/lge/systemservice/service/CliptrayService;->showCliptray_internal()V

    #@3
    return-void
.end method

.method static synthetic access$600(Lcom/lge/systemservice/service/CliptrayService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 48
    invoke-direct {p0}, Lcom/lge/systemservice/service/CliptrayService;->hideCliptray()V

    #@3
    return-void
.end method

.method static synthetic access$700(Lcom/lge/systemservice/service/CliptrayService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 48
    invoke-direct {p0}, Lcom/lge/systemservice/service/CliptrayService;->addClipdata()V

    #@3
    return-void
.end method

.method static synthetic access$800(Lcom/lge/systemservice/service/CliptrayService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 48
    invoke-direct {p0}, Lcom/lge/systemservice/service/CliptrayService;->peekCliptray()V

    #@3
    return-void
.end method

.method static synthetic access$900(Lcom/lge/systemservice/service/CliptrayService;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/lge/systemservice/service/CliptrayService;->setInputType(I)V

    #@3
    return-void
.end method

.method private addClipdata()V
    .registers 2

    #@0
    .prologue
    .line 327
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->ctWindow:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@2
    invoke-virtual {v0}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->addNewClipData()V

    #@5
    .line 328
    return-void
.end method

.method private changeViewMode(Z)V
    .registers 5
    .parameter "portrait"

    #@0
    .prologue
    .line 358
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->ctWindow:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@2
    invoke-virtual {v0, p1}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->changeViewMode(Z)Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_9

    #@8
    .line 368
    :goto_8
    return-void

    #@9
    .line 361
    :cond_9
    invoke-direct {p0}, Lcom/lge/systemservice/service/CliptrayService;->initClipData()V

    #@c
    .line 362
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->clipboard:Landroid/view/View;

    #@e
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@11
    move-result-object v0

    #@12
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@14
    iput v0, p0, Lcom/lge/systemservice/service/CliptrayService;->clipboardWidth:I

    #@16
    .line 363
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->clipboard:Landroid/view/View;

    #@18
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@1b
    move-result-object v0

    #@1c
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@1e
    iput v0, p0, Lcom/lge/systemservice/service/CliptrayService;->clipboardHeight:I

    #@20
    .line 365
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->wmParams:Landroid/view/WindowManager$LayoutParams;

    #@22
    iget v1, p0, Lcom/lge/systemservice/service/CliptrayService;->clipboardWidth:I

    #@24
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@26
    .line 366
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->wmParams:Landroid/view/WindowManager$LayoutParams;

    #@28
    iget v1, p0, Lcom/lge/systemservice/service/CliptrayService;->clipboardHeight:I

    #@2a
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@2c
    .line 367
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->wm:Landroid/view/WindowManager;

    #@2e
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService;->clipboard:Landroid/view/View;

    #@30
    iget-object v2, p0, Lcom/lge/systemservice/service/CliptrayService;->wmParams:Landroid/view/WindowManager$LayoutParams;

    #@32
    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@35
    goto :goto_8
.end method

.method private hideCliptray()V
    .registers 3

    #@0
    .prologue
    .line 313
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->clipboard:Landroid/view/View;

    #@2
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_14

    #@8
    .line 314
    const-string v0, "CliptrayService"

    #@a
    const-string v1, "hideCliptray(), ctWindow.hideClipTray()"

    #@c
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 315
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->ctWindow:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@11
    invoke-virtual {v0}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->hideClipTray()V

    #@14
    .line 317
    :cond_14
    const/4 v0, 0x0

    #@15
    iput-boolean v0, p0, Lcom/lge/systemservice/service/CliptrayService;->Lookscreenflag:Z

    #@17
    .line 320
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->wmParams:Landroid/view/WindowManager$LayoutParams;

    #@19
    const v1, 0x40228

    #@1c
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@1e
    .line 324
    return-void
.end method

.method private initClipData()V
    .registers 12

    #@0
    .prologue
    .line 481
    const-string v9, "clipboard"

    #@2
    invoke-virtual {p0, v9}, Lcom/lge/systemservice/service/CliptrayService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@5
    move-result-object v9

    #@6
    check-cast v9, Landroid/content/ClipboardManager;

    #@8
    iput-object v9, p0, Lcom/lge/systemservice/service/CliptrayService;->clipManager:Landroid/content/ClipboardManager;

    #@a
    .line 482
    iget-object v9, p0, Lcom/lge/systemservice/service/CliptrayService;->clipManager:Landroid/content/ClipboardManager;

    #@c
    invoke-virtual {v9}, Landroid/content/ClipboardManager;->getPrimaryClipCount()I

    #@f
    move-result v1

    #@10
    .line 484
    .local v1, clipCnt:I
    new-instance v2, Ljava/util/ArrayList;

    #@12
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@15
    .line 486
    .local v2, failIndex:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v3, 0x0

    #@16
    .local v3, i:I
    :goto_16
    if-ge v3, v1, :cond_4a

    #@18
    .line 487
    const/4 v7, 0x0

    #@19
    .line 488
    .local v7, txt:Ljava/lang/String;
    const/4 v8, 0x0

    #@1a
    .line 490
    .local v8, uri:Landroid/net/Uri;
    iget-object v9, p0, Lcom/lge/systemservice/service/CliptrayService;->clipManager:Landroid/content/ClipboardManager;

    #@1c
    invoke-virtual {v9, v3}, Landroid/content/ClipboardManager;->getPrimaryClipAt(I)Landroid/content/ClipData;

    #@1f
    move-result-object v0

    #@20
    .line 491
    .local v0, clip:Landroid/content/ClipData;
    if-nez v0, :cond_31

    #@22
    .line 492
    const-string v9, "CliptrayService"

    #@24
    const-string v10, "Cliptray init failed, ClipData is null"

    #@26
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 493
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2c
    move-result-object v9

    #@2d
    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@30
    .line 511
    .end local v0           #clip:Landroid/content/ClipData;
    .end local v7           #txt:Ljava/lang/String;
    .end local v8           #uri:Landroid/net/Uri;
    :goto_30
    return-void

    #@31
    .line 496
    .restart local v0       #clip:Landroid/content/ClipData;
    .restart local v7       #txt:Ljava/lang/String;
    .restart local v8       #uri:Landroid/net/Uri;
    :cond_31
    iget-object v9, p0, Lcom/lge/systemservice/service/CliptrayService;->ctWindow:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@33
    invoke-virtual {v9, v0}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->add(Landroid/content/ClipData;)Z

    #@36
    move-result v6

    #@37
    .line 497
    .local v6, success:Z
    if-nez v6, :cond_47

    #@39
    .line 498
    const-string v9, "CliptrayService"

    #@3b
    const-string v10, "Cliptray init failed, ClipData is null"

    #@3d
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 499
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@43
    move-result-object v9

    #@44
    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@47
    .line 486
    :cond_47
    add-int/lit8 v3, v3, 0x1

    #@49
    goto :goto_16

    #@4a
    .line 505
    .end local v0           #clip:Landroid/content/ClipData;
    .end local v6           #success:Z
    .end local v7           #txt:Ljava/lang/String;
    .end local v8           #uri:Landroid/net/Uri;
    :cond_4a
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@4d
    move-result v9

    #@4e
    add-int/lit8 v4, v9, -0x1

    #@50
    .local v4, j:I
    :goto_50
    if-ltz v4, :cond_64

    #@52
    .line 506
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@55
    move-result-object v9

    #@56
    check-cast v9, Ljava/lang/Integer;

    #@58
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    #@5b
    move-result v5

    #@5c
    .line 507
    .local v5, removeIndex:I
    iget-object v9, p0, Lcom/lge/systemservice/service/CliptrayService;->clipManager:Landroid/content/ClipboardManager;

    #@5e
    invoke-virtual {v9, v5}, Landroid/content/ClipboardManager;->removePrimaryClipAt(I)Z

    #@61
    .line 505
    add-int/lit8 v4, v4, -0x1

    #@63
    goto :goto_50

    #@64
    .line 510
    .end local v5           #removeIndex:I
    :cond_64
    const/4 v2, 0x0

    #@65
    .line 511
    goto :goto_30
.end method

.method private initCliptrayWindow(Z)V
    .registers 11
    .parameter "portrait"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 422
    const-string v0, "window"

    #@3
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/service/CliptrayService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Landroid/view/WindowManager;

    #@9
    iput-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->wm:Landroid/view/WindowManager;

    #@b
    .line 423
    const-string v0, "clipboard"

    #@d
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/service/CliptrayService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@10
    move-result-object v0

    #@11
    check-cast v0, Landroid/content/ClipboardManager;

    #@13
    iput-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->clipManager:Landroid/content/ClipboardManager;

    #@15
    .line 425
    new-instance v8, Landroid/util/DisplayMetrics;

    #@17
    invoke-direct {v8}, Landroid/util/DisplayMetrics;-><init>()V

    #@1a
    .line 426
    .local v8, metrics:Landroid/util/DisplayMetrics;
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->wm:Landroid/view/WindowManager;

    #@1c
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    #@1f
    move-result-object v0

    #@20
    invoke-virtual {v0, v8}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    #@23
    .line 428
    new-instance v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@25
    invoke-direct {v0, p0}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;-><init>(Landroid/content/Context;)V

    #@28
    iput-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->ctWindow:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@2a
    .line 429
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->ctWindow:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@2c
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService;->mOnPasteInterface:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnPasteInterface;

    #@2e
    invoke-virtual {v0, v1}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->setOnPasteInterface(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnPasteInterface;)V

    #@31
    .line 430
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->ctWindow:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@33
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService;->mOnDeleteMode:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnDeleteMode;

    #@35
    invoke-virtual {v0, v1}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->setOnDeleteModeListener(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnDeleteMode;)V

    #@38
    .line 431
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->ctWindow:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@3a
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService;->mOnNoContent:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnNoContent;

    #@3c
    invoke-virtual {v0, v1}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->setOnNoContentListener(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnNoContent;)V

    #@3f
    .line 433
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->ctWindow:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@41
    iget v1, v8, Landroid/util/DisplayMetrics;->widthPixels:I

    #@43
    iget v2, v8, Landroid/util/DisplayMetrics;->heightPixels:I

    #@45
    invoke-virtual {v0, v1, v2}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->getClipTrayView(II)Landroid/view/View;

    #@48
    move-result-object v0

    #@49
    iput-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->clipboard:Landroid/view/View;

    #@4b
    .line 434
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->clipboard:Landroid/view/View;

    #@4d
    if-nez v0, :cond_57

    #@4f
    .line 435
    const-string v0, "CliptrayService"

    #@51
    const-string v1, "cliptray window is null"

    #@53
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    .line 475
    :goto_56
    return-void

    #@57
    .line 439
    :cond_57
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->clipboard:Landroid/view/View;

    #@59
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@5c
    move-result-object v0

    #@5d
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@5f
    iput v0, p0, Lcom/lge/systemservice/service/CliptrayService;->clipboardWidth:I

    #@61
    .line 440
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->clipboard:Landroid/view/View;

    #@63
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@66
    move-result-object v0

    #@67
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@69
    iput v0, p0, Lcom/lge/systemservice/service/CliptrayService;->clipboardHeight:I

    #@6b
    .line 442
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    #@6d
    iget v1, p0, Lcom/lge/systemservice/service/CliptrayService;->clipboardWidth:I

    #@6f
    iget v2, p0, Lcom/lge/systemservice/service/CliptrayService;->clipboardHeight:I

    #@71
    const/16 v5, 0x7d8

    #@73
    const v6, 0x40228

    #@76
    const/4 v7, 0x1

    #@77
    move v4, v3

    #@78
    invoke-direct/range {v0 .. v7}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIIIII)V

    #@7b
    iput-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->wmParams:Landroid/view/WindowManager$LayoutParams;

    #@7d
    .line 450
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->wmParams:Landroid/view/WindowManager$LayoutParams;

    #@7f
    const/16 v1, 0x53

    #@81
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@83
    .line 451
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->wmParams:Landroid/view/WindowManager$LayoutParams;

    #@85
    const-string v1, "ClipTray"

    #@87
    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    #@8a
    .line 452
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->wmParams:Landroid/view/WindowManager$LayoutParams;

    #@8c
    const v1, 0x20a01ef

    #@8f
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    #@91
    .line 453
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->wmParams:Landroid/view/WindowManager$LayoutParams;

    #@93
    const/16 v1, 0x30

    #@95
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@97
    .line 455
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->clipboard:Landroid/view/View;

    #@99
    const/16 v1, 0x8

    #@9b
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    #@9e
    .line 456
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->wm:Landroid/view/WindowManager;

    #@a0
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService;->clipboard:Landroid/view/View;

    #@a2
    iget-object v2, p0, Lcom/lge/systemservice/service/CliptrayService;->wmParams:Landroid/view/WindowManager$LayoutParams;

    #@a4
    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@a7
    .line 458
    invoke-direct {p0}, Lcom/lge/systemservice/service/CliptrayService;->initClipData()V

    #@aa
    .line 461
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->mClipCue:Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;

    #@ac
    if-nez v0, :cond_b5

    #@ae
    .line 462
    new-instance v0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;

    #@b0
    invoke-direct {v0, p0}, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;-><init>(Lcom/lge/systemservice/service/CliptrayService;)V

    #@b3
    iput-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->mClipCue:Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;

    #@b5
    .line 465
    :cond_b5
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->clipManager:Landroid/content/ClipboardManager;

    #@b7
    new-instance v1, Lcom/lge/systemservice/service/CliptrayService$6;

    #@b9
    invoke-direct {v1, p0}, Lcom/lge/systemservice/service/CliptrayService$6;-><init>(Lcom/lge/systemservice/service/CliptrayService;)V

    #@bc
    invoke-virtual {v0, v1}, Landroid/content/ClipboardManager;->addPrimaryClipChangedListener(Landroid/content/ClipboardManager$OnPrimaryClipChangedListener;)V

    #@bf
    goto :goto_56
.end method

.method private peekCliptray()V
    .registers 5

    #@0
    .prologue
    .line 331
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService;->clipboard:Landroid/view/View;

    #@2
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_47

    #@8
    .line 333
    const-string v1, "CliptrayService"

    #@a
    const-string v2, "peekCliptray, clipboard.getVisibility() != View.VISIBLE"

    #@c
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 334
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService;->wmParams:Landroid/view/WindowManager$LayoutParams;

    #@11
    iget v2, p0, Lcom/lge/systemservice/service/CliptrayService;->clipboardWidth:I

    #@13
    iget-object v3, p0, Lcom/lge/systemservice/service/CliptrayService;->ctWindow:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@15
    invoke-virtual {v3}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->getCurrentCellWidth()I

    #@18
    move-result v3

    #@19
    sub-int/2addr v2, v3

    #@1a
    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    #@1c
    .line 337
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService;->wmParams:Landroid/view/WindowManager$LayoutParams;

    #@1e
    const v2, 0x40238

    #@21
    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@23
    .line 343
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService;->wm:Landroid/view/WindowManager;

    #@25
    iget-object v2, p0, Lcom/lge/systemservice/service/CliptrayService;->clipboard:Landroid/view/View;

    #@27
    iget-object v3, p0, Lcom/lge/systemservice/service/CliptrayService;->wmParams:Landroid/view/WindowManager$LayoutParams;

    #@29
    invoke-interface {v1, v2, v3}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@2c
    .line 345
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService;->ctWindow:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@2e
    invoke-virtual {v1}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->onPreCopyToCliptray()V

    #@31
    .line 346
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService;->ctWindow:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@33
    invoke-virtual {v1}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->getInputType()I

    #@36
    move-result v0

    #@37
    .line 347
    .local v0, curType:I
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService;->ctWindow:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@39
    const/4 v2, 0x2

    #@3a
    invoke-virtual {v1, v2}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->setInputType(I)V

    #@3d
    .line 348
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService;->ctWindow:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@3f
    invoke-virtual {v1}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->showClipTray()V

    #@42
    .line 349
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService;->ctWindow:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@44
    invoke-virtual {v1, v0}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->setInputType(I)V

    #@47
    .line 351
    .end local v0           #curType:I
    :cond_47
    return-void
.end method

.method private setInputType(I)V
    .registers 3
    .parameter "type"

    #@0
    .prologue
    .line 354
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->ctWindow:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@2
    invoke-virtual {v0, p1}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->setInputType(I)V

    #@5
    .line 355
    return-void
.end method

.method private showCliptrayCopied()V
    .registers 3

    #@0
    .prologue
    .line 379
    const v0, 0x20902a4

    #@3
    const/4 v1, 0x0

    #@4
    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    #@b
    .line 380
    return-void
.end method

.method private showCliptray_internal()V
    .registers 7

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v0, 0x0

    #@2
    .line 296
    iget-object v3, p0, Lcom/lge/systemservice/service/CliptrayService;->clipboard:Landroid/view/View;

    #@4
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    #@7
    move-result v3

    #@8
    if-eqz v3, :cond_36

    #@a
    .line 298
    iget-object v3, p0, Lcom/lge/systemservice/service/CliptrayService;->wmParams:Landroid/view/WindowManager$LayoutParams;

    #@c
    iput v0, v3, Landroid/view/WindowManager$LayoutParams;->x:I

    #@e
    .line 299
    iget-object v3, p0, Lcom/lge/systemservice/service/CliptrayService;->wm:Landroid/view/WindowManager;

    #@10
    iget-object v4, p0, Lcom/lge/systemservice/service/CliptrayService;->clipboard:Landroid/view/View;

    #@12
    iget-object v5, p0, Lcom/lge/systemservice/service/CliptrayService;->wmParams:Landroid/view/WindowManager$LayoutParams;

    #@14
    invoke-interface {v3, v4, v5}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@17
    .line 301
    new-instance v1, Landroid/util/DisplayMetrics;

    #@19
    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    #@1c
    .line 302
    .local v1, metrics:Landroid/util/DisplayMetrics;
    iget-object v3, p0, Lcom/lge/systemservice/service/CliptrayService;->wm:Landroid/view/WindowManager;

    #@1e
    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v3, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    #@25
    .line 304
    iget v3, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    #@27
    iget v4, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    #@29
    if-ge v3, v4, :cond_2c

    #@2b
    move v0, v2

    #@2c
    .line 305
    .local v0, isPortrait:Z
    :cond_2c
    invoke-direct {p0, v0}, Lcom/lge/systemservice/service/CliptrayService;->changeViewMode(Z)V

    #@2f
    .line 307
    iput-boolean v2, p0, Lcom/lge/systemservice/service/CliptrayService;->Lookscreenflag:Z

    #@31
    .line 308
    iget-object v2, p0, Lcom/lge/systemservice/service/CliptrayService;->ctWindow:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@33
    invoke-virtual {v2}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->showClipTray()V

    #@36
    .line 310
    .end local v0           #isPortrait:Z
    .end local v1           #metrics:Landroid/util/DisplayMetrics;
    :cond_36
    return-void
.end method

.method private showDecodeErr()V
    .registers 3

    #@0
    .prologue
    .line 371
    const v0, 0x20902a1

    #@3
    const/4 v1, 0x0

    #@4
    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    #@b
    .line 372
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 3
    .parameter "intent"

    #@0
    .prologue
    .line 120
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->bind:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService$Stub;

    #@2
    return-object v0
.end method

.method public onCreate()V
    .registers 3

    #@0
    .prologue
    .line 101
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    #@3
    .line 103
    const/4 v1, 0x1

    #@4
    invoke-direct {p0, v1}, Lcom/lge/systemservice/service/CliptrayService;->initCliptrayWindow(Z)V

    #@7
    .line 105
    new-instance v1, Lcom/lge/systemservice/service/CliptrayService$cliptrayEventReceiver;

    #@9
    invoke-direct {v1, p0}, Lcom/lge/systemservice/service/CliptrayService$cliptrayEventReceiver;-><init>(Lcom/lge/systemservice/service/CliptrayService;)V

    #@c
    iput-object v1, p0, Lcom/lge/systemservice/service/CliptrayService;->eventReceiver:Lcom/lge/systemservice/service/CliptrayService$cliptrayEventReceiver;

    #@e
    .line 106
    new-instance v0, Landroid/content/IntentFilter;

    #@10
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@13
    .line 107
    .local v0, eventfilter:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.PHONE_STATE"

    #@15
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@18
    .line 108
    const-string v1, "com.lge.camera.action.LGE_CAMERA_STARTED"

    #@1a
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1d
    .line 109
    const-string v1, "android.intent.action.CONFIGURATION_CHANGED"

    #@1f
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@22
    .line 110
    const-string v1, "android.intent.action.SCREEN_OFF"

    #@24
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@27
    .line 111
    const-string v1, "android.intent.action.SCREEN_ON"

    #@29
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@2c
    .line 112
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService;->eventReceiver:Lcom/lge/systemservice/service/CliptrayService$cliptrayEventReceiver;

    #@2e
    invoke-virtual {p0, v1, v0}, Lcom/lge/systemservice/service/CliptrayService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@31
    .line 114
    invoke-virtual {p0}, Lcom/lge/systemservice/service/CliptrayService;->getResources()Landroid/content/res/Resources;

    #@34
    move-result-object v1

    #@35
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@38
    move-result-object v1

    #@39
    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@3b
    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    #@3e
    move-result-object v1

    #@3f
    iput-object v1, p0, Lcom/lge/systemservice/service/CliptrayService;->prevLocale:Ljava/lang/String;

    #@41
    .line 115
    invoke-virtual {p0}, Lcom/lge/systemservice/service/CliptrayService;->getResources()Landroid/content/res/Resources;

    #@44
    move-result-object v1

    #@45
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@48
    move-result-object v1

    #@49
    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    #@4b
    iput v1, p0, Lcom/lge/systemservice/service/CliptrayService;->preorientation:I

    #@4d
    .line 116
    return-void
.end method

.method public onDestroy()V
    .registers 3

    #@0
    .prologue
    .line 288
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->wm:Landroid/view/WindowManager;

    #@2
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService;->clipboard:Landroid/view/View;

    #@4
    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    #@7
    .line 290
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->pasteListener:Landroid/os/RemoteCallbackList;

    #@9
    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->kill()V

    #@c
    .line 291
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService;->eventReceiver:Lcom/lge/systemservice/service/CliptrayService$cliptrayEventReceiver;

    #@e
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/service/CliptrayService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@11
    .line 292
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    #@14
    .line 293
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .registers 5
    .parameter "intent"
    .parameter "flags"
    .parameter "startId"

    #@0
    .prologue
    .line 96
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .registers 3
    .parameter "intent"

    #@0
    .prologue
    .line 283
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method
