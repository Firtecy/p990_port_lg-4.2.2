.class public final enum Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;
.super Ljava/lang/Enum;
.source "WfdAdaptation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ControlOperation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

.field public static final enum PAUSE:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

.field public static final enum PAUSE_TRANSIT:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

.field public static final enum PLAY:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

.field public static final enum PLAY_TRANSIT:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

.field public static final enum RESUME:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

.field public static final enum STANDBY:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

.field public static final enum START_UIBC:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

.field public static final enum STOP_UIBC:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 265
    new-instance v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@7
    const-string v1, "PLAY"

    #@9
    invoke-direct {v0, v1, v3}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;-><init>(Ljava/lang/String;I)V

    #@c
    sput-object v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->PLAY:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@e
    new-instance v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@10
    const-string v1, "PAUSE"

    #@12
    invoke-direct {v0, v1, v4}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;-><init>(Ljava/lang/String;I)V

    #@15
    sput-object v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->PAUSE:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@17
    new-instance v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@19
    const-string v1, "STANDBY"

    #@1b
    invoke-direct {v0, v1, v5}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;-><init>(Ljava/lang/String;I)V

    #@1e
    sput-object v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->STANDBY:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@20
    new-instance v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@22
    const-string v1, "RESUME"

    #@24
    invoke-direct {v0, v1, v6}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;-><init>(Ljava/lang/String;I)V

    #@27
    sput-object v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->RESUME:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@29
    new-instance v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@2b
    const-string v1, "START_UIBC"

    #@2d
    invoke-direct {v0, v1, v7}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;-><init>(Ljava/lang/String;I)V

    #@30
    sput-object v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->START_UIBC:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@32
    new-instance v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@34
    const-string v1, "STOP_UIBC"

    #@36
    const/4 v2, 0x5

    #@37
    invoke-direct {v0, v1, v2}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;-><init>(Ljava/lang/String;I)V

    #@3a
    sput-object v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->STOP_UIBC:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@3c
    .line 267
    new-instance v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@3e
    const-string v1, "PLAY_TRANSIT"

    #@40
    const/4 v2, 0x6

    #@41
    invoke-direct {v0, v1, v2}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;-><init>(Ljava/lang/String;I)V

    #@44
    sput-object v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->PLAY_TRANSIT:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@46
    new-instance v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@48
    const-string v1, "PAUSE_TRANSIT"

    #@4a
    const/4 v2, 0x7

    #@4b
    invoke-direct {v0, v1, v2}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;-><init>(Ljava/lang/String;I)V

    #@4e
    sput-object v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->PAUSE_TRANSIT:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@50
    .line 264
    const/16 v0, 0x8

    #@52
    new-array v0, v0, [Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@54
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->PLAY:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@56
    aput-object v1, v0, v3

    #@58
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->PAUSE:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@5a
    aput-object v1, v0, v4

    #@5c
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->STANDBY:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@5e
    aput-object v1, v0, v5

    #@60
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->RESUME:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@62
    aput-object v1, v0, v6

    #@64
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->START_UIBC:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@66
    aput-object v1, v0, v7

    #@68
    const/4 v1, 0x5

    #@69
    sget-object v2, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->STOP_UIBC:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@6b
    aput-object v2, v0, v1

    #@6d
    const/4 v1, 0x6

    #@6e
    sget-object v2, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->PLAY_TRANSIT:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@70
    aput-object v2, v0, v1

    #@72
    const/4 v1, 0x7

    #@73
    sget-object v2, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->PAUSE_TRANSIT:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@75
    aput-object v2, v0, v1

    #@77
    sput-object v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->$VALUES:[Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@79
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 264
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;
    .registers 2
    .parameter

    #@0
    .prologue
    .line 264
    const-class v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;
    .registers 1

    #@0
    .prologue
    .line 264
    sget-object v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->$VALUES:[Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@2
    invoke-virtual {v0}, [Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@8
    return-object v0
.end method
