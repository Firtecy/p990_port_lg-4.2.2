.class public Lcom/lge/systemservice/service/InfoCollector;
.super Ljava/lang/Object;
.source "InfoCollector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/systemservice/service/InfoCollector$RequestHandler;
    }
.end annotation


# static fields
.field private static isLocationUpdated:Z

.field private static locationListener:Landroid/location/LocationListener;

.field private static mATClient:Lcom/lge/systemservice/service/InfoCollectorATClient;

.field private static mBaseSsid:Ljava/lang/String;

.field private static mConnectedSsid:Ljava/lang/String;

.field private static mContext:Landroid/content/Context;

.field private static mGPS:Ljava/lang/String;

.field private static mlocationManager:Landroid/location/LocationManager;

.field private static position:[Ljava/lang/String;

.field private static provider:Ljava/lang/String;

.field private static runningSendingAction:Z

.field private static strReceivedData:Ljava/lang/String;


# instance fields
.field private final HTTP_CONNECTION_TIMEOUT:I

.field private final HTTP_SO_TIMEOUT:I

.field private final MOBILE_DATA_APN_TYPE_DEFAULT:Ljava/lang/String;

.field private final SERVER_URI:Ljava/lang/String;

.field private final SendReceiveBufferSize:I

.field private isDemo:Z

.field mHttpClient:Lorg/apache/http/impl/client/DefaultHttpClient;

.field private mMobileDataState:Lcom/android/internal/telephony/PhoneConstants$DataState;

.field private final mPhoneInfoReceiver:Landroid/content/BroadcastReceiver;

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;

.field private final mWifiInfoReceiver:Landroid/content/BroadcastReceiver;

.field private mWifiState:Landroid/net/NetworkInfo$State;

.field private prefs:Landroid/content/SharedPreferences;

.field private final receiveWaitTime:I

.field private serverAddrConnecting:Ljava/net/InetAddress;

.field private socket:Ljava/net/Socket;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 105
    const-string v0, ""

    #@2
    sput-object v0, Lcom/lge/systemservice/service/InfoCollector;->strReceivedData:Ljava/lang/String;

    #@4
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/16 v1, 0x1f40

    #@3
    const/4 v2, 0x0

    #@4
    .line 134
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    .line 86
    const-string v0, "default"

    #@9
    iput-object v0, p0, Lcom/lge/systemservice/service/InfoCollector;->MOBILE_DATA_APN_TYPE_DEFAULT:Ljava/lang/String;

    #@b
    .line 94
    iput-object v3, p0, Lcom/lge/systemservice/service/InfoCollector;->socket:Ljava/net/Socket;

    #@d
    .line 95
    iput-object v3, p0, Lcom/lge/systemservice/service/InfoCollector;->serverAddrConnecting:Ljava/net/InetAddress;

    #@f
    .line 118
    iput v1, p0, Lcom/lge/systemservice/service/InfoCollector;->receiveWaitTime:I

    #@11
    .line 120
    const v0, 0xc800

    #@14
    iput v0, p0, Lcom/lge/systemservice/service/InfoCollector;->SendReceiveBufferSize:I

    #@16
    .line 123
    iput-boolean v2, p0, Lcom/lge/systemservice/service/InfoCollector;->isDemo:Z

    #@18
    .line 130
    const-string v0, "https://oil.lge.com:46060/oil/spms/cudRsmDefaultData.dev"

    #@1a
    iput-object v0, p0, Lcom/lge/systemservice/service/InfoCollector;->SERVER_URI:Ljava/lang/String;

    #@1c
    .line 131
    iput v1, p0, Lcom/lge/systemservice/service/InfoCollector;->HTTP_SO_TIMEOUT:I

    #@1e
    .line 132
    const/16 v0, 0x1388

    #@20
    iput v0, p0, Lcom/lge/systemservice/service/InfoCollector;->HTTP_CONNECTION_TIMEOUT:I

    #@22
    .line 168
    new-instance v0, Lcom/lge/systemservice/service/InfoCollector$1;

    #@24
    invoke-direct {v0, p0}, Lcom/lge/systemservice/service/InfoCollector$1;-><init>(Lcom/lge/systemservice/service/InfoCollector;)V

    #@27
    iput-object v0, p0, Lcom/lge/systemservice/service/InfoCollector;->mPhoneInfoReceiver:Landroid/content/BroadcastReceiver;

    #@29
    .line 241
    new-instance v0, Lcom/lge/systemservice/service/InfoCollector$2;

    #@2b
    invoke-direct {v0, p0}, Lcom/lge/systemservice/service/InfoCollector$2;-><init>(Lcom/lge/systemservice/service/InfoCollector;)V

    #@2e
    iput-object v0, p0, Lcom/lge/systemservice/service/InfoCollector;->mWifiInfoReceiver:Landroid/content/BroadcastReceiver;

    #@30
    .line 138
    sput-object p1, Lcom/lge/systemservice/service/InfoCollector;->mContext:Landroid/content/Context;

    #@32
    .line 139
    sget-object v0, Lcom/lge/systemservice/service/InfoCollector;->mContext:Landroid/content/Context;

    #@34
    const-string v1, "lghms_prefs"

    #@36
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@39
    move-result-object v0

    #@3a
    iput-object v0, p0, Lcom/lge/systemservice/service/InfoCollector;->prefs:Landroid/content/SharedPreferences;

    #@3c
    .line 140
    sget-object v0, Lcom/lge/systemservice/service/InfoCollector;->mContext:Landroid/content/Context;

    #@3e
    const-string v1, "phone"

    #@40
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@43
    move-result-object v0

    #@44
    check-cast v0, Landroid/telephony/TelephonyManager;

    #@46
    iput-object v0, p0, Lcom/lge/systemservice/service/InfoCollector;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@48
    .line 141
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$DataState;->DISCONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@4a
    iput-object v0, p0, Lcom/lge/systemservice/service/InfoCollector;->mMobileDataState:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@4c
    .line 142
    sget-object v0, Landroid/net/NetworkInfo$State;->UNKNOWN:Landroid/net/NetworkInfo$State;

    #@4e
    iput-object v0, p0, Lcom/lge/systemservice/service/InfoCollector;->mWifiState:Landroid/net/NetworkInfo$State;

    #@50
    .line 144
    sput-boolean v2, Lcom/lge/systemservice/service/InfoCollector;->runningSendingAction:Z

    #@52
    .line 147
    :try_start_52
    sget-object v0, Lcom/lge/systemservice/service/InfoCollector;->mContext:Landroid/content/Context;

    #@54
    invoke-static {v0}, Lcom/lge/systemservice/service/InfoCollectorATClient;->getInstance(Landroid/content/Context;)Lcom/lge/systemservice/service/InfoCollectorATClient;

    #@57
    move-result-object v0

    #@58
    sput-object v0, Lcom/lge/systemservice/service/InfoCollector;->mATClient:Lcom/lge/systemservice/service/InfoCollectorATClient;
    :try_end_5a
    .catch Ljava/lang/Exception; {:try_start_52 .. :try_end_5a} :catch_5b

    #@5a
    .line 156
    :goto_5a
    return-void

    #@5b
    .line 148
    :catch_5b
    move-exception v0

    #@5c
    goto :goto_5a
.end method

.method static synthetic access$000()Z
    .registers 1

    #@0
    .prologue
    .line 80
    sget-boolean v0, Lcom/lge/systemservice/service/InfoCollector;->runningSendingAction:Z

    #@2
    return v0
.end method

.method static synthetic access$100(Lcom/lge/systemservice/service/InfoCollector;)Lcom/android/internal/telephony/PhoneConstants$DataState;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 80
    iget-object v0, p0, Lcom/lge/systemservice/service/InfoCollector;->mMobileDataState:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@2
    return-object v0
.end method

.method static synthetic access$1000()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 80
    sget-object v0, Lcom/lge/systemservice/service/InfoCollector;->provider:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$1002(Ljava/lang/String;)Ljava/lang/String;
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 80
    sput-object p0, Lcom/lge/systemservice/service/InfoCollector;->provider:Ljava/lang/String;

    #@2
    return-object p0
.end method

.method static synthetic access$102(Lcom/lge/systemservice/service/InfoCollector;Lcom/android/internal/telephony/PhoneConstants$DataState;)Lcom/android/internal/telephony/PhoneConstants$DataState;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 80
    iput-object p1, p0, Lcom/lge/systemservice/service/InfoCollector;->mMobileDataState:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@2
    return-object p1
.end method

.method static synthetic access$1102(Ljava/lang/String;)Ljava/lang/String;
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 80
    sput-object p0, Lcom/lge/systemservice/service/InfoCollector;->strReceivedData:Ljava/lang/String;

    #@2
    return-object p0
.end method

.method static synthetic access$200(Lcom/lge/systemservice/service/InfoCollector;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 80
    invoke-direct {p0}, Lcom/lge/systemservice/service/InfoCollector;->isSendCondition()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$300(Lcom/lge/systemservice/service/InfoCollector;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 80
    invoke-direct {p0}, Lcom/lge/systemservice/service/InfoCollector;->getPosition()V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/lge/systemservice/service/InfoCollector;)Landroid/net/NetworkInfo$State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 80
    iget-object v0, p0, Lcom/lge/systemservice/service/InfoCollector;->mWifiState:Landroid/net/NetworkInfo$State;

    #@2
    return-object v0
.end method

.method static synthetic access$402(Lcom/lge/systemservice/service/InfoCollector;Landroid/net/NetworkInfo$State;)Landroid/net/NetworkInfo$State;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 80
    iput-object p1, p0, Lcom/lge/systemservice/service/InfoCollector;->mWifiState:Landroid/net/NetworkInfo$State;

    #@2
    return-object p1
.end method

.method static synthetic access$500()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 80
    sget-object v0, Lcom/lge/systemservice/service/InfoCollector;->mConnectedSsid:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$502(Ljava/lang/String;)Ljava/lang/String;
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 80
    sput-object p0, Lcom/lge/systemservice/service/InfoCollector;->mConnectedSsid:Ljava/lang/String;

    #@2
    return-object p0
.end method

.method static synthetic access$600()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 80
    sget-object v0, Lcom/lge/systemservice/service/InfoCollector;->mBaseSsid:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$602(Ljava/lang/String;)Ljava/lang/String;
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 80
    sput-object p0, Lcom/lge/systemservice/service/InfoCollector;->mBaseSsid:Ljava/lang/String;

    #@2
    return-object p0
.end method

.method static synthetic access$700()Z
    .registers 1

    #@0
    .prologue
    .line 80
    sget-boolean v0, Lcom/lge/systemservice/service/InfoCollector;->isLocationUpdated:Z

    #@2
    return v0
.end method

.method static synthetic access$702(Z)Z
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 80
    sput-boolean p0, Lcom/lge/systemservice/service/InfoCollector;->isLocationUpdated:Z

    #@2
    return p0
.end method

.method static synthetic access$800()Landroid/location/LocationManager;
    .registers 1

    #@0
    .prologue
    .line 80
    sget-object v0, Lcom/lge/systemservice/service/InfoCollector;->mlocationManager:Landroid/location/LocationManager;

    #@2
    return-object v0
.end method

.method static synthetic access$900()Landroid/location/LocationListener;
    .registers 1

    #@0
    .prologue
    .line 80
    sget-object v0, Lcom/lge/systemservice/service/InfoCollector;->locationListener:Landroid/location/LocationListener;

    #@2
    return-object v0
.end method

.method private buildInfoforHTTP()Ljava/util/ArrayList;
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 400
    new-instance v0, Ljava/util/ArrayList;

    #@2
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 401
    .local v0, nameValuePairs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/apache/http/NameValuePair;>;"
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    #@7
    const-string v2, "emmcid"

    #@9
    invoke-direct {p0}, Lcom/lge/systemservice/service/InfoCollector;->getEMMCID()Ljava/lang/String;

    #@c
    move-result-object v3

    #@d
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@10
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@13
    .line 402
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    #@15
    const-string v2, "smplSn"

    #@17
    invoke-direct {p0}, Lcom/lge/systemservice/service/InfoCollector;->getSerialNum()Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@1e
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@21
    .line 403
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    #@23
    const-string v2, "eventType"

    #@25
    const-string v3, "STATUS01"

    #@27
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@2a
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2d
    .line 404
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    #@2f
    const-string v2, "imsi"

    #@31
    invoke-direct {p0}, Lcom/lge/systemservice/service/InfoCollector;->getIMSI()Ljava/lang/String;

    #@34
    move-result-object v3

    #@35
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@38
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3b
    .line 405
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    #@3d
    const-string v2, "smplPhoneNo"

    #@3f
    invoke-direct {p0}, Lcom/lge/systemservice/service/InfoCollector;->getPhoneNumber()Ljava/lang/String;

    #@42
    move-result-object v3

    #@43
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@46
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@49
    .line 407
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    #@4b
    const-string v2, "model"

    #@4d
    invoke-direct {p0}, Lcom/lge/systemservice/service/InfoCollector;->getModelName()Ljava/lang/String;

    #@50
    move-result-object v3

    #@51
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@54
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@57
    .line 408
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    #@59
    const-string v2, "imei"

    #@5b
    invoke-direct {p0}, Lcom/lge/systemservice/service/InfoCollector;->getImei()Ljava/lang/String;

    #@5e
    move-result-object v3

    #@5f
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@62
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@65
    .line 409
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    #@67
    const-string v2, "hw"

    #@69
    invoke-direct {p0}, Lcom/lge/systemservice/service/InfoCollector;->getHWRev()Ljava/lang/String;

    #@6c
    move-result-object v3

    #@6d
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@70
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@73
    .line 410
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    #@75
    const-string v2, "sw"

    #@77
    invoke-direct {p0}, Lcom/lge/systemservice/service/InfoCollector;->getSWVer()Ljava/lang/String;

    #@7a
    move-result-object v3

    #@7b
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@7e
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@81
    .line 411
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    #@83
    const-string v2, "posX"

    #@85
    sget-object v3, Lcom/lge/systemservice/service/InfoCollector;->position:[Ljava/lang/String;

    #@87
    const/4 v4, 0x0

    #@88
    aget-object v3, v3, v4

    #@8a
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@8d
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@90
    .line 412
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    #@92
    const-string v2, "posY"

    #@94
    sget-object v3, Lcom/lge/systemservice/service/InfoCollector;->position:[Ljava/lang/String;

    #@96
    const/4 v4, 0x1

    #@97
    aget-object v3, v3, v4

    #@99
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@9c
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@9f
    .line 413
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    #@a1
    const-string v2, "posAddr"

    #@a3
    sget-object v3, Lcom/lge/systemservice/service/InfoCollector;->mGPS:Ljava/lang/String;

    #@a5
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@a8
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@ab
    .line 414
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    #@ad
    const-string v2, "mpPv"

    #@af
    sget-object v3, Lcom/lge/systemservice/service/InfoCollectorService;->mMPPV:Ljava/lang/String;

    #@b1
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@b4
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@b7
    .line 420
    const-string v1, "IC"

    #@b9
    invoke-virtual {v0}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    #@bc
    move-result-object v2

    #@bd
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@c0
    .line 424
    return-object v0
.end method

.method private getEMMCID()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 682
    const/4 v0, 0x0

    #@1
    .line 685
    .local v0, emmcId:Ljava/lang/String;
    :try_start_1
    const-string v1, "ro.boot.serialno"

    #@3
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;
    :try_end_6
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_6} :catch_8

    #@6
    move-result-object v0

    #@7
    .line 691
    :goto_7
    return-object v0

    #@8
    .line 686
    :catch_8
    move-exception v1

    #@9
    goto :goto_7
.end method

.method private getHWRev()Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    .line 615
    const/4 v2, 0x0

    #@1
    .line 616
    .local v2, revision:Ljava/lang/String;
    const/4 v1, 0x0

    #@2
    .line 618
    .local v1, response:Lcom/lge/systemservice/service/InfoCollectorATClient$Response;
    :try_start_2
    sget-object v3, Lcom/lge/systemservice/service/InfoCollector;->mATClient:Lcom/lge/systemservice/service/InfoCollectorATClient;

    #@4
    const/16 v4, 0xfac

    #@6
    const-string v5, ""

    #@8
    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    #@b
    move-result-object v5

    #@c
    invoke-virtual {v3, v4, v5}, Lcom/lge/systemservice/service/InfoCollectorATClient;->request(I[B)Lcom/lge/systemservice/service/InfoCollectorATClient$Response;

    #@f
    move-result-object v1

    #@10
    .line 619
    const-wide/16 v3, 0x64

    #@12
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_15} :catch_19

    #@15
    .line 623
    :goto_15
    if-nez v1, :cond_1e

    #@17
    .line 627
    const/4 v3, 0x0

    #@18
    .line 634
    :goto_18
    return-object v3

    #@19
    .line 620
    :catch_19
    move-exception v0

    #@1a
    .line 621
    .local v0, ie:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@1d
    goto :goto_15

    #@1e
    .line 629
    .end local v0           #ie:Ljava/lang/Exception;
    :cond_1e
    new-instance v2, Ljava/lang/String;

    #@20
    .end local v2           #revision:Ljava/lang/String;
    iget-object v3, v1, Lcom/lge/systemservice/service/InfoCollectorATClient$Response;->data:[B

    #@22
    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([B)V

    #@25
    .line 634
    .restart local v2       #revision:Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@28
    move-result-object v3

    #@29
    goto :goto_18
.end method

.method private getIMSI()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 671
    const/4 v0, 0x0

    #@1
    .line 672
    .local v0, imsi:Ljava/lang/String;
    iget-object v1, p0, Lcom/lge/systemservice/service/InfoCollector;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@3
    if-eqz v1, :cond_d

    #@5
    iget-object v1, p0, Lcom/lge/systemservice/service/InfoCollector;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@7
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->hasIccCard()Z

    #@a
    move-result v1

    #@b
    if-nez v1, :cond_f

    #@d
    .line 674
    :cond_d
    const/4 v0, 0x0

    #@e
    .line 678
    :goto_e
    return-object v0

    #@f
    .line 676
    :cond_f
    iget-object v1, p0, Lcom/lge/systemservice/service/InfoCollector;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@11
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    goto :goto_e
.end method

.method private getImei()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 695
    const/4 v0, 0x0

    #@1
    .line 698
    .local v0, imei:Ljava/lang/String;
    :try_start_1
    const-string v1, "gsm.baseband.imei"

    #@3
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;
    :try_end_6
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_6} :catch_1c

    #@6
    move-result-object v0

    #@7
    .line 705
    :goto_7
    if-eqz v0, :cond_f

    #@9
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_14

    #@f
    .line 706
    :cond_f
    iget-object v1, p0, Lcom/lge/systemservice/service/InfoCollector;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@11
    if-nez v1, :cond_15

    #@13
    .line 710
    const/4 v0, 0x0

    #@14
    .line 719
    :cond_14
    :goto_14
    return-object v0

    #@15
    .line 713
    :cond_15
    iget-object v1, p0, Lcom/lge/systemservice/service/InfoCollector;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@17
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    goto :goto_14

    #@1c
    .line 699
    :catch_1c
    move-exception v1

    #@1d
    goto :goto_7
.end method

.method private getModelName()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 638
    const/4 v0, 0x0

    #@1
    .line 641
    .local v0, modelName:Ljava/lang/String;
    :try_start_1
    const-string v1, "ro.product.model"

    #@3
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;
    :try_end_6
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_6} :catch_8

    #@6
    move-result-object v0

    #@7
    .line 647
    :goto_7
    return-object v0

    #@8
    .line 642
    :catch_8
    move-exception v1

    #@9
    goto :goto_7
.end method

.method private getPhoneNumber()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 651
    const/4 v0, 0x0

    #@1
    .line 652
    .local v0, phoneNumber:Ljava/lang/String;
    iget-object v1, p0, Lcom/lge/systemservice/service/InfoCollector;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@3
    if-eqz v1, :cond_d

    #@5
    iget-object v1, p0, Lcom/lge/systemservice/service/InfoCollector;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@7
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->hasIccCard()Z

    #@a
    move-result v1

    #@b
    if-nez v1, :cond_f

    #@d
    .line 657
    :cond_d
    const/4 v0, 0x0

    #@e
    .line 667
    :goto_e
    return-object v0

    #@f
    .line 659
    :cond_f
    iget-object v1, p0, Lcom/lge/systemservice/service/InfoCollector;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@11
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    goto :goto_e
.end method

.method private getPosition()V
    .registers 13

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v11, 0x1

    #@2
    .line 431
    sput-boolean v2, Lcom/lge/systemservice/service/InfoCollector;->isLocationUpdated:Z

    #@4
    .line 432
    const/4 v8, 0x0

    #@5
    .line 433
    .local v8, location:Landroid/location/Location;
    sget-object v0, Lcom/lge/systemservice/service/InfoCollector;->locationListener:Landroid/location/LocationListener;

    #@7
    if-eqz v0, :cond_13

    #@9
    .line 438
    sget-object v0, Lcom/lge/systemservice/service/InfoCollector;->mlocationManager:Landroid/location/LocationManager;

    #@b
    sget-object v1, Lcom/lge/systemservice/service/InfoCollector;->locationListener:Landroid/location/LocationListener;

    #@d
    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    #@10
    .line 439
    const/4 v0, 0x0

    #@11
    sput-object v0, Lcom/lge/systemservice/service/InfoCollector;->locationListener:Landroid/location/LocationListener;

    #@13
    .line 441
    :cond_13
    new-instance v0, Lcom/lge/systemservice/service/InfoCollector$3;

    #@15
    invoke-direct {v0, p0}, Lcom/lge/systemservice/service/InfoCollector$3;-><init>(Lcom/lge/systemservice/service/InfoCollector;)V

    #@18
    sput-object v0, Lcom/lge/systemservice/service/InfoCollector;->locationListener:Landroid/location/LocationListener;

    #@1a
    .line 489
    new-instance v6, Landroid/location/Criteria;

    #@1c
    invoke-direct {v6}, Landroid/location/Criteria;-><init>()V

    #@1f
    .line 490
    .local v6, criteria:Landroid/location/Criteria;
    const/4 v0, 0x2

    #@20
    invoke-virtual {v6, v0}, Landroid/location/Criteria;->setAccuracy(I)V

    #@23
    .line 491
    invoke-virtual {v6, v2}, Landroid/location/Criteria;->setAltitudeRequired(Z)V

    #@26
    .line 492
    invoke-virtual {v6, v2}, Landroid/location/Criteria;->setBearingRequired(Z)V

    #@29
    .line 493
    invoke-virtual {v6, v11}, Landroid/location/Criteria;->setCostAllowed(Z)V

    #@2c
    .line 494
    invoke-virtual {v6, v11}, Landroid/location/Criteria;->setPowerRequirement(I)V

    #@2f
    .line 496
    sget-object v0, Lcom/lge/systemservice/service/InfoCollector;->mlocationManager:Landroid/location/LocationManager;

    #@31
    invoke-virtual {v0, v6, v11}, Landroid/location/LocationManager;->getBestProvider(Landroid/location/Criteria;Z)Ljava/lang/String;

    #@34
    move-result-object v0

    #@35
    sput-object v0, Lcom/lge/systemservice/service/InfoCollector;->provider:Ljava/lang/String;

    #@37
    .line 497
    sget-object v0, Lcom/lge/systemservice/service/InfoCollector;->provider:Ljava/lang/String;

    #@39
    if-eqz v0, :cond_47

    #@3b
    .line 499
    :try_start_3b
    sget-object v0, Lcom/lge/systemservice/service/InfoCollector;->mlocationManager:Landroid/location/LocationManager;

    #@3d
    sget-object v1, Lcom/lge/systemservice/service/InfoCollector;->provider:Ljava/lang/String;

    #@3f
    const-wide/16 v2, 0x0

    #@41
    const/4 v4, 0x0

    #@42
    sget-object v5, Lcom/lge/systemservice/service/InfoCollector;->locationListener:Landroid/location/LocationListener;

    #@44
    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V
    :try_end_47
    .catch Ljava/lang/Exception; {:try_start_3b .. :try_end_47} :catch_57

    #@47
    .line 514
    :cond_47
    new-instance v10, Lcom/lge/systemservice/service/InfoCollector$4;

    #@49
    invoke-direct {v10, p0}, Lcom/lge/systemservice/service/InfoCollector$4;-><init>(Lcom/lge/systemservice/service/InfoCollector;)V

    #@4c
    .line 545
    .local v10, task:Ljava/util/TimerTask;
    new-instance v9, Ljava/util/Timer;

    #@4e
    invoke-direct {v9, v11}, Ljava/util/Timer;-><init>(Z)V

    #@51
    .line 546
    .local v9, t:Ljava/util/Timer;
    const-wide/16 v0, 0x2710

    #@53
    invoke-virtual {v9, v10, v0, v1}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    #@56
    .line 547
    .end local v9           #t:Ljava/util/Timer;
    .end local v10           #task:Ljava/util/TimerTask;
    :goto_56
    return-void

    #@57
    .line 505
    :catch_57
    move-exception v7

    #@58
    .line 509
    .local v7, e:Ljava/lang/Exception;
    invoke-virtual {p0}, Lcom/lge/systemservice/service/InfoCollector;->buildAndSendInfotoHTTPs()V

    #@5b
    goto :goto_56
.end method

.method private getSWVer()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 595
    const/4 v0, 0x0

    #@1
    .line 598
    .local v0, softwareVersion:Ljava/lang/String;
    :try_start_1
    const-string v1, "ro.lge.swversion"

    #@3
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;
    :try_end_6
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_6} :catch_8

    #@6
    move-result-object v0

    #@7
    .line 604
    :goto_7
    return-object v0

    #@8
    .line 599
    :catch_8
    move-exception v1

    #@9
    goto :goto_7
.end method

.method private getSerialNum()Ljava/lang/String;
    .registers 9

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 723
    const/4 v2, 0x0

    #@2
    .line 724
    .local v2, serialNumber:Ljava/lang/String;
    const/4 v1, 0x0

    #@3
    .line 727
    .local v1, response:Lcom/lge/systemservice/service/InfoCollectorATClient$Response;
    :try_start_3
    sget-object v5, Lcom/lge/systemservice/service/InfoCollector;->mATClient:Lcom/lge/systemservice/service/InfoCollectorATClient;

    #@5
    const/16 v6, 0xfaf

    #@7
    const-string v7, ""

    #@9
    invoke-virtual {v7}, Ljava/lang/String;->getBytes()[B

    #@c
    move-result-object v7

    #@d
    invoke-virtual {v5, v6, v7}, Lcom/lge/systemservice/service/InfoCollectorATClient;->request(I[B)Lcom/lge/systemservice/service/InfoCollectorATClient$Response;

    #@10
    move-result-object v1

    #@11
    .line 728
    const-wide/16 v5, 0x32

    #@13
    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_16
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_16} :catch_19

    #@16
    .line 733
    :goto_16
    if-nez v1, :cond_1e

    #@18
    .line 746
    :goto_18
    return-object v4

    #@19
    .line 729
    :catch_19
    move-exception v0

    #@1a
    .line 730
    .local v0, ie:Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    #@1d
    goto :goto_16

    #@1e
    .line 739
    .end local v0           #ie:Ljava/lang/InterruptedException;
    :cond_1e
    new-instance v3, Ljava/lang/String;

    #@20
    iget-object v5, v1, Lcom/lge/systemservice/service/InfoCollectorATClient$Response;->data:[B

    #@22
    invoke-direct {v3, v5}, Ljava/lang/String;-><init>([B)V

    #@25
    .line 740
    .local v3, tempSerialNumber:Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@28
    move-result v5

    #@29
    const/16 v6, 0xe

    #@2b
    if-le v5, v6, :cond_30

    #@2d
    move-object v2, v4

    #@2e
    :goto_2e
    move-object v4, v2

    #@2f
    .line 746
    goto :goto_18

    #@30
    :cond_30
    move-object v2, v3

    #@31
    .line 740
    goto :goto_2e
.end method

.method private isSendCondition()Z
    .registers 2

    #@0
    .prologue
    .line 394
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method private removeAdminToRWD()V
    .registers 5

    #@0
    .prologue
    .line 958
    sget-object v2, Lcom/lge/systemservice/service/InfoCollector;->mContext:Landroid/content/Context;

    #@2
    const-string v3, "device_policy"

    #@4
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    #@a
    .line 961
    .local v0, dpm:Landroid/app/admin/DevicePolicyManager;
    new-instance v1, Landroid/content/ComponentName;

    #@c
    const-string v2, "com.lge.lghms"

    #@e
    const-string v3, "com.lge.lghms.AdminReceiver"

    #@10
    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@13
    .line 965
    .local v1, rwdAdmin:Landroid/content/ComponentName;
    :try_start_13
    const-string v2, "com.lge.lghms"

    #@15
    invoke-virtual {v0, v2}, Landroid/app/admin/DevicePolicyManager;->packageHasActiveAdmins(Ljava/lang/String;)Z

    #@18
    move-result v2

    #@19
    if-eqz v2, :cond_1e

    #@1b
    .line 969
    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->removeActiveAdmin(Landroid/content/ComponentName;)V
    :try_end_1e
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_1e} :catch_1f

    #@1e
    .line 980
    :cond_1e
    :goto_1e
    return-void

    #@1f
    .line 975
    :catch_1f
    move-exception v2

    #@20
    goto :goto_1e
.end method

.method private sendInfotoHTTPs(Ljava/util/ArrayList;)V
    .registers 21
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 752
    .local p1, payload:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/apache/http/NameValuePair;>;"
    new-instance v11, Lorg/apache/http/conn/scheme/SchemeRegistry;

    #@2
    invoke-direct {v11}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    #@5
    .line 753
    .local v11, schemeRegistry:Lorg/apache/http/conn/scheme/SchemeRegistry;
    new-instance v15, Lorg/apache/http/conn/scheme/Scheme;

    #@7
    const-string v16, "http"

    #@9
    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    #@c
    move-result-object v17

    #@d
    const/16 v18, 0x50

    #@f
    invoke-direct/range {v15 .. v18}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    #@12
    invoke-virtual {v11, v15}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    #@15
    .line 755
    new-instance v15, Lorg/apache/http/conn/scheme/Scheme;

    #@17
    const-string v16, "https"

    #@19
    invoke-static {}, Lorg/apache/http/conn/ssl/SSLSocketFactory;->getSocketFactory()Lorg/apache/http/conn/ssl/SSLSocketFactory;

    #@1c
    move-result-object v17

    #@1d
    const/16 v18, 0x1bb

    #@1f
    invoke-direct/range {v15 .. v18}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    #@22
    invoke-virtual {v11, v15}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    #@25
    .line 758
    new-instance v5, Lorg/apache/http/params/BasicHttpParams;

    #@27
    invoke-direct {v5}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    #@2a
    .line 759
    .local v5, httpParams:Lorg/apache/http/params/HttpParams;
    const-string v15, "http.socket.timeout"

    #@2c
    const/16 v16, 0x1f40

    #@2e
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@31
    move-result-object v16

    #@32
    move-object/from16 v0, v16

    #@34
    invoke-interface {v5, v15, v0}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    #@37
    .line 761
    const-string v15, "http.connection.timeout"

    #@39
    const/16 v16, 0x1388

    #@3b
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3e
    move-result-object v16

    #@3f
    move-object/from16 v0, v16

    #@41
    invoke-interface {v5, v15, v0}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    #@44
    .line 764
    new-instance v8, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    #@46
    invoke-direct {v8, v5, v11}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    #@49
    .line 766
    .local v8, mCCM:Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;
    new-instance v15, Lorg/apache/http/impl/client/DefaultHttpClient;

    #@4b
    invoke-direct {v15, v8, v5}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    #@4e
    move-object/from16 v0, p0

    #@50
    iput-object v15, v0, Lcom/lge/systemservice/service/InfoCollector;->mHttpClient:Lorg/apache/http/impl/client/DefaultHttpClient;

    #@52
    .line 768
    const/4 v3, 0x0

    #@53
    .line 770
    .local v3, entityRequest:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    const/4 v15, 0x1

    #@54
    sput-boolean v15, Lcom/lge/systemservice/service/InfoCollector;->runningSendingAction:Z

    #@56
    .line 772
    const/4 v12, 0x0

    #@57
    .local v12, timer:I
    move-object v4, v3

    #@58
    .end local v3           #entityRequest:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .local v4, entityRequest:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    :goto_58
    const/4 v15, 0x3

    #@59
    if-ge v12, v15, :cond_138

    #@5b
    .line 774
    const/4 v9, 0x0

    #@5c
    .line 777
    .local v9, requestHandler:Lcom/lge/systemservice/service/InfoCollector$RequestHandler;
    :try_start_5c
    new-instance v14, Ljava/net/URI;

    #@5e
    const-string v15, "https://oil.lge.com:46060/oil/spms/cudRsmDefaultData.dev"

    #@60
    invoke-direct {v14, v15}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    #@63
    .line 778
    .local v14, url:Ljava/net/URI;
    new-instance v3, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    #@65
    const-string v15, "UTF-8"

    #@67
    move-object/from16 v0, p1

    #@69
    invoke-direct {v3, v0, v15}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;Ljava/lang/String;)V
    :try_end_6c
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_5c .. :try_end_6c} :catch_bb
    .catch Ljava/net/URISyntaxException; {:try_start_5c .. :try_end_6c} :catch_c1

    #@6c
    .line 780
    .end local v4           #entityRequest:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .restart local v3       #entityRequest:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    :try_start_6c
    new-instance v6, Lorg/apache/http/client/methods/HttpPost;

    #@6e
    invoke-direct {v6, v14}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/net/URI;)V

    #@71
    .line 782
    .local v6, httpPost:Lorg/apache/http/client/methods/HttpPost;
    invoke-virtual {v6, v3}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    #@74
    .line 788
    new-instance v10, Lcom/lge/systemservice/service/InfoCollector$RequestHandler;

    #@76
    move-object/from16 v0, p0

    #@78
    invoke-direct {v10, v0, v6}, Lcom/lge/systemservice/service/InfoCollector$RequestHandler;-><init>(Lcom/lge/systemservice/service/InfoCollector;Lorg/apache/http/client/methods/HttpPost;)V
    :try_end_7b
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_6c .. :try_end_7b} :catch_133
    .catch Ljava/net/URISyntaxException; {:try_start_6c .. :try_end_7b} :catch_12e

    #@7b
    .line 793
    .end local v9           #requestHandler:Lcom/lge/systemservice/service/InfoCollector$RequestHandler;
    .local v10, requestHandler:Lcom/lge/systemservice/service/InfoCollector$RequestHandler;
    :try_start_7b
    invoke-virtual {v10}, Lcom/lge/systemservice/service/InfoCollector$RequestHandler;->start()V
    :try_end_7e
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_7b .. :try_end_7e} :catch_135
    .catch Ljava/net/URISyntaxException; {:try_start_7b .. :try_end_7e} :catch_130

    #@7e
    move-object v9, v10

    #@7f
    .line 806
    .end local v6           #httpPost:Lorg/apache/http/client/methods/HttpPost;
    .end local v10           #requestHandler:Lcom/lge/systemservice/service/InfoCollector$RequestHandler;
    .end local v14           #url:Ljava/net/URI;
    .restart local v9       #requestHandler:Lcom/lge/systemservice/service/InfoCollector$RequestHandler;
    :goto_7f
    const-wide/16 v15, 0x1f40

    #@81
    :try_start_81
    invoke-static/range {v15 .. v16}, Ljava/lang/Thread;->sleep(J)V
    :try_end_84
    .catch Ljava/lang/InterruptedException; {:try_start_81 .. :try_end_84} :catch_c7

    #@84
    .line 815
    :goto_84
    sget-object v15, Lcom/lge/systemservice/service/InfoCollector;->strReceivedData:Ljava/lang/String;

    #@86
    invoke-virtual {v15}, Ljava/lang/String;->isEmpty()Z

    #@89
    move-result v15

    #@8a
    if-nez v15, :cond_10a

    #@8c
    .line 821
    move-object/from16 v0, p0

    #@8e
    iget-boolean v15, v0, Lcom/lge/systemservice/service/InfoCollector;->isDemo:Z

    #@90
    if-eqz v15, :cond_9f

    #@92
    .line 822
    sget-object v15, Lcom/lge/systemservice/service/InfoCollector;->mContext:Landroid/content/Context;

    #@94
    const-string v16, "OK : Ack is received! set send_flag!"

    #@96
    const/16 v17, 0x0

    #@98
    invoke-static/range {v15 .. v17}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@9b
    move-result-object v13

    #@9c
    .line 825
    .local v13, toast:Landroid/widget/Toast;
    invoke-virtual {v13}, Landroid/widget/Toast;->show()V

    #@9f
    .line 828
    .end local v13           #toast:Landroid/widget/Toast;
    :cond_9f
    sget-object v15, Lcom/lge/systemservice/service/InfoCollector;->strReceivedData:Ljava/lang/String;

    #@a1
    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@a4
    move-result-object v15

    #@a5
    const-string v16, "FD0000"

    #@a7
    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@aa
    move-result v15

    #@ab
    if-eqz v15, :cond_cc

    #@ad
    .line 830
    const-string v15, "IC"

    #@af
    const-string v16, "ack 0"

    #@b1
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b4
    .line 855
    :goto_b4
    const/4 v15, 0x0

    #@b5
    sput-boolean v15, Lcom/lge/systemservice/service/InfoCollector;->runningSendingAction:Z

    #@b7
    .line 879
    .end local v9           #requestHandler:Lcom/lge/systemservice/service/InfoCollector$RequestHandler;
    :goto_b7
    invoke-direct/range {p0 .. p0}, Lcom/lge/systemservice/service/InfoCollector;->terminateCollector()V

    #@ba
    .line 880
    return-void

    #@bb
    .line 795
    .end local v3           #entityRequest:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .restart local v4       #entityRequest:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .restart local v9       #requestHandler:Lcom/lge/systemservice/service/InfoCollector$RequestHandler;
    :catch_bb
    move-exception v1

    #@bc
    move-object v3, v4

    #@bd
    .line 796
    .end local v4           #entityRequest:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .local v1, e:Ljava/io/UnsupportedEncodingException;
    .restart local v3       #entityRequest:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    :goto_bd
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    #@c0
    goto :goto_7f

    #@c1
    .line 797
    .end local v1           #e:Ljava/io/UnsupportedEncodingException;
    .end local v3           #entityRequest:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .restart local v4       #entityRequest:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    :catch_c1
    move-exception v1

    #@c2
    move-object v3, v4

    #@c3
    .line 798
    .end local v4           #entityRequest:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .local v1, e:Ljava/net/URISyntaxException;
    .restart local v3       #entityRequest:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    :goto_c3
    invoke-virtual {v1}, Ljava/net/URISyntaxException;->printStackTrace()V

    #@c6
    goto :goto_7f

    #@c7
    .line 811
    .end local v1           #e:Ljava/net/URISyntaxException;
    :catch_c7
    move-exception v7

    #@c8
    .line 812
    .local v7, ie:Ljava/lang/InterruptedException;
    invoke-virtual {v7}, Ljava/lang/InterruptedException;->printStackTrace()V

    #@cb
    goto :goto_84

    #@cc
    .line 831
    .end local v7           #ie:Ljava/lang/InterruptedException;
    :cond_cc
    sget-object v15, Lcom/lge/systemservice/service/InfoCollector;->strReceivedData:Ljava/lang/String;

    #@ce
    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@d1
    move-result-object v15

    #@d2
    const-string v16, "FD0001"

    #@d4
    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d7
    move-result v15

    #@d8
    if-eqz v15, :cond_102

    #@da
    .line 836
    const-string v15, "IC"

    #@dc
    const-string v16, "ack 1"

    #@de
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@e1
    .line 837
    invoke-direct/range {p0 .. p0}, Lcom/lge/systemservice/service/InfoCollector;->removeAdminToRWD()V

    #@e4
    .line 839
    move-object/from16 v0, p0

    #@e6
    iget-object v15, v0, Lcom/lge/systemservice/service/InfoCollector;->prefs:Landroid/content/SharedPreferences;

    #@e8
    if-eqz v15, :cond_fe

    #@ea
    .line 843
    move-object/from16 v0, p0

    #@ec
    iget-object v15, v0, Lcom/lge/systemservice/service/InfoCollector;->prefs:Landroid/content/SharedPreferences;

    #@ee
    invoke-interface {v15}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@f1
    move-result-object v2

    #@f2
    .line 844
    .local v2, ed:Landroid/content/SharedPreferences$Editor;
    const-string v15, "NG_flag"

    #@f4
    const/16 v16, 0x1

    #@f6
    move/from16 v0, v16

    #@f8
    invoke-interface {v2, v15, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    #@fb
    .line 845
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    #@fe
    .line 847
    .end local v2           #ed:Landroid/content/SharedPreferences$Editor;
    :cond_fe
    invoke-direct/range {p0 .. p0}, Lcom/lge/systemservice/service/InfoCollector;->stopRWD()V

    #@101
    goto :goto_b4

    #@102
    .line 850
    :cond_102
    const-string v15, "IC"

    #@104
    const-string v16, "ack 2"

    #@106
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@109
    goto :goto_b4

    #@10a
    .line 858
    :cond_10a
    if-eqz v9, :cond_10f

    #@10c
    .line 859
    invoke-virtual {v9}, Lcom/lge/systemservice/service/InfoCollector$RequestHandler;->interrupt()V

    #@10f
    .line 866
    :cond_10f
    const/4 v15, 0x2

    #@110
    if-ne v12, v15, :cond_129

    #@112
    .line 867
    move-object/from16 v0, p0

    #@114
    iget-boolean v15, v0, Lcom/lge/systemservice/service/InfoCollector;->isDemo:Z

    #@116
    if-eqz v15, :cond_125

    #@118
    .line 868
    sget-object v15, Lcom/lge/systemservice/service/InfoCollector;->mContext:Landroid/content/Context;

    #@11a
    const-string v16, "NOK : Server error!! Ack is not received!"

    #@11c
    const/16 v17, 0x0

    #@11e
    invoke-static/range {v15 .. v17}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@121
    move-result-object v13

    #@122
    .line 871
    .restart local v13       #toast:Landroid/widget/Toast;
    invoke-virtual {v13}, Landroid/widget/Toast;->show()V

    #@125
    .line 873
    .end local v13           #toast:Landroid/widget/Toast;
    :cond_125
    const/4 v15, 0x0

    #@126
    sput-boolean v15, Lcom/lge/systemservice/service/InfoCollector;->runningSendingAction:Z

    #@128
    goto :goto_b7

    #@129
    .line 772
    :cond_129
    add-int/lit8 v12, v12, 0x1

    #@12b
    move-object v4, v3

    #@12c
    .end local v3           #entityRequest:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .restart local v4       #entityRequest:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    goto/16 :goto_58

    #@12e
    .line 797
    .end local v4           #entityRequest:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .restart local v3       #entityRequest:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .restart local v14       #url:Ljava/net/URI;
    :catch_12e
    move-exception v1

    #@12f
    goto :goto_c3

    #@130
    .end local v9           #requestHandler:Lcom/lge/systemservice/service/InfoCollector$RequestHandler;
    .restart local v6       #httpPost:Lorg/apache/http/client/methods/HttpPost;
    .restart local v10       #requestHandler:Lcom/lge/systemservice/service/InfoCollector$RequestHandler;
    :catch_130
    move-exception v1

    #@131
    move-object v9, v10

    #@132
    .end local v10           #requestHandler:Lcom/lge/systemservice/service/InfoCollector$RequestHandler;
    .restart local v9       #requestHandler:Lcom/lge/systemservice/service/InfoCollector$RequestHandler;
    goto :goto_c3

    #@133
    .line 795
    .end local v6           #httpPost:Lorg/apache/http/client/methods/HttpPost;
    :catch_133
    move-exception v1

    #@134
    goto :goto_bd

    #@135
    .end local v9           #requestHandler:Lcom/lge/systemservice/service/InfoCollector$RequestHandler;
    .restart local v6       #httpPost:Lorg/apache/http/client/methods/HttpPost;
    .restart local v10       #requestHandler:Lcom/lge/systemservice/service/InfoCollector$RequestHandler;
    :catch_135
    move-exception v1

    #@136
    move-object v9, v10

    #@137
    .end local v10           #requestHandler:Lcom/lge/systemservice/service/InfoCollector$RequestHandler;
    .restart local v9       #requestHandler:Lcom/lge/systemservice/service/InfoCollector$RequestHandler;
    goto :goto_bd

    #@138
    .end local v3           #entityRequest:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .end local v6           #httpPost:Lorg/apache/http/client/methods/HttpPost;
    .end local v9           #requestHandler:Lcom/lge/systemservice/service/InfoCollector$RequestHandler;
    .end local v14           #url:Ljava/net/URI;
    .restart local v4       #entityRequest:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    :cond_138
    move-object v3, v4

    #@139
    .end local v4           #entityRequest:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .restart local v3       #entityRequest:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    goto/16 :goto_b7
.end method

.method private stopRWD()V
    .registers 7

    #@0
    .prologue
    .line 902
    :try_start_0
    sget-object v0, Lcom/lge/systemservice/service/InfoCollector;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "com.lge.lghms"

    #@4
    const/4 v2, 0x3

    #@5
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    #@8
    move-result-object v0

    #@9
    .line 906
    const-string v1, "com.lge.lghms.HMSReceiver"

    #@b
    const/4 v2, 0x1

    #@c
    invoke-virtual {v0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    #@f
    move-result-object v0

    #@10
    invoke-static {v1, v2, v0}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    #@13
    move-result-object v0

    #@14
    .line 909
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    #@17
    move-result-object v1

    #@18
    .line 911
    const-string v2, "stopProcess"

    #@1a
    const/4 v3, 0x1

    #@1b
    new-array v3, v3, [Ljava/lang/Class;

    #@1d
    const/4 v4, 0x0

    #@1e
    const-class v5, Landroid/content/Context;

    #@20
    aput-object v5, v3, v4

    #@22
    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@25
    move-result-object v0

    #@26
    .line 913
    const/4 v2, 0x1

    #@27
    new-array v2, v2, [Ljava/lang/Object;

    #@29
    const/4 v3, 0x0

    #@2a
    sget-object v4, Lcom/lge/systemservice/service/InfoCollector;->mContext:Landroid/content/Context;

    #@2c
    aput-object v4, v2, v3

    #@2e
    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_31
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_31} :catch_32

    #@31
    .line 917
    :goto_31
    return-void

    #@32
    .line 914
    :catch_32
    move-exception v0

    #@33
    .line 915
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@36
    goto :goto_31
.end method

.method private terminateCollector()V
    .registers 3

    #@0
    .prologue
    .line 888
    :try_start_0
    sget-object v0, Lcom/lge/systemservice/service/InfoCollector;->mContext:Landroid/content/Context;

    #@2
    iget-object v1, p0, Lcom/lge/systemservice/service/InfoCollector;->mPhoneInfoReceiver:Landroid/content/BroadcastReceiver;

    #@4
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_7} :catch_8

    #@7
    .line 898
    :goto_7
    return-void

    #@8
    .line 889
    :catch_8
    move-exception v0

    #@9
    goto :goto_7
.end method


# virtual methods
.method public buildAndSendInfotoHTTPs()V
    .registers 2

    #@0
    .prologue
    .line 362
    invoke-direct {p0}, Lcom/lge/systemservice/service/InfoCollector;->buildInfoforHTTP()Ljava/util/ArrayList;

    #@3
    move-result-object v0

    #@4
    .line 363
    .local v0, payload:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/apache/http/NameValuePair;>;"
    invoke-direct {p0, v0}, Lcom/lge/systemservice/service/InfoCollector;->sendInfotoHTTPs(Ljava/util/ArrayList;)V

    #@7
    .line 364
    return-void
.end method

.method public readyToSend()V
    .registers 5

    #@0
    .prologue
    .line 315
    sget-object v2, Lcom/lge/systemservice/service/InfoCollector;->mContext:Landroid/content/Context;

    #@2
    const-string v3, "location"

    #@4
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@7
    move-result-object v2

    #@8
    check-cast v2, Landroid/location/LocationManager;

    #@a
    sput-object v2, Lcom/lge/systemservice/service/InfoCollector;->mlocationManager:Landroid/location/LocationManager;

    #@c
    .line 317
    const/4 v2, 0x2

    #@d
    new-array v2, v2, [Ljava/lang/String;

    #@f
    sput-object v2, Lcom/lge/systemservice/service/InfoCollector;->position:[Ljava/lang/String;

    #@11
    .line 318
    new-instance v0, Landroid/content/IntentFilter;

    #@13
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@16
    .line 319
    .local v0, phoneFilter:Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.ANY_DATA_STATE"

    #@18
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1b
    .line 320
    sget-object v2, Lcom/lge/systemservice/service/InfoCollector;->mContext:Landroid/content/Context;

    #@1d
    iget-object v3, p0, Lcom/lge/systemservice/service/InfoCollector;->mPhoneInfoReceiver:Landroid/content/BroadcastReceiver;

    #@1f
    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@22
    .line 321
    new-instance v1, Landroid/content/IntentFilter;

    #@24
    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    #@27
    .line 322
    .local v1, wifiFilter:Landroid/content/IntentFilter;
    const-string v2, "android.net.wifi.STATE_CHANGE"

    #@29
    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@2c
    .line 323
    sget-object v2, Lcom/lge/systemservice/service/InfoCollector;->mContext:Landroid/content/Context;

    #@2e
    iget-object v3, p0, Lcom/lge/systemservice/service/InfoCollector;->mWifiInfoReceiver:Landroid/content/BroadcastReceiver;

    #@30
    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@33
    .line 359
    return-void
.end method

.method public sendLocationUpdate(Landroid/location/Location;)V
    .registers 16
    .parameter "location"

    #@0
    .prologue
    .line 554
    :try_start_0
    sget-object v5, Lcom/lge/systemservice/service/InfoCollector;->position:[Ljava/lang/String;

    #@2
    const/4 v11, 0x0

    #@3
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    #@6
    move-result-wide v12

    #@7
    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    #@a
    move-result-object v12

    #@b
    aput-object v12, v5, v11

    #@d
    .line 555
    sget-object v5, Lcom/lge/systemservice/service/InfoCollector;->position:[Ljava/lang/String;

    #@f
    const/4 v11, 0x1

    #@10
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    #@13
    move-result-wide v12

    #@14
    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    #@17
    move-result-object v12

    #@18
    aput-object v12, v5, v11

    #@1a
    .line 556
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    #@1d
    move-result-wide v1

    #@1e
    .line 557
    .local v1, latitude:D
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D
    :try_end_21
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_21} :catch_5b

    #@21
    move-result-wide v3

    #@22
    .line 564
    .local v3, longitude:D
    :try_start_22
    new-instance v0, Landroid/location/Geocoder;

    #@24
    sget-object v5, Lcom/lge/systemservice/service/InfoCollector;->mContext:Landroid/content/Context;

    #@26
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@29
    move-result-object v11

    #@2a
    invoke-direct {v0, v5, v11}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;Ljava/util/Locale;)V

    #@2d
    .line 565
    .local v0, gc:Landroid/location/Geocoder;
    const/4 v5, 0x1

    #@2e
    invoke-virtual/range {v0 .. v5}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;

    #@31
    move-result-object v7

    #@32
    .line 567
    .local v7, addresses:Ljava/util/List;,"Ljava/util/List<Landroid/location/Address;>;"
    new-instance v10, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    .line 569
    .local v10, sb:Ljava/lang/StringBuilder;
    invoke-interface {v7}, Ljava/util/List;->size()I

    #@3a
    move-result v5

    #@3b
    if-lez v5, :cond_63

    #@3d
    .line 570
    const/4 v5, 0x0

    #@3e
    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@41
    move-result-object v6

    #@42
    check-cast v6, Landroid/location/Address;

    #@44
    .line 571
    .local v6, address:Landroid/location/Address;
    const/4 v9, 0x0

    #@45
    .local v9, i:I
    :goto_45
    invoke-virtual {v6}, Landroid/location/Address;->getMaxAddressLineIndex()I

    #@48
    move-result v5

    #@49
    if-ge v9, v5, :cond_64

    #@4b
    .line 572
    invoke-virtual {v6, v9}, Landroid/location/Address;->getAddressLine(I)Ljava/lang/String;

    #@4e
    move-result-object v5

    #@4f
    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v5

    #@53
    const-string v11, "\n"

    #@55
    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_58
    .catch Ljava/io/IOException; {:try_start_22 .. :try_end_58} :catch_ac
    .catch Ljava/lang/NullPointerException; {:try_start_22 .. :try_end_58} :catch_b1

    #@58
    .line 571
    add-int/lit8 v9, v9, 0x1

    #@5a
    goto :goto_45

    #@5b
    .line 558
    .end local v0           #gc:Landroid/location/Geocoder;
    .end local v1           #latitude:D
    .end local v3           #longitude:D
    .end local v6           #address:Landroid/location/Address;
    .end local v7           #addresses:Ljava/util/List;,"Ljava/util/List<Landroid/location/Address;>;"
    .end local v9           #i:I
    .end local v10           #sb:Ljava/lang/StringBuilder;
    :catch_5b
    move-exception v8

    #@5c
    .line 559
    .local v8, e:Ljava/lang/NullPointerException;
    const-string v5, "InfoCollector"

    #@5e
    const-string v11, "[SPTS] location is null"

    #@60
    invoke-static {v5, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@63
    .line 592
    .end local v8           #e:Ljava/lang/NullPointerException;
    :cond_63
    :goto_63
    return-void

    #@64
    .line 574
    .restart local v0       #gc:Landroid/location/Geocoder;
    .restart local v1       #latitude:D
    .restart local v3       #longitude:D
    .restart local v6       #address:Landroid/location/Address;
    .restart local v7       #addresses:Ljava/util/List;,"Ljava/util/List<Landroid/location/Address;>;"
    .restart local v9       #i:I
    .restart local v10       #sb:Ljava/lang/StringBuilder;
    :cond_64
    :try_start_64
    invoke-virtual {v6}, Landroid/location/Address;->getCountryName()Ljava/lang/String;

    #@67
    move-result-object v5

    #@68
    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v5

    #@6c
    const-string v11, " "

    #@6e
    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    .line 575
    invoke-virtual {v6}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    #@74
    move-result-object v5

    #@75
    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v5

    #@79
    const-string v11, " "

    #@7b
    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    .line 576
    invoke-virtual {v6}, Landroid/location/Address;->getSubLocality()Ljava/lang/String;

    #@81
    move-result-object v5

    #@82
    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v5

    #@86
    const-string v11, " "

    #@88
    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    .line 577
    invoke-virtual {v6}, Landroid/location/Address;->getThoroughfare()Ljava/lang/String;

    #@8e
    move-result-object v5

    #@8f
    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v5

    #@93
    const-string v11, " "

    #@95
    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    .line 578
    invoke-virtual {v6}, Landroid/location/Address;->getFeatureName()Ljava/lang/String;

    #@9b
    move-result-object v5

    #@9c
    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v5

    #@a0
    const-string v11, ""

    #@a2
    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    .line 581
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a8
    move-result-object v5

    #@a9
    sput-object v5, Lcom/lge/systemservice/service/InfoCollector;->mGPS:Ljava/lang/String;
    :try_end_ab
    .catch Ljava/io/IOException; {:try_start_64 .. :try_end_ab} :catch_ac
    .catch Ljava/lang/NullPointerException; {:try_start_64 .. :try_end_ab} :catch_b1

    #@ab
    goto :goto_63

    #@ac
    .line 585
    .end local v0           #gc:Landroid/location/Geocoder;
    .end local v6           #address:Landroid/location/Address;
    .end local v7           #addresses:Ljava/util/List;,"Ljava/util/List<Landroid/location/Address;>;"
    .end local v9           #i:I
    .end local v10           #sb:Ljava/lang/StringBuilder;
    :catch_ac
    move-exception v8

    #@ad
    .line 587
    .local v8, e:Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    #@b0
    goto :goto_63

    #@b1
    .line 588
    .end local v8           #e:Ljava/io/IOException;
    :catch_b1
    move-exception v8

    #@b2
    .line 589
    .local v8, e:Ljava/lang/NullPointerException;
    const-string v5, "InfoCollector"

    #@b4
    const-string v11, "[SPTS] location is null"

    #@b6
    invoke-static {v5, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b9
    goto :goto_63
.end method

.method public startInfoCollecting()V
    .registers 1

    #@0
    .prologue
    .line 163
    invoke-virtual {p0}, Lcom/lge/systemservice/service/InfoCollector;->readyToSend()V

    #@3
    .line 165
    return-void
.end method
