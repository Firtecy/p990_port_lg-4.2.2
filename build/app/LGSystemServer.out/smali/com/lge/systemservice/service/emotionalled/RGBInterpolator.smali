.class public Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;
.super Ljava/lang/Object;
.source "RGBInterpolator.java"


# instance fields
.field private diffColor:[[I

.field private handlerThread:Ljava/lang/Thread;

.field private mBackLedPlaying:Z

.field private mHasBackLed:Z

.field public mRGBDevice:Lcom/lge/systemservice/service/emotionalled/RGBDevice;

.field private refreshCount:I

.field private refreshTime:I


# direct methods
.method public constructor <init>(I)V
    .registers 5
    .parameter "dev"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 41
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 29
    const/4 v0, 0x4

    #@5
    const/4 v1, 0x2

    #@6
    filled-new-array {v0, v1}, [I

    #@9
    move-result-object v0

    #@a
    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@c
    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, [[I

    #@12
    iput-object v0, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->diffColor:[[I

    #@14
    .line 33
    iput-boolean v2, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->mHasBackLed:Z

    #@16
    .line 34
    iput-boolean v2, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->mBackLedPlaying:Z

    #@18
    .line 42
    packed-switch p1, :pswitch_data_3c

    #@1b
    .line 46
    new-instance v0, Lcom/lge/systemservice/service/emotionalled/DeviceLOG;

    #@1d
    invoke-direct {v0}, Lcom/lge/systemservice/service/emotionalled/DeviceLOG;-><init>()V

    #@20
    iput-object v0, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->mRGBDevice:Lcom/lge/systemservice/service/emotionalled/RGBDevice;

    #@22
    .line 48
    :goto_22
    return-void

    #@23
    .line 43
    :pswitch_23
    new-instance v0, Lcom/lge/systemservice/service/emotionalled/DeviceLED;

    #@25
    invoke-direct {v0}, Lcom/lge/systemservice/service/emotionalled/DeviceLED;-><init>()V

    #@28
    iput-object v0, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->mRGBDevice:Lcom/lge/systemservice/service/emotionalled/RGBDevice;

    #@2a
    goto :goto_22

    #@2b
    .line 44
    :pswitch_2b
    new-instance v0, Lcom/lge/systemservice/service/emotionalled/DeviceVIEW;

    #@2d
    invoke-direct {v0}, Lcom/lge/systemservice/service/emotionalled/DeviceVIEW;-><init>()V

    #@30
    iput-object v0, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->mRGBDevice:Lcom/lge/systemservice/service/emotionalled/RGBDevice;

    #@32
    goto :goto_22

    #@33
    .line 45
    :pswitch_33
    new-instance v0, Lcom/lge/systemservice/service/emotionalled/DeviceLOG;

    #@35
    invoke-direct {v0}, Lcom/lge/systemservice/service/emotionalled/DeviceLOG;-><init>()V

    #@38
    iput-object v0, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->mRGBDevice:Lcom/lge/systemservice/service/emotionalled/RGBDevice;

    #@3a
    goto :goto_22

    #@3b
    .line 42
    nop

    #@3c
    :pswitch_data_3c
    .packed-switch 0x1
        :pswitch_23
        :pswitch_2b
        :pswitch_33
    .end packed-switch
.end method

.method static synthetic access$000(Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 15
    iget-boolean v0, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->mBackLedPlaying:Z

    #@2
    return v0
.end method

.method static synthetic access$002(Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 15
    iput-boolean p1, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->mBackLedPlaying:Z

    #@2
    return p1
.end method

.method static synthetic access$100(Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;Ljava/lang/String;I)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 15
    #invoke-direct {p0, p1, p2}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->writeValue(Ljava/lang/String;I)Z

    #@3
    #move-result v0
	const/4 v0, 0x1

    #@4
    return v0
.end method

.method static synthetic access$200(Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 15
    iget v0, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->refreshCount:I

    #@2
    return v0
.end method

.method static synthetic access$300(Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;)[[I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 15
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->diffColor:[[I

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 15
    iget v0, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->refreshTime:I

    #@2
    return v0
.end method

.method static synthetic access$500(Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 15
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->onBackLed()V

    #@3
    return-void
.end method

.method private offBackLed()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 204
    iget-boolean v0, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->mHasBackLed:Z

    #@3
    if-eqz v0, :cond_f

    #@5
    .line 206
    const-string v0, "/sys/class/leds/button-backlight1/brightness"

    #@7
    #invoke-direct {p0, v0, v1}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->writeValue(Ljava/lang/String;I)Z

    #@a
    .line 207
    const-string v0, "/sys/class/leds/button-backlight2/brightness"

    #@c
    #invoke-direct {p0, v0, v1}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->writeValue(Ljava/lang/String;I)Z

    #@f
    .line 209
    :cond_f
    return-void
.end method

.method private onBackLed()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 195
    iget-boolean v1, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->mHasBackLed:Z

    #@3
    if-eqz v1, :cond_28

    #@5
    .line 196
    iget-object v1, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->diffColor:[[I

    #@7
    const/4 v2, 0x1

    #@8
    aget-object v1, v1, v2

    #@a
    aget v1, v1, v4

    #@c
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->diffColor:[[I

    #@e
    const/4 v3, 0x2

    #@f
    aget-object v2, v2, v3

    #@11
    aget v2, v2, v4

    #@13
    add-int/2addr v1, v2

    #@14
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->diffColor:[[I

    #@16
    const/4 v3, 0x3

    #@17
    aget-object v2, v2, v3

    #@19
    aget v2, v2, v4

    #@1b
    add-int/2addr v1, v2

    #@1c
    div-int/lit8 v0, v1, 0x3

    #@1e
    .line 198
    .local v0, btightness:I
    const-string v1, "/sys/class/leds/button-backlight1/brightness"

    #@20
    #invoke-direct {p0, v1, v0}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->writeValue(Ljava/lang/String;I)Z

    #@23
    .line 199
    const-string v1, "/sys/class/leds/button-backlight2/brightness"

    #@25
    #invoke-direct {p0, v1, v0}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->writeValue(Ljava/lang/String;I)Z

    #@28
    .line 201
    .end local v0           #btightness:I
    :cond_28
    return-void
.end method

.method private writeValue(Ljava/lang/String;I)Z
    .registers 7
    .parameter "path"
    .parameter "value"

    #@0
    .prologue
    .line 212
    const/4 v0, 0x0

    #@1
    .line 214
    .local v0, bw:Ljava/io/BufferedWriter;
    :try_start_1
    new-instance v1, Ljava/io/BufferedWriter;

    #@3
    new-instance v3, Ljava/io/FileWriter;

    #@5
    invoke-direct {v3, p1}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    #@8
    invoke-direct {v1, v3}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_32
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_b} :catch_22

    #@b
    .line 215
    .end local v0           #bw:Ljava/io/BufferedWriter;
    .local v1, bw:Ljava/io/BufferedWriter;
    :try_start_b
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@e
    move-result-object v3

    #@f
    invoke-virtual {v1, v3}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@12
    .line 216
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->flush()V
    :try_end_15
    .catchall {:try_start_b .. :try_end_15} :catchall_3e
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_15} :catch_41

    #@15
    .line 217
    const/4 v3, 0x1

    #@16
    .line 223
    if-eqz v1, :cond_1b

    #@18
    .line 224
    :try_start_18
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_1b
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_1b} :catch_1d

    #@1b
    :cond_1b
    :goto_1b
    move-object v0, v1

    #@1c
    .line 228
    .end local v1           #bw:Ljava/io/BufferedWriter;
    .restart local v0       #bw:Ljava/io/BufferedWriter;
    :cond_1c
    :goto_1c
    return v3

    #@1d
    .line 226
    .end local v0           #bw:Ljava/io/BufferedWriter;
    .restart local v1       #bw:Ljava/io/BufferedWriter;
    :catch_1d
    move-exception v2

    #@1e
    .line 227
    .local v2, e:Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    #@21
    goto :goto_1b

    #@22
    .line 218
    .end local v1           #bw:Ljava/io/BufferedWriter;
    .end local v2           #e:Ljava/io/IOException;
    .restart local v0       #bw:Ljava/io/BufferedWriter;
    :catch_22
    move-exception v2

    #@23
    .line 219
    .local v2, e:Ljava/lang/Exception;
    :goto_23
    :try_start_23
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_26
    .catchall {:try_start_23 .. :try_end_26} :catchall_32

    #@26
    .line 220
    const/4 v3, 0x0

    #@27
    .line 223
    if-eqz v0, :cond_1c

    #@29
    .line 224
    :try_start_29
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_2c
    .catch Ljava/io/IOException; {:try_start_29 .. :try_end_2c} :catch_2d

    #@2c
    goto :goto_1c

    #@2d
    .line 226
    :catch_2d
    move-exception v2

    #@2e
    .line 227
    .local v2, e:Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    #@31
    goto :goto_1c

    #@32
    .line 222
    .end local v2           #e:Ljava/io/IOException;
    :catchall_32
    move-exception v3

    #@33
    .line 223
    :goto_33
    if-eqz v0, :cond_38

    #@35
    .line 224
    :try_start_35
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_38
    .catch Ljava/io/IOException; {:try_start_35 .. :try_end_38} :catch_39

    #@38
    .line 228
    :cond_38
    :goto_38
    throw v3

    #@39
    .line 226
    :catch_39
    move-exception v2

    #@3a
    .line 227
    .restart local v2       #e:Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    #@3d
    goto :goto_38

    #@3e
    .line 222
    .end local v0           #bw:Ljava/io/BufferedWriter;
    .end local v2           #e:Ljava/io/IOException;
    .restart local v1       #bw:Ljava/io/BufferedWriter;
    :catchall_3e
    move-exception v3

    #@3f
    move-object v0, v1

    #@40
    .end local v1           #bw:Ljava/io/BufferedWriter;
    .restart local v0       #bw:Ljava/io/BufferedWriter;
    goto :goto_33

    #@41
    .line 218
    .end local v0           #bw:Ljava/io/BufferedWriter;
    .restart local v1       #bw:Ljava/io/BufferedWriter;
    :catch_41
    move-exception v2

    #@42
    move-object v0, v1

    #@43
    .end local v1           #bw:Ljava/io/BufferedWriter;
    .restart local v0       #bw:Ljava/io/BufferedWriter;
    goto :goto_23
.end method


# virtual methods
.method public getInputFromUser(Lcom/lge/systemservice/service/emotionalled/RGBFormat;)I
    .registers 11
    .parameter "pattern"

    #@0
    .prologue
    const/4 v8, 0x3

    #@1
    const/4 v7, 0x2

    #@2
    const/4 v6, 0x1

    #@3
    const/4 v5, 0x0

    #@4
    .line 55
    invoke-virtual {p1}, Lcom/lge/systemservice/service/emotionalled/RGBFormat;->getDuration()I

    #@7
    move-result v2

    #@8
    .line 56
    .local v2, timeToTarget:I
    invoke-virtual {p1}, Lcom/lge/systemservice/service/emotionalled/RGBFormat;->getInterval()I

    #@b
    move-result v3

    #@c
    iput v3, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->refreshTime:I

    #@e
    .line 57
    if-eqz v2, :cond_14

    #@10
    iget v3, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->refreshTime:I

    #@12
    if-nez v3, :cond_17

    #@14
    .line 58
    :cond_14
    iget v2, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->refreshTime:I

    #@16
    .line 77
    .end local v2           #timeToTarget:I
    :goto_16
    return v2

    #@17
    .line 61
    .restart local v2       #timeToTarget:I
    :cond_17
    iget v3, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->refreshTime:I

    #@19
    div-int v3, v2, v3

    #@1b
    iput v3, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->refreshCount:I

    #@1d
    .line 62
    invoke-virtual {p1}, Lcom/lge/systemservice/service/emotionalled/RGBFormat;->getCurrentColor()I

    #@20
    move-result v0

    #@21
    .line 63
    .local v0, currentColor:I
    invoke-virtual {p1}, Lcom/lge/systemservice/service/emotionalled/RGBFormat;->getTargetColor()I

    #@24
    move-result v1

    #@25
    .line 66
    .local v1, targetColor:I
    iget-object v3, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->diffColor:[[I

    #@27
    aget-object v3, v3, v5

    #@29
    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    #@2c
    move-result v4

    #@2d
    aput v4, v3, v5

    #@2f
    .line 67
    iget-object v3, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->diffColor:[[I

    #@31
    aget-object v3, v3, v6

    #@33
    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    #@36
    move-result v4

    #@37
    aput v4, v3, v5

    #@39
    .line 68
    iget-object v3, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->diffColor:[[I

    #@3b
    aget-object v3, v3, v7

    #@3d
    invoke-static {v0}, Landroid/graphics/Color;->green(I)I

    #@40
    move-result v4

    #@41
    aput v4, v3, v5

    #@43
    .line 69
    iget-object v3, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->diffColor:[[I

    #@45
    aget-object v3, v3, v8

    #@47
    invoke-static {v0}, Landroid/graphics/Color;->blue(I)I

    #@4a
    move-result v4

    #@4b
    aput v4, v3, v5

    #@4d
    .line 72
    iget-object v3, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->diffColor:[[I

    #@4f
    aget-object v3, v3, v5

    #@51
    invoke-static {v1}, Landroid/graphics/Color;->alpha(I)I

    #@54
    move-result v4

    #@55
    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    #@58
    move-result v5

    #@59
    sub-int/2addr v4, v5

    #@5a
    iget v5, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->refreshCount:I

    #@5c
    div-int/2addr v4, v5

    #@5d
    aput v4, v3, v6

    #@5f
    .line 73
    iget-object v3, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->diffColor:[[I

    #@61
    aget-object v3, v3, v6

    #@63
    invoke-static {v1}, Landroid/graphics/Color;->red(I)I

    #@66
    move-result v4

    #@67
    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    #@6a
    move-result v5

    #@6b
    sub-int/2addr v4, v5

    #@6c
    iget v5, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->refreshCount:I

    #@6e
    div-int/2addr v4, v5

    #@6f
    aput v4, v3, v6

    #@71
    .line 74
    iget-object v3, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->diffColor:[[I

    #@73
    aget-object v3, v3, v7

    #@75
    invoke-static {v1}, Landroid/graphics/Color;->green(I)I

    #@78
    move-result v4

    #@79
    invoke-static {v0}, Landroid/graphics/Color;->green(I)I

    #@7c
    move-result v5

    #@7d
    sub-int/2addr v4, v5

    #@7e
    iget v5, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->refreshCount:I

    #@80
    div-int/2addr v4, v5

    #@81
    aput v4, v3, v6

    #@83
    .line 75
    iget-object v3, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->diffColor:[[I

    #@85
    aget-object v3, v3, v8

    #@87
    invoke-static {v1}, Landroid/graphics/Color;->blue(I)I

    #@8a
    move-result v4

    #@8b
    invoke-static {v0}, Landroid/graphics/Color;->blue(I)I

    #@8e
    move-result v5

    #@8f
    sub-int/2addr v4, v5

    #@90
    iget v5, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->refreshCount:I

    #@92
    div-int/2addr v4, v5

    #@93
    aput v4, v3, v6

    #@95
    goto :goto_16
.end method

.method public setBackLed(Z)V
    .registers 2
    .parameter "hasBackLed"

    #@0
    .prologue
    .line 191
    iput-boolean p1, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->mHasBackLed:Z

    #@2
    .line 192
    return-void
.end method

.method public startBackLedThread_withHandler(II)V
    .registers 5
    .parameter "ledOnMS"
    .parameter "ledOffMS"

    #@0
    .prologue
    .line 84
    new-instance v0, Ljava/lang/Thread;

    #@2
    new-instance v1, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$1;

    #@4
    invoke-direct {v1, p0, p1, p2}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$1;-><init>(Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;II)V

    #@7
    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    #@a
    iput-object v0, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->handlerThread:Ljava/lang/Thread;

    #@c
    .line 110
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->handlerThread:Ljava/lang/Thread;

    #@e
    const-string v1, "BackLED"

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    #@13
    .line 111
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->handlerThread:Ljava/lang/Thread;

    #@15
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    #@18
    .line 112
    return-void
.end method

.method public startThread_withHandler(Ljava/util/ArrayList;I)V
    .registers 5
    .parameter
    .parameter "whichLedPlay"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/lge/systemservice/service/emotionalled/RGBFormat;",
            ">;I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 116
    .local p1, playPattern:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/lge/systemservice/service/emotionalled/RGBFormat;>;"
    new-instance v0, Ljava/lang/Thread;

    #@2
    new-instance v1, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;

    #@4
    invoke-direct {v1, p0, p1, p2}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;-><init>(Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;Ljava/util/ArrayList;I)V

    #@7
    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    #@a
    iput-object v0, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->handlerThread:Ljava/lang/Thread;

    #@c
    .line 171
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->handlerThread:Ljava/lang/Thread;

    #@e
    const-string v1, "LED"

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    #@13
    .line 172
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->handlerThread:Ljava/lang/Thread;

    #@15
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    #@18
    .line 173
    return-void
.end method

.method public stopThread_withHandler()V
    .registers 3

    #@0
    .prologue
    .line 177
    const/4 v1, 0x0

    #@1
    iput-boolean v1, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->mBackLedPlaying:Z

    #@3
    .line 178
    iget-object v1, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->handlerThread:Ljava/lang/Thread;

    #@5
    if-eqz v1, :cond_1c

    #@7
    iget-object v1, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->handlerThread:Ljava/lang/Thread;

    #@9
    invoke-virtual {v1}, Ljava/lang/Thread;->isAlive()Z

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_1c

    #@f
    .line 180
    :try_start_f
    iget-object v1, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->handlerThread:Ljava/lang/Thread;

    #@11
    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    #@14
    .line 181
    iget-object v1, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->handlerThread:Ljava/lang/Thread;

    #@16
    invoke-virtual {v1}, Ljava/lang/Thread;->join()V

    #@19
    .line 182
    const/4 v1, 0x0

    #@1a
    iput-object v1, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->handlerThread:Ljava/lang/Thread;
    :try_end_1c
    .catch Ljava/lang/InterruptedException; {:try_start_f .. :try_end_1c} :catch_20

    #@1c
    .line 187
    :cond_1c
    :goto_1c
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->offBackLed()V

    #@1f
    .line 188
    return-void

    #@20
    .line 183
    :catch_20
    move-exception v0

    #@21
    .line 184
    .local v0, e:Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    #@24
    goto :goto_1c
.end method
