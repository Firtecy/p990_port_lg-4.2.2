.class Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3$1;
.super Ljava/lang/Object;
.source "WfdStateTracker.java"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 308
    iput-object p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3$1;->this$1:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onFailure(I)V
    .registers 5
    .parameter "reason"

    #@0
    .prologue
    .line 313
    const-string v0, "WfdStateTracker"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Direct disconnect fail, reason: "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 314
    return-void
.end method

.method public onSuccess()V
    .registers 3

    #@0
    .prologue
    .line 310
    const-string v0, "WfdStateTracker"

    #@2
    const-string v1, "Direct disconnect success"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 311
    return-void
.end method
