.class Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$SettingsObserver;
.super Landroid/database/ContentObserver;
.source "EmotionalLedService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Landroid/os/Handler;)V
    .registers 3
    .parameter
    .parameter "handler"

    #@0
    .prologue
    .line 162
    iput-object p1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$SettingsObserver;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@2
    .line 163
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    #@5
    .line 164
    return-void
.end method


# virtual methods
.method observe()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 167
    iget-object v1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$SettingsObserver;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@3
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$100(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Landroid/content/Context;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@a
    move-result-object v0

    #@b
    .line 168
    .local v0, resolver:Landroid/content/ContentResolver;
    const-string v1, "notification_light_pulse"

    #@d
    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@14
    .line 170
    const-string v1, "lge_notification_light_pulse"

    #@16
    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@1d
    .line 172
    const-string v1, "emotional_led_battery_charging"

    #@1f
    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@26
    .line 174
    const-string v1, "quick_cover_enable"

    #@28
    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@2b
    move-result-object v1

    #@2c
    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@2f
    .line 176
    const-string v1, "emotional_led_back_led"

    #@31
    invoke-static {v1}, Lcom/lge/provider/SettingsEx$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@34
    move-result-object v1

    #@35
    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@38
    .line 179
    invoke-virtual {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$SettingsObserver;->update()V

    #@3b
    .line 180
    return-void
.end method

.method public onChange(Z)V
    .registers 2
    .parameter "selfChange"

    #@0
    .prologue
    .line 184
    invoke-virtual {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$SettingsObserver;->update()V

    #@3
    .line 185
    return-void
.end method

.method public update()V
    .registers 10

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 188
    iget-object v6, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$SettingsObserver;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@4
    invoke-static {v6}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$200(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Ljava/util/ArrayList;

    #@7
    move-result-object v6

    #@8
    monitor-enter v6

    #@9
    .line 189
    :try_start_9
    iget-object v7, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$SettingsObserver;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@b
    invoke-static {v7}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$100(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Landroid/content/Context;

    #@e
    move-result-object v7

    #@f
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@12
    move-result-object v3

    #@13
    .line 190
    .local v3, resolver:Landroid/content/ContentResolver;
    const-string v7, "notification_light_pulse"

    #@15
    const/4 v8, 0x0

    #@16
    invoke-static {v3, v7, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@19
    move-result v7

    #@1a
    if-eqz v7, :cond_a9

    #@1c
    move v2, v4

    #@1d
    .line 192
    .local v2, pulseEnabled:Z
    :goto_1d
    const-string v7, "lge_notification_light_pulse"

    #@1f
    const/4 v8, 0x1

    #@20
    invoke-static {v3, v7, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@23
    move-result v7

    #@24
    if-eqz v7, :cond_ac

    #@26
    move v1, v4

    #@27
    .line 194
    .local v1, emotionalEnabled:Z
    :goto_27
    const-string v7, "emotional_led_battery_charging"

    #@29
    const/4 v8, 0x1

    #@2a
    invoke-static {v3, v7, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@2d
    move-result v7

    #@2e
    if-eqz v7, :cond_af

    #@30
    move v0, v4

    #@31
    .line 196
    .local v0, chargingLedEnabled:Z
    :goto_31
    const-string v7, "quick_cover_enable"

    #@33
    const/4 v8, 0x0

    #@34
    invoke-static {v3, v7, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@37
    move-result v7

    #@38
    if-eqz v7, :cond_b1

    #@3a
    .line 199
    .local v4, smartCoverEnabled:Z
    :goto_3a
    invoke-static {}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$300()Z

    #@3d
    move-result v5

    #@3e
    if-eqz v5, :cond_76

    #@40
    const-string v5, "EmotionalLed"

    #@42
    new-instance v7, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v8, "DB update: pulseEnabled="

    #@49
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v7

    #@4d
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@50
    move-result-object v7

    #@51
    const-string v8, ", emotionalEnabled="

    #@53
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v7

    #@57
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v7

    #@5b
    const-string v8, ", chargingLedEnabled="

    #@5d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v7

    #@61
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@64
    move-result-object v7

    #@65
    const-string v8, ", smartCoverEnabled="

    #@67
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v7

    #@6b
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v7

    #@6f
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@72
    move-result-object v7

    #@73
    invoke-static {v5, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@76
    .line 205
    :cond_76
    iget-object v5, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$SettingsObserver;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@78
    invoke-static {v5}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$400(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Z

    #@7b
    move-result v5

    #@7c
    if-ne v5, v2, :cond_8e

    #@7e
    iget-object v5, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$SettingsObserver;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@80
    invoke-static {v5}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$500(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Z

    #@83
    move-result v5

    #@84
    if-ne v5, v1, :cond_8e

    #@86
    iget-object v5, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$SettingsObserver;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@88
    invoke-static {v5}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$600(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Z

    #@8b
    move-result v5

    #@8c
    if-eq v5, v0, :cond_b3

    #@8e
    .line 208
    :cond_8e
    iget-object v5, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$SettingsObserver;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@90
    invoke-static {v5, v2}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$402(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Z)Z

    #@93
    .line 209
    iget-object v5, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$SettingsObserver;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@95
    invoke-static {v5, v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$502(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Z)Z

    #@98
    .line 210
    iget-object v5, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$SettingsObserver;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@9a
    invoke-static {v5, v0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$602(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Z)Z

    #@9d
    .line 212
    iget-object v5, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$SettingsObserver;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@9f
    #calls: Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->doRestart()V
    invoke-static {v5}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$700(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)V

    #@a2
    .line 216
    :cond_a2
    :goto_a2
    iget-object v5, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$SettingsObserver;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@a4
    #calls: Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->updateBatteryChargingLocked()V
    invoke-static {v5}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$900(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)V

    #@a7
    .line 217
    monitor-exit v6

    #@a8
    .line 218
    return-void

    #@a9
    .end local v0           #chargingLedEnabled:Z
    .end local v1           #emotionalEnabled:Z
    .end local v2           #pulseEnabled:Z
    .end local v4           #smartCoverEnabled:Z
    :cond_a9
    move v2, v5

    #@aa
    .line 190
    goto/16 :goto_1d

    #@ac
    .restart local v2       #pulseEnabled:Z
    :cond_ac
    move v1, v5

    #@ad
    .line 192
    goto/16 :goto_27

    #@af
    .restart local v1       #emotionalEnabled:Z
    :cond_af
    move v0, v5

    #@b0
    .line 194
    goto :goto_31

    #@b1
    .restart local v0       #chargingLedEnabled:Z
    :cond_b1
    move v4, v5

    #@b2
    .line 196
    goto :goto_3a

    #@b3
    .line 213
    .restart local v4       #smartCoverEnabled:Z
    :cond_b3
    iget-object v5, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$SettingsObserver;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@b5
    invoke-static {v5}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$800(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Z

    #@b8
    move-result v5

    #@b9
    if-eq v5, v4, :cond_a2

    #@bb
    .line 214
    iget-object v5, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$SettingsObserver;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@bd
    invoke-static {v5, v4}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$802(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Z)Z

    #@c0
    goto :goto_a2

    #@c1
    .line 217
    .end local v0           #chargingLedEnabled:Z
    .end local v1           #emotionalEnabled:Z
    .end local v2           #pulseEnabled:Z
    .end local v3           #resolver:Landroid/content/ContentResolver;
    .end local v4           #smartCoverEnabled:Z
    :catchall_c1
    move-exception v5

    #@c2
    monitor-exit v6
    :try_end_c3
    .catchall {:try_start_9 .. :try_end_c3} :catchall_c1

    #@c3
    throw v5
.end method
