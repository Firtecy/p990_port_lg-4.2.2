.class Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$4;
.super Landroid/os/Handler;
.source "EmotionalLedService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1252
    iput-object p1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$4;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 8
    .parameter "msg"

    #@0
    .prologue
    const/4 v5, 0x2

    #@1
    const/4 v0, 0x1

    #@2
    .line 1255
    iget v2, p1, Landroid/os/Message;->what:I

    #@4
    packed-switch v2, :pswitch_data_5c

    #@7
    .line 1279
    :goto_7
    return-void

    #@8
    .line 1257
    :pswitch_8
    iget v2, p1, Landroid/os/Message;->arg1:I

    #@a
    if-ne v2, v0, :cond_47

    #@c
    .line 1258
    .local v0, repeat:Z
    :goto_c
    iget v1, p1, Landroid/os/Message;->arg2:I

    #@e
    .line 1259
    .local v1, whichLedPlay:I
    invoke-static {}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$300()Z

    #@11
    move-result v2

    #@12
    if-eqz v2, :cond_36

    #@14
    const-string v2, "EmotionalLed"

    #@16
    new-instance v3, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v4, "handleMessage() repeat = "

    #@1d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    const-string v4, ", whichLedPlay = "

    #@27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v3

    #@33
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 1260
    :cond_36
    if-nez v0, :cond_3f

    #@38
    if-eq v1, v5, :cond_3f

    #@3a
    .line 1261
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$4;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@3c
    invoke-static {v2}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$3900(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)V

    #@3f
    .line 1264
    :cond_3f
    if-ne v1, v5, :cond_49

    #@41
    .line 1265
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$4;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@43
    invoke-static {v2}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$4000(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)V

    #@46
    goto :goto_7

    #@47
    .line 1257
    .end local v0           #repeat:Z
    .end local v1           #whichLedPlay:I
    :cond_47
    const/4 v0, 0x0

    #@48
    goto :goto_c

    #@49
    .line 1267
    .restart local v0       #repeat:Z
    .restart local v1       #whichLedPlay:I
    :cond_49
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$4;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@4b
    invoke-static {v2}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$700(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)V

    #@4e
    goto :goto_7

    #@4f
    .line 1271
    .end local v0           #repeat:Z
    .end local v1           #whichLedPlay:I
    :pswitch_4f
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$4;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@51
    invoke-static {v2}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$900(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)V

    #@54
    goto :goto_7

    #@55
    .line 1274
    :pswitch_55
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$4;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@57
    invoke-static {v2}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$700(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)V

    #@5a
    goto :goto_7

    #@5b
    .line 1255
    nop

    #@5c
    :pswitch_data_5c
    .packed-switch 0x0
        :pswitch_8
        :pswitch_4f
        :pswitch_55
    .end packed-switch
.end method
