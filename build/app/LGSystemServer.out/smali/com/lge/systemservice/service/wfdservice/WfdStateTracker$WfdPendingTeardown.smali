.class final enum Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;
.super Ljava/lang/Enum;
.source "WfdStateTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "WfdPendingTeardown"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

.field public static final enum NONE:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

.field public static final enum WFD_DISABLE:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

.field public static final enum WFD_DISCONNECT:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 189
    new-instance v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

    #@5
    const-string v1, "NONE"

    #@7
    invoke-direct {v0, v1, v2}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;-><init>(Ljava/lang/String;I)V

    #@a
    sput-object v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;->NONE:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

    #@c
    .line 190
    new-instance v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

    #@e
    const-string v1, "WFD_DISABLE"

    #@10
    invoke-direct {v0, v1, v3}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;-><init>(Ljava/lang/String;I)V

    #@13
    sput-object v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;->WFD_DISABLE:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

    #@15
    .line 191
    new-instance v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

    #@17
    const-string v1, "WFD_DISCONNECT"

    #@19
    invoke-direct {v0, v1, v4}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;-><init>(Ljava/lang/String;I)V

    #@1c
    sput-object v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;->WFD_DISCONNECT:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

    #@1e
    .line 188
    const/4 v0, 0x3

    #@1f
    new-array v0, v0, [Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

    #@21
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;->NONE:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

    #@23
    aput-object v1, v0, v2

    #@25
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;->WFD_DISABLE:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

    #@27
    aput-object v1, v0, v3

    #@29
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;->WFD_DISCONNECT:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

    #@2b
    aput-object v1, v0, v4

    #@2d
    sput-object v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;->$VALUES:[Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

    #@2f
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 188
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;
    .registers 2
    .parameter

    #@0
    .prologue
    .line 188
    const-class v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;
    .registers 1

    #@0
    .prologue
    .line 188
    sget-object v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;->$VALUES:[Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

    #@2
    invoke-virtual {v0}, [Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

    #@8
    return-object v0
.end method
