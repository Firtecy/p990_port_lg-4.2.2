.class Lcom/lge/systemservice/service/emotionalled/DeviceLED;
.super Ljava/lang/Object;
.source "RGBDevice.java"

# interfaces
.implements Lcom/lge/systemservice/service/emotionalled/RGBDevice;


# static fields
.field private static final DEBUG:Z


# instance fields
.field private pls:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 24
    sget-object v0, Landroid/os/Build;->TYPE:Ljava/lang/String;

    #@2
    const-string v1, "user"

    #@4
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_e

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    sput-boolean v0, Lcom/lge/systemservice/service/emotionalled/DeviceLED;->DEBUG:Z

    #@d
    return-void

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_b
.end method

.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 21
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public interrupted()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 41
    sget-boolean v0, Lcom/lge/systemservice/service/emotionalled/DeviceLED;->DEBUG:Z

    #@3
    if-eqz v0, :cond_c

    #@5
    const-string v0, "RGBDevice"

    #@7
    const-string v1, "RGBDevice::interrupted()"

    #@9
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 42
    :cond_c
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/DeviceLED;->pls:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@e
    invoke-virtual {v0, v2, v2, v2}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->setLightLED(III)V

    #@11
    .line 43
    return-void
.end method

.method public onColorUpdate(III)V
    .registers 5
    .parameter "color"
    .parameter "onMS"
    .parameter "offMS"

    #@0
    .prologue
    .line 33
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/DeviceLED;->pls:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->setLightLED(III)V

    #@5
    .line 34
    return-void
.end method

.method public onDeviceInit(Ljava/lang/Object;Landroid/view/View;)V
    .registers 5
    .parameter "obj"
    .parameter "view"

    #@0
    .prologue
    .line 27
    sget-boolean v0, Lcom/lge/systemservice/service/emotionalled/DeviceLED;->DEBUG:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    const-string v0, "RGBDevice"

    #@6
    const-string v1, "RGBDevice::onDeviceInit()"

    #@8
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 28
    :cond_b
    check-cast p1, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@d
    .end local p1
    iput-object p1, p0, Lcom/lge/systemservice/service/emotionalled/DeviceLED;->pls:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@f
    .line 29
    return-void
.end method

.method public onFinish(I)V
    .registers 4
    .parameter "whichLedPlay"

    #@0
    .prologue
    .line 37
    sget-boolean v0, Lcom/lge/systemservice/service/emotionalled/DeviceLED;->DEBUG:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    const-string v0, "RGBDevice"

    #@6
    const-string v1, "RGBDevice::onFinish()"

    #@8
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 38
    :cond_b
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/DeviceLED;->pls:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@d
    invoke-virtual {v0, p1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->onFinishPattern(I)V

    #@10
    .line 39
    return-void
.end method
