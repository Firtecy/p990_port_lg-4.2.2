.class Lcom/lge/systemservice/service/SmartcoverObserver;
.super Landroid/os/UEventObserver;
.source "SmartCoverObserver.java"


# static fields
.field private static final LOG:Z

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private final mLock:Ljava/lang/Object;

.field private mPreviousState:I

.field private mSmartCoverStateBit:I

.field private mState:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 45
    const-class v0, Lcom/lge/systemservice/service/SmartcoverObserver;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/lge/systemservice/service/SmartcoverObserver;->TAG:Ljava/lang/String;

    #@8
    .line 46
    sget-object v0, Landroid/os/Build;->TYPE:Ljava/lang/String;

    #@a
    const-string v1, "eng"

    #@c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v0

    #@10
    sput-boolean v0, Lcom/lge/systemservice/service/SmartcoverObserver;->LOG:Z

    #@12
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 62
    invoke-direct {p0}, Landroid/os/UEventObserver;-><init>()V

    #@4
    .line 54
    new-instance v0, Ljava/lang/Object;

    #@6
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@9
    iput-object v0, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mLock:Ljava/lang/Object;

    #@b
    .line 56
    iput v1, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mState:I

    #@d
    .line 57
    iput v1, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mPreviousState:I

    #@f
    .line 58
    iput v1, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mSmartCoverStateBit:I

    #@11
    .line 223
    new-instance v0, Lcom/lge/systemservice/service/SmartcoverObserver$1;

    #@13
    invoke-direct {v0, p0}, Lcom/lge/systemservice/service/SmartcoverObserver$1;-><init>(Lcom/lge/systemservice/service/SmartcoverObserver;)V

    #@16
    iput-object v0, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mHandler:Landroid/os/Handler;

    #@18
    .line 63
    iput-object p1, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mContext:Landroid/content/Context;

    #@1a
    .line 64
    invoke-direct {p0}, Lcom/lge/systemservice/service/SmartcoverObserver;->init()V

    #@1d
    .line 81
    const-string v0, "DEVPATH=/devices/virtual/switch/smartcover"

    #@1f
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/service/SmartcoverObserver;->startObserving(Ljava/lang/String;)V

    #@22
    .line 84
    iget v0, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mSmartCoverStateBit:I

    #@24
    and-int/lit8 v0, v0, 0x1

    #@26
    if-nez v0, :cond_31

    #@28
    iget v0, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mSmartCoverStateBit:I

    #@2a
    and-int/lit8 v0, v0, 0x4

    #@2c
    if-nez v0, :cond_31

    #@2e
    .line 86
    invoke-direct {p0}, Lcom/lge/systemservice/service/SmartcoverObserver;->update()V

    #@31
    .line 88
    :cond_31
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 44
    sget-object v0, Lcom/lge/systemservice/service/SmartcoverObserver;->TAG:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/lge/systemservice/service/SmartcoverObserver;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 44
    iget v0, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mSmartCoverStateBit:I

    #@2
    return v0
.end method

.method static synthetic access$200(Lcom/lge/systemservice/service/SmartcoverObserver;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 44
    iget v0, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mState:I

    #@2
    return v0
.end method

.method static synthetic access$300(Lcom/lge/systemservice/service/SmartcoverObserver;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 44
    iget-object v0, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/lge/systemservice/service/SmartcoverObserver;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 44
    iget v0, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mPreviousState:I

    #@2
    return v0
.end method

.method private final init()V
    .registers 10

    #@0
    .prologue
    .line 113
    iget-object v6, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v6

    #@3
    .line 114
    const/16 v5, 0x400

    #@5
    :try_start_5
    new-array v0, v5, [C
    :try_end_7
    .catchall {:try_start_5 .. :try_end_7} :catchall_6e

    #@7
    .line 115
    .local v0, buffer:[C
    const/4 v2, 0x0

    #@8
    .line 118
    .local v2, file:Ljava/io/FileReader;
    :try_start_8
    new-instance v3, Ljava/io/FileReader;

    #@a
    const-string v5, "/sys/class/switch/smartcover/state"

    #@c
    invoke-direct {v3, v5}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_f
    .catchall {:try_start_8 .. :try_end_f} :catchall_88
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_f} :catch_57
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_f} :catch_71

    #@f
    .line 119
    .end local v2           #file:Ljava/io/FileReader;
    .local v3, file:Ljava/io/FileReader;
    const/4 v5, 0x0

    #@10
    const/16 v7, 0x400

    #@12
    :try_start_12
    invoke-virtual {v3, v0, v5, v7}, Ljava/io/FileReader;->read([CII)I

    #@15
    move-result v4

    #@16
    .line 120
    .local v4, len:I
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V

    #@19
    .line 121
    new-instance v5, Ljava/lang/String;

    #@1b
    const/4 v7, 0x0

    #@1c
    invoke-direct {v5, v0, v7, v4}, Ljava/lang/String;-><init>([CII)V

    #@1f
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@22
    move-result-object v5

    #@23
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    #@26
    move-result-object v5

    #@27
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    #@2a
    move-result v5

    #@2b
    iput v5, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mState:I

    #@2d
    iput v5, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mPreviousState:I

    #@2f
    .line 122
    iget v5, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mState:I

    #@31
    invoke-direct {p0, v5}, Lcom/lge/systemservice/service/SmartcoverObserver;->state2bitstate(I)V

    #@34
    .line 124
    const-string v5, "vu3"

    #@36
    const-string v7, "ro.product.device"

    #@38
    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@3b
    move-result-object v7

    #@3c
    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3f
    move-result v5

    #@40
    if-eqz v5, :cond_45

    #@42
    .line 125
    invoke-direct {p0}, Lcom/lge/systemservice/service/SmartcoverObserver;->init2()V
    :try_end_45
    .catchall {:try_start_12 .. :try_end_45} :catchall_98
    .catch Ljava/io/FileNotFoundException; {:try_start_12 .. :try_end_45} :catch_9e
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_45} :catch_9b

    #@45
    .line 133
    :cond_45
    if-eqz v3, :cond_4a

    #@47
    .line 134
    :try_start_47
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V
    :try_end_4a
    .catchall {:try_start_47 .. :try_end_4a} :catchall_6e
    .catch Ljava/io/IOException; {:try_start_47 .. :try_end_4a} :catch_4d

    #@4a
    :cond_4a
    move-object v2, v3

    #@4b
    .line 140
    .end local v3           #file:Ljava/io/FileReader;
    .end local v4           #len:I
    .restart local v2       #file:Ljava/io/FileReader;
    :cond_4b
    :goto_4b
    :try_start_4b
    monitor-exit v6

    #@4c
    .line 141
    return-void

    #@4d
    .line 136
    .end local v2           #file:Ljava/io/FileReader;
    .restart local v3       #file:Ljava/io/FileReader;
    .restart local v4       #len:I
    :catch_4d
    move-exception v1

    #@4e
    .line 137
    .local v1, e:Ljava/io/IOException;
    sget-object v5, Lcom/lge/systemservice/service/SmartcoverObserver;->TAG:Ljava/lang/String;

    #@50
    const-string v7, "IOException closing file"

    #@52
    invoke-static {v5, v7, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_55
    .catchall {:try_start_4b .. :try_end_55} :catchall_6e

    #@55
    move-object v2, v3

    #@56
    .line 139
    .end local v3           #file:Ljava/io/FileReader;
    .restart local v2       #file:Ljava/io/FileReader;
    goto :goto_4b

    #@57
    .line 127
    .end local v1           #e:Ljava/io/IOException;
    .end local v4           #len:I
    :catch_57
    move-exception v1

    #@58
    .line 128
    .local v1, e:Ljava/io/FileNotFoundException;
    :goto_58
    :try_start_58
    sget-object v5, Lcom/lge/systemservice/service/SmartcoverObserver;->TAG:Ljava/lang/String;

    #@5a
    const-string v7, "This kernel does not have smartcover support"

    #@5c
    invoke-static {v5, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5f
    .catchall {:try_start_58 .. :try_end_5f} :catchall_88

    #@5f
    .line 133
    if-eqz v2, :cond_4b

    #@61
    .line 134
    :try_start_61
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_64
    .catchall {:try_start_61 .. :try_end_64} :catchall_6e
    .catch Ljava/io/IOException; {:try_start_61 .. :try_end_64} :catch_65

    #@64
    goto :goto_4b

    #@65
    .line 136
    :catch_65
    move-exception v1

    #@66
    .line 137
    .local v1, e:Ljava/io/IOException;
    :try_start_66
    sget-object v5, Lcom/lge/systemservice/service/SmartcoverObserver;->TAG:Ljava/lang/String;

    #@68
    const-string v7, "IOException closing file"

    #@6a
    invoke-static {v5, v7, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@6d
    goto :goto_4b

    #@6e
    .line 140
    .end local v0           #buffer:[C
    .end local v1           #e:Ljava/io/IOException;
    .end local v2           #file:Ljava/io/FileReader;
    :catchall_6e
    move-exception v5

    #@6f
    monitor-exit v6
    :try_end_70
    .catchall {:try_start_66 .. :try_end_70} :catchall_6e

    #@70
    throw v5

    #@71
    .line 129
    .restart local v0       #buffer:[C
    .restart local v2       #file:Ljava/io/FileReader;
    :catch_71
    move-exception v1

    #@72
    .line 130
    .local v1, e:Ljava/lang/Exception;
    :goto_72
    :try_start_72
    sget-object v5, Lcom/lge/systemservice/service/SmartcoverObserver;->TAG:Ljava/lang/String;

    #@74
    const-string v7, ""

    #@76
    invoke-static {v5, v7, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_79
    .catchall {:try_start_72 .. :try_end_79} :catchall_88

    #@79
    .line 133
    if-eqz v2, :cond_4b

    #@7b
    .line 134
    :try_start_7b
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_7e
    .catchall {:try_start_7b .. :try_end_7e} :catchall_6e
    .catch Ljava/io/IOException; {:try_start_7b .. :try_end_7e} :catch_7f

    #@7e
    goto :goto_4b

    #@7f
    .line 136
    :catch_7f
    move-exception v1

    #@80
    .line 137
    .local v1, e:Ljava/io/IOException;
    :try_start_80
    sget-object v5, Lcom/lge/systemservice/service/SmartcoverObserver;->TAG:Ljava/lang/String;

    #@82
    const-string v7, "IOException closing file"

    #@84
    invoke-static {v5, v7, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_87
    .catchall {:try_start_80 .. :try_end_87} :catchall_6e

    #@87
    goto :goto_4b

    #@88
    .line 132
    .end local v1           #e:Ljava/io/IOException;
    :catchall_88
    move-exception v5

    #@89
    .line 133
    :goto_89
    if-eqz v2, :cond_8e

    #@8b
    .line 134
    :try_start_8b
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_8e
    .catchall {:try_start_8b .. :try_end_8e} :catchall_6e
    .catch Ljava/io/IOException; {:try_start_8b .. :try_end_8e} :catch_8f

    #@8e
    .line 138
    :cond_8e
    :goto_8e
    :try_start_8e
    throw v5

    #@8f
    .line 136
    :catch_8f
    move-exception v1

    #@90
    .line 137
    .restart local v1       #e:Ljava/io/IOException;
    sget-object v7, Lcom/lge/systemservice/service/SmartcoverObserver;->TAG:Ljava/lang/String;

    #@92
    const-string v8, "IOException closing file"

    #@94
    invoke-static {v7, v8, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_97
    .catchall {:try_start_8e .. :try_end_97} :catchall_6e

    #@97
    goto :goto_8e

    #@98
    .line 132
    .end local v1           #e:Ljava/io/IOException;
    .end local v2           #file:Ljava/io/FileReader;
    .restart local v3       #file:Ljava/io/FileReader;
    :catchall_98
    move-exception v5

    #@99
    move-object v2, v3

    #@9a
    .end local v3           #file:Ljava/io/FileReader;
    .restart local v2       #file:Ljava/io/FileReader;
    goto :goto_89

    #@9b
    .line 129
    .end local v2           #file:Ljava/io/FileReader;
    .restart local v3       #file:Ljava/io/FileReader;
    :catch_9b
    move-exception v1

    #@9c
    move-object v2, v3

    #@9d
    .end local v3           #file:Ljava/io/FileReader;
    .restart local v2       #file:Ljava/io/FileReader;
    goto :goto_72

    #@9e
    .line 127
    .end local v2           #file:Ljava/io/FileReader;
    .restart local v3       #file:Ljava/io/FileReader;
    :catch_9e
    move-exception v1

    #@9f
    move-object v2, v3

    #@a0
    .end local v3           #file:Ljava/io/FileReader;
    .restart local v2       #file:Ljava/io/FileReader;
    goto :goto_58
.end method

.method private final init2()V
    .registers 4

    #@0
    .prologue
    .line 144
    iget-object v2, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 145
    const/4 v0, 0x0

    #@4
    .line 146
    .local v0, detailState:Ljava/lang/String;
    :try_start_4
    const-string v1, "/sys/devices/hall-bu52031nvx.74/pouch"

    #@6
    invoke-direct {p0, v1}, Lcom/lge/systemservice/service/SmartcoverObserver;->readSmartCoverDetail(Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    .line 147
    if-eqz v0, :cond_1e

    #@c
    .line 148
    const-string v1, "1"

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_3a

    #@14
    const/4 v1, 0x5

    #@15
    :goto_15
    iput v1, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mState:I

    #@17
    iput v1, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mPreviousState:I

    #@19
    .line 149
    iget v1, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mState:I

    #@1b
    invoke-direct {p0, v1}, Lcom/lge/systemservice/service/SmartcoverObserver;->state2bitstate(I)V

    #@1e
    .line 152
    :cond_1e
    const-string v1, "/sys/devices/hall-bu52031nvx.74/pen"

    #@20
    invoke-direct {p0, v1}, Lcom/lge/systemservice/service/SmartcoverObserver;->readSmartCoverDetail(Ljava/lang/String;)Ljava/lang/String;

    #@23
    move-result-object v0

    #@24
    .line 153
    if-eqz v0, :cond_38

    #@26
    .line 154
    const-string v1, "1"

    #@28
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2b
    move-result v1

    #@2c
    if-eqz v1, :cond_3c

    #@2e
    const/4 v1, 0x3

    #@2f
    :goto_2f
    iput v1, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mState:I

    #@31
    iput v1, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mPreviousState:I

    #@33
    .line 155
    iget v1, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mState:I

    #@35
    invoke-direct {p0, v1}, Lcom/lge/systemservice/service/SmartcoverObserver;->state2bitstate(I)V

    #@38
    .line 157
    :cond_38
    monitor-exit v2

    #@39
    .line 158
    return-void

    #@3a
    .line 148
    :cond_3a
    const/4 v1, 0x6

    #@3b
    goto :goto_15

    #@3c
    .line 154
    :cond_3c
    const/4 v1, 0x4

    #@3d
    goto :goto_2f

    #@3e
    .line 157
    :catchall_3e
    move-exception v1

    #@3f
    monitor-exit v2
    :try_end_40
    .catchall {:try_start_4 .. :try_end_40} :catchall_3e

    #@40
    throw v1
.end method

.method private readSmartCoverDetail(Ljava/lang/String;)Ljava/lang/String;
    .registers 16
    .parameter "path"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 161
    iget-object v10, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mLock:Ljava/lang/Object;

    #@3
    monitor-enter v10

    #@4
    .line 162
    const/16 v11, 0x400

    #@6
    :try_start_6
    new-array v0, v11, [C
    :try_end_8
    .catchall {:try_start_6 .. :try_end_8} :catchall_55

    #@8
    .line 163
    .local v0, buffer:[C
    const/4 v2, 0x0

    #@9
    .line 164
    .local v2, file:Ljava/io/File;
    const/4 v4, 0x0

    #@a
    .line 165
    .local v4, fileReader:Ljava/io/FileReader;
    const/4 v6, 0x0

    #@b
    .line 166
    .local v6, len:I
    const/4 v7, 0x0

    #@c
    .line 167
    .local v7, rawStr:Ljava/lang/String;
    const/4 v8, 0x0

    #@d
    .line 170
    .local v8, splitStr:[Ljava/lang/String;
    :try_start_d
    new-instance v3, Ljava/io/File;

    #@f
    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_12
    .catchall {:try_start_d .. :try_end_12} :catchall_b0
    .catch Ljava/io/FileNotFoundException; {:try_start_d .. :try_end_12} :catch_81
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_12} :catch_99

    #@12
    .line 171
    .end local v2           #file:Ljava/io/File;
    .local v3, file:Ljava/io/File;
    :try_start_12
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    #@15
    move-result v11

    #@16
    if-eqz v11, :cond_58

    #@18
    .line 172
    new-instance v5, Ljava/io/FileReader;

    #@1a
    invoke-direct {v5, v3}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V
    :try_end_1d
    .catchall {:try_start_12 .. :try_end_1d} :catchall_c0
    .catch Ljava/io/FileNotFoundException; {:try_start_12 .. :try_end_1d} :catch_ce
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_1d} :catch_c7

    #@1d
    .line 173
    .end local v4           #fileReader:Ljava/io/FileReader;
    .local v5, fileReader:Ljava/io/FileReader;
    const/4 v11, 0x0

    #@1e
    const/16 v12, 0x400

    #@20
    :try_start_20
    invoke-virtual {v5, v0, v11, v12}, Ljava/io/FileReader;->read([CII)I

    #@23
    move-result v6

    #@24
    .line 174
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    #@27
    .line 175
    new-instance v11, Ljava/lang/String;

    #@29
    const/4 v12, 0x0

    #@2a
    invoke-direct {v11, v0, v12, v6}, Ljava/lang/String;-><init>([CII)V

    #@2d
    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@30
    move-result-object v7

    #@31
    .line 176
    sget-object v11, Lcom/lge/systemservice/service/SmartcoverObserver;->TAG:Ljava/lang/String;

    #@33
    invoke-static {v11, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 177
    const-string v11, ":"

    #@38
    invoke-virtual {v7, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@3b
    move-result-object v8

    #@3c
    .line 178
    const/4 v11, 0x1

    #@3d
    aget-object v11, v8, v11

    #@3f
    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_42
    .catchall {:try_start_20 .. :try_end_42} :catchall_c3
    .catch Ljava/io/FileNotFoundException; {:try_start_20 .. :try_end_42} :catch_d1
    .catch Ljava/lang/Exception; {:try_start_20 .. :try_end_42} :catch_ca

    #@42
    move-result-object v9

    #@43
    .line 189
    if-eqz v5, :cond_48

    #@45
    .line 190
    :try_start_45
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V
    :try_end_48
    .catchall {:try_start_45 .. :try_end_48} :catchall_55
    .catch Ljava/io/IOException; {:try_start_45 .. :try_end_48} :catch_4c

    #@48
    .line 194
    :cond_48
    :goto_48
    :try_start_48
    monitor-exit v10

    #@49
    move-object v4, v5

    #@4a
    .end local v5           #fileReader:Ljava/io/FileReader;
    .restart local v4       #fileReader:Ljava/io/FileReader;
    move-object v2, v3

    #@4b
    .line 197
    .end local v3           #file:Ljava/io/File;
    .restart local v2       #file:Ljava/io/File;
    :goto_4b
    return-object v9

    #@4c
    .line 192
    .end local v2           #file:Ljava/io/File;
    .end local v4           #fileReader:Ljava/io/FileReader;
    .restart local v3       #file:Ljava/io/File;
    .restart local v5       #fileReader:Ljava/io/FileReader;
    :catch_4c
    move-exception v1

    #@4d
    .line 193
    .local v1, e:Ljava/io/IOException;
    sget-object v11, Lcom/lge/systemservice/service/SmartcoverObserver;->TAG:Ljava/lang/String;

    #@4f
    const-string v12, "IOException closing file"

    #@51
    invoke-static {v11, v12, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@54
    goto :goto_48

    #@55
    .line 196
    .end local v0           #buffer:[C
    .end local v1           #e:Ljava/io/IOException;
    .end local v3           #file:Ljava/io/File;
    .end local v5           #fileReader:Ljava/io/FileReader;
    .end local v6           #len:I
    .end local v7           #rawStr:Ljava/lang/String;
    .end local v8           #splitStr:[Ljava/lang/String;
    :catchall_55
    move-exception v9

    #@56
    monitor-exit v10
    :try_end_57
    .catchall {:try_start_48 .. :try_end_57} :catchall_55

    #@57
    throw v9

    #@58
    .line 180
    .restart local v0       #buffer:[C
    .restart local v3       #file:Ljava/io/File;
    .restart local v4       #fileReader:Ljava/io/FileReader;
    .restart local v6       #len:I
    .restart local v7       #rawStr:Ljava/lang/String;
    .restart local v8       #splitStr:[Ljava/lang/String;
    :cond_58
    :try_start_58
    sget-object v11, Lcom/lge/systemservice/service/SmartcoverObserver;->TAG:Ljava/lang/String;

    #@5a
    new-instance v12, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    invoke-virtual {v12, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v12

    #@63
    const-string v13, "is not exist."

    #@65
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v12

    #@69
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v12

    #@6d
    invoke-static {v11, v12}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_70
    .catchall {:try_start_58 .. :try_end_70} :catchall_c0
    .catch Ljava/io/FileNotFoundException; {:try_start_58 .. :try_end_70} :catch_ce
    .catch Ljava/lang/Exception; {:try_start_58 .. :try_end_70} :catch_c7

    #@70
    .line 189
    if-eqz v4, :cond_75

    #@72
    .line 190
    :try_start_72
    #Replaced unresolvable odex instruction with a throw
    throw v4
    #invoke-virtual-quick {v4}, vtable@0xb
    :try_end_75
    .catchall {:try_start_72 .. :try_end_75} :catchall_55
    .catch Ljava/io/IOException; {:try_start_72 .. :try_end_75} :catch_78

    #@75
    .line 194
    :cond_75
    :goto_75
    :try_start_75
    monitor-exit v10

    #@76
    move-object v2, v3

    #@77
    .end local v3           #file:Ljava/io/File;
    .restart local v2       #file:Ljava/io/File;
    goto :goto_4b

    #@78
    .line 192
    .end local v2           #file:Ljava/io/File;
    .restart local v3       #file:Ljava/io/File;
    :catch_78
    move-exception v1

    #@79
    .line 193
    .restart local v1       #e:Ljava/io/IOException;
    sget-object v11, Lcom/lge/systemservice/service/SmartcoverObserver;->TAG:Ljava/lang/String;

    #@7b
    const-string v12, "IOException closing file"

    #@7d
    invoke-static {v11, v12, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_80
    .catchall {:try_start_75 .. :try_end_80} :catchall_55

    #@80
    goto :goto_75

    #@81
    .line 183
    .end local v1           #e:Ljava/io/IOException;
    .end local v3           #file:Ljava/io/File;
    .restart local v2       #file:Ljava/io/File;
    :catch_81
    move-exception v1

    #@82
    .line 184
    .local v1, e:Ljava/io/FileNotFoundException;
    :goto_82
    :try_start_82
    sget-object v11, Lcom/lge/systemservice/service/SmartcoverObserver;->TAG:Ljava/lang/String;

    #@84
    const-string v12, "This kernel does not have smartcover\'s detail support"

    #@86
    invoke-static {v11, v12}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_89
    .catchall {:try_start_82 .. :try_end_89} :catchall_b0

    #@89
    .line 189
    if-eqz v4, :cond_8e

    #@8b
    .line 190
    :try_start_8b
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V
    :try_end_8e
    .catchall {:try_start_8b .. :try_end_8e} :catchall_55
    .catch Ljava/io/IOException; {:try_start_8b .. :try_end_8e} :catch_90

    #@8e
    .line 196
    .end local v1           #e:Ljava/io/FileNotFoundException;
    :cond_8e
    :goto_8e
    :try_start_8e
    monitor-exit v10

    #@8f
    goto :goto_4b

    #@90
    .line 192
    .restart local v1       #e:Ljava/io/FileNotFoundException;
    :catch_90
    move-exception v1

    #@91
    .line 193
    .local v1, e:Ljava/io/IOException;
    sget-object v11, Lcom/lge/systemservice/service/SmartcoverObserver;->TAG:Ljava/lang/String;

    #@93
    const-string v12, "IOException closing file"

    #@95
    invoke-static {v11, v12, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_98
    .catchall {:try_start_8e .. :try_end_98} :catchall_55

    #@98
    goto :goto_8e

    #@99
    .line 185
    .end local v1           #e:Ljava/io/IOException;
    :catch_99
    move-exception v1

    #@9a
    .line 186
    .local v1, e:Ljava/lang/Exception;
    :goto_9a
    :try_start_9a
    sget-object v11, Lcom/lge/systemservice/service/SmartcoverObserver;->TAG:Ljava/lang/String;

    #@9c
    const-string v12, ""

    #@9e
    invoke-static {v11, v12, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_a1
    .catchall {:try_start_9a .. :try_end_a1} :catchall_b0

    #@a1
    .line 189
    if-eqz v4, :cond_8e

    #@a3
    .line 190
    :try_start_a3
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V
    :try_end_a6
    .catchall {:try_start_a3 .. :try_end_a6} :catchall_55
    .catch Ljava/io/IOException; {:try_start_a3 .. :try_end_a6} :catch_a7

    #@a6
    goto :goto_8e

    #@a7
    .line 192
    :catch_a7
    move-exception v1

    #@a8
    .line 193
    .local v1, e:Ljava/io/IOException;
    :try_start_a8
    sget-object v11, Lcom/lge/systemservice/service/SmartcoverObserver;->TAG:Ljava/lang/String;

    #@aa
    const-string v12, "IOException closing file"

    #@ac
    invoke-static {v11, v12, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_af
    .catchall {:try_start_a8 .. :try_end_af} :catchall_55

    #@af
    goto :goto_8e

    #@b0
    .line 188
    .end local v1           #e:Ljava/io/IOException;
    :catchall_b0
    move-exception v9

    #@b1
    .line 189
    :goto_b1
    if-eqz v4, :cond_b6

    #@b3
    .line 190
    :try_start_b3
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V
    :try_end_b6
    .catchall {:try_start_b3 .. :try_end_b6} :catchall_55
    .catch Ljava/io/IOException; {:try_start_b3 .. :try_end_b6} :catch_b7

    #@b6
    .line 194
    :cond_b6
    :goto_b6
    :try_start_b6
    throw v9

    #@b7
    .line 192
    :catch_b7
    move-exception v1

    #@b8
    .line 193
    .restart local v1       #e:Ljava/io/IOException;
    sget-object v11, Lcom/lge/systemservice/service/SmartcoverObserver;->TAG:Ljava/lang/String;

    #@ba
    const-string v12, "IOException closing file"

    #@bc
    invoke-static {v11, v12, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_bf
    .catchall {:try_start_b6 .. :try_end_bf} :catchall_55

    #@bf
    goto :goto_b6

    #@c0
    .line 188
    .end local v1           #e:Ljava/io/IOException;
    .end local v2           #file:Ljava/io/File;
    .restart local v3       #file:Ljava/io/File;
    :catchall_c0
    move-exception v9

    #@c1
    move-object v2, v3

    #@c2
    .end local v3           #file:Ljava/io/File;
    .restart local v2       #file:Ljava/io/File;
    goto :goto_b1

    #@c3
    .end local v2           #file:Ljava/io/File;
    .end local v4           #fileReader:Ljava/io/FileReader;
    .restart local v3       #file:Ljava/io/File;
    .restart local v5       #fileReader:Ljava/io/FileReader;
    :catchall_c3
    move-exception v9

    #@c4
    move-object v4, v5

    #@c5
    .end local v5           #fileReader:Ljava/io/FileReader;
    .restart local v4       #fileReader:Ljava/io/FileReader;
    move-object v2, v3

    #@c6
    .end local v3           #file:Ljava/io/File;
    .restart local v2       #file:Ljava/io/File;
    goto :goto_b1

    #@c7
    .line 185
    .end local v2           #file:Ljava/io/File;
    .restart local v3       #file:Ljava/io/File;
    :catch_c7
    move-exception v1

    #@c8
    move-object v2, v3

    #@c9
    .end local v3           #file:Ljava/io/File;
    .restart local v2       #file:Ljava/io/File;
    goto :goto_9a

    #@ca
    .end local v2           #file:Ljava/io/File;
    .end local v4           #fileReader:Ljava/io/FileReader;
    .restart local v3       #file:Ljava/io/File;
    .restart local v5       #fileReader:Ljava/io/FileReader;
    :catch_ca
    move-exception v1

    #@cb
    move-object v4, v5

    #@cc
    .end local v5           #fileReader:Ljava/io/FileReader;
    .restart local v4       #fileReader:Ljava/io/FileReader;
    move-object v2, v3

    #@cd
    .end local v3           #file:Ljava/io/File;
    .restart local v2       #file:Ljava/io/File;
    goto :goto_9a

    #@ce
    .line 183
    .end local v2           #file:Ljava/io/File;
    .restart local v3       #file:Ljava/io/File;
    :catch_ce
    move-exception v1

    #@cf
    move-object v2, v3

    #@d0
    .end local v3           #file:Ljava/io/File;
    .restart local v2       #file:Ljava/io/File;
    goto :goto_82

    #@d1
    .end local v2           #file:Ljava/io/File;
    .end local v4           #fileReader:Ljava/io/FileReader;
    .restart local v3       #file:Ljava/io/File;
    .restart local v5       #fileReader:Ljava/io/FileReader;
    :catch_d1
    move-exception v1

    #@d2
    move-object v4, v5

    #@d3
    .end local v5           #fileReader:Ljava/io/FileReader;
    .restart local v4       #fileReader:Ljava/io/FileReader;
    move-object v2, v3

    #@d4
    .end local v3           #file:Ljava/io/File;
    .restart local v2       #file:Ljava/io/File;
    goto :goto_82
.end method

.method private final state2bitstate(I)V
    .registers 3
    .parameter "current"

    #@0
    .prologue
    .line 202
    const/4 v0, 0x1

    #@1
    if-ne p1, v0, :cond_a

    #@3
    .line 203
    iget v0, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mSmartCoverStateBit:I

    #@5
    and-int/lit8 v0, v0, -0x2

    #@7
    iput v0, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mSmartCoverStateBit:I

    #@9
    .line 217
    :goto_9
    return-void

    #@a
    .line 204
    :cond_a
    const/4 v0, 0x2

    #@b
    if-ne p1, v0, :cond_14

    #@d
    .line 205
    iget v0, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mSmartCoverStateBit:I

    #@f
    or-int/lit8 v0, v0, 0x1

    #@11
    iput v0, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mSmartCoverStateBit:I

    #@13
    goto :goto_9

    #@14
    .line 206
    :cond_14
    const/4 v0, 0x3

    #@15
    if-ne p1, v0, :cond_1e

    #@17
    .line 207
    iget v0, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mSmartCoverStateBit:I

    #@19
    and-int/lit8 v0, v0, -0x3

    #@1b
    iput v0, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mSmartCoverStateBit:I

    #@1d
    goto :goto_9

    #@1e
    .line 208
    :cond_1e
    const/4 v0, 0x4

    #@1f
    if-ne p1, v0, :cond_28

    #@21
    .line 209
    iget v0, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mSmartCoverStateBit:I

    #@23
    or-int/lit8 v0, v0, 0x2

    #@25
    iput v0, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mSmartCoverStateBit:I

    #@27
    goto :goto_9

    #@28
    .line 210
    :cond_28
    const/4 v0, 0x5

    #@29
    if-ne p1, v0, :cond_32

    #@2b
    .line 211
    iget v0, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mSmartCoverStateBit:I

    #@2d
    and-int/lit8 v0, v0, -0x5

    #@2f
    iput v0, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mSmartCoverStateBit:I

    #@31
    goto :goto_9

    #@32
    .line 212
    :cond_32
    const/4 v0, 0x6

    #@33
    if-ne p1, v0, :cond_3c

    #@35
    .line 213
    iget v0, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mSmartCoverStateBit:I

    #@37
    or-int/lit8 v0, v0, 0x4

    #@39
    iput v0, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mSmartCoverStateBit:I

    #@3b
    goto :goto_9

    #@3c
    .line 215
    :cond_3c
    const/4 v0, 0x0

    #@3d
    iput v0, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mSmartCoverStateBit:I

    #@3f
    goto :goto_9
.end method

.method private final update()V
    .registers 3

    #@0
    .prologue
    .line 220
    iget-object v0, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mHandler:Landroid/os/Handler;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@6
    .line 221
    return-void
.end method


# virtual methods
.method public onUEvent(Landroid/os/UEventObserver$UEvent;)V
    .registers 8
    .parameter "event"

    #@0
    .prologue
    .line 92
    sget-object v2, Lcom/lge/systemservice/service/SmartcoverObserver;->TAG:Ljava/lang/String;

    #@2
    const/4 v3, 0x2

    #@3
    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v2

    #@7
    if-eqz v2, :cond_25

    #@9
    .line 93
    sget-object v2, Lcom/lge/systemservice/service/SmartcoverObserver;->TAG:Ljava/lang/String;

    #@b
    new-instance v3, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v4, "smartcover UEVENT: "

    #@12
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    invoke-virtual {p1}, Landroid/os/UEventObserver$UEvent;->toString()Ljava/lang/String;

    #@19
    move-result-object v4

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v3

    #@22
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 96
    :cond_25
    iget-object v3, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mLock:Ljava/lang/Object;

    #@27
    monitor-enter v3

    #@28
    .line 98
    :try_start_28
    const-string v2, "SWITCH_STATE"

    #@2a
    invoke-virtual {p1, v2}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@31
    move-result v1

    #@32
    .line 99
    .local v1, newState:I
    iget v2, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mState:I

    #@34
    if-eq v1, v2, :cond_6a

    #@36
    .line 100
    iget v2, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mState:I

    #@38
    iput v2, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mPreviousState:I

    #@3a
    .line 101
    iput v1, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mState:I

    #@3c
    .line 102
    iget v2, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mState:I

    #@3e
    invoke-direct {p0, v2}, Lcom/lge/systemservice/service/SmartcoverObserver;->state2bitstate(I)V

    #@41
    .line 103
    sget-object v2, Lcom/lge/systemservice/service/SmartcoverObserver;->TAG:Ljava/lang/String;

    #@43
    new-instance v4, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v5, "smartcover state is changed from "

    #@4a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v4

    #@4e
    iget v5, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mPreviousState:I

    #@50
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@53
    move-result-object v4

    #@54
    const-string v5, " to "

    #@56
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v4

    #@5a
    iget v5, p0, Lcom/lge/systemservice/service/SmartcoverObserver;->mState:I

    #@5c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v4

    #@60
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v4

    #@64
    invoke-static {v2, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@67
    .line 104
    invoke-direct {p0}, Lcom/lge/systemservice/service/SmartcoverObserver;->update()V
    :try_end_6a
    .catchall {:try_start_28 .. :try_end_6a} :catchall_86
    .catch Ljava/lang/NumberFormatException; {:try_start_28 .. :try_end_6a} :catch_6c

    #@6a
    .line 109
    .end local v1           #newState:I
    :cond_6a
    :goto_6a
    :try_start_6a
    monitor-exit v3

    #@6b
    .line 110
    return-void

    #@6c
    .line 106
    :catch_6c
    move-exception v0

    #@6d
    .line 107
    .local v0, e:Ljava/lang/NumberFormatException;
    sget-object v2, Lcom/lge/systemservice/service/SmartcoverObserver;->TAG:Ljava/lang/String;

    #@6f
    new-instance v4, Ljava/lang/StringBuilder;

    #@71
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@74
    const-string v5, "Could not parse switch state from event "

    #@76
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v4

    #@7a
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v4

    #@7e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@81
    move-result-object v4

    #@82
    invoke-static {v2, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@85
    goto :goto_6a

    #@86
    .line 109
    .end local v0           #e:Ljava/lang/NumberFormatException;
    :catchall_86
    move-exception v2

    #@87
    monitor-exit v3
    :try_end_88
    .catchall {:try_start_6a .. :try_end_88} :catchall_86

    #@88
    throw v2
.end method
