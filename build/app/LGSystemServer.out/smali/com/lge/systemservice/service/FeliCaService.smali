.class public Lcom/lge/systemservice/service/FeliCaService;
.super Lcom/lge/systemservice/core/felicamanager/IFeliCaManager$Stub;
.source "FeliCaService.java"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 19
    invoke-direct {p0}, Lcom/lge/systemservice/core/felicamanager/IFeliCaManager$Stub;-><init>()V

    #@3
    .line 15
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Lcom/lge/systemservice/service/FeliCaService;->mContext:Landroid/content/Context;

    #@6
    .line 20
    iput-object p1, p0, Lcom/lge/systemservice/service/FeliCaService;->mContext:Landroid/content/Context;

    #@8
    .line 22
    const-string v0, "FeliCaService"

    #@a
    const-string v1, "FeliCaService start"

    #@c
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 23
    return-void
.end method


# virtual methods
.method public cmdEXTIDM([B)Z
    .registers 5
    .parameter "idm"

    #@0
    .prologue
    .line 28
    const-string v1, "FeliCaService"

    #@2
    const-string v2, "cmdEXTIDM"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 30
    new-instance v0, Lcom/lge/systemservice/service/FeliCaDevice;

    #@9
    const/4 v1, 0x0

    #@a
    const-string v2, "FeliCa"

    #@c
    invoke-direct {v0, v1, v2}, Lcom/lge/systemservice/service/FeliCaDevice;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    #@f
    .line 32
    .local v0, felicaDevice:Lcom/lge/systemservice/service/FeliCaDevice;
    invoke-virtual {v0, p1}, Lcom/lge/systemservice/service/FeliCaDevice;->cmdEXTIDM([B)Z

    #@12
    move-result v1

    #@13
    return v1
.end method

.method public cmdFreqCalRange([Ljava/lang/String;)Z
    .registers 8
    .parameter "response"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 92
    const-string v4, "FeliCaService"

    #@3
    const-string v5, "cmdFreqCalRange"

    #@5
    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 95
    new-instance v0, Lcom/lge/systemservice/service/FeliCaDevice$FeliCaCmdResult;

    #@a
    invoke-direct {v0}, Lcom/lge/systemservice/service/FeliCaDevice$FeliCaCmdResult;-><init>()V

    #@d
    .line 96
    .local v0, cmdResult:Lcom/lge/systemservice/service/FeliCaDevice$FeliCaCmdResult;
    new-instance v1, Lcom/lge/systemservice/service/FeliCaDevice;

    #@f
    const/4 v4, 0x0

    #@10
    const-string v5, "FeliCa"

    #@12
    invoke-direct {v1, v4, v5}, Lcom/lge/systemservice/service/FeliCaDevice;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    #@15
    .line 98
    .local v1, felicaDevice:Lcom/lge/systemservice/service/FeliCaDevice;
    array-length v4, p1

    #@16
    if-lez v4, :cond_21

    #@18
    .line 100
    invoke-virtual {v1, v0}, Lcom/lge/systemservice/service/FeliCaDevice;->cmdFreqCalRange(Lcom/lge/systemservice/service/FeliCaDevice$FeliCaCmdResult;)Z

    #@1b
    move-result v2

    #@1c
    .line 101
    .local v2, result:Z
    iget-object v4, v0, Lcom/lge/systemservice/service/FeliCaDevice$FeliCaCmdResult;->result:Ljava/lang/String;

    #@1e
    aput-object v4, p1, v3

    #@20
    .line 107
    .end local v2           #result:Z
    :goto_20
    return v2

    #@21
    :cond_21
    move v2, v3

    #@22
    goto :goto_20
.end method

.method public cmdFreqCalRead([Ljava/lang/String;)Z
    .registers 8
    .parameter "response"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 71
    const-string v4, "FeliCaService"

    #@3
    const-string v5, "cmdFreqCalRead"

    #@5
    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 74
    new-instance v0, Lcom/lge/systemservice/service/FeliCaDevice$FeliCaCmdResult;

    #@a
    invoke-direct {v0}, Lcom/lge/systemservice/service/FeliCaDevice$FeliCaCmdResult;-><init>()V

    #@d
    .line 75
    .local v0, cmdResult:Lcom/lge/systemservice/service/FeliCaDevice$FeliCaCmdResult;
    new-instance v1, Lcom/lge/systemservice/service/FeliCaDevice;

    #@f
    const/4 v4, 0x0

    #@10
    const-string v5, "FeliCa"

    #@12
    invoke-direct {v1, v4, v5}, Lcom/lge/systemservice/service/FeliCaDevice;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    #@15
    .line 77
    .local v1, felicaDevice:Lcom/lge/systemservice/service/FeliCaDevice;
    array-length v4, p1

    #@16
    if-lez v4, :cond_21

    #@18
    .line 79
    invoke-virtual {v1, v0}, Lcom/lge/systemservice/service/FeliCaDevice;->cmdFreqCalRead(Lcom/lge/systemservice/service/FeliCaDevice$FeliCaCmdResult;)Z

    #@1b
    move-result v2

    #@1c
    .line 80
    .local v2, result:Z
    iget-object v4, v0, Lcom/lge/systemservice/service/FeliCaDevice$FeliCaCmdResult;->result:Ljava/lang/String;

    #@1e
    aput-object v4, p1, v3

    #@20
    .line 86
    .end local v2           #result:Z
    :goto_20
    return v2

    #@21
    :cond_21
    move v2, v3

    #@22
    goto :goto_20
.end method

.method public cmdFreqCalWrite(F)Z
    .registers 6
    .parameter "freq"

    #@0
    .prologue
    .line 58
    const-string v2, "FeliCaService"

    #@2
    const-string v3, "cmdFreqCalWrite"

    #@4
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 61
    new-instance v0, Lcom/lge/systemservice/service/FeliCaDevice$FeliCaCmdResult;

    #@9
    invoke-direct {v0}, Lcom/lge/systemservice/service/FeliCaDevice$FeliCaCmdResult;-><init>()V

    #@c
    .line 62
    .local v0, cmdResult:Lcom/lge/systemservice/service/FeliCaDevice$FeliCaCmdResult;
    new-instance v1, Lcom/lge/systemservice/service/FeliCaDevice;

    #@e
    const/4 v2, 0x0

    #@f
    const-string v3, "FeliCa"

    #@11
    invoke-direct {v1, v2, v3}, Lcom/lge/systemservice/service/FeliCaDevice;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    #@14
    .line 65
    .local v1, felicaDevice:Lcom/lge/systemservice/service/FeliCaDevice;
    invoke-virtual {v1, p1}, Lcom/lge/systemservice/service/FeliCaDevice;->cmdFreqCalWrite(F)Z

    #@17
    move-result v2

    #@18
    return v2
.end method

.method public cmdIDM([Ljava/lang/String;)Z
    .registers 5
    .parameter "response"

    #@0
    .prologue
    .line 38
    const-string v1, "FeliCaService"

    #@2
    const-string v2, "cmdIDM"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 40
    new-instance v0, Lcom/lge/systemservice/service/FeliCaDevice;

    #@9
    const/4 v1, 0x0

    #@a
    const-string v2, "FeliCa"

    #@c
    invoke-direct {v0, v1, v2}, Lcom/lge/systemservice/service/FeliCaDevice;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    #@f
    .line 42
    .local v0, felicaDevice:Lcom/lge/systemservice/service/FeliCaDevice;
    invoke-virtual {v0, p1}, Lcom/lge/systemservice/service/FeliCaDevice;->cmdIDM([Ljava/lang/String;)Z

    #@12
    move-result v1

    #@13
    return v1
.end method

.method public cmdRFIDCK()I
    .registers 4

    #@0
    .prologue
    .line 48
    const-string v1, "FeliCaService"

    #@2
    const-string v2, "cmdRFIDCK"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 50
    new-instance v0, Lcom/lge/systemservice/service/FeliCaDevice;

    #@9
    const/4 v1, 0x0

    #@a
    const-string v2, "FeliCa"

    #@c
    invoke-direct {v0, v1, v2}, Lcom/lge/systemservice/service/FeliCaDevice;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    #@f
    .line 52
    .local v0, felicaDevice:Lcom/lge/systemservice/service/FeliCaDevice;
    invoke-virtual {v0}, Lcom/lge/systemservice/service/FeliCaDevice;->cmdRFIDCK()I

    #@12
    move-result v1

    #@13
    return v1
.end method

.method public cmdRFRegCalCheck()Z
    .registers 4

    #@0
    .prologue
    .line 179
    const-string v1, "FeliCaService"

    #@2
    const-string v2, "cmdRFRegCalCheck"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 181
    new-instance v0, Lcom/lge/systemservice/service/FeliCaDevice;

    #@9
    const/4 v1, 0x0

    #@a
    const-string v2, "FeliCa"

    #@c
    invoke-direct {v0, v1, v2}, Lcom/lge/systemservice/service/FeliCaDevice;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    #@f
    .line 183
    .local v0, felicaDevice:Lcom/lge/systemservice/service/FeliCaDevice;
    invoke-virtual {v0}, Lcom/lge/systemservice/service/FeliCaDevice;->cmdRFRegCalCheck()Z

    #@12
    move-result v1

    #@13
    return v1
.end method

.method public cmdRFRegCalLoad()Z
    .registers 4

    #@0
    .prologue
    .line 170
    const-string v1, "FeliCaService"

    #@2
    const-string v2, "cmdRFRegCalLoad"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 172
    new-instance v0, Lcom/lge/systemservice/service/FeliCaDevice;

    #@9
    const/4 v1, 0x0

    #@a
    const-string v2, "FeliCa"

    #@c
    invoke-direct {v0, v1, v2}, Lcom/lge/systemservice/service/FeliCaDevice;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    #@f
    .line 174
    .local v0, felicaDevice:Lcom/lge/systemservice/service/FeliCaDevice;
    invoke-virtual {v0}, Lcom/lge/systemservice/service/FeliCaDevice;->cmdRFRegCalLoad()Z

    #@12
    move-result v1

    #@13
    return v1
.end method

.method public cmdSwitchRange([Ljava/lang/String;)Z
    .registers 8
    .parameter "response"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 114
    const-string v4, "FeliCaService"

    #@3
    const-string v5, "cmdSwitchRange"

    #@5
    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 117
    new-instance v0, Lcom/lge/systemservice/service/FeliCaDevice$FeliCaCmdResult;

    #@a
    invoke-direct {v0}, Lcom/lge/systemservice/service/FeliCaDevice$FeliCaCmdResult;-><init>()V

    #@d
    .line 118
    .local v0, cmdResult:Lcom/lge/systemservice/service/FeliCaDevice$FeliCaCmdResult;
    new-instance v1, Lcom/lge/systemservice/service/FeliCaDevice;

    #@f
    const/4 v4, 0x0

    #@10
    const-string v5, "FeliCa"

    #@12
    invoke-direct {v1, v4, v5}, Lcom/lge/systemservice/service/FeliCaDevice;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    #@15
    .line 120
    .local v1, felicaDevice:Lcom/lge/systemservice/service/FeliCaDevice;
    array-length v4, p1

    #@16
    if-lez v4, :cond_21

    #@18
    .line 122
    invoke-virtual {v1, v0}, Lcom/lge/systemservice/service/FeliCaDevice;->cmdSwitchRange(Lcom/lge/systemservice/service/FeliCaDevice$FeliCaCmdResult;)Z

    #@1b
    move-result v2

    #@1c
    .line 123
    .local v2, result:Z
    iget-object v4, v0, Lcom/lge/systemservice/service/FeliCaDevice$FeliCaCmdResult;->result:Ljava/lang/String;

    #@1e
    aput-object v4, p1, v3

    #@20
    .line 129
    .end local v2           #result:Z
    :goto_20
    return v2

    #@21
    :cond_21
    move v2, v3

    #@22
    goto :goto_20
.end method

.method public cmdSwitchRead([Ljava/lang/String;)Z
    .registers 8
    .parameter "response"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 148
    const-string v4, "FeliCaService"

    #@3
    const-string v5, "cmdSwitchRead"

    #@5
    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 151
    new-instance v0, Lcom/lge/systemservice/service/FeliCaDevice$FeliCaCmdResult;

    #@a
    invoke-direct {v0}, Lcom/lge/systemservice/service/FeliCaDevice$FeliCaCmdResult;-><init>()V

    #@d
    .line 152
    .local v0, cmdResult:Lcom/lge/systemservice/service/FeliCaDevice$FeliCaCmdResult;
    new-instance v1, Lcom/lge/systemservice/service/FeliCaDevice;

    #@f
    const/4 v4, 0x0

    #@10
    const-string v5, "FeliCa"

    #@12
    invoke-direct {v1, v4, v5}, Lcom/lge/systemservice/service/FeliCaDevice;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    #@15
    .line 154
    .local v1, felicaDevice:Lcom/lge/systemservice/service/FeliCaDevice;
    array-length v4, p1

    #@16
    if-lez v4, :cond_21

    #@18
    .line 156
    invoke-virtual {v1, v0}, Lcom/lge/systemservice/service/FeliCaDevice;->cmdSwitchRead(Lcom/lge/systemservice/service/FeliCaDevice$FeliCaCmdResult;)Z

    #@1b
    move-result v2

    #@1c
    .line 157
    .local v2, result:Z
    iget-object v4, v0, Lcom/lge/systemservice/service/FeliCaDevice$FeliCaCmdResult;->result:Ljava/lang/String;

    #@1e
    aput-object v4, p1, v3

    #@20
    .line 163
    .end local v2           #result:Z
    :goto_20
    return v2

    #@21
    :cond_21
    move v2, v3

    #@22
    goto :goto_20
.end method

.method public cmdSwitchWrite(I)Z
    .registers 6
    .parameter "idx"

    #@0
    .prologue
    .line 135
    const-string v2, "FeliCaService"

    #@2
    const-string v3, "cmdSwitchWrite"

    #@4
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 138
    new-instance v0, Lcom/lge/systemservice/service/FeliCaDevice$FeliCaCmdResult;

    #@9
    invoke-direct {v0}, Lcom/lge/systemservice/service/FeliCaDevice$FeliCaCmdResult;-><init>()V

    #@c
    .line 139
    .local v0, cmdResult:Lcom/lge/systemservice/service/FeliCaDevice$FeliCaCmdResult;
    new-instance v1, Lcom/lge/systemservice/service/FeliCaDevice;

    #@e
    const/4 v2, 0x0

    #@f
    const-string v3, "FeliCa"

    #@11
    invoke-direct {v1, v2, v3}, Lcom/lge/systemservice/service/FeliCaDevice;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    #@14
    .line 142
    .local v1, felicaDevice:Lcom/lge/systemservice/service/FeliCaDevice;
    invoke-virtual {v1, p1}, Lcom/lge/systemservice/service/FeliCaDevice;->cmdSwitchWrite(I)Z

    #@17
    move-result v2

    #@18
    return v2
.end method
