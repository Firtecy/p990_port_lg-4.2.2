.class Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$2;
.super Lcom/qualcomm/wfd/service/IWfdActionListener$Stub;
.source "WfdAdaptation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 99
    iput-object p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$2;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@2
    invoke-direct {p0}, Lcom/qualcomm/wfd/service/IWfdActionListener$Stub;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public notify(Landroid/os/Bundle;I)V
    .registers 10
    .parameter "b"
    .parameter "sessionId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x3

    #@1
    const/4 v5, 0x0

    #@2
    .line 169
    const-string v3, "event"

    #@4
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    .line 170
    .local v0, event:Ljava/lang/String;
    if-nez v0, :cond_b

    #@a
    .line 187
    :cond_a
    :goto_a
    return-void

    #@b
    .line 175
    :cond_b
    const-string v3, "DLNA_SINK_UDN"

    #@d
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10
    move-result v3

    #@11
    if-eqz v3, :cond_2b

    #@13
    .line 176
    const-string v3, "WfdAdaptation"

    #@15
    const-string v4, "DLNA Sink Udn info received"

    #@17
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 177
    const-string v3, "sink_udn"

    #@1c
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    .line 178
    .local v1, sink_udn:Ljava/lang/String;
    iget-object v3, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$2;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@22
    invoke-static {v3}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->access$000(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)Lcom/lge/systemservice/service/wfdservice/WfdMonitor;

    #@25
    move-result-object v3

    #@26
    const/4 v4, 0x1

    #@27
    invoke-virtual {v3, v6, v4, v5, v1}, Lcom/lge/systemservice/service/wfdservice/WfdMonitor;->onReceiveEventFromNative(IIILjava/lang/Object;)V

    #@2a
    goto :goto_a

    #@2b
    .line 180
    .end local v1           #sink_udn:Ljava/lang/String;
    :cond_2b
    const-string v3, "SRC_IP_ADDR"

    #@2d
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@30
    move-result v3

    #@31
    if-eqz v3, :cond_a

    #@33
    .line 181
    const-string v3, "WfdAdaptation"

    #@35
    const-string v4, "WFD Src IP info received"

    #@37
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 182
    const-string v3, "src_ip_addr"

    #@3c
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@3f
    move-result-object v2

    #@40
    .line 183
    .local v2, src_ip:Ljava/lang/String;
    iget-object v3, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$2;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@42
    invoke-static {v3}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->access$000(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)Lcom/lge/systemservice/service/wfdservice/WfdMonitor;

    #@45
    move-result-object v3

    #@46
    const/4 v4, 0x2

    #@47
    invoke-virtual {v3, v6, v4, v5, v2}, Lcom/lge/systemservice/service/wfdservice/WfdMonitor;->onReceiveEventFromNative(IIILjava/lang/Object;)V

    #@4a
    goto :goto_a
.end method

.method public notifyEvent(II)V
    .registers 7
    .parameter "event"
    .parameter "sessionId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 137
    const/4 v0, 0x0

    #@2
    .line 138
    .local v0, cb_event:I
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->HDCP_CONNECT_SUCCESS:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@4
    invoke-virtual {v1}, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->ordinal()I

    #@7
    move-result v1

    #@8
    if-ne p1, v1, :cond_27

    #@a
    .line 139
    const-string v1, "WfdAdaptation"

    #@c
    const-string v2, "HDCP Connect Success"

    #@e
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 140
    const/4 v0, 0x1

    #@12
    .line 161
    :cond_12
    :goto_12
    if-eqz v0, :cond_26

    #@14
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$2;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@16
    invoke-static {v1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->access$000(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)Lcom/lge/systemservice/service/wfdservice/WfdMonitor;

    #@19
    move-result-object v1

    #@1a
    if-eqz v1, :cond_26

    #@1c
    .line 162
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$2;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@1e
    invoke-static {v1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->access$000(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)Lcom/lge/systemservice/service/wfdservice/WfdMonitor;

    #@21
    move-result-object v1

    #@22
    const/4 v2, 0x2

    #@23
    invoke-virtual {v1, v2, v0, v3}, Lcom/lge/systemservice/service/wfdservice/WfdMonitor;->onReceiveEventFromNative(III)V

    #@26
    .line 165
    :cond_26
    return-void

    #@27
    .line 141
    :cond_27
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->HDCP_CONNECT_FAIL:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@29
    invoke-virtual {v1}, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->ordinal()I

    #@2c
    move-result v1

    #@2d
    if-ne p1, v1, :cond_38

    #@2f
    .line 142
    const-string v1, "WfdAdaptation"

    #@31
    const-string v2, "HDCP Connect Fail"

    #@33
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 143
    const/4 v0, 0x2

    #@37
    goto :goto_12

    #@38
    .line 144
    :cond_38
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->TEARDOWN_START:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@3a
    invoke-virtual {v1}, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->ordinal()I

    #@3d
    move-result v1

    #@3e
    if-ne p1, v1, :cond_4e

    #@40
    .line 145
    const-string v1, "WfdAdaptation"

    #@42
    const-string v2, "TEARDOWN started"

    #@44
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    .line 146
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->eventHandler:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$EventHandler;

    #@49
    invoke-virtual {v1, v3}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$EventHandler;->removeMessages(I)V

    #@4c
    .line 147
    const/4 v0, 0x5

    #@4d
    goto :goto_12

    #@4e
    .line 148
    :cond_4e
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->UIBC_SUPPORTED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@50
    invoke-virtual {v1}, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->ordinal()I

    #@53
    move-result v1

    #@54
    if-ne p1, v1, :cond_5f

    #@56
    .line 149
    const-string v1, "WfdAdaptation"

    #@58
    const-string v2, "UIBC supported report"

    #@5a
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    .line 150
    const/4 v0, 0x3

    #@5e
    goto :goto_12

    #@5f
    .line 151
    :cond_5f
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->UIBC_NOT_SUPPORTED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@61
    invoke-virtual {v1}, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->ordinal()I

    #@64
    move-result v1

    #@65
    if-ne p1, v1, :cond_70

    #@67
    .line 152
    const-string v1, "WfdAdaptation"

    #@69
    const-string v2, "UIBC not supported report"

    #@6b
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6e
    .line 153
    const/4 v0, 0x4

    #@6f
    goto :goto_12

    #@70
    .line 154
    :cond_70
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->UIBC_ENABLED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@72
    invoke-virtual {v1}, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->ordinal()I

    #@75
    move-result v1

    #@76
    if-ne p1, v1, :cond_81

    #@78
    .line 155
    const-string v1, "WfdAdaptation"

    #@7a
    const-string v2, "UIBC enabled report"

    #@7c
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7f
    .line 156
    const/4 v0, 0x6

    #@80
    goto :goto_12

    #@81
    .line 157
    :cond_81
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->UIBC_DISABLED:Lcom/qualcomm/wfd/WfdEnums$WfdEvent;

    #@83
    invoke-virtual {v1}, Lcom/qualcomm/wfd/WfdEnums$WfdEvent;->ordinal()I

    #@86
    move-result v1

    #@87
    if-ne p1, v1, :cond_12

    #@89
    .line 158
    const-string v1, "WfdAdaptation"

    #@8b
    const-string v2, "UIBC disabled report"

    #@8d
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@90
    .line 159
    const/4 v0, 0x7

    #@91
    goto :goto_12
.end method

.method public onStateUpdate(II)V
    .registers 8
    .parameter "newState"
    .parameter "sessionId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 102
    invoke-static {}, Lcom/qualcomm/wfd/WfdEnums$SessionState;->values()[Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@4
    move-result-object v1

    #@5
    aget-object v0, v1, p1

    #@7
    .line 103
    .local v0, state:Lcom/qualcomm/wfd/WfdEnums$SessionState;
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$6;->$SwitchMap$com$qualcomm$wfd$WfdEnums$SessionState:[I

    #@9
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ordinal()I

    #@c
    move-result v2

    #@d
    aget v1, v1, v2

    #@f
    packed-switch v1, :pswitch_data_7e

    #@12
    .line 126
    const-string v1, "WfdAdaptation"

    #@14
    new-instance v2, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v3, "Unhandled SessionState: "

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 133
    :cond_2a
    :goto_2a
    return-void

    #@2b
    .line 105
    :pswitch_2b
    const-string v1, "WfdAdaptation"

    #@2d
    const-string v2, "WfdEnums.SessionState==initialized"

    #@2f
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 129
    :goto_32
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$2;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@34
    invoke-static {v1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->access$000(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)Lcom/lge/systemservice/service/wfdservice/WfdMonitor;

    #@37
    move-result-object v1

    #@38
    if-eqz v1, :cond_2a

    #@3a
    .line 130
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$2;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@3c
    invoke-static {v1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->access$000(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)Lcom/lge/systemservice/service/wfdservice/WfdMonitor;

    #@3f
    move-result-object v1

    #@40
    invoke-static {}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->access$100()[I

    #@43
    move-result-object v2

    #@44
    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ordinal()I

    #@47
    move-result v3

    #@48
    aget v2, v2, v3

    #@4a
    invoke-virtual {v1, v4, v2, v4}, Lcom/lge/systemservice/service/wfdservice/WfdMonitor;->onReceiveEventFromNative(III)V

    #@4d
    goto :goto_2a

    #@4e
    .line 108
    :pswitch_4e
    const-string v1, "WfdAdaptation"

    #@50
    const-string v2, "WfdEnums.SessionState==invalid"

    #@52
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@55
    goto :goto_32

    #@56
    .line 111
    :pswitch_56
    const-string v1, "WfdAdaptation"

    #@58
    const-string v2, "WfdEnums.SessionState==IDLE"

    #@5a
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    goto :goto_32

    #@5e
    .line 114
    :pswitch_5e
    const-string v1, "WfdAdaptation"

    #@60
    const-string v2, "WfdEnums.SessionState==PLAY"

    #@62
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@65
    goto :goto_32

    #@66
    .line 117
    :pswitch_66
    const-string v1, "WfdAdaptation"

    #@68
    const-string v2, "WfdEnums.SessionState==PAUSE"

    #@6a
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6d
    goto :goto_32

    #@6e
    .line 120
    :pswitch_6e
    const-string v1, "WfdAdaptation"

    #@70
    const-string v2, "WfdEnums.SessionState==ESTABLISHED"

    #@72
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@75
    goto :goto_32

    #@76
    .line 123
    :pswitch_76
    const-string v1, "WfdAdaptation"

    #@78
    const-string v2, "WfdEnums.SessionState==TEARDOWN"

    #@7a
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7d
    goto :goto_32

    #@7e
    .line 103
    :pswitch_data_7e
    .packed-switch 0x1
        :pswitch_2b
        :pswitch_4e
        :pswitch_56
        :pswitch_5e
        :pswitch_66
        :pswitch_6e
        :pswitch_76
    .end packed-switch
.end method
