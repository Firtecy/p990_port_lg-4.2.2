.class Lcom/lge/systemservice/service/BtLgeExtService$1;
.super Landroid/content/BroadcastReceiver;
.source "BtLgeExtService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/BtLgeExtService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/BtLgeExtService;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/BtLgeExtService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 121
    iput-object p1, p0, Lcom/lge/systemservice/service/BtLgeExtService$1;->this$0:Lcom/lge/systemservice/service/BtLgeExtService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 11
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 124
    iget-object v4, p0, Lcom/lge/systemservice/service/BtLgeExtService$1;->this$0:Lcom/lge/systemservice/service/BtLgeExtService;

    #@3
    invoke-static {v4}, Lcom/lge/systemservice/service/BtLgeExtService;->access$000(Lcom/lge/systemservice/service/BtLgeExtService;)Landroid/content/Context;

    #@6
    move-result-object v4

    #@7
    if-nez v4, :cond_a

    #@9
    .line 158
    :cond_9
    :goto_9
    return-void

    #@a
    .line 128
    :cond_a
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    .line 131
    .local v0, action:Ljava/lang/String;
    const-string v4, "BtLgeExtService"

    #@10
    new-instance v5, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v6, "[BTUI] onReceive action : "

    #@17
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v5

    #@1b
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v5

    #@1f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v5

    #@23
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 134
    const-string v4, "android.bluetooth.adapter.action.STATE_CHANGED"

    #@28
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2b
    move-result v4

    #@2c
    if-eqz v4, :cond_54

    #@2e
    .line 135
    const/16 v4, 0xc

    #@30
    const-string v5, "android.bluetooth.adapter.extra.STATE"

    #@32
    const/high16 v6, -0x8000

    #@34
    invoke-virtual {p2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@37
    move-result v5

    #@38
    if-ne v4, v5, :cond_9

    #@3a
    .line 136
    iget-object v4, p0, Lcom/lge/systemservice/service/BtLgeExtService$1;->this$0:Lcom/lge/systemservice/service/BtLgeExtService;

    #@3c
    invoke-static {v4}, Lcom/lge/systemservice/service/BtLgeExtService;->access$000(Lcom/lge/systemservice/service/BtLgeExtService;)Landroid/content/Context;

    #@3f
    move-result-object v4

    #@40
    const-string v5, "com.lge.bluetooth.CPUFreq.min"

    #@42
    invoke-virtual {v4, v5, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@45
    move-result-object v2

    #@46
    .line 137
    .local v2, pref:Landroid/content/SharedPreferences;
    const-string v4, "com.lge.bluetooth.CPUFreq.min.changed"

    #@48
    invoke-interface {v2, v4, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    #@4b
    move-result v4

    #@4c
    if-eqz v4, :cond_9

    #@4e
    .line 138
    iget-object v4, p0, Lcom/lge/systemservice/service/BtLgeExtService$1;->this$0:Lcom/lge/systemservice/service/BtLgeExtService;

    #@50
    invoke-static {v4, v7}, Lcom/lge/systemservice/service/BtLgeExtService;->access$100(Lcom/lge/systemservice/service/BtLgeExtService;Z)V

    #@53
    goto :goto_9

    #@54
    .line 141
    .end local v2           #pref:Landroid/content/SharedPreferences;
    :cond_54
    const-string v4, "android.bluetooth.a2dp.profile.action.PLAYING_STATE_CHANGED"

    #@56
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@59
    move-result v4

    #@5a
    if-eqz v4, :cond_9

    #@5c
    .line 142
    const-string v4, "android.bluetooth.profile.extra.STATE"

    #@5e
    invoke-virtual {p2, v4, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@61
    move-result v1

    #@62
    .line 143
    .local v1, curStatus:I
    const-string v4, "android.bluetooth.profile.extra.PREVIOUS_STATE"

    #@64
    invoke-virtual {p2, v4, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@67
    move-result v3

    #@68
    .line 145
    .local v3, prevStatus:I
    const-string v4, "BtLgeExtService"

    #@6a
    new-instance v5, Ljava/lang/StringBuilder;

    #@6c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@6f
    const-string v6, "[BTUI] curStatus : "

    #@71
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v5

    #@75
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@78
    move-result-object v5

    #@79
    const-string v6, ", prevStatus : "

    #@7b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v5

    #@7f
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@82
    move-result-object v5

    #@83
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@86
    move-result-object v5

    #@87
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8a
    .line 147
    if-eqz v1, :cond_9

    #@8c
    if-eqz v3, :cond_9

    #@8e
    if-eq v1, v3, :cond_9

    #@90
    .line 148
    const/16 v4, 0xb

    #@92
    if-ne v1, v4, :cond_a0

    #@94
    .line 149
    iget-object v4, p0, Lcom/lge/systemservice/service/BtLgeExtService$1;->this$0:Lcom/lge/systemservice/service/BtLgeExtService;

    #@96
    invoke-static {v4, v7}, Lcom/lge/systemservice/service/BtLgeExtService;->access$100(Lcom/lge/systemservice/service/BtLgeExtService;Z)V

    #@99
    .line 151
    iget-object v4, p0, Lcom/lge/systemservice/service/BtLgeExtService$1;->this$0:Lcom/lge/systemservice/service/BtLgeExtService;

    #@9b
    invoke-static {v4}, Lcom/lge/systemservice/service/BtLgeExtService;->access$200(Lcom/lge/systemservice/service/BtLgeExtService;)V

    #@9e
    goto/16 :goto_9

    #@a0
    .line 153
    :cond_a0
    const/16 v4, 0xa

    #@a2
    if-ne v1, v4, :cond_9

    #@a4
    .line 154
    iget-object v4, p0, Lcom/lge/systemservice/service/BtLgeExtService$1;->this$0:Lcom/lge/systemservice/service/BtLgeExtService;

    #@a6
    const/4 v5, 0x1

    #@a7
    invoke-static {v4, v5}, Lcom/lge/systemservice/service/BtLgeExtService;->access$100(Lcom/lge/systemservice/service/BtLgeExtService;Z)V

    #@aa
    goto/16 :goto_9
.end method
