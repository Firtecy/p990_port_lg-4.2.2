.class Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$EventHandler;
.super Landroid/os/Handler;
.source "WfdAdaptation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "EventHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 468
    iput-object p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$EventHandler;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    .line 472
    const-string v0, "WfdAdaptation"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Event handler received: "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget v2, p1, Landroid/os/Message;->what:I

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 473
    iget v0, p1, Landroid/os/Message;->what:I

    #@1c
    packed-switch v0, :pswitch_data_4c

    #@1f
    .line 479
    const-string v0, "WfdAdaptation"

    #@21
    new-instance v1, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v2, "Unknown event received: "

    #@28
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v1

    #@2c
    iget v2, p1, Landroid/os/Message;->what:I

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v1

    #@36
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    .line 481
    :goto_39
    return-void

    #@3a
    .line 475
    :pswitch_3a
    const-string v0, "WfdAdaptation"

    #@3c
    const-string v1, "timeout for TEARDOWN - start local clean up"

    #@3e
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    .line 476
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$EventHandler;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@43
    invoke-static {v0}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->access$200(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;

    #@46
    move-result-object v0

    #@47
    invoke-virtual {v0}, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->deinit()I

    #@4a
    goto :goto_39

    #@4b
    .line 473
    nop

    #@4c
    :pswitch_data_4c
    .packed-switch 0x0
        :pswitch_3a
    .end packed-switch
.end method
