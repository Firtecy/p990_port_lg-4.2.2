.class Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3$2;
.super Ljava/lang/Object;
.source "WfdStateTracker.java"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 321
    iput-object p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3$2;->this$1:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onFailure(I)V
    .registers 5
    .parameter "reason"

    #@0
    .prologue
    .line 332
    const-string v0, "WfdStateTracker"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Direct disconnect fail, reason: "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 333
    return-void
.end method

.method public onSuccess()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x2

    #@1
    .line 323
    const-string v0, "WfdStateTracker"

    #@3
    const-string v1, "Direct disconnect success"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 324
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3$2;->this$1:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;

    #@a
    iget-object v0, v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@c
    invoke-static {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$200(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)I

    #@f
    move-result v0

    #@10
    if-nez v0, :cond_1a

    #@12
    .line 325
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3$2;->this$1:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;

    #@14
    iget-object v0, v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@16
    invoke-static {v0, v2}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;I)V

    #@19
    .line 330
    :cond_19
    :goto_19
    return-void

    #@1a
    .line 326
    :cond_1a
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3$2;->this$1:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;

    #@1c
    iget-object v0, v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@1e
    invoke-static {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$200(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)I

    #@21
    move-result v0

    #@22
    const/4 v1, 0x1

    #@23
    if-ne v0, v1, :cond_19

    #@25
    .line 327
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3$2;->this$1:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;

    #@27
    iget-object v0, v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@29
    invoke-static {v0, v2}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;I)V

    #@2c
    .line 328
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3$2;->this$1:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;

    #@2e
    iget-object v0, v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@30
    const/4 v1, 0x4

    #@31
    invoke-static {v0, v1}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;I)V

    #@34
    goto :goto_19
.end method
