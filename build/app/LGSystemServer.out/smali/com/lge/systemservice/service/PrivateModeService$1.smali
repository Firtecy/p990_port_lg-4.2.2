.class Lcom/lge/systemservice/service/PrivateModeService$1;
.super Landroid/os/Handler;
.source "PrivateModeService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lge/systemservice/service/PrivateModeService;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/PrivateModeService;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/PrivateModeService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 132
    iput-object p1, p0, Lcom/lge/systemservice/service/PrivateModeService$1;->this$0:Lcom/lge/systemservice/service/PrivateModeService;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 134
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v0, :pswitch_data_48

    #@5
    .line 145
    :cond_5
    :goto_5
    return-void

    #@6
    .line 136
    :pswitch_6
    iget-object v0, p0, Lcom/lge/systemservice/service/PrivateModeService$1;->this$0:Lcom/lge/systemservice/service/PrivateModeService;

    #@8
    invoke-static {v0}, Lcom/lge/systemservice/service/PrivateModeService;->access$300(Lcom/lge/systemservice/service/PrivateModeService;)Z

    #@b
    move-result v0

    #@c
    if-nez v0, :cond_2b

    #@e
    iget-object v0, p0, Lcom/lge/systemservice/service/PrivateModeService$1;->this$0:Lcom/lge/systemservice/service/PrivateModeService;

    #@10
    invoke-static {v0}, Lcom/lge/systemservice/service/PrivateModeService;->access$400(Lcom/lge/systemservice/service/PrivateModeService;)Z

    #@13
    move-result v0

    #@14
    if-nez v0, :cond_2b

    #@16
    .line 137
    invoke-static {}, Lcom/lge/systemservice/service/PrivateModeService;->access$000()Z

    #@19
    move-result v0

    #@1a
    if-eqz v0, :cond_25

    #@1c
    invoke-static {}, Lcom/lge/systemservice/service/PrivateModeService;->access$100()Ljava/lang/String;

    #@1f
    move-result-object v0

    #@20
    const-string v1, "TOGGLE_PRIVATE_MODE:: call startPrivateMode()"

    #@22
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 138
    :cond_25
    iget-object v0, p0, Lcom/lge/systemservice/service/PrivateModeService$1;->this$0:Lcom/lge/systemservice/service/PrivateModeService;

    #@27
    invoke-static {v0}, Lcom/lge/systemservice/service/PrivateModeService;->access$500(Lcom/lge/systemservice/service/PrivateModeService;)V

    #@2a
    goto :goto_5

    #@2b
    .line 139
    :cond_2b
    iget-object v0, p0, Lcom/lge/systemservice/service/PrivateModeService$1;->this$0:Lcom/lge/systemservice/service/PrivateModeService;

    #@2d
    invoke-static {v0}, Lcom/lge/systemservice/service/PrivateModeService;->access$400(Lcom/lge/systemservice/service/PrivateModeService;)Z

    #@30
    move-result v0

    #@31
    if-eqz v0, :cond_5

    #@33
    .line 140
    invoke-static {}, Lcom/lge/systemservice/service/PrivateModeService;->access$000()Z

    #@36
    move-result v0

    #@37
    if-eqz v0, :cond_42

    #@39
    invoke-static {}, Lcom/lge/systemservice/service/PrivateModeService;->access$100()Ljava/lang/String;

    #@3c
    move-result-object v0

    #@3d
    const-string v1, "TOGGLE_PRIVATE_MODE:: call stopPrivateMode()"

    #@3f
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    .line 141
    :cond_42
    iget-object v0, p0, Lcom/lge/systemservice/service/PrivateModeService$1;->this$0:Lcom/lge/systemservice/service/PrivateModeService;

    #@44
    invoke-static {v0}, Lcom/lge/systemservice/service/PrivateModeService;->access$600(Lcom/lge/systemservice/service/PrivateModeService;)V

    #@47
    goto :goto_5

    #@48
    .line 134
    :pswitch_data_48
    .packed-switch 0x1
        :pswitch_6
    .end packed-switch
.end method
