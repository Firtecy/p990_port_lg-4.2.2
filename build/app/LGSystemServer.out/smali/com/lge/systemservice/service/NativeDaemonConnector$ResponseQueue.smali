.class Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue;
.super Ljava/lang/Object;
.source "NativeDaemonConnector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/NativeDaemonConnector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ResponseQueue"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    }
.end annotation


# instance fields
.field private mMaxCount:I

.field private final mResponses:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(I)V
    .registers 3
    .parameter "maxCount"

    #@0
    .prologue
    .line 486
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 487
    new-instance v0, Ljava/util/LinkedList;

    #@5
    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    #@8
    iput-object v0, p0, Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue;->mResponses:Ljava/util/LinkedList;

    #@a
    .line 488
    iput p1, p0, Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue;->mMaxCount:I

    #@c
    .line 489
    return-void
.end method


# virtual methods
.method public add(ILcom/lge/systemservice/service/NativeDaemonEvent;)V
    .registers 11
    .parameter "cmdNum"
    .parameter "response"

    #@0
    .prologue
    .line 492
    const/4 v0, 0x0

    #@1
    .line 493
    .local v0, found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    iget-object v5, p0, Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue;->mResponses:Ljava/util/LinkedList;

    #@3
    monitor-enter v5

    #@4
    .line 494
    :try_start_4
    iget-object v4, p0, Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue;->mResponses:Ljava/util/LinkedList;

    #@6
    invoke-virtual {v4}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v2

    #@a
    .local v2, i$:Ljava/util/Iterator;
    :cond_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v4

    #@e
    if-eqz v4, :cond_a9

    #@10
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v3

    #@14
    check-cast v3, Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;

    #@16
    .line 495
    .local v3, r:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    iget v4, v3, Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;->cmdNum:I
    :try_end_18
    .catchall {:try_start_4 .. :try_end_18} :catchall_a5

    #@18
    if-ne v4, p1, :cond_a

    #@1a
    .line 496
    move-object v0, v3

    #@1b
    move-object v1, v0

    #@1c
    .line 500
    .end local v0           #found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    .end local v3           #r:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    .local v1, found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    :goto_1c
    if-nez v1, :cond_a7

    #@1e
    .line 503
    :goto_1e
    :try_start_1e
    iget-object v4, p0, Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue;->mResponses:Ljava/util/LinkedList;

    #@20
    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    #@23
    move-result v4

    #@24
    iget v6, p0, Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue;->mMaxCount:I

    #@26
    if-lt v4, v6, :cond_8b

    #@28
    .line 504
    const-string v4, "NativeDaemonConnector.ResponseQueue"

    #@2a
    new-instance v6, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v7, "more buffered than allowed: "

    #@31
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v6

    #@35
    iget-object v7, p0, Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue;->mResponses:Ljava/util/LinkedList;

    #@37
    invoke-virtual {v7}, Ljava/util/LinkedList;->size()I

    #@3a
    move-result v7

    #@3b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v6

    #@3f
    const-string v7, " >= "

    #@41
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v6

    #@45
    iget v7, p0, Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue;->mMaxCount:I

    #@47
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v6

    #@4b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v6

    #@4f
    invoke-static {v4, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 508
    iget-object v4, p0, Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue;->mResponses:Ljava/util/LinkedList;

    #@54
    invoke-virtual {v4}, Ljava/util/LinkedList;->remove()Ljava/lang/Object;

    #@57
    move-result-object v3

    #@58
    check-cast v3, Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;

    #@5a
    .line 509
    .restart local v3       #r:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    const-string v4, "NativeDaemonConnector.ResponseQueue"

    #@5c
    new-instance v6, Ljava/lang/StringBuilder;

    #@5e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@61
    const-string v7, "Removing request: "

    #@63
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v6

    #@67
    iget-object v7, v3, Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;->request:Ljava/lang/String;

    #@69
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v6

    #@6d
    const-string v7, " ("

    #@6f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v6

    #@73
    iget v7, v3, Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;->cmdNum:I

    #@75
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@78
    move-result-object v6

    #@79
    const-string v7, ")"

    #@7b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v6

    #@7f
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@82
    move-result-object v6

    #@83
    invoke-static {v4, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_86
    .catchall {:try_start_1e .. :try_end_86} :catchall_87

    #@86
    goto :goto_1e

    #@87
    .line 516
    .end local v3           #r:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    :catchall_87
    move-exception v4

    #@88
    move-object v0, v1

    #@89
    .end local v1           #found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    .end local v2           #i$:Ljava/util/Iterator;
    .restart local v0       #found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    :goto_89
    :try_start_89
    monitor-exit v5
    :try_end_8a
    .catchall {:try_start_89 .. :try_end_8a} :catchall_a5

    #@8a
    throw v4

    #@8b
    .line 512
    .end local v0           #found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    .restart local v1       #found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    .restart local v2       #i$:Ljava/util/Iterator;
    :cond_8b
    :try_start_8b
    new-instance v0, Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;

    #@8d
    const/4 v4, 0x0

    #@8e
    invoke-direct {v0, p1, v4}, Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;-><init>(ILjava/lang/String;)V
    :try_end_91
    .catchall {:try_start_8b .. :try_end_91} :catchall_87

    #@91
    .line 513
    .end local v1           #found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    .restart local v0       #found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    :try_start_91
    iget-object v4, p0, Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue;->mResponses:Ljava/util/LinkedList;

    #@93
    invoke-virtual {v4, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    #@96
    .line 515
    :goto_96
    iget-object v4, v0, Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;->responses:Ljava/util/LinkedList;

    #@98
    invoke-virtual {v4, p2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    #@9b
    .line 516
    monitor-exit v5
    :try_end_9c
    .catchall {:try_start_91 .. :try_end_9c} :catchall_a5

    #@9c
    .line 517
    monitor-enter v0

    #@9d
    .line 518
    :try_start_9d
    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    #@a0
    .line 519
    monitor-exit v0

    #@a1
    .line 520
    return-void

    #@a2
    .line 519
    :catchall_a2
    move-exception v4

    #@a3
    monitor-exit v0
    :try_end_a4
    .catchall {:try_start_9d .. :try_end_a4} :catchall_a2

    #@a4
    throw v4

    #@a5
    .line 516
    .end local v2           #i$:Ljava/util/Iterator;
    :catchall_a5
    move-exception v4

    #@a6
    goto :goto_89

    #@a7
    .end local v0           #found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    .restart local v1       #found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    .restart local v2       #i$:Ljava/util/Iterator;
    :cond_a7
    move-object v0, v1

    #@a8
    .end local v1           #found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    .restart local v0       #found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    goto :goto_96

    #@a9
    :cond_a9
    move-object v1, v0

    #@aa
    .end local v0           #found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    .restart local v1       #found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    goto/16 :goto_1c
.end method

.method public remove(IILjava/lang/String;)Lcom/lge/systemservice/service/NativeDaemonEvent;
    .registers 16
    .parameter "cmdNum"
    .parameter "timeoutMs"
    .parameter "origCmd"

    #@0
    .prologue
    .line 525
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@3
    move-result-wide v8

    #@4
    int-to-long v10, p2

    #@5
    add-long v0, v8, v10

    #@7
    .line 527
    .local v0, endTime:J
    const/4 v2, 0x0

    #@8
    .line 529
    .local v2, found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    :goto_8
    iget-object v9, p0, Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue;->mResponses:Ljava/util/LinkedList;

    #@a
    monitor-enter v9

    #@b
    .line 530
    :try_start_b
    iget-object v8, p0, Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue;->mResponses:Ljava/util/LinkedList;

    #@d
    invoke-virtual {v8}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;
    :try_end_10
    .catchall {:try_start_b .. :try_end_10} :catchall_3e

    #@10
    move-result-object v4

    #@11
    .local v4, i$:Ljava/util/Iterator;
    move-object v3, v2

    #@12
    .end local v2           #found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    .local v3, found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    :goto_12
    :try_start_12
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@15
    move-result v8

    #@16
    if-eqz v8, :cond_44

    #@18
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1b
    move-result-object v7

    #@1c
    check-cast v7, Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;

    #@1e
    .line 531
    .local v7, response:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    iget v8, v7, Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;->cmdNum:I
    :try_end_20
    .catchall {:try_start_12 .. :try_end_20} :catchall_71

    #@20
    if-ne v8, p1, :cond_41

    #@22
    .line 532
    move-object v2, v7

    #@23
    .line 534
    .end local v3           #found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    .restart local v2       #found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    :try_start_23
    iget-object v8, v7, Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;->responses:Ljava/util/LinkedList;

    #@25
    invoke-virtual {v8}, Ljava/util/LinkedList;->size()I

    #@28
    move-result v8

    #@29
    packed-switch v8, :pswitch_data_76

    #@2c
    .line 540
    :goto_2c
    iput-object p3, v7, Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;->request:Ljava/lang/String;

    #@2e
    .line 541
    iget-object v8, v7, Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;->responses:Ljava/util/LinkedList;

    #@30
    invoke-virtual {v8}, Ljava/util/LinkedList;->remove()Ljava/lang/Object;

    #@33
    move-result-object v8

    #@34
    check-cast v8, Lcom/lge/systemservice/service/NativeDaemonEvent;

    #@36
    monitor-exit v9

    #@37
    .line 549
    .end local v7           #response:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    :goto_37
    return-object v8

    #@38
    .line 538
    .restart local v7       #response:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    :pswitch_38
    iget-object v8, p0, Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue;->mResponses:Ljava/util/LinkedList;

    #@3a
    invoke-virtual {v8, v7}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    #@3d
    goto :goto_2c

    #@3e
    .line 557
    .end local v4           #i$:Ljava/util/Iterator;
    .end local v7           #response:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    :catchall_3e
    move-exception v8

    #@3f
    :goto_3f
    monitor-exit v9
    :try_end_40
    .catchall {:try_start_23 .. :try_end_40} :catchall_3e

    #@40
    throw v8

    #@41
    .end local v2           #found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    .restart local v3       #found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    .restart local v4       #i$:Ljava/util/Iterator;
    .restart local v7       #response:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    :cond_41
    move-object v2, v3

    #@42
    .end local v3           #found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    .restart local v2       #found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    :pswitch_42
    move-object v3, v2

    #@43
    .line 541
    .end local v2           #found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    .restart local v3       #found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    goto :goto_12

    #@44
    .line 545
    .end local v7           #response:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    :cond_44
    :try_start_44
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@47
    move-result-wide v5

    #@48
    .line 546
    .local v5, nowTime:J
    cmp-long v8, v0, v5

    #@4a
    if-gtz v8, :cond_57

    #@4c
    .line 547
    const-string v8, "NativeDaemonConnector.ResponseQueue"

    #@4e
    const-string v10, "Timeout waiting for response"

    #@50
    invoke-static {v8, v10}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    .line 549
    const/4 v8, 0x0

    #@54
    monitor-exit v9

    #@55
    move-object v2, v3

    #@56
    .end local v3           #found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    .restart local v2       #found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    goto :goto_37

    #@57
    .line 553
    .end local v2           #found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    .restart local v3       #found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    :cond_57
    if-nez v3, :cond_74

    #@59
    .line 554
    new-instance v2, Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;

    #@5b
    invoke-direct {v2, p1, p3}, Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;-><init>(ILjava/lang/String;)V
    :try_end_5e
    .catchall {:try_start_44 .. :try_end_5e} :catchall_71

    #@5e
    .line 555
    .end local v3           #found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    .restart local v2       #found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    :try_start_5e
    iget-object v8, p0, Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue;->mResponses:Ljava/util/LinkedList;

    #@60
    invoke-virtual {v8, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    #@63
    .line 557
    :goto_63
    monitor-exit v9
    :try_end_64
    .catchall {:try_start_5e .. :try_end_64} :catchall_3e

    #@64
    .line 559
    :try_start_64
    monitor-enter v2
    :try_end_65
    .catch Ljava/lang/InterruptedException; {:try_start_64 .. :try_end_65} :catch_6f

    #@65
    .line 560
    sub-long v8, v0, v5

    #@67
    :try_start_67
    invoke-virtual {v2, v8, v9}, Ljava/lang/Object;->wait(J)V

    #@6a
    .line 561
    monitor-exit v2

    #@6b
    goto :goto_8

    #@6c
    :catchall_6c
    move-exception v8

    #@6d
    monitor-exit v2
    :try_end_6e
    .catchall {:try_start_67 .. :try_end_6e} :catchall_6c

    #@6e
    :try_start_6e
    throw v8
    :try_end_6f
    .catch Ljava/lang/InterruptedException; {:try_start_6e .. :try_end_6f} :catch_6f

    #@6f
    .line 562
    :catch_6f
    move-exception v8

    #@70
    goto :goto_8

    #@71
    .line 557
    .end local v2           #found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    .end local v5           #nowTime:J
    .restart local v3       #found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    :catchall_71
    move-exception v8

    #@72
    move-object v2, v3

    #@73
    .end local v3           #found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    .restart local v2       #found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    goto :goto_3f

    #@74
    .end local v2           #found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    .restart local v3       #found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    .restart local v5       #nowTime:J
    :cond_74
    move-object v2, v3

    #@75
    .end local v3           #found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    .restart local v2       #found:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue$Response;
    goto :goto_63

    #@76
    .line 534
    :pswitch_data_76
    .packed-switch 0x0
        :pswitch_42
        :pswitch_38
    .end packed-switch
.end method
