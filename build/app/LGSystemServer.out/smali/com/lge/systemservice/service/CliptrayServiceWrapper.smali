.class public Lcom/lge/systemservice/service/CliptrayServiceWrapper;
.super Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService$Stub;
.source "CliptrayServiceWrapper.java"


# instance fields
.field private mClipManager:Landroid/content/ClipboardManager;

.field private mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

.field private mConnected:Z

.field private mContext:Landroid/content/Context;

.field private mCurrPasteListener:Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;

.field private mCurrentInputType:I

.field private mIsShowing:Z

.field private mServiceConn:Landroid/content/ServiceConnection;

.field private thumbnailHeight:I

.field private thumbnailWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 105
    invoke-direct {p0}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService$Stub;-><init>()V

    #@5
    .line 76
    iput-object v3, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mContext:Landroid/content/Context;

    #@7
    .line 78
    iput-boolean v2, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mIsShowing:Z

    #@9
    .line 79
    iput-boolean v2, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mConnected:Z

    #@b
    .line 80
    iput v2, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCurrentInputType:I

    #@d
    .line 81
    iput-object v3, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@f
    .line 84
    iput-object v3, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCurrPasteListener:Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;

    #@11
    .line 86
    new-instance v2, Lcom/lge/systemservice/service/CliptrayServiceWrapper$1;

    #@13
    invoke-direct {v2, p0}, Lcom/lge/systemservice/service/CliptrayServiceWrapper$1;-><init>(Lcom/lge/systemservice/service/CliptrayServiceWrapper;)V

    #@16
    iput-object v2, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mServiceConn:Landroid/content/ServiceConnection;

    #@18
    .line 106
    iput-object p1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mContext:Landroid/content/Context;

    #@1a
    .line 108
    iget-object v2, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mContext:Landroid/content/Context;

    #@1c
    const-string v3, "clipboard"

    #@1e
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@21
    move-result-object v2

    #@22
    check-cast v2, Landroid/content/ClipboardManager;

    #@24
    iput-object v2, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mClipManager:Landroid/content/ClipboardManager;

    #@26
    .line 111
    iget-object v2, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mContext:Landroid/content/Context;

    #@28
    const-string v3, "window"

    #@2a
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@2d
    move-result-object v1

    #@2e
    check-cast v1, Landroid/view/WindowManager;

    #@30
    .line 112
    .local v1, wm:Landroid/view/WindowManager;
    new-instance v0, Landroid/util/DisplayMetrics;

    #@32
    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    #@35
    .line 113
    .local v0, metrics:Landroid/util/DisplayMetrics;
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    #@3c
    .line 115
    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    #@3e
    iput v2, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->thumbnailWidth:I

    #@40
    .line 116
    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    #@42
    iput v2, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->thumbnailHeight:I

    #@44
    .line 117
    const-string v2, "CliptrayServiceWrapper"

    #@46
    const-string v3, "new CliptrayManager"

    #@48
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    .line 118
    invoke-direct {p0}, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->startCliptrayService()V

    #@4e
    .line 119
    return-void
.end method

.method static synthetic access$000(Lcom/lge/systemservice/service/CliptrayServiceWrapper;)Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 65
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@2
    return-object v0
.end method

.method static synthetic access$002(Lcom/lge/systemservice/service/CliptrayServiceWrapper;Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;)Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 65
    iput-object p1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@2
    return-object p1
.end method

.method private startCliptrayService()V
    .registers 5

    #@0
    .prologue
    .line 194
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mContext:Landroid/content/Context;

    #@2
    new-instance v1, Landroid/content/Intent;

    #@4
    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    #@7
    const-string v2, "lge.android.intent.action.CLIPTRAY_SERVICE"

    #@9
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@c
    move-result-object v1

    #@d
    iget-object v2, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mServiceConn:Landroid/content/ServiceConnection;

    #@f
    const/4 v3, 0x1

    #@10
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    #@13
    .line 195
    return-void
.end method


# virtual methods
.method public addNewClipData()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 201
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@2
    if-eqz v1, :cond_9

    #@4
    .line 203
    :try_start_4
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@6
    invoke-interface {v1}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->addNewClipData()V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 208
    :cond_9
    :goto_9
    return-void

    #@a
    .line 204
    :catch_a
    move-exception v0

    #@b
    .line 205
    .local v0, e1:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@e
    goto :goto_9
.end method

.method public doCopyAnimation()V
    .registers 3

    #@0
    .prologue
    .line 314
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@2
    if-eqz v1, :cond_9

    #@4
    .line 316
    :try_start_4
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@6
    invoke-interface {v1}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->doCopyAnimation()V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 321
    :cond_9
    :goto_9
    return-void

    #@a
    .line 317
    :catch_a
    move-exception v0

    #@b
    .line 318
    .local v0, e1:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@e
    goto :goto_9
.end method

.method public getClose()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 212
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@2
    if-eqz v1, :cond_9

    #@4
    .line 214
    :try_start_4
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@6
    invoke-interface {v1}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->getClose()V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 219
    :cond_9
    :goto_9
    return-void

    #@a
    .line 215
    :catch_a
    move-exception v0

    #@b
    .line 216
    .local v0, e1:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@e
    goto :goto_9
.end method

.method public getPeek()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 224
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@2
    if-eqz v1, :cond_9

    #@4
    .line 226
    :try_start_4
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@6
    invoke-interface {v1}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->getPeek()V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 231
    :cond_9
    :goto_9
    return-void

    #@a
    .line 227
    :catch_a
    move-exception v0

    #@b
    .line 228
    .local v0, e1:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@e
    goto :goto_9
.end method

.method public getServiceConnected()Z
    .registers 3

    #@0
    .prologue
    .line 302
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@2
    if-eqz v1, :cond_f

    #@4
    .line 304
    :try_start_4
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@6
    invoke-interface {v1}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->getServiceConnected()Z
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_b

    #@9
    move-result v1

    #@a
    .line 309
    :goto_a
    return v1

    #@b
    .line 305
    :catch_b
    move-exception v0

    #@c
    .line 306
    .local v0, e1:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@f
    .line 309
    .end local v0           #e1:Landroid/os/RemoteException;
    :cond_f
    iget-boolean v1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mConnected:Z

    #@11
    goto :goto_a
.end method

.method public getShow()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 235
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@2
    if-eqz v1, :cond_9

    #@4
    .line 237
    :try_start_4
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@6
    invoke-interface {v1}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->getShow()V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 242
    :cond_9
    :goto_9
    return-void

    #@a
    .line 238
    :catch_a
    move-exception v0

    #@b
    .line 239
    .local v0, e1:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@e
    goto :goto_9
.end method

.method public getVisibility()I
    .registers 4

    #@0
    .prologue
    .line 123
    const/4 v1, -0x1

    #@1
    .line 124
    .local v1, visibility:I
    iget-object v2, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@3
    if-eqz v2, :cond_b

    #@5
    .line 126
    :try_start_5
    iget-object v2, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@7
    invoke-interface {v2}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->getVisibility()I
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_a} :catch_c

    #@a
    move-result v1

    #@b
    .line 131
    :cond_b
    :goto_b
    return v1

    #@c
    .line 127
    :catch_c
    move-exception v0

    #@d
    .line 128
    .local v0, e1:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@10
    goto :goto_b
.end method

.method public hideCliptraycue()V
    .registers 3

    #@0
    .prologue
    .line 182
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@2
    if-eqz v1, :cond_9

    #@4
    .line 184
    :try_start_4
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@6
    invoke-interface {v1}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->hideCliptraycue()V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 189
    :cond_9
    :goto_9
    return-void

    #@a
    .line 185
    :catch_a
    move-exception v0

    #@b
    .line 186
    .local v0, e1:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@e
    goto :goto_9
.end method

.method public isCliptraycueShowing()Z
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 269
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@2
    if-eqz v1, :cond_f

    #@4
    .line 271
    :try_start_4
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@6
    invoke-interface {v1}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->isCliptraycueShowing()Z
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_b

    #@9
    move-result v1

    #@a
    .line 276
    :goto_a
    return v1

    #@b
    .line 272
    :catch_b
    move-exception v0

    #@c
    .line 273
    .local v0, e1:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@f
    .line 276
    .end local v0           #e1:Landroid/os/RemoteException;
    :cond_f
    const/4 v1, 0x0

    #@10
    goto :goto_a
.end method

.method public orientationChanged(Z)V
    .registers 4
    .parameter "portrait"

    #@0
    .prologue
    .line 148
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@2
    if-eqz v1, :cond_9

    #@4
    .line 150
    :try_start_4
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@6
    invoke-interface {v1, p1}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->orientationChanged(Z)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 155
    :cond_9
    :goto_9
    return-void

    #@a
    .line 151
    :catch_a
    move-exception v0

    #@b
    .line 152
    .local v0, e1:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@e
    goto :goto_9
.end method

.method public removePasteListener(Lcom/lge/systemservice/core/cliptraymanager/IOnPasteListener;)V
    .registers 4
    .parameter "arg0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 246
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@2
    if-eqz v1, :cond_9

    #@4
    .line 248
    :try_start_4
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@6
    invoke-interface {v1, p1}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->removePasteListener(Lcom/lge/systemservice/core/cliptraymanager/IOnPasteListener;)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 253
    :cond_9
    :goto_9
    return-void

    #@a
    .line 249
    :catch_a
    move-exception v0

    #@b
    .line 250
    .local v0, e1:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@e
    goto :goto_9
.end method

.method public setInputType(I)V
    .registers 4
    .parameter "type"

    #@0
    .prologue
    .line 136
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@2
    if-eqz v1, :cond_b

    #@4
    .line 138
    :try_start_4
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@6
    invoke-interface {v1, p1}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->setInputType(I)V

    #@9
    .line 139
    iput p1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCurrentInputType:I
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_b} :catch_c

    #@b
    .line 144
    :cond_b
    :goto_b
    return-void

    #@c
    .line 140
    :catch_c
    move-exception v0

    #@d
    .line 141
    .local v0, e1:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@10
    goto :goto_b
.end method

.method public setPasteListener(Lcom/lge/systemservice/core/cliptraymanager/IOnPasteListener;)V
    .registers 4
    .parameter "arg0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 257
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@2
    if-eqz v1, :cond_9

    #@4
    .line 259
    :try_start_4
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@6
    invoke-interface {v1, p1}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->setPasteListener(Lcom/lge/systemservice/core/cliptraymanager/IOnPasteListener;)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 264
    :cond_9
    :goto_9
    return-void

    #@a
    .line 260
    :catch_a
    move-exception v0

    #@b
    .line 261
    .local v0, e1:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@e
    goto :goto_9
.end method

.method public setServiceConnected(Z)V
    .registers 4
    .parameter "mConnect"

    #@0
    .prologue
    .line 291
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@2
    if-eqz v1, :cond_9

    #@4
    .line 293
    :try_start_4
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@6
    invoke-interface {v1, p1}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->setServiceConnected(Z)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 298
    :cond_9
    :goto_9
    return-void

    #@a
    .line 294
    :catch_a
    move-exception v0

    #@b
    .line 295
    .local v0, e1:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@e
    goto :goto_9
.end method

.method public showCliptrayCopiedToast()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 325
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@2
    if-eqz v1, :cond_9

    #@4
    .line 327
    :try_start_4
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@6
    invoke-interface {v1}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->showCliptrayCopiedToast()V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 332
    :cond_9
    :goto_9
    return-void

    #@a
    .line 328
    :catch_a
    move-exception v0

    #@b
    .line 329
    .local v0, e1:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@e
    goto :goto_9
.end method

.method public showCliptraycue()V
    .registers 3

    #@0
    .prologue
    .line 160
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@2
    if-eqz v1, :cond_9

    #@4
    .line 162
    :try_start_4
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@6
    invoke-interface {v1}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->showCliptraycue()V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 167
    :cond_9
    :goto_9
    return-void

    #@a
    .line 163
    :catch_a
    move-exception v0

    #@b
    .line 164
    .local v0, e1:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@e
    goto :goto_9
.end method

.method public showCliptraycueClose()V
    .registers 3

    #@0
    .prologue
    .line 171
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@2
    if-eqz v1, :cond_9

    #@4
    .line 173
    :try_start_4
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@6
    invoke-interface {v1}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->showCliptraycueClose()V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 178
    :cond_9
    :goto_9
    return-void

    #@a
    .line 174
    :catch_a
    move-exception v0

    #@b
    .line 175
    .local v0, e1:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@e
    goto :goto_9
.end method

.method public showDecodeErrorToast()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 281
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@2
    if-eqz v1, :cond_9

    #@4
    .line 283
    :try_start_4
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->mCliptrayService:Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@6
    invoke-interface {v1}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->showDecodeErrorToast()V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 288
    :cond_9
    :goto_9
    return-void

    #@a
    .line 284
    :catch_a
    move-exception v0

    #@b
    .line 285
    .local v0, e1:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@e
    goto :goto_9
.end method
