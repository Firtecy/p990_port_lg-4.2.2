.class Lcom/lge/systemservice/service/ClayboxLauncher$1;
.super Lcom/lge/systemservice/service/IClayboxLauncher$Stub;
.source "ClayboxLauncher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/ClayboxLauncher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/ClayboxLauncher;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/ClayboxLauncher;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 54
    iput-object p1, p0, Lcom/lge/systemservice/service/ClayboxLauncher$1;->this$0:Lcom/lge/systemservice/service/ClayboxLauncher;

    #@2
    invoke-direct {p0}, Lcom/lge/systemservice/service/IClayboxLauncher$Stub;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public startClaybox(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 30
    .parameter "path"
    .parameter "jarfile"

    #@0
    .prologue
    .line 60
    const/4 v8, 0x0

    #@1
    .line 62
    .local v8, callerIsART:Z
    :try_start_1
    const-string v24, "MIIDJDCCAgygAwIBAgIET9A5qzANBgkqhkiG9w0BAQUFADBTMQswCQYDVQQGEwJrbzEMMAoGA1UECBMDbGdlMQwwCgYDVQQHEwNsZ2UxDDAKBgNVBAoTA2xnZTEMMAoGA1UECxMDbGdlMQwwCgYDVQQDEwNsZ2UwIBcNMTIwNjA3MDUxODM1WhgPMjI4NjAzMjMwNTE4MzVaMFMxCzAJBgNVBAYTAmtvMQwwCgYDVQQIEwNsZ2UxDDAKBgNVBAcTA2xnZTEMMAoGA1UEChMDbGdlMQwwCgYDVQQLEwNsZ2UxDDAKBgNVBAMTA2xnZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOZe8xG85261iyweZIbTu/0oFFlTLGqCF5CRXS+jcs8eHgpEO6H07XX9gVlQ2da5SEjjJeDoEUQhDlygR/z1kSmEDwYwX/s3bDhghKpXHPkOsXIWKj7/hO76e4kPlKtck2KzhdSbjkQEktCXkRRTpNB+mlUc1BQsRFGVKCEyIEiypP0jwtR9+yLjUfsBhgI9V3EWsRP5Af0WJ07wPONgr7rZqBBWzIc3lQP1jX8jk8ycJG22+j5wlAJ5je+gdvxX4JyI9QPlDYQEsExeV8kHopjkL6bGDjmoBLP5h5Z6Q+ht2oBkRrstSm7oXGaBsBZd9Hid3Dx3FGwEixcp9BaQqZ8CAwEAATANBgkqhkiG9w0BAQUFAAOCAQEAX3grEnXhx/QapiBK0FMTRwQXnODncpM7Mqq++DiyTfRC0Yh8ARr2TySRdT8wLKH/bsgwIV/GGKArYUvvvI3bmLsKYskYO38U1PHedBvpKLVHLkTVJ0qVJHpRVPGYOaXzb82MOzxzm9Hn5rg4HQf6k4Wwl5jU8ofwczicdcf/CSQb0SnzJKZJhO1okYqanFTm5WBbb+9WoWlHyQDt7GEm3akDwVd/gdQ8LItmbPGv1gI3Yax+ww0CJVoCvkdtWwwcjCpq0Wb3q+HAEGzbQNOJHLnKBzF/bNdVne3aTkEHpTKjWxnDDms7d/nrqK+Vz2IKLt1Izy3OMvH7OlbC1UZiRA=="

    #@3
    const/16 v25, 0x0

    #@5
    invoke-static/range {v24 .. v25}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    #@8
    move-result-object v12

    #@9
    .line 63
    .local v12, encoded:[B
    const-string v24, "X.509"

    #@b
    invoke-static/range {v24 .. v24}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    #@e
    move-result-object v9

    #@f
    .line 64
    .local v9, cf:Ljava/security/cert/CertificateFactory;
    new-instance v6, Ljava/io/ByteArrayInputStream;

    #@11
    invoke-direct {v6, v12}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    #@14
    .line 65
    .local v6, bais:Ljava/io/ByteArrayInputStream;
    invoke-virtual {v9, v6}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    #@17
    move-result-object v5

    #@18
    .line 67
    .local v5, artitecert:Ljava/security/cert/Certificate;
    invoke-static {}, Lcom/lge/systemservice/service/ClayboxLauncher$1;->getCallingUid()I

    #@1b
    move-result v23

    #@1c
    .line 68
    .local v23, uid:I
    const-string v24, "ArtSystemServer"

    #@1e
    new-instance v25, Ljava/lang/StringBuilder;

    #@20
    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v26, "uid : "

    #@25
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v25

    #@29
    move-object/from16 v0, v25

    #@2b
    move/from16 v1, v23

    #@2d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30
    move-result-object v25

    #@31
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v25

    #@35
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 69
    move-object/from16 v0, p0

    #@3a
    iget-object v0, v0, Lcom/lge/systemservice/service/ClayboxLauncher$1;->this$0:Lcom/lge/systemservice/service/ClayboxLauncher;

    #@3c
    move-object/from16 v24, v0

    #@3e
    invoke-virtual/range {v24 .. v24}, Lcom/lge/systemservice/service/ClayboxLauncher;->getPackageManager()Landroid/content/pm/PackageManager;

    #@41
    move-result-object v21

    #@42
    .line 70
    .local v21, pm:Landroid/content/pm/PackageManager;
    move-object/from16 v0, v21

    #@44
    move/from16 v1, v23

    #@46
    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    #@49
    move-result-object v19

    #@4a
    .line 71
    .local v19, packages:[Ljava/lang/String;
    if-nez v19, :cond_56

    #@4c
    .line 72
    const-string v24, "ArtSystemServer"

    #@4e
    const-string v25, "packages null!!"

    #@50
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    .line 73
    const/16 v24, 0x0

    #@55
    .line 133
    .end local v5           #artitecert:Ljava/security/cert/Certificate;
    .end local v6           #bais:Ljava/io/ByteArrayInputStream;
    .end local v9           #cf:Ljava/security/cert/CertificateFactory;
    .end local v12           #encoded:[B
    .end local v19           #packages:[Ljava/lang/String;
    .end local v21           #pm:Landroid/content/pm/PackageManager;
    .end local v23           #uid:I
    :goto_55
    return v24

    #@56
    .line 75
    .restart local v5       #artitecert:Ljava/security/cert/Certificate;
    .restart local v6       #bais:Ljava/io/ByteArrayInputStream;
    .restart local v9       #cf:Ljava/security/cert/CertificateFactory;
    .restart local v12       #encoded:[B
    .restart local v19       #packages:[Ljava/lang/String;
    .restart local v21       #pm:Landroid/content/pm/PackageManager;
    .restart local v23       #uid:I
    :cond_56
    move-object/from16 v3, v19

    #@58
    .local v3, arr$:[Ljava/lang/String;
    array-length v0, v3

    #@59
    move/from16 v16, v0

    #@5b
    .local v16, len$:I
    const/4 v14, 0x0

    #@5c
    .local v14, i$:I
    move v15, v14

    #@5d
    .end local v3           #arr$:[Ljava/lang/String;
    .end local v14           #i$:I
    .end local v16           #len$:I
    .local v15, i$:I
    :goto_5d
    move/from16 v0, v16

    #@5f
    if-ge v15, v0, :cond_9f

    #@61
    aget-object v18, v3, v15

    #@63
    .line 76
    .local v18, p:Ljava/lang/String;
    const-string v24, "com.lge.art"

    #@65
    move-object/from16 v0, v18

    #@67
    move-object/from16 v1, v24

    #@69
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6c
    move-result v24

    #@6d
    if-eqz v24, :cond_94

    #@6f
    .line 77
    const/16 v24, 0x40

    #@71
    move-object/from16 v0, v21

    #@73
    move-object/from16 v1, v18

    #@75
    move/from16 v2, v24

    #@77
    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    #@7a
    move-result-object v20

    #@7b
    .line 78
    .local v20, pi:Landroid/content/pm/PackageInfo;
    move-object/from16 v0, v20

    #@7d
    iget-object v4, v0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    #@7f
    .local v4, arr$:[Landroid/content/pm/Signature;
    array-length v0, v4

    #@80
    move/from16 v17, v0

    #@82
    .local v17, len$:I
    const/4 v14, 0x0

    #@83
    .end local v15           #i$:I
    .restart local v14       #i$:I
    :goto_83
    move/from16 v0, v17

    #@85
    if-ge v14, v0, :cond_94

    #@87
    aget-object v22, v4, v14

    #@89
    .line 79
    .local v22, s:Landroid/content/pm/Signature;
    invoke-virtual/range {v22 .. v22}, Landroid/content/pm/Signature;->toByteArray()[B

    #@8c
    move-result-object v24

    #@8d
    invoke-static/range {v24 .. v24}, Lcom/lge/systemservice/service/ClayboxLauncher;->access$000([B)Z
    :try_end_90
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_90} :catch_9b

    #@90
    move-result v24

    #@91
    if-eqz v24, :cond_98

    #@93
    .line 80
    const/4 v8, 0x1

    #@94
    .line 75
    .end local v4           #arr$:[Landroid/content/pm/Signature;
    .end local v14           #i$:I
    .end local v17           #len$:I
    .end local v20           #pi:Landroid/content/pm/PackageInfo;
    .end local v22           #s:Landroid/content/pm/Signature;
    :cond_94
    add-int/lit8 v14, v15, 0x1

    #@96
    .restart local v14       #i$:I
    move v15, v14

    #@97
    .end local v14           #i$:I
    .restart local v15       #i$:I
    goto :goto_5d

    #@98
    .line 78
    .end local v15           #i$:I
    .restart local v4       #arr$:[Landroid/content/pm/Signature;
    .restart local v14       #i$:I
    .restart local v17       #len$:I
    .restart local v20       #pi:Landroid/content/pm/PackageInfo;
    .restart local v22       #s:Landroid/content/pm/Signature;
    :cond_98
    add-int/lit8 v14, v14, 0x1

    #@9a
    goto :goto_83

    #@9b
    .line 86
    .end local v4           #arr$:[Landroid/content/pm/Signature;
    .end local v5           #artitecert:Ljava/security/cert/Certificate;
    .end local v6           #bais:Ljava/io/ByteArrayInputStream;
    .end local v9           #cf:Ljava/security/cert/CertificateFactory;
    .end local v12           #encoded:[B
    .end local v14           #i$:I
    .end local v17           #len$:I
    .end local v18           #p:Ljava/lang/String;
    .end local v19           #packages:[Ljava/lang/String;
    .end local v20           #pi:Landroid/content/pm/PackageInfo;
    .end local v21           #pm:Landroid/content/pm/PackageManager;
    .end local v22           #s:Landroid/content/pm/Signature;
    .end local v23           #uid:I
    :catch_9b
    move-exception v11

    #@9c
    .line 87
    .local v11, e:Ljava/lang/Exception;
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V

    #@9f
    .line 90
    .end local v11           #e:Ljava/lang/Exception;
    :cond_9f
    if-nez v8, :cond_ab

    #@a1
    .line 91
    const-string v24, "ArtSystemServer"

    #@a3
    const-string v25, "invalid art service!!! "

    #@a5
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a8
    .line 92
    const/16 v24, 0x0

    #@aa
    goto :goto_55

    #@ab
    .line 95
    :cond_ab
    new-instance v24, Ljava/lang/StringBuilder;

    #@ad
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@b0
    move-object/from16 v0, v24

    #@b2
    move-object/from16 v1, p1

    #@b4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v24

    #@b8
    const-string v25, "/"

    #@ba
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v24

    #@be
    move-object/from16 v0, v24

    #@c0
    move-object/from16 v1, p2

    #@c2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v24

    #@c6
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c9
    move-result-object v13

    #@ca
    .line 97
    .local v13, fullpath:Ljava/lang/String;
    invoke-static {v13}, Lcom/lge/systemservice/service/ClayboxLauncher;->access$100(Ljava/lang/String;)Z

    #@cd
    move-result v24

    #@ce
    if-eqz v24, :cond_13f

    #@d0
    .line 100
    :try_start_d0
    move-object/from16 v0, p0

    #@d2
    iget-object v0, v0, Lcom/lge/systemservice/service/ClayboxLauncher$1;->this$0:Lcom/lge/systemservice/service/ClayboxLauncher;

    #@d4
    move-object/from16 v24, v0

    #@d6
    invoke-static/range {v24 .. v24}, Lcom/lge/systemservice/service/ClayboxLauncher;->access$200(Lcom/lge/systemservice/service/ClayboxLauncher;)Ljava/lang/Process;

    #@d9
    move-result-object v24

    #@da
    if-eqz v24, :cond_e9

    #@dc
    .line 102
    move-object/from16 v0, p0

    #@de
    iget-object v0, v0, Lcom/lge/systemservice/service/ClayboxLauncher$1;->this$0:Lcom/lge/systemservice/service/ClayboxLauncher;

    #@e0
    move-object/from16 v24, v0

    #@e2
    invoke-static/range {v24 .. v24}, Lcom/lge/systemservice/service/ClayboxLauncher;->access$200(Lcom/lge/systemservice/service/ClayboxLauncher;)Ljava/lang/Process;

    #@e5
    move-result-object v24

    #@e6
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Process;->destroy()V
    :try_end_e9
    .catch Ljava/lang/Exception; {:try_start_d0 .. :try_end_e9} :catch_132

    #@e9
    .line 111
    :cond_e9
    :goto_e9
    :try_start_e9
    new-instance v10, Ljava/util/ArrayList;

    #@eb
    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    #@ee
    .line 112
    .local v10, clayboxprog:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const-string v24, "/system/bin/app_process"

    #@f0
    move-object/from16 v0, v24

    #@f2
    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@f5
    .line 113
    move-object/from16 v0, p1

    #@f7
    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@fa
    .line 114
    const-string v24, "com.lge.claybox.rpc.ClayboxServer"

    #@fc
    move-object/from16 v0, v24

    #@fe
    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@101
    .line 116
    new-instance v7, Ljava/lang/ProcessBuilder;

    #@103
    invoke-direct {v7, v10}, Ljava/lang/ProcessBuilder;-><init>(Ljava/util/List;)V

    #@106
    .line 117
    .local v7, builder:Ljava/lang/ProcessBuilder;
    invoke-virtual {v7}, Ljava/lang/ProcessBuilder;->environment()Ljava/util/Map;

    #@109
    move-result-object v24

    #@10a
    const-string v25, "CLASSPATH"

    #@10c
    move-object/from16 v0, v24

    #@10e
    move-object/from16 v1, v25

    #@110
    invoke-interface {v0, v1, v13}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@113
    .line 118
    new-instance v24, Ljava/io/File;

    #@115
    move-object/from16 v0, v24

    #@117
    move-object/from16 v1, p1

    #@119
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@11c
    move-object/from16 v0, v24

    #@11e
    invoke-virtual {v7, v0}, Ljava/lang/ProcessBuilder;->directory(Ljava/io/File;)Ljava/lang/ProcessBuilder;

    #@121
    .line 119
    move-object/from16 v0, p0

    #@123
    iget-object v0, v0, Lcom/lge/systemservice/service/ClayboxLauncher$1;->this$0:Lcom/lge/systemservice/service/ClayboxLauncher;

    #@125
    move-object/from16 v24, v0

    #@127
    invoke-virtual {v7}, Ljava/lang/ProcessBuilder;->start()Ljava/lang/Process;

    #@12a
    move-result-object v25

    #@12b
    invoke-static/range {v24 .. v25}, Lcom/lge/systemservice/service/ClayboxLauncher;->access$202(Lcom/lge/systemservice/service/ClayboxLauncher;Ljava/lang/Process;)Ljava/lang/Process;
    :try_end_12e
    .catch Ljava/lang/Exception; {:try_start_e9 .. :try_end_12e} :catch_137

    #@12e
    .line 126
    const/16 v24, 0x1

    #@130
    goto/16 :goto_55

    #@132
    .line 105
    .end local v7           #builder:Ljava/lang/ProcessBuilder;
    .end local v10           #clayboxprog:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :catch_132
    move-exception v11

    #@133
    .line 107
    .restart local v11       #e:Ljava/lang/Exception;
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V

    #@136
    goto :goto_e9

    #@137
    .line 120
    .end local v11           #e:Ljava/lang/Exception;
    :catch_137
    move-exception v11

    #@138
    .line 121
    .restart local v11       #e:Ljava/lang/Exception;
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V

    #@13b
    .line 123
    const/16 v24, 0x0

    #@13d
    goto/16 :goto_55

    #@13f
    .line 130
    .end local v11           #e:Ljava/lang/Exception;
    :cond_13f
    const-string v24, "ArtSystemServer"

    #@141
    new-instance v25, Ljava/lang/StringBuilder;

    #@143
    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    #@146
    const-string v26, "invalid claybox file!!! "

    #@148
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14b
    move-result-object v25

    #@14c
    move-object/from16 v0, v25

    #@14e
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@151
    move-result-object v25

    #@152
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@155
    move-result-object v25

    #@156
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@159
    .line 133
    const/16 v24, 0x0

    #@15b
    goto/16 :goto_55
.end method

.method public stopClaybox()V
    .registers 3

    #@0
    .prologue
    .line 139
    iget-object v0, p0, Lcom/lge/systemservice/service/ClayboxLauncher$1;->this$0:Lcom/lge/systemservice/service/ClayboxLauncher;

    #@2
    invoke-static {v0}, Lcom/lge/systemservice/service/ClayboxLauncher;->access$200(Lcom/lge/systemservice/service/ClayboxLauncher;)Ljava/lang/Process;

    #@5
    move-result-object v0

    #@6
    if-eqz v0, :cond_17

    #@8
    .line 141
    iget-object v0, p0, Lcom/lge/systemservice/service/ClayboxLauncher$1;->this$0:Lcom/lge/systemservice/service/ClayboxLauncher;

    #@a
    invoke-static {v0}, Lcom/lge/systemservice/service/ClayboxLauncher;->access$200(Lcom/lge/systemservice/service/ClayboxLauncher;)Ljava/lang/Process;

    #@d
    move-result-object v0

    #@e
    invoke-virtual {v0}, Ljava/lang/Process;->destroy()V

    #@11
    .line 142
    iget-object v0, p0, Lcom/lge/systemservice/service/ClayboxLauncher$1;->this$0:Lcom/lge/systemservice/service/ClayboxLauncher;

    #@13
    const/4 v1, 0x0

    #@14
    invoke-static {v0, v1}, Lcom/lge/systemservice/service/ClayboxLauncher;->access$202(Lcom/lge/systemservice/service/ClayboxLauncher;Ljava/lang/Process;)Ljava/lang/Process;

    #@17
    .line 144
    :cond_17
    return-void
.end method
