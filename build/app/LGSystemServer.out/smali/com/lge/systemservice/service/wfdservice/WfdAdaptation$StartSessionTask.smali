.class public Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$StartSessionTask;
.super Landroid/os/AsyncTask;
.source "WfdAdaptation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "StartSessionTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/net/wifi/p2p/WifiP2pDevice;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;


# direct methods
.method public constructor <init>(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 415
    iput-object p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$StartSessionTask;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@2
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Landroid/net/wifi/p2p/WifiP2pDevice;)Ljava/lang/Integer;
    .registers 7
    .parameter "devices"

    #@0
    .prologue
    .line 419
    const-string v2, "WfdAdaptation"

    #@2
    const-string v3, "StartSessionTask- doInBackground called"

    #@4
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 420
    const/4 v0, 0x0

    #@8
    .line 427
    .local v0, ret:I
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$StartSessionTask;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@a
    invoke-static {v2}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->access$200(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;

    #@d
    move-result-object v2

    #@e
    if-eqz v2, :cond_47

    #@10
    .line 428
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$StartSessionTask;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@12
    invoke-static {v2}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->access$200(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;

    #@15
    move-result-object v2

    #@16
    iget-object v3, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$StartSessionTask;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@18
    invoke-static {v3}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->access$400(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)Lcom/qualcomm/wfd/service/IWfdActionListener;

    #@1b
    move-result-object v3

    #@1c
    const/4 v4, 0x1

    #@1d
    invoke-static {v4}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->access$500(Z)Lcom/qualcomm/wfd/WfdDevice;

    #@20
    move-result-object v4

    #@21
    invoke-virtual {v2, v3, v4}, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->init(Lcom/qualcomm/wfd/service/IWfdActionListener;Lcom/qualcomm/wfd/WfdDevice;)I

    #@24
    .line 429
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$StartSessionTask;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@26
    invoke-static {v2}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->access$200(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;

    #@29
    move-result-object v2

    #@2a
    sget-object v3, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->SOURCE:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    #@2c
    invoke-virtual {v3}, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->getCode()I

    #@2f
    move-result v3

    #@30
    invoke-virtual {v2, v3}, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->setDeviceType(I)I

    #@33
    .line 430
    const/4 v2, 0x0

    #@34
    invoke-static {v2}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->access$500(Z)Lcom/qualcomm/wfd/WfdDevice;

    #@37
    move-result-object v1

    #@38
    .line 432
    .local v1, wfdDevice:Lcom/qualcomm/wfd/WfdDevice;
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$StartSessionTask;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@3a
    invoke-static {v2}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->access$200(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v2, v1}, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->startWfdSession(Lcom/qualcomm/wfd/WfdDevice;)I

    #@41
    move-result v0

    #@42
    .line 437
    .end local v1           #wfdDevice:Lcom/qualcomm/wfd/WfdDevice;
    :goto_42
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@45
    move-result-object v2

    #@46
    return-object v2

    #@47
    .line 435
    :cond_47
    const-string v2, "WfdAdaptation"

    #@49
    const-string v3, "StartSessionTask: mWfdSessionManager is null"

    #@4b
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    goto :goto_42
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 415
    check-cast p1, [Landroid/net/wifi/p2p/WifiP2pDevice;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$StartSessionTask;->doInBackground([Landroid/net/wifi/p2p/WifiP2pDevice;)Ljava/lang/Integer;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method protected onCancelled()V
    .registers 3

    #@0
    .prologue
    .line 447
    const-string v0, "WfdAdaptation"

    #@2
    const-string v1, "StartSessionTask- onCancelled called because startSessionTask returned"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 449
    return-void
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .registers 5
    .parameter "sessionId"

    #@0
    .prologue
    .line 453
    const-string v0, "WfdAdaptation"

    #@2
    const-string v1, "StartSessionTask- onPostExecute() called"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 454
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    #@a
    move-result v0

    #@b
    if-gez v0, :cond_26

    #@d
    .line 455
    const-string v0, "WfdAdaptation"

    #@f
    new-instance v1, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v2, "Failed to start session with error: "

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 462
    :goto_25
    return-void

    #@26
    .line 457
    :cond_26
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    #@29
    move-result v0

    #@2a
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->OPERATION_TIMED_OUT:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    #@2c
    invoke-virtual {v1}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->getCode()I

    #@2f
    move-result v1

    #@30
    if-ne v0, v1, :cond_3a

    #@32
    .line 458
    const-string v0, "WfdAdaptation"

    #@34
    const-string v1, "Start session is taking longer than expected"

    #@36
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    goto :goto_25

    #@3a
    .line 460
    :cond_3a
    const-string v0, "WfdAdaptation"

    #@3c
    new-instance v1, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v2, "StartSession with session ID: "

    #@43
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v1

    #@47
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v1

    #@4b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v1

    #@4f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    goto :goto_25
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 415
    check-cast p1, Ljava/lang/Integer;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$StartSessionTask;->onPostExecute(Ljava/lang/Integer;)V

    #@5
    return-void
.end method

.method protected onPreExecute()V
    .registers 1

    #@0
    .prologue
    .line 443
    return-void
.end method
