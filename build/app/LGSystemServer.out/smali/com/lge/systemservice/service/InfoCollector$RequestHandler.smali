.class Lcom/lge/systemservice/service/InfoCollector$RequestHandler;
.super Ljava/lang/Thread;
.source "InfoCollector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/InfoCollector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RequestHandler"
.end annotation


# instance fields
.field private mHttpContext:Lorg/apache/http/protocol/HttpContext;

.field private mHttpPost:Lorg/apache/http/client/methods/HttpPost;

.field final synthetic this$0:Lcom/lge/systemservice/service/InfoCollector;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/InfoCollector;Lorg/apache/http/client/methods/HttpPost;)V
    .registers 4
    .parameter
    .parameter "httpPost"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 924
    iput-object p1, p0, Lcom/lge/systemservice/service/InfoCollector$RequestHandler;->this$0:Lcom/lge/systemservice/service/InfoCollector;

    #@3
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@6
    .line 921
    iput-object v0, p0, Lcom/lge/systemservice/service/InfoCollector$RequestHandler;->mHttpContext:Lorg/apache/http/protocol/HttpContext;

    #@8
    .line 922
    iput-object v0, p0, Lcom/lge/systemservice/service/InfoCollector$RequestHandler;->mHttpPost:Lorg/apache/http/client/methods/HttpPost;

    #@a
    .line 926
    new-instance v0, Lorg/apache/http/protocol/BasicHttpContext;

    #@c
    invoke-direct {v0}, Lorg/apache/http/protocol/BasicHttpContext;-><init>()V

    #@f
    iput-object v0, p0, Lcom/lge/systemservice/service/InfoCollector$RequestHandler;->mHttpContext:Lorg/apache/http/protocol/HttpContext;

    #@11
    .line 927
    iput-object p2, p0, Lcom/lge/systemservice/service/InfoCollector$RequestHandler;->mHttpPost:Lorg/apache/http/client/methods/HttpPost;

    #@13
    .line 928
    return-void
.end method


# virtual methods
.method public run()V
    .registers 7

    #@0
    .prologue
    .line 932
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    #@3
    .line 935
    :try_start_3
    iget-object v3, p0, Lcom/lge/systemservice/service/InfoCollector$RequestHandler;->this$0:Lcom/lge/systemservice/service/InfoCollector;

    #@5
    iget-object v3, v3, Lcom/lge/systemservice/service/InfoCollector;->mHttpClient:Lorg/apache/http/impl/client/DefaultHttpClient;

    #@7
    iget-object v4, p0, Lcom/lge/systemservice/service/InfoCollector$RequestHandler;->mHttpPost:Lorg/apache/http/client/methods/HttpPost;

    #@9
    iget-object v5, p0, Lcom/lge/systemservice/service/InfoCollector$RequestHandler;->mHttpContext:Lorg/apache/http/protocol/HttpContext;

    #@b
    invoke-virtual {v3, v4, v5}, Lorg/apache/http/impl/client/DefaultHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;

    #@e
    move-result-object v2

    #@f
    .line 937
    .local v2, response:Lorg/apache/http/HttpResponse;
    invoke-interface {v2}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    #@12
    move-result-object v1

    #@13
    .line 938
    .local v1, entity:Lorg/apache/http/HttpEntity;
    if-eqz v1, :cond_1c

    #@15
    .line 939
    invoke-static {v1}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    #@18
    move-result-object v3

    #@19
    invoke-static {v3}, Lcom/lge/systemservice/service/InfoCollector;->access$1102(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1c
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_3 .. :try_end_1c} :catch_1d
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_1c} :catch_22

    #@1c
    .line 951
    .end local v1           #entity:Lorg/apache/http/HttpEntity;
    .end local v2           #response:Lorg/apache/http/HttpResponse;
    :cond_1c
    :goto_1c
    return-void

    #@1d
    .line 946
    :catch_1d
    move-exception v0

    #@1e
    .line 947
    .local v0, e:Lorg/apache/http/client/ClientProtocolException;
    invoke-virtual {v0}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    #@21
    goto :goto_1c

    #@22
    .line 948
    .end local v0           #e:Lorg/apache/http/client/ClientProtocolException;
    :catch_22
    move-exception v0

    #@23
    .line 949
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@26
    goto :goto_1c
.end method
