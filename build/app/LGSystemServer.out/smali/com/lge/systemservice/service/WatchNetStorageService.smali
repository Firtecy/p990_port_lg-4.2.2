.class Lcom/lge/systemservice/service/WatchNetStorageService;
.super Lcom/lge/systemservice/core/watchnetstoragemanager/IWatchNetStorageManager$Stub;
.source "WatchNetStorageService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;,
        Lcom/lge/systemservice/service/WatchNetStorageService$MediaScanThread;
    }
.end annotation


# static fields
.field public static isFirstRun:Z

.field private static mStorageManager:Landroid/os/storage/StorageManager;

.field private static mStorageVolumes:[Landroid/os/storage/StorageVolume;


# instance fields
.field private mContext:Landroid/content/Context;

.field mMediaScanRequestList:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/lge/systemservice/service/WatchNetStorageService$MediaScanThread;",
            ">;"
        }
    .end annotation
.end field

.field mMountList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mMountService:Landroid/os/storage/IMountService;

.field mWatchThreadList:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 77
    const/4 v0, 0x0

    #@2
    sput-boolean v0, Lcom/lge/systemservice/service/WatchNetStorageService;->isFirstRun:Z

    #@4
    .line 82
    sput-object v1, Lcom/lge/systemservice/service/WatchNetStorageService;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    #@6
    .line 83
    sput-object v1, Lcom/lge/systemservice/service/WatchNetStorageService;->mStorageManager:Landroid/os/storage/StorageManager;

    #@8
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 93
    invoke-direct {p0}, Lcom/lge/systemservice/core/watchnetstoragemanager/IWatchNetStorageManager$Stub;-><init>()V

    #@3
    .line 80
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Lcom/lge/systemservice/service/WatchNetStorageService;->mMountService:Landroid/os/storage/IMountService;

    #@6
    .line 85
    new-instance v0, Ljava/util/ArrayList;

    #@8
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@b
    iput-object v0, p0, Lcom/lge/systemservice/service/WatchNetStorageService;->mMountList:Ljava/util/List;

    #@d
    .line 86
    new-instance v0, Ljava/util/Hashtable;

    #@f
    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    #@12
    iput-object v0, p0, Lcom/lge/systemservice/service/WatchNetStorageService;->mWatchThreadList:Ljava/util/Map;

    #@14
    .line 185
    new-instance v0, Ljava/util/Hashtable;

    #@16
    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    #@19
    iput-object v0, p0, Lcom/lge/systemservice/service/WatchNetStorageService;->mMediaScanRequestList:Ljava/util/Map;

    #@1b
    .line 94
    iput-object p1, p0, Lcom/lge/systemservice/service/WatchNetStorageService;->mContext:Landroid/content/Context;

    #@1d
    .line 96
    invoke-direct {p0}, Lcom/lge/systemservice/service/WatchNetStorageService;->delete_NetStorage_RemainDirs()V

    #@20
    .line 98
    const/4 v0, 0x1

    #@21
    sput-boolean v0, Lcom/lge/systemservice/service/WatchNetStorageService;->isFirstRun:Z

    #@23
    .line 99
    iget-object v0, p0, Lcom/lge/systemservice/service/WatchNetStorageService;->mContext:Landroid/content/Context;

    #@25
    const-string v1, "storage"

    #@27
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@2a
    move-result-object v0

    #@2b
    check-cast v0, Landroid/os/storage/StorageManager;

    #@2d
    sput-object v0, Lcom/lge/systemservice/service/WatchNetStorageService;->mStorageManager:Landroid/os/storage/StorageManager;

    #@2f
    .line 100
    sget-object v0, Lcom/lge/systemservice/service/WatchNetStorageService;->mStorageManager:Landroid/os/storage/StorageManager;

    #@31
    invoke-virtual {v0}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    #@34
    move-result-object v0

    #@35
    sput-object v0, Lcom/lge/systemservice/service/WatchNetStorageService;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    #@37
    .line 102
    return-void
.end method

.method private IsmountOn(Ljava/lang/String;)Z
    .registers 11
    .parameter "mountPoint"

    #@0
    .prologue
    .line 387
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    #@2
    const-string v7, "/proc/mounts"

    #@4
    invoke-direct {v1, v7}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    #@7
    .line 388
    .local v1, fis:Ljava/io/FileInputStream;
    new-instance v4, Ljava/io/InputStreamReader;

    #@9
    const-string v7, "UTF-8"

    #@b
    invoke-direct {v4, v1, v7}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    #@e
    .line 389
    .local v4, isr:Ljava/io/InputStreamReader;
    new-instance v0, Ljava/io/BufferedReader;

    #@10
    invoke-direct {v0, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    #@13
    .line 393
    .local v0, br:Ljava/io/BufferedReader;
    :cond_13
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@16
    move-result-object v5

    #@17
    .line 394
    .local v5, line:Ljava/lang/String;
    if-nez v5, :cond_1e

    #@19
    .line 396
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    #@1c
    .line 417
    .end local v0           #br:Ljava/io/BufferedReader;
    .end local v1           #fis:Ljava/io/FileInputStream;
    .end local v4           #isr:Ljava/io/InputStreamReader;
    .end local v5           #line:Ljava/lang/String;
    :goto_1c
    const/4 v7, 0x0

    #@1d
    :goto_1d
    return v7

    #@1e
    .line 399
    .restart local v0       #br:Ljava/io/BufferedReader;
    .restart local v1       #fis:Ljava/io/FileInputStream;
    .restart local v4       #isr:Ljava/io/InputStreamReader;
    .restart local v5       #line:Ljava/lang/String;
    :cond_1e
    const-string v7, "\\040"

    #@20
    const-string v8, " "

    #@22
    invoke-virtual {v5, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@25
    move-result-object v5

    #@26
    .line 400
    invoke-virtual {v5, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@29
    move-result v7

    #@2a
    if-eqz v7, :cond_13

    #@2c
    .line 403
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_2f
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_2f} :catch_31
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_2f} :catch_3a
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_2f} :catch_43

    #@2f
    .line 404
    const/4 v7, 0x1

    #@30
    goto :goto_1d

    #@31
    .line 407
    .end local v0           #br:Ljava/io/BufferedReader;
    .end local v1           #fis:Ljava/io/FileInputStream;
    .end local v4           #isr:Ljava/io/InputStreamReader;
    .end local v5           #line:Ljava/lang/String;
    :catch_31
    move-exception v2

    #@32
    .line 408
    .local v2, fnfe:Ljava/io/FileNotFoundException;
    const-string v7, "WatchNetStorageService"

    #@34
    const-string v8, "IsmountOn:"

    #@36
    invoke-static {v7, v8, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@39
    goto :goto_1c

    #@3a
    .line 409
    .end local v2           #fnfe:Ljava/io/FileNotFoundException;
    :catch_3a
    move-exception v6

    #@3b
    .line 410
    .local v6, uee:Ljava/io/UnsupportedEncodingException;
    const-string v7, "WatchNetStorageService"

    #@3d
    const-string v8, "IsmountOn:"

    #@3f
    invoke-static {v7, v8, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@42
    goto :goto_1c

    #@43
    .line 411
    .end local v6           #uee:Ljava/io/UnsupportedEncodingException;
    :catch_43
    move-exception v3

    #@44
    .line 412
    .local v3, ioe:Ljava/io/IOException;
    const-string v7, "WatchNetStorageService"

    #@46
    const-string v8, "IsmountOn:"

    #@48
    invoke-static {v7, v8, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4b
    goto :goto_1c
.end method

.method private NFSUMount(Ljava/lang/String;)Z
    .registers 7
    .parameter "mountPoint"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 354
    :try_start_1
    invoke-direct {p0}, Lcom/lge/systemservice/service/WatchNetStorageService;->getMountService()Landroid/os/storage/IMountService;

    #@4
    move-result-object v1

    #@5
    .line 355
    .local v1, mountService:Landroid/os/storage/IMountService;
    const/4 v4, 0x1

    #@6
    invoke-interface {v1, p1, v4}, Landroid/os/storage/IMountService;->unmountNetStorage(Ljava/lang/String;Z)I
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_9} :catch_d

    #@9
    move-result v2

    #@a
    .line 357
    .local v2, ret:I
    if-nez v2, :cond_15

    #@c
    .line 363
    .end local v1           #mountService:Landroid/os/storage/IMountService;
    .end local v2           #ret:I
    :goto_c
    return v3

    #@d
    .line 359
    :catch_d
    move-exception v0

    #@e
    .line 360
    .local v0, e:Landroid/os/RemoteException;
    const-string v3, "WatchNetStorageService"

    #@10
    const-string v4, "Failed talking with mount service"

    #@12
    invoke-static {v3, v4, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@15
    .line 363
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_15
    const/4 v3, 0x0

    #@16
    goto :goto_c
.end method

.method static synthetic access$000()[Landroid/os/storage/StorageVolume;
    .registers 1

    #@0
    .prologue
    .line 65
    sget-object v0, Lcom/lge/systemservice/service/WatchNetStorageService;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/lge/systemservice/service/WatchNetStorageService;Ljava/lang/String;)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/lge/systemservice/service/WatchNetStorageService;->checkHangCall(Ljava/lang/String;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$200(Lcom/lge/systemservice/service/WatchNetStorageService;Ljava/lang/String;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/lge/systemservice/service/WatchNetStorageService;->IsmountOn(Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$300(Lcom/lge/systemservice/service/WatchNetStorageService;Ljava/lang/String;J)Z
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 65
    invoke-direct {p0, p1, p2, p3}, Lcom/lge/systemservice/service/WatchNetStorageService;->checkHang(Ljava/lang/String;J)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$400(Lcom/lge/systemservice/service/WatchNetStorageService;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/lge/systemservice/service/WatchNetStorageService;->sendHangBroadcast(I)V

    #@3
    return-void
.end method

.method static synthetic access$500(Lcom/lge/systemservice/service/WatchNetStorageService;Ljava/lang/String;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/lge/systemservice/service/WatchNetStorageService;->NFSUMount(Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private checkHang(Ljava/lang/String;J)Z
    .registers 14
    .parameter "mountPoint"
    .parameter "timeOut"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    .line 294
    move-object v4, p1

    #@3
    .line 296
    .local v4, mMountPoint:Ljava/lang/String;
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    #@6
    move-result-object v2

    #@7
    .line 297
    .local v2, executor:Ljava/util/concurrent/ExecutorService;
    new-instance v6, Ljava/util/concurrent/FutureTask;

    #@9
    new-instance v9, Lcom/lge/systemservice/service/WatchNetStorageService$1;

    #@b
    invoke-direct {v9, p0, v4}, Lcom/lge/systemservice/service/WatchNetStorageService$1;-><init>(Lcom/lge/systemservice/service/WatchNetStorageService;Ljava/lang/String;)V

    #@e
    invoke-direct {v6, v9}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    #@11
    .line 306
    .local v6, task:Ljava/util/concurrent/FutureTask;,"Ljava/util/concurrent/FutureTask<Ljava/lang/Integer;>;"
    :try_start_11
    invoke-interface {v2, v6}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    #@14
    .line 307
    sget-object v9, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    #@16
    invoke-virtual {v6, p2, p3, v9}, Ljava/util/concurrent/FutureTask;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    #@19
    move-result-object v5

    #@1a
    check-cast v5, Ljava/lang/Integer;

    #@1c
    .line 308
    .local v5, result:Ljava/lang/Integer;
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
    :try_end_1f
    .catchall {:try_start_11 .. :try_end_1f} :catchall_3d
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_11 .. :try_end_1f} :catch_2b
    .catch Ljava/lang/InterruptedException; {:try_start_11 .. :try_end_1f} :catch_31
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_11 .. :try_end_1f} :catch_37

    #@1f
    move-result v9

    #@20
    if-ne v9, v7, :cond_26

    #@22
    .line 330
    invoke-interface {v2}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    #@25
    .end local v5           #result:Ljava/lang/Integer;
    :goto_25
    return v7

    #@26
    .restart local v5       #result:Ljava/lang/Integer;
    :cond_26
    invoke-interface {v2}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    #@29
    move v7, v8

    #@2a
    goto :goto_25

    #@2b
    .line 313
    .end local v5           #result:Ljava/lang/Integer;
    :catch_2b
    move-exception v1

    #@2c
    .line 330
    .local v1, ex:Ljava/util/concurrent/TimeoutException;
    invoke-interface {v2}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    #@2f
    move v7, v8

    #@30
    goto :goto_25

    #@31
    .line 318
    .end local v1           #ex:Ljava/util/concurrent/TimeoutException;
    :catch_31
    move-exception v3

    #@32
    .line 330
    .local v3, ie:Ljava/lang/InterruptedException;
    invoke-interface {v2}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    #@35
    move v7, v8

    #@36
    goto :goto_25

    #@37
    .line 323
    .end local v3           #ie:Ljava/lang/InterruptedException;
    :catch_37
    move-exception v0

    #@38
    .line 330
    .local v0, ee:Ljava/util/concurrent/ExecutionException;
    invoke-interface {v2}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    #@3b
    move v7, v8

    #@3c
    goto :goto_25

    #@3d
    .end local v0           #ee:Ljava/util/concurrent/ExecutionException;
    :catchall_3d
    move-exception v7

    #@3e
    invoke-interface {v2}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    #@41
    throw v7
.end method

.method private checkHangCall(Ljava/lang/String;)I
    .registers 7
    .parameter "mountPoint"

    #@0
    .prologue
    .line 339
    new-instance v0, Ljava/io/File;

    #@2
    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@5
    .line 341
    .local v0, mFile:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    #@8
    move-result-wide v1

    #@9
    const-wide/16 v3, 0x0

    #@b
    cmp-long v1, v1, v3

    #@d
    if-eqz v1, :cond_11

    #@f
    .line 342
    const/4 v1, 0x1

    #@10
    .line 344
    :goto_10
    return v1

    #@11
    :cond_11
    const/4 v1, 0x0

    #@12
    goto :goto_10
.end method

.method private delete_NetStorage_RemainDirs()V
    .registers 11

    #@0
    .prologue
    .line 229
    const-string v5, "/storage/sdcard0/"

    #@2
    .line 230
    .local v5, sdPath:Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    #@4
    invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@7
    .line 231
    .local v1, dir:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    #@a
    move-result-object v0

    #@b
    .line 232
    .local v0, childFile:[Ljava/io/File;
    if-eqz v0, :cond_51

    #@d
    .line 233
    array-length v6, v0

    #@e
    .line 234
    .local v6, size:I
    if-lez v6, :cond_51

    #@10
    .line 235
    const/4 v4, 0x0

    #@11
    .local v4, i:I
    :goto_11
    if-ge v4, v6, :cond_51

    #@13
    .line 236
    aget-object v7, v0, v4

    #@15
    invoke-virtual {v7}, Ljava/io/File;->isDirectory()Z

    #@18
    move-result v7

    #@19
    if-eqz v7, :cond_49

    #@1b
    .line 237
    aget-object v7, v0, v4

    #@1d
    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    .line 238
    .local v2, dirName:Ljava/lang/String;
    const-string v7, "Network_folder"

    #@23
    invoke-virtual {v2, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@26
    move-result v7

    #@27
    if-eqz v7, :cond_49

    #@29
    .line 240
    :try_start_29
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    #@2c
    move-result-object v7

    #@2d
    new-instance v8, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v9, "rm -r "

    #@34
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v8

    #@38
    aget-object v9, v0, v4

    #@3a
    invoke-virtual {v9}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@3d
    move-result-object v9

    #@3e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v8

    #@42
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v8

    #@46
    invoke-virtual {v7, v8}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;
    :try_end_49
    .catch Ljava/io/IOException; {:try_start_29 .. :try_end_49} :catch_4c

    #@49
    .line 235
    .end local v2           #dirName:Ljava/lang/String;
    :cond_49
    :goto_49
    add-int/lit8 v4, v4, 0x1

    #@4b
    goto :goto_11

    #@4c
    .line 241
    .restart local v2       #dirName:Ljava/lang/String;
    :catch_4c
    move-exception v3

    #@4d
    .line 243
    .local v3, e:Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    #@50
    goto :goto_49

    #@51
    .line 250
    .end local v2           #dirName:Ljava/lang/String;
    .end local v3           #e:Ljava/io/IOException;
    .end local v4           #i:I
    .end local v6           #size:I
    :cond_51
    return-void
.end method

.method private getMountService()Landroid/os/storage/IMountService;
    .registers 4

    #@0
    .prologue
    .line 421
    iget-object v1, p0, Lcom/lge/systemservice/service/WatchNetStorageService;->mMountService:Landroid/os/storage/IMountService;

    #@2
    if-nez v1, :cond_12

    #@4
    .line 422
    const-string v1, "mount"

    #@6
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@9
    move-result-object v0

    #@a
    .line 423
    .local v0, service:Landroid/os/IBinder;
    if-eqz v0, :cond_15

    #@c
    .line 424
    invoke-static {v0}, Landroid/os/storage/IMountService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/storage/IMountService;

    #@f
    move-result-object v1

    #@10
    iput-object v1, p0, Lcom/lge/systemservice/service/WatchNetStorageService;->mMountService:Landroid/os/storage/IMountService;

    #@12
    .line 429
    .end local v0           #service:Landroid/os/IBinder;
    :cond_12
    :goto_12
    iget-object v1, p0, Lcom/lge/systemservice/service/WatchNetStorageService;->mMountService:Landroid/os/storage/IMountService;

    #@14
    return-object v1

    #@15
    .line 426
    .restart local v0       #service:Landroid/os/IBinder;
    :cond_15
    const-string v1, "WatchNetStorageService"

    #@17
    const-string v2, "Can\'t get mount service"

    #@19
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    goto :goto_12
.end method

.method private sendHangBroadcast(I)V
    .registers 4
    .parameter "session"

    #@0
    .prologue
    .line 434
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.server.action.MOUNT_HANG"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 435
    .local v0, mountHang:Landroid/content/Intent;
    const-string v1, "MOUNT_SESSON"

    #@9
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@c
    .line 436
    iget-object v1, p0, Lcom/lge/systemservice/service/WatchNetStorageService;->mContext:Landroid/content/Context;

    #@e
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@11
    .line 438
    return-void
.end method

.method private validatePermission(Ljava/lang/String;)V
    .registers 6
    .parameter "perm"

    #@0
    .prologue
    .line 105
    iget-object v0, p0, Lcom/lge/systemservice/service/WatchNetStorageService;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_1a

    #@8
    .line 106
    new-instance v0, Ljava/lang/SecurityException;

    #@a
    const-string v1, "Requires %s permission"

    #@c
    const/4 v2, 0x1

    #@d
    new-array v2, v2, [Ljava/lang/Object;

    #@f
    const/4 v3, 0x0

    #@10
    aput-object p1, v2, v3

    #@12
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@19
    throw v0

    #@1a
    .line 108
    :cond_1a
    return-void
.end method


# virtual methods
.method public addNetStorage(Ljava/lang/String;)Z
    .registers 5
    .parameter "mountPoint"

    #@0
    .prologue
    .line 206
    const-string v0, "android.permission.MOUNT_UNMOUNT_FILESYSTEMS"

    #@2
    invoke-direct {p0, v0}, Lcom/lge/systemservice/service/WatchNetStorageService;->validatePermission(Ljava/lang/String;)V

    #@5
    .line 209
    const-string v0, "/storage/sdcard0/Net"

    #@7
    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@a
    move-result v0

    #@b
    if-nez v0, :cond_2d

    #@d
    .line 211
    const-string v0, "WatchNetStorageService"

    #@f
    new-instance v1, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v2, "addNetStorage : unallowed mount point("

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    const-string v2, ")"

    #@20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 212
    const/4 v0, 0x0

    #@2c
    .line 216
    :goto_2c
    return v0

    #@2d
    .line 214
    :cond_2d
    iget-object v0, p0, Lcom/lge/systemservice/service/WatchNetStorageService;->mMountList:Ljava/util/List;

    #@2f
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@32
    .line 216
    const/4 v0, 0x1

    #@33
    goto :goto_2c
.end method

.method public checkFirstRunAfterBoot()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 283
    sget-boolean v1, Lcom/lge/systemservice/service/WatchNetStorageService;->isFirstRun:Z

    #@3
    if-eqz v1, :cond_8

    #@5
    .line 284
    sput-boolean v0, Lcom/lge/systemservice/service/WatchNetStorageService;->isFirstRun:Z

    #@7
    .line 285
    const/4 v0, 0x1

    #@8
    .line 288
    :cond_8
    return v0
.end method

.method public removeNetStorage(Ljava/lang/String;)Z
    .registers 3
    .parameter "mountPoint"

    #@0
    .prologue
    .line 220
    const-string v0, "android.permission.MOUNT_UNMOUNT_FILESYSTEMS"

    #@2
    invoke-direct {p0, v0}, Lcom/lge/systemservice/service/WatchNetStorageService;->validatePermission(Ljava/lang/String;)V

    #@5
    .line 225
    const/4 v0, 0x0

    #@6
    return v0
.end method

.method public requestMediaScanFile(Ljava/lang/String;I)Z
    .registers 9
    .parameter "path"
    .parameter "timeout"

    #@0
    .prologue
    .line 188
    const-string v1, "WatchNetStorageService"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "requestMediaScanFile:"

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    const-string v3, " delay:"

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 189
    iget-object v1, p0, Lcom/lge/systemservice/service/WatchNetStorageService;->mMediaScanRequestList:Ljava/util/Map;

    #@24
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@27
    move-result-object v0

    #@28
    check-cast v0, Lcom/lge/systemservice/service/WatchNetStorageService$MediaScanThread;

    #@2a
    .line 190
    .local v0, thread:Lcom/lge/systemservice/service/WatchNetStorageService$MediaScanThread;
    if-nez v0, :cond_40

    #@2c
    .line 192
    new-instance v0, Lcom/lge/systemservice/service/WatchNetStorageService$MediaScanThread;

    #@2e
    .end local v0           #thread:Lcom/lge/systemservice/service/WatchNetStorageService$MediaScanThread;
    iget-object v2, p0, Lcom/lge/systemservice/service/WatchNetStorageService;->mContext:Landroid/content/Context;

    #@30
    int-to-long v4, p2

    #@31
    move-object v1, p0

    #@32
    move-object v3, p1

    #@33
    invoke-direct/range {v0 .. v5}, Lcom/lge/systemservice/service/WatchNetStorageService$MediaScanThread;-><init>(Lcom/lge/systemservice/service/WatchNetStorageService;Landroid/content/Context;Ljava/lang/String;J)V

    #@36
    .line 193
    .restart local v0       #thread:Lcom/lge/systemservice/service/WatchNetStorageService$MediaScanThread;
    iget-object v1, p0, Lcom/lge/systemservice/service/WatchNetStorageService;->mMediaScanRequestList:Ljava/util/Map;

    #@38
    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3b
    .line 194
    invoke-virtual {v0}, Lcom/lge/systemservice/service/WatchNetStorageService$MediaScanThread;->start()V

    #@3e
    .line 202
    :goto_3e
    const/4 v1, 0x1

    #@3f
    return v1

    #@40
    .line 198
    :cond_40
    int-to-long v1, p2

    #@41
    invoke-virtual {v0, v1, v2}, Lcom/lge/systemservice/service/WatchNetStorageService$MediaScanThread;->setExpireTime(J)V

    #@44
    .line 199
    invoke-virtual {v0}, Lcom/lge/systemservice/service/WatchNetStorageService$MediaScanThread;->interrupt()V

    #@47
    goto :goto_3e
.end method

.method public startThread(I)Z
    .registers 6
    .parameter "session"

    #@0
    .prologue
    .line 255
    :try_start_0
    new-instance v1, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;

    #@2
    iget-object v2, p0, Lcom/lge/systemservice/service/WatchNetStorageService;->mMountList:Ljava/util/List;

    #@4
    invoke-direct {v1, p0, p1, v2}, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;-><init>(Lcom/lge/systemservice/service/WatchNetStorageService;ILjava/util/List;)V

    #@7
    .line 256
    .local v1, watchThread:Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;
    invoke-virtual {v1}, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->startThread()V

    #@a
    .line 258
    new-instance v2, Ljava/util/ArrayList;

    #@c
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@f
    iput-object v2, p0, Lcom/lge/systemservice/service/WatchNetStorageService;->mMountList:Ljava/util/List;

    #@11
    .line 259
    iget-object v2, p0, Lcom/lge/systemservice/service/WatchNetStorageService;->mWatchThreadList:Ljava/util/Map;

    #@13
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@16
    move-result-object v3

    #@17
    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1a
    .catch Ljava/lang/IllegalThreadStateException; {:try_start_0 .. :try_end_1a} :catch_1c

    #@1a
    .line 261
    const/4 v2, 0x1

    #@1b
    .line 264
    .end local v1           #watchThread:Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;
    :goto_1b
    return v2

    #@1c
    .line 262
    :catch_1c
    move-exception v0

    #@1d
    .line 264
    .local v0, itse:Ljava/lang/IllegalThreadStateException;
    const/4 v2, 0x0

    #@1e
    goto :goto_1b
.end method

.method public stopThread(I)Z
    .registers 5
    .parameter "session"

    #@0
    .prologue
    .line 269
    iget-object v1, p0, Lcom/lge/systemservice/service/WatchNetStorageService;->mWatchThreadList:Ljava/util/Map;

    #@2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5
    move-result-object v2

    #@6
    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;

    #@c
    .line 270
    .local v0, wt:Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;
    if-eqz v0, :cond_1c

    #@e
    .line 272
    invoke-virtual {v0}, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->stopThread()V

    #@11
    .line 273
    iget-object v1, p0, Lcom/lge/systemservice/service/WatchNetStorageService;->mWatchThreadList:Ljava/util/Map;

    #@13
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@16
    move-result-object v2

    #@17
    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@1a
    .line 274
    const/4 v1, 0x1

    #@1b
    .line 278
    :goto_1b
    return v1

    #@1c
    :cond_1c
    const/4 v1, 0x0

    #@1d
    goto :goto_1b
.end method
