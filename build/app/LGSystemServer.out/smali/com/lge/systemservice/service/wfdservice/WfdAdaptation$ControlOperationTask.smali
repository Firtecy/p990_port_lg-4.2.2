.class Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperationTask;
.super Landroid/os/AsyncTask;
.source "WfdAdaptation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ControlOperationTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;",
        "Ljava/lang/Void;",
        "Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;


# direct methods
.method private constructor <init>(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 271
    iput-object p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperationTask;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@2
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 271
    invoke-direct {p0, p1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperationTask;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)V

    #@3
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;)Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;
    .registers 7
    .parameter "controlOperations"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 274
    const-string v1, "WfdAdaptation"

    #@3
    const-string v2, "Control Operation- do in background started"

    #@5
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 275
    const/4 v0, -0x1

    #@9
    .line 276
    .local v0, ret:I
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$6;->$SwitchMap$com$lge$systemservice$service$wfdservice$WfdAdaptation$ControlOperation:[I

    #@b
    aget-object v2, p1, v4

    #@d
    invoke-virtual {v2}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->ordinal()I

    #@10
    move-result v2

    #@11
    aget v1, v1, v2

    #@13
    packed-switch v1, :pswitch_data_19a

    #@16
    .line 336
    const-string v1, "WfdAdaptation"

    #@18
    new-instance v2, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v3, "Unexpected Operation: "

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    aget-object v3, p1, v4

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v2

    #@2d
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 340
    :cond_30
    :goto_30
    aget-object v1, p1, v4

    #@32
    return-object v1

    #@33
    .line 278
    :pswitch_33
    const-string v1, "WfdAdaptation"

    #@35
    const-string v2, "Control Operation- play()"

    #@37
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 279
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperationTask;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@3c
    invoke-static {v1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->access$200(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;

    #@3f
    move-result-object v1

    #@40
    invoke-virtual {v1}, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->play()I

    #@43
    move-result v0

    #@44
    .line 280
    if-gez v0, :cond_30

    #@46
    .line 281
    const-string v1, "WfdAdaptation"

    #@48
    new-instance v2, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v3, "Error when calling play(): "

    #@4f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v2

    #@53
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@56
    move-result-object v2

    #@57
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v2

    #@5b
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    goto :goto_30

    #@5f
    .line 285
    :pswitch_5f
    const-string v1, "WfdAdaptation"

    #@61
    const-string v2, "Control Operation- pause()"

    #@63
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@66
    .line 286
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperationTask;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@68
    invoke-static {v1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->access$200(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;

    #@6b
    move-result-object v1

    #@6c
    invoke-virtual {v1}, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->pause()I

    #@6f
    move-result v0

    #@70
    .line 287
    if-gez v0, :cond_30

    #@72
    .line 288
    const-string v1, "WfdAdaptation"

    #@74
    new-instance v2, Ljava/lang/StringBuilder;

    #@76
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@79
    const-string v3, "Error when calling pause(): "

    #@7b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v2

    #@7f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@82
    move-result-object v2

    #@83
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@86
    move-result-object v2

    #@87
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8a
    goto :goto_30

    #@8b
    .line 292
    :pswitch_8b
    const-string v1, "WfdAdaptation"

    #@8d
    const-string v2, "Control Operation- standby()"

    #@8f
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@92
    .line 293
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperationTask;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@94
    invoke-static {v1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->access$200(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;

    #@97
    move-result-object v1

    #@98
    invoke-virtual {v1}, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->standby()I

    #@9b
    move-result v0

    #@9c
    .line 294
    if-gez v0, :cond_30

    #@9e
    .line 295
    const-string v1, "WfdAdaptation"

    #@a0
    new-instance v2, Ljava/lang/StringBuilder;

    #@a2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a5
    const-string v3, "Error when calling standby():"

    #@a7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v2

    #@ab
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v2

    #@af
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b2
    move-result-object v2

    #@b3
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b6
    goto/16 :goto_30

    #@b8
    .line 299
    :pswitch_b8
    const-string v1, "WfdAdaptation"

    #@ba
    const-string v2, "Control Operation- resume()/play()"

    #@bc
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@bf
    .line 300
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperationTask;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@c1
    invoke-static {v1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->access$200(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;

    #@c4
    move-result-object v1

    #@c5
    invoke-virtual {v1}, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->play()I

    #@c8
    move-result v0

    #@c9
    .line 301
    if-gez v0, :cond_30

    #@cb
    .line 302
    const-string v1, "WfdAdaptation"

    #@cd
    new-instance v2, Ljava/lang/StringBuilder;

    #@cf
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d2
    const-string v3, "Error when calling resume()/play(): "

    #@d4
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v2

    #@d8
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@db
    move-result-object v2

    #@dc
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@df
    move-result-object v2

    #@e0
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e3
    goto/16 :goto_30

    #@e5
    .line 306
    :pswitch_e5
    const-string v1, "WfdAdaptation"

    #@e7
    const-string v2, "Control Operation- startUibcSession()"

    #@e9
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ec
    .line 307
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperationTask;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@ee
    invoke-static {v1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->access$200(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;

    #@f1
    move-result-object v1

    #@f2
    invoke-virtual {v1}, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->startUibcSession()I

    #@f5
    move-result v0

    #@f6
    .line 308
    if-gez v0, :cond_30

    #@f8
    .line 309
    const-string v1, "WfdAdaptation"

    #@fa
    new-instance v2, Ljava/lang/StringBuilder;

    #@fc
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@ff
    const-string v3, "Error when calling startUibcSession(): "

    #@101
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@104
    move-result-object v2

    #@105
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@108
    move-result-object v2

    #@109
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10c
    move-result-object v2

    #@10d
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@110
    goto/16 :goto_30

    #@112
    .line 313
    :pswitch_112
    const-string v1, "WfdAdaptation"

    #@114
    const-string v2, "Control Operation- stopUibcSession()"

    #@116
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@119
    .line 314
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperationTask;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@11b
    invoke-static {v1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->access$200(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;

    #@11e
    move-result-object v1

    #@11f
    invoke-virtual {v1}, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->stopUibcSession()I

    #@122
    move-result v0

    #@123
    .line 315
    if-gez v0, :cond_30

    #@125
    .line 316
    const-string v1, "WfdAdaptation"

    #@127
    new-instance v2, Ljava/lang/StringBuilder;

    #@129
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12c
    const-string v3, "Error when calling stopUibcSession(): "

    #@12e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@131
    move-result-object v2

    #@132
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@135
    move-result-object v2

    #@136
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@139
    move-result-object v2

    #@13a
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@13d
    goto/16 :goto_30

    #@13f
    .line 321
    :pswitch_13f
    const-string v1, "WfdAdaptation"

    #@141
    const-string v2, "Control Operation- play()"

    #@143
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@146
    .line 322
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperationTask;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@148
    invoke-static {v1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->access$200(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;

    #@14b
    move-result-object v1

    #@14c
    invoke-virtual {v1}, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->play_transit()I

    #@14f
    move-result v0

    #@150
    .line 323
    if-gez v0, :cond_30

    #@152
    .line 324
    const-string v1, "WfdAdaptation"

    #@154
    new-instance v2, Ljava/lang/StringBuilder;

    #@156
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@159
    const-string v3, "Error when calling play(): "

    #@15b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15e
    move-result-object v2

    #@15f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@162
    move-result-object v2

    #@163
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@166
    move-result-object v2

    #@167
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@16a
    goto/16 :goto_30

    #@16c
    .line 328
    :pswitch_16c
    const-string v1, "WfdAdaptation"

    #@16e
    const-string v2, "Control Operation- pause()"

    #@170
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@173
    .line 329
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperationTask;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@175
    invoke-static {v1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->access$200(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;

    #@178
    move-result-object v1

    #@179
    invoke-virtual {v1}, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->pause_transit()I

    #@17c
    move-result v0

    #@17d
    .line 330
    if-gez v0, :cond_30

    #@17f
    .line 331
    const-string v1, "WfdAdaptation"

    #@181
    new-instance v2, Ljava/lang/StringBuilder;

    #@183
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@186
    const-string v3, "Error when calling pause(): "

    #@188
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18b
    move-result-object v2

    #@18c
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18f
    move-result-object v2

    #@190
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@193
    move-result-object v2

    #@194
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@197
    goto/16 :goto_30

    #@199
    .line 276
    nop

    #@19a
    :pswitch_data_19a
    .packed-switch 0x1
        :pswitch_33
        :pswitch_5f
        :pswitch_8b
        :pswitch_b8
        :pswitch_e5
        :pswitch_112
        :pswitch_13f
        :pswitch_16c
    .end packed-switch
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 271
    check-cast p1, [Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperationTask;->doInBackground([Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;)Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method protected onPostExecute(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;)V
    .registers 2
    .parameter "controlOperation"

    #@0
    .prologue
    .line 349
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 271
    check-cast p1, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperationTask;->onPostExecute(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;)V

    #@5
    return-void
.end method

.method protected onPreExecute()V
    .registers 1

    #@0
    .prologue
    .line 345
    return-void
.end method
