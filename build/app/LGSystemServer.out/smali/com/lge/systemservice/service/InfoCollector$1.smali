.class Lcom/lge/systemservice/service/InfoCollector$1;
.super Landroid/content/BroadcastReceiver;
.source "InfoCollector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/InfoCollector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/InfoCollector;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/InfoCollector;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 168
    iput-object p1, p0, Lcom/lge/systemservice/service/InfoCollector$1;->this$0:Lcom/lge/systemservice/service/InfoCollector;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 10
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 173
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 176
    .local v0, action:Ljava/lang/String;
    const-string v5, "android.intent.action.ANY_DATA_STATE"

    #@6
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v5

    #@a
    if-eqz v5, :cond_12

    #@c
    .line 178
    invoke-static {}, Lcom/lge/systemservice/service/InfoCollector;->access$000()Z

    #@f
    move-result v5

    #@10
    if-eqz v5, :cond_13

    #@12
    .line 238
    :cond_12
    :goto_12
    return-void

    #@13
    .line 187
    :cond_13
    const-string v5, "apnType"

    #@15
    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    .line 189
    .local v2, apnType:Ljava/lang/String;
    if-eqz v2, :cond_12

    #@1b
    .line 193
    const-string v5, "default"

    #@1d
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@20
    move-result v5

    #@21
    if-eqz v5, :cond_12

    #@23
    .line 203
    const-class v5, Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@25
    const-string v6, "state"

    #@27
    invoke-virtual {p2, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@2a
    move-result-object v6

    #@2b
    invoke-static {v5, v6}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@2e
    move-result-object v4

    #@2f
    check-cast v4, Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@31
    .line 206
    .local v4, state:Lcom/android/internal/telephony/PhoneConstants$DataState;
    const-string v5, "reason"

    #@33
    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@36
    move-result-object v3

    #@37
    .line 208
    .local v3, reason:Ljava/lang/String;
    const-string v5, "apn"

    #@39
    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@3c
    move-result-object v1

    #@3d
    .line 217
    .local v1, apnName:Ljava/lang/String;
    iget-object v5, p0, Lcom/lge/systemservice/service/InfoCollector$1;->this$0:Lcom/lge/systemservice/service/InfoCollector;

    #@3f
    invoke-static {v5}, Lcom/lge/systemservice/service/InfoCollector;->access$100(Lcom/lge/systemservice/service/InfoCollector;)Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@42
    move-result-object v5

    #@43
    if-eq v5, v4, :cond_12

    #@45
    .line 218
    iget-object v5, p0, Lcom/lge/systemservice/service/InfoCollector$1;->this$0:Lcom/lge/systemservice/service/InfoCollector;

    #@47
    invoke-static {v5, v4}, Lcom/lge/systemservice/service/InfoCollector;->access$102(Lcom/lge/systemservice/service/InfoCollector;Lcom/android/internal/telephony/PhoneConstants$DataState;)Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@4a
    .line 220
    sget-object v5, Lcom/android/internal/telephony/PhoneConstants$DataState;->DISCONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@4c
    if-eq v5, v4, :cond_12

    #@4e
    .line 222
    sget-object v5, Lcom/android/internal/telephony/PhoneConstants$DataState;->CONNECTING:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@50
    if-eq v5, v4, :cond_12

    #@52
    .line 224
    sget-object v5, Lcom/android/internal/telephony/PhoneConstants$DataState;->SUSPENDED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@54
    if-eq v5, v4, :cond_12

    #@56
    .line 226
    sget-object v5, Lcom/android/internal/telephony/PhoneConstants$DataState;->CONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@58
    if-ne v5, v4, :cond_12

    #@5a
    .line 227
    iget-object v5, p0, Lcom/lge/systemservice/service/InfoCollector$1;->this$0:Lcom/lge/systemservice/service/InfoCollector;

    #@5c
    invoke-static {v5}, Lcom/lge/systemservice/service/InfoCollector;->access$200(Lcom/lge/systemservice/service/InfoCollector;)Z

    #@5f
    move-result v5

    #@60
    if-eqz v5, :cond_12

    #@62
    .line 233
    iget-object v5, p0, Lcom/lge/systemservice/service/InfoCollector$1;->this$0:Lcom/lge/systemservice/service/InfoCollector;

    #@64
    invoke-static {v5}, Lcom/lge/systemservice/service/InfoCollector;->access$300(Lcom/lge/systemservice/service/InfoCollector;)V

    #@67
    goto :goto_12
.end method
