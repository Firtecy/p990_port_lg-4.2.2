.class Lcom/lge/systemservice/service/CliptrayService$2;
.super Landroid/os/Handler;
.source "CliptrayService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/CliptrayService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/CliptrayService;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/CliptrayService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 235
    iput-object p1, p0, Lcom/lge/systemservice/service/CliptrayService$2;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    .line 239
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    #@3
    .line 240
    iget v1, p1, Landroid/os/Message;->what:I

    #@5
    packed-switch v1, :pswitch_data_60

    #@8
    .line 278
    :goto_8
    return-void

    #@9
    .line 242
    :pswitch_9
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService$2;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@b
    invoke-static {v1}, Lcom/lge/systemservice/service/CliptrayService;->access$500(Lcom/lge/systemservice/service/CliptrayService;)V

    #@e
    goto :goto_8

    #@f
    .line 245
    :pswitch_f
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService$2;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@11
    invoke-static {v1}, Lcom/lge/systemservice/service/CliptrayService;->access$600(Lcom/lge/systemservice/service/CliptrayService;)V

    #@14
    goto :goto_8

    #@15
    .line 248
    :pswitch_15
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService$2;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@17
    invoke-static {v1}, Lcom/lge/systemservice/service/CliptrayService;->access$700(Lcom/lge/systemservice/service/CliptrayService;)V

    #@1a
    goto :goto_8

    #@1b
    .line 251
    :pswitch_1b
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService$2;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@1d
    invoke-static {v1}, Lcom/lge/systemservice/service/CliptrayService;->access$800(Lcom/lge/systemservice/service/CliptrayService;)V

    #@20
    goto :goto_8

    #@21
    .line 254
    :pswitch_21
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService$2;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@23
    iget v2, p1, Landroid/os/Message;->arg1:I

    #@25
    invoke-static {v1, v2}, Lcom/lge/systemservice/service/CliptrayService;->access$900(Lcom/lge/systemservice/service/CliptrayService;I)V

    #@28
    goto :goto_8

    #@29
    .line 257
    :pswitch_29
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@2b
    if-nez v1, :cond_34

    #@2d
    const/4 v0, 0x1

    #@2e
    .line 258
    .local v0, viewmode:Z
    :goto_2e
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService$2;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@30
    invoke-static {v1, v0}, Lcom/lge/systemservice/service/CliptrayService;->access$1000(Lcom/lge/systemservice/service/CliptrayService;Z)V

    #@33
    goto :goto_8

    #@34
    .line 257
    .end local v0           #viewmode:Z
    :cond_34
    const/4 v0, 0x0

    #@35
    goto :goto_2e

    #@36
    .line 261
    :pswitch_36
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService$2;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@38
    invoke-static {v1}, Lcom/lge/systemservice/service/CliptrayService;->access$200(Lcom/lge/systemservice/service/CliptrayService;)Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;

    #@3b
    move-result-object v1

    #@3c
    invoke-virtual {v1}, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->showCliptraycue()V

    #@3f
    goto :goto_8

    #@40
    .line 264
    :pswitch_40
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService$2;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@42
    invoke-static {v1}, Lcom/lge/systemservice/service/CliptrayService;->access$200(Lcom/lge/systemservice/service/CliptrayService;)Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;

    #@45
    move-result-object v1

    #@46
    invoke-virtual {v1}, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->showCliptraycueClose()V

    #@49
    goto :goto_8

    #@4a
    .line 267
    :pswitch_4a
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService$2;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@4c
    invoke-static {v1}, Lcom/lge/systemservice/service/CliptrayService;->access$200(Lcom/lge/systemservice/service/CliptrayService;)Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;

    #@4f
    move-result-object v1

    #@50
    invoke-virtual {v1}, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->hideCliptraycue()V

    #@53
    goto :goto_8

    #@54
    .line 270
    :pswitch_54
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService$2;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@56
    invoke-static {v1}, Lcom/lge/systemservice/service/CliptrayService;->access$1100(Lcom/lge/systemservice/service/CliptrayService;)V

    #@59
    goto :goto_8

    #@5a
    .line 273
    :pswitch_5a
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService$2;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@5c
    invoke-static {v1}, Lcom/lge/systemservice/service/CliptrayService;->access$1200(Lcom/lge/systemservice/service/CliptrayService;)V

    #@5f
    goto :goto_8

    #@60
    .line 240
    :pswitch_data_60
    .packed-switch 0x0
        :pswitch_9
        :pswitch_f
        :pswitch_15
        :pswitch_1b
        :pswitch_21
        :pswitch_29
        :pswitch_36
        :pswitch_40
        :pswitch_4a
        :pswitch_54
        :pswitch_5a
    .end packed-switch
.end method
