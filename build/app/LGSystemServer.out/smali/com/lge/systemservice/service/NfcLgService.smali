.class public Lcom/lge/systemservice/service/NfcLgService;
.super Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager$Stub;
.source "NfcLgService.java"


# static fields
.field private static mFactoryAdaptor:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mIsNfcModeOn:Z

.field private mNfcAdapterAddon:Lcom/lge/nfcaddon/NfcAdapterAddon;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 21
    const/4 v0, 0x0

    #@1
    sput-object v0, Lcom/lge/systemservice/service/NfcLgService;->mFactoryAdaptor:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;

    #@3
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 33
    invoke-direct {p0}, Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager$Stub;-><init>()V

    #@4
    .line 18
    iput-object v1, p0, Lcom/lge/systemservice/service/NfcLgService;->mContext:Landroid/content/Context;

    #@6
    .line 20
    const/4 v0, 0x0

    #@7
    iput-boolean v0, p0, Lcom/lge/systemservice/service/NfcLgService;->mIsNfcModeOn:Z

    #@9
    .line 22
    iput-object v1, p0, Lcom/lge/systemservice/service/NfcLgService;->mNfcAdapterAddon:Lcom/lge/nfcaddon/NfcAdapterAddon;

    #@b
    .line 35
    iput-object p1, p0, Lcom/lge/systemservice/service/NfcLgService;->mContext:Landroid/content/Context;

    #@d
    .line 39
    return-void
.end method


# virtual methods
.method public createNfcFactoryObj()Z
    .registers 4

    #@0
    .prologue
    .line 53
    const/4 v0, 0x0

    #@1
    .line 55
    .local v0, rd:Z
    const-string v1, "NfcLgService"

    #@3
    const-string v2, "createNfcFactoryObj::In"

    #@5
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 56
    sget-object v1, Lcom/lge/systemservice/service/NfcLgService;->mFactoryAdaptor:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;

    #@a
    if-eqz v1, :cond_29

    #@c
    .line 57
    sget-object v1, Lcom/lge/systemservice/service/NfcLgService;->mFactoryAdaptor:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;

    #@e
    invoke-virtual {v1}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->isReady()Z

    #@11
    move-result v1

    #@12
    const/4 v2, 0x1

    #@13
    if-ne v1, v2, :cond_27

    #@15
    .line 58
    const/4 v0, 0x1

    #@16
    .line 68
    :goto_16
    if-nez v0, :cond_21

    #@18
    .line 69
    new-instance v1, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;

    #@1a
    iget-object v2, p0, Lcom/lge/systemservice/service/NfcLgService;->mContext:Landroid/content/Context;

    #@1c
    invoke-direct {v1, v2}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;-><init>(Landroid/content/Context;)V

    #@1f
    sput-object v1, Lcom/lge/systemservice/service/NfcLgService;->mFactoryAdaptor:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;

    #@21
    .line 72
    :cond_21
    sget-object v1, Lcom/lge/systemservice/service/NfcLgService;->mFactoryAdaptor:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;

    #@23
    if-nez v1, :cond_2b

    #@25
    .line 73
    const/4 v1, 0x0

    #@26
    .line 75
    :goto_26
    return v1

    #@27
    .line 61
    :cond_27
    const/4 v0, 0x0

    #@28
    goto :goto_16

    #@29
    .line 65
    :cond_29
    const/4 v0, 0x0

    #@2a
    goto :goto_16

    #@2b
    .line 75
    :cond_2b
    sget-object v1, Lcom/lge/systemservice/service/NfcLgService;->mFactoryAdaptor:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;

    #@2d
    invoke-virtual {v1}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->isReady()Z

    #@30
    move-result v1

    #@31
    goto :goto_26
.end method

.method public handleNfcFactory(I[B)Ljava/lang/String;
    .registers 6
    .parameter "command"
    .parameter "retData"

    #@0
    .prologue
    .line 42
    const-string v0, "NfcLgService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "handleNfcFactory::command ==>"

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 44
    sget-object v0, Lcom/lge/systemservice/service/NfcLgService;->mFactoryAdaptor:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;

    #@1a
    if-eqz v0, :cond_23

    #@1c
    .line 45
    sget-object v0, Lcom/lge/systemservice/service/NfcLgService;->mFactoryAdaptor:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;

    #@1e
    invoke-virtual {v0, p1, p2}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->NfcTestCmd(I[B)Ljava/lang/String;

    #@21
    move-result-object v0

    #@22
    .line 48
    :goto_22
    return-object v0

    #@23
    :cond_23
    const/4 v0, 0x0

    #@24
    goto :goto_22
.end method

.method public sendNfcTestCommand(I[B)Z
    .registers 5
    .parameter "command"
    .parameter "response"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 131
    invoke-static {}, Lcom/lge/nfcaddon/NfcAdapterAddon;->getNfcAdapterAddon()Lcom/lge/nfcaddon/NfcAdapterAddon;

    #@3
    move-result-object v0

    #@4
    iput-object v0, p0, Lcom/lge/systemservice/service/NfcLgService;->mNfcAdapterAddon:Lcom/lge/nfcaddon/NfcAdapterAddon;

    #@6
    .line 133
    iget-object v0, p0, Lcom/lge/systemservice/service/NfcLgService;->mNfcAdapterAddon:Lcom/lge/nfcaddon/NfcAdapterAddon;

    #@8
    if-nez v0, :cond_13

    #@a
    .line 134
    const-string v0, "NfcLgService"

    #@c
    const-string v1, "sendNfcTestCommand::NfcAdapterAddon is null"

    #@e
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 135
    const/4 v0, 0x0

    #@12
    .line 138
    :goto_12
    return v0

    #@13
    :cond_13
    iget-object v0, p0, Lcom/lge/systemservice/service/NfcLgService;->mNfcAdapterAddon:Lcom/lge/nfcaddon/NfcAdapterAddon;

    #@15
    invoke-virtual {v0, p1, p2}, Lcom/lge/nfcaddon/NfcAdapterAddon;->sendNfcTestCommand(I[B)Z

    #@18
    move-result v0

    #@19
    goto :goto_12
.end method
