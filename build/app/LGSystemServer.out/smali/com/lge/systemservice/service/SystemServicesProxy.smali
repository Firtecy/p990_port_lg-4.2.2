.class public Lcom/lge/systemservice/service/SystemServicesProxy;
.super Landroid/app/Service;
.source "SystemServicesProxy.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 12
    const-class v0, Lcom/lge/systemservice/service/SystemServicesProxy;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/lge/systemservice/service/SystemServicesProxy;->TAG:Ljava/lang/String;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 11
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 3
    .parameter "intent"

    #@0
    .prologue
    .line 47
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .registers 15
    .parameter "intent"
    .parameter "flags"
    .parameter "startId"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 18
    if-eqz p1, :cond_6c

    #@4
    .line 19
    const-string v8, "command"

    #@6
    invoke-virtual {p1, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v1

    #@a
    .line 20
    .local v1, command:Ljava/lang/String;
    const-string v8, "args"

    #@c
    invoke-virtual {p1, v8}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    #@f
    move-result-object v0

    #@10
    .line 21
    .local v0, args:[Ljava/lang/String;
    if-eqz v1, :cond_6c

    #@12
    if-eqz v0, :cond_6c

    #@14
    .line 23
    :try_start_14
    sget-object v8, Lcom/lge/systemservice/service/SystemServicesProxy;->TAG:Ljava/lang/String;

    #@16
    new-instance v9, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v10, "command="

    #@1d
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v9

    #@21
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v9

    #@25
    const-string v10, " args="

    #@27
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v9

    #@2b
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@2e
    move-result-object v10

    #@2f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v9

    #@33
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v9

    #@37
    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 24
    const-string v8, "DisableOverlays"

    #@3c
    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3f
    move-result v8

    #@40
    if-eqz v8, :cond_6c

    #@42
    array-length v8, v0

    #@43
    if-ne v8, v6, :cond_6c

    #@45
    .line 25
    const/4 v8, 0x0

    #@46
    aget-object v8, v0, v8

    #@48
    invoke-static {v8}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    #@4b
    move-result v3

    #@4c
    .line 26
    .local v3, disable:Z
    const-string v8, "SurfaceFlinger"

    #@4e
    invoke-static {v8}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@51
    move-result-object v5

    #@52
    .line 27
    .local v5, flinger:Landroid/os/IBinder;
    if-eqz v5, :cond_6c

    #@54
    .line 28
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@57
    move-result-object v2

    #@58
    .line 29
    .local v2, data:Landroid/os/Parcel;
    const-string v8, "android.ui.ISurfaceComposer"

    #@5a
    invoke-virtual {v2, v8}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@5d
    .line 30
    if-eqz v3, :cond_71

    #@5f
    :goto_5f
    invoke-virtual {v2, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@62
    .line 31
    const/16 v6, 0x3f0

    #@64
    const/4 v7, 0x0

    #@65
    const/4 v8, 0x0

    #@66
    invoke-interface {v5, v6, v2, v7, v8}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@69
    .line 32
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
    :try_end_6c
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_6c} :catch_73

    #@6c
    .line 41
    .end local v0           #args:[Ljava/lang/String;
    .end local v1           #command:Ljava/lang/String;
    .end local v2           #data:Landroid/os/Parcel;
    .end local v3           #disable:Z
    .end local v5           #flinger:Landroid/os/IBinder;
    :cond_6c
    :goto_6c
    invoke-virtual {p0, p3}, Lcom/lge/systemservice/service/SystemServicesProxy;->stopSelfResult(I)Z

    #@6f
    .line 42
    const/4 v6, 0x2

    #@70
    return v6

    #@71
    .restart local v0       #args:[Ljava/lang/String;
    .restart local v1       #command:Ljava/lang/String;
    .restart local v2       #data:Landroid/os/Parcel;
    .restart local v3       #disable:Z
    .restart local v5       #flinger:Landroid/os/IBinder;
    :cond_71
    move v6, v7

    #@72
    .line 30
    goto :goto_5f

    #@73
    .line 36
    .end local v2           #data:Landroid/os/Parcel;
    .end local v3           #disable:Z
    .end local v5           #flinger:Landroid/os/IBinder;
    :catch_73
    move-exception v4

    #@74
    .line 37
    .local v4, e:Ljava/lang/Exception;
    sget-object v6, Lcom/lge/systemservice/service/SystemServicesProxy;->TAG:Ljava/lang/String;

    #@76
    new-instance v7, Ljava/lang/StringBuilder;

    #@78
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@7b
    const-string v8, "failed to execute command="

    #@7d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v7

    #@81
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v7

    #@85
    const-string v8, " args="

    #@87
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v7

    #@8b
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@8e
    move-result-object v8

    #@8f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v7

    #@93
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@96
    move-result-object v7

    #@97
    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@9a
    goto :goto_6c
.end method
