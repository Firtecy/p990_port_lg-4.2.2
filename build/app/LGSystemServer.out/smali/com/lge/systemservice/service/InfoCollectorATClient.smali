.class public Lcom/lge/systemservice/service/InfoCollectorATClient;
.super Ljava/lang/Object;
.source "InfoCollectorATClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/systemservice/service/InfoCollectorATClient$Response;
    }
.end annotation


# static fields
.field private static mSingletonATClient:Lcom/lge/systemservice/service/InfoCollectorATClient;


# instance fields
.field private mATService:Lcom/lge/android/atservice/client/ILGATCMDService;

.field private mBound:Z

.field private mConnection:Landroid/content/ServiceConnection;

.field private mContext:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 58
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 33
    new-instance v0, Lcom/lge/systemservice/service/InfoCollectorATClient$1;

    #@5
    invoke-direct {v0, p0}, Lcom/lge/systemservice/service/InfoCollectorATClient$1;-><init>(Lcom/lge/systemservice/service/InfoCollectorATClient;)V

    #@8
    iput-object v0, p0, Lcom/lge/systemservice/service/InfoCollectorATClient;->mConnection:Landroid/content/ServiceConnection;

    #@a
    .line 59
    iput-object p1, p0, Lcom/lge/systemservice/service/InfoCollectorATClient;->mContext:Landroid/content/Context;

    #@c
    .line 60
    const/4 v0, 0x0

    #@d
    iput-boolean v0, p0, Lcom/lge/systemservice/service/InfoCollectorATClient;->mBound:Z

    #@f
    .line 61
    invoke-direct {p0}, Lcom/lge/systemservice/service/InfoCollectorATClient;->bindATService()V

    #@12
    .line 62
    return-void
.end method

.method static synthetic access$002(Lcom/lge/systemservice/service/InfoCollectorATClient;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 17
    iput-boolean p1, p0, Lcom/lge/systemservice/service/InfoCollectorATClient;->mBound:Z

    #@2
    return p1
.end method

.method static synthetic access$102(Lcom/lge/systemservice/service/InfoCollectorATClient;Lcom/lge/android/atservice/client/ILGATCMDService;)Lcom/lge/android/atservice/client/ILGATCMDService;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 17
    iput-object p1, p0, Lcom/lge/systemservice/service/InfoCollectorATClient;->mATService:Lcom/lge/android/atservice/client/ILGATCMDService;

    #@2
    return-object p1
.end method

.method static synthetic access$200(Lcom/lge/systemservice/service/InfoCollectorATClient;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 17
    iget-object v0, p0, Lcom/lge/systemservice/service/InfoCollectorATClient;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method private bindATService()V
    .registers 5

    #@0
    .prologue
    .line 73
    iget-boolean v1, p0, Lcom/lge/systemservice/service/InfoCollectorATClient;->mBound:Z

    #@2
    if-nez v1, :cond_20

    #@4
    .line 74
    new-instance v0, Landroid/content/Intent;

    #@6
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@9
    .line 75
    .local v0, intent:Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    #@b
    const-string v2, "com.lge.android.atservice"

    #@d
    const-string v3, "com.lge.android.atservice.LGATCMDService"

    #@f
    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@12
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@15
    .line 78
    iget-object v1, p0, Lcom/lge/systemservice/service/InfoCollectorATClient;->mContext:Landroid/content/Context;

    #@17
    iget-object v2, p0, Lcom/lge/systemservice/service/InfoCollectorATClient;->mConnection:Landroid/content/ServiceConnection;

    #@19
    const/4 v3, 0x1

    #@1a
    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    #@1d
    move-result v1

    #@1e
    if-eqz v1, :cond_20

    #@20
    .line 87
    .end local v0           #intent:Landroid/content/Intent;
    :cond_20
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/lge/systemservice/service/InfoCollectorATClient;
    .registers 2
    .parameter "context"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    #@0
    .prologue
    .line 66
    sget-object v0, Lcom/lge/systemservice/service/InfoCollectorATClient;->mSingletonATClient:Lcom/lge/systemservice/service/InfoCollectorATClient;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 67
    new-instance v0, Lcom/lge/systemservice/service/InfoCollectorATClient;

    #@6
    invoke-direct {v0, p0}, Lcom/lge/systemservice/service/InfoCollectorATClient;-><init>(Landroid/content/Context;)V

    #@9
    sput-object v0, Lcom/lge/systemservice/service/InfoCollectorATClient;->mSingletonATClient:Lcom/lge/systemservice/service/InfoCollectorATClient;

    #@b
    .line 69
    :cond_b
    sget-object v0, Lcom/lge/systemservice/service/InfoCollectorATClient;->mSingletonATClient:Lcom/lge/systemservice/service/InfoCollectorATClient;

    #@d
    return-object v0
.end method


# virtual methods
.method public request(I[B)Lcom/lge/systemservice/service/InfoCollectorATClient$Response;
    .registers 8
    .parameter "action"
    .parameter "in"

    #@0
    .prologue
    .line 124
    const/4 v3, 0x0

    #@1
    .line 126
    .local v3, result:[B
    iget-boolean v4, p0, Lcom/lge/systemservice/service/InfoCollectorATClient;->mBound:Z

    #@3
    if-eqz v4, :cond_2a

    #@5
    .line 128
    :try_start_5
    new-instance v0, Landroid/os/Bundle;

    #@7
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@a
    .line 129
    .local v0, data:Landroid/os/Bundle;
    const-string v4, "action"

    #@c
    invoke-virtual {v0, v4, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@f
    .line 130
    const-string v4, "data"

    #@11
    invoke-virtual {v0, v4, p2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    #@14
    .line 131
    iget-object v4, p0, Lcom/lge/systemservice/service/InfoCollectorATClient;->mATService:Lcom/lge/android/atservice/client/ILGATCMDService;

    #@16
    invoke-interface {v4, v0}, Lcom/lge/android/atservice/client/ILGATCMDService;->request(Landroid/os/Bundle;)Landroid/os/Bundle;

    #@19
    move-result-object v2

    #@1a
    .line 132
    .local v2, response:Landroid/os/Bundle;
    if-eqz v2, :cond_2a

    #@1c
    const-string v4, "result"

    #@1e
    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@21
    move-result v4

    #@22
    if-nez v4, :cond_2a

    #@24
    .line 133
    const-string v4, "data"

    #@26
    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B
    :try_end_29
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_29} :catch_2e

    #@29
    move-result-object v3

    #@2a
    .line 142
    .end local v0           #data:Landroid/os/Bundle;
    .end local v2           #response:Landroid/os/Bundle;
    :cond_2a
    :goto_2a
    if-nez v3, :cond_33

    #@2c
    .line 143
    const/4 v4, 0x0

    #@2d
    .line 146
    :goto_2d
    return-object v4

    #@2e
    .line 135
    :catch_2e
    move-exception v1

    #@2f
    .line 136
    .local v1, ex:Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    #@32
    goto :goto_2a

    #@33
    .line 146
    .end local v1           #ex:Landroid/os/RemoteException;
    :cond_33
    new-instance v4, Lcom/lge/systemservice/service/InfoCollectorATClient$Response;

    #@35
    invoke-direct {v4, p0, v3}, Lcom/lge/systemservice/service/InfoCollectorATClient$Response;-><init>(Lcom/lge/systemservice/service/InfoCollectorATClient;[B)V

    #@38
    goto :goto_2d
.end method
