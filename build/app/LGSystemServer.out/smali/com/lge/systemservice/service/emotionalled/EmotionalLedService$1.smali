.class Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;
.super Landroid/content/BroadcastReceiver;
.source "EmotionalLedService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 238
    iput-object p1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 23
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 241
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v13

    #@4
    .line 242
    .local v13, action:Ljava/lang/String;
    const/16 v17, 0x0

    #@6
    .line 244
    .local v17, patternId:I
    const-string v1, "android.intent.action.SCREEN_ON"

    #@8
    invoke-virtual {v13, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_d5

    #@e
    .line 245
    invoke-static {}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$300()Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_1b

    #@14
    const-string v1, "EmotionalLed"

    #@16
    const-string v3, "ACTION_SCREEN_ON"

    #@18
    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 246
    :cond_1b
    move-object/from16 v0, p0

    #@1d
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@1f
    const/4 v3, 0x1

    #@20
    invoke-static {v1, v3}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$1002(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Z)Z

    #@23
    .line 249
    move-object/from16 v0, p0

    #@25
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@27
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$500(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Z

    #@2a
    move-result v1

    #@2b
    if-eqz v1, :cond_a5

    #@2d
    move-object/from16 v0, p0

    #@2f
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@31
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$1100(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Z

    #@34
    move-result v1

    #@35
    if-eqz v1, :cond_a5

    #@37
    move-object/from16 v0, p0

    #@39
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@3b
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$1200(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@3e
    move-result-object v1

    #@3f
    if-eqz v1, :cond_88

    #@41
    move-object/from16 v0, p0

    #@43
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@45
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$1200(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@48
    move-result-object v1

    #@49
    if-eqz v1, :cond_57

    #@4b
    move-object/from16 v0, p0

    #@4d
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@4f
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$1200(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@52
    move-result-object v1

    #@53
    iget v1, v1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->flags:I

    #@55
    if-eqz v1, :cond_88

    #@57
    :cond_57
    move-object/from16 v0, p0

    #@59
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@5b
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$1200(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@5e
    move-result-object v1

    #@5f
    if-eqz v1, :cond_a5

    #@61
    move-object/from16 v0, p0

    #@63
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@65
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$1200(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@68
    move-result-object v1

    #@69
    iget v1, v1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@6b
    const/4 v3, 0x3

    #@6c
    if-eq v1, v3, :cond_7b

    #@6e
    move-object/from16 v0, p0

    #@70
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@72
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$1200(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@75
    move-result-object v1

    #@76
    iget v1, v1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@78
    const/4 v3, 0x4

    #@79
    if-ne v1, v3, :cond_a5

    #@7b
    :cond_7b
    move-object/from16 v0, p0

    #@7d
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@7f
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$1200(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@82
    move-result-object v1

    #@83
    iget v1, v1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->flags:I

    #@85
    const/4 v3, 0x2

    #@86
    if-ne v1, v3, :cond_a5

    #@88
    .line 252
    :cond_88
    move-object/from16 v0, p0

    #@8a
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@8c
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$1300(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@8f
    move-result-object v1

    #@90
    if-eqz v1, :cond_9e

    #@92
    move-object/from16 v0, p0

    #@94
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@96
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$1300(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@99
    move-result-object v1

    #@9a
    iget-boolean v1, v1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->mExceptional:Z

    #@9c
    if-nez v1, :cond_a5

    #@9e
    .line 253
    :cond_9e
    move-object/from16 v0, p0

    #@a0
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@a2
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$1400(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)V

    #@a5
    .line 257
    :cond_a5
    move-object/from16 v0, p0

    #@a7
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@a9
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$1500(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Z

    #@ac
    move-result v1

    #@ad
    if-eqz v1, :cond_b9

    #@af
    .line 259
    move-object/from16 v0, p0

    #@b1
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@b3
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$1600(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)V

    #@b6
    move/from16 v6, v17

    #@b8
    .line 405
    .end local v17           #patternId:I
    .local v6, patternId:I
    :cond_b8
    :goto_b8
    return-void

    #@b9
    .line 261
    .end local v6           #patternId:I
    .restart local v17       #patternId:I
    :cond_b9
    move-object/from16 v0, p0

    #@bb
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@bd
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$1700(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Z

    #@c0
    move-result v1

    #@c1
    if-eqz v1, :cond_4aa

    #@c3
    .line 263
    move-object/from16 v0, p0

    #@c5
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@c7
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$1800(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)V

    #@ca
    .line 264
    move-object/from16 v0, p0

    #@cc
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@ce
    const/4 v3, 0x0

    #@cf
    invoke-static {v1, v3}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$1900(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Z)V

    #@d2
    move/from16 v6, v17

    #@d4
    .end local v17           #patternId:I
    .restart local v6       #patternId:I
    goto :goto_b8

    #@d5
    .line 267
    .end local v6           #patternId:I
    .restart local v17       #patternId:I
    :cond_d5
    const-string v1, "android.intent.action.SCREEN_OFF"

    #@d7
    invoke-virtual {v13, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@da
    move-result v1

    #@db
    if-eqz v1, :cond_122

    #@dd
    .line 268
    invoke-static {}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$300()Z

    #@e0
    move-result v1

    #@e1
    if-eqz v1, :cond_ea

    #@e3
    const-string v1, "EmotionalLed"

    #@e5
    const-string v3, "ACTION_SCREEN_OFF"

    #@e7
    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ea
    .line 269
    :cond_ea
    move-object/from16 v0, p0

    #@ec
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@ee
    const/4 v3, 0x0

    #@ef
    invoke-static {v1, v3}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$1002(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Z)Z

    #@f2
    .line 271
    move-object/from16 v0, p0

    #@f4
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@f6
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$1500(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Z

    #@f9
    move-result v1

    #@fa
    if-eqz v1, :cond_106

    #@fc
    .line 272
    move-object/from16 v0, p0

    #@fe
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@100
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$2000(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)V

    #@103
    move/from16 v6, v17

    #@105
    .end local v17           #patternId:I
    .restart local v6       #patternId:I
    goto :goto_b8

    #@106
    .line 274
    .end local v6           #patternId:I
    .restart local v17       #patternId:I
    :cond_106
    move-object/from16 v0, p0

    #@108
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@10a
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$1700(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Z

    #@10d
    move-result v1

    #@10e
    if-eqz v1, :cond_4aa

    #@110
    .line 276
    move-object/from16 v0, p0

    #@112
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@114
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$1800(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)V

    #@117
    .line 277
    move-object/from16 v0, p0

    #@119
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@11b
    const/4 v3, 0x0

    #@11c
    invoke-static {v1, v3}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$1900(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Z)V

    #@11f
    move/from16 v6, v17

    #@121
    .end local v17           #patternId:I
    .restart local v6       #patternId:I
    goto :goto_b8

    #@122
    .line 280
    .end local v6           #patternId:I
    .restart local v17       #patternId:I
    :cond_122
    const-string v1, "android.intent.action.PHONE_STATE"

    #@124
    invoke-virtual {v13, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@127
    move-result v1

    #@128
    if-eqz v1, :cond_170

    #@12a
    .line 281
    const-string v1, "state"

    #@12c
    move-object/from16 v0, p2

    #@12e
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@131
    move-result-object v19

    #@132
    .line 282
    .local v19, state:Ljava/lang/String;
    invoke-static {}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$300()Z

    #@135
    move-result v1

    #@136
    if-eqz v1, :cond_152

    #@138
    const-string v1, "EmotionalLed"

    #@13a
    new-instance v3, Ljava/lang/StringBuilder;

    #@13c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@13f
    const-string v4, "TelephonyManager.ACTION_PHONE_STATE_CHANGED : state="

    #@141
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@144
    move-result-object v3

    #@145
    move-object/from16 v0, v19

    #@147
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14a
    move-result-object v3

    #@14b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14e
    move-result-object v3

    #@14f
    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@152
    .line 283
    :cond_152
    if-eqz v19, :cond_167

    #@154
    .line 284
    move-object/from16 v0, p0

    #@156
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@158
    sget-object v3, Landroid/telephony/TelephonyManager;->EXTRA_STATE_RINGING:Ljava/lang/String;

    #@15a
    move-object/from16 v0, v19

    #@15c
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15f
    move-result v3

    #@160
    invoke-static {v1, v3}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$2102(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Z)Z

    #@163
    :goto_163
    move/from16 v6, v17

    #@165
    .line 288
    .end local v17           #patternId:I
    .restart local v6       #patternId:I
    goto/16 :goto_b8

    #@167
    .line 286
    .end local v6           #patternId:I
    .restart local v17       #patternId:I
    :cond_167
    move-object/from16 v0, p0

    #@169
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@16b
    const/4 v3, 0x0

    #@16c
    invoke-static {v1, v3}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$2102(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Z)Z

    #@16f
    goto :goto_163

    #@170
    .line 288
    .end local v19           #state:Ljava/lang/String;
    :cond_170
    const-string v1, "android.intent.action.USER_PRESENT"

    #@172
    invoke-virtual {v13, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@175
    move-result v1

    #@176
    if-eqz v1, :cond_189

    #@178
    .line 289
    invoke-static {}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$300()Z

    #@17b
    move-result v1

    #@17c
    if-eqz v1, :cond_4aa

    #@17e
    .line 290
    const-string v1, "EmotionalLed"

    #@180
    const-string v3, "ACTION_USER_PRESENT"

    #@182
    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@185
    move/from16 v6, v17

    #@187
    .end local v17           #patternId:I
    .restart local v6       #patternId:I
    goto/16 :goto_b8

    #@189
    .line 292
    .end local v6           #patternId:I
    .restart local v17       #patternId:I
    :cond_189
    const-string v1, "android.intent.action.BATTERY_CHANGED"

    #@18b
    invoke-virtual {v13, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18e
    move-result v1

    #@18f
    if-eqz v1, :cond_21a

    #@191
    .line 293
    const-string v1, "plugged"

    #@193
    const/4 v3, 0x0

    #@194
    move-object/from16 v0, p2

    #@196
    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@199
    move-result v18

    #@19a
    .line 294
    .local v18, plugType:I
    invoke-static {}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$300()Z

    #@19d
    move-result v1

    #@19e
    if-eqz v1, :cond_1ba

    #@1a0
    const-string v1, "EmotionalLed"

    #@1a2
    new-instance v3, Ljava/lang/StringBuilder;

    #@1a4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1a7
    const-string v4, "ACTION_BATTERY_CHANGED : plugged type="

    #@1a9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ac
    move-result-object v3

    #@1ad
    move/from16 v0, v18

    #@1af
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b2
    move-result-object v3

    #@1b3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b6
    move-result-object v3

    #@1b7
    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1ba
    .line 295
    :cond_1ba
    const/4 v1, 0x1

    #@1bb
    move/from16 v0, v18

    #@1bd
    if-eq v0, v1, :cond_1c9

    #@1bf
    const/4 v1, 0x2

    #@1c0
    move/from16 v0, v18

    #@1c2
    if-eq v0, v1, :cond_1c9

    #@1c4
    const/4 v1, 0x4

    #@1c5
    move/from16 v0, v18

    #@1c7
    if-ne v0, v1, :cond_20e

    #@1c9
    .line 298
    :cond_1c9
    move-object/from16 v0, p0

    #@1cb
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@1cd
    const/4 v3, 0x1

    #@1ce
    invoke-static {v1, v3}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$2202(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Z)Z

    #@1d1
    .line 303
    move-object/from16 v0, p0

    #@1d3
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@1d5
    move-object/from16 v0, p0

    #@1d7
    iget-object v3, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@1d9
    move-object/from16 v0, p2

    #@1db
    invoke-static {v3, v0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$2400(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Landroid/content/Intent;)I

    #@1de
    move-result v3

    #@1df
    invoke-static {v1, v3}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$2302(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;I)I

    #@1e2
    .line 304
    move-object/from16 v0, p0

    #@1e4
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@1e6
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$2500(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)I

    #@1e9
    move-result v1

    #@1ea
    move-object/from16 v0, p0

    #@1ec
    iget-object v3, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@1ee
    invoke-static {v3}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$2300(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)I

    #@1f1
    move-result v3

    #@1f2
    if-eq v1, v3, :cond_1fb

    #@1f4
    .line 305
    move-object/from16 v0, p0

    #@1f6
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@1f8
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$900(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)V

    #@1fb
    .line 307
    :cond_1fb
    move-object/from16 v0, p0

    #@1fd
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@1ff
    move-object/from16 v0, p0

    #@201
    iget-object v3, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@203
    invoke-static {v3}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$2300(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)I

    #@206
    move-result v3

    #@207
    invoke-static {v1, v3}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$2502(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;I)I

    #@20a
    move/from16 v6, v17

    #@20c
    .line 308
    .end local v17           #patternId:I
    .restart local v6       #patternId:I
    goto/16 :goto_b8

    #@20e
    .line 300
    .end local v6           #patternId:I
    .restart local v17       #patternId:I
    :cond_20e
    move-object/from16 v0, p0

    #@210
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@212
    const/4 v3, 0x0

    #@213
    invoke-static {v1, v3}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$2202(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Z)Z

    #@216
    move/from16 v6, v17

    #@218
    .line 301
    .end local v17           #patternId:I
    .restart local v6       #patternId:I
    goto/16 :goto_b8

    #@21a
    .line 308
    .end local v6           #patternId:I
    .end local v18           #plugType:I
    .restart local v17       #patternId:I
    :cond_21a
    const-string v1, "android.intent.action.ACTION_POWER_CONNECTED"

    #@21c
    invoke-virtual {v13, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21f
    move-result v1

    #@220
    if-eqz v1, :cond_26f

    #@222
    .line 309
    invoke-static {}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$300()Z

    #@225
    move-result v1

    #@226
    if-eqz v1, :cond_22f

    #@228
    const-string v1, "EmotionalLed"

    #@22a
    const-string v3, "ACTION_POWER_CONNECTED"

    #@22c
    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22f
    .line 310
    :cond_22f
    move-object/from16 v0, p0

    #@231
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@233
    const/4 v3, 0x1

    #@234
    invoke-static {v1, v3}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$2202(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Z)Z

    #@237
    .line 312
    const-string v1, "sys.allautotest.run"

    #@239
    const-string v3, "false"

    #@23b
    invoke-static {v1, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@23e
    move-result-object v1

    #@23f
    const-string v3, "false"

    #@241
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@244
    move-result v1

    #@245
    if-eqz v1, :cond_24e

    #@247
    .line 313
    move-object/from16 v0, p0

    #@249
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@24b
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$900(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)V

    #@24e
    .line 315
    :cond_24e
    move-object/from16 v0, p0

    #@250
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@252
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$1700(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Z

    #@255
    move-result v1

    #@256
    if-eqz v1, :cond_4aa

    #@258
    .line 316
    move-object/from16 v0, p0

    #@25a
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@25c
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$1800(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)V

    #@25f
    .line 318
    move-object/from16 v0, p0

    #@261
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@263
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@266
    move-result-wide v3

    #@267
    const/4 v5, 0x1

    #@268
    invoke-static {v1, v3, v4, v5}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$2600(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;JI)V

    #@26b
    move/from16 v6, v17

    #@26d
    .end local v17           #patternId:I
    .restart local v6       #patternId:I
    goto/16 :goto_b8

    #@26f
    .line 320
    .end local v6           #patternId:I
    .restart local v17       #patternId:I
    :cond_26f
    const-string v1, "android.intent.action.ACTION_POWER_DISCONNECTED"

    #@271
    invoke-virtual {v13, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@274
    move-result v1

    #@275
    if-eqz v1, :cond_2df

    #@277
    .line 321
    invoke-static {}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$300()Z

    #@27a
    move-result v1

    #@27b
    if-eqz v1, :cond_284

    #@27d
    const-string v1, "EmotionalLed"

    #@27f
    const-string v3, "ACTION_POWER_DISCONNECTED"

    #@281
    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@284
    .line 322
    :cond_284
    move-object/from16 v0, p0

    #@286
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@288
    const/4 v3, 0x0

    #@289
    invoke-static {v1, v3}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$2202(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Z)Z

    #@28c
    .line 324
    const-string v1, "sys.allautotest.run"

    #@28e
    const-string v3, "false"

    #@290
    invoke-static {v1, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@293
    move-result-object v1

    #@294
    const-string v3, "false"

    #@296
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@299
    move-result v1

    #@29a
    if-eqz v1, :cond_2a3

    #@29c
    .line 325
    move-object/from16 v0, p0

    #@29e
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@2a0
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$900(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)V

    #@2a3
    .line 327
    :cond_2a3
    move-object/from16 v0, p0

    #@2a5
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@2a7
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$1700(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Z

    #@2aa
    move-result v1

    #@2ab
    if-eqz v1, :cond_4aa

    #@2ad
    .line 329
    move-object/from16 v0, p0

    #@2af
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@2b1
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$1800(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)V

    #@2b4
    .line 330
    move-object/from16 v0, p0

    #@2b6
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@2b8
    const/4 v3, 0x0

    #@2b9
    invoke-static {v1, v3}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$1900(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Z)V

    #@2bc
    .line 333
    move-object/from16 v0, p0

    #@2be
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@2c0
    move-object/from16 v0, p0

    #@2c2
    iget-object v3, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@2c4
    invoke-static {v3}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$2700(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)[I

    #@2c7
    move-result-object v3

    #@2c8
    move-object/from16 v0, p0

    #@2ca
    iget-object v4, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@2cc
    invoke-static {v4}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$2700(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)[I

    #@2cf
    move-result-object v4

    #@2d0
    array-length v4, v4

    #@2d1
    add-int/lit8 v4, v4, -0x1

    #@2d3
    aget v3, v3, v4

    #@2d5
    add-int/lit8 v3, v3, 0x1

    #@2d7
    const/4 v4, 0x0

    #@2d8
    invoke-static {v1, v3, v4}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$2800(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;IZ)V

    #@2db
    move/from16 v6, v17

    #@2dd
    .end local v17           #patternId:I
    .restart local v6       #patternId:I
    goto/16 :goto_b8

    #@2df
    .line 335
    .end local v6           #patternId:I
    .restart local v17       #patternId:I
    :cond_2df
    const-string v1, "android.intent.action.ACTION_SHUTDOWN"

    #@2e1
    invoke-virtual {v13, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2e4
    move-result v1

    #@2e5
    if-eqz v1, :cond_30e

    #@2e7
    .line 336
    invoke-static {}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$300()Z

    #@2ea
    move-result v1

    #@2eb
    if-eqz v1, :cond_2f4

    #@2ed
    const-string v1, "EmotionalLed"

    #@2ef
    const-string v3, "Intent.ACTION_SHUTDOWN"

    #@2f1
    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2f4
    .line 337
    :cond_2f4
    move-object/from16 v0, p0

    #@2f6
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@2f8
    const/4 v3, 0x1

    #@2f9
    invoke-static {v1, v3}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$2902(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Z)Z

    #@2fc
    .line 338
    move-object/from16 v0, p0

    #@2fe
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@300
    const-string v2, "shutdown"

    #@302
    const/4 v3, 0x0

    #@303
    const/4 v4, 0x2

    #@304
    const/4 v5, 0x2

    #@305
    const/4 v6, 0x6

    #@306
    const/4 v7, 0x1

    #@307
    invoke-static/range {v1 .. v7}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$3000(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Ljava/lang/String;IIIIZ)V

    #@30a
    move/from16 v6, v17

    #@30c
    .end local v17           #patternId:I
    .restart local v6       #patternId:I
    goto/16 :goto_b8

    #@30e
    .line 341
    .end local v6           #patternId:I
    .restart local v17       #patternId:I
    :cond_30e
    const-string v1, "com.lge.android.intent.action.ACCESSORY_EVENT"

    #@310
    invoke-virtual {v13, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@313
    move-result v1

    #@314
    if-eqz v1, :cond_3db

    #@316
    .line 342
    invoke-static {}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$300()Z

    #@319
    move-result v1

    #@31a
    if-eqz v1, :cond_323

    #@31c
    const-string v1, "EmotionalLed"

    #@31e
    const-string v3, "IntentEx.ACTION_ACCESSORY_EVENT "

    #@320
    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@323
    .line 343
    :cond_323
    move-object/from16 v0, p0

    #@325
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@327
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$800(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Z

    #@32a
    move-result v1

    #@32b
    if-eqz v1, :cond_4aa

    #@32d
    .line 344
    move-object/from16 v0, p0

    #@32f
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@331
    const-string v3, "com.lge.android.intent.extra.ACCESSORY_STATE"

    #@333
    const/4 v4, 0x0

    #@334
    move-object/from16 v0, p2

    #@336
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@339
    move-result v3

    #@33a
    iput v3, v1, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mSmartcoverMode:I

    #@33c
    .line 346
    move-object/from16 v0, p0

    #@33e
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@340
    iget v1, v1, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mSmartcoverMode:I

    #@342
    const/4 v3, 0x6

    #@343
    if-ne v1, v3, :cond_37f

    #@345
    .line 347
    move-object/from16 v0, p0

    #@347
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@349
    const/4 v3, 0x1

    #@34a
    invoke-static {v1, v3}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$1002(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Z)Z

    #@34d
    .line 348
    move-object/from16 v0, p0

    #@34f
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@351
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$1700(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Z

    #@354
    move-result v1

    #@355
    if-eqz v1, :cond_374

    #@357
    move-object/from16 v0, p0

    #@359
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@35b
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$2200(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Z

    #@35e
    move-result v1

    #@35f
    if-eqz v1, :cond_374

    #@361
    .line 349
    move-object/from16 v0, p0

    #@363
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@365
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$1800(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)V

    #@368
    .line 351
    move-object/from16 v0, p0

    #@36a
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@36c
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@36f
    move-result-wide v3

    #@370
    const/4 v5, 0x1

    #@371
    invoke-static {v1, v3, v4, v5}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$2600(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;JI)V

    #@374
    .line 366
    :cond_374
    :goto_374
    move-object/from16 v0, p0

    #@376
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@378
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$700(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)V

    #@37b
    move/from16 v6, v17

    #@37d
    .end local v17           #patternId:I
    .restart local v6       #patternId:I
    goto/16 :goto_b8

    #@37f
    .line 353
    .end local v6           #patternId:I
    .restart local v17       #patternId:I
    :cond_37f
    move-object/from16 v0, p0

    #@381
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@383
    iget v1, v1, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mSmartcoverMode:I

    #@385
    const/4 v3, 0x5

    #@386
    if-ne v1, v3, :cond_391

    #@388
    .line 354
    move-object/from16 v0, p0

    #@38a
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@38c
    const/4 v3, 0x0

    #@38d
    invoke-static {v1, v3}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$1002(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Z)Z

    #@390
    goto :goto_374

    #@391
    .line 355
    :cond_391
    move-object/from16 v0, p0

    #@393
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@395
    iget v1, v1, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mSmartcoverMode:I

    #@397
    const/4 v3, 0x5

    #@398
    if-ne v1, v3, :cond_374

    #@39a
    .line 356
    move-object/from16 v0, p0

    #@39c
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@39e
    const/4 v3, 0x0

    #@39f
    invoke-static {v1, v3}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$1002(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Z)Z

    #@3a2
    .line 357
    move-object/from16 v0, p0

    #@3a4
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@3a6
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$1700(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Z

    #@3a9
    move-result v1

    #@3aa
    if-eqz v1, :cond_374

    #@3ac
    .line 359
    move-object/from16 v0, p0

    #@3ae
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@3b0
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$1800(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)V

    #@3b3
    .line 360
    move-object/from16 v0, p0

    #@3b5
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@3b7
    const/4 v3, 0x0

    #@3b8
    invoke-static {v1, v3}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$1900(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Z)V

    #@3bb
    .line 363
    move-object/from16 v0, p0

    #@3bd
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@3bf
    move-object/from16 v0, p0

    #@3c1
    iget-object v3, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@3c3
    invoke-static {v3}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$2700(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)[I

    #@3c6
    move-result-object v3

    #@3c7
    move-object/from16 v0, p0

    #@3c9
    iget-object v4, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@3cb
    invoke-static {v4}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$2700(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)[I

    #@3ce
    move-result-object v4

    #@3cf
    array-length v4, v4

    #@3d0
    add-int/lit8 v4, v4, -0x1

    #@3d2
    aget v3, v3, v4

    #@3d4
    add-int/lit8 v3, v3, 0x1

    #@3d6
    const/4 v4, 0x0

    #@3d7
    invoke-static {v1, v3, v4}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$2800(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;IZ)V

    #@3da
    goto :goto_374

    #@3db
    .line 368
    :cond_3db
    const-string v1, "android.media.VOLUME_CHANGED_ACTION"

    #@3dd
    invoke-virtual {v13, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3e0
    move-result v1

    #@3e1
    if-eqz v1, :cond_450

    #@3e3
    .line 369
    const-string v1, "android.media.EXTRA_VOLUME_STREAM_VALUE"

    #@3e5
    const/4 v3, 0x0

    #@3e6
    move-object/from16 v0, p2

    #@3e8
    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@3eb
    move-result v14

    #@3ec
    .line 370
    .local v14, index:I
    const-string v1, "android.media.EXTRA_PREV_VOLUME_STREAM_VALUE"

    #@3ee
    const/4 v3, 0x0

    #@3ef
    move-object/from16 v0, p2

    #@3f1
    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@3f4
    move-result v16

    #@3f5
    .line 371
    .local v16, oldIndex:I
    invoke-static {}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$300()Z

    #@3f8
    move-result v1

    #@3f9
    if-eqz v1, :cond_41f

    #@3fb
    const-string v1, "EmotionalLed"

    #@3fd
    new-instance v3, Ljava/lang/StringBuilder;

    #@3ff
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@402
    const-string v4, "Intent.VOLUME_CHANGED_ACTION:index="

    #@404
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@407
    move-result-object v3

    #@408
    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40b
    move-result-object v3

    #@40c
    const-string v4, " ,oldIndex="

    #@40e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@411
    move-result-object v3

    #@412
    move/from16 v0, v16

    #@414
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@417
    move-result-object v3

    #@418
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41b
    move-result-object v3

    #@41c
    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@41f
    .line 372
    :cond_41f
    move-object/from16 v0, p0

    #@421
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@423
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$2100(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Z

    #@426
    move-result v1

    #@427
    if-nez v1, :cond_4aa

    #@429
    move-object/from16 v0, p0

    #@42b
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@42d
    iget v1, v1, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mSmartcoverMode:I

    #@42f
    const/4 v3, 0x5

    #@430
    if-ne v1, v3, :cond_4aa

    #@432
    .line 374
    move/from16 v0, v16

    #@434
    if-eq v14, v0, :cond_4aa

    #@436
    .line 375
    const/4 v2, 0x0

    #@437
    .line 376
    .local v2, name:Ljava/lang/String;
    move/from16 v0, v16

    #@439
    if-le v14, v0, :cond_44b

    #@43b
    .line 377
    const/16 v6, 0xc

    #@43d
    .line 378
    .end local v17           #patternId:I
    .restart local v6       #patternId:I
    const-string v2, "volumeup"

    #@43f
    .line 383
    :goto_43f
    move-object/from16 v0, p0

    #@441
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@443
    const/4 v3, 0x0

    #@444
    const/4 v4, 0x1

    #@445
    const/4 v5, 0x2

    #@446
    invoke-static/range {v1 .. v6}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$3100(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Ljava/lang/String;IIII)V

    #@449
    goto/16 :goto_b8

    #@44b
    .line 380
    .end local v6           #patternId:I
    .restart local v17       #patternId:I
    :cond_44b
    const/16 v6, 0xd

    #@44d
    .line 381
    .end local v17           #patternId:I
    .restart local v6       #patternId:I
    const-string v2, "volumedown"

    #@44f
    goto :goto_43f

    #@450
    .line 387
    .end local v2           #name:Ljava/lang/String;
    .end local v6           #patternId:I
    .end local v14           #index:I
    .end local v16           #oldIndex:I
    .restart local v17       #patternId:I
    :cond_450
    const-string v1, "com.lge.android.intent.action.GNSS_ALERT_LED_CHANGED"

    #@452
    invoke-virtual {v13, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@455
    move-result v1

    #@456
    if-eqz v1, :cond_4aa

    #@458
    .line 388
    invoke-static {}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$300()Z

    #@45b
    move-result v1

    #@45c
    if-eqz v1, :cond_465

    #@45e
    const-string v1, "EmotionalLed"

    #@460
    const-string v3, "IntentEx.GNSS_LED_CHANGED"

    #@462
    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@465
    .line 389
    :cond_465
    const/4 v15, 0x0

    #@466
    .line 390
    .local v15, newGpsState:Z
    const-string v8, "gpsenabled"

    #@468
    .line 391
    .local v8, pkgName:Ljava/lang/String;
    const/4 v9, 0x0

    #@469
    .line 392
    .local v9, recordId:I
    const/16 v6, 0x66

    #@46b
    .line 393
    .end local v17           #patternId:I
    .restart local v6       #patternId:I
    const-string v1, "com.lge.android.intent.extra.EXTRA_GNSS_LED_STATE"

    #@46d
    const/4 v3, 0x0

    #@46e
    move-object/from16 v0, p2

    #@470
    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@473
    move-result v1

    #@474
    const/4 v3, 0x1

    #@475
    if-ne v1, v3, :cond_49f

    #@477
    const/4 v15, 0x1

    #@478
    .line 394
    :goto_478
    move-object/from16 v0, p0

    #@47a
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@47c
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$3200(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Z

    #@47f
    move-result v1

    #@480
    if-eq v1, v15, :cond_b8

    #@482
    .line 395
    move-object/from16 v0, p0

    #@484
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@486
    invoke-static {v1, v15}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$3202(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Z)Z

    #@489
    .line 396
    move-object/from16 v0, p0

    #@48b
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@48d
    invoke-static {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$3200(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Z

    #@490
    move-result v1

    #@491
    if-eqz v1, :cond_4a1

    #@493
    .line 397
    move-object/from16 v0, p0

    #@495
    iget-object v7, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@497
    const/4 v10, 0x1

    #@498
    const/4 v11, 0x2

    #@499
    move v12, v6

    #@49a
    invoke-static/range {v7 .. v12}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$3100(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Ljava/lang/String;IIII)V

    #@49d
    goto/16 :goto_b8

    #@49f
    .line 393
    :cond_49f
    const/4 v15, 0x0

    #@4a0
    goto :goto_478

    #@4a1
    .line 400
    :cond_4a1
    move-object/from16 v0, p0

    #@4a3
    iget-object v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@4a5
    invoke-static {v1, v8, v9}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$3300(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Ljava/lang/String;I)V

    #@4a8
    goto/16 :goto_b8

    #@4aa
    .end local v6           #patternId:I
    .end local v8           #pkgName:Ljava/lang/String;
    .end local v9           #recordId:I
    .end local v15           #newGpsState:Z
    .restart local v17       #patternId:I
    :cond_4aa
    move/from16 v6, v17

    #@4ac
    .end local v17           #patternId:I
    .restart local v6       #patternId:I
    goto/16 :goto_b8
.end method
