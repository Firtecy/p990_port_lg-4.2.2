.class synthetic Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$6;
.super Ljava/lang/Object;
.source "WfdAdaptation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$lge$systemservice$service$wfdservice$WfdAdaptation$ControlOperation:[I

.field static final synthetic $SwitchMap$com$qualcomm$wfd$WfdEnums$SessionState:[I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 276
    invoke-static {}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->values()[Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@3
    move-result-object v0

    #@4
    array-length v0, v0

    #@5
    new-array v0, v0, [I

    #@7
    sput-object v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$6;->$SwitchMap$com$lge$systemservice$service$wfdservice$WfdAdaptation$ControlOperation:[I

    #@9
    :try_start_9
    sget-object v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$6;->$SwitchMap$com$lge$systemservice$service$wfdservice$WfdAdaptation$ControlOperation:[I

    #@b
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->PLAY:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@d
    invoke-virtual {v1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->ordinal()I

    #@10
    move-result v1

    #@11
    const/4 v2, 0x1

    #@12
    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_14} :catch_da

    #@14
    :goto_14
    :try_start_14
    sget-object v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$6;->$SwitchMap$com$lge$systemservice$service$wfdservice$WfdAdaptation$ControlOperation:[I

    #@16
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->PAUSE:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@18
    invoke-virtual {v1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->ordinal()I

    #@1b
    move-result v1

    #@1c
    const/4 v2, 0x2

    #@1d
    aput v2, v0, v1
    :try_end_1f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_1f} :catch_d7

    #@1f
    :goto_1f
    :try_start_1f
    sget-object v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$6;->$SwitchMap$com$lge$systemservice$service$wfdservice$WfdAdaptation$ControlOperation:[I

    #@21
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->STANDBY:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@23
    invoke-virtual {v1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->ordinal()I

    #@26
    move-result v1

    #@27
    const/4 v2, 0x3

    #@28
    aput v2, v0, v1
    :try_end_2a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1f .. :try_end_2a} :catch_d4

    #@2a
    :goto_2a
    :try_start_2a
    sget-object v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$6;->$SwitchMap$com$lge$systemservice$service$wfdservice$WfdAdaptation$ControlOperation:[I

    #@2c
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->RESUME:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@2e
    invoke-virtual {v1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->ordinal()I

    #@31
    move-result v1

    #@32
    const/4 v2, 0x4

    #@33
    aput v2, v0, v1
    :try_end_35
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2a .. :try_end_35} :catch_d1

    #@35
    :goto_35
    :try_start_35
    sget-object v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$6;->$SwitchMap$com$lge$systemservice$service$wfdservice$WfdAdaptation$ControlOperation:[I

    #@37
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->START_UIBC:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@39
    invoke-virtual {v1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->ordinal()I

    #@3c
    move-result v1

    #@3d
    const/4 v2, 0x5

    #@3e
    aput v2, v0, v1
    :try_end_40
    .catch Ljava/lang/NoSuchFieldError; {:try_start_35 .. :try_end_40} :catch_ce

    #@40
    :goto_40
    :try_start_40
    sget-object v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$6;->$SwitchMap$com$lge$systemservice$service$wfdservice$WfdAdaptation$ControlOperation:[I

    #@42
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->STOP_UIBC:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@44
    invoke-virtual {v1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->ordinal()I

    #@47
    move-result v1

    #@48
    const/4 v2, 0x6

    #@49
    aput v2, v0, v1
    :try_end_4b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_40 .. :try_end_4b} :catch_cb

    #@4b
    :goto_4b
    :try_start_4b
    sget-object v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$6;->$SwitchMap$com$lge$systemservice$service$wfdservice$WfdAdaptation$ControlOperation:[I

    #@4d
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->PLAY_TRANSIT:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@4f
    invoke-virtual {v1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->ordinal()I

    #@52
    move-result v1

    #@53
    const/4 v2, 0x7

    #@54
    aput v2, v0, v1
    :try_end_56
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4b .. :try_end_56} :catch_c9

    #@56
    :goto_56
    :try_start_56
    sget-object v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$6;->$SwitchMap$com$lge$systemservice$service$wfdservice$WfdAdaptation$ControlOperation:[I

    #@58
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->PAUSE_TRANSIT:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@5a
    invoke-virtual {v1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->ordinal()I

    #@5d
    move-result v1

    #@5e
    const/16 v2, 0x8

    #@60
    aput v2, v0, v1
    :try_end_62
    .catch Ljava/lang/NoSuchFieldError; {:try_start_56 .. :try_end_62} :catch_c7

    #@62
    .line 103
    :goto_62
    invoke-static {}, Lcom/qualcomm/wfd/WfdEnums$SessionState;->values()[Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@65
    move-result-object v0

    #@66
    array-length v0, v0

    #@67
    new-array v0, v0, [I

    #@69
    sput-object v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$6;->$SwitchMap$com$qualcomm$wfd$WfdEnums$SessionState:[I

    #@6b
    :try_start_6b
    sget-object v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$6;->$SwitchMap$com$qualcomm$wfd$WfdEnums$SessionState:[I

    #@6d
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$SessionState;->INITIALIZED:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@6f
    invoke-virtual {v1}, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ordinal()I

    #@72
    move-result v1

    #@73
    const/4 v2, 0x1

    #@74
    aput v2, v0, v1
    :try_end_76
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6b .. :try_end_76} :catch_c5

    #@76
    :goto_76
    :try_start_76
    sget-object v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$6;->$SwitchMap$com$qualcomm$wfd$WfdEnums$SessionState:[I

    #@78
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$SessionState;->INVALID:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@7a
    invoke-virtual {v1}, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ordinal()I

    #@7d
    move-result v1

    #@7e
    const/4 v2, 0x2

    #@7f
    aput v2, v0, v1
    :try_end_81
    .catch Ljava/lang/NoSuchFieldError; {:try_start_76 .. :try_end_81} :catch_c3

    #@81
    :goto_81
    :try_start_81
    sget-object v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$6;->$SwitchMap$com$qualcomm$wfd$WfdEnums$SessionState:[I

    #@83
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$SessionState;->IDLE:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@85
    invoke-virtual {v1}, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ordinal()I

    #@88
    move-result v1

    #@89
    const/4 v2, 0x3

    #@8a
    aput v2, v0, v1
    :try_end_8c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_81 .. :try_end_8c} :catch_c1

    #@8c
    :goto_8c
    :try_start_8c
    sget-object v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$6;->$SwitchMap$com$qualcomm$wfd$WfdEnums$SessionState:[I

    #@8e
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PLAY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@90
    invoke-virtual {v1}, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ordinal()I

    #@93
    move-result v1

    #@94
    const/4 v2, 0x4

    #@95
    aput v2, v0, v1
    :try_end_97
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8c .. :try_end_97} :catch_bf

    #@97
    :goto_97
    :try_start_97
    sget-object v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$6;->$SwitchMap$com$qualcomm$wfd$WfdEnums$SessionState:[I

    #@99
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PAUSE:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@9b
    invoke-virtual {v1}, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ordinal()I

    #@9e
    move-result v1

    #@9f
    const/4 v2, 0x5

    #@a0
    aput v2, v0, v1
    :try_end_a2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_97 .. :try_end_a2} :catch_bd

    #@a2
    :goto_a2
    :try_start_a2
    sget-object v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$6;->$SwitchMap$com$qualcomm$wfd$WfdEnums$SessionState:[I

    #@a4
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ESTABLISHED:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@a6
    invoke-virtual {v1}, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ordinal()I

    #@a9
    move-result v1

    #@aa
    const/4 v2, 0x6

    #@ab
    aput v2, v0, v1
    :try_end_ad
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a2 .. :try_end_ad} :catch_bb

    #@ad
    :goto_ad
    :try_start_ad
    sget-object v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$6;->$SwitchMap$com$qualcomm$wfd$WfdEnums$SessionState:[I

    #@af
    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$SessionState;->TEARDOWN:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@b1
    invoke-virtual {v1}, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ordinal()I

    #@b4
    move-result v1

    #@b5
    const/4 v2, 0x7

    #@b6
    aput v2, v0, v1
    :try_end_b8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_ad .. :try_end_b8} :catch_b9

    #@b8
    :goto_b8
    return-void

    #@b9
    :catch_b9
    move-exception v0

    #@ba
    goto :goto_b8

    #@bb
    :catch_bb
    move-exception v0

    #@bc
    goto :goto_ad

    #@bd
    :catch_bd
    move-exception v0

    #@be
    goto :goto_a2

    #@bf
    :catch_bf
    move-exception v0

    #@c0
    goto :goto_97

    #@c1
    :catch_c1
    move-exception v0

    #@c2
    goto :goto_8c

    #@c3
    :catch_c3
    move-exception v0

    #@c4
    goto :goto_81

    #@c5
    :catch_c5
    move-exception v0

    #@c6
    goto :goto_76

    #@c7
    .line 276
    :catch_c7
    move-exception v0

    #@c8
    goto :goto_62

    #@c9
    :catch_c9
    move-exception v0

    #@ca
    goto :goto_56

    #@cb
    :catch_cb
    move-exception v0

    #@cc
    goto/16 :goto_4b

    #@ce
    :catch_ce
    move-exception v0

    #@cf
    goto/16 :goto_40

    #@d1
    :catch_d1
    move-exception v0

    #@d2
    goto/16 :goto_35

    #@d4
    :catch_d4
    move-exception v0

    #@d5
    goto/16 :goto_2a

    #@d7
    :catch_d7
    move-exception v0

    #@d8
    goto/16 :goto_1f

    #@da
    :catch_da
    move-exception v0

    #@db
    goto/16 :goto_14
.end method
