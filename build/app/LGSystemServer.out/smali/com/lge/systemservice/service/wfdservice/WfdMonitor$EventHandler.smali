.class Lcom/lge/systemservice/service/wfdservice/WfdMonitor$EventHandler;
.super Landroid/os/Handler;
.source "WfdMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/wfdservice/WfdMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EventHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/wfdservice/WfdMonitor;


# direct methods
.method public constructor <init>(Lcom/lge/systemservice/service/wfdservice/WfdMonitor;Landroid/os/Looper;)V
    .registers 3
    .parameter
    .parameter "looper"

    #@0
    .prologue
    .line 89
    iput-object p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdMonitor$EventHandler;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdMonitor;

    #@2
    .line 90
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 91
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    .line 94
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v0, :pswitch_data_48

    #@5
    .line 107
    :pswitch_5
    const-string v0, "WfdMonitor"

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v2, "Unhandled Message: "

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    iget v2, p1, Landroid/os/Message;->what:I

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 110
    :goto_1f
    return-void

    #@20
    .line 96
    :pswitch_20
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdMonitor$EventHandler;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdMonitor;

    #@22
    invoke-static {v0}, Lcom/lge/systemservice/service/wfdservice/WfdMonitor;->access$000(Lcom/lge/systemservice/service/wfdservice/WfdMonitor;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@25
    move-result-object v0

    #@26
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@28
    invoke-virtual {v0, v1}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->handleRtspStateChange(I)V

    #@2b
    goto :goto_1f

    #@2c
    .line 99
    :pswitch_2c
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdMonitor$EventHandler;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdMonitor;

    #@2e
    invoke-static {v0}, Lcom/lge/systemservice/service/wfdservice/WfdMonitor;->access$000(Lcom/lge/systemservice/service/wfdservice/WfdMonitor;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@31
    move-result-object v0

    #@32
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@34
    invoke-virtual {v0, v1}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->handleSessionStateChange(I)V

    #@37
    goto :goto_1f

    #@38
    .line 103
    :pswitch_38
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdMonitor$EventHandler;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdMonitor;

    #@3a
    invoke-static {v0}, Lcom/lge/systemservice/service/wfdservice/WfdMonitor;->access$000(Lcom/lge/systemservice/service/wfdservice/WfdMonitor;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@3d
    move-result-object v1

    #@3e
    iget v2, p1, Landroid/os/Message;->arg1:I

    #@40
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@42
    check-cast v0, Ljava/lang/String;

    #@44
    invoke-virtual {v1, v2, v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->handleWfdDlnaEvent(ILjava/lang/String;)V

    #@47
    goto :goto_1f

    #@48
    .line 94
    :pswitch_data_48
    .packed-switch 0x0
        :pswitch_20
        :pswitch_5
        :pswitch_2c
        :pswitch_38
    .end packed-switch
.end method
