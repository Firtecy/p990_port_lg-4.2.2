.class Lcom/lge/systemservice/service/CliptrayService$4;
.super Ljava/lang/Object;
.source "CliptrayService.java"

# interfaces
.implements Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnDeleteMode;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/CliptrayService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/CliptrayService;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/CliptrayService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 405
    iput-object p1, p0, Lcom/lge/systemservice/service/CliptrayService$4;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onDeleteModeEntered()V
    .registers 3

    #@0
    .prologue
    .line 408
    const-string v0, "CliptrayService"

    #@2
    const-string v1, "onDeleteModeEntered() :: change to delete mode"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 409
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$4;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@9
    invoke-static {v0}, Lcom/lge/systemservice/service/CliptrayService;->access$200(Lcom/lge/systemservice/service/CliptrayService;)Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;

    #@c
    move-result-object v0

    #@d
    invoke-virtual {v0}, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->changeToDeleteMode()V

    #@10
    .line 410
    return-void
.end method
