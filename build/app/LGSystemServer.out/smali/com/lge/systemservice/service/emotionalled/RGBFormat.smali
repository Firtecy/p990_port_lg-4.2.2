.class public Lcom/lge/systemservice/service/emotionalled/RGBFormat;
.super Ljava/lang/Object;
.source "RGBFormat.java"


# instance fields
.field private mcCtCtPpP:[I


# direct methods
.method public constructor <init>()V
    .registers 4

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 11
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 7
    const/4 v0, 0x4

    #@5
    new-array v0, v0, [I

    #@7
    iput-object v0, p0, Lcom/lge/systemservice/service/emotionalled/RGBFormat;->mcCtCtPpP:[I

    #@9
    .line 13
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/RGBFormat;->mcCtCtPpP:[I

    #@b
    const/4 v1, 0x0

    #@c
    aput v2, v0, v1

    #@e
    .line 14
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/RGBFormat;->mcCtCtPpP:[I

    #@10
    const/4 v1, 0x1

    #@11
    aput v2, v0, v1

    #@13
    .line 15
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/RGBFormat;->mcCtCtPpP:[I

    #@15
    const/4 v1, 0x2

    #@16
    const/16 v2, 0x64

    #@18
    aput v2, v0, v1

    #@1a
    .line 16
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/RGBFormat;->mcCtCtPpP:[I

    #@1c
    const/4 v1, 0x3

    #@1d
    const/16 v2, 0x14

    #@1f
    aput v2, v0, v1

    #@21
    .line 17
    return-void
.end method


# virtual methods
.method public getCurrentColor()I
    .registers 3

    #@0
    .prologue
    .line 55
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/RGBFormat;->mcCtCtPpP:[I

    #@2
    const/4 v1, 0x0

    #@3
    aget v0, v0, v1

    #@5
    return v0
.end method

.method public getDuration()I
    .registers 3

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/RGBFormat;->mcCtCtPpP:[I

    #@2
    const/4 v1, 0x2

    #@3
    aget v0, v0, v1

    #@5
    return v0
.end method

.method public getInterval()I
    .registers 3

    #@0
    .prologue
    .line 67
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/RGBFormat;->mcCtCtPpP:[I

    #@2
    const/4 v1, 0x3

    #@3
    aget v0, v0, v1

    #@5
    return v0
.end method

.method public getTargetColor()I
    .registers 3

    #@0
    .prologue
    .line 59
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/RGBFormat;->mcCtCtPpP:[I

    #@2
    const/4 v1, 0x1

    #@3
    aget v0, v0, v1

    #@5
    return v0
.end method

.method public setCurrentColorARGB(IIII)V
    .registers 11
    .parameter "Brightness"
    .parameter "R"
    .parameter "G"
    .parameter "B"

    #@0
    .prologue
    .line 37
    int-to-float v1, p1

    #@1
    const/high16 v2, 0x42c8

    #@3
    div-float v0, v1, v2

    #@5
    .line 38
    .local v0, intensity:F
    iget-object v1, p0, Lcom/lge/systemservice/service/emotionalled/RGBFormat;->mcCtCtPpP:[I

    #@7
    const/4 v2, 0x0

    #@8
    int-to-float v3, p2

    #@9
    mul-float/2addr v3, v0

    #@a
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    #@d
    move-result v3

    #@e
    int-to-float v4, p3

    #@f
    mul-float/2addr v4, v0

    #@10
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    #@13
    move-result v4

    #@14
    int-to-float v5, p4

    #@15
    mul-float/2addr v5, v0

    #@16
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    #@19
    move-result v5

    #@1a
    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    #@1d
    move-result v3

    #@1e
    aput v3, v1, v2

    #@20
    .line 39
    return-void
.end method

.method public setDuration(I)V
    .registers 4
    .parameter "dur"

    #@0
    .prologue
    .line 47
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/RGBFormat;->mcCtCtPpP:[I

    #@2
    const/4 v1, 0x2

    #@3
    aput p1, v0, v1

    #@5
    .line 48
    return-void
.end method

.method public setInterval(I)V
    .registers 4
    .parameter "itv"

    #@0
    .prologue
    .line 51
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/RGBFormat;->mcCtCtPpP:[I

    #@2
    const/4 v1, 0x3

    #@3
    aput p1, v0, v1

    #@5
    .line 52
    return-void
.end method

.method public setTargetColorARGB(IIII)V
    .registers 11
    .parameter "Brightness"
    .parameter "R"
    .parameter "G"
    .parameter "B"

    #@0
    .prologue
    .line 42
    int-to-float v1, p1

    #@1
    const/high16 v2, 0x42c8

    #@3
    div-float v0, v1, v2

    #@5
    .line 43
    .local v0, intensity:F
    iget-object v1, p0, Lcom/lge/systemservice/service/emotionalled/RGBFormat;->mcCtCtPpP:[I

    #@7
    const/4 v2, 0x1

    #@8
    int-to-float v3, p2

    #@9
    mul-float/2addr v3, v0

    #@a
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    #@d
    move-result v3

    #@e
    int-to-float v4, p3

    #@f
    mul-float/2addr v4, v0

    #@10
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    #@13
    move-result v4

    #@14
    int-to-float v5, p4

    #@15
    mul-float/2addr v5, v0

    #@16
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    #@19
    move-result v5

    #@1a
    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    #@1d
    move-result v3

    #@1e
    aput v3, v1, v2

    #@20
    .line 44
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 82
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "[CUR:"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Lcom/lge/systemservice/service/emotionalled/RGBFormat;->mcCtCtPpP:[I

    #@d
    const/4 v2, 0x0

    #@e
    aget v1, v1, v2

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v0

    #@14
    const-string v1, "] [TAR:"

    #@16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v0

    #@1a
    iget-object v1, p0, Lcom/lge/systemservice/service/emotionalled/RGBFormat;->mcCtCtPpP:[I

    #@1c
    const/4 v2, 0x1

    #@1d
    aget v1, v1, v2

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    const-string v1, "] [DUR:"

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    iget-object v1, p0, Lcom/lge/systemservice/service/emotionalled/RGBFormat;->mcCtCtPpP:[I

    #@2b
    const/4 v2, 0x2

    #@2c
    aget v1, v1, v2

    #@2e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@31
    move-result-object v0

    #@32
    const-string v1, "] [ITR:"

    #@34
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v0

    #@38
    iget-object v1, p0, Lcom/lge/systemservice/service/emotionalled/RGBFormat;->mcCtCtPpP:[I

    #@3a
    const/4 v2, 0x3

    #@3b
    aget v1, v1, v2

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    const-string v1, "]"

    #@43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v0

    #@47
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v0

    #@4b
    return-object v0
.end method
