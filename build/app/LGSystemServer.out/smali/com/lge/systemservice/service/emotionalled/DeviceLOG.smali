.class Lcom/lge/systemservice/service/emotionalled/DeviceLOG;
.super Ljava/lang/Object;
.source "RGBDevice.java"

# interfaces
.implements Lcom/lge/systemservice/service/emotionalled/RGBDevice;


# instance fields
.field className:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 68
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public interrupted()V
    .registers 1

    #@0
    .prologue
    .line 86
    return-void
.end method

.method public onColorUpdate(III)V
    .registers 7
    .parameter "color"
    .parameter "onMS"
    .parameter "offMS"

    #@0
    .prologue
    .line 76
    const-string v0, "className"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "color:"

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 77
    return-void
.end method

.method public onDeviceInit(Ljava/lang/Object;Landroid/view/View;)V
    .registers 4
    .parameter "obj"
    .parameter "view"

    #@0
    .prologue
    .line 72
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    iput-object v0, p0, Lcom/lge/systemservice/service/emotionalled/DeviceLOG;->className:Ljava/lang/String;

    #@6
    .line 73
    return-void
.end method

.method public onFinish(I)V
    .registers 2
    .parameter "whichPlay"

    #@0
    .prologue
    .line 83
    return-void
.end method
