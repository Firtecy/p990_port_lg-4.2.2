.class Lcom/lge/systemservice/service/CliptrayService$3;
.super Ljava/lang/Object;
.source "CliptrayService.java"

# interfaces
.implements Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnPasteInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/CliptrayService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/CliptrayService;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/CliptrayService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 382
    iput-object p1, p0, Lcom/lge/systemservice/service/CliptrayService$3;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onPasteClipData()V
    .registers 7

    #@0
    .prologue
    .line 386
    iget-object v2, p0, Lcom/lge/systemservice/service/CliptrayService$3;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@2
    invoke-static {v2}, Lcom/lge/systemservice/service/CliptrayService;->access$000(Lcom/lge/systemservice/service/CliptrayService;)Landroid/os/RemoteCallbackList;

    #@5
    move-result-object v3

    #@6
    monitor-enter v3

    #@7
    .line 388
    :try_start_7
    iget-object v2, p0, Lcom/lge/systemservice/service/CliptrayService$3;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@9
    invoke-static {v2}, Lcom/lge/systemservice/service/CliptrayService;->access$000(Lcom/lge/systemservice/service/CliptrayService;)Landroid/os/RemoteCallbackList;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    #@10
    move-result v1

    #@11
    .line 389
    .local v1, m:I
    iget-object v2, p0, Lcom/lge/systemservice/service/CliptrayService$3;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@13
    invoke-static {v2}, Lcom/lge/systemservice/service/CliptrayService;->access$400(Lcom/lge/systemservice/service/CliptrayService;)Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@16
    move-result-object v2

    #@17
    new-instance v4, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v5, "cliptray service : paste listener count = "

    #@1e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v4

    #@22
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25
    move-result-object v4

    #@26
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v4

    #@2a
    invoke-virtual {v2, v4}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->log(Ljava/lang/String;)V
    :try_end_2d
    .catchall {:try_start_7 .. :try_end_2d} :catchall_56

    #@2d
    .line 390
    const/4 v0, 0x0

    #@2e
    .local v0, i:I
    :goto_2e
    if-ge v0, v1, :cond_42

    #@30
    .line 392
    :try_start_30
    iget-object v2, p0, Lcom/lge/systemservice/service/CliptrayService$3;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@32
    invoke-static {v2}, Lcom/lge/systemservice/service/CliptrayService;->access$000(Lcom/lge/systemservice/service/CliptrayService;)Landroid/os/RemoteCallbackList;

    #@35
    move-result-object v2

    #@36
    invoke-virtual {v2, v0}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@39
    move-result-object v2

    #@3a
    check-cast v2, Lcom/lge/systemservice/core/cliptraymanager/IOnPasteListener;

    #@3c
    invoke-interface {v2}, Lcom/lge/systemservice/core/cliptraymanager/IOnPasteListener;->dispatchPaste()V
    :try_end_3f
    .catchall {:try_start_30 .. :try_end_3f} :catchall_56
    .catch Landroid/os/RemoteException; {:try_start_30 .. :try_end_3f} :catch_59

    #@3f
    .line 390
    :goto_3f
    add-int/lit8 v0, v0, 0x1

    #@41
    goto :goto_2e

    #@42
    .line 398
    :cond_42
    :try_start_42
    iget-object v2, p0, Lcom/lge/systemservice/service/CliptrayService$3;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@44
    invoke-static {v2}, Lcom/lge/systemservice/service/CliptrayService;->access$000(Lcom/lge/systemservice/service/CliptrayService;)Landroid/os/RemoteCallbackList;

    #@47
    move-result-object v2

    #@48
    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    #@4b
    .line 399
    monitor-exit v3
    :try_end_4c
    .catchall {:try_start_42 .. :try_end_4c} :catchall_56

    #@4c
    .line 401
    iget-object v2, p0, Lcom/lge/systemservice/service/CliptrayService$3;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@4e
    invoke-static {v2}, Lcom/lge/systemservice/service/CliptrayService;->access$200(Lcom/lge/systemservice/service/CliptrayService;)Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;

    #@51
    move-result-object v2

    #@52
    invoke-virtual {v2}, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->hideCliptraycue()V

    #@55
    .line 402
    return-void

    #@56
    .line 399
    .end local v0           #i:I
    .end local v1           #m:I
    :catchall_56
    move-exception v2

    #@57
    :try_start_57
    monitor-exit v3
    :try_end_58
    .catchall {:try_start_57 .. :try_end_58} :catchall_56

    #@58
    throw v2

    #@59
    .line 393
    .restart local v0       #i:I
    .restart local v1       #m:I
    :catch_59
    move-exception v2

    #@5a
    goto :goto_3f
.end method
