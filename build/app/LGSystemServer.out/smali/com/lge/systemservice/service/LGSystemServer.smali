.class public Lcom/lge/systemservice/service/LGSystemServer;
.super Landroid/app/Service;
.source "LGSystemServer.java"


# static fields
.field private static DEBUG:Z

.field private static SERVICE_LIST:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/lge/systemservice/core/LGServiceStubFetcher;",
            ">;"
        }
    .end annotation
.end field

.field static coreControlService:Lcom/lge/systemservice/service/CoreControlService;

.field static mBtLgeExtService:Lcom/lge/systemservice/service/BtLgeExtService;


# instance fields
.field private mHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    .line 62
    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    #@2
    const-string v2, "eng"

    #@4
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v1

    #@8
    sput-boolean v1, Lcom/lge/systemservice/service/LGSystemServer;->DEBUG:Z

    #@a
    .line 147
    new-instance v1, Ljava/util/ArrayList;

    #@c
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@f
    sput-object v1, Lcom/lge/systemservice/service/LGSystemServer;->SERVICE_LIST:Ljava/util/ArrayList;

    #@11
    .line 158
    new-instance v1, Lcom/lge/systemservice/service/LGSystemServer$2;

    #@13
    const-string v2, "byeworld"

    #@15
    const-string v3, "com.lge.software.byeworld"

    #@17
    invoke-direct {v1, v2, v3}, Lcom/lge/systemservice/service/LGSystemServer$2;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@1a
    invoke-static {v1}, Lcom/lge/systemservice/service/LGSystemServer;->registerServiceStub(Lcom/lge/systemservice/core/LGServiceStubFetcher;)V

    #@1d
    .line 166
    new-instance v1, Lcom/lge/systemservice/service/LGSystemServer$3;

    #@1f
    const-string v2, "infocollector"

    #@21
    const-string v3, "com.lge.software.infocollector"

    #@23
    invoke-direct {v1, v2, v3}, Lcom/lge/systemservice/service/LGSystemServer$3;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@26
    invoke-static {v1}, Lcom/lge/systemservice/service/LGSystemServer;->registerServiceStub(Lcom/lge/systemservice/core/LGServiceStubFetcher;)V

    #@29
    .line 174
    new-instance v1, Lcom/lge/systemservice/service/LGSystemServer$4;

    #@37
    const-string v2, "volumevibrator"

    #@39
    const-string v3, "com.lge.software.volumevibrator"

    #@3b
    invoke-direct {v1, v2, v3}, Lcom/lge/systemservice/service/LGSystemServer$5;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@3e
    invoke-static {v1}, Lcom/lge/systemservice/service/LGSystemServer;->registerServiceStub(Lcom/lge/systemservice/core/LGServiceStubFetcher;)V

    #@41
    .line 192
    new-instance v1, Lcom/lge/systemservice/service/LGSystemServer$6;

    #@43
    const-string v2, "watchnetstorage"

    #@45
    const-string v3, "com.lge.software.nfs"

    #@47
    invoke-direct {v1, v2, v3}, Lcom/lge/systemservice/service/LGSystemServer$6;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@4a
    invoke-static {v1}, Lcom/lge/systemservice/service/LGSystemServer;->registerServiceStub(Lcom/lge/systemservice/core/LGServiceStubFetcher;)V

    #@4d
    .line 201
    new-instance v1, Lcom/lge/systemservice/service/LGSystemServer$7;

    #@4f
    const-string v2, "lgsdencryption"

    #@51
    const-string v3, "com.lge.software.sdencryption"

    #@53
    invoke-direct {v1, v2, v3}, Lcom/lge/systemservice/service/LGSystemServer$7;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@56
    invoke-static {v1}, Lcom/lge/systemservice/service/LGSystemServer;->registerServiceStub(Lcom/lge/systemservice/core/LGServiceStubFetcher;)V

    #@59
    .line 210
    new-instance v1, Lcom/lge/systemservice/service/LGSystemServer$8;

    #@5b
    const-string v2, "AAT"

    #@5d
    invoke-direct {v1, v2}, Lcom/lge/systemservice/service/LGSystemServer$8;-><init>(Ljava/lang/String;)V

    #@60
    invoke-static {v1}, Lcom/lge/systemservice/service/LGSystemServer;->registerServiceStub(Lcom/lge/systemservice/core/LGServiceStubFetcher;)V

    #@63
    .line 218
    new-instance v1, Lcom/lge/systemservice/service/LGSystemServer$9;

    #@65
    const-string v2, "wifiLgeExtService"

    #@67
    invoke-direct {v1, v2}, Lcom/lge/systemservice/service/LGSystemServer$9;-><init>(Ljava/lang/String;)V

    #@6a
    invoke-static {v1}, Lcom/lge/systemservice/service/LGSystemServer;->registerServiceStub(Lcom/lge/systemservice/core/LGServiceStubFetcher;)V

    #@6d
    .line 226
    new-instance v1, Lcom/lge/systemservice/service/LGSystemServer$10;

    #@6f
    const-string v2, "BtLgeExt"

    #@71
    invoke-direct {v1, v2}, Lcom/lge/systemservice/service/LGSystemServer$10;-><init>(Ljava/lang/String;)V

    #@74
    invoke-static {v1}, Lcom/lge/systemservice/service/LGSystemServer;->registerServiceStub(Lcom/lge/systemservice/core/LGServiceStubFetcher;)V

    #@77
    .line 243
    new-instance v1, Lcom/lge/systemservice/service/LGSystemServer$11;

    #@79
    const-string v2, "wfdService"

    #@7b
    const-string v3, "com.lge.software.wfdService"

    #@7d
    invoke-direct {v1, v2, v3}, Lcom/lge/systemservice/service/LGSystemServer$11;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@80
    invoke-static {v1}, Lcom/lge/systemservice/service/LGSystemServer;->registerServiceStub(Lcom/lge/systemservice/core/LGServiceStubFetcher;)V

    #@83
    .line 252
    new-instance v1, Lcom/lge/systemservice/service/LGSystemServer$12;

    #@85
    const-string v2, "wfdSessionService"

    #@87
    const-string v3, "com.lge.software.wfdSessionService"

    #@89
    invoke-direct {v1, v2, v3}, Lcom/lge/systemservice/service/LGSystemServer$12;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@8c
    invoke-static {v1}, Lcom/lge/systemservice/service/LGSystemServer;->registerServiceStub(Lcom/lge/systemservice/core/LGServiceStubFetcher;)V

    #@8f
    .line 261
    const-string v1, "ro.build.target_operator"

    #@91
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@94
    move-result-object v0

    #@95
    .line 263
    .local v0, operator_tmp:Ljava/lang/String;
    if-eqz v0, :cond_b1

    #@97
    const-string v1, "DCM"

    #@99
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9c
    move-result v1

    #@9d
    if-nez v1, :cond_a7

    #@9f
    const-string v1, "KDDI"

    #@a1
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a4
    move-result v1

    #@a5
    if-eqz v1, :cond_b1

    #@a7
    .line 264
    :cond_a7
    new-instance v1, Lcom/lge/systemservice/service/LGSystemServer$13;

    #@a9
    const-string v2, "FeliCaService"

    #@ab
    invoke-direct {v1, v2}, Lcom/lge/systemservice/service/LGSystemServer$13;-><init>(Ljava/lang/String;)V

    #@ae
    invoke-static {v1}, Lcom/lge/systemservice/service/LGSystemServer;->registerServiceStub(Lcom/lge/systemservice/core/LGServiceStubFetcher;)V

    #@b1
    .line 272
    :cond_b1
    new-instance v1, Lcom/lge/systemservice/service/LGSystemServer$14;

    #@d3
    const-string v2, "cliptray"

    #@d5
    const-string v3, "com.lge.software.cliptray"

    #@d7
    invoke-direct {v1, v2, v3}, Lcom/lge/systemservice/service/LGSystemServer$16;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@da
    invoke-static {v1}, Lcom/lge/systemservice/service/LGSystemServer;->registerServiceStub(Lcom/lge/systemservice/core/LGServiceStubFetcher;)V

    #@dd
    .line 299
    new-instance v1, Lcom/lge/systemservice/service/LGSystemServer$17;

    #@df
    const-string v2, "corecontrol"

    #@e1
    invoke-direct {v1, v2}, Lcom/lge/systemservice/service/LGSystemServer$17;-><init>(Ljava/lang/String;)V

    #@e4
    invoke-static {v1}, Lcom/lge/systemservice/service/LGSystemServer;->registerServiceStub(Lcom/lge/systemservice/core/LGServiceStubFetcher;)V

    #@e7
    .line 324
    if-eqz v0, :cond_fb

    #@e9
    const-string v1, "VZW"

    #@eb
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ee
    move-result v1

    #@ef
    if-eqz v1, :cond_fb

    #@f1
    .line 325
    new-instance v1, Lcom/lge/systemservice/service/LGSystemServer$18;

    #@f3
    const-string v2, "wifiLgeVZWService"

    #@f5
    invoke-direct {v1, v2}, Lcom/lge/systemservice/service/LGSystemServer$18;-><init>(Ljava/lang/String;)V

    #@f8
    invoke-static {v1}, Lcom/lge/systemservice/service/LGSystemServer;->registerServiceStub(Lcom/lge/systemservice/core/LGServiceStubFetcher;)V

    #@fb
    .line 336
    :cond_fb
    new-instance v1, Lcom/lge/systemservice/service/LGSystemServer$19;

    #@fd
    const-string v2, "logcatcher"

    #@ff
    invoke-direct {v1, v2}, Lcom/lge/systemservice/service/LGSystemServer$19;-><init>(Ljava/lang/String;)V

    #@102
    invoke-static {v1}, Lcom/lge/systemservice/service/LGSystemServer;->registerServiceStub(Lcom/lge/systemservice/core/LGServiceStubFetcher;)V

    #@105
    .line 342
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 55
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    #@3
    .line 58
    new-instance v0, Landroid/os/Handler;

    #@5
    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    #@8
    iput-object v0, p0, Lcom/lge/systemservice/service/LGSystemServer;->mHandler:Landroid/os/Handler;

    #@a
    return-void
.end method

.method static synthetic access$000(Lcom/lge/systemservice/service/LGSystemServer;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 55
    invoke-direct {p0}, Lcom/lge/systemservice/service/LGSystemServer;->runLGSystemService()V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/lge/systemservice/service/LGSystemServer;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 55
    invoke-direct {p0}, Lcom/lge/systemservice/service/LGSystemServer;->runObservers()V

    #@3
    return-void
.end method

.method private static registerServiceStub(Lcom/lge/systemservice/core/LGServiceStubFetcher;)V
    .registers 2
    .parameter "register"

    #@0
    .prologue
    .line 150
    sget-object v0, Lcom/lge/systemservice/service/LGSystemServer;->SERVICE_LIST:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5
    .line 151
    return-void
.end method

.method private runLGSystemService()V
    .registers 4

    #@0
    .prologue
    .line 142
    sget-object v2, Lcom/lge/systemservice/service/LGSystemServer;->SERVICE_LIST:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v0

    #@6
    .local v0, i$:Ljava/util/Iterator;
    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_16

    #@c
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Lcom/lge/systemservice/core/LGServiceStubFetcher;

    #@12
    .line 143
    .local v1, serviceRegister:Lcom/lge/systemservice/core/LGServiceStubFetcher;
    invoke-virtual {v1, p0}, Lcom/lge/systemservice/core/LGServiceStubFetcher;->newInstance(Landroid/content/Context;)V

    #@15
    goto :goto_6

    #@16
    .line 145
    .end local v1           #serviceRegister:Lcom/lge/systemservice/core/LGServiceStubFetcher;
    :cond_16
    return-void
.end method

.method private runObservers()V
    .registers 2

    #@0
    .prologue
    .line 137
    new-instance v0, Lcom/lge/systemservice/service/SmartcoverObserver;

    #@2
    invoke-direct {v0, p0}, Lcom/lge/systemservice/service/SmartcoverObserver;-><init>(Landroid/content/Context;)V

    #@5
    .line 138
    .local v0, smartcover:Lcom/lge/systemservice/service/SmartcoverObserver;
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 3
    .parameter "intent"

    #@0
    .prologue
    .line 89
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public onCreate()V
    .registers 3

    #@0
    .prologue
    .line 69
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    #@3
    .line 70
    const-string v0, "LGESystemService"

    #@5
    const-string v1, "Create LGESystemService"

    #@7
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 75
    iget-object v0, p0, Lcom/lge/systemservice/service/LGSystemServer;->mHandler:Landroid/os/Handler;

    #@c
    new-instance v1, Lcom/lge/systemservice/service/LGSystemServer$1;

    #@e
    invoke-direct {v1, p0}, Lcom/lge/systemservice/service/LGSystemServer$1;-><init>(Lcom/lge/systemservice/service/LGSystemServer;)V

    #@11
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@14
    .line 85
    return-void
.end method

.method public onDestroy()V
    .registers 3

    #@0
    .prologue
    .line 121
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    #@3
    .line 122
    const-string v0, "LGESystemService"

    #@5
    const-string v1, "Destory LGESystemService"

    #@7
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 124
    sget-object v0, Lcom/lge/systemservice/service/LGSystemServer;->coreControlService:Lcom/lge/systemservice/service/CoreControlService;

    #@c
    if-eqz v0, :cond_13

    #@e
    .line 125
    sget-object v0, Lcom/lge/systemservice/service/LGSystemServer;->coreControlService:Lcom/lge/systemservice/service/CoreControlService;

    #@10
    invoke-virtual {v0}, Lcom/lge/systemservice/service/CoreControlService;->unregisterEcoModeObserver()V

    #@13
    .line 129
    :cond_13
    sget-object v0, Lcom/lge/systemservice/service/LGSystemServer;->mBtLgeExtService:Lcom/lge/systemservice/service/BtLgeExtService;

    #@15
    if-eqz v0, :cond_1c

    #@17
    .line 130
    sget-object v0, Lcom/lge/systemservice/service/LGSystemServer;->mBtLgeExtService:Lcom/lge/systemservice/service/BtLgeExtService;

    #@19
    invoke-virtual {v0}, Lcom/lge/systemservice/service/BtLgeExtService;->unregisterReceiver()V

    #@1c
    .line 133
    :cond_1c
    return-void
.end method

.method public onLowMemory()V
    .registers 1

    #@0
    .prologue
    .line 95
    invoke-super {p0}, Landroid/app/Service;->onLowMemory()V

    #@3
    .line 96
    return-void
.end method

.method public onRebind(Landroid/content/Intent;)V
    .registers 2
    .parameter "intent"

    #@0
    .prologue
    .line 101
    invoke-super {p0, p1}, Landroid/app/Service;->onRebind(Landroid/content/Intent;)V

    #@3
    .line 102
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .registers 5
    .parameter "intent"
    .parameter "flags"
    .parameter "startId"

    #@0
    .prologue
    .line 106
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .registers 3
    .parameter "intent"

    #@0
    .prologue
    .line 115
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method
