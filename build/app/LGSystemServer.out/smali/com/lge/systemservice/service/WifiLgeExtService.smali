.class public Lcom/lge/systemservice/service/WifiLgeExtService;
.super Lcom/lge/systemservice/core/wifilgeextmanager/IWifiLgeExtManager$Stub;
.source "WifiLgeExtService.java"


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 25
    #const-string v0, "wifilgeext_jni"

    #@2
    #invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@5
    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 28
    invoke-direct {p0}, Lcom/lge/systemservice/core/wifilgeextmanager/IWifiLgeExtManager$Stub;-><init>()V

    #@3
    .line 29
    iput-object p1, p0, Lcom/lge/systemservice/service/WifiLgeExtService;->mContext:Landroid/content/Context;

    #@5
    .line 30
    return-void
.end method


# virtual methods
.method public Channel5G_HiddenMenu(II)Z
    .registers 5
    .parameter "Channel"
    .parameter "BondingInfo"

    #@0
    .prologue
    .line 217
    invoke-static {p1, p2}, Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenuNative;->Channel5G(II)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_f

    #@6
    .line 219
    const-string v0, "WifiLgeExtService"

    #@8
    const-string v1, "[LGE_RFT_Channel5G] Failed to call Channel."

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 221
    const/4 v0, 0x0

    #@e
    .line 226
    :goto_e
    return v0

    #@f
    .line 224
    :cond_f
    const-string v0, "WifiLgeExtService"

    #@11
    const-string v1, "[LGE_RFT_Channel5G] is called."

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 226
    const/4 v0, 0x1

    #@17
    goto :goto_e
.end method

.method public Channel_HiddenMenu(II)Z
    .registers 5
    .parameter "Channel"
    .parameter "BondingInfo"

    #@0
    .prologue
    .line 204
    invoke-static {p1, p2}, Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenuNative;->Channel(II)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_f

    #@6
    .line 206
    const-string v0, "WifiLgeExtService"

    #@8
    const-string v1, "[LGE_RFT_Channel] Failed to call Channel."

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 208
    const/4 v0, 0x0

    #@e
    .line 213
    :goto_e
    return v0

    #@f
    .line 211
    :cond_f
    const-string v0, "WifiLgeExtService"

    #@11
    const-string v1, "[LGE_RFT_Channel] is called."

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 213
    const/4 v0, 0x1

    #@17
    goto :goto_e
.end method

.method public CloseDUT_HiddenMenu(Z)Z
    .registers 4
    .parameter "enable"

    #@0
    .prologue
    .line 50
    invoke-static {}, Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenuNative;->CloseDUT()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_f

    #@6
    .line 52
    const-string v0, "WifiLgeExtService"

    #@8
    const-string v1, "[LGE_RFT_CloseDUT] Failed to unload Wi-Fi drvier."

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 54
    const/4 v0, 0x0

    #@e
    .line 60
    :goto_e
    return v0

    #@f
    .line 57
    :cond_f
    const-string v0, "WifiLgeExtService"

    #@11
    const-string v1, "[LGE_RFT_CloseDUT] Success to unload Wi-Fi driver."

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 60
    const/4 v0, 0x1

    #@17
    goto :goto_e
.end method

.method public FRError_HiddenMenu()I
    .registers 4

    #@0
    .prologue
    .line 353
    invoke-static {}, Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenuNative;->FRError()I

    #@3
    move-result v0

    #@4
    .line 354
    .local v0, frError:I
    if-gez v0, :cond_e

    #@6
    .line 356
    const-string v1, "WifiLgeExtService"

    #@8
    const-string v2, "[LGE_RFT_FRError] Failed to call FRError."

    #@a
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 363
    :goto_d
    return v0

    #@e
    .line 361
    :cond_e
    const-string v1, "WifiLgeExtService"

    #@10
    const-string v2, "[LGE_RFT_FRError] is called."

    #@12
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    goto :goto_d
.end method

.method public FRGood_HiddenMenu()I
    .registers 4

    #@0
    .prologue
    .line 336
    invoke-static {}, Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenuNative;->FRGood()I

    #@3
    move-result v0

    #@4
    .line 337
    .local v0, frGood:I
    if-gez v0, :cond_e

    #@6
    .line 339
    const-string v1, "WifiLgeExtService"

    #@8
    const-string v2, "[LGE_RFT_FRGood] Failed to call FRGood."

    #@a
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 346
    :goto_d
    return v0

    #@e
    .line 344
    :cond_e
    const-string v1, "WifiLgeExtService"

    #@10
    const-string v2, "[LGE_RFT_FRGood] is called."

    #@12
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    goto :goto_d
.end method

.method public FRTotal_HiddenMenu()I
    .registers 4

    #@0
    .prologue
    .line 319
    invoke-static {}, Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenuNative;->FRTotal()I

    #@3
    move-result v0

    #@4
    .line 320
    .local v0, frTotal:I
    if-gez v0, :cond_e

    #@6
    .line 322
    const-string v1, "WifiLgeExtService"

    #@8
    const-string v2, "[LGE_RFT_FRTotal] Failed to call FRTotal."

    #@a
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 329
    :goto_d
    return v0

    #@e
    .line 327
    :cond_e
    const-string v1, "WifiLgeExtService"

    #@10
    const-string v2, "[LGE_RFT_FRTotal] is called."

    #@12
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    goto :goto_d
.end method

.method public FrequencyAccuracy5G_HiddenMenu(Ljava/lang/String;I)Z
    .registers 5
    .parameter "band"
    .parameter "ChannelNo"

    #@0
    .prologue
    .line 390
    invoke-static {p1, p2}, Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenuNative;->FrequencyAccuracy5G(Ljava/lang/String;I)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_f

    #@6
    .line 392
    const-string v0, "WifiLgeExtService"

    #@8
    const-string v1, "[LGE_RFT_FrequencyAccuracy5G] Failed to call FrequencyAccuracy"

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 394
    const/4 v0, 0x0

    #@e
    .line 399
    :goto_e
    return v0

    #@f
    .line 397
    :cond_f
    const-string v0, "WifiLgeExtService"

    #@11
    const-string v1, "[LGE_RFT_FrequencyAccuracy5G] is called."

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 399
    const/4 v0, 0x1

    #@17
    goto :goto_e
.end method

.method public FrequencyAccuracy_HiddenMenu(Ljava/lang/String;I)Z
    .registers 5
    .parameter "band"
    .parameter "ChannelNo"

    #@0
    .prologue
    .line 377
    invoke-static {p1, p2}, Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenuNative;->FrequencyAccuracy(Ljava/lang/String;I)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_f

    #@6
    .line 379
    const-string v0, "WifiLgeExtService"

    #@8
    const-string v1, "[LGE_RFT_FrequencyAccuracy] Failed to call FrequencyAccuracy"

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 381
    const/4 v0, 0x0

    #@e
    .line 386
    :goto_e
    return v0

    #@f
    .line 384
    :cond_f
    const-string v0, "WifiLgeExtService"

    #@11
    const-string v1, "[LGE_RFT_FrequencyAccuracy] is called."

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 386
    const/4 v0, 0x1

    #@17
    goto :goto_e
.end method

.method public IsRunning_HiddenMenu()Z
    .registers 3

    #@0
    .prologue
    .line 501
    invoke-static {}, Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenuNative;->IsRunning()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_f

    #@6
    .line 503
    const-string v0, "WifiLgeExtService"

    #@8
    const-string v1, "[LGE_RFT_IsRunning] Failed to call IsRunning()."

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 505
    const/4 v0, 0x0

    #@e
    .line 510
    :goto_e
    return v0

    #@f
    .line 508
    :cond_f
    const-string v0, "WifiLgeExtService"

    #@11
    const-string v1, "[LGE_RFT_TxStop] is IsRunning()."

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 510
    const/4 v0, 0x1

    #@17
    goto :goto_e
.end method

.method public NoModTxStart_BCM_HiddenMenu(I)Z
    .registers 4
    .parameter "channel"

    #@0
    .prologue
    .line 474
    invoke-static {p1}, Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenuNative;->NoModTxStartBCM(I)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_f

    #@6
    .line 476
    const-string v0, "WifiLgeExtService"

    #@8
    const-string v1, "[NoModTxStart_BCM_HiddenMenu] Failed to call TxStart."

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 478
    const/4 v0, 0x0

    #@e
    .line 483
    :goto_e
    return v0

    #@f
    .line 481
    :cond_f
    const-string v0, "WifiLgeExtService"

    #@11
    const-string v1, "[NoModTxStart_BCM_HiddenMenu] is called."

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 483
    const/4 v0, 0x1

    #@17
    goto :goto_e
.end method

.method public NoModTxStart_HiddenMenu()Z
    .registers 3

    #@0
    .prologue
    .line 461
    invoke-static {}, Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenuNative;->NoModTxStart()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_f

    #@6
    .line 463
    const-string v0, "WifiLgeExtService"

    #@8
    const-string v1, "[LGE_RFT_NoMOdTxStart] Failed to call TxStart."

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 465
    const/4 v0, 0x0

    #@e
    .line 470
    :goto_e
    return v0

    #@f
    .line 468
    :cond_f
    const-string v0, "WifiLgeExtService"

    #@11
    const-string v1, "[LGE_RFT_TxStart] is called."

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 470
    const/4 v0, 0x1

    #@17
    goto :goto_e
.end method

.method public NoModTxStop_HiddenMenu()Z
    .registers 3

    #@0
    .prologue
    .line 486
    invoke-static {}, Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenuNative;->NoModTxStop()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_f

    #@6
    .line 488
    const-string v0, "WifiLgeExtService"

    #@8
    const-string v1, "[LGE_RFT_NoModTxStop] Failed to call TxStop."

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 490
    const/4 v0, 0x0

    #@e
    .line 495
    :goto_e
    return v0

    #@f
    .line 493
    :cond_f
    const-string v0, "WifiLgeExtService"

    #@11
    const-string v1, "[LGE_RFT_TxStop] is called."

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 495
    const/4 v0, 0x1

    #@17
    goto :goto_e
.end method

.method public OpenDUT_HiddenMenu(Z)Z
    .registers 4
    .parameter "enable"

    #@0
    .prologue
    .line 36
    invoke-static {}, Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenuNative;->OpenDUT()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_f

    #@6
    .line 38
    const-string v0, "WifiLgeExtService"

    #@8
    const-string v1, "[LGE_RFT_OpenDUT] Failed to load Wi-Fi driver."

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 40
    const/4 v0, 0x0

    #@e
    .line 46
    :goto_e
    return v0

    #@f
    .line 43
    :cond_f
    const-string v0, "WifiLgeExtService"

    #@11
    const-string v1, "[LGE_RFT_OpenDUT] Success to load Wi-Fi driver."

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 46
    const/4 v0, 0x1

    #@17
    goto :goto_e
.end method

.method public OtaDisable_HiddenMenu()Z
    .registers 3

    #@0
    .prologue
    .line 448
    invoke-static {}, Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenuNative;->OtaDisable()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_f

    #@6
    .line 450
    const-string v0, "WifiLgeExtService"

    #@8
    const-string v1, "[wfc_util_qcom_ota_disable] Failed to call disable()"

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 452
    const/4 v0, 0x0

    #@e
    .line 457
    :goto_e
    return v0

    #@f
    .line 455
    :cond_f
    const-string v0, "WifiLgeExtService"

    #@11
    const-string v1, "[wfc_util_qcom_ota_enable] is called."

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 457
    const/4 v0, 0x1

    #@17
    goto :goto_e
.end method

.method public OtaEnable_HiddenMenu()Z
    .registers 3

    #@0
    .prologue
    .line 435
    invoke-static {}, Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenuNative;->OtaEnable()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_f

    #@6
    .line 437
    const-string v0, "WifiLgeExtService"

    #@8
    const-string v1, "[wfc_util_qcom_ota_enable] Failed to call otaEnable()"

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 439
    const/4 v0, 0x0

    #@e
    .line 444
    :goto_e
    return v0

    #@f
    .line 442
    :cond_f
    const-string v0, "WifiLgeExtService"

    #@11
    const-string v1, "[wfc_util_qcom_ota_enable] is called."

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 444
    const/4 v0, 0x1

    #@17
    goto :goto_e
.end method

.method public RSSI_HiddenMenu()I
    .registers 4

    #@0
    .prologue
    .line 369
    invoke-static {}, Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenuNative;->RSSI()I

    #@3
    move-result v0

    #@4
    .line 371
    .local v0, RSSI:I
    const-string v1, "WifiLgeExtService"

    #@6
    const-string v2, "[LGE_RFT_RSSI] is called."

    #@8
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 373
    return v0
.end method

.method public RxPER_HiddenMenu(Ljava/lang/String;)I
    .registers 6
    .parameter "ifname"

    #@0
    .prologue
    .line 527
    const/4 v0, 0x0

    #@1
    .line 529
    .local v0, per:I
    invoke-static {p1}, Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenuNative;->RxPER(Ljava/lang/String;)I

    #@4
    move-result v0

    #@5
    if-gez v0, :cond_10

    #@7
    .line 530
    const-string v1, "WifiLgeExtService"

    #@9
    const-string v2, "Failed to call RxPER"

    #@b
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 531
    const/4 v0, 0x0

    #@f
    .line 534
    .end local v0           #per:I
    :goto_f
    return v0

    #@10
    .line 533
    .restart local v0       #per:I
    :cond_10
    const-string v1, "WifiLgeExtService"

    #@12
    new-instance v2, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v3, "RxPER is called. "

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    goto :goto_f
.end method

.method public RxStart_HiddenMenu(Z)Z
    .registers 4
    .parameter "enable"

    #@0
    .prologue
    .line 90
    invoke-static {}, Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenuNative;->RxStart()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_f

    #@6
    .line 92
    const-string v0, "WifiLgeExtService"

    #@8
    const-string v1, "LGE_RFT_RxStart] Failed to call RxStart."

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 94
    const/4 v0, 0x0

    #@e
    .line 99
    :goto_e
    return v0

    #@f
    .line 97
    :cond_f
    const-string v0, "WifiLgeExtService"

    #@11
    const-string v1, "[LGE_RFT_RxStart] is called."

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 99
    const/4 v0, 0x1

    #@17
    goto :goto_e
.end method

.method public RxStop_HiddenMenu(Z)Z
    .registers 4
    .parameter "enable"

    #@0
    .prologue
    .line 103
    invoke-static {}, Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenuNative;->RxStop()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_f

    #@6
    .line 105
    const-string v0, "WifiLgeExtService"

    #@8
    const-string v1, "[LGE_RFT_RxStop] Failed to call RxStop."

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 107
    const/4 v0, 0x0

    #@e
    .line 112
    :goto_e
    return v0

    #@f
    .line 110
    :cond_f
    const-string v0, "WifiLgeExtService"

    #@11
    const-string v1, "[LGE_RFT_RxStop] is called."

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 112
    const/4 v0, 0x1

    #@17
    goto :goto_e
.end method

.method public Set11nPreamble_HiddenMenu(I)Z
    .registers 4
    .parameter "Preamble"

    #@0
    .prologue
    .line 304
    invoke-static {p1}, Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenuNative;->Set11nPreamble(I)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_f

    #@6
    .line 306
    const-string v0, "WifiLgeExtService"

    #@8
    const-string v1, "[LGE_RFT_Set11nPreamble] Failed to call Set11nPreamble."

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 308
    const/4 v0, 0x0

    #@e
    .line 313
    :goto_e
    return v0

    #@f
    .line 311
    :cond_f
    const-string v0, "WifiLgeExtService"

    #@11
    const-string v1, "[LGE_RFT_Set11nPreamble] is called."

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 313
    const/4 v0, 0x1

    #@17
    goto :goto_e
.end method

.method public SetPreamble_HiddenMenu(I)Z
    .registers 4
    .parameter "Preamble"

    #@0
    .prologue
    .line 291
    invoke-static {p1}, Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenuNative;->SetPreamble(I)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_f

    #@6
    .line 293
    const-string v0, "WifiLgeExtService"

    #@8
    const-string v1, "[LGE_RFT_SetPreamble] Failed to call SetPreamble."

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 295
    const/4 v0, 0x0

    #@e
    .line 300
    :goto_e
    return v0

    #@f
    .line 298
    :cond_f
    const-string v0, "WifiLgeExtService"

    #@11
    const-string v1, "[LGE_RFT_SetPreamble] is called."

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 300
    const/4 v0, 0x1

    #@17
    goto :goto_e
.end method

.method public TXBW_40M_HiddenMenu(I)Z
    .registers 4
    .parameter "ChannelNo"

    #@0
    .prologue
    .line 404
    invoke-static {p1}, Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenuNative;->TXBW_40M(I)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_f

    #@6
    .line 406
    const-string v0, "WifiLgeExtService"

    #@8
    const-string v1, "[TXBW_40M_HiddenMenu] Failed to call TXBW_40M_HiddenMenu"

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 408
    const/4 v0, 0x0

    #@e
    .line 413
    :goto_e
    return v0

    #@f
    .line 411
    :cond_f
    const-string v0, "WifiLgeExtService"

    #@11
    const-string v1, "[TXBW_40M_HiddenMenu] is called."

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 413
    const/4 v0, 0x1

    #@17
    goto :goto_e
.end method

.method public TXBW_80M_HiddenMenu(I)Z
    .registers 4
    .parameter "ChannelNo"

    #@0
    .prologue
    .line 418
    invoke-static {p1}, Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenuNative;->TXBW_80M(I)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_f

    #@6
    .line 420
    const-string v0, "WifiLgeExtService"

    #@8
    const-string v1, "[TXBW_80M_HiddenMenu] Failed to call TXBW_80M_HiddenMenu"

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 422
    const/4 v0, 0x0

    #@e
    .line 427
    :goto_e
    return v0

    #@f
    .line 425
    :cond_f
    const-string v0, "WifiLgeExtService"

    #@11
    const-string v1, "[TXBW_80M_HiddenMenu] is called."

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 427
    const/4 v0, 0x1

    #@17
    goto :goto_e
.end method

.method public TxBurstFrames_HiddenMenu(I)Z
    .registers 4
    .parameter "FramNumbers"

    #@0
    .prologue
    .line 265
    invoke-static {p1}, Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenuNative;->TxBurstFrames(I)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_f

    #@6
    .line 267
    const-string v0, "WifiLgeExtService"

    #@8
    const-string v1, "[LGE_RFT_TxBurstFrames] Failed to call TxBurstFrames."

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 269
    const/4 v0, 0x0

    #@e
    .line 274
    :goto_e
    return v0

    #@f
    .line 272
    :cond_f
    const-string v0, "WifiLgeExtService"

    #@11
    const-string v1, "[LGE_RFT_TxBurstFrames] is called."

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 274
    const/4 v0, 0x1

    #@17
    goto :goto_e
.end method

.method public TxBurstInterval_HiddenMenu(I)Z
    .registers 4
    .parameter "SIFS"

    #@0
    .prologue
    .line 243
    invoke-static {p1}, Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenuNative;->TxBurstInterval(I)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_f

    #@6
    .line 245
    const-string v0, "WifiLgeExtService"

    #@8
    const-string v1, "[LGE_RFT_TxBurstInterval] Failed to call TxBurstInterval."

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 247
    const/4 v0, 0x0

    #@e
    .line 250
    :goto_e
    return v0

    #@f
    .line 249
    :cond_f
    const-string v0, "WifiLgeExtService"

    #@11
    const-string v1, "[LGE_RFT_TxBurstInterval] is called."

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 250
    const/4 v0, 0x1

    #@17
    goto :goto_e
.end method

.method public TxDataRate11ac_HiddenMenu(IIII)Z
    .registers 7
    .parameter "nDataRate"
    .parameter "nBandWidth"
    .parameter "nChannel"
    .parameter "nGI"

    #@0
    .prologue
    .line 191
    invoke-static {p1, p2, p3, p4}, Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenuNative;->TxDataRate11ac(IIII)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_f

    #@6
    .line 193
    const-string v0, "WifiLgeExtService"

    #@8
    const-string v1, "[TxDataRate11ac_HiddenMenu] Failed to call TxDataRate."

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 195
    const/4 v0, 0x0

    #@e
    .line 200
    :goto_e
    return v0

    #@f
    .line 198
    :cond_f
    const-string v0, "WifiLgeExtService"

    #@11
    const-string v1, "[TxDataRate11ac_HiddenMenu] is called."

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 200
    const/4 v0, 0x1

    #@17
    goto :goto_e
.end method

.method public TxDataRate11n5G40M_HiddenMenu(III)Z
    .registers 6
    .parameter "nDataRate"
    .parameter "nFrameFormat"
    .parameter "nGI"

    #@0
    .prologue
    .line 176
    invoke-static {p1, p2, p3}, Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenuNative;->TxDataRate11n5G40M(III)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_f

    #@6
    .line 178
    const-string v0, "WifiLgeExtService"

    #@8
    const-string v1, "[TxDataRate11n5G40M_HiddenMenu] Failed to call TxDataRate."

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 180
    const/4 v0, 0x0

    #@e
    .line 185
    :goto_e
    return v0

    #@f
    .line 183
    :cond_f
    const-string v0, "WifiLgeExtService"

    #@11
    const-string v1, "[TxDataRate11n5G40M_HiddenMenu] is called."

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 185
    const/4 v0, 0x1

    #@17
    goto :goto_e
.end method

.method public TxDataRate11n5G_HiddenMenu(III)Z
    .registers 6
    .parameter "nDataRate"
    .parameter "nFrameFormat"
    .parameter "nGI"

    #@0
    .prologue
    .line 161
    invoke-static {p1, p2, p3}, Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenuNative;->TxDataRate11n5G(III)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_f

    #@6
    .line 163
    const-string v0, "WifiLgeExtService"

    #@8
    const-string v1, "[LGE_RFT_TxDataRate11n5G] Failed to call TxDataRate."

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 165
    const/4 v0, 0x0

    #@e
    .line 171
    :goto_e
    return v0

    #@f
    .line 168
    :cond_f
    const-string v0, "WifiLgeExtService"

    #@11
    const-string v1, "[LGE_RFT_TxDataRate11n5G] is called."

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 171
    const/4 v0, 0x1

    #@17
    goto :goto_e
.end method

.method public TxDataRate11n_HiddenMenu(III)Z
    .registers 6
    .parameter "nDataRate"
    .parameter "nFrameFormat"
    .parameter "nGI"

    #@0
    .prologue
    .line 146
    invoke-static {p1, p2, p3}, Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenuNative;->TxDataRate11n(III)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_f

    #@6
    .line 148
    const-string v0, "WifiLgeExtService"

    #@8
    const-string v1, "[LGE_RFT_TxDataRate11n] Failed to call TxDataRate."

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 150
    const/4 v0, 0x0

    #@e
    .line 156
    :goto_e
    return v0

    #@f
    .line 153
    :cond_f
    const-string v0, "WifiLgeExtService"

    #@11
    const-string v1, "[LGE_RFT_TxDataRate11n] is called."

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 156
    const/4 v0, 0x1

    #@17
    goto :goto_e
.end method

.method public TxDataRate5G_HiddenMenu(Ljava/lang/String;)Z
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 131
    invoke-static {p1}, Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenuNative;->TxDataRate5G(Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_f

    #@6
    .line 133
    const-string v0, "WifiLgeExtService"

    #@8
    const-string v1, "[LGE_RFT_TxDataRate5G] Failed to call TxDataRate."

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 135
    const/4 v0, 0x0

    #@e
    .line 141
    :goto_e
    return v0

    #@f
    .line 138
    :cond_f
    const-string v0, "WifiLgeExtService"

    #@11
    const-string v1, "[LGE_RFT_TxDataRate5G] is called."

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 141
    const/4 v0, 0x1

    #@17
    goto :goto_e
.end method

.method public TxDataRate_HiddenMenu(Ljava/lang/String;)Z
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 117
    invoke-static {p1}, Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenuNative;->TxDataRate(Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_f

    #@6
    .line 119
    const-string v0, "WifiLgeExtService"

    #@8
    const-string v1, "[LGE_RFT_TxDataRate] Failed to call TxDataRate."

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 121
    const/4 v0, 0x0

    #@e
    .line 126
    :goto_e
    return v0

    #@f
    .line 124
    :cond_f
    const-string v0, "WifiLgeExtService"

    #@11
    const-string v1, "[LGE_RFT_TxDataRate] is called."

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 126
    const/4 v0, 0x1

    #@17
    goto :goto_e
.end method

.method public TxDestAddress_HiddenMenu(Ljava/lang/String;)Z
    .registers 4
    .parameter "dstMacAddr"

    #@0
    .prologue
    .line 278
    invoke-static {p1}, Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenuNative;->TxDestAddress(Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_f

    #@6
    .line 280
    const-string v0, "WifiLgeExtService"

    #@8
    const-string v1, "[LGE_RFT_TxDestAddress] Failed to call TxDestAddress."

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 282
    const/4 v0, 0x0

    #@e
    .line 287
    :goto_e
    return v0

    #@f
    .line 285
    :cond_f
    const-string v0, "WifiLgeExtService"

    #@11
    const-string v1, "[LGE_RFT_TxDestAddress] is called."

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 287
    const/4 v0, 0x1

    #@17
    goto :goto_e
.end method

.method public TxGain_HiddenMenu(I)Z
    .registers 4
    .parameter "TxGain"

    #@0
    .prologue
    .line 230
    invoke-static {p1}, Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenuNative;->TxGain(I)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_f

    #@6
    .line 232
    const-string v0, "WifiLgeExtService"

    #@8
    const-string v1, "[LGE_RFT_TxGain] Failed to call TxGain."

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 234
    const/4 v0, 0x0

    #@e
    .line 239
    :goto_e
    return v0

    #@f
    .line 237
    :cond_f
    const-string v0, "WifiLgeExtService"

    #@11
    const-string v1, "[LGE_RFT_TxGain] is called."

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 239
    const/4 v0, 0x1

    #@17
    goto :goto_e
.end method

.method public TxPER_HiddenMenu(Ljava/lang/String;)I
    .registers 6
    .parameter "ifname"

    #@0
    .prologue
    .line 516
    const/4 v0, 0x0

    #@1
    .line 518
    .local v0, per:I
    invoke-static {p1}, Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenuNative;->TxPER(Ljava/lang/String;)I

    #@4
    move-result v0

    #@5
    if-gez v0, :cond_10

    #@7
    .line 519
    const-string v1, "WifiLgeExtService"

    #@9
    const-string v2, "Failed to call TxPER"

    #@b
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 520
    const/4 v0, 0x0

    #@f
    .line 523
    .end local v0           #per:I
    :goto_f
    return v0

    #@10
    .line 522
    .restart local v0       #per:I
    :cond_10
    const-string v1, "WifiLgeExtService"

    #@12
    new-instance v2, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v3, "TxPER is called. "

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    goto :goto_f
.end method

.method public TxPayloadLength_HiddenMenu(I)Z
    .registers 4
    .parameter "PayLength"

    #@0
    .prologue
    .line 254
    invoke-static {p1}, Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenuNative;->TxPayloadLength(I)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_f

    #@6
    .line 256
    const-string v0, "WifiLgeExtService"

    #@8
    const-string v1, "[LGE_RFT_TxPayloadLength] Failed to call TxPayloadLength."

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 258
    const/4 v0, 0x0

    #@e
    .line 261
    :goto_e
    return v0

    #@f
    .line 260
    :cond_f
    const-string v0, "WifiLgeExtService"

    #@11
    const-string v1, "[LGE_RFT_TxPayloadLength] is called."

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 261
    const/4 v0, 0x1

    #@17
    goto :goto_e
.end method

.method public TxStart_HiddenMenu(Z)Z
    .registers 4
    .parameter "enable"

    #@0
    .prologue
    .line 64
    invoke-static {}, Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenuNative;->TxStart()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_f

    #@6
    .line 66
    const-string v0, "WifiLgeExtService"

    #@8
    const-string v1, "[LGE_RFT_TxStart] Failed to call TxStart."

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 68
    const/4 v0, 0x0

    #@e
    .line 73
    :goto_e
    return v0

    #@f
    .line 71
    :cond_f
    const-string v0, "WifiLgeExtService"

    #@11
    const-string v1, "[LGE_RFT_TxStart] is called."

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 73
    const/4 v0, 0x1

    #@17
    goto :goto_e
.end method

.method public TxStop_HiddenMenu(Z)Z
    .registers 4
    .parameter "enable"

    #@0
    .prologue
    .line 77
    invoke-static {}, Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenuNative;->TxStop()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_f

    #@6
    .line 79
    const-string v0, "WifiLgeExtService"

    #@8
    const-string v1, "[LGE_RFT_TxStop] Failed to call TxStop."

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 81
    const/4 v0, 0x0

    #@e
    .line 86
    :goto_e
    return v0

    #@f
    .line 84
    :cond_f
    const-string v0, "WifiLgeExtService"

    #@11
    const-string v1, "[LGE_RFT_TxStop] is called."

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 86
    const/4 v0, 0x1

    #@17
    goto :goto_e
.end method
