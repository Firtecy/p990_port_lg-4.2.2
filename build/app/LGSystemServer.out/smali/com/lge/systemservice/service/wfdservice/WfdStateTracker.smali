.class public Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;
.super Ljava/lang/Object;
.source "WfdStateTracker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$CallState;,
        Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;,
        Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;
    }
.end annotation


# static fields
.field private static final BLOCK_PROCESS:[[Ljava/lang/String;

.field private static connectedUdn:Ljava/lang/String;

.field private static mBlockHdcp:Z

.field private static mCallState:Ljava/lang/String;

.field private static myIpAddr:Ljava/lang/String;

.field private static pendingUdn:Ljava/lang/String;

.field private static wifiInterfaceName:Ljava/lang/String;


# instance fields
.field private connectedDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

.field private drmMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private drmPauseCount:I

.field private forcedDrmPauseCount:I

.field private hdcpEnabled:Z

.field private isBatteryLow:Z

.field private isHdmiConnected:Z

.field listener:Landroid/content/DialogInterface$OnClickListener;

.field listener_cancel:Landroid/content/DialogInterface$OnCancelListener;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

.field private mContext:Landroid/content/Context;

.field private mDialogHdcpFailed:Landroid/app/AlertDialog;

.field private mDialogP2pDisconnect:Landroid/app/AlertDialog;

.field private final mDirectState:Ljava/util/concurrent/atomic/AtomicInteger;

.field private mHandler:Landroid/os/Handler;

.field private mNotification:Landroid/app/Notification;

.field private final mRtspState:Ljava/util/concurrent/atomic/AtomicInteger;

.field private mStatusBarManager:Landroid/app/StatusBarManager;

.field private mWfdAdaptation:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

.field private mWfdBlockProcessDialog:Landroid/app/AlertDialog;

.field private mWfdDlnaTransitEnabled:Z

.field private mWfdEnableState:Z

.field private mWfdHdmiConnectedDialog:Landroid/app/AlertDialog;

.field private mWfdInfraModeEnabled:Z

.field private mWfdIntroDialog:Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;

.field private mWfdMonitor:Lcom/lge/systemservice/service/wfdservice/WfdMonitor;

.field private mWfdNeedConnectionPopUp:Z

.field private mWfdPendingCmd:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;

.field private mWfdPendingTeardown:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

.field private mWfdPlayingDlna:Z

.field private final mWfdState:Ljava/util/concurrent/atomic/AtomicInteger;

.field private mWfdViaWifi:Z

.field private mWifiManager:Landroid/net/wifi/WifiManager;

.field private mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

.field private mWifiState:I

.field private mWindowManager:Landroid/view/WindowManager;

.field private noDirectDisconnect:Z

.field private onPauseTransition:Z

.field private ongoingNoti:Z

.field private pauseStateBitMap:I

.field private popupIntro:Z

.field private rtspPort:I

.field private targetRtspPort:I

.field private targetUrl:Ljava/lang/String;

.field private udnForDlnaTransit:Ljava/lang/String;

.field private uibcEnabled:Z

.field private uibcSupported:Z

.field private wasWifiOn:Z

.field private wfdInterfaceName:Ljava/lang/String;

.field private wfdMode:I


# direct methods
.method static constructor <clinit>()V
    .registers 7

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v5, 0x2

    #@2
    const/4 v4, 0x1

    #@3
    const/4 v3, 0x0

    #@4
    .line 130
    sput-object v6, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->wifiInterfaceName:Ljava/lang/String;

    #@6
    .line 131
    sput-object v6, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->connectedUdn:Ljava/lang/String;

    #@8
    .line 132
    sput-object v6, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->pendingUdn:Ljava/lang/String;

    #@a
    .line 140
    invoke-static {}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->init()V

    #@d
    .line 176
    const/4 v0, 0x3

    #@e
    new-array v0, v0, [[Ljava/lang/String;

    #@10
    new-array v1, v5, [Ljava/lang/String;

    #@12
    const-string v2, "itectokyo.wiflus.service:wiflusservice"

    #@14
    aput-object v2, v1, v3

    #@16
    const-string v2, "SmartShare Beam"

    #@18
    aput-object v2, v1, v4

    #@1a
    aput-object v1, v0, v3

    #@1c
    new-array v1, v5, [Ljava/lang/String;

    #@1e
    const-string v2, "android.process.filesharewrapper"

    #@20
    aput-object v2, v1, v3

    #@22
    const-string v2, "FileShare"

    #@24
    aput-object v2, v1, v4

    #@26
    aput-object v1, v0, v4

    #@28
    new-array v1, v5, [Ljava/lang/String;

    #@2a
    const-string v2, "itectokyo.wiflus.service:wiflusnfcservice"

    #@2c
    aput-object v2, v1, v3

    #@2e
    const-string v2, "Direct Beam"

    #@30
    aput-object v2, v1, v4

    #@32
    aput-object v1, v0, v5

    #@34
    sput-object v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->BLOCK_PROCESS:[[Ljava/lang/String;

    #@36
    .line 204
    sput-object v6, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->myIpAddr:Ljava/lang/String;

    #@38
    .line 205
    sput-boolean v3, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mBlockHdcp:Z

    #@3a
    .line 217
    sget-object v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$CallState;->IDLE:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$CallState;

    #@3c
    invoke-virtual {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$CallState;->toString()Ljava/lang/String;

    #@3f
    move-result-object v0

    #@40
    sput-object v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mCallState:Ljava/lang/String;

    #@42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 8
    .parameter "context"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    const/4 v1, 0x0

    #@3
    .line 220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 88
    iput-object v5, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@8
    .line 89
    iput-object v5, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@a
    .line 90
    iput-object v5, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@c
    .line 92
    iput-object v5, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mStatusBarManager:Landroid/app/StatusBarManager;

    #@e
    .line 94
    iput-object v5, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mAudioManager:Landroid/media/AudioManager;

    #@10
    .line 98
    iput-object v5, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mDialogP2pDisconnect:Landroid/app/AlertDialog;

    #@12
    .line 99
    iput-object v5, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdIntroDialog:Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;

    #@14
    .line 100
    iput-object v5, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdHdmiConnectedDialog:Landroid/app/AlertDialog;

    #@16
    .line 101
    iput-object v5, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdBlockProcessDialog:Landroid/app/AlertDialog;

    #@18
    .line 102
    iput-object v5, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mDialogHdcpFailed:Landroid/app/AlertDialog;

    #@1a
    .line 105
    iput-boolean v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->ongoingNoti:Z

    #@1c
    .line 106
    iput-boolean v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->popupIntro:Z

    #@1e
    .line 107
    iput-boolean v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->isHdmiConnected:Z

    #@20
    .line 109
    iput-boolean v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->onPauseTransition:Z

    #@22
    .line 110
    iput v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->pauseStateBitMap:I

    #@24
    .line 111
    new-instance v0, Ljava/util/HashMap;

    #@26
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@29
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->drmMap:Ljava/util/HashMap;

    #@2b
    .line 112
    iput v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->forcedDrmPauseCount:I

    #@2d
    .line 113
    iput v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->drmPauseCount:I

    #@2f
    .line 114
    iput-boolean v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->hdcpEnabled:Z

    #@31
    .line 115
    iput-boolean v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->uibcSupported:Z

    #@33
    .line 116
    iput-boolean v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->uibcEnabled:Z

    #@35
    .line 117
    iput-boolean v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->wasWifiOn:Z

    #@37
    .line 118
    iput-boolean v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->noDirectDisconnect:Z

    #@39
    .line 120
    iput-boolean v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->isBatteryLow:Z

    #@3b
    .line 123
    iput-boolean v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdInfraModeEnabled:Z

    #@3d
    .line 124
    iput-boolean v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdDlnaTransitEnabled:Z

    #@3f
    .line 125
    iput-boolean v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdPlayingDlna:Z

    #@41
    .line 126
    iput-boolean v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdViaWifi:Z

    #@43
    .line 127
    iput v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiState:I

    #@45
    .line 128
    iput-object v5, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->udnForDlnaTransit:Ljava/lang/String;

    #@47
    .line 129
    iput-object v5, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->wfdInterfaceName:Ljava/lang/String;

    #@49
    .line 201
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    #@4b
    const/16 v3, 0x8

    #@4d
    invoke-direct {v0, v3}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    #@50
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdState:Ljava/util/concurrent/atomic/AtomicInteger;

    #@52
    .line 202
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    #@54
    const/4 v3, 0x7

    #@55
    invoke-direct {v0, v3}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    #@58
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mRtspState:Ljava/util/concurrent/atomic/AtomicInteger;

    #@5a
    .line 203
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    #@5c
    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    #@5f
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mDirectState:Ljava/util/concurrent/atomic/AtomicInteger;

    #@61
    .line 207
    iput-boolean v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdEnableState:Z

    #@63
    .line 208
    iput-boolean v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdNeedConnectionPopUp:Z

    #@65
    .line 209
    sget-object v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;->NONE:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;

    #@67
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdPendingCmd:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;

    #@69
    .line 210
    sget-object v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;->NONE:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

    #@6b
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdPendingTeardown:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

    #@6d
    .line 211
    iput v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->wfdMode:I

    #@6f
    .line 213
    iput-object v5, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->targetUrl:Ljava/lang/String;

    #@71
    .line 214
    const/16 v0, 0x22a

    #@73
    iput v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->targetRtspPort:I

    #@75
    .line 215
    const/16 v0, 0x216a

    #@77
    iput v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->rtspPort:I

    #@79
    .line 263
    new-instance v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$1;

    #@7b
    invoke-direct {v0, p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$1;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)V

    #@7e
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->listener:Landroid/content/DialogInterface$OnClickListener;

    #@80
    .line 290
    new-instance v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$2;

    #@82
    invoke-direct {v0, p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$2;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)V

    #@85
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->listener_cancel:Landroid/content/DialogInterface$OnCancelListener;

    #@87
    .line 297
    new-instance v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;

    #@89
    invoke-direct {v0, p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)V

    #@8c
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mHandler:Landroid/os/Handler;

    #@8e
    .line 221
    const-string v0, "WfdStateTracker"

    #@90
    const-string v3, "WfdStateTracker Created"

    #@92
    invoke-static {v0, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@95
    .line 222
    iput-object p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mContext:Landroid/content/Context;

    #@97
    .line 223
    new-instance v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@99
    invoke-direct {v0, p1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;-><init>(Landroid/content/Context;)V

    #@9c
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdAdaptation:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@9e
    .line 224
    new-instance v0, Lcom/lge/systemservice/service/wfdservice/WfdMonitor;

    #@a0
    iget-object v3, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdAdaptation:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@a2
    invoke-direct {v0, p0, v3}, Lcom/lge/systemservice/service/wfdservice/WfdMonitor;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)V

    #@a5
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdMonitor:Lcom/lge/systemservice/service/wfdservice/WfdMonitor;

    #@a7
    .line 225
    iput-object v5, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->connectedDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    #@a9
    .line 238
    const-string v0, "wifip2p"

    #@ab
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@ae
    move-result-object v0

    #@af
    check-cast v0, Landroid/net/wifi/p2p/WifiP2pManager;

    #@b1
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@b3
    .line 239
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@b5
    if-eqz v0, :cond_129

    #@b7
    .line 240
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@b9
    iget-object v3, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mContext:Landroid/content/Context;

    #@bb
    iget-object v4, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mContext:Landroid/content/Context;

    #@bd
    invoke-virtual {v4}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    #@c0
    move-result-object v4

    #@c1
    invoke-virtual {v0, v3, v4, v5}, Landroid/net/wifi/p2p/WifiP2pManager;->initialize(Landroid/content/Context;Landroid/os/Looper;Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@c4
    move-result-object v0

    #@c5
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@c7
    .line 241
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@c9
    if-nez v0, :cond_d4

    #@cb
    .line 243
    const-string v0, "WfdStateTracker"

    #@cd
    const-string v3, "Failed to set up connection with wifi p2p service"

    #@cf
    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d2
    .line 244
    iput-object v5, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@d4
    .line 250
    :cond_d4
    :goto_d4
    const-string v0, "window"

    #@d6
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@d9
    move-result-object v0

    #@da
    check-cast v0, Landroid/view/WindowManager;

    #@dc
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWindowManager:Landroid/view/WindowManager;

    #@de
    .line 251
    const-string v0, "audio"

    #@e0
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@e3
    move-result-object v0

    #@e4
    check-cast v0, Landroid/media/AudioManager;

    #@e6
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mAudioManager:Landroid/media/AudioManager;

    #@e8
    .line 253
    const-string v0, "ro.build.target_operator"

    #@ea
    const-string v3, ""

    #@ec
    invoke-static {v0, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@ef
    move-result-object v0

    #@f0
    const-string v3, "DCM"

    #@f2
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f5
    move-result v0

    #@f6
    if-nez v0, :cond_108

    #@f8
    const-string v0, "ro.build.target_operator"

    #@fa
    const-string v3, ""

    #@fc
    invoke-static {v0, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@ff
    move-result-object v0

    #@100
    const-string v3, "KDDI"

    #@102
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@105
    move-result v0

    #@106
    if-eqz v0, :cond_131

    #@108
    :cond_108
    move v0, v2

    #@109
    :goto_109
    sput-boolean v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mBlockHdcp:Z

    #@10b
    .line 256
    const-string v0, "wifi.interface"

    #@10d
    const-string v2, "wlan0"

    #@10f
    invoke-static {v0, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@112
    move-result-object v0

    #@113
    sput-object v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->wifiInterfaceName:Ljava/lang/String;

    #@115
    .line 257
    const-string v0, "wlan.lge.wifidisplay"

    #@117
    const-string v2, "direct"

    #@119
    invoke-static {v0, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@11c
    move-result-object v0

    #@11d
    const-string v2, "both"

    #@11f
    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@122
    move-result v0

    #@123
    iput-boolean v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdInfraModeEnabled:Z

    #@125
    .line 260
    invoke-direct {p0, v1}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@128
    .line 261
    return-void

    #@129
    .line 247
    :cond_129
    const-string v0, "WfdStateTracker"

    #@12b
    const-string v3, "mWifiP2pManager is null!"

    #@12d
    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@130
    goto :goto_d4

    #@131
    :cond_131
    move v0, v1

    #@132
    .line 253
    goto :goto_109
.end method

.method static synthetic access$000(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 85
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@2
    return-object v0
.end method

.method static synthetic access$1000()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 85
    sget-object v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->connectedUdn:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$1002(Ljava/lang/String;)Ljava/lang/String;
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 85
    sput-object p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->connectedUdn:Ljava/lang/String;

    #@2
    return-object p0
.end method

.method static synthetic access$102(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 85
    iput-boolean p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdEnableState:Z

    #@2
    return p1
.end method

.method static synthetic access$1102(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;Landroid/net/wifi/p2p/WifiP2pDevice;)Landroid/net/wifi/p2p/WifiP2pDevice;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 85
    iput-object p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->connectedDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    #@2
    return-object p1
.end method

.method static synthetic access$1200(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Landroid/app/StatusBarManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 85
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getStatusBarManager()Landroid/app/StatusBarManager;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$1300(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Landroid/app/StatusBarManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 85
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mStatusBarManager:Landroid/app/StatusBarManager;

    #@2
    return-object v0
.end method

.method static synthetic access$1400(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Landroid/app/AlertDialog;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 85
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mDialogP2pDisconnect:Landroid/app/AlertDialog;

    #@2
    return-object v0
.end method

.method static synthetic access$1402(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 85
    iput-object p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mDialogP2pDisconnect:Landroid/app/AlertDialog;

    #@2
    return-object p1
.end method

.method static synthetic access$1500(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 85
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1600(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->popupIntro:Z

    #@2
    return v0
.end method

.method static synthetic access$1602(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 85
    iput-boolean p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->popupIntro:Z

    #@2
    return p1
.end method

.method static synthetic access$1700(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 85
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdIntroDialog:Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;

    #@2
    return-object v0
.end method

.method static synthetic access$1702(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;)Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 85
    iput-object p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdIntroDialog:Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;

    #@2
    return-object p1
.end method

.method static synthetic access$1802(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 85
    iput-boolean p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdNeedConnectionPopUp:Z

    #@2
    return p1
.end method

.method static synthetic access$1900(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Landroid/app/AlertDialog;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 85
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdBlockProcessDialog:Landroid/app/AlertDialog;

    #@2
    return-object v0
.end method

.method static synthetic access$1902(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 85
    iput-object p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdBlockProcessDialog:Landroid/app/AlertDialog;

    #@2
    return-object p1
.end method

.method static synthetic access$200(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 85
    iget v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiState:I

    #@2
    return v0
.end method

.method static synthetic access$2000()[[Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 85
    sget-object v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->BLOCK_PROCESS:[[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$2100(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Landroid/app/AlertDialog;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 85
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mDialogHdcpFailed:Landroid/app/AlertDialog;

    #@2
    return-object v0
.end method

.method static synthetic access$2102(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 85
    iput-object p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mDialogHdcpFailed:Landroid/app/AlertDialog;

    #@2
    return-object p1
.end method

.method static synthetic access$2202(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 85
    iput-object p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdPendingCmd:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;

    #@2
    return-object p1
.end method

.method static synthetic access$300(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 85
    invoke-direct {p0, p1}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Landroid/net/wifi/p2p/WifiP2pManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 85
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@2
    return-object v0
.end method

.method static synthetic access$502(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 85
    iput-boolean p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->onPauseTransition:Z

    #@2
    return p1
.end method

.method static synthetic access$600(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 85
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->handleWfdPauseResumeState()V

    #@3
    return-void
.end method

.method static synthetic access$700(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 85
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->disableWifiP2p()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$802(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 85
    iput-boolean p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->uibcEnabled:Z

    #@2
    return p1
.end method

.method static synthetic access$900(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 85
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdPendingTeardown:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

    #@2
    return-object v0
.end method

.method static synthetic access$902(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 85
    iput-object p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdPendingTeardown:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

    #@2
    return-object p1
.end method

.method private checkProcess()I
    .registers 9

    #@0
    .prologue
    const/4 v3, -0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 1822
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mContext:Landroid/content/Context;

    #@4
    const-string v1, "activity"

    #@6
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/app/ActivityManager;

    #@c
    .line 1823
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    #@f
    move-result-object v5

    #@10
    .line 1824
    if-nez v5, :cond_14

    #@12
    move v0, v3

    #@13
    .line 1836
    :goto_13
    return v0

    #@14
    :cond_14
    move v1, v2

    #@15
    .line 1827
    :goto_15
    invoke-interface {v5}, Ljava/util/List;->size()I

    #@18
    move-result v0

    #@19
    if-ge v1, v0, :cond_5a

    #@1b
    .line 1828
    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@1e
    move-result-object v0

    #@1f
    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    #@21
    move v4, v2

    #@22
    .line 1829
    :goto_22
    const/4 v6, 0x3

    #@23
    if-ge v4, v6, :cond_56

    #@25
    .line 1830
    sget-object v6, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->BLOCK_PROCESS:[[Ljava/lang/String;

    #@27
    aget-object v6, v6, v4

    #@29
    aget-object v6, v6, v2

    #@2b
    iget-object v7, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    #@2d
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@30
    move-result v6

    #@31
    if-eqz v6, :cond_53

    #@33
    .line 1831
    const-string v0, "WfdStateTracker"

    #@35
    new-instance v1, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v2, "Process "

    #@3c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v1

    #@40
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@43
    move-result-object v1

    #@44
    const-string v2, " is running"

    #@46
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v1

    #@4a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v1

    #@4e
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@51
    move v0, v4

    #@52
    .line 1832
    goto :goto_13

    #@53
    .line 1829
    :cond_53
    add-int/lit8 v4, v4, 0x1

    #@55
    goto :goto_22

    #@56
    .line 1827
    :cond_56
    add-int/lit8 v0, v1, 0x1

    #@58
    move v1, v0

    #@59
    goto :goto_15

    #@5a
    :cond_5a
    move v0, v3

    #@5b
    .line 1836
    goto :goto_13
.end method

.method private clearNotification()V
    .registers 4

    #@0
    .prologue
    .line 1806
    iget-boolean v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->ongoingNoti:Z

    #@2
    if-nez v1, :cond_5

    #@4
    .line 1817
    :cond_4
    :goto_4
    return-void

    #@5
    .line 1810
    :cond_5
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mContext:Landroid/content/Context;

    #@7
    const-string v2, "notification"

    #@9
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@c
    move-result-object v0

    #@d
    check-cast v0, Landroid/app/NotificationManager;

    #@f
    .line 1812
    .local v0, notificationManager:Landroid/app/NotificationManager;
    if-eqz v0, :cond_4

    #@11
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mNotification:Landroid/app/Notification;

    #@13
    if-eqz v1, :cond_4

    #@15
    .line 1813
    const/4 v1, 0x0

    #@16
    iput-boolean v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->ongoingNoti:Z

    #@18
    .line 1814
    const v1, 0x20206e1

    #@1b
    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    #@1e
    .line 1815
    const/4 v1, 0x0

    #@1f
    iput-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mNotification:Landroid/app/Notification;

    #@21
    goto :goto_4
.end method

.method private disableWifiP2p()Z
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 938
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@3
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@5
    new-instance v2, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$4;

    #@7
    invoke-direct {v2, p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$4;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)V

    #@a
    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->cancelConnect(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    #@d
    .line 947
    iput-boolean v3, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->wasWifiOn:Z

    #@f
    .line 948
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->stopEventLoop()V

    #@12
    .line 949
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->stopRtspThread()Z

    #@15
    .line 952
    invoke-direct {p0, v3}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@18
    .line 953
    const/4 v0, 0x1

    #@19
    return v0
.end method

.method private enableWifi()Z
    .registers 5

    #@0
    .prologue
    .line 923
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@2
    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getWifiApState()I

    #@5
    move-result v0

    #@6
    .line 924
    .local v0, wifiApState:I
    const/16 v1, 0xc

    #@8
    if-eq v0, v1, :cond_e

    #@a
    const/16 v1, 0xd

    #@c
    if-ne v0, v1, :cond_15

    #@e
    .line 926
    :cond_e
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@10
    const/4 v2, 0x0

    #@11
    const/4 v3, 0x0

    #@12
    invoke-virtual {v1, v2, v3}, Landroid/net/wifi/WifiManager;->setWifiApEnabled(Landroid/net/wifi/WifiConfiguration;Z)Z

    #@15
    .line 929
    :cond_15
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@17
    const/4 v2, 0x1

    #@18
    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    #@1b
    move-result v1

    #@1c
    return v1
.end method

.method private enableWifiP2p()Z
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    const/4 v1, 0x0

    #@3
    .line 866
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@5
    if-nez v0, :cond_29

    #@7
    .line 867
    const-string v0, "WfdStateTracker"

    #@9
    const-string v3, "WifiP2pManager is null - Retrieve it"

    #@b
    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 868
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mContext:Landroid/content/Context;

    #@10
    const-string v3, "wifip2p"

    #@12
    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@15
    move-result-object v0

    #@16
    check-cast v0, Landroid/net/wifi/p2p/WifiP2pManager;

    #@18
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@1a
    .line 869
    iput-object v5, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@1c
    .line 870
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@1e
    if-nez v0, :cond_29

    #@20
    .line 871
    const-string v0, "WfdStateTracker"

    #@22
    const-string v2, "Failed to get WifiP2pManager Service"

    #@24
    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    move v0, v1

    #@28
    .line 912
    :goto_28
    return v0

    #@29
    .line 875
    :cond_29
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@2b
    if-nez v0, :cond_53

    #@2d
    .line 876
    const-string v0, "WfdStateTracker"

    #@2f
    const-string v3, "WifiP2p Channel is null - initialize it"

    #@31
    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 877
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@36
    iget-object v3, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mContext:Landroid/content/Context;

    #@38
    iget-object v4, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mContext:Landroid/content/Context;

    #@3a
    invoke-virtual {v4}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    #@3d
    move-result-object v4

    #@3e
    invoke-virtual {v0, v3, v4, v5}, Landroid/net/wifi/p2p/WifiP2pManager;->initialize(Landroid/content/Context;Landroid/os/Looper;Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@41
    move-result-object v0

    #@42
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@44
    .line 878
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@46
    if-nez v0, :cond_53

    #@48
    .line 879
    const-string v0, "WfdStateTracker"

    #@4a
    const-string v2, "Failed to set up wifi p2p service channel"

    #@4c
    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 880
    iput-object v5, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@51
    move v0, v1

    #@52
    .line 881
    goto :goto_28

    #@53
    .line 886
    :cond_53
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@55
    if-nez v0, :cond_70

    #@57
    .line 887
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mContext:Landroid/content/Context;

    #@59
    const-string v3, "wifi"

    #@5b
    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@5e
    move-result-object v0

    #@5f
    check-cast v0, Landroid/net/wifi/WifiManager;

    #@61
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@63
    .line 888
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@65
    if-nez v0, :cond_70

    #@67
    .line 889
    const-string v0, "WfdStateTracker"

    #@69
    const-string v2, "Failed to get WifiManager Service"

    #@6b
    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6e
    move v0, v1

    #@6f
    .line 890
    goto :goto_28

    #@70
    .line 894
    :cond_70
    const-string v0, "WfdStateTracker"

    #@72
    new-instance v3, Ljava/lang/StringBuilder;

    #@74
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@77
    const-string v4, "Enable WifiP2p: "

    #@79
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v3

    #@7d
    iget-object v4, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@7f
    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->getWifiState()I

    #@82
    move-result v4

    #@83
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@86
    move-result-object v3

    #@87
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8a
    move-result-object v3

    #@8b
    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8e
    .line 895
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@90
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getWifiState()I

    #@93
    move-result v0

    #@94
    packed-switch v0, :pswitch_data_ba

    #@97
    .line 911
    const-string v0, "WfdStateTracker"

    #@99
    const-string v2, "Unexpected Wi-Fi State"

    #@9b
    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@9e
    move v0, v1

    #@9f
    .line 912
    goto :goto_28

    #@a0
    .line 897
    :pswitch_a0
    iput-boolean v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->wasWifiOn:Z

    #@a2
    .line 898
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->enableWifi()Z

    #@a5
    move-result v0

    #@a6
    goto :goto_28

    #@a7
    .line 900
    :pswitch_a7
    iput-boolean v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->wasWifiOn:Z

    #@a9
    .line 901
    sget-object v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;->ENABLE:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;

    #@ab
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdPendingCmd:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;

    #@ad
    move v0, v2

    #@ae
    .line 902
    goto/16 :goto_28

    #@b0
    .line 904
    :pswitch_b0
    iput-boolean v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->wasWifiOn:Z

    #@b2
    move v0, v2

    #@b3
    .line 905
    goto/16 :goto_28

    #@b5
    .line 907
    :pswitch_b5
    iput-boolean v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->wasWifiOn:Z

    #@b7
    move v0, v2

    #@b8
    .line 908
    goto/16 :goto_28

    #@ba
    .line 895
    :pswitch_data_ba
    .packed-switch 0x0
        :pswitch_a7
        :pswitch_a0
        :pswitch_b5
        :pswitch_b0
    .end packed-switch
.end method

.method private getStatusBarManager()Landroid/app/StatusBarManager;
    .registers 3

    #@0
    .prologue
    .line 859
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mStatusBarManager:Landroid/app/StatusBarManager;

    #@2
    if-nez v0, :cond_10

    #@4
    .line 860
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mContext:Landroid/content/Context;

    #@6
    const-string v1, "statusbar"

    #@8
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@b
    move-result-object v0

    #@c
    check-cast v0, Landroid/app/StatusBarManager;

    #@e
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mStatusBarManager:Landroid/app/StatusBarManager;

    #@10
    .line 862
    :cond_10
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mStatusBarManager:Landroid/app/StatusBarManager;

    #@12
    return-object v0
.end method

.method private handleWfdPauseResumeState()V
    .registers 5

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 1950
    iget-boolean v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->onPauseTransition:Z

    #@3
    if-nez v0, :cond_41

    #@5
    iget-boolean v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdPlayingDlna:Z

    #@7
    if-nez v0, :cond_41

    #@9
    .line 1954
    iget v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->pauseStateBitMap:I

    #@b
    if-eqz v0, :cond_28

    #@d
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWindowManager:Landroid/view/WindowManager;

    #@f
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0}, Landroid/view/Display;->getDsdrStatus()I

    #@16
    move-result v0

    #@17
    const/4 v1, 0x3

    #@18
    if-eq v0, v1, :cond_28

    #@1a
    iget v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->pauseStateBitMap:I

    #@1c
    and-int/lit8 v0, v0, 0x12

    #@1e
    if-nez v0, :cond_42

    #@20
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mAudioManager:Landroid/media/AudioManager;

    #@22
    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    #@25
    move-result v0

    #@26
    if-eqz v0, :cond_42

    #@28
    .line 1959
    :cond_28
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getRtspState()I

    #@2b
    move-result v0

    #@2c
    const/4 v1, 0x5

    #@2d
    if-ne v0, v1, :cond_34

    #@2f
    .line 1960
    iput-boolean v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->onPauseTransition:Z

    #@31
    .line 1961
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->play()Z

    #@34
    .line 1969
    :cond_34
    :goto_34
    iget-boolean v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->onPauseTransition:Z

    #@36
    if-eqz v0, :cond_41

    #@38
    .line 1970
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mHandler:Landroid/os/Handler;

    #@3a
    const/16 v1, 0x3e9

    #@3c
    const-wide/16 v2, 0x3e8

    #@3e
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@41
    .line 1973
    :cond_41
    return-void

    #@42
    .line 1964
    :cond_42
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getRtspState()I

    #@45
    move-result v0

    #@46
    const/4 v1, 0x6

    #@47
    if-ne v0, v1, :cond_34

    #@49
    .line 1965
    iput-boolean v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->onPauseTransition:Z

    #@4b
    .line 1966
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->pause()Z

    #@4e
    goto :goto_34
.end method

.method private declared-synchronized initRtspEnv()Z
    .registers 2

    #@0
    .prologue
    .line 578
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdAdaptation:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@3
    invoke-virtual {v0}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->initRtspEnv()Z
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_9

    #@6
    move-result v0

    #@7
    monitor-exit p0

    #@8
    return v0

    #@9
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0

    #@b
    throw v0
.end method

.method private declared-synchronized initRtspServer(Ljava/lang/String;I)Z
    .registers 4
    .parameter "ipaddr"
    .parameter "port"

    #@0
    .prologue
    .line 582
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdAdaptation:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@3
    invoke-virtual {v0, p1, p2}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->initRtspServer(Ljava/lang/String;I)Z
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_9

    #@6
    move-result v0

    #@7
    monitor-exit p0

    #@8
    return v0

    #@9
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0

    #@b
    throw v0
.end method

.method private initWfdDisabledState()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 545
    iput-boolean v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdEnableState:Z

    #@4
    .line 546
    iput v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->pauseStateBitMap:I

    #@6
    .line 547
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdAdaptation:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@8
    invoke-virtual {v1, v2, v2}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->setWfdIe(ZZ)Z

    #@b
    .line 548
    iput-boolean v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->onPauseTransition:Z

    #@d
    .line 549
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->drmMap:Ljava/util/HashMap;

    #@f
    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    #@12
    .line 550
    iput v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->forcedDrmPauseCount:I

    #@14
    .line 551
    iput v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->drmPauseCount:I

    #@16
    .line 552
    iput-boolean v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->hdcpEnabled:Z

    #@18
    .line 553
    iput-boolean v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->uibcSupported:Z

    #@1a
    .line 554
    iput-boolean v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->uibcEnabled:Z

    #@1c
    .line 555
    iput-object v3, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->connectedDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    #@1e
    .line 556
    iput-boolean v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->noDirectDisconnect:Z

    #@20
    .line 559
    iput-boolean v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdDlnaTransitEnabled:Z

    #@22
    .line 560
    iput-boolean v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdPlayingDlna:Z

    #@24
    .line 561
    iput-boolean v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdViaWifi:Z

    #@26
    .line 562
    iput-object v3, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->udnForDlnaTransit:Ljava/lang/String;

    #@28
    .line 563
    iput-object v3, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->wfdInterfaceName:Ljava/lang/String;

    #@2a
    .line 564
    sput-object v3, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->connectedUdn:Ljava/lang/String;

    #@2c
    .line 565
    sput-object v3, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->pendingUdn:Ljava/lang/String;

    #@2e
    .line 568
    iput-boolean v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->ongoingNoti:Z

    #@30
    .line 569
    iput-object v3, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mNotification:Landroid/app/Notification;

    #@32
    .line 570
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mContext:Landroid/content/Context;

    #@34
    const-string v2, "notification"

    #@36
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@39
    move-result-object v0

    #@3a
    check-cast v0, Landroid/app/NotificationManager;

    #@3c
    .line 572
    .local v0, notificationManager:Landroid/app/NotificationManager;
    if-eqz v0, :cond_44

    #@3e
    .line 573
    const v1, 0x20206e1

    #@41
    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    #@44
    .line 575
    :cond_44
    return-void
.end method

.method private isCallRunning()Z
    .registers 4

    #@0
    .prologue
    .line 1905
    const-string v0, "WfdStateTracker"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "isCallRunning mCallState:"

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    sget-object v2, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mCallState:Ljava/lang/String;

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 1906
    sget-object v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mCallState:Ljava/lang/String;

    #@1c
    const-string v1, "IDLE"

    #@1e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21
    move-result v0

    #@22
    if-nez v0, :cond_26

    #@24
    .line 1907
    const/4 v0, 0x1

    #@25
    .line 1910
    :goto_25
    return v0

    #@26
    :cond_26
    const/4 v0, 0x0

    #@27
    goto :goto_25
.end method

.method private setRtspState(I)V
    .registers 4
    .parameter "rtspState"

    #@0
    .prologue
    .line 849
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getRtspState()I

    #@3
    move-result v1

    #@4
    if-ne v1, p1, :cond_7

    #@6
    .line 856
    :goto_6
    return-void

    #@7
    .line 852
    :cond_7
    new-instance v0, Landroid/content/Intent;

    #@9
    const-string v1, "com.lge.systemservice.core.wfdmanager.rtsp.STATE_CHANGE"

    #@b
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@e
    .line 853
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "rtsp_state"

    #@10
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@13
    .line 854
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mRtspState:Ljava/util/concurrent/atomic/AtomicInteger;

    #@15
    invoke-virtual {v1, p1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    #@18
    .line 855
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mContext:Landroid/content/Context;

    #@1a
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@1d
    goto :goto_6
.end method

.method private declared-synchronized setWfdState(I)V
    .registers 8
    .parameter "wfdState"

    #@0
    .prologue
    const/4 v5, 0x4

    #@1
    .line 682
    monitor-enter p0

    #@2
    :try_start_2
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdState()I
    :try_end_5
    .catchall {:try_start_2 .. :try_end_5} :catchall_36

    #@5
    move-result v2

    #@6
    if-ne v2, p1, :cond_e

    #@8
    if-eqz p1, :cond_e

    #@a
    if-eq p1, v5, :cond_e

    #@c
    .line 842
    :goto_c
    monitor-exit p0

    #@d
    return-void

    #@e
    .line 687
    :cond_e
    :try_start_e
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdState()I

    #@11
    move-result v2

    #@12
    const/4 v3, 0x7

    #@13
    if-ne v2, v3, :cond_39

    #@15
    if-eqz p1, :cond_39

    #@17
    .line 688
    const-string v2, "WfdStateTracker"

    #@19
    new-instance v3, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v4, "setWfdState: unexpected state "

    #@20
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@27
    move-result-object v3

    #@28
    const-string v4, " while on disabling. Ignore it"

    #@2a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v3

    #@2e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v3

    #@32
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_35
    .catchall {:try_start_e .. :try_end_35} :catchall_36

    #@35
    goto :goto_c

    #@36
    .line 682
    :catchall_36
    move-exception v2

    #@37
    monitor-exit p0

    #@38
    throw v2

    #@39
    .line 692
    :cond_39
    :try_start_39
    const-string v2, "WfdStateTracker"

    #@3b
    new-instance v3, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v4, "Wfd State Changed from "

    #@42
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v3

    #@46
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdState()I

    #@49
    move-result v4

    #@4a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v3

    #@4e
    const-string v4, " to "

    #@50
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v3

    #@54
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@57
    move-result-object v3

    #@58
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v3

    #@5c
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5f
    .line 694
    new-instance v1, Landroid/content/Intent;

    #@61
    const-string v2, "com.lge.systemservice.core.wfdmanager.WFD_STATE_CHANGED"

    #@63
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@66
    .line 695
    .local v1, intent:Landroid/content/Intent;
    const-string v2, "wfd_state"

    #@68
    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@6b
    .line 697
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mDialogP2pDisconnect:Landroid/app/AlertDialog;

    #@6d
    if-eqz v2, :cond_86

    #@6f
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mDialogP2pDisconnect:Landroid/app/AlertDialog;

    #@71
    invoke-virtual {v2}, Landroid/app/AlertDialog;->isShowing()Z

    #@74
    move-result v2

    #@75
    if-eqz v2, :cond_86

    #@77
    .line 698
    const-string v2, "WfdStateTracker"

    #@79
    const-string v3, "Clear Disconnect dialog"

    #@7b
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7e
    .line 699
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mDialogP2pDisconnect:Landroid/app/AlertDialog;

    #@80
    invoke-virtual {v2}, Landroid/app/AlertDialog;->dismiss()V

    #@83
    .line 700
    const/4 v2, 0x0

    #@84
    iput-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mDialogP2pDisconnect:Landroid/app/AlertDialog;

    #@86
    .line 715
    :cond_86
    packed-switch p1, :pswitch_data_1c4

    #@89
    .line 826
    :cond_89
    :goto_89
    :pswitch_89
    const/4 v2, 0x5

    #@8a
    if-ne p1, v2, :cond_1b7

    #@8c
    .line 827
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->showNotification()V

    #@8f
    .line 828
    iget-boolean v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdDlnaTransitEnabled:Z

    #@91
    if-eqz v2, :cond_1ae

    #@93
    .line 829
    const-string v2, "wfd_switch"

    #@95
    const-string v3, "on"

    #@97
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@9a
    .line 837
    :goto_9a
    const-string v2, "dlan_udn"

    #@9c
    iget-object v3, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->udnForDlnaTransit:Ljava/lang/String;

    #@9e
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@a1
    .line 840
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdState:Ljava/util/concurrent/atomic/AtomicInteger;

    #@a3
    invoke-virtual {v2, p1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    #@a6
    .line 841
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mContext:Landroid/content/Context;

    #@a8
    invoke-virtual {v2, v1}, Landroid/content/Context;->sendStickyBroadcast(Landroid/content/Intent;)V

    #@ab
    goto/16 :goto_c

    #@ad
    .line 717
    :pswitch_ad
    iget-boolean v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdNeedConnectionPopUp:Z

    #@af
    if-eqz v2, :cond_b4

    #@b1
    .line 718
    const/4 v2, 0x0

    #@b2
    iput-boolean v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdNeedConnectionPopUp:Z

    #@b4
    .line 720
    :cond_b4
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->initWfdDisabledState()V

    #@b7
    goto :goto_89

    #@b8
    .line 725
    :pswitch_b8
    iget-boolean v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdNeedConnectionPopUp:Z

    #@ba
    if-eqz v2, :cond_db

    #@bc
    .line 726
    const/4 v2, 0x0

    #@bd
    iput-boolean v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdNeedConnectionPopUp:Z

    #@bf
    .line 729
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getStatusBarManager()Landroid/app/StatusBarManager;

    #@c2
    move-result-object v2

    #@c3
    if-eqz v2, :cond_ca

    #@c5
    .line 730
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mStatusBarManager:Landroid/app/StatusBarManager;

    #@c7
    invoke-virtual {v2}, Landroid/app/StatusBarManager;->collapsePanels()V

    #@ca
    .line 732
    :cond_ca
    new-instance v0, Landroid/content/Intent;

    #@cc
    const-string v2, "android.settings.WIFI_SCREEN_DIALOG"

    #@ce
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@d1
    .line 733
    .local v0, dialogIntent:Landroid/content/Intent;
    const/high16 v2, 0x1000

    #@d3
    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@d6
    .line 734
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mContext:Landroid/content/Context;

    #@d8
    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@db
    .line 737
    .end local v0           #dialogIntent:Landroid/content/Intent;
    :cond_db
    iget-boolean v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->uibcEnabled:Z

    #@dd
    if-eqz v2, :cond_e8

    #@df
    .line 738
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdAdaptation:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@e1
    const/4 v3, 0x0

    #@e2
    invoke-virtual {v2, v3}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->startUIBC(Z)Z

    #@e5
    .line 739
    const/4 v2, 0x0

    #@e6
    iput-boolean v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->uibcEnabled:Z

    #@e8
    .line 742
    :cond_e8
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdAdaptation:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@ea
    const/4 v3, 0x1

    #@eb
    const/4 v4, 0x1

    #@ec
    invoke-virtual {v2, v3, v4}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->setWfdIe(ZZ)Z

    #@ef
    .line 744
    const/4 v2, 0x0

    #@f0
    iput-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->connectedDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    #@f2
    .line 745
    const/4 v2, 0x0

    #@f3
    iput-boolean v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdViaWifi:Z

    #@f5
    .line 746
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->startEventLoop()V

    #@f8
    .line 747
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->stopRtspThread()Z

    #@fb
    goto :goto_89

    #@fc
    .line 752
    :pswitch_fc
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdState()I

    #@ff
    move-result v2

    #@100
    if-eq v2, v5, :cond_13f

    #@102
    .line 753
    iget-boolean v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdNeedConnectionPopUp:Z

    #@104
    if-eqz v2, :cond_125

    #@106
    .line 754
    const/4 v2, 0x0

    #@107
    iput-boolean v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdNeedConnectionPopUp:Z

    #@109
    .line 757
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getStatusBarManager()Landroid/app/StatusBarManager;

    #@10c
    move-result-object v2

    #@10d
    if-eqz v2, :cond_114

    #@10f
    .line 758
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mStatusBarManager:Landroid/app/StatusBarManager;

    #@111
    invoke-virtual {v2}, Landroid/app/StatusBarManager;->collapsePanels()V

    #@114
    .line 760
    :cond_114
    new-instance v0, Landroid/content/Intent;

    #@116
    const-string v2, "android.settings.WIFI_SCREEN_DIALOG"

    #@118
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@11b
    .line 761
    .restart local v0       #dialogIntent:Landroid/content/Intent;
    const/high16 v2, 0x1000

    #@11d
    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@120
    .line 762
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mContext:Landroid/content/Context;

    #@122
    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@125
    .line 764
    .end local v0           #dialogIntent:Landroid/content/Intent;
    :cond_125
    sget-object v2, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->myIpAddr:Ljava/lang/String;

    #@127
    if-nez v2, :cond_12d

    #@129
    .line 765
    const-string v2, "0.0.0.0"

    #@12b
    sput-object v2, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->myIpAddr:Ljava/lang/String;

    #@12d
    .line 767
    :cond_12d
    const/4 v2, 0x0

    #@12e
    sput-object v2, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->connectedUdn:Ljava/lang/String;

    #@130
    .line 768
    const/4 v2, 0x0

    #@131
    iput-boolean v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdViaWifi:Z

    #@133
    .line 769
    sget-object v2, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->myIpAddr:Ljava/lang/String;

    #@135
    iget v3, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->rtspPort:I

    #@137
    invoke-direct {p0, v2, v3}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->initRtspServer(Ljava/lang/String;I)Z

    #@13a
    .line 770
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->startRtspThread()Z

    #@13d
    goto/16 :goto_89

    #@13f
    .line 772
    :cond_13f
    sget-object v2, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->connectedUdn:Ljava/lang/String;

    #@141
    if-eqz v2, :cond_15c

    #@143
    .line 773
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mHandler:Landroid/os/Handler;

    #@145
    const/16 v3, 0x3ee

    #@147
    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    #@14a
    .line 774
    const-string v2, "connected_udn"

    #@14c
    sget-object v3, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->connectedUdn:Ljava/lang/String;

    #@14e
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@151
    .line 775
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mHandler:Landroid/os/Handler;

    #@153
    const/16 v3, 0x3ee

    #@155
    const-wide/16 v4, 0x4e20

    #@157
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@15a
    goto/16 :goto_89

    #@15c
    .line 777
    :cond_15c
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mHandler:Landroid/os/Handler;

    #@15e
    const/16 v3, 0x3ee

    #@160
    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    #@163
    goto/16 :goto_89

    #@165
    .line 784
    :pswitch_165
    iget-boolean v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdViaWifi:Z

    #@167
    if-eqz v2, :cond_195

    #@169
    .line 785
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdAdaptation:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@16b
    const/4 v3, 0x0

    #@16c
    const/4 v4, 0x0

    #@16d
    invoke-virtual {v2, v3, v4}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->setWfdIe(ZZ)Z

    #@170
    .line 790
    :goto_170
    iget-boolean v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->uibcSupported:Z

    #@172
    if-eqz v2, :cond_17a

    #@174
    .line 791
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdAdaptation:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@176
    const/4 v3, 0x1

    #@177
    invoke-virtual {v2, v3}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->startUIBC(Z)Z

    #@17a
    .line 793
    :cond_17a
    sget-object v2, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->connectedUdn:Ljava/lang/String;

    #@17c
    if-eqz v2, :cond_18c

    #@17e
    .line 794
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mHandler:Landroid/os/Handler;

    #@180
    const/16 v3, 0x3ee

    #@182
    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    #@185
    .line 795
    const-string v2, "connected_udn"

    #@187
    sget-object v3, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->connectedUdn:Ljava/lang/String;

    #@189
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@18c
    .line 797
    :cond_18c
    const-string v2, "hdcp_enabled"

    #@18e
    iget-boolean v3, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->hdcpEnabled:Z

    #@190
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@193
    goto/16 :goto_89

    #@195
    .line 787
    :cond_195
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdAdaptation:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@197
    const/4 v3, 0x1

    #@198
    const/4 v4, 0x0

    #@199
    invoke-virtual {v2, v3, v4}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->setWfdIe(ZZ)Z

    #@19c
    goto :goto_170

    #@19d
    .line 803
    :pswitch_19d
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@19f
    if-eqz v2, :cond_89

    #@1a1
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdAdaptation:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@1a3
    iget-boolean v2, v2, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->concurrentScanEnabled:Z

    #@1a5
    if-nez v2, :cond_89

    #@1a7
    .line 814
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdAdaptation:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@1a9
    const/4 v3, 0x1

    #@1aa
    iput-boolean v3, v2, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->concurrentScanEnabled:Z

    #@1ac
    goto/16 :goto_89

    #@1ae
    .line 831
    :cond_1ae
    const-string v2, "wfd_switch"

    #@1b0
    const-string v3, "off"

    #@1b2
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@1b5
    goto/16 :goto_9a

    #@1b7
    .line 834
    :cond_1b7
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->clearNotification()V

    #@1ba
    .line 835
    const-string v2, "wfd_switch"

    #@1bc
    const-string v3, "off"

    #@1be
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_1c1
    .catchall {:try_start_39 .. :try_end_1c1} :catchall_36

    #@1c1
    goto/16 :goto_9a

    #@1c3
    .line 715
    nop

    #@1c4
    :pswitch_data_1c4
    .packed-switch 0x0
        :pswitch_ad
        :pswitch_89
        :pswitch_b8
        :pswitch_89
        :pswitch_fc
        :pswitch_165
        :pswitch_89
        :pswitch_19d
        :pswitch_89
    .end packed-switch
.end method

.method private showNotification()V
    .registers 9

    #@0
    .prologue
    const v7, 0x20206e1

    #@3
    const/4 v4, 0x0

    #@4
    .line 1755
    iget-boolean v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->ongoingNoti:Z

    #@6
    if-eqz v0, :cond_9

    #@8
    .line 1803
    :cond_8
    :goto_8
    return-void

    #@9
    .line 1759
    :cond_9
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mContext:Landroid/content/Context;

    #@b
    const-string v1, "notification"

    #@d
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@10
    move-result-object v0

    #@11
    check-cast v0, Landroid/app/NotificationManager;

    #@13
    .line 1761
    if-eqz v0, :cond_8

    #@15
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mNotification:Landroid/app/Notification;

    #@17
    if-nez v1, :cond_8

    #@19
    .line 1765
    new-instance v1, Landroid/content/Intent;

    #@1b
    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    #@1e
    .line 1766
    const-string v2, "android.intent.action.MAIN"

    #@20
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@23
    .line 1767
    const-string v2, "com.android.settings"

    #@25
    const-string v3, "com.android.settings.Settings$WifiScreenSettingsActivity"

    #@27
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@2a
    .line 1768
    const/high16 v2, 0x1400

    #@2c
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@2f
    .line 1770
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mContext:Landroid/content/Context;

    #@31
    invoke-static {v2, v4, v1, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@34
    move-result-object v1

    #@35
    .line 1772
    new-instance v2, Ljava/lang/String;

    #@37
    const-string v3, ""

    #@39
    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    #@3c
    .line 1787
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mContext:Landroid/content/Context;

    #@3e
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@41
    move-result-object v2

    #@42
    const v3, 0x2090391

    #@45
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@48
    move-result-object v2

    #@49
    .line 1788
    iget-object v3, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mContext:Landroid/content/Context;

    #@4b
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@4e
    move-result-object v3

    #@4f
    const v4, 0x2090398

    #@52
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@55
    move-result-object v3

    #@56
    .line 1793
    new-instance v4, Landroid/app/Notification;

    #@58
    invoke-direct {v4}, Landroid/app/Notification;-><init>()V

    #@5b
    iput-object v4, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mNotification:Landroid/app/Notification;

    #@5d
    .line 1794
    iget-object v4, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mNotification:Landroid/app/Notification;

    #@5f
    const-wide/16 v5, 0x0

    #@61
    iput-wide v5, v4, Landroid/app/Notification;->when:J

    #@63
    .line 1795
    iget-object v4, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mNotification:Landroid/app/Notification;

    #@65
    iput v7, v4, Landroid/app/Notification;->icon:I

    #@67
    .line 1796
    iget-object v4, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mNotification:Landroid/app/Notification;

    #@69
    iget v5, v4, Landroid/app/Notification;->defaults:I

    #@6b
    and-int/lit8 v5, v5, -0x2

    #@6d
    iput v5, v4, Landroid/app/Notification;->defaults:I

    #@6f
    .line 1797
    iget-object v4, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mNotification:Landroid/app/Notification;

    #@71
    const/16 v5, 0x1002

    #@73
    iput v5, v4, Landroid/app/Notification;->flags:I

    #@75
    .line 1798
    iget-object v4, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mNotification:Landroid/app/Notification;

    #@77
    iput-object v2, v4, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    #@79
    .line 1799
    iget-object v4, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mNotification:Landroid/app/Notification;

    #@7b
    iget-object v5, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mContext:Landroid/content/Context;

    #@7d
    invoke-virtual {v4, v5, v2, v3, v1}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@80
    .line 1801
    const/4 v1, 0x1

    #@81
    iput-boolean v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->ongoingNoti:Z

    #@83
    .line 1802
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mNotification:Landroid/app/Notification;

    #@85
    invoke-virtual {v0, v7, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    #@88
    goto :goto_8
.end method

.method private startEventLoop()V
    .registers 2

    #@0
    .prologue
    .line 668
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdMonitor:Lcom/lge/systemservice/service/wfdservice/WfdMonitor;

    #@2
    invoke-virtual {v0}, Lcom/lge/systemservice/service/wfdservice/WfdMonitor;->startMonitoring()V

    #@5
    .line 670
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->initRtspEnv()Z

    #@8
    .line 671
    return-void
.end method

.method private declared-synchronized startRtspThread()Z
    .registers 2

    #@0
    .prologue
    .line 590
    monitor-enter p0

    #@1
    :try_start_1
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdState()I

    #@4
    move-result v0

    #@5
    if-eqz v0, :cond_f

    #@7
    .line 591
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdAdaptation:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@9
    invoke-virtual {v0}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->rtspStartThread()Z
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_11

    #@c
    move-result v0

    #@d
    .line 594
    :goto_d
    monitor-exit p0

    #@e
    return v0

    #@f
    :cond_f
    const/4 v0, 0x0

    #@10
    goto :goto_d

    #@11
    .line 590
    :catchall_11
    move-exception v0

    #@12
    monitor-exit p0

    #@13
    throw v0
.end method

.method private stopEventLoop()V
    .registers 2

    #@0
    .prologue
    .line 674
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdMonitor:Lcom/lge/systemservice/service/wfdservice/WfdMonitor;

    #@2
    invoke-virtual {v0}, Lcom/lge/systemservice/service/wfdservice/WfdMonitor;->stopMonitoring()V

    #@5
    .line 675
    return-void
.end method

.method private declared-synchronized stopRtspThread()Z
    .registers 2

    #@0
    .prologue
    .line 599
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdAdaptation:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@3
    invoke-virtual {v0}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->rtspStopThread()Z
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_9

    #@6
    move-result v0

    #@7
    monitor-exit p0

    #@8
    return v0

    #@9
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0

    #@b
    throw v0
.end method


# virtual methods
.method public cancelWifiDisplayConnect()Z
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x2

    #@2
    const/4 v5, 0x6

    #@3
    const/4 v0, 0x1

    #@4
    const/4 v1, 0x0

    #@5
    .line 1194
    const-string v2, "WfdStateTracker"

    #@7
    new-instance v3, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v4, "cancelWifiDisplayConnect: "

    #@e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v3

    #@12
    iget-object v4, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mDirectState:Ljava/util/concurrent/atomic/AtomicInteger;

    #@14
    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@17
    move-result v4

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v3

    #@20
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 1195
    iget-boolean v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->uibcEnabled:Z

    #@25
    if-eqz v2, :cond_41

    #@27
    .line 1196
    const-string v2, "WfdStateTracker"

    #@29
    const-string v3, "pending cancelWifiDisplayConnect"

    #@2b
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 1197
    sget-object v2, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;->WFD_DISCONNECT:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

    #@30
    iput-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdPendingTeardown:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

    #@32
    .line 1198
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mHandler:Landroid/os/Handler;

    #@34
    const/16 v3, 0x3ed

    #@36
    const-wide/16 v4, 0xfa0

    #@38
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@3b
    .line 1199
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdAdaptation:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@3d
    invoke-virtual {v2, v1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->startUIBC(Z)Z

    #@40
    .line 1269
    :cond_40
    :goto_40
    return v0

    #@41
    .line 1202
    :cond_41
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdState()I

    #@44
    move-result v2

    #@45
    const/4 v3, 0x5

    #@46
    if-eq v2, v3, :cond_4e

    #@48
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdState()I

    #@4b
    move-result v2

    #@4c
    if-ne v2, v7, :cond_66

    #@4e
    .line 1203
    :cond_4e
    iget-boolean v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdViaWifi:Z

    #@50
    if-eqz v1, :cond_56

    #@52
    .line 1204
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->teardown()Z

    #@55
    goto :goto_40

    #@56
    .line 1206
    :cond_56
    invoke-direct {p0, v5}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@59
    .line 1207
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->teardown()Z

    #@5c
    .line 1208
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mHandler:Landroid/os/Handler;

    #@5e
    const/16 v2, 0x3eb

    #@60
    const-wide/16 v3, 0x1388

    #@62
    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@65
    goto :goto_40

    #@66
    .line 1211
    :cond_66
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mDirectState:Ljava/util/concurrent/atomic/AtomicInteger;

    #@68
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@6b
    move-result v2

    #@6c
    packed-switch v2, :pswitch_data_100

    #@6f
    .line 1265
    const-string v0, "WfdStateTracker"

    #@71
    new-instance v2, Ljava/lang/StringBuilder;

    #@73
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    const-string v3, "cancelWifiDisplayConnect: Unhandled direct state "

    #@78
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v2

    #@7c
    iget-object v3, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mDirectState:Ljava/util/concurrent/atomic/AtomicInteger;

    #@7e
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@81
    move-result v3

    #@82
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@85
    move-result-object v2

    #@86
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@89
    move-result-object v2

    #@8a
    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8d
    move v0, v1

    #@8e
    .line 1266
    goto :goto_40

    #@8f
    .line 1214
    :pswitch_8f
    const-string v0, "WfdStateTracker"

    #@91
    const-string v2, "cancelWifiDisplayConnect: DIRECT is not connected"

    #@93
    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@96
    move v0, v1

    #@97
    .line 1215
    goto :goto_40

    #@98
    .line 1218
    :pswitch_98
    invoke-direct {p0, v5}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@9b
    .line 1219
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@9d
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@9f
    new-instance v3, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$7;

    #@a1
    invoke-direct {v3, p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$7;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)V

    #@a4
    invoke-virtual {v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->cancelConnect(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    #@a7
    .line 1227
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->teardown()Z

    #@aa
    .line 1228
    iget v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiState:I

    #@ac
    if-nez v1, :cond_b2

    #@ae
    .line 1229
    invoke-direct {p0, v6}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@b1
    goto :goto_40

    #@b2
    .line 1230
    :cond_b2
    iget v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiState:I

    #@b4
    if-ne v1, v0, :cond_40

    #@b6
    .line 1231
    invoke-direct {p0, v6}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@b9
    .line 1232
    invoke-direct {p0, v7}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@bc
    goto :goto_40

    #@bd
    .line 1236
    :pswitch_bd
    invoke-direct {p0, v5}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@c0
    .line 1237
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getRtspState()I

    #@c3
    move-result v1

    #@c4
    const/4 v2, 0x5

    #@c5
    if-eq v1, v2, :cond_cd

    #@c7
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getRtspState()I

    #@ca
    move-result v1

    #@cb
    if-ne v1, v5, :cond_db

    #@cd
    .line 1238
    :cond_cd
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->teardown()Z

    #@d0
    .line 1239
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mHandler:Landroid/os/Handler;

    #@d2
    const/16 v2, 0x3eb

    #@d4
    const-wide/16 v3, 0x1388

    #@d6
    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@d9
    goto/16 :goto_40

    #@db
    .line 1241
    :cond_db
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->teardown()Z

    #@de
    .line 1242
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@e0
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@e2
    new-instance v3, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$8;

    #@e4
    invoke-direct {v3, p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$8;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)V

    #@e7
    invoke-virtual {v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->removeGroup(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    #@ea
    goto/16 :goto_40

    #@ec
    .line 1259
    :pswitch_ec
    const-string v1, "WfdStateTracker"

    #@ee
    const-string v2, "cancelWifiDisplayConnect: already on disconnecting"

    #@f0
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f3
    goto/16 :goto_40

    #@f5
    .line 1262
    :pswitch_f5
    const-string v0, "WfdStateTracker"

    #@f7
    const-string v2, "cancelWifiDisplayConnect: Direct is diabling"

    #@f9
    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@fc
    move v0, v1

    #@fd
    .line 1263
    goto/16 :goto_40

    #@ff
    .line 1211
    nop

    #@100
    :pswitch_data_100
    .packed-switch 0x0
        :pswitch_8f
        :pswitch_8f
        :pswitch_98
        :pswitch_98
        :pswitch_bd
        :pswitch_ec
        :pswitch_f5
    .end packed-switch
.end method

.method public declared-synchronized getRtspAddress()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 607
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdAdaptation:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@3
    invoke-virtual {v0}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->rtspGetMyAddress()Ljava/lang/String;
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_9

    #@6
    move-result-object v0

    #@7
    monitor-exit p0

    #@8
    return-object v0

    #@9
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0

    #@b
    throw v0
.end method

.method public getRtspState()I
    .registers 2

    #@0
    .prologue
    .line 845
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mRtspState:Ljava/util/concurrent/atomic/AtomicInteger;

    #@2
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getTargetUrl()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 2086
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->targetUrl:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getWfdMode()I
    .registers 2

    #@0
    .prologue
    .line 2073
    iget v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->wfdMode:I

    #@2
    return v0
.end method

.method public getWfdState()I
    .registers 2

    #@0
    .prologue
    .line 678
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdState:Ljava/util/concurrent/atomic/AtomicInteger;

    #@2
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public handleBatteryEvent(Z)V
    .registers 5
    .parameter

    #@0
    .prologue
    .line 1939
    const-string v0, "WfdStateTracker"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "handleBatteryEvent: "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1940
    iput-boolean p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->isBatteryLow:Z

    #@1a
    .line 1941
    if-eqz p1, :cond_33

    #@1c
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdState()I

    #@1f
    move-result v0

    #@20
    if-eqz v0, :cond_33

    #@22
    .line 1942
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mContext:Landroid/content/Context;

    #@24
    const v1, 0x209039d

    #@27
    const/4 v2, 0x1

    #@28
    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    #@2b
    move-result-object v0

    #@2c
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    #@2f
    .line 1945
    const/4 v0, 0x0

    #@30
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWifiDisplayEnabled(Z)Z

    #@33
    .line 1947
    :cond_33
    return-void
.end method

.method public handleDRMPauseEvent(Ljava/lang/String;II)V
    .registers 10
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v2, 0x0

    #@2
    const/4 v1, 0x1

    #@3
    .line 1977
    packed-switch p2, :pswitch_data_122

    #@6
    .line 2029
    const-string v0, "WfdStateTracker"

    #@8
    new-instance v3, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v4, "cannot handle request "

    #@f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 2033
    :cond_1e
    :goto_1e
    iget-boolean v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->hdcpEnabled:Z

    #@20
    if-eqz v0, :cond_117

    #@22
    .line 2034
    iget v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->forcedDrmPauseCount:I

    #@24
    if-lez v0, :cond_114

    #@26
    move v0, v1

    #@27
    :goto_27
    invoke-virtual {p0, v0, p3}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->handlePauseEvent(ZI)V

    #@2a
    .line 2038
    :goto_2a
    return-void

    #@2b
    .line 1979
    :pswitch_2b
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->drmMap:Ljava/util/HashMap;

    #@2d
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@30
    move-result v0

    #@31
    if-eqz v0, :cond_7c

    #@33
    .line 1980
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->drmMap:Ljava/util/HashMap;

    #@35
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@38
    move-result-object v0

    #@39
    check-cast v0, Ljava/lang/Integer;

    #@3b
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@3e
    move-result v0

    #@3f
    .line 1981
    if-ne v0, v1, :cond_4c

    #@41
    .line 1982
    iget v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->drmPauseCount:I

    #@43
    if-lez v0, :cond_1e

    #@45
    .line 1983
    iget v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->drmPauseCount:I

    #@47
    add-int/lit8 v0, v0, -0x1

    #@49
    iput v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->drmPauseCount:I

    #@4b
    goto :goto_1e

    #@4c
    .line 1985
    :cond_4c
    if-ne v0, v4, :cond_63

    #@4e
    .line 1986
    iget v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->forcedDrmPauseCount:I

    #@50
    if-lez v0, :cond_58

    #@52
    .line 1987
    iget v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->forcedDrmPauseCount:I

    #@54
    add-int/lit8 v0, v0, -0x1

    #@56
    iput v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->forcedDrmPauseCount:I

    #@58
    .line 1989
    :cond_58
    iget v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->drmPauseCount:I

    #@5a
    if-lez v0, :cond_1e

    #@5c
    .line 1990
    iget v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->drmPauseCount:I

    #@5e
    add-int/lit8 v0, v0, -0x1

    #@60
    iput v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->drmPauseCount:I

    #@62
    goto :goto_1e

    #@63
    .line 1993
    :cond_63
    const-string v3, "WfdStateTracker"

    #@65
    new-instance v4, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    const-string v5, "Can\'t get proper value: "

    #@6c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v4

    #@70
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@73
    move-result-object v0

    #@74
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@77
    move-result-object v0

    #@78
    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7b
    goto :goto_1e

    #@7c
    .line 1996
    :cond_7c
    const-string v0, "WfdStateTracker"

    #@7e
    const-string v3, "already removed - ignore"

    #@80
    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@83
    goto :goto_1e

    #@84
    .line 2000
    :pswitch_84
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->drmMap:Ljava/util/HashMap;

    #@86
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@89
    move-result v0

    #@8a
    if-eqz v0, :cond_b7

    #@8c
    .line 2001
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->drmMap:Ljava/util/HashMap;

    #@8e
    new-instance v3, Ljava/lang/Integer;

    #@90
    invoke-direct {v3, p2}, Ljava/lang/Integer;-><init>(I)V

    #@93
    invoke-virtual {v0, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@96
    move-result-object v0

    #@97
    check-cast v0, Ljava/lang/Integer;

    #@99
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@9c
    move-result v0

    #@9d
    .line 2002
    if-ne v0, v4, :cond_ae

    #@9f
    .line 2003
    const-string v0, "WfdStateTracker"

    #@a1
    const-string v3, "changed req category to 1"

    #@a3
    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@a6
    .line 2004
    iget v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->forcedDrmPauseCount:I

    #@a8
    add-int/lit8 v0, v0, -0x1

    #@aa
    iput v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->forcedDrmPauseCount:I

    #@ac
    goto/16 :goto_1e

    #@ae
    .line 2006
    :cond_ae
    const-string v0, "WfdStateTracker"

    #@b0
    const-string v3, "duplicated input or not vaild input"

    #@b2
    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@b5
    goto/16 :goto_1e

    #@b7
    .line 2009
    :cond_b7
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->drmMap:Ljava/util/HashMap;

    #@b9
    new-instance v3, Ljava/lang/Integer;

    #@bb
    invoke-direct {v3, p2}, Ljava/lang/Integer;-><init>(I)V

    #@be
    invoke-virtual {v0, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@c1
    .line 2010
    iget v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->drmPauseCount:I

    #@c3
    add-int/lit8 v0, v0, 0x1

    #@c5
    iput v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->drmPauseCount:I

    #@c7
    goto/16 :goto_1e

    #@c9
    .line 2014
    :pswitch_c9
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->drmMap:Ljava/util/HashMap;

    #@cb
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@ce
    move-result v0

    #@cf
    if-eqz v0, :cond_fc

    #@d1
    .line 2015
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->drmMap:Ljava/util/HashMap;

    #@d3
    new-instance v3, Ljava/lang/Integer;

    #@d5
    invoke-direct {v3, p2}, Ljava/lang/Integer;-><init>(I)V

    #@d8
    invoke-virtual {v0, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@db
    move-result-object v0

    #@dc
    check-cast v0, Ljava/lang/Integer;

    #@de
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@e1
    move-result v0

    #@e2
    .line 2016
    if-ne v0, v1, :cond_f3

    #@e4
    .line 2017
    const-string v0, "WfdStateTracker"

    #@e6
    const-string v3, "changed req category to 2"

    #@e8
    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@eb
    .line 2018
    iget v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->forcedDrmPauseCount:I

    #@ed
    add-int/lit8 v0, v0, 0x1

    #@ef
    iput v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->forcedDrmPauseCount:I

    #@f1
    goto/16 :goto_1e

    #@f3
    .line 2020
    :cond_f3
    const-string v0, "WfdStateTracker"

    #@f5
    const-string v3, "duplicated input or not vaild input"

    #@f7
    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@fa
    goto/16 :goto_1e

    #@fc
    .line 2023
    :cond_fc
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->drmMap:Ljava/util/HashMap;

    #@fe
    new-instance v3, Ljava/lang/Integer;

    #@100
    invoke-direct {v3, p2}, Ljava/lang/Integer;-><init>(I)V

    #@103
    invoke-virtual {v0, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@106
    .line 2024
    iget v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->drmPauseCount:I

    #@108
    add-int/lit8 v0, v0, 0x1

    #@10a
    iput v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->drmPauseCount:I

    #@10c
    .line 2025
    iget v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->forcedDrmPauseCount:I

    #@10e
    add-int/lit8 v0, v0, 0x1

    #@110
    iput v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->forcedDrmPauseCount:I

    #@112
    goto/16 :goto_1e

    #@114
    :cond_114
    move v0, v2

    #@115
    .line 2034
    goto/16 :goto_27

    #@117
    .line 2036
    :cond_117
    iget v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->drmPauseCount:I

    #@119
    if-lez v0, :cond_120

    #@11b
    :goto_11b
    invoke-virtual {p0, v1, p3}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->handlePauseEvent(ZI)V

    #@11e
    goto/16 :goto_2a

    #@120
    :cond_120
    move v1, v2

    #@121
    goto :goto_11b

    #@122
    .line 1977
    :pswitch_data_122
    .packed-switch 0x0
        :pswitch_2b
        :pswitch_84
        :pswitch_c9
    .end packed-switch
.end method

.method public handleDirectStateChangedEvent(I)V
    .registers 10
    .parameter

    #@0
    .prologue
    const/4 v7, 0x5

    #@1
    const/4 v6, 0x0

    #@2
    const/4 v5, 0x4

    #@3
    const/4 v4, 0x2

    #@4
    const/4 v3, 0x1

    #@5
    .line 1464
    const-string v0, "WfdStateTracker"

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v2, "WfdStateTracker handleDirectStateChangedEvent: "

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 1465
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mDirectState:Ljava/util/concurrent/atomic/AtomicInteger;

    #@1f
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@22
    move-result v0

    #@23
    if-ne p1, v0, :cond_28

    #@25
    if-eqz p1, :cond_28

    #@27
    .line 1584
    :cond_27
    :goto_27
    :pswitch_27
    return-void

    #@28
    .line 1468
    :cond_28
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mDirectState:Ljava/util/concurrent/atomic/AtomicInteger;

    #@2a
    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    #@2d
    .line 1469
    iget-boolean v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdEnableState:Z

    #@2f
    if-nez v0, :cond_39

    #@31
    if-nez p1, :cond_27

    #@33
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdState()I

    #@36
    move-result v0

    #@37
    if-eqz v0, :cond_27

    #@39
    .line 1471
    :cond_39
    packed-switch p1, :pswitch_data_c6

    #@3c
    goto :goto_27

    #@3d
    .line 1473
    :pswitch_3d
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->stopEventLoop()V

    #@40
    .line 1474
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->stopRtspThread()Z

    #@43
    .line 1476
    invoke-direct {p0, v6}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@46
    .line 1477
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdPendingCmd:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;

    #@48
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;->ENABLE:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;

    #@4a
    if-ne v0, v1, :cond_27

    #@4c
    .line 1478
    sget-object v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;->NONE:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;

    #@4e
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdPendingCmd:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;

    #@50
    .line 1479
    iput-boolean v3, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdEnableState:Z

    #@52
    .line 1482
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->enableWifiP2p()Z

    #@55
    move-result v0

    #@56
    if-nez v0, :cond_27

    #@58
    .line 1483
    invoke-direct {p0, v6}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@5b
    goto :goto_27

    #@5c
    .line 1488
    :pswitch_5c
    invoke-direct {p0, v3}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@5f
    goto :goto_27

    #@60
    .line 1495
    :pswitch_60
    iget v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiState:I

    #@62
    if-ne v0, v3, :cond_96

    #@64
    .line 1496
    iget-boolean v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdViaWifi:Z

    #@66
    if-nez v0, :cond_7b

    #@68
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdState()I

    #@6b
    move-result v0

    #@6c
    if-ne v0, v7, :cond_7b

    #@6e
    .line 1497
    invoke-direct {p0, v4}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@71
    .line 1498
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getRtspState()I

    #@74
    move-result v0

    #@75
    const/4 v1, 0x3

    #@76
    if-ne v0, v1, :cond_7b

    #@78
    .line 1499
    invoke-direct {p0, v5}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@7b
    .line 1505
    :cond_7b
    :goto_7b
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdState()I

    #@7e
    move-result v0

    #@7f
    if-ne v0, v3, :cond_88

    #@81
    .line 1506
    iget v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiState:I

    #@83
    if-nez v0, :cond_9a

    #@85
    .line 1507
    invoke-direct {p0, v4}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@88
    .line 1513
    :cond_88
    :goto_88
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdPendingCmd:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;

    #@8a
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;->DISABLE:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;

    #@8c
    if-ne v0, v1, :cond_27

    #@8e
    .line 1514
    sget-object v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;->NONE:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;

    #@90
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdPendingCmd:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;

    #@92
    .line 1516
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->disableWifiP2p()Z

    #@95
    goto :goto_27

    #@96
    .line 1503
    :cond_96
    invoke-direct {p0, v4}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@99
    goto :goto_7b

    #@9a
    .line 1508
    :cond_9a
    iget v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiState:I

    #@9c
    if-ne v0, v3, :cond_88

    #@9e
    .line 1509
    invoke-direct {p0, v4}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@a1
    .line 1510
    invoke-direct {p0, v5}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@a4
    goto :goto_88

    #@a5
    .line 1525
    :pswitch_a5
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdState()I

    #@a8
    move-result v0

    #@a9
    if-eq v0, v5, :cond_27

    #@ab
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdState()I

    #@ae
    move-result v0

    #@af
    if-eq v0, v7, :cond_27

    #@b1
    .line 1526
    invoke-direct {p0, v5}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@b4
    goto/16 :goto_27

    #@b6
    .line 1572
    :pswitch_b6
    iget-boolean v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdViaWifi:Z

    #@b8
    if-nez v0, :cond_27

    #@ba
    .line 1573
    const/4 v0, 0x6

    #@bb
    invoke-direct {p0, v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@be
    goto/16 :goto_27

    #@c0
    .line 1578
    :pswitch_c0
    const/4 v0, 0x7

    #@c1
    invoke-direct {p0, v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@c4
    goto/16 :goto_27

    #@c6
    .line 1471
    :pswitch_data_c6
    .packed-switch 0x0
        :pswitch_3d
        :pswitch_5c
        :pswitch_60
        :pswitch_27
        :pswitch_a5
        :pswitch_b6
        :pswitch_c0
    .end packed-switch
.end method

.method public handleDlnaTransitRequest(Z)V
    .registers 5
    .parameter

    #@0
    .prologue
    .line 1873
    const-string v0, "WfdStateTracker"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "handleDlnaTransitRequest: "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1874
    iget-boolean v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdDlnaTransitEnabled:Z

    #@1a
    if-eqz v0, :cond_65

    #@1c
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->isCallRunning()Z

    #@1f
    move-result v0

    #@20
    if-nez v0, :cond_2f

    #@22
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWindowManager:Landroid/view/WindowManager;

    #@24
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    #@27
    move-result-object v0

    #@28
    invoke-virtual {v0}, Landroid/view/Display;->getDsdrStatus()I

    #@2b
    move-result v0

    #@2c
    const/4 v1, 0x3

    #@2d
    if-eq v0, v1, :cond_65

    #@2f
    .line 1878
    :cond_2f
    if-eqz p1, :cond_4b

    #@31
    .line 1879
    iget-boolean v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdPlayingDlna:Z

    #@33
    if-nez v0, :cond_43

    #@35
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getRtspState()I

    #@38
    move-result v0

    #@39
    const/4 v1, 0x6

    #@3a
    if-ne v0, v1, :cond_43

    #@3c
    .line 1880
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->pauseTransit()Z

    #@3f
    .line 1881
    const/4 v0, 0x1

    #@40
    iput-boolean v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdPlayingDlna:Z

    #@42
    .line 1896
    :goto_42
    return-void

    #@43
    .line 1883
    :cond_43
    const-string v0, "WfdStateTracker"

    #@45
    const-string v1, "handleDlnaTransitRequest: already paused"

    #@47
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    goto :goto_42

    #@4b
    .line 1886
    :cond_4b
    iget-boolean v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdPlayingDlna:Z

    #@4d
    if-eqz v0, :cond_5d

    #@4f
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getRtspState()I

    #@52
    move-result v0

    #@53
    const/4 v1, 0x5

    #@54
    if-ne v0, v1, :cond_5d

    #@56
    .line 1887
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->playTransit()Z

    #@59
    .line 1888
    const/4 v0, 0x0

    #@5a
    iput-boolean v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdPlayingDlna:Z

    #@5c
    goto :goto_42

    #@5d
    .line 1890
    :cond_5d
    const-string v0, "WfdStateTracker"

    #@5f
    const-string v1, "handleDlnaTransitRequest: already playing"

    #@61
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@64
    goto :goto_42

    #@65
    .line 1894
    :cond_65
    const-string v0, "WfdStateTracker"

    #@67
    const-string v1, "Unable to transit dlna"

    #@69
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@6c
    goto :goto_42
.end method

.method public handleHdmiConnected(Z)V
    .registers 4
    .parameter "connected"

    #@0
    .prologue
    .line 1916
    iput-boolean p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->isHdmiConnected:Z

    #@2
    .line 1917
    iget-boolean v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdEnableState:Z

    #@4
    const/4 v1, 0x1

    #@5
    if-ne v0, v1, :cond_b

    #@7
    .line 1918
    const/4 v0, 0x0

    #@8
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWifiDisplayEnabled(Z)Z

    #@b
    .line 1920
    :cond_b
    return-void
.end method

.method public handleP2pThisDeviceChanged(Landroid/net/wifi/p2p/WifiP2pDevice;)V
    .registers 4
    .parameter

    #@0
    .prologue
    .line 1628
    const-string v0, "WfdStateTracker"

    #@2
    const-string v1, "handleP2pThisDeviceChanged"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1629
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdAdaptation:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@9
    invoke-virtual {v0, p1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->updateThisDevice(Landroid/net/wifi/p2p/WifiP2pDevice;)V

    #@c
    .line 1630
    return-void
.end method

.method public handlePauseEvent(ZI)V
    .registers 6
    .parameter
    .parameter

    #@0
    .prologue
    .line 1923
    iget-boolean v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdEnableState:Z

    #@2
    if-eqz v0, :cond_3c

    #@4
    .line 1924
    const-string v0, "WfdStateTracker"

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "handlePauseEvent current: "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    iget v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->pauseStateBitMap:I

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, " request:"

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    const-string v2, " from "

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v1

    #@2f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 1926
    if-eqz p1, :cond_3d

    #@34
    .line 1928
    iget v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->pauseStateBitMap:I

    #@36
    or-int/2addr v0, p2

    #@37
    iput v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->pauseStateBitMap:I

    #@39
    .line 1934
    :goto_39
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->handleWfdPauseResumeState()V

    #@3c
    .line 1936
    :cond_3c
    return-void

    #@3d
    .line 1931
    :cond_3d
    iget v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->pauseStateBitMap:I

    #@3f
    xor-int/lit8 v1, p2, -0x1

    #@41
    and-int/2addr v0, v1

    #@42
    iput v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->pauseStateBitMap:I

    #@44
    goto :goto_39
.end method

.method public handleRequestSetWifiEnabled(I)V
    .registers 5
    .parameter

    #@0
    .prologue
    .line 2054
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getStatusBarManager()Landroid/app/StatusBarManager;

    #@3
    move-result-object v0

    #@4
    if-eqz v0, :cond_b

    #@6
    .line 2055
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mStatusBarManager:Landroid/app/StatusBarManager;

    #@8
    invoke-virtual {v0}, Landroid/app/StatusBarManager;->collapsePanels()V

    #@b
    .line 2057
    :cond_b
    packed-switch p1, :pswitch_data_1e

    #@e
    .line 2070
    :goto_e
    :pswitch_e
    return-void

    #@f
    .line 2062
    :pswitch_f
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mContext:Landroid/content/Context;

    #@11
    const v1, 0x2090399

    #@14
    const/4 v2, 0x1

    #@15
    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    #@1c
    goto :goto_e

    #@1d
    .line 2057
    nop

    #@1e
    :pswitch_data_1e
    .packed-switch 0x1
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method public handleRtspStateChange(I)V
    .registers 11
    .parameter "rtspState"

    #@0
    .prologue
    const/16 v8, 0x3ea

    #@2
    const/4 v7, 0x6

    #@3
    const/4 v6, 0x7

    #@4
    const/4 v5, 0x5

    #@5
    const/4 v4, 0x4

    #@6
    .line 1280
    const-string v1, "WfdStateTracker"

    #@8
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v3, "handleRtspStateChange ("

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    const-string v3, ") "

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 1282
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getRtspState()I

    #@27
    move-result v0

    #@28
    .line 1283
    .local v0, prevRtspState:I
    invoke-direct {p0, p1}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setRtspState(I)V

    #@2b
    .line 1285
    packed-switch p1, :pswitch_data_17c

    #@2e
    .line 1461
    :cond_2e
    :goto_2e
    :pswitch_2e
    return-void

    #@2f
    .line 1288
    :pswitch_2f
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdState()I

    #@32
    move-result v1

    #@33
    if-eq v1, v5, :cond_3b

    #@35
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdState()I

    #@38
    move-result v1

    #@39
    if-ne v1, v4, :cond_60

    #@3b
    .line 1289
    :cond_3b
    iget-boolean v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdViaWifi:Z

    #@3d
    if-nez v1, :cond_4f

    #@3f
    if-eq v0, v6, :cond_4f

    #@41
    if-eqz v0, :cond_4f

    #@43
    .line 1291
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@45
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@47
    new-instance v3, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$9;

    #@49
    invoke-direct {v3, p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$9;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)V

    #@4c
    invoke-virtual {v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->removeGroup(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    #@4f
    .line 1300
    :cond_4f
    iget v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiState:I

    #@51
    const/4 v2, 0x1

    #@52
    if-ne v1, v2, :cond_2e

    #@54
    .line 1301
    if-eq v0, v6, :cond_5c

    #@56
    if-eqz v0, :cond_5c

    #@58
    .line 1302
    const/4 v1, 0x2

    #@59
    invoke-direct {p0, v1}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@5c
    .line 1304
    :cond_5c
    invoke-direct {p0, v4}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@5f
    goto :goto_2e

    #@60
    .line 1306
    :cond_60
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdState()I

    #@63
    move-result v1

    #@64
    const/4 v2, 0x2

    #@65
    if-ne v1, v2, :cond_70

    #@67
    .line 1307
    iget v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiState:I

    #@69
    const/4 v2, 0x1

    #@6a
    if-ne v1, v2, :cond_2e

    #@6c
    .line 1308
    invoke-direct {p0, v4}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@6f
    goto :goto_2e

    #@70
    .line 1310
    :cond_70
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdState()I

    #@73
    move-result v1

    #@74
    if-ne v1, v6, :cond_97

    #@76
    .line 1311
    iget-boolean v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdViaWifi:Z

    #@78
    if-eqz v1, :cond_85

    #@7a
    .line 1312
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mHandler:Landroid/os/Handler;

    #@7c
    const/16 v2, 0x3ec

    #@7e
    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@81
    .line 1313
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->disableWifiP2p()Z

    #@84
    goto :goto_2e

    #@85
    .line 1315
    :cond_85
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mHandler:Landroid/os/Handler;

    #@87
    invoke-virtual {v1, v8}, Landroid/os/Handler;->removeMessages(I)V

    #@8a
    .line 1316
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@8c
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@8e
    new-instance v3, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$10;

    #@90
    invoke-direct {v3, p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$10;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)V

    #@93
    invoke-virtual {v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->removeGroup(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    #@96
    goto :goto_2e

    #@97
    .line 1328
    :cond_97
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdState()I

    #@9a
    move-result v1

    #@9b
    if-ne v1, v7, :cond_2e

    #@9d
    .line 1329
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mHandler:Landroid/os/Handler;

    #@9f
    const/16 v2, 0x3eb

    #@a1
    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@a4
    .line 1331
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@a6
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@a8
    new-instance v3, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$11;

    #@aa
    invoke-direct {v3, p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$11;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)V

    #@ad
    invoke-virtual {v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->removeGroup(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    #@b0
    goto/16 :goto_2e

    #@b2
    .line 1348
    :pswitch_b2
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdState()I

    #@b5
    move-result v1

    #@b6
    if-ne v1, v5, :cond_bb

    #@b8
    .line 1349
    invoke-direct {p0, v4}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@bb
    .line 1352
    :cond_bb
    if-eq v0, v7, :cond_bf

    #@bd
    if-ne v0, v5, :cond_cb

    #@bf
    .line 1353
    :cond_bf
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@c1
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@c3
    new-instance v3, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$12;

    #@c5
    invoke-direct {v3, p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$12;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)V

    #@c8
    invoke-virtual {v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->removeGroup(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    #@cb
    .line 1363
    :cond_cb
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdState()I

    #@ce
    move-result v1

    #@cf
    if-ne v1, v6, :cond_df

    #@d1
    .line 1364
    iget-boolean v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdViaWifi:Z

    #@d3
    if-eqz v1, :cond_fa

    #@d5
    .line 1365
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mHandler:Landroid/os/Handler;

    #@d7
    const/16 v2, 0x3ec

    #@d9
    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@dc
    .line 1366
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->disableWifiP2p()Z

    #@df
    .line 1383
    :cond_df
    :goto_df
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdState()I

    #@e2
    move-result v1

    #@e3
    if-ne v1, v7, :cond_2e

    #@e5
    .line 1384
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mHandler:Landroid/os/Handler;

    #@e7
    const/16 v2, 0x3eb

    #@e9
    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@ec
    .line 1386
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@ee
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@f0
    new-instance v3, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$14;

    #@f2
    invoke-direct {v3, p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$14;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)V

    #@f5
    invoke-virtual {v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->removeGroup(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    #@f8
    goto/16 :goto_2e

    #@fa
    .line 1368
    :cond_fa
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mHandler:Landroid/os/Handler;

    #@fc
    invoke-virtual {v1, v8}, Landroid/os/Handler;->removeMessages(I)V

    #@ff
    .line 1370
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@101
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@103
    new-instance v3, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$13;

    #@105
    invoke-direct {v3, p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$13;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)V

    #@108
    invoke-virtual {v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->removeGroup(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    #@10b
    goto :goto_df

    #@10c
    .line 1403
    :pswitch_10c
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdState()I

    #@10f
    move-result v1

    #@110
    if-ne v1, v4, :cond_12d

    #@112
    .line 1425
    :cond_112
    :goto_112
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdState()I

    #@115
    move-result v1

    #@116
    if-ne v1, v7, :cond_2e

    #@118
    .line 1426
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mHandler:Landroid/os/Handler;

    #@11a
    const/16 v2, 0x3eb

    #@11c
    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@11f
    .line 1428
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@121
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@123
    new-instance v3, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$16;

    #@125
    invoke-direct {v3, p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$16;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)V

    #@128
    invoke-virtual {v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->removeGroup(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    #@12b
    goto/16 :goto_2e

    #@12d
    .line 1404
    :cond_12d
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdState()I

    #@130
    move-result v1

    #@131
    if-ne v1, v5, :cond_137

    #@133
    .line 1405
    invoke-direct {p0, v4}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@136
    goto :goto_112

    #@137
    .line 1406
    :cond_137
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdState()I

    #@13a
    move-result v1

    #@13b
    if-ne v1, v6, :cond_112

    #@13d
    .line 1407
    iget-boolean v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdViaWifi:Z

    #@13f
    if-eqz v1, :cond_14c

    #@141
    .line 1408
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mHandler:Landroid/os/Handler;

    #@143
    const/16 v2, 0x3ec

    #@145
    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@148
    .line 1409
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->disableWifiP2p()Z

    #@14b
    goto :goto_112

    #@14c
    .line 1411
    :cond_14c
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mHandler:Landroid/os/Handler;

    #@14e
    invoke-virtual {v1, v8}, Landroid/os/Handler;->removeMessages(I)V

    #@151
    .line 1412
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@153
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@155
    new-instance v3, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$15;

    #@157
    invoke-direct {v3, p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$15;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)V

    #@15a
    invoke-virtual {v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->removeGroup(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    #@15d
    goto :goto_112

    #@15e
    .line 1446
    :pswitch_15e
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdState()I

    #@161
    move-result v1

    #@162
    if-ne v1, v4, :cond_2e

    #@164
    .line 1447
    sget-boolean v1, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mBlockHdcp:Z

    #@166
    if-eqz v1, :cond_176

    #@168
    iget-boolean v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->hdcpEnabled:Z

    #@16a
    if-nez v1, :cond_176

    #@16c
    .line 1448
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mHandler:Landroid/os/Handler;

    #@16e
    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@171
    .line 1449
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->cancelWifiDisplayConnect()Z

    #@174
    goto/16 :goto_2e

    #@176
    .line 1451
    :cond_176
    invoke-direct {p0, v5}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@179
    goto/16 :goto_2e

    #@17b
    .line 1285
    nop

    #@17c
    :pswitch_data_17c
    .packed-switch 0x0
        :pswitch_2f
        :pswitch_2f
        :pswitch_2e
        :pswitch_b2
        :pswitch_10c
        :pswitch_15e
        :pswitch_15e
    .end packed-switch
.end method

.method public handleSessionStateChange(I)V
    .registers 7
    .parameter

    #@0
    .prologue
    const/4 v4, 0x4

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 1633
    packed-switch p1, :pswitch_data_9a

    #@6
    .line 1675
    const-string v0, "WfdStateTracker"

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "Unhandled SessionStateChanged Event: "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 1678
    :cond_1e
    :goto_1e
    return-void

    #@1f
    .line 1635
    :pswitch_1f
    const-string v0, "WfdStateTracker"

    #@21
    const-string v1, "HDCP enabled for current session"

    #@23
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 1636
    iput-boolean v3, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->hdcpEnabled:Z

    #@28
    goto :goto_1e

    #@29
    .line 1639
    :pswitch_29
    const-string v0, "WfdStateTracker"

    #@2b
    const-string v1, "HDCP connect failed for current session"

    #@2d
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 1640
    iput-boolean v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->hdcpEnabled:Z

    #@32
    goto :goto_1e

    #@33
    .line 1643
    :pswitch_33
    const-string v0, "WfdStateTracker"

    #@35
    const-string v1, "Peer device supports UIBC"

    #@37
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 1644
    iput-boolean v3, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->uibcSupported:Z

    #@3c
    goto :goto_1e

    #@3d
    .line 1647
    :pswitch_3d
    const-string v0, "WfdStateTracker"

    #@3f
    const-string v1, "Peer device does not support UIBC"

    #@41
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 1648
    iput-boolean v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->uibcSupported:Z

    #@46
    goto :goto_1e

    #@47
    .line 1651
    :pswitch_47
    const-string v0, "WfdStateTracker"

    #@49
    const-string v1, "Tearing down"

    #@4b
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 1652
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdState()I

    #@51
    move-result v0

    #@52
    if-ne v0, v4, :cond_1e

    #@54
    sget-object v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->connectedUdn:Ljava/lang/String;

    #@56
    if-eqz v0, :cond_1e

    #@58
    .line 1653
    const/4 v0, 0x0

    #@59
    sput-object v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->connectedUdn:Ljava/lang/String;

    #@5b
    .line 1654
    invoke-direct {p0, v4}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@5e
    goto :goto_1e

    #@5f
    .line 1658
    :pswitch_5f
    const-string v0, "WfdStateTracker"

    #@61
    const-string v1, "UIBC feature is enabled"

    #@63
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@66
    .line 1659
    iput-boolean v3, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->uibcEnabled:Z

    #@68
    goto :goto_1e

    #@69
    .line 1662
    :pswitch_69
    const-string v0, "WfdStateTracker"

    #@6b
    const-string v1, "UIBC feature is disabled"

    #@6d
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@70
    .line 1663
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mHandler:Landroid/os/Handler;

    #@72
    const/16 v1, 0x3ed

    #@74
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@77
    .line 1664
    iput-boolean v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->uibcEnabled:Z

    #@79
    .line 1665
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdPendingTeardown:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

    #@7b
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;->WFD_DISABLE:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

    #@7d
    if-ne v0, v1, :cond_87

    #@7f
    .line 1666
    invoke-virtual {p0, v2}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWifiDisplayEnabled(Z)Z

    #@82
    .line 1672
    :goto_82
    sget-object v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;->NONE:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

    #@84
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdPendingTeardown:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

    #@86
    goto :goto_1e

    #@87
    .line 1667
    :cond_87
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdPendingTeardown:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

    #@89
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;->WFD_DISCONNECT:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

    #@8b
    if-ne v0, v1, :cond_91

    #@8d
    .line 1668
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->cancelWifiDisplayConnect()Z

    #@90
    goto :goto_82

    #@91
    .line 1670
    :cond_91
    const-string v0, "WfdStateTracker"

    #@93
    const-string v1, "No pending teardown command"

    #@95
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@98
    goto :goto_82

    #@99
    .line 1633
    nop

    #@9a
    :pswitch_data_9a
    .packed-switch 0x1
        :pswitch_1f
        :pswitch_29
        :pswitch_33
        :pswitch_3d
        :pswitch_47
        :pswitch_5f
        :pswitch_69
    .end packed-switch
.end method

.method public handleWfdDlnaEvent(ILjava/lang/String;)V
    .registers 7
    .parameter
    .parameter

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 1682
    packed-switch p1, :pswitch_data_128

    #@4
    .line 1748
    const-string v0, "WfdStateTracker"

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "Unhandled handleWfdDlnaEvent: "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 1751
    :cond_1c
    :goto_1c
    return-void

    #@1d
    .line 1684
    :pswitch_1d
    const-string v0, "WfdStateTracker"

    #@1f
    new-instance v1, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v2, "inframode_enable: "

    #@26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    iget-boolean v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdInfraModeEnabled:Z

    #@2c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v1

    #@34
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 1685
    iget-boolean v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdInfraModeEnabled:Z

    #@39
    if-eqz v0, :cond_75

    #@3b
    if-eqz p2, :cond_75

    #@3d
    .line 1686
    iput-boolean v3, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdDlnaTransitEnabled:Z

    #@3f
    .line 1687
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->wfdInterfaceName:Ljava/lang/String;

    #@41
    if-eqz v0, :cond_5f

    #@43
    .line 1688
    new-instance v0, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v0

    #@4c
    const-string v1, "-"

    #@4e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v0

    #@52
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->wfdInterfaceName:Ljava/lang/String;

    #@54
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v0

    #@58
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v0

    #@5c
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->udnForDlnaTransit:Ljava/lang/String;

    #@5e
    goto :goto_1c

    #@5f
    .line 1690
    :cond_5f
    new-instance v0, Ljava/lang/StringBuilder;

    #@61
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v0

    #@68
    const-string v1, "-p2p0"

    #@6a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v0

    #@6e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v0

    #@72
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->udnForDlnaTransit:Ljava/lang/String;

    #@74
    goto :goto_1c

    #@75
    .line 1693
    :cond_75
    const/4 v0, 0x0

    #@76
    iput-boolean v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdDlnaTransitEnabled:Z

    #@78
    .line 1694
    const/4 v0, 0x0

    #@79
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->udnForDlnaTransit:Ljava/lang/String;

    #@7b
    .line 1695
    const-string v0, "WfdStateTracker"

    #@7d
    const-string v1, "Transit Disabled"

    #@7f
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@82
    goto :goto_1c

    #@83
    .line 1699
    :pswitch_83
    if-eqz p2, :cond_120

    #@85
    .line 1701
    const-string v0, "WfdStateTracker"

    #@87
    new-instance v1, Ljava/lang/StringBuilder;

    #@89
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8c
    const-string v2, "RTSP Source ip = "

    #@8e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v1

    #@92
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v1

    #@96
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@99
    move-result-object v1

    #@9a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9d
    .line 1703
    :try_start_9d
    invoke-static {p2}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;
    :try_end_a0
    .catch Ljava/net/UnknownHostException; {:try_start_9d .. :try_end_a0} :catch_b2

    #@a0
    move-result-object v0

    #@a1
    .line 1708
    if-eqz v0, :cond_10e

    #@a3
    .line 1711
    :try_start_a3
    invoke-static {v0}, Ljava/net/NetworkInterface;->getByInetAddress(Ljava/net/InetAddress;)Ljava/net/NetworkInterface;
    :try_end_a6
    .catch Ljava/net/SocketException; {:try_start_a3 .. :try_end_a6} :catch_cd

    #@a6
    move-result-object v0

    #@a7
    .line 1717
    if-nez v0, :cond_d7

    #@a9
    .line 1718
    const-string v0, "WfdStateTracker"

    #@ab
    const-string v1, "wfdInterface is null"

    #@ad
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b0
    goto/16 :goto_1c

    #@b2
    .line 1704
    :catch_b2
    move-exception v0

    #@b3
    .line 1705
    const-string v0, "WfdStateTracker"

    #@b5
    new-instance v1, Ljava/lang/StringBuilder;

    #@b7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@ba
    const-string v2, "Cannot get InetAddress from "

    #@bc
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v1

    #@c0
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v1

    #@c4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c7
    move-result-object v1

    #@c8
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@cb
    goto/16 :goto_1c

    #@cd
    .line 1712
    :catch_cd
    move-exception v0

    #@ce
    .line 1713
    const-string v0, "WfdStateTracker"

    #@d0
    const-string v1, "Error while getting source interface name"

    #@d2
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d5
    goto/16 :goto_1c

    #@d7
    .line 1721
    :cond_d7
    invoke-virtual {v0}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    #@da
    move-result-object v0

    #@db
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->wfdInterfaceName:Ljava/lang/String;

    #@dd
    .line 1724
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->wfdInterfaceName:Ljava/lang/String;

    #@df
    if-eqz v0, :cond_10e

    #@e1
    .line 1725
    const-string v0, "WfdStateTracker"

    #@e3
    new-instance v1, Ljava/lang/StringBuilder;

    #@e5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e8
    const-string v2, "Connected interface = "

    #@ea
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v1

    #@ee
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->wfdInterfaceName:Ljava/lang/String;

    #@f0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v1

    #@f4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f7
    move-result-object v1

    #@f8
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@fb
    .line 1726
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->wfdInterfaceName:Ljava/lang/String;

    #@fd
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->wifiInterfaceName:Ljava/lang/String;

    #@ff
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@102
    move-result v0

    #@103
    if-eqz v0, :cond_10e

    #@105
    .line 1727
    const-string v0, "WfdStateTracker"

    #@107
    const-string v1, "Wfd Via Wi-Fi enabled"

    #@109
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@10c
    .line 1728
    iput-boolean v3, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdViaWifi:Z

    #@10e
    .line 1736
    :cond_10e
    :goto_10e
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@110
    if-eqz v0, :cond_1c

    #@112
    .line 1737
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@114
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@116
    new-instance v2, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$17;

    #@118
    invoke-direct {v2, p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$17;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)V

    #@11b
    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->stopPeerDiscovery(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    #@11e
    goto/16 :goto_1c

    #@120
    .line 1733
    :cond_120
    const-string v0, "WfdStateTracker"

    #@122
    const-string v1, "null IP string received"

    #@124
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@127
    goto :goto_10e

    #@128
    .line 1682
    :pswitch_data_128
    .packed-switch 0x1
        :pswitch_1d
        :pswitch_83
    .end packed-switch
.end method

.method public handleWifiStateChangedEvent(I)V
    .registers 8
    .parameter

    #@0
    .prologue
    const/4 v5, 0x5

    #@1
    const/4 v4, 0x4

    #@2
    const/4 v3, 0x2

    #@3
    .line 1842
    const-string v0, "WfdStateTracker"

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "handleWifiStateChangedEvent: "

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 1843
    iput p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiState:I

    #@1d
    .line 1844
    iget-boolean v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdEnableState:Z

    #@1f
    if-eqz v0, :cond_24

    #@21
    .line 1845
    packed-switch p1, :pswitch_data_60

    #@24
    .line 1870
    :cond_24
    :goto_24
    return-void

    #@25
    .line 1847
    :pswitch_25
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mDirectState:Ljava/util/concurrent/atomic/AtomicInteger;

    #@27
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@2a
    move-result v0

    #@2b
    if-ne v0, v3, :cond_3e

    #@2d
    .line 1848
    invoke-direct {p0, v3}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@30
    .line 1855
    :cond_30
    :goto_30
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdPendingCmd:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;

    #@32
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;->DISABLE:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;

    #@34
    if-ne v0, v1, :cond_24

    #@36
    .line 1856
    sget-object v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;->NONE:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;

    #@38
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdPendingCmd:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;

    #@3a
    .line 1858
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->disableWifiP2p()Z

    #@3d
    goto :goto_24

    #@3e
    .line 1850
    :cond_3e
    iget-boolean v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdViaWifi:Z

    #@40
    if-eqz v0, :cond_30

    #@42
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdState()I

    #@45
    move-result v0

    #@46
    if-ne v0, v5, :cond_30

    #@48
    .line 1851
    invoke-direct {p0, v3}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@4b
    .line 1852
    invoke-direct {p0, v4}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@4e
    goto :goto_30

    #@4f
    .line 1862
    :pswitch_4f
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdState()I

    #@52
    move-result v0

    #@53
    if-eq v0, v4, :cond_24

    #@55
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdState()I

    #@58
    move-result v0

    #@59
    if-eq v0, v5, :cond_24

    #@5b
    .line 1863
    invoke-direct {p0, v4}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@5e
    goto :goto_24

    #@5f
    .line 1845
    nop

    #@60
    :pswitch_data_60
    .packed-switch 0x0
        :pswitch_25
        :pswitch_4f
    .end packed-switch
.end method

.method public informConnectionRequstedUdn(Ljava/lang/String;)V
    .registers 4
    .parameter "req_udn"

    #@0
    .prologue
    const/4 v1, 0x4

    #@1
    .line 1273
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdState()I

    #@4
    move-result v0

    #@5
    if-ne v0, v1, :cond_c

    #@7
    .line 1274
    sput-object p1, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->connectedUdn:Ljava/lang/String;

    #@9
    .line 1275
    invoke-direct {p0, v1}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@c
    .line 1277
    :cond_c
    return-void
.end method

.method public declared-synchronized initRtspClient()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 639
    monitor-enter p0

    #@2
    :try_start_2
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdState()I

    #@5
    move-result v1

    #@6
    const/4 v2, 0x4

    #@7
    if-ne v1, v2, :cond_14

    #@9
    .line 640
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->targetUrl:Ljava/lang/String;

    #@b
    if-nez v1, :cond_16

    #@d
    .line 641
    const-string v1, "WfdStateTracker"

    #@f
    const-string v2, "initRtspClient: targetUrl must be set before initializing WFD Client"

    #@11
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_14
    .catchall {:try_start_2 .. :try_end_14} :catchall_27

    #@14
    .line 650
    :cond_14
    :goto_14
    monitor-exit p0

    #@15
    return v0

    #@16
    .line 644
    :cond_16
    :try_start_16
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdAdaptation:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@18
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->targetUrl:Ljava/lang/String;

    #@1a
    invoke-virtual {v1, v2}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->initRtspClient(Ljava/lang/String;)Z

    #@1d
    move-result v1

    #@1e
    if-eqz v1, :cond_14

    #@20
    .line 645
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdAdaptation:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@22
    invoke-virtual {v0}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->rtspStartThread()Z
    :try_end_25
    .catchall {:try_start_16 .. :try_end_25} :catchall_27

    #@25
    move-result v0

    #@26
    goto :goto_14

    #@27
    .line 639
    :catchall_27
    move-exception v0

    #@28
    monitor-exit p0

    #@29
    throw v0
.end method

.method public declared-synchronized pause()Z
    .registers 2

    #@0
    .prologue
    .line 615
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdAdaptation:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@3
    invoke-virtual {v0}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->rtspSendPause()Z
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_9

    #@6
    move-result v0

    #@7
    monitor-exit p0

    #@8
    return v0

    #@9
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0

    #@b
    throw v0
.end method

.method public declared-synchronized pauseTransit()Z
    .registers 2

    #@0
    .prologue
    .line 624
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdAdaptation:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@3
    invoke-virtual {v0}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->rtspSendPauseTransit()Z
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_9

    #@6
    move-result v0

    #@7
    monitor-exit p0

    #@8
    return v0

    #@9
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0

    #@b
    throw v0
.end method

.method public declared-synchronized play()Z
    .registers 2

    #@0
    .prologue
    .line 611
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdAdaptation:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@3
    invoke-virtual {v0}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->rtspSendPlay()Z
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_9

    #@6
    move-result v0

    #@7
    monitor-exit p0

    #@8
    return v0

    #@9
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0

    #@b
    throw v0
.end method

.method public declared-synchronized playTransit()Z
    .registers 2

    #@0
    .prologue
    .line 620
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdAdaptation:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@3
    invoke-virtual {v0}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->rtspSendPlayTransit()Z
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_9

    #@6
    move-result v0

    #@7
    monitor-exit p0

    #@8
    return v0

    #@9
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0

    #@b
    throw v0
.end method

.method public setCallState(Ljava/lang/String;)V
    .registers 2
    .parameter "state"

    #@0
    .prologue
    .line 1901
    sput-object p1, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mCallState:Ljava/lang/String;

    #@2
    .line 1902
    return-void
.end method

.method public setTargetIpAddress_Url(I)V
    .registers 6
    .parameter

    #@0
    .prologue
    .line 2095
    const-string v0, "rtsp://%d.%d.%d.%d:%d"

    #@2
    const/4 v1, 0x5

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    and-int/lit16 v3, p1, 0xff

    #@8
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@b
    move-result-object v3

    #@c
    aput-object v3, v1, v2

    #@e
    const/4 v2, 0x1

    #@f
    shr-int/lit8 v3, p1, 0x8

    #@11
    and-int/lit16 v3, v3, 0xff

    #@13
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@16
    move-result-object v3

    #@17
    aput-object v3, v1, v2

    #@19
    const/4 v2, 0x2

    #@1a
    shr-int/lit8 v3, p1, 0x10

    #@1c
    and-int/lit16 v3, v3, 0xff

    #@1e
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@21
    move-result-object v3

    #@22
    aput-object v3, v1, v2

    #@24
    const/4 v2, 0x3

    #@25
    shr-int/lit8 v3, p1, 0x18

    #@27
    and-int/lit16 v3, v3, 0xff

    #@29
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2c
    move-result-object v3

    #@2d
    aput-object v3, v1, v2

    #@2f
    const/4 v2, 0x4

    #@30
    iget v3, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->targetRtspPort:I

    #@32
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@35
    move-result-object v3

    #@36
    aput-object v3, v1, v2

    #@38
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@3b
    move-result-object v0

    #@3c
    .line 2097
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setTargetUrl(Ljava/lang/String;)V

    #@3f
    .line 2098
    return-void
.end method

.method public setTargetRtspPort(I)V
    .registers 2
    .parameter "port"

    #@0
    .prologue
    .line 2090
    iput p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->targetRtspPort:I

    #@2
    .line 2091
    return-void
.end method

.method public setTargetUrl(Ljava/lang/String;)V
    .registers 5
    .parameter

    #@0
    .prologue
    .line 2081
    const-string v0, "WfdStateTracker"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Target Url:"

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 2082
    iput-object p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->targetUrl:Ljava/lang/String;

    #@1a
    .line 2083
    return-void
.end method

.method public setWfdMode(I)V
    .registers 2
    .parameter "mode"

    #@0
    .prologue
    .line 2077
    iput p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->wfdMode:I

    #@2
    .line 2078
    return-void
.end method

.method public setWifiDisplayEnabled(Z)Z
    .registers 11
    .parameter "enabled"

    #@0
    .prologue
    const/4 v8, 0x7

    #@1
    const/4 v7, 0x0

    #@2
    const/4 v6, 0x4

    #@3
    const/4 v4, 0x2

    #@4
    const/4 v5, 0x1

    #@5
    .line 995
    const-string v1, "WfdStateTracker"

    #@7
    new-instance v2, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v3, "setWifiDisplayEnabled ("

    #@e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdState()I

    #@15
    move-result v3

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    const-string v3, ") "

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 996
    iget-boolean v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdEnableState:Z

    #@2d
    if-ne v1, p1, :cond_34

    #@2f
    if-ne p1, v5, :cond_34

    #@31
    .line 997
    iput-boolean v7, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->noDirectDisconnect:Z

    #@33
    .line 1190
    :goto_33
    return v5

    #@34
    .line 1001
    :cond_34
    if-eqz p1, :cond_123

    #@36
    .line 1002
    iget-boolean v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->isHdmiConnected:Z

    #@38
    if-eqz v1, :cond_40

    #@3a
    .line 1003
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mHandler:Landroid/os/Handler;

    #@3c
    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@3f
    goto :goto_33

    #@40
    .line 1006
    :cond_40
    iget-boolean v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->isBatteryLow:Z

    #@42
    if-eqz v1, :cond_4b

    #@44
    .line 1007
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mHandler:Landroid/os/Handler;

    #@46
    const/4 v2, 0x5

    #@47
    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@4a
    goto :goto_33

    #@4b
    .line 1010
    :cond_4b
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->checkProcess()I

    #@4e
    move-result v0

    #@4f
    .line 1011
    .local v0, block_proc:I
    if-ltz v0, :cond_5e

    #@51
    .line 1012
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mHandler:Landroid/os/Handler;

    #@53
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mHandler:Landroid/os/Handler;

    #@55
    const/4 v3, 0x3

    #@56
    invoke-static {v2, v3, v0, v7}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    #@59
    move-result-object v2

    #@5a
    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@5d
    goto :goto_33

    #@5e
    .line 1016
    :cond_5e
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mDirectState:Ljava/util/concurrent/atomic/AtomicInteger;

    #@60
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@63
    move-result v1

    #@64
    packed-switch v1, :pswitch_data_20c

    #@67
    .line 1099
    :cond_67
    :goto_67
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->enableWifiP2p()Z

    #@6a
    move-result v1

    #@6b
    if-nez v1, :cond_70

    #@6d
    .line 1100
    invoke-direct {p0, v7}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@70
    .line 1189
    .end local v0           #block_proc:I
    :cond_70
    :goto_70
    iput-boolean v7, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->noDirectDisconnect:Z

    #@72
    goto :goto_33

    #@73
    .line 1018
    .restart local v0       #block_proc:I
    :pswitch_73
    iput-boolean v5, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdEnableState:Z

    #@75
    .line 1019
    invoke-direct {p0, v5}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@78
    goto :goto_67

    #@79
    .line 1022
    :pswitch_79
    iput-boolean v5, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdEnableState:Z

    #@7b
    .line 1023
    invoke-direct {p0, v5}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@7e
    goto :goto_67

    #@7f
    .line 1026
    :pswitch_7f
    iput-boolean v5, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdEnableState:Z

    #@81
    .line 1027
    iget v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiState:I

    #@83
    if-nez v1, :cond_89

    #@85
    .line 1028
    invoke-direct {p0, v4}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@88
    goto :goto_67

    #@89
    .line 1029
    :cond_89
    iget v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiState:I

    #@8b
    if-ne v1, v5, :cond_67

    #@8d
    .line 1030
    invoke-direct {p0, v4}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@90
    .line 1031
    invoke-direct {p0, v6}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@93
    .line 1032
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->pendingUdn:Ljava/lang/String;

    #@95
    if-eqz v1, :cond_67

    #@97
    .line 1033
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->pendingUdn:Ljava/lang/String;

    #@99
    sput-object v1, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->connectedUdn:Ljava/lang/String;

    #@9b
    .line 1034
    const/4 v1, 0x0

    #@9c
    sput-object v1, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->pendingUdn:Ljava/lang/String;

    #@9e
    .line 1035
    invoke-direct {p0, v6}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@a1
    goto :goto_67

    #@a2
    .line 1040
    :pswitch_a2
    invoke-direct {p0, v5}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@a5
    .line 1041
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@a7
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@a9
    new-instance v3, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$5;

    #@ab
    invoke-direct {v3, p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$5;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)V

    #@ae
    invoke-virtual {v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->cancelConnect(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    #@b1
    .line 1048
    iput-boolean v5, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdEnableState:Z

    #@b3
    .line 1049
    iget v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiState:I

    #@b5
    if-nez v1, :cond_bb

    #@b7
    .line 1050
    invoke-direct {p0, v4}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@ba
    goto :goto_67

    #@bb
    .line 1051
    :cond_bb
    iget v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiState:I

    #@bd
    if-ne v1, v5, :cond_67

    #@bf
    .line 1052
    invoke-direct {p0, v4}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@c2
    .line 1053
    invoke-direct {p0, v6}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@c5
    .line 1054
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->pendingUdn:Ljava/lang/String;

    #@c7
    if-eqz v1, :cond_67

    #@c9
    .line 1055
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->pendingUdn:Ljava/lang/String;

    #@cb
    sput-object v1, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->connectedUdn:Ljava/lang/String;

    #@cd
    .line 1056
    const/4 v1, 0x0

    #@ce
    sput-object v1, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->pendingUdn:Ljava/lang/String;

    #@d0
    .line 1057
    invoke-direct {p0, v6}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@d3
    goto :goto_67

    #@d4
    .line 1064
    :pswitch_d4
    iget-boolean v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->noDirectDisconnect:Z

    #@d6
    if-nez v1, :cond_de

    #@d8
    .line 1065
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mHandler:Landroid/os/Handler;

    #@da
    invoke-virtual {v1, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@dd
    goto :goto_67

    #@de
    .line 1067
    :cond_de
    iput-boolean v5, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdEnableState:Z

    #@e0
    .line 1068
    invoke-direct {p0, v4}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@e3
    .line 1069
    invoke-direct {p0, v6}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@e6
    .line 1070
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->pendingUdn:Ljava/lang/String;

    #@e8
    if-eqz v1, :cond_67

    #@ea
    .line 1071
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->pendingUdn:Ljava/lang/String;

    #@ec
    sput-object v1, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->connectedUdn:Ljava/lang/String;

    #@ee
    .line 1072
    const/4 v1, 0x0

    #@ef
    sput-object v1, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->pendingUdn:Ljava/lang/String;

    #@f1
    .line 1073
    invoke-direct {p0, v6}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@f4
    goto/16 :goto_67

    #@f6
    .line 1078
    :pswitch_f6
    iput-boolean v5, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdEnableState:Z

    #@f8
    .line 1079
    iget v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiState:I

    #@fa
    if-nez v1, :cond_104

    #@fc
    .line 1080
    invoke-direct {p0, v4}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@ff
    .line 1090
    :cond_ff
    :goto_ff
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->startEventLoop()V

    #@102
    goto/16 :goto_67

    #@104
    .line 1081
    :cond_104
    iget v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiState:I

    #@106
    if-ne v1, v5, :cond_ff

    #@108
    .line 1082
    invoke-direct {p0, v4}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@10b
    .line 1083
    invoke-direct {p0, v6}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@10e
    .line 1084
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->pendingUdn:Ljava/lang/String;

    #@110
    if-eqz v1, :cond_ff

    #@112
    .line 1085
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->pendingUdn:Ljava/lang/String;

    #@114
    sput-object v1, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->connectedUdn:Ljava/lang/String;

    #@116
    .line 1086
    const/4 v1, 0x0

    #@117
    sput-object v1, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->pendingUdn:Ljava/lang/String;

    #@119
    .line 1087
    invoke-direct {p0, v6}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@11c
    goto :goto_ff

    #@11d
    .line 1094
    :pswitch_11d
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;->ENABLE:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;

    #@11f
    iput-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdPendingCmd:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;

    #@121
    goto/16 :goto_67

    #@123
    .line 1103
    .end local v0           #block_proc:I
    :cond_123
    iget-boolean v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->uibcEnabled:Z

    #@125
    if-eqz v1, :cond_142

    #@127
    .line 1104
    const-string v1, "WfdStateTracker"

    #@129
    const-string v2, "pending WFD_DISABLE"

    #@12b
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@12e
    .line 1105
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;->WFD_DISABLE:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

    #@130
    iput-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdPendingTeardown:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

    #@132
    .line 1106
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mHandler:Landroid/os/Handler;

    #@134
    const/16 v2, 0x3ed

    #@136
    const-wide/16 v3, 0xfa0

    #@138
    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@13b
    .line 1107
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdAdaptation:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@13d
    invoke-virtual {v1, v7}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->startUIBC(Z)Z

    #@140
    goto/16 :goto_33

    #@142
    .line 1110
    :cond_142
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mDirectState:Ljava/util/concurrent/atomic/AtomicInteger;

    #@144
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@147
    move-result v1

    #@148
    packed-switch v1, :pswitch_data_21e

    #@14b
    goto/16 :goto_70

    #@14d
    .line 1113
    :pswitch_14d
    invoke-direct {p0, v7}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@150
    goto/16 :goto_70

    #@152
    .line 1116
    :pswitch_152
    invoke-direct {p0, v8}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@155
    .line 1118
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;->DISABLE:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;

    #@157
    iput-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdPendingCmd:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;

    #@159
    goto/16 :goto_70

    #@15b
    .line 1121
    :pswitch_15b
    invoke-direct {p0, v8}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@15e
    .line 1122
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getRtspState()I

    #@161
    move-result v1

    #@162
    const/4 v2, 0x5

    #@163
    if-eq v1, v2, :cond_16c

    #@165
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getRtspState()I

    #@168
    move-result v1

    #@169
    const/4 v2, 0x6

    #@16a
    if-ne v1, v2, :cond_17a

    #@16c
    .line 1123
    :cond_16c
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->teardown()Z

    #@16f
    .line 1124
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mHandler:Landroid/os/Handler;

    #@171
    const/16 v2, 0x3ec

    #@173
    const-wide/16 v3, 0x1388

    #@175
    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@178
    goto/16 :goto_70

    #@17a
    .line 1126
    :cond_17a
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->disableWifiP2p()Z

    #@17d
    goto/16 :goto_70

    #@17f
    .line 1130
    :pswitch_17f
    invoke-direct {p0, v8}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@182
    .line 1131
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@184
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@186
    new-instance v3, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$6;

    #@188
    invoke-direct {v3, p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$6;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)V

    #@18b
    invoke-virtual {v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->cancelConnect(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    #@18e
    .line 1139
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getRtspState()I

    #@191
    move-result v1

    #@192
    const/4 v2, 0x5

    #@193
    if-eq v1, v2, :cond_19c

    #@195
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getRtspState()I

    #@198
    move-result v1

    #@199
    const/4 v2, 0x6

    #@19a
    if-ne v1, v2, :cond_1aa

    #@19c
    .line 1140
    :cond_19c
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->teardown()Z

    #@19f
    .line 1141
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mHandler:Landroid/os/Handler;

    #@1a1
    const/16 v2, 0x3ec

    #@1a3
    const-wide/16 v3, 0x1388

    #@1a5
    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@1a8
    goto/16 :goto_70

    #@1aa
    .line 1143
    :cond_1aa
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->disableWifiP2p()Z

    #@1ad
    goto/16 :goto_70

    #@1af
    .line 1147
    :pswitch_1af
    invoke-direct {p0, v8}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@1b2
    .line 1150
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getRtspState()I

    #@1b5
    move-result v1

    #@1b6
    const/4 v2, 0x5

    #@1b7
    if-eq v1, v2, :cond_1c0

    #@1b9
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getRtspState()I

    #@1bc
    move-result v1

    #@1bd
    const/4 v2, 0x6

    #@1be
    if-ne v1, v2, :cond_1dd

    #@1c0
    .line 1151
    :cond_1c0
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->teardown()Z

    #@1c3
    .line 1152
    iget-boolean v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdViaWifi:Z

    #@1c5
    if-eqz v1, :cond_1d2

    #@1c7
    .line 1153
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mHandler:Landroid/os/Handler;

    #@1c9
    const/16 v2, 0x3ec

    #@1cb
    const-wide/16 v3, 0x1388

    #@1cd
    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@1d0
    goto/16 :goto_70

    #@1d2
    .line 1155
    :cond_1d2
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mHandler:Landroid/os/Handler;

    #@1d4
    const/16 v2, 0x3ea

    #@1d6
    const-wide/16 v3, 0x1388

    #@1d8
    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@1db
    goto/16 :goto_70

    #@1dd
    .line 1167
    :cond_1dd
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->disableWifiP2p()Z

    #@1e0
    goto/16 :goto_70

    #@1e2
    .line 1172
    :pswitch_1e2
    invoke-direct {p0, v8}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@1e5
    .line 1173
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getRtspState()I

    #@1e8
    move-result v1

    #@1e9
    const/4 v2, 0x5

    #@1ea
    if-eq v1, v2, :cond_1f3

    #@1ec
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getRtspState()I

    #@1ef
    move-result v1

    #@1f0
    const/4 v2, 0x6

    #@1f1
    if-ne v1, v2, :cond_201

    #@1f3
    .line 1174
    :cond_1f3
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->teardown()Z

    #@1f6
    .line 1175
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mHandler:Landroid/os/Handler;

    #@1f8
    const/16 v2, 0x3ec

    #@1fa
    const-wide/16 v3, 0x1388

    #@1fc
    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@1ff
    goto/16 :goto_70

    #@201
    .line 1178
    :cond_201
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;->DISABLE:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;

    #@203
    iput-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdPendingCmd:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;

    #@205
    goto/16 :goto_70

    #@207
    .line 1182
    :pswitch_207
    invoke-direct {p0, v8}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@20a
    goto/16 :goto_70

    #@20c
    .line 1016
    :pswitch_data_20c
    .packed-switch 0x0
        :pswitch_73
        :pswitch_79
        :pswitch_7f
        :pswitch_a2
        :pswitch_d4
        :pswitch_f6
        :pswitch_11d
    .end packed-switch

    #@21e
    .line 1110
    :pswitch_data_21e
    .packed-switch 0x0
        :pswitch_14d
        :pswitch_152
        :pswitch_15b
        :pswitch_17f
        :pswitch_1af
        :pswitch_1e2
        :pswitch_207
    .end packed-switch
.end method

.method public setWifiDisplayEnabledNoDisconnect(ZLjava/lang/String;)Z
    .registers 4
    .parameter "no_disconnect"
    .parameter "udn"

    #@0
    .prologue
    .line 987
    if-eqz p2, :cond_6

    #@2
    .line 988
    iput-boolean p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->noDirectDisconnect:Z

    #@4
    .line 989
    sput-object p2, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->pendingUdn:Ljava/lang/String;

    #@6
    .line 991
    :cond_6
    const/4 v0, 0x1

    #@7
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWifiDisplayEnabled(Z)Z

    #@a
    move-result v0

    #@b
    return v0
.end method

.method public setWifiDisplayEnabledWithPopUp(Z)Z
    .registers 8
    .parameter "enabled"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 960
    const/4 v0, 0x1

    #@3
    .line 961
    .local v0, ret:Z
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdIntroDialog:Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;

    #@5
    if-eqz v2, :cond_1d

    #@7
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdIntroDialog:Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;

    #@9
    invoke-virtual {v2}, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->isShowing()Z

    #@c
    move-result v2

    #@d
    if-eqz v2, :cond_1d

    #@f
    .line 962
    const/4 v2, 0x7

    #@10
    invoke-direct {p0, v2}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@13
    .line 963
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdIntroDialog:Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;

    #@15
    invoke-virtual {v2}, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->dismiss()V

    #@18
    .line 964
    invoke-direct {p0, v4}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdState(I)V

    #@1b
    move v1, v0

    #@1c
    .line 983
    .end local v0           #ret:Z
    .local v1, ret:I
    :goto_1c
    return v1

    #@1d
    .line 967
    .end local v1           #ret:I
    .restart local v0       #ret:Z
    :cond_1d
    iget-boolean v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->popupIntro:Z

    #@1f
    if-eqz v2, :cond_41

    #@21
    if-eqz p1, :cond_41

    #@23
    .line 968
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mContext:Landroid/content/Context;

    #@25
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@28
    move-result-object v2

    #@29
    const-string v5, "wifi_screen_share_intro_first_check"

    #@2b
    invoke-static {v2, v5, v3}, Lcom/lge/provider/SettingsEx$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@2e
    move-result v2

    #@2f
    if-ne v3, v2, :cond_3f

    #@31
    move v2, v3

    #@32
    :goto_32
    iput-boolean v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->popupIntro:Z

    #@34
    .line 971
    iget-boolean v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->popupIntro:Z

    #@36
    if-eqz v2, :cond_41

    #@38
    .line 973
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mHandler:Landroid/os/Handler;

    #@3a
    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@3d
    move v1, v0

    #@3e
    .line 974
    .restart local v1       #ret:I
    goto :goto_1c

    #@3f
    .end local v1           #ret:I
    :cond_3f
    move v2, v4

    #@40
    .line 968
    goto :goto_32

    #@41
    .line 978
    :cond_41
    iput-boolean v3, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdNeedConnectionPopUp:Z

    #@43
    .line 979
    invoke-virtual {p0, p1}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWifiDisplayEnabled(Z)Z

    #@46
    move-result v0

    #@47
    .line 980
    if-nez v0, :cond_4b

    #@49
    .line 981
    iput-boolean v4, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdNeedConnectionPopUp:Z

    #@4b
    :cond_4b
    move v1, v0

    #@4c
    .line 983
    .restart local v1       #ret:I
    goto :goto_1c
.end method

.method public declared-synchronized teardown()Z
    .registers 2

    #@0
    .prologue
    .line 629
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdAdaptation:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@3
    invoke-virtual {v0}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->rtspSendTeardown()Z
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_9

    #@6
    move-result v0

    #@7
    monitor-exit p0

    #@8
    return v0

    #@9
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0

    #@b
    throw v0
.end method
