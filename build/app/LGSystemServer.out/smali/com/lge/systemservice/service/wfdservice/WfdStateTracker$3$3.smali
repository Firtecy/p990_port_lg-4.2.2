.class Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3$3;
.super Ljava/lang/Object;
.source "WfdStateTracker.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 403
    iput-object p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3$3;->this$1:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;

    #@2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 7
    .parameter "dialog"
    .parameter "which"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 406
    const/4 v0, -0x1

    #@3
    if-ne p2, v0, :cond_38

    #@5
    .line 407
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3$3;->this$1:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;

    #@7
    iget-object v0, v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@9
    #getter for: Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->popupIntro:Z
    invoke-static {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$1600(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Z

    #@c
    move-result v0

    #@d
    if-nez v0, :cond_20

    #@f
    .line 408
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3$3;->this$1:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;

    #@11
    iget-object v0, v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@13
    #getter for: Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$1500(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Landroid/content/Context;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1a
    move-result-object v0

    #@1b
    const-string v1, "wifi_screen_share_intro_first_check"

    #@1d
    invoke-static {v0, v1, v2}, Lcom/lge/provider/SettingsEx$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@20
    .line 411
    :cond_20
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3$3;->this$1:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;

    #@22
    iget-object v0, v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@24
    #setter for: Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdNeedConnectionPopUp:Z
    invoke-static {v0, v3}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$1802(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;Z)Z

    #@27
    .line 412
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3$3;->this$1:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;

    #@29
    iget-object v0, v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@2b
    invoke-virtual {v0, v3}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWifiDisplayEnabled(Z)Z

    #@2e
    move-result v0

    #@2f
    if-nez v0, :cond_38

    #@31
    .line 413
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3$3;->this$1:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;

    #@33
    iget-object v0, v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@35
    #setter for: Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->mWfdNeedConnectionPopUp:Z
    invoke-static {v0, v2}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$1802(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;Z)Z

    #@38
    .line 416
    :cond_38
    return-void
.end method
