.class Lcom/lge/systemservice/service/InfoCollectorATClient$1;
.super Ljava/lang/Object;
.source "InfoCollectorATClient.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/InfoCollectorATClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/InfoCollectorATClient;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/InfoCollectorATClient;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 33
    iput-object p1, p0, Lcom/lge/systemservice/service/InfoCollectorATClient$1;->this$0:Lcom/lge/systemservice/service/InfoCollectorATClient;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .registers 9
    .parameter "className"
    .parameter "binder"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 38
    iget-object v3, p0, Lcom/lge/systemservice/service/InfoCollectorATClient$1;->this$0:Lcom/lge/systemservice/service/InfoCollectorATClient;

    #@3
    invoke-static {v3, v5}, Lcom/lge/systemservice/service/InfoCollectorATClient;->access$002(Lcom/lge/systemservice/service/InfoCollectorATClient;Z)Z

    #@6
    .line 39
    iget-object v3, p0, Lcom/lge/systemservice/service/InfoCollectorATClient$1;->this$0:Lcom/lge/systemservice/service/InfoCollectorATClient;

    #@8
    invoke-static {p2}, Lcom/lge/android/atservice/client/ILGATCMDService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/lge/android/atservice/client/ILGATCMDService;

    #@b
    move-result-object v4

    #@c
    invoke-static {v3, v4}, Lcom/lge/systemservice/service/InfoCollectorATClient;->access$102(Lcom/lge/systemservice/service/InfoCollectorATClient;Lcom/lge/android/atservice/client/ILGATCMDService;)Lcom/lge/android/atservice/client/ILGATCMDService;

    #@f
    .line 41
    new-instance v1, Lcom/lge/systemservice/service/InfoCollectorService;

    #@11
    iget-object v3, p0, Lcom/lge/systemservice/service/InfoCollectorATClient$1;->this$0:Lcom/lge/systemservice/service/InfoCollectorATClient;

    #@13
    invoke-static {v3}, Lcom/lge/systemservice/service/InfoCollectorATClient;->access$200(Lcom/lge/systemservice/service/InfoCollectorATClient;)Landroid/content/Context;

    #@16
    move-result-object v3

    #@17
    invoke-direct {v1, v3}, Lcom/lge/systemservice/service/InfoCollectorService;-><init>(Landroid/content/Context;)V

    #@1a
    .line 42
    .local v1, info:Lcom/lge/systemservice/service/InfoCollectorService;
    new-instance v0, Lcom/lge/systemservice/service/InfoCollectorService$IncomingHandler;

    #@1c
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@1f
    invoke-direct {v0, v1}, Lcom/lge/systemservice/service/InfoCollectorService$IncomingHandler;-><init>(Lcom/lge/systemservice/service/InfoCollectorService;)V

    #@22
    .line 43
    .local v0, incomingHandler:Lcom/lge/systemservice/service/InfoCollectorService$IncomingHandler;
    const/4 v3, 0x0

    #@23
    invoke-static {v3, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@26
    move-result-object v2

    #@27
    .line 45
    .local v2, msg:Landroid/os/Message;
    invoke-virtual {v0, v2}, Lcom/lge/systemservice/service/InfoCollectorService$IncomingHandler;->sendMessage(Landroid/os/Message;)Z

    #@2a
    .line 46
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .registers 4
    .parameter "className"

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Lcom/lge/systemservice/service/InfoCollectorATClient$1;->this$0:Lcom/lge/systemservice/service/InfoCollectorATClient;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-static {v0, v1}, Lcom/lge/systemservice/service/InfoCollectorATClient;->access$102(Lcom/lge/systemservice/service/InfoCollectorATClient;Lcom/lge/android/atservice/client/ILGATCMDService;)Lcom/lge/android/atservice/client/ILGATCMDService;

    #@6
    .line 53
    iget-object v0, p0, Lcom/lge/systemservice/service/InfoCollectorATClient$1;->this$0:Lcom/lge/systemservice/service/InfoCollectorATClient;

    #@8
    const/4 v1, 0x0

    #@9
    invoke-static {v0, v1}, Lcom/lge/systemservice/service/InfoCollectorATClient;->access$002(Lcom/lge/systemservice/service/InfoCollectorATClient;Z)Z

    #@c
    .line 54
    return-void
.end method
