.class public Lcom/lge/systemservice/service/BtLgeExtService;
.super Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub;
.source "BtLgeExtService.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mPowerManager:Landroid/os/PowerManager;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 45
    const-string v0, "btlgeext_jni"

    #@2
    #invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@5
    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 48
    invoke-direct {p0}, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub;-><init>()V

    #@4
    .line 36
    iput-object v0, p0, Lcom/lge/systemservice/service/BtLgeExtService;->mPowerManager:Landroid/os/PowerManager;

    #@6
    .line 37
    iput-object v0, p0, Lcom/lge/systemservice/service/BtLgeExtService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@8
    .line 121
    new-instance v0, Lcom/lge/systemservice/service/BtLgeExtService$1;

    #@a
    invoke-direct {v0, p0}, Lcom/lge/systemservice/service/BtLgeExtService$1;-><init>(Lcom/lge/systemservice/service/BtLgeExtService;)V

    #@d
    iput-object v0, p0, Lcom/lge/systemservice/service/BtLgeExtService;->mReceiver:Landroid/content/BroadcastReceiver;

    #@f
    .line 49
    iput-object p1, p0, Lcom/lge/systemservice/service/BtLgeExtService;->mContext:Landroid/content/Context;

    #@11
    .line 50
    return-void
.end method

.method private HCIDumpDisableNative()I
	.registers 2
	const/4 v0, 0x1
	return v0
.end method

.method private HCIDumpEnableNative()I
	.registers 2
	const/4 v0, 0x1
	return v0
.end method

.method private SSPDebugModeDisableNative()I
	.registers 2
	const/4 v0, 0x1
	return v0
.end method

.method private SSPDebugModeEnableNative()I
	.registers 2
	const/4 v0, 0x1
	return v0
.end method

.method static synthetic access$000(Lcom/lge/systemservice/service/BtLgeExtService;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 27
    iget-object v0, p0, Lcom/lge/systemservice/service/BtLgeExtService;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/lge/systemservice/service/BtLgeExtService;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/lge/systemservice/service/BtLgeExtService;->setCPUFreq(Z)V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/lge/systemservice/service/BtLgeExtService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 27
    invoke-direct {p0}, Lcom/lge/systemservice/service/BtLgeExtService;->acquirePartialWakeLock()V

    #@3
    return-void
.end method

.method private acquirePartialWakeLock()V
    .registers 4

    #@0
    .prologue
    .line 105
    iget-object v0, p0, Lcom/lge/systemservice/service/BtLgeExtService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "power"

    #@4
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Landroid/os/PowerManager;

    #@a
    iput-object v0, p0, Lcom/lge/systemservice/service/BtLgeExtService;->mPowerManager:Landroid/os/PowerManager;

    #@c
    .line 107
    iget-object v0, p0, Lcom/lge/systemservice/service/BtLgeExtService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@e
    if-nez v0, :cond_1d

    #@10
    .line 108
    iget-object v0, p0, Lcom/lge/systemservice/service/BtLgeExtService;->mPowerManager:Landroid/os/PowerManager;

    #@12
    const v1, 0x30000001

    #@15
    const-string v2, "BtLgeExtService"

    #@17
    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@1a
    move-result-object v0

    #@1b
    iput-object v0, p0, Lcom/lge/systemservice/service/BtLgeExtService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@1d
    .line 115
    :cond_1d
    iget-object v0, p0, Lcom/lge/systemservice/service/BtLgeExtService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@1f
    if-eqz v0, :cond_30

    #@21
    iget-object v0, p0, Lcom/lge/systemservice/service/BtLgeExtService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@23
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@26
    move-result v0

    #@27
    if-nez v0, :cond_30

    #@29
    .line 116
    iget-object v0, p0, Lcom/lge/systemservice/service/BtLgeExtService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2b
    const-wide/16 v1, 0x1b58

    #@2d
    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    #@30
    .line 118
    :cond_30
    return-void
.end method

.method private changeCPUMinclockNative(I)I
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method private endLeRxTestNative()I
	.registers 2
	const/4 v0, 0x1
	return v0
.end method

.method private endLeTxTestNative()I
	.registers 2
	const/4 v0, 0x1
	return v0
.end method

.method private enterBluetoothDUTModeBleNative(I)I
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method private enterBluetoothDUTModeNative(I)I
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method private exitBluetoothDUTModeBleNative()I
	.registers 2
	const/4 v0, 0x1
	return v0
.end method

.method private exitBluetoothDUTModeNative()I
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method private isBluetoothBleDUTModeNative()I
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method private isBluetoothDUTModeNative()I
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method private loadBluetoothAddressNative()I
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method private setCPUFreq(Z)V
    .registers 7
    .parameter "flag"

    #@0
    .prologue
    .line 92
    if-eqz p1, :cond_1e

    #@2
    .line 93
    const v2, 0x7a120

    #@5
    invoke-direct {p0, v2}, Lcom/lge/systemservice/service/BtLgeExtService;->changeCPUMinclockNative(I)I

    #@8
    .line 97
    :goto_8
    iget-object v2, p0, Lcom/lge/systemservice/service/BtLgeExtService;->mContext:Landroid/content/Context;

    #@a
    const-string v3, "com.lge.bluetooth.CPUFreq.min"

    #@c
    const/4 v4, 0x0

    #@d
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@10
    move-result-object v1

    #@11
    .line 98
    .local v1, pref:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@14
    move-result-object v0

    #@15
    .line 99
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "com.lge.bluetooth.CPUFreq.min.changed"

    #@17
    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    #@1a
    .line 100
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    #@1d
    .line 101
    return-void

    #@1e
    .line 95
    .end local v0           #editor:Landroid/content/SharedPreferences$Editor;
    .end local v1           #pref:Landroid/content/SharedPreferences;
    :cond_1e
    const v2, 0x493e0

    #@21
    invoke-direct {p0, v2}, Lcom/lge/systemservice/service/BtLgeExtService;->changeCPUMinclockNative(I)I

    #@24
    goto :goto_8
.end method

.method private startLeReceiverTestNative(I)I
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method private startLeTransmitterTestNative(III)I
	.registers 5
	const/4 v0, 0x1
	return v0
.end method


# virtual methods
.method public declared-synchronized HCIDumpDisable()I
    .registers 4

    #@0
    .prologue
    .line 331
    monitor-enter p0

    #@1
    :try_start_1
    const-string v0, "BtLgeExtService"

    #@3
    const-string v1, "HCIDumpDisable"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 333
    iget-object v0, p0, Lcom/lge/systemservice/service/BtLgeExtService;->mContext:Landroid/content/Context;

    #@a
    const-string v1, "android.permission.BLUETOOTH_ADMIN"

    #@c
    const-string v2, "Need BLUETOOTH ADMIN permission"

    #@e
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 334
    invoke-direct {p0}, Lcom/lge/systemservice/service/BtLgeExtService;->HCIDumpDisableNative()I
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_17

    #@14
    move-result v0

    #@15
    monitor-exit p0

    #@16
    return v0

    #@17
    .line 331
    :catchall_17
    move-exception v0

    #@18
    monitor-exit p0

    #@19
    throw v0
.end method

.method public declared-synchronized HCIDumpEnable()I
    .registers 4

    #@0
    .prologue
    .line 320
    monitor-enter p0

    #@1
    :try_start_1
    const-string v0, "BtLgeExtService"

    #@3
    const-string v1, "HCIDumpEnable"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 322
    iget-object v0, p0, Lcom/lge/systemservice/service/BtLgeExtService;->mContext:Landroid/content/Context;

    #@a
    const-string v1, "android.permission.BLUETOOTH_ADMIN"

    #@c
    const-string v2, "Need BLUETOOTH ADMIN permission"

    #@e
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 323
    invoke-direct {p0}, Lcom/lge/systemservice/service/BtLgeExtService;->HCIDumpEnableNative()I
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_17

    #@14
    move-result v0

    #@15
    monitor-exit p0

    #@16
    return v0

    #@17
    .line 320
    :catchall_17
    move-exception v0

    #@18
    monitor-exit p0

    #@19
    throw v0
.end method

.method public declared-synchronized SSPDebugModeDisable()I
    .registers 4

    #@0
    .prologue
    .line 254
    monitor-enter p0

    #@1
    :try_start_1
    const-string v0, "BtLgeExtService"

    #@3
    const-string v1, "SSPDebugModeDisable"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 256
    iget-object v0, p0, Lcom/lge/systemservice/service/BtLgeExtService;->mContext:Landroid/content/Context;

    #@a
    const-string v1, "android.permission.BLUETOOTH_ADMIN"

    #@c
    const-string v2, "Need BLUETOOTH ADMIN permission"

    #@e
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 257
    invoke-direct {p0}, Lcom/lge/systemservice/service/BtLgeExtService;->SSPDebugModeDisableNative()I
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_17

    #@14
    move-result v0

    #@15
    monitor-exit p0

    #@16
    return v0

    #@17
    .line 254
    :catchall_17
    move-exception v0

    #@18
    monitor-exit p0

    #@19
    throw v0
.end method

.method public declared-synchronized SSPDebugModeEnable()I
    .registers 4

    #@0
    .prologue
    .line 243
    monitor-enter p0

    #@1
    :try_start_1
    const-string v0, "BtLgeExtService"

    #@3
    const-string v1, "SSPDebugModeEnable"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 245
    iget-object v0, p0, Lcom/lge/systemservice/service/BtLgeExtService;->mContext:Landroid/content/Context;

    #@a
    const-string v1, "android.permission.BLUETOOTH_ADMIN"

    #@c
    const-string v2, "Need BLUETOOTH ADMIN permission"

    #@e
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 246
    invoke-direct {p0}, Lcom/lge/systemservice/service/BtLgeExtService;->SSPDebugModeEnableNative()I
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_17

    #@14
    move-result v0

    #@15
    monitor-exit p0

    #@16
    return v0

    #@17
    .line 243
    :catchall_17
    move-exception v0

    #@18
    monitor-exit p0

    #@19
    throw v0
.end method

.method public declared-synchronized endLeRxTest()I
    .registers 4

    #@0
    .prologue
    .line 307
    monitor-enter p0

    #@1
    :try_start_1
    const-string v0, "BtLgeExtService"

    #@3
    const-string v1, "endLeRxTest"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 309
    iget-object v0, p0, Lcom/lge/systemservice/service/BtLgeExtService;->mContext:Landroid/content/Context;

    #@a
    const-string v1, "android.permission.BLUETOOTH_ADMIN"

    #@c
    const-string v2, "Need BLUETOOTH ADMIN permission"

    #@e
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 310
    invoke-direct {p0}, Lcom/lge/systemservice/service/BtLgeExtService;->endLeRxTestNative()I
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_17

    #@14
    move-result v0

    #@15
    monitor-exit p0

    #@16
    return v0

    #@17
    .line 307
    :catchall_17
    move-exception v0

    #@18
    monitor-exit p0

    #@19
    throw v0
.end method

.method public declared-synchronized endLeTxTest()I
    .registers 4

    #@0
    .prologue
    .line 296
    monitor-enter p0

    #@1
    :try_start_1
    const-string v0, "BtLgeExtService"

    #@3
    const-string v1, "endLeTxTest"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 298
    iget-object v0, p0, Lcom/lge/systemservice/service/BtLgeExtService;->mContext:Landroid/content/Context;

    #@a
    const-string v1, "android.permission.BLUETOOTH_ADMIN"

    #@c
    const-string v2, "Need BLUETOOTH ADMIN permission"

    #@e
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 299
    invoke-direct {p0}, Lcom/lge/systemservice/service/BtLgeExtService;->endLeTxTestNative()I
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_17

    #@14
    move-result v0

    #@15
    monitor-exit p0

    #@16
    return v0

    #@17
    .line 296
    :catchall_17
    move-exception v0

    #@18
    monitor-exit p0

    #@19
    throw v0
.end method

.method public declared-synchronized enterBluetoothDUTMode(I)I
    .registers 5
    .parameter "index"

    #@0
    .prologue
    .line 169
    monitor-enter p0

    #@1
    :try_start_1
    const-string v0, "BtLgeExtService"

    #@3
    new-instance v1, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v2, "[BTUI] enterBluetoothDUTMode : index = "

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 171
    iget-object v0, p0, Lcom/lge/systemservice/service/BtLgeExtService;->mContext:Landroid/content/Context;

    #@1b
    const-string v1, "android.permission.BLUETOOTH_ADMIN"

    #@1d
    const-string v2, "Need BLUETOOTH ADMIN permission"

    #@1f
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@22
    .line 172
    invoke-direct {p0, p1}, Lcom/lge/systemservice/service/BtLgeExtService;->enterBluetoothDUTModeNative(I)I
    :try_end_25
    .catchall {:try_start_1 .. :try_end_25} :catchall_28

    #@25
    move-result v0

    #@26
    monitor-exit p0

    #@27
    return v0

    #@28
    .line 169
    :catchall_28
    move-exception v0

    #@29
    monitor-exit p0

    #@2a
    throw v0
.end method

.method public declared-synchronized enterBluetoothDUTModeBle(I)I
    .registers 5
    .parameter "channel"

    #@0
    .prologue
    .line 206
    monitor-enter p0

    #@1
    :try_start_1
    const-string v0, "BtLgeExtService"

    #@3
    new-instance v1, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v2, "[BTUI] enterBluetoothDUTModeBle : channel = "

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 208
    iget-object v0, p0, Lcom/lge/systemservice/service/BtLgeExtService;->mContext:Landroid/content/Context;

    #@1b
    const-string v1, "android.permission.BLUETOOTH_ADMIN"

    #@1d
    const-string v2, "Need BLUETOOTH ADMIN permission"

    #@1f
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@22
    .line 209
    invoke-direct {p0, p1}, Lcom/lge/systemservice/service/BtLgeExtService;->enterBluetoothDUTModeBleNative(I)I
    :try_end_25
    .catchall {:try_start_1 .. :try_end_25} :catchall_28

    #@25
    move-result v0

    #@26
    monitor-exit p0

    #@27
    return v0

    #@28
    .line 206
    :catchall_28
    move-exception v0

    #@29
    monitor-exit p0

    #@2a
    throw v0
.end method

.method public declared-synchronized exitBluetoothDUTMode()I
    .registers 4

    #@0
    .prologue
    .line 180
    monitor-enter p0

    #@1
    :try_start_1
    const-string v0, "BtLgeExtService"

    #@3
    const-string v1, "[BTUI] exitBluetoothDUTMode"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 182
    iget-object v0, p0, Lcom/lge/systemservice/service/BtLgeExtService;->mContext:Landroid/content/Context;

    #@a
    const-string v1, "android.permission.BLUETOOTH_ADMIN"

    #@c
    const-string v2, "Need BLUETOOTH ADMIN permission"

    #@e
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 183
    invoke-direct {p0}, Lcom/lge/systemservice/service/BtLgeExtService;->exitBluetoothDUTModeNative()I
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_17

    #@14
    move-result v0

    #@15
    monitor-exit p0

    #@16
    return v0

    #@17
    .line 180
    :catchall_17
    move-exception v0

    #@18
    monitor-exit p0

    #@19
    throw v0
.end method

.method public declared-synchronized exitBluetoothDUTModeBle()I
    .registers 4

    #@0
    .prologue
    .line 217
    monitor-enter p0

    #@1
    :try_start_1
    const-string v0, "BtLgeExtService"

    #@3
    const-string v1, "[BTUI] exitBluetoothDUTModeBle"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 219
    iget-object v0, p0, Lcom/lge/systemservice/service/BtLgeExtService;->mContext:Landroid/content/Context;

    #@a
    const-string v1, "android.permission.BLUETOOTH_ADMIN"

    #@c
    const-string v2, "Need BLUETOOTH ADMIN permission"

    #@e
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 220
    invoke-direct {p0}, Lcom/lge/systemservice/service/BtLgeExtService;->exitBluetoothDUTModeBleNative()I
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_17

    #@14
    move-result v0

    #@15
    monitor-exit p0

    #@16
    return v0

    #@17
    .line 217
    :catchall_17
    move-exception v0

    #@18
    monitor-exit p0

    #@19
    throw v0
.end method

.method public declared-synchronized isBluetoothBleDUTMode()I
    .registers 4

    #@0
    .prologue
    .line 228
    monitor-enter p0

    #@1
    :try_start_1
    const-string v0, "BtLgeExtService"

    #@3
    const-string v1, "[BTUI] isBluetoothBleDUTMode"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 230
    iget-object v0, p0, Lcom/lge/systemservice/service/BtLgeExtService;->mContext:Landroid/content/Context;

    #@a
    const-string v1, "android.permission.BLUETOOTH_ADMIN"

    #@c
    const-string v2, "Need BLUETOOTH ADMIN permission"

    #@e
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 231
    invoke-direct {p0}, Lcom/lge/systemservice/service/BtLgeExtService;->isBluetoothBleDUTModeNative()I
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_17

    #@14
    move-result v0

    #@15
    monitor-exit p0

    #@16
    return v0

    #@17
    .line 228
    :catchall_17
    move-exception v0

    #@18
    monitor-exit p0

    #@19
    throw v0
.end method

.method public declared-synchronized isBluetoothDUTMode()I
    .registers 4

    #@0
    .prologue
    .line 191
    monitor-enter p0

    #@1
    :try_start_1
    const-string v0, "BtLgeExtService"

    #@3
    const-string v1, "[BTUI] isBluetoothDUTMode"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 193
    iget-object v0, p0, Lcom/lge/systemservice/service/BtLgeExtService;->mContext:Landroid/content/Context;

    #@a
    const-string v1, "android.permission.BLUETOOTH_ADMIN"

    #@c
    const-string v2, "Need BLUETOOTH ADMIN permission"

    #@e
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 194
    invoke-direct {p0}, Lcom/lge/systemservice/service/BtLgeExtService;->isBluetoothDUTModeNative()I
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_17

    #@14
    move-result v0

    #@15
    monitor-exit p0

    #@16
    return v0

    #@17
    .line 191
    :catchall_17
    move-exception v0

    #@18
    monitor-exit p0

    #@19
    throw v0
.end method

.method public declared-synchronized loadBluetoothAddress()I
    .registers 4

    #@0
    .prologue
    .line 60
    monitor-enter p0

    #@1
    :try_start_1
    const-string v0, "BtLgeExtService"

    #@3
    const-string v1, "[BTUI] loadBluetoothAddress"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 62
    iget-object v0, p0, Lcom/lge/systemservice/service/BtLgeExtService;->mContext:Landroid/content/Context;

    #@a
    const-string v1, "android.permission.BLUETOOTH_ADMIN"

    #@c
    const-string v2, "Need BLUETOOTH ADMIN permission"

    #@e
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 63
    invoke-direct {p0}, Lcom/lge/systemservice/service/BtLgeExtService;->loadBluetoothAddressNative()I
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_17

    #@14
    move-result v0

    #@15
    monitor-exit p0

    #@16
    return v0

    #@17
    .line 60
    :catchall_17
    move-exception v0

    #@18
    monitor-exit p0

    #@19
    throw v0
.end method

.method public registerReceiver()V
    .registers 4

    #@0
    .prologue
    .line 70
    iget-object v1, p0, Lcom/lge/systemservice/service/BtLgeExtService;->mContext:Landroid/content/Context;

    #@2
    if-eqz v1, :cond_1e

    #@4
    .line 71
    new-instance v0, Landroid/content/IntentFilter;

    #@6
    const-string v1, "android.bluetooth.adapter.action.STATE_CHANGED"

    #@8
    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@b
    .line 72
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "android.bluetooth.a2dp.profile.action.PLAYING_STATE_CHANGED"

    #@d
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@10
    .line 73
    iget-object v1, p0, Lcom/lge/systemservice/service/BtLgeExtService;->mContext:Landroid/content/Context;

    #@12
    iget-object v2, p0, Lcom/lge/systemservice/service/BtLgeExtService;->mReceiver:Landroid/content/BroadcastReceiver;

    #@14
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@17
    .line 75
    const-string v1, "BtLgeExtService"

    #@19
    const-string v2, "[BTUI] registerReceiver"

    #@1b
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 78
    .end local v0           #filter:Landroid/content/IntentFilter;
    :cond_1e
    return-void
.end method

.method public declared-synchronized startLeReceiverTest(I)I
    .registers 5
    .parameter "rx_freq"

    #@0
    .prologue
    .line 282
    monitor-enter p0

    #@1
    :try_start_1
    const-string v0, "BtLgeExtService"

    #@3
    const-string v1, "startLeReceiverTest"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 284
    iget-object v0, p0, Lcom/lge/systemservice/service/BtLgeExtService;->mContext:Landroid/content/Context;

    #@a
    const-string v1, "android.permission.BLUETOOTH_ADMIN"

    #@c
    const-string v2, "Need BLUETOOTH ADMIN permission"

    #@e
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 286
    const-string v0, "BtLgeExtService"

    #@13
    new-instance v1, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v2, "rx_freq: "

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v1

    #@26
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 288
    invoke-direct {p0, p1}, Lcom/lge/systemservice/service/BtLgeExtService;->startLeReceiverTestNative(I)I
    :try_end_2c
    .catchall {:try_start_1 .. :try_end_2c} :catchall_2f

    #@2c
    move-result v0

    #@2d
    monitor-exit p0

    #@2e
    return v0

    #@2f
    .line 282
    :catchall_2f
    move-exception v0

    #@30
    monitor-exit p0

    #@31
    throw v0
.end method

.method public declared-synchronized startLeTransmitterTest(III)I
    .registers 7
    .parameter "tx_freq"
    .parameter "data_len"
    .parameter "pkt_type"

    #@0
    .prologue
    .line 268
    monitor-enter p0

    #@1
    :try_start_1
    const-string v0, "BtLgeExtService"

    #@3
    const-string v1, "startLeTransmitterTest"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 270
    iget-object v0, p0, Lcom/lge/systemservice/service/BtLgeExtService;->mContext:Landroid/content/Context;

    #@a
    const-string v1, "android.permission.BLUETOOTH_ADMIN"

    #@c
    const-string v2, "Need BLUETOOTH ADMIN permission"

    #@e
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 272
    const-string v0, "BtLgeExtService"

    #@13
    new-instance v1, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v2, "tx_freq: "

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    const-string v2, ", data_len: "

    #@24
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v1

    #@28
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v1

    #@2c
    const-string v2, ", pkt_type: "

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35
    move-result-object v1

    #@36
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v1

    #@3a
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 274
    invoke-direct {p0, p1, p2, p3}, Lcom/lge/systemservice/service/BtLgeExtService;->startLeTransmitterTestNative(III)I
    :try_end_40
    .catchall {:try_start_1 .. :try_end_40} :catchall_43

    #@40
    move-result v0

    #@41
    monitor-exit p0

    #@42
    return v0

    #@43
    .line 268
    :catchall_43
    move-exception v0

    #@44
    monitor-exit p0

    #@45
    throw v0
.end method

.method public unregisterReceiver()V
    .registers 3

    #@0
    .prologue
    .line 80
    iget-object v0, p0, Lcom/lge/systemservice/service/BtLgeExtService;->mContext:Landroid/content/Context;

    #@2
    if-eqz v0, :cond_12

    #@4
    .line 81
    iget-object v0, p0, Lcom/lge/systemservice/service/BtLgeExtService;->mContext:Landroid/content/Context;

    #@6
    iget-object v1, p0, Lcom/lge/systemservice/service/BtLgeExtService;->mReceiver:Landroid/content/BroadcastReceiver;

    #@8
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@b
    .line 83
    const-string v0, "BtLgeExtService"

    #@d
    const-string v1, "[BTUI] unregisterReceiver"

    #@f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 86
    :cond_12
    return-void
.end method
