.class Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice$DeleteGabageFilesThread;
.super Ljava/lang/Thread;
.source "WatchNetStorageService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DeleteGabageFilesThread"
.end annotation


# instance fields
.field mMountPoint:Ljava/lang/String;

.field mRetryCount:I

.field final synthetic this$1:Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;


# direct methods
.method public constructor <init>(Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;Ljava/lang/String;I)V
    .registers 4
    .parameter
    .parameter "path"
    .parameter "retryCount"

    #@0
    .prologue
    .line 635
    iput-object p1, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice$DeleteGabageFilesThread;->this$1:Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;

    #@2
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@5
    .line 636
    iput-object p2, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice$DeleteGabageFilesThread;->mMountPoint:Ljava/lang/String;

    #@7
    .line 637
    iput p3, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice$DeleteGabageFilesThread;->mRetryCount:I

    #@9
    .line 638
    return-void
.end method

.method private removeGabageFiles()V
    .registers 3

    #@0
    .prologue
    .line 642
    iget-object v0, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice$DeleteGabageFilesThread;->this$1:Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;

    #@2
    iget-object v0, v0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->this$0:Lcom/lge/systemservice/service/WatchNetStorageService;

    #@4
    iget-object v1, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice$DeleteGabageFilesThread;->mMountPoint:Ljava/lang/String;

    #@6
    invoke-static {v0, v1}, Lcom/lge/systemservice/service/WatchNetStorageService;->access$200(Lcom/lge/systemservice/service/WatchNetStorageService;Ljava/lang/String;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_10

    #@c
    .line 644
    const/4 v0, 0x0

    #@d
    iput v0, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice$DeleteGabageFilesThread;->mRetryCount:I

    #@f
    .line 659
    :goto_f
    return-void

    #@10
    .line 648
    :cond_10
    new-instance v0, Ljava/io/File;

    #@12
    iget-object v1, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice$DeleteGabageFilesThread;->mMountPoint:Ljava/lang/String;

    #@14
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@17
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@1a
    move-result v0

    #@1b
    if-eqz v0, :cond_27

    #@1d
    .line 651
    iget-object v0, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice$DeleteGabageFilesThread;->this$1:Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;

    #@1f
    iget-object v0, v0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->this$0:Lcom/lge/systemservice/service/WatchNetStorageService;

    #@21
    iget-object v1, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice$DeleteGabageFilesThread;->mMountPoint:Ljava/lang/String;

    #@23
    invoke-static {v0, v1}, Lcom/lge/systemservice/service/WatchNetStorageService;->access$500(Lcom/lge/systemservice/service/WatchNetStorageService;Ljava/lang/String;)Z

    #@26
    goto :goto_f

    #@27
    .line 655
    :cond_27
    iget v0, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice$DeleteGabageFilesThread;->mRetryCount:I

    #@29
    add-int/lit8 v0, v0, -0x1

    #@2b
    iput v0, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice$DeleteGabageFilesThread;->mRetryCount:I

    #@2d
    goto :goto_f
.end method


# virtual methods
.method public run()V
    .registers 3

    #@0
    .prologue
    .line 666
    :goto_0
    iget v0, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice$DeleteGabageFilesThread;->mRetryCount:I

    #@2
    if-lez v0, :cond_f

    #@4
    .line 669
    const-wide/16 v0, 0x3e8

    #@6
    :try_start_6
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_9
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_9} :catch_d

    #@9
    .line 672
    :goto_9
    invoke-direct {p0}, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice$DeleteGabageFilesThread;->removeGabageFiles()V

    #@c
    goto :goto_0

    #@d
    .line 670
    :catch_d
    move-exception v0

    #@e
    goto :goto_9

    #@f
    .line 676
    :cond_f
    return-void
.end method
