.class final Lcom/lge/systemservice/service/NativeDaemonConnector;
.super Ljava/lang/Object;
.source "NativeDaemonConnector.java"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue;,
        Lcom/lge/systemservice/service/NativeDaemonConnector$NativeDaemonFailureException;,
        Lcom/lge/systemservice/service/NativeDaemonConnector$NativeDaemonArgumentException;
    }
.end annotation


# instance fields
.field private final BUFFER_SIZE:I

.field private final TAG:Ljava/lang/String;

.field private mCallbackHandler:Landroid/os/Handler;

.field private mCallbacks:Lcom/lge/systemservice/service/INativeDaemonConnectorCallbacks;

.field private final mDaemonLock:Ljava/lang/Object;

.field private mLocalLog:Landroid/util/LocalLog;

.field private mOutputStream:Ljava/io/OutputStream;

.field private final mResponseQueue:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue;

.field private mSequenceNumber:Ljava/util/concurrent/atomic/AtomicInteger;

.field private mSocket:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/INativeDaemonConnectorCallbacks;Ljava/lang/String;ILjava/lang/String;I)V
    .registers 8
    .parameter "callbacks"
    .parameter "socket"
    .parameter "responseQueueSize"
    .parameter "logTag"
    .parameter "maxLogSize"

    #@0
    .prologue
    .line 69
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 64
    new-instance v0, Ljava/lang/Object;

    #@5
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@8
    iput-object v0, p0, Lcom/lge/systemservice/service/NativeDaemonConnector;->mDaemonLock:Ljava/lang/Object;

    #@a
    .line 66
    const/16 v0, 0x1000

    #@c
    iput v0, p0, Lcom/lge/systemservice/service/NativeDaemonConnector;->BUFFER_SIZE:I

    #@e
    .line 70
    iput-object p1, p0, Lcom/lge/systemservice/service/NativeDaemonConnector;->mCallbacks:Lcom/lge/systemservice/service/INativeDaemonConnectorCallbacks;

    #@10
    .line 71
    iput-object p2, p0, Lcom/lge/systemservice/service/NativeDaemonConnector;->mSocket:Ljava/lang/String;

    #@12
    .line 72
    new-instance v0, Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue;

    #@14
    invoke-direct {v0, p3}, Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue;-><init>(I)V

    #@17
    iput-object v0, p0, Lcom/lge/systemservice/service/NativeDaemonConnector;->mResponseQueue:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue;

    #@19
    .line 73
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    #@1b
    const/4 v1, 0x0

    #@1c
    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    #@1f
    iput-object v0, p0, Lcom/lge/systemservice/service/NativeDaemonConnector;->mSequenceNumber:Ljava/util/concurrent/atomic/AtomicInteger;

    #@21
    .line 74
    if-eqz p4, :cond_2d

    #@23
    .end local p4
    :goto_23
    iput-object p4, p0, Lcom/lge/systemservice/service/NativeDaemonConnector;->TAG:Ljava/lang/String;

    #@25
    .line 75
    new-instance v0, Landroid/util/LocalLog;

    #@27
    invoke-direct {v0, p5}, Landroid/util/LocalLog;-><init>(I)V

    #@2a
    iput-object v0, p0, Lcom/lge/systemservice/service/NativeDaemonConnector;->mLocalLog:Landroid/util/LocalLog;

    #@2c
    .line 76
    return-void

    #@2d
    .line 74
    .restart local p4
    :cond_2d
    const-string p4, "NativeDaemonConnector"

    #@2f
    goto :goto_23
.end method

.method static appendEscaped(Ljava/lang/StringBuilder;Ljava/lang/String;)V
    .registers 8
    .parameter "builder"
    .parameter "arg"

    #@0
    .prologue
    const/16 v5, 0x22

    #@2
    .line 398
    const/16 v4, 0x20

    #@4
    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(I)I

    #@7
    move-result v4

    #@8
    if-ltz v4, :cond_25

    #@a
    const/4 v1, 0x1

    #@b
    .line 399
    .local v1, hasSpaces:Z
    :goto_b
    if-eqz v1, :cond_10

    #@d
    .line 400
    invoke-virtual {p0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@10
    .line 403
    :cond_10
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@13
    move-result v3

    #@14
    .line 404
    .local v3, length:I
    const/4 v2, 0x0

    #@15
    .local v2, i:I
    :goto_15
    if-ge v2, v3, :cond_35

    #@17
    .line 405
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    #@1a
    move-result v0

    #@1b
    .line 407
    .local v0, c:C
    if-ne v0, v5, :cond_27

    #@1d
    .line 408
    const-string v4, "\\\""

    #@1f
    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    .line 404
    :goto_22
    add-int/lit8 v2, v2, 0x1

    #@24
    goto :goto_15

    #@25
    .line 398
    .end local v0           #c:C
    .end local v1           #hasSpaces:Z
    .end local v2           #i:I
    .end local v3           #length:I
    :cond_25
    const/4 v1, 0x0

    #@26
    goto :goto_b

    #@27
    .line 409
    .restart local v0       #c:C
    .restart local v1       #hasSpaces:Z
    .restart local v2       #i:I
    .restart local v3       #length:I
    :cond_27
    const/16 v4, 0x5c

    #@29
    if-ne v0, v4, :cond_31

    #@2b
    .line 410
    const-string v4, "\\\\"

    #@2d
    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    goto :goto_22

    #@31
    .line 412
    :cond_31
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@34
    goto :goto_22

    #@35
    .line 416
    .end local v0           #c:C
    :cond_35
    if-eqz v1, :cond_3a

    #@37
    .line 417
    invoke-virtual {p0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@3a
    .line 419
    :cond_3a
    return-void
.end method

.method private listenToSocket()V
    .registers 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 108
    const/4 v12, 0x0

    #@1
    .line 111
    .local v12, socket:Landroid/net/LocalSocket;
    :try_start_1
    new-instance v13, Landroid/net/LocalSocket;

    #@3
    invoke-direct {v13}, Landroid/net/LocalSocket;-><init>()V
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_c0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_6} :catch_249

    #@6
    .line 112
    .end local v12           #socket:Landroid/net/LocalSocket;
    .local v13, socket:Landroid/net/LocalSocket;
    :try_start_6
    new-instance v2, Landroid/net/LocalSocketAddress;

    #@8
    move-object/from16 v0, p0

    #@a
    iget-object v15, v0, Lcom/lge/systemservice/service/NativeDaemonConnector;->mSocket:Ljava/lang/String;

    #@c
    sget-object v16, Landroid/net/LocalSocketAddress$Namespace;->RESERVED:Landroid/net/LocalSocketAddress$Namespace;

    #@e
    move-object/from16 v0, v16

    #@10
    invoke-direct {v2, v15, v0}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;Landroid/net/LocalSocketAddress$Namespace;)V

    #@13
    .line 114
    .local v2, address:Landroid/net/LocalSocketAddress;
    invoke-virtual {v13, v2}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V

    #@16
    .line 115
    invoke-virtual {v13}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;

    #@19
    move-result-object v9

    #@1a
    .line 116
    .local v9, inputStream:Ljava/io/InputStream;
    move-object/from16 v0, p0

    #@1c
    iget-object v0, v0, Lcom/lge/systemservice/service/NativeDaemonConnector;->mDaemonLock:Ljava/lang/Object;

    #@1e
    move-object/from16 v16, v0

    #@20
    monitor-enter v16
    :try_end_21
    .catchall {:try_start_6 .. :try_end_21} :catchall_194
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_21} :catch_a5

    #@21
    .line 117
    :try_start_21
    invoke-virtual {v13}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    #@24
    move-result-object v15

    #@25
    move-object/from16 v0, p0

    #@27
    iput-object v15, v0, Lcom/lge/systemservice/service/NativeDaemonConnector;->mOutputStream:Ljava/io/OutputStream;

    #@29
    .line 118
    monitor-exit v16
    :try_end_2a
    .catchall {:try_start_21 .. :try_end_2a} :catchall_a2

    #@2a
    .line 119
    :try_start_2a
    move-object/from16 v0, p0

    #@2c
    iget-object v15, v0, Lcom/lge/systemservice/service/NativeDaemonConnector;->mCallbacks:Lcom/lge/systemservice/service/INativeDaemonConnectorCallbacks;

    #@2e
    invoke-interface {v15}, Lcom/lge/systemservice/service/INativeDaemonConnectorCallbacks;->onDaemonConnected()V

    #@31
    .line 120
    const/16 v15, 0x1000

    #@33
    new-array v3, v15, [B

    #@35
    .line 121
    .local v3, buffer:[B
    const/4 v14, 0x0

    #@36
    .line 124
    .local v14, start:I
    :goto_36
    rsub-int v15, v14, 0x1000

    #@38
    invoke-virtual {v9, v3, v14, v15}, Ljava/io/InputStream;->read([BII)I

    #@3b
    move-result v4

    #@3c
    .line 125
    .local v4, count:I
    if-gez v4, :cond_108

    #@3e
    .line 126
    new-instance v15, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v16, "got "

    #@45
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v15

    #@49
    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v15

    #@4d
    const-string v16, " reading with start = "

    #@4f
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v15

    #@53
    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@56
    move-result-object v15

    #@57
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v15

    #@5b
    move-object/from16 v0, p0

    #@5d
    invoke-direct {v0, v15}, Lcom/lge/systemservice/service/NativeDaemonConnector;->loge(Ljava/lang/String;)V
    :try_end_60
    .catchall {:try_start_2a .. :try_end_60} :catchall_194
    .catch Ljava/io/IOException; {:try_start_2a .. :try_end_60} :catch_a5

    #@60
    .line 176
    move-object/from16 v0, p0

    #@62
    iget-object v0, v0, Lcom/lge/systemservice/service/NativeDaemonConnector;->mDaemonLock:Ljava/lang/Object;

    #@64
    move-object/from16 v16, v0

    #@66
    monitor-enter v16

    #@67
    .line 177
    :try_start_67
    move-object/from16 v0, p0

    #@69
    iget-object v15, v0, Lcom/lge/systemservice/service/NativeDaemonConnector;->mOutputStream:Ljava/io/OutputStream;
    :try_end_6b
    .catchall {:try_start_67 .. :try_end_6b} :catchall_1ea

    #@6b
    if-eqz v15, :cond_9b

    #@6d
    .line 179
    :try_start_6d
    new-instance v15, Ljava/lang/StringBuilder;

    #@6f
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@72
    const-string v17, "closing stream for "

    #@74
    move-object/from16 v0, v17

    #@76
    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v15

    #@7a
    move-object/from16 v0, p0

    #@7c
    iget-object v0, v0, Lcom/lge/systemservice/service/NativeDaemonConnector;->mSocket:Ljava/lang/String;

    #@7e
    move-object/from16 v17, v0

    #@80
    move-object/from16 v0, v17

    #@82
    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v15

    #@86
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@89
    move-result-object v15

    #@8a
    move-object/from16 v0, p0

    #@8c
    invoke-direct {v0, v15}, Lcom/lge/systemservice/service/NativeDaemonConnector;->loge(Ljava/lang/String;)V

    #@8f
    .line 180
    move-object/from16 v0, p0

    #@91
    iget-object v15, v0, Lcom/lge/systemservice/service/NativeDaemonConnector;->mOutputStream:Ljava/io/OutputStream;

    #@93
    invoke-virtual {v15}, Ljava/io/OutputStream;->close()V
    :try_end_96
    .catchall {:try_start_6d .. :try_end_96} :catchall_1ea
    .catch Ljava/io/IOException; {:try_start_6d .. :try_end_96} :catch_1cd

    #@96
    .line 184
    :goto_96
    const/4 v15, 0x0

    #@97
    :try_start_97
    move-object/from16 v0, p0

    #@99
    iput-object v15, v0, Lcom/lge/systemservice/service/NativeDaemonConnector;->mOutputStream:Ljava/io/OutputStream;

    #@9b
    .line 186
    :cond_9b
    monitor-exit v16
    :try_end_9c
    .catchall {:try_start_97 .. :try_end_9c} :catchall_1ea

    #@9c
    .line 189
    if-eqz v13, :cond_a1

    #@9e
    .line 190
    :try_start_9e
    invoke-virtual {v13}, Landroid/net/LocalSocket;->close()V
    :try_end_a1
    .catch Ljava/io/IOException; {:try_start_9e .. :try_end_a1} :catch_1ed

    #@a1
    .line 196
    :cond_a1
    :goto_a1
    return-void

    #@a2
    .line 118
    .end local v3           #buffer:[B
    .end local v4           #count:I
    .end local v14           #start:I
    :catchall_a2
    move-exception v15

    #@a3
    :try_start_a3
    monitor-exit v16
    :try_end_a4
    .catchall {:try_start_a3 .. :try_end_a4} :catchall_a2

    #@a4
    :try_start_a4
    throw v15
    :try_end_a5
    .catchall {:try_start_a4 .. :try_end_a5} :catchall_194
    .catch Ljava/io/IOException; {:try_start_a4 .. :try_end_a5} :catch_a5

    #@a5
    .line 172
    .end local v2           #address:Landroid/net/LocalSocketAddress;
    .end local v9           #inputStream:Ljava/io/InputStream;
    :catch_a5
    move-exception v7

    #@a6
    move-object v12, v13

    #@a7
    .line 173
    .end local v13           #socket:Landroid/net/LocalSocket;
    .local v7, ex:Ljava/io/IOException;
    .restart local v12       #socket:Landroid/net/LocalSocket;
    :goto_a7
    :try_start_a7
    new-instance v15, Ljava/lang/StringBuilder;

    #@a9
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@ac
    const-string v16, "Communications error: "

    #@ae
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v15

    #@b2
    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v15

    #@b6
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b9
    move-result-object v15

    #@ba
    move-object/from16 v0, p0

    #@bc
    invoke-direct {v0, v15}, Lcom/lge/systemservice/service/NativeDaemonConnector;->loge(Ljava/lang/String;)V

    #@bf
    .line 174
    throw v7
    :try_end_c0
    .catchall {:try_start_a7 .. :try_end_c0} :catchall_c0

    #@c0
    .line 176
    .end local v7           #ex:Ljava/io/IOException;
    :catchall_c0
    move-exception v15

    #@c1
    :goto_c1
    move-object/from16 v0, p0

    #@c3
    iget-object v0, v0, Lcom/lge/systemservice/service/NativeDaemonConnector;->mDaemonLock:Ljava/lang/Object;

    #@c5
    move-object/from16 v16, v0

    #@c7
    monitor-enter v16

    #@c8
    .line 177
    :try_start_c8
    move-object/from16 v0, p0

    #@ca
    iget-object v0, v0, Lcom/lge/systemservice/service/NativeDaemonConnector;->mOutputStream:Ljava/io/OutputStream;

    #@cc
    move-object/from16 v17, v0
    :try_end_ce
    .catchall {:try_start_c8 .. :try_end_ce} :catchall_227

    #@ce
    if-eqz v17, :cond_101

    #@d0
    .line 179
    :try_start_d0
    new-instance v17, Ljava/lang/StringBuilder;

    #@d2
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@d5
    const-string v18, "closing stream for "

    #@d7
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@da
    move-result-object v17

    #@db
    move-object/from16 v0, p0

    #@dd
    iget-object v0, v0, Lcom/lge/systemservice/service/NativeDaemonConnector;->mSocket:Ljava/lang/String;

    #@df
    move-object/from16 v18, v0

    #@e1
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v17

    #@e5
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e8
    move-result-object v17

    #@e9
    move-object/from16 v0, p0

    #@eb
    move-object/from16 v1, v17

    #@ed
    invoke-direct {v0, v1}, Lcom/lge/systemservice/service/NativeDaemonConnector;->loge(Ljava/lang/String;)V

    #@f0
    .line 180
    move-object/from16 v0, p0

    #@f2
    iget-object v0, v0, Lcom/lge/systemservice/service/NativeDaemonConnector;->mOutputStream:Ljava/io/OutputStream;

    #@f4
    move-object/from16 v17, v0

    #@f6
    invoke-virtual/range {v17 .. v17}, Ljava/io/OutputStream;->close()V
    :try_end_f9
    .catchall {:try_start_d0 .. :try_end_f9} :catchall_227
    .catch Ljava/io/IOException; {:try_start_d0 .. :try_end_f9} :catch_208

    #@f9
    .line 184
    :goto_f9
    const/16 v17, 0x0

    #@fb
    :try_start_fb
    move-object/from16 v0, v17

    #@fd
    move-object/from16 v1, p0

    #@ff
    iput-object v0, v1, Lcom/lge/systemservice/service/NativeDaemonConnector;->mOutputStream:Ljava/io/OutputStream;

    #@101
    .line 186
    :cond_101
    monitor-exit v16
    :try_end_102
    .catchall {:try_start_fb .. :try_end_102} :catchall_227

    #@102
    .line 189
    if-eqz v12, :cond_107

    #@104
    .line 190
    :try_start_104
    invoke-virtual {v12}, Landroid/net/LocalSocket;->close()V
    :try_end_107
    .catch Ljava/io/IOException; {:try_start_104 .. :try_end_107} :catch_22a

    #@107
    .line 194
    :cond_107
    :goto_107
    throw v15

    #@108
    .line 131
    .end local v12           #socket:Landroid/net/LocalSocket;
    .restart local v2       #address:Landroid/net/LocalSocketAddress;
    .restart local v3       #buffer:[B
    .restart local v4       #count:I
    .restart local v9       #inputStream:Ljava/io/InputStream;
    .restart local v13       #socket:Landroid/net/LocalSocket;
    .restart local v14       #start:I
    :cond_108
    add-int/2addr v4, v14

    #@109
    .line 132
    const/4 v14, 0x0

    #@10a
    .line 134
    const/4 v8, 0x0

    #@10b
    .local v8, i:I
    :goto_10b
    if-ge v8, v4, :cond_198

    #@10d
    .line 135
    :try_start_10d
    aget-byte v15, v3, v8

    #@10f
    if-nez v15, :cond_15f

    #@111
    .line 136
    new-instance v10, Ljava/lang/String;

    #@113
    sub-int v15, v8, v14

    #@115
    sget-object v16, Ljava/nio/charset/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    #@117
    move-object/from16 v0, v16

    #@119
    invoke-direct {v10, v3, v14, v15, v0}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    #@11c
    .line 138
    .local v10, rawEvent:Ljava/lang/String;
    new-instance v15, Ljava/lang/StringBuilder;

    #@11e
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@121
    const-string v16, "RCV <- {"

    #@123
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@126
    move-result-object v15

    #@127
    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12a
    move-result-object v15

    #@12b
    const-string v16, "}"

    #@12d
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@130
    move-result-object v15

    #@131
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@134
    move-result-object v15

    #@135
    move-object/from16 v0, p0

    #@137
    invoke-direct {v0, v15}, Lcom/lge/systemservice/service/NativeDaemonConnector;->log(Ljava/lang/String;)V
    :try_end_13a
    .catchall {:try_start_10d .. :try_end_13a} :catchall_194
    .catch Ljava/io/IOException; {:try_start_10d .. :try_end_13a} :catch_a5

    #@13a
    .line 141
    :try_start_13a
    invoke-static {v10}, Lcom/lge/systemservice/service/NativeDaemonEvent;->parseRawEvent(Ljava/lang/String;)Lcom/lge/systemservice/service/NativeDaemonEvent;

    #@13d
    move-result-object v6

    #@13e
    .line 143
    .local v6, event:Lcom/lge/systemservice/service/NativeDaemonEvent;
    invoke-virtual {v6}, Lcom/lge/systemservice/service/NativeDaemonEvent;->isClassUnsolicited()Z

    #@141
    move-result v15

    #@142
    if-eqz v15, :cond_162

    #@144
    .line 145
    move-object/from16 v0, p0

    #@146
    iget-object v15, v0, Lcom/lge/systemservice/service/NativeDaemonConnector;->mCallbackHandler:Landroid/os/Handler;

    #@148
    move-object/from16 v0, p0

    #@14a
    iget-object v0, v0, Lcom/lge/systemservice/service/NativeDaemonConnector;->mCallbackHandler:Landroid/os/Handler;

    #@14c
    move-object/from16 v16, v0

    #@14e
    invoke-virtual {v6}, Lcom/lge/systemservice/service/NativeDaemonEvent;->getCode()I

    #@151
    move-result v17

    #@152
    invoke-virtual {v6}, Lcom/lge/systemservice/service/NativeDaemonEvent;->getRawEvent()Ljava/lang/String;

    #@155
    move-result-object v18

    #@156
    invoke-virtual/range {v16 .. v18}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@159
    move-result-object v16

    #@15a
    invoke-virtual/range {v15 .. v16}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@15d
    .line 154
    .end local v6           #event:Lcom/lge/systemservice/service/NativeDaemonEvent;
    :goto_15d
    add-int/lit8 v14, v8, 0x1

    #@15f
    .line 134
    .end local v10           #rawEvent:Ljava/lang/String;
    :cond_15f
    add-int/lit8 v8, v8, 0x1

    #@161
    goto :goto_10b

    #@162
    .line 148
    .restart local v6       #event:Lcom/lge/systemservice/service/NativeDaemonEvent;
    .restart local v10       #rawEvent:Ljava/lang/String;
    :cond_162
    move-object/from16 v0, p0

    #@164
    iget-object v15, v0, Lcom/lge/systemservice/service/NativeDaemonConnector;->mResponseQueue:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue;

    #@166
    invoke-virtual {v6}, Lcom/lge/systemservice/service/NativeDaemonEvent;->getCmdNumber()I

    #@169
    move-result v16

    #@16a
    move/from16 v0, v16

    #@16c
    invoke-virtual {v15, v0, v6}, Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue;->add(ILcom/lge/systemservice/service/NativeDaemonEvent;)V
    :try_end_16f
    .catchall {:try_start_13a .. :try_end_16f} :catchall_194
    .catch Ljava/lang/IllegalArgumentException; {:try_start_13a .. :try_end_16f} :catch_170
    .catch Ljava/io/IOException; {:try_start_13a .. :try_end_16f} :catch_a5

    #@16f
    goto :goto_15d

    #@170
    .line 150
    .end local v6           #event:Lcom/lge/systemservice/service/NativeDaemonEvent;
    :catch_170
    move-exception v5

    #@171
    .line 151
    .local v5, e:Ljava/lang/IllegalArgumentException;
    :try_start_171
    new-instance v15, Ljava/lang/StringBuilder;

    #@173
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@176
    const-string v16, "Problem parsing message: "

    #@178
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17b
    move-result-object v15

    #@17c
    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17f
    move-result-object v15

    #@180
    const-string v16, " - "

    #@182
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@185
    move-result-object v15

    #@186
    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@189
    move-result-object v15

    #@18a
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18d
    move-result-object v15

    #@18e
    move-object/from16 v0, p0

    #@190
    invoke-direct {v0, v15}, Lcom/lge/systemservice/service/NativeDaemonConnector;->log(Ljava/lang/String;)V

    #@193
    goto :goto_15d

    #@194
    .line 176
    .end local v2           #address:Landroid/net/LocalSocketAddress;
    .end local v3           #buffer:[B
    .end local v4           #count:I
    .end local v5           #e:Ljava/lang/IllegalArgumentException;
    .end local v8           #i:I
    .end local v9           #inputStream:Ljava/io/InputStream;
    .end local v10           #rawEvent:Ljava/lang/String;
    .end local v14           #start:I
    :catchall_194
    move-exception v15

    #@195
    move-object v12, v13

    #@196
    .end local v13           #socket:Landroid/net/LocalSocket;
    .restart local v12       #socket:Landroid/net/LocalSocket;
    goto/16 :goto_c1

    #@198
    .line 157
    .end local v12           #socket:Landroid/net/LocalSocket;
    .restart local v2       #address:Landroid/net/LocalSocketAddress;
    .restart local v3       #buffer:[B
    .restart local v4       #count:I
    .restart local v8       #i:I
    .restart local v9       #inputStream:Ljava/io/InputStream;
    .restart local v13       #socket:Landroid/net/LocalSocket;
    .restart local v14       #start:I
    :cond_198
    if-nez v14, :cond_1bf

    #@19a
    .line 158
    new-instance v10, Ljava/lang/String;

    #@19c
    sget-object v15, Ljava/nio/charset/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    #@19e
    invoke-direct {v10, v3, v14, v4, v15}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    #@1a1
    .line 159
    .restart local v10       #rawEvent:Ljava/lang/String;
    new-instance v15, Ljava/lang/StringBuilder;

    #@1a3
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@1a6
    const-string v16, "RCV incomplete <- {"

    #@1a8
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ab
    move-result-object v15

    #@1ac
    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1af
    move-result-object v15

    #@1b0
    const-string v16, "}"

    #@1b2
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b5
    move-result-object v15

    #@1b6
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b9
    move-result-object v15

    #@1ba
    move-object/from16 v0, p0

    #@1bc
    invoke-direct {v0, v15}, Lcom/lge/systemservice/service/NativeDaemonConnector;->log(Ljava/lang/String;)V

    #@1bf
    .line 164
    .end local v10           #rawEvent:Ljava/lang/String;
    :cond_1bf
    if-eq v14, v4, :cond_1ca

    #@1c1
    .line 165
    rsub-int v11, v14, 0x1000

    #@1c3
    .line 166
    .local v11, remaining:I
    const/4 v15, 0x0

    #@1c4
    invoke-static {v3, v14, v3, v15, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_1c7
    .catchall {:try_start_171 .. :try_end_1c7} :catchall_194
    .catch Ljava/io/IOException; {:try_start_171 .. :try_end_1c7} :catch_a5

    #@1c7
    .line 167
    move v14, v11

    #@1c8
    .line 168
    goto/16 :goto_36

    #@1ca
    .line 169
    .end local v11           #remaining:I
    :cond_1ca
    const/4 v14, 0x0

    #@1cb
    goto/16 :goto_36

    #@1cd
    .line 181
    .end local v8           #i:I
    :catch_1cd
    move-exception v5

    #@1ce
    .line 182
    .local v5, e:Ljava/io/IOException;
    :try_start_1ce
    new-instance v15, Ljava/lang/StringBuilder;

    #@1d0
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@1d3
    const-string v17, "Failed closing output stream: "

    #@1d5
    move-object/from16 v0, v17

    #@1d7
    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1da
    move-result-object v15

    #@1db
    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1de
    move-result-object v15

    #@1df
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e2
    move-result-object v15

    #@1e3
    move-object/from16 v0, p0

    #@1e5
    invoke-direct {v0, v15}, Lcom/lge/systemservice/service/NativeDaemonConnector;->loge(Ljava/lang/String;)V

    #@1e8
    goto/16 :goto_96

    #@1ea
    .line 186
    .end local v5           #e:Ljava/io/IOException;
    :catchall_1ea
    move-exception v15

    #@1eb
    monitor-exit v16
    :try_end_1ec
    .catchall {:try_start_1ce .. :try_end_1ec} :catchall_1ea

    #@1ec
    throw v15

    #@1ed
    .line 192
    :catch_1ed
    move-exception v7

    #@1ee
    .line 193
    .restart local v7       #ex:Ljava/io/IOException;
    new-instance v15, Ljava/lang/StringBuilder;

    #@1f0
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@1f3
    const-string v16, "Failed closing socket: "

    #@1f5
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f8
    move-result-object v15

    #@1f9
    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1fc
    move-result-object v15

    #@1fd
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@200
    move-result-object v15

    #@201
    move-object/from16 v0, p0

    #@203
    invoke-direct {v0, v15}, Lcom/lge/systemservice/service/NativeDaemonConnector;->loge(Ljava/lang/String;)V

    #@206
    goto/16 :goto_a1

    #@208
    .line 181
    .end local v2           #address:Landroid/net/LocalSocketAddress;
    .end local v3           #buffer:[B
    .end local v4           #count:I
    .end local v7           #ex:Ljava/io/IOException;
    .end local v9           #inputStream:Ljava/io/InputStream;
    .end local v13           #socket:Landroid/net/LocalSocket;
    .end local v14           #start:I
    .restart local v12       #socket:Landroid/net/LocalSocket;
    :catch_208
    move-exception v5

    #@209
    .line 182
    .restart local v5       #e:Ljava/io/IOException;
    :try_start_209
    new-instance v17, Ljava/lang/StringBuilder;

    #@20b
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@20e
    const-string v18, "Failed closing output stream: "

    #@210
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@213
    move-result-object v17

    #@214
    move-object/from16 v0, v17

    #@216
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@219
    move-result-object v17

    #@21a
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21d
    move-result-object v17

    #@21e
    move-object/from16 v0, p0

    #@220
    move-object/from16 v1, v17

    #@222
    invoke-direct {v0, v1}, Lcom/lge/systemservice/service/NativeDaemonConnector;->loge(Ljava/lang/String;)V

    #@225
    goto/16 :goto_f9

    #@227
    .line 186
    .end local v5           #e:Ljava/io/IOException;
    :catchall_227
    move-exception v15

    #@228
    monitor-exit v16
    :try_end_229
    .catchall {:try_start_209 .. :try_end_229} :catchall_227

    #@229
    throw v15

    #@22a
    .line 192
    :catch_22a
    move-exception v7

    #@22b
    .line 193
    .restart local v7       #ex:Ljava/io/IOException;
    new-instance v16, Ljava/lang/StringBuilder;

    #@22d
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@230
    const-string v17, "Failed closing socket: "

    #@232
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@235
    move-result-object v16

    #@236
    move-object/from16 v0, v16

    #@238
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23b
    move-result-object v16

    #@23c
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23f
    move-result-object v16

    #@240
    move-object/from16 v0, p0

    #@242
    move-object/from16 v1, v16

    #@244
    invoke-direct {v0, v1}, Lcom/lge/systemservice/service/NativeDaemonConnector;->loge(Ljava/lang/String;)V

    #@247
    goto/16 :goto_107

    #@249
    .line 172
    .end local v7           #ex:Ljava/io/IOException;
    :catch_249
    move-exception v7

    #@24a
    goto/16 :goto_a7
.end method

.method private log(Ljava/lang/String;)V
    .registers 3
    .parameter "logstring"

    #@0
    .prologue
    .line 466
    iget-object v0, p0, Lcom/lge/systemservice/service/NativeDaemonConnector;->mLocalLog:Landroid/util/LocalLog;

    #@2
    invoke-virtual {v0, p1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    #@5
    .line 467
    return-void
.end method

.method private loge(Ljava/lang/String;)V
    .registers 3
    .parameter "logstring"

    #@0
    .prologue
    .line 470
    iget-object v0, p0, Lcom/lge/systemservice/service/NativeDaemonConnector;->TAG:Ljava/lang/String;

    #@2
    invoke-static {v0, p1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 471
    iget-object v0, p0, Lcom/lge/systemservice/service/NativeDaemonConnector;->mLocalLog:Landroid/util/LocalLog;

    #@7
    invoke-virtual {v0, p1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    #@a
    .line 472
    return-void
.end method

.method private varargs makeCommand(Ljava/lang/StringBuilder;Ljava/lang/String;[Ljava/lang/Object;)V
    .registers 12
    .parameter "builder"
    .parameter "cmd"
    .parameter "args"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lge/systemservice/service/NativeDaemonConnectorException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 204
    invoke-virtual {p2, v6}, Ljava/lang/String;->indexOf(I)I

    #@4
    move-result v5

    #@5
    if-ltz v5, :cond_20

    #@7
    .line 205
    new-instance v5, Ljava/lang/IllegalArgumentException;

    #@9
    new-instance v6, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v7, "unexpected command: "

    #@10
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v6

    #@14
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v6

    #@18
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v6

    #@1c
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v5

    #@20
    .line 208
    :cond_20
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    .line 209
    move-object v2, p3

    #@24
    .local v2, arr$:[Ljava/lang/Object;
    array-length v4, v2

    #@25
    .local v4, len$:I
    const/4 v3, 0x0

    #@26
    .local v3, i$:I
    :goto_26
    if-ge v3, v4, :cond_58

    #@28
    aget-object v0, v2, v3

    #@2a
    .line 210
    .local v0, arg:Ljava/lang/Object;
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@2d
    move-result-object v1

    #@2e
    .line 211
    .local v1, argString:Ljava/lang/String;
    invoke-virtual {v1, v6}, Ljava/lang/String;->indexOf(I)I

    #@31
    move-result v5

    #@32
    if-ltz v5, :cond_4d

    #@34
    .line 212
    new-instance v5, Ljava/lang/IllegalArgumentException;

    #@36
    new-instance v6, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v7, "unexpected argument: "

    #@3d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v6

    #@41
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v6

    #@45
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v6

    #@49
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@4c
    throw v5

    #@4d
    .line 215
    :cond_4d
    const/16 v5, 0x20

    #@4f
    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@52
    .line 216
    invoke-static {p1, v1}, Lcom/lge/systemservice/service/NativeDaemonConnector;->appendEscaped(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    #@55
    .line 209
    add-int/lit8 v3, v3, 0x1

    #@57
    goto :goto_26

    #@58
    .line 218
    .end local v0           #arg:Ljava/lang/Object;
    .end local v1           #argString:Ljava/lang/String;
    :cond_58
    return-void
.end method


# virtual methods
.method public varargs execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/lge/systemservice/service/NativeDaemonEvent;
    .registers 7
    .parameter "cmd"
    .parameter "args"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lge/systemservice/service/NativeDaemonConnectorException;
        }
    .end annotation

    #@0
    .prologue
    .line 244
    invoke-virtual {p0, p1, p2}, Lcom/lge/systemservice/service/NativeDaemonConnector;->executeForList(Ljava/lang/String;[Ljava/lang/Object;)[Lcom/lge/systemservice/service/NativeDaemonEvent;

    #@3
    move-result-object v0

    #@4
    .line 245
    .local v0, events:[Lcom/lge/systemservice/service/NativeDaemonEvent;
    array-length v1, v0

    #@5
    const/4 v2, 0x1

    #@6
    if-eq v1, v2, :cond_22

    #@8
    .line 246
    new-instance v1, Lcom/lge/systemservice/service/NativeDaemonConnectorException;

    #@a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "Expected exactly one response, but received "

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    array-length v3, v0

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-direct {v1, v2}, Lcom/lge/systemservice/service/NativeDaemonConnectorException;-><init>(Ljava/lang/String;)V

    #@21
    throw v1

    #@22
    .line 249
    :cond_22
    const/4 v1, 0x0

    #@23
    aget-object v1, v0, v1

    #@25
    return-object v1
.end method

.method public varargs execute(ILjava/lang/String;[Ljava/lang/Object;)[Lcom/lge/systemservice/service/NativeDaemonEvent;
    .registers 22
    .parameter "timeout"
    .parameter "cmd"
    .parameter "args"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lge/systemservice/service/NativeDaemonConnectorException;
        }
    .end annotation

    #@0
    .prologue
    .line 294
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    #@3
    move-result-object v8

    #@4
    .line 296
    .local v8, events:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/lge/systemservice/service/NativeDaemonEvent;>;"
    move-object/from16 v0, p0

    #@6
    iget-object v14, v0, Lcom/lge/systemservice/service/NativeDaemonConnector;->mSequenceNumber:Ljava/util/concurrent/atomic/AtomicInteger;

    #@8
    invoke-virtual {v14}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    #@b
    move-result v11

    #@c
    .line 297
    .local v11, sequenceNumber:I
    new-instance v14, Ljava/lang/StringBuilder;

    #@e
    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@11
    move-result-object v15

    #@12
    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@15
    const/16 v15, 0x20

    #@17
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    .line 299
    .local v3, cmdBuilder:Ljava/lang/StringBuilder;
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@1e
    move-result-wide v12

    #@1f
    .line 301
    .local v12, startTime:J
    move-object/from16 v0, p0

    #@21
    move-object/from16 v1, p2

    #@23
    move-object/from16 v2, p3

    #@25
    invoke-direct {v0, v3, v1, v2}, Lcom/lge/systemservice/service/NativeDaemonConnector;->makeCommand(Ljava/lang/StringBuilder;Ljava/lang/String;[Ljava/lang/Object;)V

    #@28
    .line 303
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v9

    #@2c
    .line 304
    .local v9, logCmd:Ljava/lang/String;
    new-instance v14, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v15, "SND -> {"

    #@33
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v14

    #@37
    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v14

    #@3b
    const-string v15, "}"

    #@3d
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v14

    #@41
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v14

    #@45
    move-object/from16 v0, p0

    #@47
    invoke-direct {v0, v14}, Lcom/lge/systemservice/service/NativeDaemonConnector;->log(Ljava/lang/String;)V

    #@4a
    .line 306
    const/4 v14, 0x0

    #@4b
    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@4e
    .line 307
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v10

    #@52
    .line 309
    .local v10, sentCmd:Ljava/lang/String;
    move-object/from16 v0, p0

    #@54
    iget-object v15, v0, Lcom/lge/systemservice/service/NativeDaemonConnector;->mDaemonLock:Ljava/lang/Object;

    #@56
    monitor-enter v15

    #@57
    .line 310
    :try_start_57
    move-object/from16 v0, p0

    #@59
    iget-object v14, v0, Lcom/lge/systemservice/service/NativeDaemonConnector;->mOutputStream:Ljava/io/OutputStream;

    #@5b
    if-nez v14, :cond_6a

    #@5d
    .line 311
    new-instance v14, Lcom/lge/systemservice/service/NativeDaemonConnectorException;

    #@5f
    const-string v16, "missing output stream"

    #@61
    move-object/from16 v0, v16

    #@63
    invoke-direct {v14, v0}, Lcom/lge/systemservice/service/NativeDaemonConnectorException;-><init>(Ljava/lang/String;)V

    #@66
    throw v14

    #@67
    .line 319
    :catchall_67
    move-exception v14

    #@68
    monitor-exit v15
    :try_end_69
    .catchall {:try_start_57 .. :try_end_69} :catchall_67

    #@69
    throw v14

    #@6a
    .line 314
    :cond_6a
    :try_start_6a
    move-object/from16 v0, p0

    #@6c
    iget-object v14, v0, Lcom/lge/systemservice/service/NativeDaemonConnector;->mOutputStream:Ljava/io/OutputStream;

    #@6e
    sget-object v16, Ljava/nio/charset/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    #@70
    move-object/from16 v0, v16

    #@72
    invoke-virtual {v10, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    #@75
    move-result-object v16

    #@76
    move-object/from16 v0, v16

    #@78
    invoke-virtual {v14, v0}, Ljava/io/OutputStream;->write([B)V
    :try_end_7b
    .catchall {:try_start_6a .. :try_end_7b} :catchall_67
    .catch Ljava/io/IOException; {:try_start_6a .. :try_end_7b} :catch_a7

    #@7b
    .line 319
    :try_start_7b
    monitor-exit v15
    :try_end_7c
    .catchall {:try_start_7b .. :try_end_7c} :catchall_67

    #@7c
    .line 321
    const/4 v7, 0x0

    #@7d
    .line 323
    .local v7, event:Lcom/lge/systemservice/service/NativeDaemonEvent;
    :cond_7d
    move-object/from16 v0, p0

    #@7f
    iget-object v14, v0, Lcom/lge/systemservice/service/NativeDaemonConnector;->mResponseQueue:Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue;

    #@81
    move/from16 v0, p1

    #@83
    invoke-virtual {v14, v11, v0, v10}, Lcom/lge/systemservice/service/NativeDaemonConnector$ResponseQueue;->remove(IILjava/lang/String;)Lcom/lge/systemservice/service/NativeDaemonEvent;

    #@86
    move-result-object v7

    #@87
    .line 324
    if-nez v7, :cond_b2

    #@89
    .line 325
    new-instance v14, Ljava/lang/StringBuilder;

    #@8b
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@8e
    const-string v15, "timed-out waiting for response to "

    #@90
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v14

    #@94
    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v14

    #@98
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9b
    move-result-object v14

    #@9c
    move-object/from16 v0, p0

    #@9e
    invoke-direct {v0, v14}, Lcom/lge/systemservice/service/NativeDaemonConnector;->loge(Ljava/lang/String;)V

    #@a1
    .line 326
    new-instance v14, Lcom/lge/systemservice/service/NativeDaemonConnector$NativeDaemonFailureException;

    #@a3
    invoke-direct {v14, v9, v7}, Lcom/lge/systemservice/service/NativeDaemonConnector$NativeDaemonFailureException;-><init>(Ljava/lang/String;Lcom/lge/systemservice/service/NativeDaemonEvent;)V

    #@a6
    throw v14

    #@a7
    .line 315
    .end local v7           #event:Lcom/lge/systemservice/service/NativeDaemonEvent;
    :catch_a7
    move-exception v4

    #@a8
    .line 316
    .local v4, e:Ljava/io/IOException;
    :try_start_a8
    new-instance v14, Lcom/lge/systemservice/service/NativeDaemonConnectorException;

    #@aa
    const-string v16, "problem sending command"

    #@ac
    move-object/from16 v0, v16

    #@ae
    invoke-direct {v14, v0, v4}, Lcom/lge/systemservice/service/NativeDaemonConnectorException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@b1
    throw v14
    :try_end_b2
    .catchall {:try_start_a8 .. :try_end_b2} :catchall_67

    #@b2
    .line 328
    .end local v4           #e:Ljava/io/IOException;
    .restart local v7       #event:Lcom/lge/systemservice/service/NativeDaemonEvent;
    :cond_b2
    new-instance v14, Ljava/lang/StringBuilder;

    #@b4
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@b7
    const-string v15, "RMV <- {"

    #@b9
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v14

    #@bd
    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v14

    #@c1
    const-string v15, "}"

    #@c3
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v14

    #@c7
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ca
    move-result-object v14

    #@cb
    move-object/from16 v0, p0

    #@cd
    invoke-direct {v0, v14}, Lcom/lge/systemservice/service/NativeDaemonConnector;->log(Ljava/lang/String;)V

    #@d0
    .line 329
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@d3
    .line 330
    invoke-virtual {v7}, Lcom/lge/systemservice/service/NativeDaemonEvent;->isClassContinue()Z

    #@d6
    move-result v14

    #@d7
    if-nez v14, :cond_7d

    #@d9
    .line 332
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@dc
    move-result-wide v5

    #@dd
    .line 333
    .local v5, endTime:J
    sub-long v14, v5, v12

    #@df
    const-wide/16 v16, 0x1f4

    #@e1
    cmp-long v14, v14, v16

    #@e3
    if-lez v14, :cond_10f

    #@e5
    .line 334
    new-instance v14, Ljava/lang/StringBuilder;

    #@e7
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@ea
    const-string v15, "NDC Command {"

    #@ec
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ef
    move-result-object v14

    #@f0
    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v14

    #@f4
    const-string v15, "} took too long ("

    #@f6
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f9
    move-result-object v14

    #@fa
    sub-long v15, v5, v12

    #@fc
    invoke-virtual/range {v14 .. v16}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v14

    #@100
    const-string v15, "ms)"

    #@102
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@105
    move-result-object v14

    #@106
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@109
    move-result-object v14

    #@10a
    move-object/from16 v0, p0

    #@10c
    invoke-direct {v0, v14}, Lcom/lge/systemservice/service/NativeDaemonConnector;->loge(Ljava/lang/String;)V

    #@10f
    .line 337
    :cond_10f
    invoke-virtual {v7}, Lcom/lge/systemservice/service/NativeDaemonEvent;->isClassClientError()Z

    #@112
    move-result v14

    #@113
    if-eqz v14, :cond_11b

    #@115
    .line 338
    new-instance v14, Lcom/lge/systemservice/service/NativeDaemonConnector$NativeDaemonArgumentException;

    #@117
    invoke-direct {v14, v9, v7}, Lcom/lge/systemservice/service/NativeDaemonConnector$NativeDaemonArgumentException;-><init>(Ljava/lang/String;Lcom/lge/systemservice/service/NativeDaemonEvent;)V

    #@11a
    throw v14

    #@11b
    .line 340
    :cond_11b
    invoke-virtual {v7}, Lcom/lge/systemservice/service/NativeDaemonEvent;->isClassServerError()Z

    #@11e
    move-result v14

    #@11f
    if-eqz v14, :cond_127

    #@121
    .line 341
    new-instance v14, Lcom/lge/systemservice/service/NativeDaemonConnector$NativeDaemonFailureException;

    #@123
    invoke-direct {v14, v9, v7}, Lcom/lge/systemservice/service/NativeDaemonConnector$NativeDaemonFailureException;-><init>(Ljava/lang/String;Lcom/lge/systemservice/service/NativeDaemonEvent;)V

    #@126
    throw v14

    #@127
    .line 344
    :cond_127
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    #@12a
    move-result v14

    #@12b
    new-array v14, v14, [Lcom/lge/systemservice/service/NativeDaemonEvent;

    #@12d
    invoke-virtual {v8, v14}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@130
    move-result-object v14

    #@131
    check-cast v14, [Lcom/lge/systemservice/service/NativeDaemonEvent;

    #@133
    return-object v14
.end method

.method public varargs executeForList(Ljava/lang/String;[Ljava/lang/Object;)[Lcom/lge/systemservice/service/NativeDaemonEvent;
    .registers 4
    .parameter "cmd"
    .parameter "args"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lge/systemservice/service/NativeDaemonConnectorException;
        }
    .end annotation

    #@0
    .prologue
    .line 278
    const v0, 0xea60

    #@3
    invoke-virtual {p0, v0, p1, p2}, Lcom/lge/systemservice/service/NativeDaemonConnector;->execute(ILjava/lang/String;[Ljava/lang/Object;)[Lcom/lge/systemservice/service/NativeDaemonEvent;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .registers 8
    .parameter "msg"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 96
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@3
    check-cast v1, Ljava/lang/String;

    #@5
    .line 98
    .local v1, event:Ljava/lang/String;
    :try_start_5
    iget-object v2, p0, Lcom/lge/systemservice/service/NativeDaemonConnector;->mCallbacks:Lcom/lge/systemservice/service/INativeDaemonConnectorCallbacks;

    #@7
    iget v3, p1, Landroid/os/Message;->what:I

    #@9
    invoke-static {v1}, Lcom/lge/systemservice/service/NativeDaemonEvent;->unescapeArgs(Ljava/lang/String;)[Ljava/lang/String;

    #@c
    move-result-object v4

    #@d
    invoke-interface {v2, v3, v1, v4}, Lcom/lge/systemservice/service/INativeDaemonConnectorCallbacks;->onEvent(ILjava/lang/String;[Ljava/lang/String;)Z

    #@10
    move-result v2

    #@11
    if-nez v2, :cond_22

    #@13
    .line 99
    const-string v2, "Unhandled event \'%s\'"

    #@15
    const/4 v3, 0x1

    #@16
    new-array v3, v3, [Ljava/lang/Object;

    #@18
    const/4 v4, 0x0

    #@19
    aput-object v1, v3, v4

    #@1b
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-direct {p0, v2}, Lcom/lge/systemservice/service/NativeDaemonConnector;->log(Ljava/lang/String;)V
    :try_end_22
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_22} :catch_23

    #@22
    .line 104
    :cond_22
    :goto_22
    return v5

    #@23
    .line 101
    :catch_23
    move-exception v0

    #@24
    .line 102
    .local v0, e:Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v3, "Error handling \'"

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    const-string v3, "\': "

    #@35
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v2

    #@3d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v2

    #@41
    invoke-direct {p0, v2}, Lcom/lge/systemservice/service/NativeDaemonConnector;->loge(Ljava/lang/String;)V

    #@44
    goto :goto_22
.end method

.method public run()V
    .registers 5

    #@0
    .prologue
    .line 80
    new-instance v1, Landroid/os/HandlerThread;

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    iget-object v3, p0, Lcom/lge/systemservice/service/NativeDaemonConnector;->TAG:Ljava/lang/String;

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    const-string v3, ".CallbackHandler"

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@1a
    .line 81
    .local v1, thread:Landroid/os/HandlerThread;
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    #@1d
    .line 82
    new-instance v2, Landroid/os/Handler;

    #@1f
    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@22
    move-result-object v3

    #@23
    invoke-direct {v2, v3, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    #@26
    iput-object v2, p0, Lcom/lge/systemservice/service/NativeDaemonConnector;->mCallbackHandler:Landroid/os/Handler;

    #@28
    .line 86
    :goto_28
    :try_start_28
    invoke-direct {p0}, Lcom/lge/systemservice/service/NativeDaemonConnector;->listenToSocket()V
    :try_end_2b
    .catch Ljava/lang/Exception; {:try_start_28 .. :try_end_2b} :catch_2c

    #@2b
    goto :goto_28

    #@2c
    .line 87
    :catch_2c
    move-exception v0

    #@2d
    .line 88
    .local v0, e:Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v3, "Error in NativeDaemonConnector: "

    #@34
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v2

    #@38
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v2

    #@3c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v2

    #@40
    invoke-direct {p0, v2}, Lcom/lge/systemservice/service/NativeDaemonConnector;->loge(Ljava/lang/String;)V

    #@43
    .line 89
    const-wide/16 v2, 0x1388

    #@45
    invoke-static {v2, v3}, Landroid/os/SystemClock;->sleep(J)V

    #@48
    goto :goto_28
.end method
