.class public Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;
.super Lcom/lge/systemservice/core/logcatchermanager/ILogCatcherManager$Stub;
.source "LogCatcherService.java"


# static fields
.field private static INTENT_CALL_NEW_DEFECT_ACTIVITY:Ljava/lang/String;

.field private static INTENT_COPYLOGS_COMPLETED:Ljava/lang/String;

.field private static INTENT_COPYLOGS_FAILED:Ljava/lang/String;

.field private static INTENT_LOGCATCHER_COPYLOGS_COMPLETED:Ljava/lang/String;

.field private static INTENT_LOGCATCHER_COPYLOGS_FAILED:Ljava/lang/String;

.field private static INTENT_LOGCATCHER_QUICKDUMP_COMPLETED:Ljava/lang/String;

.field private static INTENT_LOGCATCHER_QUICKDUMP_FAILED:Ljava/lang/String;

.field private static INTENT_NOTIFY_QUICKDUMP_FAILED:Ljava/lang/String;

.field private static INTENT_QUICKDUMP_COMPLETED:Ljava/lang/String;

.field private static INTENT_QUICKDUMP_FAILED:Ljava/lang/String;

.field private static SERVICE_NAME_COPYLOGS_TO_EXTERNAL:Ljava/lang/String;

.field private static SERVICE_NAME_COPYLOGS_TO_INTERNAL:Ljava/lang/String;

.field private static SERVICE_NAME_QUICKDUMP:Ljava/lang/String;

.field private static TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mIntentReceiver:Landroid/content/BroadcastReceiver;

.field private m_Is_triggered_by_QuickDumpAPI:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 22
    const-string v0, "LogCatcherService"

    #@2
    sput-object v0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->TAG:Ljava/lang/String;

    #@4
    .line 25
    const-string v0, "quickdump"

    #@6
    sput-object v0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->SERVICE_NAME_QUICKDUMP:Ljava/lang/String;

    #@8
    .line 26
    const-string v0, "digicl_internal"

    #@a
    sput-object v0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->SERVICE_NAME_COPYLOGS_TO_INTERNAL:Ljava/lang/String;

    #@c
    .line 27
    const-string v0, "digicl_external"

    #@e
    sput-object v0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->SERVICE_NAME_COPYLOGS_TO_EXTERNAL:Ljava/lang/String;

    #@10
    .line 30
    const-string v0, "com.lge.android.quickdump.intent.QUICKDUMP_COMPLETED"

    #@12
    sput-object v0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->INTENT_QUICKDUMP_COMPLETED:Ljava/lang/String;

    #@14
    .line 31
    const-string v0, "com.lge.android.quickdump.intent.QUICKDUMP_FAILED"

    #@16
    sput-object v0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->INTENT_QUICKDUMP_FAILED:Ljava/lang/String;

    #@18
    .line 32
    const-string v0, "com.lge.android.digicl.intent.COPYLOGS_COMPLETED"

    #@1a
    sput-object v0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->INTENT_COPYLOGS_COMPLETED:Ljava/lang/String;

    #@1c
    .line 33
    const-string v0, "com.lge.android.digicl.intent.COPYLOGS_FAILED"

    #@1e
    sput-object v0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->INTENT_COPYLOGS_FAILED:Ljava/lang/String;

    #@20
    .line 36
    const-string v0, "com.lge.android.logcatcher.intent.QUICKDUMP_COMPLETED"

    #@22
    sput-object v0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->INTENT_LOGCATCHER_QUICKDUMP_COMPLETED:Ljava/lang/String;

    #@24
    .line 37
    const-string v0, "com.lge.android.logcatcher.intent.QUICKDUMP_FAILED"

    #@26
    sput-object v0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->INTENT_LOGCATCHER_QUICKDUMP_FAILED:Ljava/lang/String;

    #@28
    .line 38
    const-string v0, "com.lge.android.logcatcher.intent.COPYLOGS_COMPLETED"

    #@2a
    sput-object v0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->INTENT_LOGCATCHER_COPYLOGS_COMPLETED:Ljava/lang/String;

    #@2c
    .line 39
    const-string v0, "com.lge.android.logcatcher.intent.COPYLOGS_FAILED"

    #@2e
    sput-object v0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->INTENT_LOGCATCHER_COPYLOGS_FAILED:Ljava/lang/String;

    #@30
    .line 42
    const-string v0, "com.lge.android.logcatcher.intent.action.NOTIFY_QUICKDUMP_FAILED"

    #@32
    sput-object v0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->INTENT_NOTIFY_QUICKDUMP_FAILED:Ljava/lang/String;

    #@34
    .line 43
    const-string v0, "com.lge.android.logcatcher.intent.action.CALL_NEW_DEFECT_ACTIVITY"

    #@36
    sput-object v0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->INTENT_CALL_NEW_DEFECT_ACTIVITY:Ljava/lang/String;

    #@38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 49
    invoke-direct {p0}, Lcom/lge/systemservice/core/logcatchermanager/ILogCatcherManager$Stub;-><init>()V

    #@3
    .line 46
    const/4 v1, 0x0

    #@4
    iput-object v1, p0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->mContext:Landroid/content/Context;

    #@6
    .line 47
    const/4 v1, 0x0

    #@7
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@a
    move-result-object v1

    #@b
    iput-object v1, p0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->m_Is_triggered_by_QuickDumpAPI:Ljava/lang/Boolean;

    #@d
    .line 60
    new-instance v1, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService$1;

    #@f
    invoke-direct {v1, p0}, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService$1;-><init>(Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;)V

    #@12
    iput-object v1, p0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@14
    .line 50
    iput-object p1, p0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->mContext:Landroid/content/Context;

    #@16
    .line 52
    new-instance v0, Landroid/content/IntentFilter;

    #@18
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@1b
    .line 53
    .local v0, filter:Landroid/content/IntentFilter;
    sget-object v1, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->INTENT_QUICKDUMP_COMPLETED:Ljava/lang/String;

    #@1d
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@20
    .line 54
    sget-object v1, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->INTENT_QUICKDUMP_FAILED:Ljava/lang/String;

    #@22
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@25
    .line 55
    sget-object v1, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->INTENT_COPYLOGS_COMPLETED:Ljava/lang/String;

    #@27
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@2a
    .line 56
    sget-object v1, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->INTENT_COPYLOGS_FAILED:Ljava/lang/String;

    #@2c
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@2f
    .line 57
    iget-object v1, p0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->mContext:Landroid/content/Context;

    #@31
    iget-object v2, p0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@33
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@36
    .line 58
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 21
    sget-object v0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->TAG:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 21
    sget-object v0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->INTENT_QUICKDUMP_COMPLETED:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$1000()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 21
    sget-object v0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->INTENT_NOTIFY_QUICKDUMP_FAILED:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$1100()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 21
    sget-object v0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->INTENT_LOGCATCHER_COPYLOGS_COMPLETED:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$1200()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 21
    sget-object v0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->INTENT_LOGCATCHER_COPYLOGS_FAILED:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;)Ljava/lang/Boolean;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 21
    iget-object v0, p0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->m_Is_triggered_by_QuickDumpAPI:Ljava/lang/Boolean;

    #@2
    return-object v0
.end method

.method static synthetic access$202(Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 21
    iput-object p1, p0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->m_Is_triggered_by_QuickDumpAPI:Ljava/lang/Boolean;

    #@2
    return-object p1
.end method

.method static synthetic access$300()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 21
    sget-object v0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->INTENT_QUICKDUMP_FAILED:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$400()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 21
    sget-object v0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->INTENT_COPYLOGS_COMPLETED:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$500()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 21
    sget-object v0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->INTENT_COPYLOGS_FAILED:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$600()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 21
    sget-object v0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->INTENT_LOGCATCHER_QUICKDUMP_COMPLETED:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 21
    iget-object v0, p0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$800()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 21
    sget-object v0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->INTENT_LOGCATCHER_QUICKDUMP_FAILED:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$900()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 21
    sget-object v0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->INTENT_CALL_NEW_DEFECT_ACTIVITY:Ljava/lang/String;

    #@2
    return-object v0
.end method


# virtual methods
.method public cancelCopyLogsToExternalStorage()I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 250
    const-string v0, "ctl.stop"

    #@2
    sget-object v1, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->SERVICE_NAME_COPYLOGS_TO_EXTERNAL:Ljava/lang/String;

    #@4
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 251
    sget-object v0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->TAG:Ljava/lang/String;

    #@9
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v2, "Stop "

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    sget-object v2, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->SERVICE_NAME_COPYLOGS_TO_EXTERNAL:Ljava/lang/String;

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    const-string v2, " service : "

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    new-instance v2, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v3, "init.svc."

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    sget-object v3, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->SERVICE_NAME_COPYLOGS_TO_EXTERNAL:Ljava/lang/String;

    #@2d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v2

    #@35
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v1

    #@41
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 252
    const/4 v0, 0x0

    #@45
    return v0
.end method

.method public cancelCopyLogsToInternalStorage()I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 243
    const-string v0, "ctl.stop"

    #@2
    sget-object v1, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->SERVICE_NAME_COPYLOGS_TO_INTERNAL:Ljava/lang/String;

    #@4
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 244
    sget-object v0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->TAG:Ljava/lang/String;

    #@9
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v2, "Stop "

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    sget-object v2, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->SERVICE_NAME_COPYLOGS_TO_INTERNAL:Ljava/lang/String;

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    const-string v2, " service : "

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    new-instance v2, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v3, "init.svc."

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    sget-object v3, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->SERVICE_NAME_COPYLOGS_TO_INTERNAL:Ljava/lang/String;

    #@2d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v2

    #@35
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v1

    #@41
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 245
    const/4 v0, 0x0

    #@45
    return v0
.end method

.method public cancelQuickDump()I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 236
    const-string v0, "ctl.stop"

    #@2
    sget-object v1, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->SERVICE_NAME_QUICKDUMP:Ljava/lang/String;

    #@4
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 237
    sget-object v0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->TAG:Ljava/lang/String;

    #@9
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v2, "Stop "

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    sget-object v2, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->SERVICE_NAME_QUICKDUMP:Ljava/lang/String;

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    const-string v2, " service : "

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    new-instance v2, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v3, "init.svc."

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    sget-object v3, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->SERVICE_NAME_QUICKDUMP:Ljava/lang/String;

    #@2d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v2

    #@35
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v1

    #@41
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 238
    const/4 v0, 0x0

    #@45
    return v0
.end method

.method public copyLogsToExternalStorage()I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 203
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "init.svc."

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    sget-object v2, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->SERVICE_NAME_COPYLOGS_TO_EXTERNAL:Ljava/lang/String;

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    const-string v2, ""

    #@17
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    .line 205
    .local v0, stateDigiclService:Ljava/lang/String;
    const-string v1, "stopped"

    #@1d
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@20
    move-result v1

    #@21
    if-nez v1, :cond_2b

    #@23
    const-string v1, ""

    #@25
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28
    move-result v1

    #@29
    if-eqz v1, :cond_34

    #@2b
    .line 206
    :cond_2b
    const-string v1, "ctl.start"

    #@2d
    sget-object v2, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->SERVICE_NAME_COPYLOGS_TO_EXTERNAL:Ljava/lang/String;

    #@2f
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@32
    .line 207
    const/4 v1, 0x1

    #@33
    .line 214
    :goto_33
    return v1

    #@34
    .line 208
    :cond_34
    const-string v1, "running"

    #@36
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@39
    move-result v1

    #@3a
    if-eqz v1, :cond_58

    #@3c
    .line 209
    sget-object v1, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->TAG:Ljava/lang/String;

    #@3e
    new-instance v2, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    sget-object v3, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->SERVICE_NAME_COPYLOGS_TO_EXTERNAL:Ljava/lang/String;

    #@45
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v2

    #@49
    const-string v3, " is already running."

    #@4b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v2

    #@4f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v2

    #@53
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    .line 210
    const/4 v1, 0x0

    #@57
    goto :goto_33

    #@58
    .line 212
    :cond_58
    sget-object v1, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->TAG:Ljava/lang/String;

    #@5a
    new-instance v2, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string v3, "Cannot copy logs to external storage. Is "

    #@61
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v2

    #@65
    sget-object v3, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->SERVICE_NAME_COPYLOGS_TO_EXTERNAL:Ljava/lang/String;

    #@67
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v2

    #@6b
    const-string v3, " declared in init.*.rc file?"

    #@6d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v2

    #@71
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v2

    #@75
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@78
    .line 214
    const/4 v1, -0x1

    #@79
    goto :goto_33
.end method

.method public copyLogsToInternalStorage()I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 187
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "init.svc."

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    sget-object v2, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->SERVICE_NAME_COPYLOGS_TO_INTERNAL:Ljava/lang/String;

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    const-string v2, ""

    #@17
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    .line 189
    .local v0, stateDigiclService:Ljava/lang/String;
    const-string v1, "stopped"

    #@1d
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@20
    move-result v1

    #@21
    if-nez v1, :cond_2b

    #@23
    const-string v1, ""

    #@25
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28
    move-result v1

    #@29
    if-eqz v1, :cond_34

    #@2b
    .line 190
    :cond_2b
    const-string v1, "ctl.start"

    #@2d
    sget-object v2, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->SERVICE_NAME_COPYLOGS_TO_INTERNAL:Ljava/lang/String;

    #@2f
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@32
    .line 191
    const/4 v1, 0x1

    #@33
    .line 198
    :goto_33
    return v1

    #@34
    .line 192
    :cond_34
    const-string v1, "running"

    #@36
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@39
    move-result v1

    #@3a
    if-eqz v1, :cond_58

    #@3c
    .line 193
    sget-object v1, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->TAG:Ljava/lang/String;

    #@3e
    new-instance v2, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    sget-object v3, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->SERVICE_NAME_COPYLOGS_TO_INTERNAL:Ljava/lang/String;

    #@45
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v2

    #@49
    const-string v3, " is already running."

    #@4b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v2

    #@4f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v2

    #@53
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    .line 194
    const/4 v1, 0x0

    #@57
    goto :goto_33

    #@58
    .line 196
    :cond_58
    sget-object v1, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->TAG:Ljava/lang/String;

    #@5a
    new-instance v2, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string v3, "Cannot copy logs to internal storage. Is "

    #@61
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v2

    #@65
    sget-object v3, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->SERVICE_NAME_COPYLOGS_TO_INTERNAL:Ljava/lang/String;

    #@67
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v2

    #@6b
    const-string v3, " declared in init.*.rc file?"

    #@6d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v2

    #@71
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v2

    #@75
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@78
    .line 198
    const/4 v1, -0x1

    #@79
    goto :goto_33
.end method

.method public startQuickDump()I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 219
    new-instance v2, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v3, "init.svc."

    #@8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v2

    #@c
    sget-object v3, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->SERVICE_NAME_QUICKDUMP:Ljava/lang/String;

    #@e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    const-string v3, ""

    #@18
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    .line 221
    .local v0, stateQuickDumpService:Ljava/lang/String;
    const-string v2, "stopped"

    #@1e
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21
    move-result v2

    #@22
    if-nez v2, :cond_2c

    #@24
    const-string v2, ""

    #@26
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@29
    move-result v2

    #@2a
    if-eqz v2, :cond_3a

    #@2c
    .line 222
    :cond_2c
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@2f
    move-result-object v2

    #@30
    iput-object v2, p0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->m_Is_triggered_by_QuickDumpAPI:Ljava/lang/Boolean;

    #@32
    .line 223
    const-string v2, "ctl.start"

    #@34
    sget-object v3, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->SERVICE_NAME_QUICKDUMP:Ljava/lang/String;

    #@36
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@39
    .line 231
    :goto_39
    return v1

    #@3a
    .line 225
    :cond_3a
    const-string v1, "running"

    #@3c
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3f
    move-result v1

    #@40
    if-eqz v1, :cond_5e

    #@42
    .line 226
    sget-object v1, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->TAG:Ljava/lang/String;

    #@44
    new-instance v2, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    sget-object v3, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->SERVICE_NAME_QUICKDUMP:Ljava/lang/String;

    #@4b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v2

    #@4f
    const-string v3, " is already running"

    #@51
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v2

    #@55
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v2

    #@59
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@5c
    .line 227
    const/4 v1, 0x0

    #@5d
    goto :goto_39

    #@5e
    .line 229
    :cond_5e
    sget-object v1, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->TAG:Ljava/lang/String;

    #@60
    new-instance v2, Ljava/lang/StringBuilder;

    #@62
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@65
    const-string v3, "Cannot start QuickDump. Is "

    #@67
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v2

    #@6b
    sget-object v3, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->SERVICE_NAME_QUICKDUMP:Ljava/lang/String;

    #@6d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v2

    #@71
    const-string v3, " declared in init.*.rc file?"

    #@73
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v2

    #@77
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7a
    move-result-object v2

    #@7b
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7e
    .line 231
    const/4 v1, -0x1

    #@7f
    goto :goto_39
.end method
