.class Lcom/lge/systemservice/service/CliptrayService$mCueBtnView;
.super Landroid/widget/ImageView;
.source "CliptrayService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/CliptrayService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "mCueBtnView"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/CliptrayService;


# direct methods
.method public constructor <init>(Lcom/lge/systemservice/service/CliptrayService;Landroid/content/Context;)V
    .registers 3
    .parameter
    .parameter "context"

    #@0
    .prologue
    .line 567
    iput-object p1, p0, Lcom/lge/systemservice/service/CliptrayService$mCueBtnView;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@2
    .line 568
    invoke-direct {p0, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    #@5
    .line 569
    return-void
.end method


# virtual methods
.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 5
    .parameter "newConfig"

    #@0
    .prologue
    .line 572
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$mCueBtnView;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@2
    invoke-static {v0}, Lcom/lge/systemservice/service/CliptrayService;->access$1500(Lcom/lge/systemservice/service/CliptrayService;)I

    #@5
    move-result v0

    #@6
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    #@8
    if-eq v0, v1, :cond_50

    #@a
    .line 573
    const-string v0, "CliptrayService"

    #@c
    new-instance v1, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v2, "configuration is changed. newconfig="

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {p1}, Landroid/content/res/Configuration;->toString()Ljava/lang/String;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 574
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$mCueBtnView;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@28
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    #@2a
    invoke-static {v0, v1}, Lcom/lge/systemservice/service/CliptrayService;->access$1502(Lcom/lge/systemservice/service/CliptrayService;I)I

    #@2d
    .line 575
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$mCueBtnView;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@2f
    invoke-static {v0}, Lcom/lge/systemservice/service/CliptrayService;->access$200(Lcom/lge/systemservice/service/CliptrayService;)Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;

    #@32
    move-result-object v0

    #@33
    invoke-static {v0}, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->access$1600(Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;)I

    #@36
    move-result v0

    #@37
    const/4 v1, 0x2

    #@38
    if-eq v0, v1, :cond_47

    #@3a
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$mCueBtnView;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@3c
    invoke-static {v0}, Lcom/lge/systemservice/service/CliptrayService;->access$200(Lcom/lge/systemservice/service/CliptrayService;)Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;

    #@3f
    move-result-object v0

    #@40
    invoke-static {v0}, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->access$1600(Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;)I

    #@43
    move-result v0

    #@44
    const/4 v1, 0x3

    #@45
    if-ne v0, v1, :cond_50

    #@47
    .line 577
    :cond_47
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$mCueBtnView;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@49
    invoke-static {v0}, Lcom/lge/systemservice/service/CliptrayService;->access$200(Lcom/lge/systemservice/service/CliptrayService;)Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;

    #@4c
    move-result-object v0

    #@4d
    invoke-virtual {v0}, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->hideCliptraycue()V

    #@50
    .line 580
    :cond_50
    return-void
.end method
