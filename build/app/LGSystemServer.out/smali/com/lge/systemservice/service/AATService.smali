.class public Lcom/lge/systemservice/service/AATService;
.super Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;
.source "AATService.java"


# static fields
.field private static CARKITFILE:Ljava/lang/String;

.field private static ChargingLedBlueFilePath:Ljava/lang/String;

.field private static ChargingLedGreenFilePath:Ljava/lang/String;

.field private static ChargingLedRedFilePath:Ljava/lang/String;

.field private static HALLDEVICEFILE:Ljava/lang/String;

.field private static HALLDEVICEPENINOUT:Ljava/lang/String;

.field private static OTGFilePath:Ljava/lang/String;

.field private static ledon:Ljava/lang/Thread;

.field private static mIsAdapterEnabled:Z

.field private static model_name:Ljava/lang/String;

.field private static product_device:Ljava/lang/String;

.field private static product_name:Ljava/lang/String;


# instance fields
.field final BACK_LED_1:Ljava/lang/String;

.field final BACK_LED_2:Ljava/lang/String;

.field private GPS_POSITION_MODE_AAT:I

.field private Led_Timeout:I

.field private MISC_BLK_SIZE:I

.field final MISC_PATH:Ljava/lang/String;

.field final TOUCH_LED_LEVEL_MAX:Ljava/lang/String;

.field final TOUCH_LED_LEVEL_MIN:Ljava/lang/String;

.field final TOUCH_LED_LEVEL_PATH:Ljava/lang/String;

.field private animation_scale:F

.field private disableVendorApkCheckFilePath:Ljava/lang/String;

.field private disableVendorApkListTmus:[Ljava/lang/String;

.field private felicaManagerLegacy:Lcom/lge/systemservice/core/felicamanager/FeliCaManager;

.field handler:Landroid/os/Handler;

.field private isRunning:Z

.field private mAudioManager:Landroid/media/AudioManager;

.field private mContext:Landroid/content/Context;

.field private mDefaultLcdBrightValue:Ljava/lang/String;

.field private mDoubleTapSetting:Z

.field private mFmRadioMgr:Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;

.field private mLocationManager:Landroid/location/LocationManager;

.field private mMaxVoiceCallVolume:I

.field private mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

.field private mSaveMusicVolume:I

.field private mSaveRingVolume:I

.field private mSaveVoiceCallVolume:I

.field private mUseLog:Z

.field mWindowManager:Landroid/view/IWindowManager;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 115
    sput-object v1, Lcom/lge/systemservice/service/AATService;->ledon:Ljava/lang/Thread;

    #@3
    .line 118
    const-string v0, ""

    #@5
    sput-object v0, Lcom/lge/systemservice/service/AATService;->ChargingLedRedFilePath:Ljava/lang/String;

    #@7
    .line 119
    const-string v0, ""

    #@9
    sput-object v0, Lcom/lge/systemservice/service/AATService;->ChargingLedGreenFilePath:Ljava/lang/String;

    #@b
    .line 120
    const-string v0, ""

    #@d
    sput-object v0, Lcom/lge/systemservice/service/AATService;->ChargingLedBlueFilePath:Ljava/lang/String;

    #@f
    .line 137
    const-string v0, "/sys/class/android_usb/android0/otg"

    #@11
    sput-object v0, Lcom/lge/systemservice/service/AATService;->OTGFilePath:Ljava/lang/String;

    #@13
    .line 140
    const-string v0, ""

    #@15
    sput-object v0, Lcom/lge/systemservice/service/AATService;->HALLDEVICEFILE:Ljava/lang/String;

    #@17
    .line 141
    const-string v0, ""

    #@19
    sput-object v0, Lcom/lge/systemservice/service/AATService;->CARKITFILE:Ljava/lang/String;

    #@1b
    .line 142
    const-string v0, ""

    #@1d
    sput-object v0, Lcom/lge/systemservice/service/AATService;->HALLDEVICEPENINOUT:Ljava/lang/String;

    #@1f
    .line 158
    const/4 v0, 0x0

    #@20
    sput-boolean v0, Lcom/lge/systemservice/service/AATService;->mIsAdapterEnabled:Z

    #@22
    .line 179
    sput-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@24
    .line 180
    sput-object v1, Lcom/lge/systemservice/service/AATService;->product_name:Ljava/lang/String;

    #@26
    .line 181
    sput-object v1, Lcom/lge/systemservice/service/AATService;->product_device:Ljava/lang/String;

    #@28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 6
    .parameter

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    const/4 v1, 0x0

    #@3
    .line 191
    invoke-direct {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;-><init>()V

    #@6
    .line 97
    const-string v0, "/dev/block/platform/msm_sdcc.1/by-name/misc"

    #@8
    iput-object v0, p0, Lcom/lge/systemservice/service/AATService;->MISC_PATH:Ljava/lang/String;

    #@a
    .line 98
    const/16 v0, 0x800

    #@c
    iput v0, p0, Lcom/lge/systemservice/service/AATService;->MISC_BLK_SIZE:I

    #@e
    .line 100
    const-string v0, "/sys/class/leds/button-backlight/brightness"

    #@10
    iput-object v0, p0, Lcom/lge/systemservice/service/AATService;->TOUCH_LED_LEVEL_PATH:Ljava/lang/String;

    #@12
    .line 101
    const-string v0, "15"

    #@14
    iput-object v0, p0, Lcom/lge/systemservice/service/AATService;->TOUCH_LED_LEVEL_MAX:Ljava/lang/String;

    #@16
    .line 102
    const-string v0, "0"

    #@18
    iput-object v0, p0, Lcom/lge/systemservice/service/AATService;->TOUCH_LED_LEVEL_MIN:Ljava/lang/String;

    #@1a
    .line 103
    iput-boolean v2, p0, Lcom/lge/systemservice/service/AATService;->isRunning:Z

    #@1c
    .line 104
    iput-boolean v3, p0, Lcom/lge/systemservice/service/AATService;->mUseLog:Z

    #@1e
    .line 105
    iput v2, p0, Lcom/lge/systemservice/service/AATService;->Led_Timeout:I

    #@20
    .line 106
    const-string v0, ""

    #@22
    iput-object v0, p0, Lcom/lge/systemservice/service/AATService;->mDefaultLcdBrightValue:Ljava/lang/String;

    #@24
    .line 108
    const-string v0, "/sys/class/leds/button-backlight1/brightness"

    #@26
    iput-object v0, p0, Lcom/lge/systemservice/service/AATService;->BACK_LED_1:Ljava/lang/String;

    #@28
    .line 109
    const-string v0, "/sys/class/leds/button-backlight2/brightness"

    #@2a
    iput-object v0, p0, Lcom/lge/systemservice/service/AATService;->BACK_LED_2:Ljava/lang/String;

    #@2c
    .line 112
    iput-object v1, p0, Lcom/lge/systemservice/service/AATService;->mContext:Landroid/content/Context;

    #@2e
    .line 114
    iput-object v1, p0, Lcom/lge/systemservice/service/AATService;->handler:Landroid/os/Handler;

    #@30
    .line 156
    iput-object v1, p0, Lcom/lge/systemservice/service/AATService;->mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@32
    .line 157
    iput-object v1, p0, Lcom/lge/systemservice/service/AATService;->felicaManagerLegacy:Lcom/lge/systemservice/core/felicamanager/FeliCaManager;

    #@34
    .line 161
    const/16 v0, 0xa

    #@36
    iput v0, p0, Lcom/lge/systemservice/service/AATService;->GPS_POSITION_MODE_AAT:I

    #@38
    .line 166
    const-string v0, "window"

    #@3a
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@3d
    move-result-object v0

    #@3e
    invoke-static {v0}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    #@41
    move-result-object v0

    #@42
    iput-object v0, p0, Lcom/lge/systemservice/service/AATService;->mWindowManager:Landroid/view/IWindowManager;

    #@44
    .line 173
    iput-object v1, p0, Lcom/lge/systemservice/service/AATService;->mFmRadioMgr:Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;

    #@46
    .line 185
    const/4 v0, 0x2

    #@47
    new-array v0, v0, [Ljava/lang/String;

    #@49
    const-string v1, "com.tmobile.pr.mytmobile"

    #@4b
    aput-object v1, v0, v2

    #@4d
    const-string v1, "com.tmobile.vvm.application"

    #@4f
    aput-object v1, v0, v3

    #@51
    iput-object v0, p0, Lcom/lge/systemservice/service/AATService;->disableVendorApkListTmus:[Ljava/lang/String;

    #@53
    .line 189
    const-string v0, "/data/system/disabled_apps"

    #@55
    iput-object v0, p0, Lcom/lge/systemservice/service/AATService;->disableVendorApkCheckFilePath:Ljava/lang/String;

    #@57
    .line 192
    const-string v0, "AAT"

    #@59
    const-string v1, "[AATService] AATService is working!"

    #@5b
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    .line 193
    iput-object p1, p0, Lcom/lge/systemservice/service/AATService;->mContext:Landroid/content/Context;

    #@60
    .line 196
    iget-object v0, p0, Lcom/lge/systemservice/service/AATService;->mLocationManager:Landroid/location/LocationManager;

    #@62
    if-nez v0, :cond_70

    #@64
    .line 198
    iget-object v0, p0, Lcom/lge/systemservice/service/AATService;->mContext:Landroid/content/Context;

    #@66
    const-string v1, "location"

    #@68
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@6b
    move-result-object v0

    #@6c
    check-cast v0, Landroid/location/LocationManager;

    #@6e
    iput-object v0, p0, Lcom/lge/systemservice/service/AATService;->mLocationManager:Landroid/location/LocationManager;

    #@70
    .line 203
    :cond_70
    iget-object v0, p0, Lcom/lge/systemservice/service/AATService;->mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@72
    if-nez v0, :cond_89

    #@74
    .line 205
    new-instance v0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@76
    iget-object v1, p0, Lcom/lge/systemservice/service/AATService;->mContext:Landroid/content/Context;

    #@78
    invoke-direct {v0, v1}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;-><init>(Landroid/content/Context;)V

    #@7b
    iput-object v0, p0, Lcom/lge/systemservice/service/AATService;->mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@7d
    .line 206
    iget-object v0, p0, Lcom/lge/systemservice/service/AATService;->mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@7f
    if-eqz v0, :cond_89

    #@81
    .line 208
    iget-object v0, p0, Lcom/lge/systemservice/service/AATService;->mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@83
    invoke-virtual {v0}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;->createNfcFactoryObj()Z

    #@86
    move-result v0

    #@87
    sput-boolean v0, Lcom/lge/systemservice/service/AATService;->mIsAdapterEnabled:Z

    #@89
    .line 216
    :cond_89
    iget-object v0, p0, Lcom/lge/systemservice/service/AATService;->felicaManagerLegacy:Lcom/lge/systemservice/core/felicamanager/FeliCaManager;

    #@8b
    if-nez v0, :cond_9e

    #@8d
    .line 218
    new-instance v0, Lcom/lge/systemservice/core/LGContextImpl;

    #@8f
    iget-object v1, p0, Lcom/lge/systemservice/service/AATService;->mContext:Landroid/content/Context;

    #@91
    invoke-direct {v0, v1}, Lcom/lge/systemservice/core/LGContextImpl;-><init>(Landroid/content/Context;)V

    #@94
    .line 219
    sget-object v1, Lcom/lge/systemservice/core/LGContext$ServiceList;->FELICA_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@96
    invoke-virtual {v0, v1}, Lcom/lge/systemservice/core/LGContext;->getLegacyService(Lcom/lge/systemservice/core/LGContext$ServiceList;)Ljava/lang/Object;

    #@99
    move-result-object v0

    #@9a
    check-cast v0, Lcom/lge/systemservice/core/felicamanager/FeliCaManager;

    #@9c
    iput-object v0, p0, Lcom/lge/systemservice/service/AATService;->felicaManagerLegacy:Lcom/lge/systemservice/core/felicamanager/FeliCaManager;

    #@9e
    .line 224
    :cond_9e
    iget-object v0, p0, Lcom/lge/systemservice/service/AATService;->mFmRadioMgr:Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;

    #@a0
    if-nez v0, :cond_ab

    #@a2
    .line 225
    new-instance v0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;

    #@a4
    iget-object v1, p0, Lcom/lge/systemservice/service/AATService;->mContext:Landroid/content/Context;

    #@a6
    invoke-direct {v0, v1}, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;-><init>(Landroid/content/Context;)V

    #@a9
    iput-object v0, p0, Lcom/lge/systemservice/service/AATService;->mFmRadioMgr:Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;

    #@ab
    .line 229
    :cond_ab
    return-void
.end method

.method private AATAnimationScale(Z)V
    .registers 6
    .parameter

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 497
    .line 498
    const-string v0, "AAT"

    #@3
    new-instance v1, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v2, "[AATService] AATAnimationScale = "

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    iget v2, p0, Lcom/lge/systemservice/service/AATService;->animation_scale:F

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 499
    if-eqz p1, :cond_57

    #@1d
    .line 501
    iget-object v0, p0, Lcom/lge/systemservice/service/AATService;->mContext:Landroid/content/Context;

    #@1f
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@22
    move-result-object v0

    #@23
    const-string v1, "transition_animation_scale"

    #@25
    iget v2, p0, Lcom/lge/systemservice/service/AATService;->animation_scale:F

    #@27
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)Z

    #@2a
    .line 503
    :try_start_2a
    iget-object v0, p0, Lcom/lge/systemservice/service/AATService;->mWindowManager:Landroid/view/IWindowManager;

    #@2c
    const/4 v1, 0x1

    #@2d
    iget v2, p0, Lcom/lge/systemservice/service/AATService;->animation_scale:F

    #@2f
    invoke-interface {v0, v1, v2}, Landroid/view/IWindowManager;->setAnimationScale(IF)V
    :try_end_32
    .catch Landroid/os/RemoteException; {:try_start_2a .. :try_end_32} :catch_6c

    #@32
    .line 513
    :goto_32
    iget-object v0, p0, Lcom/lge/systemservice/service/AATService;->mContext:Landroid/content/Context;

    #@34
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@37
    move-result-object v0

    #@38
    const-string v1, "transition_animation_scale"

    #@3a
    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    #@3d
    move-result v0

    #@3e
    .line 514
    const-string v1, "AAT"

    #@40
    new-instance v2, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v3, "[AATService] TRANSITION_ANIMATION_SCALE = "

    #@47
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v0

    #@4f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v0

    #@53
    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    .line 515
    return-void

    #@57
    .line 508
    :cond_57
    iget-object v0, p0, Lcom/lge/systemservice/service/AATService;->mContext:Landroid/content/Context;

    #@59
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5c
    move-result-object v0

    #@5d
    const-string v1, "transition_animation_scale"

    #@5f
    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)Z

    #@62
    .line 510
    :try_start_62
    iget-object v0, p0, Lcom/lge/systemservice/service/AATService;->mWindowManager:Landroid/view/IWindowManager;

    #@64
    const/4 v1, 0x1

    #@65
    const/4 v2, 0x0

    #@66
    invoke-interface {v0, v1, v2}, Landroid/view/IWindowManager;->setAnimationScale(IF)V
    :try_end_69
    .catch Landroid/os/RemoteException; {:try_start_62 .. :try_end_69} :catch_6a

    #@69
    goto :goto_32

    #@6a
    .line 511
    :catch_6a
    move-exception v0

    #@6b
    goto :goto_32

    #@6c
    .line 504
    :catch_6c
    move-exception v0

    #@6d
    goto :goto_32
.end method

.method private AccessMiscRead(I)Ljava/lang/String;
    .registers 10
    .parameter

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 320
    iget-boolean v0, p0, Lcom/lge/systemservice/service/AATService;->mUseLog:Z

    #@3
    if-eqz v0, :cond_1d

    #@5
    .line 322
    const-string v0, "AAT"

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v3, "[AATService] AccessMiscRead, index : "

    #@e
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 327
    :cond_1d
    new-instance v3, Ljava/io/File;

    #@1f
    const-string v0, "/dev/block/platform/msm_sdcc.1/by-name/misc"

    #@21
    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@24
    .line 329
    iget v0, p0, Lcom/lge/systemservice/service/AATService;->MISC_BLK_SIZE:I

    #@26
    mul-int v4, v0, p1

    #@28
    .line 334
    :try_start_28
    new-instance v1, Ljava/io/RandomAccessFile;

    #@2a
    const-string v0, "rw"

    #@2c
    invoke-direct {v1, v3, v0}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_2f
    .catchall {:try_start_28 .. :try_end_2f} :catchall_93
    .catch Ljava/lang/Exception; {:try_start_28 .. :try_end_2f} :catch_6d

    #@2f
    .line 336
    if-eqz v1, :cond_45

    #@31
    .line 338
    const/16 v0, 0x14

    #@33
    if-eq p1, v0, :cond_39

    #@35
    const/16 v0, 0x1f

    #@37
    if-ne p1, v0, :cond_6a

    #@39
    :cond_39
    const/16 v0, 0x80

    #@3b
    :goto_3b
    add-int/2addr v0, v4

    #@3c
    int-to-long v5, v0

    #@3d
    :try_start_3d
    invoke-virtual {v1, v5, v6}, Ljava/io/RandomAccessFile;->seek(J)V

    #@40
    .line 339
    const-string v0, "\n"

    #@42
    invoke-virtual {v1, v0}, Ljava/io/RandomAccessFile;->writeBytes(Ljava/lang/String;)V
    :try_end_45
    .catchall {:try_start_3d .. :try_end_45} :catchall_bf
    .catch Ljava/lang/Exception; {:try_start_3d .. :try_end_45} :catch_c1

    #@45
    .line 357
    :cond_45
    if-eqz v1, :cond_4a

    #@47
    .line 359
    :try_start_47
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_4a
    .catch Ljava/lang/Exception; {:try_start_47 .. :try_end_4a} :catch_bb

    #@4a
    .line 372
    :cond_4a
    :goto_4a
    :try_start_4a
    new-instance v1, Ljava/io/RandomAccessFile;

    #@4c
    const-string v0, "r"

    #@4e
    invoke-direct {v1, v3, v0}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@51
    .line 374
    if-eqz v1, :cond_c3

    #@53
    .line 376
    int-to-long v2, v4

    #@54
    invoke-virtual {v1, v2, v3}, Ljava/io/RandomAccessFile;->seek(J)V

    #@57
    .line 377
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->readLine()Ljava/lang/String;

    #@5a
    move-result-object v0

    #@5b
    .line 378
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V

    #@5e
    .line 383
    :goto_5e
    iget-boolean v1, p0, Lcom/lge/systemservice/service/AATService;->mUseLog:Z

    #@60
    if-eqz v1, :cond_69

    #@62
    .line 385
    const-string v1, "AAT"

    #@64
    const-string v2, "[AATService] AccessMiscRead, read success"

    #@66
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_69
    .catch Ljava/io/IOException; {:try_start_4a .. :try_end_69} :catch_9b

    #@69
    .line 401
    :cond_69
    :goto_69
    return-object v0

    #@6a
    .line 338
    :cond_6a
    const/16 v0, 0x20

    #@6c
    goto :goto_3b

    #@6d
    .line 344
    :catch_6d
    move-exception v0

    #@6e
    move-object v1, v2

    #@6f
    .line 346
    :goto_6f
    :try_start_6f
    iget-boolean v5, p0, Lcom/lge/systemservice/service/AATService;->mUseLog:Z

    #@71
    if-eqz v5, :cond_8b

    #@73
    .line 348
    const-string v5, "AAT"

    #@75
    new-instance v6, Ljava/lang/StringBuilder;

    #@77
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@7a
    const-string v7, "[AATService] AccessMiscWrite, error : "

    #@7c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v6

    #@80
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v0

    #@84
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@87
    move-result-object v0

    #@88
    invoke-static {v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8b
    .catchall {:try_start_6f .. :try_end_8b} :catchall_bf

    #@8b
    .line 357
    :cond_8b
    if-eqz v1, :cond_4a

    #@8d
    .line 359
    :try_start_8d
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_90
    .catch Ljava/lang/Exception; {:try_start_8d .. :try_end_90} :catch_91

    #@90
    goto :goto_4a

    #@91
    .line 364
    :catch_91
    move-exception v0

    #@92
    goto :goto_4a

    #@93
    .line 355
    :catchall_93
    move-exception v0

    #@94
    move-object v1, v2

    #@95
    .line 357
    :goto_95
    if-eqz v1, :cond_9a

    #@97
    .line 359
    :try_start_97
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_9a
    .catch Ljava/lang/Exception; {:try_start_97 .. :try_end_9a} :catch_bd

    #@9a
    .line 365
    :cond_9a
    :goto_9a
    throw v0

    #@9b
    .line 388
    :catch_9b
    move-exception v0

    #@9c
    .line 390
    iget-boolean v1, p0, Lcom/lge/systemservice/service/AATService;->mUseLog:Z

    #@9e
    if-eqz v1, :cond_b8

    #@a0
    .line 392
    const-string v1, "AAT"

    #@a2
    new-instance v2, Ljava/lang/StringBuilder;

    #@a4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a7
    const-string v3, "[AATService] AccessMiscRead, error : "

    #@a9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v2

    #@ad
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v0

    #@b1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b4
    move-result-object v0

    #@b5
    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b8
    .line 396
    :cond_b8
    const-string v0, "8888888888888888888888888888888"

    #@ba
    goto :goto_69

    #@bb
    .line 364
    :catch_bb
    move-exception v0

    #@bc
    goto :goto_4a

    #@bd
    :catch_bd
    move-exception v1

    #@be
    goto :goto_9a

    #@bf
    .line 355
    :catchall_bf
    move-exception v0

    #@c0
    goto :goto_95

    #@c1
    .line 344
    :catch_c1
    move-exception v0

    #@c2
    goto :goto_6f

    #@c3
    :cond_c3
    move-object v0, v2

    #@c4
    goto :goto_5e
.end method

.method private AccessMiscWrite(ILjava/lang/String;)V
    .registers 8
    .parameter
    .parameter

    #@0
    .prologue
    .line 406
    iget v0, p0, Lcom/lge/systemservice/service/AATService;->MISC_BLK_SIZE:I

    #@2
    mul-int/2addr v0, p1

    #@3
    .line 407
    new-instance v3, Ljava/io/File;

    #@5
    const-string v1, "/dev/block/platform/msm_sdcc.1/by-name/misc"

    #@7
    invoke-direct {v3, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@a
    .line 408
    const/4 v2, 0x0

    #@b
    .line 410
    const-string v1, "AAT"

    #@d
    const-string v4, "[AATService] AccessMiscWrite, check"

    #@f
    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 414
    :try_start_12
    new-instance v1, Ljava/io/RandomAccessFile;

    #@14
    const-string v4, "rw"

    #@16
    invoke-direct {v1, v3, v4}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_19
    .catchall {:try_start_12 .. :try_end_19} :catchall_4a
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_19} :catch_28

    #@19
    .line 416
    if-eqz v1, :cond_22

    #@1b
    .line 418
    int-to-long v2, v0

    #@1c
    :try_start_1c
    invoke-virtual {v1, v2, v3}, Ljava/io/RandomAccessFile;->seek(J)V

    #@1f
    .line 419
    invoke-virtual {v1, p2}, Ljava/io/RandomAccessFile;->writeBytes(Ljava/lang/String;)V
    :try_end_22
    .catchall {:try_start_1c .. :try_end_22} :catchall_56
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_22} :catch_58

    #@22
    .line 433
    :cond_22
    if-eqz v1, :cond_27

    #@24
    .line 435
    :try_start_24
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_27
    .catch Ljava/lang/Exception; {:try_start_24 .. :try_end_27} :catch_52

    #@27
    .line 446
    :cond_27
    :goto_27
    return-void

    #@28
    .line 425
    :catch_28
    move-exception v0

    #@29
    move-object v1, v2

    #@2a
    .line 427
    :goto_2a
    :try_start_2a
    const-string v2, "AAT"

    #@2c
    new-instance v3, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v4, "[AATService] AccessMiscWrite, error : "

    #@33
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v0

    #@3f
    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_42
    .catchall {:try_start_2a .. :try_end_42} :catchall_56

    #@42
    .line 433
    if-eqz v1, :cond_27

    #@44
    .line 435
    :try_start_44
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_47
    .catch Ljava/lang/Exception; {:try_start_44 .. :try_end_47} :catch_48

    #@47
    goto :goto_27

    #@48
    .line 440
    :catch_48
    move-exception v0

    #@49
    goto :goto_27

    #@4a
    .line 431
    :catchall_4a
    move-exception v0

    #@4b
    move-object v1, v2

    #@4c
    .line 433
    :goto_4c
    if-eqz v1, :cond_51

    #@4e
    .line 435
    :try_start_4e
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_51
    .catch Ljava/lang/Exception; {:try_start_4e .. :try_end_51} :catch_54

    #@51
    .line 441
    :cond_51
    :goto_51
    throw v0

    #@52
    .line 440
    :catch_52
    move-exception v0

    #@53
    goto :goto_27

    #@54
    :catch_54
    move-exception v1

    #@55
    goto :goto_51

    #@56
    .line 431
    :catchall_56
    move-exception v0

    #@57
    goto :goto_4c

    #@58
    .line 425
    :catch_58
    move-exception v0

    #@59
    goto :goto_2a
.end method

.method private AnimationOff()V
    .registers 3

    #@0
    .prologue
    .line 492
    const-string v0, "AAT"

    #@2
    const-string v1, "[AATService] Animation Effect restore"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 493
    const/4 v0, 0x0

    #@8
    invoke-direct {p0, v0}, Lcom/lge/systemservice/service/AATService;->AATAnimationScale(Z)V

    #@b
    .line 494
    return-void
.end method

.method private AnimationOn()V
    .registers 3

    #@0
    .prologue
    .line 487
    const-string v0, "AAT"

    #@2
    const-string v1, "[AATService] Animation Effect On"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 488
    const/4 v0, 0x1

    #@8
    invoke-direct {p0, v0}, Lcom/lge/systemservice/service/AATService;->AATAnimationScale(Z)V

    #@b
    .line 489
    return-void
.end method

.method private TouchLedOff()V
    .registers 5

    #@0
    .prologue
    .line 470
    const-string v1, "AAT"

    #@2
    const-string v2, "[AATService] TouchLedOff"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 472
    iget v0, p0, Lcom/lge/systemservice/service/AATService;->Led_Timeout:I

    #@9
    .line 473
    .local v0, value:I
    const-string v1, "AAT"

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, " ------FRONTKEY_LED_TIMEOUT--- value = "

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 475
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@23
    const-string v2, "LG-F300L"

    #@25
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28
    move-result v1

    #@29
    if-nez v1, :cond_3f

    #@2b
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@2d
    const-string v2, "LG-F300K"

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@32
    move-result v1

    #@33
    if-nez v1, :cond_3f

    #@35
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@37
    const-string v2, "LG-F300S"

    #@39
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3c
    move-result v1

    #@3d
    if-eqz v1, :cond_4a

    #@3f
    .line 477
    :cond_3f
    iget-object v1, p0, Lcom/lge/systemservice/service/AATService;->mContext:Landroid/content/Context;

    #@41
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@44
    move-result-object v1

    #@45
    const-string v2, "frontkey_led_timeout"

    #@47
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@4a
    .line 483
    :cond_4a
    return-void
.end method

.method private TouchLedOn()V
    .registers 5

    #@0
    .prologue
    .line 450
    const-string v1, "AAT"

    #@2
    const-string v2, "[AATService] TouchLedOn"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 453
    :try_start_7
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@9
    const-string v2, "LG-F300L"

    #@b
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e
    move-result v1

    #@f
    if-nez v1, :cond_25

    #@11
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@13
    const-string v2, "LG-F300K"

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18
    move-result v1

    #@19
    if-nez v1, :cond_25

    #@1b
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@1d
    const-string v2, "LG-F300S"

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22
    move-result v1

    #@23
    if-eqz v1, :cond_59

    #@25
    .line 455
    :cond_25
    iget-object v1, p0, Lcom/lge/systemservice/service/AATService;->mContext:Landroid/content/Context;

    #@27
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2a
    move-result-object v1

    #@2b
    const-string v2, "frontkey_led_timeout"

    #@2d
    invoke-static {v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    #@30
    move-result v1

    #@31
    iput v1, p0, Lcom/lge/systemservice/service/AATService;->Led_Timeout:I

    #@33
    .line 456
    const v0, 0x3b9aca00

    #@36
    .line 457
    .local v0, value:I
    const-string v1, "AAT"

    #@38
    new-instance v2, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v3, "[AATService] ----- FRONTKEY_LED_TIMEOUT ------ value = "

    #@3f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v2

    #@43
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v2

    #@4b
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 458
    iget-object v1, p0, Lcom/lge/systemservice/service/AATService;->mContext:Landroid/content/Context;

    #@50
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@53
    move-result-object v1

    #@54
    const-string v2, "frontkey_led_timeout"

    #@56
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_59
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_59} :catch_5a

    #@59
    .line 467
    .end local v0           #value:I
    :cond_59
    :goto_59
    return-void

    #@5a
    .line 465
    :catch_5a
    move-exception v1

    #@5b
    goto :goto_59
.end method

.method private createDisableApkCheckFile(Ljava/lang/String;)V
    .registers 10
    .parameter "filePath"

    #@0
    .prologue
    .line 2878
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@3
    move-result v5

    #@4
    .line 2879
    .local v5, uid:I
    const/16 v4, 0x3e8

    #@6
    .line 2881
    .local v4, gid:I
    new-instance v1, Ljava/io/File;

    #@8
    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@b
    .line 2882
    .local v1, f:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    #@e
    move-result v6

    #@f
    if-nez v6, :cond_31

    #@11
    .line 2883
    const/4 v2, 0x0

    #@12
    .line 2886
    .local v2, fos:Ljava/io/FileOutputStream;
    :try_start_12
    new-instance v3, Ljava/io/FileOutputStream;

    #@14
    invoke-direct {v3, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_17
    .catchall {:try_start_12 .. :try_end_17} :catchall_48
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_17} :catch_38

    #@17
    .line 2887
    .end local v2           #fos:Ljava/io/FileOutputStream;
    .local v3, fos:Ljava/io/FileOutputStream;
    const/16 v6, 0x31

    #@19
    :try_start_19
    invoke-virtual {v3, v6}, Ljava/io/FileOutputStream;->write(I)V

    #@1c
    .line 2888
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->flush()V

    #@1f
    .line 2889
    invoke-static {v3}, Landroid/os/FileUtils;->sync(Ljava/io/FileOutputStream;)Z
    :try_end_22
    .catchall {:try_start_19 .. :try_end_22} :catchall_55
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_22} :catch_58

    #@22
    .line 2894
    if-eqz v3, :cond_5b

    #@24
    .line 2895
    :try_start_24
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_27
    .catch Ljava/io/IOException; {:try_start_24 .. :try_end_27} :catch_32

    #@27
    .line 2896
    const/4 v2, 0x0

    #@28
    .line 2903
    .end local v3           #fos:Ljava/io/FileOutputStream;
    .restart local v2       #fos:Ljava/io/FileOutputStream;
    :cond_28
    :goto_28
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@2b
    move-result-object v6

    #@2c
    const/16 v7, 0x1b4

    #@2e
    invoke-static {v6, v7, v5, v4}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    #@31
    .line 2905
    .end local v2           #fos:Ljava/io/FileOutputStream;
    :cond_31
    return-void

    #@32
    .line 2898
    .restart local v3       #fos:Ljava/io/FileOutputStream;
    :catch_32
    move-exception v0

    #@33
    .line 2899
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@36
    move-object v2, v3

    #@37
    .line 2901
    .end local v3           #fos:Ljava/io/FileOutputStream;
    .restart local v2       #fos:Ljava/io/FileOutputStream;
    goto :goto_28

    #@38
    .line 2890
    .end local v0           #e:Ljava/io/IOException;
    :catch_38
    move-exception v0

    #@39
    .line 2891
    .restart local v0       #e:Ljava/io/IOException;
    :goto_39
    :try_start_39
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3c
    .catchall {:try_start_39 .. :try_end_3c} :catchall_48

    #@3c
    .line 2894
    if-eqz v2, :cond_28

    #@3e
    .line 2895
    :try_start_3e
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_41
    .catch Ljava/io/IOException; {:try_start_3e .. :try_end_41} :catch_43

    #@41
    .line 2896
    const/4 v2, 0x0

    #@42
    goto :goto_28

    #@43
    .line 2898
    :catch_43
    move-exception v0

    #@44
    .line 2899
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@47
    goto :goto_28

    #@48
    .line 2893
    .end local v0           #e:Ljava/io/IOException;
    :catchall_48
    move-exception v6

    #@49
    .line 2894
    :goto_49
    if-eqz v2, :cond_4f

    #@4b
    .line 2895
    :try_start_4b
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_4e
    .catch Ljava/io/IOException; {:try_start_4b .. :try_end_4e} :catch_50

    #@4e
    .line 2896
    const/4 v2, 0x0

    #@4f
    .line 2900
    :cond_4f
    :goto_4f
    throw v6

    #@50
    .line 2898
    :catch_50
    move-exception v0

    #@51
    .line 2899
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@54
    goto :goto_4f

    #@55
    .line 2893
    .end local v0           #e:Ljava/io/IOException;
    .end local v2           #fos:Ljava/io/FileOutputStream;
    .restart local v3       #fos:Ljava/io/FileOutputStream;
    :catchall_55
    move-exception v6

    #@56
    move-object v2, v3

    #@57
    .end local v3           #fos:Ljava/io/FileOutputStream;
    .restart local v2       #fos:Ljava/io/FileOutputStream;
    goto :goto_49

    #@58
    .line 2890
    .end local v2           #fos:Ljava/io/FileOutputStream;
    .restart local v3       #fos:Ljava/io/FileOutputStream;
    :catch_58
    move-exception v0

    #@59
    move-object v2, v3

    #@5a
    .end local v3           #fos:Ljava/io/FileOutputStream;
    .restart local v2       #fos:Ljava/io/FileOutputStream;
    goto :goto_39

    #@5b
    .end local v2           #fos:Ljava/io/FileOutputStream;
    .restart local v3       #fos:Ljava/io/FileOutputStream;
    :cond_5b
    move-object v2, v3

    #@5c
    .end local v3           #fos:Ljava/io/FileOutputStream;
    .restart local v2       #fos:Ljava/io/FileOutputStream;
    goto :goto_28
.end method

.method private disableVendorApk(Z)V
    .registers 10
    .parameter

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v0, 0x0

    #@2
    .line 2822
    const-string v1, "ro.build.target_operator"

    #@4
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    .line 2823
    const-string v2, "ro.build.target_country"

    #@a
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@d
    move-result-object v2

    #@e
    .line 2825
    iget-object v4, p0, Lcom/lge/systemservice/service/AATService;->mContext:Landroid/content/Context;

    #@10
    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@13
    move-result-object v4

    #@14
    .line 2827
    const-string v5, "AAT"

    #@16
    new-instance v6, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v7, "[AATService] disableVendorApk() operator : "

    #@1d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v6

    #@21
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v6

    #@25
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v6

    #@29
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 2828
    const-string v5, "AAT"

    #@2e
    new-instance v6, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v7, "[AATService] disableVendorApk() country : "

    #@35
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v6

    #@39
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v6

    #@3d
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v6

    #@41
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 2834
    const-string v5, "TMO"

    #@46
    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@49
    move-result v1

    #@4a
    if-eqz v1, :cond_127

    #@4c
    const-string v1, "US"

    #@4e
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@51
    move-result v1

    #@52
    if-eqz v1, :cond_127

    #@54
    move v1, v0

    #@55
    move v2, v0

    #@56
    .line 2835
    :goto_56
    iget-object v5, p0, Lcom/lge/systemservice/service/AATService;->disableVendorApkListTmus:[Ljava/lang/String;

    #@58
    array-length v5, v5

    #@59
    if-ge v0, v5, :cond_10d

    #@5b
    .line 2836
    iget-object v5, p0, Lcom/lge/systemservice/service/AATService;->disableVendorApkListTmus:[Ljava/lang/String;

    #@5d
    aget-object v5, v5, v0

    #@5f
    if-eqz v5, :cond_b9

    #@61
    .line 2838
    :try_start_61
    iget-object v5, p0, Lcom/lge/systemservice/service/AATService;->disableVendorApkListTmus:[Ljava/lang/String;

    #@63
    aget-object v5, v5, v0

    #@65
    const/4 v6, 0x0

    #@66
    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    #@69
    .line 2839
    const-string v5, "AAT"

    #@6b
    new-instance v6, Ljava/lang/StringBuilder;

    #@6d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@70
    const-string v7, "[AATService] disableVendorApk() "

    #@72
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v6

    #@76
    iget-object v7, p0, Lcom/lge/systemservice/service/AATService;->disableVendorApkListTmus:[Ljava/lang/String;

    #@78
    aget-object v7, v7, v0

    #@7a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v6

    #@7e
    const-string v7, " is found"

    #@80
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v6

    #@84
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@87
    move-result-object v6

    #@88
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@8b
    .line 2840
    if-eqz p1, :cond_bc

    #@8d
    .line 2841
    iget-object v5, p0, Lcom/lge/systemservice/service/AATService;->disableVendorApkListTmus:[Ljava/lang/String;

    #@8f
    aget-object v5, v5, v0

    #@91
    const/4 v6, 0x2

    #@92
    const/4 v7, 0x0

    #@93
    invoke-virtual {v4, v5, v6, v7}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V
    :try_end_96
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_61 .. :try_end_96} :catch_e9

    #@96
    .line 2843
    :try_start_96
    const-string v2, "AAT"

    #@98
    new-instance v5, Ljava/lang/StringBuilder;

    #@9a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@9d
    const-string v6, "[AATService] disableVendorApk() "

    #@9f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v5

    #@a3
    iget-object v6, p0, Lcom/lge/systemservice/service/AATService;->disableVendorApkListTmus:[Ljava/lang/String;

    #@a5
    aget-object v6, v6, v0

    #@a7
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v5

    #@ab
    const-string v6, " is disabled"

    #@ad
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v5

    #@b1
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b4
    move-result-object v5

    #@b5
    invoke-static {v2, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b8
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_96 .. :try_end_b8} :catch_128

    #@b8
    move v2, v3

    #@b9
    .line 2835
    :cond_b9
    :goto_b9
    add-int/lit8 v0, v0, 0x1

    #@bb
    goto :goto_56

    #@bc
    .line 2846
    :cond_bc
    :try_start_bc
    iget-object v5, p0, Lcom/lge/systemservice/service/AATService;->disableVendorApkListTmus:[Ljava/lang/String;

    #@be
    aget-object v5, v5, v0

    #@c0
    const/4 v6, 0x1

    #@c1
    const/4 v7, 0x0

    #@c2
    invoke-virtual {v4, v5, v6, v7}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V
    :try_end_c5
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_bc .. :try_end_c5} :catch_e9

    #@c5
    .line 2848
    :try_start_c5
    const-string v1, "AAT"

    #@c7
    new-instance v5, Ljava/lang/StringBuilder;

    #@c9
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@cc
    const-string v6, "[AATService] disableVendorApk() "

    #@ce
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v5

    #@d2
    iget-object v6, p0, Lcom/lge/systemservice/service/AATService;->disableVendorApkListTmus:[Ljava/lang/String;

    #@d4
    aget-object v6, v6, v0

    #@d6
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v5

    #@da
    const-string v6, " is eabled"

    #@dc
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@df
    move-result-object v5

    #@e0
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e3
    move-result-object v5

    #@e4
    invoke-static {v1, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_e7
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_c5 .. :try_end_e7} :catch_12b

    #@e7
    move v1, v3

    #@e8
    goto :goto_b9

    #@e9
    .line 2850
    :catch_e9
    move-exception v5

    #@ea
    .line 2851
    :goto_ea
    const-string v5, "AAT"

    #@ec
    new-instance v6, Ljava/lang/StringBuilder;

    #@ee
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@f1
    const-string v7, "[AATService] disableVendorApk() "

    #@f3
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f6
    move-result-object v6

    #@f7
    iget-object v7, p0, Lcom/lge/systemservice/service/AATService;->disableVendorApkListTmus:[Ljava/lang/String;

    #@f9
    aget-object v7, v7, v0

    #@fb
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fe
    move-result-object v6

    #@ff
    const-string v7, " is not found"

    #@101
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@104
    move-result-object v6

    #@105
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@108
    move-result-object v6

    #@109
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@10c
    goto :goto_b9

    #@10d
    .line 2856
    :cond_10d
    if-eqz v2, :cond_114

    #@10f
    .line 2857
    iget-object v0, p0, Lcom/lge/systemservice/service/AATService;->disableVendorApkCheckFilePath:Ljava/lang/String;

    #@111
    invoke-direct {p0, v0}, Lcom/lge/systemservice/service/AATService;->createDisableApkCheckFile(Ljava/lang/String;)V

    #@114
    .line 2859
    :cond_114
    if-eqz v1, :cond_127

    #@116
    .line 2860
    iget-object v0, p0, Lcom/lge/systemservice/service/AATService;->disableVendorApkCheckFilePath:Ljava/lang/String;

    #@118
    invoke-direct {p0, v0}, Lcom/lge/systemservice/service/AATService;->removeDisableCheckFile(Ljava/lang/String;)V

    #@11b
    .line 2861
    iget-object v0, p0, Lcom/lge/systemservice/service/AATService;->mContext:Landroid/content/Context;

    #@11d
    new-instance v1, Landroid/content/Intent;

    #@11f
    const-string v2, "com.lge.appbox.bootinstall.completed"

    #@121
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@124
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@127
    .line 2865
    :cond_127
    return-void

    #@128
    .line 2850
    :catch_128
    move-exception v2

    #@129
    move v2, v3

    #@12a
    goto :goto_ea

    #@12b
    :catch_12b
    move-exception v1

    #@12c
    move v1, v3

    #@12d
    goto :goto_ea
.end method

.method private removeDisableCheckFile(Ljava/lang/String;)V
    .registers 5
    .parameter

    #@0
    .prologue
    .line 2907
    new-instance v0, Ljava/io/File;

    #@2
    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@5
    .line 2908
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_2c

    #@b
    .line 2909
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@e
    .line 2910
    const-string v0, "AAT"

    #@10
    new-instance v1, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v2, "[AATService] removeDisableCheckFile() "

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    const-string v2, " file is removed"

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v1

    #@29
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 2912
    :cond_2c
    return-void
.end method


# virtual methods
.method public AATFinalize()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 758
    const-string v0, "AAT"

    #@4
    const-string v1, "[AATService] AATFinalize"

    #@6
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 759
    const-string v0, "geehdc_kddi_jp"

    #@b
    const-string v1, "ro.product.name"

    #@d
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v0

    #@15
    if-nez v0, :cond_33

    #@17
    const-string v0, "geefhd_kddi_jp"

    #@19
    const-string v1, "ro.product.name"

    #@1b
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22
    move-result v0

    #@23
    if-nez v0, :cond_33

    #@25
    const-string v0, "g2_jp_kdi"

    #@27
    const-string v1, "ro.product.name"

    #@29
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@2c
    move-result-object v1

    #@2d
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@30
    move-result v0

    #@31
    if-eqz v0, :cond_58

    #@33
    .line 762
    :cond_33
    const-string v0, "sys.irdatest.enable"

    #@35
    const-string v1, "false"

    #@37
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@3a
    .line 763
    const-string v0, "sys.irdatest.enable"

    #@3c
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@3f
    move-result-object v0

    #@40
    .line 764
    const-string v1, "AAT"

    #@42
    new-instance v2, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v3, "[AATService] AATFinalize, sys.irdatest.enable : "

    #@49
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v2

    #@4d
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v0

    #@51
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v0

    #@55
    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@58
    .line 769
    :cond_58
    const-string v0, "sys.allautotest.run"

    #@5a
    const-string v1, "false"

    #@5c
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@5f
    .line 771
    new-instance v0, Landroid/content/Intent;

    #@61
    const-string v1, "com.lge.intent.action.ABS"

    #@63
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@66
    .line 772
    const-string v1, "abs_is_enabled"

    #@68
    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@6b
    .line 773
    iget-object v1, p0, Lcom/lge/systemservice/service/AATService;->mContext:Landroid/content/Context;

    #@6d
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@70
    .line 777
    iget-boolean v0, p0, Lcom/lge/systemservice/service/AATService;->mDoubleTapSetting:Z

    #@72
    if-eqz v0, :cond_7f

    #@74
    .line 778
    iget-object v0, p0, Lcom/lge/systemservice/service/AATService;->mContext:Landroid/content/Context;

    #@76
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@79
    move-result-object v0

    #@7a
    const-string v1, "gesture_trun_screen_on"

    #@7c
    invoke-static {v0, v1, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@7f
    .line 782
    :cond_7f
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@81
    const-string v1, "LG-F300L"

    #@83
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@86
    move-result v0

    #@87
    if-nez v0, :cond_9d

    #@89
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@8b
    const-string v1, "LG-F300K"

    #@8d
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@90
    move-result v0

    #@91
    if-nez v0, :cond_9d

    #@93
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@95
    const-string v1, "LG-F300S"

    #@97
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9a
    move-result v0

    #@9b
    if-eqz v0, :cond_a0

    #@9d
    .line 784
    :cond_9d
    invoke-direct {p0}, Lcom/lge/systemservice/service/AATService;->TouchLedOff()V

    #@a0
    .line 791
    :cond_a0
    const-string v0, "/sys/class/leds/button-backlight1/brightness"

    #@a2
    const-string v1, "0"

    #@a4
    invoke-virtual {p0, v0, v1}, Lcom/lge/systemservice/service/AATService;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    #@a7
    .line 792
    const-string v0, "/sys/class/leds/button-backlight2/brightness"

    #@a9
    const-string v1, "0"

    #@ab
    invoke-virtual {p0, v0, v1}, Lcom/lge/systemservice/service/AATService;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    #@ae
    .line 794
    invoke-direct {p0}, Lcom/lge/systemservice/service/AATService;->AnimationOn()V

    #@b1
    .line 796
    invoke-direct {p0, v4}, Lcom/lge/systemservice/service/AATService;->disableVendorApk(Z)V

    #@b4
    .line 798
    iget-object v0, p0, Lcom/lge/systemservice/service/AATService;->mAudioManager:Landroid/media/AudioManager;

    #@b6
    if-eqz v0, :cond_fc

    #@b8
    .line 800
    const-string v0, "AAT"

    #@ba
    new-instance v1, Ljava/lang/StringBuilder;

    #@bc
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@bf
    const-string v2, "[AATService] Restored default STEAM_MUSIC Volume: "

    #@c1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v1

    #@c5
    iget v2, p0, Lcom/lge/systemservice/service/AATService;->mSaveMusicVolume:I

    #@c7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v1

    #@cb
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ce
    move-result-object v1

    #@cf
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d2
    .line 801
    const-string v0, "AAT"

    #@d4
    new-instance v1, Ljava/lang/StringBuilder;

    #@d6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d9
    const-string v2, "[AATService] Restored default STEAM_RING Volume: "

    #@db
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@de
    move-result-object v1

    #@df
    iget v2, p0, Lcom/lge/systemservice/service/AATService;->mSaveRingVolume:I

    #@e1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v1

    #@e5
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e8
    move-result-object v1

    #@e9
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ec
    .line 802
    iget-object v0, p0, Lcom/lge/systemservice/service/AATService;->mAudioManager:Landroid/media/AudioManager;

    #@ee
    const/4 v1, 0x3

    #@ef
    iget v2, p0, Lcom/lge/systemservice/service/AATService;->mSaveMusicVolume:I

    #@f1
    invoke-virtual {v0, v1, v2, v4}, Landroid/media/AudioManager;->setStreamVolume(III)V

    #@f4
    .line 803
    iget-object v0, p0, Lcom/lge/systemservice/service/AATService;->mAudioManager:Landroid/media/AudioManager;

    #@f6
    const/4 v1, 0x2

    #@f7
    iget v2, p0, Lcom/lge/systemservice/service/AATService;->mSaveRingVolume:I

    #@f9
    invoke-virtual {v0, v1, v2, v4}, Landroid/media/AudioManager;->setStreamVolume(III)V

    #@fc
    .line 808
    :cond_fc
    iget-object v0, p0, Lcom/lge/systemservice/service/AATService;->mContext:Landroid/content/Context;

    #@fe
    const-string v1, "statusbar"

    #@100
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@103
    move-result-object v0

    #@104
    check-cast v0, Landroid/app/StatusBarManager;

    #@106
    .line 810
    invoke-virtual {v0, v4}, Landroid/app/StatusBarManager;->disable(I)V

    #@109
    .line 812
    const-string v0, "AAT"

    #@10b
    const-string v1, "[AATService] Enable Global Access"

    #@10d
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@110
    .line 814
    return-void
.end method

.method public AATInitialize()V
    .registers 7

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 665
    const-string v0, "AAT"

    #@4
    const-string v3, "[AATService] AATInitialize"

    #@6
    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 666
    const-string v0, "geehdc_kddi_jp"

    #@b
    const-string v3, "ro.product.name"

    #@d
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v0

    #@15
    if-nez v0, :cond_33

    #@17
    const-string v0, "geefhd_kddi_jp"

    #@19
    const-string v3, "ro.product.name"

    #@1b
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22
    move-result v0

    #@23
    if-nez v0, :cond_33

    #@25
    const-string v0, "g2_jp_kdi"

    #@27
    const-string v3, "ro.product.name"

    #@29
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@30
    move-result v0

    #@31
    if-eqz v0, :cond_58

    #@33
    .line 669
    :cond_33
    const-string v0, "sys.irdatest.enable"

    #@35
    const-string v3, "true"

    #@37
    invoke-static {v0, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@3a
    .line 670
    const-string v0, "sys.irdatest.enable"

    #@3c
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@3f
    move-result-object v0

    #@40
    .line 671
    const-string v3, "AAT"

    #@42
    new-instance v4, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v5, "[AATService] AATInitialize, sys.irdatest.enable : "

    #@49
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v4

    #@4d
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v0

    #@51
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v0

    #@55
    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@58
    .line 676
    :cond_58
    const-string v0, "ro.product.model"

    #@5a
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5d
    move-result-object v0

    #@5e
    sput-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@60
    .line 677
    const-string v0, "ro.product.name"

    #@62
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@65
    move-result-object v0

    #@66
    sput-object v0, Lcom/lge/systemservice/service/AATService;->product_name:Ljava/lang/String;

    #@68
    .line 678
    const-string v0, "ro.product.device"

    #@6a
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6d
    move-result-object v0

    #@6e
    sput-object v0, Lcom/lge/systemservice/service/AATService;->product_device:Ljava/lang/String;

    #@70
    .line 680
    const-string v0, "sys.allautotest.run"

    #@72
    const-string v3, "true"

    #@74
    invoke-static {v0, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@77
    .line 682
    new-instance v0, Landroid/content/Intent;

    #@79
    const-string v3, "com.lge.intent.action.ABS"

    #@7b
    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7e
    .line 683
    const-string v3, "abs_is_enabled"

    #@80
    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@83
    .line 684
    iget-object v3, p0, Lcom/lge/systemservice/service/AATService;->mContext:Landroid/content/Context;

    #@85
    invoke-virtual {v3, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@88
    .line 688
    invoke-virtual {p0}, Lcom/lge/systemservice/service/AATService;->isFactoryTestMode()Z

    #@8b
    move-result v0

    #@8c
    if-ne v0, v1, :cond_a1

    #@8e
    .line 690
    new-instance v0, Landroid/content/Intent;

    #@90
    const-string v3, "com.lge.aat.navigation.hide"

    #@92
    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@95
    .line 691
    iget-object v3, p0, Lcom/lge/systemservice/service/AATService;->mContext:Landroid/content/Context;

    #@97
    invoke-virtual {v3, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@9a
    .line 693
    const-string v0, "AAT"

    #@9c
    const-string v3, "[AATService] NAVIGATION BAR DISABLED, com.lge.aat.navigation.hide"

    #@9e
    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a1
    .line 700
    :cond_a1
    iget-object v0, p0, Lcom/lge/systemservice/service/AATService;->mContext:Landroid/content/Context;

    #@a3
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@a6
    move-result-object v0

    #@a7
    const-string v3, "gesture_trun_screen_on"

    #@a9
    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@ac
    move-result v0

    #@ad
    if-ne v0, v1, :cond_228

    #@af
    move v0, v1

    #@b0
    :goto_b0
    iput-boolean v0, p0, Lcom/lge/systemservice/service/AATService;->mDoubleTapSetting:Z

    #@b2
    .line 701
    iget-boolean v0, p0, Lcom/lge/systemservice/service/AATService;->mDoubleTapSetting:Z

    #@b4
    if-eqz v0, :cond_c1

    #@b6
    .line 702
    iget-object v0, p0, Lcom/lge/systemservice/service/AATService;->mContext:Landroid/content/Context;

    #@b8
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@bb
    move-result-object v0

    #@bc
    const-string v3, "gesture_trun_screen_on"

    #@be
    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@c1
    .line 706
    :cond_c1
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@c3
    const-string v2, "LG-F300L"

    #@c5
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c8
    move-result v0

    #@c9
    if-nez v0, :cond_df

    #@cb
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@cd
    const-string v2, "LG-F300K"

    #@cf
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d2
    move-result v0

    #@d3
    if-nez v0, :cond_df

    #@d5
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@d7
    const-string v2, "LG-F300S"

    #@d9
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@dc
    move-result v0

    #@dd
    if-eqz v0, :cond_e2

    #@df
    .line 708
    :cond_df
    invoke-direct {p0}, Lcom/lge/systemservice/service/AATService;->TouchLedOn()V

    #@e2
    .line 715
    :cond_e2
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@e4
    const-string v2, "LG-F320L"

    #@e6
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e9
    move-result v0

    #@ea
    if-nez v0, :cond_178

    #@ec
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@ee
    const-string v2, "LG-F320K"

    #@f0
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f3
    move-result v0

    #@f4
    if-nez v0, :cond_178

    #@f6
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@f8
    const-string v2, "LG-F320S"

    #@fa
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@fd
    move-result v0

    #@fe
    if-nez v0, :cond_178

    #@100
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@102
    const-string v2, "LG-DS1203"

    #@104
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@107
    move-result v0

    #@108
    if-nez v0, :cond_178

    #@10a
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@10c
    const-string v2, "VS980 4G"

    #@10e
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@111
    move-result v0

    #@112
    if-nez v0, :cond_178

    #@114
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@116
    const-string v2, "LG-D800"

    #@118
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11b
    move-result v0

    #@11c
    if-nez v0, :cond_178

    #@11e
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@120
    const-string v2, "LG-D801"

    #@122
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@125
    move-result v0

    #@126
    if-nez v0, :cond_178

    #@128
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@12a
    const-string v2, "LG-D803"

    #@12c
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12f
    move-result v0

    #@130
    if-nez v0, :cond_178

    #@132
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@134
    const-string v2, "LG-LS980"

    #@136
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@139
    move-result v0

    #@13a
    if-nez v0, :cond_178

    #@13c
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@13e
    const-string v2, "LG-D802"

    #@140
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@143
    move-result v0

    #@144
    if-nez v0, :cond_178

    #@146
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@148
    const-string v2, "LG-D802T"

    #@14a
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14d
    move-result v0

    #@14e
    if-nez v0, :cond_178

    #@150
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@152
    const-string v2, "LG-D805"

    #@154
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@157
    move-result v0

    #@158
    if-nez v0, :cond_178

    #@15a
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@15c
    const-string v2, "LG-D806"

    #@15e
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@161
    move-result v0

    #@162
    if-nez v0, :cond_178

    #@164
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@166
    const-string v2, "LG-D807"

    #@168
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16b
    move-result v0

    #@16c
    if-nez v0, :cond_178

    #@16e
    sget-object v0, Lcom/lge/systemservice/service/AATService;->product_name:Ljava/lang/String;

    #@170
    const-string v2, "g2_jp_kdi"

    #@172
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@175
    move-result v0

    #@176
    if-eqz v0, :cond_22b

    #@178
    .line 721
    :cond_178
    const-string v0, "140"

    #@17a
    iput-object v0, p0, Lcom/lge/systemservice/service/AATService;->mDefaultLcdBrightValue:Ljava/lang/String;

    #@17c
    .line 722
    const-string v0, "AAT"

    #@17e
    new-instance v2, Ljava/lang/StringBuilder;

    #@180
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@183
    const-string v3, "[AATService] Default Brightness Value: "

    #@185
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@188
    move-result-object v2

    #@189
    iget-object v3, p0, Lcom/lge/systemservice/service/AATService;->mDefaultLcdBrightValue:Ljava/lang/String;

    #@18b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18e
    move-result-object v2

    #@18f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@192
    move-result-object v2

    #@193
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@196
    .line 729
    :goto_196
    iget-object v0, p0, Lcom/lge/systemservice/service/AATService;->mContext:Landroid/content/Context;

    #@198
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@19b
    move-result-object v0

    #@19c
    const-string v2, "transition_animation_scale"

    #@19e
    const/4 v3, 0x0

    #@19f
    invoke-static {v0, v2, v3}, Landroid/provider/Settings$System;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    #@1a2
    move-result v0

    #@1a3
    iput v0, p0, Lcom/lge/systemservice/service/AATService;->animation_scale:F

    #@1a5
    .line 730
    invoke-direct {p0}, Lcom/lge/systemservice/service/AATService;->AnimationOff()V

    #@1a8
    .line 732
    invoke-virtual {p0}, Lcom/lge/systemservice/service/AATService;->setLEDFilePath()V

    #@1ab
    .line 733
    invoke-virtual {p0}, Lcom/lge/systemservice/service/AATService;->setHallICFilePath()V

    #@1ae
    .line 735
    invoke-direct {p0, v1}, Lcom/lge/systemservice/service/AATService;->disableVendorApk(Z)V

    #@1b1
    .line 737
    const-string v0, "/sys/class/leds/button-backlight1/brightness"

    #@1b3
    const-string v1, "255"

    #@1b5
    invoke-virtual {p0, v0, v1}, Lcom/lge/systemservice/service/AATService;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    #@1b8
    .line 738
    const-string v0, "/sys/class/leds/button-backlight2/brightness"

    #@1ba
    const-string v1, "255"

    #@1bc
    invoke-virtual {p0, v0, v1}, Lcom/lge/systemservice/service/AATService;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    #@1bf
    .line 741
    iget-object v0, p0, Lcom/lge/systemservice/service/AATService;->mContext:Landroid/content/Context;

    #@1c1
    const-string v1, "audio"

    #@1c3
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1c6
    move-result-object v0

    #@1c7
    check-cast v0, Landroid/media/AudioManager;

    #@1c9
    iput-object v0, p0, Lcom/lge/systemservice/service/AATService;->mAudioManager:Landroid/media/AudioManager;

    #@1cb
    .line 742
    iget-object v0, p0, Lcom/lge/systemservice/service/AATService;->mAudioManager:Landroid/media/AudioManager;

    #@1cd
    const/4 v1, 0x3

    #@1ce
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    #@1d1
    move-result v0

    #@1d2
    iput v0, p0, Lcom/lge/systemservice/service/AATService;->mSaveMusicVolume:I

    #@1d4
    .line 743
    iget-object v0, p0, Lcom/lge/systemservice/service/AATService;->mAudioManager:Landroid/media/AudioManager;

    #@1d6
    const/4 v1, 0x2

    #@1d7
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    #@1da
    move-result v0

    #@1db
    iput v0, p0, Lcom/lge/systemservice/service/AATService;->mSaveRingVolume:I

    #@1dd
    .line 745
    const-string v0, "AAT"

    #@1df
    new-instance v1, Ljava/lang/StringBuilder;

    #@1e1
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1e4
    const-string v2, "[AATService] Saved default STREAM_MUSIC Volume: "

    #@1e6
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e9
    move-result-object v1

    #@1ea
    iget v2, p0, Lcom/lge/systemservice/service/AATService;->mSaveMusicVolume:I

    #@1ec
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1ef
    move-result-object v1

    #@1f0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f3
    move-result-object v1

    #@1f4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f7
    .line 746
    const-string v0, "AAT"

    #@1f9
    new-instance v1, Ljava/lang/StringBuilder;

    #@1fb
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1fe
    const-string v2, "[AATService] Saved default STREAM_RING Volume: "

    #@200
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@203
    move-result-object v1

    #@204
    iget v2, p0, Lcom/lge/systemservice/service/AATService;->mSaveRingVolume:I

    #@206
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@209
    move-result-object v1

    #@20a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20d
    move-result-object v1

    #@20e
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@211
    .line 748
    iget-object v0, p0, Lcom/lge/systemservice/service/AATService;->mContext:Landroid/content/Context;

    #@213
    const-string v1, "statusbar"

    #@215
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@218
    move-result-object v0

    #@219
    check-cast v0, Landroid/app/StatusBarManager;

    #@21b
    .line 749
    const/high16 v1, 0x200

    #@21d
    .line 750
    invoke-virtual {v0, v1}, Landroid/app/StatusBarManager;->disable(I)V

    #@220
    .line 752
    const-string v0, "AAT"

    #@222
    const-string v1, "[AATService] Disable Global Access"

    #@224
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@227
    .line 754
    return-void

    #@228
    :cond_228
    move v0, v2

    #@229
    .line 700
    goto/16 :goto_b0

    #@22b
    .line 726
    :cond_22b
    invoke-virtual {p0}, Lcom/lge/systemservice/service/AATService;->ReadOriginalLcdBrightValue()V

    #@22e
    goto/16 :goto_196
.end method

.method public AATsetLCDOnOff(Z)V
    .registers 7
    .parameter

    #@0
    .prologue
    .line 1882
    :try_start_0
    iget-object v0, p0, Lcom/lge/systemservice/service/AATService;->mDefaultLcdBrightValue:Ljava/lang/String;

    #@2
    .line 1883
    new-instance v1, Ljava/io/BufferedWriter;

    #@4
    new-instance v2, Ljava/io/FileWriter;

    #@6
    const-string v3, "/sys/class/leds/lcd-backlight/brightness"

    #@8
    invoke-direct {v2, v3}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    #@b
    invoke-direct {v1, v2}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    #@e
    .line 1885
    if-eqz p1, :cond_48

    #@10
    .line 1888
    const/4 v2, 0x0

    #@11
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@14
    move-result v3

    #@15
    invoke-virtual {v1, v0, v2, v3}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;II)V

    #@18
    .line 1889
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->newLine()V

    #@1b
    .line 1890
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->flush()V

    #@1e
    .line 1891
    const-string v2, "/sys/class/leds/button-backlight1/brightness"

    #@20
    const-string v3, "255"

    #@22
    invoke-virtual {p0, v2, v3}, Lcom/lge/systemservice/service/AATService;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    #@25
    .line 1892
    const-string v2, "/sys/class/leds/button-backlight2/brightness"

    #@27
    const-string v3, "255"

    #@29
    invoke-virtual {p0, v2, v3}, Lcom/lge/systemservice/service/AATService;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    #@2c
    .line 1894
    const-string v2, "AAT"

    #@2e
    new-instance v3, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v4, "[AATService] AATsetLCDOnOff LCD, LED On: "

    #@35
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v3

    #@39
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v0

    #@3d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v0

    #@41
    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 1907
    :goto_44
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V

    #@47
    .line 1914
    :goto_47
    return-void

    #@48
    .line 1898
    :cond_48
    const-string v0, "0"

    #@4a
    .line 1899
    const/4 v2, 0x0

    #@4b
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@4e
    move-result v3

    #@4f
    invoke-virtual {v1, v0, v2, v3}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;II)V

    #@52
    .line 1900
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->newLine()V

    #@55
    .line 1901
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->flush()V

    #@58
    .line 1902
    const-string v2, "/sys/class/leds/button-backlight1/brightness"

    #@5a
    const-string v3, "0"

    #@5c
    invoke-virtual {p0, v2, v3}, Lcom/lge/systemservice/service/AATService;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    #@5f
    .line 1903
    const-string v2, "/sys/class/leds/button-backlight2/brightness"

    #@61
    const-string v3, "0"

    #@63
    invoke-virtual {p0, v2, v3}, Lcom/lge/systemservice/service/AATService;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    #@66
    .line 1905
    const-string v2, "AAT"

    #@68
    new-instance v3, Ljava/lang/StringBuilder;

    #@6a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@6d
    const-string v4, "[AATService] AATsetLCDOnOff LCD, LED Off: "

    #@6f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v3

    #@73
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v0

    #@77
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7a
    move-result-object v0

    #@7b
    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7e
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_7e} :catch_7f

    #@7e
    goto :goto_44

    #@7f
    .line 1909
    :catch_7f
    move-exception v0

    #@80
    .line 1911
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@83
    .line 1912
    const-string v0, "AAT"

    #@85
    const-string v1, "[AATService] setLCDOnOff FileWriter Execption error()"

    #@87
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8a
    goto :goto_47
.end method

.method public FolderTest_GetDetailTestSupportValue(I)Z
    .registers 6
    .parameter

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v0, 0x0

    #@2
    .line 2082
    .line 2096
    packed-switch p1, :pswitch_data_4b0

    #@5
    .line 2337
    :cond_5
    :goto_5
    :pswitch_5
    return v0

    #@6
    .line 2099
    :pswitch_6
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@8
    const-string v3, "LG-E450f"

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v2

    #@e
    if-nez v2, :cond_5

    #@10
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@12
    const-string v3, "LG-E455f"

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v2

    #@18
    if-nez v2, :cond_5

    #@1a
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@1c
    const-string v3, "LG-E460"

    #@1e
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21
    move-result v2

    #@22
    if-nez v2, :cond_5

    #@24
    .line 2103
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@26
    const-string v3, "LG-E430"

    #@28
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2b
    move-result v2

    #@2c
    if-nez v2, :cond_42

    #@2e
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@30
    const-string v3, "LG-E430g"

    #@32
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@35
    move-result v2

    #@36
    if-nez v2, :cond_42

    #@38
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@3a
    const-string v3, "LG-E430f"

    #@3c
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3f
    move-result v2

    #@40
    if-eqz v2, :cond_44

    #@42
    :cond_42
    move v0, v1

    #@43
    .line 2105
    goto :goto_5

    #@44
    .line 2107
    :cond_44
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@46
    const-string v3, "LG-F180L"

    #@48
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4b
    move-result v2

    #@4c
    if-nez v2, :cond_62

    #@4e
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@50
    const-string v3, "LG-F180K"

    #@52
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@55
    move-result v2

    #@56
    if-nez v2, :cond_62

    #@58
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@5a
    const-string v3, "LG-F180S"

    #@5c
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5f
    move-result v2

    #@60
    if-eqz v2, :cond_5

    #@62
    :cond_62
    move v0, v1

    #@63
    .line 2109
    goto :goto_5

    #@64
    .line 2118
    :pswitch_64
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@66
    const-string v2, "LG-E450f"

    #@68
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6b
    move-result v0

    #@6c
    if-nez v0, :cond_82

    #@6e
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@70
    const-string v2, "LG-E455f"

    #@72
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@75
    move-result v0

    #@76
    if-nez v0, :cond_82

    #@78
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@7a
    const-string v2, "LG-E460"

    #@7c
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7f
    move-result v0

    #@80
    if-eqz v0, :cond_84

    #@82
    :cond_82
    move v0, v1

    #@83
    .line 2120
    goto :goto_5

    #@84
    .line 2122
    :cond_84
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@86
    const-string v2, "LG-E430"

    #@88
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8b
    move-result v0

    #@8c
    if-nez v0, :cond_a2

    #@8e
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@90
    const-string v2, "LG-E430g"

    #@92
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@95
    move-result v0

    #@96
    if-nez v0, :cond_a2

    #@98
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@9a
    const-string v2, "LG-E430f"

    #@9c
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9f
    move-result v0

    #@a0
    if-eqz v0, :cond_a5

    #@a2
    :cond_a2
    move v0, v1

    #@a3
    .line 2124
    goto/16 :goto_5

    #@a5
    .line 2126
    :cond_a5
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@a7
    const-string v2, "LG-F180L"

    #@a9
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ac
    move-result v0

    #@ad
    if-nez v0, :cond_c3

    #@af
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@b1
    const-string v2, "LG-F180K"

    #@b3
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b6
    move-result v0

    #@b7
    if-nez v0, :cond_c3

    #@b9
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@bb
    const-string v2, "LG-F180S"

    #@bd
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c0
    move-result v0

    #@c1
    if-eqz v0, :cond_c6

    #@c3
    :cond_c3
    move v0, v1

    #@c4
    .line 2128
    goto/16 :goto_5

    #@c6
    :cond_c6
    move v0, v1

    #@c7
    .line 2132
    goto/16 :goto_5

    #@c9
    .line 2136
    :pswitch_c9
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@cb
    const-string v2, "LG-E450f"

    #@cd
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d0
    move-result v1

    #@d1
    if-nez v1, :cond_5

    #@d3
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@d5
    const-string v2, "LG-E455f"

    #@d7
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@da
    move-result v1

    #@db
    if-nez v1, :cond_5

    #@dd
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@df
    const-string v2, "LG-E460"

    #@e1
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e4
    move-result v1

    #@e5
    if-nez v1, :cond_5

    #@e7
    .line 2140
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@e9
    const-string v2, "LG-E430"

    #@eb
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ee
    move-result v1

    #@ef
    if-nez v1, :cond_5

    #@f1
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@f3
    const-string v2, "LG-E430g"

    #@f5
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f8
    move-result v1

    #@f9
    if-nez v1, :cond_5

    #@fb
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@fd
    const-string v2, "LG-E430f"

    #@ff
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@102
    move-result v1

    #@103
    if-nez v1, :cond_5

    #@105
    .line 2144
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@107
    const-string v2, "LG-F180L"

    #@109
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10c
    move-result v1

    #@10d
    if-nez v1, :cond_5

    #@10f
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@111
    const-string v2, "LG-F180K"

    #@113
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@116
    move-result v1

    #@117
    if-nez v1, :cond_5

    #@119
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@11b
    const-string v2, "LG-F180S"

    #@11d
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@120
    move-result v1

    #@121
    if-eqz v1, :cond_5

    #@123
    goto/16 :goto_5

    #@125
    .line 2154
    :pswitch_125
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@127
    const-string v2, "LG-E450f"

    #@129
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12c
    move-result v0

    #@12d
    if-nez v0, :cond_143

    #@12f
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@131
    const-string v2, "LG-E455f"

    #@133
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@136
    move-result v0

    #@137
    if-nez v0, :cond_143

    #@139
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@13b
    const-string v2, "LG-E460"

    #@13d
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@140
    move-result v0

    #@141
    if-eqz v0, :cond_146

    #@143
    :cond_143
    move v0, v1

    #@144
    .line 2156
    goto/16 :goto_5

    #@146
    .line 2158
    :cond_146
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@148
    const-string v2, "LG-E430"

    #@14a
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14d
    move-result v0

    #@14e
    if-nez v0, :cond_164

    #@150
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@152
    const-string v2, "LG-E430g"

    #@154
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@157
    move-result v0

    #@158
    if-nez v0, :cond_164

    #@15a
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@15c
    const-string v2, "LG-E430f"

    #@15e
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@161
    move-result v0

    #@162
    if-eqz v0, :cond_167

    #@164
    :cond_164
    move v0, v1

    #@165
    .line 2160
    goto/16 :goto_5

    #@167
    .line 2162
    :cond_167
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@169
    const-string v2, "LG-F180L"

    #@16b
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16e
    move-result v0

    #@16f
    if-nez v0, :cond_185

    #@171
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@173
    const-string v2, "LG-F180K"

    #@175
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@178
    move-result v0

    #@179
    if-nez v0, :cond_185

    #@17b
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@17d
    const-string v2, "LG-F180S"

    #@17f
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@182
    move-result v0

    #@183
    if-eqz v0, :cond_188

    #@185
    :cond_185
    move v0, v1

    #@186
    .line 2164
    goto/16 :goto_5

    #@188
    :cond_188
    move v0, v1

    #@189
    .line 2168
    goto/16 :goto_5

    #@18b
    .line 2172
    :pswitch_18b
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@18d
    const-string v2, "LG-E450f"

    #@18f
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@192
    move-result v0

    #@193
    if-nez v0, :cond_1a9

    #@195
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@197
    const-string v2, "LG-E455f"

    #@199
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@19c
    move-result v0

    #@19d
    if-nez v0, :cond_1a9

    #@19f
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@1a1
    const-string v2, "LG-E460"

    #@1a3
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1a6
    move-result v0

    #@1a7
    if-eqz v0, :cond_1ac

    #@1a9
    :cond_1a9
    move v0, v1

    #@1aa
    .line 2174
    goto/16 :goto_5

    #@1ac
    .line 2176
    :cond_1ac
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@1ae
    const-string v2, "LG-E430"

    #@1b0
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b3
    move-result v0

    #@1b4
    if-nez v0, :cond_1ca

    #@1b6
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@1b8
    const-string v2, "LG-E430g"

    #@1ba
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1bd
    move-result v0

    #@1be
    if-nez v0, :cond_1ca

    #@1c0
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@1c2
    const-string v2, "LG-E430f"

    #@1c4
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c7
    move-result v0

    #@1c8
    if-eqz v0, :cond_1cd

    #@1ca
    :cond_1ca
    move v0, v1

    #@1cb
    .line 2178
    goto/16 :goto_5

    #@1cd
    .line 2180
    :cond_1cd
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@1cf
    const-string v2, "LG-F180L"

    #@1d1
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d4
    move-result v0

    #@1d5
    if-nez v0, :cond_1eb

    #@1d7
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@1d9
    const-string v2, "LG-F180K"

    #@1db
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1de
    move-result v0

    #@1df
    if-nez v0, :cond_1eb

    #@1e1
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@1e3
    const-string v2, "LG-F180S"

    #@1e5
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e8
    move-result v0

    #@1e9
    if-eqz v0, :cond_1ee

    #@1eb
    :cond_1eb
    move v0, v1

    #@1ec
    .line 2182
    goto/16 :goto_5

    #@1ee
    :cond_1ee
    move v0, v1

    #@1ef
    .line 2186
    goto/16 :goto_5

    #@1f1
    .line 2190
    :pswitch_1f1
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@1f3
    const-string v3, "LG-E450f"

    #@1f5
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f8
    move-result v2

    #@1f9
    if-nez v2, :cond_5

    #@1fb
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@1fd
    const-string v3, "LG-E455f"

    #@1ff
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@202
    move-result v2

    #@203
    if-nez v2, :cond_5

    #@205
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@207
    const-string v3, "LG-E460"

    #@209
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@20c
    move-result v2

    #@20d
    if-nez v2, :cond_5

    #@20f
    .line 2194
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@211
    const-string v3, "LG-E430"

    #@213
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@216
    move-result v2

    #@217
    if-nez v2, :cond_5

    #@219
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@21b
    const-string v3, "LG-E430g"

    #@21d
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@220
    move-result v2

    #@221
    if-nez v2, :cond_5

    #@223
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@225
    const-string v3, "LG-E430f"

    #@227
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22a
    move-result v2

    #@22b
    if-nez v2, :cond_5

    #@22d
    .line 2198
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@22f
    const-string v2, "LG-F180L"

    #@231
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@234
    move-result v0

    #@235
    if-nez v0, :cond_24b

    #@237
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@239
    const-string v2, "LG-F180K"

    #@23b
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23e
    move-result v0

    #@23f
    if-nez v0, :cond_24b

    #@241
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@243
    const-string v2, "LG-F180S"

    #@245
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@248
    move-result v0

    #@249
    if-eqz v0, :cond_24e

    #@24b
    :cond_24b
    move v0, v1

    #@24c
    .line 2200
    goto/16 :goto_5

    #@24e
    :cond_24e
    move v0, v1

    #@24f
    .line 2204
    goto/16 :goto_5

    #@251
    .line 2208
    :pswitch_251
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@253
    const-string v2, "LG-E450f"

    #@255
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@258
    move-result v0

    #@259
    if-nez v0, :cond_26f

    #@25b
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@25d
    const-string v2, "LG-E455f"

    #@25f
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@262
    move-result v0

    #@263
    if-nez v0, :cond_26f

    #@265
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@267
    const-string v2, "LG-E460"

    #@269
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@26c
    move-result v0

    #@26d
    if-eqz v0, :cond_272

    #@26f
    :cond_26f
    move v0, v1

    #@270
    .line 2210
    goto/16 :goto_5

    #@272
    .line 2212
    :cond_272
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@274
    const-string v2, "LG-E430"

    #@276
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@279
    move-result v0

    #@27a
    if-nez v0, :cond_290

    #@27c
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@27e
    const-string v2, "LG-E430g"

    #@280
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@283
    move-result v0

    #@284
    if-nez v0, :cond_290

    #@286
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@288
    const-string v2, "LG-E430f"

    #@28a
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28d
    move-result v0

    #@28e
    if-eqz v0, :cond_293

    #@290
    :cond_290
    move v0, v1

    #@291
    .line 2214
    goto/16 :goto_5

    #@293
    .line 2216
    :cond_293
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@295
    const-string v2, "LG-F180L"

    #@297
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@29a
    move-result v0

    #@29b
    if-nez v0, :cond_2b1

    #@29d
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@29f
    const-string v2, "LG-F180K"

    #@2a1
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2a4
    move-result v0

    #@2a5
    if-nez v0, :cond_2b1

    #@2a7
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@2a9
    const-string v2, "LG-F180S"

    #@2ab
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2ae
    move-result v0

    #@2af
    if-eqz v0, :cond_2b4

    #@2b1
    :cond_2b1
    move v0, v1

    #@2b2
    .line 2218
    goto/16 :goto_5

    #@2b4
    :cond_2b4
    move v0, v1

    #@2b5
    .line 2222
    goto/16 :goto_5

    #@2b7
    .line 2226
    :pswitch_2b7
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@2b9
    const-string v2, "LG-E450f"

    #@2bb
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2be
    move-result v1

    #@2bf
    if-nez v1, :cond_5

    #@2c1
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@2c3
    const-string v2, "LG-E455f"

    #@2c5
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2c8
    move-result v1

    #@2c9
    if-nez v1, :cond_5

    #@2cb
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@2cd
    const-string v2, "LG-E460"

    #@2cf
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2d2
    move-result v1

    #@2d3
    if-nez v1, :cond_5

    #@2d5
    .line 2230
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@2d7
    const-string v2, "LG-E430"

    #@2d9
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2dc
    move-result v1

    #@2dd
    if-nez v1, :cond_5

    #@2df
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@2e1
    const-string v2, "LG-E430g"

    #@2e3
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2e6
    move-result v1

    #@2e7
    if-nez v1, :cond_5

    #@2e9
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@2eb
    const-string v2, "LG-E430f"

    #@2ed
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2f0
    move-result v1

    #@2f1
    if-nez v1, :cond_5

    #@2f3
    .line 2234
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@2f5
    const-string v2, "LG-F180L"

    #@2f7
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2fa
    move-result v1

    #@2fb
    if-nez v1, :cond_5

    #@2fd
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@2ff
    const-string v2, "LG-F180K"

    #@301
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@304
    move-result v1

    #@305
    if-nez v1, :cond_5

    #@307
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@309
    const-string v2, "LG-F180S"

    #@30b
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@30e
    move-result v1

    #@30f
    if-eqz v1, :cond_5

    #@311
    goto/16 :goto_5

    #@313
    .line 2244
    :pswitch_313
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@315
    const-string v3, "LG-E450f"

    #@317
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@31a
    move-result v2

    #@31b
    if-nez v2, :cond_5

    #@31d
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@31f
    const-string v3, "LG-E455f"

    #@321
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@324
    move-result v2

    #@325
    if-nez v2, :cond_5

    #@327
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@329
    const-string v3, "LG-E460"

    #@32b
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@32e
    move-result v2

    #@32f
    if-nez v2, :cond_5

    #@331
    .line 2248
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@333
    const-string v3, "LG-E430"

    #@335
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@338
    move-result v2

    #@339
    if-nez v2, :cond_5

    #@33b
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@33d
    const-string v3, "LG-E430g"

    #@33f
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@342
    move-result v2

    #@343
    if-nez v2, :cond_5

    #@345
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@347
    const-string v3, "LG-E430f"

    #@349
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@34c
    move-result v2

    #@34d
    if-nez v2, :cond_5

    #@34f
    .line 2252
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@351
    const-string v2, "LG-F180L"

    #@353
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@356
    move-result v0

    #@357
    if-nez v0, :cond_36d

    #@359
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@35b
    const-string v2, "LG-F180K"

    #@35d
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@360
    move-result v0

    #@361
    if-nez v0, :cond_36d

    #@363
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@365
    const-string v2, "LG-F180S"

    #@367
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@36a
    move-result v0

    #@36b
    if-eqz v0, :cond_370

    #@36d
    :cond_36d
    move v0, v1

    #@36e
    .line 2254
    goto/16 :goto_5

    #@370
    :cond_370
    move v0, v1

    #@371
    .line 2258
    goto/16 :goto_5

    #@373
    .line 2262
    :pswitch_373
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@375
    const-string v2, "LG-E450f"

    #@377
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@37a
    move-result v1

    #@37b
    if-nez v1, :cond_5

    #@37d
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@37f
    const-string v2, "LG-E455f"

    #@381
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@384
    move-result v1

    #@385
    if-nez v1, :cond_5

    #@387
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@389
    const-string v2, "LG-E460"

    #@38b
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@38e
    move-result v1

    #@38f
    if-nez v1, :cond_5

    #@391
    .line 2266
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@393
    const-string v2, "LG-E430"

    #@395
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@398
    move-result v1

    #@399
    if-nez v1, :cond_5

    #@39b
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@39d
    const-string v2, "LG-E430g"

    #@39f
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3a2
    move-result v1

    #@3a3
    if-nez v1, :cond_5

    #@3a5
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@3a7
    const-string v2, "LG-E430f"

    #@3a9
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3ac
    move-result v1

    #@3ad
    if-nez v1, :cond_5

    #@3af
    .line 2270
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@3b1
    const-string v2, "LG-F180L"

    #@3b3
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3b6
    move-result v1

    #@3b7
    if-nez v1, :cond_5

    #@3b9
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@3bb
    const-string v2, "LG-F180K"

    #@3bd
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3c0
    move-result v1

    #@3c1
    if-nez v1, :cond_5

    #@3c3
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@3c5
    const-string v2, "LG-F180S"

    #@3c7
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3ca
    move-result v1

    #@3cb
    if-eqz v1, :cond_5

    #@3cd
    goto/16 :goto_5

    #@3cf
    .line 2280
    :pswitch_3cf
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@3d1
    const-string v3, "LG-E450f"

    #@3d3
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3d6
    move-result v2

    #@3d7
    if-nez v2, :cond_5

    #@3d9
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@3db
    const-string v3, "LG-E455f"

    #@3dd
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3e0
    move-result v2

    #@3e1
    if-nez v2, :cond_5

    #@3e3
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@3e5
    const-string v3, "LG-E460"

    #@3e7
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3ea
    move-result v2

    #@3eb
    if-nez v2, :cond_5

    #@3ed
    .line 2284
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@3ef
    const-string v3, "LG-E430"

    #@3f1
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3f4
    move-result v2

    #@3f5
    if-nez v2, :cond_40b

    #@3f7
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@3f9
    const-string v3, "LG-E430g"

    #@3fb
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3fe
    move-result v2

    #@3ff
    if-nez v2, :cond_40b

    #@401
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@403
    const-string v3, "LG-E430f"

    #@405
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@408
    move-result v2

    #@409
    if-eqz v2, :cond_40e

    #@40b
    :cond_40b
    move v0, v1

    #@40c
    .line 2286
    goto/16 :goto_5

    #@40e
    .line 2288
    :cond_40e
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@410
    const-string v3, "LG-F180L"

    #@412
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@415
    move-result v2

    #@416
    if-eqz v2, :cond_5

    #@418
    move v0, v1

    #@419
    .line 2290
    goto/16 :goto_5

    #@41b
    .line 2298
    :pswitch_41b
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@41d
    const-string v3, "LG-E450f"

    #@41f
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@422
    move-result v2

    #@423
    if-nez v2, :cond_5

    #@425
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@427
    const-string v3, "LG-E455f"

    #@429
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@42c
    move-result v2

    #@42d
    if-nez v2, :cond_5

    #@42f
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@431
    const-string v3, "LG-E460"

    #@433
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@436
    move-result v2

    #@437
    if-nez v2, :cond_5

    #@439
    .line 2302
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@43b
    const-string v3, "LG-E430"

    #@43d
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@440
    move-result v2

    #@441
    if-nez v2, :cond_5

    #@443
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@445
    const-string v3, "LG-E430g"

    #@447
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@44a
    move-result v2

    #@44b
    if-nez v2, :cond_5

    #@44d
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@44f
    const-string v3, "LG-E430f"

    #@451
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@454
    move-result v2

    #@455
    if-nez v2, :cond_5

    #@457
    .line 2306
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@459
    const-string v3, "LG-F180L"

    #@45b
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@45e
    move-result v2

    #@45f
    if-eqz v2, :cond_5

    #@461
    move v0, v1

    #@462
    .line 2308
    goto/16 :goto_5

    #@464
    .line 2316
    :pswitch_464
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@466
    const-string v3, "LG-E450f"

    #@468
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@46b
    move-result v2

    #@46c
    if-nez v2, :cond_5

    #@46e
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@470
    const-string v3, "LG-E455f"

    #@472
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@475
    move-result v2

    #@476
    if-nez v2, :cond_5

    #@478
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@47a
    const-string v3, "LG-E460"

    #@47c
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@47f
    move-result v2

    #@480
    if-nez v2, :cond_5

    #@482
    .line 2320
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@484
    const-string v3, "LG-E430"

    #@486
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@489
    move-result v2

    #@48a
    if-nez v2, :cond_4a0

    #@48c
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@48e
    const-string v3, "LG-E430g"

    #@490
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@493
    move-result v2

    #@494
    if-nez v2, :cond_4a0

    #@496
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@498
    const-string v3, "LG-E430f"

    #@49a
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@49d
    move-result v2

    #@49e
    if-eqz v2, :cond_4a3

    #@4a0
    :cond_4a0
    move v0, v1

    #@4a1
    .line 2322
    goto/16 :goto_5

    #@4a3
    .line 2324
    :cond_4a3
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@4a5
    const-string v2, "LG-F180L"

    #@4a7
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4aa
    move-result v1

    #@4ab
    if-eqz v1, :cond_5

    #@4ad
    goto/16 :goto_5

    #@4af
    .line 2096
    nop

    #@4b0
    :pswitch_data_4b0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_64
        :pswitch_c9
        :pswitch_125
        :pswitch_18b
        :pswitch_1f1
        :pswitch_251
        :pswitch_5
        :pswitch_2b7
        :pswitch_313
        :pswitch_373
        :pswitch_3cf
        :pswitch_41b
        :pswitch_464
    .end packed-switch
.end method

.method public FolderTest_GetDimOnBacklightValue()F
    .registers 4

    #@0
    .prologue
    const v2, 0x3e99999a

    #@3
    .line 2428
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@5
    const-string v1, "LG-E450f"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v0

    #@b
    if-nez v0, :cond_21

    #@d
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@f
    const-string v1, "LG-E455f"

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v0

    #@15
    if-nez v0, :cond_21

    #@17
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@19
    const-string v1, "LG-E460"

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e
    move-result v0

    #@1f
    if-eqz v0, :cond_22

    #@21
    .line 2442
    :cond_21
    :goto_21
    return v2

    #@22
    .line 2432
    :cond_22
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@24
    const-string v1, "LG-E430"

    #@26
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@29
    move-result v0

    #@2a
    if-nez v0, :cond_21

    #@2c
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@2e
    const-string v1, "LG-E430g"

    #@30
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@33
    move-result v0

    #@34
    if-nez v0, :cond_21

    #@36
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@38
    const-string v1, "LG-E430f"

    #@3a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3d
    move-result v0

    #@3e
    if-nez v0, :cond_21

    #@40
    .line 2436
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@42
    const-string v1, "LG-F180L"

    #@44
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@47
    move-result v0

    #@48
    if-nez v0, :cond_21

    #@4a
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@4c
    const-string v1, "LG-F180K"

    #@4e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@51
    move-result v0

    #@52
    if-nez v0, :cond_21

    #@54
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@56
    const-string v1, "LG-F180S"

    #@58
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5b
    move-result v0

    #@5c
    if-eqz v0, :cond_21

    #@5e
    goto :goto_21
.end method

.method public FolderTest_GetFilePath(I)Ljava/lang/String;
    .registers 4
    .parameter

    #@0
    .prologue
    .line 2341
    .line 2349
    const-string v0, ""

    #@2
    .line 2351
    packed-switch p1, :pswitch_data_c6

    #@5
    .line 2422
    :goto_5
    return-object v0

    #@6
    .line 2355
    :pswitch_6
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@8
    const-string v1, "LG-E450f"

    #@a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_24

    #@10
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@12
    const-string v1, "LG-E455f"

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v0

    #@18
    if-nez v0, :cond_24

    #@1a
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@1c
    const-string v1, "LG-E460"

    #@1e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21
    move-result v0

    #@22
    if-eqz v0, :cond_27

    #@24
    .line 2357
    :cond_24
    const-string v0, "Not Supported"

    #@26
    goto :goto_5

    #@27
    .line 2361
    :cond_27
    const-string v0, "/sys/devices/platform/msm_ssbi.0/pm8921-core/pm8xxx-adc/vcoin"

    #@29
    goto :goto_5

    #@2a
    .line 2366
    :pswitch_2a
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@2c
    const-string v1, "LG-E450f"

    #@2e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@31
    move-result v0

    #@32
    if-nez v0, :cond_48

    #@34
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@36
    const-string v1, "LG-E455f"

    #@38
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3b
    move-result v0

    #@3c
    if-nez v0, :cond_48

    #@3e
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@40
    const-string v1, "LG-E460"

    #@42
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@45
    move-result v0

    #@46
    if-eqz v0, :cond_4b

    #@48
    .line 2368
    :cond_48
    const-string v0, "Not Supported"

    #@4a
    goto :goto_5

    #@4b
    .line 2372
    :cond_4b
    const-string v0, "/sys/module/pm8921_charger/parameters/coincell_charging"

    #@4d
    goto :goto_5

    #@4e
    .line 2377
    :pswitch_4e
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@50
    const-string v1, "LG-E450f"

    #@52
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@55
    move-result v0

    #@56
    if-nez v0, :cond_6c

    #@58
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@5a
    const-string v1, "LG-E455f"

    #@5c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5f
    move-result v0

    #@60
    if-nez v0, :cond_6c

    #@62
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@64
    const-string v1, "LG-E460"

    #@66
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@69
    move-result v0

    #@6a
    if-eqz v0, :cond_6f

    #@6c
    .line 2379
    :cond_6c
    const-string v0, "Not Supported"

    #@6e
    goto :goto_5

    #@6f
    .line 2383
    :cond_6f
    const-string v0, "/sys/devices/platform/bu52014hfv/sensing"

    #@71
    goto :goto_5

    #@72
    .line 2389
    :pswitch_72
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@74
    const-string v1, "LG-E450f"

    #@76
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@79
    move-result v0

    #@7a
    if-nez v0, :cond_90

    #@7c
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@7e
    const-string v1, "LG-E455f"

    #@80
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@83
    move-result v0

    #@84
    if-nez v0, :cond_90

    #@86
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@88
    const-string v1, "LG-E460"

    #@8a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8d
    move-result v0

    #@8e
    if-eqz v0, :cond_94

    #@90
    .line 2391
    :cond_90
    const-string v0, "/system/media/audio/ringtones/"

    #@92
    goto/16 :goto_5

    #@94
    .line 2395
    :cond_94
    const-string v0, "/system/media/audio/ringtones/"

    #@96
    goto/16 :goto_5

    #@98
    .line 2400
    :pswitch_98
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@9a
    const-string v1, "LG-E450f"

    #@9c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9f
    move-result v0

    #@a0
    if-nez v0, :cond_b6

    #@a2
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@a4
    const-string v1, "LG-E455f"

    #@a6
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a9
    move-result v0

    #@aa
    if-nez v0, :cond_b6

    #@ac
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@ae
    const-string v1, "LG-E460"

    #@b0
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b3
    move-result v0

    #@b4
    if-eqz v0, :cond_ba

    #@b6
    .line 2402
    :cond_b6
    const-string v0, "Not Supported"

    #@b8
    goto/16 :goto_5

    #@ba
    .line 2406
    :cond_ba
    const-string v0, "/sys/devices/virtual/input/lge_touch/fw_ver"

    #@bc
    goto/16 :goto_5

    #@be
    .line 2411
    :pswitch_be
    const-string v0, "/system/sounds/lgeSounds/test_left.mp3"

    #@c0
    goto/16 :goto_5

    #@c2
    .line 2415
    :pswitch_c2
    const-string v0, "/system/sounds/lgeSounds/test_right.mp3"

    #@c4
    goto/16 :goto_5

    #@c6
    .line 2351
    :pswitch_data_c6
    .packed-switch 0x65
        :pswitch_6
        :pswitch_2a
        :pswitch_4e
        :pswitch_72
        :pswitch_98
        :pswitch_be
        :pswitch_c2
    .end packed-switch
.end method

.method public FolderTest_GetSupportedMenuList()[Z
    .registers 7

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 2009
    const-string v0, "AAT"

    #@4
    new-instance v3, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v4, "[AATService] FolderTest_GetSupportedMenuList model_name: "

    #@b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v3

    #@f
    sget-object v4, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@11
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v3

    #@15
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v3

    #@19
    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 2011
    const/16 v0, 0x8

    #@1e
    new-array v4, v0, [Z

    #@20
    .line 2022
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@22
    const-string v3, "LG-E450f"

    #@24
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@27
    move-result v0

    #@28
    if-nez v0, :cond_3e

    #@2a
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@2c
    const-string v3, "LG-E455f"

    #@2e
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@31
    move-result v0

    #@32
    if-nez v0, :cond_3e

    #@34
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@36
    const-string v3, "LG-E460"

    #@38
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3b
    move-result v0

    #@3c
    if-eqz v0, :cond_57

    #@3e
    :cond_3e
    move v0, v1

    #@3f
    move v3, v2

    #@40
    .line 2069
    :goto_40
    aput-boolean v1, v4, v2

    #@42
    .line 2070
    aput-boolean v1, v4, v1

    #@44
    .line 2071
    const/4 v5, 0x2

    #@45
    aput-boolean v3, v4, v5

    #@47
    .line 2072
    const/4 v3, 0x3

    #@48
    aput-boolean v1, v4, v3

    #@4a
    .line 2073
    const/4 v3, 0x4

    #@4b
    aput-boolean v0, v4, v3

    #@4d
    .line 2074
    const/4 v0, 0x5

    #@4e
    aput-boolean v1, v4, v0

    #@50
    .line 2075
    const/4 v0, 0x6

    #@51
    aput-boolean v2, v4, v0

    #@53
    .line 2076
    const/4 v0, 0x7

    #@54
    aput-boolean v1, v4, v0

    #@56
    .line 2078
    return-object v4

    #@57
    .line 2033
    :cond_57
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@59
    const-string v3, "LG-E430"

    #@5b
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5e
    move-result v0

    #@5f
    if-nez v0, :cond_75

    #@61
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@63
    const-string v3, "LG-E430g"

    #@65
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@68
    move-result v0

    #@69
    if-nez v0, :cond_75

    #@6b
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@6d
    const-string v3, "LG-E430f"

    #@6f
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@72
    move-result v0

    #@73
    if-eqz v0, :cond_78

    #@75
    :cond_75
    move v0, v2

    #@76
    move v3, v2

    #@77
    .line 2042
    goto :goto_40

    #@78
    .line 2044
    :cond_78
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@7a
    const-string v3, "LG-F180L"

    #@7c
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7f
    move-result v0

    #@80
    if-nez v0, :cond_96

    #@82
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@84
    const-string v3, "LG-F180K"

    #@86
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@89
    move-result v0

    #@8a
    if-nez v0, :cond_96

    #@8c
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@8e
    const-string v3, "LG-F180S"

    #@90
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@93
    move-result v0

    #@94
    if-eqz v0, :cond_99

    #@96
    :cond_96
    move v0, v1

    #@97
    move v3, v1

    #@98
    .line 2054
    goto :goto_40

    #@99
    :cond_99
    move v0, v1

    #@9a
    move v3, v1

    #@9b
    .line 2066
    goto :goto_40
.end method

.method public GetUsbOnOffValue()I
    .registers 6

    #@0
    .prologue
    .line 2448
    const-string v0, ""

    #@2
    .line 2450
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@4
    const-string v1, "DS1201"

    #@6
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_16

    #@c
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@e
    const-string v1, "L-04E"

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13
    move-result v0

    #@14
    if-eqz v0, :cond_36

    #@16
    .line 2452
    :cond_16
    const-string v0, "sys/class/power_supply/usb/online"

    #@18
    .line 2460
    :goto_18
    const/4 v1, 0x0

    #@19
    .line 2461
    const/4 v3, 0x0

    #@1a
    .line 2465
    :try_start_1a
    new-instance v2, Ljava/io/BufferedReader;

    #@1c
    new-instance v4, Ljava/io/FileReader;

    #@1e
    invoke-direct {v4, v0}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    #@21
    const/16 v0, 0x8

    #@23
    invoke-direct {v2, v4, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_26
    .catchall {:try_start_1a .. :try_end_26} :catchall_50
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_26} :catch_3e

    #@26
    .line 2466
    :try_start_26
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@29
    move-result-object v0

    #@2a
    .line 2467
    if-eqz v0, :cond_62

    #@2c
    .line 2469
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_2f
    .catchall {:try_start_26 .. :try_end_2f} :catchall_5d
    .catch Ljava/io/IOException; {:try_start_26 .. :try_end_2f} :catch_60

    #@2f
    move-result v0

    #@30
    .line 2482
    :goto_30
    if-eqz v2, :cond_35

    #@32
    .line 2484
    :try_start_32
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_35
    .catch Ljava/io/IOException; {:try_start_32 .. :try_end_35} :catch_39

    #@35
    .line 2498
    :cond_35
    :goto_35
    return v0

    #@36
    .line 2456
    :cond_36
    const-string v0, "sys/class/power_supply/usb/online"

    #@38
    goto :goto_18

    #@39
    .line 2492
    :catch_39
    move-exception v1

    #@3a
    .line 2494
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@3d
    goto :goto_35

    #@3e
    .line 2474
    :catch_3e
    move-exception v0

    #@3f
    move-object v2, v3

    #@40
    .line 2476
    :goto_40
    :try_start_40
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_43
    .catchall {:try_start_40 .. :try_end_43} :catchall_5d

    #@43
    .line 2482
    if-eqz v2, :cond_48

    #@45
    .line 2484
    :try_start_45
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_48
    .catch Ljava/io/IOException; {:try_start_45 .. :try_end_48} :catch_4a

    #@48
    :cond_48
    move v0, v1

    #@49
    .line 2490
    goto :goto_35

    #@4a
    .line 2492
    :catch_4a
    move-exception v0

    #@4b
    .line 2494
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@4e
    move v0, v1

    #@4f
    .line 2496
    goto :goto_35

    #@50
    .line 2480
    :catchall_50
    move-exception v0

    #@51
    .line 2482
    :goto_51
    if-eqz v3, :cond_56

    #@53
    .line 2484
    :try_start_53
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_56
    .catch Ljava/io/IOException; {:try_start_53 .. :try_end_56} :catch_58

    #@56
    :cond_56
    move v0, v1

    #@57
    .line 2490
    goto :goto_35

    #@58
    .line 2492
    :catch_58
    move-exception v1

    #@59
    .line 2494
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@5c
    .line 2495
    throw v0

    #@5d
    .line 2480
    :catchall_5d
    move-exception v0

    #@5e
    move-object v3, v2

    #@5f
    goto :goto_51

    #@60
    .line 2474
    :catch_60
    move-exception v0

    #@61
    goto :goto_40

    #@62
    :cond_62
    move v0, v1

    #@63
    goto :goto_30
.end method

.method public IsFMRadio()Z
    .registers 2

    #@0
    .prologue
    .line 1739
    iget-object v0, p0, Lcom/lge/systemservice/service/AATService;->mFmRadioMgr:Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;

    #@2
    if-eqz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public IsSupportAutoFocus()Z
    .registers 2

    #@0
    .prologue
    .line 1930
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public IsSupportBarometer()Z
    .registers 2

    #@0
    .prologue
    .line 1570
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public IsSupportGLOTestGps()Z
    .registers 2

    #@0
    .prologue
    .line 942
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public IsSupportHookKeyTest()Z
    .registers 2

    #@0
    .prologue
    .line 1575
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public IsSupportMura()Z
    .registers 2

    #@0
    .prologue
    .line 1735
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public IsSupportProximityCalibration()Z
    .registers 2

    #@0
    .prologue
    .line 1563
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public IsSupportSubMic()Z
    .registers 4

    #@0
    .prologue
    .line 1933
    const-string v0, "AAT"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[AATService] IsSupportSubMic : model_name : "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    sget-object v2, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 1934
    const-string v0, "LG-E435"

    #@1c
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@1e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21
    move-result v0

    #@22
    if-nez v0, :cond_56

    #@24
    const-string v0, "LG-E435f"

    #@26
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@28
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2b
    move-result v0

    #@2c
    if-nez v0, :cond_56

    #@2e
    const-string v0, "LG-E435g"

    #@30
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@32
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@35
    move-result v0

    #@36
    if-nez v0, :cond_56

    #@38
    const-string v0, "LG-E430"

    #@3a
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@3c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3f
    move-result v0

    #@40
    if-nez v0, :cond_56

    #@42
    const-string v0, "LG-E430f"

    #@44
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@46
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@49
    move-result v0

    #@4a
    if-nez v0, :cond_56

    #@4c
    const-string v0, "LG-E430g"

    #@4e
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@50
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@53
    move-result v0

    #@54
    if-eqz v0, :cond_58

    #@56
    .line 1937
    :cond_56
    const/4 v0, 0x0

    #@57
    .line 1941
    :goto_57
    return v0

    #@58
    :cond_58
    const/4 v0, 0x1

    #@59
    goto :goto_57
.end method

.method public IsSupportUSIM()Z
    .registers 2

    #@0
    .prologue
    .line 1731
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public NFC_Disable()Z
    .registers 6

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1023
    .line 1024
    const/16 v2, 0x80

    #@4
    new-array v2, v2, [B

    #@6
    .line 1026
    const-string v3, "AAT"

    #@8
    const-string v4, "[AATService] AAT_nfcDisable : "

    #@a
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 1028
    sget-boolean v3, Lcom/lge/systemservice/service/AATService;->mIsAdapterEnabled:Z

    #@f
    if-nez v3, :cond_29

    #@11
    .line 1030
    iget-object v3, p0, Lcom/lge/systemservice/service/AATService;->mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@13
    if-eqz v3, :cond_29

    #@15
    .line 1032
    iget-object v3, p0, Lcom/lge/systemservice/service/AATService;->mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@17
    invoke-virtual {v3}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;->createNfcFactoryObj()Z

    #@1a
    move-result v3

    #@1b
    sput-boolean v3, Lcom/lge/systemservice/service/AATService;->mIsAdapterEnabled:Z

    #@1d
    .line 1033
    const-wide/16 v3, 0x12c

    #@1f
    invoke-static {v3, v4}, Landroid/os/SystemClock;->sleep(J)V

    #@22
    .line 1034
    const-string v3, "AAT"

    #@24
    const-string v4, "[AATService] createNfcFactoryObj"

    #@26
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 1042
    :cond_29
    sget-boolean v3, Lcom/lge/systemservice/service/AATService;->mIsAdapterEnabled:Z

    #@2b
    if-nez v3, :cond_35

    #@2d
    .line 1044
    const-string v0, "AAT"

    #@2f
    const-string v2, "[AATService] TestEnvironment not ready"

    #@31
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 1065
    :goto_34
    return v1

    #@35
    .line 1050
    :cond_35
    iget-object v3, p0, Lcom/lge/systemservice/service/AATService;->mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@37
    if-eqz v3, :cond_40

    #@39
    .line 1052
    iget-object v3, p0, Lcom/lge/systemservice/service/AATService;->mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@3b
    const/16 v4, 0x15

    #@3d
    invoke-virtual {v3, v4, v2}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;->handleNfcTest(I[B)Ljava/lang/String;

    #@40
    .line 1057
    :cond_40
    aget-byte v2, v2, v1

    #@42
    if-ne v2, v0, :cond_46

    #@44
    :goto_44
    move v1, v0

    #@45
    .line 1065
    goto :goto_34

    #@46
    :cond_46
    move v0, v1

    #@47
    .line 1063
    goto :goto_44
.end method

.method public NFC_Enable()Z
    .registers 6

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 974
    .line 976
    const/16 v2, 0x80

    #@4
    new-array v2, v2, [B

    #@6
    .line 978
    const-string v3, "AAT"

    #@8
    const-string v4, "[AATService] AAT_nfcEnable : "

    #@a
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 980
    sget-boolean v3, Lcom/lge/systemservice/service/AATService;->mIsAdapterEnabled:Z

    #@f
    if-nez v3, :cond_29

    #@11
    .line 982
    iget-object v3, p0, Lcom/lge/systemservice/service/AATService;->mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@13
    if-eqz v3, :cond_29

    #@15
    .line 984
    iget-object v3, p0, Lcom/lge/systemservice/service/AATService;->mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@17
    invoke-virtual {v3}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;->createNfcFactoryObj()Z

    #@1a
    move-result v3

    #@1b
    sput-boolean v3, Lcom/lge/systemservice/service/AATService;->mIsAdapterEnabled:Z

    #@1d
    .line 985
    const-wide/16 v3, 0x12c

    #@1f
    invoke-static {v3, v4}, Landroid/os/SystemClock;->sleep(J)V

    #@22
    .line 986
    const-string v3, "AAT"

    #@24
    const-string v4, "[AATService] createNfcFactoryObj"

    #@26
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 994
    :cond_29
    sget-boolean v3, Lcom/lge/systemservice/service/AATService;->mIsAdapterEnabled:Z

    #@2b
    if-nez v3, :cond_35

    #@2d
    .line 996
    const-string v0, "AAT"

    #@2f
    const-string v2, "[AATService] TestEnvironment not ready"

    #@31
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 1018
    :goto_34
    return v1

    #@35
    .line 1002
    :cond_35
    iget-object v3, p0, Lcom/lge/systemservice/service/AATService;->mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@37
    if-eqz v3, :cond_40

    #@39
    .line 1004
    iget-object v3, p0, Lcom/lge/systemservice/service/AATService;->mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@3b
    const/16 v4, 0x14

    #@3d
    invoke-virtual {v3, v4, v2}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;->handleNfcTest(I[B)Ljava/lang/String;

    #@40
    .line 1009
    :cond_40
    aget-byte v2, v2, v1

    #@42
    if-ne v2, v0, :cond_46

    #@44
    :goto_44
    move v1, v0

    #@45
    .line 1018
    goto :goto_34

    #@46
    :cond_46
    move v0, v1

    #@47
    .line 1015
    goto :goto_44
.end method

.method public NFC_Off([B)I
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 1117
    .line 1118
    const/16 v2, 0x80

    #@4
    new-array v2, v2, [B

    #@6
    .line 1120
    const-string v3, "AAT"

    #@8
    const-string v4, "[AATService] AAT_atCmd_NFC_Off : "

    #@a
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 1122
    sget-boolean v3, Lcom/lge/systemservice/service/AATService;->mIsAdapterEnabled:Z

    #@f
    if-nez v3, :cond_1d

    #@11
    .line 1124
    iget-object v3, p0, Lcom/lge/systemservice/service/AATService;->mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@13
    if-eqz v3, :cond_1d

    #@15
    .line 1126
    iget-object v3, p0, Lcom/lge/systemservice/service/AATService;->mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@17
    invoke-virtual {v3}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;->createNfcFactoryObj()Z

    #@1a
    move-result v3

    #@1b
    sput-boolean v3, Lcom/lge/systemservice/service/AATService;->mIsAdapterEnabled:Z

    #@1d
    .line 1134
    :cond_1d
    sget-boolean v3, Lcom/lge/systemservice/service/AATService;->mIsAdapterEnabled:Z

    #@1f
    if-nez v3, :cond_29

    #@21
    .line 1136
    const-string v0, "AAT"

    #@23
    const-string v2, "[AATService] TestEnvironment not ready"

    #@25
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 1157
    :goto_28
    return v1

    #@29
    .line 1142
    :cond_29
    iget-object v3, p0, Lcom/lge/systemservice/service/AATService;->mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@2b
    if-eqz v3, :cond_32

    #@2d
    .line 1144
    iget-object v3, p0, Lcom/lge/systemservice/service/AATService;->mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@2f
    invoke-virtual {v3, v1, v2}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;->handleNfcTest(I[B)Ljava/lang/String;

    #@32
    .line 1149
    :cond_32
    aget-byte v2, v2, v0

    #@34
    if-ne v2, v1, :cond_38

    #@36
    :goto_36
    move v1, v0

    #@37
    .line 1157
    goto :goto_28

    #@38
    :cond_38
    move v0, v1

    #@39
    .line 1155
    goto :goto_36
.end method

.method public NFC_On([B)I
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v0, 0x0

    #@2
    .line 1070
    .line 1071
    const/16 v2, 0x80

    #@4
    new-array v2, v2, [B

    #@6
    .line 1073
    const-string v3, "AAT"

    #@8
    const-string v4, "[AATService] AAT_atCmd_NFC_On : "

    #@a
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 1075
    sget-boolean v3, Lcom/lge/systemservice/service/AATService;->mIsAdapterEnabled:Z

    #@f
    if-nez v3, :cond_29

    #@11
    .line 1077
    iget-object v3, p0, Lcom/lge/systemservice/service/AATService;->mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@13
    if-eqz v3, :cond_29

    #@15
    .line 1079
    iget-object v3, p0, Lcom/lge/systemservice/service/AATService;->mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@17
    invoke-virtual {v3}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;->createNfcFactoryObj()Z

    #@1a
    move-result v3

    #@1b
    sput-boolean v3, Lcom/lge/systemservice/service/AATService;->mIsAdapterEnabled:Z

    #@1d
    .line 1080
    const-wide/16 v3, 0x12c

    #@1f
    invoke-static {v3, v4}, Landroid/os/SystemClock;->sleep(J)V

    #@22
    .line 1081
    const-string v3, "AAT"

    #@24
    const-string v4, "[AATService] createNfcFactoryObj"

    #@26
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 1089
    :cond_29
    sget-boolean v3, Lcom/lge/systemservice/service/AATService;->mIsAdapterEnabled:Z

    #@2b
    if-nez v3, :cond_35

    #@2d
    .line 1091
    const-string v0, "AAT"

    #@2f
    const-string v2, "[AATService] TestEnvironment not ready"

    #@31
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 1112
    :goto_34
    return v1

    #@35
    .line 1097
    :cond_35
    iget-object v3, p0, Lcom/lge/systemservice/service/AATService;->mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@37
    if-eqz v3, :cond_3e

    #@39
    .line 1099
    iget-object v3, p0, Lcom/lge/systemservice/service/AATService;->mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@3b
    invoke-virtual {v3, v0, v2}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;->handleNfcTest(I[B)Ljava/lang/String;

    #@3e
    .line 1104
    :cond_3e
    aget-byte v2, v2, v0

    #@40
    if-ne v2, v1, :cond_44

    #@42
    :goto_42
    move v1, v0

    #@43
    .line 1112
    goto :goto_34

    #@44
    :cond_44
    move v0, v1

    #@45
    .line 1110
    goto :goto_42
.end method

.method public NFC_Reader([B)I
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1254
    .line 1255
    const/16 v2, 0x80

    #@4
    new-array v2, v2, [B

    #@6
    .line 1257
    const-string v3, "AAT"

    #@8
    const-string v4, "[AATService] AAT_atCmd_NFC_Reader : "

    #@a
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 1259
    sget-boolean v3, Lcom/lge/systemservice/service/AATService;->mIsAdapterEnabled:Z

    #@f
    if-nez v3, :cond_1d

    #@11
    .line 1261
    iget-object v3, p0, Lcom/lge/systemservice/service/AATService;->mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@13
    if-eqz v3, :cond_1d

    #@15
    .line 1263
    iget-object v3, p0, Lcom/lge/systemservice/service/AATService;->mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@17
    invoke-virtual {v3}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;->createNfcFactoryObj()Z

    #@1a
    move-result v3

    #@1b
    sput-boolean v3, Lcom/lge/systemservice/service/AATService;->mIsAdapterEnabled:Z

    #@1d
    .line 1271
    :cond_1d
    sget-boolean v3, Lcom/lge/systemservice/service/AATService;->mIsAdapterEnabled:Z

    #@1f
    if-nez v3, :cond_29

    #@21
    .line 1273
    const-string v0, "AAT"

    #@23
    const-string v2, "[AATService] TestEnvironment not ready"

    #@25
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 1294
    :goto_28
    return v1

    #@29
    .line 1279
    :cond_29
    iget-object v3, p0, Lcom/lge/systemservice/service/AATService;->mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@2b
    if-eqz v3, :cond_33

    #@2d
    .line 1281
    iget-object v3, p0, Lcom/lge/systemservice/service/AATService;->mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@2f
    const/4 v4, 0x4

    #@30
    invoke-virtual {v3, v4, v2}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;->handleNfcTest(I[B)Ljava/lang/String;

    #@33
    .line 1286
    :cond_33
    aget-byte v2, v2, v1

    #@35
    if-ne v2, v0, :cond_39

    #@37
    :goto_37
    move v1, v0

    #@38
    .line 1294
    goto :goto_28

    #@39
    :cond_39
    move v0, v1

    #@3a
    .line 1292
    goto :goto_37
.end method

.method public NFC_SmartMX([B)I
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1209
    .line 1210
    const/16 v0, 0x80

    #@3
    new-array v2, v0, [B

    #@5
    .line 1211
    const/4 v0, 0x0

    #@6
    .line 1212
    const-string v3, "AAT"

    #@8
    const-string v4, "[AATService] AAT_atCmd_NFC_SmartMX : "

    #@a
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 1214
    sget-boolean v3, Lcom/lge/systemservice/service/AATService;->mIsAdapterEnabled:Z

    #@f
    if-nez v3, :cond_1d

    #@11
    .line 1216
    iget-object v3, p0, Lcom/lge/systemservice/service/AATService;->mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@13
    if-eqz v3, :cond_1d

    #@15
    .line 1218
    iget-object v3, p0, Lcom/lge/systemservice/service/AATService;->mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@17
    invoke-virtual {v3}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;->createNfcFactoryObj()Z

    #@1a
    move-result v3

    #@1b
    sput-boolean v3, Lcom/lge/systemservice/service/AATService;->mIsAdapterEnabled:Z

    #@1d
    .line 1226
    :cond_1d
    sget-boolean v3, Lcom/lge/systemservice/service/AATService;->mIsAdapterEnabled:Z

    #@1f
    if-nez v3, :cond_29

    #@21
    .line 1228
    const-string v0, "AAT"

    #@23
    const-string v2, "[AATService] TestEnvironment not ready"

    #@25
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 1249
    :goto_28
    return v1

    #@29
    .line 1234
    :cond_29
    iget-object v3, p0, Lcom/lge/systemservice/service/AATService;->mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@2b
    if-eqz v3, :cond_35

    #@2d
    .line 1236
    iget-object v0, p0, Lcom/lge/systemservice/service/AATService;->mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@2f
    const/16 v3, 0xe

    #@31
    invoke-virtual {v0, v3, v2}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;->handleNfcTest(I[B)Ljava/lang/String;

    #@34
    move-result-object v0

    #@35
    .line 1241
    :cond_35
    aget-byte v2, v2, v1

    #@37
    if-lez v2, :cond_3e

    #@39
    if-eqz v0, :cond_3e

    #@3b
    .line 1243
    const/4 v0, 0x1

    #@3c
    :goto_3c
    move v1, v0

    #@3d
    .line 1249
    goto :goto_28

    #@3e
    :cond_3e
    move v0, v1

    #@3f
    .line 1247
    goto :goto_3c
.end method

.method public NFC_Swp([B)I
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x2

    #@1
    const/4 v0, 0x1

    #@2
    const/4 v1, 0x0

    #@3
    .line 1162
    .line 1163
    const/16 v2, 0x80

    #@5
    new-array v2, v2, [B

    #@7
    .line 1165
    const-string v3, "AAT"

    #@9
    const-string v4, "[AATService] AAT_atCmd_NFC_Swp : "

    #@b
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 1167
    sget-boolean v3, Lcom/lge/systemservice/service/AATService;->mIsAdapterEnabled:Z

    #@10
    if-nez v3, :cond_1e

    #@12
    .line 1169
    iget-object v3, p0, Lcom/lge/systemservice/service/AATService;->mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@14
    if-eqz v3, :cond_1e

    #@16
    .line 1171
    iget-object v3, p0, Lcom/lge/systemservice/service/AATService;->mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@18
    invoke-virtual {v3}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;->createNfcFactoryObj()Z

    #@1b
    move-result v3

    #@1c
    sput-boolean v3, Lcom/lge/systemservice/service/AATService;->mIsAdapterEnabled:Z

    #@1e
    .line 1179
    :cond_1e
    sget-boolean v3, Lcom/lge/systemservice/service/AATService;->mIsAdapterEnabled:Z

    #@20
    if-nez v3, :cond_2a

    #@22
    .line 1181
    const-string v0, "AAT"

    #@24
    const-string v2, "[AATService] TestEnvironment not ready"

    #@26
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 1203
    :goto_29
    return v1

    #@2a
    .line 1187
    :cond_2a
    iget-object v3, p0, Lcom/lge/systemservice/service/AATService;->mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@2c
    if-eqz v3, :cond_33

    #@2e
    .line 1189
    iget-object v3, p0, Lcom/lge/systemservice/service/AATService;->mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@30
    invoke-virtual {v3, v5, v2}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;->handleNfcTest(I[B)Ljava/lang/String;

    #@33
    .line 1194
    :cond_33
    aget-byte v3, v2, v1

    #@35
    if-ne v3, v0, :cond_3d

    #@37
    .line 1197
    aget-byte v1, v2, v0

    #@39
    aput-byte v1, p1, v5

    #@3b
    :goto_3b
    move v1, v0

    #@3c
    .line 1203
    goto :goto_29

    #@3d
    :cond_3d
    move v0, v1

    #@3e
    .line 1201
    goto :goto_3b
.end method

.method public QwertyLedOff()V
    .registers 1

    #@0
    .prologue
    .line 1330
    return-void
.end method

.method public QwertyLedOn()V
    .registers 1

    #@0
    .prologue
    .line 1326
    return-void
.end method

.method ReadOriginalLcdBrightValue()V
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1836
    .line 1839
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    #@3
    new-instance v0, Ljava/io/FileReader;

    #@5
    const-string v3, "/sys/class/leds/lcd-backlight/brightness"

    #@7
    invoke-direct {v0, v3}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    #@a
    invoke-direct {v1, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_5d
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_d} :catch_3d
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_d} :catch_4d

    #@d
    .line 1840
    :try_start_d
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@10
    move-result-object v0

    #@11
    iput-object v0, p0, Lcom/lge/systemservice/service/AATService;->mDefaultLcdBrightValue:Ljava/lang/String;

    #@13
    .line 1841
    const-string v0, "AAT"

    #@15
    new-instance v3, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v4, "[AATService] Default Brightness Value: "

    #@1c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    iget-object v4, p0, Lcom/lge/systemservice/service/AATService;->mDefaultLcdBrightValue:Ljava/lang/String;

    #@22
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 1842
    if-eqz v1, :cond_70

    #@2f
    .line 1844
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_32
    .catchall {:try_start_d .. :try_end_32} :catchall_6a
    .catch Ljava/io/FileNotFoundException; {:try_start_d .. :try_end_32} :catch_6e
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_32} :catch_6c

    #@32
    .line 1862
    :goto_32
    if-eqz v2, :cond_37

    #@34
    .line 1864
    :try_start_34
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_37
    .catch Ljava/io/IOException; {:try_start_34 .. :try_end_37} :catch_38

    #@37
    .line 1875
    :cond_37
    :goto_37
    return-void

    #@38
    .line 1870
    :catch_38
    move-exception v0

    #@39
    .line 1872
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@3c
    goto :goto_37

    #@3d
    .line 1850
    :catch_3d
    move-exception v0

    #@3e
    move-object v1, v2

    #@3f
    .line 1852
    :goto_3f
    :try_start_3f
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_42
    .catchall {:try_start_3f .. :try_end_42} :catchall_6a

    #@42
    .line 1862
    if-eqz v1, :cond_37

    #@44
    .line 1864
    :try_start_44
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_47
    .catch Ljava/io/IOException; {:try_start_44 .. :try_end_47} :catch_48

    #@47
    goto :goto_37

    #@48
    .line 1870
    :catch_48
    move-exception v0

    #@49
    .line 1872
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@4c
    goto :goto_37

    #@4d
    .line 1854
    :catch_4d
    move-exception v0

    #@4e
    move-object v1, v2

    #@4f
    .line 1856
    :goto_4f
    :try_start_4f
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_52
    .catchall {:try_start_4f .. :try_end_52} :catchall_6a

    #@52
    .line 1862
    if-eqz v1, :cond_37

    #@54
    .line 1864
    :try_start_54
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_57
    .catch Ljava/io/IOException; {:try_start_54 .. :try_end_57} :catch_58

    #@57
    goto :goto_37

    #@58
    .line 1870
    :catch_58
    move-exception v0

    #@59
    .line 1872
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@5c
    goto :goto_37

    #@5d
    .line 1860
    :catchall_5d
    move-exception v0

    #@5e
    move-object v1, v2

    #@5f
    .line 1862
    :goto_5f
    if-eqz v1, :cond_64

    #@61
    .line 1864
    :try_start_61
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_64
    .catch Ljava/io/IOException; {:try_start_61 .. :try_end_64} :catch_65

    #@64
    .line 1873
    :cond_64
    :goto_64
    throw v0

    #@65
    .line 1870
    :catch_65
    move-exception v1

    #@66
    .line 1872
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@69
    goto :goto_64

    #@6a
    .line 1860
    :catchall_6a
    move-exception v0

    #@6b
    goto :goto_5f

    #@6c
    .line 1854
    :catch_6c
    move-exception v0

    #@6d
    goto :goto_4f

    #@6e
    .line 1850
    :catch_6e
    move-exception v0

    #@6f
    goto :goto_3f

    #@70
    :cond_70
    move-object v2, v1

    #@71
    goto :goto_32
.end method

.method public RebootforModeChange(II)V
    .registers 8
    .parameter
    .parameter

    #@0
    .prologue
    .line 1588
    iget v0, p0, Lcom/lge/systemservice/service/AATService;->MISC_BLK_SIZE:I

    #@2
    mul-int/2addr v0, p1

    #@3
    .line 1589
    new-instance v3, Ljava/io/File;

    #@5
    const-string v1, "/dev/block/platform/msm_sdcc.1/by-name/misc"

    #@7
    invoke-direct {v3, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@a
    .line 1590
    const/4 v2, 0x0

    #@b
    .line 1592
    const-string v1, "AAT"

    #@d
    const-string v4, "[AATService] AccessMiscWrite, check"

    #@f
    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 1596
    :try_start_12
    new-instance v1, Ljava/io/RandomAccessFile;

    #@14
    const-string v4, "rw"

    #@16
    invoke-direct {v1, v3, v4}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_19
    .catchall {:try_start_12 .. :try_end_19} :catchall_4a
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_19} :catch_28

    #@19
    .line 1598
    if-eqz v1, :cond_22

    #@1b
    .line 1600
    int-to-long v2, v0

    #@1c
    :try_start_1c
    invoke-virtual {v1, v2, v3}, Ljava/io/RandomAccessFile;->seek(J)V

    #@1f
    .line 1601
    invoke-virtual {v1, p2}, Ljava/io/RandomAccessFile;->writeByte(I)V
    :try_end_22
    .catchall {:try_start_1c .. :try_end_22} :catchall_56
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_22} :catch_58

    #@22
    .line 1615
    :cond_22
    if-eqz v1, :cond_27

    #@24
    .line 1617
    :try_start_24
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_27
    .catch Ljava/lang/Exception; {:try_start_24 .. :try_end_27} :catch_52

    #@27
    .line 1628
    :cond_27
    :goto_27
    return-void

    #@28
    .line 1607
    :catch_28
    move-exception v0

    #@29
    move-object v1, v2

    #@2a
    .line 1609
    :goto_2a
    :try_start_2a
    const-string v2, "AAT"

    #@2c
    new-instance v3, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v4, "[AATService] AccessMiscWrite, error : "

    #@33
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v0

    #@3f
    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_42
    .catchall {:try_start_2a .. :try_end_42} :catchall_56

    #@42
    .line 1615
    if-eqz v1, :cond_27

    #@44
    .line 1617
    :try_start_44
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_47
    .catch Ljava/lang/Exception; {:try_start_44 .. :try_end_47} :catch_48

    #@47
    goto :goto_27

    #@48
    .line 1622
    :catch_48
    move-exception v0

    #@49
    goto :goto_27

    #@4a
    .line 1613
    :catchall_4a
    move-exception v0

    #@4b
    move-object v1, v2

    #@4c
    .line 1615
    :goto_4c
    if-eqz v1, :cond_51

    #@4e
    .line 1617
    :try_start_4e
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_51
    .catch Ljava/lang/Exception; {:try_start_4e .. :try_end_51} :catch_54

    #@51
    .line 1623
    :cond_51
    :goto_51
    throw v0

    #@52
    .line 1622
    :catch_52
    move-exception v0

    #@53
    goto :goto_27

    #@54
    :catch_54
    move-exception v1

    #@55
    goto :goto_51

    #@56
    .line 1613
    :catchall_56
    move-exception v0

    #@57
    goto :goto_4c

    #@58
    .line 1607
    :catch_58
    move-exception v0

    #@59
    goto :goto_2a
.end method

.method public SetLoopbackParam(I)Ljava/lang/String;
    .registers 6
    .parameter

    #@0
    .prologue
    .line 911
    const-string v0, "AAT"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[AATService] requestToService, data : "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 913
    packed-switch p1, :pswitch_data_52

    #@1b
    .line 933
    const-string v0, "invaild mode number"

    #@1d
    .line 936
    :goto_1d
    const-string v1, "AAT"

    #@1f
    new-instance v2, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v3, "[AATService] SetLoopbackParam, res : "

    #@26
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v2

    #@2a
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v2

    #@2e
    const-string v3, ", mode : "

    #@30
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@37
    move-result-object v2

    #@38
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v2

    #@3c
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    .line 937
    return-object v0

    #@40
    .line 915
    :pswitch_40
    const-string v0, "loopback=rcv_on"

    #@42
    goto :goto_1d

    #@43
    .line 918
    :pswitch_43
    const-string v0, "loopback=rcv_off"

    #@45
    goto :goto_1d

    #@46
    .line 921
    :pswitch_46
    const-string v0, "loopback=spk_on"

    #@48
    goto :goto_1d

    #@49
    .line 924
    :pswitch_49
    const-string v0, "loopback=spk_off"

    #@4b
    goto :goto_1d

    #@4c
    .line 927
    :pswitch_4c
    const-string v0, "loopback=hds_on"

    #@4e
    goto :goto_1d

    #@4f
    .line 930
    :pswitch_4f
    const-string v0, "loopback=hds_off"

    #@51
    goto :goto_1d

    #@52
    .line 913
    :pswitch_data_52
    .packed-switch 0x0
        :pswitch_40
        :pswitch_43
        :pswitch_46
        :pswitch_49
        :pswitch_4c
        :pswitch_4f
    .end packed-switch
.end method

.method public Start_AccCalibration([F)I
    .registers 9
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    .line 1518
    .line 1520
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@5
    move-result-object v0

    #@6
    .line 1521
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@9
    move-result-object v1

    #@a
    .line 1522
    const-string v2, "AAT"

    #@c
    new-instance v3, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v4, "[AATService] Start_AccCalibration, returnval[0] : "

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    aget v4, p1, v6

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    const-string v4, ", returnval[1] : "

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    aget v4, p1, v5

    #@25
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    const-string v4, ", returnval[2] : "

    #@2b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    const/4 v4, 0x2

    #@30
    aget v4, p1, v4

    #@32
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@35
    move-result-object v3

    #@36
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v3

    #@3a
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 1524
    new-instance v2, Lcom/qualcomm/sensors/sensortest/SensorID;

    #@3f
    sget-object v3, Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;->ACCEL:Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;

    #@41
    invoke-direct {v2, v3, v6}, Lcom/qualcomm/sensors/sensortest/SensorID;-><init>(Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;I)V

    #@44
    sget-object v3, Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;->PRIMARY:Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;

    #@46
    sget-object v4, Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;->SELFTEST:Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;

    #@48
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    #@4b
    move-result v0

    #@4c
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    #@4f
    move-result v1

    #@50
    invoke-static {v2, v3, v4, v0, v1}, Lcom/qualcomm/sensors/sensortest/SensorTest;->runSensorTest(Lcom/qualcomm/sensors/sensortest/SensorID;Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;ZZ)I

    #@53
    move-result v0

    #@54
    .line 1528
    return v0
.end method

.method public Start_ProximityCalibration()I
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1544
    .line 1546
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@4
    move-result-object v0

    #@5
    .line 1547
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@8
    move-result-object v1

    #@9
    .line 1549
    new-instance v2, Lcom/qualcomm/sensors/sensortest/SensorID;

    #@b
    sget-object v3, Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;->PROX_LIGHT:Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;

    #@d
    const/4 v4, 0x0

    #@e
    invoke-direct {v2, v3, v4}, Lcom/qualcomm/sensors/sensortest/SensorID;-><init>(Lcom/qualcomm/sensors/sensortest/SensorID$SensorType;I)V

    #@11
    sget-object v3, Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;->PRIMARY:Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;

    #@13
    sget-object v4, Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;->SELFTEST:Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;

    #@15
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    #@18
    move-result v0

    #@19
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    #@1c
    move-result v1

    #@1d
    invoke-static {v2, v3, v4, v0, v1}, Lcom/qualcomm/sensors/sensortest/SensorTest;->runSensorTest(Lcom/qualcomm/sensors/sensortest/SensorID;Lcom/qualcomm/sensors/sensortest/SensorTest$DataType;Lcom/qualcomm/sensors/sensortest/SensorTest$TestType;ZZ)I

    #@20
    move-result v0

    #@21
    .line 1553
    const-string v1, "AAT"

    #@23
    new-instance v2, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v3, "[AATService] Start_ProximityCalibration, cross_talk : "

    #@2a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v2

    #@2e
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@31
    move-result-object v2

    #@32
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v2

    #@36
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    .line 1554
    return v0
.end method

.method public Start_SmartFactoryReset()V
    .registers 1

    #@0
    .prologue
    .line 1514
    return-void
.end method

.method public ThresholdALC()F
    .registers 3

    #@0
    .prologue
    .line 1313
    const-string v0, "AAT"

    #@2
    const-string v1, "[AATService] ThresholdALC"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1314
    const/high16 v0, 0x40a0

    #@9
    .line 1315
    return v0
.end method

.method public camcorder_submic(Z)V
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x2

    #@1
    .line 1299
    if-eqz p1, :cond_20

    #@3
    .line 1301
    const/4 v0, 0x1

    #@4
    invoke-static {v1, v0}, Landroid/media/AudioSystem;->setForceUse(II)I

    #@7
    .line 1302
    const-string v0, "AAT"

    #@9
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v2, "[AATService] camcorder_submic : "

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 1309
    :goto_1f
    return-void

    #@20
    .line 1306
    :cond_20
    const/4 v0, 0x0

    #@21
    invoke-static {v1, v0}, Landroid/media/AudioSystem;->setForceUse(II)I

    #@24
    .line 1307
    const-string v0, "AAT"

    #@26
    new-instance v1, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v2, "[AATService] camcorder_submic : "

    #@2d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v1

    #@31
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@34
    move-result-object v1

    #@35
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v1

    #@39
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    goto :goto_1f
.end method

.method public convert_month_to_english(I)Ljava/lang/String;
    .registers 3
    .parameter

    #@0
    .prologue
    .line 2998
    const-string v0, "ERR"

    #@2
    .line 3000
    packed-switch p1, :pswitch_data_2a

    #@5
    .line 3053
    :goto_5
    return-object v0

    #@6
    .line 3003
    :pswitch_6
    const-string v0, "JAN"

    #@8
    goto :goto_5

    #@9
    .line 3007
    :pswitch_9
    const-string v0, "FEB"

    #@b
    goto :goto_5

    #@c
    .line 3011
    :pswitch_c
    const-string v0, "MAR"

    #@e
    goto :goto_5

    #@f
    .line 3015
    :pswitch_f
    const-string v0, "APR"

    #@11
    goto :goto_5

    #@12
    .line 3019
    :pswitch_12
    const-string v0, "MAY"

    #@14
    goto :goto_5

    #@15
    .line 3023
    :pswitch_15
    const-string v0, "JUN"

    #@17
    goto :goto_5

    #@18
    .line 3026
    :pswitch_18
    const-string v0, "JUL"

    #@1a
    goto :goto_5

    #@1b
    .line 3030
    :pswitch_1b
    const-string v0, "AUG"

    #@1d
    goto :goto_5

    #@1e
    .line 3034
    :pswitch_1e
    const-string v0, "SEP"

    #@20
    goto :goto_5

    #@21
    .line 3038
    :pswitch_21
    const-string v0, "OCT"

    #@23
    goto :goto_5

    #@24
    .line 3042
    :pswitch_24
    const-string v0, "NOV"

    #@26
    goto :goto_5

    #@27
    .line 3046
    :pswitch_27
    const-string v0, "DEC"

    #@29
    goto :goto_5

    #@2a
    .line 3000
    :pswitch_data_2a
    .packed-switch 0x1
        :pswitch_6
        :pswitch_9
        :pswitch_c
        :pswitch_f
        :pswitch_12
        :pswitch_15
        :pswitch_18
        :pswitch_1b
        :pswitch_1e
        :pswitch_21
        :pswitch_24
        :pswitch_27
    .end packed-switch
.end method

.method public createFmRadioMgrFMRadio()V
    .registers 1

    #@0
    .prologue
    .line 1782
    return-void
.end method

.method public disableOisProp()V
    .registers 3

    #@0
    .prologue
    .line 2766
    const-string v0, "sys.allautotest.cameraois"

    #@2
    const-string v1, "false"

    #@4
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 2767
    return-void
.end method

.method public enableOisProp()V
    .registers 3

    #@0
    .prologue
    .line 2761
    const-string v0, "sys.allautotest.cameraois"

    #@2
    const-string v1, "true"

    #@4
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 2762
    return-void
.end method

.method public felicacmdEXTIDM([B)Z
    .registers 3
    .parameter "idm"

    #@0
    .prologue
    .line 1805
    iget-object v0, p0, Lcom/lge/systemservice/service/AATService;->felicaManagerLegacy:Lcom/lge/systemservice/core/felicamanager/FeliCaManager;

    #@2
    invoke-virtual {v0, p1}, Lcom/lge/systemservice/core/felicamanager/FeliCaManager;->cmdEXTIDM([B)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public finalizeGps()V
    .registers 1

    #@0
    .prologue
    .line 970
    return-void
.end method

.method public finalizeLoopback()V
    .registers 3

    #@0
    .prologue
    .line 905
    const-string v0, "AAT"

    #@2
    const-string v1, "[AATService] finalizeLoopback"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 906
    const-string v0, "LG_LOOPBACK_TEST=OFF"

    #@9
    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    #@c
    .line 907
    return-void
.end method

.method public getAATSWversion()Ljava/lang/String;
    .registers 10

    #@0
    .prologue
    .line 3058
    const-string v0, ""

    #@2
    .line 3059
    const-string v0, "ro.lge.factoryversion"

    #@4
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    .line 3062
    const-string v1, ""

    #@a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v1

    #@e
    if-nez v1, :cond_11

    #@10
    .line 3135
    :goto_10
    return-object v0

    #@11
    .line 3075
    :cond_11
    const-string v0, "ro.product.model"

    #@13
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    .line 3076
    const-string v0, "ro.lge.swversion_short"

    #@19
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    .line 3077
    const-string v0, "ro.build.target_operator"

    #@1f
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    .line 3078
    const-string v3, "ro.build.target_country"

    #@25
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@28
    move-result-object v3

    #@29
    .line 3081
    const-string v4, "ro.build.date.utc"

    #@2b
    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@2e
    move-result-object v4

    #@2f
    .line 3083
    const-string v5, "BELL"

    #@31
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@34
    move-result v5

    #@35
    if-eqz v5, :cond_cc

    #@37
    .line 3085
    const-string v0, "BLM"

    #@39
    .line 3098
    :cond_39
    :goto_39
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@3c
    move-result v4

    #@3d
    int-to-long v4, v4

    #@3e
    .line 3100
    const-wide/16 v6, 0x7e90

    #@40
    add-long/2addr v4, v6

    #@41
    .line 3102
    new-instance v6, Ljava/util/Date;

    #@43
    invoke-direct {v6}, Ljava/util/Date;-><init>()V

    #@46
    .line 3103
    const-wide/16 v7, 0x3e8

    #@48
    mul-long/2addr v4, v7

    #@49
    invoke-virtual {v6, v4, v5}, Ljava/util/Date;->setTime(J)V

    #@4c
    .line 3105
    const-string v4, ""

    #@4e
    .line 3107
    const-string v4, "-"

    #@50
    const-string v5, ""

    #@52
    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@55
    move-result-object v4

    #@56
    .line 3119
    invoke-virtual {v6}, Ljava/util/Date;->getDate()I

    #@59
    move-result v1

    #@5a
    .line 3120
    invoke-virtual {v6}, Ljava/util/Date;->getMonth()I

    #@5d
    move-result v5

    #@5e
    add-int/lit8 v5, v5, 0x1

    #@60
    invoke-virtual {p0, v5}, Lcom/lge/systemservice/service/AATService;->convert_month_to_english(I)Ljava/lang/String;

    #@63
    move-result-object v5

    #@64
    .line 3121
    invoke-virtual {v6}, Ljava/util/Date;->getYear()I

    #@67
    move-result v6

    #@68
    add-int/lit16 v6, v6, 0x76c

    #@6a
    .line 3123
    const/16 v7, 0xa

    #@6c
    if-ge v1, v7, :cond_e4

    #@6e
    .line 3125
    new-instance v7, Ljava/lang/StringBuilder;

    #@70
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@73
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v5

    #@77
    const-string v7, "-0"

    #@79
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v5

    #@7d
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@80
    move-result-object v1

    #@81
    const-string v5, "-"

    #@83
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v1

    #@87
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v1

    #@8b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8e
    move-result-object v1

    #@8f
    .line 3132
    :goto_8f
    new-instance v5, Ljava/lang/StringBuilder;

    #@91
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@94
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v4

    #@98
    const-string v5, "AT-"

    #@9a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v4

    #@9e
    const-string v5, "00-"

    #@a0
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v4

    #@a4
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v2

    #@a8
    const-string v4, "-"

    #@aa
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v2

    #@ae
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v0

    #@b2
    const-string v2, "-"

    #@b4
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v0

    #@b8
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v0

    #@bc
    const-string v2, "-"

    #@be
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v0

    #@c2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v0

    #@c6
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c9
    move-result-object v0

    #@ca
    goto/16 :goto_10

    #@cc
    .line 3087
    :cond_cc
    const-string v5, "VTR"

    #@ce
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d1
    move-result v5

    #@d2
    if-eqz v5, :cond_d8

    #@d4
    .line 3089
    const-string v0, "AVC"

    #@d6
    goto/16 :goto_39

    #@d8
    .line 3091
    :cond_d8
    const-string v5, "WIN"

    #@da
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@dd
    move-result v5

    #@de
    if-eqz v5, :cond_39

    #@e0
    .line 3093
    const-string v0, "AWC"

    #@e2
    goto/16 :goto_39

    #@e4
    .line 3129
    :cond_e4
    new-instance v7, Ljava/lang/StringBuilder;

    #@e6
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@e9
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ec
    move-result-object v5

    #@ed
    const-string v7, "-"

    #@ef
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f2
    move-result-object v5

    #@f3
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f6
    move-result-object v1

    #@f7
    const-string v5, "-"

    #@f9
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fc
    move-result-object v1

    #@fd
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@100
    move-result-object v1

    #@101
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@104
    move-result-object v1

    #@105
    goto :goto_8f
.end method

.method public getAccelCalSleep()Z
    .registers 2

    #@0
    .prologue
    .line 1829
    const/4 v0, 0x1

    #@1
    .line 1831
    .local v0, returnval:Z
    return v0
.end method

.method public getAccelerometerCalOption()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1820
    const-string v0, "need_pause_accelerometer"

    #@2
    .line 1824
    .local v0, returnAccelerometerCalOption:Ljava/lang/String;
    return-object v0
.end method

.method public getBatteryCapacityFilePath()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 541
    const-string v0, ""

    #@2
    .line 543
    .local v0, mBatt_capacity:Ljava/lang/String;
    const-string v0, "/sys/class/power_supply/battery/capacity"

    #@4
    .line 545
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@6
    const-string v2, "LG-F320L"

    #@8
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v1

    #@c
    if-nez v1, :cond_22

    #@e
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@10
    const-string v2, "LG-F320K"

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v1

    #@16
    if-nez v1, :cond_22

    #@18
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@1a
    const-string v2, "LG-F320S"

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v1

    #@20
    if-eqz v1, :cond_23

    #@22
    .line 558
    :cond_22
    :goto_22
    return-object v0

    #@23
    .line 549
    :cond_23
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@25
    const-string v2, "LG-F320L"

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2a
    move-result v1

    #@2b
    if-eqz v1, :cond_22

    #@2d
    goto :goto_22
.end method

.method public getBatteryIDFilePath()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 562
    const-string v0, ""

    #@2
    .line 564
    .local v0, mBatt_valid_id:Ljava/lang/String;
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@4
    const-string v2, "LG-F320L"

    #@6
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v1

    #@a
    if-nez v1, :cond_20

    #@c
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@e
    const-string v2, "LG-F320K"

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13
    move-result v1

    #@14
    if-nez v1, :cond_20

    #@16
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@18
    const-string v2, "LG-F320S"

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v1

    #@1e
    if-eqz v1, :cond_23

    #@20
    .line 566
    :cond_20
    const-string v0, "/sys/class/power_supply/battery_id/valid_batt_id"

    #@22
    .line 581
    :cond_22
    :goto_22
    return-object v0

    #@23
    .line 568
    :cond_23
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@25
    const-string v2, "LG-F300L"

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2a
    move-result v1

    #@2b
    if-nez v1, :cond_41

    #@2d
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@2f
    const-string v2, "LG-F300K"

    #@31
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@34
    move-result v1

    #@35
    if-nez v1, :cond_41

    #@37
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@39
    const-string v2, "LG-F300S"

    #@3b
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3e
    move-result v1

    #@3f
    if-eqz v1, :cond_44

    #@41
    .line 570
    :cond_41
    const-string v0, "/sys/class/power_supply/battery_id/valid_batt_id"

    #@43
    goto :goto_22

    #@44
    .line 572
    :cond_44
    sget-object v1, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@46
    const-string v2, "LG-F320L"

    #@48
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4b
    move-result v1

    #@4c
    if-eqz v1, :cond_22

    #@4e
    goto :goto_22
.end method

.method public getCameraResolution()[I
    .registers 4

    #@0
    .prologue
    .line 1579
    const/4 v1, 0x2

    #@1
    new-array v0, v1, [I

    #@3
    .line 1580
    .local v0, resolution:[I
    const/4 v1, 0x0

    #@4
    const/16 v2, 0x3da

    #@6
    aput v2, v0, v1

    #@8
    .line 1581
    const/4 v1, 0x1

    #@9
    const/16 v2, 0x2b6

    #@b
    aput v2, v0, v1

    #@d
    .line 1582
    return-object v0
.end method

.method public getCountSuppotedSIM()I
    .registers 3

    #@0
    .prologue
    .line 1635
    .line 1641
    const-string v0, "true"

    #@2
    const-string v1, "ro.lge.mtk_dualsim"

    #@4
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_10

    #@e
    .line 1643
    const/4 v0, 0x2

    #@f
    .line 1654
    :goto_f
    return v0

    #@10
    .line 1645
    :cond_10
    const-string v0, "true"

    #@12
    const-string v1, "ro.lge.mtk_triplesim"

    #@14
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b
    move-result v0

    #@1c
    if-eqz v0, :cond_20

    #@1e
    .line 1647
    const/4 v0, 0x3

    #@1f
    goto :goto_f

    #@20
    .line 1651
    :cond_20
    const/4 v0, 0x1

    #@21
    goto :goto_f
.end method

.method public getHallSensorResult()[Ljava/lang/String;
    .registers 10

    #@0
    .prologue
    const/16 v8, 0x30

    #@2
    const/4 v7, 0x3

    #@3
    const/4 v6, 0x2

    #@4
    const/4 v5, 0x0

    #@5
    const/4 v4, 0x1

    #@6
    .line 2582
    const/4 v0, 0x4

    #@7
    new-array v0, v0, [Ljava/lang/String;

    #@9
    .line 2592
    :try_start_9
    new-instance v2, Ljava/io/BufferedReader;

    #@b
    new-instance v1, Ljava/io/FileReader;

    #@d
    sget-object v3, Lcom/lge/systemservice/service/AATService;->HALLDEVICEFILE:Ljava/lang/String;

    #@f
    invoke-direct {v1, v3}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    #@12
    invoke-direct {v2, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_15
    .catch Ljava/io/FileNotFoundException; {:try_start_9 .. :try_end_15} :catch_d8

    #@15
    .line 2605
    sget-object v1, Lcom/lge/systemservice/service/AATService;->product_device:Ljava/lang/String;

    #@17
    const-string v3, "vu3"

    #@19
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c
    move-result v1

    #@1d
    if-eqz v1, :cond_142

    #@1f
    .line 2609
    :try_start_1f
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    .line 2610
    const-string v3, "AAT"

    #@25
    new-instance v4, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v5, "[AATService] smartcover bit state >> "

    #@2c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v4

    #@30
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v4

    #@34
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v4

    #@38
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 2611
    const/16 v3, 0x8

    #@3d
    invoke-virtual {v1, v3}, Ljava/lang/String;->charAt(I)C

    #@40
    move-result v1

    #@41
    .line 2613
    if-ne v1, v8, :cond_ed

    #@43
    .line 2615
    const/4 v1, 0x1

    #@44
    const-string v3, "Pass"

    #@46
    aput-object v3, v0, v1

    #@48
    .line 2616
    const-string v1, "AAT"

    #@4a
    const-string v3, "[AATService] smartcover opened"

    #@4c
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 2629
    :goto_4f
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_52
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_52} :catch_ff

    #@52
    .line 2637
    :goto_52
    :try_start_52
    new-instance v1, Ljava/io/BufferedReader;

    #@54
    new-instance v3, Ljava/io/FileReader;

    #@56
    sget-object v4, Lcom/lge/systemservice/service/AATService;->HALLDEVICEPENINOUT:Ljava/lang/String;

    #@58
    invoke-direct {v3, v4}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    #@5b
    invoke-direct {v1, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_5e
    .catch Ljava/io/FileNotFoundException; {:try_start_52 .. :try_end_5e} :catch_113

    #@5e
    .line 2647
    :try_start_5e
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@61
    move-result-object v1

    #@62
    .line 2648
    const-string v3, "AAT"

    #@64
    new-instance v4, Ljava/lang/StringBuilder;

    #@66
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@69
    const-string v5, "[AATService] pen in-out bit state >> "

    #@6b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v4

    #@6f
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v4

    #@73
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v4

    #@77
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7a
    .line 2649
    const/4 v3, 0x6

    #@7b
    invoke-virtual {v1, v3}, Ljava/lang/String;->charAt(I)C

    #@7e
    move-result v1

    #@7f
    .line 2651
    if-ne v1, v8, :cond_11c

    #@81
    .line 2653
    const/4 v1, 0x3

    #@82
    const-string v3, "Pass"

    #@84
    aput-object v3, v0, v1

    #@86
    .line 2654
    const-string v1, "AAT"

    #@88
    const-string v3, "[AATService] pen out"

    #@8a
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8d
    .line 2667
    :goto_8d
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_90
    .catch Ljava/io/IOException; {:try_start_5e .. :try_end_90} :catch_12e

    #@90
    .line 2717
    :goto_90
    sget-object v1, Lcom/lge/systemservice/service/AATService;->product_name:Ljava/lang/String;

    #@92
    const-string v3, "g2_vzw"

    #@94
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@97
    move-result v1

    #@98
    if-eqz v1, :cond_d2

    #@9a
    .line 2722
    :try_start_9a
    new-instance v1, Ljava/io/BufferedReader;

    #@9c
    new-instance v3, Ljava/io/FileReader;

    #@9e
    sget-object v4, Lcom/lge/systemservice/service/AATService;->CARKITFILE:Ljava/lang/String;

    #@a0
    invoke-direct {v3, v4}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    #@a3
    invoke-direct {v1, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_a6
    .catch Ljava/io/FileNotFoundException; {:try_start_9a .. :try_end_a6} :catch_1d4

    #@a6
    .line 2734
    :try_start_a6
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@a9
    move-result-object v1

    #@aa
    .line 2735
    const-string v3, "AAT"

    #@ac
    new-instance v4, Ljava/lang/StringBuilder;

    #@ae
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@b1
    const-string v5, "[AATService] Carkit Sensor result: "

    #@b3
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v4

    #@b7
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v4

    #@bb
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@be
    move-result-object v4

    #@bf
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c2
    .line 2737
    const-string v3, "1"

    #@c4
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c7
    move-result v3

    #@c8
    if-eqz v3, :cond_1de

    #@ca
    .line 2738
    const/4 v1, 0x2

    #@cb
    const-string v3, "Fail"

    #@cd
    aput-object v3, v0, v1

    #@cf
    .line 2748
    :goto_cf
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_d2
    .catch Ljava/io/IOException; {:try_start_a6 .. :try_end_d2} :catch_1ed

    #@d2
    .line 2754
    :cond_d2
    :goto_d2
    const-wide/16 v1, 0x64

    #@d4
    invoke-static {v1, v2}, Landroid/os/SystemClock;->sleep(J)V

    #@d7
    .line 2755
    :goto_d7
    return-object v0

    #@d8
    .line 2593
    :catch_d8
    move-exception v1

    #@d9
    .line 2594
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    #@dc
    .line 2597
    const-string v1, "Error"

    #@de
    aput-object v1, v0, v5

    #@e0
    .line 2598
    const-string v1, "Error"

    #@e2
    aput-object v1, v0, v4

    #@e4
    .line 2599
    const-string v1, "Error"

    #@e6
    aput-object v1, v0, v6

    #@e8
    .line 2600
    const-string v1, "Error"

    #@ea
    aput-object v1, v0, v7

    #@ec
    goto :goto_d7

    #@ed
    .line 2618
    :cond_ed
    const/16 v3, 0x31

    #@ef
    if-ne v1, v3, :cond_105

    #@f1
    .line 2620
    const/4 v1, 0x1

    #@f2
    :try_start_f2
    const-string v3, "Fail"

    #@f4
    aput-object v3, v0, v1

    #@f6
    .line 2621
    const-string v1, "AAT"

    #@f8
    const-string v3, "[AATService] smartcover closed"

    #@fa
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_fd
    .catch Ljava/io/IOException; {:try_start_f2 .. :try_end_fd} :catch_ff

    #@fd
    goto/16 :goto_4f

    #@ff
    .line 2630
    :catch_ff
    move-exception v1

    #@100
    .line 2631
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@103
    goto/16 :goto_52

    #@105
    .line 2625
    :cond_105
    const/4 v1, 0x1

    #@106
    :try_start_106
    const-string v3, "Error"

    #@108
    aput-object v3, v0, v1

    #@10a
    .line 2626
    const-string v1, "AAT"

    #@10c
    const-string v3, "[AATService] Unexpected smartcover state"

    #@10e
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_111
    .catch Ljava/io/IOException; {:try_start_106 .. :try_end_111} :catch_ff

    #@111
    goto/16 :goto_4f

    #@113
    .line 2638
    :catch_113
    move-exception v1

    #@114
    .line 2639
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    #@117
    .line 2641
    const-string v1, "Error"

    #@119
    aput-object v1, v0, v7

    #@11b
    goto :goto_d7

    #@11c
    .line 2656
    :cond_11c
    const/16 v3, 0x31

    #@11e
    if-ne v1, v3, :cond_134

    #@120
    .line 2658
    const/4 v1, 0x3

    #@121
    :try_start_121
    const-string v3, "Fail"

    #@123
    aput-object v3, v0, v1

    #@125
    .line 2659
    const-string v1, "AAT"

    #@127
    const-string v3, "[AATService] pen in"

    #@129
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_12c
    .catch Ljava/io/IOException; {:try_start_121 .. :try_end_12c} :catch_12e

    #@12c
    goto/16 :goto_8d

    #@12e
    .line 2668
    :catch_12e
    move-exception v1

    #@12f
    .line 2669
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@132
    goto/16 :goto_90

    #@134
    .line 2663
    :cond_134
    const/4 v1, 0x3

    #@135
    :try_start_135
    const-string v3, "Error"

    #@137
    aput-object v3, v0, v1

    #@139
    .line 2664
    const-string v1, "AAT"

    #@13b
    const-string v3, "[AATService] Unexpected pen state"

    #@13d
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_140
    .catch Ljava/io/IOException; {:try_start_135 .. :try_end_140} :catch_12e

    #@140
    goto/16 :goto_8d

    #@142
    .line 2675
    :cond_142
    :try_start_142
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@145
    move-result-object v1

    #@146
    .line 2676
    const-string v3, "AAT"

    #@148
    new-instance v4, Ljava/lang/StringBuilder;

    #@14a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@14d
    const-string v5, "[AATService] IsSupportHallSensor result: "

    #@14f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@152
    move-result-object v4

    #@153
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@156
    move-result-object v4

    #@157
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15a
    move-result-object v4

    #@15b
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@15e
    .line 2678
    const-string v3, "2"

    #@160
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@163
    move-result v3

    #@164
    if-eqz v3, :cond_17b

    #@166
    .line 2679
    const/4 v1, 0x0

    #@167
    const-string v3, "Fail"

    #@169
    aput-object v3, v0, v1

    #@16b
    .line 2680
    const/4 v1, 0x1

    #@16c
    const-string v3, "Pass"

    #@16e
    aput-object v3, v0, v1

    #@170
    .line 2708
    :goto_170
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_173
    .catch Ljava/io/IOException; {:try_start_142 .. :try_end_173} :catch_175

    #@173
    goto/16 :goto_90

    #@175
    .line 2709
    :catch_175
    move-exception v1

    #@176
    .line 2712
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@179
    goto/16 :goto_90

    #@17b
    .line 2686
    :cond_17b
    :try_start_17b
    const-string v3, "5"

    #@17d
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@180
    move-result v3

    #@181
    if-eqz v3, :cond_18e

    #@183
    .line 2687
    const/4 v1, 0x0

    #@184
    const-string v3, "Pass"

    #@186
    aput-object v3, v0, v1

    #@188
    .line 2688
    const/4 v1, 0x1

    #@189
    const-string v3, "Fail"

    #@18b
    aput-object v3, v0, v1

    #@18d
    goto :goto_170

    #@18e
    .line 2694
    :cond_18e
    const-string v3, "1"

    #@190
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@193
    move-result v3

    #@194
    if-nez v3, :cond_1a6

    #@196
    const-string v3, "6"

    #@198
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@19b
    move-result v3

    #@19c
    if-nez v3, :cond_1a6

    #@19e
    const-string v3, "0"

    #@1a0
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1a3
    move-result v3

    #@1a4
    if-eqz v3, :cond_1b1

    #@1a6
    .line 2695
    :cond_1a6
    const/4 v1, 0x0

    #@1a7
    const-string v3, "Pass"

    #@1a9
    aput-object v3, v0, v1

    #@1ab
    .line 2696
    const/4 v1, 0x1

    #@1ac
    const-string v3, "Pass"

    #@1ae
    aput-object v3, v0, v1

    #@1b0
    goto :goto_170

    #@1b1
    .line 2703
    :cond_1b1
    const/4 v3, 0x0

    #@1b2
    const-string v4, "Fail"

    #@1b4
    aput-object v4, v0, v3

    #@1b6
    .line 2704
    const/4 v3, 0x1

    #@1b7
    const-string v4, "Fail"

    #@1b9
    aput-object v4, v0, v3

    #@1bb
    .line 2706
    const-string v3, "AAT"

    #@1bd
    new-instance v4, Ljava/lang/StringBuilder;

    #@1bf
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1c2
    const-string v5, "[AATService] IsSupportHallSensor result: "

    #@1c4
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c7
    move-result-object v4

    #@1c8
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cb
    move-result-object v1

    #@1cc
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1cf
    move-result-object v1

    #@1d0
    invoke-static {v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1d3
    .catch Ljava/io/IOException; {:try_start_17b .. :try_end_1d3} :catch_175

    #@1d3
    goto :goto_170

    #@1d4
    .line 2723
    :catch_1d4
    move-exception v1

    #@1d5
    .line 2724
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    #@1d8
    .line 2726
    const-string v1, "Error"

    #@1da
    aput-object v1, v0, v6

    #@1dc
    goto/16 :goto_d7

    #@1de
    .line 2740
    :cond_1de
    :try_start_1de
    const-string v3, "0"

    #@1e0
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e3
    move-result v3

    #@1e4
    if-eqz v3, :cond_1f3

    #@1e6
    .line 2741
    const/4 v1, 0x2

    #@1e7
    const-string v3, "Pass"

    #@1e9
    aput-object v3, v0, v1
    :try_end_1eb
    .catch Ljava/io/IOException; {:try_start_1de .. :try_end_1eb} :catch_1ed

    #@1eb
    goto/16 :goto_cf

    #@1ed
    .line 2749
    :catch_1ed
    move-exception v1

    #@1ee
    .line 2751
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@1f1
    goto/16 :goto_d2

    #@1f3
    .line 2744
    :cond_1f3
    const/4 v3, 0x2

    #@1f4
    :try_start_1f4
    const-string v4, "Fail"

    #@1f6
    aput-object v4, v0, v3

    #@1f8
    .line 2746
    const-string v3, "AAT"

    #@1fa
    new-instance v4, Ljava/lang/StringBuilder;

    #@1fc
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1ff
    const-string v5, "[AATService] IsSupportHallSensor result: "

    #@201
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@204
    move-result-object v4

    #@205
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@208
    move-result-object v1

    #@209
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20c
    move-result-object v1

    #@20d
    invoke-static {v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_210
    .catch Ljava/io/IOException; {:try_start_1f4 .. :try_end_210} :catch_1ed

    #@210
    goto/16 :goto_cf
.end method

.method public getLedVal()[Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1440
    const/4 v0, 0x3

    #@2
    new-array v2, v0, [Ljava/lang/String;

    #@4
    .line 1448
    :try_start_4
    new-instance v0, Ljava/io/BufferedReader;

    #@6
    new-instance v3, Ljava/io/FileReader;

    #@8
    sget-object v4, Lcom/lge/systemservice/service/AATService;->ChargingLedRedFilePath:Ljava/lang/String;

    #@a
    invoke-direct {v3, v4}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    #@d
    invoke-direct {v0, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_10
    .catchall {:try_start_4 .. :try_end_10} :catchall_6f
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_10} :catch_5b

    #@10
    .line 1449
    const/4 v3, 0x0

    #@11
    :try_start_11
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@14
    move-result-object v4

    #@15
    aput-object v4, v2, v3

    #@17
    .line 1450
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_1a
    .catchall {:try_start_11 .. :try_end_1a} :catchall_d3
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_1a} :catch_d8

    #@1a
    .line 1455
    if-eqz v0, :cond_1f

    #@1c
    .line 1457
    :try_start_1c
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_1f
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_1f} :catch_56

    #@1f
    .line 1466
    :cond_1f
    :goto_1f
    :try_start_1f
    new-instance v0, Ljava/io/BufferedReader;

    #@21
    new-instance v3, Ljava/io/FileReader;

    #@23
    sget-object v4, Lcom/lge/systemservice/service/AATService;->ChargingLedGreenFilePath:Ljava/lang/String;

    #@25
    invoke-direct {v3, v4}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    #@28
    invoke-direct {v0, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_2b
    .catchall {:try_start_1f .. :try_end_2b} :catchall_94
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_2b} :catch_80

    #@2b
    .line 1467
    const/4 v3, 0x1

    #@2c
    :try_start_2c
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@2f
    move-result-object v4

    #@30
    aput-object v4, v2, v3

    #@32
    .line 1468
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_35
    .catchall {:try_start_2c .. :try_end_35} :catchall_cc
    .catch Ljava/io/IOException; {:try_start_2c .. :try_end_35} :catch_d1

    #@35
    .line 1473
    if-eqz v0, :cond_3a

    #@37
    .line 1475
    :try_start_37
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_3a
    .catch Ljava/io/IOException; {:try_start_37 .. :try_end_3a} :catch_7b

    #@3a
    .line 1485
    :cond_3a
    :goto_3a
    :try_start_3a
    new-instance v0, Ljava/io/BufferedReader;

    #@3c
    new-instance v3, Ljava/io/FileReader;

    #@3e
    sget-object v4, Lcom/lge/systemservice/service/AATService;->ChargingLedBlueFilePath:Ljava/lang/String;

    #@40
    invoke-direct {v3, v4}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    #@43
    invoke-direct {v0, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_46
    .catchall {:try_start_3a .. :try_end_46} :catchall_b8
    .catch Ljava/io/IOException; {:try_start_3a .. :try_end_46} :catch_a5

    #@46
    .line 1486
    const/4 v1, 0x2

    #@47
    :try_start_47
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@4a
    move-result-object v3

    #@4b
    aput-object v3, v2, v1

    #@4d
    .line 1487
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_50
    .catchall {:try_start_47 .. :try_end_50} :catchall_c4
    .catch Ljava/io/IOException; {:try_start_47 .. :try_end_50} :catch_c9

    #@50
    .line 1492
    if-eqz v0, :cond_55

    #@52
    .line 1494
    :try_start_52
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_55
    .catch Ljava/io/IOException; {:try_start_52 .. :try_end_55} :catch_a0

    #@55
    .line 1507
    :cond_55
    :goto_55
    return-object v2

    #@56
    .line 1461
    :catch_56
    move-exception v0

    #@57
    .line 1462
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@5a
    goto :goto_1f

    #@5b
    .line 1451
    :catch_5b
    move-exception v0

    #@5c
    move-object v0, v1

    #@5d
    .line 1452
    :goto_5d
    :try_start_5d
    const-string v3, "AAT"

    #@5f
    const-string v4, "[AATService] fail to read red"

    #@61
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_64
    .catchall {:try_start_5d .. :try_end_64} :catchall_d3

    #@64
    .line 1455
    if-eqz v0, :cond_1f

    #@66
    .line 1457
    :try_start_66
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_69
    .catch Ljava/io/IOException; {:try_start_66 .. :try_end_69} :catch_6a

    #@69
    goto :goto_1f

    #@6a
    .line 1461
    :catch_6a
    move-exception v0

    #@6b
    .line 1462
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@6e
    goto :goto_1f

    #@6f
    .line 1454
    :catchall_6f
    move-exception v0

    #@70
    .line 1455
    :goto_70
    if-eqz v1, :cond_75

    #@72
    .line 1457
    :try_start_72
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_75
    .catch Ljava/io/IOException; {:try_start_72 .. :try_end_75} :catch_76

    #@75
    .line 1463
    :cond_75
    :goto_75
    throw v0

    #@76
    .line 1461
    :catch_76
    move-exception v1

    #@77
    .line 1462
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@7a
    goto :goto_75

    #@7b
    .line 1479
    :catch_7b
    move-exception v0

    #@7c
    .line 1480
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@7f
    goto :goto_3a

    #@80
    .line 1469
    :catch_80
    move-exception v0

    #@81
    move-object v0, v1

    #@82
    .line 1470
    :goto_82
    :try_start_82
    const-string v3, "AAT"

    #@84
    const-string v4, "[AATService] fail to read green"

    #@86
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_89
    .catchall {:try_start_82 .. :try_end_89} :catchall_cc

    #@89
    .line 1473
    if-eqz v0, :cond_3a

    #@8b
    .line 1475
    :try_start_8b
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_8e
    .catch Ljava/io/IOException; {:try_start_8b .. :try_end_8e} :catch_8f

    #@8e
    goto :goto_3a

    #@8f
    .line 1479
    :catch_8f
    move-exception v0

    #@90
    .line 1480
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@93
    goto :goto_3a

    #@94
    .line 1472
    :catchall_94
    move-exception v0

    #@95
    .line 1473
    :goto_95
    if-eqz v1, :cond_9a

    #@97
    .line 1475
    :try_start_97
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_9a
    .catch Ljava/io/IOException; {:try_start_97 .. :try_end_9a} :catch_9b

    #@9a
    .line 1481
    :cond_9a
    :goto_9a
    throw v0

    #@9b
    .line 1479
    :catch_9b
    move-exception v1

    #@9c
    .line 1480
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@9f
    goto :goto_9a

    #@a0
    .line 1498
    :catch_a0
    move-exception v0

    #@a1
    .line 1499
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@a4
    goto :goto_55

    #@a5
    .line 1488
    :catch_a5
    move-exception v0

    #@a6
    .line 1489
    :goto_a6
    :try_start_a6
    const-string v0, "AAT"

    #@a8
    const-string v3, "[AATService] fail to read blue"

    #@aa
    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_ad
    .catchall {:try_start_a6 .. :try_end_ad} :catchall_b8

    #@ad
    .line 1492
    if-eqz v1, :cond_55

    #@af
    .line 1494
    :try_start_af
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_b2
    .catch Ljava/io/IOException; {:try_start_af .. :try_end_b2} :catch_b3

    #@b2
    goto :goto_55

    #@b3
    .line 1498
    :catch_b3
    move-exception v0

    #@b4
    .line 1499
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@b7
    goto :goto_55

    #@b8
    .line 1491
    :catchall_b8
    move-exception v0

    #@b9
    .line 1492
    :goto_b9
    if-eqz v1, :cond_be

    #@bb
    .line 1494
    :try_start_bb
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_be
    .catch Ljava/io/IOException; {:try_start_bb .. :try_end_be} :catch_bf

    #@be
    .line 1500
    :cond_be
    :goto_be
    throw v0

    #@bf
    .line 1498
    :catch_bf
    move-exception v1

    #@c0
    .line 1499
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@c3
    goto :goto_be

    #@c4
    .line 1491
    :catchall_c4
    move-exception v1

    #@c5
    move-object v5, v1

    #@c6
    move-object v1, v0

    #@c7
    move-object v0, v5

    #@c8
    goto :goto_b9

    #@c9
    .line 1488
    :catch_c9
    move-exception v1

    #@ca
    move-object v1, v0

    #@cb
    goto :goto_a6

    #@cc
    .line 1472
    :catchall_cc
    move-exception v1

    #@cd
    move-object v5, v1

    #@ce
    move-object v1, v0

    #@cf
    move-object v0, v5

    #@d0
    goto :goto_95

    #@d1
    .line 1469
    :catch_d1
    move-exception v3

    #@d2
    goto :goto_82

    #@d3
    .line 1454
    :catchall_d3
    move-exception v1

    #@d4
    move-object v5, v1

    #@d5
    move-object v1, v0

    #@d6
    move-object v0, v5

    #@d7
    goto :goto_70

    #@d8
    .line 1451
    :catch_d8
    move-exception v3

    #@d9
    goto :goto_5d
.end method

.method public getMaxVolume(I)I
    .registers 6
    .parameter

    #@0
    .prologue
    const/16 v3, 0x9

    #@2
    .line 2959
    const-string v0, "AAT"

    #@4
    new-instance v1, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v2, "[AATService] mMaxVoiceCallVolume type:"

    #@b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 2961
    packed-switch p1, :pswitch_data_e2

    #@1d
    .line 2994
    :pswitch_1d
    const/4 v0, -0x1

    #@1e
    :goto_1e
    return v0

    #@1f
    .line 2967
    :pswitch_1f
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@21
    const-string v1, "LG-F320L"

    #@23
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@26
    move-result v0

    #@27
    if-nez v0, :cond_3d

    #@29
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@2b
    const-string v1, "LG-F320S"

    #@2d
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@30
    move-result v0

    #@31
    if-nez v0, :cond_3d

    #@33
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@35
    const-string v1, "LG-F320K"

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3a
    move-result v0

    #@3b
    if-eqz v0, :cond_5c

    #@3d
    .line 2969
    :cond_3d
    iput v3, p0, Lcom/lge/systemservice/service/AATService;->mMaxVoiceCallVolume:I

    #@3f
    .line 2987
    :goto_3f
    const-string v0, "AAT"

    #@41
    new-instance v1, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v2, "[AATService] returned MAX STREAM_VOICE_CALL: "

    #@48
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v1

    #@4c
    iget v2, p0, Lcom/lge/systemservice/service/AATService;->mMaxVoiceCallVolume:I

    #@4e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@51
    move-result-object v1

    #@52
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v1

    #@56
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@59
    .line 2988
    iget v0, p0, Lcom/lge/systemservice/service/AATService;->mMaxVoiceCallVolume:I

    #@5b
    goto :goto_1e

    #@5c
    .line 2971
    :cond_5c
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@5e
    const-string v1, "VS980 4G"

    #@60
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@63
    move-result v0

    #@64
    if-eqz v0, :cond_6a

    #@66
    .line 2973
    const/4 v0, 0x7

    #@67
    iput v0, p0, Lcom/lge/systemservice/service/AATService;->mMaxVoiceCallVolume:I

    #@69
    goto :goto_3f

    #@6a
    .line 2975
    :cond_6a
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@6c
    const-string v1, "LG-D800"

    #@6e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@71
    move-result v0

    #@72
    if-nez v0, :cond_d8

    #@74
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@76
    const-string v1, "LG-D801"

    #@78
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7b
    move-result v0

    #@7c
    if-nez v0, :cond_d8

    #@7e
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@80
    const-string v1, "LG-D803"

    #@82
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@85
    move-result v0

    #@86
    if-nez v0, :cond_d8

    #@88
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@8a
    const-string v1, "LG-LS980"

    #@8c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8f
    move-result v0

    #@90
    if-nez v0, :cond_d8

    #@92
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@94
    const-string v1, "LG-D802"

    #@96
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@99
    move-result v0

    #@9a
    if-nez v0, :cond_d8

    #@9c
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@9e
    const-string v1, "LG-D802T"

    #@a0
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a3
    move-result v0

    #@a4
    if-nez v0, :cond_d8

    #@a6
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@a8
    const-string v1, "LG-D805"

    #@aa
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ad
    move-result v0

    #@ae
    if-nez v0, :cond_d8

    #@b0
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@b2
    const-string v1, "LG-D806"

    #@b4
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b7
    move-result v0

    #@b8
    if-nez v0, :cond_d8

    #@ba
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@bc
    const-string v1, "LG-D807"

    #@be
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c1
    move-result v0

    #@c2
    if-nez v0, :cond_d8

    #@c4
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@c6
    const-string v1, "LG-DS1203"

    #@c8
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@cb
    move-result v0

    #@cc
    if-nez v0, :cond_d8

    #@ce
    sget-object v0, Lcom/lge/systemservice/service/AATService;->product_name:Ljava/lang/String;

    #@d0
    const-string v1, "g2_jp_kdi"

    #@d2
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d5
    move-result v0

    #@d6
    if-eqz v0, :cond_dd

    #@d8
    .line 2980
    :cond_d8
    const/4 v0, 0x6

    #@d9
    iput v0, p0, Lcom/lge/systemservice/service/AATService;->mMaxVoiceCallVolume:I

    #@db
    goto/16 :goto_3f

    #@dd
    .line 2984
    :cond_dd
    iput v3, p0, Lcom/lge/systemservice/service/AATService;->mMaxVoiceCallVolume:I

    #@df
    goto/16 :goto_3f

    #@e1
    .line 2961
    nop

    #@e2
    :pswitch_data_e2
    .packed-switch 0x0
        :pswitch_1d
        :pswitch_1d
        :pswitch_1f
    .end packed-switch
.end method

.method public getQuickMemoKeyCodeValue()I
    .registers 4

    #@0
    .prologue
    .line 1918
    const/4 v0, -0x1

    #@1
    .line 1920
    sget-object v1, Lcom/lge/systemservice/service/AATService;->product_device:Ljava/lang/String;

    #@3
    const-string v2, "vu3"

    #@5
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_d

    #@b
    .line 1922
    const/16 v0, 0xe1

    #@d
    .line 1925
    :cond_d
    return v0
.end method

.method public getQwertyStatusValue()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1334
    const-string v0, ""

    #@2
    .line 1335
    .local v0, enableval:Ljava/lang/String;
    return-object v0
.end method

.method public getSaveVolume(I)I
    .registers 6
    .parameter

    #@0
    .prologue
    const/4 v3, 0x5

    #@1
    .line 2916
    const-string v0, "AAT"

    #@3
    new-instance v1, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v2, "[AATService] getSaveVolume type:"

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 2918
    packed-switch p1, :pswitch_data_11a

    #@1c
    .line 2954
    const/4 v0, -0x1

    #@1d
    :goto_1d
    return v0

    #@1e
    .line 2921
    :pswitch_1e
    const-string v0, "AAT"

    #@20
    new-instance v1, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v2, "[AATService] returned Default STREAM_MUSIC: "

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    iget v2, p0, Lcom/lge/systemservice/service/AATService;->mSaveMusicVolume:I

    #@2d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30
    move-result-object v1

    #@31
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v1

    #@35
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 2922
    iget v0, p0, Lcom/lge/systemservice/service/AATService;->mSaveMusicVolume:I

    #@3a
    goto :goto_1d

    #@3b
    .line 2924
    :pswitch_3b
    const-string v0, "AAT"

    #@3d
    new-instance v1, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    const-string v2, "[AATService] returned Default STREAM_RING: "

    #@44
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v1

    #@48
    iget v2, p0, Lcom/lge/systemservice/service/AATService;->mSaveRingVolume:I

    #@4a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v1

    #@4e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v1

    #@52
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@55
    .line 2925
    iget v0, p0, Lcom/lge/systemservice/service/AATService;->mSaveRingVolume:I

    #@57
    goto :goto_1d

    #@58
    .line 2927
    :pswitch_58
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@5a
    const-string v1, "LG-F320L"

    #@5c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5f
    move-result v0

    #@60
    if-nez v0, :cond_76

    #@62
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@64
    const-string v1, "LG-F320S"

    #@66
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@69
    move-result v0

    #@6a
    if-nez v0, :cond_76

    #@6c
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@6e
    const-string v1, "VS980 4G"

    #@70
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@73
    move-result v0

    #@74
    if-eqz v0, :cond_95

    #@76
    .line 2929
    :cond_76
    iput v3, p0, Lcom/lge/systemservice/service/AATService;->mSaveVoiceCallVolume:I

    #@78
    .line 2947
    :goto_78
    const-string v0, "AAT"

    #@7a
    new-instance v1, Ljava/lang/StringBuilder;

    #@7c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7f
    const-string v2, "[AATService] returned Default STREAM_VOICE_CALL: "

    #@81
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v1

    #@85
    iget v2, p0, Lcom/lge/systemservice/service/AATService;->mSaveVoiceCallVolume:I

    #@87
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v1

    #@8b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8e
    move-result-object v1

    #@8f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@92
    .line 2948
    iget v0, p0, Lcom/lge/systemservice/service/AATService;->mSaveVoiceCallVolume:I

    #@94
    goto :goto_1d

    #@95
    .line 2931
    :cond_95
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@97
    const-string v1, "LG-F320K"

    #@99
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9c
    move-result v0

    #@9d
    if-eqz v0, :cond_a3

    #@9f
    .line 2933
    const/4 v0, 0x6

    #@a0
    iput v0, p0, Lcom/lge/systemservice/service/AATService;->mSaveVoiceCallVolume:I

    #@a2
    goto :goto_78

    #@a3
    .line 2935
    :cond_a3
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@a5
    const-string v1, "LG-D800"

    #@a7
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@aa
    move-result v0

    #@ab
    if-nez v0, :cond_111

    #@ad
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@af
    const-string v1, "LG-D801"

    #@b1
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b4
    move-result v0

    #@b5
    if-nez v0, :cond_111

    #@b7
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@b9
    const-string v1, "LG-D803"

    #@bb
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@be
    move-result v0

    #@bf
    if-nez v0, :cond_111

    #@c1
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@c3
    const-string v1, "LG-LS980"

    #@c5
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c8
    move-result v0

    #@c9
    if-nez v0, :cond_111

    #@cb
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@cd
    const-string v1, "LG-D802"

    #@cf
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d2
    move-result v0

    #@d3
    if-nez v0, :cond_111

    #@d5
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@d7
    const-string v1, "LG-D802T"

    #@d9
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@dc
    move-result v0

    #@dd
    if-nez v0, :cond_111

    #@df
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@e1
    const-string v1, "LG-D805"

    #@e3
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e6
    move-result v0

    #@e7
    if-nez v0, :cond_111

    #@e9
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@eb
    const-string v1, "LG-D806"

    #@ed
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f0
    move-result v0

    #@f1
    if-nez v0, :cond_111

    #@f3
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@f5
    const-string v1, "LG-D807"

    #@f7
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@fa
    move-result v0

    #@fb
    if-nez v0, :cond_111

    #@fd
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@ff
    const-string v1, "LG-DS1203"

    #@101
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@104
    move-result v0

    #@105
    if-nez v0, :cond_111

    #@107
    sget-object v0, Lcom/lge/systemservice/service/AATService;->product_name:Ljava/lang/String;

    #@109
    const-string v1, "g2_jp_kdi"

    #@10b
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10e
    move-result v0

    #@10f
    if-eqz v0, :cond_116

    #@111
    .line 2940
    :cond_111
    const/4 v0, 0x3

    #@112
    iput v0, p0, Lcom/lge/systemservice/service/AATService;->mSaveVoiceCallVolume:I

    #@114
    goto/16 :goto_78

    #@116
    .line 2944
    :cond_116
    iput v3, p0, Lcom/lge/systemservice/service/AATService;->mSaveVoiceCallVolume:I

    #@118
    goto/16 :goto_78

    #@11a
    .line 2918
    :pswitch_data_11a
    .packed-switch 0x0
        :pswitch_1e
        :pswitch_3b
        :pswitch_58
    .end packed-switch
.end method

.method public getSupportVideoEncorder()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1811
    const-string v0, "H264"

    #@2
    .line 1814
    return-object v0
.end method

.method public getTestOrderLength()I
    .registers 5

    #@0
    .prologue
    .line 2522
    const-string v0, "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24"

    #@2
    .line 2523
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@5
    move-result v0

    #@6
    .line 2524
    const-string v1, "AAT"

    #@8
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v3, "[AATService] getTestOrderLength: "

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v2

    #@1b
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 2526
    return v0
.end method

.method public getTestOrderNumber()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    const/16 v2, 0x2c

    #@2
    const/4 v4, 0x1

    #@3
    .line 2503
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Lcom/lge/systemservice/service/AATService;->mUseLog:Z

    #@6
    .line 2504
    const/16 v0, 0x1f

    #@8
    invoke-direct {p0, v0}, Lcom/lge/systemservice/service/AATService;->AccessMiscRead(I)Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    .line 2506
    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    #@f
    move-result v1

    #@10
    if-eq v1, v2, :cond_22

    #@12
    const/4 v1, 0x2

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    #@16
    move-result v1

    #@17
    if-eq v1, v2, :cond_22

    #@19
    .line 2508
    const-string v0, "AAT"

    #@1b
    const-string v1, "[AATService] AATSET Order is NULL"

    #@1d
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 2509
    const-string v0, "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24"

    #@22
    .line 2514
    :cond_22
    const-string v1, "AAT"

    #@24
    new-instance v2, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v3, "[AATService] getTestOrderNumber: "

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 2515
    iput-boolean v4, p0, Lcom/lge/systemservice/service/AATService;->mUseLog:Z

    #@3c
    .line 2517
    return-object v0
.end method

.method public initializeGps()V
    .registers 1

    #@0
    .prologue
    .line 956
    return-void
.end method

.method public initializeLoopback()V
    .registers 3

    #@0
    .prologue
    .line 899
    const-string v0, "AAT"

    #@2
    const-string v1, "[AATService] initializeLoopback"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 900
    const-string v0, "LG_LOOPBACK_TEST=ON"

    #@9
    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    #@c
    .line 901
    return-void
.end method

.method public isDualSIM()Z
    .registers 2

    #@0
    .prologue
    .line 1631
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public isFactoryTestMode()Z
    .registers 6

    #@0
    .prologue
    .line 1947
    const/4 v0, 0x0

    #@1
    .line 1957
    const-string v1, "sys.factory.qem"

    #@3
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v1

    #@7
    .line 1959
    const-string v2, "1"

    #@9
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c
    move-result v2

    #@d
    if-eqz v2, :cond_10

    #@f
    .line 1961
    const/4 v0, 0x1

    #@10
    .line 1966
    :cond_10
    const-string v2, "AAT"

    #@12
    new-instance v3, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v4, "[AATService] isFactoryTestMode factoryTestStr : "

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 1967
    const-string v1, "AAT"

    #@2a
    new-instance v2, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v3, "[AATService] isFactoryTestMode valueFactoryTestMode : "

    #@31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v2

    #@3d
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 1969
    return v0
.end method

.method public requestToService(ILjava/lang/String;)Ljava/lang/String;
    .registers 11
    .parameter
    .parameter

    #@0
    .prologue
    const/4 v7, 0x2

    #@1
    const/4 v6, 0x5

    #@2
    const/4 v3, 0x0

    #@3
    const/16 v5, 0x14

    #@5
    const/4 v4, 0x1

    #@6
    .line 818
    const-string v0, "AAT"

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "[AATService] requestToService, data : "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 820
    invoke-virtual {p2, v3}, Ljava/lang/String;->charAt(I)C

    #@21
    move-result v0

    #@22
    .line 821
    invoke-virtual {p2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@25
    move-result-object v1

    #@26
    .line 823
    iput-boolean v4, p0, Lcom/lge/systemservice/service/AATService;->mUseLog:Z

    #@28
    .line 825
    if-ne p1, v7, :cond_47

    #@2a
    .line 827
    invoke-direct {p0, v5}, Lcom/lge/systemservice/service/AATService;->AccessMiscRead(I)Ljava/lang/String;

    #@2d
    move-result-object v0

    #@2e
    .line 828
    const-string v1, "AAT"

    #@30
    new-instance v2, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v3, "[AATService] requestToService, aatset = "

    #@37
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v2

    #@3b
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v2

    #@43
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    .line 894
    :goto_46
    return-object v0

    #@47
    .line 830
    :cond_47
    if-ne p1, v6, :cond_85

    #@49
    .line 832
    iput-boolean v3, p0, Lcom/lge/systemservice/service/AATService;->mUseLog:Z

    #@4b
    .line 833
    const/16 v0, 0x1f

    #@4d
    invoke-direct {p0, v0}, Lcom/lge/systemservice/service/AATService;->AccessMiscRead(I)Ljava/lang/String;

    #@50
    move-result-object v0

    #@51
    .line 835
    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    #@54
    move-result v1

    #@55
    const/16 v2, 0x2c

    #@57
    if-eq v1, v2, :cond_82

    #@59
    invoke-virtual {v0, v7}, Ljava/lang/String;->charAt(I)C

    #@5c
    move-result v1

    #@5d
    const/16 v2, 0x2c

    #@5f
    if-eq v1, v2, :cond_82

    #@61
    .line 837
    const-string v0, "AAT"

    #@63
    const-string v1, "[AATService] AATSET Order is NULL"

    #@65
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@68
    .line 838
    const-string v0, "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24"

    #@6a
    .line 839
    const-string v1, "AAT"

    #@6c
    new-instance v2, Ljava/lang/StringBuilder;

    #@6e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@71
    const-string v3, "[AATService] AATSET Order: "

    #@73
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v2

    #@77
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v2

    #@7b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e
    move-result-object v2

    #@7f
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@82
    .line 844
    :cond_82
    iput-boolean v4, p0, Lcom/lge/systemservice/service/AATService;->mUseLog:Z

    #@84
    goto :goto_46

    #@85
    .line 846
    :cond_85
    const/4 v2, 0x4

    #@86
    if-ne p1, v2, :cond_91

    #@88
    .line 848
    iput-boolean v3, p0, Lcom/lge/systemservice/service/AATService;->mUseLog:Z

    #@8a
    .line 849
    invoke-direct {p0, v5}, Lcom/lge/systemservice/service/AATService;->AccessMiscRead(I)Ljava/lang/String;

    #@8d
    move-result-object v0

    #@8e
    .line 850
    iput-boolean v4, p0, Lcom/lge/systemservice/service/AATService;->mUseLog:Z

    #@90
    goto :goto_46

    #@91
    .line 852
    :cond_91
    const/4 v2, 0x3

    #@92
    if-ne p1, v2, :cond_d7

    #@94
    .line 855
    invoke-direct {p0, v5}, Lcom/lge/systemservice/service/AATService;->AccessMiscRead(I)Ljava/lang/String;

    #@97
    move-result-object v0

    #@98
    .line 856
    const-string v1, "AAT"

    #@9a
    new-instance v2, Ljava/lang/StringBuilder;

    #@9c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@9f
    const-string v3, "[AATService] oldSetVaule, aatset = "

    #@a1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v2

    #@a5
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v2

    #@a9
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ac
    move-result-object v2

    #@ad
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b0
    .line 857
    const-string v1, "0"

    #@b2
    const-string v2, "1"

    #@b4
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@b7
    move-result-object v0

    #@b8
    .line 858
    const-string v1, "AAT"

    #@ba
    new-instance v2, Ljava/lang/StringBuilder;

    #@bc
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@bf
    const-string v3, "[AATService] fullAatSetData, aatset = "

    #@c1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v2

    #@c5
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v2

    #@c9
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cc
    move-result-object v2

    #@cd
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d0
    .line 860
    invoke-direct {p0, v5, v0}, Lcom/lge/systemservice/service/AATService;->AccessMiscWrite(ILjava/lang/String;)V

    #@d3
    .line 861
    const-string v0, "misc write sccess"

    #@d5
    goto/16 :goto_46

    #@d7
    .line 865
    :cond_d7
    const/16 v2, 0x31

    #@d9
    if-ne v0, v2, :cond_ea

    #@db
    .line 867
    if-nez p1, :cond_e3

    #@dd
    .line 869
    invoke-direct {p0, v6}, Lcom/lge/systemservice/service/AATService;->AccessMiscRead(I)Ljava/lang/String;

    #@e0
    move-result-object v0

    #@e1
    goto/16 :goto_46

    #@e3
    .line 873
    :cond_e3
    const/4 v0, 0x7

    #@e4
    invoke-direct {p0, v0}, Lcom/lge/systemservice/service/AATService;->AccessMiscRead(I)Ljava/lang/String;

    #@e7
    move-result-object v0

    #@e8
    goto/16 :goto_46

    #@ea
    .line 876
    :cond_ea
    const/16 v2, 0x32

    #@ec
    if-ne v0, v2, :cond_fc

    #@ee
    .line 878
    if-nez p1, :cond_f7

    #@f0
    .line 880
    invoke-direct {p0, v6, v1}, Lcom/lge/systemservice/service/AATService;->AccessMiscWrite(ILjava/lang/String;)V

    #@f3
    .line 886
    :goto_f3
    const-string v0, "misc write sccess"

    #@f5
    goto/16 :goto_46

    #@f7
    .line 884
    :cond_f7
    const/4 v0, 0x7

    #@f8
    invoke-direct {p0, v0, v1}, Lcom/lge/systemservice/service/AATService;->AccessMiscWrite(ILjava/lang/String;)V

    #@fb
    goto :goto_f3

    #@fc
    .line 890
    :cond_fc
    const-string v1, "AAT"

    #@fe
    new-instance v2, Ljava/lang/StringBuilder;

    #@100
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@103
    const-string v3, "[AATService] requestToService, invaild header value = "

    #@105
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@108
    move-result-object v2

    #@109
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@10c
    move-result-object v0

    #@10d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@110
    move-result-object v0

    #@111
    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@114
    .line 891
    const-string v0, "invaild rw"

    #@116
    goto/16 :goto_46
.end method

.method public resetData()V
    .registers 4

    #@0
    .prologue
    const/16 v2, 0x14

    #@2
    .line 2533
    const/16 v0, 0x1f

    #@4
    const-string v1, "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24"

    #@6
    invoke-direct {p0, v0, v1}, Lcom/lge/systemservice/service/AATService;->AccessMiscWrite(ILjava/lang/String;)V

    #@9
    .line 2535
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@b
    const-string v1, "LG-F320L"

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10
    move-result v0

    #@11
    if-nez v0, :cond_27

    #@13
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@15
    const-string v1, "LG-F320K"

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1a
    move-result v0

    #@1b
    if-nez v0, :cond_27

    #@1d
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@1f
    const-string v1, "LG-F320S"

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24
    move-result v0

    #@25
    if-eqz v0, :cond_34

    #@27
    .line 2537
    :cond_27
    const-string v0, "1,1211,2,1,1,1,2,11,1,1,21,1,111,1111,11,2,121,2,2,11222,2,2111,11,1"

    #@29
    invoke-direct {p0, v2, v0}, Lcom/lge/systemservice/service/AATService;->AccessMiscWrite(ILjava/lang/String;)V

    #@2c
    .line 2577
    :goto_2c
    const-string v0, "AAT"

    #@2e
    const-string v1, "[AATService] AATSET + AAR ORDER Initialized "

    #@30
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 2578
    return-void

    #@34
    .line 2539
    :cond_34
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@36
    const-string v1, "LG-DS1203"

    #@38
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3b
    move-result v0

    #@3c
    if-eqz v0, :cond_44

    #@3e
    .line 2541
    const-string v0, "1,1211,2,1,1,1,2,11,1,1,21,1,111,1111,11,2,112,2,2,11222,2,2111,11,1"

    #@40
    invoke-direct {p0, v2, v0}, Lcom/lge/systemservice/service/AATService;->AccessMiscWrite(ILjava/lang/String;)V

    #@43
    goto :goto_2c

    #@44
    .line 2543
    :cond_44
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@46
    const-string v1, "VS980 4G"

    #@48
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4b
    move-result v0

    #@4c
    if-eqz v0, :cond_54

    #@4e
    .line 2545
    const-string v0, "1,1211,2,1,1,1,2,11,1,1,21,1,111,1111,21,2,121,2,2,11222,2,2111,11,1"

    #@50
    invoke-direct {p0, v2, v0}, Lcom/lge/systemservice/service/AATService;->AccessMiscWrite(ILjava/lang/String;)V

    #@53
    goto :goto_2c

    #@54
    .line 2547
    :cond_54
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@56
    const-string v1, "LG-D800"

    #@58
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5b
    move-result v0

    #@5c
    if-nez v0, :cond_72

    #@5e
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@60
    const-string v1, "LG-D801"

    #@62
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@65
    move-result v0

    #@66
    if-nez v0, :cond_72

    #@68
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@6a
    const-string v1, "LG-D803"

    #@6c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6f
    move-result v0

    #@70
    if-eqz v0, :cond_78

    #@72
    .line 2549
    :cond_72
    const-string v0, "1,1211,2,1,1,1,2,11,1,1,21,1,111,1111,21,2,122,2,2,11222,2,2111,11,1"

    #@74
    invoke-direct {p0, v2, v0}, Lcom/lge/systemservice/service/AATService;->AccessMiscWrite(ILjava/lang/String;)V

    #@77
    goto :goto_2c

    #@78
    .line 2551
    :cond_78
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@7a
    const-string v1, "LG-LS980"

    #@7c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7f
    move-result v0

    #@80
    if-nez v0, :cond_b4

    #@82
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@84
    const-string v1, "LG-D802"

    #@86
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@89
    move-result v0

    #@8a
    if-nez v0, :cond_b4

    #@8c
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@8e
    const-string v1, "LG-D802T"

    #@90
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@93
    move-result v0

    #@94
    if-nez v0, :cond_b4

    #@96
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@98
    const-string v1, "LG-D805"

    #@9a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9d
    move-result v0

    #@9e
    if-nez v0, :cond_b4

    #@a0
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@a2
    const-string v1, "LG-D806"

    #@a4
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a7
    move-result v0

    #@a8
    if-nez v0, :cond_b4

    #@aa
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@ac
    const-string v1, "LG-D807"

    #@ae
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b1
    move-result v0

    #@b2
    if-eqz v0, :cond_bb

    #@b4
    .line 2554
    :cond_b4
    const-string v0, "1,1211,2,1,1,1,2,11,1,1,21,1,111,1111,21,2,122,2,1,11222,2,2111,11,1"

    #@b6
    invoke-direct {p0, v2, v0}, Lcom/lge/systemservice/service/AATService;->AccessMiscWrite(ILjava/lang/String;)V

    #@b9
    goto/16 :goto_2c

    #@bb
    .line 2556
    :cond_bb
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@bd
    const-string v1, "LG-F240L"

    #@bf
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c2
    move-result v0

    #@c3
    if-eqz v0, :cond_cc

    #@c5
    .line 2558
    const-string v0, "1,1111,2,1,1,1,2,11,1,1,21,1,111,1111,11,2,122,2,2,11,2,1111,11"

    #@c7
    invoke-direct {p0, v2, v0}, Lcom/lge/systemservice/service/AATService;->AccessMiscWrite(ILjava/lang/String;)V

    #@ca
    goto/16 :goto_2c

    #@cc
    .line 2560
    :cond_cc
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@ce
    const-string v1, "LG-F300L"

    #@d0
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d3
    move-result v0

    #@d4
    if-nez v0, :cond_ea

    #@d6
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@d8
    const-string v1, "LG-F300K"

    #@da
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@dd
    move-result v0

    #@de
    if-nez v0, :cond_ea

    #@e0
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@e2
    const-string v1, "LG-F300S"

    #@e4
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e7
    move-result v0

    #@e8
    if-eqz v0, :cond_f1

    #@ea
    .line 2562
    :cond_ea
    const-string v0, "1,1111,2,1,1,1,2,11,1,1,21,1,111,1111,11,2,121,2,2,11222,2,1111,11,2"

    #@ec
    invoke-direct {p0, v2, v0}, Lcom/lge/systemservice/service/AATService;->AccessMiscWrite(ILjava/lang/String;)V

    #@ef
    goto/16 :goto_2c

    #@f1
    .line 2564
    :cond_f1
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@f3
    const-string v1, "LG-F340L"

    #@f5
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f8
    move-result v0

    #@f9
    if-nez v0, :cond_10f

    #@fb
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@fd
    const-string v1, "LG-F340K"

    #@ff
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@102
    move-result v0

    #@103
    if-nez v0, :cond_10f

    #@105
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@107
    const-string v1, "LG-F340S"

    #@109
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10c
    move-result v0

    #@10d
    if-eqz v0, :cond_116

    #@10f
    .line 2566
    :cond_10f
    const-string v0, "1,1211,2,1,1,1,2,11,1,1,21,1,111,1111,11,2,122,2,2,11222,2,2111,11,2"

    #@111
    invoke-direct {p0, v2, v0}, Lcom/lge/systemservice/service/AATService;->AccessMiscWrite(ILjava/lang/String;)V

    #@114
    goto/16 :goto_2c

    #@116
    .line 2568
    :cond_116
    sget-object v0, Lcom/lge/systemservice/service/AATService;->product_name:Ljava/lang/String;

    #@118
    const-string v1, "g2_jp_kdi"

    #@11a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11d
    move-result v0

    #@11e
    if-eqz v0, :cond_127

    #@120
    .line 2570
    const-string v0, "1,1211,2,1,1,1,2,11,1,1,11,1,111,1111,11,2,112,2,2,11222,2,2111,11,1"

    #@122
    invoke-direct {p0, v2, v0}, Lcom/lge/systemservice/service/AATService;->AccessMiscWrite(ILjava/lang/String;)V

    #@125
    goto/16 :goto_2c

    #@127
    .line 2574
    :cond_127
    const-string v0, "1,1111,2,1,1,1,2,11,1,1,21,1,111,1111,21,2,122,2,2,11222,2,2111,11,2"

    #@129
    invoke-direct {p0, v2, v0}, Lcom/lge/systemservice/service/AATService;->AccessMiscWrite(ILjava/lang/String;)V

    #@12c
    goto/16 :goto_2c
.end method

.method public setHallICFilePath()V
    .registers 4

    #@0
    .prologue
    .line 587
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@2
    const-string v1, "LG-F300L"

    #@4
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_1e

    #@a
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@c
    const-string v1, "LG-F300K"

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v0

    #@12
    if-nez v0, :cond_1e

    #@14
    sget-object v0, Lcom/lge/systemservice/service/AATService;->model_name:Ljava/lang/String;

    #@16
    const-string v1, "LG-F300S"

    #@18
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b
    move-result v0

    #@1c
    if-eqz v0, :cond_5b

    #@1e
    .line 589
    :cond_1e
    const-string v0, "/sys/devices/hall-bu52031nvx.74/pouch"

    #@20
    sput-object v0, Lcom/lge/systemservice/service/AATService;->HALLDEVICEFILE:Ljava/lang/String;

    #@22
    .line 590
    const-string v0, "/sys/devices/hall-bu52031nvx.74/pen"

    #@24
    sput-object v0, Lcom/lge/systemservice/service/AATService;->HALLDEVICEPENINOUT:Ljava/lang/String;

    #@26
    .line 592
    const-string v0, "AAT"

    #@28
    new-instance v1, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v2, "[AATService] HALLDEVICEFILE     :"

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    sget-object v2, Lcom/lge/systemservice/service/AATService;->HALLDEVICEFILE:Ljava/lang/String;

    #@35
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v1

    #@39
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v1

    #@3d
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 593
    const-string v0, "AAT"

    #@42
    new-instance v1, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v2, "[AATService] HALLDEVICEPENINOUT :"

    #@49
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v1

    #@4d
    sget-object v2, Lcom/lge/systemservice/service/AATService;->HALLDEVICEPENINOUT:Ljava/lang/String;

    #@4f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v1

    #@53
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v1

    #@57
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5a
    .line 604
    :goto_5a
    return-void

    #@5b
    .line 598
    :cond_5b
    const-string v0, "/sys/class/switch/smartcover/state"

    #@5d
    sput-object v0, Lcom/lge/systemservice/service/AATService;->HALLDEVICEFILE:Ljava/lang/String;

    #@5f
    .line 599
    const-string v0, "/sys/class/switch/dock/state"

    #@61
    sput-object v0, Lcom/lge/systemservice/service/AATService;->CARKITFILE:Ljava/lang/String;

    #@63
    .line 601
    const-string v0, "AAT"

    #@65
    new-instance v1, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    const-string v2, "[AATService] HALLDEVICEFILE :"

    #@6c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v1

    #@70
    sget-object v2, Lcom/lge/systemservice/service/AATService;->HALLDEVICEFILE:Ljava/lang/String;

    #@72
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v1

    #@76
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object v1

    #@7a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7d
    .line 602
    const-string v0, "AAT"

    #@7f
    new-instance v1, Ljava/lang/StringBuilder;

    #@81
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@84
    const-string v2, "[AATService] CARKITFILE     :"

    #@86
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v1

    #@8a
    sget-object v2, Lcom/lge/systemservice/service/AATService;->CARKITFILE:Ljava/lang/String;

    #@8c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v1

    #@90
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@93
    move-result-object v1

    #@94
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@97
    goto :goto_5a
.end method

.method public setLEDFilePath()V
    .registers 4

    #@0
    .prologue
    .line 520
    sget-object v0, Lcom/lge/systemservice/service/AATService;->product_device:Ljava/lang/String;

    #@2
    const-string v1, "vu3"

    #@4
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_65

    #@a
    .line 522
    const-string v0, "/sys/class/leds/R/brightness"

    #@c
    sput-object v0, Lcom/lge/systemservice/service/AATService;->ChargingLedRedFilePath:Ljava/lang/String;

    #@e
    .line 523
    const-string v0, "/sys/class/leds/G/brightness"

    #@10
    sput-object v0, Lcom/lge/systemservice/service/AATService;->ChargingLedGreenFilePath:Ljava/lang/String;

    #@12
    .line 524
    const-string v0, "/sys/class/leds/B/brightness"

    #@14
    sput-object v0, Lcom/lge/systemservice/service/AATService;->ChargingLedBlueFilePath:Ljava/lang/String;

    #@16
    .line 533
    :goto_16
    const-string v0, "AAT"

    #@18
    new-instance v1, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v2, "[AATService] RED LED:"

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    sget-object v2, Lcom/lge/systemservice/service/AATService;->ChargingLedRedFilePath:Ljava/lang/String;

    #@25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v1

    #@2d
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 534
    const-string v0, "AAT"

    #@32
    new-instance v1, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    const-string v2, "[AATService] GREEN LED:"

    #@39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    sget-object v2, Lcom/lge/systemservice/service/AATService;->ChargingLedRedFilePath:Ljava/lang/String;

    #@3f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v1

    #@43
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v1

    #@47
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    .line 535
    const-string v0, "AAT"

    #@4c
    new-instance v1, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v2, "[AATService] BLUE LED:"

    #@53
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v1

    #@57
    sget-object v2, Lcom/lge/systemservice/service/AATService;->ChargingLedRedFilePath:Ljava/lang/String;

    #@59
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v1

    #@5d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v1

    #@61
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@64
    .line 537
    return-void

    #@65
    .line 528
    :cond_65
    const-string v0, "/sys/class/leds/red/brightness"

    #@67
    sput-object v0, Lcom/lge/systemservice/service/AATService;->ChargingLedRedFilePath:Ljava/lang/String;

    #@69
    .line 529
    const-string v0, "/sys/class/leds/green/brightness"

    #@6b
    sput-object v0, Lcom/lge/systemservice/service/AATService;->ChargingLedGreenFilePath:Ljava/lang/String;

    #@6d
    .line 530
    const-string v0, "/sys/class/leds/blue/brightness"

    #@6f
    sput-object v0, Lcom/lge/systemservice/service/AATService;->ChargingLedBlueFilePath:Ljava/lang/String;

    #@71
    goto :goto_16
.end method

.method public setLedVal(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 11
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 1341
    const-string v0, "AAT"

    #@4
    new-instance v2, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v4, "[AATService] setLedVal : red = "

    #@b
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    const-string v4, ",  green = "

    #@15
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    const-string v4, ",  blue = "

    #@1f
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v2

    #@2b
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 1348
    const/4 v0, 0x0

    #@2f
    .line 1350
    const-string v2, "0"

    #@31
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@34
    move-result v2

    #@35
    if-eqz v2, :cond_14e

    #@37
    const-string v2, "0"

    #@39
    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3c
    move-result v2

    #@3d
    if-eqz v2, :cond_14e

    #@3f
    const-string v2, "0"

    #@41
    invoke-virtual {p3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@44
    move-result v2

    #@45
    if-eqz v2, :cond_14e

    #@47
    move v2, v3

    #@48
    .line 1357
    :goto_48
    if-eq v2, v3, :cond_52

    #@4a
    const-string v0, "0"

    #@4c
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4f
    move-result v0

    #@50
    if-nez v0, :cond_73

    #@52
    .line 1360
    :cond_52
    :try_start_52
    const-string v0, "AAT"

    #@54
    const-string v4, "[AATService] setLedVal ==> RED "

    #@56
    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@59
    .line 1361
    new-instance v0, Ljava/io/BufferedWriter;

    #@5b
    new-instance v4, Ljava/io/FileWriter;

    #@5d
    sget-object v5, Lcom/lge/systemservice/service/AATService;->ChargingLedRedFilePath:Ljava/lang/String;

    #@5f
    invoke-direct {v4, v5}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    #@62
    invoke-direct {v0, v4}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_65
    .catchall {:try_start_52 .. :try_end_65} :catchall_e3
    .catch Ljava/io/IOException; {:try_start_52 .. :try_end_65} :catch_cf

    #@65
    .line 1362
    :try_start_65
    invoke-virtual {v0, p1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@68
    .line 1363
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->flush()V

    #@6b
    .line 1364
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_6e
    .catchall {:try_start_65 .. :try_end_6e} :catchall_147
    .catch Ljava/io/IOException; {:try_start_65 .. :try_end_6e} :catch_14c

    #@6e
    .line 1369
    if-eqz v0, :cond_73

    #@70
    .line 1371
    :try_start_70
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_73
    .catch Ljava/io/IOException; {:try_start_70 .. :try_end_73} :catch_ca

    #@73
    .line 1384
    :cond_73
    :goto_73
    if-eq v2, v3, :cond_7d

    #@75
    const-string v0, "0"

    #@77
    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7a
    move-result v0

    #@7b
    if-nez v0, :cond_9e

    #@7d
    .line 1387
    :cond_7d
    :try_start_7d
    const-string v0, "AAT"

    #@7f
    const-string v4, "[AATService] setLedVal ==> GREEN "

    #@81
    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@84
    .line 1388
    new-instance v0, Ljava/io/BufferedWriter;

    #@86
    new-instance v4, Ljava/io/FileWriter;

    #@88
    sget-object v5, Lcom/lge/systemservice/service/AATService;->ChargingLedGreenFilePath:Ljava/lang/String;

    #@8a
    invoke-direct {v4, v5}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    #@8d
    invoke-direct {v0, v4}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_90
    .catchall {:try_start_7d .. :try_end_90} :catchall_108
    .catch Ljava/io/IOException; {:try_start_7d .. :try_end_90} :catch_f4

    #@90
    .line 1389
    :try_start_90
    invoke-virtual {v0, p2}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@93
    .line 1390
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->flush()V

    #@96
    .line 1391
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_99
    .catchall {:try_start_90 .. :try_end_99} :catchall_140
    .catch Ljava/io/IOException; {:try_start_90 .. :try_end_99} :catch_145

    #@99
    .line 1396
    if-eqz v0, :cond_9e

    #@9b
    .line 1398
    :try_start_9b
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_9e
    .catch Ljava/io/IOException; {:try_start_9b .. :try_end_9e} :catch_ef

    #@9e
    .line 1409
    :cond_9e
    :goto_9e
    if-eq v2, v3, :cond_a8

    #@a0
    const-string v0, "0"

    #@a2
    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a5
    move-result v0

    #@a6
    if-nez v0, :cond_c9

    #@a8
    .line 1412
    :cond_a8
    :try_start_a8
    const-string v0, "AAT"

    #@aa
    const-string v2, "[AATService] setLedVal ==> BLUE "

    #@ac
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@af
    .line 1413
    new-instance v0, Ljava/io/BufferedWriter;

    #@b1
    new-instance v2, Ljava/io/FileWriter;

    #@b3
    sget-object v3, Lcom/lge/systemservice/service/AATService;->ChargingLedBlueFilePath:Ljava/lang/String;

    #@b5
    invoke-direct {v2, v3}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    #@b8
    invoke-direct {v0, v2}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_bb
    .catchall {:try_start_a8 .. :try_end_bb} :catchall_12c
    .catch Ljava/io/IOException; {:try_start_a8 .. :try_end_bb} :catch_119

    #@bb
    .line 1414
    :try_start_bb
    invoke-virtual {v0, p3}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@be
    .line 1415
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->flush()V

    #@c1
    .line 1416
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_c4
    .catchall {:try_start_bb .. :try_end_c4} :catchall_138
    .catch Ljava/io/IOException; {:try_start_bb .. :try_end_c4} :catch_13d

    #@c4
    .line 1421
    if-eqz v0, :cond_c9

    #@c6
    .line 1423
    :try_start_c6
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_c9
    .catch Ljava/io/IOException; {:try_start_c6 .. :try_end_c9} :catch_114

    #@c9
    .line 1433
    :cond_c9
    :goto_c9
    return-void

    #@ca
    .line 1375
    :catch_ca
    move-exception v0

    #@cb
    .line 1376
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@ce
    goto :goto_73

    #@cf
    .line 1365
    :catch_cf
    move-exception v0

    #@d0
    move-object v0, v1

    #@d1
    .line 1366
    :goto_d1
    :try_start_d1
    const-string v4, "AAT"

    #@d3
    const-string v5, "[AATService] fail to write red"

    #@d5
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_d8
    .catchall {:try_start_d1 .. :try_end_d8} :catchall_147

    #@d8
    .line 1369
    if-eqz v0, :cond_73

    #@da
    .line 1371
    :try_start_da
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_dd
    .catch Ljava/io/IOException; {:try_start_da .. :try_end_dd} :catch_de

    #@dd
    goto :goto_73

    #@de
    .line 1375
    :catch_de
    move-exception v0

    #@df
    .line 1376
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@e2
    goto :goto_73

    #@e3
    .line 1368
    :catchall_e3
    move-exception v0

    #@e4
    .line 1369
    :goto_e4
    if-eqz v1, :cond_e9

    #@e6
    .line 1371
    :try_start_e6
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_e9
    .catch Ljava/io/IOException; {:try_start_e6 .. :try_end_e9} :catch_ea

    #@e9
    .line 1377
    :cond_e9
    :goto_e9
    throw v0

    #@ea
    .line 1375
    :catch_ea
    move-exception v1

    #@eb
    .line 1376
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@ee
    goto :goto_e9

    #@ef
    .line 1402
    :catch_ef
    move-exception v0

    #@f0
    .line 1403
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@f3
    goto :goto_9e

    #@f4
    .line 1392
    :catch_f4
    move-exception v0

    #@f5
    move-object v0, v1

    #@f6
    .line 1393
    :goto_f6
    :try_start_f6
    const-string v4, "AAT"

    #@f8
    const-string v5, "[AATService] fail to write green"

    #@fa
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_fd
    .catchall {:try_start_f6 .. :try_end_fd} :catchall_140

    #@fd
    .line 1396
    if-eqz v0, :cond_9e

    #@ff
    .line 1398
    :try_start_ff
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_102
    .catch Ljava/io/IOException; {:try_start_ff .. :try_end_102} :catch_103

    #@102
    goto :goto_9e

    #@103
    .line 1402
    :catch_103
    move-exception v0

    #@104
    .line 1403
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@107
    goto :goto_9e

    #@108
    .line 1395
    :catchall_108
    move-exception v0

    #@109
    .line 1396
    :goto_109
    if-eqz v1, :cond_10e

    #@10b
    .line 1398
    :try_start_10b
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_10e
    .catch Ljava/io/IOException; {:try_start_10b .. :try_end_10e} :catch_10f

    #@10e
    .line 1404
    :cond_10e
    :goto_10e
    throw v0

    #@10f
    .line 1402
    :catch_10f
    move-exception v1

    #@110
    .line 1403
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@113
    goto :goto_10e

    #@114
    .line 1427
    :catch_114
    move-exception v0

    #@115
    .line 1428
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@118
    goto :goto_c9

    #@119
    .line 1417
    :catch_119
    move-exception v0

    #@11a
    .line 1418
    :goto_11a
    :try_start_11a
    const-string v0, "AAT"

    #@11c
    const-string v2, "[AATService] fail to write blue"

    #@11e
    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_121
    .catchall {:try_start_11a .. :try_end_121} :catchall_12c

    #@121
    .line 1421
    if-eqz v1, :cond_c9

    #@123
    .line 1423
    :try_start_123
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_126
    .catch Ljava/io/IOException; {:try_start_123 .. :try_end_126} :catch_127

    #@126
    goto :goto_c9

    #@127
    .line 1427
    :catch_127
    move-exception v0

    #@128
    .line 1428
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@12b
    goto :goto_c9

    #@12c
    .line 1420
    :catchall_12c
    move-exception v0

    #@12d
    .line 1421
    :goto_12d
    if-eqz v1, :cond_132

    #@12f
    .line 1423
    :try_start_12f
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_132
    .catch Ljava/io/IOException; {:try_start_12f .. :try_end_132} :catch_133

    #@132
    .line 1429
    :cond_132
    :goto_132
    throw v0

    #@133
    .line 1427
    :catch_133
    move-exception v1

    #@134
    .line 1428
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@137
    goto :goto_132

    #@138
    .line 1420
    :catchall_138
    move-exception v1

    #@139
    move-object v6, v1

    #@13a
    move-object v1, v0

    #@13b
    move-object v0, v6

    #@13c
    goto :goto_12d

    #@13d
    .line 1417
    :catch_13d
    move-exception v1

    #@13e
    move-object v1, v0

    #@13f
    goto :goto_11a

    #@140
    .line 1395
    :catchall_140
    move-exception v1

    #@141
    move-object v6, v1

    #@142
    move-object v1, v0

    #@143
    move-object v0, v6

    #@144
    goto :goto_109

    #@145
    .line 1392
    :catch_145
    move-exception v4

    #@146
    goto :goto_f6

    #@147
    .line 1368
    :catchall_147
    move-exception v1

    #@148
    move-object v6, v1

    #@149
    move-object v1, v0

    #@14a
    move-object v0, v6

    #@14b
    goto :goto_e4

    #@14c
    .line 1365
    :catch_14c
    move-exception v4

    #@14d
    goto :goto_d1

    #@14e
    :cond_14e
    move v2, v0

    #@14f
    goto/16 :goto_48
.end method

.method public setRotationSensor(Z)V
    .registers 2
    .parameter "enable"

    #@0
    .prologue
    .line 1321
    return-void
.end method

.method public setSpeakerState(Ljava/lang/String;)V
    .registers 7
    .parameter

    #@0
    .prologue
    .line 2772
    const-string v1, "1"

    #@2
    .line 2773
    const-string v0, "/sys/bus/i2c/drivers/tpa2028d/0-0058/power_on"

    #@4
    .line 2775
    const-string v2, "topSpeakerOn"

    #@6
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_66

    #@c
    .line 2777
    const-string v1, "1"

    #@e
    .line 2778
    const-string v0, "/sys/bus/i2c/drivers/tpa2028d/0-0058/power_on"

    #@10
    .line 2798
    :cond_10
    :goto_10
    const-string v2, "AAT"

    #@12
    new-instance v3, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v4, "[AATService] setSpeakerState : "

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    const-string v4, "## onOff : "

    #@23
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v3

    #@2f
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 2802
    :try_start_32
    new-instance v2, Ljava/io/BufferedWriter;

    #@34
    new-instance v3, Ljava/io/FileWriter;

    #@36
    invoke-direct {v3, v0}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    #@39
    invoke-direct {v2, v3}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    #@3c
    .line 2803
    const-string v0, "AAT"

    #@3e
    new-instance v3, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v4, "[AATService] setSpeakerState aatSpeakerState = "

    #@45
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v3

    #@49
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v3

    #@4d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v3

    #@51
    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    .line 2805
    const/4 v0, 0x0

    #@55
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@58
    move-result v3

    #@59
    invoke-virtual {v2, v1, v0, v3}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;II)V

    #@5c
    .line 2806
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->newLine()V

    #@5f
    .line 2807
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->flush()V

    #@62
    .line 2809
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->close()V
    :try_end_65
    .catch Ljava/io/IOException; {:try_start_32 .. :try_end_65} :catch_8d

    #@65
    .line 2816
    :goto_65
    return-void

    #@66
    .line 2780
    :cond_66
    const-string v2, "topSpeakerOff"

    #@68
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6b
    move-result v2

    #@6c
    if-eqz v2, :cond_73

    #@6e
    .line 2782
    const-string v1, "0"

    #@70
    .line 2783
    const-string v0, "/sys/bus/i2c/drivers/tpa2028d/0-0058/power_on"

    #@72
    goto :goto_10

    #@73
    .line 2785
    :cond_73
    const-string v2, "bottomSpeakerOn"

    #@75
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@78
    move-result v2

    #@79
    if-eqz v2, :cond_80

    #@7b
    .line 2787
    const-string v1, "1"

    #@7d
    .line 2788
    const-string v0, "/sys/bus/i2c/drivers/tpa2028d2_amp/7-0058/power_on"

    #@7f
    goto :goto_10

    #@80
    .line 2790
    :cond_80
    const-string v2, "bottomSpeakerOff"

    #@82
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@85
    move-result v2

    #@86
    if-eqz v2, :cond_10

    #@88
    .line 2792
    const-string v1, "0"

    #@8a
    .line 2793
    const-string v0, "/sys/bus/i2c/drivers/tpa2028d2_amp/7-0058/power_on"

    #@8c
    goto :goto_10

    #@8d
    .line 2811
    :catch_8d
    move-exception v0

    #@8e
    .line 2813
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@91
    .line 2814
    const-string v0, "AAT"

    #@93
    const-string v1, "[AATService] setSpeakerState FileWriter Execption error()"

    #@95
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@98
    goto :goto_65
.end method

.method public startAutoScanFMRadio()Z
    .registers 2

    #@0
    .prologue
    .line 1786
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public startBackwardScanFMRadio()Z
    .registers 2

    #@0
    .prologue
    .line 1791
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public startForwardScanFMRadio()Z
    .registers 2

    #@0
    .prologue
    .line 1796
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public switchUSBMode(I)V
    .registers 8
    .parameter

    #@0
    .prologue
    .line 1974
    const/4 v1, 0x0

    #@1
    .line 1975
    const-string v0, ""

    #@3
    .line 1977
    if-nez p1, :cond_3b

    #@5
    .line 1979
    const-string v0, "0"

    #@7
    move-object v2, v0

    #@8
    .line 1987
    :goto_8
    :try_start_8
    const-string v0, "AAT"

    #@a
    new-instance v3, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v4, "[AATService] switchUSBMode: "

    #@11
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v3

    #@15
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v3

    #@1d
    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 1988
    new-instance v0, Ljava/io/BufferedWriter;

    #@22
    new-instance v3, Ljava/io/FileWriter;

    #@24
    sget-object v4, Lcom/lge/systemservice/service/AATService;->OTGFilePath:Ljava/lang/String;

    #@26
    invoke-direct {v3, v4}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    #@29
    invoke-direct {v0, v3}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_2c
    .catchall {:try_start_8 .. :try_end_2c} :catchall_58
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_2c} :catch_44

    #@2c
    .line 1989
    :try_start_2c
    invoke-virtual {v0, v2}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@2f
    .line 1990
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->flush()V

    #@32
    .line 1991
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_35
    .catchall {:try_start_2c .. :try_end_35} :catchall_64
    .catch Ljava/io/IOException; {:try_start_2c .. :try_end_35} :catch_69

    #@35
    .line 1996
    if-eqz v0, :cond_3a

    #@37
    .line 1998
    :try_start_37
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_3a
    .catch Ljava/io/IOException; {:try_start_37 .. :try_end_3a} :catch_3f

    #@3a
    .line 2006
    :cond_3a
    :goto_3a
    return-void

    #@3b
    .line 1983
    :cond_3b
    const-string v0, "1"

    #@3d
    move-object v2, v0

    #@3e
    goto :goto_8

    #@3f
    .line 2002
    :catch_3f
    move-exception v0

    #@40
    .line 2003
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@43
    goto :goto_3a

    #@44
    .line 1992
    :catch_44
    move-exception v0

    #@45
    move-object v0, v1

    #@46
    .line 1993
    :goto_46
    :try_start_46
    const-string v1, "AAT"

    #@48
    const-string v2, "[AATService] Fail to write switchUSBMode"

    #@4a
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4d
    .catchall {:try_start_46 .. :try_end_4d} :catchall_64

    #@4d
    .line 1996
    if-eqz v0, :cond_3a

    #@4f
    .line 1998
    :try_start_4f
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_52
    .catch Ljava/io/IOException; {:try_start_4f .. :try_end_52} :catch_53

    #@52
    goto :goto_3a

    #@53
    .line 2002
    :catch_53
    move-exception v0

    #@54
    .line 2003
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@57
    goto :goto_3a

    #@58
    .line 1995
    :catchall_58
    move-exception v0

    #@59
    .line 1996
    :goto_59
    if-eqz v1, :cond_5e

    #@5b
    .line 1998
    :try_start_5b
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_5e
    .catch Ljava/io/IOException; {:try_start_5b .. :try_end_5e} :catch_5f

    #@5e
    .line 2004
    :cond_5e
    :goto_5e
    throw v0

    #@5f
    .line 2002
    :catch_5f
    move-exception v1

    #@60
    .line 2003
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@63
    goto :goto_5e

    #@64
    .line 1995
    :catchall_64
    move-exception v1

    #@65
    move-object v5, v1

    #@66
    move-object v1, v0

    #@67
    move-object v0, v5

    #@68
    goto :goto_59

    #@69
    .line 1992
    :catch_69
    move-exception v1

    #@6a
    goto :goto_46
.end method

.method public testDualUSIM1()I
    .registers 2

    #@0
    .prologue
    .line 1659
    const/4 v0, -0x1

    #@1
    .line 1669
    .local v0, test_result:I
    return v0
.end method

.method public testDualUSIM2()I
    .registers 2

    #@0
    .prologue
    .line 1673
    const/4 v0, -0x1

    #@1
    .line 1683
    .local v0, test_result:I
    return v0
.end method

.method public testDualUSIM3()I
    .registers 2

    #@0
    .prologue
    .line 1687
    const/4 v0, -0x1

    #@1
    .line 1725
    .local v0, test_result:I
    const/4 v0, 0x1

    #@2
    .line 1727
    return v0
.end method

.method public tuneFrequencyFMRadio(I)Z
    .registers 3
    .parameter "nFreq"

    #@0
    .prologue
    .line 1801
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public turnOffFMRadio()Z
    .registers 2

    #@0
    .prologue
    .line 1760
    iget-object v0, p0, Lcom/lge/systemservice/service/AATService;->mFmRadioMgr:Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;

    #@2
    invoke-virtual {v0}, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->turnOff()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public turnOnFMRadio()Z
    .registers 2

    #@0
    .prologue
    .line 1744
    iget-object v0, p0, Lcom/lge/systemservice/service/AATService;->mFmRadioMgr:Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;

    #@2
    invoke-virtual {v0}, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->turnOn()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public writeFile(Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "Path"
    .parameter "value"

    #@0
    .prologue
    .line 609
    const-string v3, "AAT"

    #@2
    new-instance v4, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v5, "[AATService] writeFile : Path = "

    #@9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v4

    #@d
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v4

    #@11
    const-string v5, ", value = "

    #@13
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v4

    #@1f
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 610
    const/4 v0, 0x0

    #@23
    .line 613
    .local v0, Writer:Ljava/io/BufferedWriter;
    :try_start_23
    const-string v3, "AAT"

    #@25
    const-string v4, "[AATService] writeFile ==> POWER KEY LED "

    #@27
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 614
    new-instance v1, Ljava/io/BufferedWriter;

    #@2c
    new-instance v3, Ljava/io/FileWriter;

    #@2e
    invoke-direct {v3, p1}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    #@31
    invoke-direct {v1, v3}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_34
    .catchall {:try_start_23 .. :try_end_34} :catchall_5e
    .catch Ljava/io/IOException; {:try_start_23 .. :try_end_34} :catch_4b

    #@34
    .line 615
    .end local v0           #Writer:Ljava/io/BufferedWriter;
    .local v1, Writer:Ljava/io/BufferedWriter;
    :try_start_34
    invoke-virtual {v1, p2}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@37
    .line 616
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->flush()V

    #@3a
    .line 617
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_3d
    .catchall {:try_start_34 .. :try_end_3d} :catchall_6a
    .catch Ljava/io/IOException; {:try_start_34 .. :try_end_3d} :catch_6d

    #@3d
    .line 622
    if-eqz v1, :cond_42

    #@3f
    .line 624
    :try_start_3f
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_42
    .catch Ljava/io/IOException; {:try_start_3f .. :try_end_42} :catch_45

    #@42
    :cond_42
    move-object v0, v1

    #@43
    .line 632
    .end local v1           #Writer:Ljava/io/BufferedWriter;
    .restart local v0       #Writer:Ljava/io/BufferedWriter;
    :cond_43
    :goto_43
    const/4 v0, 0x0

    #@44
    .line 633
    return-void

    #@45
    .line 628
    .end local v0           #Writer:Ljava/io/BufferedWriter;
    .restart local v1       #Writer:Ljava/io/BufferedWriter;
    :catch_45
    move-exception v2

    #@46
    .line 629
    .local v2, e:Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    #@49
    move-object v0, v1

    #@4a
    .line 631
    .end local v1           #Writer:Ljava/io/BufferedWriter;
    .restart local v0       #Writer:Ljava/io/BufferedWriter;
    goto :goto_43

    #@4b
    .line 618
    .end local v2           #e:Ljava/io/IOException;
    :catch_4b
    move-exception v2

    #@4c
    .line 619
    .restart local v2       #e:Ljava/io/IOException;
    :goto_4c
    :try_start_4c
    const-string v3, "AAT"

    #@4e
    const-string v4, "[AATService] Fail to Write"

    #@50
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_53
    .catchall {:try_start_4c .. :try_end_53} :catchall_5e

    #@53
    .line 622
    if-eqz v0, :cond_43

    #@55
    .line 624
    :try_start_55
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_58
    .catch Ljava/io/IOException; {:try_start_55 .. :try_end_58} :catch_59

    #@58
    goto :goto_43

    #@59
    .line 628
    :catch_59
    move-exception v2

    #@5a
    .line 629
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    #@5d
    goto :goto_43

    #@5e
    .line 621
    .end local v2           #e:Ljava/io/IOException;
    :catchall_5e
    move-exception v3

    #@5f
    .line 622
    :goto_5f
    if-eqz v0, :cond_64

    #@61
    .line 624
    :try_start_61
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_64
    .catch Ljava/io/IOException; {:try_start_61 .. :try_end_64} :catch_65

    #@64
    .line 630
    :cond_64
    :goto_64
    throw v3

    #@65
    .line 628
    :catch_65
    move-exception v2

    #@66
    .line 629
    .restart local v2       #e:Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    #@69
    goto :goto_64

    #@6a
    .line 621
    .end local v0           #Writer:Ljava/io/BufferedWriter;
    .end local v2           #e:Ljava/io/IOException;
    .restart local v1       #Writer:Ljava/io/BufferedWriter;
    :catchall_6a
    move-exception v3

    #@6b
    move-object v0, v1

    #@6c
    .end local v1           #Writer:Ljava/io/BufferedWriter;
    .restart local v0       #Writer:Ljava/io/BufferedWriter;
    goto :goto_5f

    #@6d
    .line 618
    .end local v0           #Writer:Ljava/io/BufferedWriter;
    .restart local v1       #Writer:Ljava/io/BufferedWriter;
    :catch_6d
    move-exception v2

    #@6e
    move-object v0, v1

    #@6f
    .end local v1           #Writer:Ljava/io/BufferedWriter;
    .restart local v0       #Writer:Ljava/io/BufferedWriter;
    goto :goto_4c
.end method
