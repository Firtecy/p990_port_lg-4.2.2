.class Lcom/lge/systemservice/service/InfoCollector$4;
.super Ljava/util/TimerTask;
.source "InfoCollector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lge/systemservice/service/InfoCollector;->getPosition()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/InfoCollector;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/InfoCollector;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 514
    iput-object p1, p0, Lcom/lge/systemservice/service/InfoCollector$4;->this$0:Lcom/lge/systemservice/service/InfoCollector;

    #@2
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 8

    #@0
    .prologue
    .line 518
    invoke-static {}, Landroid/os/Looper;->prepare()V

    #@3
    .line 522
    invoke-static {}, Lcom/lge/systemservice/service/InfoCollector;->access$700()Z

    #@6
    move-result v0

    #@7
    if-nez v0, :cond_3d

    #@9
    .line 523
    invoke-static {}, Lcom/lge/systemservice/service/InfoCollector;->access$1000()Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    const-string v1, "gps"

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_3d

    #@15
    invoke-static {}, Lcom/lge/systemservice/service/InfoCollector;->access$800()Landroid/location/LocationManager;

    #@18
    move-result-object v0

    #@19
    if-eqz v0, :cond_3d

    #@1b
    .line 525
    :try_start_1b
    const-string v0, "network"

    #@1d
    invoke-static {v0}, Lcom/lge/systemservice/service/InfoCollector;->access$1002(Ljava/lang/String;)Ljava/lang/String;

    #@20
    .line 526
    invoke-static {}, Lcom/lge/systemservice/service/InfoCollector;->access$800()Landroid/location/LocationManager;

    #@23
    move-result-object v0

    #@24
    invoke-static {}, Lcom/lge/systemservice/service/InfoCollector;->access$900()Landroid/location/LocationListener;

    #@27
    move-result-object v1

    #@28
    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    #@2b
    .line 527
    invoke-static {}, Lcom/lge/systemservice/service/InfoCollector;->access$800()Landroid/location/LocationManager;

    #@2e
    move-result-object v0

    #@2f
    invoke-static {}, Lcom/lge/systemservice/service/InfoCollector;->access$1000()Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    const-wide/16 v2, 0x0

    #@35
    const/4 v4, 0x0

    #@36
    invoke-static {}, Lcom/lge/systemservice/service/InfoCollector;->access$900()Landroid/location/LocationListener;

    #@39
    move-result-object v5

    #@3a
    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V
    :try_end_3d
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_3d} :catch_41

    #@3d
    .line 541
    :cond_3d
    invoke-static {}, Landroid/os/Looper;->loop()V

    #@40
    .line 542
    :goto_40
    return-void

    #@41
    .line 532
    :catch_41
    move-exception v6

    #@42
    .line 536
    .local v6, e:Ljava/lang/Exception;
    iget-object v0, p0, Lcom/lge/systemservice/service/InfoCollector$4;->this$0:Lcom/lge/systemservice/service/InfoCollector;

    #@44
    invoke-virtual {v0}, Lcom/lge/systemservice/service/InfoCollector;->buildAndSendInfotoHTTPs()V

    #@47
    goto :goto_40
.end method
