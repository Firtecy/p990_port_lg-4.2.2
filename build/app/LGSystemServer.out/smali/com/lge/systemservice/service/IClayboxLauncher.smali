.class public interface abstract Lcom/lge/systemservice/service/IClayboxLauncher;
.super Ljava/lang/Object;
.source "IClayboxLauncher.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/systemservice/service/IClayboxLauncher$Stub;
    }
.end annotation


# virtual methods
.method public abstract startClaybox(Ljava/lang/String;Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract stopClaybox()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
