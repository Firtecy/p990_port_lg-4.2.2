.class final enum Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;
.super Ljava/lang/Enum;
.source "WfdIntroDialog.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;

.field public static final enum ORIENTATION_LAND:Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;

.field public static final enum ORIENTATION_PORT:Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;

.field public static final enum ORIENTATION_UNKNOWN:Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 40
    new-instance v0, Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;

    #@5
    const-string v1, "ORIENTATION_PORT"

    #@7
    invoke-direct {v0, v1, v2}, Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;-><init>(Ljava/lang/String;I)V

    #@a
    sput-object v0, Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;->ORIENTATION_PORT:Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;

    #@c
    .line 41
    new-instance v0, Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;

    #@e
    const-string v1, "ORIENTATION_LAND"

    #@10
    invoke-direct {v0, v1, v3}, Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;-><init>(Ljava/lang/String;I)V

    #@13
    sput-object v0, Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;->ORIENTATION_LAND:Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;

    #@15
    .line 42
    new-instance v0, Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;

    #@17
    const-string v1, "ORIENTATION_UNKNOWN"

    #@19
    invoke-direct {v0, v1, v4}, Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;-><init>(Ljava/lang/String;I)V

    #@1c
    sput-object v0, Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;->ORIENTATION_UNKNOWN:Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;

    #@1e
    .line 39
    const/4 v0, 0x3

    #@1f
    new-array v0, v0, [Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;

    #@21
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;->ORIENTATION_PORT:Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;

    #@23
    aput-object v1, v0, v2

    #@25
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;->ORIENTATION_LAND:Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;

    #@27
    aput-object v1, v0, v3

    #@29
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;->ORIENTATION_UNKNOWN:Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;

    #@2b
    aput-object v1, v0, v4

    #@2d
    sput-object v0, Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;->$VALUES:[Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;

    #@2f
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;
    .registers 2
    .parameter

    #@0
    .prologue
    .line 39
    const-class v0, Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;
    .registers 1

    #@0
    .prologue
    .line 39
    sget-object v0, Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;->$VALUES:[Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;

    #@2
    invoke-virtual {v0}, [Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;

    #@8
    return-object v0
.end method
