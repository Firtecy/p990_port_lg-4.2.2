.class Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$15;
.super Ljava/lang/Object;
.source "WfdStateTracker.java"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->handleRtspStateChange(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1412
    iput-object p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$15;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onFailure(I)V
    .registers 5
    .parameter "reason"

    #@0
    .prologue
    .line 1418
    const-string v0, "WfdStateTracker"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "RTSP deinit: Direct disconnect fail, reason: "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1419
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$15;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@1a
    invoke-static {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$700(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Z

    #@1d
    .line 1420
    return-void
.end method

.method public onSuccess()V
    .registers 3

    #@0
    .prologue
    .line 1414
    const-string v0, "WfdStateTracker"

    #@2
    const-string v1, "RTSP deinit: Direct disconnect success"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1415
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$15;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@9
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;->DISABLE:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;

    #@b
    invoke-static {v0, v1}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$2202(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingCmd;

    #@e
    .line 1416
    return-void
.end method
