.class Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$TimeoutTask;
.super Ljava/lang/Object;
.source "EmotionalLedService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TimeoutTask"
.end annotation


# instance fields
.field nextState:I

.field final synthetic this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;


# direct methods
.method private constructor <init>(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1177
    iput-object p1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$TimeoutTask;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1177
    invoke-direct {p0, p1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$TimeoutTask;-><init>(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)V

    #@3
    return-void
.end method


# virtual methods
.method public run()V
    .registers 6

    #@0
    .prologue
    .line 1180
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$TimeoutTask;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@2
    invoke-static {v2}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$3400(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Ljava/lang/Object;

    #@5
    move-result-object v3

    #@6
    monitor-enter v3

    #@7
    .line 1181
    :try_start_7
    iget v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$TimeoutTask;->nextState:I

    #@9
    const/4 v4, -0x1

    #@a
    if-ne v2, v4, :cond_e

    #@c
    .line 1182
    monitor-exit v3

    #@d
    .line 1197
    :goto_d
    return-void

    #@e
    .line 1184
    :cond_e
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@11
    move-result-wide v0

    #@12
    .line 1186
    .local v0, now:J
    iget v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$TimeoutTask;->nextState:I

    #@14
    packed-switch v2, :pswitch_data_2a

    #@17
    .line 1196
    :goto_17
    monitor-exit v3

    #@18
    goto :goto_d

    #@19
    .end local v0           #now:J
    :catchall_19
    move-exception v2

    #@1a
    monitor-exit v3
    :try_end_1b
    .catchall {:try_start_7 .. :try_end_1b} :catchall_19

    #@1b
    throw v2

    #@1c
    .line 1188
    .restart local v0       #now:J
    :pswitch_1c
    :try_start_1c
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$TimeoutTask;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@1e
    const/4 v4, 0x0

    #@1f
    invoke-static {v2, v0, v1, v4}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$2600(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;JI)V

    #@22
    goto :goto_17

    #@23
    .line 1191
    :pswitch_23
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$TimeoutTask;->this$0:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;

    #@25
    const/4 v4, 0x1

    #@26
    invoke-static {v2, v0, v1, v4}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->access$2600(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;JI)V
    :try_end_29
    .catchall {:try_start_1c .. :try_end_29} :catchall_19

    #@29
    goto :goto_17

    #@2a
    .line 1186
    :pswitch_data_2a
    .packed-switch 0x0
        :pswitch_23
        :pswitch_1c
    .end packed-switch
.end method
