.class Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;
.super Landroid/os/Handler;
.source "WfdStateTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 297
    iput-object p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 13
    .parameter "msg"

    #@0
    .prologue
    const v10, 0x2090254

    #@3
    const v2, 0x20a01cb

    #@6
    const/16 v9, 0x7d8

    #@8
    const/4 v8, 0x0

    #@9
    const/4 v7, 0x1

    #@a
    .line 300
    iget v0, p1, Landroid/os/Message;->what:I

    #@c
    const/16 v1, 0x3e8

    #@e
    if-le v0, v1, :cond_e5

    #@10
    .line 301
    iget v0, p1, Landroid/os/Message;->what:I

    #@12
    packed-switch v0, :pswitch_data_278

    #@15
    .line 363
    const-string v0, "WfdStateTracker"

    #@17
    new-instance v1, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v2, "Unhandled Timer: "

    #@1e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    iget v2, p1, Landroid/os/Message;->what:I

    #@24
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@27
    move-result-object v1

    #@28
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v1

    #@2c
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 540
    :cond_2f
    :goto_2f
    return-void

    #@30
    .line 303
    :pswitch_30
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@32
    invoke-static {v0, v8}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$502(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;Z)Z

    #@35
    .line 304
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@37
    invoke-static {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$600(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)V

    #@3a
    goto :goto_2f

    #@3b
    .line 307
    :pswitch_3b
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@3d
    invoke-virtual {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdState()I

    #@40
    move-result v0

    #@41
    const/4 v1, 0x7

    #@42
    if-ne v0, v1, :cond_2f

    #@44
    .line 308
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@46
    invoke-static {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$400(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Landroid/net/wifi/p2p/WifiP2pManager;

    #@49
    move-result-object v0

    #@4a
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@4c
    invoke-static {v1}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$000(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@4f
    move-result-object v1

    #@50
    new-instance v2, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3$1;

    #@52
    invoke-direct {v2, p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3$1;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;)V

    #@55
    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->removeGroup(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    #@58
    .line 316
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@5a
    invoke-static {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$700(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Z

    #@5d
    goto :goto_2f

    #@5e
    .line 320
    :pswitch_5e
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@60
    invoke-virtual {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdState()I

    #@63
    move-result v0

    #@64
    const/4 v1, 0x6

    #@65
    if-ne v0, v1, :cond_2f

    #@67
    .line 321
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@69
    invoke-static {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$400(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Landroid/net/wifi/p2p/WifiP2pManager;

    #@6c
    move-result-object v0

    #@6d
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@6f
    invoke-static {v1}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$000(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@72
    move-result-object v1

    #@73
    new-instance v2, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3$2;

    #@75
    invoke-direct {v2, p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3$2;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;)V

    #@78
    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->removeGroup(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    #@7b
    goto :goto_2f

    #@7c
    .line 338
    :pswitch_7c
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@7e
    invoke-virtual {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdState()I

    #@81
    move-result v0

    #@82
    const/4 v1, 0x7

    #@83
    if-ne v0, v1, :cond_2f

    #@85
    .line 339
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@87
    invoke-static {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$700(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Z

    #@8a
    goto :goto_2f

    #@8b
    .line 343
    :pswitch_8b
    const-string v0, "WfdStateTracker"

    #@8d
    const-string v1, "WFD_TIMER_UIBC_DISABLE_TIMEOUT"

    #@8f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@92
    .line 344
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@94
    invoke-static {v0, v8}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$802(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;Z)Z

    #@97
    .line 345
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@99
    invoke-static {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$900(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

    #@9c
    move-result-object v0

    #@9d
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;->WFD_DISABLE:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

    #@9f
    if-ne v0, v1, :cond_ae

    #@a1
    .line 346
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@a3
    invoke-virtual {v0, v8}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWifiDisplayEnabled(Z)Z

    #@a6
    .line 352
    :goto_a6
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@a8
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;->NONE:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

    #@aa
    invoke-static {v0, v1}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$902(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

    #@ad
    goto :goto_2f

    #@ae
    .line 347
    :cond_ae
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@b0
    invoke-static {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$900(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

    #@b3
    move-result-object v0

    #@b4
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;->WFD_DISCONNECT:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$WfdPendingTeardown;

    #@b6
    if-ne v0, v1, :cond_be

    #@b8
    .line 348
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@ba
    invoke-virtual {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->cancelWifiDisplayConnect()Z

    #@bd
    goto :goto_a6

    #@be
    .line 350
    :cond_be
    const-string v0, "WfdStateTracker"

    #@c0
    const-string v1, "UIBC_TIMEOUT: No pending teardown command"

    #@c2
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c5
    goto :goto_a6

    #@c6
    .line 355
    :pswitch_c6
    const-string v0, "WfdStateTracker"

    #@c8
    const-string v1, "WFD_TIMER_DLNA_CONNECT_TIMEOUT"

    #@ca
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@cd
    .line 356
    invoke-static {}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$1000()Ljava/lang/String;

    #@d0
    move-result-object v0

    #@d1
    if-eqz v0, :cond_dd

    #@d3
    .line 357
    const/4 v0, 0x0

    #@d4
    invoke-static {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$1002(Ljava/lang/String;)Ljava/lang/String;

    #@d7
    .line 358
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@d9
    const/4 v1, 0x0

    #@da
    invoke-static {v0, v1}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$1102(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;Landroid/net/wifi/p2p/WifiP2pDevice;)Landroid/net/wifi/p2p/WifiP2pDevice;

    #@dd
    .line 360
    :cond_dd
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@df
    const/4 v1, 0x4

    #@e0
    invoke-static {v0, v1}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;I)V

    #@e3
    goto/16 :goto_2f

    #@e5
    .line 380
    :cond_e5
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@e7
    invoke-static {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$1200(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Landroid/app/StatusBarManager;

    #@ea
    move-result-object v0

    #@eb
    if-eqz v0, :cond_f6

    #@ed
    .line 381
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@ef
    invoke-static {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$1300(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Landroid/app/StatusBarManager;

    #@f2
    move-result-object v0

    #@f3
    invoke-virtual {v0}, Landroid/app/StatusBarManager;->collapsePanels()V

    #@f6
    .line 383
    :cond_f6
    iget v0, p1, Landroid/os/Message;->what:I

    #@f8
    packed-switch v0, :pswitch_data_288

    #@fb
    goto/16 :goto_2f

    #@fd
    .line 385
    :pswitch_fd
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@ff
    invoke-static {v0, v7}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;I)V

    #@102
    .line 386
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@104
    new-instance v1, Landroid/app/AlertDialog$Builder;

    #@106
    iget-object v3, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@108
    invoke-static {v3}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$1500(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Landroid/content/Context;

    #@10b
    move-result-object v3

    #@10c
    invoke-direct {v1, v3, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    #@10f
    const v2, 0x2090391

    #@112
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    #@115
    move-result-object v1

    #@116
    const v2, 0x2090396

    #@119
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    #@11c
    move-result-object v1

    #@11d
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@11f
    iget-object v2, v2, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->listener:Landroid/content/DialogInterface$OnClickListener;

    #@121
    invoke-virtual {v1, v10, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@124
    move-result-object v1

    #@125
    const v2, 0x2090393

    #@128
    iget-object v3, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@12a
    iget-object v3, v3, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->listener:Landroid/content/DialogInterface$OnClickListener;

    #@12c
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@12f
    move-result-object v1

    #@130
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@132
    iget-object v2, v2, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->listener_cancel:Landroid/content/DialogInterface$OnCancelListener;

    #@134
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    #@137
    move-result-object v1

    #@138
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@13b
    move-result-object v1

    #@13c
    invoke-static {v0, v1}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$1402(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    #@13f
    .line 393
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@141
    invoke-static {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$1400(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Landroid/app/AlertDialog;

    #@144
    move-result-object v0

    #@145
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@148
    move-result-object v0

    #@149
    invoke-virtual {v0, v9}, Landroid/view/Window;->setType(I)V

    #@14c
    .line 394
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@14e
    invoke-static {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$1400(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Landroid/app/AlertDialog;

    #@151
    move-result-object v0

    #@152
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    #@155
    goto/16 :goto_2f

    #@157
    .line 397
    :pswitch_157
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@159
    invoke-static {v0, v7}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;I)V

    #@15c
    .line 399
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@15e
    invoke-static {v0, v8}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$1602(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;Z)Z

    #@161
    .line 401
    iget-object v6, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@163
    new-instance v0, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;

    #@165
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@167
    invoke-static {v1}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$1500(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Landroid/content/Context;

    #@16a
    move-result-object v1

    #@16b
    new-instance v3, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3$3;

    #@16d
    invoke-direct {v3, p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3$3;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;)V

    #@170
    new-instance v4, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3$4;

    #@172
    invoke-direct {v4, p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3$4;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;)V

    #@175
    new-instance v5, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3$5;

    #@177
    invoke-direct {v5, p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3$5;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;)V

    #@17a
    invoke-direct/range {v0 .. v5}, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;-><init>(Landroid/content/Context;ILandroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;Landroid/view/View$OnClickListener;)V

    #@17d
    invoke-static {v6, v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$1702(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;)Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;

    #@180
    .line 445
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@182
    invoke-static {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$1700(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;

    #@185
    move-result-object v0

    #@186
    invoke-virtual {v0}, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->getWindow()Landroid/view/Window;

    #@189
    move-result-object v0

    #@18a
    invoke-virtual {v0, v9}, Landroid/view/Window;->setType(I)V

    #@18d
    .line 446
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@18f
    invoke-static {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$1700(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;

    #@192
    move-result-object v0

    #@193
    invoke-virtual {v0}, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->show()V

    #@196
    goto/16 :goto_2f

    #@198
    .line 449
    :pswitch_198
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@19a
    invoke-static {v0, v7}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;I)V

    #@19d
    .line 473
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@19f
    invoke-static {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$1500(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Landroid/content/Context;

    #@1a2
    move-result-object v0

    #@1a3
    const v1, 0x209039e

    #@1a6
    invoke-static {v0, v1, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    #@1a9
    move-result-object v0

    #@1aa
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    #@1ad
    .line 476
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@1af
    invoke-static {v0, v8}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;I)V

    #@1b2
    goto/16 :goto_2f

    #@1b4
    .line 481
    :pswitch_1b4
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@1b6
    new-instance v1, Landroid/app/AlertDialog$Builder;

    #@1b8
    iget-object v3, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@1ba
    invoke-static {v3}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$1500(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Landroid/content/Context;

    #@1bd
    move-result-object v3

    #@1be
    invoke-direct {v1, v3, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    #@1c1
    const v2, 0x2090256

    #@1c4
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    #@1c7
    move-result-object v1

    #@1c8
    const v2, 0x1010355

    #@1cb
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    #@1ce
    move-result-object v1

    #@1cf
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@1d1
    invoke-static {v2}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$1500(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Landroid/content/Context;

    #@1d4
    move-result-object v2

    #@1d5
    const v3, 0x209039a

    #@1d8
    new-array v4, v7, [Ljava/lang/Object;

    #@1da
    invoke-static {}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$2000()[[Ljava/lang/String;

    #@1dd
    move-result-object v5

    #@1de
    iget v6, p1, Landroid/os/Message;->arg1:I

    #@1e0
    aget-object v5, v5, v6

    #@1e2
    aget-object v5, v5, v7

    #@1e4
    aput-object v5, v4, v8

    #@1e6
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@1e9
    move-result-object v2

    #@1ea
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@1ed
    move-result-object v1

    #@1ee
    new-instance v2, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3$7;

    #@1f0
    invoke-direct {v2, p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3$7;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;)V

    #@1f3
    invoke-virtual {v1, v10, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@1f6
    move-result-object v1

    #@1f7
    new-instance v2, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3$6;

    #@1f9
    invoke-direct {v2, p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3$6;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;)V

    #@1fc
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    #@1ff
    move-result-object v1

    #@200
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@203
    move-result-object v1

    #@204
    invoke-static {v0, v1}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$1902(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    #@207
    .line 502
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@209
    invoke-static {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$1900(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Landroid/app/AlertDialog;

    #@20c
    move-result-object v0

    #@20d
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@210
    move-result-object v0

    #@211
    invoke-virtual {v0, v9}, Landroid/view/Window;->setType(I)V

    #@214
    .line 503
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@216
    invoke-static {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$1900(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Landroid/app/AlertDialog;

    #@219
    move-result-object v0

    #@21a
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    #@21d
    goto/16 :goto_2f

    #@21f
    .line 506
    :pswitch_21f
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@221
    new-instance v1, Landroid/app/AlertDialog$Builder;

    #@223
    iget-object v3, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@225
    invoke-static {v3}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$1500(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Landroid/content/Context;

    #@228
    move-result-object v3

    #@229
    invoke-direct {v1, v3, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    #@22c
    const v2, 0x209039b

    #@22f
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    #@232
    move-result-object v1

    #@233
    new-instance v2, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3$8;

    #@235
    invoke-direct {v2, p0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3$8;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;)V

    #@238
    invoke-virtual {v1, v10, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@23b
    move-result-object v1

    #@23c
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@23f
    move-result-object v1

    #@240
    invoke-static {v0, v1}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$2102(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    #@243
    .line 523
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@245
    invoke-static {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$2100(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Landroid/app/AlertDialog;

    #@248
    move-result-object v0

    #@249
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@24c
    move-result-object v0

    #@24d
    invoke-virtual {v0, v9}, Landroid/view/Window;->setType(I)V

    #@250
    .line 524
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@252
    invoke-static {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$2100(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Landroid/app/AlertDialog;

    #@255
    move-result-object v0

    #@256
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    #@259
    goto/16 :goto_2f

    #@25b
    .line 527
    :pswitch_25b
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@25d
    invoke-static {v0, v7}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;I)V

    #@260
    .line 528
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@262
    invoke-static {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$1500(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)Landroid/content/Context;

    #@265
    move-result-object v0

    #@266
    const v1, 0x209039d

    #@269
    invoke-static {v0, v1, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    #@26c
    move-result-object v0

    #@26d
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    #@270
    .line 531
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$3;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@272
    invoke-static {v0, v8}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;I)V

    #@275
    goto/16 :goto_2f

    #@277
    .line 301
    nop

    #@278
    :pswitch_data_278
    .packed-switch 0x3e9
        :pswitch_30
        :pswitch_3b
        :pswitch_5e
        :pswitch_7c
        :pswitch_8b
        :pswitch_c6
    .end packed-switch

    #@288
    .line 383
    :pswitch_data_288
    .packed-switch 0x0
        :pswitch_fd
        :pswitch_157
        :pswitch_198
        :pswitch_1b4
        :pswitch_21f
        :pswitch_25b
    .end packed-switch
.end method
