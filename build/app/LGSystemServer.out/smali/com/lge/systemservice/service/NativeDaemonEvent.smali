.class public Lcom/lge/systemservice/service/NativeDaemonEvent;
.super Ljava/lang/Object;
.source "NativeDaemonEvent.java"


# instance fields
.field private final mCmdNumber:I

.field private final mCode:I

.field private final mMessage:Ljava/lang/String;

.field private mParsed:[Ljava/lang/String;

.field private final mRawEvent:Ljava/lang/String;


# direct methods
.method private constructor <init>(IILjava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "cmdNumber"
    .parameter "code"
    .parameter "message"
    .parameter "rawEvent"

    #@0
    .prologue
    .line 38
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 39
    iput p1, p0, Lcom/lge/systemservice/service/NativeDaemonEvent;->mCmdNumber:I

    #@5
    .line 40
    iput p2, p0, Lcom/lge/systemservice/service/NativeDaemonEvent;->mCode:I

    #@7
    .line 41
    iput-object p3, p0, Lcom/lge/systemservice/service/NativeDaemonEvent;->mMessage:Ljava/lang/String;

    #@9
    .line 42
    iput-object p4, p0, Lcom/lge/systemservice/service/NativeDaemonEvent;->mRawEvent:Ljava/lang/String;

    #@b
    .line 43
    const/4 v0, 0x0

    #@c
    iput-object v0, p0, Lcom/lge/systemservice/service/NativeDaemonEvent;->mParsed:[Ljava/lang/String;

    #@e
    .line 44
    return-void
.end method

.method private static isClassUnsolicited(I)Z
    .registers 2
    .parameter "code"

    #@0
    .prologue
    .line 105
    const/16 v0, 0x258

    #@2
    if-lt p0, v0, :cond_a

    #@4
    const/16 v0, 0x2bc

    #@6
    if-ge p0, v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public static parseRawEvent(Ljava/lang/String;)Lcom/lge/systemservice/service/NativeDaemonEvent;
    .registers 9
    .parameter "rawEvent"

    #@0
    .prologue
    .line 126
    const-string v6, " "

    #@2
    invoke-virtual {p0, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@5
    move-result-object v4

    #@6
    .line 127
    .local v4, parsed:[Ljava/lang/String;
    array-length v6, v4

    #@7
    const/4 v7, 0x2

    #@8
    if-ge v6, v7, :cond_12

    #@a
    .line 128
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@c
    const-string v7, "Insufficient arguments"

    #@e
    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@11
    throw v6

    #@12
    .line 131
    :cond_12
    const/4 v5, 0x0

    #@13
    .line 135
    .local v5, skiplength:I
    const/4 v6, 0x0

    #@14
    :try_start_14
    aget-object v6, v4, v6

    #@16
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@19
    move-result v1

    #@1a
    .line 136
    .local v1, code:I
    const/4 v6, 0x0

    #@1b
    aget-object v6, v4, v6

    #@1d
    invoke-virtual {v6}, Ljava/lang/String;->length()I
    :try_end_20
    .catch Ljava/lang/NumberFormatException; {:try_start_14 .. :try_end_20} :catch_36

    #@20
    move-result v6

    #@21
    add-int/lit8 v5, v6, 0x1

    #@23
    .line 141
    const/4 v0, -0x1

    #@24
    .line 142
    .local v0, cmdNumber:I
    invoke-static {v1}, Lcom/lge/systemservice/service/NativeDaemonEvent;->isClassUnsolicited(I)Z

    #@27
    move-result v6

    #@28
    if-nez v6, :cond_50

    #@2a
    .line 143
    array-length v6, v4

    #@2b
    const/4 v7, 0x3

    #@2c
    if-ge v6, v7, :cond_3f

    #@2e
    .line 144
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@30
    const-string v7, "Insufficient arguemnts"

    #@32
    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@35
    throw v6

    #@36
    .line 137
    .end local v0           #cmdNumber:I
    .end local v1           #code:I
    :catch_36
    move-exception v2

    #@37
    .line 138
    .local v2, e:Ljava/lang/NumberFormatException;
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@39
    const-string v7, "problem parsing code"

    #@3b
    invoke-direct {v6, v7, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@3e
    throw v6

    #@3f
    .line 147
    .end local v2           #e:Ljava/lang/NumberFormatException;
    .restart local v0       #cmdNumber:I
    .restart local v1       #code:I
    :cond_3f
    const/4 v6, 0x1

    #@40
    :try_start_40
    aget-object v6, v4, v6

    #@42
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@45
    move-result v0

    #@46
    .line 148
    const/4 v6, 0x1

    #@47
    aget-object v6, v4, v6

    #@49
    invoke-virtual {v6}, Ljava/lang/String;->length()I
    :try_end_4c
    .catch Ljava/lang/NumberFormatException; {:try_start_40 .. :try_end_4c} :catch_5a

    #@4c
    move-result v6

    #@4d
    add-int/lit8 v6, v6, 0x1

    #@4f
    add-int/2addr v5, v6

    #@50
    .line 154
    :cond_50
    invoke-virtual {p0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@53
    move-result-object v3

    #@54
    .line 156
    .local v3, message:Ljava/lang/String;
    new-instance v6, Lcom/lge/systemservice/service/NativeDaemonEvent;

    #@56
    invoke-direct {v6, v0, v1, v3, p0}, Lcom/lge/systemservice/service/NativeDaemonEvent;-><init>(IILjava/lang/String;Ljava/lang/String;)V

    #@59
    return-object v6

    #@5a
    .line 149
    .end local v3           #message:Ljava/lang/String;
    :catch_5a
    move-exception v2

    #@5b
    .line 150
    .restart local v2       #e:Ljava/lang/NumberFormatException;
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@5d
    const-string v7, "problem parsing cmdNumber"

    #@5f
    invoke-direct {v6, v7, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@62
    throw v6
.end method

.method public static unescapeArgs(Ljava/lang/String;)[Ljava/lang/String;
    .registers 16
    .parameter "rawEvent"

    #@0
    .prologue
    const/16 v14, 0x22

    #@2
    const/16 v13, 0x20

    #@4
    const/4 v12, -0x1

    #@5
    .line 189
    const/4 v0, 0x0

    #@6
    .line 190
    .local v0, DEBUG_ROUTINE:Z
    const-string v1, "unescapeArgs"

    #@8
    .line 191
    .local v1, LOGTAG:Ljava/lang/String;
    new-instance v6, Ljava/util/ArrayList;

    #@a
    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    #@d
    .line 192
    .local v6, parsed:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@10
    move-result v3

    #@11
    .line 193
    .local v3, length:I
    const/4 v2, 0x0

    #@12
    .line 194
    .local v2, current:I
    const/4 v9, -0x1

    #@13
    .line 195
    .local v9, wordEnd:I
    const/4 v7, 0x0

    #@14
    .line 198
    .local v7, quoted:Z
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    #@17
    move-result v10

    #@18
    if-ne v10, v14, :cond_1d

    #@1a
    .line 199
    const/4 v7, 0x1

    #@1b
    .line 200
    add-int/lit8 v2, v2, 0x1

    #@1d
    .line 202
    :cond_1d
    :goto_1d
    if-ge v2, v3, :cond_78

    #@1f
    .line 204
    if-eqz v7, :cond_6a

    #@21
    .line 205
    move v9, v2

    #@22
    .line 206
    :goto_22
    invoke-virtual {p0, v14, v9}, Ljava/lang/String;->indexOf(II)I

    #@25
    move-result v9

    #@26
    if-eq v9, v12, :cond_32

    #@28
    .line 207
    add-int/lit8 v10, v9, -0x1

    #@2a
    invoke-virtual {p0, v10}, Ljava/lang/String;->charAt(I)C

    #@2d
    move-result v10

    #@2e
    const/16 v11, 0x5c

    #@30
    if-eq v10, v11, :cond_67

    #@32
    .line 217
    :cond_32
    :goto_32
    if-ne v9, v12, :cond_35

    #@34
    move v9, v3

    #@35
    .line 218
    :cond_35
    invoke-virtual {p0, v2, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@38
    move-result-object v8

    #@39
    .line 219
    .local v8, word:Ljava/lang/String;
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    #@3c
    move-result v10

    #@3d
    add-int/2addr v2, v10

    #@3e
    .line 220
    if-nez v7, :cond_6f

    #@40
    .line 221
    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@43
    move-result-object v8

    #@44
    .line 226
    :goto_44
    const-string v10, "\\\\"

    #@46
    const-string v11, "\\"

    #@48
    invoke-virtual {v8, v10, v11}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@4b
    .line 227
    const-string v10, "\\\""

    #@4d
    const-string v11, "\""

    #@4f
    invoke-virtual {v8, v10, v11}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@52
    .line 230
    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@55
    .line 233
    invoke-virtual {p0, v13, v2}, Ljava/lang/String;->indexOf(II)I

    #@58
    move-result v5

    #@59
    .line 234
    .local v5, nextSpace:I
    const-string v10, " \""

    #@5b
    invoke-virtual {p0, v10, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    #@5e
    move-result v4

    #@5f
    .line 238
    .local v4, nextQuote:I
    if-le v4, v12, :cond_72

    #@61
    if-gt v4, v5, :cond_72

    #@63
    .line 239
    const/4 v7, 0x1

    #@64
    .line 240
    add-int/lit8 v2, v4, 0x2

    #@66
    goto :goto_1d

    #@67
    .line 210
    .end local v4           #nextQuote:I
    .end local v5           #nextSpace:I
    .end local v8           #word:Ljava/lang/String;
    :cond_67
    add-int/lit8 v9, v9, 0x1

    #@69
    goto :goto_22

    #@6a
    .line 214
    :cond_6a
    invoke-virtual {p0, v13, v2}, Ljava/lang/String;->indexOf(II)I

    #@6d
    move-result v9

    #@6e
    goto :goto_32

    #@6f
    .line 223
    .restart local v8       #word:Ljava/lang/String;
    :cond_6f
    add-int/lit8 v2, v2, 0x1

    #@71
    goto :goto_44

    #@72
    .line 242
    .restart local v4       #nextQuote:I
    .restart local v5       #nextSpace:I
    :cond_72
    const/4 v7, 0x0

    #@73
    .line 243
    if-le v5, v12, :cond_1d

    #@75
    .line 244
    add-int/lit8 v2, v5, 0x1

    #@77
    goto :goto_1d

    #@78
    .line 252
    .end local v4           #nextQuote:I
    .end local v5           #nextSpace:I
    .end local v8           #word:Ljava/lang/String;
    :cond_78
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@7b
    move-result v10

    #@7c
    new-array v10, v10, [Ljava/lang/String;

    #@7e
    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@81
    move-result-object v10

    #@82
    check-cast v10, [Ljava/lang/String;

    #@84
    return-object v10
.end method


# virtual methods
.method public getCmdNumber()I
    .registers 2

    #@0
    .prologue
    .line 47
    iget v0, p0, Lcom/lge/systemservice/service/NativeDaemonEvent;->mCmdNumber:I

    #@2
    return v0
.end method

.method public getCode()I
    .registers 2

    #@0
    .prologue
    .line 51
    iget v0, p0, Lcom/lge/systemservice/service/NativeDaemonEvent;->mCode:I

    #@2
    return v0
.end method

.method public getRawEvent()Ljava/lang/String;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 60
    iget-object v0, p0, Lcom/lge/systemservice/service/NativeDaemonEvent;->mRawEvent:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public isClassClientError()Z
    .registers 3

    #@0
    .prologue
    .line 94
    iget v0, p0, Lcom/lge/systemservice/service/NativeDaemonEvent;->mCode:I

    #@2
    const/16 v1, 0x1f4

    #@4
    if-lt v0, v1, :cond_e

    #@6
    iget v0, p0, Lcom/lge/systemservice/service/NativeDaemonEvent;->mCode:I

    #@8
    const/16 v1, 0x258

    #@a
    if-ge v0, v1, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public isClassContinue()Z
    .registers 3

    #@0
    .prologue
    .line 73
    iget v0, p0, Lcom/lge/systemservice/service/NativeDaemonEvent;->mCode:I

    #@2
    const/16 v1, 0x64

    #@4
    if-lt v0, v1, :cond_e

    #@6
    iget v0, p0, Lcom/lge/systemservice/service/NativeDaemonEvent;->mCode:I

    #@8
    const/16 v1, 0xc8

    #@a
    if-ge v0, v1, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public isClassServerError()Z
    .registers 3

    #@0
    .prologue
    .line 87
    iget v0, p0, Lcom/lge/systemservice/service/NativeDaemonEvent;->mCode:I

    #@2
    const/16 v1, 0x190

    #@4
    if-lt v0, v1, :cond_e

    #@6
    iget v0, p0, Lcom/lge/systemservice/service/NativeDaemonEvent;->mCode:I

    #@8
    const/16 v1, 0x1f4

    #@a
    if-ge v0, v1, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public isClassUnsolicited()Z
    .registers 2

    #@0
    .prologue
    .line 101
    iget v0, p0, Lcom/lge/systemservice/service/NativeDaemonEvent;->mCode:I

    #@2
    invoke-static {v0}, Lcom/lge/systemservice/service/NativeDaemonEvent;->isClassUnsolicited(I)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 65
    iget-object v0, p0, Lcom/lge/systemservice/service/NativeDaemonEvent;->mRawEvent:Ljava/lang/String;

    #@2
    return-object v0
.end method
