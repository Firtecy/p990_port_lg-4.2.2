.class Lcom/lge/systemservice/service/SecureClockService$1;
.super Landroid/content/BroadcastReceiver;
.source "SecureClockService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/SecureClockService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/SecureClockService;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/SecureClockService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 55
    iput-object p1, p0, Lcom/lge/systemservice/service/SecureClockService$1;->this$0:Lcom/lge/systemservice/service/SecureClockService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 8
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 58
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    const-string v2, "android.intent.action.NETWORK_SET_TIME"

    #@6
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_5e

    #@c
    .line 59
    const-string v1, "ro.build.target_operator"

    #@e
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@11
    move-result-object v1

    #@12
    const-string v2, "DCM"

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v1

    #@18
    if-eqz v1, :cond_55

    #@1a
    .line 60
    const-string v1, "time"

    #@1c
    const-wide/16 v2, 0x0

    #@1e
    invoke-virtual {p2, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    #@21
    move-result-wide v1

    #@22
    const-wide/16 v3, 0x3e8

    #@24
    div-long/2addr v1, v3

    #@25
    long-to-int v0, v1

    #@26
    .line 61
    .local v0, gmt:I
    const-string v1, "SecureClockService"

    #@28
    const-string v2, "TelephonyIntents.ACTION_NETWORK_SET_TIME"

    #@2a
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 62
    const-string v1, "SecureClockService"

    #@2f
    new-instance v2, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v3, "MMInfoBroadcastReceived gmt : "

    #@36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v2

    #@3a
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v2

    #@42
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 65
    iget-object v1, p0, Lcom/lge/systemservice/service/SecureClockService$1;->this$0:Lcom/lge/systemservice/service/SecureClockService;

    #@47
    invoke-virtual {v1, v0}, Lcom/lge/systemservice/service/SecureClockService;->SetTime(I)I

    #@4a
    move-result v1

    #@4b
    const/4 v2, 0x1

    #@4c
    if-ne v1, v2, :cond_56

    #@4e
    .line 66
    const-string v1, "SecureClockService"

    #@50
    const-string v2, "SetTime Success "

    #@52
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@55
    .line 75
    .end local v0           #gmt:I
    :cond_55
    :goto_55
    return-void

    #@56
    .line 68
    .restart local v0       #gmt:I
    :cond_56
    const-string v1, "SecureClockService"

    #@58
    const-string v2, "SetTime Fail "

    #@5a
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    goto :goto_55

    #@5e
    .line 70
    .end local v0           #gmt:I
    :cond_5e
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@61
    move-result-object v1

    #@62
    const-string v2, "android.intent.action.TIME_SET"

    #@64
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@67
    move-result v1

    #@68
    if-eqz v1, :cond_55

    #@6a
    .line 71
    const-string v1, "SecureClockService"

    #@6c
    const-string v2, "Intent.ACTION_TIME_CHANGED "

    #@6e
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@71
    goto :goto_55
.end method
