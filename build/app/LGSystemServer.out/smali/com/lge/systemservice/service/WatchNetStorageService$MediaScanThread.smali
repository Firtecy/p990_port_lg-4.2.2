.class Lcom/lge/systemservice/service/WatchNetStorageService$MediaScanThread;
.super Ljava/lang/Thread;
.source "WatchNetStorageService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/WatchNetStorageService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MediaScanThread"
.end annotation


# instance fields
.field expire_time:J

.field private mContext:Landroid/content/Context;

.field mOriPath:Ljava/lang/String;

.field mPath:Ljava/lang/String;

.field final synthetic this$0:Lcom/lge/systemservice/service/WatchNetStorageService;


# direct methods
.method public constructor <init>(Lcom/lge/systemservice/service/WatchNetStorageService;Landroid/content/Context;Ljava/lang/String;J)V
    .registers 8
    .parameter
    .parameter "ctx"
    .parameter "path"
    .parameter "time"

    #@0
    .prologue
    .line 122
    iput-object p1, p0, Lcom/lge/systemservice/service/WatchNetStorageService$MediaScanThread;->this$0:Lcom/lge/systemservice/service/WatchNetStorageService;

    #@2
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@5
    .line 119
    const-wide/16 v0, 0x0

    #@7
    iput-wide v0, p0, Lcom/lge/systemservice/service/WatchNetStorageService$MediaScanThread;->expire_time:J

    #@9
    .line 123
    iput-object p2, p0, Lcom/lge/systemservice/service/WatchNetStorageService$MediaScanThread;->mContext:Landroid/content/Context;

    #@b
    .line 124
    iput-object p3, p0, Lcom/lge/systemservice/service/WatchNetStorageService$MediaScanThread;->mPath:Ljava/lang/String;

    #@d
    .line 125
    invoke-virtual {p0, p4, p5}, Lcom/lge/systemservice/service/WatchNetStorageService$MediaScanThread;->setExpireTime(J)V

    #@10
    .line 126
    return-void
.end method

.method private deleteMediaDB()V
    .registers 8

    #@0
    .prologue
    .line 136
    iget-object v4, p0, Lcom/lge/systemservice/service/WatchNetStorageService$MediaScanThread;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v4

    #@6
    const-string v5, "media"

    #@8
    invoke-virtual {v4, v5}, Landroid/content/ContentResolver;->acquireContentProviderClient(Ljava/lang/String;)Landroid/content/ContentProviderClient;

    #@b
    move-result-object v3

    #@c
    .line 137
    .local v3, mMediaProvider:Landroid/content/ContentProviderClient;
    iget-object v4, p0, Lcom/lge/systemservice/service/WatchNetStorageService$MediaScanThread;->mPath:Ljava/lang/String;

    #@e
    const-string v5, "\'"

    #@10
    const-string v6, "\'\'"

    #@12
    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    .line 138
    .local v1, encPath:Ljava/lang/String;
    const-string v4, "external"

    #@18
    invoke-static {v4}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    #@1b
    move-result-object v2

    #@1c
    .line 139
    .local v2, mFilesUri:Landroid/net/Uri;
    if-eqz v3, :cond_3e

    #@1e
    .line 142
    :try_start_1e
    new-instance v4, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v5, "_data = \'"

    #@25
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v4

    #@29
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    const-string v5, "\'"

    #@2f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v4

    #@37
    const/4 v5, 0x0

    #@38
    invoke-virtual {v3, v2, v4, v5}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_3b
    .catch Landroid/os/RemoteException; {:try_start_1e .. :try_end_3b} :catch_3f

    #@3b
    .line 147
    :goto_3b
    invoke-virtual {v3}, Landroid/content/ContentProviderClient;->release()Z

    #@3e
    .line 150
    :cond_3e
    return-void

    #@3f
    .line 143
    :catch_3f
    move-exception v0

    #@40
    .line 144
    .local v0, e:Landroid/os/RemoteException;
    const-string v4, "WatchNetStorageService"

    #@42
    const-string v5, "deleteMediaDB RemoteException Occur"

    #@44
    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    goto :goto_3b
.end method


# virtual methods
.method public run()V
    .registers 8

    #@0
    .prologue
    .line 154
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@3
    move-result-wide v2

    #@4
    iget-wide v4, p0, Lcom/lge/systemservice/service/WatchNetStorageService$MediaScanThread;->expire_time:J

    #@6
    cmp-long v2, v2, v4

    #@8
    if-gez v2, :cond_17

    #@a
    .line 157
    :try_start_a
    iget-wide v2, p0, Lcom/lge/systemservice/service/WatchNetStorageService$MediaScanThread;->expire_time:J

    #@c
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@f
    move-result-wide v4

    #@10
    sub-long/2addr v2, v4

    #@11
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_14
    .catch Ljava/lang/InterruptedException; {:try_start_a .. :try_end_14} :catch_15

    #@14
    goto :goto_0

    #@15
    .line 158
    :catch_15
    move-exception v2

    #@16
    goto :goto_0

    #@17
    .line 161
    :cond_17
    new-instance v0, Ljava/io/File;

    #@19
    iget-object v2, p0, Lcom/lge/systemservice/service/WatchNetStorageService$MediaScanThread;->mPath:Ljava/lang/String;

    #@1b
    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@1e
    .line 162
    .local v0, f:Ljava/io/File;
    iget-object v2, p0, Lcom/lge/systemservice/service/WatchNetStorageService$MediaScanThread;->mPath:Ljava/lang/String;

    #@20
    iput-object v2, p0, Lcom/lge/systemservice/service/WatchNetStorageService$MediaScanThread;->mOriPath:Ljava/lang/String;

    #@22
    .line 164
    iget-object v2, p0, Lcom/lge/systemservice/service/WatchNetStorageService$MediaScanThread;->mPath:Ljava/lang/String;

    #@24
    const-string v3, "/storage/sdcard0/"

    #@26
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@29
    move-result v2

    #@2a
    if-eqz v2, :cond_5c

    #@2c
    .line 165
    invoke-static {}, Lcom/lge/systemservice/service/WatchNetStorageService;->access$000()[Landroid/os/storage/StorageVolume;

    #@2f
    move-result-object v2

    #@30
    const/4 v3, 0x0

    #@31
    aget-object v2, v2, v3

    #@33
    invoke-virtual {v2}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    #@36
    move-result-object v1

    #@37
    .line 166
    .local v1, internalPath:Ljava/lang/String;
    iget-object v2, p0, Lcom/lge/systemservice/service/WatchNetStorageService$MediaScanThread;->mPath:Ljava/lang/String;

    #@39
    const/16 v3, 0x10

    #@3b
    iget-object v4, p0, Lcom/lge/systemservice/service/WatchNetStorageService$MediaScanThread;->mPath:Ljava/lang/String;

    #@3d
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@40
    move-result v4

    #@41
    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@44
    move-result-object v2

    #@45
    iput-object v2, p0, Lcom/lge/systemservice/service/WatchNetStorageService$MediaScanThread;->mPath:Ljava/lang/String;

    #@47
    .line 167
    new-instance v2, Ljava/lang/StringBuilder;

    #@49
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v2

    #@50
    iget-object v3, p0, Lcom/lge/systemservice/service/WatchNetStorageService$MediaScanThread;->mPath:Ljava/lang/String;

    #@52
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v2

    #@56
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v2

    #@5a
    iput-object v2, p0, Lcom/lge/systemservice/service/WatchNetStorageService$MediaScanThread;->mPath:Ljava/lang/String;

    #@5c
    .line 170
    .end local v1           #internalPath:Ljava/lang/String;
    :cond_5c
    const-string v2, "WatchNetStorageService"

    #@5e
    new-instance v3, Ljava/lang/StringBuilder;

    #@60
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@63
    const-string v4, "sendBroadcast ACTION_MEDIA_SCANNER_SCAN_FILE file:"

    #@65
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v3

    #@69
    iget-object v4, p0, Lcom/lge/systemservice/service/WatchNetStorageService$MediaScanThread;->mPath:Ljava/lang/String;

    #@6b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v3

    #@6f
    const-string v4, " ("

    #@71
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v3

    #@75
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@78
    move-result v4

    #@79
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v3

    #@7d
    const-string v4, ")"

    #@7f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v3

    #@83
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@86
    move-result-object v3

    #@87
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8a
    .line 172
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@8d
    move-result v2

    #@8e
    if-eqz v2, :cond_bf

    #@90
    .line 174
    iget-object v2, p0, Lcom/lge/systemservice/service/WatchNetStorageService$MediaScanThread;->mContext:Landroid/content/Context;

    #@92
    new-instance v3, Landroid/content/Intent;

    #@94
    const-string v4, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    #@96
    new-instance v5, Ljava/lang/StringBuilder;

    #@98
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@9b
    const-string v6, "file://"

    #@9d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v5

    #@a1
    iget-object v6, p0, Lcom/lge/systemservice/service/WatchNetStorageService$MediaScanThread;->mPath:Ljava/lang/String;

    #@a3
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v5

    #@a7
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@aa
    move-result-object v5

    #@ab
    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@ae
    move-result-object v5

    #@af
    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    #@b2
    invoke-virtual {v2, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@b5
    .line 181
    :goto_b5
    iget-object v2, p0, Lcom/lge/systemservice/service/WatchNetStorageService$MediaScanThread;->this$0:Lcom/lge/systemservice/service/WatchNetStorageService;

    #@b7
    iget-object v2, v2, Lcom/lge/systemservice/service/WatchNetStorageService;->mMediaScanRequestList:Ljava/util/Map;

    #@b9
    iget-object v3, p0, Lcom/lge/systemservice/service/WatchNetStorageService$MediaScanThread;->mOriPath:Ljava/lang/String;

    #@bb
    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@be
    .line 182
    return-void

    #@bf
    .line 178
    :cond_bf
    invoke-direct {p0}, Lcom/lge/systemservice/service/WatchNetStorageService$MediaScanThread;->deleteMediaDB()V

    #@c2
    goto :goto_b5
.end method

.method public setExpireTime(J)V
    .registers 5
    .parameter "time"

    #@0
    .prologue
    .line 130
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@3
    move-result-wide v0

    #@4
    add-long/2addr v0, p1

    #@5
    iput-wide v0, p0, Lcom/lge/systemservice/service/WatchNetStorageService$MediaScanThread;->expire_time:J

    #@7
    .line 131
    return-void
.end method
