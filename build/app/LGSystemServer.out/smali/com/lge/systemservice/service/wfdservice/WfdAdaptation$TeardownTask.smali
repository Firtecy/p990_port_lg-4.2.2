.class Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$TeardownTask;
.super Landroid/os/AsyncTask;
.source "WfdAdaptation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TeardownTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;


# direct methods
.method private constructor <init>(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 389
    iput-object p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$TeardownTask;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@2
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 389
    invoke-direct {p0, p1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$TeardownTask;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)V

    #@3
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 389
    check-cast p1, [Ljava/lang/Void;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$TeardownTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .registers 6
    .parameter "params"

    #@0
    .prologue
    .line 392
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$TeardownTask;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@2
    invoke-static {v0}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->access$200(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;

    #@5
    move-result-object v0

    #@6
    if-eqz v0, :cond_29

    #@8
    .line 393
    const-string v0, "WfdAdaptation"

    #@a
    const-string v1, "WFD Session Teardown- do in background started"

    #@c
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 394
    sget-object v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->eventHandler:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$EventHandler;

    #@11
    const/4 v1, 0x0

    #@12
    const-wide/16 v2, 0xbb8

    #@14
    invoke-virtual {v0, v1, v2, v3}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$EventHandler;->sendEmptyMessageDelayed(IJ)Z

    #@17
    .line 395
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$TeardownTask;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@19
    invoke-static {v0}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->access$200(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {v0}, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->teardown()I

    #@20
    .line 398
    const-string v0, "WfdAdaptation"

    #@22
    const-string v1, "WFD Session Teardown- do in background finished"

    #@24
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 402
    :goto_27
    const/4 v0, 0x0

    #@28
    return-object v0

    #@29
    .line 400
    :cond_29
    const-string v0, "WfdAdaptation"

    #@2b
    const-string v1, "TeardownTask: mWfdSessioManager is null"

    #@2d
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    goto :goto_27
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 389
    check-cast p1, Ljava/lang/Void;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$TeardownTask;->onPostExecute(Ljava/lang/Void;)V

    #@5
    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .registers 2
    .parameter "params"

    #@0
    .prologue
    .line 412
    return-void
.end method

.method protected onPreExecute()V
    .registers 1

    #@0
    .prologue
    .line 407
    return-void
.end method
