.class public Lcom/lge/systemservice/service/ClayboxLauncher;
.super Landroid/app/Service;
.source "ClayboxLauncher.java"


# instance fields
.field private final mBinder:Lcom/lge/systemservice/service/IClayboxLauncher$Stub;

.field private mClayboxProc:Ljava/lang/Process;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 37
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    #@3
    .line 52
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Lcom/lge/systemservice/service/ClayboxLauncher;->mClayboxProc:Ljava/lang/Process;

    #@6
    .line 54
    new-instance v0, Lcom/lge/systemservice/service/ClayboxLauncher$1;

    #@8
    invoke-direct {v0, p0}, Lcom/lge/systemservice/service/ClayboxLauncher$1;-><init>(Lcom/lge/systemservice/service/ClayboxLauncher;)V

    #@b
    iput-object v0, p0, Lcom/lge/systemservice/service/ClayboxLauncher;->mBinder:Lcom/lge/systemservice/service/IClayboxLauncher$Stub;

    #@d
    return-void
.end method

.method static synthetic access$000([B)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 37
    invoke-static {p0}, Lcom/lge/systemservice/service/ClayboxLauncher;->verifyArtSignature([B)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$100(Ljava/lang/String;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 37
    invoke-static {p0}, Lcom/lge/systemservice/service/ClayboxLauncher;->verifyJarFile(Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$200(Lcom/lge/systemservice/service/ClayboxLauncher;)Ljava/lang/Process;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 37
    iget-object v0, p0, Lcom/lge/systemservice/service/ClayboxLauncher;->mClayboxProc:Ljava/lang/Process;

    #@2
    return-object v0
.end method

.method static synthetic access$202(Lcom/lge/systemservice/service/ClayboxLauncher;Ljava/lang/Process;)Ljava/lang/Process;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 37
    iput-object p1, p0, Lcom/lge/systemservice/service/ClayboxLauncher;->mClayboxProc:Ljava/lang/Process;

    #@2
    return-object p1
.end method

.method private static verifyArtSignature([B)Z
    .registers 11
    .parameter "sig"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 150
    :try_start_1
    const-string v8, "X.509"

    #@3
    invoke-static {v8}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    #@6
    move-result-object v4

    #@7
    .line 151
    .local v4, cf:Ljava/security/cert/CertificateFactory;
    new-instance v1, Ljava/io/ByteArrayInputStream;

    #@9
    invoke-direct {v1, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    #@c
    .line 152
    .local v1, bais:Ljava/io/ByteArrayInputStream;
    invoke-virtual {v4, v1}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    #@f
    move-result-object v3

    #@10
    .line 154
    .local v3, cert:Ljava/security/cert/Certificate;
    const-string v8, "MIIDdDCCAlygAwIBAgIEUG46NjANBgkqhkiG9w0BAQUFADB7MQswCQYDVQQGEwJrbzEQMA4GA1UECBMHVW5rbm93bjEQMA4GA1UEBxMHVW5rbm93bjEsMCoGA1UEChMjU29mdHdhcmVDYXBhYmlsaXR5RGV2ZWxvcG1lbnRDZW50ZXIxDDAKBgNVBAsTA0NUTzEMMAoGA1UEAxMDTEdFMCAXDTEyMTAwNTAxMzkwMloYDzIyODYwNzIxMDEzOTAyWjB7MQswCQYDVQQGEwJrbzEQMA4GA1UECBMHVW5rbm93bjEQMA4GA1UEBxMHVW5rbm93bjEsMCoGA1UEChMjU29mdHdhcmVDYXBhYmlsaXR5RGV2ZWxvcG1lbnRDZW50ZXIxDDAKBgNVBAsTA0NUTzEMMAoGA1UEAxMDTEdFMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAx+Cy5lvjiUS6j5V6RA3brYHMnyYYLsSqs4gGXIWiFvBWl0qWt24mbOI4dmrDRcXKjl1Tq/UtnK+Na01RMLv1DKSRfDeuKjY0anvhtae0o8ctLZ9zWBApezqW3q/jlwBvm2UAuUK6rshWkCJW7A3TtK3AyHmc0/OGDXdjWVthhuR+5Z9rJFmneyZeQnNqPEcccGJKAFdq0KtxdE8h+IByQDUQIWiAIRXUwxr5J+vDuEAE8lpvU5dx9b+s6TEYJZL6gQWHhm/KZmAxkHbsSBCcg02jAtq7HGMjUsHptiqHMfQmzNKzb84JYLhQ/pp7ywCDdeKra/GYrduc6L6c8+sw3wIDAQABMA0GCSqGSIb3DQEBBQUAA4IBAQBqGHvAm40E1EJTSkNFzZSyfJ5Una9jj32aqD8lT/T+gJFFzJ2Tijkt9LkbGGFOQPgAebffqOQnRoQ6PY35JlA2vU51iEtJPBNbpODTiNTL1EoZExwv3AOyDQcLsrxt+YxCrHuo0kDsnmOYg76TvVFsmzo4tV1HXzixTPUSYNOYx9KegSDS0JIF2rzCUjdnJy7ZR0RDZt4iTdxhm2al98TxIeEkmOlcDXMuKXBJAxIP75dnSvHXSdGQgSR7SefZajL6EZhEc+ArFb0pdHHG/dWX8ZCdApS2JjOHs5fKXB3EFxKU7jHtHj/3Zk1vRUwZK1R65EKuV2NN1aNIypG0zrJb"

    #@12
    const/4 v9, 0x0

    #@13
    invoke-static {v8, v9}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    #@16
    move-result-object v6

    #@17
    .line 156
    .local v6, encoded:[B
    new-instance v2, Ljava/io/ByteArrayInputStream;

    #@19
    invoke-direct {v2, v6}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    #@1c
    .line 157
    .local v2, bais2:Ljava/io/ByteArrayInputStream;
    invoke-virtual {v4, v2}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    #@1f
    move-result-object v0

    #@20
    .line 158
    .local v0, artitecert:Ljava/security/cert/Certificate;
    invoke-virtual {v0}, Ljava/security/cert/Certificate;->getPublicKey()Ljava/security/PublicKey;

    #@23
    move-result-object v8

    #@24
    invoke-virtual {v3, v8}, Ljava/security/cert/Certificate;->verify(Ljava/security/PublicKey;)V
    :try_end_27
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_27} :catch_29

    #@27
    .line 160
    const/4 v7, 0x1

    #@28
    .line 165
    .end local v0           #artitecert:Ljava/security/cert/Certificate;
    .end local v1           #bais:Ljava/io/ByteArrayInputStream;
    .end local v2           #bais2:Ljava/io/ByteArrayInputStream;
    .end local v3           #cert:Ljava/security/cert/Certificate;
    .end local v4           #cf:Ljava/security/cert/CertificateFactory;
    .end local v6           #encoded:[B
    :goto_28
    return v7

    #@29
    .line 162
    :catch_29
    move-exception v5

    #@2a
    .line 163
    .local v5, e:Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    #@2d
    goto :goto_28
.end method

.method private static verifyJarFile(Ljava/lang/String;)Z
    .registers 16
    .parameter "jarfile"

    #@0
    .prologue
    const/4 v12, 0x0

    #@1
    .line 170
    const/4 v10, 0x0

    #@2
    .line 173
    .local v10, jarFile:Ljava/util/jar/JarFile;
    :try_start_2
    new-instance v11, Ljava/util/jar/JarFile;

    #@4
    invoke-direct {v11, p0}, Ljava/util/jar/JarFile;-><init>(Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_2 .. :try_end_7} :catchall_95
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_7} :catch_86

    #@7
    .line 175
    .end local v10           #jarFile:Ljava/util/jar/JarFile;
    .local v11, jarFile:Ljava/util/jar/JarFile;
    :try_start_7
    const-string v13, "classes.dex"

    #@9
    invoke-virtual {v11, v13}, Ljava/util/jar/JarFile;->getJarEntry(Ljava/lang/String;)Ljava/util/jar/JarEntry;

    #@c
    move-result-object v9

    #@d
    .line 177
    .local v9, jarEntry:Ljava/util/jar/JarEntry;
    invoke-virtual {v11, v9}, Ljava/util/jar/JarFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    #@10
    move-result-object v8

    #@11
    .line 178
    .local v8, in:Ljava/io/InputStream;
    const/16 v13, 0x400

    #@13
    new-array v2, v13, [B

    #@15
    .line 179
    .local v2, buffer:[B
    :cond_15
    invoke-virtual {v8, v2}, Ljava/io/InputStream;->read([B)I
    :try_end_18
    .catchall {:try_start_7 .. :try_end_18} :catchall_a1
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_18} :catch_a4

    #@18
    move-result v13

    #@19
    if-gez v13, :cond_15

    #@1b
    .line 182
    :try_start_1b
    const-string v13, "MIIDJDCCAgygAwIBAgIET9A5qzANBgkqhkiG9w0BAQUFADBTMQswCQYDVQQGEwJrbzEMMAoGA1UECBMDbGdlMQwwCgYDVQQHEwNsZ2UxDDAKBgNVBAoTA2xnZTEMMAoGA1UECxMDbGdlMQwwCgYDVQQDEwNsZ2UwIBcNMTIwNjA3MDUxODM1WhgPMjI4NjAzMjMwNTE4MzVaMFMxCzAJBgNVBAYTAmtvMQwwCgYDVQQIEwNsZ2UxDDAKBgNVBAcTA2xnZTEMMAoGA1UEChMDbGdlMQwwCgYDVQQLEwNsZ2UxDDAKBgNVBAMTA2xnZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOZe8xG85261iyweZIbTu/0oFFlTLGqCF5CRXS+jcs8eHgpEO6H07XX9gVlQ2da5SEjjJeDoEUQhDlygR/z1kSmEDwYwX/s3bDhghKpXHPkOsXIWKj7/hO76e4kPlKtck2KzhdSbjkQEktCXkRRTpNB+mlUc1BQsRFGVKCEyIEiypP0jwtR9+yLjUfsBhgI9V3EWsRP5Af0WJ07wPONgr7rZqBBWzIc3lQP1jX8jk8ycJG22+j5wlAJ5je+gdvxX4JyI9QPlDYQEsExeV8kHopjkL6bGDjmoBLP5h5Z6Q+ht2oBkRrstSm7oXGaBsBZd9Hid3Dx3FGwEixcp9BaQqZ8CAwEAATANBgkqhkiG9w0BAQUFAAOCAQEAX3grEnXhx/QapiBK0FMTRwQXnODncpM7Mqq++DiyTfRC0Yh8ARr2TySRdT8wLKH/bsgwIV/GGKArYUvvvI3bmLsKYskYO38U1PHedBvpKLVHLkTVJ0qVJHpRVPGYOaXzb82MOzxzm9Hn5rg4HQf6k4Wwl5jU8ofwczicdcf/CSQb0SnzJKZJhO1okYqanFTm5WBbb+9WoWlHyQDt7GEm3akDwVd/gdQ8LItmbPGv1gI3Yax+ww0CJVoCvkdtWwwcjCpq0Wb3q+HAEGzbQNOJHLnKBzF/bNdVne3aTkEHpTKjWxnDDms7d/nrqK+Vz2IKLt1Izy3OMvH7OlbC1UZiRA=="

    #@1d
    const/4 v14, 0x0

    #@1e
    invoke-static {v13, v14}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    #@21
    move-result-object v6

    #@22
    .line 184
    .local v6, encoded:[B
    const-string v13, "X.509"

    #@24
    invoke-static {v13}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    #@27
    move-result-object v4

    #@28
    .line 185
    .local v4, cf:Ljava/security/cert/CertificateFactory;
    new-instance v1, Ljava/io/ByteArrayInputStream;

    #@2a
    invoke-direct {v1, v6}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    #@2d
    .line 186
    .local v1, bais:Ljava/io/ByteArrayInputStream;
    invoke-virtual {v4, v1}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    #@30
    move-result-object v0

    #@31
    .line 188
    .local v0, artitecert:Ljava/security/cert/Certificate;
    invoke-virtual {v9}, Ljava/util/jar/JarEntry;->getCertificates()[Ljava/security/cert/Certificate;

    #@34
    move-result-object v3

    #@35
    .line 189
    .local v3, certs:[Ljava/security/cert/Certificate;
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V

    #@38
    .line 190
    if-eqz v3, :cond_79

    #@3a
    .line 192
    const/4 v7, 0x0

    #@3b
    .local v7, i:I
    :goto_3b
    array-length v13, v3

    #@3c
    if-ge v7, v13, :cond_79

    #@3e
    .line 194
    aget-object v13, v3, v7

    #@40
    invoke-virtual {v0}, Ljava/security/cert/Certificate;->getPublicKey()Ljava/security/PublicKey;

    #@43
    move-result-object v14

    #@44
    invoke-virtual {v13, v14}, Ljava/security/cert/Certificate;->verify(Ljava/security/PublicKey;)V

    #@47
    .line 196
    aget-object v13, v3, v7

    #@49
    invoke-virtual {v13}, Ljava/security/cert/Certificate;->getEncoded()[B

    #@4c
    move-result-object v13

    #@4d
    invoke-virtual {v0}, Ljava/security/cert/Certificate;->getEncoded()[B

    #@50
    move-result-object v14

    #@51
    invoke-static {v13, v14}, Ljava/util/Arrays;->equals([B[B)Z

    #@54
    move-result v13

    #@55
    if-eqz v13, :cond_6b

    #@57
    .line 198
    const-string v13, "ArtSystemServer"

    #@59
    const-string v14, "certificate compare ok!"

    #@5b
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5e
    .catchall {:try_start_1b .. :try_end_5e} :catchall_a1
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_5e} :catch_75

    #@5e
    .line 199
    const/4 v12, 0x1

    #@5f
    .line 215
    if-eqz v11, :cond_64

    #@61
    .line 216
    :try_start_61
    invoke-virtual {v11}, Ljava/util/jar/JarFile;->close()V
    :try_end_64
    .catch Ljava/io/IOException; {:try_start_61 .. :try_end_64} :catch_66

    #@64
    :cond_64
    :goto_64
    move-object v10, v11

    #@65
    .line 222
    .end local v0           #artitecert:Ljava/security/cert/Certificate;
    .end local v1           #bais:Ljava/io/ByteArrayInputStream;
    .end local v2           #buffer:[B
    .end local v3           #certs:[Ljava/security/cert/Certificate;
    .end local v4           #cf:Ljava/security/cert/CertificateFactory;
    .end local v6           #encoded:[B
    .end local v7           #i:I
    .end local v8           #in:Ljava/io/InputStream;
    .end local v9           #jarEntry:Ljava/util/jar/JarEntry;
    .end local v11           #jarFile:Ljava/util/jar/JarFile;
    .restart local v10       #jarFile:Ljava/util/jar/JarFile;
    :cond_65
    :goto_65
    return v12

    #@66
    .line 218
    .end local v10           #jarFile:Ljava/util/jar/JarFile;
    .restart local v0       #artitecert:Ljava/security/cert/Certificate;
    .restart local v1       #bais:Ljava/io/ByteArrayInputStream;
    .restart local v2       #buffer:[B
    .restart local v3       #certs:[Ljava/security/cert/Certificate;
    .restart local v4       #cf:Ljava/security/cert/CertificateFactory;
    .restart local v6       #encoded:[B
    .restart local v7       #i:I
    .restart local v8       #in:Ljava/io/InputStream;
    .restart local v9       #jarEntry:Ljava/util/jar/JarEntry;
    .restart local v11       #jarFile:Ljava/util/jar/JarFile;
    :catch_66
    move-exception v5

    #@67
    .line 219
    .local v5, e:Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    #@6a
    goto :goto_64

    #@6b
    .line 203
    .end local v5           #e:Ljava/io/IOException;
    :cond_6b
    :try_start_6b
    const-string v13, "ArtSystemServer"

    #@6d
    const-string v14, "certificate compare fail!"

    #@6f
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_72
    .catchall {:try_start_6b .. :try_end_72} :catchall_a1
    .catch Ljava/lang/Exception; {:try_start_6b .. :try_end_72} :catch_75

    #@72
    .line 192
    add-int/lit8 v7, v7, 0x1

    #@74
    goto :goto_3b

    #@75
    .line 207
    .end local v0           #artitecert:Ljava/security/cert/Certificate;
    .end local v1           #bais:Ljava/io/ByteArrayInputStream;
    .end local v3           #certs:[Ljava/security/cert/Certificate;
    .end local v4           #cf:Ljava/security/cert/CertificateFactory;
    .end local v6           #encoded:[B
    .end local v7           #i:I
    :catch_75
    move-exception v5

    #@76
    .line 208
    .local v5, e:Ljava/lang/Exception;
    :try_start_76
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_79
    .catchall {:try_start_76 .. :try_end_79} :catchall_a1
    .catch Ljava/lang/Exception; {:try_start_76 .. :try_end_79} :catch_a4

    #@79
    .line 215
    .end local v5           #e:Ljava/lang/Exception;
    :cond_79
    if-eqz v11, :cond_7e

    #@7b
    .line 216
    :try_start_7b
    invoke-virtual {v11}, Ljava/util/jar/JarFile;->close()V
    :try_end_7e
    .catch Ljava/io/IOException; {:try_start_7b .. :try_end_7e} :catch_80

    #@7e
    :cond_7e
    move-object v10, v11

    #@7f
    .line 220
    .end local v11           #jarFile:Ljava/util/jar/JarFile;
    .restart local v10       #jarFile:Ljava/util/jar/JarFile;
    goto :goto_65

    #@80
    .line 218
    .end local v10           #jarFile:Ljava/util/jar/JarFile;
    .restart local v11       #jarFile:Ljava/util/jar/JarFile;
    :catch_80
    move-exception v5

    #@81
    .line 219
    .local v5, e:Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    #@84
    move-object v10, v11

    #@85
    .line 221
    .end local v11           #jarFile:Ljava/util/jar/JarFile;
    .restart local v10       #jarFile:Ljava/util/jar/JarFile;
    goto :goto_65

    #@86
    .line 210
    .end local v2           #buffer:[B
    .end local v5           #e:Ljava/io/IOException;
    .end local v8           #in:Ljava/io/InputStream;
    .end local v9           #jarEntry:Ljava/util/jar/JarEntry;
    :catch_86
    move-exception v5

    #@87
    .line 211
    .local v5, e:Ljava/lang/Exception;
    :goto_87
    :try_start_87
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_8a
    .catchall {:try_start_87 .. :try_end_8a} :catchall_95

    #@8a
    .line 215
    if-eqz v10, :cond_65

    #@8c
    .line 216
    :try_start_8c
    invoke-virtual {v10}, Ljava/util/jar/JarFile;->close()V
    :try_end_8f
    .catch Ljava/io/IOException; {:try_start_8c .. :try_end_8f} :catch_90

    #@8f
    goto :goto_65

    #@90
    .line 218
    :catch_90
    move-exception v5

    #@91
    .line 219
    .local v5, e:Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    #@94
    goto :goto_65

    #@95
    .line 213
    .end local v5           #e:Ljava/io/IOException;
    :catchall_95
    move-exception v12

    #@96
    .line 215
    :goto_96
    if-eqz v10, :cond_9b

    #@98
    .line 216
    :try_start_98
    invoke-virtual {v10}, Ljava/util/jar/JarFile;->close()V
    :try_end_9b
    .catch Ljava/io/IOException; {:try_start_98 .. :try_end_9b} :catch_9c

    #@9b
    .line 220
    :cond_9b
    :goto_9b
    throw v12

    #@9c
    .line 218
    :catch_9c
    move-exception v5

    #@9d
    .line 219
    .restart local v5       #e:Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    #@a0
    goto :goto_9b

    #@a1
    .line 213
    .end local v5           #e:Ljava/io/IOException;
    .end local v10           #jarFile:Ljava/util/jar/JarFile;
    .restart local v11       #jarFile:Ljava/util/jar/JarFile;
    :catchall_a1
    move-exception v12

    #@a2
    move-object v10, v11

    #@a3
    .end local v11           #jarFile:Ljava/util/jar/JarFile;
    .restart local v10       #jarFile:Ljava/util/jar/JarFile;
    goto :goto_96

    #@a4
    .line 210
    .end local v10           #jarFile:Ljava/util/jar/JarFile;
    .restart local v11       #jarFile:Ljava/util/jar/JarFile;
    :catch_a4
    move-exception v5

    #@a5
    move-object v10, v11

    #@a6
    .end local v11           #jarFile:Ljava/util/jar/JarFile;
    .restart local v10       #jarFile:Ljava/util/jar/JarFile;
    goto :goto_87
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 4
    .parameter "intent"

    #@0
    .prologue
    .line 48
    const-string v0, "ArtSystemServer"

    #@2
    const-string v1, "OnBind!!!"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 49
    iget-object v0, p0, Lcom/lge/systemservice/service/ClayboxLauncher;->mBinder:Lcom/lge/systemservice/service/IClayboxLauncher$Stub;

    #@9
    return-object v0
.end method

.method public onCreate()V
    .registers 1

    #@0
    .prologue
    .line 43
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    #@3
    .line 44
    return-void
.end method
