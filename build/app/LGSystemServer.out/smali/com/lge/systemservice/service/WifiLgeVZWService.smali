.class public Lcom/lge/systemservice/service/WifiLgeVZWService;
.super Lcom/lge/systemservice/core/wifilgevzwmanager/IWifiLgeVZWManager$Stub;
.source "WifiLgeVZWService.java"


# instance fields
.field private final NOTIFICATION_NETWORK_AVAILABLE:I

.field private final NOTIFICATION_NONETWORK_DETECTED:I

.field private final NOTIFICATION_WIFI_CONNECTED:I

.field private final NOTIFICATION_WIFI_OFF:I

.field private SECURITY_EAP:I

.field private SECURITY_NONE:I

.field private SECURITY_PSK:I

.field private SECURITY_WEP:I

.field private contentIntent:Landroid/app/PendingIntent;

.field private mConnectivityManager:Landroid/net/ConnectivityManager;

.field private final mContext:Landroid/content/Context;

.field private mFilter:Landroid/content/IntentFilter;

.field private mNetworkInfo:Landroid/net/NetworkInfo;

.field private mNotiState:I

.field private mNotificationManager:Landroid/app/NotificationManager;

.field private mNotification_network_available:Landroid/app/Notification;

.field private mNotification_nonetwork_detected:Landroid/app/Notification;

.field private mNotification_wifi_conntected:Landroid/app/Notification;

.field private mNotification_wifioff:Landroid/app/Notification;

.field mReceiver:Landroid/content/BroadcastReceiver;

.field private mWifiManager:Landroid/net/wifi/WifiManager;

.field private mWifiNoti_NetworkAvail:Z

.field private mWifiNoti_NonetwrkDetected:Z

.field private mWifiNoti_WifiConntected:Z

.field private mWifiNoti_WifiOff:Z

.field private mwifiState:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 7
    .parameter "context"

    #@0
    .prologue
    const/4 v4, 0x3

    #@1
    const/4 v3, 0x2

    #@2
    const/4 v2, 0x1

    #@3
    const/4 v1, 0x0

    #@4
    .line 72
    invoke-direct {p0}, Lcom/lge/systemservice/core/wifilgevzwmanager/IWifiLgeVZWManager$Stub;-><init>()V

    #@7
    .line 51
    const/4 v0, 0x0

    #@8
    iput-object v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotificationManager:Landroid/app/NotificationManager;

    #@a
    .line 55
    iput v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->NOTIFICATION_WIFI_OFF:I

    #@c
    .line 56
    iput v3, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->NOTIFICATION_NONETWORK_DETECTED:I

    #@e
    .line 57
    iput v4, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->NOTIFICATION_NETWORK_AVAILABLE:I

    #@10
    .line 58
    const/4 v0, 0x4

    #@11
    iput v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->NOTIFICATION_WIFI_CONNECTED:I

    #@13
    .line 59
    iput-boolean v1, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mWifiNoti_WifiOff:Z

    #@15
    .line 60
    iput-boolean v1, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mWifiNoti_NonetwrkDetected:Z

    #@17
    .line 61
    iput-boolean v1, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mWifiNoti_NetworkAvail:Z

    #@19
    .line 62
    iput-boolean v1, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mWifiNoti_WifiConntected:Z

    #@1b
    .line 68
    iput v1, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->SECURITY_NONE:I

    #@1d
    .line 69
    iput v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->SECURITY_WEP:I

    #@1f
    .line 70
    iput v3, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->SECURITY_PSK:I

    #@21
    .line 71
    iput v4, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->SECURITY_EAP:I

    #@23
    .line 90
    new-instance v0, Lcom/lge/systemservice/service/WifiLgeVZWService$1;

    #@25
    invoke-direct {v0, p0}, Lcom/lge/systemservice/service/WifiLgeVZWService$1;-><init>(Lcom/lge/systemservice/service/WifiLgeVZWService;)V

    #@28
    iput-object v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mReceiver:Landroid/content/BroadcastReceiver;

    #@2a
    .line 73
    iput-object p1, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mContext:Landroid/content/Context;

    #@2c
    .line 75
    new-instance v0, Landroid/content/IntentFilter;

    #@2e
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@31
    iput-object v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mFilter:Landroid/content/IntentFilter;

    #@33
    .line 76
    iget-object v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mFilter:Landroid/content/IntentFilter;

    #@35
    const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"

    #@37
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@3a
    .line 77
    iget-object v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mFilter:Landroid/content/IntentFilter;

    #@3c
    const-string v1, "android.net.wifi.SCAN_RESULTS"

    #@3e
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@41
    .line 78
    iget-object v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mFilter:Landroid/content/IntentFilter;

    #@43
    const-string v1, "android.net.wifi.NETWORK_IDS_CHANGED"

    #@45
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@48
    .line 79
    iget-object v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mFilter:Landroid/content/IntentFilter;

    #@4a
    const-string v1, "android.net.wifi.supplicant.STATE_CHANGE"

    #@4c
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@4f
    .line 80
    iget-object v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mFilter:Landroid/content/IntentFilter;

    #@51
    const-string v1, "android.net.wifi.STATE_CHANGE"

    #@53
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@56
    .line 81
    iget-object v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mFilter:Landroid/content/IntentFilter;

    #@58
    const-string v1, "android.net.wifi.RSSI_CHANGED"

    #@5a
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@5d
    .line 82
    iget-object v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mFilter:Landroid/content/IntentFilter;

    #@5f
    const-string v1, "android.intent.action.SCREEN_OFF"

    #@61
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@64
    .line 83
    iget-object v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mFilter:Landroid/content/IntentFilter;

    #@66
    const-string v1, "android.intent.action.LOCALE_CHANGED"

    #@68
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@6b
    .line 84
    invoke-virtual {p0}, Lcom/lge/systemservice/service/WifiLgeVZWService;->init()V

    #@6e
    .line 85
    iget-object v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mContext:Landroid/content/Context;

    #@70
    iget-object v1, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mReceiver:Landroid/content/BroadcastReceiver;

    #@72
    iget-object v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mFilter:Landroid/content/IntentFilter;

    #@74
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@77
    .line 86
    return-void
.end method

.method static synthetic access$000(Lcom/lge/systemservice/service/WifiLgeVZWService;Landroid/content/Intent;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/lge/systemservice/service/WifiLgeVZWService;->handleEvent(Landroid/content/Intent;)V

    #@3
    return-void
.end method

.method private getSecurity(Landroid/net/wifi/WifiConfiguration;)I
    .registers 4
    .parameter "config"

    #@0
    .prologue
    .line 514
    iget-object v0, p1, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_c

    #@9
    .line 515
    iget v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->SECURITY_PSK:I

    #@b
    .line 521
    :goto_b
    return v0

    #@c
    .line 517
    :cond_c
    iget-object v0, p1, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@e
    const/4 v1, 0x2

    #@f
    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    #@12
    move-result v0

    #@13
    if-nez v0, :cond_1e

    #@15
    iget-object v0, p1, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@17
    const/4 v1, 0x3

    #@18
    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    #@1b
    move-result v0

    #@1c
    if-eqz v0, :cond_21

    #@1e
    .line 519
    :cond_1e
    iget v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->SECURITY_EAP:I

    #@20
    goto :goto_b

    #@21
    .line 521
    :cond_21
    iget-object v0, p1, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    #@23
    const/4 v1, 0x0

    #@24
    aget-object v0, v0, v1

    #@26
    if-eqz v0, :cond_2b

    #@28
    iget v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->SECURITY_WEP:I

    #@2a
    goto :goto_b

    #@2b
    :cond_2b
    iget v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->SECURITY_NONE:I

    #@2d
    goto :goto_b
.end method

.method private handleEvent(Landroid/content/Intent;)V
    .registers 16
    .parameter "intent"

    #@0
    .prologue
    .line 135
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 138
    .local v0, action:Ljava/lang/String;
    const-string v11, "android.net.wifi.WIFI_STATE_CHANGED"

    #@6
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v11

    #@a
    if-eqz v11, :cond_59

    #@c
    .line 140
    const-string v11, "wifi_state"

    #@e
    const/4 v12, 0x1

    #@f
    invoke-virtual {p1, v11, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@12
    move-result v11

    #@13
    iput v11, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mwifiState:I

    #@15
    .line 143
    const-string v11, "WifiLgeVZWService"

    #@17
    new-instance v12, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v13, " WIFI_STATE_CHANGED_ACTION "

    #@1e
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v12

    #@22
    iget v13, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mwifiState:I

    #@24
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@27
    move-result-object v12

    #@28
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v12

    #@2c
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 146
    iget v11, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mwifiState:I

    #@31
    const/4 v12, 0x1

    #@32
    if-ne v11, v12, :cond_44

    #@34
    .line 148
    const/4 v11, 0x2

    #@35
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@38
    .line 149
    const/4 v11, 0x3

    #@39
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@3c
    .line 150
    const/4 v11, 0x4

    #@3d
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@40
    .line 152
    const/4 v11, 0x1

    #@41
    iput v11, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotiState:I

    #@43
    .line 335
    :cond_43
    :goto_43
    return-void

    #@44
    .line 154
    :cond_44
    iget v11, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mwifiState:I

    #@46
    const/4 v12, 0x3

    #@47
    if-ne v11, v12, :cond_43

    #@49
    .line 155
    const/4 v11, 0x1

    #@4a
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@4d
    .line 156
    const/4 v11, 0x3

    #@4e
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@51
    .line 157
    const/4 v11, 0x4

    #@52
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@55
    .line 159
    const/4 v11, 0x2

    #@56
    iput v11, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotiState:I

    #@58
    goto :goto_43

    #@59
    .line 162
    :cond_59
    const-string v11, "android.net.wifi.SCAN_RESULTS"

    #@5b
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5e
    move-result v11

    #@5f
    if-eqz v11, :cond_b3

    #@61
    .line 163
    const-string v11, "WifiLgeVZWService"

    #@63
    const-string v12, " SCAN_RESULTS_AVAILABLE_ACTION"

    #@65
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@68
    .line 164
    iget v11, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotiState:I

    #@6a
    const/4 v12, 0x2

    #@6b
    if-ne v11, v12, :cond_90

    #@6d
    .line 165
    iget-object v11, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@6f
    invoke-virtual {v11}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    #@72
    move-result-object v5

    #@73
    .line 166
    .local v5, scanResults1:Ljava/util/List;,"Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    if-eqz v5, :cond_43

    #@75
    invoke-interface {v5}, Ljava/util/List;->size()I

    #@78
    move-result v11

    #@79
    if-eqz v11, :cond_43

    #@7b
    .line 168
    const/4 v11, 0x1

    #@7c
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@7f
    .line 169
    const/4 v11, 0x2

    #@80
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@83
    .line 170
    const/4 v11, 0x4

    #@84
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@87
    .line 172
    const/4 v11, 0x3

    #@88
    const/4 v12, 0x0

    #@89
    invoke-direct {p0, v11, v12}, Lcom/lge/systemservice/service/WifiLgeVZWService;->showNotification(ILjava/lang/String;)V

    #@8c
    .line 173
    const/4 v11, 0x3

    #@8d
    iput v11, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotiState:I

    #@8f
    goto :goto_43

    #@90
    .line 175
    .end local v5           #scanResults1:Ljava/util/List;,"Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    :cond_90
    iget v11, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotiState:I

    #@92
    const/4 v12, 0x3

    #@93
    if-ne v11, v12, :cond_43

    #@95
    .line 176
    iget-object v11, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@97
    invoke-virtual {v11}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    #@9a
    move-result-object v5

    #@9b
    .line 177
    .restart local v5       #scanResults1:Ljava/util/List;,"Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    if-eqz v5, :cond_43

    #@9d
    invoke-interface {v5}, Ljava/util/List;->size()I

    #@a0
    move-result v11

    #@a1
    if-nez v11, :cond_43

    #@a3
    .line 179
    const/4 v11, 0x1

    #@a4
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@a7
    .line 180
    const/4 v11, 0x3

    #@a8
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@ab
    .line 181
    const/4 v11, 0x4

    #@ac
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@af
    .line 183
    const/4 v11, 0x2

    #@b0
    iput v11, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotiState:I

    #@b2
    goto :goto_43

    #@b3
    .line 188
    .end local v5           #scanResults1:Ljava/util/List;,"Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    :cond_b3
    const-string v11, "android.net.wifi.STATE_CHANGE"

    #@b5
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b8
    move-result v11

    #@b9
    if-eqz v11, :cond_1d7

    #@bb
    .line 189
    const-string v11, "WifiLgeVZWService"

    #@bd
    new-instance v12, Ljava/lang/StringBuilder;

    #@bf
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@c2
    const-string v13, " NETWORK_STATE_CHANGED_ACTION : "

    #@c4
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v12

    #@c8
    iget-object v13, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@ca
    invoke-virtual {v13}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    #@cd
    move-result-object v13

    #@ce
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v12

    #@d2
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d5
    move-result-object v12

    #@d6
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d9
    .line 190
    const-string v11, "networkInfo"

    #@db
    invoke-virtual {p1, v11}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@de
    move-result-object v11

    #@df
    check-cast v11, Landroid/net/NetworkInfo;

    #@e1
    iput-object v11, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@e3
    .line 192
    iget-object v11, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@e5
    if-eqz v11, :cond_43

    #@e7
    .line 193
    iget-object v11, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@e9
    invoke-virtual {v11}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    #@ec
    move-result-object v11

    #@ed
    sget-object v12, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    #@ef
    if-ne v11, v12, :cond_17d

    #@f1
    .line 194
    const-string v11, "wifiInfo"

    #@f3
    invoke-virtual {p1, v11}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@f6
    move-result-object v9

    #@f7
    check-cast v9, Landroid/net/wifi/WifiInfo;

    #@f9
    .line 196
    .local v9, wifiInfo:Landroid/net/wifi/WifiInfo;
    if-eqz v9, :cond_43

    #@fb
    .line 197
    invoke-virtual {v9}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    #@fe
    move-result-object v11

    #@ff
    invoke-static {v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->removeDoubleQuotes(Ljava/lang/String;)Ljava/lang/String;

    #@102
    move-result-object v3

    #@103
    .line 198
    .local v3, connectedSsid:Ljava/lang/String;
    if-eqz v3, :cond_43

    #@105
    .line 200
    const/4 v11, 0x1

    #@106
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@109
    .line 201
    const/4 v11, 0x2

    #@10a
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@10d
    .line 202
    const/4 v11, 0x3

    #@10e
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@111
    .line 204
    const-string v7, "open"

    #@113
    .line 205
    .local v7, secure_type:Ljava/lang/String;
    invoke-virtual {v9}, Landroid/net/wifi/WifiInfo;->getNetworkId()I

    #@116
    move-result v11

    #@117
    if-gez v11, :cond_11b

    #@119
    .line 206
    const-string v7, "open"

    #@11b
    .line 209
    :cond_11b
    iget-object v11, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@11d
    invoke-virtual {v11}, Landroid/net/wifi/WifiManager;->getConfiguredNetworks()Ljava/util/List;

    #@120
    move-result-object v1

    #@121
    .line 211
    .local v1, cfgs:Ljava/util/List;,"Ljava/util/List<Landroid/net/wifi/WifiConfiguration;>;"
    if-eqz v1, :cond_17a

    #@123
    .line 212
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@126
    move-result-object v4

    #@127
    .local v4, i$:Ljava/util/Iterator;
    :cond_127
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@12a
    move-result v11

    #@12b
    if-eqz v11, :cond_145

    #@12d
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@130
    move-result-object v2

    #@131
    check-cast v2, Landroid/net/wifi/WifiConfiguration;

    #@133
    .line 213
    .local v2, config:Landroid/net/wifi/WifiConfiguration;
    iget v11, v2, Landroid/net/wifi/WifiConfiguration;->networkId:I

    #@135
    invoke-virtual {v9}, Landroid/net/wifi/WifiInfo;->getNetworkId()I

    #@138
    move-result v12

    #@139
    if-ne v11, v12, :cond_127

    #@13b
    .line 214
    invoke-direct {p0, v2}, Lcom/lge/systemservice/service/WifiLgeVZWService;->getSecurity(Landroid/net/wifi/WifiConfiguration;)I

    #@13e
    move-result v11

    #@13f
    iget v12, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->SECURITY_NONE:I

    #@141
    if-ne v11, v12, :cond_177

    #@143
    .line 215
    const-string v7, "open"

    #@145
    .line 225
    .end local v2           #config:Landroid/net/wifi/WifiConfiguration;
    .end local v4           #i$:Ljava/util/Iterator;
    :cond_145
    :goto_145
    const/4 v11, 0x4

    #@146
    new-instance v12, Ljava/lang/StringBuilder;

    #@148
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@14b
    const-string v13, "\""

    #@14d
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@150
    move-result-object v12

    #@151
    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@154
    move-result-object v12

    #@155
    const-string v13, "\""

    #@157
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15a
    move-result-object v12

    #@15b
    const-string v13, " ("

    #@15d
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@160
    move-result-object v12

    #@161
    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@164
    move-result-object v12

    #@165
    const-string v13, ")"

    #@167
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16a
    move-result-object v12

    #@16b
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16e
    move-result-object v12

    #@16f
    invoke-direct {p0, v11, v12}, Lcom/lge/systemservice/service/WifiLgeVZWService;->showNotification(ILjava/lang/String;)V

    #@172
    .line 226
    const/4 v11, 0x4

    #@173
    iput v11, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotiState:I

    #@175
    goto/16 :goto_43

    #@177
    .line 217
    .restart local v2       #config:Landroid/net/wifi/WifiConfiguration;
    .restart local v4       #i$:Ljava/util/Iterator;
    :cond_177
    const-string v7, "secure"

    #@179
    .line 219
    goto :goto_145

    #@17a
    .line 223
    .end local v2           #config:Landroid/net/wifi/WifiConfiguration;
    .end local v4           #i$:Ljava/util/Iterator;
    :cond_17a
    const-string v7, "open"

    #@17c
    goto :goto_145

    #@17d
    .line 229
    .end local v1           #cfgs:Ljava/util/List;,"Ljava/util/List<Landroid/net/wifi/WifiConfiguration;>;"
    .end local v3           #connectedSsid:Ljava/lang/String;
    .end local v7           #secure_type:Ljava/lang/String;
    .end local v9           #wifiInfo:Landroid/net/wifi/WifiInfo;
    :cond_17d
    iget-object v11, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@17f
    invoke-virtual {v11}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    #@182
    move-result-object v11

    #@183
    sget-object v12, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    #@185
    if-eq v11, v12, :cond_191

    #@187
    iget-object v11, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@189
    invoke-virtual {v11}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    #@18c
    move-result-object v11

    #@18d
    sget-object v12, Landroid/net/NetworkInfo$State;->UNKNOWN:Landroid/net/NetworkInfo$State;

    #@18f
    if-ne v11, v12, :cond_43

    #@191
    .line 232
    :cond_191
    iget-object v11, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@193
    invoke-virtual {v11}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    #@196
    move-result-object v6

    #@197
    .line 233
    .local v6, scanResults2:Ljava/util/List;,"Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    if-eqz v6, :cond_1c6

    #@199
    .line 234
    invoke-interface {v6}, Ljava/util/List;->size()I

    #@19c
    move-result v11

    #@19d
    if-eqz v11, :cond_1b5

    #@19f
    .line 235
    const/4 v11, 0x1

    #@1a0
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@1a3
    .line 236
    const/4 v11, 0x2

    #@1a4
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@1a7
    .line 237
    const/4 v11, 0x4

    #@1a8
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@1ab
    .line 239
    const/4 v11, 0x3

    #@1ac
    const/4 v12, 0x0

    #@1ad
    invoke-direct {p0, v11, v12}, Lcom/lge/systemservice/service/WifiLgeVZWService;->showNotification(ILjava/lang/String;)V

    #@1b0
    .line 240
    const/4 v11, 0x3

    #@1b1
    iput v11, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotiState:I

    #@1b3
    goto/16 :goto_43

    #@1b5
    .line 242
    :cond_1b5
    const/4 v11, 0x1

    #@1b6
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@1b9
    .line 243
    const/4 v11, 0x3

    #@1ba
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@1bd
    .line 244
    const/4 v11, 0x4

    #@1be
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@1c1
    .line 246
    const/4 v11, 0x2

    #@1c2
    iput v11, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotiState:I

    #@1c4
    goto/16 :goto_43

    #@1c6
    .line 249
    :cond_1c6
    const/4 v11, 0x1

    #@1c7
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@1ca
    .line 250
    const/4 v11, 0x3

    #@1cb
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@1ce
    .line 251
    const/4 v11, 0x4

    #@1cf
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@1d2
    .line 253
    const/4 v11, 0x2

    #@1d3
    iput v11, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotiState:I

    #@1d5
    goto/16 :goto_43

    #@1d7
    .line 257
    .end local v6           #scanResults2:Ljava/util/List;,"Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    :cond_1d7
    const-string v11, "android.intent.action.LOCALE_CHANGED"

    #@1d9
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1dc
    move-result v11

    #@1dd
    if-eqz v11, :cond_43

    #@1df
    .line 258
    const-string v11, "WifiLgeVZWService"

    #@1e1
    const-string v12, "Intent.ACTION_LOCALE_CHANGED"

    #@1e3
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e6
    .line 259
    iget-object v11, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@1e8
    invoke-virtual {v11}, Landroid/net/wifi/WifiManager;->getWifiState()I

    #@1eb
    move-result v10

    #@1ec
    .line 260
    .local v10, wifiState:I
    const/4 v11, 0x1

    #@1ed
    if-ne v10, v11, :cond_204

    #@1ef
    .line 261
    const/4 v11, 0x2

    #@1f0
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@1f3
    .line 262
    const/4 v11, 0x3

    #@1f4
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@1f7
    .line 263
    const/4 v11, 0x4

    #@1f8
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@1fb
    .line 264
    const/4 v11, 0x1

    #@1fc
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@1ff
    .line 265
    const/4 v11, 0x1

    #@200
    iput v11, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotiState:I

    #@202
    goto/16 :goto_43

    #@204
    .line 267
    :cond_204
    const/4 v11, 0x3

    #@205
    if-ne v10, v11, :cond_43

    #@207
    .line 268
    iget-object v11, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mConnectivityManager:Landroid/net/ConnectivityManager;

    #@209
    const/4 v12, 0x1

    #@20a
    invoke-virtual {v11, v12}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@20d
    move-result-object v8

    #@20e
    .line 269
    .local v8, wifi:Landroid/net/NetworkInfo;
    if-eqz v8, :cond_2a0

    #@210
    invoke-virtual {v8}, Landroid/net/NetworkInfo;->isConnected()Z

    #@213
    move-result v11

    #@214
    if-eqz v11, :cond_2a0

    #@216
    .line 270
    iget-object v11, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@218
    invoke-virtual {v11}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    #@21b
    move-result-object v9

    #@21c
    .line 271
    .restart local v9       #wifiInfo:Landroid/net/wifi/WifiInfo;
    if-eqz v9, :cond_43

    #@21e
    .line 272
    invoke-virtual {v9}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    #@221
    move-result-object v3

    #@222
    .line 273
    .restart local v3       #connectedSsid:Ljava/lang/String;
    if-eqz v3, :cond_43

    #@224
    .line 275
    const/4 v11, 0x1

    #@225
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@228
    .line 276
    const/4 v11, 0x2

    #@229
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@22c
    .line 277
    const/4 v11, 0x3

    #@22d
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@230
    .line 278
    const/4 v11, 0x4

    #@231
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@234
    .line 280
    const-string v7, "open"

    #@236
    .line 281
    .restart local v7       #secure_type:Ljava/lang/String;
    invoke-virtual {v9}, Landroid/net/wifi/WifiInfo;->getNetworkId()I

    #@239
    move-result v11

    #@23a
    if-gez v11, :cond_23e

    #@23c
    .line 282
    const-string v7, "open"

    #@23e
    .line 285
    :cond_23e
    iget-object v11, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@240
    invoke-virtual {v11}, Landroid/net/wifi/WifiManager;->getConfiguredNetworks()Ljava/util/List;

    #@243
    move-result-object v1

    #@244
    .line 287
    .restart local v1       #cfgs:Ljava/util/List;,"Ljava/util/List<Landroid/net/wifi/WifiConfiguration;>;"
    if-eqz v1, :cond_29d

    #@246
    .line 288
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@249
    move-result-object v4

    #@24a
    .restart local v4       #i$:Ljava/util/Iterator;
    :cond_24a
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@24d
    move-result v11

    #@24e
    if-eqz v11, :cond_268

    #@250
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@253
    move-result-object v2

    #@254
    check-cast v2, Landroid/net/wifi/WifiConfiguration;

    #@256
    .line 289
    .restart local v2       #config:Landroid/net/wifi/WifiConfiguration;
    iget v11, v2, Landroid/net/wifi/WifiConfiguration;->networkId:I

    #@258
    invoke-virtual {v9}, Landroid/net/wifi/WifiInfo;->getNetworkId()I

    #@25b
    move-result v12

    #@25c
    if-ne v11, v12, :cond_24a

    #@25e
    .line 290
    invoke-direct {p0, v2}, Lcom/lge/systemservice/service/WifiLgeVZWService;->getSecurity(Landroid/net/wifi/WifiConfiguration;)I

    #@261
    move-result v11

    #@262
    iget v12, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->SECURITY_NONE:I

    #@264
    if-ne v11, v12, :cond_29a

    #@266
    .line 291
    const-string v7, "open"

    #@268
    .line 301
    .end local v2           #config:Landroid/net/wifi/WifiConfiguration;
    .end local v4           #i$:Ljava/util/Iterator;
    :cond_268
    :goto_268
    const/4 v11, 0x4

    #@269
    new-instance v12, Ljava/lang/StringBuilder;

    #@26b
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@26e
    const-string v13, "\""

    #@270
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@273
    move-result-object v12

    #@274
    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@277
    move-result-object v12

    #@278
    const-string v13, "\""

    #@27a
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27d
    move-result-object v12

    #@27e
    const-string v13, " ("

    #@280
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@283
    move-result-object v12

    #@284
    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@287
    move-result-object v12

    #@288
    const-string v13, ")"

    #@28a
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28d
    move-result-object v12

    #@28e
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@291
    move-result-object v12

    #@292
    invoke-direct {p0, v11, v12}, Lcom/lge/systemservice/service/WifiLgeVZWService;->showNotification(ILjava/lang/String;)V

    #@295
    .line 302
    const/4 v11, 0x4

    #@296
    iput v11, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotiState:I

    #@298
    goto/16 :goto_43

    #@29a
    .line 293
    .restart local v2       #config:Landroid/net/wifi/WifiConfiguration;
    .restart local v4       #i$:Ljava/util/Iterator;
    :cond_29a
    const-string v7, "secure"

    #@29c
    .line 295
    goto :goto_268

    #@29d
    .line 299
    .end local v2           #config:Landroid/net/wifi/WifiConfiguration;
    .end local v4           #i$:Ljava/util/Iterator;
    :cond_29d
    const-string v7, "open"

    #@29f
    goto :goto_268

    #@2a0
    .line 307
    .end local v1           #cfgs:Ljava/util/List;,"Ljava/util/List<Landroid/net/wifi/WifiConfiguration;>;"
    .end local v3           #connectedSsid:Ljava/lang/String;
    .end local v7           #secure_type:Ljava/lang/String;
    .end local v9           #wifiInfo:Landroid/net/wifi/WifiInfo;
    :cond_2a0
    iget-object v11, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@2a2
    invoke-virtual {v11}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    #@2a5
    move-result-object v6

    #@2a6
    .line 308
    .restart local v6       #scanResults2:Ljava/util/List;,"Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    if-eqz v6, :cond_2dd

    #@2a8
    .line 309
    invoke-interface {v6}, Ljava/util/List;->size()I

    #@2ab
    move-result v11

    #@2ac
    if-eqz v11, :cond_2c8

    #@2ae
    .line 310
    const/4 v11, 0x1

    #@2af
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@2b2
    .line 311
    const/4 v11, 0x2

    #@2b3
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@2b6
    .line 312
    const/4 v11, 0x4

    #@2b7
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@2ba
    .line 313
    const/4 v11, 0x3

    #@2bb
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@2be
    .line 314
    const/4 v11, 0x3

    #@2bf
    const/4 v12, 0x0

    #@2c0
    invoke-direct {p0, v11, v12}, Lcom/lge/systemservice/service/WifiLgeVZWService;->showNotification(ILjava/lang/String;)V

    #@2c3
    .line 315
    const/4 v11, 0x3

    #@2c4
    iput v11, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotiState:I

    #@2c6
    goto/16 :goto_43

    #@2c8
    .line 317
    :cond_2c8
    const/4 v11, 0x1

    #@2c9
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@2cc
    .line 318
    const/4 v11, 0x3

    #@2cd
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@2d0
    .line 319
    const/4 v11, 0x4

    #@2d1
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@2d4
    .line 320
    const/4 v11, 0x2

    #@2d5
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@2d8
    .line 321
    const/4 v11, 0x2

    #@2d9
    iput v11, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotiState:I

    #@2db
    goto/16 :goto_43

    #@2dd
    .line 325
    :cond_2dd
    const/4 v11, 0x1

    #@2de
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@2e1
    .line 326
    const/4 v11, 0x3

    #@2e2
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@2e5
    .line 327
    const/4 v11, 0x4

    #@2e6
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@2e9
    .line 328
    const/4 v11, 0x2

    #@2ea
    invoke-direct {p0, v11}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@2ed
    .line 329
    const/4 v11, 0x2

    #@2ee
    iput v11, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotiState:I

    #@2f0
    goto/16 :goto_43
.end method

.method static removeDoubleQuotes(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "string"

    #@0
    .prologue
    const/16 v3, 0x22

    #@2
    const/4 v2, 0x1

    #@3
    .line 526
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@6
    move-result v0

    #@7
    .line 527
    .local v0, length:I
    if-le v0, v2, :cond_1e

    #@9
    const/4 v1, 0x0

    #@a
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@d
    move-result v1

    #@e
    if-ne v1, v3, :cond_1e

    #@10
    add-int/lit8 v1, v0, -0x1

    #@12
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@15
    move-result v1

    #@16
    if-ne v1, v3, :cond_1e

    #@18
    .line 529
    add-int/lit8 v1, v0, -0x1

    #@1a
    invoke-virtual {p0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1d
    move-result-object p0

    #@1e
    .line 531
    .end local p0
    :cond_1e
    return-object p0
.end method

.method private resetNotification(I)V
    .registers 10
    .parameter "index"

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 338
    const-string v0, "WifiLgeVZWService"

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v2, " resetNotification : "

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 340
    if-ne p1, v4, :cond_27

    #@1f
    .line 342
    iput-boolean v3, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mWifiNoti_WifiOff:Z

    #@21
    .line 343
    iget-object v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotificationManager:Landroid/app/NotificationManager;

    #@23
    invoke-virtual {v0, v4}, Landroid/app/NotificationManager;->cancel(I)V

    #@26
    .line 376
    :goto_26
    return-void

    #@27
    .line 345
    :cond_27
    if-ne p1, v5, :cond_31

    #@29
    .line 347
    iput-boolean v3, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mWifiNoti_NonetwrkDetected:Z

    #@2b
    .line 348
    iget-object v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotificationManager:Landroid/app/NotificationManager;

    #@2d
    invoke-virtual {v0, v5}, Landroid/app/NotificationManager;->cancel(I)V

    #@30
    goto :goto_26

    #@31
    .line 350
    :cond_31
    if-ne p1, v6, :cond_3b

    #@33
    .line 352
    iput-boolean v3, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mWifiNoti_NetworkAvail:Z

    #@35
    .line 353
    iget-object v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotificationManager:Landroid/app/NotificationManager;

    #@37
    invoke-virtual {v0, v6}, Landroid/app/NotificationManager;->cancel(I)V

    #@3a
    goto :goto_26

    #@3b
    .line 355
    :cond_3b
    if-ne p1, v7, :cond_45

    #@3d
    .line 357
    iput-boolean v3, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mWifiNoti_WifiConntected:Z

    #@3f
    .line 358
    iget-object v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotificationManager:Landroid/app/NotificationManager;

    #@41
    invoke-virtual {v0, v7}, Landroid/app/NotificationManager;->cancel(I)V

    #@44
    goto :goto_26

    #@45
    .line 361
    :cond_45
    const-string v0, "WifiLgeVZWService"

    #@47
    const-string v1, " resetNotification : all"

    #@49
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    .line 363
    iput-boolean v3, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mWifiNoti_WifiOff:Z

    #@4e
    .line 364
    iget-object v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotificationManager:Landroid/app/NotificationManager;

    #@50
    invoke-virtual {v0, v4}, Landroid/app/NotificationManager;->cancel(I)V

    #@53
    .line 366
    iput-boolean v3, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mWifiNoti_NonetwrkDetected:Z

    #@55
    .line 367
    iget-object v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotificationManager:Landroid/app/NotificationManager;

    #@57
    invoke-virtual {v0, v5}, Landroid/app/NotificationManager;->cancel(I)V

    #@5a
    .line 369
    iput-boolean v3, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mWifiNoti_NetworkAvail:Z

    #@5c
    .line 370
    iget-object v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotificationManager:Landroid/app/NotificationManager;

    #@5e
    invoke-virtual {v0, v6}, Landroid/app/NotificationManager;->cancel(I)V

    #@61
    .line 372
    iput-boolean v3, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mWifiNoti_WifiConntected:Z

    #@63
    .line 373
    iget-object v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotificationManager:Landroid/app/NotificationManager;

    #@65
    invoke-virtual {v0, v7}, Landroid/app/NotificationManager;->cancel(I)V

    #@68
    goto :goto_26
.end method

.method private showNotification(ILjava/lang/String;)V
    .registers 15
    .parameter "index"
    .parameter "connected_ssid"

    #@0
    .prologue
    const/high16 v11, 0x2000

    #@2
    const/high16 v10, 0x800

    #@4
    const/4 v9, 0x0

    #@5
    const/4 v8, -0x2

    #@6
    const/4 v7, 0x1

    #@7
    .line 410
    const-string v2, "WifiLgeVZWService"

    #@9
    new-instance v3, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v4, " showNotification : "

    #@10
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v3

    #@1c
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 412
    if-ne p1, v7, :cond_7f

    #@21
    .line 414
    iget-boolean v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mWifiNoti_WifiOff:Z

    #@23
    if-ne v2, v7, :cond_26

    #@25
    .line 511
    :cond_25
    :goto_25
    return-void

    #@26
    .line 418
    :cond_26
    new-instance v2, Landroid/app/Notification;

    #@28
    const v3, 0x202056c

    #@2b
    iget-object v4, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mContext:Landroid/content/Context;

    #@2d
    const v5, 0x20903b2

    #@30
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@33
    move-result-object v4

    #@34
    const-wide/16 v5, 0x0

    #@36
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    #@39
    iput-object v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotification_wifioff:Landroid/app/Notification;

    #@3b
    .line 420
    iget-object v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotification_wifioff:Landroid/app/Notification;

    #@3d
    new-instance v3, Landroid/widget/RemoteViews;

    #@3f
    iget-object v4, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mContext:Landroid/content/Context;

    #@41
    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@44
    move-result-object v4

    #@45
    const v5, 0x2030024

    #@48
    invoke-direct {v3, v4, v5}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    #@4b
    iput-object v3, v2, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    #@4d
    .line 423
    new-instance v0, Landroid/content/Intent;

    #@4f
    const-string v2, "android.settings.WIFI_SETTINGS"

    #@51
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@54
    .line 424
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {v0, v11}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@57
    .line 425
    iget-object v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mContext:Landroid/content/Context;

    #@59
    invoke-static {v2, v9, v0, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@5c
    move-result-object v2

    #@5d
    iput-object v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->contentIntent:Landroid/app/PendingIntent;

    #@5f
    .line 426
    iget-object v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotification_wifioff:Landroid/app/Notification;

    #@61
    iget-object v3, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->contentIntent:Landroid/app/PendingIntent;

    #@63
    iput-object v3, v2, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    #@65
    .line 427
    iget-object v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotification_wifioff:Landroid/app/Notification;

    #@67
    iget-object v3, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotification_wifioff:Landroid/app/Notification;

    #@69
    iget v3, v3, Landroid/app/Notification;->flags:I

    #@6b
    or-int/lit8 v3, v3, 0x10

    #@6d
    iput v3, v2, Landroid/app/Notification;->flags:I

    #@6f
    .line 428
    iget-object v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotification_wifioff:Landroid/app/Notification;

    #@71
    iget-object v3, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotification_wifioff:Landroid/app/Notification;

    #@73
    iput v8, v2, Landroid/app/Notification;->priority:I

    #@75
    .line 429
    iget-object v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotificationManager:Landroid/app/NotificationManager;

    #@77
    iget-object v3, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotification_wifioff:Landroid/app/Notification;

    #@79
    invoke-virtual {v2, v7, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    #@7c
    .line 430
    iput-boolean v7, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mWifiNoti_WifiOff:Z

    #@7e
    goto :goto_25

    #@7f
    .line 432
    .end local v0           #intent:Landroid/content/Intent;
    :cond_7f
    const/4 v2, 0x2

    #@80
    if-ne p1, v2, :cond_e1

    #@82
    .line 434
    iget-boolean v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mWifiNoti_NonetwrkDetected:Z

    #@84
    if-eq v2, v7, :cond_25

    #@86
    .line 437
    new-instance v2, Landroid/app/Notification;

    #@88
    const v3, 0x202056a

    #@8b
    iget-object v4, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mContext:Landroid/content/Context;

    #@8d
    const v5, 0x20903b3

    #@90
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@93
    move-result-object v4

    #@94
    const-wide/16 v5, 0x0

    #@96
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    #@99
    iput-object v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotification_nonetwork_detected:Landroid/app/Notification;

    #@9b
    .line 438
    iget-object v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotification_nonetwork_detected:Landroid/app/Notification;

    #@9d
    new-instance v3, Landroid/widget/RemoteViews;

    #@9f
    iget-object v4, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mContext:Landroid/content/Context;

    #@a1
    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@a4
    move-result-object v4

    #@a5
    const v5, 0x2030023

    #@a8
    invoke-direct {v3, v4, v5}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    #@ab
    iput-object v3, v2, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    #@ad
    .line 444
    new-instance v0, Landroid/content/Intent;

    #@af
    const-string v2, "android.settings.WIFI_SETTINGS"

    #@b1
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@b4
    .line 445
    .restart local v0       #intent:Landroid/content/Intent;
    invoke-virtual {v0, v11}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@b7
    .line 446
    iget-object v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mContext:Landroid/content/Context;

    #@b9
    invoke-static {v2, v9, v0, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@bc
    move-result-object v2

    #@bd
    iput-object v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->contentIntent:Landroid/app/PendingIntent;

    #@bf
    .line 447
    iget-object v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotification_nonetwork_detected:Landroid/app/Notification;

    #@c1
    iget-object v3, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->contentIntent:Landroid/app/PendingIntent;

    #@c3
    iput-object v3, v2, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    #@c5
    .line 449
    iget-object v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotification_nonetwork_detected:Landroid/app/Notification;

    #@c7
    iget-object v3, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotification_nonetwork_detected:Landroid/app/Notification;

    #@c9
    iget v3, v3, Landroid/app/Notification;->flags:I

    #@cb
    or-int/lit8 v3, v3, 0x10

    #@cd
    iput v3, v2, Landroid/app/Notification;->flags:I

    #@cf
    .line 450
    iget-object v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotification_nonetwork_detected:Landroid/app/Notification;

    #@d1
    iget-object v3, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotification_nonetwork_detected:Landroid/app/Notification;

    #@d3
    iput v8, v2, Landroid/app/Notification;->priority:I

    #@d5
    .line 451
    iget-object v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotificationManager:Landroid/app/NotificationManager;

    #@d7
    const/4 v3, 0x2

    #@d8
    iget-object v4, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotification_nonetwork_detected:Landroid/app/Notification;

    #@da
    invoke-virtual {v2, v3, v4}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    #@dd
    .line 452
    iput-boolean v7, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mWifiNoti_NonetwrkDetected:Z

    #@df
    goto/16 :goto_25

    #@e1
    .line 454
    .end local v0           #intent:Landroid/content/Intent;
    :cond_e1
    const/4 v2, 0x3

    #@e2
    if-ne p1, v2, :cond_143

    #@e4
    .line 457
    iget-boolean v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mWifiNoti_NetworkAvail:Z

    #@e6
    if-eq v2, v7, :cond_25

    #@e8
    .line 460
    new-instance v2, Landroid/app/Notification;

    #@ea
    const v3, 0x202056b

    #@ed
    iget-object v4, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mContext:Landroid/content/Context;

    #@ef
    const v5, 0x20903b4

    #@f2
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@f5
    move-result-object v4

    #@f6
    const-wide/16 v5, 0x0

    #@f8
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    #@fb
    iput-object v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotification_network_available:Landroid/app/Notification;

    #@fd
    .line 462
    iget-object v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotification_network_available:Landroid/app/Notification;

    #@ff
    new-instance v3, Landroid/widget/RemoteViews;

    #@101
    iget-object v4, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mContext:Landroid/content/Context;

    #@103
    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@106
    move-result-object v4

    #@107
    const v5, 0x2030022

    #@10a
    invoke-direct {v3, v4, v5}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    #@10d
    iput-object v3, v2, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    #@10f
    .line 466
    new-instance v0, Landroid/content/Intent;

    #@111
    const-string v2, "android.settings.WIFI_SETTINGS"

    #@113
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@116
    .line 467
    .restart local v0       #intent:Landroid/content/Intent;
    invoke-virtual {v0, v11}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@119
    .line 468
    iget-object v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mContext:Landroid/content/Context;

    #@11b
    invoke-static {v2, v9, v0, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@11e
    move-result-object v2

    #@11f
    iput-object v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->contentIntent:Landroid/app/PendingIntent;

    #@121
    .line 469
    iget-object v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotification_network_available:Landroid/app/Notification;

    #@123
    iget-object v3, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->contentIntent:Landroid/app/PendingIntent;

    #@125
    iput-object v3, v2, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    #@127
    .line 473
    iget-object v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotification_network_available:Landroid/app/Notification;

    #@129
    iget-object v3, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotification_network_available:Landroid/app/Notification;

    #@12b
    iget v3, v3, Landroid/app/Notification;->flags:I

    #@12d
    or-int/lit8 v3, v3, 0x10

    #@12f
    iput v3, v2, Landroid/app/Notification;->flags:I

    #@131
    .line 474
    iget-object v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotification_network_available:Landroid/app/Notification;

    #@133
    iget-object v3, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotification_network_available:Landroid/app/Notification;

    #@135
    iput v8, v2, Landroid/app/Notification;->priority:I

    #@137
    .line 475
    iget-object v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotificationManager:Landroid/app/NotificationManager;

    #@139
    const/4 v3, 0x3

    #@13a
    iget-object v4, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotification_network_available:Landroid/app/Notification;

    #@13c
    invoke-virtual {v2, v3, v4}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    #@13f
    .line 476
    iput-boolean v7, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mWifiNoti_NetworkAvail:Z

    #@141
    goto/16 :goto_25

    #@143
    .line 478
    .end local v0           #intent:Landroid/content/Intent;
    :cond_143
    const/4 v2, 0x4

    #@144
    if-ne p1, v2, :cond_1c8

    #@146
    .line 481
    iget-boolean v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mWifiNoti_WifiConntected:Z

    #@148
    if-eq v2, v7, :cond_25

    #@14a
    .line 485
    if-eqz p2, :cond_25

    #@14c
    .line 486
    new-instance v2, Ljava/lang/StringBuilder;

    #@14e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@151
    iget-object v3, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mContext:Landroid/content/Context;

    #@153
    const v4, 0x20903b5

    #@156
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@159
    move-result-object v3

    #@15a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15d
    move-result-object v2

    #@15e
    const-string v3, " "

    #@160
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@163
    move-result-object v2

    #@164
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@167
    move-result-object v2

    #@168
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16b
    move-result-object v1

    #@16c
    .line 487
    .local v1, mtitle:Ljava/lang/String;
    new-instance v2, Landroid/app/Notification;

    #@16e
    const v3, 0x2020566

    #@171
    const-wide/16 v4, 0x0

    #@173
    invoke-direct {v2, v3, v1, v4, v5}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    #@176
    iput-object v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotification_wifi_conntected:Landroid/app/Notification;

    #@178
    .line 488
    iget-object v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotification_wifi_conntected:Landroid/app/Notification;

    #@17a
    new-instance v3, Landroid/widget/RemoteViews;

    #@17c
    iget-object v4, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mContext:Landroid/content/Context;

    #@17e
    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@181
    move-result-object v4

    #@182
    const v5, 0x2030021

    #@185
    invoke-direct {v3, v4, v5}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    #@188
    iput-object v3, v2, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    #@18a
    .line 492
    new-instance v0, Landroid/content/Intent;

    #@18c
    const-string v2, "android.settings.WIFI_SETTINGS"

    #@18e
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@191
    .line 493
    .restart local v0       #intent:Landroid/content/Intent;
    invoke-virtual {v0, v11}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@194
    .line 494
    iget-object v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mContext:Landroid/content/Context;

    #@196
    invoke-static {v2, v9, v0, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@199
    move-result-object v2

    #@19a
    iput-object v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->contentIntent:Landroid/app/PendingIntent;

    #@19c
    .line 495
    iget-object v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotification_wifi_conntected:Landroid/app/Notification;

    #@19e
    iget-object v3, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->contentIntent:Landroid/app/PendingIntent;

    #@1a0
    iput-object v3, v2, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    #@1a2
    .line 496
    iget-object v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotification_wifi_conntected:Landroid/app/Notification;

    #@1a4
    iget-object v2, v2, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    #@1a6
    const v3, 0x20d0079

    #@1a9
    invoke-virtual {v2, v3, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    #@1ac
    .line 502
    iget-object v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotification_wifi_conntected:Landroid/app/Notification;

    #@1ae
    iget-object v3, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotification_wifi_conntected:Landroid/app/Notification;

    #@1b0
    iget v3, v3, Landroid/app/Notification;->flags:I

    #@1b2
    or-int/lit8 v3, v3, 0x10

    #@1b4
    iput v3, v2, Landroid/app/Notification;->flags:I

    #@1b6
    .line 504
    iget-object v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotification_wifi_conntected:Landroid/app/Notification;

    #@1b8
    iget-object v3, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotification_wifi_conntected:Landroid/app/Notification;

    #@1ba
    iput v8, v2, Landroid/app/Notification;->priority:I

    #@1bc
    .line 505
    iget-object v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotificationManager:Landroid/app/NotificationManager;

    #@1be
    const/4 v3, 0x4

    #@1bf
    iget-object v4, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotification_wifi_conntected:Landroid/app/Notification;

    #@1c1
    invoke-virtual {v2, v3, v4}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    #@1c4
    .line 506
    iput-boolean v7, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mWifiNoti_WifiConntected:Z

    #@1c6
    goto/16 :goto_25

    #@1c8
    .line 509
    .end local v0           #intent:Landroid/content/Intent;
    .end local v1           #mtitle:Ljava/lang/String;
    :cond_1c8
    const-string v2, "WifiLgeVZWService"

    #@1ca
    const-string v3, " showNotification : index unavailable"

    #@1cc
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1cf
    goto/16 :goto_25
.end method


# virtual methods
.method public init()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 98
    const-string v0, "WifiLgeVZWService"

    #@3
    const-string v1, " WifiLgeVZWService Init!!"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 100
    iget-object v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@a
    if-nez v0, :cond_18

    #@c
    .line 101
    iget-object v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mContext:Landroid/content/Context;

    #@e
    const-string v1, "wifi"

    #@10
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@13
    move-result-object v0

    #@14
    check-cast v0, Landroid/net/wifi/WifiManager;

    #@16
    iput-object v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@18
    .line 104
    :cond_18
    iget-object v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mConnectivityManager:Landroid/net/ConnectivityManager;

    #@1a
    if-nez v0, :cond_28

    #@1c
    .line 105
    iget-object v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mContext:Landroid/content/Context;

    #@1e
    const-string v1, "connectivity"

    #@20
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@23
    move-result-object v0

    #@24
    check-cast v0, Landroid/net/ConnectivityManager;

    #@26
    iput-object v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mConnectivityManager:Landroid/net/ConnectivityManager;

    #@28
    .line 108
    :cond_28
    iget-object v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@2a
    if-nez v0, :cond_38

    #@2c
    .line 109
    new-instance v0, Landroid/net/NetworkInfo;

    #@2e
    const/4 v1, 0x0

    #@2f
    const-string v2, "WIFI"

    #@31
    const-string v3, ""

    #@33
    invoke-direct {v0, v4, v1, v2, v3}, Landroid/net/NetworkInfo;-><init>(IILjava/lang/String;Ljava/lang/String;)V

    #@36
    iput-object v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@38
    .line 116
    :cond_38
    iget-object v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotificationManager:Landroid/app/NotificationManager;

    #@3a
    if-nez v0, :cond_48

    #@3c
    .line 118
    iget-object v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mContext:Landroid/content/Context;

    #@3e
    const-string v1, "notification"

    #@40
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@43
    move-result-object v0

    #@44
    check-cast v0, Landroid/app/NotificationManager;

    #@46
    iput-object v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotificationManager:Landroid/app/NotificationManager;

    #@48
    .line 122
    :cond_48
    iget-object v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@4a
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getWifiState()I

    #@4d
    move-result v0

    #@4e
    iput v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mwifiState:I

    #@50
    .line 123
    const-string v0, "WifiLgeVZWService"

    #@52
    new-instance v1, Ljava/lang/StringBuilder;

    #@54
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@57
    const-string v2, " mwifiState "

    #@59
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v1

    #@5d
    iget v2, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mwifiState:I

    #@5f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@62
    move-result-object v1

    #@63
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66
    move-result-object v1

    #@67
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6a
    .line 124
    iget v0, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mwifiState:I

    #@6c
    if-ne v0, v4, :cond_7c

    #@6e
    .line 125
    const/4 v0, 0x2

    #@6f
    invoke-direct {p0, v0}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@72
    .line 126
    const/4 v0, 0x3

    #@73
    invoke-direct {p0, v0}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@76
    .line 127
    const/4 v0, 0x4

    #@77
    invoke-direct {p0, v0}, Lcom/lge/systemservice/service/WifiLgeVZWService;->resetNotification(I)V

    #@7a
    .line 129
    iput v4, p0, Lcom/lge/systemservice/service/WifiLgeVZWService;->mNotiState:I

    #@7c
    .line 131
    :cond_7c
    return-void
.end method
