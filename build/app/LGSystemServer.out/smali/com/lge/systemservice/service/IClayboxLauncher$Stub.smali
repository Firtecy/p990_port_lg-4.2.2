.class public abstract Lcom/lge/systemservice/service/IClayboxLauncher$Stub;
.super Landroid/os/Binder;
.source "IClayboxLauncher.java"

# interfaces
.implements Lcom/lge/systemservice/service/IClayboxLauncher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/IClayboxLauncher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 15
    const-string v0, "com.lge.systemservice.service.IClayboxLauncher"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/lge/systemservice/service/IClayboxLauncher$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 16
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 10
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 38
    sparse-switch p1, :sswitch_data_38

    #@4
    .line 65
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v4

    #@8
    :goto_8
    return v4

    #@9
    .line 42
    :sswitch_9
    const-string v3, "com.lge.systemservice.service.IClayboxLauncher"

    #@b
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 47
    :sswitch_f
    const-string v3, "com.lge.systemservice.service.IClayboxLauncher"

    #@11
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@17
    move-result-object v0

    #@18
    .line 51
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    .line 52
    .local v1, _arg1:Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/lge/systemservice/service/IClayboxLauncher$Stub;->startClaybox(Ljava/lang/String;Ljava/lang/String;)Z

    #@1f
    move-result v2

    #@20
    .line 53
    .local v2, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@23
    .line 54
    if-eqz v2, :cond_2a

    #@25
    move v3, v4

    #@26
    :goto_26
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@29
    goto :goto_8

    #@2a
    :cond_2a
    const/4 v3, 0x0

    #@2b
    goto :goto_26

    #@2c
    .line 59
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:Ljava/lang/String;
    .end local v2           #_result:Z
    :sswitch_2c
    const-string v3, "com.lge.systemservice.service.IClayboxLauncher"

    #@2e
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@31
    .line 60
    invoke-virtual {p0}, Lcom/lge/systemservice/service/IClayboxLauncher$Stub;->stopClaybox()V

    #@34
    .line 61
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@37
    goto :goto_8

    #@38
    .line 38
    :sswitch_data_38
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_2c
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
