.class Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$8;
.super Ljava/lang/Object;
.source "WfdStateTracker.java"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->cancelWifiDisplayConnect()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1242
    iput-object p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$8;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onFailure(I)V
    .registers 5
    .parameter "reason"

    #@0
    .prologue
    .line 1253
    const-string v0, "WfdStateTracker"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Direct disconnect fail, reason: "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1254
    return-void
.end method

.method public onSuccess()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x2

    #@1
    .line 1244
    const-string v0, "WfdStateTracker"

    #@3
    const-string v1, "Direct disconnect success"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 1245
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$8;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@a
    invoke-static {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$200(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)I

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_16

    #@10
    .line 1246
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$8;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@12
    invoke-static {v0, v2}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;I)V

    #@15
    .line 1251
    :cond_15
    :goto_15
    return-void

    #@16
    .line 1247
    :cond_16
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$8;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@18
    invoke-static {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$200(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)I

    #@1b
    move-result v0

    #@1c
    const/4 v1, 0x1

    #@1d
    if-ne v0, v1, :cond_15

    #@1f
    .line 1248
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$8;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@21
    invoke-static {v0, v2}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;I)V

    #@24
    .line 1249
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$8;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@26
    const/4 v1, 0x4

    #@27
    invoke-static {v0, v1}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;I)V

    #@2a
    goto :goto_15
.end method
