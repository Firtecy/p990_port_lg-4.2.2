.class Lcom/lge/systemservice/service/InfoCollector$3;
.super Ljava/lang/Object;
.source "InfoCollector.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lge/systemservice/service/InfoCollector;->getPosition()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/InfoCollector;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/InfoCollector;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 441
    iput-object p1, p0, Lcom/lge/systemservice/service/InfoCollector$3;->this$0:Lcom/lge/systemservice/service/InfoCollector;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .registers 4
    .parameter "location"

    #@0
    .prologue
    .line 444
    const-string v0, "IC"

    #@2
    const-string v1, "loc ok"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 445
    const/4 v0, 0x1

    #@8
    invoke-static {v0}, Lcom/lge/systemservice/service/InfoCollector;->access$702(Z)Z

    #@b
    .line 446
    invoke-static {}, Lcom/lge/systemservice/service/InfoCollector;->access$800()Landroid/location/LocationManager;

    #@e
    move-result-object v0

    #@f
    if-eqz v0, :cond_1c

    #@11
    .line 454
    invoke-static {}, Lcom/lge/systemservice/service/InfoCollector;->access$800()Landroid/location/LocationManager;

    #@14
    move-result-object v0

    #@15
    invoke-static {}, Lcom/lge/systemservice/service/InfoCollector;->access$900()Landroid/location/LocationListener;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    #@1c
    .line 457
    :cond_1c
    iget-object v0, p0, Lcom/lge/systemservice/service/InfoCollector$3;->this$0:Lcom/lge/systemservice/service/InfoCollector;

    #@1e
    invoke-virtual {v0, p1}, Lcom/lge/systemservice/service/InfoCollector;->sendLocationUpdate(Landroid/location/Location;)V

    #@21
    .line 464
    iget-object v0, p0, Lcom/lge/systemservice/service/InfoCollector$3;->this$0:Lcom/lge/systemservice/service/InfoCollector;

    #@23
    invoke-virtual {v0}, Lcom/lge/systemservice/service/InfoCollector;->buildAndSendInfotoHTTPs()V

    #@26
    .line 466
    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .registers 5
    .parameter "arg0"

    #@0
    .prologue
    .line 475
    const-string v0, "InfoCollector"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[SJJ] onProviderDisabled : "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 476
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .registers 2
    .parameter "arg0"

    #@0
    .prologue
    .line 482
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .registers 4
    .parameter "arg0"
    .parameter "arg1"
    .parameter "arg2"

    #@0
    .prologue
    .line 472
    return-void
.end method
