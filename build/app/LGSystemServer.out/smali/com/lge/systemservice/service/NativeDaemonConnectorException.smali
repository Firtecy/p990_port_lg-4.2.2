.class public Lcom/lge/systemservice/service/NativeDaemonConnectorException;
.super Ljava/lang/Exception;
.source "NativeDaemonConnectorException.java"


# instance fields
.field private mCmd:Ljava/lang/String;

.field private mEvent:Lcom/lge/systemservice/service/NativeDaemonEvent;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 2
    .parameter "detailMessage"

    #@0
    .prologue
    .line 30
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    #@3
    .line 31
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/lge/systemservice/service/NativeDaemonEvent;)V
    .registers 5
    .parameter "cmd"
    .parameter "event"

    #@0
    .prologue
    .line 38
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "command \'"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    const-string v1, "\' failed with \'"

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    const-string v1, "\'"

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    invoke-direct {p0, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    #@26
    .line 39
    iput-object p1, p0, Lcom/lge/systemservice/service/NativeDaemonConnectorException;->mCmd:Ljava/lang/String;

    #@28
    .line 40
    iput-object p2, p0, Lcom/lge/systemservice/service/NativeDaemonConnectorException;->mEvent:Lcom/lge/systemservice/service/NativeDaemonEvent;

    #@2a
    .line 41
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .registers 3
    .parameter "detailMessage"
    .parameter "throwable"

    #@0
    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@3
    .line 35
    return-void
.end method


# virtual methods
.method public getCode()I
    .registers 2

    #@0
    .prologue
    .line 44
    iget-object v0, p0, Lcom/lge/systemservice/service/NativeDaemonConnectorException;->mEvent:Lcom/lge/systemservice/service/NativeDaemonEvent;

    #@2
    invoke-virtual {v0}, Lcom/lge/systemservice/service/NativeDaemonEvent;->getCode()I

    #@5
    move-result v0

    #@6
    return v0
.end method
