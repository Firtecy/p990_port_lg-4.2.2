.class public Lcom/lge/systemservice/service/wfdservice/WfdMonitor;
.super Ljava/lang/Object;
.source "WfdMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/systemservice/service/wfdservice/WfdMonitor$EventHandler;
    }
.end annotation


# instance fields
.field private mEventHandler:Lcom/lge/systemservice/service/wfdservice/WfdMonitor$EventHandler;

.field private mWfdAdaptation:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

.field private final mWfdStateTracker:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;


# direct methods
.method public constructor <init>(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)V
    .registers 3
    .parameter "tracker"
    .parameter "adaptation"

    #@0
    .prologue
    .line 35
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 36
    iput-object p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdMonitor;->mWfdStateTracker:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@5
    .line 37
    iput-object p2, p0, Lcom/lge/systemservice/service/wfdservice/WfdMonitor;->mWfdAdaptation:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@7
    .line 38
    return-void
.end method

.method static synthetic access$000(Lcom/lge/systemservice/service/wfdservice/WfdMonitor;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 19
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdMonitor;->mWfdStateTracker:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@2
    return-object v0
.end method


# virtual methods
.method public onReceiveEventFromNative(III)V
    .registers 6
    .parameter "what"
    .parameter "ext1"
    .parameter "ext2"

    #@0
    .prologue
    .line 76
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdMonitor;->mEventHandler:Lcom/lge/systemservice/service/wfdservice/WfdMonitor$EventHandler;

    #@2
    invoke-static {v1, p1, p2, p3}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    #@5
    move-result-object v0

    #@6
    .line 77
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdMonitor;->mEventHandler:Lcom/lge/systemservice/service/wfdservice/WfdMonitor$EventHandler;

    #@8
    invoke-virtual {v1, v0}, Lcom/lge/systemservice/service/wfdservice/WfdMonitor$EventHandler;->sendMessage(Landroid/os/Message;)Z

    #@b
    .line 78
    return-void
.end method

.method public onReceiveEventFromNative(IIILjava/lang/Object;)V
    .registers 7
    .parameter "what"
    .parameter "ext1"
    .parameter "ext2"
    .parameter "obj"

    #@0
    .prologue
    .line 82
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdMonitor;->mEventHandler:Lcom/lge/systemservice/service/wfdservice/WfdMonitor$EventHandler;

    #@2
    invoke-static {v1, p1, p2, p3}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    #@5
    move-result-object v0

    #@6
    .line 83
    .local v0, msg:Landroid/os/Message;
    iput-object p4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@8
    .line 84
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdMonitor;->mEventHandler:Lcom/lge/systemservice/service/wfdservice/WfdMonitor$EventHandler;

    #@a
    invoke-virtual {v1, v0}, Lcom/lge/systemservice/service/wfdservice/WfdMonitor$EventHandler;->sendMessage(Landroid/os/Message;)Z

    #@d
    .line 85
    return-void
.end method

.method public startMonitoring()V
    .registers 4

    #@0
    .prologue
    .line 41
    const-string v1, "WfdMonitor"

    #@2
    const-string v2, "Start WfdMonitor"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 44
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@a
    move-result-object v0

    #@b
    .local v0, looper:Landroid/os/Looper;
    if-eqz v0, :cond_1a

    #@d
    .line 45
    new-instance v1, Lcom/lge/systemservice/service/wfdservice/WfdMonitor$EventHandler;

    #@f
    invoke-direct {v1, p0, v0}, Lcom/lge/systemservice/service/wfdservice/WfdMonitor$EventHandler;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdMonitor;Landroid/os/Looper;)V

    #@12
    iput-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdMonitor;->mEventHandler:Lcom/lge/systemservice/service/wfdservice/WfdMonitor$EventHandler;

    #@14
    .line 56
    :goto_14
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdMonitor;->mWfdAdaptation:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@16
    invoke-virtual {v1, p0}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->native_setup(Lcom/lge/systemservice/service/wfdservice/WfdMonitor;)V

    #@19
    .line 57
    return-void

    #@1a
    .line 46
    :cond_1a
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    #@1d
    move-result-object v0

    #@1e
    if-eqz v0, :cond_28

    #@20
    .line 47
    new-instance v1, Lcom/lge/systemservice/service/wfdservice/WfdMonitor$EventHandler;

    #@22
    invoke-direct {v1, p0, v0}, Lcom/lge/systemservice/service/wfdservice/WfdMonitor$EventHandler;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdMonitor;Landroid/os/Looper;)V

    #@25
    iput-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdMonitor;->mEventHandler:Lcom/lge/systemservice/service/wfdservice/WfdMonitor$EventHandler;

    #@27
    goto :goto_14

    #@28
    .line 49
    :cond_28
    const/4 v1, 0x0

    #@29
    iput-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdMonitor;->mEventHandler:Lcom/lge/systemservice/service/wfdservice/WfdMonitor$EventHandler;

    #@2b
    .line 50
    const-string v1, "WfdMonitor"

    #@2d
    const-string v2, "fail to get event looper"

    #@2f
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    goto :goto_14
.end method

.method public stopMonitoring()V
    .registers 3

    #@0
    .prologue
    .line 60
    const-string v0, "WfdMonitor"

    #@2
    const-string v1, "Stop WfdMonitor"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 61
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdMonitor;->mWfdAdaptation:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@9
    invoke-virtual {v0}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->native_finalize()V

    #@c
    .line 62
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdMonitor;->mEventHandler:Lcom/lge/systemservice/service/wfdservice/WfdMonitor$EventHandler;

    #@e
    if-eqz v0, :cond_16

    #@10
    .line 63
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdMonitor;->mEventHandler:Lcom/lge/systemservice/service/wfdservice/WfdMonitor$EventHandler;

    #@12
    const/4 v1, 0x0

    #@13
    invoke-virtual {v0, v1}, Lcom/lge/systemservice/service/wfdservice/WfdMonitor$EventHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    #@16
    .line 64
    :cond_16
    return-void
.end method
