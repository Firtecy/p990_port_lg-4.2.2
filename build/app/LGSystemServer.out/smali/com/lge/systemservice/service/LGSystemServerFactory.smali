.class public Lcom/lge/systemservice/service/LGSystemServerFactory;
.super Landroid/app/Service;
.source "LGSystemServerFactory.java"


# instance fields
.field private mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    #@3
    .line 17
    new-instance v0, Landroid/os/Handler;

    #@5
    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    #@8
    iput-object v0, p0, Lcom/lge/systemservice/service/LGSystemServerFactory;->mHandler:Landroid/os/Handler;

    #@a
    return-void
.end method

.method static synthetic access$000(Lcom/lge/systemservice/service/LGSystemServerFactory;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Lcom/lge/systemservice/service/LGSystemServerFactory;->handleSystemService()V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/lge/systemservice/service/LGSystemServerFactory;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Lcom/lge/systemservice/service/LGSystemServerFactory;->handleObservers()V

    #@3
    return-void
.end method

.method private handleObservers()V
    .registers 3

    #@0
    .prologue
    .line 100
    const-string v0, "LGSystemServerFactory"

    #@2
    const-string v1, "Start Observer"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 117
    return-void
.end method

.method private handleSystemService()V
    .registers 3

    #@0
    .prologue
    .line 122
    const-string v0, "LGSystemServerFactory"

    #@2
    const-string v1, "Start SystemService"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 125
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 3
    .parameter "arg0"

    #@0
    .prologue
    .line 45
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public onCreate()V
    .registers 3

    #@0
    .prologue
    .line 27
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    #@3
    .line 28
    const-string v0, "LGSystemServerFactory"

    #@5
    const-string v1, "Create LGSystemServer for factory features"

    #@7
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 32
    iget-object v0, p0, Lcom/lge/systemservice/service/LGSystemServerFactory;->mHandler:Landroid/os/Handler;

    #@c
    new-instance v1, Lcom/lge/systemservice/service/LGSystemServerFactory$1;

    #@e
    invoke-direct {v1, p0}, Lcom/lge/systemservice/service/LGSystemServerFactory$1;-><init>(Lcom/lge/systemservice/service/LGSystemServerFactory;)V

    #@11
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@14
    .line 40
    return-void
.end method

.method public onDestroy()V
    .registers 3

    #@0
    .prologue
    .line 91
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    #@3
    .line 92
    const-string v0, "LGSystemServerFactory"

    #@5
    const-string v1, "Destory LGSystemServerFactory"

    #@7
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 93
    return-void
.end method

.method public onLowMemory()V
    .registers 1

    #@0
    .prologue
    .line 51
    invoke-super {p0}, Landroid/app/Service;->onLowMemory()V

    #@3
    .line 52
    return-void
.end method

.method public onRebind(Landroid/content/Intent;)V
    .registers 2
    .parameter "intent"

    #@0
    .prologue
    .line 57
    invoke-super {p0, p1}, Landroid/app/Service;->onRebind(Landroid/content/Intent;)V

    #@3
    .line 58
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .registers 5
    .parameter "intent"
    .parameter "flags"
    .parameter "startId"

    #@0
    .prologue
    .line 78
    const/4 v0, 0x2

    #@1
    return v0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .registers 3
    .parameter "intent"

    #@0
    .prologue
    .line 84
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method
