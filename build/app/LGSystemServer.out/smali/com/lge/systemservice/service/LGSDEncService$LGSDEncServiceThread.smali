.class Lcom/lge/systemservice/service/LGSDEncService$LGSDEncServiceThread;
.super Ljava/lang/Thread;
.source "LGSDEncService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/LGSDEncService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LGSDEncServiceThread"
.end annotation


# instance fields
.field mediatypeStr:Ljava/lang/String;

.field rc:I

.field final synthetic this$0:Lcom/lge/systemservice/service/LGSDEncService;


# direct methods
.method private constructor <init>(Lcom/lge/systemservice/service/LGSDEncService;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 381
    iput-object p1, p0, Lcom/lge/systemservice/service/LGSDEncService$LGSDEncServiceThread;->this$0:Lcom/lge/systemservice/service/LGSDEncService;

    #@2
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@5
    .line 382
    const/4 v0, 0x0

    #@6
    iput-object v0, p0, Lcom/lge/systemservice/service/LGSDEncService$LGSDEncServiceThread;->mediatypeStr:Ljava/lang/String;

    #@8
    .line 383
    const/4 v0, 0x0

    #@9
    iput v0, p0, Lcom/lge/systemservice/service/LGSDEncService$LGSDEncServiceThread;->rc:I

    #@b
    return-void
.end method

.method synthetic constructor <init>(Lcom/lge/systemservice/service/LGSDEncService;Lcom/lge/systemservice/service/LGSDEncService$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 381
    invoke-direct {p0, p1}, Lcom/lge/systemservice/service/LGSDEncService$LGSDEncServiceThread;-><init>(Lcom/lge/systemservice/service/LGSDEncService;)V

    #@3
    return-void
.end method


# virtual methods
.method public run()V
    .registers 7

    #@0
    .prologue
    const/4 v5, -0x1

    #@1
    .line 387
    :try_start_1
    iget-object v2, p0, Lcom/lge/systemservice/service/LGSDEncService$LGSDEncServiceThread;->this$0:Lcom/lge/systemservice/service/LGSDEncService;

    #@3
    invoke-virtual {v2}, Lcom/lge/systemservice/service/LGSDEncService;->getExtensionMedia()Ljava/lang/String;

    #@6
    move-result-object v2

    #@7
    iput-object v2, p0, Lcom/lge/systemservice/service/LGSDEncService$LGSDEncServiceThread;->mediatypeStr:Ljava/lang/String;

    #@9
    .line 388
    iget-object v2, p0, Lcom/lge/systemservice/service/LGSDEncService$LGSDEncServiceThread;->mediatypeStr:Ljava/lang/String;

    #@b
    invoke-static {v2}, Lcom/lge/systemservice/core/lgsdencmanager/LGSDEncNative;->sysCallMediaExt(Ljava/lang/String;)Z

    #@e
    .line 389
    iget-object v2, p0, Lcom/lge/systemservice/service/LGSDEncService$LGSDEncServiceThread;->this$0:Lcom/lge/systemservice/service/LGSDEncService;

    #@10
    invoke-virtual {v2}, Lcom/lge/systemservice/service/LGSDEncService;->getMediaProperty()I

    #@13
    move-result v2

    #@14
    invoke-static {v2}, Lcom/lge/systemservice/core/lgsdencmanager/LGSDEncNative;->sysCallMediaProperty(I)I

    #@17
    .line 390
    const-wide/16 v2, 0x0

    #@19
    invoke-static {v2, v3}, Lcom/lge/systemservice/service/LGSDEncService;->access$202(J)J

    #@1c
    .line 391
    const-string v2, "LGSDEncService"

    #@1e
    new-instance v3, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v4, "BOOT_BroadcastReceiver Media SystemCall :: getMediaProperty = "

    #@25
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    iget-object v4, p0, Lcom/lge/systemservice/service/LGSDEncService$LGSDEncServiceThread;->this$0:Lcom/lge/systemservice/service/LGSDEncService;

    #@2b
    invoke-virtual {v4}, Lcom/lge/systemservice/service/LGSDEncService;->getMediaProperty()I

    #@2e
    move-result v4

    #@2f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v3

    #@37
    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 392
    const-string v2, "LGSDEncService"

    #@3c
    const-string v3, "Initializing Encryption start"

    #@3e
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    .line 394
    const-string v2, "ro.crypto.state"

    #@43
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@46
    move-result-object v2

    #@47
    const-string v3, "encrypted"

    #@49
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4c
    move-result v2

    #@4d
    if-eqz v2, :cond_f6

    #@4f
    .line 395
    const-string v2, "persist.insdcrypto.enabled"

    #@51
    const/4 v3, -0x1

    #@52
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@55
    move-result v1

    #@56
    .line 396
    .local v1, enabled:I
    const-string v2, "LGSDEncService"

    #@58
    new-instance v3, Ljava/lang/StringBuilder;

    #@5a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5d
    const-string v4, "Property enabled="

    #@5f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v3

    #@63
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@66
    move-result-object v3

    #@67
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6a
    move-result-object v3

    #@6b
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6e
    .line 398
    if-nez v1, :cond_f7

    #@70
    .line 399
    iget-object v2, p0, Lcom/lge/systemservice/service/LGSDEncService$LGSDEncServiceThread;->this$0:Lcom/lge/systemservice/service/LGSDEncService;

    #@72
    const-string v3, "internalStorage"

    #@74
    invoke-virtual {v2, v3}, Lcom/lge/systemservice/service/LGSDEncService;->externalSDCardEnableEncryption(Ljava/lang/String;)I

    #@77
    move-result v2

    #@78
    iput v2, p0, Lcom/lge/systemservice/service/LGSDEncService$LGSDEncServiceThread;->rc:I

    #@7a
    .line 400
    iget v2, p0, Lcom/lge/systemservice/service/LGSDEncService$LGSDEncServiceThread;->rc:I

    #@7c
    if-eqz v2, :cond_98

    #@7e
    .line 401
    const-string v2, "LGSDEncService"

    #@80
    new-instance v3, Ljava/lang/StringBuilder;

    #@82
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@85
    const-string v4, "Failed to set internal encryption rc="

    #@87
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v3

    #@8b
    iget v4, p0, Lcom/lge/systemservice/service/LGSDEncService$LGSDEncServiceThread;->rc:I

    #@8d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@90
    move-result-object v3

    #@91
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v3

    #@95
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@98
    .line 403
    :cond_98
    iget-object v2, p0, Lcom/lge/systemservice/service/LGSDEncService$LGSDEncServiceThread;->this$0:Lcom/lge/systemservice/service/LGSDEncService;

    #@9a
    const-string v3, "internalStorage"

    #@9c
    invoke-virtual {v2, v3}, Lcom/lge/systemservice/service/LGSDEncService;->externalSDCardDisableEncryption(Ljava/lang/String;)I

    #@9f
    move-result v2

    #@a0
    iput v2, p0, Lcom/lge/systemservice/service/LGSDEncService$LGSDEncServiceThread;->rc:I

    #@a2
    .line 408
    :cond_a2
    :goto_a2
    iget v2, p0, Lcom/lge/systemservice/service/LGSDEncService$LGSDEncServiceThread;->rc:I

    #@a4
    if-eqz v2, :cond_c3

    #@a6
    .line 409
    const-string v2, "LGSDEncService"

    #@a8
    new-instance v3, Ljava/lang/StringBuilder;

    #@aa
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@ad
    const-string v4, "Failed to set internal SD encryption setting rc="

    #@af
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b2
    move-result-object v3

    #@b3
    iget v4, p0, Lcom/lge/systemservice/service/LGSDEncService$LGSDEncServiceThread;->rc:I

    #@b5
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v3

    #@b9
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bc
    move-result-object v3

    #@bd
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c0
    .line 410
    const/4 v2, 0x0

    #@c1
    iput v2, p0, Lcom/lge/systemservice/service/LGSDEncService$LGSDEncServiceThread;->rc:I

    #@c3
    .line 413
    :cond_c3
    const-string v2, "persist.sdcrypto.enabled"

    #@c5
    const/4 v3, -0x1

    #@c6
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@c9
    move-result v2

    #@ca
    if-eq v2, v5, :cond_d8

    #@cc
    .line 414
    iget-object v2, p0, Lcom/lge/systemservice/service/LGSDEncService$LGSDEncServiceThread;->this$0:Lcom/lge/systemservice/service/LGSDEncService;

    #@ce
    const-string v3, "mount"

    #@d0
    const-string v4, "externalStorage"

    #@d2
    invoke-virtual {v2, v3, v4}, Lcom/lge/systemservice/service/LGSDEncService;->doCommand(Ljava/lang/String;Ljava/lang/String;)I

    #@d5
    move-result v2

    #@d6
    iput v2, p0, Lcom/lge/systemservice/service/LGSDEncService$LGSDEncServiceThread;->rc:I

    #@d8
    .line 417
    :cond_d8
    iget v2, p0, Lcom/lge/systemservice/service/LGSDEncService$LGSDEncServiceThread;->rc:I

    #@da
    if-eqz v2, :cond_f6

    #@dc
    .line 418
    const-string v2, "LGSDEncService"

    #@de
    new-instance v3, Ljava/lang/StringBuilder;

    #@e0
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@e3
    const-string v4, "Failed to set external SD encryption setting rc="

    #@e5
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e8
    move-result-object v3

    #@e9
    iget v4, p0, Lcom/lge/systemservice/service/LGSDEncService$LGSDEncServiceThread;->rc:I

    #@eb
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ee
    move-result-object v3

    #@ef
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f2
    move-result-object v3

    #@f3
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f6
    .line 424
    .end local v1           #enabled:I
    :cond_f6
    :goto_f6
    return-void

    #@f7
    .line 404
    .restart local v1       #enabled:I
    :cond_f7
    const/4 v2, 0x1

    #@f8
    if-ne v1, v2, :cond_a2

    #@fa
    .line 405
    iget-object v2, p0, Lcom/lge/systemservice/service/LGSDEncService$LGSDEncServiceThread;->this$0:Lcom/lge/systemservice/service/LGSDEncService;

    #@fc
    const-string v3, "internalStorage"

    #@fe
    invoke-virtual {v2, v3}, Lcom/lge/systemservice/service/LGSDEncService;->externalSDCardDisableEncryption(Ljava/lang/String;)I

    #@101
    move-result v2

    #@102
    iput v2, p0, Lcom/lge/systemservice/service/LGSDEncService$LGSDEncServiceThread;->rc:I
    :try_end_104
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_104} :catch_105

    #@104
    goto :goto_a2

    #@105
    .line 421
    .end local v1           #enabled:I
    :catch_105
    move-exception v0

    #@106
    .line 422
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@109
    goto :goto_f6
.end method
