.class Lcom/lge/systemservice/service/CliptrayService$1;
.super Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService$Stub;
.source "CliptrayService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/CliptrayService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/CliptrayService;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/CliptrayService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 123
    iput-object p1, p0, Lcom/lge/systemservice/service/CliptrayService$1;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@2
    invoke-direct {p0}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService$Stub;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public addNewClipData()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 137
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$1;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@2
    iget-object v0, v0, Lcom/lge/systemservice/service/CliptrayService;->handler:Landroid/os/Handler;

    #@4
    const/4 v1, 0x2

    #@5
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@8
    .line 138
    return-void
.end method

.method public doCopyAnimation()V
    .registers 3

    #@0
    .prologue
    .line 226
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$1;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@2
    invoke-static {v0}, Lcom/lge/systemservice/service/CliptrayService;->access$400(Lcom/lge/systemservice/service/CliptrayService;)Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@5
    move-result-object v0

    #@6
    const/4 v1, 0x1

    #@7
    invoke-virtual {v0, v1}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->setDoCopyAnimation(Z)V

    #@a
    .line 227
    return-void
.end method

.method public getClose()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 132
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$1;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@2
    iget-object v0, v0, Lcom/lge/systemservice/service/CliptrayService;->handler:Landroid/os/Handler;

    #@4
    const/4 v1, 0x1

    #@5
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@8
    .line 133
    return-void
.end method

.method public getPeek()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 142
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$1;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@2
    iget-object v0, v0, Lcom/lge/systemservice/service/CliptrayService;->handler:Landroid/os/Handler;

    #@4
    const/4 v1, 0x3

    #@5
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@8
    .line 143
    return-void
.end method

.method public getServiceConnected()Z
    .registers 4

    #@0
    .prologue
    .line 220
    const-string v0, "CliptrayService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "getServiceConnected(): mCliptrayConnected : "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-object v2, p0, Lcom/lge/systemservice/service/CliptrayService$1;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@f
    invoke-static {v2}, Lcom/lge/systemservice/service/CliptrayService;->access$300(Lcom/lge/systemservice/service/CliptrayService;)Z

    #@12
    move-result v2

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 221
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$1;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@20
    invoke-static {v0}, Lcom/lge/systemservice/service/CliptrayService;->access$300(Lcom/lge/systemservice/service/CliptrayService;)Z

    #@23
    move-result v0

    #@24
    return v0
.end method

.method public getShow()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 127
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$1;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@2
    iget-object v0, v0, Lcom/lge/systemservice/service/CliptrayService;->handler:Landroid/os/Handler;

    #@4
    const/4 v1, 0x0

    #@5
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@8
    .line 128
    return-void
.end method

.method public getVisibility()I
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 168
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$1;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@2
    invoke-static {v0}, Lcom/lge/systemservice/service/CliptrayService;->access$100(Lcom/lge/systemservice/service/CliptrayService;)Landroid/view/View;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public hideCliptraycue()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 199
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$1;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@2
    iget-object v0, v0, Lcom/lge/systemservice/service/CliptrayService;->handler:Landroid/os/Handler;

    #@4
    const/16 v1, 0x8

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@9
    .line 200
    return-void
.end method

.method public isCliptraycueShowing()Z
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 204
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$1;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@2
    invoke-static {v0}, Lcom/lge/systemservice/service/CliptrayService;->access$200(Lcom/lge/systemservice/service/CliptrayService;)Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->isShowing()Z

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public orientationChanged(Z)V
    .registers 4
    .parameter "portrait"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 181
    new-instance v0, Landroid/os/Message;

    #@2
    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    #@5
    .line 182
    .local v0, msg:Landroid/os/Message;
    const/4 v1, 0x5

    #@6
    iput v1, v0, Landroid/os/Message;->what:I

    #@8
    .line 183
    if-eqz p1, :cond_15

    #@a
    const/4 v1, 0x0

    #@b
    :goto_b
    iput v1, v0, Landroid/os/Message;->arg1:I

    #@d
    .line 184
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService$1;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@f
    iget-object v1, v1, Lcom/lge/systemservice/service/CliptrayService;->handler:Landroid/os/Handler;

    #@11
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@14
    .line 185
    return-void

    #@15
    .line 183
    :cond_15
    const/4 v1, 0x1

    #@16
    goto :goto_b
.end method

.method public removePasteListener(Lcom/lge/systemservice/core/cliptraymanager/IOnPasteListener;)V
    .registers 4
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 161
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$1;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@2
    invoke-static {v0}, Lcom/lge/systemservice/service/CliptrayService;->access$000(Lcom/lge/systemservice/service/CliptrayService;)Landroid/os/RemoteCallbackList;

    #@5
    move-result-object v1

    #@6
    monitor-enter v1

    #@7
    .line 162
    :try_start_7
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$1;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@9
    invoke-static {v0}, Lcom/lge/systemservice/service/CliptrayService;->access$000(Lcom/lge/systemservice/service/CliptrayService;)Landroid/os/RemoteCallbackList;

    #@c
    move-result-object v0

    #@d
    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    #@10
    .line 163
    monitor-exit v1

    #@11
    .line 164
    return-void

    #@12
    .line 163
    :catchall_12
    move-exception v0

    #@13
    monitor-exit v1
    :try_end_14
    .catchall {:try_start_7 .. :try_end_14} :catchall_12

    #@14
    throw v0
.end method

.method public setInputType(I)V
    .registers 4
    .parameter "type"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 173
    new-instance v0, Landroid/os/Message;

    #@2
    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    #@5
    .line 174
    .local v0, msg:Landroid/os/Message;
    const/4 v1, 0x4

    #@6
    iput v1, v0, Landroid/os/Message;->what:I

    #@8
    .line 175
    iput p1, v0, Landroid/os/Message;->arg1:I

    #@a
    .line 176
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService$1;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@c
    iget-object v1, v1, Lcom/lge/systemservice/service/CliptrayService;->handler:Landroid/os/Handler;

    #@e
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@11
    .line 177
    return-void
.end method

.method public setPasteListener(Lcom/lge/systemservice/core/cliptraymanager/IOnPasteListener;)V
    .registers 7
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 147
    iget-object v3, p0, Lcom/lge/systemservice/service/CliptrayService$1;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@2
    invoke-static {v3}, Lcom/lge/systemservice/service/CliptrayService;->access$000(Lcom/lge/systemservice/service/CliptrayService;)Landroid/os/RemoteCallbackList;

    #@5
    move-result-object v4

    #@6
    monitor-enter v4

    #@7
    .line 148
    :try_start_7
    iget-object v3, p0, Lcom/lge/systemservice/service/CliptrayService$1;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@9
    invoke-static {v3}, Lcom/lge/systemservice/service/CliptrayService;->access$000(Lcom/lge/systemservice/service/CliptrayService;)Landroid/os/RemoteCallbackList;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    #@10
    move-result v1

    #@11
    .line 149
    .local v1, m:I
    const/4 v0, 0x0

    #@12
    .local v0, i:I
    :goto_12
    if-ge v0, v1, :cond_2c

    #@14
    .line 150
    iget-object v3, p0, Lcom/lge/systemservice/service/CliptrayService$1;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@16
    invoke-static {v3}, Lcom/lge/systemservice/service/CliptrayService;->access$000(Lcom/lge/systemservice/service/CliptrayService;)Landroid/os/RemoteCallbackList;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3, v0}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@1d
    move-result-object v2

    #@1e
    check-cast v2, Lcom/lge/systemservice/core/cliptraymanager/IOnPasteListener;

    #@20
    .line 151
    .local v2, prev:Lcom/lge/systemservice/core/cliptraymanager/IOnPasteListener;
    iget-object v3, p0, Lcom/lge/systemservice/service/CliptrayService$1;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@22
    invoke-static {v3}, Lcom/lge/systemservice/service/CliptrayService;->access$000(Lcom/lge/systemservice/service/CliptrayService;)Landroid/os/RemoteCallbackList;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    #@29
    .line 149
    add-int/lit8 v0, v0, 0x1

    #@2b
    goto :goto_12

    #@2c
    .line 153
    .end local v2           #prev:Lcom/lge/systemservice/core/cliptraymanager/IOnPasteListener;
    :cond_2c
    iget-object v3, p0, Lcom/lge/systemservice/service/CliptrayService$1;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@2e
    invoke-static {v3}, Lcom/lge/systemservice/service/CliptrayService;->access$000(Lcom/lge/systemservice/service/CliptrayService;)Landroid/os/RemoteCallbackList;

    #@31
    move-result-object v3

    #@32
    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    #@35
    .line 155
    iget-object v3, p0, Lcom/lge/systemservice/service/CliptrayService$1;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@37
    invoke-static {v3}, Lcom/lge/systemservice/service/CliptrayService;->access$000(Lcom/lge/systemservice/service/CliptrayService;)Landroid/os/RemoteCallbackList;

    #@3a
    move-result-object v3

    #@3b
    invoke-virtual {v3, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    #@3e
    .line 156
    monitor-exit v4

    #@3f
    .line 157
    return-void

    #@40
    .line 156
    .end local v0           #i:I
    .end local v1           #m:I
    :catchall_40
    move-exception v3

    #@41
    monitor-exit v4
    :try_end_42
    .catchall {:try_start_7 .. :try_end_42} :catchall_40

    #@42
    throw v3
.end method

.method public setServiceConnected(Z)V
    .registers 5
    .parameter "mConnect"

    #@0
    .prologue
    .line 213
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$1;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@2
    invoke-static {v0, p1}, Lcom/lge/systemservice/service/CliptrayService;->access$302(Lcom/lge/systemservice/service/CliptrayService;Z)Z

    #@5
    .line 214
    const-string v0, "CliptrayService"

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v2, "setServiceConnected"

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    iget-object v2, p0, Lcom/lge/systemservice/service/CliptrayService$1;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@14
    invoke-static {v2}, Lcom/lge/systemservice/service/CliptrayService;->access$300(Lcom/lge/systemservice/service/CliptrayService;)Z

    #@17
    move-result v2

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 216
    return-void
.end method

.method public showCliptrayCopiedToast()V
    .registers 3

    #@0
    .prologue
    .line 231
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$1;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@2
    iget-object v0, v0, Lcom/lge/systemservice/service/CliptrayService;->handler:Landroid/os/Handler;

    #@4
    const/16 v1, 0xa

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@9
    .line 232
    return-void
.end method

.method public showCliptraycue()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 189
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$1;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@2
    iget-object v0, v0, Lcom/lge/systemservice/service/CliptrayService;->handler:Landroid/os/Handler;

    #@4
    const/4 v1, 0x6

    #@5
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@8
    .line 190
    return-void
.end method

.method public showCliptraycueClose()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 194
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$1;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@2
    iget-object v0, v0, Lcom/lge/systemservice/service/CliptrayService;->handler:Landroid/os/Handler;

    #@4
    const/4 v1, 0x7

    #@5
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@8
    .line 195
    return-void
.end method

.method public showDecodeErrorToast()V
    .registers 3

    #@0
    .prologue
    .line 209
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$1;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@2
    iget-object v0, v0, Lcom/lge/systemservice/service/CliptrayService;->handler:Landroid/os/Handler;

    #@4
    const/16 v1, 0x9

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@9
    .line 210
    return-void
.end method
