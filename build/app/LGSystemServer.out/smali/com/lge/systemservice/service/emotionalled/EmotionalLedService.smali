.class public Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;
.super Lcom/lge/systemservice/core/emotionalledmanager/IEmotionalLedManager$Stub;
.source "EmotionalLedService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$TimeoutTask;,
        Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$SettingsObserver;
    }
.end annotation


# static fields
.field private static final DEBUG:Z


# instance fields
.field private mAutoBrightnessLevels:[I

.field private mBatteryLevel:I

.field private mChargingLedEnabled:Z

.field private mContext:Landroid/content/Context;

.field private mCurrentLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

.field private mCurrentPatternId:I

.field private mDeviceName:Ljava/lang/String;

.field private mEmotionalEnabled:Z

.field private mEmotionalLedType:I

.field private mGpsState:Z

.field private mH:Landroid/os/Handler;

.field private mHandler:Landroid/os/Handler;

.field private mHasBackLed:Z

.field private mHighestLightSensorValue:I

.field private mInCall:Z

.field private mInfinite:Z

.field private mIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mIsShutdown:Z

.field private mLastBatteryLevel:I

.field private mLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

.field private mLedArrayPriority:I

.field private mLedBrightness:I

.field private mLedBrightnessTask:Ljava/lang/Runnable;

.field private mLedEnabled:Z

.field private mLedList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;",
            ">;"
        }
    .end annotation
.end field

.field private mLedOnEvenWhenLcdOn:Z

.field private mLedlightValues:[I

.field mLightListener:Landroid/hardware/SensorEventListener;

.field private mLightSensor:Landroid/hardware/Sensor;

.field private mLightSensorAdjustSetting:F

.field private mLightSensorEnabled:Z

.field private mLightSensorLedBrightness:I

.field private mLightSensorOffDuration:I

.field private mLightSensorOnDuration:I

.field private mLightSensorPendingDecrease:Z

.field private mLightSensorPendingIncrease:Z

.field private mLightSensorPendingValue:F

.field private mLightSensorValue:F

.field private mLightsLock:Ljava/lang/Object;

.field private mNativePointer:I

.field private mNotificationExceptionalPackages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mNotificationIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mNotificationManager:Landroid/app/NotificationManager;

.field private mPowered:Z

.field private mRinging:Z

.field private mScreenOn:Z

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mSmartCoverSetting:Z

.field mSmartcoverMode:I

.field private mSramPattern:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mTempLedList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;",
            ">;"
        }
    .end annotation
.end field

.field private final mTimeoutTask:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$TimeoutTask;

.field private mUseLedAutoBrightness:Z

.field private pattern:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/lge/systemservice/service/emotionalled/RGBFormat;",
            ">;"
        }
    .end annotation
.end field

.field private restart_led_delay:I

.field private rgbPatternPlayerRunner:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 62
    sget-object v0, Landroid/os/Build;->TYPE:Ljava/lang/String;

    #@2
    const-string v1, "user"

    #@4
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_1e

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    sput-boolean v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->DEBUG:Z

    #@d
    .line 1236
    sget-boolean v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->DEBUG:Z

    #@f
    if-eqz v0, :cond_18

    #@11
    .line 1237
    const-string v0, "EmotionalLed"

    #@13
    const-string v1, "Load jni lib..."

    #@15
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1239
    :cond_18
    const-string v0, "emotionalled_jni"

    #@1d
    .line 1240
    return-void

    #@1e
    .line 62
    :cond_1e
    const/4 v0, 0x0

    #@1f
    goto :goto_b
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 10
    .parameter "context"

    #@0
    .prologue
    const/high16 v7, -0x4080

    #@2
    const/4 v6, -0x1

    #@3
    const/4 v5, 0x0

    #@4
    const/4 v4, 0x1

    #@5
    const/4 v3, 0x0

    #@6
    .line 521
    invoke-direct {p0}, Lcom/lge/systemservice/core/emotionalledmanager/IEmotionalLedManager$Stub;-><init>()V

    #@9
    .line 89
    iput-object v5, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mContext:Landroid/content/Context;

    #@b
    .line 90
    iput-object v5, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->pattern:Ljava/util/ArrayList;

    #@d
    .line 93
    new-instance v2, Ljava/util/ArrayList;

    #@f
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@12
    iput-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mSramPattern:Ljava/util/ArrayList;

    #@14
    .line 94
    iput-boolean v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mInfinite:Z

    #@16
    .line 97
    const/4 v2, -0x2

    #@17
    iput v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedArrayPriority:I

    #@19
    .line 98
    new-instance v2, Ljava/util/ArrayList;

    #@1b
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@1e
    iput-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@20
    .line 99
    new-instance v2, Ljava/util/ArrayList;

    #@22
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@25
    iput-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mTempLedList:Ljava/util/ArrayList;

    #@27
    .line 102
    iput-boolean v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mScreenOn:Z

    #@29
    .line 103
    iput-boolean v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mInCall:Z

    #@2b
    .line 104
    iput-boolean v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mRinging:Z

    #@2d
    .line 105
    iput-boolean v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedEnabled:Z

    #@2f
    .line 106
    iput-boolean v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mEmotionalEnabled:Z

    #@31
    .line 107
    iput-boolean v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mSmartCoverSetting:Z

    #@33
    .line 108
    new-instance v2, Ljava/lang/Object;

    #@35
    invoke-direct/range {v2 .. v2}, Ljava/lang/Object;-><init>()V

    #@38
    iput-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightsLock:Ljava/lang/Object;

    #@3a
    .line 109
    iput v6, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mBatteryLevel:I

    #@3c
    .line 110
    iput v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLastBatteryLevel:I

    #@3e
    .line 111
    iput-boolean v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mChargingLedEnabled:Z

    #@40
    .line 112
    iput-boolean v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedOnEvenWhenLcdOn:Z

    #@42
    .line 113
    iput-boolean v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mUseLedAutoBrightness:Z

    #@44
    .line 114
    new-instance v2, Ljava/util/ArrayList;

    #@46
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@49
    iput-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mNotificationIds:Ljava/util/ArrayList;

    #@4b
    .line 118
    iput v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mSmartcoverMode:I

    #@4d
    .line 119
    iput-boolean v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mGpsState:Z

    #@4f
    .line 120
    iput-object v5, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mNotificationExceptionalPackages:Ljava/util/List;

    #@51
    .line 121
    iput-boolean v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mPowered:Z

    #@53
    .line 122
    iput-boolean v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mIsShutdown:Z

    #@55
    .line 126
    iput-boolean v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorEnabled:Z

    #@57
    .line 127
    iput v7, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorValue:F

    #@59
    .line 128
    iput v6, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mHighestLightSensorValue:I

    #@5b
    .line 129
    iput-boolean v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorPendingDecrease:Z

    #@5d
    .line 130
    iput-boolean v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorPendingIncrease:Z

    #@5f
    .line 131
    iput v7, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorPendingValue:F

    #@61
    .line 132
    const/4 v2, 0x0

    #@62
    iput v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorAdjustSetting:F

    #@64
    .line 133
    new-instance v2, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$TimeoutTask;

    #@66
    invoke-direct {v2, p0, v5}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$TimeoutTask;-><init>(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;)V

    #@69
    iput-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mTimeoutTask:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$TimeoutTask;

    #@6b
    .line 134
    const/16 v2, 0x1388

    #@6d
    iput v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorOnDuration:I

    #@6f
    .line 135
    const/16 v2, 0x7530

    #@71
    iput v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorOffDuration:I

    #@73
    .line 138
    iput v6, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorLedBrightness:I

    #@75
    .line 140
    const/16 v2, 0xff

    #@77
    iput v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedBrightness:I

    #@79
    .line 147
    const/16 v2, 0x190

    #@7b
    iput v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->restart_led_delay:I

    #@7d
    .line 149
    iput v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mEmotionalLedType:I

    #@7f
    .line 155
    iput-boolean v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mHasBackLed:Z

    #@81
    .line 238
    new-instance v2, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;

    #@83
    invoke-direct {v2, p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$1;-><init>(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)V

    #@86
    iput-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@88
    .line 1019
    new-instance v2, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$2;

    #@8a
    invoke-direct {v2, p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$2;-><init>(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)V

    #@8d
    iput-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedBrightnessTask:Ljava/lang/Runnable;

    #@8f
    .line 1092
    new-instance v2, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$3;

    #@91
    invoke-direct {v2, p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$3;-><init>(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)V

    #@94
    iput-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightListener:Landroid/hardware/SensorEventListener;

    #@96
    .line 1252
    new-instance v2, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$4;

    #@98
    invoke-direct {v2, p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$4;-><init>(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)V

    #@9b
    iput-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mH:Landroid/os/Handler;

    #@9d
    .line 1324
    new-instance v2, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@9f
    invoke-direct {v2, v4}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;-><init>(I)V

    #@a2
    iput-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->rgbPatternPlayerRunner:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@a4
    .line 522
    iput-object p1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mContext:Landroid/content/Context;

    #@a6
    .line 523
    invoke-static {}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->init_native()I

    #@a9
    move-result v2

    #@aa
    iput v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mNativePointer:I

    #@ac
    .line 524
    iget v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mNativePointer:I

    #@ae
    if-nez v2, :cond_b7

    #@b0
    .line 525
    const-string v2, "EmotionalLed"

    #@b2
    const-string v3, "mNativePointer is null"

    #@b4
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b7
    .line 527
    :cond_b7
    new-instance v2, Landroid/os/Handler;

    #@b9
    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    #@bc
    iput-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mHandler:Landroid/os/Handler;

    #@be
    .line 528
    const-string v2, "notification"

    #@c0
    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@c3
    move-result-object v2

    #@c4
    check-cast v2, Landroid/app/NotificationManager;

    #@c6
    iput-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mNotificationManager:Landroid/app/NotificationManager;

    #@c8
    .line 533
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mContext:Landroid/content/Context;

    #@ca
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@cd
    move-result-object v2

    #@ce
    const v3, 0x2060024

    #@d1
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@d4
    move-result v2

    #@d5
    iput-boolean v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedOnEvenWhenLcdOn:Z

    #@d7
    .line 535
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mContext:Landroid/content/Context;

    #@d9
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@dc
    move-result-object v2

    #@dd
    const v3, 0x2060027

    #@e0
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@e3
    move-result v2

    #@e4
    iput-boolean v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mHasBackLed:Z

    #@e6
    .line 537
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mContext:Landroid/content/Context;

    #@e8
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@eb
    move-result-object v2

    #@ec
    const v3, 0x2060025

    #@ef
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@f2
    move-result v2

    #@f3
    iput-boolean v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mUseLedAutoBrightness:Z

    #@f5
    .line 540
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mContext:Landroid/content/Context;

    #@f7
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@fa
    move-result-object v2

    #@fb
    const v3, 0x2080001

    #@fe
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    #@101
    move-result-object v2

    #@102
    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    #@105
    move-result-object v2

    #@106
    iput-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mNotificationExceptionalPackages:Ljava/util/List;

    #@108
    .line 542
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mContext:Landroid/content/Context;

    #@10a
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@10d
    move-result-object v2

    #@10e
    const v3, 0x1070031

    #@111
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getIntArray(I)[I

    #@114
    move-result-object v2

    #@115
    iput-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mAutoBrightnessLevels:[I

    #@117
    .line 544
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mContext:Landroid/content/Context;

    #@119
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@11c
    move-result-object v2

    #@11d
    const v3, 0x2080002

    #@120
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getIntArray(I)[I

    #@123
    move-result-object v2

    #@124
    iput-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedlightValues:[I

    #@126
    .line 546
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mContext:Landroid/content/Context;

    #@128
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@12b
    move-result-object v2

    #@12c
    const/high16 v3, 0x20b

    #@12e
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    #@131
    move-result v2

    #@132
    iput v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorOnDuration:I

    #@134
    .line 548
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mContext:Landroid/content/Context;

    #@136
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@139
    move-result-object v2

    #@13a
    const v3, 0x20b0001

    #@13d
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    #@140
    move-result v2

    #@141
    iput v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorOffDuration:I

    #@143
    .line 550
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mContext:Landroid/content/Context;

    #@145
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@148
    move-result-object v2

    #@149
    const v3, 0x20b0012

    #@14c
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    #@14f
    move-result v2

    #@150
    iput v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->restart_led_delay:I

    #@152
    .line 552
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mContext:Landroid/content/Context;

    #@154
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@157
    move-result-object v2

    #@158
    const v3, 0x20b0002

    #@15b
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    #@15e
    move-result v2

    #@15f
    iput v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mEmotionalLedType:I

    #@161
    .line 558
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->turnOff()V

    #@164
    .line 561
    new-instance v0, Landroid/content/IntentFilter;

    #@166
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@169
    .line 562
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.SCREEN_ON"

    #@16b
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@16e
    .line 563
    const-string v2, "android.intent.action.SCREEN_OFF"

    #@170
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@173
    .line 564
    const-string v2, "android.intent.action.PHONE_STATE"

    #@175
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@178
    .line 565
    const-string v2, "android.intent.action.USER_PRESENT"

    #@17a
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@17d
    .line 566
    const-string v2, "android.intent.action.BATTERY_CHANGED"

    #@17f
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@182
    .line 567
    const-string v2, "android.intent.action.ACTION_POWER_CONNECTED"

    #@184
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@187
    .line 568
    const-string v2, "android.intent.action.ACTION_POWER_DISCONNECTED"

    #@189
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@18c
    .line 569
    const-string v2, "android.intent.action.ACTION_SHUTDOWN"

    #@18e
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@191
    .line 570
    const-string v2, "com.lge.android.intent.action.ACCESSORY_EVENT"

    #@193
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@196
    .line 571
    const-string v2, "android.media.VOLUME_CHANGED_ACTION"

    #@198
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@19b
    .line 572
    const-string v2, "com.lge.android.intent.action.GNSS_ALERT_LED_CHANGED"

    #@19d
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1a0
    .line 573
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mContext:Landroid/content/Context;

    #@1a2
    iget-object v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@1a4
    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@1a7
    .line 575
    new-instance v1, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$SettingsObserver;

    #@1a9
    invoke-direct {v1, p0, v5}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$SettingsObserver;-><init>(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Landroid/os/Handler;)V

    #@1ac
    .line 576
    .local v1, observer:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$SettingsObserver;
    invoke-virtual {v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$SettingsObserver;->observe()V

    #@1af
    .line 577
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->initEmotionalLedService()V

    #@1b2
    .line 578
    return-void
.end method

.method static synthetic access$100(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1002(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mScreenOn:Z

    #@2
    return p1
.end method

.method static synthetic access$1100(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mHasBackLed:Z

    #@2
    return v0
.end method

.method static synthetic access$1200(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@2
    return-object v0
.end method

.method static synthetic access$1300(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mCurrentLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@2
    return-object v0
.end method

.method static synthetic access$1400(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 60
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->startInternalBackLed()V

    #@3
    return-void
.end method

.method static synthetic access$1500(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->checkToRestartLed()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1600(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 60
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->restartInternalWithBackLed()V

    #@3
    return-void
.end method

.method static synthetic access$1700(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mUseLedAutoBrightness:Z

    #@2
    return v0
.end method

.method static synthetic access$1800(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 60
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->cancelTimerLocked()V

    #@3
    return-void
.end method

.method static synthetic access$1900(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->enableLightSensorLocked(Z)V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method static synthetic access$2000(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 60
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->restartInternal()V

    #@3
    return-void
.end method

.method static synthetic access$2100(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mRinging:Z

    #@2
    return v0
.end method

.method static synthetic access$2102(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mRinging:Z

    #@2
    return p1
.end method

.method static synthetic access$2200(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mPowered:Z

    #@2
    return v0
.end method

.method static synthetic access$2202(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mPowered:Z

    #@2
    return p1
.end method

.method static synthetic access$2300(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mBatteryLevel:I

    #@2
    return v0
.end method

.method static synthetic access$2302(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 60
    iput p1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mBatteryLevel:I

    #@2
    return p1
.end method

.method static synthetic access$2400(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Landroid/content/Intent;)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->getBatteryLevel(Landroid/content/Intent;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$2500(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLastBatteryLevel:I

    #@2
    return v0
.end method

.method static synthetic access$2502(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 60
    iput p1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLastBatteryLevel:I

    #@2
    return p1
.end method

.method static synthetic access$2600(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;JI)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 60
    invoke-direct {p0, p1, p2, p3}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->setTimeoutLocked(JI)V

    #@3
    return-void
.end method

.method static synthetic access$2700(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)[I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mAutoBrightnessLevels:[I

    #@2
    return-object v0
.end method

.method static synthetic access$2800(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;IZ)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->lightSensorChangedLocked(IZ)V

    #@3
    return-void
.end method

.method static synthetic access$2902(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mIsShutdown:Z

    #@2
    return p1
.end method

.method static synthetic access$300()Z
    .registers 1

    #@0
    .prologue
    .line 60
    sget-boolean v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->DEBUG:Z

    #@2
    return v0
.end method

.method static synthetic access$3000(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Ljava/lang/String;IIIIZ)V
    .registers 7
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"
    .parameter "x6"

    #@0
    .prologue
    .line 60
    invoke-direct/range {p0 .. p6}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->startInternalWithRecord(Ljava/lang/String;IIIIZ)V

    #@3
    return-void
.end method

.method static synthetic access$3100(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Ljava/lang/String;IIII)V
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"

    #@0
    .prologue
    .line 60
    invoke-direct/range {p0 .. p5}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->startInternalWithRecord(Ljava/lang/String;IIII)V

    #@3
    return-void
.end method

.method static synthetic access$3200(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mGpsState:Z

    #@2
    return v0
.end method

.method static synthetic access$3202(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mGpsState:Z

    #@2
    return p1
.end method

.method static synthetic access$3300(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Ljava/lang/String;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->stopInternal(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method static synthetic access$3400(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightsLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$3500(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorPendingDecrease:Z

    #@2
    return v0
.end method

.method static synthetic access$3502(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorPendingDecrease:Z

    #@2
    return p1
.end method

.method static synthetic access$3600(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorPendingIncrease:Z

    #@2
    return v0
.end method

.method static synthetic access$3602(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorPendingIncrease:Z

    #@2
    return p1
.end method

.method static synthetic access$3700(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorPendingValue:F

    #@2
    return v0
.end method

.method static synthetic access$3800(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;IZ)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->handleLightSensorValue(IZ)V

    #@3
    return-void
.end method

.method static synthetic access$3900(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 60
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->removeCurLights()V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedEnabled:Z

    #@2
    return v0
.end method

.method static synthetic access$4000(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 60
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->stopPattern()V

    #@3
    return-void
.end method

.method static synthetic access$402(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedEnabled:Z

    #@2
    return p1
.end method

.method static synthetic access$500(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mEmotionalEnabled:Z

    #@2
    return v0
.end method

.method static synthetic access$502(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mEmotionalEnabled:Z

    #@2
    return p1
.end method

.method static synthetic access$600(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mChargingLedEnabled:Z

    #@2
    return v0
.end method

.method static synthetic access$602(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mChargingLedEnabled:Z

    #@2
    return p1
.end method

.method static synthetic access$700(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 60
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->doRestart()V

    #@3
    return-void
.end method

.method static synthetic access$800(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mSmartCoverSetting:Z

    #@2
    return v0
.end method

.method static synthetic access$802(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mSmartCoverSetting:Z

    #@2
    return p1
.end method

.method static synthetic access$900(Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 60
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->updateBatteryChargingLocked()V

    #@3
    return-void
.end method

.method private addSramPattern()V
    .registers 3

    #@0
    .prologue
    .line 1327
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mSramPattern:Ljava/util/ArrayList;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@a
    .line 1328
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mSramPattern:Ljava/util/ArrayList;

    #@c
    const/4 v1, 0x6

    #@d
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@14
    .line 1329
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mSramPattern:Ljava/util/ArrayList;

    #@16
    const/4 v1, 0x3

    #@17
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1e
    .line 1330
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mSramPattern:Ljava/util/ArrayList;

    #@20
    const/4 v1, 0x4

    #@21
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@28
    .line 1331
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mSramPattern:Ljava/util/ArrayList;

    #@2a
    const/4 v1, 0x7

    #@2b
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@32
    .line 1332
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mSramPattern:Ljava/util/ArrayList;

    #@34
    const/16 v1, 0x65

    #@36
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@39
    move-result-object v1

    #@3a
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3d
    .line 1333
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mSramPattern:Ljava/util/ArrayList;

    #@3f
    const/16 v1, 0x66

    #@41
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@44
    move-result-object v1

    #@45
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@48
    .line 1334
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mSramPattern:Ljava/util/ArrayList;

    #@4a
    const/16 v1, 0xe

    #@4c
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4f
    move-result-object v1

    #@50
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@53
    .line 1335
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mSramPattern:Ljava/util/ArrayList;

    #@55
    const/16 v1, 0x12

    #@57
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5a
    move-result-object v1

    #@5b
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5e
    .line 1336
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mSramPattern:Ljava/util/ArrayList;

    #@60
    const/16 v1, 0x11

    #@62
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@65
    move-result-object v1

    #@66
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@69
    .line 1337
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mSramPattern:Ljava/util/ArrayList;

    #@6b
    const/16 v1, 0x14

    #@6d
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@70
    move-result-object v1

    #@71
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@74
    .line 1338
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mSramPattern:Ljava/util/ArrayList;

    #@76
    const/16 v1, 0x13

    #@78
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@7b
    move-result-object v1

    #@7c
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@7f
    .line 1339
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mSramPattern:Ljava/util/ArrayList;

    #@81
    const/16 v1, 0x1d

    #@83
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@86
    move-result-object v1

    #@87
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@8a
    .line 1340
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mSramPattern:Ljava/util/ArrayList;

    #@8c
    const/16 v1, 0x1e

    #@8e
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@91
    move-result-object v1

    #@92
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@95
    .line 1341
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mSramPattern:Ljava/util/ArrayList;

    #@97
    const/16 v1, 0x1f

    #@99
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9c
    move-result-object v1

    #@9d
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@a0
    .line 1342
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mSramPattern:Ljava/util/ArrayList;

    #@a2
    const/16 v1, 0x20

    #@a4
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a7
    move-result-object v1

    #@a8
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@ab
    .line 1343
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mSramPattern:Ljava/util/ArrayList;

    #@ad
    const/16 v1, 0x23

    #@af
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@b2
    move-result-object v1

    #@b3
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@b6
    .line 1344
    return-void
.end method

.method private cancelTimerLocked()V
    .registers 3

    #@0
    .prologue
    .line 1145
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mHandler:Landroid/os/Handler;

    #@2
    if-eqz v0, :cond_11

    #@4
    .line 1146
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mHandler:Landroid/os/Handler;

    #@6
    iget-object v1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mTimeoutTask:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$TimeoutTask;

    #@8
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@b
    .line 1147
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mTimeoutTask:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$TimeoutTask;

    #@d
    const/4 v1, -0x1

    #@e
    iput v1, v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$TimeoutTask;->nextState:I

    #@10
    .line 1151
    :goto_10
    return-void

    #@11
    .line 1149
    :cond_11
    const-string v0, "EmotionalLed"

    #@13
    const-string v1, "can\'t get mHandler"

    #@15
    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    goto :goto_10
.end method

.method private checkToRestartLed()Z
    .registers 6

    #@0
    .prologue
    .line 901
    const/4 v0, 0x1

    #@1
    .line 903
    .local v0, result:Z
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@3
    monitor-enter v2

    #@4
    .line 904
    :try_start_4
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->getCurrentHighestLGLedRecord()V

    #@7
    .line 906
    iget-object v1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@9
    if-eqz v1, :cond_40

    #@b
    iget-object v1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mCurrentLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@d
    if-eqz v1, :cond_40

    #@f
    .line 907
    sget-boolean v1, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->DEBUG:Z

    #@11
    if-eqz v1, :cond_2d

    #@13
    const-string v1, "EmotionalLed"

    #@15
    new-instance v3, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v4, "*mCurrentLed = "

    #@1c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    iget-object v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mCurrentLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@22
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 909
    :cond_2d
    iget-object v1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@2f
    iget-object v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mCurrentLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@31
    invoke-virtual {v1, v3}, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->equals(Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;)Z

    #@34
    move-result v1

    #@35
    if-eqz v1, :cond_40

    #@37
    iget-object v1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@39
    iget v1, v1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->flags:I

    #@3b
    and-int/lit8 v1, v1, 0x2

    #@3d
    if-eqz v1, :cond_40

    #@3f
    .line 910
    const/4 v0, 0x0

    #@40
    .line 913
    :cond_40
    monitor-exit v2
    :try_end_41
    .catchall {:try_start_4 .. :try_end_41} :catchall_5e

    #@41
    .line 915
    sget-boolean v1, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->DEBUG:Z

    #@43
    if-eqz v1, :cond_5d

    #@45
    const-string v1, "EmotionalLed"

    #@47
    new-instance v2, Ljava/lang/StringBuilder;

    #@49
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    const-string v3, "checkToRestartLed() result = "

    #@4e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v2

    #@52
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@55
    move-result-object v2

    #@56
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v2

    #@5a
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    .line 917
    :cond_5d
    return v0

    #@5e
    .line 913
    :catchall_5e
    move-exception v1

    #@5f
    :try_start_5f
    monitor-exit v2
    :try_end_60
    .catchall {:try_start_5f .. :try_end_60} :catchall_5e

    #@60
    throw v1
.end method

.method private clearLedListLocked()V
    .registers 2

    #@0
    .prologue
    .line 462
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->clearLedListLocked(Z)V

    #@4
    .line 463
    return-void
.end method

.method private clearLedListLocked(Z)V
    .registers 10
    .parameter "onlyNative"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 466
    iget-object v5, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@4
    if-eqz v5, :cond_b3

    #@6
    iget-object v5, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mTempLedList:Ljava/util/ArrayList;

    #@8
    if-eqz v5, :cond_b3

    #@a
    .line 467
    const-string v5, "EmotionalLed"

    #@c
    new-instance v6, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v7, "clearLedListLocked:mLedList :"

    #@13
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v6

    #@17
    iget-object v7, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v6

    #@1d
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v6

    #@21
    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 468
    iget-object v5, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mTempLedList:Ljava/util/ArrayList;

    #@26
    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    #@29
    .line 469
    iget-object v5, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@2b
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@2e
    move-result v5

    #@2f
    add-int/lit8 v1, v5, -0x1

    #@31
    .local v1, i:I
    :goto_31
    if-ltz v1, :cond_86

    #@33
    .line 470
    iget-object v5, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@35
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@38
    move-result-object v2

    #@39
    check-cast v2, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@3b
    .line 472
    .local v2, r:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
    if-eqz p1, :cond_5b

    #@3d
    .line 473
    iget-boolean v5, v2, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->mNativeNotification:Z

    #@3f
    if-nez v5, :cond_59

    #@41
    move v0, v3

    #@42
    .line 483
    .local v0, exp:Z
    :goto_42
    if-eqz v0, :cond_49

    #@44
    .line 484
    iget-object v5, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mTempLedList:Ljava/util/ArrayList;

    #@46
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@49
    .line 486
    :cond_49
    iget-boolean v5, v2, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->mExceptional:Z

    #@4b
    if-eqz v5, :cond_56

    #@4d
    .line 487
    iget-object v5, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->rgbPatternPlayerRunner:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@4f
    if-eqz v5, :cond_56

    #@51
    .line 488
    iget-object v5, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->rgbPatternPlayerRunner:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@53
    invoke-virtual {v5}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->stopThread_withHandler()V

    #@56
    .line 469
    :cond_56
    add-int/lit8 v1, v1, -0x1

    #@58
    goto :goto_31

    #@59
    .end local v0           #exp:Z
    :cond_59
    move v0, v4

    #@5a
    .line 473
    goto :goto_42

    #@5b
    .line 475
    :cond_5b
    iget-boolean v5, v2, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->mNativeNotification:Z

    #@5d
    if-nez v5, :cond_84

    #@5f
    iget v5, v2, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@61
    const/4 v6, 0x7

    #@62
    if-eq v5, v6, :cond_84

    #@64
    iget v5, v2, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@66
    const/16 v6, 0xe

    #@68
    if-eq v5, v6, :cond_84

    #@6a
    iget v5, v2, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@6c
    const/16 v6, 0x11

    #@6e
    if-eq v5, v6, :cond_84

    #@70
    iget v5, v2, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@72
    const/16 v6, 0x12

    #@74
    if-eq v5, v6, :cond_84

    #@76
    iget v5, v2, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@78
    const/16 v6, 0x13

    #@7a
    if-eq v5, v6, :cond_84

    #@7c
    iget v5, v2, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@7e
    const/16 v6, 0x14

    #@80
    if-eq v5, v6, :cond_84

    #@82
    move v0, v3

    #@83
    .restart local v0       #exp:Z
    :goto_83
    goto :goto_42

    #@84
    .end local v0           #exp:Z
    :cond_84
    move v0, v4

    #@85
    goto :goto_83

    #@86
    .line 492
    .end local v2           #r:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
    :cond_86
    iget-object v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@88
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    #@8b
    .line 493
    iget-object v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@8d
    iget-object v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mTempLedList:Ljava/util/ArrayList;

    #@8f
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    #@92
    move-result v3

    #@93
    if-nez v3, :cond_b3

    #@95
    .line 494
    const-string v3, "EmotionalLed"

    #@97
    new-instance v4, Ljava/lang/StringBuilder;

    #@99
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@9c
    const-string v5, "failed : temp led list size is"

    #@9e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v4

    #@a2
    iget-object v5, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mTempLedList:Ljava/util/ArrayList;

    #@a4
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@a7
    move-result v5

    #@a8
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v4

    #@ac
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@af
    move-result-object v4

    #@b0
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b3
    .line 497
    .end local v1           #i:I
    :cond_b3
    return-void
.end method

.method private doRestart()V
    .registers 4

    #@0
    .prologue
    .line 658
    iget-object v1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@2
    monitor-enter v1

    #@3
    .line 659
    :try_start_3
    sget-boolean v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->DEBUG:Z

    #@5
    if-eqz v0, :cond_e

    #@7
    const-string v0, "EmotionalLed"

    #@9
    const-string v2, "doRestart()"

    #@b
    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 660
    :cond_e
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->stopPatternFlashing()V

    #@11
    .line 661
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->turnOff()V

    #@14
    .line 662
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->updateLightsLocked()V

    #@17
    .line 663
    monitor-exit v1

    #@18
    .line 664
    return-void

    #@19
    .line 663
    :catchall_19
    move-exception v0

    #@1a
    monitor-exit v1
    :try_end_1b
    .catchall {:try_start_3 .. :try_end_1b} :catchall_19

    #@1b
    throw v0
.end method

.method private enableLightSensorLocked(Z)V
    .registers 9
    .parameter "enable"

    #@0
    .prologue
    .line 1115
    iget-object v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mSensorManager:Landroid/hardware/SensorManager;

    #@2
    if-eqz v3, :cond_32

    #@4
    iget-boolean v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorEnabled:Z

    #@6
    if-eq v3, p1, :cond_32

    #@8
    iget v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mSmartcoverMode:I

    #@a
    const/4 v4, 0x5

    #@b
    if-eq v3, v4, :cond_32

    #@d
    .line 1117
    iput-boolean p1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorEnabled:Z

    #@f
    .line 1119
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@12
    move-result-wide v0

    #@13
    .line 1121
    .local v0, identity:J
    if-eqz p1, :cond_33

    #@15
    .line 1123
    const/4 v3, -0x1

    #@16
    :try_start_16
    iput v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mHighestLightSensorValue:I

    #@18
    .line 1125
    iget v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorValue:F

    #@1a
    float-to-int v2, v3

    #@1b
    .line 1126
    .local v2, value:I
    if-ltz v2, :cond_25

    #@1d
    .line 1127
    const/high16 v3, -0x4080

    #@1f
    iput v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorValue:F

    #@21
    .line 1128
    const/4 v3, 0x1

    #@22
    invoke-direct {p0, v2, v3}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->handleLightSensorValue(IZ)V

    #@25
    .line 1130
    :cond_25
    iget-object v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mSensorManager:Landroid/hardware/SensorManager;

    #@27
    iget-object v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightListener:Landroid/hardware/SensorEventListener;

    #@29
    iget-object v5, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensor:Landroid/hardware/Sensor;

    #@2b
    const/4 v6, 0x2

    #@2c
    invoke-virtual {v3, v4, v5, v6}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z
    :try_end_2f
    .catchall {:try_start_16 .. :try_end_2f} :catchall_48

    #@2f
    .line 1139
    .end local v2           #value:I
    :goto_2f
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@32
    .line 1142
    .end local v0           #identity:J
    :cond_32
    return-void

    #@33
    .line 1133
    .restart local v0       #identity:J
    :cond_33
    :try_start_33
    iget-object v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mSensorManager:Landroid/hardware/SensorManager;

    #@35
    iget-object v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightListener:Landroid/hardware/SensorEventListener;

    #@37
    invoke-virtual {v3, v4}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    #@3a
    .line 1134
    iget-object v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mHandler:Landroid/os/Handler;

    #@3c
    iget-object v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedBrightnessTask:Ljava/lang/Runnable;

    #@3e
    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@41
    .line 1135
    const/4 v3, 0x0

    #@42
    iput-boolean v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorPendingDecrease:Z

    #@44
    .line 1136
    const/4 v3, 0x0

    #@45
    iput-boolean v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorPendingIncrease:Z
    :try_end_47
    .catchall {:try_start_33 .. :try_end_47} :catchall_48

    #@47
    goto :goto_2f

    #@48
    .line 1139
    :catchall_48
    move-exception v3

    #@49
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@4c
    throw v3
.end method

.method private static finalize_native(I)V
	.registers 1
	return-void
.end method

.method private findPriorityLevelLocked()I
    .registers 8

    #@0
    .prologue
    .line 828
    const/4 v2, -0x2

    #@1
    .line 829
    .local v2, priority:I
    iget-boolean v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mScreenOn:Z

    #@3
    if-eqz v4, :cond_20

    #@5
    const/4 v1, 0x3

    #@6
    .line 833
    .local v1, mask:I
    :goto_6
    const/4 v0, 0x0

    #@7
    .local v0, i:I
    :goto_7
    iget-object v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@9
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@c
    move-result v4

    #@d
    if-ge v0, v4, :cond_29

    #@f
    .line 834
    iget-object v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@11
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@14
    move-result-object v3

    #@15
    check-cast v3, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@17
    .line 835
    .local v3, r:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
    invoke-direct {p0, v3, v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->isValid(Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;I)Z

    #@1a
    move-result v4

    #@1b
    if-nez v4, :cond_22

    #@1d
    .line 833
    :cond_1d
    :goto_1d
    add-int/lit8 v0, v0, 0x1

    #@1f
    goto :goto_7

    #@20
    .line 829
    .end local v0           #i:I
    .end local v1           #mask:I
    .end local v3           #r:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
    :cond_20
    const/4 v1, 0x1

    #@21
    goto :goto_6

    #@22
    .line 839
    .restart local v0       #i:I
    .restart local v1       #mask:I
    .restart local v3       #r:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
    :cond_22
    iget v4, v3, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->priority:I

    #@24
    if-le v4, v2, :cond_1d

    #@26
    .line 840
    iget v2, v3, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->priority:I

    #@28
    goto :goto_1d

    #@29
    .line 843
    .end local v3           #r:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
    :cond_29
    sget-boolean v4, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->DEBUG:Z

    #@2b
    if-eqz v4, :cond_45

    #@2d
    .line 844
    const-string v4, "EmotionalLed"

    #@2f
    new-instance v5, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v6, "findPriorityLevelLocked priority : "

    #@36
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v5

    #@3a
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v5

    #@3e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v5

    #@42
    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 846
    :cond_45
    return v2
.end method

.method private findSameSourceLocked(Ljava/lang/String;I)Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
    .registers 6
    .parameter "pkg"
    .parameter "recordId"

    #@0
    .prologue
    .line 684
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v2

    #@7
    if-ge v0, v2, :cond_21

    #@9
    .line 685
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@11
    .line 686
    .local v1, r:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
    iget-object v2, v1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->pkgName:Ljava/lang/String;

    #@13
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16
    move-result v2

    #@17
    if-eqz v2, :cond_1e

    #@19
    iget v2, v1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->recordId:I

    #@1b
    if-ne v2, p2, :cond_1e

    #@1d
    .line 690
    .end local v1           #r:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
    :goto_1d
    return-object v1

    #@1e
    .line 684
    .restart local v1       #r:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
    :cond_1e
    add-int/lit8 v0, v0, 0x1

    #@20
    goto :goto_1

    #@21
    .line 690
    .end local v1           #r:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
    :cond_21
    const/4 v1, 0x0

    #@22
    goto :goto_1d
.end method

.method private getAutoBrightnessValue(I[I)I
    .registers 6
    .parameter
    .parameter

    #@0
    .prologue
    .line 1035
    const/4 v0, 0x0

    #@1
    :goto_1
    :try_start_1
    iget-object v1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mAutoBrightnessLevels:[I

    #@3
    array-length v1, v1

    #@4
    if-ge v0, v1, :cond_c

    #@6
    .line 1036
    iget-object v1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mAutoBrightnessLevels:[I

    #@8
    aget v1, v1, v0

    #@a
    if-ge p1, v1, :cond_f

    #@c
    .line 1040
    :cond_c
    aget v0, p2, v0
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_e} :catch_12

    #@e
    .line 1046
    :goto_e
    return v0

    #@f
    .line 1035
    :cond_f
    add-int/lit8 v0, v0, 0x1

    #@11
    goto :goto_1

    #@12
    .line 1042
    :catch_12
    move-exception v0

    #@13
    .line 1044
    const-string v1, "EmotionalLed"

    #@15
    const-string v2, "Values array must be non-empty and must be one element longer than the auto-brightness levels array.  Check config.xml."

    #@17
    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1a
    .line 1046
    const/16 v0, 0xff

    #@1c
    goto :goto_e
.end method

.method private getBatteryLevel(Landroid/content/Intent;)I
    .registers 8
    .parameter "intent"

    #@0
    .prologue
    const/4 v4, -0x1

    #@1
    .line 581
    const-string v3, "level"

    #@3
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@6
    move-result v1

    #@7
    .line 582
    .local v1, rawlevel:I
    const-string v3, "scale"

    #@9
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@c
    move-result v2

    #@d
    .line 583
    .local v2, scale:I
    const/4 v0, -0x1

    #@e
    .line 584
    .local v0, level:I
    if-ltz v1, :cond_16

    #@10
    if-lez v2, :cond_16

    #@12
    .line 585
    mul-int/lit8 v3, v1, 0x64

    #@14
    div-int v0, v3, v2

    #@16
    .line 587
    :cond_16
    sget-boolean v3, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->DEBUG:Z

    #@18
    if-eqz v3, :cond_38

    #@1a
    .line 588
    const-string v3, "EmotionalLed"

    #@1c
    new-instance v4, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v5, "Battery Level Remaining: "

    #@23
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v4

    #@27
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v4

    #@2b
    const-string v5, "%"

    #@2d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v4

    #@31
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v4

    #@35
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 590
    :cond_38
    return v0
.end method

.method private getCurrentHighestLGLedRecord()V
    .registers 6

    #@0
    .prologue
    .line 885
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v1

    #@6
    .line 886
    .local v1, n:I
    const-string v2, "EmotionalLed"

    #@8
    new-instance v3, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v4, "getCurrentHighestLGLedRecord() start. mLedList.size="

    #@f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 888
    sget-boolean v2, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->DEBUG:Z

    #@20
    if-eqz v2, :cond_4b

    #@22
    .line 889
    add-int/lit8 v0, v1, -0x1

    #@24
    .local v0, i:I
    :goto_24
    if-ltz v0, :cond_4b

    #@26
    .line 890
    const-string v2, "EmotionalLed"

    #@28
    new-instance v3, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    const-string v4, " : "

    #@33
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    iget-object v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@39
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@3c
    move-result-object v4

    #@3d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v3

    #@41
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v3

    #@45
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 889
    add-int/lit8 v0, v0, -0x1

    #@4a
    goto :goto_24

    #@4b
    .line 894
    .end local v0           #i:I
    :cond_4b
    const/4 v2, 0x0

    #@4c
    iput-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@4e
    .line 895
    if-lez v1, :cond_56

    #@50
    .line 896
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->getHighestPriorityRecordLocked()Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@53
    move-result-object v2

    #@54
    iput-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@56
    .line 898
    :cond_56
    return-void
.end method

.method private getHighestPriorityRecordLocked()Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
    .registers 8

    #@0
    .prologue
    .line 850
    sget-boolean v4, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->DEBUG:Z

    #@2
    if-eqz v4, :cond_b

    #@4
    const-string v4, "EmotionalLed"

    #@6
    const-string v5, "getHighestPriorityRecordLocked"

    #@8
    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 852
    :cond_b
    const/4 v3, 0x0

    #@c
    .line 853
    .local v3, result:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->findPriorityLevelLocked()I

    #@f
    move-result v4

    #@10
    iput v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedArrayPriority:I

    #@12
    .line 854
    iget-boolean v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mScreenOn:Z

    #@14
    if-eqz v4, :cond_80

    #@16
    const/4 v1, 0x3

    #@17
    .line 858
    .local v1, mask:I
    :goto_17
    iget-object v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@1c
    move-result v4

    #@1d
    add-int/lit8 v0, v4, -0x1

    #@1f
    .local v0, i:I
    :goto_1f
    if-ltz v0, :cond_8f

    #@21
    .line 859
    iget-object v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@23
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@26
    move-result-object v2

    #@27
    check-cast v2, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@29
    .line 860
    .local v2, r:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
    sget-boolean v4, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->DEBUG:Z

    #@2b
    if-eqz v4, :cond_77

    #@2d
    .line 861
    const-string v4, "EmotionalLed"

    #@2f
    new-instance v5, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v6, "i : "

    #@36
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v5

    #@3a
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v5

    #@3e
    const-string v6, " r="

    #@40
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v5

    #@44
    iget-object v6, v2, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->pkgName:Ljava/lang/String;

    #@46
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v5

    #@4a
    const-string v6, ", flag="

    #@4c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v5

    #@50
    iget v6, v2, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->flags:I

    #@52
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@55
    move-result-object v5

    #@56
    const-string v6, ", priority="

    #@58
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v5

    #@5c
    iget v6, v2, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->priority:I

    #@5e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@61
    move-result-object v5

    #@62
    const-string v6, ", isValid="

    #@64
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v5

    #@68
    invoke-direct {p0, v2, v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->isValid(Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;I)Z

    #@6b
    move-result v6

    #@6c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v5

    #@70
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@73
    move-result-object v5

    #@74
    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@77
    .line 864
    :cond_77
    invoke-direct {p0, v2, v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->isValid(Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;I)Z

    #@7a
    move-result v4

    #@7b
    if-nez v4, :cond_82

    #@7d
    .line 858
    :cond_7d
    add-int/lit8 v0, v0, -0x1

    #@7f
    goto :goto_1f

    #@80
    .line 854
    .end local v0           #i:I
    .end local v1           #mask:I
    .end local v2           #r:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
    :cond_80
    const/4 v1, 0x1

    #@81
    goto :goto_17

    #@82
    .line 867
    .restart local v0       #i:I
    .restart local v1       #mask:I
    .restart local v2       #r:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
    :cond_82
    if-eqz v2, :cond_7d

    #@84
    iget v4, v2, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->priority:I

    #@86
    iget v5, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedArrayPriority:I

    #@88
    if-lt v4, v5, :cond_7d

    #@8a
    .line 868
    iget v4, v2, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->priority:I

    #@8c
    iput v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedArrayPriority:I

    #@8e
    .line 869
    move-object v3, v2

    #@8f
    .line 873
    .end local v2           #r:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
    :cond_8f
    sget-boolean v4, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->DEBUG:Z

    #@91
    if-eqz v4, :cond_b9

    #@93
    .line 874
    if-eqz v3, :cond_ba

    #@95
    .line 875
    const-string v4, "EmotionalLed"

    #@97
    new-instance v5, Ljava/lang/StringBuilder;

    #@99
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@9c
    const-string v6, "*result="

    #@9e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v5

    #@a2
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v5

    #@a6
    const-string v6, ", mLedArrayPriority="

    #@a8
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v5

    #@ac
    iget v6, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedArrayPriority:I

    #@ae
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v5

    #@b2
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b5
    move-result-object v5

    #@b6
    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b9
    .line 881
    :cond_b9
    :goto_b9
    return-object v3

    #@ba
    .line 877
    :cond_ba
    const-string v4, "EmotionalLed"

    #@bc
    const-string v5, "result is null"

    #@be
    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@c1
    goto :goto_b9
.end method

.method private handleLightSensorValue(IZ)V
    .registers 7
    .parameter "value"
    .parameter "immediate"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 1068
    iget v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorValue:F

    #@4
    const/high16 v3, -0x4080

    #@6
    cmpl-float v0, v0, v3

    #@8
    if-nez v0, :cond_19

    #@a
    .line 1069
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mHandler:Landroid/os/Handler;

    #@c
    iget-object v1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedBrightnessTask:Ljava/lang/Runnable;

    #@e
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@11
    .line 1070
    iput-boolean v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorPendingDecrease:Z

    #@13
    .line 1071
    iput-boolean v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorPendingIncrease:Z

    #@15
    .line 1072
    invoke-direct {p0, p1, p2}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->lightSensorChangedLocked(IZ)V

    #@18
    .line 1090
    :cond_18
    :goto_18
    return-void

    #@19
    .line 1074
    :cond_19
    int-to-float v0, p1

    #@1a
    iget v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorValue:F

    #@1c
    cmpl-float v0, v0, v3

    #@1e
    if-lez v0, :cond_24

    #@20
    iget-boolean v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorPendingDecrease:Z

    #@22
    if-nez v0, :cond_3e

    #@24
    :cond_24
    int-to-float v0, p1

    #@25
    iget v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorValue:F

    #@27
    cmpg-float v0, v0, v3

    #@29
    if-gez v0, :cond_2f

    #@2b
    iget-boolean v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorPendingIncrease:Z

    #@2d
    if-nez v0, :cond_3e

    #@2f
    :cond_2f
    int-to-float v0, p1

    #@30
    iget v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorValue:F

    #@32
    cmpl-float v0, v0, v3

    #@34
    if-eqz v0, :cond_3e

    #@36
    iget-boolean v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorPendingDecrease:Z

    #@38
    if-nez v0, :cond_71

    #@3a
    iget-boolean v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorPendingIncrease:Z

    #@3c
    if-nez v0, :cond_71

    #@3e
    .line 1079
    :cond_3e
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mHandler:Landroid/os/Handler;

    #@40
    iget-object v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedBrightnessTask:Ljava/lang/Runnable;

    #@42
    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@45
    .line 1080
    int-to-float v0, p1

    #@46
    iget v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorValue:F

    #@48
    cmpg-float v0, v0, v3

    #@4a
    if-gez v0, :cond_6d

    #@4c
    move v0, v1

    #@4d
    :goto_4d
    iput-boolean v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorPendingDecrease:Z

    #@4f
    .line 1081
    int-to-float v0, p1

    #@50
    iget v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorValue:F

    #@52
    cmpl-float v0, v0, v3

    #@54
    if-lez v0, :cond_6f

    #@56
    :goto_56
    iput-boolean v1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorPendingIncrease:Z

    #@58
    .line 1082
    iget-boolean v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorPendingDecrease:Z

    #@5a
    if-nez v0, :cond_60

    #@5c
    iget-boolean v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorPendingIncrease:Z

    #@5e
    if-eqz v0, :cond_18

    #@60
    .line 1083
    :cond_60
    int-to-float v0, p1

    #@61
    iput v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorPendingValue:F

    #@63
    .line 1084
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mHandler:Landroid/os/Handler;

    #@65
    iget-object v1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedBrightnessTask:Ljava/lang/Runnable;

    #@67
    const-wide/16 v2, 0xfa

    #@69
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@6c
    goto :goto_18

    #@6d
    :cond_6d
    move v0, v2

    #@6e
    .line 1080
    goto :goto_4d

    #@6f
    :cond_6f
    move v1, v2

    #@70
    .line 1081
    goto :goto_56

    #@71
    .line 1087
    :cond_71
    int-to-float v0, p1

    #@72
    iput v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorPendingValue:F

    #@74
    goto :goto_18
.end method

.method private initEmotionalLedService()V
    .registers 4

    #@0
    .prologue
    .line 1347
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->addSramPattern()V

    #@3
    .line 1348
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->rgbPatternPlayerRunner:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@5
    iget-object v0, v0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->mRGBDevice:Lcom/lge/systemservice/service/emotionalled/RGBDevice;

    #@7
    const/4 v1, 0x0

    #@8
    invoke-interface {v0, p0, v1}, Lcom/lge/systemservice/service/emotionalled/RGBDevice;->onDeviceInit(Ljava/lang/Object;Landroid/view/View;)V

    #@b
    .line 1349
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->rgbPatternPlayerRunner:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@d
    iget-boolean v1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mHasBackLed:Z

    #@f
    invoke-virtual {v0, v1}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->setBackLed(Z)V

    #@12
    .line 1350
    const-string v0, "ro.product.device"

    #@14
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@17
    move-result-object v0

    #@18
    iput-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mDeviceName:Ljava/lang/String;

    #@1a
    .line 1353
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->doRestart()V

    #@1d
    .line 1356
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mContext:Landroid/content/Context;

    #@1f
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@22
    move-result-object v0

    #@23
    .line 1357
    const-string v1, "led_brightness"

    #@25
    const/16 v2, 0xff

    #@27
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@2a
    move-result v0

    #@2b
    iput v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedBrightness:I

    #@2d
    .line 1358
    iget v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedBrightness:I

    #@2f
    invoke-direct {p0, v0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->setBrightnessInternal(I)V

    #@32
    .line 1360
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mContext:Landroid/content/Context;

    #@34
    const-string v1, "sensor"

    #@36
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@39
    move-result-object v0

    #@3a
    check-cast v0, Landroid/hardware/SensorManager;

    #@3c
    iput-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mSensorManager:Landroid/hardware/SensorManager;

    #@3e
    .line 1361
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mSensorManager:Landroid/hardware/SensorManager;

    #@40
    if-eqz v0, :cond_4c

    #@42
    .line 1362
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mSensorManager:Landroid/hardware/SensorManager;

    #@44
    const/4 v1, 0x5

    #@45
    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    #@48
    move-result-object v0

    #@49
    iput-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensor:Landroid/hardware/Sensor;

    #@4b
    .line 1366
    :goto_4b
    return-void

    #@4c
    .line 1364
    :cond_4c
    const-string v0, "EmotionalLed"

    #@4e
    const-string v1, "can\'t get SensorManager!"

    #@50
    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    goto :goto_4b
.end method

.method private static init_native()I
	.registers 1
	const/4 v0, 0x1
	return v0
.end method

.method private isSRAMPattern(I)Z
    .registers 4
    .parameter "patternId"

    #@0
    .prologue
    .line 1321
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mSramPattern:Ljava/util/ArrayList;

    #@2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    return v0
.end method

.method private isValid(Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;I)Z
    .registers 5
    .parameter "r"
    .parameter "mask"

    #@0
    .prologue
    .line 799
    const/4 v0, 0x0

    #@1
    .line 800
    .local v0, result:Z
    iget-boolean v1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mScreenOn:Z

    #@3
    if-eqz v1, :cond_a

    #@5
    iget v1, p1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->flags:I

    #@7
    and-int/2addr v1, p2

    #@8
    if-nez v1, :cond_13

    #@a
    :cond_a
    iget-boolean v1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mScreenOn:Z

    #@c
    if-nez v1, :cond_24

    #@e
    iget v1, p1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->flags:I

    #@10
    and-int/2addr v1, p2

    #@11
    if-nez v1, :cond_24

    #@13
    .line 802
    :cond_13
    iget-boolean v1, p1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->mNativeNotification:Z

    #@15
    if-eqz v1, :cond_1b

    #@17
    iget-boolean v1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedEnabled:Z

    #@19
    if-nez v1, :cond_23

    #@1b
    :cond_1b
    iget-boolean v1, p1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->mNativeNotification:Z

    #@1d
    if-nez v1, :cond_24

    #@1f
    iget-boolean v1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mEmotionalEnabled:Z

    #@21
    if-eqz v1, :cond_24

    #@23
    .line 804
    :cond_23
    const/4 v0, 0x1

    #@24
    .line 807
    :cond_24
    iget-boolean v1, p1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->mExceptional:Z

    #@26
    if-eqz v1, :cond_29

    #@28
    .line 808
    const/4 v0, 0x1

    #@29
    .line 810
    :cond_29
    return v0
.end method

.method private lightSensorChangedLocked(IZ)V
    .registers 5
    .parameter "value"
    .parameter "immediate"

    #@0
    .prologue
    .line 1051
    iget v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorValue:F

    #@2
    int-to-float v1, p1

    #@3
    cmpl-float v0, v0, v1

    #@5
    if-eqz v0, :cond_21

    #@7
    .line 1052
    int-to-float v0, p1

    #@8
    iput v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorValue:F

    #@a
    .line 1053
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedlightValues:[I

    #@c
    invoke-direct {p0, p1, v0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->getAutoBrightnessValue(I[I)I

    #@f
    move-result v0

    #@10
    iput v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorLedBrightness:I

    #@12
    .line 1057
    iget v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedBrightness:I

    #@14
    iget v1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorLedBrightness:I

    #@16
    if-ge v0, v1, :cond_1c

    #@18
    .line 1058
    iget v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedBrightness:I

    #@1a
    iput v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorLedBrightness:I

    #@1c
    .line 1060
    :cond_1c
    iget v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorLedBrightness:I

    #@1e
    invoke-direct {p0, v0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->setLightCurrent(I)V

    #@21
    .line 1062
    :cond_21
    return-void
.end method

.method private loadFile(Ljava/lang/String;)Ljava/lang/String;
    .registers 8
    .parameter

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1548
    .line 1549
    new-instance v3, Ljava/lang/StringBuilder;

    #@3
    const/16 v1, 0x80

    #@5
    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@8
    .line 1551
    :try_start_8
    new-instance v2, Ljava/io/BufferedReader;

    #@a
    new-instance v1, Ljava/io/InputStreamReader;

    #@c
    iget-object v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mContext:Landroid/content/Context;

    #@e
    invoke-virtual {v4}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    #@11
    move-result-object v4

    #@12
    invoke-virtual {v4, p1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    #@15
    move-result-object v4

    #@16
    invoke-direct {v1, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    #@19
    invoke-direct {v2, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1c
    .catchall {:try_start_8 .. :try_end_1c} :catchall_5f
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_1c} :catch_76

    #@1c
    .line 1555
    :goto_1c
    :try_start_1c
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    if-eqz v1, :cond_41

    #@22
    .line 1556
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_25
    .catchall {:try_start_1c .. :try_end_25} :catchall_74
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_25} :catch_26

    #@25
    goto :goto_1c

    #@26
    .line 1559
    :catch_26
    move-exception v1

    #@27
    .line 1560
    :goto_27
    :try_start_27
    const-string v4, "EmotionalLed"

    #@29
    const-string v5, "File IOException!"

    #@2b
    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 1561
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_31
    .catchall {:try_start_27 .. :try_end_31} :catchall_74

    #@31
    .line 1564
    if-eqz v2, :cond_36

    #@33
    .line 1565
    :try_start_33
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_36
    .catch Ljava/io/IOException; {:try_start_33 .. :try_end_36} :catch_53

    #@36
    .line 1573
    :cond_36
    :goto_36
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    #@39
    move-result v1

    #@3a
    if-lez v1, :cond_40

    #@3c
    .line 1574
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v0

    #@40
    .line 1576
    :cond_40
    return-object v0

    #@41
    .line 1564
    :cond_41
    if-eqz v2, :cond_36

    #@43
    .line 1565
    :try_start_43
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_46
    .catch Ljava/io/IOException; {:try_start_43 .. :try_end_46} :catch_47

    #@46
    goto :goto_36

    #@47
    .line 1567
    :catch_47
    move-exception v1

    #@48
    .line 1568
    const-string v2, "EmotionalLed"

    #@4a
    const-string v4, "File IOException!"

    #@4c
    invoke-static {v2, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 1569
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@52
    goto :goto_36

    #@53
    .line 1567
    :catch_53
    move-exception v1

    #@54
    .line 1568
    const-string v2, "EmotionalLed"

    #@56
    const-string v4, "File IOException!"

    #@58
    invoke-static {v2, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5b
    .line 1569
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@5e
    goto :goto_36

    #@5f
    .line 1563
    :catchall_5f
    move-exception v1

    #@60
    move-object v2, v0

    #@61
    move-object v0, v1

    #@62
    .line 1564
    :goto_62
    if-eqz v2, :cond_67

    #@64
    .line 1565
    :try_start_64
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_67
    .catch Ljava/io/IOException; {:try_start_64 .. :try_end_67} :catch_68

    #@67
    .line 1570
    :cond_67
    :goto_67
    throw v0

    #@68
    .line 1567
    :catch_68
    move-exception v1

    #@69
    .line 1568
    const-string v2, "EmotionalLed"

    #@6b
    const-string v3, "File IOException!"

    #@6d
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@70
    .line 1569
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@73
    goto :goto_67

    #@74
    .line 1563
    :catchall_74
    move-exception v0

    #@75
    goto :goto_62

    #@76
    .line 1559
    :catch_76
    move-exception v1

    #@77
    move-object v2, v0

    #@78
    goto :goto_27
.end method

.method private makeCustomPattern(Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 9
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/lge/systemservice/service/emotionalled/RGBFormat;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1521
    if-nez p1, :cond_4

    #@2
    .line 1522
    const/4 v0, 0x0

    #@3
    .line 1544
    :cond_3
    return-object v0

    #@4
    .line 1524
    :cond_4
    new-instance v0, Ljava/util/ArrayList;

    #@6
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@9
    .line 1525
    new-instance v1, Ljava/util/StringTokenizer;

    #@b
    const-string v2, ","

    #@d
    invoke-direct {v1, p1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@10
    .line 1528
    :goto_10
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->countTokens()I

    #@13
    move-result v2

    #@14
    const/16 v3, 0xa

    #@16
    if-lt v2, v3, :cond_3

    #@18
    .line 1529
    new-instance v2, Lcom/lge/systemservice/service/emotionalled/RGBFormat;

    #@1a
    invoke-direct {v2}, Lcom/lge/systemservice/service/emotionalled/RGBFormat;-><init>()V

    #@1d
    .line 1530
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@28
    move-result v3

    #@29
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@30
    move-result-object v4

    #@31
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@34
    move-result v4

    #@35
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@38
    move-result-object v5

    #@39
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@3c
    move-result-object v5

    #@3d
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@40
    move-result v5

    #@41
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@44
    move-result-object v6

    #@45
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@48
    move-result-object v6

    #@49
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@4c
    move-result v6

    #@4d
    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/lge/systemservice/service/emotionalled/RGBFormat;->setCurrentColorARGB(IIII)V

    #@50
    .line 1535
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@53
    move-result-object v3

    #@54
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@57
    move-result-object v3

    #@58
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@5b
    move-result v3

    #@5c
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@5f
    move-result-object v4

    #@60
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@63
    move-result-object v4

    #@64
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@67
    move-result v4

    #@68
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@6b
    move-result-object v5

    #@6c
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@6f
    move-result-object v5

    #@70
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@73
    move-result v5

    #@74
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@77
    move-result-object v6

    #@78
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@7b
    move-result-object v6

    #@7c
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@7f
    move-result v6

    #@80
    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/lge/systemservice/service/emotionalled/RGBFormat;->setTargetColorARGB(IIII)V

    #@83
    .line 1540
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@86
    move-result-object v3

    #@87
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@8a
    move-result-object v3

    #@8b
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@8e
    move-result v3

    #@8f
    invoke-virtual {v2, v3}, Lcom/lge/systemservice/service/emotionalled/RGBFormat;->setDuration(I)V

    #@92
    .line 1541
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@95
    move-result-object v3

    #@96
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@99
    move-result-object v3

    #@9a
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@9d
    move-result v3

    #@9e
    invoke-virtual {v2, v3}, Lcom/lge/systemservice/service/emotionalled/RGBFormat;->setInterval(I)V

    #@a1
    .line 1542
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@a4
    goto/16 :goto_10
.end method

.method private parsePattern(I)Ljava/util/ArrayList;
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/lge/systemservice/service/emotionalled/RGBFormat;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1369
    sget-boolean v1, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->DEBUG:Z

    #@3
    if-eqz v1, :cond_1d

    #@5
    .line 1370
    const-string v1, "EmotionalLed"

    #@7
    new-instance v2, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v3, "parsePattern id : "

    #@e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v2

    #@1a
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 1375
    :cond_1d
    packed-switch p1, :pswitch_data_94

    #@20
    .line 1453
    :pswitch_20
    const-string v1, "EmotionalLed"

    #@22
    const-string v2, "parsePattern()::Unknown Pattern ID"

    #@24
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    :cond_27
    move-object v1, v0

    #@28
    .line 1456
    :goto_28
    if-eqz v1, :cond_91

    #@2a
    .line 1457
    invoke-direct {p0, v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->loadFile(Ljava/lang/String;)Ljava/lang/String;

    #@2d
    move-result-object v1

    #@2e
    .line 1459
    :goto_2e
    if-eqz v1, :cond_34

    #@30
    .line 1460
    invoke-direct {p0, v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->makeCustomPattern(Ljava/lang/String;)Ljava/util/ArrayList;

    #@33
    move-result-object v0

    #@34
    .line 1462
    :cond_34
    return-object v0

    #@35
    .line 1377
    :pswitch_35
    const-string v1, "emotionalled/Power_on_animation.txt"

    #@37
    goto :goto_28

    #@38
    .line 1380
    :pswitch_38
    const-string v1, "emotionalled/LCD_on.txt"

    #@3a
    goto :goto_28

    #@3b
    .line 1383
    :pswitch_3b
    const-string v1, "emotionalled/Charging.txt"

    #@3d
    goto :goto_28

    #@3e
    .line 1386
    :pswitch_3e
    const-string v1, "emotionalled/ChargingFull.txt"

    #@40
    goto :goto_28

    #@41
    .line 1389
    :pswitch_41
    const-string v1, "emotionalled/Power_off_animation.txt"

    #@43
    goto :goto_28

    #@44
    .line 1392
    :pswitch_44
    const-string v1, "emotionalled/Missed_Noti.txt"

    #@46
    goto :goto_28

    #@47
    .line 1395
    :pswitch_47
    const-string v1, "emotionalled/Alarm.txt"

    #@49
    goto :goto_28

    #@4a
    .line 1398
    :pswitch_4a
    const-string v1, "emotionalled/Call_incoming_call01.txt"

    #@4c
    goto :goto_28

    #@4d
    .line 1401
    :pswitch_4d
    const-string v1, "emotionalled/Call_incoming_call02.txt"

    #@4f
    goto :goto_28

    #@50
    .line 1404
    :pswitch_50
    const-string v1, "emotionalled/Call_incoming_call03.txt"

    #@52
    goto :goto_28

    #@53
    .line 1407
    :pswitch_53
    const-string v1, "emotionalled/Calendar_Reminder.txt"

    #@55
    goto :goto_28

    #@56
    .line 1410
    :pswitch_56
    const-string v1, "emotionalled/VolumeUp.txt"

    #@58
    goto :goto_28

    #@59
    .line 1413
    :pswitch_59
    const-string v1, "emotionalled/VolumeDown.txt"

    #@5b
    goto :goto_28

    #@5c
    .line 1416
    :pswitch_5c
    const-string v1, "emotionalled/Call_incoming_call_pink.txt"

    #@5e
    goto :goto_28

    #@5f
    .line 1419
    :pswitch_5f
    const-string v1, "emotionalled/Call_incoming_call_blue.txt"

    #@61
    goto :goto_28

    #@62
    .line 1422
    :pswitch_62
    const-string v1, "emotionalled/Call_incoming_call_orange.txt"

    #@64
    goto :goto_28

    #@65
    .line 1425
    :pswitch_65
    const-string v1, "emotionalled/Call_incoming_call_yellow.txt"

    #@67
    goto :goto_28

    #@68
    .line 1428
    :pswitch_68
    const-string v1, "emotionalled/Call_incoming_call_turquoise.txt"

    #@6a
    goto :goto_28

    #@6b
    .line 1431
    :pswitch_6b
    const-string v1, "emotionalled/Call_incoming_call_purple.txt"

    #@6d
    goto :goto_28

    #@6e
    .line 1434
    :pswitch_6e
    const-string v1, "emotionalled/Call_incoming_call_red.txt"

    #@70
    goto :goto_28

    #@71
    .line 1437
    :pswitch_71
    const-string v1, "emotionalled/Call_incoming_call_lime.txt"

    #@73
    goto :goto_28

    #@74
    .line 1440
    :pswitch_74
    iget-object v1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mDeviceName:Ljava/lang/String;

    #@76
    const-string v2, "g2"

    #@78
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7b
    move-result v1

    #@7c
    if-nez v1, :cond_27

    #@7e
    .line 1441
    const-string v1, "emotionalled/TangibleConnect.txt"

    #@80
    goto :goto_28

    #@81
    .line 1445
    :pswitch_81
    iget-object v1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mDeviceName:Ljava/lang/String;

    #@83
    const-string v2, "g2"

    #@85
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@88
    move-result v1

    #@89
    if-nez v1, :cond_27

    #@8b
    .line 1446
    const-string v1, "emotionalled/SoundRecording.txt"

    #@8d
    goto :goto_28

    #@8e
    .line 1450
    :pswitch_8e
    const-string v1, "emotionalled/Calling.txt"

    #@90
    goto :goto_28

    #@91
    :cond_91
    move-object v1, v0

    #@92
    goto :goto_2e

    #@93
    .line 1375
    nop

    #@94
    :pswitch_data_94
    .packed-switch 0x1
        :pswitch_35
        :pswitch_38
        :pswitch_3b
        :pswitch_3e
        :pswitch_53
        :pswitch_41
        :pswitch_44
        :pswitch_47
        :pswitch_4a
        :pswitch_4d
        :pswitch_50
        :pswitch_56
        :pswitch_59
        :pswitch_20
        :pswitch_20
        :pswitch_20
        :pswitch_20
        :pswitch_20
        :pswitch_20
        :pswitch_20
        :pswitch_5c
        :pswitch_5f
        :pswitch_62
        :pswitch_65
        :pswitch_68
        :pswitch_6b
        :pswitch_6e
        :pswitch_71
        :pswitch_20
        :pswitch_20
        :pswitch_20
        :pswitch_20
        :pswitch_74
        :pswitch_81
        :pswitch_8e
    .end packed-switch
.end method

.method private removeCurLights()V
    .registers 3

    #@0
    .prologue
    .line 822
    iget-object v1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@2
    monitor-enter v1

    #@3
    .line 823
    :try_start_3
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@5
    invoke-direct {p0, v0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->removeLightsLocked(Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;)V

    #@8
    .line 824
    monitor-exit v1

    #@9
    .line 825
    return-void

    #@a
    .line 824
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method private removeLightsLocked(Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;)V
    .registers 3
    .parameter "record"

    #@0
    .prologue
    .line 814
    if-nez p1, :cond_3

    #@2
    .line 819
    :goto_2
    return-void

    #@3
    .line 817
    :cond_3
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@8
    .line 818
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->findPriorityLevelLocked()I

    #@b
    move-result v0

    #@c
    iput v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedArrayPriority:I

    #@e
    goto :goto_2
.end method

.method private restartInternal()V
    .registers 5

    #@0
    .prologue
    const/4 v2, 0x2

    #@1
    .line 652
    iget-object v1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mH:Landroid/os/Handler;

    #@3
    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@6
    .line 653
    iget-object v1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mH:Landroid/os/Handler;

    #@8
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@b
    move-result-object v0

    #@c
    .line 654
    .local v0, nmsg:Landroid/os/Message;
    iget-object v1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mH:Landroid/os/Handler;

    #@e
    iget v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->restart_led_delay:I

    #@10
    int-to-long v2, v2

    #@11
    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@14
    .line 655
    return-void
.end method

.method private restartInternalWithBackLed()V
    .registers 4

    #@0
    .prologue
    .line 633
    iget-object v1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@2
    monitor-enter v1

    #@3
    .line 634
    :try_start_3
    sget-boolean v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->DEBUG:Z

    #@5
    if-eqz v0, :cond_e

    #@7
    const-string v0, "EmotionalLed"

    #@9
    const-string v2, "restartInternalWithBackLed()"

    #@b
    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 635
    :cond_e
    iget v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mCurrentPatternId:I

    #@10
    invoke-direct {p0, v0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->isSRAMPattern(I)Z

    #@13
    move-result v0

    #@14
    if-eqz v0, :cond_26

    #@16
    .line 636
    const/4 v0, 0x0

    #@17
    invoke-direct {p0, v0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->setLightPattern(I)V

    #@1a
    .line 643
    :cond_1a
    :goto_1a
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->turnOff()V

    #@1d
    .line 645
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@1f
    if-eqz v0, :cond_24

    #@21
    .line 646
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->updateLightsLocked()V

    #@24
    .line 648
    :cond_24
    monitor-exit v1

    #@25
    .line 649
    return-void

    #@26
    .line 637
    :cond_26
    iget-boolean v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mEmotionalEnabled:Z

    #@28
    if-nez v0, :cond_1a

    #@2a
    .line 639
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@2c
    if-eqz v0, :cond_1a

    #@2e
    .line 640
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->stopPattern()V

    #@31
    goto :goto_1a

    #@32
    .line 648
    :catchall_32
    move-exception v0

    #@33
    monitor-exit v1
    :try_end_34
    .catchall {:try_start_3 .. :try_end_34} :catchall_32

    #@34
    throw v0
.end method

.method private setBrightnessInternal(I)V
    .registers 6
    .parameter

    #@0
    .prologue
    .line 1299
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@2
    monitor-enter v2

    #@3
    .line 1300
    :try_start_3
    sget-boolean v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->DEBUG:Z

    #@5
    if-eqz v0, :cond_1f

    #@7
    const-string v0, "EmotionalLed"

    #@9
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v3, "setBrightness:"

    #@10
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1f
    .catchall {:try_start_3 .. :try_end_1f} :catchall_3f

    #@1f
    .line 1302
    :cond_1f
    const/4 v0, 0x0

    #@20
    .line 1304
    :try_start_20
    iget-object v1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedlightValues:[I

    #@22
    iget-object v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedlightValues:[I

    #@24
    array-length v3, v3

    #@25
    add-int/lit8 v3, v3, -0x1

    #@27
    aget v1, v1, v3
    :try_end_29
    .catchall {:try_start_20 .. :try_end_29} :catchall_3f
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_20 .. :try_end_29} :catch_34

    #@29
    .line 1309
    :goto_29
    if-le p1, v1, :cond_44

    #@2b
    .line 1312
    :goto_2b
    if-le v0, v1, :cond_42

    #@2d
    .line 1315
    :goto_2d
    :try_start_2d
    iput v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedBrightness:I

    #@2f
    .line 1316
    invoke-direct {p0, v0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->setLightCurrent(I)V

    #@32
    .line 1317
    monitor-exit v2

    #@33
    .line 1318
    return-void

    #@34
    .line 1305
    :catch_34
    move-exception v1

    #@35
    .line 1306
    const-string v1, "EmotionalLed"

    #@37
    const-string v3, "ArrayIndexOutOfBoundsException !!"

    #@39
    invoke-static {v1, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    .line 1307
    const/16 v1, 0xff

    #@3e
    goto :goto_29

    #@3f
    .line 1317
    :catchall_3f
    move-exception v0

    #@40
    monitor-exit v2
    :try_end_41
    .catchall {:try_start_2d .. :try_end_41} :catchall_3f

    #@41
    throw v0

    #@42
    :cond_42
    move v0, v1

    #@43
    goto :goto_2d

    #@44
    :cond_44
    move v1, p1

    #@45
    goto :goto_2b
.end method

.method private setFlashing(IIII)V
    .registers 7
    .parameter "color"
    .parameter "mode"
    .parameter "onMS"
    .parameter "offMS"

    #@0
    .prologue
    .line 673
    iget-object v1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightsLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 674
    :try_start_3
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->setLightLocked(IIII)V

    #@6
    .line 675
    monitor-exit v1

    #@7
    .line 676
    return-void

    #@8
    .line 675
    :catchall_8
    move-exception v0

    #@9
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_8

    #@a
    throw v0
.end method

.method private setLightCurrent(I)V
    .registers 4
    .parameter "current"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1291
    const/4 v0, 0x4

    #@2
    invoke-direct {p0, p1, v0, v1, v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->setFlashing(IIII)V

    #@5
    .line 1292
    return-void
.end method

.method private setLightLocked(IIII)V
    .registers 12
    .parameter "color"
    .parameter "mode"
    .parameter "onMS"
    .parameter "offMS"

    #@0
    .prologue
    .line 679
    iget v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mNativePointer:I

    #@2
    const/4 v1, 0x4

    #@3
    const/4 v6, 0x0

    #@4
    move v2, p1

    #@5
    move v3, p2

    #@6
    move v4, p3

    #@7
    move v5, p4

    #@8
    invoke-static/range {v0 .. v6}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->setLight_native(IIIIIII)V

    #@b
    .line 681
    return-void
.end method

.method private setLightPattern(I)V
    .registers 4
    .parameter "patternId"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1287
    const/4 v0, 0x3

    #@2
    invoke-direct {p0, p1, v0, v1, v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->setFlashing(IIII)V

    #@5
    .line 1288
    return-void
.end method

.method private setLightPredefinedPattern(II)V
    .registers 6
    .parameter "patternId"
    .parameter "whichLedPlay"

    #@0
    .prologue
    .line 594
    iput p1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mCurrentPatternId:I

    #@2
    .line 595
    invoke-direct {p0, p1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->isSRAMPattern(I)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_63

    #@8
    .line 596
    sget-boolean v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->DEBUG:Z

    #@a
    if-eqz v0, :cond_24

    #@c
    .line 597
    const-string v0, "EmotionalLed"

    #@e
    new-instance v1, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v2, "[support SRAM] setLightPredefinedPattern..patterId:"

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 601
    :cond_24
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@26
    if-eqz v0, :cond_5c

    #@28
    const-string v0, "com.android.settings"

    #@2a
    iget-object v1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@2c
    iget-object v1, v1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->pkgName:Ljava/lang/String;

    #@2e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@31
    move-result v0

    #@32
    if-eqz v0, :cond_5c

    #@34
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mDeviceName:Ljava/lang/String;

    #@36
    const-string v1, "vu3"

    #@38
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3b
    move-result v0

    #@3c
    if-nez v0, :cond_5c

    #@3e
    .line 603
    add-int/lit16 p1, p1, 0x3e8

    #@40
    .line 604
    sget-boolean v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->DEBUG:Z

    #@42
    if-eqz v0, :cond_5c

    #@44
    const-string v0, "EmotionalLed"

    #@46
    new-instance v1, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v2, "For preview, sram pattern change to "

    #@4d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v1

    #@51
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@54
    move-result-object v1

    #@55
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v1

    #@59
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5c
    .line 607
    :cond_5c
    invoke-direct {p0, p1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->setLightPattern(I)V

    #@5f
    .line 608
    const/4 v0, 0x1

    #@60
    iput-boolean v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mInfinite:Z

    #@62
    .line 615
    :goto_62
    return-void

    #@63
    .line 610
    :cond_63
    sget-boolean v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->DEBUG:Z

    #@65
    if-eqz v0, :cond_7f

    #@67
    .line 611
    const-string v0, "EmotionalLed"

    #@69
    new-instance v1, Ljava/lang/StringBuilder;

    #@6b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6e
    const-string v2, "[Not support SRAM] setLightPredefinedPattern..patterId:"

    #@70
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v1

    #@74
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@77
    move-result-object v1

    #@78
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v1

    #@7c
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7f
    .line 613
    :cond_7f
    invoke-direct {p0, p1, p2}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->startPattern(II)V

    #@82
    goto :goto_62
.end method

.method private static setLight_native(IIIIIII)V
	.registers 7
	return-void
.end method

.method private setTimeoutLocked(JI)V
    .registers 10
    .parameter "now"
    .parameter "nextState"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 1154
    iget-object v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mHandler:Landroid/os/Handler;

    #@3
    if-eqz v3, :cond_26

    #@5
    .line 1155
    iget-object v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightsLock:Ljava/lang/Object;

    #@7
    monitor-enter v3

    #@8
    .line 1156
    const-wide/16 v0, 0x0

    #@a
    .line 1157
    .local v0, when:J
    packed-switch p3, :pswitch_data_38

    #@d
    .line 1165
    move-wide v0, p1

    #@e
    .line 1168
    :goto_e
    if-ne p3, v2, :cond_33

    #@10
    :goto_10
    :try_start_10
    invoke-direct {p0, v2}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->enableLightSensorLocked(Z)V

    #@13
    .line 1170
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mHandler:Landroid/os/Handler;

    #@15
    iget-object v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mTimeoutTask:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$TimeoutTask;

    #@17
    invoke-virtual {v2, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@1a
    .line 1171
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mTimeoutTask:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$TimeoutTask;

    #@1c
    iput p3, v2, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$TimeoutTask;->nextState:I

    #@1e
    .line 1172
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mHandler:Landroid/os/Handler;

    #@20
    iget-object v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mTimeoutTask:Lcom/lge/systemservice/service/emotionalled/EmotionalLedService$TimeoutTask;

    #@22
    invoke-virtual {v2, v4, v0, v1}, Landroid/os/Handler;->postAtTime(Ljava/lang/Runnable;J)Z

    #@25
    .line 1173
    monitor-exit v3

    #@26
    .line 1175
    .end local v0           #when:J
    :cond_26
    return-void

    #@27
    .line 1159
    .restart local v0       #when:J
    :pswitch_27
    iget v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorOnDuration:I

    #@29
    int-to-long v4, v4

    #@2a
    add-long v0, p1, v4

    #@2c
    .line 1160
    goto :goto_e

    #@2d
    .line 1162
    :pswitch_2d
    iget v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightSensorOffDuration:I

    #@2f
    int-to-long v4, v4

    #@30
    add-long v0, p1, v4

    #@32
    .line 1163
    goto :goto_e

    #@33
    .line 1168
    :cond_33
    const/4 v2, 0x0

    #@34
    goto :goto_10

    #@35
    .line 1173
    :catchall_35
    move-exception v2

    #@36
    monitor-exit v3
    :try_end_37
    .catchall {:try_start_10 .. :try_end_37} :catchall_35

    #@37
    throw v2

    #@38
    .line 1157
    :pswitch_data_38
    .packed-switch 0x0
        :pswitch_2d
        :pswitch_27
    .end packed-switch
.end method

.method private startBackLedPattern(II)V
    .registers 5
    .parameter
    .parameter

    #@0
    .prologue
    .line 1475
    sget-boolean v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->DEBUG:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 1476
    const-string v0, "EmotionalLed"

    #@6
    const-string v1, "startBackLedPattern()"

    #@8
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 1478
    :cond_b
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->rgbPatternPlayerRunner:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@d
    invoke-virtual {v0}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->stopThread_withHandler()V

    #@10
    .line 1479
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->rgbPatternPlayerRunner:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@12
    invoke-virtual {v0, p1, p2}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->startBackLedThread_withHandler(II)V

    #@15
    .line 1480
    return-void
.end method

.method private startInternal(Ljava/lang/String;ILcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;)V
    .registers 10
    .parameter "pkg"
    .parameter "recordId"
    .parameter "record"

    #@0
    .prologue
    .line 698
    iget-object v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@2
    monitor-enter v3

    #@3
    .line 699
    if-nez p3, :cond_e

    #@5
    .line 700
    :try_start_5
    const-string v2, "EmotionalLed"

    #@7
    const-string v4, " requested record is null"

    #@9
    invoke-static {v2, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 701
    monitor-exit v3

    #@d
    .line 744
    :goto_d
    return-void

    #@e
    .line 703
    :cond_e
    const-string v2, "EmotionalLed"

    #@10
    new-instance v4, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v5, "startInternal : pkg="

    #@17
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v4

    #@1f
    const-string v5, ", recordId="

    #@21
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v4

    #@25
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v4

    #@29
    const-string v5, " : "

    #@2b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v4

    #@2f
    invoke-virtual {p3}, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->toString()Ljava/lang/String;

    #@32
    move-result-object v5

    #@33
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v4

    #@37
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v4

    #@3b
    invoke-static {v2, v4}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 705
    move-object v1, p3

    #@3f
    .line 706
    .local v1, requestLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
    const/4 v0, 0x0

    #@40
    .line 707
    .local v0, oldLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
    invoke-direct {p0, p1, p2}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->findSameSourceLocked(Ljava/lang/String;I)Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@43
    move-result-object v0

    #@44
    .line 709
    iget-boolean v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mEmotionalEnabled:Z

    #@46
    if-nez v2, :cond_58

    #@48
    iget-boolean v2, v1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->mExceptional:Z

    #@4a
    if-nez v2, :cond_58

    #@4c
    .line 710
    const-string v2, "EmotionalLed"

    #@4e
    const-string v4, "mEmotionalEnabled or mExceptional is null"

    #@50
    invoke-static {v2, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    .line 711
    monitor-exit v3

    #@54
    goto :goto_d

    #@55
    .line 743
    .end local v0           #oldLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
    .end local v1           #requestLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
    :catchall_55
    move-exception v2

    #@56
    monitor-exit v3
    :try_end_57
    .catchall {:try_start_5 .. :try_end_57} :catchall_55

    #@57
    throw v2

    #@58
    .line 714
    .restart local v0       #oldLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
    .restart local v1       #requestLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
    :cond_58
    :try_start_58
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@5a
    if-eqz v2, :cond_8e

    #@5c
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@5e
    iget v2, v2, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@60
    if-ltz v2, :cond_8e

    #@62
    iget v2, v1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->priority:I

    #@64
    iget v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedArrayPriority:I

    #@66
    if-lt v2, v4, :cond_8e

    #@68
    .line 717
    sget-boolean v2, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->DEBUG:Z

    #@6a
    if-eqz v2, :cond_73

    #@6c
    const-string v2, "EmotionalLed"

    #@6e
    const-string v4, "mLed is stop"

    #@70
    invoke-static {v2, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@73
    .line 718
    :cond_73
    invoke-virtual {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->isInfinite()Z

    #@76
    move-result v2

    #@77
    if-nez v2, :cond_8b

    #@79
    .line 719
    sget-boolean v2, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->DEBUG:Z

    #@7b
    if-eqz v2, :cond_84

    #@7d
    const-string v2, "EmotionalLed"

    #@7f
    const-string v4, "mLed removed from array"

    #@81
    invoke-static {v2, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@84
    .line 720
    :cond_84
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@86
    iget-object v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@88
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@8b
    .line 722
    :cond_8b
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->stopPatternFlashing()V

    #@8e
    .line 725
    :cond_8e
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@90
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@93
    .line 726
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@95
    if-ne v2, v0, :cond_9d

    #@97
    .line 727
    const/4 v2, 0x0

    #@98
    iput-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@9a
    .line 728
    const/4 v2, -0x2

    #@9b
    iput v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedArrayPriority:I

    #@9d
    .line 730
    :cond_9d
    const-string v2, "EmotionalLed"

    #@9f
    new-instance v4, Ljava/lang/StringBuilder;

    #@a1
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@a4
    const-string v5, "enqueuing start. mLedList.size()="

    #@a6
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v4

    #@aa
    iget-object v5, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@ac
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@af
    move-result v5

    #@b0
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v4

    #@b4
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b7
    move-result-object v4

    #@b8
    invoke-static {v2, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@bb
    .line 731
    iget v2, v1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@bd
    if-gez v2, :cond_cb

    #@bf
    iget v2, v1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->color:I

    #@c1
    if-eqz v2, :cond_118

    #@c3
    iget v2, v1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->onMS:I

    #@c5
    if-nez v2, :cond_cb

    #@c7
    iget v2, v1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->offMS:I

    #@c9
    if-eqz v2, :cond_118

    #@cb
    .line 733
    :cond_cb
    sget-boolean v2, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->DEBUG:Z

    #@cd
    if-eqz v2, :cond_e9

    #@cf
    const-string v2, "EmotionalLed"

    #@d1
    new-instance v4, Ljava/lang/StringBuilder;

    #@d3
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@d6
    const-string v5, "mLedList.add : pkgName="

    #@d8
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@db
    move-result-object v4

    #@dc
    iget-object v5, v1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->pkgName:Ljava/lang/String;

    #@de
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e1
    move-result-object v4

    #@e2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e5
    move-result-object v4

    #@e6
    invoke-static {v2, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e9
    .line 734
    :cond_e9
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@eb
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@ee
    .line 739
    :cond_ee
    :goto_ee
    iget v2, v1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->priority:I

    #@f0
    iget v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedArrayPriority:I

    #@f2
    if-lt v2, v4, :cond_f7

    #@f4
    .line 740
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->updateLightsLocked()V

    #@f7
    .line 742
    :cond_f7
    const-string v2, "EmotionalLed"

    #@f9
    new-instance v4, Ljava/lang/StringBuilder;

    #@fb
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@fe
    const-string v5, "enqueuing end. mLedList.size()="

    #@100
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@103
    move-result-object v4

    #@104
    iget-object v5, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@106
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@109
    move-result v5

    #@10a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10d
    move-result-object v4

    #@10e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@111
    move-result-object v4

    #@112
    invoke-static {v2, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@115
    .line 743
    monitor-exit v3

    #@116
    goto/16 :goto_d

    #@118
    .line 735
    :cond_118
    iget v2, v1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->priority:I

    #@11a
    iget v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedArrayPriority:I

    #@11c
    if-lt v2, v4, :cond_ee

    #@11e
    .line 736
    const-string v2, "EmotionalLed"

    #@120
    const-string v4, "remove the mLed record"

    #@122
    invoke-static {v2, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@125
    .line 737
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@127
    invoke-direct {p0, v2}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->removeLightsLocked(Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;)V
    :try_end_12a
    .catchall {:try_start_58 .. :try_end_12a} :catchall_55

    #@12a
    goto :goto_ee
.end method

.method private declared-synchronized startInternalBackLed()V
    .registers 3

    #@0
    .prologue
    .line 775
    monitor-enter p0

    #@1
    :try_start_1
    sget-boolean v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->DEBUG:Z

    #@3
    if-eqz v0, :cond_c

    #@5
    const-string v0, "EmotionalLed"

    #@7
    const-string v1, "startInternalBackLed()"

    #@9
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 776
    :cond_c
    const/4 v0, 0x2

    #@d
    const/4 v1, 0x2

    #@e
    invoke-direct {p0, v0, v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->startPattern(II)V
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_13

    #@11
    .line 777
    monitor-exit p0

    #@12
    return-void

    #@13
    .line 775
    :catchall_13
    move-exception v0

    #@14
    monitor-exit p0

    #@15
    throw v0
.end method

.method private startInternalWithRecord(Ljava/lang/String;IIII)V
    .registers 13
    .parameter "pkgName"
    .parameter "recordId"
    .parameter "priority"
    .parameter "flags"
    .parameter "patternId"

    #@0
    .prologue
    .line 780
    sget-boolean v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->DEBUG:Z

    #@2
    if-eqz v0, :cond_1c

    #@4
    const-string v0, "EmotionalLed"

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "startInternalWithRecord:pkg="

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 781
    :cond_1c
    const/4 v6, 0x0

    #@1d
    move-object v0, p0

    #@1e
    move-object v1, p1

    #@1f
    move v2, p2

    #@20
    move v3, p3

    #@21
    move v4, p4

    #@22
    move v5, p5

    #@23
    invoke-direct/range {v0 .. v6}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->startInternalWithRecord(Ljava/lang/String;IIIIZ)V

    #@26
    .line 782
    return-void
.end method

.method private startInternalWithRecord(Ljava/lang/String;IIIIZ)V
    .registers 9
    .parameter "pkgName"
    .parameter "recordId"
    .parameter "priority"
    .parameter "flags"
    .parameter "patternId"
    .parameter "alwaysOn"

    #@0
    .prologue
    .line 786
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->checkToRestartLed()Z

    #@3
    move-result v1

    #@4
    if-nez v1, :cond_a

    #@6
    iget v1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mCurrentPatternId:I

    #@8
    if-eq p5, v1, :cond_21

    #@a
    .line 787
    :cond_a
    new-instance v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@c
    invoke-direct {v0}, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;-><init>()V

    #@f
    .line 788
    .local v0, led:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
    iput p3, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->priority:I

    #@11
    .line 789
    iget v1, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->flags:I

    #@13
    or-int/2addr v1, p4

    #@14
    iput v1, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->flags:I

    #@16
    .line 790
    iput p5, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@18
    .line 791
    iput-object p1, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->pkgName:Ljava/lang/String;

    #@1a
    .line 792
    iput p2, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->recordId:I

    #@1c
    .line 793
    iput-boolean p6, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->mExceptional:Z

    #@1e
    .line 794
    invoke-direct {p0, p1, p2, v0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->startInternal(Ljava/lang/String;ILcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;)V

    #@21
    .line 796
    .end local v0           #led:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
    :cond_21
    return-void
.end method

.method private startPattern(II)V
    .registers 6
    .parameter
    .parameter

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1483
    sget-boolean v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->DEBUG:Z

    #@3
    if-eqz v0, :cond_c

    #@5
    .line 1484
    const-string v0, "EmotionalLed"

    #@7
    const-string v1, "startPattern()"

    #@9
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 1486
    :cond_c
    iput-boolean v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mInfinite:Z

    #@e
    .line 1487
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->rgbPatternPlayerRunner:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@10
    invoke-virtual {v0}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->stopThread_withHandler()V

    #@13
    .line 1488
    invoke-direct {p0, p1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->parsePattern(I)Ljava/util/ArrayList;

    #@16
    move-result-object v1

    #@17
    .line 1489
    if-nez v1, :cond_32

    #@19
    .line 1490
    const-string v0, "EmotionalLed"

    #@1b
    new-instance v1, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v2, "Can\'t handle this request, patternId : "

    #@22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v1

    #@2e
    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    .line 1499
    :goto_31
    return-void

    #@32
    .line 1494
    :cond_32
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@35
    move-result-object v0

    #@36
    check-cast v0, Lcom/lge/systemservice/service/emotionalled/RGBFormat;

    #@38
    invoke-virtual {v0}, Lcom/lge/systemservice/service/emotionalled/RGBFormat;->getInterval()I

    #@3b
    move-result v0

    #@3c
    if-nez v0, :cond_41

    #@3e
    .line 1495
    const/4 v0, 0x1

    #@3f
    iput-boolean v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mInfinite:Z

    #@41
    .line 1498
    :cond_41
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->rgbPatternPlayerRunner:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@43
    invoke-virtual {v0, v1, p2}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->startThread_withHandler(Ljava/util/ArrayList;I)V

    #@46
    goto :goto_31
.end method

.method private stopInternal(Ljava/lang/String;I)V
    .registers 7
    .parameter
    .parameter

    #@0
    .prologue
    .line 1001
    iget-object v1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@2
    monitor-enter v1

    #@3
    .line 1002
    :try_start_3
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@5
    if-nez v0, :cond_10

    #@7
    .line 1003
    const-string v0, "EmotionalLed"

    #@9
    const-string v2, "mLedList is null"

    #@b
    invoke-static {v0, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 1004
    monitor-exit v1

    #@f
    .line 1017
    :goto_f
    return-void

    #@10
    .line 1006
    :cond_10
    const-string v0, "EmotionalLed"

    #@12
    new-instance v2, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v3, "stopInternal() start. mLedList.size()="

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    iget-object v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@1f
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@22
    move-result v3

    #@23
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    const-string v3, ", pkg="

    #@29
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    const-string v3, ", id="

    #@33
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v2

    #@3b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v2

    #@3f
    invoke-static {v0, v2}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    .line 1007
    invoke-direct {p0, p1, p2}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->findSameSourceLocked(Ljava/lang/String;I)Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@45
    move-result-object v0

    #@46
    .line 1008
    if-eqz v0, :cond_5d

    #@48
    .line 1009
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->getHighestPriorityRecordLocked()Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@4b
    move-result-object v2

    #@4c
    if-ne v0, v2, :cond_55

    #@4e
    iget v2, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@50
    if-ltz v2, :cond_55

    #@52
    .line 1010
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->stopPatternFlashing()V

    #@55
    .line 1012
    :cond_55
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@57
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@5a
    .line 1013
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->updateLightsLocked()V

    #@5d
    .line 1015
    :cond_5d
    sget-boolean v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->DEBUG:Z

    #@5f
    if-eqz v0, :cond_7f

    #@61
    const-string v0, "EmotionalLed"

    #@63
    new-instance v2, Ljava/lang/StringBuilder;

    #@65
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@68
    const-string v3, "stopInternal() end. mLedList.size()="

    #@6a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v2

    #@6e
    iget-object v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@70
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@73
    move-result v3

    #@74
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@77
    move-result-object v2

    #@78
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v2

    #@7c
    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7f
    .line 1016
    :cond_7f
    monitor-exit v1

    #@80
    goto :goto_f

    #@81
    :catchall_81
    move-exception v0

    #@82
    monitor-exit v1
    :try_end_83
    .catchall {:try_start_3 .. :try_end_83} :catchall_81

    #@83
    throw v0
.end method

.method private stopPattern()V
    .registers 3

    #@0
    .prologue
    .line 1502
    sget-boolean v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->DEBUG:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 1503
    const-string v0, "EmotionalLed"

    #@6
    const-string v1, "stopPattern()"

    #@8
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 1505
    :cond_b
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->rgbPatternPlayerRunner:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@d
    if-eqz v0, :cond_17

    #@f
    .line 1506
    const/4 v0, 0x0

    #@10
    iput-boolean v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mInfinite:Z

    #@12
    .line 1507
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->rgbPatternPlayerRunner:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@14
    invoke-virtual {v0}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->stopThread_withHandler()V

    #@17
    .line 1509
    :cond_17
    return-void
.end method

.method private stopPatternFlashing()V
    .registers 2

    #@0
    .prologue
    .line 618
    iget v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mCurrentPatternId:I

    #@2
    invoke-direct {p0, v0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->isSRAMPattern(I)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_d

    #@8
    .line 619
    const/4 v0, 0x0

    #@9
    invoke-direct {p0, v0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->setLightPattern(I)V

    #@c
    .line 623
    :goto_c
    return-void

    #@d
    .line 621
    :cond_d
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->stopPattern()V

    #@10
    goto :goto_c
.end method

.method private turnOff()V
    .registers 6

    #@0
    .prologue
    .line 667
    iget-object v1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLightsLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 668
    const/4 v0, 0x0

    #@4
    const/4 v2, 0x0

    #@5
    const/4 v3, 0x0

    #@6
    const/4 v4, 0x0

    #@7
    :try_start_7
    invoke-direct {p0, v0, v2, v3, v4}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->setLightLocked(IIII)V

    #@a
    .line 669
    monitor-exit v1

    #@b
    .line 670
    return-void

    #@c
    .line 669
    :catchall_c
    move-exception v0

    #@d
    monitor-exit v1
    :try_end_e
    .catchall {:try_start_7 .. :try_end_e} :catchall_c

    #@e
    throw v0
.end method

.method private updateBatteryChargingLocked()V
    .registers 7

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 222
    iget-boolean v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mEmotionalEnabled:Z

    #@3
    if-eqz v0, :cond_23

    #@5
    iget-boolean v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mChargingLedEnabled:Z

    #@7
    if-eqz v0, :cond_23

    #@9
    iget-boolean v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mPowered:Z

    #@b
    if-eqz v0, :cond_23

    #@d
    .line 223
    const/4 v5, 0x3

    #@e
    .line 224
    .local v5, patternId:I
    iget v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mBatteryLevel:I

    #@10
    const/16 v1, 0x64

    #@12
    if-ne v0, v1, :cond_15

    #@14
    .line 225
    const/4 v5, 0x4

    #@15
    .line 227
    :cond_15
    const/4 v4, 0x0

    #@16
    .line 228
    .local v4, flags:I
    iget-boolean v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedOnEvenWhenLcdOn:Z

    #@18
    if-eqz v0, :cond_1b

    #@1a
    .line 229
    const/4 v4, 0x2

    #@1b
    .line 231
    :cond_1b
    const-string v1, "batteryinfo"

    #@1d
    const/4 v3, -0x1

    #@1e
    move-object v0, p0

    #@1f
    invoke-direct/range {v0 .. v5}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->startInternalWithRecord(Ljava/lang/String;IIII)V

    #@22
    .line 236
    .end local v4           #flags:I
    .end local v5           #patternId:I
    :goto_22
    return-void

    #@23
    .line 234
    :cond_23
    const-string v0, "batteryinfo"

    #@25
    invoke-direct {p0, v0, v2}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->stopInternal(Ljava/lang/String;I)V

    #@28
    goto :goto_22
.end method

.method private updateLightListInternal(IIZIIILjava/lang/String;)V
    .registers 15
    .parameter "action"
    .parameter "recordId"
    .parameter "exceptional"
    .parameter "ledARGB"
    .parameter "ledOnMS"
    .parameter "ledOffMS"
    .parameter "pkg"

    #@0
    .prologue
    .line 416
    iget-object v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@2
    monitor-enter v4

    #@3
    .line 417
    :try_start_3
    const-string v3, "EmotionalLed"

    #@5
    new-instance v5, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v6, "updateLightListInternal: action="

    #@c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v5

    #@10
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v5

    #@14
    const-string v6, " pkg="

    #@16
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v5

    #@1a
    invoke-virtual {v5, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v5

    #@1e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v5

    #@22
    invoke-static {v3, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 419
    const/4 v3, 0x1

    #@26
    if-ne p1, v3, :cond_2d

    #@28
    .line 421
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->clearLedListLocked()V

    #@2b
    .line 455
    :cond_2b
    :goto_2b
    monitor-exit v4

    #@2c
    .line 456
    return-void

    #@2d
    .line 423
    :cond_2d
    new-instance v1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@2f
    invoke-direct {v1}, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;-><init>()V

    #@32
    .line 424
    .local v1, r:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
    if-eqz v1, :cond_2b

    #@34
    iget-object v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@36
    if-eqz v3, :cond_2b

    #@38
    .line 425
    if-eqz p3, :cond_3d

    #@3a
    .line 426
    const/4 v3, 0x1

    #@3b
    iput-boolean v3, v1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->mExceptional:Z

    #@3d
    .line 428
    :cond_3d
    iget-boolean v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedOnEvenWhenLcdOn:Z

    #@3f
    if-eqz v3, :cond_44

    #@41
    .line 429
    const/4 v3, 0x2

    #@42
    iput v3, v1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->flags:I

    #@44
    .line 431
    :cond_44
    const/4 v3, 0x1

    #@45
    iput-boolean v3, v1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->mNativeNotification:Z

    #@47
    .line 432
    iput p4, v1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->color:I

    #@49
    .line 433
    iput p5, v1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->onMS:I

    #@4b
    .line 434
    iput p6, v1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->offMS:I

    #@4d
    .line 435
    iput p2, v1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->recordId:I

    #@4f
    .line 436
    iput-object p7, v1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->pkgName:Ljava/lang/String;

    #@51
    .line 438
    const/4 v3, 0x2

    #@52
    if-ne p1, v3, :cond_81

    #@54
    .line 439
    iget-object v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@56
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@59
    move-result v3

    #@5a
    add-int/lit8 v0, v3, -0x1

    #@5c
    .local v0, i:I
    :goto_5c
    if-ltz v0, :cond_2b

    #@5e
    .line 440
    iget-object v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@60
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@63
    move-result-object v2

    #@64
    check-cast v2, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@66
    .line 441
    .local v2, record:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
    invoke-virtual {v2, v1}, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->equals(Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;)Z

    #@69
    move-result v3

    #@6a
    if-eqz v3, :cond_7e

    #@6c
    .line 442
    iget-object v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@6e
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@71
    .line 443
    iget-boolean v3, v1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->mExceptional:Z

    #@73
    if-eqz v3, :cond_7e

    #@75
    .line 444
    iget-object v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->rgbPatternPlayerRunner:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@77
    if-eqz v3, :cond_7e

    #@79
    .line 445
    iget-object v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->rgbPatternPlayerRunner:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@7b
    invoke-virtual {v3}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->stopThread_withHandler()V

    #@7e
    .line 439
    :cond_7e
    add-int/lit8 v0, v0, -0x1

    #@80
    goto :goto_5c

    #@81
    .line 450
    .end local v0           #i:I
    .end local v2           #record:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
    :cond_81
    const/4 v3, 0x3

    #@82
    if-ne p1, v3, :cond_2b

    #@84
    .line 451
    iget-object v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@86
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@89
    goto :goto_2b

    #@8a
    .line 455
    .end local v1           #r:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
    :catchall_8a
    move-exception v3

    #@8b
    monitor-exit v4
    :try_end_8c
    .catchall {:try_start_3 .. :try_end_8c} :catchall_8a

    #@8c
    throw v3
.end method

.method private updateLightsLocked()V
    .registers 8

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 921
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v3

    #@8
    .line 922
    const-string v0, "EmotionalLed"

    #@a
    new-instance v4, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v5, "updateLightsLocked() start. mLedList.size="

    #@11
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v4

    #@15
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v4

    #@19
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v4

    #@1d
    invoke-static {v0, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 924
    sget-boolean v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->DEBUG:Z

    #@22
    if-eqz v0, :cond_4d

    #@24
    .line 925
    add-int/lit8 v0, v3, -0x1

    #@26
    :goto_26
    if-ltz v0, :cond_4d

    #@28
    .line 926
    const-string v4, "EmotionalLed"

    #@2a
    new-instance v5, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v5

    #@33
    const-string v6, " : "

    #@35
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v5

    #@39
    iget-object v6, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@3b
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@3e
    move-result-object v6

    #@3f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v5

    #@43
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v5

    #@47
    invoke-static {v4, v5}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    .line 925
    add-int/lit8 v0, v0, -0x1

    #@4c
    goto :goto_26

    #@4d
    .line 930
    :cond_4d
    const/4 v0, 0x0

    #@4e
    iput-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@50
    .line 931
    if-lez v3, :cond_58

    #@52
    .line 932
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->getHighestPriorityRecordLocked()Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@55
    move-result-object v0

    #@56
    iput-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@58
    .line 937
    :cond_58
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@5a
    if-eqz v0, :cond_1b6

    #@5c
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@5e
    iget v0, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->flags:I

    #@60
    and-int/lit8 v0, v0, 0x2

    #@62
    if-eqz v0, :cond_1b6

    #@64
    move v0, v1

    #@65
    .line 941
    :goto_65
    iget-object v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@67
    if-eqz v3, :cond_1b3

    #@69
    iget-object v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@6b
    iget v3, v3, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->flags:I

    #@6d
    and-int/lit8 v3, v3, 0x1

    #@6f
    if-eqz v3, :cond_1b3

    #@71
    move v3, v1

    #@72
    .line 945
    :goto_72
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->turnOff()V

    #@75
    .line 947
    sget-boolean v4, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->DEBUG:Z

    #@77
    if-eqz v4, :cond_cb

    #@79
    .line 948
    const-string v4, "EmotionalLed"

    #@7b
    new-instance v5, Ljava/lang/StringBuilder;

    #@7d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@80
    const-string v6, "mInCall="

    #@82
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v5

    #@86
    iget-boolean v6, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mInCall:Z

    #@88
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v5

    #@8c
    const-string v6, ", mLedOnEvenWhenLcdOn="

    #@8e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v5

    #@92
    iget-boolean v6, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedOnEvenWhenLcdOn:Z

    #@94
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@97
    move-result-object v5

    #@98
    const-string v6, ", mChargingLedEnabled="

    #@9a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v5

    #@9e
    iget-boolean v6, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mChargingLedEnabled:Z

    #@a0
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v5

    #@a4
    const-string v6, ", mScreenOn="

    #@a6
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v5

    #@aa
    iget-boolean v6, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mScreenOn:Z

    #@ac
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@af
    move-result-object v5

    #@b0
    const-string v6, ", onlyLedOnLighting="

    #@b2
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v5

    #@b6
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v5

    #@ba
    const-string v6, ", isForceLighting="

    #@bc
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v5

    #@c0
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v5

    #@c4
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c7
    move-result-object v5

    #@c8
    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@cb
    .line 953
    :cond_cb
    iget-object v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@cd
    if-eqz v4, :cond_da

    #@cf
    iget-boolean v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mInCall:Z

    #@d1
    if-nez v4, :cond_d8

    #@d3
    iget-boolean v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mScreenOn:Z

    #@d5
    xor-int/2addr v3, v4

    #@d6
    if-eqz v3, :cond_118

    #@d8
    :cond_d8
    if-nez v0, :cond_118

    #@da
    .line 954
    :cond_da
    sget-boolean v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->DEBUG:Z

    #@dc
    if-eqz v0, :cond_e5

    #@de
    const-string v0, "EmotionalLed"

    #@e0
    const-string v1, "updateLightsLocked : turn off..."

    #@e2
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e5
    .line 955
    :cond_e5
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->turnOff()V

    #@e8
    .line 956
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->stopPattern()V

    #@eb
    .line 957
    iget-boolean v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mUseLedAutoBrightness:Z

    #@ed
    if-eqz v0, :cond_f5

    #@ef
    .line 959
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->cancelTimerLocked()V

    #@f2
    .line 960
    invoke-direct {p0, v2}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->enableLightSensorLocked(Z)V

    #@f5
    .line 993
    :cond_f5
    :goto_f5
    sget-boolean v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->DEBUG:Z

    #@f7
    if-eqz v0, :cond_117

    #@f9
    const-string v0, "EmotionalLed"

    #@fb
    new-instance v1, Ljava/lang/StringBuilder;

    #@fd
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@100
    const-string v2, "updateLightsLocked() end. mLedList.size="

    #@102
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@105
    move-result-object v1

    #@106
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@108
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@10b
    move-result v2

    #@10c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10f
    move-result-object v1

    #@110
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@113
    move-result-object v1

    #@114
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@117
    .line 994
    :cond_117
    return-void

    #@118
    .line 963
    :cond_118
    sget-boolean v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->DEBUG:Z

    #@11a
    if-eqz v0, :cond_123

    #@11c
    const-string v0, "EmotionalLed"

    #@11e
    const-string v2, "updateLightsLocked : turn on..."

    #@120
    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@123
    .line 965
    :cond_123
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@125
    iget v0, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@127
    .line 966
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@129
    iget v2, v2, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->whichLedPlay:I

    #@12b
    .line 968
    if-lez v0, :cond_175

    #@12d
    .line 969
    sget-boolean v3, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->DEBUG:Z

    #@12f
    if-eqz v3, :cond_153

    #@131
    const-string v3, "EmotionalLed"

    #@133
    new-instance v4, Ljava/lang/StringBuilder;

    #@135
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@138
    const-string v5, "Pattern play pattern id : "

    #@13a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13d
    move-result-object v4

    #@13e
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@141
    move-result-object v4

    #@142
    const-string v5, " whichLedPlay : "

    #@144
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@147
    move-result-object v4

    #@148
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14b
    move-result-object v4

    #@14c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14f
    move-result-object v4

    #@150
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@153
    .line 970
    :cond_153
    iget-boolean v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mEmotionalEnabled:Z

    #@155
    if-eqz v3, :cond_15e

    #@157
    .line 971
    iget-object v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@159
    iput-object v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mCurrentLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@15b
    .line 972
    invoke-direct {p0, v0, v2}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->setLightPredefinedPattern(II)V

    #@15e
    .line 987
    :cond_15e
    :goto_15e
    iget-boolean v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mUseLedAutoBrightness:Z

    #@160
    if-eqz v0, :cond_f5

    #@162
    iget-boolean v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mPowered:Z

    #@164
    if-eqz v0, :cond_f5

    #@166
    iget-boolean v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mIsShutdown:Z

    #@168
    if-nez v0, :cond_f5

    #@16a
    .line 988
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->cancelTimerLocked()V

    #@16d
    .line 990
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@170
    move-result-wide v2

    #@171
    invoke-direct {p0, v2, v3, v1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->setTimeoutLocked(JI)V

    #@174
    goto :goto_f5

    #@175
    .line 975
    :cond_175
    sget-boolean v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->DEBUG:Z

    #@177
    if-eqz v0, :cond_180

    #@179
    const-string v0, "EmotionalLed"

    #@17b
    const-string v2, "Google native play"

    #@17d
    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@180
    .line 976
    :cond_180
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@182
    iget v0, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->color:I

    #@184
    .line 977
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@186
    iget v2, v2, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->onMS:I

    #@188
    .line 978
    iget-object v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@18a
    iget v3, v3, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->offMS:I

    #@18c
    .line 979
    iget-boolean v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedEnabled:Z

    #@18e
    if-eqz v4, :cond_194

    #@190
    iget-boolean v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mEmotionalEnabled:Z

    #@192
    if-nez v4, :cond_19e

    #@194
    :cond_194
    iget-object v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@196
    if-eqz v4, :cond_15e

    #@198
    iget-object v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@19a
    iget-boolean v4, v4, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->mExceptional:Z

    #@19c
    if-eqz v4, :cond_15e

    #@19e
    .line 980
    :cond_19e
    iget-object v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@1a0
    iput-object v4, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mCurrentLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@1a2
    .line 981
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->setFlashing(IIII)V

    #@1a5
    .line 982
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLed:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@1a7
    iget-boolean v0, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->mExceptional:Z

    #@1a9
    if-eqz v0, :cond_15e

    #@1ab
    iget-boolean v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mHasBackLed:Z

    #@1ad
    if-eqz v0, :cond_15e

    #@1af
    .line 983
    invoke-direct {p0, v2, v3}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->startBackLedPattern(II)V

    #@1b2
    goto :goto_15e

    #@1b3
    :cond_1b3
    move v3, v2

    #@1b4
    goto/16 :goto_72

    #@1b6
    :cond_1b6
    move v0, v2

    #@1b7
    goto/16 :goto_65
.end method


# virtual methods
.method public clearAllLeds()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1201
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@2
    monitor-enter v2

    #@3
    .line 1202
    :try_start_3
    sget-boolean v0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->DEBUG:Z

    #@5
    if-eqz v0, :cond_21

    #@7
    const-string v0, "EmotionalLed"

    #@9
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v3, "clearAllLeds:"

    #@10
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    iget-object v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@16
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 1203
    :cond_21
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mTempLedList:Ljava/util/ArrayList;

    #@23
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@26
    .line 1204
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@28
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@2b
    move-result v0

    #@2c
    add-int/lit8 v0, v0, -0x1

    #@2e
    move v1, v0

    #@2f
    :goto_2f
    if-ltz v1, :cond_a6

    #@31
    .line 1205
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@33
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@36
    move-result-object v0

    #@37
    check-cast v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@39
    .line 1206
    iget v3, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@3b
    const/16 v4, 0x8

    #@3d
    if-eq v3, v4, :cond_9d

    #@3f
    iget v3, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@41
    const/4 v4, 0x3

    #@42
    if-eq v3, v4, :cond_9d

    #@44
    iget v3, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@46
    const/4 v4, 0x4

    #@47
    if-eq v3, v4, :cond_9d

    #@49
    iget v3, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@4b
    const/16 v4, 0x9

    #@4d
    if-eq v3, v4, :cond_9d

    #@4f
    iget v3, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@51
    const/16 v4, 0xa

    #@53
    if-eq v3, v4, :cond_9d

    #@55
    iget v3, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@57
    const/16 v4, 0xb

    #@59
    if-eq v3, v4, :cond_9d

    #@5b
    iget v3, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@5d
    const/16 v4, 0x15

    #@5f
    if-eq v3, v4, :cond_9d

    #@61
    iget v3, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@63
    const/16 v4, 0x16

    #@65
    if-eq v3, v4, :cond_9d

    #@67
    iget v3, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@69
    const/16 v4, 0x17

    #@6b
    if-eq v3, v4, :cond_9d

    #@6d
    iget v3, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@6f
    const/16 v4, 0x18

    #@71
    if-eq v3, v4, :cond_9d

    #@73
    iget v3, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@75
    const/16 v4, 0x19

    #@77
    if-eq v3, v4, :cond_9d

    #@79
    iget v3, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@7b
    const/16 v4, 0x1a

    #@7d
    if-eq v3, v4, :cond_9d

    #@7f
    iget v3, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@81
    const/16 v4, 0x1b

    #@83
    if-eq v3, v4, :cond_9d

    #@85
    iget v3, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@87
    const/16 v4, 0x1c

    #@89
    if-eq v3, v4, :cond_9d

    #@8b
    iget v3, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@8d
    const/16 v4, 0x65

    #@8f
    if-eq v3, v4, :cond_9d

    #@91
    iget v3, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@93
    const/16 v4, 0x66

    #@95
    if-eq v3, v4, :cond_9d

    #@97
    iget v3, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@99
    const/16 v4, 0x23

    #@9b
    if-ne v3, v4, :cond_a2

    #@9d
    .line 1223
    :cond_9d
    iget-object v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mTempLedList:Ljava/util/ArrayList;

    #@9f
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@a2
    .line 1204
    :cond_a2
    add-int/lit8 v0, v1, -0x1

    #@a4
    move v1, v0

    #@a5
    goto :goto_2f

    #@a6
    .line 1226
    :cond_a6
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@a8
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@ab
    .line 1227
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mLedList:Ljava/util/ArrayList;

    #@ad
    iget-object v1, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mTempLedList:Ljava/util/ArrayList;

    #@af
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    #@b2
    move-result v0

    #@b3
    if-nez v0, :cond_d3

    #@b5
    .line 1228
    const-string v0, "EmotionalLed"

    #@b7
    new-instance v1, Ljava/lang/StringBuilder;

    #@b9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@bc
    const-string v3, "clearAllLeds:addAll failed list size ="

    #@be
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v1

    #@c2
    iget-object v3, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mTempLedList:Ljava/util/ArrayList;

    #@c4
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@c7
    move-result v3

    #@c8
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@cb
    move-result-object v1

    #@cc
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cf
    move-result-object v1

    #@d0
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@d3
    .line 1230
    :cond_d3
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->stopPatternFlashing()V

    #@d6
    .line 1231
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->updateLightsLocked()V

    #@d9
    .line 1232
    monitor-exit v2

    #@da
    .line 1233
    return-void

    #@db
    .line 1232
    :catchall_db
    move-exception v0

    #@dc
    monitor-exit v2
    :try_end_dd
    .catchall {:try_start_3 .. :try_end_dd} :catchall_db

    #@dd
    throw v0
.end method

.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 1248
    iget v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mNativePointer:I

    #@2
    invoke-static {v0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->finalize_native(I)V

    #@5
    .line 1249
    invoke-super {p0}, Lcom/lge/systemservice/core/emotionalledmanager/IEmotionalLedManager$Stub;->finalize()V

    #@8
    .line 1250
    return-void
.end method

.method public getEmotionalLedType()I
    .registers 2

    #@0
    .prologue
    .line 1581
    iget v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mEmotionalLedType:I

    #@2
    return v0
.end method

.method public isInfinite()Z
    .registers 2

    #@0
    .prologue
    .line 1467
    iget-boolean v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mInfinite:Z

    #@2
    return v0
.end method

.method public onFinishPattern(I)V
    .registers 6
    .parameter

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1512
    sget-boolean v1, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->DEBUG:Z

    #@3
    if-eqz v1, :cond_1d

    #@5
    const-string v1, "EmotionalLed"

    #@7
    new-instance v2, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v3, "onFinishPattern() : whichLedPlay="

    #@e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v2

    #@1a
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 1513
    :cond_1d
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@20
    move-result-object v1

    #@21
    .line 1514
    iput v0, v1, Landroid/os/Message;->what:I

    #@23
    .line 1515
    iget-boolean v2, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mInfinite:Z

    #@25
    if-eqz v2, :cond_28

    #@27
    const/4 v0, 0x1

    #@28
    :cond_28
    iput v0, v1, Landroid/os/Message;->arg1:I

    #@2a
    .line 1516
    iput p1, v1, Landroid/os/Message;->arg2:I

    #@2c
    .line 1517
    iget-object v0, p0, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->mH:Landroid/os/Handler;

    #@2e
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@31
    .line 1518
    return-void
.end method

.method public restart()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 626
    invoke-direct {p0}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->restartInternal()V

    #@3
    .line 627
    return-void
.end method

.method public setBrightness(I)V
    .registers 2
    .parameter "brightness"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1295
    invoke-direct {p0, p1}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->setBrightnessInternal(I)V

    #@3
    .line 1296
    return-void
.end method

.method public setLightLED(III)V
    .registers 5
    .parameter "color"
    .parameter "onMS"
    .parameter "offMS"

    #@0
    .prologue
    .line 1283
    const/4 v0, 0x2

    #@1
    invoke-direct {p0, p1, v0, p2, p3}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->setFlashing(IIII)V

    #@4
    .line 1284
    return-void
.end method

.method public start(Ljava/lang/String;ILcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;)V
    .registers 4
    .parameter "pkg"
    .parameter "recordId"
    .parameter "record"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 694
    invoke-direct {p0, p1, p2, p3}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->startInternal(Ljava/lang/String;ILcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;)V

    #@3
    .line 695
    return-void
.end method

.method public stop(Ljava/lang/String;I)V
    .registers 3
    .parameter "pkg"
    .parameter "id"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 997
    invoke-direct {p0, p1, p2}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->stopInternal(Ljava/lang/String;I)V

    #@3
    .line 998
    return-void
.end method

.method public updateLightList(IIIIIILjava/lang/String;)V
    .registers 16
    .parameter "action"
    .parameter "recordId"
    .parameter "exceptional"
    .parameter "ledARGB"
    .parameter "ledOnMS"
    .parameter "ledOffMS"
    .parameter "pkg"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 410
    if-ne p3, v3, :cond_e

    #@3
    :goto_3
    move-object v0, p0

    #@4
    move v1, p1

    #@5
    move v2, p2

    #@6
    move v4, p4

    #@7
    move v5, p5

    #@8
    move v6, p6

    #@9
    move-object v7, p7

    #@a
    invoke-direct/range {v0 .. v7}, Lcom/lge/systemservice/service/emotionalled/EmotionalLedService;->updateLightListInternal(IIZIIILjava/lang/String;)V

    #@d
    .line 412
    return-void

    #@e
    .line 410
    :cond_e
    const/4 v3, 0x0

    #@f
    goto :goto_3
.end method
