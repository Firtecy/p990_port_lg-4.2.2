.class Lcom/lge/systemservice/service/LGSDEncService;
.super Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager$Stub;
.source "LGSDEncService.java"

# interfaces
.implements Lcom/lge/systemservice/service/INativeDaemonConnectorCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/systemservice/service/LGSDEncService$LGSDEncServiceThread;
    }
.end annotation


# static fields
.field private static mtotalMemory:J


# instance fields
.field WarningDialogHandler:Landroid/os/Handler;

.field private mConnector:Lcom/lge/systemservice/service/NativeDaemonConnector;

.field private mContext:Landroid/content/Context;

.field private mIntentReceiver:Landroid/content/BroadcastReceiver;

.field private final mVolumes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/storage/StorageVolume;",
            ">;"
        }
    .end annotation
.end field

.field mountService:Landroid/os/storage/IMountService;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 54
    const-string v0, "lgsdenc_jni"

    #@2
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@5
    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 10
    .parameter "context"

    #@0
    .prologue
    .line 118
    invoke-direct {p0}, Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager$Stub;-><init>()V

    #@3
    .line 57
    new-instance v0, Lcom/lge/systemservice/service/LGSDEncService$1;

    #@5
    invoke-direct {v0, p0}, Lcom/lge/systemservice/service/LGSDEncService$1;-><init>(Lcom/lge/systemservice/service/LGSDEncService;)V

    #@8
    iput-object v0, p0, Lcom/lge/systemservice/service/LGSDEncService;->WarningDialogHandler:Landroid/os/Handler;

    #@a
    .line 81
    new-instance v0, Ljava/util/ArrayList;

    #@c
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@f
    iput-object v0, p0, Lcom/lge/systemservice/service/LGSDEncService;->mVolumes:Ljava/util/ArrayList;

    #@11
    .line 358
    new-instance v0, Lcom/lge/systemservice/service/LGSDEncService$2;

    #@13
    invoke-direct {v0, p0}, Lcom/lge/systemservice/service/LGSDEncService$2;-><init>(Lcom/lge/systemservice/service/LGSDEncService;)V

    #@16
    iput-object v0, p0, Lcom/lge/systemservice/service/LGSDEncService;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@18
    .line 119
    iput-object p1, p0, Lcom/lge/systemservice/service/LGSDEncService;->mContext:Landroid/content/Context;

    #@1a
    .line 121
    new-instance v6, Landroid/content/IntentFilter;

    #@1c
    invoke-direct {v6}, Landroid/content/IntentFilter;-><init>()V

    #@1f
    .line 122
    .local v6, filter:Landroid/content/IntentFilter;
    const-string v0, "android.intent.action.BOOT_COMPLETED"

    #@21
    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@24
    .line 123
    iget-object v0, p0, Lcom/lge/systemservice/service/LGSDEncService;->mContext:Landroid/content/Context;

    #@26
    iget-object v1, p0, Lcom/lge/systemservice/service/LGSDEncService;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@28
    invoke-virtual {v0, v1, v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@2b
    .line 126
    new-instance v0, Lcom/lge/systemservice/service/NativeDaemonConnector;

    #@2d
    const-string v2, "vold"

    #@2f
    const/16 v3, 0x1f4

    #@31
    const-string v4, "VoldConnector3"

    #@33
    const/16 v5, 0x19

    #@35
    move-object v1, p0

    #@36
    invoke-direct/range {v0 .. v5}, Lcom/lge/systemservice/service/NativeDaemonConnector;-><init>(Lcom/lge/systemservice/service/INativeDaemonConnectorCallbacks;Ljava/lang/String;ILjava/lang/String;I)V

    #@39
    iput-object v0, p0, Lcom/lge/systemservice/service/LGSDEncService;->mConnector:Lcom/lge/systemservice/service/NativeDaemonConnector;

    #@3b
    .line 127
    new-instance v7, Ljava/lang/Thread;

    #@3d
    iget-object v0, p0, Lcom/lge/systemservice/service/LGSDEncService;->mConnector:Lcom/lge/systemservice/service/NativeDaemonConnector;

    #@3f
    const-string v1, "VoldConnector3"

    #@41
    invoke-direct {v7, v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    #@44
    .line 128
    .local v7, thread:Ljava/lang/Thread;
    invoke-virtual {v7}, Ljava/lang/Thread;->start()V

    #@47
    .line 129
    return-void
.end method

.method static synthetic access$000(Lcom/lge/systemservice/service/LGSDEncService;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 38
    iget-object v0, p0, Lcom/lge/systemservice/service/LGSDEncService;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$202(J)J
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 38
    sput-wide p0, Lcom/lge/systemservice/service/LGSDEncService;->mtotalMemory:J

    #@2
    return-wide p0
.end method


# virtual methods
.method public MDMStorageEncryptionStatus()Z
    .registers 6

    #@0
    .prologue
    .line 214
    const/4 v1, 0x0

    #@1
    .line 215
    .local v1, intPath:Ljava/lang/String;
    const/4 v0, 0x0

    #@2
    .line 216
    .local v0, extPath:Ljava/lang/String;
    const/4 v2, 0x0

    #@3
    .line 219
    .local v2, ret:Z
    invoke-virtual {p0}, Lcom/lge/systemservice/service/LGSDEncService;->getExternalSDCardMountPath()Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 230
    if-eqz v0, :cond_1a

    #@9
    .line 232
    const-string v3, "persist.sdcrypto.enabled"

    #@b
    const-string v4, "0"

    #@d
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@10
    move-result-object v3

    #@11
    const-string v4, "1"

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16
    move-result v3

    #@17
    if-eqz v3, :cond_1b

    #@19
    .line 233
    const/4 v2, 0x1

    #@1a
    .line 238
    :cond_1a
    :goto_1a
    return v2

    #@1b
    .line 235
    :cond_1b
    const/4 v2, 0x0

    #@1c
    goto :goto_1a
.end method

.method public StorageEncryptionStatus()I
    .registers 4

    #@0
    .prologue
    .line 200
    const/4 v0, 0x0

    #@1
    .line 201
    .local v0, ret:I
    const-string v1, "persist.insdcrypto.enabled"

    #@3
    const-string v2, "0"

    #@5
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    const-string v2, "1"

    #@b
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e
    move-result v1

    #@f
    if-eqz v1, :cond_26

    #@11
    .line 202
    or-int/lit8 v0, v0, 0x1

    #@13
    .line 207
    :goto_13
    const-string v1, "persist.sdcrypto.enabled"

    #@15
    const-string v2, "0"

    #@17
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    const-string v2, "1"

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@20
    move-result v1

    #@21
    if-eqz v1, :cond_25

    #@23
    .line 208
    or-int/lit8 v0, v0, 0x4

    #@25
    .line 210
    :cond_25
    return v0

    #@26
    .line 204
    :cond_26
    const/4 v0, 0x0

    #@27
    goto :goto_13
.end method

.method public doCommand(Ljava/lang/String;Ljava/lang/String;)I
    .registers 11
    .parameter "cmd"
    .parameter "path"

    #@0
    .prologue
    .line 299
    const/4 v2, 0x0

    #@1
    .line 300
    .local v2, intPath:Ljava/lang/String;
    const/4 v1, 0x0

    #@2
    .line 301
    .local v1, extPath:Ljava/lang/String;
    const/4 v3, 0x0

    #@3
    .line 303
    .local v3, ret:I
    iget-object v4, p0, Lcom/lge/systemservice/service/LGSDEncService;->mContext:Landroid/content/Context;

    #@5
    const-string v5, "android.permission.CRYPT_KEEPER"

    #@7
    const-string v6, "no permission to access the SD Encryption"

    #@9
    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@c
    .line 306
    const-string v4, "internalStorage"

    #@e
    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v4

    #@12
    if-eqz v4, :cond_5a

    #@14
    .line 307
    invoke-virtual {p0}, Lcom/lge/systemservice/service/LGSDEncService;->getInternalSDCardMountPath()Ljava/lang/String;

    #@17
    move-result-object v2

    #@18
    .line 320
    :goto_18
    if-eqz v2, :cond_2a

    #@1a
    .line 322
    :try_start_1a
    iget-object v4, p0, Lcom/lge/systemservice/service/LGSDEncService;->mConnector:Lcom/lge/systemservice/service/NativeDaemonConnector;

    #@1c
    const-string v5, "sdcryptfs"

    #@1e
    const/4 v6, 0x2

    #@1f
    new-array v6, v6, [Ljava/lang/Object;

    #@21
    const/4 v7, 0x0

    #@22
    aput-object p1, v6, v7

    #@24
    const/4 v7, 0x1

    #@25
    aput-object v2, v6, v7

    #@27
    invoke-virtual {v4, v5, v6}, Lcom/lge/systemservice/service/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/lge/systemservice/service/NativeDaemonEvent;
    :try_end_2a
    .catch Lcom/lge/systemservice/service/NativeDaemonConnectorException; {:try_start_1a .. :try_end_2a} :catch_9e

    #@2a
    .line 328
    :cond_2a
    :goto_2a
    if-eqz v1, :cond_3c

    #@2c
    .line 330
    :try_start_2c
    iget-object v4, p0, Lcom/lge/systemservice/service/LGSDEncService;->mConnector:Lcom/lge/systemservice/service/NativeDaemonConnector;

    #@2e
    const-string v5, "sdcryptfs"

    #@30
    const/4 v6, 0x2

    #@31
    new-array v6, v6, [Ljava/lang/Object;

    #@33
    const/4 v7, 0x0

    #@34
    aput-object p1, v6, v7

    #@36
    const/4 v7, 0x1

    #@37
    aput-object v1, v6, v7

    #@39
    invoke-virtual {v4, v5, v6}, Lcom/lge/systemservice/service/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/lge/systemservice/service/NativeDaemonEvent;
    :try_end_3c
    .catch Lcom/lge/systemservice/service/NativeDaemonConnectorException; {:try_start_2c .. :try_end_3c} :catch_a4

    #@3c
    .line 336
    :cond_3c
    :goto_3c
    const-string v4, "LGSDEncService"

    #@3e
    new-instance v5, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v6, "sdcryptfs "

    #@45
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v5

    #@49
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v5

    #@4d
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@50
    move-result-object v5

    #@51
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v5

    #@55
    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@58
    move v4, v3

    #@59
    .line 337
    :goto_59
    return v4

    #@5a
    .line 308
    :cond_5a
    const-string v4, "externalStorage"

    #@5c
    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5f
    move-result v4

    #@60
    if-eqz v4, :cond_67

    #@62
    .line 309
    invoke-virtual {p0}, Lcom/lge/systemservice/service/LGSDEncService;->getExternalSDCardMountPath()Ljava/lang/String;

    #@65
    move-result-object v1

    #@66
    goto :goto_18

    #@67
    .line 310
    :cond_67
    const-string v4, "AllStorage"

    #@69
    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6c
    move-result v4

    #@6d
    if-eqz v4, :cond_7e

    #@6f
    .line 311
    invoke-static {}, Landroid/os/Environment;->isExternalStorageRemovable()Z

    #@72
    move-result v4

    #@73
    if-nez v4, :cond_79

    #@75
    .line 312
    invoke-virtual {p0}, Lcom/lge/systemservice/service/LGSDEncService;->getInternalSDCardMountPath()Ljava/lang/String;

    #@78
    move-result-object v2

    #@79
    .line 314
    :cond_79
    invoke-virtual {p0}, Lcom/lge/systemservice/service/LGSDEncService;->getExternalSDCardMountPath()Ljava/lang/String;

    #@7c
    move-result-object v1

    #@7d
    goto :goto_18

    #@7e
    .line 316
    :cond_7e
    const-string v4, "LGSDEncService"

    #@80
    new-instance v5, Ljava/lang/StringBuilder;

    #@82
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@85
    const-string v6, "cmd : "

    #@87
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v5

    #@8b
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v5

    #@8f
    const-string v6, ". Wrong path name"

    #@91
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v5

    #@95
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@98
    move-result-object v5

    #@99
    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@9c
    .line 317
    const/4 v4, -0x1

    #@9d
    goto :goto_59

    #@9e
    .line 323
    :catch_9e
    move-exception v0

    #@9f
    .line 325
    .local v0, e:Lcom/lge/systemservice/service/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/lge/systemservice/service/NativeDaemonConnectorException;->getCode()I

    #@a2
    move-result v3

    #@a3
    goto :goto_2a

    #@a4
    .line 331
    .end local v0           #e:Lcom/lge/systemservice/service/NativeDaemonConnectorException;
    :catch_a4
    move-exception v0

    #@a5
    .line 333
    .restart local v0       #e:Lcom/lge/systemservice/service/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/lge/systemservice/service/NativeDaemonConnectorException;->getCode()I

    #@a8
    move-result v3

    #@a9
    goto :goto_3c
.end method

.method public externalSDCardCheckTotalmemory()V
    .registers 11

    #@0
    .prologue
    const-wide/16 v8, 0x0

    #@2
    .line 251
    sget-wide v2, Lcom/lge/systemservice/service/LGSDEncService;->mtotalMemory:J

    #@4
    cmp-long v2, v2, v8

    #@6
    if-nez v2, :cond_2d

    #@8
    .line 252
    invoke-virtual {p0}, Lcom/lge/systemservice/service/LGSDEncService;->getExternalSDCardMountPath()Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    .line 253
    .local v0, extPath:Ljava/lang/String;
    if-eqz v0, :cond_26

    #@e
    .line 254
    new-instance v1, Ljava/io/File;

    #@10
    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@13
    .line 255
    .local v1, fileList:Ljava/io/File;
    const-wide v2, 0x3feff487fcb923a3L

    #@18
    invoke-virtual {v1}, Ljava/io/File;->getTotalSpace()J

    #@1b
    move-result-wide v4

    #@1c
    invoke-virtual {v1}, Ljava/io/File;->getFreeSpace()J

    #@1f
    move-result-wide v6

    #@20
    sub-long/2addr v4, v6

    #@21
    long-to-double v4, v4

    #@22
    mul-double/2addr v2, v4

    #@23
    double-to-long v2, v2

    #@24
    sput-wide v2, Lcom/lge/systemservice/service/LGSDEncService;->mtotalMemory:J

    #@26
    .line 257
    .end local v1           #fileList:Ljava/io/File;
    :cond_26
    sget-wide v2, Lcom/lge/systemservice/service/LGSDEncService;->mtotalMemory:J

    #@28
    invoke-static {v2, v3}, Lcom/lge/systemservice/core/lgsdencmanager/LGSDEncNative;->fullTotalMemory(J)I

    #@2b
    .line 258
    sput-wide v8, Lcom/lge/systemservice/service/LGSDEncService;->mtotalMemory:J

    #@2d
    .line 260
    .end local v0           #extPath:Ljava/lang/String;
    :cond_2d
    return-void
.end method

.method public externalSDCardDisableEncryption(Ljava/lang/String;)I
    .registers 3
    .parameter "path"

    #@0
    .prologue
    .line 246
    const-string v0, "disablecrypto"

    #@2
    invoke-virtual {p0, v0, p1}, Lcom/lge/systemservice/service/LGSDEncService;->doCommand(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public externalSDCardEnableEncryption(Ljava/lang/String;)I
    .registers 3
    .parameter "path"

    #@0
    .prologue
    .line 242
    const-string v0, "enablecrypto"

    #@2
    invoke-virtual {p0, v0, p1}, Lcom/lge/systemservice/service/LGSDEncService;->doCommand(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public externalSDCardFullDisableEncryption(Ljava/lang/String;)I
    .registers 3
    .parameter "path"

    #@0
    .prologue
    .line 268
    invoke-virtual {p0}, Lcom/lge/systemservice/service/LGSDEncService;->externalSDCardCheckTotalmemory()V

    #@3
    .line 269
    const/4 v0, 0x0

    #@4
    invoke-static {p1, v0}, Lcom/lge/systemservice/core/lgsdencmanager/LGSDEncNative;->fullEncryption(Ljava/lang/String;I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public externalSDCardFullEnableEncryption(Ljava/lang/String;)I
    .registers 3
    .parameter "path"

    #@0
    .prologue
    .line 263
    invoke-virtual {p0}, Lcom/lge/systemservice/service/LGSDEncService;->externalSDCardCheckTotalmemory()V

    #@3
    .line 264
    const/4 v0, 0x1

    #@4
    invoke-static {p1, v0}, Lcom/lge/systemservice/core/lgsdencmanager/LGSDEncNative;->fullEncryption(Ljava/lang/String;I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public externalSDCardFullTotalMemory(J)I
    .registers 4
    .parameter "totalMemory"

    #@0
    .prologue
    .line 273
    sput-wide p1, Lcom/lge/systemservice/service/LGSDEncService;->mtotalMemory:J

    #@2
    .line 274
    invoke-static {p1, p2}, Lcom/lge/systemservice/core/lgsdencmanager/LGSDEncNative;->fullTotalMemory(J)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public externalSDCardMediaDisableEncryption()I
    .registers 2

    #@0
    .prologue
    .line 283
    const/4 v0, 0x0

    #@1
    invoke-static {v0}, Lcom/lge/systemservice/core/lgsdencmanager/LGSDEncNative;->sysCallMediaProperty(I)I

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public externalSDCardMediaEnableEncryption()I
    .registers 2

    #@0
    .prologue
    .line 279
    const/4 v0, 0x1

    #@1
    invoke-static {v0}, Lcom/lge/systemservice/core/lgsdencmanager/LGSDEncNative;->sysCallMediaProperty(I)I

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public externalSDCardMountComplete(Ljava/lang/String;)I
    .registers 3
    .parameter "path"

    #@0
    .prologue
    .line 288
    const-string v0, "complete"

    #@2
    invoke-virtual {p0, v0, p1}, Lcom/lge/systemservice/service/LGSDEncService;->doCommand(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getExtensionMedia()Ljava/lang/String;
    .registers 9

    #@0
    .prologue
    .line 342
    const-string v0, ""

    #@2
    .line 343
    .local v0, extensionMedia:Ljava/lang/String;
    invoke-static {}, Landroid/media/MediaFile;->getFileTypeMediaHash()Ljava/util/HashMap;

    #@5
    move-result-object v4

    #@6
    .line 345
    .local v4, sFileMediaMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Landroid/media/MediaFile$MediaFileType;>;"
    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@9
    move-result-object v5

    #@a
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@d
    move-result-object v2

    #@e
    .line 346
    .local v2, iterator:Ljava/util/Iterator;
    :cond_e
    :goto_e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@11
    move-result v5

    #@12
    if-eqz v5, :cond_5c

    #@14
    .line 347
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@17
    move-result-object v3

    #@18
    check-cast v3, Ljava/util/Map$Entry;

    #@1a
    .line 348
    .local v3, keyEntry:Ljava/util/Map$Entry;
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@1d
    move-result-object v1

    #@1e
    check-cast v1, Landroid/media/MediaFile$MediaFileType;

    #@20
    .line 349
    .local v1, fileMediaVal:Landroid/media/MediaFile$MediaFileType;
    iget v5, v1, Landroid/media/MediaFile$MediaFileType;->fileType:I

    #@22
    invoke-static {v5}, Landroid/media/MediaFile;->isAudioFileType(I)Z

    #@25
    move-result v5

    #@26
    if-nez v5, :cond_40

    #@28
    iget v5, v1, Landroid/media/MediaFile$MediaFileType;->fileType:I

    #@2a
    invoke-static {v5}, Landroid/media/MediaFile;->isVideoFileType(I)Z

    #@2d
    move-result v5

    #@2e
    if-nez v5, :cond_40

    #@30
    iget v5, v1, Landroid/media/MediaFile$MediaFileType;->fileType:I

    #@32
    invoke-static {v5}, Landroid/media/MediaFile;->isImageFileType(I)Z

    #@35
    move-result v5

    #@36
    if-nez v5, :cond_40

    #@38
    iget v5, v1, Landroid/media/MediaFile$MediaFileType;->fileType:I

    #@3a
    invoke-static {v5}, Landroid/media/MediaFile;->isPlayListFileType(I)Z

    #@3d
    move-result v5

    #@3e
    if-eqz v5, :cond_e

    #@40
    .line 351
    :cond_40
    new-instance v5, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v5

    #@49
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@4c
    move-result-object v6

    #@4d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v5

    #@51
    const-string v6, "/"

    #@53
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v5

    #@57
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v0

    #@5b
    goto :goto_e

    #@5c
    .line 354
    .end local v1           #fileMediaVal:Landroid/media/MediaFile$MediaFileType;
    .end local v3           #keyEntry:Ljava/util/Map$Entry;
    :cond_5c
    const-string v5, "LGSDEncService"

    #@5e
    new-instance v6, Ljava/lang/StringBuilder;

    #@60
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@63
    const-string v7, "MediaExceptionType="

    #@65
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v6

    #@69
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v6

    #@6d
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@70
    move-result-object v6

    #@71
    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@74
    .line 355
    return-object v0
.end method

.method public getExternalSDCardMountPath()Ljava/lang/String;
    .registers 10

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 179
    :try_start_1
    invoke-virtual {p0}, Lcom/lge/systemservice/service/LGSDEncService;->getVolumeList()[Landroid/os/storage/StorageVolume;

    #@4
    move-result-object v4

    #@5
    .line 180
    .local v4, mVolumes:[Landroid/os/storage/StorageVolume;
    if-nez v4, :cond_8

    #@7
    .line 195
    .end local v4           #mVolumes:[Landroid/os/storage/StorageVolume;
    :cond_7
    :goto_7
    return-object v5

    #@8
    .line 183
    .restart local v4       #mVolumes:[Landroid/os/storage/StorageVolume;
    :cond_8
    array-length v2, v4

    #@9
    .line 185
    .local v2, length:I
    const/4 v1, 0x0

    #@a
    .local v1, i:I
    :goto_a
    if-ge v1, v2, :cond_7

    #@c
    .line 186
    aget-object v3, v4, v1

    #@e
    .line 187
    .local v3, mStorageVolume:Landroid/os/storage/StorageVolume;
    invoke-virtual {v3}, Landroid/os/storage/StorageVolume;->isRemovable()Z

    #@11
    move-result v6

    #@12
    const/4 v7, 0x1

    #@13
    if-ne v6, v7, :cond_1a

    #@15
    .line 188
    invoke-virtual {v3}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_18} :catch_1d

    #@18
    move-result-object v5

    #@19
    goto :goto_7

    #@1a
    .line 185
    :cond_1a
    add-int/lit8 v1, v1, 0x1

    #@1c
    goto :goto_a

    #@1d
    .line 193
    .end local v1           #i:I
    .end local v2           #length:I
    .end local v3           #mStorageVolume:Landroid/os/storage/StorageVolume;
    .end local v4           #mVolumes:[Landroid/os/storage/StorageVolume;
    :catch_1d
    move-exception v0

    #@1e
    .line 194
    .local v0, e:Ljava/lang/Exception;
    const-string v6, "LGSDEncService"

    #@20
    new-instance v7, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v8, "getExternalSDCardMountPath...exception : "

    #@27
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v7

    #@2b
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v7

    #@2f
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v7

    #@33
    invoke-static {v6, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    goto :goto_7
.end method

.method public getInternalSDCardMountPath()Ljava/lang/String;
    .registers 10

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 157
    :try_start_1
    invoke-virtual {p0}, Lcom/lge/systemservice/service/LGSDEncService;->getVolumeList()[Landroid/os/storage/StorageVolume;

    #@4
    move-result-object v4

    #@5
    .line 158
    .local v4, mVolumes:[Landroid/os/storage/StorageVolume;
    if-nez v4, :cond_8

    #@7
    .line 173
    .end local v4           #mVolumes:[Landroid/os/storage/StorageVolume;
    :cond_7
    :goto_7
    return-object v5

    #@8
    .line 161
    .restart local v4       #mVolumes:[Landroid/os/storage/StorageVolume;
    :cond_8
    array-length v2, v4

    #@9
    .line 163
    .local v2, length:I
    const/4 v1, 0x0

    #@a
    .local v1, i:I
    :goto_a
    if-ge v1, v2, :cond_7

    #@c
    .line 164
    aget-object v3, v4, v1

    #@e
    .line 165
    .local v3, mStorageVolume:Landroid/os/storage/StorageVolume;
    invoke-virtual {v3}, Landroid/os/storage/StorageVolume;->isRemovable()Z

    #@11
    move-result v6

    #@12
    if-nez v6, :cond_19

    #@14
    .line 166
    invoke-virtual {v3}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_17} :catch_1c

    #@17
    move-result-object v5

    #@18
    goto :goto_7

    #@19
    .line 163
    :cond_19
    add-int/lit8 v1, v1, 0x1

    #@1b
    goto :goto_a

    #@1c
    .line 171
    .end local v1           #i:I
    .end local v2           #length:I
    .end local v3           #mStorageVolume:Landroid/os/storage/StorageVolume;
    .end local v4           #mVolumes:[Landroid/os/storage/StorageVolume;
    :catch_1c
    move-exception v0

    #@1d
    .line 172
    .local v0, e:Ljava/lang/Exception;
    const-string v6, "LGSDEncService"

    #@1f
    new-instance v7, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v8, "getInternalSDCardMountPath...exception : "

    #@26
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v7

    #@2a
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v7

    #@2e
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v7

    #@32
    invoke-static {v6, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    goto :goto_7
.end method

.method public getMediaProperty()I
    .registers 4

    #@0
    .prologue
    .line 372
    const/4 v0, 0x0

    #@1
    .line 374
    .local v0, enable:I
    const-string v1, "persist.security.sdmediacrypto"

    #@3
    const-string v2, "0"

    #@5
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    const-string v2, "1"

    #@b
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e
    move-result v1

    #@f
    if-eqz v1, :cond_12

    #@11
    .line 375
    const/4 v0, 0x1

    #@12
    .line 378
    :cond_12
    return v0
.end method

.method public getSDEncSupportStatus()Z
    .registers 3

    #@0
    .prologue
    .line 292
    const-string v0, "ro.lge.capp_sdencryption"

    #@2
    const-string v1, "false"

    #@4
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    const-string v1, "true"

    #@a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_12

    #@10
    .line 293
    const/4 v0, 0x1

    #@11
    .line 295
    :goto_11
    return v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method public getVolumeList()[Landroid/os/storage/StorageVolume;
    .registers 10

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 132
    const-string v6, "mount"

    #@3
    invoke-static {v6}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@6
    move-result-object v5

    #@7
    .line 133
    .local v5, service:Landroid/os/IBinder;
    if-nez v5, :cond_12

    #@9
    .line 134
    const-string v6, "LGSDEncService"

    #@b
    const-string v8, "getInternalSDCardMountPath...service null"

    #@d
    invoke-static {v6, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    move-object v4, v7

    #@11
    .line 149
    :cond_11
    :goto_11
    return-object v4

    #@12
    .line 137
    :cond_12
    invoke-static {v5}, Landroid/os/storage/IMountService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/storage/IMountService;

    #@15
    move-result-object v6

    #@16
    iput-object v6, p0, Lcom/lge/systemservice/service/LGSDEncService;->mountService:Landroid/os/storage/IMountService;

    #@18
    .line 139
    :try_start_18
    iget-object v6, p0, Lcom/lge/systemservice/service/LGSDEncService;->mountService:Landroid/os/storage/IMountService;

    #@1a
    invoke-interface {v6}, Landroid/os/storage/IMountService;->getVolumeList()[Landroid/os/storage/StorageVolume;

    #@1d
    move-result-object v3

    #@1e
    .line 140
    .local v3, list:[Landroid/os/Parcelable;
    if-nez v3, :cond_24

    #@20
    const/4 v6, 0x0

    #@21
    new-array v4, v6, [Landroid/os/storage/StorageVolume;

    #@23
    goto :goto_11

    #@24
    .line 141
    :cond_24
    array-length v2, v3

    #@25
    .line 142
    .local v2, length:I
    new-array v4, v2, [Landroid/os/storage/StorageVolume;

    #@27
    .line 143
    .local v4, result:[Landroid/os/storage/StorageVolume;
    const/4 v1, 0x0

    #@28
    .local v1, i:I
    :goto_28
    if-ge v1, v2, :cond_11

    #@2a
    .line 144
    aget-object v6, v3, v1

    #@2c
    check-cast v6, Landroid/os/storage/StorageVolume;

    #@2e
    aput-object v6, v4, v1
    :try_end_30
    .catch Landroid/os/RemoteException; {:try_start_18 .. :try_end_30} :catch_33

    #@30
    .line 143
    add-int/lit8 v1, v1, 0x1

    #@32
    goto :goto_28

    #@33
    .line 147
    .end local v1           #i:I
    .end local v2           #length:I
    .end local v3           #list:[Landroid/os/Parcelable;
    .end local v4           #result:[Landroid/os/storage/StorageVolume;
    :catch_33
    move-exception v0

    #@34
    .line 148
    .local v0, e:Landroid/os/RemoteException;
    const-string v6, "LGSDEncService"

    #@36
    const-string v8, "Failed to get volume list"

    #@38
    invoke-static {v6, v8, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3b
    move-object v4, v7

    #@3c
    .line 149
    goto :goto_11
.end method

.method public onDaemonConnected()V
    .registers 1

    #@0
    .prologue
    .line 85
    return-void
.end method

.method public onEvent(ILjava/lang/String;[Ljava/lang/String;)Z
    .registers 13
    .parameter "code"
    .parameter "raw"
    .parameter "cooked"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 89
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@5
    move-result-object v1

    #@6
    .line 90
    .local v1, msg:Landroid/os/Message;
    const/16 v5, 0x28a

    #@8
    if-ne p1, v5, :cond_1a

    #@a
    .line 91
    const-string v5, "LGSDEncService"

    #@c
    const-string v6, "SDCryptNewSDCardInserted"

    #@e
    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 92
    const-string v5, "com.android.settings.EncryptWarningDialogActivity"

    #@13
    iput-object v5, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@15
    .line 93
    iget-object v5, p0, Lcom/lge/systemservice/service/LGSDEncService;->WarningDialogHandler:Landroid/os/Handler;

    #@17
    invoke-virtual {v5, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@1a
    .line 96
    :cond_1a
    const/16 v5, 0x28b

    #@1c
    if-ne p1, v5, :cond_4f

    #@1e
    .line 97
    const-string v5, "LGSDEncService"

    #@20
    const-string v6, "SDCryptOtherDeviceEnCrypted"

    #@22
    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 98
    const-string v5, "com.android.settings.OtherDeviceDialogActivity"

    #@27
    iput-object v5, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@29
    .line 99
    iget-object v5, p0, Lcom/lge/systemservice/service/LGSDEncService;->WarningDialogHandler:Landroid/os/Handler;

    #@2b
    invoke-virtual {v5, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@2e
    .line 100
    const-string v5, "mount"

    #@30
    invoke-static {v5}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@33
    move-result-object v2

    #@34
    .line 101
    .local v2, service:Landroid/os/IBinder;
    if-nez v2, :cond_3e

    #@36
    .line 102
    const-string v4, "LGSDEncService"

    #@38
    const-string v5, "SDCryptOtherDeviceEnCrypted...service null"

    #@3a
    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 115
    .end local v2           #service:Landroid/os/IBinder;
    :goto_3d
    return v3

    #@3e
    .line 105
    .restart local v2       #service:Landroid/os/IBinder;
    :cond_3e
    invoke-static {v2}, Landroid/os/storage/IMountService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/storage/IMountService;

    #@41
    move-result-object v5

    #@42
    iput-object v5, p0, Lcom/lge/systemservice/service/LGSDEncService;->mountService:Landroid/os/storage/IMountService;

    #@44
    .line 107
    :try_start_44
    iget-object v5, p0, Lcom/lge/systemservice/service/LGSDEncService;->mountService:Landroid/os/storage/IMountService;

    #@46
    invoke-virtual {p0}, Lcom/lge/systemservice/service/LGSDEncService;->getExternalSDCardMountPath()Ljava/lang/String;

    #@49
    move-result-object v6

    #@4a
    const/4 v7, 0x1

    #@4b
    const/4 v8, 0x0

    #@4c
    invoke-interface {v5, v6, v7, v8}, Landroid/os/storage/IMountService;->unmountVolume(Ljava/lang/String;ZZ)V
    :try_end_4f
    .catch Landroid/os/RemoteException; {:try_start_44 .. :try_end_4f} :catch_51

    #@4f
    .end local v2           #service:Landroid/os/IBinder;
    :cond_4f
    move v3, v4

    #@50
    .line 115
    goto :goto_3d

    #@51
    .line 108
    .restart local v2       #service:Landroid/os/IBinder;
    :catch_51
    move-exception v0

    #@52
    .line 109
    .local v0, e:Landroid/os/RemoteException;
    const-string v4, "LGSDEncService"

    #@54
    const-string v5, "Error occured while unmounting external SD card"

    #@56
    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@59
    goto :goto_3d
.end method
