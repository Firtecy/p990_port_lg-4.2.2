.class public Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;
.super Ljava/lang/Object;
.source "WfdAdaptation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$6;,
        Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$EventHandler;,
        Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$StartSessionTask;,
        Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$TeardownTask;,
        Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$PreTeardownTaskSessionCheck;,
        Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperationTask;,
        Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;
    }
.end annotation


# static fields
.field private static final QctWfdRtspStateMap:[I

.field protected static eventHandler:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$EventHandler;

.field private static wfdEnabled:Z

.field private static wfdSessionAvailable:Z


# instance fields
.field public concurrentScanEnabled:Z

.field private connectedDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

.field private mActionListener:Lcom/qualcomm/wfd/service/IWfdActionListener;

.field public mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

.field private final mContext:Landroid/content/Context;

.field private mWfdMonitor:Lcom/lge/systemservice/service/wfdservice/WfdMonitor;

.field private mWfdSessionManager:Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;

.field public mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

.field private thisDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

.field private wfdSettingStatuslistener:Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 41
    const/4 v0, 0x1

    #@1
    sput-boolean v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->wfdEnabled:Z

    #@3
    .line 42
    const/4 v0, 0x0

    #@4
    sput-boolean v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->wfdSessionAvailable:Z

    #@6
    .line 54
    const/4 v0, 0x7

    #@7
    new-array v0, v0, [I

    #@9
    fill-array-data v0, :array_10

    #@c
    sput-object v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->QctWfdRtspStateMap:[I

    #@e
    return-void

    #@f
    nop

    #@10
    :array_10
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x6t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 72
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 45
    iput-object v3, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->mWfdSessionManager:Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;

    #@6
    .line 46
    iput-object v3, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->mActionListener:Lcom/qualcomm/wfd/service/IWfdActionListener;

    #@8
    .line 47
    iput-object v3, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->connectedDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    #@a
    .line 48
    iput-object v3, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->thisDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    #@c
    .line 49
    iput-object v3, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->mWfdMonitor:Lcom/lge/systemservice/service/wfdservice/WfdMonitor;

    #@e
    .line 70
    const/4 v0, 0x1

    #@f
    iput-boolean v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->concurrentScanEnabled:Z

    #@11
    .line 73
    iput-object p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->mContext:Landroid/content/Context;

    #@13
    .line 74
    const-string v0, "wifip2p"

    #@15
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@18
    move-result-object v0

    #@19
    check-cast v0, Landroid/net/wifi/p2p/WifiP2pManager;

    #@1b
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@1d
    .line 75
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@1f
    if-eqz v0, :cond_54

    #@21
    .line 76
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@23
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->mContext:Landroid/content/Context;

    #@25
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->mContext:Landroid/content/Context;

    #@27
    invoke-virtual {v2}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v0, v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->initialize(Landroid/content/Context;Landroid/os/Looper;Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@2e
    move-result-object v0

    #@2f
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@31
    .line 77
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@33
    if-nez v0, :cond_3e

    #@35
    .line 79
    const-string v0, "WfdAdaptation"

    #@37
    const-string v1, "Failed to set up connection with wifi p2p service"

    #@39
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    .line 80
    iput-object v3, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@3e
    .line 86
    :cond_3e
    :goto_3e
    new-instance v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$EventHandler;

    #@40
    invoke-direct {v0, p0}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$EventHandler;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)V

    #@43
    sput-object v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->eventHandler:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$EventHandler;

    #@45
    .line 88
    new-instance v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$1;

    #@47
    invoke-direct {v0, p0}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$1;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)V

    #@4a
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->wfdSettingStatuslistener:Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;

    #@4c
    .line 99
    new-instance v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$2;

    #@4e
    invoke-direct {v0, p0}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$2;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)V

    #@51
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->mActionListener:Lcom/qualcomm/wfd/service/IWfdActionListener;

    #@53
    .line 189
    return-void

    #@54
    .line 83
    :cond_54
    const-string v0, "WfdAdaptation"

    #@56
    const-string v1, "mWifiP2pManager is null!"

    #@58
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5b
    goto :goto_3e
.end method

.method static synthetic access$000(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)Lcom/lge/systemservice/service/wfdservice/WfdMonitor;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 38
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->mWfdMonitor:Lcom/lge/systemservice/service/wfdservice/WfdMonitor;

    #@2
    return-object v0
.end method

.method static synthetic access$100()[I
    .registers 1

    #@0
    .prologue
    .line 38
    sget-object v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->QctWfdRtspStateMap:[I

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 38
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->mWfdSessionManager:Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)Lcom/qualcomm/wfd/service/IWfdActionListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 38
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->mActionListener:Lcom/qualcomm/wfd/service/IWfdActionListener;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Z)Lcom/qualcomm/wfd/WfdDevice;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 38
    invoke-static {p0}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->useDummyWfdDevice(Z)Lcom/qualcomm/wfd/WfdDevice;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method private getWfdSessionManager()Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;
    .registers 3

    #@0
    .prologue
    .line 192
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->mWfdSessionManager:Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;

    #@2
    if-nez v1, :cond_15

    #@4
    .line 193
    new-instance v0, Lcom/lge/systemservice/core/LGContextImpl;

    #@6
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->mContext:Landroid/content/Context;

    #@8
    invoke-direct {v0, v1}, Lcom/lge/systemservice/core/LGContextImpl;-><init>(Landroid/content/Context;)V

    #@b
    .line 194
    .local v0, mServiceContext:Lcom/lge/systemservice/core/LGContext;
    sget-object v1, Lcom/lge/systemservice/core/LGContext$ServiceList;->QCTWIFIDISPLAY_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@d
    invoke-virtual {v0, v1}, Lcom/lge/systemservice/core/LGContextImpl;->getLegacyService(Lcom/lge/systemservice/core/LGContext$ServiceList;)Ljava/lang/Object;

    #@10
    move-result-object v1

    #@11
    check-cast v1, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;

    #@13
    iput-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->mWfdSessionManager:Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;

    #@15
    .line 196
    .end local v0           #mServiceContext:Lcom/lge/systemservice/core/LGContext;
    :cond_15
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->mWfdSessionManager:Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;

    #@17
    return-object v1
.end method

.method public static init()V
    .registers 0

    #@0
    .prologue
    .line 560
    return-void
.end method

.method private teardownWFDService()V
    .registers 4

    #@0
    .prologue
    .line 550
    const-string v1, "WfdAdaptation"

    #@2
    const-string v2, "teardownWFDService() called"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 551
    new-instance v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$PreTeardownTaskSessionCheck;

    #@9
    const/4 v1, 0x0

    #@a
    invoke-direct {v0, p0, v1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$PreTeardownTaskSessionCheck;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$1;)V

    #@d
    .line 552
    .local v0, task:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$PreTeardownTaskSessionCheck;
    const/4 v1, 0x0

    #@e
    new-array v1, v1, [Ljava/lang/Void;

    #@10
    invoke-virtual {v0, v1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$PreTeardownTaskSessionCheck;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    #@13
    .line 553
    return-void
.end method

.method private static useDummyWfdDevice(Z)Lcom/qualcomm/wfd/WfdDevice;
    .registers 7
    .parameter "isSource"

    #@0
    .prologue
    const/16 v5, 0x216a

    #@2
    const/4 v4, 0x1

    #@3
    const/4 v3, 0x0

    #@4
    const/4 v2, 0x0

    #@5
    .line 522
    new-instance v0, Lcom/qualcomm/wfd/WfdDevice;

    #@7
    invoke-direct {v0}, Lcom/qualcomm/wfd/WfdDevice;-><init>()V

    #@a
    .line 523
    .local v0, wfdDevice:Lcom/qualcomm/wfd/WfdDevice;
    if-eqz p0, :cond_25

    #@c
    .line 524
    iput v3, v0, Lcom/qualcomm/wfd/WfdDevice;->deviceType:I

    #@e
    .line 525
    const-string v1, ""

    #@10
    iput-object v1, v0, Lcom/qualcomm/wfd/WfdDevice;->macAddress:Ljava/lang/String;

    #@12
    .line 526
    const-string v1, ""

    #@14
    iput-object v1, v0, Lcom/qualcomm/wfd/WfdDevice;->deviceName:Ljava/lang/String;

    #@16
    .line 527
    iput v5, v0, Lcom/qualcomm/wfd/WfdDevice;->rtspPort:I

    #@18
    .line 528
    iput-boolean v4, v0, Lcom/qualcomm/wfd/WfdDevice;->isAvailableForSession:Z

    #@1a
    .line 529
    iput v3, v0, Lcom/qualcomm/wfd/WfdDevice;->preferredConnectivity:I

    #@1c
    .line 530
    iput-object v2, v0, Lcom/qualcomm/wfd/WfdDevice;->addressOfAP:Ljava/lang/String;

    #@1e
    .line 531
    iput v3, v0, Lcom/qualcomm/wfd/WfdDevice;->coupledSinkStatus:I

    #@20
    .line 532
    iput-object v2, v0, Lcom/qualcomm/wfd/WfdDevice;->capabilities:Landroid/os/Bundle;

    #@22
    .line 533
    iput-object v2, v0, Lcom/qualcomm/wfd/WfdDevice;->ipAddress:Ljava/lang/String;

    #@24
    .line 546
    :goto_24
    return-object v0

    #@25
    .line 535
    :cond_25
    iput v4, v0, Lcom/qualcomm/wfd/WfdDevice;->deviceType:I

    #@27
    .line 536
    const-string v1, ""

    #@29
    iput-object v1, v0, Lcom/qualcomm/wfd/WfdDevice;->macAddress:Ljava/lang/String;

    #@2b
    .line 537
    const-string v1, ""

    #@2d
    iput-object v1, v0, Lcom/qualcomm/wfd/WfdDevice;->deviceName:Ljava/lang/String;

    #@2f
    .line 538
    iput v5, v0, Lcom/qualcomm/wfd/WfdDevice;->rtspPort:I

    #@31
    .line 539
    iput-boolean v4, v0, Lcom/qualcomm/wfd/WfdDevice;->isAvailableForSession:Z

    #@33
    .line 540
    iput v3, v0, Lcom/qualcomm/wfd/WfdDevice;->preferredConnectivity:I

    #@35
    .line 541
    iput-object v2, v0, Lcom/qualcomm/wfd/WfdDevice;->addressOfAP:Ljava/lang/String;

    #@37
    .line 542
    iput v3, v0, Lcom/qualcomm/wfd/WfdDevice;->coupledSinkStatus:I

    #@39
    .line 543
    iput-object v2, v0, Lcom/qualcomm/wfd/WfdDevice;->capabilities:Landroid/os/Bundle;

    #@3b
    .line 544
    iput-object v2, v0, Lcom/qualcomm/wfd/WfdDevice;->ipAddress:Ljava/lang/String;

    #@3d
    goto :goto_24
.end method


# virtual methods
.method public initRtspClient(Ljava/lang/String;)Z
    .registers 3
    .parameter "target_url"

    #@0
    .prologue
    .line 803
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public initRtspEnv()Z
    .registers 3

    #@0
    .prologue
    .line 618
    const-string v0, "WfdAdaptation"

    #@2
    const-string v1, "initRtspEnv"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 619
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->getWfdSessionManager()Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;

    #@a
    move-result-object v0

    #@b
    if-nez v0, :cond_16

    #@d
    .line 620
    const-string v0, "WfdAdaptation"

    #@f
    const-string v1, "initRtspEnv: WfdSessionManager not initialized!"

    #@11
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    .line 621
    const/4 v0, 0x0

    #@15
    .line 627
    :goto_15
    return v0

    #@16
    :cond_16
    const/4 v0, 0x1

    #@17
    goto :goto_15
.end method

.method public initRtspServer(Ljava/lang/String;I)Z
    .registers 5
    .parameter "ipaddr"
    .parameter "port"

    #@0
    .prologue
    .line 632
    const-string v0, "WfdAdaptation"

    #@2
    const-string v1, "initRtspServer"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 634
    const/4 v0, 0x1

    #@8
    return v0
.end method

.method public native_finalize()V
    .registers 1

    #@0
    .prologue
    .line 574
    return-void
.end method

.method public native_setup(Lcom/lge/systemservice/service/wfdservice/WfdMonitor;)V
    .registers 2
    .parameter "wfdMonitor"

    #@0
    .prologue
    .line 567
    iput-object p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->mWfdMonitor:Lcom/lge/systemservice/service/wfdservice/WfdMonitor;

    #@2
    .line 570
    return-void
.end method

.method public rtspGetMyAddress()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 725
    const-string v0, "WfdAdaptation"

    #@2
    const-string v1, "rtspGetMyAddress"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 726
    const/4 v0, 0x0

    #@8
    return-object v0
.end method

.method public rtspSendPause()Z
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 742
    const-string v3, "WfdAdaptation"

    #@4
    const-string v4, "rtspSendPause"

    #@6
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 743
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->getWfdSessionManager()Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;

    #@c
    move-result-object v3

    #@d
    if-nez v3, :cond_17

    #@f
    .line 744
    const-string v2, "WfdAdaptation"

    #@11
    const-string v3, "rtspSendPause: WfdSessionManager not initialized!"

    #@13
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 750
    :goto_16
    return v1

    #@17
    .line 748
    :cond_17
    new-instance v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperationTask;

    #@19
    const/4 v3, 0x0

    #@1a
    invoke-direct {v0, p0, v3}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperationTask;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$1;)V

    #@1d
    .line 749
    .local v0, task:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperationTask;
    new-array v3, v2, [Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@1f
    sget-object v4, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->PAUSE:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@21
    aput-object v4, v3, v1

    #@23
    invoke-virtual {v0, v3}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperationTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    #@26
    move v1, v2

    #@27
    .line 750
    goto :goto_16
.end method

.method public rtspSendPauseTransit()Z
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 767
    const-string v3, "WfdAdaptation"

    #@4
    const-string v4, "rtspSendPauseTransit"

    #@6
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 768
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->getWfdSessionManager()Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;

    #@c
    move-result-object v3

    #@d
    if-nez v3, :cond_17

    #@f
    .line 769
    const-string v2, "WfdAdaptation"

    #@11
    const-string v3, "rtspSendPauseTransit: WfdSessionManager not initialized!"

    #@13
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 775
    :goto_16
    return v1

    #@17
    .line 773
    :cond_17
    new-instance v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperationTask;

    #@19
    const/4 v3, 0x0

    #@1a
    invoke-direct {v0, p0, v3}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperationTask;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$1;)V

    #@1d
    .line 774
    .local v0, task:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperationTask;
    new-array v3, v2, [Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@1f
    sget-object v4, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->PAUSE_TRANSIT:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@21
    aput-object v4, v3, v1

    #@23
    invoke-virtual {v0, v3}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperationTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    #@26
    move v1, v2

    #@27
    .line 775
    goto :goto_16
.end method

.method public rtspSendPlay()Z
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 730
    const-string v3, "WfdAdaptation"

    #@4
    const-string v4, "rtspSendPlay"

    #@6
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 731
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->getWfdSessionManager()Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;

    #@c
    move-result-object v3

    #@d
    if-nez v3, :cond_17

    #@f
    .line 732
    const-string v2, "WfdAdaptation"

    #@11
    const-string v3, "rtspSendPlay: WfdSessionManager not initialized!"

    #@13
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 738
    :goto_16
    return v1

    #@17
    .line 736
    :cond_17
    new-instance v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperationTask;

    #@19
    const/4 v3, 0x0

    #@1a
    invoke-direct {v0, p0, v3}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperationTask;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$1;)V

    #@1d
    .line 737
    .local v0, task:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperationTask;
    new-array v3, v2, [Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@1f
    sget-object v4, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->PLAY:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@21
    aput-object v4, v3, v1

    #@23
    invoke-virtual {v0, v3}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperationTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    #@26
    move v1, v2

    #@27
    .line 738
    goto :goto_16
.end method

.method public rtspSendPlayTransit()Z
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 755
    const-string v3, "WfdAdaptation"

    #@4
    const-string v4, "rtspSendPlayTransit"

    #@6
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 756
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->getWfdSessionManager()Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;

    #@c
    move-result-object v3

    #@d
    if-nez v3, :cond_17

    #@f
    .line 757
    const-string v2, "WfdAdaptation"

    #@11
    const-string v3, "rtspSendPlayTransit: WfdSessionManager not initialized!"

    #@13
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 763
    :goto_16
    return v1

    #@17
    .line 761
    :cond_17
    new-instance v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperationTask;

    #@19
    const/4 v3, 0x0

    #@1a
    invoke-direct {v0, p0, v3}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperationTask;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$1;)V

    #@1d
    .line 762
    .local v0, task:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperationTask;
    new-array v3, v2, [Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@1f
    sget-object v4, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->PLAY_TRANSIT:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@21
    aput-object v4, v3, v1

    #@23
    invoke-virtual {v0, v3}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperationTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    #@26
    move v1, v2

    #@27
    .line 763
    goto :goto_16
.end method

.method public rtspSendTeardown()Z
    .registers 3

    #@0
    .prologue
    .line 785
    const-string v0, "WfdAdaptation"

    #@2
    const-string v1, "rtspSendTeardown"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 786
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->teardownWFDService()V

    #@a
    .line 787
    const/4 v0, 0x1

    #@b
    return v0
.end method

.method public rtspStartThread()Z
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 654
    const-string v3, "WfdAdaptation"

    #@4
    const-string v4, "rtspStartThread"

    #@6
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 656
    iget-object v3, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@b
    if-eqz v3, :cond_13

    #@d
    iget-boolean v3, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->concurrentScanEnabled:Z

    #@f
    if-ne v3, v2, :cond_13

    #@11
    .line 666
    iput-boolean v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->concurrentScanEnabled:Z

    #@13
    .line 669
    :cond_13
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->getWfdSessionManager()Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;

    #@16
    move-result-object v3

    #@17
    if-nez v3, :cond_21

    #@19
    .line 670
    const-string v2, "WfdAdaptation"

    #@1b
    const-string v3, "rtspStartThread: WfdSessionManager not initialized!"

    #@1d
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 685
    :goto_20
    return v1

    #@21
    .line 678
    :cond_21
    new-instance v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$StartSessionTask;

    #@23
    invoke-direct {v0, p0}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$StartSessionTask;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)V

    #@26
    .line 679
    .local v0, task:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$StartSessionTask;
    new-array v3, v2, [Landroid/net/wifi/p2p/WifiP2pDevice;

    #@28
    iget-object v4, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->connectedDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    #@2a
    aput-object v4, v3, v1

    #@2c
    invoke-virtual {v0, v3}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$StartSessionTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    #@2f
    move v1, v2

    #@30
    .line 685
    goto :goto_20
.end method

.method public rtspStopThread()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v0, 0x0

    #@2
    .line 690
    const-string v2, "WfdAdaptation"

    #@4
    const-string v3, "rtspStopThread"

    #@6
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 692
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@b
    if-eqz v2, :cond_13

    #@d
    iget-boolean v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->concurrentScanEnabled:Z

    #@f
    if-nez v2, :cond_13

    #@11
    .line 702
    iput-boolean v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->concurrentScanEnabled:Z

    #@13
    .line 705
    :cond_13
    invoke-direct {p0}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->getWfdSessionManager()Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;

    #@16
    move-result-object v2

    #@17
    if-nez v2, :cond_21

    #@19
    .line 706
    const-string v1, "WfdAdaptation"

    #@1b
    const-string v2, "rtspStopThread: WfdSessionManager not initialized!"

    #@1d
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 714
    :cond_20
    :goto_20
    return v0

    #@21
    .line 710
    :cond_21
    const/4 v2, 0x0

    #@22
    iput-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->connectedDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    #@24
    .line 711
    const/4 v2, -0x1

    #@25
    iget-object v3, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->mWfdSessionManager:Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;

    #@27
    invoke-virtual {v3}, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->stopWfdSession()I

    #@2a
    move-result v3

    #@2b
    if-eq v2, v3, :cond_20

    #@2d
    move v0, v1

    #@2e
    .line 714
    goto :goto_20
.end method

.method public send_wfd_set(ZI)Z
    .registers 7
    .parameter "wfdEnable"
    .parameter "wfd_device_type"

    #@0
    .prologue
    .line 200
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pWfdInfo;

    #@2
    const/16 v1, 0x110

    #@4
    const/16 v2, 0x216a

    #@6
    const/16 v3, 0x14

    #@8
    invoke-direct {v0, v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pWfdInfo;-><init>(III)V

    #@b
    .line 202
    .local v0, p2pWfdInfo:Landroid/net/wifi/p2p/WifiP2pWfdInfo;
    sget-boolean v1, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->wfdEnabled:Z

    #@d
    invoke-virtual {v0, v1}, Landroid/net/wifi/p2p/WifiP2pWfdInfo;->setWfdEnabled(Z)V

    #@10
    .line 203
    invoke-virtual {v0, p2}, Landroid/net/wifi/p2p/WifiP2pWfdInfo;->setDeviceType(I)Z

    #@13
    .line 204
    sget-boolean v1, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->wfdSessionAvailable:Z

    #@15
    invoke-virtual {v0, v1}, Landroid/net/wifi/p2p/WifiP2pWfdInfo;->setSessionAvailable(Z)V

    #@18
    .line 206
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@1a
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@1c
    new-instance v3, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$3;

    #@1e
    invoke-direct {v3, p0}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$3;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)V

    #@21
    invoke-virtual {v1, v2, v0, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->setWFDInfo(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pWfdInfo;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    #@24
    .line 215
    const/4 v1, 0x1

    #@25
    return v1
.end method

.method public setWfdIe(ZZ)Z
    .registers 8
    .parameter "enable"
    .parameter "session_available"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 577
    const-string v1, "WfdAdaptation"

    #@3
    new-instance v2, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v3, "setWfdIe enable="

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    const-string v3, " session="

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 578
    const/4 v0, 0x1

    #@24
    .line 579
    .local v0, result:Z
    sget-boolean v1, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->wfdEnabled:Z

    #@26
    if-eq v1, p1, :cond_37

    #@28
    .line 580
    sput-boolean p1, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->wfdEnabled:Z

    #@2a
    .line 581
    if-eqz p1, :cond_34

    #@2c
    .line 583
    const/4 v1, 0x1

    #@2d
    sput-boolean v1, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->wfdSessionAvailable:Z

    #@2f
    .line 589
    :goto_2f
    invoke-virtual {p0, p1, v4}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->send_wfd_set(ZI)Z

    #@32
    move-result v0

    #@33
    .line 598
    :cond_33
    :goto_33
    return v0

    #@34
    .line 586
    :cond_34
    sput-boolean v4, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->wfdSessionAvailable:Z

    #@36
    goto :goto_2f

    #@37
    .line 591
    :cond_37
    if-eqz p1, :cond_33

    #@39
    .line 592
    sget-boolean v1, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->wfdSessionAvailable:Z

    #@3b
    if-eq v1, p2, :cond_33

    #@3d
    .line 593
    sput-boolean p2, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->wfdSessionAvailable:Z

    #@3f
    .line 594
    invoke-virtual {p0, p1, v4}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->send_wfd_set(ZI)Z

    #@42
    move-result v0

    #@43
    goto :goto_33
.end method

.method public startUIBC(Z)Z
    .registers 7
    .parameter "uibcEnabled"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 791
    new-instance v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperationTask;

    #@4
    const/4 v1, 0x0

    #@5
    invoke-direct {v0, p0, v1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperationTask;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$1;)V

    #@8
    .line 792
    .local v0, task:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperationTask;
    if-eqz p1, :cond_1b

    #@a
    .line 793
    const-string v1, "WfdAdaptation"

    #@c
    const-string v2, "Starting UIBC"

    #@e
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 794
    new-array v1, v3, [Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@13
    sget-object v2, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->START_UIBC:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@15
    aput-object v2, v1, v4

    #@17
    invoke-virtual {v0, v1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperationTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    #@1a
    .line 799
    :goto_1a
    return v3

    #@1b
    .line 796
    :cond_1b
    const-string v1, "WfdAdaptation"

    #@1d
    const-string v2, "Stopping UIBC"

    #@1f
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 797
    new-array v1, v3, [Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@24
    sget-object v2, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;->STOP_UIBC:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperation;

    #@26
    aput-object v2, v1, v4

    #@28
    invoke-virtual {v0, v1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$ControlOperationTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    #@2b
    goto :goto_1a
.end method

.method public updateThisDevice(Landroid/net/wifi/p2p/WifiP2pDevice;)V
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 219
    if-nez p1, :cond_a

    #@2
    .line 220
    const-string v0, "WfdAdaptation"

    #@4
    const-string v1, "updateThisDevice: no device information (null)"

    #@6
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 224
    :goto_9
    return-void

    #@a
    .line 223
    :cond_a
    iput-object p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->thisDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    #@c
    goto :goto_9
.end method
