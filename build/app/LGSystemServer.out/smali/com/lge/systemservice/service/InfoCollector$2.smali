.class Lcom/lge/systemservice/service/InfoCollector$2;
.super Landroid/content/BroadcastReceiver;
.source "InfoCollector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/InfoCollector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/InfoCollector;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/InfoCollector;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 241
    iput-object p1, p0, Lcom/lge/systemservice/service/InfoCollector$2;->this$0:Lcom/lge/systemservice/service/InfoCollector;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 9
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 244
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 245
    .local v0, action:Ljava/lang/String;
    const-string v4, "android.net.wifi.STATE_CHANGE"

    #@6
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v4

    #@a
    if-eqz v4, :cond_12

    #@c
    .line 247
    invoke-static {}, Lcom/lge/systemservice/service/InfoCollector;->access$000()Z

    #@f
    move-result v4

    #@10
    if-eqz v4, :cond_13

    #@12
    .line 305
    :cond_12
    :goto_12
    return-void

    #@13
    .line 260
    :cond_13
    const-string v4, "networkInfo"

    #@15
    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@18
    move-result-object v1

    #@19
    check-cast v1, Landroid/net/NetworkInfo;

    #@1b
    .line 262
    .local v1, mNetworkInfo:Landroid/net/NetworkInfo;
    if-eqz v1, :cond_12

    #@1d
    .line 265
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    #@20
    move-result-object v2

    #@21
    .line 271
    .local v2, state:Landroid/net/NetworkInfo$State;
    iget-object v4, p0, Lcom/lge/systemservice/service/InfoCollector$2;->this$0:Lcom/lge/systemservice/service/InfoCollector;

    #@23
    invoke-static {v4}, Lcom/lge/systemservice/service/InfoCollector;->access$400(Lcom/lge/systemservice/service/InfoCollector;)Landroid/net/NetworkInfo$State;

    #@26
    move-result-object v4

    #@27
    if-eq v4, v2, :cond_12

    #@29
    .line 272
    iget-object v4, p0, Lcom/lge/systemservice/service/InfoCollector$2;->this$0:Lcom/lge/systemservice/service/InfoCollector;

    #@2b
    invoke-static {v4, v2}, Lcom/lge/systemservice/service/InfoCollector;->access$402(Lcom/lge/systemservice/service/InfoCollector;Landroid/net/NetworkInfo$State;)Landroid/net/NetworkInfo$State;

    #@2e
    .line 274
    sget-object v4, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    #@30
    if-eq v4, v2, :cond_12

    #@32
    .line 276
    sget-object v4, Landroid/net/NetworkInfo$State;->CONNECTING:Landroid/net/NetworkInfo$State;

    #@34
    if-eq v4, v2, :cond_12

    #@36
    .line 278
    sget-object v4, Landroid/net/NetworkInfo$State;->SUSPENDED:Landroid/net/NetworkInfo$State;

    #@38
    if-eq v4, v2, :cond_12

    #@3a
    .line 280
    sget-object v4, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    #@3c
    if-ne v4, v2, :cond_12

    #@3e
    .line 281
    const-string v4, "wifiInfo"

    #@40
    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@43
    move-result-object v3

    #@44
    check-cast v3, Landroid/net/wifi/WifiInfo;

    #@46
    .line 283
    .local v3, wifiInfo:Landroid/net/wifi/WifiInfo;
    if-eqz v3, :cond_4f

    #@48
    .line 284
    invoke-virtual {v3}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    #@4b
    move-result-object v4

    #@4c
    invoke-static {v4}, Lcom/lge/systemservice/service/InfoCollector;->access$502(Ljava/lang/String;)Ljava/lang/String;

    #@4f
    .line 291
    :cond_4f
    invoke-static {}, Lcom/lge/systemservice/service/InfoCollector;->access$500()Ljava/lang/String;

    #@52
    move-result-object v4

    #@53
    invoke-static {}, Lcom/lge/systemservice/service/InfoCollector;->access$600()Ljava/lang/String;

    #@56
    move-result-object v5

    #@57
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5a
    move-result v4

    #@5b
    if-nez v4, :cond_12

    #@5d
    .line 292
    invoke-static {}, Lcom/lge/systemservice/service/InfoCollector;->access$500()Ljava/lang/String;

    #@60
    move-result-object v4

    #@61
    invoke-static {v4}, Lcom/lge/systemservice/service/InfoCollector;->access$602(Ljava/lang/String;)Ljava/lang/String;

    #@64
    .line 293
    iget-object v4, p0, Lcom/lge/systemservice/service/InfoCollector$2;->this$0:Lcom/lge/systemservice/service/InfoCollector;

    #@66
    invoke-static {v4}, Lcom/lge/systemservice/service/InfoCollector;->access$200(Lcom/lge/systemservice/service/InfoCollector;)Z

    #@69
    move-result v4

    #@6a
    if-eqz v4, :cond_12

    #@6c
    .line 299
    iget-object v4, p0, Lcom/lge/systemservice/service/InfoCollector$2;->this$0:Lcom/lge/systemservice/service/InfoCollector;

    #@6e
    invoke-static {v4}, Lcom/lge/systemservice/service/InfoCollector;->access$300(Lcom/lge/systemservice/service/InfoCollector;)V

    #@71
    goto :goto_12
.end method
