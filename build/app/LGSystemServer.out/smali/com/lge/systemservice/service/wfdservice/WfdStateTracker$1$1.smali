.class Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$1$1;
.super Ljava/lang/Object;
.source "WfdStateTracker.java"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$1;->onClick(Landroid/content/DialogInterface;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$1;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$1;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 268
    iput-object p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$1$1;->this$1:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$1;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onFailure(I)V
    .registers 5
    .parameter "reason"

    #@0
    .prologue
    .line 280
    const-string v0, "WfdStateTracker"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Direct disconnect fail, reason: "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 281
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$1$1;->this$1:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$1;

    #@1a
    iget-object v0, v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@1c
    const/4 v1, 0x0

    #@1d
    invoke-static {v0, v1}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;I)V

    #@20
    .line 282
    return-void
.end method

.method public onSuccess()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x2

    #@1
    const/4 v1, 0x1

    #@2
    .line 270
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$1$1;->this$1:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$1;

    #@4
    iget-object v0, v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@6
    invoke-static {v0, v1}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$102(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;Z)Z

    #@9
    .line 272
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$1$1;->this$1:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$1;

    #@b
    iget-object v0, v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@d
    invoke-static {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$200(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)I

    #@10
    move-result v0

    #@11
    if-nez v0, :cond_1b

    #@13
    .line 273
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$1$1;->this$1:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$1;

    #@15
    iget-object v0, v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@17
    invoke-static {v0, v2}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;I)V

    #@1a
    .line 278
    :cond_1a
    :goto_1a
    return-void

    #@1b
    .line 274
    :cond_1b
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$1$1;->this$1:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$1;

    #@1d
    iget-object v0, v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@1f
    invoke-static {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$200(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)I

    #@22
    move-result v0

    #@23
    if-ne v0, v1, :cond_1a

    #@25
    .line 275
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$1$1;->this$1:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$1;

    #@27
    iget-object v0, v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@29
    invoke-static {v0, v2}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;I)V

    #@2c
    .line 276
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$1$1;->this$1:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$1;

    #@2e
    iget-object v0, v0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@30
    const/4 v1, 0x4

    #@31
    invoke-static {v0, v1}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;I)V

    #@34
    goto :goto_1a
.end method
