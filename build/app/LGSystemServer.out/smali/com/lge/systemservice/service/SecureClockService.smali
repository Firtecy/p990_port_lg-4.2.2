.class public Lcom/lge/systemservice/service/SecureClockService;
.super Lcom/lge/systemservice/core/secureclockmanager/ISecureClockManager$Stub;
.source "SecureClockService.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 36
    const-string v0, "ro.build.target_operator"

    #@2
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    const-string v1, "DCM"

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_13

    #@e
    .line 37
    const-string v0, "secureclock_jni"

    #@10
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@13
    .line 39
    :cond_13
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 41
    invoke-direct {p0}, Lcom/lge/systemservice/core/secureclockmanager/ISecureClockManager$Stub;-><init>()V

    #@3
    .line 55
    new-instance v1, Lcom/lge/systemservice/service/SecureClockService$1;

    #@5
    invoke-direct {v1, p0}, Lcom/lge/systemservice/service/SecureClockService$1;-><init>(Lcom/lge/systemservice/service/SecureClockService;)V

    #@8
    iput-object v1, p0, Lcom/lge/systemservice/service/SecureClockService;->mReceiver:Landroid/content/BroadcastReceiver;

    #@a
    .line 42
    iput-object p1, p0, Lcom/lge/systemservice/service/SecureClockService;->mContext:Landroid/content/Context;

    #@c
    .line 44
    invoke-virtual {p0}, Lcom/lge/systemservice/service/SecureClockService;->init()V

    #@f
    .line 46
    const-string v1, "SecureClockService"

    #@11
    const-string v2, "SecureClockService Started!!!!:"

    #@13
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 48
    new-instance v0, Landroid/content/IntentFilter;

    #@18
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@1b
    .line 49
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.NETWORK_SET_TIME"

    #@1d
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@20
    .line 50
    const-string v1, "android.intent.action.TIME_SET"

    #@22
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@25
    .line 51
    iget-object v1, p0, Lcom/lge/systemservice/service/SecureClockService;->mContext:Landroid/content/Context;

    #@27
    iget-object v2, p0, Lcom/lge/systemservice/service/SecureClockService;->mReceiver:Landroid/content/BroadcastReceiver;

    #@29
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@2c
    .line 53
    return-void
.end method

.method private static final native native_init()V
.end method

.method private static native native_setTime(I)I
.end method


# virtual methods
.method public SetTime(I)I
    .registers 5
    .parameter "time"

    #@0
    .prologue
    .line 87
    const-string v0, "SecureClockService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "SetTime() time : "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 89
    invoke-static {p1}, Lcom/lge/systemservice/service/SecureClockService;->native_setTime(I)I

    #@1b
    move-result v0

    #@1c
    return v0
.end method

.method public init()V
    .registers 3

    #@0
    .prologue
    .line 80
    const-string v0, "SecureClockService"

    #@2
    const-string v1, "native_init()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 81
    invoke-static {}, Lcom/lge/systemservice/service/SecureClockService;->native_init()V

    #@a
    .line 83
    return-void
.end method
