.class Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;
.super Ljava/lang/Object;
.source "VolumeVibratorService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VolumeVibration"
.end annotation


# instance fields
.field private mPattern:[J

.field private mRepeat:I

.field private mToken:Landroid/os/IBinder;

.field private final mUid:I

.field private mVolumePattern:[I

.field final synthetic this$0:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;


# direct methods
.method public constructor <init>(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;[JI[ILandroid/os/IBinder;)V
    .registers 8
    .parameter
    .parameter "pattern"
    .parameter "repeat"
    .parameter "volumePattern"
    .parameter "token"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 68
    iput-object p1, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;->this$0:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;

    #@3
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 63
    const/4 v0, -0x1

    #@7
    iput v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;->mRepeat:I

    #@9
    .line 64
    iput-object v1, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;->mVolumePattern:[I

    #@b
    .line 65
    iput-object v1, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;->mPattern:[J

    #@d
    .line 66
    iput-object v1, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;->mToken:Landroid/os/IBinder;

    #@f
    .line 69
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@12
    move-result v0

    #@13
    iput v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;->mUid:I

    #@15
    .line 70
    iput-object p2, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;->mPattern:[J

    #@17
    .line 71
    iput-object p4, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;->mVolumePattern:[I

    #@19
    .line 72
    iput p3, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;->mRepeat:I

    #@1b
    .line 73
    iput-object p5, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;->mToken:Landroid/os/IBinder;

    #@1d
    .line 74
    return-void
.end method

.method static synthetic access$000(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 61
    iget v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;->mRepeat:I

    #@2
    return v0
.end method

.method static synthetic access$100(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;)[J
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 61
    iget-object v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;->mPattern:[J

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;)[I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 61
    iget-object v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;->mVolumePattern:[I

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;)Landroid/os/IBinder;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 61
    iget-object v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;->mToken:Landroid/os/IBinder;

    #@2
    return-object v0
.end method
