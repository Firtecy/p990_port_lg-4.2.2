.class Lcom/lge/systemservice/service/CliptrayServiceWrapper$1;
.super Ljava/lang/Object;
.source "CliptrayServiceWrapper.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/CliptrayServiceWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/CliptrayServiceWrapper;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/CliptrayServiceWrapper;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 86
    iput-object p1, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper$1;->this$0:Lcom/lge/systemservice/service/CliptrayServiceWrapper;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .registers 5
    .parameter "name"
    .parameter "bind"

    #@0
    .prologue
    .line 90
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper$1;->this$0:Lcom/lge/systemservice/service/CliptrayServiceWrapper;

    #@2
    invoke-static {p2}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@5
    move-result-object v1

    #@6
    invoke-static {v0, v1}, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->access$002(Lcom/lge/systemservice/service/CliptrayServiceWrapper;Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;)Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@9
    .line 91
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper$1;->this$0:Lcom/lge/systemservice/service/CliptrayServiceWrapper;

    #@b
    invoke-static {v0}, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->access$000(Lcom/lge/systemservice/service/CliptrayServiceWrapper;)Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@e
    move-result-object v0

    #@f
    if-eqz v0, :cond_17

    #@11
    .line 92
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper$1;->this$0:Lcom/lge/systemservice/service/CliptrayServiceWrapper;

    #@13
    const/4 v1, 0x1

    #@14
    invoke-virtual {v0, v1}, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->setServiceConnected(Z)V

    #@17
    .line 95
    :cond_17
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .registers 4
    .parameter "name"

    #@0
    .prologue
    .line 99
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper$1;->this$0:Lcom/lge/systemservice/service/CliptrayServiceWrapper;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-static {v0, v1}, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->access$002(Lcom/lge/systemservice/service/CliptrayServiceWrapper;Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;)Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@6
    .line 100
    const-string v0, "CliptrayServiceWrapper"

    #@8
    const-string v1, "onServiceDisconnected, mConnected=false "

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 101
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayServiceWrapper$1;->this$0:Lcom/lge/systemservice/service/CliptrayServiceWrapper;

    #@f
    const/4 v1, 0x0

    #@10
    invoke-virtual {v0, v1}, Lcom/lge/systemservice/service/CliptrayServiceWrapper;->setServiceConnected(Z)V

    #@13
    .line 102
    return-void
.end method
