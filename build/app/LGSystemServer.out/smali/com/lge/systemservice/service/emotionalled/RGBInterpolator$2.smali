.class Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;
.super Ljava/lang/Object;
.source "RGBInterpolator.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->startThread_withHandler(Ljava/util/ArrayList;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

.field final synthetic val$playPattern:Ljava/util/ArrayList;

.field final synthetic val$whichLedPlay:I


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;Ljava/util/ArrayList;I)V
    .registers 4
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 116
    iput-object p1, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;->this$0:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@2
    iput-object p2, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;->val$playPattern:Ljava/util/ArrayList;

    #@4
    iput p3, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;->val$whichLedPlay:I

    #@6
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@9
    return-void
.end method


# virtual methods
.method public run()V
    .registers 13

    #@0
    .prologue
    const/4 v11, 0x2

    #@1
    const/4 v10, 0x1

    #@2
    const/4 v7, 0x0

    #@3
    .line 118
    const/4 v4, 0x0

    #@4
    .line 119
    .local v4, tempColor:I
    iget-object v5, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;->val$playPattern:Ljava/util/ArrayList;

    #@6
    if-nez v5, :cond_10

    #@8
    .line 120
    const-string v5, "RGBInterpolator"

    #@a
    const-string v6, "startThread_withHandler thread failed. patternInput is null"

    #@c
    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 168
    :goto_f
    return-void

    #@10
    .line 125
    :cond_10
    iget-object v6, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;->this$0:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@12
    iget-object v5, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;->val$playPattern:Ljava/util/ArrayList;

    #@14
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@17
    move-result-object v5

    #@18
    check-cast v5, Lcom/lge/systemservice/service/emotionalled/RGBFormat;

    #@1a
    invoke-virtual {v6, v5}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->getInputFromUser(Lcom/lge/systemservice/service/emotionalled/RGBFormat;)I

    #@1d
    move-result v3

    #@1e
    .line 126
    .local v3, repeatCount:I
    const/16 v5, 0x14

    #@20
    if-gt v3, v5, :cond_24

    #@22
    if-nez v3, :cond_26

    #@24
    .line 127
    :cond_24
    const/16 v3, 0x14

    #@26
    .line 130
    :cond_26
    :goto_26
    if-lez v3, :cond_153

    #@28
    .line 133
    const/4 v2, 0x1

    #@29
    .local v2, pCount:I
    :goto_29
    iget-object v5, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;->val$playPattern:Ljava/util/ArrayList;

    #@2b
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@2e
    move-result v5

    #@2f
    if-ge v2, v5, :cond_48

    #@31
    .line 134
    iget-object v6, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;->this$0:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@33
    iget-object v5, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;->val$playPattern:Ljava/util/ArrayList;

    #@35
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@38
    move-result-object v5

    #@39
    check-cast v5, Lcom/lge/systemservice/service/emotionalled/RGBFormat;

    #@3b
    invoke-virtual {v6, v5}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->getInputFromUser(Lcom/lge/systemservice/service/emotionalled/RGBFormat;)I

    #@3e
    move-result v5

    #@3f
    if-nez v5, :cond_4b

    #@41
    .line 135
    const-string v5, "RGBInterpolator"

    #@43
    const-string v6, "Error Pattern. Stop the Pattern play"

    #@45
    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 130
    :cond_48
    add-int/lit8 v3, v3, -0x1

    #@4a
    goto :goto_26

    #@4b
    .line 140
    :cond_4b
    const/4 v1, 0x0

    #@4c
    .local v1, i:I
    :goto_4c
    iget-object v5, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;->this$0:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@4e
    invoke-static {v5}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->access$200(Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;)I

    #@51
    move-result v5

    #@52
    if-ge v1, v5, :cond_14f

    #@54
    .line 142
    :try_start_54
    iget-object v5, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;->this$0:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@56
    invoke-static {v5}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->access$300(Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;)[[I

    #@59
    move-result-object v5

    #@5a
    const/4 v6, 0x0

    #@5b
    aget-object v5, v5, v6

    #@5d
    const/4 v6, 0x0

    #@5e
    iget-object v7, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;->this$0:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@60
    invoke-static {v7}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->access$300(Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;)[[I

    #@63
    move-result-object v7

    #@64
    const/4 v8, 0x0

    #@65
    aget-object v7, v7, v8

    #@67
    const/4 v8, 0x0

    #@68
    aget v7, v7, v8

    #@6a
    iget-object v8, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;->this$0:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@6c
    invoke-static {v8}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->access$300(Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;)[[I

    #@6f
    move-result-object v8

    #@70
    const/4 v9, 0x0

    #@71
    aget-object v8, v8, v9

    #@73
    const/4 v9, 0x1

    #@74
    aget v8, v8, v9

    #@76
    add-int/2addr v7, v8

    #@77
    aput v7, v5, v6

    #@79
    .line 143
    iget-object v5, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;->this$0:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@7b
    invoke-static {v5}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->access$300(Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;)[[I

    #@7e
    move-result-object v5

    #@7f
    const/4 v6, 0x1

    #@80
    aget-object v5, v5, v6

    #@82
    const/4 v6, 0x0

    #@83
    iget-object v7, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;->this$0:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@85
    invoke-static {v7}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->access$300(Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;)[[I

    #@88
    move-result-object v7

    #@89
    const/4 v8, 0x1

    #@8a
    aget-object v7, v7, v8

    #@8c
    const/4 v8, 0x0

    #@8d
    aget v7, v7, v8

    #@8f
    iget-object v8, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;->this$0:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@91
    invoke-static {v8}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->access$300(Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;)[[I

    #@94
    move-result-object v8

    #@95
    const/4 v9, 0x1

    #@96
    aget-object v8, v8, v9

    #@98
    const/4 v9, 0x1

    #@99
    aget v8, v8, v9

    #@9b
    add-int/2addr v7, v8

    #@9c
    aput v7, v5, v6

    #@9e
    .line 144
    iget-object v5, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;->this$0:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@a0
    invoke-static {v5}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->access$300(Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;)[[I

    #@a3
    move-result-object v5

    #@a4
    const/4 v6, 0x2

    #@a5
    aget-object v5, v5, v6

    #@a7
    const/4 v6, 0x0

    #@a8
    iget-object v7, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;->this$0:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@aa
    invoke-static {v7}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->access$300(Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;)[[I

    #@ad
    move-result-object v7

    #@ae
    const/4 v8, 0x2

    #@af
    aget-object v7, v7, v8

    #@b1
    const/4 v8, 0x0

    #@b2
    aget v7, v7, v8

    #@b4
    iget-object v8, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;->this$0:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@b6
    invoke-static {v8}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->access$300(Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;)[[I

    #@b9
    move-result-object v8

    #@ba
    const/4 v9, 0x2

    #@bb
    aget-object v8, v8, v9

    #@bd
    const/4 v9, 0x1

    #@be
    aget v8, v8, v9

    #@c0
    add-int/2addr v7, v8

    #@c1
    aput v7, v5, v6

    #@c3
    .line 145
    iget-object v5, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;->this$0:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@c5
    invoke-static {v5}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->access$300(Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;)[[I

    #@c8
    move-result-object v5

    #@c9
    const/4 v6, 0x3

    #@ca
    aget-object v5, v5, v6

    #@cc
    const/4 v6, 0x0

    #@cd
    iget-object v7, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;->this$0:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@cf
    invoke-static {v7}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->access$300(Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;)[[I

    #@d2
    move-result-object v7

    #@d3
    const/4 v8, 0x3

    #@d4
    aget-object v7, v7, v8

    #@d6
    const/4 v8, 0x0

    #@d7
    aget v7, v7, v8

    #@d9
    iget-object v8, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;->this$0:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@db
    invoke-static {v8}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->access$300(Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;)[[I

    #@de
    move-result-object v8

    #@df
    const/4 v9, 0x3

    #@e0
    aget-object v8, v8, v9

    #@e2
    const/4 v9, 0x1

    #@e3
    aget v8, v8, v9

    #@e5
    add-int/2addr v7, v8

    #@e6
    aput v7, v5, v6

    #@e8
    .line 146
    iget-object v5, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;->this$0:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@ea
    invoke-static {v5}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->access$300(Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;)[[I

    #@ed
    move-result-object v5

    #@ee
    const/4 v6, 0x0

    #@ef
    aget-object v5, v5, v6

    #@f1
    const/4 v6, 0x0

    #@f2
    aget v5, v5, v6

    #@f4
    iget-object v6, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;->this$0:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@f6
    invoke-static {v6}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->access$300(Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;)[[I

    #@f9
    move-result-object v6

    #@fa
    const/4 v7, 0x1

    #@fb
    aget-object v6, v6, v7

    #@fd
    const/4 v7, 0x0

    #@fe
    aget v6, v6, v7

    #@100
    iget-object v7, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;->this$0:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@102
    invoke-static {v7}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->access$300(Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;)[[I

    #@105
    move-result-object v7

    #@106
    const/4 v8, 0x2

    #@107
    aget-object v7, v7, v8

    #@109
    const/4 v8, 0x0

    #@10a
    aget v7, v7, v8

    #@10c
    iget-object v8, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;->this$0:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@10e
    invoke-static {v8}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->access$300(Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;)[[I

    #@111
    move-result-object v8

    #@112
    const/4 v9, 0x3

    #@113
    aget-object v8, v8, v9

    #@115
    const/4 v9, 0x0

    #@116
    aget v8, v8, v9

    #@118
    invoke-static {v5, v6, v7, v8}, Landroid/graphics/Color;->argb(IIII)I

    #@11b
    move-result v4

    #@11c
    .line 149
    iget v5, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;->val$whichLedPlay:I

    #@11e
    if-eq v5, v11, :cond_12e

    #@120
    .line 150
    iget-object v5, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;->this$0:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@122
    iget-object v5, v5, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->mRGBDevice:Lcom/lge/systemservice/service/emotionalled/RGBDevice;

    #@124
    iget-object v6, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;->this$0:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@126
    invoke-static {v6}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->access$400(Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;)I

    #@129
    move-result v6

    #@12a
    const/4 v7, 0x0

    #@12b
    invoke-interface {v5, v4, v6, v7}, Lcom/lge/systemservice/service/emotionalled/RGBDevice;->onColorUpdate(III)V

    #@12e
    .line 153
    :cond_12e
    iget v5, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;->val$whichLedPlay:I

    #@130
    if-eq v5, v10, :cond_137

    #@132
    .line 154
    iget-object v5, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;->this$0:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@134
    invoke-static {v5}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->access$500(Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;)V

    #@137
    .line 157
    :cond_137
    iget-object v5, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;->this$0:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@139
    invoke-static {v5}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->access$400(Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;)I

    #@13c
    move-result v5

    #@13d
    int-to-long v5, v5

    #@13e
    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_141
    .catch Ljava/lang/InterruptedException; {:try_start_54 .. :try_end_141} :catch_145

    #@141
    .line 140
    add-int/lit8 v1, v1, 0x1

    #@143
    goto/16 :goto_4c

    #@145
    .line 159
    :catch_145
    move-exception v0

    #@146
    .line 161
    .local v0, e:Ljava/lang/InterruptedException;
    iget-object v5, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;->this$0:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@148
    iget-object v5, v5, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->mRGBDevice:Lcom/lge/systemservice/service/emotionalled/RGBDevice;

    #@14a
    invoke-interface {v5}, Lcom/lge/systemservice/service/emotionalled/RGBDevice;->interrupted()V

    #@14d
    goto/16 :goto_f

    #@14f
    .line 133
    .end local v0           #e:Ljava/lang/InterruptedException;
    :cond_14f
    add-int/lit8 v2, v2, 0x1

    #@151
    goto/16 :goto_29

    #@153
    .line 167
    .end local v1           #i:I
    .end local v2           #pCount:I
    :cond_153
    iget-object v5, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;->this$0:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@155
    iget-object v5, v5, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->mRGBDevice:Lcom/lge/systemservice/service/emotionalled/RGBDevice;

    #@157
    iget v6, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$2;->val$whichLedPlay:I

    #@159
    invoke-interface {v5, v6}, Lcom/lge/systemservice/service/emotionalled/RGBDevice;->onFinish(I)V

    #@15c
    goto/16 :goto_f
.end method
