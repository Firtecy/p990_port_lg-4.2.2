.class Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$1;
.super Ljava/lang/Object;
.source "RGBInterpolator.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->startBackLedThread_withHandler(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

.field final synthetic val$ledOffMS:I

.field final synthetic val$ledOnMS:I


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;II)V
    .registers 4
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 84
    iput-object p1, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$1;->this$0:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@2
    iput p2, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$1;->val$ledOnMS:I

    #@4
    iput p3, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$1;->val$ledOffMS:I

    #@6
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@9
    return-void
.end method


# virtual methods
.method public run()V
    .registers 6

    #@0
    .prologue
    .line 86
    const/4 v0, 0x1

    #@1
    .line 87
    .local v0, bOnOffToggle:Z
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$1;->this$0:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@3
    const/4 v3, 0x1

    #@4
    invoke-static {v2, v3}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->access$002(Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;Z)Z

    #@7
    .line 88
    :goto_7
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$1;->this$0:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@9
    invoke-static {v2}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->access$000(Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;)Z

    #@c
    move-result v2

    #@d
    if-eqz v2, :cond_4c

    #@f
    .line 91
    if-eqz v0, :cond_2b

    #@11
    .line 92
    :try_start_11
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$1;->this$0:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@13
    const-string v3, "/sys/class/leds/button-backlight1/brightness"

    #@15
    const/16 v4, 0xff

    #@17
    invoke-static {v2, v3, v4}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->access$100(Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;Ljava/lang/String;I)Z

    #@1a
    .line 93
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$1;->this$0:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@1c
    const-string v3, "/sys/class/leds/button-backlight2/brightness"

    #@1e
    const/16 v4, 0xff

    #@20
    invoke-static {v2, v3, v4}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->access$100(Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;Ljava/lang/String;I)Z

    #@23
    .line 94
    iget v2, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$1;->val$ledOnMS:I

    #@25
    int-to-long v2, v2

    #@26
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    #@29
    .line 95
    const/4 v0, 0x0

    #@2a
    goto :goto_7

    #@2b
    .line 97
    :cond_2b
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$1;->this$0:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@2d
    const-string v3, "/sys/class/leds/button-backlight1/brightness"

    #@2f
    const/4 v4, 0x0

    #@30
    invoke-static {v2, v3, v4}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->access$100(Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;Ljava/lang/String;I)Z

    #@33
    .line 98
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$1;->this$0:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@35
    const-string v3, "/sys/class/leds/button-backlight2/brightness"

    #@37
    const/4 v4, 0x0

    #@38
    invoke-static {v2, v3, v4}, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->access$100(Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;Ljava/lang/String;I)Z

    #@3b
    .line 99
    iget v2, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$1;->val$ledOffMS:I

    #@3d
    int-to-long v2, v2

    #@3e
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_41
    .catch Ljava/lang/InterruptedException; {:try_start_11 .. :try_end_41} :catch_43

    #@41
    .line 100
    const/4 v0, 0x1

    #@42
    goto :goto_7

    #@43
    .line 102
    :catch_43
    move-exception v1

    #@44
    .line 104
    .local v1, e:Ljava/lang/InterruptedException;
    iget-object v2, p0, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator$1;->this$0:Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;

    #@46
    iget-object v2, v2, Lcom/lge/systemservice/service/emotionalled/RGBInterpolator;->mRGBDevice:Lcom/lge/systemservice/service/emotionalled/RGBDevice;

    #@48
    invoke-interface {v2}, Lcom/lge/systemservice/service/emotionalled/RGBDevice;->interrupted()V

    #@4b
    goto :goto_7

    #@4c
    .line 107
    .end local v1           #e:Ljava/lang/InterruptedException;
    :cond_4c
    return-void
.end method
