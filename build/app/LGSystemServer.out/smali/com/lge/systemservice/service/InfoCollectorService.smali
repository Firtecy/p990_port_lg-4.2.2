.class public Lcom/lge/systemservice/service/InfoCollectorService;
.super Lcom/lge/systemservice/core/infocollectormanager/IInfoCollectorManager$Stub;
.source "InfoCollectorService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/systemservice/service/InfoCollectorService$IncomingHandler;
    }
.end annotation


# static fields
.field private static mATClient:Lcom/lge/systemservice/service/InfoCollectorATClient;

.field static mMPPV:Ljava/lang/String;


# instance fields
.field private final REMOTE_WATCHDOG_START_ACTION:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field final mMessenger:Landroid/os/Messenger;

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 40
    const-string v0, "PV"

    #@2
    sput-object v0, Lcom/lge/systemservice/service/InfoCollectorService;->mMPPV:Ljava/lang/String;

    #@4
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 8
    .parameter "context"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 50
    invoke-direct {p0}, Lcom/lge/systemservice/core/infocollectormanager/IInfoCollectorManager$Stub;-><init>()V

    #@4
    .line 35
    const-string v3, "com.lge.lghms.REMOTE_WATCHDOG_START"

    #@6
    iput-object v3, p0, Lcom/lge/systemservice/service/InfoCollectorService;->REMOTE_WATCHDOG_START_ACTION:Ljava/lang/String;

    #@8
    .line 47
    new-instance v3, Landroid/os/Messenger;

    #@a
    new-instance v4, Lcom/lge/systemservice/service/InfoCollectorService$IncomingHandler;

    #@c
    invoke-direct {v4, p0}, Lcom/lge/systemservice/service/InfoCollectorService$IncomingHandler;-><init>(Lcom/lge/systemservice/service/InfoCollectorService;)V

    #@f
    invoke-direct {v3, v4}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    #@12
    iput-object v3, p0, Lcom/lge/systemservice/service/InfoCollectorService;->mMessenger:Landroid/os/Messenger;

    #@14
    .line 54
    iput-object p1, p0, Lcom/lge/systemservice/service/InfoCollectorService;->mContext:Landroid/content/Context;

    #@16
    .line 55
    iget-object v3, p0, Lcom/lge/systemservice/service/InfoCollectorService;->mContext:Landroid/content/Context;

    #@18
    const-string v4, "phone"

    #@1a
    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1d
    move-result-object v3

    #@1e
    check-cast v3, Landroid/telephony/TelephonyManager;

    #@20
    iput-object v3, p0, Lcom/lge/systemservice/service/InfoCollectorService;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@22
    .line 58
    iget-object v3, p0, Lcom/lge/systemservice/service/InfoCollectorService;->mContext:Landroid/content/Context;

    #@24
    const-string v4, "lghms_prefs"

    #@26
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@29
    move-result-object v2

    #@2a
    .line 60
    .local v2, prefs:Landroid/content/SharedPreferences;
    const-string v3, "NG_flag"

    #@2c
    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    #@2f
    move-result v0

    #@30
    .line 62
    .local v0, NG_flag:Z
    if-nez v0, :cond_3a

    #@32
    .line 64
    :try_start_32
    iget-object v3, p0, Lcom/lge/systemservice/service/InfoCollectorService;->mContext:Landroid/content/Context;

    #@34
    invoke-static {v3}, Lcom/lge/systemservice/service/InfoCollectorATClient;->getInstance(Landroid/content/Context;)Lcom/lge/systemservice/service/InfoCollectorATClient;

    #@37
    move-result-object v3

    #@38
    sput-object v3, Lcom/lge/systemservice/service/InfoCollectorService;->mATClient:Lcom/lge/systemservice/service/InfoCollectorATClient;
    :try_end_3a
    .catch Ljava/lang/Exception; {:try_start_32 .. :try_end_3a} :catch_3b

    #@3a
    .line 75
    :cond_3a
    :goto_3a
    return-void

    #@3b
    .line 65
    :catch_3b
    move-exception v1

    #@3c
    .line 66
    .local v1, e:Ljava/lang/Exception;
    const-string v3, "InfoCollectorService"

    #@3e
    new-instance v4, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v5, "[SPTS] exception get singleton AT instance: "

    #@45
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v4

    #@49
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@4c
    move-result-object v5

    #@4d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v4

    #@51
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v4

    #@55
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@58
    goto :goto_3a
.end method

.method private getAdminToRWD()V
    .registers 5

    #@0
    .prologue
    .line 387
    iget-object v2, p0, Lcom/lge/systemservice/service/InfoCollectorService;->mContext:Landroid/content/Context;

    #@2
    const-string v3, "device_policy"

    #@4
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    #@a
    .line 390
    .local v0, dpm:Landroid/app/admin/DevicePolicyManager;
    new-instance v1, Landroid/content/ComponentName;

    #@c
    const-string v2, "com.lge.lghms"

    #@e
    const-string v3, "com.lge.lghms.AdminReceiver"

    #@10
    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@13
    .line 393
    .local v1, rwdAdmin:Landroid/content/ComponentName;
    :try_start_13
    const-string v2, "com.lge.lghms"

    #@15
    invoke-virtual {v0, v2}, Landroid/app/admin/DevicePolicyManager;->packageHasActiveAdmins(Ljava/lang/String;)Z

    #@18
    move-result v2

    #@19
    if-nez v2, :cond_1f

    #@1b
    .line 397
    const/4 v2, 0x0

    #@1c
    invoke-virtual {v0, v1, v2}, Landroid/app/admin/DevicePolicyManager;->setActiveAdmin(Landroid/content/ComponentName;Z)V
    :try_end_1f
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_1f} :catch_20

    #@1f
    .line 408
    :cond_1f
    :goto_1f
    return-void

    #@20
    .line 403
    :catch_20
    move-exception v2

    #@21
    goto :goto_1f
.end method

.method private isIMPLEnabled()Z
    .registers 10

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 246
    const/4 v2, 0x0

    #@3
    .line 247
    .local v2, impl:Ljava/lang/String;
    const/4 v3, 0x0

    #@4
    .line 249
    .local v3, response:Lcom/lge/systemservice/service/InfoCollectorATClient$Response;
    :try_start_4
    sget-object v6, Lcom/lge/systemservice/service/InfoCollectorService;->mATClient:Lcom/lge/systemservice/service/InfoCollectorATClient;

    #@6
    const/16 v7, 0x232a

    #@8
    const-string v8, ""

    #@a
    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    #@d
    move-result-object v8

    #@e
    invoke-virtual {v6, v7, v8}, Lcom/lge/systemservice/service/InfoCollectorATClient;->request(I[B)Lcom/lge/systemservice/service/InfoCollectorATClient$Response;

    #@11
    move-result-object v3

    #@12
    .line 250
    const-wide/16 v6, 0x64

    #@14
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_17} :catch_1a

    #@17
    .line 255
    if-nez v3, :cond_1f

    #@19
    .line 274
    :cond_19
    :goto_19
    return v4

    #@1a
    .line 251
    :catch_1a
    move-exception v1

    #@1b
    .line 252
    .local v1, ie:Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    #@1e
    goto :goto_19

    #@1f
    .line 261
    .end local v1           #ie:Ljava/lang/Exception;
    :cond_1f
    new-instance v2, Ljava/lang/String;

    #@21
    .end local v2           #impl:Ljava/lang/String;
    iget-object v6, v3, Lcom/lge/systemservice/service/InfoCollectorATClient$Response;->data:[B

    #@23
    invoke-direct {v2, v6}, Ljava/lang/String;-><init>([B)V

    #@26
    .line 263
    .restart local v2       #impl:Ljava/lang/String;
    const/4 v6, 0x0

    #@27
    :try_start_27
    iget v7, v3, Lcom/lge/systemservice/service/InfoCollectorATClient$Response;->length:I

    #@29
    invoke-virtual {v2, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_2c
    .catch Ljava/lang/Exception; {:try_start_27 .. :try_end_2c} :catch_37

    #@2c
    move-result-object v2

    #@2d
    .line 271
    const-string v6, "1"

    #@2f
    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@32
    move-result v6

    #@33
    if-nez v6, :cond_19

    #@35
    move v4, v5

    #@36
    .line 274
    goto :goto_19

    #@37
    .line 264
    :catch_37
    move-exception v0

    #@38
    .line 265
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@3b
    goto :goto_19
.end method

.method private isQFuseEnabled()Z
    .registers 10

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 213
    const/4 v2, 0x0

    #@3
    .line 214
    .local v2, isQfuse:Ljava/lang/String;
    const/4 v3, 0x0

    #@4
    .line 216
    .local v3, response:Lcom/lge/systemservice/service/InfoCollectorATClient$Response;
    :try_start_4
    sget-object v6, Lcom/lge/systemservice/service/InfoCollectorService;->mATClient:Lcom/lge/systemservice/service/InfoCollectorATClient;

    #@6
    const/16 v7, 0x2329

    #@8
    const-string v8, ""

    #@a
    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    #@d
    move-result-object v8

    #@e
    invoke-virtual {v6, v7, v8}, Lcom/lge/systemservice/service/InfoCollectorATClient;->request(I[B)Lcom/lge/systemservice/service/InfoCollectorATClient$Response;

    #@11
    move-result-object v3

    #@12
    .line 217
    const-wide/16 v6, 0x64

    #@14
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_17} :catch_1a

    #@17
    .line 222
    if-nez v3, :cond_1f

    #@19
    .line 241
    :cond_19
    :goto_19
    return v4

    #@1a
    .line 218
    :catch_1a
    move-exception v1

    #@1b
    .line 219
    .local v1, ie:Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    #@1e
    goto :goto_19

    #@1f
    .line 228
    .end local v1           #ie:Ljava/lang/Exception;
    :cond_1f
    new-instance v2, Ljava/lang/String;

    #@21
    .end local v2           #isQfuse:Ljava/lang/String;
    iget-object v6, v3, Lcom/lge/systemservice/service/InfoCollectorATClient$Response;->data:[B

    #@23
    invoke-direct {v2, v6}, Ljava/lang/String;-><init>([B)V

    #@26
    .line 230
    .restart local v2       #isQfuse:Ljava/lang/String;
    const/4 v6, 0x0

    #@27
    :try_start_27
    iget v7, v3, Lcom/lge/systemservice/service/InfoCollectorATClient$Response;->length:I

    #@29
    invoke-virtual {v2, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_2c
    .catch Ljava/lang/Exception; {:try_start_27 .. :try_end_2c} :catch_37

    #@2c
    move-result-object v2

    #@2d
    .line 238
    const-string v6, "1"

    #@2f
    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@32
    move-result v6

    #@33
    if-nez v6, :cond_19

    #@35
    move v4, v5

    #@36
    .line 241
    goto :goto_19

    #@37
    .line 231
    :catch_37
    move-exception v0

    #@38
    .line 232
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@3b
    goto :goto_19
.end method

.method private isRunningProcess(Landroid/content/Context;Ljava/lang/String;)Z
    .registers 9
    .parameter "context"
    .parameter "packageName"

    #@0
    .prologue
    .line 123
    const/4 v2, 0x0

    #@1
    .line 124
    .local v2, isRunning:Z
    const-string v5, "activity"

    #@3
    invoke-virtual {p1, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Landroid/app/ActivityManager;

    #@9
    .line 126
    .local v0, actMng:Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    #@c
    move-result-object v3

    #@d
    .line 127
    .local v3, list:Ljava/util/List;,"Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    if-nez v3, :cond_11

    #@f
    .line 128
    const/4 v5, 0x0

    #@10
    .line 139
    :goto_10
    return v5

    #@11
    .line 130
    :cond_11
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@14
    move-result-object v1

    #@15
    .local v1, i$:Ljava/util/Iterator;
    :cond_15
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@18
    move-result v5

    #@19
    if-eqz v5, :cond_2a

    #@1b
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1e
    move-result-object v4

    #@1f
    check-cast v4, Landroid/app/ActivityManager$RunningAppProcessInfo;

    #@21
    .line 131
    .local v4, rap:Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget-object v5, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    #@23
    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@26
    move-result v5

    #@27
    if-eqz v5, :cond_15

    #@29
    .line 135
    const/4 v2, 0x1

    #@2a
    .end local v4           #rap:Landroid/app/ActivityManager$RunningAppProcessInfo;
    :cond_2a
    move v5, v2

    #@2b
    .line 139
    goto :goto_10
.end method

.method private isSPTUnlock()Z
    .registers 10

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 178
    const/4 v2, 0x0

    #@3
    .line 179
    .local v2, isSPT:Ljava/lang/String;
    const/4 v3, 0x0

    #@4
    .line 181
    .local v3, response:Lcom/lge/systemservice/service/InfoCollectorATClient$Response;
    :try_start_4
    sget-object v6, Lcom/lge/systemservice/service/InfoCollectorService;->mATClient:Lcom/lge/systemservice/service/InfoCollectorATClient;

    #@6
    const/16 v7, 0x232c

    #@8
    const-string v8, ""

    #@a
    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    #@d
    move-result-object v8

    #@e
    invoke-virtual {v6, v7, v8}, Lcom/lge/systemservice/service/InfoCollectorATClient;->request(I[B)Lcom/lge/systemservice/service/InfoCollectorATClient$Response;

    #@11
    move-result-object v3

    #@12
    .line 182
    const-wide/16 v6, 0x64

    #@14
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_17} :catch_1a

    #@17
    .line 187
    if-nez v3, :cond_1f

    #@19
    .line 207
    :goto_19
    return v4

    #@1a
    .line 183
    :catch_1a
    move-exception v1

    #@1b
    .line 184
    .local v1, ie:Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    #@1e
    goto :goto_19

    #@1f
    .line 193
    .end local v1           #ie:Ljava/lang/Exception;
    :cond_1f
    new-instance v2, Ljava/lang/String;

    #@21
    .end local v2           #isSPT:Ljava/lang/String;
    iget-object v6, v3, Lcom/lge/systemservice/service/InfoCollectorATClient$Response;->data:[B

    #@23
    invoke-direct {v2, v6}, Ljava/lang/String;-><init>([B)V

    #@26
    .line 195
    .restart local v2       #isSPT:Ljava/lang/String;
    const/4 v6, 0x0

    #@27
    :try_start_27
    iget v7, v3, Lcom/lge/systemservice/service/InfoCollectorATClient$Response;->length:I

    #@29
    invoke-virtual {v2, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_2c
    .catch Ljava/lang/Exception; {:try_start_27 .. :try_end_2c} :catch_3a

    #@2c
    move-result-object v2

    #@2d
    .line 203
    const-string v6, "SPT UNLOCKED"

    #@2f
    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@32
    move-result v6

    #@33
    if-eqz v6, :cond_3f

    #@35
    .line 204
    const-string v5, "MP"

    #@37
    sput-object v5, Lcom/lge/systemservice/service/InfoCollectorService;->mMPPV:Ljava/lang/String;

    #@39
    goto :goto_19

    #@3a
    .line 196
    :catch_3a
    move-exception v0

    #@3b
    .line 197
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@3e
    goto :goto_19

    #@3f
    .end local v0           #e:Ljava/lang/Exception;
    :cond_3f
    move v4, v5

    #@40
    .line 207
    goto :goto_19
.end method

.method private isStartingRWD()Z
    .registers 5

    #@0
    .prologue
    .line 143
    invoke-direct {p0}, Lcom/lge/systemservice/service/InfoCollectorService;->isQFuseEnabled()Z

    #@3
    move-result v1

    #@4
    .line 144
    .local v1, isQFuseEnabled:Z
    invoke-direct {p0}, Lcom/lge/systemservice/service/InfoCollectorService;->isIMPLEnabled()Z

    #@7
    move-result v0

    #@8
    .line 145
    .local v0, isIMPLEnabled:Z
    invoke-direct {p0}, Lcom/lge/systemservice/service/InfoCollectorService;->isSPTUnlock()Z

    #@b
    move-result v2

    #@c
    .line 162
    .local v2, isSPTUnLocked:Z
    if-eqz v1, :cond_14

    #@e
    .line 163
    if-eqz v0, :cond_14

    #@10
    .line 164
    if-eqz v2, :cond_14

    #@12
    .line 165
    const/4 v3, 0x0

    #@13
    .line 170
    :goto_13
    return v3

    #@14
    :cond_14
    const/4 v3, 0x1

    #@15
    goto :goto_13
.end method

.method private isTestSim()Z
    .registers 8

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 339
    const/4 v2, 0x0

    #@2
    .line 341
    .local v2, mccmnc:Ljava/lang/String;
    :try_start_2
    const-string v5, "persist.radio.camped_mccmnc"

    #@4
    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;
    :try_end_7
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_7} :catch_17

    #@7
    move-result-object v2

    #@8
    .line 349
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@b
    move-result v5

    #@c
    const/4 v6, 0x5

    #@d
    if-ge v5, v6, :cond_19

    #@f
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@12
    move-result v5

    #@13
    const/4 v6, 0x6

    #@14
    if-le v5, v6, :cond_19

    #@16
    .line 380
    :cond_16
    :goto_16
    return v4

    #@17
    .line 342
    :catch_17
    move-exception v0

    #@18
    .line 346
    .local v0, e:Ljava/lang/NullPointerException;
    goto :goto_16

    #@19
    .line 356
    .end local v0           #e:Ljava/lang/NullPointerException;
    :cond_19
    const/4 v1, 0x0

    #@1a
    .line 358
    .local v1, mcc:Ljava/lang/String;
    const/4 v5, 0x0

    #@1b
    const/4 v6, 0x2

    #@1c
    :try_start_1c
    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_1f
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_1f} :catch_39

    #@1f
    move-result-object v1

    #@20
    .line 366
    const-string v5, "001"

    #@22
    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25
    move-result v5

    #@26
    if-eqz v5, :cond_16

    #@28
    .line 367
    const/4 v5, 0x3

    #@29
    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@2c
    move-result-object v5

    #@2d
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@30
    move-result v3

    #@31
    .line 371
    .local v3, mnc:I
    const/16 v5, 0x12

    #@33
    if-ge v3, v5, :cond_16

    #@35
    if-lez v3, :cond_16

    #@37
    .line 372
    const/4 v4, 0x1

    #@38
    goto :goto_16

    #@39
    .line 359
    .end local v3           #mnc:I
    :catch_39
    move-exception v0

    #@3a
    .line 363
    .local v0, e:Ljava/lang/Exception;
    goto :goto_16
.end method


# virtual methods
.method public startInfoCollecting()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 104
    new-instance v0, Lcom/lge/systemservice/service/InfoCollector;

    #@2
    iget-object v1, p0, Lcom/lge/systemservice/service/InfoCollectorService;->mContext:Landroid/content/Context;

    #@4
    invoke-direct {v0, v1}, Lcom/lge/systemservice/service/InfoCollector;-><init>(Landroid/content/Context;)V

    #@7
    .line 105
    .local v0, info:Lcom/lge/systemservice/service/InfoCollector;
    invoke-virtual {v0}, Lcom/lge/systemservice/service/InfoCollector;->startInfoCollecting()V

    #@a
    .line 106
    return-void
.end method

.method public startRWD()V
    .registers 4

    #@0
    .prologue
    .line 78
    invoke-direct {p0}, Lcom/lge/systemservice/service/InfoCollectorService;->isStartingRWD()Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_2f

    #@6
    iget-object v1, p0, Lcom/lge/systemservice/service/InfoCollectorService;->mContext:Landroid/content/Context;

    #@8
    const-string v2, "com.lge.lghms"

    #@a
    invoke-direct {p0, v1, v2}, Lcom/lge/systemservice/service/InfoCollectorService;->isRunningProcess(Landroid/content/Context;Ljava/lang/String;)Z

    #@d
    move-result v1

    #@e
    if-nez v1, :cond_2f

    #@10
    .line 81
    invoke-direct {p0}, Lcom/lge/systemservice/service/InfoCollectorService;->isTestSim()Z

    #@13
    move-result v1

    #@14
    if-nez v1, :cond_2f

    #@16
    .line 82
    const-string v1, "IC"

    #@18
    const-string v2, "start RWD"

    #@1a
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 83
    new-instance v0, Landroid/content/Intent;

    #@1f
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@22
    const-string v1, "com.lge.lghms.REMOTE_WATCHDOG_START"

    #@24
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@27
    .line 84
    .local v0, intent:Landroid/content/Intent;
    iget-object v1, p0, Lcom/lge/systemservice/service/InfoCollectorService;->mContext:Landroid/content/Context;

    #@29
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@2c
    .line 86
    invoke-direct {p0}, Lcom/lge/systemservice/service/InfoCollectorService;->getAdminToRWD()V

    #@2f
    .line 97
    .end local v0           #intent:Landroid/content/Intent;
    :cond_2f
    return-void
.end method
