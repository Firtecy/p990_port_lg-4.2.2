.class Lcom/lge/systemservice/service/LGSDEncService$1;
.super Landroid/os/Handler;
.source "LGSDEncService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/LGSDEncService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/LGSDEncService;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/LGSDEncService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 57
    iput-object p1, p0, Lcom/lge/systemservice/service/LGSDEncService$1;->this$0:Lcom/lge/systemservice/service/LGSDEncService;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 6
    .parameter "msg"

    #@0
    .prologue
    .line 59
    const-string v1, "LGSDEncService"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "handleMessage msg "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 61
    new-instance v0, Landroid/content/Intent;

    #@1a
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@1d
    .line 62
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "com.android.settings"

    #@1f
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@21
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@28
    .line 63
    const/high16 v1, 0x1000

    #@2a
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@2d
    .line 64
    iget-object v1, p0, Lcom/lge/systemservice/service/LGSDEncService$1;->this$0:Lcom/lge/systemservice/service/LGSDEncService;

    #@2f
    invoke-static {v1}, Lcom/lge/systemservice/service/LGSDEncService;->access$000(Lcom/lge/systemservice/service/LGSDEncService;)Landroid/content/Context;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@36
    .line 65
    return-void
.end method
