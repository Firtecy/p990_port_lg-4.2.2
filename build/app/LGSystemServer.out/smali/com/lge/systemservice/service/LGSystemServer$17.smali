.class final Lcom/lge/systemservice/service/LGSystemServer$17;
.super Lcom/lge/systemservice/core/LGServiceStubFetcher;
.source "LGSystemServer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/LGSystemServer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 299
    invoke-direct {p0, p1}, Lcom/lge/systemservice/core/LGServiceStubFetcher;-><init>(Ljava/lang/String;)V

    #@3
    return-void
.end method


# virtual methods
.method public createServiceStub(Landroid/content/Context;)Landroid/os/IBinder;
    .registers 3
    .parameter "ctx"

    #@0
    .prologue
    .line 303
    new-instance v0, Lcom/lge/systemservice/service/CoreControlService;

    #@2
    invoke-direct {v0, p1}, Lcom/lge/systemservice/service/CoreControlService;-><init>(Landroid/content/Context;)V

    #@5
    sput-object v0, Lcom/lge/systemservice/service/LGSystemServer;->coreControlService:Lcom/lge/systemservice/service/CoreControlService;

    #@7
    .line 304
    sget-object v0, Lcom/lge/systemservice/service/LGSystemServer;->coreControlService:Lcom/lge/systemservice/service/CoreControlService;

    #@9
    if-eqz v0, :cond_13

    #@b
    .line 305
    sget-object v0, Lcom/lge/systemservice/service/LGSystemServer;->coreControlService:Lcom/lge/systemservice/service/CoreControlService;

    #@d
    invoke-virtual {v0}, Lcom/lge/systemservice/service/CoreControlService;->registerEcoModeObserver()V

    #@10
    .line 306
    sget-object v0, Lcom/lge/systemservice/service/LGSystemServer;->coreControlService:Lcom/lge/systemservice/service/CoreControlService;

    #@12
    .line 308
    :goto_12
    return-object v0

    #@13
    :cond_13
    const/4 v0, 0x0

    #@14
    goto :goto_12
.end method
