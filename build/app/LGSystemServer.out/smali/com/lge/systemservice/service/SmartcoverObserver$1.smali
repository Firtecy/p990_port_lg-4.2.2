.class Lcom/lge/systemservice/service/SmartcoverObserver$1;
.super Landroid/os/Handler;
.source "SmartCoverObserver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/SmartcoverObserver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/SmartcoverObserver;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/SmartcoverObserver;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 223
    iput-object p1, p0, Lcom/lge/systemservice/service/SmartcoverObserver$1;->this$0:Lcom/lge/systemservice/service/SmartcoverObserver;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 7
    .parameter "msg"

    #@0
    .prologue
    .line 226
    iget v2, p1, Landroid/os/Message;->what:I

    #@2
    if-nez v2, :cond_37

    #@4
    .line 227
    monitor-enter p0

    #@5
    .line 228
    :try_start_5
    invoke-static {}, Lcom/lge/systemservice/service/SmartcoverObserver;->access$000()Ljava/lang/String;

    #@8
    move-result-object v2

    #@9
    new-instance v3, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v4, "smartcover state Bit changed: "

    #@10
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v3

    #@14
    iget-object v4, p0, Lcom/lge/systemservice/service/SmartcoverObserver$1;->this$0:Lcom/lge/systemservice/service/SmartcoverObserver;

    #@16
    invoke-static {v4}, Lcom/lge/systemservice/service/SmartcoverObserver;->access$100(Lcom/lge/systemservice/service/SmartcoverObserver;)I

    #@19
    move-result v4

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v3

    #@22
    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 230
    iget-object v2, p0, Lcom/lge/systemservice/service/SmartcoverObserver$1;->this$0:Lcom/lge/systemservice/service/SmartcoverObserver;

    #@27
    invoke-static {v2}, Lcom/lge/systemservice/service/SmartcoverObserver;->access$200(Lcom/lge/systemservice/service/SmartcoverObserver;)I

    #@2a
    move-result v2

    #@2b
    if-nez v2, :cond_38

    #@2d
    .line 231
    invoke-static {}, Lcom/lge/systemservice/service/SmartcoverObserver;->access$000()Ljava/lang/String;

    #@30
    move-result-object v2

    #@31
    const-string v3, "mSState is 0, skipping smartcover broadcast"

    #@33
    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 232
    monitor-exit p0

    #@37
    .line 265
    :cond_37
    :goto_37
    return-void

    #@38
    .line 235
    :cond_38
    const-string v2, "ro.factorytest"

    #@3a
    const/4 v3, 0x0

    #@3b
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@3e
    move-result v2

    #@3f
    const/4 v3, 0x2

    #@40
    if-ne v2, v3, :cond_50

    #@42
    .line 236
    invoke-static {}, Lcom/lge/systemservice/service/SmartcoverObserver;->access$000()Ljava/lang/String;

    #@45
    move-result-object v2

    #@46
    const-string v3, "On FTM test, skipping smartcover broadcast"

    #@48
    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    .line 237
    monitor-exit p0

    #@4c
    goto :goto_37

    #@4d
    .line 263
    :catchall_4d
    move-exception v2

    #@4e
    monitor-exit p0
    :try_end_4f
    .catchall {:try_start_5 .. :try_end_4f} :catchall_4d

    #@4f
    throw v2

    #@50
    .line 240
    :cond_50
    :try_start_50
    const-string v2, "sys.allautotest.run"

    #@52
    const/4 v3, 0x0

    #@53
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@56
    move-result v2

    #@57
    if-eqz v2, :cond_64

    #@59
    .line 241
    invoke-static {}, Lcom/lge/systemservice/service/SmartcoverObserver;->access$000()Ljava/lang/String;

    #@5c
    move-result-object v2

    #@5d
    const-string v3, "On AAT test, skipping smartcover broadcast"

    #@5f
    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@62
    .line 242
    monitor-exit p0

    #@63
    goto :goto_37

    #@64
    .line 245
    :cond_64
    iget-object v2, p0, Lcom/lge/systemservice/service/SmartcoverObserver$1;->this$0:Lcom/lge/systemservice/service/SmartcoverObserver;

    #@66
    invoke-static {v2}, Lcom/lge/systemservice/service/SmartcoverObserver;->access$300(Lcom/lge/systemservice/service/SmartcoverObserver;)Landroid/content/Context;

    #@69
    move-result-object v2

    #@6a
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6d
    move-result-object v0

    #@6e
    .line 247
    .local v0, cr:Landroid/content/ContentResolver;
    const-string v2, "device_provisioned"

    #@70
    const/4 v3, 0x0

    #@71
    invoke-static {v0, v2, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@74
    move-result v2

    #@75
    if-nez v2, :cond_82

    #@77
    .line 248
    invoke-static {}, Lcom/lge/systemservice/service/SmartcoverObserver;->access$000()Ljava/lang/String;

    #@7a
    move-result-object v2

    #@7b
    const-string v3, "Device not provisioned, skipping smartcover broadcast"

    #@7d
    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@80
    .line 249
    monitor-exit p0

    #@81
    goto :goto_37

    #@82
    .line 256
    :cond_82
    new-instance v1, Landroid/content/Intent;

    #@84
    const-string v2, "com.lge.android.intent.action.ACCESSORY_EVENT"

    #@86
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@89
    .line 257
    .local v1, intent:Landroid/content/Intent;
    const/high16 v2, 0x2000

    #@8b
    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@8e
    .line 258
    const-string v2, "com.lge.android.intent.extra.ACCESSORY_STATE"

    #@90
    iget-object v3, p0, Lcom/lge/systemservice/service/SmartcoverObserver$1;->this$0:Lcom/lge/systemservice/service/SmartcoverObserver;

    #@92
    invoke-static {v3}, Lcom/lge/systemservice/service/SmartcoverObserver;->access$200(Lcom/lge/systemservice/service/SmartcoverObserver;)I

    #@95
    move-result v3

    #@96
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@99
    .line 259
    const-string v2, "com.lge.android.intent.extra.PREVIOUS_ACCESSORY_STATE"

    #@9b
    iget-object v3, p0, Lcom/lge/systemservice/service/SmartcoverObserver$1;->this$0:Lcom/lge/systemservice/service/SmartcoverObserver;

    #@9d
    invoke-static {v3}, Lcom/lge/systemservice/service/SmartcoverObserver;->access$400(Lcom/lge/systemservice/service/SmartcoverObserver;)I

    #@a0
    move-result v3

    #@a1
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@a4
    .line 260
    const-string v2, "com.lge.android.intent.extra.ACCESSORY_BIT_STATE"

    #@a6
    iget-object v3, p0, Lcom/lge/systemservice/service/SmartcoverObserver$1;->this$0:Lcom/lge/systemservice/service/SmartcoverObserver;

    #@a8
    invoke-static {v3}, Lcom/lge/systemservice/service/SmartcoverObserver;->access$100(Lcom/lge/systemservice/service/SmartcoverObserver;)I

    #@ab
    move-result v3

    #@ac
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@af
    .line 262
    iget-object v2, p0, Lcom/lge/systemservice/service/SmartcoverObserver$1;->this$0:Lcom/lge/systemservice/service/SmartcoverObserver;

    #@b1
    invoke-static {v2}, Lcom/lge/systemservice/service/SmartcoverObserver;->access$300(Lcom/lge/systemservice/service/SmartcoverObserver;)Landroid/content/Context;

    #@b4
    move-result-object v2

    #@b5
    invoke-virtual {v2, v1}, Landroid/content/Context;->sendStickyBroadcast(Landroid/content/Intent;)V

    #@b8
    .line 263
    monitor-exit p0
    :try_end_b9
    .catchall {:try_start_50 .. :try_end_b9} :catchall_4d

    #@b9
    goto/16 :goto_37
.end method
