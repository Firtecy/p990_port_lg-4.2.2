.class Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;
.super Landroid/app/AlertDialog;
.source "WfdIntroDialog.java"


# instance fields
.field private currentOrientation:Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;

.field private landView:Landroid/view/View;

.field private final mCancelListener:Landroid/content/DialogInterface$OnCancelListener;

.field private final mCheckBoxListener:Landroid/view/View$OnClickListener;

.field private final mClickListener:Landroid/content/DialogInterface$OnClickListener;

.field private mContext:Landroid/content/Context;

.field private mDisplay:Landroid/view/Display;

.field mOrientationEventListener:Landroid/view/OrientationEventListener;

.field private mView:Landroid/view/View;

.field private mWindowManager:Landroid/view/WindowManager;

.field private portView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;Landroid/view/View$OnClickListener;)V
    .registers 8
    .parameter "context"
    .parameter "theme"
    .parameter "click_listener"
    .parameter "cancel_listener"
    .parameter "checkbox_listener"

    #@0
    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;I)V

    #@3
    .line 60
    sget-object v0, Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;->ORIENTATION_UNKNOWN:Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;

    #@5
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->currentOrientation:Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;

    #@7
    .line 69
    iput-object p3, p0, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->mClickListener:Landroid/content/DialogInterface$OnClickListener;

    #@9
    .line 70
    iput-object p4, p0, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->mCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    #@b
    .line 71
    iput-object p5, p0, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->mCheckBoxListener:Landroid/view/View$OnClickListener;

    #@d
    .line 72
    iput-object p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->mContext:Landroid/content/Context;

    #@f
    .line 74
    new-instance v0, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog$1;

    #@11
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->mContext:Landroid/content/Context;

    #@13
    invoke-direct {v0, p0, v1}, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog$1;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;Landroid/content/Context;)V

    #@16
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->mOrientationEventListener:Landroid/view/OrientationEventListener;

    #@18
    .line 80
    return-void
.end method

.method static synthetic access$000(Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;)Landroid/view/Display;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->mDisplay:Landroid/view/Display;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->setRotationView(I)V

    #@3
    return-void
.end method

.method private setRotationView(I)V
    .registers 6
    .parameter "rotation"

    #@0
    .prologue
    const/16 v3, 0x8

    #@2
    const/4 v2, 0x0

    #@3
    .line 115
    const/4 v0, 0x1

    #@4
    if-eq p1, v0, :cond_9

    #@6
    const/4 v0, 0x3

    #@7
    if-ne p1, v0, :cond_25

    #@9
    .line 116
    :cond_9
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->currentOrientation:Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;

    #@b
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;->ORIENTATION_LAND:Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;

    #@d
    if-eq v0, v1, :cond_24

    #@f
    .line 117
    const-string v0, "WfdIntroDialog"

    #@11
    const-string v1, "landscape"

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 118
    sget-object v0, Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;->ORIENTATION_LAND:Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;

    #@18
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->currentOrientation:Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;

    #@1a
    .line 119
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->portView:Landroid/view/View;

    #@1c
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    #@1f
    .line 120
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->landView:Landroid/view/View;

    #@21
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    #@24
    .line 130
    :cond_24
    :goto_24
    return-void

    #@25
    .line 123
    :cond_25
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->currentOrientation:Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;

    #@27
    sget-object v1, Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;->ORIENTATION_PORT:Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;

    #@29
    if-eq v0, v1, :cond_24

    #@2b
    .line 124
    const-string v0, "WfdIntroDialog"

    #@2d
    const-string v1, "portrait"

    #@2f
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 125
    sget-object v0, Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;->ORIENTATION_PORT:Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;

    #@34
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->currentOrientation:Lcom/lge/systemservice/service/wfdservice/ScreenOrientation;

    #@36
    .line 126
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->portView:Landroid/view/View;

    #@38
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    #@3b
    .line 127
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->landView:Landroid/view/View;

    #@3d
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    #@40
    goto :goto_24
.end method


# virtual methods
.method public onBackPressed()V
    .registers 2

    #@0
    .prologue
    .line 108
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->mOrientationEventListener:Landroid/view/OrientationEventListener;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 109
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->mOrientationEventListener:Landroid/view/OrientationEventListener;

    #@6
    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    #@9
    .line 111
    :cond_9
    invoke-super {p0}, Landroid/app/AlertDialog;->onBackPressed()V

    #@c
    .line 112
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 6
    .parameter "savedInstanceState"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 84
    invoke-virtual {p0}, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->getLayoutInflater()Landroid/view/LayoutInflater;

    #@4
    move-result-object v0

    #@5
    const v1, 0x2030025

    #@8
    const/4 v2, 0x0

    #@9
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@c
    move-result-object v0

    #@d
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->mView:Landroid/view/View;

    #@f
    .line 85
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->mView:Landroid/view/View;

    #@11
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->setView(Landroid/view/View;)V

    #@14
    .line 86
    invoke-virtual {p0, v3}, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->setInverseBackgroundForced(Z)V

    #@17
    .line 87
    const v0, 0x2090392

    #@1a
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->setTitle(I)V

    #@1d
    .line 88
    const/4 v0, -0x1

    #@1e
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->mContext:Landroid/content/Context;

    #@20
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@23
    move-result-object v1

    #@24
    const v2, 0x2090254

    #@27
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->mClickListener:Landroid/content/DialogInterface$OnClickListener;

    #@2d
    invoke-virtual {p0, v0, v1, v2}, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    #@30
    .line 89
    invoke-virtual {p0, v3}, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->setCancelable(Z)V

    #@33
    .line 90
    const/4 v0, 0x0

    #@34
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->setCanceledOnTouchOutside(Z)V

    #@37
    .line 91
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->mCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    #@39
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    #@3c
    .line 93
    invoke-super {p0, p1}, Landroid/app/AlertDialog;->onCreate(Landroid/os/Bundle;)V

    #@3f
    .line 95
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->mView:Landroid/view/View;

    #@41
    const v1, 0x20d007b

    #@44
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@47
    move-result-object v0

    #@48
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->portView:Landroid/view/View;

    #@4a
    .line 96
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->mView:Landroid/view/View;

    #@4c
    const v1, 0x20d007f

    #@4f
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@52
    move-result-object v0

    #@53
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->landView:Landroid/view/View;

    #@55
    .line 97
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->mContext:Landroid/content/Context;

    #@57
    const-string v1, "window"

    #@59
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@5c
    move-result-object v0

    #@5d
    check-cast v0, Landroid/view/WindowManager;

    #@5f
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->mWindowManager:Landroid/view/WindowManager;

    #@61
    .line 98
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->mWindowManager:Landroid/view/WindowManager;

    #@63
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    #@66
    move-result-object v0

    #@67
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->mDisplay:Landroid/view/Display;

    #@69
    .line 99
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->mDisplay:Landroid/view/Display;

    #@6b
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    #@6e
    move-result v0

    #@6f
    invoke-direct {p0, v0}, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->setRotationView(I)V

    #@72
    .line 101
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->mOrientationEventListener:Landroid/view/OrientationEventListener;

    #@74
    if-eqz v0, :cond_7b

    #@76
    .line 102
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdIntroDialog;->mOrientationEventListener:Landroid/view/OrientationEventListener;

    #@78
    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->enable()V

    #@7b
    .line 104
    :cond_7b
    return-void
.end method
