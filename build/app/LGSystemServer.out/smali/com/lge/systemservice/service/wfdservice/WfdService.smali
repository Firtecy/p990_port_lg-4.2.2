.class public Lcom/lge/systemservice/service/wfdservice/WfdService;
.super Lcom/lge/systemservice/core/wfdmanager/IWfdManager$Stub;
.source "WfdService.java"


# instance fields
.field private mBattLow:Z

.field private mContext:Landroid/content/Context;

.field private final mIntentFilter:Landroid/content/IntentFilter;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mWfdStateTracker:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

.field private mWifiP2pState:I

.field private mWifiState:I

.field private wfdInterfaceState:I

.field private wifiConnected:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)V
    .registers 6
    .parameter "context"
    .parameter "tracker"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 240
    invoke-direct {p0}, Lcom/lge/systemservice/core/wfdmanager/IWfdManager$Stub;-><init>()V

    #@5
    .line 63
    iput v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mWifiState:I

    #@7
    .line 64
    iput v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mWifiP2pState:I

    #@9
    .line 65
    iput-boolean v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mBattLow:Z

    #@b
    .line 68
    new-instance v0, Landroid/content/IntentFilter;

    #@d
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@10
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mIntentFilter:Landroid/content/IntentFilter;

    #@12
    .line 71
    iput-boolean v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->wifiConnected:Z

    #@14
    .line 73
    iput v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->wfdInterfaceState:I

    #@16
    .line 75
    new-instance v0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;

    #@18
    invoke-direct {v0, p0}, Lcom/lge/systemservice/service/wfdservice/WfdService$1;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdService;)V

    #@1b
    iput-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mReceiver:Landroid/content/BroadcastReceiver;

    #@1d
    .line 241
    const-string v0, "WfdService"

    #@1f
    const-string v1, "WfdService Created"

    #@21
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 242
    iput-object p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mContext:Landroid/content/Context;

    #@26
    .line 243
    iput-object p2, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mWfdStateTracker:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@28
    .line 245
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mIntentFilter:Landroid/content/IntentFilter;

    #@2a
    const-string v1, "android.net.wifi.p2p.STATE_CHANGED"

    #@2c
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@2f
    .line 246
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mIntentFilter:Landroid/content/IntentFilter;

    #@31
    const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"

    #@33
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@36
    .line 247
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mIntentFilter:Landroid/content/IntentFilter;

    #@38
    const-string v1, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    #@3a
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@3d
    .line 249
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mIntentFilter:Landroid/content/IntentFilter;

    #@3f
    const-string v1, "android.net.wifi.p2p.THIS_DEVICE_CHANGED"

    #@41
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@44
    .line 251
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mIntentFilter:Landroid/content/IntentFilter;

    #@46
    const-string v1, "android.net.wifi.STATE_CHANGE"

    #@48
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@4b
    .line 252
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mIntentFilter:Landroid/content/IntentFilter;

    #@4d
    const-string v1, "lge.wfd.switch.start"

    #@4f
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@52
    .line 253
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mIntentFilter:Landroid/content/IntentFilter;

    #@54
    const-string v1, "lge.wfd.switch.stop"

    #@56
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@59
    .line 254
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mIntentFilter:Landroid/content/IntentFilter;

    #@5b
    const-string v1, "com.lge.systemservice.core.wfdmanager.WFD_ENABLE"

    #@5d
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@60
    .line 255
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mIntentFilter:Landroid/content/IntentFilter;

    #@62
    const-string v1, "com.lge.systemservice.core.wfdmanager.WFD_DISABLE"

    #@64
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@67
    .line 257
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mIntentFilter:Landroid/content/IntentFilter;

    #@69
    const-string v1, "android.intent.action.HDMI_PLUGGED"

    #@6b
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@6e
    .line 259
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mIntentFilter:Landroid/content/IntentFilter;

    #@70
    const-string v1, "com.lge.systemservice.core.wfdmanager.WFD_INFORM_DRM_STATUS"

    #@72
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@75
    .line 262
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mIntentFilter:Landroid/content/IntentFilter;

    #@77
    const-string v1, "android.intent.action.SCREEN_ON"

    #@79
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@7c
    .line 263
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mIntentFilter:Landroid/content/IntentFilter;

    #@7e
    const-string v1, "android.intent.action.SCREEN_OFF"

    #@80
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@83
    .line 266
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mIntentFilter:Landroid/content/IntentFilter;

    #@85
    const-string v1, "android.intent.action.BATTERY_CHANGED"

    #@87
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@8a
    .line 273
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mIntentFilter:Landroid/content/IntentFilter;

    #@8c
    const-string v1, "android.intent.action.PHONE_STATE"

    #@8e
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@91
    .line 278
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mIntentFilter:Landroid/content/IntentFilter;

    #@93
    const-string v1, "com.lge.oneseg.wifi.display.disable"

    #@95
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@98
    .line 279
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mIntentFilter:Landroid/content/IntentFilter;

    #@9a
    const-string v1, "com.lge.oneseg.wifi.display.enable"

    #@9c
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@9f
    .line 281
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mIntentFilter:Landroid/content/IntentFilter;

    #@a1
    const-string v1, "com.lge.systemservice.core.wfdmanager.WFD_REQUEST_WIFI_ENABLED_ACTION"

    #@a3
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@a6
    .line 283
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mContext:Landroid/content/Context;

    #@a8
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mReceiver:Landroid/content/BroadcastReceiver;

    #@aa
    iget-object v2, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mIntentFilter:Landroid/content/IntentFilter;

    #@ac
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@af
    .line 286
    return-void
.end method

.method static synthetic access$000(Lcom/lge/systemservice/service/wfdservice/WfdService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 34
    iget v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mWifiP2pState:I

    #@2
    return v0
.end method

.method static synthetic access$002(Lcom/lge/systemservice/service/wfdservice/WfdService;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    iput p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mWifiP2pState:I

    #@2
    return p1
.end method

.method static synthetic access$100(Lcom/lge/systemservice/service/wfdservice/WfdService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 34
    iget v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mWifiState:I

    #@2
    return v0
.end method

.method static synthetic access$102(Lcom/lge/systemservice/service/wfdservice/WfdService;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    iput p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mWifiState:I

    #@2
    return p1
.end method

.method static synthetic access$200(Lcom/lge/systemservice/service/wfdservice/WfdService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 34
    iget v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->wfdInterfaceState:I

    #@2
    return v0
.end method

.method static synthetic access$202(Lcom/lge/systemservice/service/wfdservice/WfdService;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    iput p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->wfdInterfaceState:I

    #@2
    return p1
.end method

.method static synthetic access$300(Lcom/lge/systemservice/service/wfdservice/WfdService;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 34
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mWfdStateTracker:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/lge/systemservice/service/wfdservice/WfdService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->wifiConnected:Z

    #@2
    return v0
.end method

.method static synthetic access$402(Lcom/lge/systemservice/service/wfdservice/WfdService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->wifiConnected:Z

    #@2
    return p1
.end method

.method static synthetic access$500(Lcom/lge/systemservice/service/wfdservice/WfdService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mBattLow:Z

    #@2
    return v0
.end method

.method static synthetic access$502(Lcom/lge/systemservice/service/wfdservice/WfdService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mBattLow:Z

    #@2
    return p1
.end method


# virtual methods
.method public cancelWifiDisplayConnect()Z
    .registers 2

    #@0
    .prologue
    .line 314
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mWfdStateTracker:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@2
    invoke-virtual {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->cancelWifiDisplayConnect()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public clientConnect()Z
    .registers 2

    #@0
    .prologue
    .line 350
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mWfdStateTracker:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@2
    invoke-virtual {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->initRtspClient()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getRtspAddress()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 322
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mWfdStateTracker:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@2
    invoke-virtual {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getRtspAddress()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getRtspState()I
    .registers 2

    #@0
    .prologue
    .line 370
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mWfdStateTracker:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@2
    invoke-virtual {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getRtspState()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getTargetUrl()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 338
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mWfdStateTracker:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@2
    invoke-virtual {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getTargetUrl()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getWfdMode()I
    .registers 2

    #@0
    .prologue
    .line 330
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mWfdStateTracker:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@2
    invoke-virtual {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdMode()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getWfdState()I
    .registers 2

    #@0
    .prologue
    .line 366
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mWfdStateTracker:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@2
    invoke-virtual {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->getWfdState()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public informConnectionRequstedUdn(Ljava/lang/String;)V
    .registers 3
    .parameter "req_udn"

    #@0
    .prologue
    .line 318
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mWfdStateTracker:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@2
    invoke-virtual {v0, p1}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->informConnectionRequstedUdn(Ljava/lang/String;)V

    #@5
    .line 319
    return-void
.end method

.method public pause()Z
    .registers 2

    #@0
    .prologue
    .line 358
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mWfdStateTracker:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@2
    invoke-virtual {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->pause()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public play()Z
    .registers 2

    #@0
    .prologue
    .line 354
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mWfdStateTracker:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@2
    invoke-virtual {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->play()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public setTargetIpAddress_Url(I)V
    .registers 3
    .parameter "inet"

    #@0
    .prologue
    .line 346
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mWfdStateTracker:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@2
    invoke-virtual {v0, p1}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setTargetIpAddress_Url(I)V

    #@5
    .line 347
    return-void
.end method

.method public setTargetRtspPort(I)V
    .registers 3
    .parameter "port"

    #@0
    .prologue
    .line 342
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mWfdStateTracker:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@2
    invoke-virtual {v0, p1}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setTargetRtspPort(I)V

    #@5
    .line 343
    return-void
.end method

.method public setTargetUrl(Ljava/lang/String;)V
    .registers 3
    .parameter "url"

    #@0
    .prologue
    .line 334
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mWfdStateTracker:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@2
    invoke-virtual {v0, p1}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setTargetUrl(Ljava/lang/String;)V

    #@5
    .line 335
    return-void
.end method

.method public setWfdMode(I)V
    .registers 3
    .parameter "mode"

    #@0
    .prologue
    .line 326
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mWfdStateTracker:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@2
    invoke-virtual {v0, p1}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWfdMode(I)V

    #@5
    .line 327
    return-void
.end method

.method public setWifiDisplayEnabled(Z)Z
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 306
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mWfdStateTracker:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@2
    invoke-virtual {v0, p1}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWifiDisplayEnabled(Z)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public setWifiDisplayEnabledWithPopUp(Z)Z
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 310
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mWfdStateTracker:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@2
    invoke-virtual {v0, p1}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWifiDisplayEnabledWithPopUp(Z)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public teardown()Z
    .registers 2

    #@0
    .prologue
    .line 362
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdService;->mWfdStateTracker:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@2
    invoke-virtual {v0}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->teardown()Z

    #@5
    move-result v0

    #@6
    return v0
.end method
