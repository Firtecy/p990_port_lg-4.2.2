.class public Lcom/lge/systemservice/service/FeliCaDevice;
.super Ljava/lang/Object;
.source "FeliCaDevice.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/systemservice/service/FeliCaDevice$FeliCaCmdResult;
    }
.end annotation


# static fields
.field private static final CALIBRATION_CENTER_FREQ:F

.field private static final CALIBRATION_DEFAULT_SWITCH:I

.field private static final CALIBRATION_DEFAULT_SWITCH_INDEX:I

.field private static final CALIBRATION_PASS_RANGE:F

.field private static final CAL_ENTRY_COUNT:I

.field private static final CMD_POLLING:[B

.field private static final CMD_POLLING_EXT:[B

.field private static final CMD_READ_RF_REG:[B

.field private static final CMD_SET_RF_PARAM:[B

.field private static final CRC_TABLE:[I

.field private static final PACKET_ACK:[B

.field private static final PACKET_ACK_LEN:I

.field private static final PACKET_PORT_READY:[B

.field private static final PACKET_PORT_READY_LEN:I

.field private static final RES_POLLING_EXT:[B

.field private static final RES_READ_RF_REG:[B

.field private static final RES_SET_RF_REG:[B

.field private static final RF_CALIBRATION_TABLE:[[I

.field private static final RF_CALIBRATION_TABLE_DCM_L04E:[[I

.field private static final RF_CALIBRATION_TABLE_DCM_L05E:[[I

.field private static final RF_CALIBRATION_TABLE_KDDI:[[I

.field private static final RF_PARAMETER_DEFAULT:[B

.field private static final RF_PARAMETER_DEFAULT_DCM_L04E:[B

.field private static final RF_PARAMETER_DEFAULT_DCM_L05E:[B

.field private static final RF_PARAMETER_DEFAULT_KDDI:[B

.field private static final RF_PARAMETER_DEFAULT_ZERO_INIT:[B

.field private static fos_pon:Ljava/io/FileOutputStream;

.field private static m_felica_pon_fd:I


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDeviceName:Ljava/lang/String;

.field private m_calibration_retry_count:I


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/16 v7, 0xe

    #@2
    const/4 v6, 0x4

    #@3
    const/4 v5, 0x3

    #@4
    const/16 v4, 0x1c

    #@6
    const/4 v3, 0x2

    #@7
    .line 50
    new-array v0, v6, [B

    #@9
    fill-array-data v0, :array_4de

    #@c
    sput-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_ACK:[B

    #@e
    .line 52
    new-array v0, v6, [B

    #@10
    fill-array-data v0, :array_4e4

    #@13
    sput-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_PORT_READY:[B

    #@15
    .line 55
    const/16 v0, 0xb

    #@17
    new-array v0, v0, [B

    #@19
    fill-array-data v0, :array_4ea

    #@1c
    sput-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->CMD_POLLING:[B

    #@1e
    .line 58
    sget-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_ACK:[B

    #@20
    array-length v0, v0

    #@21
    sput v0, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_ACK_LEN:I

    #@23
    .line 59
    sget-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_PORT_READY:[B

    #@25
    array-length v0, v0

    #@26
    sput v0, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_PORT_READY_LEN:I

    #@28
    .line 61
    const/4 v0, -0x1

    #@29
    sput v0, Lcom/lge/systemservice/service/FeliCaDevice;->m_felica_pon_fd:I

    #@2b
    .line 62
    const/4 v0, 0x0

    #@2c
    sput-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->fos_pon:Ljava/io/FileOutputStream;

    #@2e
    .line 67
    const/16 v0, 0x21

    #@30
    new-array v0, v0, [[I

    #@32
    const/4 v1, 0x0

    #@33
    new-array v2, v3, [I

    #@35
    fill-array-data v2, :array_4f4

    #@38
    aput-object v2, v0, v1

    #@3a
    const/4 v1, 0x1

    #@3b
    new-array v2, v3, [I

    #@3d
    fill-array-data v2, :array_4fc

    #@40
    aput-object v2, v0, v1

    #@42
    new-array v1, v3, [I

    #@44
    fill-array-data v1, :array_504

    #@47
    aput-object v1, v0, v3

    #@49
    new-array v1, v3, [I

    #@4b
    fill-array-data v1, :array_50c

    #@4e
    aput-object v1, v0, v5

    #@50
    new-array v1, v3, [I

    #@52
    fill-array-data v1, :array_514

    #@55
    aput-object v1, v0, v6

    #@57
    const/4 v1, 0x5

    #@58
    new-array v2, v3, [I

    #@5a
    fill-array-data v2, :array_51c

    #@5d
    aput-object v2, v0, v1

    #@5f
    const/4 v1, 0x6

    #@60
    new-array v2, v3, [I

    #@62
    fill-array-data v2, :array_524

    #@65
    aput-object v2, v0, v1

    #@67
    const/4 v1, 0x7

    #@68
    new-array v2, v3, [I

    #@6a
    fill-array-data v2, :array_52c

    #@6d
    aput-object v2, v0, v1

    #@6f
    const/16 v1, 0x8

    #@71
    new-array v2, v3, [I

    #@73
    fill-array-data v2, :array_534

    #@76
    aput-object v2, v0, v1

    #@78
    const/16 v1, 0x9

    #@7a
    new-array v2, v3, [I

    #@7c
    fill-array-data v2, :array_53c

    #@7f
    aput-object v2, v0, v1

    #@81
    const/16 v1, 0xa

    #@83
    new-array v2, v3, [I

    #@85
    fill-array-data v2, :array_544

    #@88
    aput-object v2, v0, v1

    #@8a
    const/16 v1, 0xb

    #@8c
    new-array v2, v3, [I

    #@8e
    fill-array-data v2, :array_54c

    #@91
    aput-object v2, v0, v1

    #@93
    const/16 v1, 0xc

    #@95
    new-array v2, v3, [I

    #@97
    fill-array-data v2, :array_554

    #@9a
    aput-object v2, v0, v1

    #@9c
    const/16 v1, 0xd

    #@9e
    new-array v2, v3, [I

    #@a0
    fill-array-data v2, :array_55c

    #@a3
    aput-object v2, v0, v1

    #@a5
    new-array v1, v3, [I

    #@a7
    fill-array-data v1, :array_564

    #@aa
    aput-object v1, v0, v7

    #@ac
    const/16 v1, 0xf

    #@ae
    new-array v2, v3, [I

    #@b0
    fill-array-data v2, :array_56c

    #@b3
    aput-object v2, v0, v1

    #@b5
    const/16 v1, 0x10

    #@b7
    new-array v2, v3, [I

    #@b9
    fill-array-data v2, :array_574

    #@bc
    aput-object v2, v0, v1

    #@be
    const/16 v1, 0x11

    #@c0
    new-array v2, v3, [I

    #@c2
    fill-array-data v2, :array_57c

    #@c5
    aput-object v2, v0, v1

    #@c7
    const/16 v1, 0x12

    #@c9
    new-array v2, v3, [I

    #@cb
    fill-array-data v2, :array_584

    #@ce
    aput-object v2, v0, v1

    #@d0
    const/16 v1, 0x13

    #@d2
    new-array v2, v3, [I

    #@d4
    fill-array-data v2, :array_58c

    #@d7
    aput-object v2, v0, v1

    #@d9
    const/16 v1, 0x14

    #@db
    new-array v2, v3, [I

    #@dd
    fill-array-data v2, :array_594

    #@e0
    aput-object v2, v0, v1

    #@e2
    const/16 v1, 0x15

    #@e4
    new-array v2, v3, [I

    #@e6
    fill-array-data v2, :array_59c

    #@e9
    aput-object v2, v0, v1

    #@eb
    const/16 v1, 0x16

    #@ed
    new-array v2, v3, [I

    #@ef
    fill-array-data v2, :array_5a4

    #@f2
    aput-object v2, v0, v1

    #@f4
    const/16 v1, 0x17

    #@f6
    new-array v2, v3, [I

    #@f8
    fill-array-data v2, :array_5ac

    #@fb
    aput-object v2, v0, v1

    #@fd
    const/16 v1, 0x18

    #@ff
    new-array v2, v3, [I

    #@101
    fill-array-data v2, :array_5b4

    #@104
    aput-object v2, v0, v1

    #@106
    const/16 v1, 0x19

    #@108
    new-array v2, v3, [I

    #@10a
    fill-array-data v2, :array_5bc

    #@10d
    aput-object v2, v0, v1

    #@10f
    const/16 v1, 0x1a

    #@111
    new-array v2, v3, [I

    #@113
    fill-array-data v2, :array_5c4

    #@116
    aput-object v2, v0, v1

    #@118
    const/16 v1, 0x1b

    #@11a
    new-array v2, v3, [I

    #@11c
    fill-array-data v2, :array_5cc

    #@11f
    aput-object v2, v0, v1

    #@121
    new-array v1, v3, [I

    #@123
    fill-array-data v1, :array_5d4

    #@126
    aput-object v1, v0, v4

    #@128
    const/16 v1, 0x1d

    #@12a
    new-array v2, v3, [I

    #@12c
    fill-array-data v2, :array_5dc

    #@12f
    aput-object v2, v0, v1

    #@131
    const/16 v1, 0x1e

    #@133
    new-array v2, v3, [I

    #@135
    fill-array-data v2, :array_5e4

    #@138
    aput-object v2, v0, v1

    #@13a
    const/16 v1, 0x1f

    #@13c
    new-array v2, v3, [I

    #@13e
    fill-array-data v2, :array_5ec

    #@141
    aput-object v2, v0, v1

    #@143
    const/16 v1, 0x20

    #@145
    new-array v2, v3, [I

    #@147
    fill-array-data v2, :array_5f4

    #@14a
    aput-object v2, v0, v1

    #@14c
    sput-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->RF_CALIBRATION_TABLE_DCM_L04E:[[I

    #@14e
    .line 106
    const/16 v0, 0x31

    #@150
    new-array v0, v0, [[I

    #@152
    const/4 v1, 0x0

    #@153
    new-array v2, v3, [I

    #@155
    fill-array-data v2, :array_5fc

    #@158
    aput-object v2, v0, v1

    #@15a
    const/4 v1, 0x1

    #@15b
    new-array v2, v3, [I

    #@15d
    fill-array-data v2, :array_604

    #@160
    aput-object v2, v0, v1

    #@162
    new-array v1, v3, [I

    #@164
    fill-array-data v1, :array_60c

    #@167
    aput-object v1, v0, v3

    #@169
    new-array v1, v3, [I

    #@16b
    fill-array-data v1, :array_614

    #@16e
    aput-object v1, v0, v5

    #@170
    new-array v1, v3, [I

    #@172
    fill-array-data v1, :array_61c

    #@175
    aput-object v1, v0, v6

    #@177
    const/4 v1, 0x5

    #@178
    new-array v2, v3, [I

    #@17a
    fill-array-data v2, :array_624

    #@17d
    aput-object v2, v0, v1

    #@17f
    const/4 v1, 0x6

    #@180
    new-array v2, v3, [I

    #@182
    fill-array-data v2, :array_62c

    #@185
    aput-object v2, v0, v1

    #@187
    const/4 v1, 0x7

    #@188
    new-array v2, v3, [I

    #@18a
    fill-array-data v2, :array_634

    #@18d
    aput-object v2, v0, v1

    #@18f
    const/16 v1, 0x8

    #@191
    new-array v2, v3, [I

    #@193
    fill-array-data v2, :array_63c

    #@196
    aput-object v2, v0, v1

    #@198
    const/16 v1, 0x9

    #@19a
    new-array v2, v3, [I

    #@19c
    fill-array-data v2, :array_644

    #@19f
    aput-object v2, v0, v1

    #@1a1
    const/16 v1, 0xa

    #@1a3
    new-array v2, v3, [I

    #@1a5
    fill-array-data v2, :array_64c

    #@1a8
    aput-object v2, v0, v1

    #@1aa
    const/16 v1, 0xb

    #@1ac
    new-array v2, v3, [I

    #@1ae
    fill-array-data v2, :array_654

    #@1b1
    aput-object v2, v0, v1

    #@1b3
    const/16 v1, 0xc

    #@1b5
    new-array v2, v3, [I

    #@1b7
    fill-array-data v2, :array_65c

    #@1ba
    aput-object v2, v0, v1

    #@1bc
    const/16 v1, 0xd

    #@1be
    new-array v2, v3, [I

    #@1c0
    fill-array-data v2, :array_664

    #@1c3
    aput-object v2, v0, v1

    #@1c5
    new-array v1, v3, [I

    #@1c7
    fill-array-data v1, :array_66c

    #@1ca
    aput-object v1, v0, v7

    #@1cc
    const/16 v1, 0xf

    #@1ce
    new-array v2, v3, [I

    #@1d0
    fill-array-data v2, :array_674

    #@1d3
    aput-object v2, v0, v1

    #@1d5
    const/16 v1, 0x10

    #@1d7
    new-array v2, v3, [I

    #@1d9
    fill-array-data v2, :array_67c

    #@1dc
    aput-object v2, v0, v1

    #@1de
    const/16 v1, 0x11

    #@1e0
    new-array v2, v3, [I

    #@1e2
    fill-array-data v2, :array_684

    #@1e5
    aput-object v2, v0, v1

    #@1e7
    const/16 v1, 0x12

    #@1e9
    new-array v2, v3, [I

    #@1eb
    fill-array-data v2, :array_68c

    #@1ee
    aput-object v2, v0, v1

    #@1f0
    const/16 v1, 0x13

    #@1f2
    new-array v2, v3, [I

    #@1f4
    fill-array-data v2, :array_694

    #@1f7
    aput-object v2, v0, v1

    #@1f9
    const/16 v1, 0x14

    #@1fb
    new-array v2, v3, [I

    #@1fd
    fill-array-data v2, :array_69c

    #@200
    aput-object v2, v0, v1

    #@202
    const/16 v1, 0x15

    #@204
    new-array v2, v3, [I

    #@206
    fill-array-data v2, :array_6a4

    #@209
    aput-object v2, v0, v1

    #@20b
    const/16 v1, 0x16

    #@20d
    new-array v2, v3, [I

    #@20f
    fill-array-data v2, :array_6ac

    #@212
    aput-object v2, v0, v1

    #@214
    const/16 v1, 0x17

    #@216
    new-array v2, v3, [I

    #@218
    fill-array-data v2, :array_6b4

    #@21b
    aput-object v2, v0, v1

    #@21d
    const/16 v1, 0x18

    #@21f
    new-array v2, v3, [I

    #@221
    fill-array-data v2, :array_6bc

    #@224
    aput-object v2, v0, v1

    #@226
    const/16 v1, 0x19

    #@228
    new-array v2, v3, [I

    #@22a
    fill-array-data v2, :array_6c4

    #@22d
    aput-object v2, v0, v1

    #@22f
    const/16 v1, 0x1a

    #@231
    new-array v2, v3, [I

    #@233
    fill-array-data v2, :array_6cc

    #@236
    aput-object v2, v0, v1

    #@238
    const/16 v1, 0x1b

    #@23a
    new-array v2, v3, [I

    #@23c
    fill-array-data v2, :array_6d4

    #@23f
    aput-object v2, v0, v1

    #@241
    new-array v1, v3, [I

    #@243
    fill-array-data v1, :array_6dc

    #@246
    aput-object v1, v0, v4

    #@248
    const/16 v1, 0x1d

    #@24a
    new-array v2, v3, [I

    #@24c
    fill-array-data v2, :array_6e4

    #@24f
    aput-object v2, v0, v1

    #@251
    const/16 v1, 0x1e

    #@253
    new-array v2, v3, [I

    #@255
    fill-array-data v2, :array_6ec

    #@258
    aput-object v2, v0, v1

    #@25a
    const/16 v1, 0x1f

    #@25c
    new-array v2, v3, [I

    #@25e
    fill-array-data v2, :array_6f4

    #@261
    aput-object v2, v0, v1

    #@263
    const/16 v1, 0x20

    #@265
    new-array v2, v3, [I

    #@267
    fill-array-data v2, :array_6fc

    #@26a
    aput-object v2, v0, v1

    #@26c
    const/16 v1, 0x21

    #@26e
    new-array v2, v3, [I

    #@270
    fill-array-data v2, :array_704

    #@273
    aput-object v2, v0, v1

    #@275
    const/16 v1, 0x22

    #@277
    new-array v2, v3, [I

    #@279
    fill-array-data v2, :array_70c

    #@27c
    aput-object v2, v0, v1

    #@27e
    const/16 v1, 0x23

    #@280
    new-array v2, v3, [I

    #@282
    fill-array-data v2, :array_714

    #@285
    aput-object v2, v0, v1

    #@287
    const/16 v1, 0x24

    #@289
    new-array v2, v3, [I

    #@28b
    fill-array-data v2, :array_71c

    #@28e
    aput-object v2, v0, v1

    #@290
    const/16 v1, 0x25

    #@292
    new-array v2, v3, [I

    #@294
    fill-array-data v2, :array_724

    #@297
    aput-object v2, v0, v1

    #@299
    const/16 v1, 0x26

    #@29b
    new-array v2, v3, [I

    #@29d
    fill-array-data v2, :array_72c

    #@2a0
    aput-object v2, v0, v1

    #@2a2
    const/16 v1, 0x27

    #@2a4
    new-array v2, v3, [I

    #@2a6
    fill-array-data v2, :array_734

    #@2a9
    aput-object v2, v0, v1

    #@2ab
    const/16 v1, 0x28

    #@2ad
    new-array v2, v3, [I

    #@2af
    fill-array-data v2, :array_73c

    #@2b2
    aput-object v2, v0, v1

    #@2b4
    const/16 v1, 0x29

    #@2b6
    new-array v2, v3, [I

    #@2b8
    fill-array-data v2, :array_744

    #@2bb
    aput-object v2, v0, v1

    #@2bd
    const/16 v1, 0x2a

    #@2bf
    new-array v2, v3, [I

    #@2c1
    fill-array-data v2, :array_74c

    #@2c4
    aput-object v2, v0, v1

    #@2c6
    const/16 v1, 0x2b

    #@2c8
    new-array v2, v3, [I

    #@2ca
    fill-array-data v2, :array_754

    #@2cd
    aput-object v2, v0, v1

    #@2cf
    const/16 v1, 0x2c

    #@2d1
    new-array v2, v3, [I

    #@2d3
    fill-array-data v2, :array_75c

    #@2d6
    aput-object v2, v0, v1

    #@2d8
    const/16 v1, 0x2d

    #@2da
    new-array v2, v3, [I

    #@2dc
    fill-array-data v2, :array_764

    #@2df
    aput-object v2, v0, v1

    #@2e1
    const/16 v1, 0x2e

    #@2e3
    new-array v2, v3, [I

    #@2e5
    fill-array-data v2, :array_76c

    #@2e8
    aput-object v2, v0, v1

    #@2ea
    const/16 v1, 0x2f

    #@2ec
    new-array v2, v3, [I

    #@2ee
    fill-array-data v2, :array_774

    #@2f1
    aput-object v2, v0, v1

    #@2f3
    const/16 v1, 0x30

    #@2f5
    new-array v2, v3, [I

    #@2f7
    fill-array-data v2, :array_77c

    #@2fa
    aput-object v2, v0, v1

    #@2fc
    sput-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->RF_CALIBRATION_TABLE_DCM_L05E:[[I

    #@2fe
    .line 159
    new-array v0, v4, [[I

    #@300
    const/4 v1, 0x0

    #@301
    new-array v2, v3, [I

    #@303
    fill-array-data v2, :array_784

    #@306
    aput-object v2, v0, v1

    #@308
    const/4 v1, 0x1

    #@309
    new-array v2, v3, [I

    #@30b
    fill-array-data v2, :array_78c

    #@30e
    aput-object v2, v0, v1

    #@310
    new-array v1, v3, [I

    #@312
    fill-array-data v1, :array_794

    #@315
    aput-object v1, v0, v3

    #@317
    new-array v1, v3, [I

    #@319
    fill-array-data v1, :array_79c

    #@31c
    aput-object v1, v0, v5

    #@31e
    new-array v1, v3, [I

    #@320
    fill-array-data v1, :array_7a4

    #@323
    aput-object v1, v0, v6

    #@325
    const/4 v1, 0x5

    #@326
    new-array v2, v3, [I

    #@328
    fill-array-data v2, :array_7ac

    #@32b
    aput-object v2, v0, v1

    #@32d
    const/4 v1, 0x6

    #@32e
    new-array v2, v3, [I

    #@330
    fill-array-data v2, :array_7b4

    #@333
    aput-object v2, v0, v1

    #@335
    const/4 v1, 0x7

    #@336
    new-array v2, v3, [I

    #@338
    fill-array-data v2, :array_7bc

    #@33b
    aput-object v2, v0, v1

    #@33d
    const/16 v1, 0x8

    #@33f
    new-array v2, v3, [I

    #@341
    fill-array-data v2, :array_7c4

    #@344
    aput-object v2, v0, v1

    #@346
    const/16 v1, 0x9

    #@348
    new-array v2, v3, [I

    #@34a
    fill-array-data v2, :array_7cc

    #@34d
    aput-object v2, v0, v1

    #@34f
    const/16 v1, 0xa

    #@351
    new-array v2, v3, [I

    #@353
    fill-array-data v2, :array_7d4

    #@356
    aput-object v2, v0, v1

    #@358
    const/16 v1, 0xb

    #@35a
    new-array v2, v3, [I

    #@35c
    fill-array-data v2, :array_7dc

    #@35f
    aput-object v2, v0, v1

    #@361
    const/16 v1, 0xc

    #@363
    new-array v2, v3, [I

    #@365
    fill-array-data v2, :array_7e4

    #@368
    aput-object v2, v0, v1

    #@36a
    const/16 v1, 0xd

    #@36c
    new-array v2, v3, [I

    #@36e
    fill-array-data v2, :array_7ec

    #@371
    aput-object v2, v0, v1

    #@373
    new-array v1, v3, [I

    #@375
    fill-array-data v1, :array_7f4

    #@378
    aput-object v1, v0, v7

    #@37a
    const/16 v1, 0xf

    #@37c
    new-array v2, v3, [I

    #@37e
    fill-array-data v2, :array_7fc

    #@381
    aput-object v2, v0, v1

    #@383
    const/16 v1, 0x10

    #@385
    new-array v2, v3, [I

    #@387
    fill-array-data v2, :array_804

    #@38a
    aput-object v2, v0, v1

    #@38c
    const/16 v1, 0x11

    #@38e
    new-array v2, v3, [I

    #@390
    fill-array-data v2, :array_80c

    #@393
    aput-object v2, v0, v1

    #@395
    const/16 v1, 0x12

    #@397
    new-array v2, v3, [I

    #@399
    fill-array-data v2, :array_814

    #@39c
    aput-object v2, v0, v1

    #@39e
    const/16 v1, 0x13

    #@3a0
    new-array v2, v3, [I

    #@3a2
    fill-array-data v2, :array_81c

    #@3a5
    aput-object v2, v0, v1

    #@3a7
    const/16 v1, 0x14

    #@3a9
    new-array v2, v3, [I

    #@3ab
    fill-array-data v2, :array_824

    #@3ae
    aput-object v2, v0, v1

    #@3b0
    const/16 v1, 0x15

    #@3b2
    new-array v2, v3, [I

    #@3b4
    fill-array-data v2, :array_82c

    #@3b7
    aput-object v2, v0, v1

    #@3b9
    const/16 v1, 0x16

    #@3bb
    new-array v2, v3, [I

    #@3bd
    fill-array-data v2, :array_834

    #@3c0
    aput-object v2, v0, v1

    #@3c2
    const/16 v1, 0x17

    #@3c4
    new-array v2, v3, [I

    #@3c6
    fill-array-data v2, :array_83c

    #@3c9
    aput-object v2, v0, v1

    #@3cb
    const/16 v1, 0x18

    #@3cd
    new-array v2, v3, [I

    #@3cf
    fill-array-data v2, :array_844

    #@3d2
    aput-object v2, v0, v1

    #@3d4
    const/16 v1, 0x19

    #@3d6
    new-array v2, v3, [I

    #@3d8
    fill-array-data v2, :array_84c

    #@3db
    aput-object v2, v0, v1

    #@3dd
    const/16 v1, 0x1a

    #@3df
    new-array v2, v3, [I

    #@3e1
    fill-array-data v2, :array_854

    #@3e4
    aput-object v2, v0, v1

    #@3e6
    const/16 v1, 0x1b

    #@3e8
    new-array v2, v3, [I

    #@3ea
    fill-array-data v2, :array_85c

    #@3ed
    aput-object v2, v0, v1

    #@3ef
    sput-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->RF_CALIBRATION_TABLE_KDDI:[[I

    #@3f1
    .line 195
    new-array v0, v4, [B

    #@3f3
    fill-array-data v0, :array_864

    #@3f6
    sput-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT_DCM_L04E:[B

    #@3f8
    .line 213
    new-array v0, v4, [B

    #@3fa
    fill-array-data v0, :array_876

    #@3fd
    sput-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT_DCM_L05E:[B

    #@3ff
    .line 232
    new-array v0, v4, [B

    #@401
    fill-array-data v0, :array_888

    #@404
    sput-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT_KDDI:[B

    #@406
    .line 250
    new-array v0, v4, [B

    #@408
    fill-array-data v0, :array_89a

    #@40b
    sput-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT_ZERO_INIT:[B

    #@40d
    .line 270
    const-string v0, "ro.build.target_operator"

    #@40f
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@412
    move-result-object v0

    #@413
    const-string v1, "DCM"

    #@415
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@418
    move-result v0

    #@419
    if-eqz v0, :cond_49d

    #@41b
    .line 272
    const-string v0, "ro.product.model"

    #@41d
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@420
    move-result-object v0

    #@421
    const-string v1, "L-04E"

    #@423
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@426
    move-result v0

    #@427
    if-eqz v0, :cond_482

    #@429
    .line 274
    sget-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->RF_CALIBRATION_TABLE_DCM_L04E:[[I

    #@42b
    sput-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->RF_CALIBRATION_TABLE:[[I

    #@42d
    .line 275
    sget-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT_DCM_L04E:[B

    #@42f
    sput-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT:[B

    #@431
    .line 276
    const v0, 0x4157999a

    #@434
    sput v0, Lcom/lge/systemservice/service/FeliCaDevice;->CALIBRATION_CENTER_FREQ:F

    #@436
    .line 277
    const v0, 0x3dd70a3d

    #@439
    sput v0, Lcom/lge/systemservice/service/FeliCaDevice;->CALIBRATION_PASS_RANGE:F

    #@43b
    .line 278
    const/16 v0, 0x13

    #@43d
    sput v0, Lcom/lge/systemservice/service/FeliCaDevice;->CALIBRATION_DEFAULT_SWITCH_INDEX:I

    #@43f
    .line 279
    const/16 v0, 0x2a

    #@441
    sput v0, Lcom/lge/systemservice/service/FeliCaDevice;->CALIBRATION_DEFAULT_SWITCH:I

    #@443
    .line 313
    :goto_443
    sget-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->RF_CALIBRATION_TABLE:[[I

    #@445
    array-length v0, v0

    #@446
    sput v0, Lcom/lge/systemservice/service/FeliCaDevice;->CAL_ENTRY_COUNT:I

    #@448
    .line 335
    const/16 v0, 0x8

    #@44a
    new-array v0, v0, [B

    #@44c
    fill-array-data v0, :array_8ac

    #@44f
    sput-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->CMD_READ_RF_REG:[B

    #@451
    .line 336
    new-array v0, v5, [B

    #@453
    fill-array-data v0, :array_8b4

    #@456
    sput-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->RES_READ_RF_REG:[B

    #@458
    .line 342
    const/16 v0, 0x26

    #@45a
    new-array v0, v0, [B

    #@45c
    fill-array-data v0, :array_8ba

    #@45f
    sput-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->CMD_SET_RF_PARAM:[B

    #@461
    .line 349
    new-array v0, v5, [B

    #@463
    fill-array-data v0, :array_8d2

    #@466
    sput-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->RES_SET_RF_REG:[B

    #@468
    .line 357
    const/16 v0, 0x10

    #@46a
    new-array v0, v0, [B

    #@46c
    fill-array-data v0, :array_8d8

    #@46f
    sput-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->CMD_POLLING_EXT:[B

    #@471
    .line 360
    new-array v0, v4, [B

    #@473
    fill-array-data v0, :array_8e4

    #@476
    sput-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->RES_POLLING_EXT:[B

    #@478
    .line 370
    const/16 v0, 0x100

    #@47a
    new-array v0, v0, [I

    #@47c
    fill-array-data v0, :array_8f6

    #@47f
    sput-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->CRC_TABLE:[I

    #@481
    return-void

    #@482
    .line 283
    :cond_482
    sget-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->RF_CALIBRATION_TABLE_DCM_L05E:[[I

    #@484
    sput-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->RF_CALIBRATION_TABLE:[[I

    #@486
    .line 284
    sget-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT_DCM_L05E:[B

    #@488
    sput-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT:[B

    #@48a
    .line 285
    const v0, 0x4157999a

    #@48d
    sput v0, Lcom/lge/systemservice/service/FeliCaDevice;->CALIBRATION_CENTER_FREQ:F

    #@48f
    .line 286
    const v0, 0x3dd70a3d

    #@492
    sput v0, Lcom/lge/systemservice/service/FeliCaDevice;->CALIBRATION_PASS_RANGE:F

    #@494
    .line 287
    const/16 v0, 0x13

    #@496
    sput v0, Lcom/lge/systemservice/service/FeliCaDevice;->CALIBRATION_DEFAULT_SWITCH_INDEX:I

    #@498
    .line 288
    const/16 v0, 0x2a

    #@49a
    sput v0, Lcom/lge/systemservice/service/FeliCaDevice;->CALIBRATION_DEFAULT_SWITCH:I

    #@49c
    goto :goto_443

    #@49d
    .line 291
    :cond_49d
    const-string v0, "ro.build.target_operator"

    #@49f
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@4a2
    move-result-object v0

    #@4a3
    const-string v1, "KDDI"

    #@4a5
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4a8
    move-result v0

    #@4a9
    if-eqz v0, :cond_4c3

    #@4ab
    .line 293
    sget-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->RF_CALIBRATION_TABLE_KDDI:[[I

    #@4ad
    sput-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->RF_CALIBRATION_TABLE:[[I

    #@4af
    .line 294
    sget-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT_KDDI:[B

    #@4b1
    sput-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT:[B

    #@4b3
    .line 295
    const/high16 v0, 0x4156

    #@4b5
    sput v0, Lcom/lge/systemservice/service/FeliCaDevice;->CALIBRATION_CENTER_FREQ:F

    #@4b7
    .line 296
    const v0, 0x3dd70a3d

    #@4ba
    sput v0, Lcom/lge/systemservice/service/FeliCaDevice;->CALIBRATION_PASS_RANGE:F

    #@4bc
    .line 297
    sput v7, Lcom/lge/systemservice/service/FeliCaDevice;->CALIBRATION_DEFAULT_SWITCH_INDEX:I

    #@4be
    .line 298
    const/16 v0, 0x24

    #@4c0
    sput v0, Lcom/lge/systemservice/service/FeliCaDevice;->CALIBRATION_DEFAULT_SWITCH:I

    #@4c2
    goto :goto_443

    #@4c3
    .line 302
    :cond_4c3
    sget-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->RF_CALIBRATION_TABLE_DCM_L04E:[[I

    #@4c5
    sput-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->RF_CALIBRATION_TABLE:[[I

    #@4c7
    .line 303
    sget-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT_ZERO_INIT:[B

    #@4c9
    sput-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT:[B

    #@4cb
    .line 304
    const v0, 0x4158f5c3

    #@4ce
    sput v0, Lcom/lge/systemservice/service/FeliCaDevice;->CALIBRATION_CENTER_FREQ:F

    #@4d0
    .line 305
    const v0, 0x3dcccccd

    #@4d3
    sput v0, Lcom/lge/systemservice/service/FeliCaDevice;->CALIBRATION_PASS_RANGE:F

    #@4d5
    .line 306
    sput v7, Lcom/lge/systemservice/service/FeliCaDevice;->CALIBRATION_DEFAULT_SWITCH_INDEX:I

    #@4d7
    .line 307
    const/16 v0, 0x24

    #@4d9
    sput v0, Lcom/lge/systemservice/service/FeliCaDevice;->CALIBRATION_DEFAULT_SWITCH:I

    #@4db
    goto/16 :goto_443

    #@4dd
    .line 50
    nop

    #@4de
    :array_4de
    .array-data 0x1
        0x0t
        0xf5t
        0x5t
        0x0t
    .end array-data

    #@4e4
    .line 52
    :array_4e4
    .array-data 0x1
        0x0t
        0xf5t
        0x55t
        0x0t
    .end array-data

    #@4ea
    .line 55
    :array_4ea
    .array-data 0x1
        0x0t
        0xfat
        0x6t
        0x0t
        0xfft
        0xfft
        0x0t
        0x0t
        0x9t
        0x21t
        0x0t
    .end array-data

    #@4f4
    .line 67
    :array_4f4
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    #@4fc
    :array_4fc
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
    .end array-data

    #@504
    :array_504
    .array-data 0x4
        0x3t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
    .end array-data

    #@50c
    :array_50c
    .array-data 0x4
        0x4t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
    .end array-data

    #@514
    :array_514
    .array-data 0x4
        0x5t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
    .end array-data

    #@51c
    :array_51c
    .array-data 0x4
        0x6t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
    .end array-data

    #@524
    :array_524
    .array-data 0x4
        0x7t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
    .end array-data

    #@52c
    :array_52c
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data

    #@534
    :array_534
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data

    #@53c
    :array_53c
    .array-data 0x4
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data

    #@544
    :array_544
    .array-data 0x4
        0x4t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data

    #@54c
    :array_54c
    .array-data 0x4
        0x5t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data

    #@554
    :array_554
    .array-data 0x4
        0x6t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data

    #@55c
    :array_55c
    .array-data 0x4
        0x7t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data

    #@564
    :array_564
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
    .end array-data

    #@56c
    :array_56c
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
    .end array-data

    #@574
    :array_574
    .array-data 0x4
        0x3t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
    .end array-data

    #@57c
    :array_57c
    .array-data 0x4
        0x4t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
    .end array-data

    #@584
    :array_584
    .array-data 0x4
        0x5t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
    .end array-data

    #@58c
    :array_58c
    .array-data 0x4
        0x6t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
    .end array-data

    #@594
    :array_594
    .array-data 0x4
        0x7t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
    .end array-data

    #@59c
    :array_59c
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data

    #@5a4
    :array_5a4
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data

    #@5ac
    :array_5ac
    .array-data 0x4
        0x3t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data

    #@5b4
    :array_5b4
    .array-data 0x4
        0x4t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data

    #@5bc
    :array_5bc
    .array-data 0x4
        0x5t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data

    #@5c4
    :array_5c4
    .array-data 0x4
        0x6t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data

    #@5cc
    :array_5cc
    .array-data 0x4
        0x7t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data

    #@5d4
    :array_5d4
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x6t 0x0t 0x0t 0x0t
    .end array-data

    #@5dc
    :array_5dc
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x6t 0x0t 0x0t 0x0t
    .end array-data

    #@5e4
    :array_5e4
    .array-data 0x4
        0x3t 0x0t 0x0t 0x0t
        0x6t 0x0t 0x0t 0x0t
    .end array-data

    #@5ec
    :array_5ec
    .array-data 0x4
        0x4t 0x0t 0x0t 0x0t
        0x6t 0x0t 0x0t 0x0t
    .end array-data

    #@5f4
    :array_5f4
    .array-data 0x4
        0x7t 0x0t 0x0t 0x0t
        0x7t 0x0t 0x0t 0x0t
    .end array-data

    #@5fc
    .line 106
    :array_5fc
    .array-data 0x4
        0x7t 0x0t 0x0t 0x0t
        0x7t 0x0t 0x0t 0x0t
    .end array-data

    #@604
    :array_604
    .array-data 0x4
        0x7t 0x0t 0x0t 0x0t
        0x6t 0x0t 0x0t 0x0t
    .end array-data

    #@60c
    :array_60c
    .array-data 0x4
        0x7t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data

    #@614
    :array_614
    .array-data 0x4
        0x7t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
    .end array-data

    #@61c
    :array_61c
    .array-data 0x4
        0x7t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data

    #@624
    :array_624
    .array-data 0x4
        0x7t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
    .end array-data

    #@62c
    :array_62c
    .array-data 0x4
        0x7t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    #@634
    :array_634
    .array-data 0x4
        0x6t 0x0t 0x0t 0x0t
        0x7t 0x0t 0x0t 0x0t
    .end array-data

    #@63c
    :array_63c
    .array-data 0x4
        0x6t 0x0t 0x0t 0x0t
        0x6t 0x0t 0x0t 0x0t
    .end array-data

    #@644
    :array_644
    .array-data 0x4
        0x6t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data

    #@64c
    :array_64c
    .array-data 0x4
        0x6t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
    .end array-data

    #@654
    :array_654
    .array-data 0x4
        0x6t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data

    #@65c
    :array_65c
    .array-data 0x4
        0x6t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
    .end array-data

    #@664
    :array_664
    .array-data 0x4
        0x6t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    #@66c
    :array_66c
    .array-data 0x4
        0x5t 0x0t 0x0t 0x0t
        0x7t 0x0t 0x0t 0x0t
    .end array-data

    #@674
    :array_674
    .array-data 0x4
        0x5t 0x0t 0x0t 0x0t
        0x6t 0x0t 0x0t 0x0t
    .end array-data

    #@67c
    :array_67c
    .array-data 0x4
        0x5t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data

    #@684
    :array_684
    .array-data 0x4
        0x5t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
    .end array-data

    #@68c
    :array_68c
    .array-data 0x4
        0x5t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data

    #@694
    :array_694
    .array-data 0x4
        0x5t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    #@69c
    :array_69c
    .array-data 0x4
        0x5t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    #@6a4
    :array_6a4
    .array-data 0x4
        0x4t 0x0t 0x0t 0x0t
        0x7t 0x0t 0x0t 0x0t
    .end array-data

    #@6ac
    :array_6ac
    .array-data 0x4
        0x4t 0x0t 0x0t 0x0t
        0x6t 0x0t 0x0t 0x0t
    .end array-data

    #@6b4
    :array_6b4
    .array-data 0x4
        0x4t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data

    #@6bc
    :array_6bc
    .array-data 0x4
        0x4t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
    .end array-data

    #@6c4
    :array_6c4
    .array-data 0x4
        0x4t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data

    #@6cc
    :array_6cc
    .array-data 0x4
        0x4t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
    .end array-data

    #@6d4
    :array_6d4
    .array-data 0x4
        0x4t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    #@6dc
    :array_6dc
    .array-data 0x4
        0x3t 0x0t 0x0t 0x0t
        0x7t 0x0t 0x0t 0x0t
    .end array-data

    #@6e4
    :array_6e4
    .array-data 0x4
        0x3t 0x0t 0x0t 0x0t
        0x6t 0x0t 0x0t 0x0t
    .end array-data

    #@6ec
    :array_6ec
    .array-data 0x4
        0x3t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data

    #@6f4
    :array_6f4
    .array-data 0x4
        0x3t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
    .end array-data

    #@6fc
    :array_6fc
    .array-data 0x4
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data

    #@704
    :array_704
    .array-data 0x4
        0x3t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
    .end array-data

    #@70c
    :array_70c
    .array-data 0x4
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    #@714
    :array_714
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x7t 0x0t 0x0t 0x0t
    .end array-data

    #@71c
    :array_71c
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x6t 0x0t 0x0t 0x0t
    .end array-data

    #@724
    :array_724
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data

    #@72c
    :array_72c
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
    .end array-data

    #@734
    :array_734
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data

    #@73c
    :array_73c
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
    .end array-data

    #@744
    :array_744
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    #@74c
    :array_74c
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x7t 0x0t 0x0t 0x0t
    .end array-data

    #@754
    :array_754
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x6t 0x0t 0x0t 0x0t
    .end array-data

    #@75c
    :array_75c
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data

    #@764
    :array_764
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
    .end array-data

    #@76c
    :array_76c
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data

    #@774
    :array_774
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
    .end array-data

    #@77c
    :array_77c
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    #@784
    .line 159
    :array_784
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    #@78c
    :array_78c
    .array-data 0x4
        0x4t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    #@794
    :array_794
    .array-data 0x4
        0x7t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    #@79c
    :array_79c
    .array-data 0x4
        0x3t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
    .end array-data

    #@7a4
    :array_7a4
    .array-data 0x4
        0x6t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
    .end array-data

    #@7ac
    :array_7ac
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data

    #@7b4
    :array_7b4
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data

    #@7bc
    :array_7bc
    .array-data 0x4
        0x4t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data

    #@7c4
    :array_7c4
    .array-data 0x4
        0x5t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data

    #@7cc
    :array_7cc
    .array-data 0x4
        0x7t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data

    #@7d4
    :array_7d4
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
    .end array-data

    #@7dc
    :array_7dc
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
    .end array-data

    #@7e4
    :array_7e4
    .array-data 0x4
        0x3t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
    .end array-data

    #@7ec
    :array_7ec
    .array-data 0x4
        0x4t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
    .end array-data

    #@7f4
    :array_7f4
    .array-data 0x4
        0x5t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
    .end array-data

    #@7fc
    :array_7fc
    .array-data 0x4
        0x6t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
    .end array-data

    #@804
    :array_804
    .array-data 0x4
        0x7t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
    .end array-data

    #@80c
    :array_80c
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data

    #@814
    :array_814
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data

    #@81c
    :array_81c
    .array-data 0x4
        0x3t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data

    #@824
    :array_824
    .array-data 0x4
        0x5t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data

    #@82c
    :array_82c
    .array-data 0x4
        0x6t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data

    #@834
    :array_834
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x6t 0x0t 0x0t 0x0t
    .end array-data

    #@83c
    :array_83c
    .array-data 0x4
        0x4t 0x0t 0x0t 0x0t
        0x6t 0x0t 0x0t 0x0t
    .end array-data

    #@844
    :array_844
    .array-data 0x4
        0x5t 0x0t 0x0t 0x0t
        0x6t 0x0t 0x0t 0x0t
    .end array-data

    #@84c
    :array_84c
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x7t 0x0t 0x0t 0x0t
    .end array-data

    #@854
    :array_854
    .array-data 0x4
        0x4t 0x0t 0x0t 0x0t
        0x7t 0x0t 0x0t 0x0t
    .end array-data

    #@85c
    :array_85c
    .array-data 0x4
        0x7t 0x0t 0x0t 0x0t
        0x7t 0x0t 0x0t 0x0t
    .end array-data

    #@864
    .line 195
    :array_864
    .array-data 0x1
        0x5t
        0x14t
        0x6t
        0xat
        0x10t
        0x3t
        0x11t
        0x5t
        0x12t
        0x20t
        0x13t
        0x90t
        0x14t
        0x32t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    #@876
    .line 213
    :array_876
    .array-data 0x1
        0x5t
        0x1et
        0x6t
        0xat
        0x10t
        0x1t
        0x11t
        0xct
        0x12t
        0x20t
        0x13t
        0x10t
        0x14t
        0xc5t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    #@888
    .line 232
    :array_888
    .array-data 0x1
        0x5t
        0x3ft
        0x6t
        0x1ct
        0x10t
        0x3t
        0x11t
        0x41t
        0x12t
        0x3ft
        0x13t
        0x88t
        0x14t
        0x29t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    #@89a
    .line 250
    :array_89a
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    #@8ac
    .line 335
    :array_8ac
    .array-data 0x1
        0x0t
        0xfat
        0x3t
        0xf8t
        0x2t
        0xe3t
        0x7at
        0x0t
    .end array-data

    #@8b4
    .line 336
    :array_8b4
    .array-data 0x1
        0xf9t
        0x2t
        0x0t
    .end array-data

    #@8ba
    .line 342
    :array_8ba
    .array-data 0x1
        0x0t
        0xfat
        0x21t
        0xf8t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x81t
        0x0t
        0x0t
        0x0t
    .end array-data

    #@8d1
    .line 349
    nop

    #@8d2
    :array_8d2
    .array-data 0x1
        0xf9t
        0x1t
        0x0t
    .end array-data

    #@8d8
    .line 357
    :array_8d8
    .array-data 0x1
        0x0t
        0xfat
        0xbt
        0xcct
        0x11t
        0x32t
        0x0t
        0x6t
        0x0t
        0xfft
        0xfft
        0x0t
        0x0t
        0xd3t
        0xa2t
        0x0t
    .end array-data

    #@8e4
    .line 360
    :array_8e4
    .array-data 0x1
        0x0t
        0xfat
        0x17t
        0xcdt
        0x11t
        0x0t
        0x0t
        0x12t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    #@8f6
    .line 370
    :array_8f6
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x21t 0x10t 0x0t 0x0t
        0x42t 0x20t 0x0t 0x0t
        0x63t 0x30t 0x0t 0x0t
        0x84t 0x40t 0x0t 0x0t
        0xa5t 0x50t 0x0t 0x0t
        0xc6t 0x60t 0x0t 0x0t
        0xe7t 0x70t 0x0t 0x0t
        0x8t 0x81t 0x0t 0x0t
        0x29t 0x91t 0x0t 0x0t
        0x4at 0xa1t 0x0t 0x0t
        0x6bt 0xb1t 0x0t 0x0t
        0x8ct 0xc1t 0x0t 0x0t
        0xadt 0xd1t 0x0t 0x0t
        0xcet 0xe1t 0x0t 0x0t
        0xeft 0xf1t 0x0t 0x0t
        0x31t 0x12t 0x0t 0x0t
        0x10t 0x2t 0x0t 0x0t
        0x73t 0x32t 0x0t 0x0t
        0x52t 0x22t 0x0t 0x0t
        0xb5t 0x52t 0x0t 0x0t
        0x94t 0x42t 0x0t 0x0t
        0xf7t 0x72t 0x0t 0x0t
        0xd6t 0x62t 0x0t 0x0t
        0x39t 0x93t 0x0t 0x0t
        0x18t 0x83t 0x0t 0x0t
        0x7bt 0xb3t 0x0t 0x0t
        0x5at 0xa3t 0x0t 0x0t
        0xbdt 0xd3t 0x0t 0x0t
        0x9ct 0xc3t 0x0t 0x0t
        0xfft 0xf3t 0x0t 0x0t
        0xdet 0xe3t 0x0t 0x0t
        0x62t 0x24t 0x0t 0x0t
        0x43t 0x34t 0x0t 0x0t
        0x20t 0x4t 0x0t 0x0t
        0x1t 0x14t 0x0t 0x0t
        0xe6t 0x64t 0x0t 0x0t
        0xc7t 0x74t 0x0t 0x0t
        0xa4t 0x44t 0x0t 0x0t
        0x85t 0x54t 0x0t 0x0t
        0x6at 0xa5t 0x0t 0x0t
        0x4bt 0xb5t 0x0t 0x0t
        0x28t 0x85t 0x0t 0x0t
        0x9t 0x95t 0x0t 0x0t
        0xeet 0xe5t 0x0t 0x0t
        0xcft 0xf5t 0x0t 0x0t
        0xact 0xc5t 0x0t 0x0t
        0x8dt 0xd5t 0x0t 0x0t
        0x53t 0x36t 0x0t 0x0t
        0x72t 0x26t 0x0t 0x0t
        0x11t 0x16t 0x0t 0x0t
        0x30t 0x6t 0x0t 0x0t
        0xd7t 0x76t 0x0t 0x0t
        0xf6t 0x66t 0x0t 0x0t
        0x95t 0x56t 0x0t 0x0t
        0xb4t 0x46t 0x0t 0x0t
        0x5bt 0xb7t 0x0t 0x0t
        0x7at 0xa7t 0x0t 0x0t
        0x19t 0x97t 0x0t 0x0t
        0x38t 0x87t 0x0t 0x0t
        0xdft 0xf7t 0x0t 0x0t
        0xfet 0xe7t 0x0t 0x0t
        0x9dt 0xd7t 0x0t 0x0t
        0xbct 0xc7t 0x0t 0x0t
        0xc4t 0x48t 0x0t 0x0t
        0xe5t 0x58t 0x0t 0x0t
        0x86t 0x68t 0x0t 0x0t
        0xa7t 0x78t 0x0t 0x0t
        0x40t 0x8t 0x0t 0x0t
        0x61t 0x18t 0x0t 0x0t
        0x2t 0x28t 0x0t 0x0t
        0x23t 0x38t 0x0t 0x0t
        0xcct 0xc9t 0x0t 0x0t
        0xedt 0xd9t 0x0t 0x0t
        0x8et 0xe9t 0x0t 0x0t
        0xaft 0xf9t 0x0t 0x0t
        0x48t 0x89t 0x0t 0x0t
        0x69t 0x99t 0x0t 0x0t
        0xat 0xa9t 0x0t 0x0t
        0x2bt 0xb9t 0x0t 0x0t
        0xf5t 0x5at 0x0t 0x0t
        0xd4t 0x4at 0x0t 0x0t
        0xb7t 0x7at 0x0t 0x0t
        0x96t 0x6at 0x0t 0x0t
        0x71t 0x1at 0x0t 0x0t
        0x50t 0xat 0x0t 0x0t
        0x33t 0x3at 0x0t 0x0t
        0x12t 0x2at 0x0t 0x0t
        0xfdt 0xdbt 0x0t 0x0t
        0xdct 0xcbt 0x0t 0x0t
        0xbft 0xfbt 0x0t 0x0t
        0x9et 0xebt 0x0t 0x0t
        0x79t 0x9bt 0x0t 0x0t
        0x58t 0x8bt 0x0t 0x0t
        0x3bt 0xbbt 0x0t 0x0t
        0x1at 0xabt 0x0t 0x0t
        0xa6t 0x6ct 0x0t 0x0t
        0x87t 0x7ct 0x0t 0x0t
        0xe4t 0x4ct 0x0t 0x0t
        0xc5t 0x5ct 0x0t 0x0t
        0x22t 0x2ct 0x0t 0x0t
        0x3t 0x3ct 0x0t 0x0t
        0x60t 0xct 0x0t 0x0t
        0x41t 0x1ct 0x0t 0x0t
        0xaet 0xedt 0x0t 0x0t
        0x8ft 0xfdt 0x0t 0x0t
        0xect 0xcdt 0x0t 0x0t
        0xcdt 0xddt 0x0t 0x0t
        0x2at 0xadt 0x0t 0x0t
        0xbt 0xbdt 0x0t 0x0t
        0x68t 0x8dt 0x0t 0x0t
        0x49t 0x9dt 0x0t 0x0t
        0x97t 0x7et 0x0t 0x0t
        0xb6t 0x6et 0x0t 0x0t
        0xd5t 0x5et 0x0t 0x0t
        0xf4t 0x4et 0x0t 0x0t
        0x13t 0x3et 0x0t 0x0t
        0x32t 0x2et 0x0t 0x0t
        0x51t 0x1et 0x0t 0x0t
        0x70t 0xet 0x0t 0x0t
        0x9ft 0xfft 0x0t 0x0t
        0xbet 0xeft 0x0t 0x0t
        0xddt 0xdft 0x0t 0x0t
        0xfct 0xcft 0x0t 0x0t
        0x1bt 0xbft 0x0t 0x0t
        0x3at 0xaft 0x0t 0x0t
        0x59t 0x9ft 0x0t 0x0t
        0x78t 0x8ft 0x0t 0x0t
        0x88t 0x91t 0x0t 0x0t
        0xa9t 0x81t 0x0t 0x0t
        0xcat 0xb1t 0x0t 0x0t
        0xebt 0xa1t 0x0t 0x0t
        0xct 0xd1t 0x0t 0x0t
        0x2dt 0xc1t 0x0t 0x0t
        0x4et 0xf1t 0x0t 0x0t
        0x6ft 0xe1t 0x0t 0x0t
        0x80t 0x10t 0x0t 0x0t
        0xa1t 0x0t 0x0t 0x0t
        0xc2t 0x30t 0x0t 0x0t
        0xe3t 0x20t 0x0t 0x0t
        0x4t 0x50t 0x0t 0x0t
        0x25t 0x40t 0x0t 0x0t
        0x46t 0x70t 0x0t 0x0t
        0x67t 0x60t 0x0t 0x0t
        0xb9t 0x83t 0x0t 0x0t
        0x98t 0x93t 0x0t 0x0t
        0xfbt 0xa3t 0x0t 0x0t
        0xdat 0xb3t 0x0t 0x0t
        0x3dt 0xc3t 0x0t 0x0t
        0x1ct 0xd3t 0x0t 0x0t
        0x7ft 0xe3t 0x0t 0x0t
        0x5et 0xf3t 0x0t 0x0t
        0xb1t 0x2t 0x0t 0x0t
        0x90t 0x12t 0x0t 0x0t
        0xf3t 0x22t 0x0t 0x0t
        0xd2t 0x32t 0x0t 0x0t
        0x35t 0x42t 0x0t 0x0t
        0x14t 0x52t 0x0t 0x0t
        0x77t 0x62t 0x0t 0x0t
        0x56t 0x72t 0x0t 0x0t
        0xeat 0xb5t 0x0t 0x0t
        0xcbt 0xa5t 0x0t 0x0t
        0xa8t 0x95t 0x0t 0x0t
        0x89t 0x85t 0x0t 0x0t
        0x6et 0xf5t 0x0t 0x0t
        0x4ft 0xe5t 0x0t 0x0t
        0x2ct 0xd5t 0x0t 0x0t
        0xdt 0xc5t 0x0t 0x0t
        0xe2t 0x34t 0x0t 0x0t
        0xc3t 0x24t 0x0t 0x0t
        0xa0t 0x14t 0x0t 0x0t
        0x81t 0x4t 0x0t 0x0t
        0x66t 0x74t 0x0t 0x0t
        0x47t 0x64t 0x0t 0x0t
        0x24t 0x54t 0x0t 0x0t
        0x5t 0x44t 0x0t 0x0t
        0xdbt 0xa7t 0x0t 0x0t
        0xfat 0xb7t 0x0t 0x0t
        0x99t 0x87t 0x0t 0x0t
        0xb8t 0x97t 0x0t 0x0t
        0x5ft 0xe7t 0x0t 0x0t
        0x7et 0xf7t 0x0t 0x0t
        0x1dt 0xc7t 0x0t 0x0t
        0x3ct 0xd7t 0x0t 0x0t
        0xd3t 0x26t 0x0t 0x0t
        0xf2t 0x36t 0x0t 0x0t
        0x91t 0x6t 0x0t 0x0t
        0xb0t 0x16t 0x0t 0x0t
        0x57t 0x66t 0x0t 0x0t
        0x76t 0x76t 0x0t 0x0t
        0x15t 0x46t 0x0t 0x0t
        0x34t 0x56t 0x0t 0x0t
        0x4ct 0xd9t 0x0t 0x0t
        0x6dt 0xc9t 0x0t 0x0t
        0xet 0xf9t 0x0t 0x0t
        0x2ft 0xe9t 0x0t 0x0t
        0xc8t 0x99t 0x0t 0x0t
        0xe9t 0x89t 0x0t 0x0t
        0x8at 0xb9t 0x0t 0x0t
        0xabt 0xa9t 0x0t 0x0t
        0x44t 0x58t 0x0t 0x0t
        0x65t 0x48t 0x0t 0x0t
        0x6t 0x78t 0x0t 0x0t
        0x27t 0x68t 0x0t 0x0t
        0xc0t 0x18t 0x0t 0x0t
        0xe1t 0x8t 0x0t 0x0t
        0x82t 0x38t 0x0t 0x0t
        0xa3t 0x28t 0x0t 0x0t
        0x7dt 0xcbt 0x0t 0x0t
        0x5ct 0xdbt 0x0t 0x0t
        0x3ft 0xebt 0x0t 0x0t
        0x1et 0xfbt 0x0t 0x0t
        0xf9t 0x8bt 0x0t 0x0t
        0xd8t 0x9bt 0x0t 0x0t
        0xbbt 0xabt 0x0t 0x0t
        0x9at 0xbbt 0x0t 0x0t
        0x75t 0x4at 0x0t 0x0t
        0x54t 0x5at 0x0t 0x0t
        0x37t 0x6at 0x0t 0x0t
        0x16t 0x7at 0x0t 0x0t
        0xf1t 0xat 0x0t 0x0t
        0xd0t 0x1at 0x0t 0x0t
        0xb3t 0x2at 0x0t 0x0t
        0x92t 0x3at 0x0t 0x0t
        0x2et 0xfdt 0x0t 0x0t
        0xft 0xedt 0x0t 0x0t
        0x6ct 0xddt 0x0t 0x0t
        0x4dt 0xcdt 0x0t 0x0t
        0xaat 0xbdt 0x0t 0x0t
        0x8bt 0xadt 0x0t 0x0t
        0xe8t 0x9dt 0x0t 0x0t
        0xc9t 0x8dt 0x0t 0x0t
        0x26t 0x7ct 0x0t 0x0t
        0x7t 0x6ct 0x0t 0x0t
        0x64t 0x5ct 0x0t 0x0t
        0x45t 0x4ct 0x0t 0x0t
        0xa2t 0x3ct 0x0t 0x0t
        0x83t 0x2ct 0x0t 0x0t
        0xe0t 0x1ct 0x0t 0x0t
        0xc1t 0xct 0x0t 0x0t
        0x1ft 0xeft 0x0t 0x0t
        0x3et 0xfft 0x0t 0x0t
        0x5dt 0xcft 0x0t 0x0t
        0x7ct 0xdft 0x0t 0x0t
        0x9bt 0xaft 0x0t 0x0t
        0xbat 0xbft 0x0t 0x0t
        0xd9t 0x8ft 0x0t 0x0t
        0xf8t 0x9ft 0x0t 0x0t
        0x17t 0x6et 0x0t 0x0t
        0x36t 0x7et 0x0t 0x0t
        0x55t 0x4et 0x0t 0x0t
        0x74t 0x5et 0x0t 0x0t
        0x93t 0x2et 0x0t 0x0t
        0xb2t 0x3et 0x0t 0x0t
        0xd1t 0xet 0x0t 0x0t
        0xf0t 0x1et 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .registers 5
    .parameter "context"
    .parameter "deviceName"

    #@0
    .prologue
    .line 396
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 331
    const/4 v0, 0x0

    #@4
    iput v0, p0, Lcom/lge/systemservice/service/FeliCaDevice;->m_calibration_retry_count:I

    #@6
    .line 399
    const-string v0, "FeliCaDevice"

    #@8
    const-string v1, "Creating FeliCaDevice"

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 401
    iput-object p1, p0, Lcom/lge/systemservice/service/FeliCaDevice;->mContext:Landroid/content/Context;

    #@f
    .line 402
    iput-object p2, p0, Lcom/lge/systemservice/service/FeliCaDevice;->mDeviceName:Ljava/lang/String;

    #@11
    .line 403
    return-void
.end method

.method private static calcCRC16(IB)I
    .registers 5
    .parameter "crc"
    .parameter "b"

    #@0
    .prologue
    .line 2101
    sget-object v0, Lcom/lge/systemservice/service/FeliCaDevice;->CRC_TABLE:[I

    #@2
    shr-int/lit8 v1, p0, 0x8

    #@4
    and-int/lit16 v2, p1, 0xff

    #@6
    xor-int/2addr v1, v2

    #@7
    and-int/lit16 v1, v1, 0xff

    #@9
    aget v0, v0, v1

    #@b
    shl-int/lit8 v1, p0, 0x8

    #@d
    xor-int/2addr v0, v1

    #@e
    const v1, 0xffff

    #@11
    and-int/2addr v0, v1

    #@12
    return v0
.end method

.method public static calcCRC16([BII)I
    .registers 6
    .parameter "buf"
    .parameter "off"
    .parameter "len"

    #@0
    .prologue
    .line 2106
    const/4 v0, 0x0

    #@1
    .line 2107
    .local v0, crc:I
    move v1, p1

    #@2
    .local v1, i:I
    :goto_2
    add-int v2, p1, p2

    #@4
    if-ge v1, v2, :cond_f

    #@6
    .line 2108
    aget-byte v2, p0, v1

    #@8
    invoke-static {v0, v2}, Lcom/lge/systemservice/service/FeliCaDevice;->calcCRC16(IB)I

    #@b
    move-result v0

    #@c
    .line 2107
    add-int/lit8 v1, v1, 0x1

    #@e
    goto :goto_2

    #@f
    .line 2110
    :cond_f
    return v0
.end method

.method private calibrate_frequency(F)I
    .registers 14
    .parameter "in_freq"

    #@0
    .prologue
    const/4 v7, -0x1

    #@1
    const/4 v6, 0x1

    #@2
    const/4 v8, 0x0

    #@3
    .line 1881
    const/4 v3, 0x0

    #@4
    .line 1882
    .local v3, i:I
    const/4 v5, 0x0

    #@5
    .line 1883
    .local v5, sw_val:I
    const/4 v0, 0x0

    #@6
    .line 1884
    .local v0, bMatch:I
    const/4 v4, 0x1

    #@7
    .line 1886
    .local v4, m_freq_cal_cur_sw_idx:I
    const-string v9, "FeliCaDevice"

    #@9
    const-string v10, "calibrate_frequency"

    #@b
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 1887
    const-string v9, "FeliCaDevice"

    #@10
    new-instance v10, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v11, "m_calibration_retry_count : "

    #@17
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v10

    #@1b
    iget v11, p0, Lcom/lge/systemservice/service/FeliCaDevice;->m_calibration_retry_count:I

    #@1d
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v10

    #@21
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v10

    #@25
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 1891
    sget v9, Lcom/lge/systemservice/service/FeliCaDevice;->CALIBRATION_CENTER_FREQ:F

    #@2a
    sget v10, Lcom/lge/systemservice/service/FeliCaDevice;->CALIBRATION_PASS_RANGE:F

    #@2c
    sub-float v2, v9, v10

    #@2e
    .line 1892
    .local v2, freq_min:F
    sget v9, Lcom/lge/systemservice/service/FeliCaDevice;->CALIBRATION_CENTER_FREQ:F

    #@30
    sget v10, Lcom/lge/systemservice/service/FeliCaDevice;->CALIBRATION_PASS_RANGE:F

    #@32
    add-float v1, v9, v10

    #@34
    .line 1895
    .local v1, freq_max:F
    cmpl-float v9, p1, v2

    #@36
    if-ltz v9, :cond_4d

    #@38
    cmpg-float v9, p1, v1

    #@3a
    if-gtz v9, :cond_4d

    #@3c
    .line 1896
    const-string v7, "FeliCaDevice"

    #@3e
    const-string v9, "freq is in proper range."

    #@40
    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    .line 1898
    const-string v7, "FeliCaDevice"

    #@45
    const-string v9, "m_calibration_retry_count set zero."

    #@47
    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    .line 1899
    iput v8, p0, Lcom/lge/systemservice/service/FeliCaDevice;->m_calibration_retry_count:I

    #@4c
    .line 2001
    :goto_4c
    return v6

    #@4d
    .line 1906
    :cond_4d
    iget v9, p0, Lcom/lge/systemservice/service/FeliCaDevice;->m_calibration_retry_count:I

    #@4f
    const/16 v10, 0x71a

    #@51
    if-lt v9, v10, :cond_65

    #@53
    .line 1907
    const-string v6, "FeliCaDevice"

    #@55
    const-string v9, "freq cal try count is over max : 1818"

    #@57
    invoke-static {v6, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5a
    .line 1910
    const-string v6, "FeliCaDevice"

    #@5c
    const-string v9, "m_calibration_retry_count set zero."

    #@5e
    invoke-static {v6, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@61
    .line 1911
    iput v8, p0, Lcom/lge/systemservice/service/FeliCaDevice;->m_calibration_retry_count:I

    #@63
    move v6, v7

    #@64
    .line 1912
    goto :goto_4c

    #@65
    .line 1917
    :cond_65
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->read_switch_value()I

    #@68
    move-result v5

    #@69
    .line 1919
    if-gez v5, :cond_74

    #@6b
    .line 1920
    const-string v9, "FeliCaDevice"

    #@6d
    const-string v10, "read_switch_value failed(). Using sw_val with default."

    #@6f
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@72
    .line 1922
    const/16 v5, 0x3f

    #@74
    .line 1926
    :cond_74
    const/4 v3, 0x0

    #@75
    :goto_75
    sget v9, Lcom/lge/systemservice/service/FeliCaDevice;->CAL_ENTRY_COUNT:I

    #@77
    if-ge v3, v9, :cond_90

    #@79
    .line 1927
    sget-object v9, Lcom/lge/systemservice/service/FeliCaDevice;->RF_CALIBRATION_TABLE:[[I

    #@7b
    aget-object v9, v9, v3

    #@7d
    aget v9, v9, v8

    #@7f
    and-int/lit8 v10, v5, 0x7

    #@81
    if-ne v9, v10, :cond_b7

    #@83
    sget-object v9, Lcom/lge/systemservice/service/FeliCaDevice;->RF_CALIBRATION_TABLE:[[I

    #@85
    aget-object v9, v9, v3

    #@87
    aget v9, v9, v6

    #@89
    and-int/lit8 v10, v5, 0x38

    #@8b
    shr-int/lit8 v10, v10, 0x3

    #@8d
    if-ne v9, v10, :cond_b7

    #@8f
    .line 1929
    const/4 v0, 0x1

    #@90
    .line 1934
    :cond_90
    if-ne v0, v6, :cond_ba

    #@92
    .line 1936
    const-string v9, "FeliCaDevice"

    #@94
    const-string v10, "found matching switch value in cal table."

    #@96
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@99
    .line 1937
    add-int/lit8 v4, v3, 0x1

    #@9b
    .line 1948
    :goto_9b
    sget v9, Lcom/lge/systemservice/service/FeliCaDevice;->CALIBRATION_CENTER_FREQ:F

    #@9d
    cmpl-float v9, p1, v9

    #@9f
    if-lez v9, :cond_f8

    #@a1
    .line 1950
    sget v6, Lcom/lge/systemservice/service/FeliCaDevice;->CAL_ENTRY_COUNT:I

    #@a3
    if-ne v4, v6, :cond_d7

    #@a5
    .line 1951
    const-string v6, "FeliCaDevice"

    #@a7
    const-string v9, "CAL-NG : cal index is out of range. reached to upper limit."

    #@a9
    invoke-static {v6, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ac
    .line 1955
    const-string v6, "FeliCaDevice"

    #@ae
    const-string v9, "m_calibration_retry_count set zero."

    #@b0
    invoke-static {v6, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b3
    .line 1957
    iput v8, p0, Lcom/lge/systemservice/service/FeliCaDevice;->m_calibration_retry_count:I

    #@b5
    move v6, v7

    #@b6
    .line 1958
    goto :goto_4c

    #@b7
    .line 1926
    :cond_b7
    add-int/lit8 v3, v3, 0x1

    #@b9
    goto :goto_75

    #@ba
    .line 1939
    :cond_ba
    const-string v9, "FeliCaDevice"

    #@bc
    new-instance v10, Ljava/lang/StringBuilder;

    #@be
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@c1
    const-string v11, "can\'t found matching switch value in cal table.\nSet sw_index to default value : "

    #@c3
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v10

    #@c7
    sget v11, Lcom/lge/systemservice/service/FeliCaDevice;->CALIBRATION_DEFAULT_SWITCH_INDEX:I

    #@c9
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@cc
    move-result-object v10

    #@cd
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d0
    move-result-object v10

    #@d1
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d4
    .line 1944
    sget v4, Lcom/lge/systemservice/service/FeliCaDevice;->CALIBRATION_DEFAULT_SWITCH_INDEX:I

    #@d6
    goto :goto_9b

    #@d7
    .line 1960
    :cond_d7
    const-string v6, "FeliCaDevice"

    #@d9
    const-string v9, "step up sw_index."

    #@db
    invoke-static {v6, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@de
    .line 1963
    add-int/lit8 v4, v4, 0x2

    #@e0
    .line 1965
    sget v6, Lcom/lge/systemservice/service/FeliCaDevice;->CAL_ENTRY_COUNT:I

    #@e2
    if-le v4, v6, :cond_e6

    #@e4
    .line 1966
    sget v4, Lcom/lge/systemservice/service/FeliCaDevice;->CAL_ENTRY_COUNT:I

    #@e6
    .line 1989
    :cond_e6
    :goto_e6
    invoke-static {v4}, Lcom/lge/systemservice/service/FeliCaDevice;->set_switch_by_idx(I)I

    #@e9
    move-result v6

    #@ea
    if-gez v6, :cond_11a

    #@ec
    .line 1990
    const-string v6, "FeliCaDevice"

    #@ee
    const-string v9, "m_calibration_retry_count set zero."

    #@f0
    invoke-static {v6, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f3
    .line 1991
    iput v8, p0, Lcom/lge/systemservice/service/FeliCaDevice;->m_calibration_retry_count:I

    #@f5
    move v6, v7

    #@f6
    .line 1992
    goto/16 :goto_4c

    #@f8
    .line 1970
    :cond_f8
    if-ne v4, v6, :cond_10d

    #@fa
    .line 1971
    const-string v6, "FeliCaDevice"

    #@fc
    const-string v9, "CAL-NG : cal index is out of range. reached to lower limit"

    #@fe
    invoke-static {v6, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@101
    .line 1975
    const-string v6, "FeliCaDevice"

    #@103
    const-string v9, "m_calibration_retry_count set zero."

    #@105
    invoke-static {v6, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@108
    .line 1976
    iput v8, p0, Lcom/lge/systemservice/service/FeliCaDevice;->m_calibration_retry_count:I

    #@10a
    move v6, v7

    #@10b
    .line 1977
    goto/16 :goto_4c

    #@10d
    .line 1979
    :cond_10d
    const-string v9, "FeliCaDevice"

    #@10f
    const-string v10, "step down sw_index."

    #@111
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@114
    .line 1981
    add-int/lit8 v4, v4, -0x2

    #@116
    .line 1983
    if-ge v4, v6, :cond_e6

    #@118
    .line 1984
    const/4 v4, 0x1

    #@119
    goto :goto_e6

    #@11a
    .line 1996
    :cond_11a
    iget v6, p0, Lcom/lge/systemservice/service/FeliCaDevice;->m_calibration_retry_count:I

    #@11c
    add-int/lit8 v6, v6, 0x1

    #@11e
    iput v6, p0, Lcom/lge/systemservice/service/FeliCaDevice;->m_calibration_retry_count:I

    #@120
    .line 1998
    const-string v6, "FeliCaDevice"

    #@122
    new-instance v7, Ljava/lang/StringBuilder;

    #@124
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@127
    const-string v9, "CAL-RETRY is required. retry count upto now : "

    #@129
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12c
    move-result-object v7

    #@12d
    iget v9, p0, Lcom/lge/systemservice/service/FeliCaDevice;->m_calibration_retry_count:I

    #@12f
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@132
    move-result-object v7

    #@133
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@136
    move-result-object v7

    #@137
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@13a
    move v6, v8

    #@13b
    .line 2001
    goto/16 :goto_4c
.end method

.method private checkRFRegCal()Z
    .registers 12

    #@0
    .prologue
    const/4 v10, 0x4

    #@1
    const/4 v9, 0x3

    #@2
    const/4 v8, 0x2

    #@3
    const/4 v1, 0x1

    #@4
    const/4 v2, 0x0

    #@5
    .line 681
    const/16 v3, 0x1c

    #@7
    new-array v0, v3, [B

    #@9
    .line 683
    .local v0, rfRegs:[B
    array-length v3, v0

    #@a
    invoke-static {v0, v3}, Lcom/lge/systemservice/service/FeliCaDevice;->readRFRegisters([BI)Z

    #@d
    move-result v3

    #@e
    if-eqz v3, :cond_2eb

    #@10
    .line 688
    const-string v3, "FeliCaDevice"

    #@12
    const-string v4, "rfRegs-read : 0x%x - 0x%x, 0x%x - 0x%x, 0x%x - 0x%x, 0x%x - 0x%x, 0x%x - 0x%x, 0x%x - 0x%x, 0x%x - 0x%x,0x%x - 0x%x, 0x%x - 0x%x, 0x%x - 0x%x, 0x%x - 0x%x, 0x%x - 0x%x, 0x%x - 0x%x, 0x%x - 0x%x"

    #@14
    const/16 v5, 0x1c

    #@16
    new-array v5, v5, [Ljava/lang/Object;

    #@18
    aget-byte v6, v0, v2

    #@1a
    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@1d
    move-result-object v6

    #@1e
    aput-object v6, v5, v2

    #@20
    aget-byte v6, v0, v1

    #@22
    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@25
    move-result-object v6

    #@26
    aput-object v6, v5, v1

    #@28
    aget-byte v6, v0, v8

    #@2a
    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@2d
    move-result-object v6

    #@2e
    aput-object v6, v5, v8

    #@30
    aget-byte v6, v0, v9

    #@32
    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@35
    move-result-object v6

    #@36
    aput-object v6, v5, v9

    #@38
    aget-byte v6, v0, v10

    #@3a
    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@3d
    move-result-object v6

    #@3e
    aput-object v6, v5, v10

    #@40
    const/4 v6, 0x5

    #@41
    const/4 v7, 0x5

    #@42
    aget-byte v7, v0, v7

    #@44
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@47
    move-result-object v7

    #@48
    aput-object v7, v5, v6

    #@4a
    const/4 v6, 0x6

    #@4b
    const/4 v7, 0x6

    #@4c
    aget-byte v7, v0, v7

    #@4e
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@51
    move-result-object v7

    #@52
    aput-object v7, v5, v6

    #@54
    const/4 v6, 0x7

    #@55
    const/4 v7, 0x7

    #@56
    aget-byte v7, v0, v7

    #@58
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@5b
    move-result-object v7

    #@5c
    aput-object v7, v5, v6

    #@5e
    const/16 v6, 0x8

    #@60
    const/16 v7, 0x8

    #@62
    aget-byte v7, v0, v7

    #@64
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@67
    move-result-object v7

    #@68
    aput-object v7, v5, v6

    #@6a
    const/16 v6, 0x9

    #@6c
    const/16 v7, 0x9

    #@6e
    aget-byte v7, v0, v7

    #@70
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@73
    move-result-object v7

    #@74
    aput-object v7, v5, v6

    #@76
    const/16 v6, 0xa

    #@78
    const/16 v7, 0xa

    #@7a
    aget-byte v7, v0, v7

    #@7c
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@7f
    move-result-object v7

    #@80
    aput-object v7, v5, v6

    #@82
    const/16 v6, 0xb

    #@84
    const/16 v7, 0xb

    #@86
    aget-byte v7, v0, v7

    #@88
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@8b
    move-result-object v7

    #@8c
    aput-object v7, v5, v6

    #@8e
    const/16 v6, 0xc

    #@90
    const/16 v7, 0xc

    #@92
    aget-byte v7, v0, v7

    #@94
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@97
    move-result-object v7

    #@98
    aput-object v7, v5, v6

    #@9a
    const/16 v6, 0xd

    #@9c
    const/16 v7, 0xd

    #@9e
    aget-byte v7, v0, v7

    #@a0
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@a3
    move-result-object v7

    #@a4
    aput-object v7, v5, v6

    #@a6
    const/16 v6, 0xe

    #@a8
    const/16 v7, 0xe

    #@aa
    aget-byte v7, v0, v7

    #@ac
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@af
    move-result-object v7

    #@b0
    aput-object v7, v5, v6

    #@b2
    const/16 v6, 0xf

    #@b4
    const/16 v7, 0xf

    #@b6
    aget-byte v7, v0, v7

    #@b8
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@bb
    move-result-object v7

    #@bc
    aput-object v7, v5, v6

    #@be
    const/16 v6, 0x10

    #@c0
    const/16 v7, 0x10

    #@c2
    aget-byte v7, v0, v7

    #@c4
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@c7
    move-result-object v7

    #@c8
    aput-object v7, v5, v6

    #@ca
    const/16 v6, 0x11

    #@cc
    const/16 v7, 0x11

    #@ce
    aget-byte v7, v0, v7

    #@d0
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@d3
    move-result-object v7

    #@d4
    aput-object v7, v5, v6

    #@d6
    const/16 v6, 0x12

    #@d8
    const/16 v7, 0x12

    #@da
    aget-byte v7, v0, v7

    #@dc
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@df
    move-result-object v7

    #@e0
    aput-object v7, v5, v6

    #@e2
    const/16 v6, 0x13

    #@e4
    const/16 v7, 0x13

    #@e6
    aget-byte v7, v0, v7

    #@e8
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@eb
    move-result-object v7

    #@ec
    aput-object v7, v5, v6

    #@ee
    const/16 v6, 0x14

    #@f0
    const/16 v7, 0x14

    #@f2
    aget-byte v7, v0, v7

    #@f4
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@f7
    move-result-object v7

    #@f8
    aput-object v7, v5, v6

    #@fa
    const/16 v6, 0x15

    #@fc
    const/16 v7, 0x15

    #@fe
    aget-byte v7, v0, v7

    #@100
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@103
    move-result-object v7

    #@104
    aput-object v7, v5, v6

    #@106
    const/16 v6, 0x16

    #@108
    const/16 v7, 0x16

    #@10a
    aget-byte v7, v0, v7

    #@10c
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@10f
    move-result-object v7

    #@110
    aput-object v7, v5, v6

    #@112
    const/16 v6, 0x17

    #@114
    const/16 v7, 0x17

    #@116
    aget-byte v7, v0, v7

    #@118
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@11b
    move-result-object v7

    #@11c
    aput-object v7, v5, v6

    #@11e
    const/16 v6, 0x18

    #@120
    const/16 v7, 0x18

    #@122
    aget-byte v7, v0, v7

    #@124
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@127
    move-result-object v7

    #@128
    aput-object v7, v5, v6

    #@12a
    const/16 v6, 0x19

    #@12c
    const/16 v7, 0x19

    #@12e
    aget-byte v7, v0, v7

    #@130
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@133
    move-result-object v7

    #@134
    aput-object v7, v5, v6

    #@136
    const/16 v6, 0x1a

    #@138
    const/16 v7, 0x1a

    #@13a
    aget-byte v7, v0, v7

    #@13c
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@13f
    move-result-object v7

    #@140
    aput-object v7, v5, v6

    #@142
    const/16 v6, 0x1b

    #@144
    const/16 v7, 0x1b

    #@146
    aget-byte v7, v0, v7

    #@148
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@14b
    move-result-object v7

    #@14c
    aput-object v7, v5, v6

    #@14e
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@151
    move-result-object v4

    #@152
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@155
    .line 697
    const-string v3, "FeliCaDevice"

    #@157
    const-string v4, "rfRegs-init : 0x%x - 0x%x, 0x%x - 0x%x, 0x%x - 0x%x, 0x%x - 0x%x, 0x%x - 0x%x, 0x%x - 0x%x, 0x%x - 0x%x,0x%x - 0x%x, 0x%x - 0x%x, 0x%x - 0x%x, 0x%x - 0x%x, 0x%x - 0x%x, 0x%x - 0x%x, 0x%x - 0x%x"

    #@159
    const/16 v5, 0x1c

    #@15b
    new-array v5, v5, [Ljava/lang/Object;

    #@15d
    sget-object v6, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT:[B

    #@15f
    aget-byte v6, v6, v2

    #@161
    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@164
    move-result-object v6

    #@165
    aput-object v6, v5, v2

    #@167
    sget-object v6, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT:[B

    #@169
    aget-byte v6, v6, v1

    #@16b
    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@16e
    move-result-object v6

    #@16f
    aput-object v6, v5, v1

    #@171
    sget-object v6, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT:[B

    #@173
    aget-byte v6, v6, v8

    #@175
    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@178
    move-result-object v6

    #@179
    aput-object v6, v5, v8

    #@17b
    sget-object v6, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT:[B

    #@17d
    aget-byte v6, v6, v9

    #@17f
    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@182
    move-result-object v6

    #@183
    aput-object v6, v5, v9

    #@185
    sget-object v6, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT:[B

    #@187
    aget-byte v6, v6, v10

    #@189
    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@18c
    move-result-object v6

    #@18d
    aput-object v6, v5, v10

    #@18f
    const/4 v6, 0x5

    #@190
    sget-object v7, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT:[B

    #@192
    const/4 v8, 0x5

    #@193
    aget-byte v7, v7, v8

    #@195
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@198
    move-result-object v7

    #@199
    aput-object v7, v5, v6

    #@19b
    const/4 v6, 0x6

    #@19c
    sget-object v7, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT:[B

    #@19e
    const/4 v8, 0x6

    #@19f
    aget-byte v7, v7, v8

    #@1a1
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@1a4
    move-result-object v7

    #@1a5
    aput-object v7, v5, v6

    #@1a7
    const/4 v6, 0x7

    #@1a8
    sget-object v7, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT:[B

    #@1aa
    const/4 v8, 0x7

    #@1ab
    aget-byte v7, v7, v8

    #@1ad
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@1b0
    move-result-object v7

    #@1b1
    aput-object v7, v5, v6

    #@1b3
    const/16 v6, 0x8

    #@1b5
    sget-object v7, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT:[B

    #@1b7
    const/16 v8, 0x8

    #@1b9
    aget-byte v7, v7, v8

    #@1bb
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@1be
    move-result-object v7

    #@1bf
    aput-object v7, v5, v6

    #@1c1
    const/16 v6, 0x9

    #@1c3
    sget-object v7, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT:[B

    #@1c5
    const/16 v8, 0x9

    #@1c7
    aget-byte v7, v7, v8

    #@1c9
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@1cc
    move-result-object v7

    #@1cd
    aput-object v7, v5, v6

    #@1cf
    const/16 v6, 0xa

    #@1d1
    sget-object v7, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT:[B

    #@1d3
    const/16 v8, 0xa

    #@1d5
    aget-byte v7, v7, v8

    #@1d7
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@1da
    move-result-object v7

    #@1db
    aput-object v7, v5, v6

    #@1dd
    const/16 v6, 0xb

    #@1df
    sget-object v7, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT:[B

    #@1e1
    const/16 v8, 0xb

    #@1e3
    aget-byte v7, v7, v8

    #@1e5
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@1e8
    move-result-object v7

    #@1e9
    aput-object v7, v5, v6

    #@1eb
    const/16 v6, 0xc

    #@1ed
    sget-object v7, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT:[B

    #@1ef
    const/16 v8, 0xc

    #@1f1
    aget-byte v7, v7, v8

    #@1f3
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@1f6
    move-result-object v7

    #@1f7
    aput-object v7, v5, v6

    #@1f9
    const/16 v6, 0xd

    #@1fb
    sget-object v7, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT:[B

    #@1fd
    const/16 v8, 0xd

    #@1ff
    aget-byte v7, v7, v8

    #@201
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@204
    move-result-object v7

    #@205
    aput-object v7, v5, v6

    #@207
    const/16 v6, 0xe

    #@209
    sget-object v7, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT:[B

    #@20b
    const/16 v8, 0xe

    #@20d
    aget-byte v7, v7, v8

    #@20f
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@212
    move-result-object v7

    #@213
    aput-object v7, v5, v6

    #@215
    const/16 v6, 0xf

    #@217
    sget-object v7, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT:[B

    #@219
    const/16 v8, 0xf

    #@21b
    aget-byte v7, v7, v8

    #@21d
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@220
    move-result-object v7

    #@221
    aput-object v7, v5, v6

    #@223
    const/16 v6, 0x10

    #@225
    sget-object v7, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT:[B

    #@227
    const/16 v8, 0x10

    #@229
    aget-byte v7, v7, v8

    #@22b
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@22e
    move-result-object v7

    #@22f
    aput-object v7, v5, v6

    #@231
    const/16 v6, 0x11

    #@233
    sget-object v7, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT:[B

    #@235
    const/16 v8, 0x11

    #@237
    aget-byte v7, v7, v8

    #@239
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@23c
    move-result-object v7

    #@23d
    aput-object v7, v5, v6

    #@23f
    const/16 v6, 0x12

    #@241
    sget-object v7, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT:[B

    #@243
    const/16 v8, 0x12

    #@245
    aget-byte v7, v7, v8

    #@247
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@24a
    move-result-object v7

    #@24b
    aput-object v7, v5, v6

    #@24d
    const/16 v6, 0x13

    #@24f
    sget-object v7, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT:[B

    #@251
    const/16 v8, 0x13

    #@253
    aget-byte v7, v7, v8

    #@255
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@258
    move-result-object v7

    #@259
    aput-object v7, v5, v6

    #@25b
    const/16 v6, 0x14

    #@25d
    sget-object v7, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT:[B

    #@25f
    const/16 v8, 0x14

    #@261
    aget-byte v7, v7, v8

    #@263
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@266
    move-result-object v7

    #@267
    aput-object v7, v5, v6

    #@269
    const/16 v6, 0x15

    #@26b
    sget-object v7, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT:[B

    #@26d
    const/16 v8, 0x15

    #@26f
    aget-byte v7, v7, v8

    #@271
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@274
    move-result-object v7

    #@275
    aput-object v7, v5, v6

    #@277
    const/16 v6, 0x16

    #@279
    sget-object v7, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT:[B

    #@27b
    const/16 v8, 0x16

    #@27d
    aget-byte v7, v7, v8

    #@27f
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@282
    move-result-object v7

    #@283
    aput-object v7, v5, v6

    #@285
    const/16 v6, 0x17

    #@287
    sget-object v7, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT:[B

    #@289
    const/16 v8, 0x17

    #@28b
    aget-byte v7, v7, v8

    #@28d
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@290
    move-result-object v7

    #@291
    aput-object v7, v5, v6

    #@293
    const/16 v6, 0x18

    #@295
    sget-object v7, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT:[B

    #@297
    const/16 v8, 0x18

    #@299
    aget-byte v7, v7, v8

    #@29b
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@29e
    move-result-object v7

    #@29f
    aput-object v7, v5, v6

    #@2a1
    const/16 v6, 0x19

    #@2a3
    sget-object v7, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT:[B

    #@2a5
    const/16 v8, 0x19

    #@2a7
    aget-byte v7, v7, v8

    #@2a9
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@2ac
    move-result-object v7

    #@2ad
    aput-object v7, v5, v6

    #@2af
    const/16 v6, 0x1a

    #@2b1
    sget-object v7, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT:[B

    #@2b3
    const/16 v8, 0x1a

    #@2b5
    aget-byte v7, v7, v8

    #@2b7
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@2ba
    move-result-object v7

    #@2bb
    aput-object v7, v5, v6

    #@2bd
    const/16 v6, 0x1b

    #@2bf
    sget-object v7, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT:[B

    #@2c1
    const/16 v8, 0x1b

    #@2c3
    aget-byte v7, v7, v8

    #@2c5
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@2c8
    move-result-object v7

    #@2c9
    aput-object v7, v5, v6

    #@2cb
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@2ce
    move-result-object v4

    #@2cf
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2d2
    .line 711
    sget-object v3, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT:[B

    #@2d4
    invoke-static {v0, v3}, Ljava/util/Arrays;->equals([B[B)Z

    #@2d7
    move-result v3

    #@2d8
    if-eqz v3, :cond_2e2

    #@2da
    .line 713
    const-string v2, "FeliCaDevice"

    #@2dc
    const-string v3, "RF register values init ok"

    #@2de
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2e1
    .line 725
    :goto_2e1
    return v1

    #@2e2
    .line 718
    :cond_2e2
    const-string v1, "FeliCaDevice"

    #@2e4
    const-string v3, "RF register values are different."

    #@2e6
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2e9
    move v1, v2

    #@2ea
    .line 719
    goto :goto_2e1

    #@2eb
    .line 724
    :cond_2eb
    const-string v1, "FeliCaDevice"

    #@2ed
    const-string v3, "RF register values init fail"

    #@2ef
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2f2
    move v1, v2

    #@2f3
    .line 725
    goto :goto_2e1
.end method

.method static felica_pon_close()I
    .registers 7

    #@0
    .prologue
    const/4 v3, -0x1

    #@1
    const/4 v4, 0x0

    #@2
    const/4 v6, 0x0

    #@3
    .line 1172
    const/4 v5, 0x1

    #@4
    new-array v2, v5, [B

    #@6
    aput-byte v4, v2, v4

    #@8
    .line 1177
    .local v2, pon_data_off:[B
    :try_start_8
    sget-object v5, Lcom/lge/systemservice/service/FeliCaDevice;->fos_pon:Ljava/io/FileOutputStream;
    :try_end_a
    .catchall {:try_start_8 .. :try_end_a} :catchall_49
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_a} :catch_34

    #@a
    if-nez v5, :cond_1d

    #@c
    .line 1194
    sget-object v4, Lcom/lge/systemservice/service/FeliCaDevice;->fos_pon:Ljava/io/FileOutputStream;

    #@e
    if-eqz v4, :cond_17

    #@10
    .line 1196
    :try_start_10
    sget-object v4, Lcom/lge/systemservice/service/FeliCaDevice;->fos_pon:Ljava/io/FileOutputStream;

    #@12
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_15} :catch_18

    #@15
    .line 1201
    :goto_15
    sput-object v6, Lcom/lge/systemservice/service/FeliCaDevice;->fos_pon:Ljava/io/FileOutputStream;

    #@17
    :cond_17
    :goto_17
    return v3

    #@18
    .line 1197
    :catch_18
    move-exception v1

    #@19
    .line 1198
    .local v1, ignore:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@1c
    goto :goto_15

    #@1d
    .line 1183
    .end local v1           #ignore:Ljava/io/IOException;
    :cond_1d
    :try_start_1d
    sget-object v5, Lcom/lge/systemservice/service/FeliCaDevice;->fos_pon:Ljava/io/FileOutputStream;

    #@1f
    invoke-virtual {v5, v2}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_22
    .catchall {:try_start_1d .. :try_end_22} :catchall_49
    .catch Ljava/io/IOException; {:try_start_1d .. :try_end_22} :catch_34

    #@22
    .line 1194
    sget-object v3, Lcom/lge/systemservice/service/FeliCaDevice;->fos_pon:Ljava/io/FileOutputStream;

    #@24
    if-eqz v3, :cond_2d

    #@26
    .line 1196
    :try_start_26
    sget-object v3, Lcom/lge/systemservice/service/FeliCaDevice;->fos_pon:Ljava/io/FileOutputStream;

    #@28
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_2b
    .catch Ljava/io/IOException; {:try_start_26 .. :try_end_2b} :catch_2f

    #@2b
    .line 1201
    :goto_2b
    sput-object v6, Lcom/lge/systemservice/service/FeliCaDevice;->fos_pon:Ljava/io/FileOutputStream;

    #@2d
    :cond_2d
    move v3, v4

    #@2e
    goto :goto_17

    #@2f
    .line 1197
    :catch_2f
    move-exception v1

    #@30
    .line 1198
    .restart local v1       #ignore:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@33
    goto :goto_2b

    #@34
    .line 1187
    .end local v1           #ignore:Ljava/io/IOException;
    :catch_34
    move-exception v0

    #@35
    .line 1190
    .local v0, e:Ljava/io/IOException;
    :try_start_35
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_38
    .catchall {:try_start_35 .. :try_end_38} :catchall_49

    #@38
    .line 1194
    sget-object v4, Lcom/lge/systemservice/service/FeliCaDevice;->fos_pon:Ljava/io/FileOutputStream;

    #@3a
    if-eqz v4, :cond_17

    #@3c
    .line 1196
    :try_start_3c
    sget-object v4, Lcom/lge/systemservice/service/FeliCaDevice;->fos_pon:Ljava/io/FileOutputStream;

    #@3e
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_41
    .catch Ljava/io/IOException; {:try_start_3c .. :try_end_41} :catch_44

    #@41
    .line 1201
    :goto_41
    sput-object v6, Lcom/lge/systemservice/service/FeliCaDevice;->fos_pon:Ljava/io/FileOutputStream;

    #@43
    goto :goto_17

    #@44
    .line 1197
    :catch_44
    move-exception v1

    #@45
    .line 1198
    .restart local v1       #ignore:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@48
    goto :goto_41

    #@49
    .line 1194
    .end local v0           #e:Ljava/io/IOException;
    .end local v1           #ignore:Ljava/io/IOException;
    :catchall_49
    move-exception v3

    #@4a
    sget-object v4, Lcom/lge/systemservice/service/FeliCaDevice;->fos_pon:Ljava/io/FileOutputStream;

    #@4c
    if-eqz v4, :cond_55

    #@4e
    .line 1196
    :try_start_4e
    sget-object v4, Lcom/lge/systemservice/service/FeliCaDevice;->fos_pon:Ljava/io/FileOutputStream;

    #@50
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_53
    .catch Ljava/io/IOException; {:try_start_4e .. :try_end_53} :catch_56

    #@53
    .line 1201
    :goto_53
    sput-object v6, Lcom/lge/systemservice/service/FeliCaDevice;->fos_pon:Ljava/io/FileOutputStream;

    #@55
    :cond_55
    throw v3

    #@56
    .line 1197
    :catch_56
    move-exception v1

    #@57
    .line 1198
    .restart local v1       #ignore:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@5a
    goto :goto_53
.end method

.method private static felica_pon_open()I
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    const/4 v4, -0x1

    #@3
    .line 1133
    new-array v2, v5, [B

    #@5
    aput-byte v5, v2, v3

    #@7
    .line 1138
    .local v2, pon_data_on:[B
    :try_start_7
    sget-object v5, Lcom/lge/systemservice/service/FeliCaDevice;->fos_pon:Ljava/io/FileOutputStream;

    #@9
    if-nez v5, :cond_14

    #@b
    .line 1139
    new-instance v5, Ljava/io/FileOutputStream;

    #@d
    const-string v6, "/dev/felica_pon"

    #@f
    invoke-direct {v5, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    #@12
    sput-object v5, Lcom/lge/systemservice/service/FeliCaDevice;->fos_pon:Ljava/io/FileOutputStream;

    #@14
    .line 1142
    :cond_14
    sget-object v5, Lcom/lge/systemservice/service/FeliCaDevice;->fos_pon:Ljava/io/FileOutputStream;

    #@16
    invoke-virtual {v5, v2}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_19
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_19} :catch_1a
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_19} :catch_20

    #@19
    .line 1168
    :goto_19
    return v3

    #@1a
    .line 1144
    :catch_1a
    move-exception v0

    #@1b
    .line 1146
    .local v0, e:Ljava/io/FileNotFoundException;
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    #@1e
    move v3, v4

    #@1f
    .line 1147
    goto :goto_19

    #@20
    .line 1149
    .end local v0           #e:Ljava/io/FileNotFoundException;
    :catch_20
    move-exception v0

    #@21
    .line 1153
    .local v0, e:Ljava/io/IOException;
    sget-object v3, Lcom/lge/systemservice/service/FeliCaDevice;->fos_pon:Ljava/io/FileOutputStream;

    #@23
    if-eqz v3, :cond_2d

    #@25
    .line 1155
    :try_start_25
    sget-object v3, Lcom/lge/systemservice/service/FeliCaDevice;->fos_pon:Ljava/io/FileOutputStream;

    #@27
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_2a
    .catch Ljava/io/IOException; {:try_start_25 .. :try_end_2a} :catch_32

    #@2a
    .line 1160
    :goto_2a
    const/4 v3, 0x0

    #@2b
    sput-object v3, Lcom/lge/systemservice/service/FeliCaDevice;->fos_pon:Ljava/io/FileOutputStream;

    #@2d
    .line 1163
    :cond_2d
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@30
    move v3, v4

    #@31
    .line 1165
    goto :goto_19

    #@32
    .line 1156
    :catch_32
    move-exception v1

    #@33
    .line 1157
    .local v1, ignore:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@36
    goto :goto_2a
.end method

.method private felica_read_ext_idm([BI)I
    .registers 21
    .parameter "idm"
    .parameter "idm_buf_size"

    #@0
    .prologue
    .line 1441
    const/4 v12, 0x0

    #@1
    .line 1442
    .local v12, uart_result_len:I
    const/16 v13, 0x400

    #@3
    new-array v7, v13, [B

    #@5
    .line 1443
    .local v7, felica_uart_buff:[B
    const-string v3, "FeliCaTest-IDM"

    #@7
    .line 1445
    .local v3, LOG_TAG:Ljava/lang/String;
    const-string v13, "FeliCaTest-IDM"

    #@9
    const-string v14, "Openning FeliCa-UART driver."

    #@b
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 1447
    const/4 v13, 0x0

    #@f
    invoke-static {v7, v13}, Ljava/util/Arrays;->fill([BB)V

    #@12
    .line 1448
    const/4 v13, 0x0

    #@13
    move-object/from16 v0, p1

    #@15
    invoke-static {v0, v13}, Ljava/util/Arrays;->fill([BB)V

    #@18
    .line 1450
    const/4 v8, 0x0

    #@19
    .line 1451
    .local v8, fis_felica_uart:Ljava/io/FileInputStream;
    const/4 v10, 0x0

    #@1a
    .line 1456
    .local v10, fos_felica_uart:Ljava/io/FileOutputStream;
    :try_start_1a
    new-instance v9, Ljava/io/FileInputStream;

    #@1c
    const-string v13, "/dev/felica"

    #@1e
    invoke-direct {v9, v13}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_21
    .catchall {:try_start_1a .. :try_end_21} :catchall_5dc
    .catch Ljava/io/FileNotFoundException; {:try_start_1a .. :try_end_21} :catch_5ea
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_21} :catch_5e3

    #@21
    .line 1457
    .end local v8           #fis_felica_uart:Ljava/io/FileInputStream;
    .local v9, fis_felica_uart:Ljava/io/FileInputStream;
    :try_start_21
    new-instance v11, Ljava/io/FileOutputStream;

    #@23
    const-string v13, "/dev/felica"

    #@25
    invoke-direct {v11, v13}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_28
    .catchall {:try_start_21 .. :try_end_28} :catchall_5df
    .catch Ljava/io/FileNotFoundException; {:try_start_21 .. :try_end_28} :catch_5ed
    .catch Ljava/io/IOException; {:try_start_21 .. :try_end_28} :catch_5e6

    #@28
    .line 1459
    .end local v10           #fos_felica_uart:Ljava/io/FileOutputStream;
    .local v11, fos_felica_uart:Ljava/io/FileOutputStream;
    :try_start_28
    const-string v13, "FeliCaTest-IDM"

    #@2a
    const-string v14, "Set FeliCa ON."

    #@2c
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 1461
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_open()I

    #@32
    move-result v13

    #@33
    if-gez v13, :cond_6f

    #@35
    .line 1463
    const-string v13, "FeliCaTest-IDM"

    #@37
    const-string v14, "Set PON ON failed"

    #@39
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3c
    .catchall {:try_start_28 .. :try_end_3c} :catchall_147
    .catch Ljava/io/FileNotFoundException; {:try_start_28 .. :try_end_3c} :catch_c0
    .catch Ljava/io/IOException; {:try_start_28 .. :try_end_3c} :catch_114

    #@3c
    .line 1464
    const/4 v13, -0x1

    #@3d
    .line 1646
    if-eqz v9, :cond_42

    #@3f
    .line 1649
    :try_start_3f
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_42
    .catch Ljava/io/IOException; {:try_start_3f .. :try_end_42} :catch_65

    #@42
    .line 1655
    :cond_42
    :goto_42
    if-eqz v11, :cond_4e

    #@44
    .line 1657
    const-string v14, "FeliCaTest-IDM"

    #@46
    const-string v15, "Closing FeliCa-UART."

    #@48
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    .line 1661
    :try_start_4b
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_4e
    .catch Ljava/io/IOException; {:try_start_4b .. :try_end_4e} :catch_6a

    #@4e
    .line 1668
    :cond_4e
    :goto_4e
    const-string v14, "FeliCaTest-IDM"

    #@50
    const-string v15, "Set FeliCa OFF."

    #@52
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@55
    .line 1670
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@58
    move-result v14

    #@59
    if-gez v14, :cond_62

    #@5b
    .line 1672
    const-string v14, "FeliCaTest-IDM"

    #@5d
    const-string v15, "Set PON OFF failed"

    #@5f
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@62
    :cond_62
    move-object v10, v11

    #@63
    .end local v11           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v10       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v8, v9

    #@64
    .end local v9           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v8       #fis_felica_uart:Ljava/io/FileInputStream;
    :cond_64
    :goto_64
    return v13

    #@65
    .line 1650
    .end local v8           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v10           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v9       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v11       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_65
    move-exception v6

    #@66
    .line 1652
    .local v6, e:Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    #@69
    goto :goto_42

    #@6a
    .line 1662
    .end local v6           #e:Ljava/io/IOException;
    :catch_6a
    move-exception v6

    #@6b
    .line 1664
    .restart local v6       #e:Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    #@6e
    goto :goto_4e

    #@6f
    .line 1478
    .end local v6           #e:Ljava/io/IOException;
    :cond_6f
    const-wide/16 v13, 0x64

    #@71
    :try_start_71
    invoke-static {v13, v14}, Ljava/lang/Thread;->sleep(J)V
    :try_end_74
    .catchall {:try_start_71 .. :try_end_74} :catchall_147
    .catch Ljava/lang/InterruptedException; {:try_start_71 .. :try_end_74} :catch_bb
    .catch Ljava/io/FileNotFoundException; {:try_start_71 .. :try_end_74} :catch_c0
    .catch Ljava/io/IOException; {:try_start_71 .. :try_end_74} :catch_114

    #@74
    .line 1483
    :goto_74
    :try_start_74
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@77
    move-result-wide v4

    #@78
    .line 1484
    .local v4, current:J
    const/16 v2, 0x64

    #@7a
    .line 1486
    .local v2, DEFAULT_TIMEOUT:I
    const-string v13, "FeliCaTest-IDM"

    #@7c
    const-string v14, "Try reading FeilCa UART port ready."

    #@7e
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@81
    .line 1491
    :goto_81
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@84
    move-result-wide v13

    #@85
    sub-long/2addr v13, v4

    #@86
    int-to-long v15, v2

    #@87
    cmp-long v13, v13, v15

    #@89
    if-lez v13, :cond_ff

    #@8b
    .line 1493
    const-string v13, "FeliCaTest-IDM"

    #@8d
    const-string v14, "Timeout on waiting port-ready"

    #@8f
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_92
    .catchall {:try_start_74 .. :try_end_92} :catchall_147
    .catch Ljava/io/FileNotFoundException; {:try_start_74 .. :try_end_92} :catch_c0
    .catch Ljava/io/IOException; {:try_start_74 .. :try_end_92} :catch_114

    #@92
    .line 1494
    const/4 v13, -0x1

    #@93
    .line 1646
    if-eqz v9, :cond_98

    #@95
    .line 1649
    :try_start_95
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_98
    .catch Ljava/io/IOException; {:try_start_95 .. :try_end_98} :catch_f5

    #@98
    .line 1655
    :cond_98
    :goto_98
    if-eqz v11, :cond_a4

    #@9a
    .line 1657
    const-string v14, "FeliCaTest-IDM"

    #@9c
    const-string v15, "Closing FeliCa-UART."

    #@9e
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a1
    .line 1661
    :try_start_a1
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_a4
    .catch Ljava/io/IOException; {:try_start_a1 .. :try_end_a4} :catch_fa

    #@a4
    .line 1668
    :cond_a4
    :goto_a4
    const-string v14, "FeliCaTest-IDM"

    #@a6
    const-string v15, "Set FeliCa OFF."

    #@a8
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ab
    .line 1670
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@ae
    move-result v14

    #@af
    if-gez v14, :cond_b8

    #@b1
    .line 1672
    const-string v14, "FeliCaTest-IDM"

    #@b3
    const-string v15, "Set PON OFF failed"

    #@b5
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b8
    :cond_b8
    move-object v10, v11

    #@b9
    .end local v11           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v10       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v8, v9

    #@ba
    .end local v9           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v8       #fis_felica_uart:Ljava/io/FileInputStream;
    goto :goto_64

    #@bb
    .line 1479
    .end local v2           #DEFAULT_TIMEOUT:I
    .end local v4           #current:J
    .end local v8           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v10           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v9       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v11       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_bb
    move-exception v6

    #@bc
    .line 1480
    .local v6, e:Ljava/lang/InterruptedException;
    :try_start_bc
    invoke-virtual {v6}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_bf
    .catchall {:try_start_bc .. :try_end_bf} :catchall_147
    .catch Ljava/io/FileNotFoundException; {:try_start_bc .. :try_end_bf} :catch_c0
    .catch Ljava/io/IOException; {:try_start_bc .. :try_end_bf} :catch_114

    #@bf
    goto :goto_74

    #@c0
    .line 1629
    .end local v6           #e:Ljava/lang/InterruptedException;
    :catch_c0
    move-exception v6

    #@c1
    move-object v10, v11

    #@c2
    .end local v11           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v10       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v8, v9

    #@c3
    .line 1631
    .end local v9           #fis_felica_uart:Ljava/io/FileInputStream;
    .local v6, e:Ljava/io/FileNotFoundException;
    .restart local v8       #fis_felica_uart:Ljava/io/FileInputStream;
    :goto_c3
    :try_start_c3
    const-string v13, "FeliCaTest-IDM"

    #@c5
    const-string v14, "Can\'t open FeliCa-UART !. Can\'t read IDM"

    #@c7
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ca
    .line 1633
    invoke-virtual {v6}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_cd
    .catchall {:try_start_c3 .. :try_end_cd} :catchall_5dc

    #@cd
    .line 1635
    const/4 v13, -0x1

    #@ce
    .line 1646
    if-eqz v8, :cond_d3

    #@d0
    .line 1649
    :try_start_d0
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_d3
    .catch Ljava/io/IOException; {:try_start_d0 .. :try_end_d3} :catch_5b5

    #@d3
    .line 1655
    .end local v6           #e:Ljava/io/FileNotFoundException;
    :cond_d3
    :goto_d3
    if-eqz v10, :cond_df

    #@d5
    .line 1657
    const-string v14, "FeliCaTest-IDM"

    #@d7
    const-string v15, "Closing FeliCa-UART."

    #@d9
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@dc
    .line 1661
    :try_start_dc
    invoke-virtual {v10}, Ljava/io/FileOutputStream;->close()V
    :try_end_df
    .catch Ljava/io/IOException; {:try_start_dc .. :try_end_df} :catch_5bb

    #@df
    .line 1668
    :cond_df
    :goto_df
    const-string v14, "FeliCaTest-IDM"

    #@e1
    const-string v15, "Set FeliCa OFF."

    #@e3
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e6
    .line 1670
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@e9
    move-result v14

    #@ea
    if-gez v14, :cond_64

    #@ec
    .line 1672
    const-string v14, "FeliCaTest-IDM"

    #@ee
    const-string v15, "Set PON OFF failed"

    #@f0
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f3
    goto/16 :goto_64

    #@f5
    .line 1650
    .end local v8           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v10           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v2       #DEFAULT_TIMEOUT:I
    .restart local v4       #current:J
    .restart local v9       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v11       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_f5
    move-exception v6

    #@f6
    .line 1652
    .local v6, e:Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    #@f9
    goto :goto_98

    #@fa
    .line 1662
    .end local v6           #e:Ljava/io/IOException;
    :catch_fa
    move-exception v6

    #@fb
    .line 1664
    .restart local v6       #e:Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    #@fe
    goto :goto_a4

    #@ff
    .line 1497
    .end local v6           #e:Ljava/io/IOException;
    :cond_ff
    :try_start_ff
    invoke-virtual {v9}, Ljava/io/FileInputStream;->available()I
    :try_end_102
    .catchall {:try_start_ff .. :try_end_102} :catchall_147
    .catch Ljava/io/FileNotFoundException; {:try_start_ff .. :try_end_102} :catch_c0
    .catch Ljava/io/IOException; {:try_start_ff .. :try_end_102} :catch_114

    #@102
    move-result v13

    #@103
    const/4 v14, 0x4

    #@104
    if-ge v13, v14, :cond_170

    #@106
    .line 1503
    const-wide/16 v13, 0x1e

    #@108
    :try_start_108
    invoke-static {v13, v14}, Ljava/lang/Thread;->sleep(J)V
    :try_end_10b
    .catchall {:try_start_108 .. :try_end_10b} :catchall_147
    .catch Ljava/lang/InterruptedException; {:try_start_108 .. :try_end_10b} :catch_142
    .catch Ljava/io/FileNotFoundException; {:try_start_108 .. :try_end_10b} :catch_c0
    .catch Ljava/io/IOException; {:try_start_108 .. :try_end_10b} :catch_114

    #@10b
    .line 1508
    :goto_10b
    :try_start_10b
    const-string v13, "FeliCaTest-IDM"

    #@10d
    const-string v14, "FeliCa-UART read-avail size isn\'t enough. "

    #@10f
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_112
    .catchall {:try_start_10b .. :try_end_112} :catchall_147
    .catch Ljava/io/FileNotFoundException; {:try_start_10b .. :try_end_112} :catch_c0
    .catch Ljava/io/IOException; {:try_start_10b .. :try_end_112} :catch_114

    #@112
    goto/16 :goto_81

    #@114
    .line 1637
    .end local v2           #DEFAULT_TIMEOUT:I
    .end local v4           #current:J
    :catch_114
    move-exception v6

    #@115
    move-object v10, v11

    #@116
    .end local v11           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v10       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v8, v9

    #@117
    .line 1640
    .end local v9           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v6       #e:Ljava/io/IOException;
    .restart local v8       #fis_felica_uart:Ljava/io/FileInputStream;
    :goto_117
    :try_start_117
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V
    :try_end_11a
    .catchall {:try_start_117 .. :try_end_11a} :catchall_5dc

    #@11a
    .line 1642
    const/4 v13, -0x1

    #@11b
    .line 1646
    if-eqz v8, :cond_120

    #@11d
    .line 1649
    :try_start_11d
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_120
    .catch Ljava/io/IOException; {:try_start_11d .. :try_end_120} :catch_5c1

    #@120
    .line 1655
    :cond_120
    :goto_120
    if-eqz v10, :cond_12c

    #@122
    .line 1657
    const-string v14, "FeliCaTest-IDM"

    #@124
    const-string v15, "Closing FeliCa-UART."

    #@126
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@129
    .line 1661
    :try_start_129
    invoke-virtual {v10}, Ljava/io/FileOutputStream;->close()V
    :try_end_12c
    .catch Ljava/io/IOException; {:try_start_129 .. :try_end_12c} :catch_5c7

    #@12c
    .line 1668
    :cond_12c
    :goto_12c
    const-string v14, "FeliCaTest-IDM"

    #@12e
    const-string v15, "Set FeliCa OFF."

    #@130
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@133
    .line 1670
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@136
    move-result v14

    #@137
    if-gez v14, :cond_64

    #@139
    .line 1672
    const-string v14, "FeliCaTest-IDM"

    #@13b
    const-string v15, "Set PON OFF failed"

    #@13d
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@140
    goto/16 :goto_64

    #@142
    .line 1504
    .end local v6           #e:Ljava/io/IOException;
    .end local v8           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v10           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v2       #DEFAULT_TIMEOUT:I
    .restart local v4       #current:J
    .restart local v9       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v11       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_142
    move-exception v6

    #@143
    .line 1505
    .local v6, e:Ljava/lang/InterruptedException;
    :try_start_143
    invoke-virtual {v6}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_146
    .catchall {:try_start_143 .. :try_end_146} :catchall_147
    .catch Ljava/io/FileNotFoundException; {:try_start_143 .. :try_end_146} :catch_c0
    .catch Ljava/io/IOException; {:try_start_143 .. :try_end_146} :catch_114

    #@146
    goto :goto_10b

    #@147
    .line 1646
    .end local v2           #DEFAULT_TIMEOUT:I
    .end local v4           #current:J
    .end local v6           #e:Ljava/lang/InterruptedException;
    :catchall_147
    move-exception v13

    #@148
    move-object v10, v11

    #@149
    .end local v11           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v10       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v8, v9

    #@14a
    .end local v9           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v8       #fis_felica_uart:Ljava/io/FileInputStream;
    :goto_14a
    if-eqz v8, :cond_14f

    #@14c
    .line 1649
    :try_start_14c
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_14f
    .catch Ljava/io/IOException; {:try_start_14c .. :try_end_14f} :catch_5cd

    #@14f
    .line 1655
    :cond_14f
    :goto_14f
    if-eqz v10, :cond_15b

    #@151
    .line 1657
    const-string v14, "FeliCaTest-IDM"

    #@153
    const-string v15, "Closing FeliCa-UART."

    #@155
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@158
    .line 1661
    :try_start_158
    invoke-virtual {v10}, Ljava/io/FileOutputStream;->close()V
    :try_end_15b
    .catch Ljava/io/IOException; {:try_start_158 .. :try_end_15b} :catch_5d3

    #@15b
    .line 1668
    :cond_15b
    :goto_15b
    const-string v14, "FeliCaTest-IDM"

    #@15d
    const-string v15, "Set FeliCa OFF."

    #@15f
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@162
    .line 1670
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@165
    move-result v14

    #@166
    if-gez v14, :cond_16f

    #@168
    .line 1672
    const-string v14, "FeliCaTest-IDM"

    #@16a
    const-string v15, "Set PON OFF failed"

    #@16c
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16f
    :cond_16f
    throw v13

    #@170
    .line 1512
    .end local v8           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v10           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v2       #DEFAULT_TIMEOUT:I
    .restart local v4       #current:J
    .restart local v9       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v11       #fos_felica_uart:Ljava/io/FileOutputStream;
    :cond_170
    :try_start_170
    const-string v13, "FeliCaTest-IDM"

    #@172
    const-string v14, "Now Reading FeilCa UART port ready"

    #@174
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@177
    .line 1514
    const/4 v13, 0x0

    #@178
    sget v14, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_PORT_READY_LEN:I

    #@17a
    invoke-virtual {v9, v7, v13, v14}, Ljava/io/FileInputStream;->read([BII)I

    #@17d
    move-result v12

    #@17e
    .line 1516
    const-string v13, "FeliCaTest-IDM"

    #@180
    const-string v14, "Port ready : 0x%x,0x%x,0x%x,0x%x"

    #@182
    const/4 v15, 0x4

    #@183
    new-array v15, v15, [Ljava/lang/Object;

    #@185
    const/16 v16, 0x0

    #@187
    const/16 v17, 0x0

    #@189
    aget-byte v17, v7, v17

    #@18b
    invoke-static/range {v17 .. v17}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@18e
    move-result-object v17

    #@18f
    aput-object v17, v15, v16

    #@191
    const/16 v16, 0x1

    #@193
    const/16 v17, 0x1

    #@195
    aget-byte v17, v7, v17

    #@197
    invoke-static/range {v17 .. v17}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@19a
    move-result-object v17

    #@19b
    aput-object v17, v15, v16

    #@19d
    const/16 v16, 0x2

    #@19f
    const/16 v17, 0x2

    #@1a1
    aget-byte v17, v7, v17

    #@1a3
    invoke-static/range {v17 .. v17}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@1a6
    move-result-object v17

    #@1a7
    aput-object v17, v15, v16

    #@1a9
    const/16 v16, 0x3

    #@1ab
    const/16 v17, 0x3

    #@1ad
    aget-byte v17, v7, v17

    #@1af
    invoke-static/range {v17 .. v17}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@1b2
    move-result-object v17

    #@1b3
    aput-object v17, v15, v16

    #@1b5
    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@1b8
    move-result-object v14

    #@1b9
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1bc
    .line 1519
    sget v13, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_PORT_READY_LEN:I

    #@1be
    if-eq v12, v13, :cond_20c

    #@1c0
    .line 1521
    const-string v13, "FeliCaTest-IDM"

    #@1c2
    new-instance v14, Ljava/lang/StringBuilder;

    #@1c4
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@1c7
    const-string v15, "Read Length Mismatch : "

    #@1c9
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cc
    move-result-object v14

    #@1cd
    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d0
    move-result-object v14

    #@1d1
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d4
    move-result-object v14

    #@1d5
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1d8
    .catchall {:try_start_170 .. :try_end_1d8} :catchall_147
    .catch Ljava/io/FileNotFoundException; {:try_start_170 .. :try_end_1d8} :catch_c0
    .catch Ljava/io/IOException; {:try_start_170 .. :try_end_1d8} :catch_114

    #@1d8
    .line 1522
    const/4 v13, -0x1

    #@1d9
    .line 1646
    if-eqz v9, :cond_1de

    #@1db
    .line 1649
    :try_start_1db
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_1de
    .catch Ljava/io/IOException; {:try_start_1db .. :try_end_1de} :catch_202

    #@1de
    .line 1655
    :cond_1de
    :goto_1de
    if-eqz v11, :cond_1ea

    #@1e0
    .line 1657
    const-string v14, "FeliCaTest-IDM"

    #@1e2
    const-string v15, "Closing FeliCa-UART."

    #@1e4
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e7
    .line 1661
    :try_start_1e7
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_1ea
    .catch Ljava/io/IOException; {:try_start_1e7 .. :try_end_1ea} :catch_207

    #@1ea
    .line 1668
    :cond_1ea
    :goto_1ea
    const-string v14, "FeliCaTest-IDM"

    #@1ec
    const-string v15, "Set FeliCa OFF."

    #@1ee
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f1
    .line 1670
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@1f4
    move-result v14

    #@1f5
    if-gez v14, :cond_1fe

    #@1f7
    .line 1672
    const-string v14, "FeliCaTest-IDM"

    #@1f9
    const-string v15, "Set PON OFF failed"

    #@1fb
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1fe
    :cond_1fe
    move-object v10, v11

    #@1ff
    .end local v11           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v10       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v8, v9

    #@200
    .end local v9           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v8       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_64

    #@202
    .line 1650
    .end local v8           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v10           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v9       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v11       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_202
    move-exception v6

    #@203
    .line 1652
    .local v6, e:Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    #@206
    goto :goto_1de

    #@207
    .line 1662
    .end local v6           #e:Ljava/io/IOException;
    :catch_207
    move-exception v6

    #@208
    .line 1664
    .restart local v6       #e:Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    #@20b
    goto :goto_1ea

    #@20c
    .line 1526
    .end local v6           #e:Ljava/io/IOException;
    :cond_20c
    :try_start_20c
    sget v13, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_PORT_READY_LEN:I

    #@20e
    invoke-static {v7, v13}, Ljava/util/Arrays;->copyOf([BI)[B

    #@211
    move-result-object v13

    #@212
    sget-object v14, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_PORT_READY:[B

    #@214
    invoke-static {v13, v14}, Ljava/util/Arrays;->equals([B[B)Z

    #@217
    move-result v13

    #@218
    if-nez v13, :cond_255

    #@21a
    .line 1528
    const-string v13, "FeliCaTest-IDM"

    #@21c
    const-string v14, "Port-Ready Mismatch!"

    #@21e
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_221
    .catchall {:try_start_20c .. :try_end_221} :catchall_147
    .catch Ljava/io/FileNotFoundException; {:try_start_20c .. :try_end_221} :catch_c0
    .catch Ljava/io/IOException; {:try_start_20c .. :try_end_221} :catch_114

    #@221
    .line 1529
    const/4 v13, -0x1

    #@222
    .line 1646
    if-eqz v9, :cond_227

    #@224
    .line 1649
    :try_start_224
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_227
    .catch Ljava/io/IOException; {:try_start_224 .. :try_end_227} :catch_24b

    #@227
    .line 1655
    :cond_227
    :goto_227
    if-eqz v11, :cond_233

    #@229
    .line 1657
    const-string v14, "FeliCaTest-IDM"

    #@22b
    const-string v15, "Closing FeliCa-UART."

    #@22d
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@230
    .line 1661
    :try_start_230
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_233
    .catch Ljava/io/IOException; {:try_start_230 .. :try_end_233} :catch_250

    #@233
    .line 1668
    :cond_233
    :goto_233
    const-string v14, "FeliCaTest-IDM"

    #@235
    const-string v15, "Set FeliCa OFF."

    #@237
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@23a
    .line 1670
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@23d
    move-result v14

    #@23e
    if-gez v14, :cond_247

    #@240
    .line 1672
    const-string v14, "FeliCaTest-IDM"

    #@242
    const-string v15, "Set PON OFF failed"

    #@244
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@247
    :cond_247
    move-object v10, v11

    #@248
    .end local v11           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v10       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v8, v9

    #@249
    .end local v9           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v8       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_64

    #@24b
    .line 1650
    .end local v8           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v10           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v9       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v11       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_24b
    move-exception v6

    #@24c
    .line 1652
    .restart local v6       #e:Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    #@24f
    goto :goto_227

    #@250
    .line 1662
    .end local v6           #e:Ljava/io/IOException;
    :catch_250
    move-exception v6

    #@251
    .line 1664
    .restart local v6       #e:Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    #@254
    goto :goto_233

    #@255
    .line 1541
    .end local v6           #e:Ljava/io/IOException;
    :cond_255
    :try_start_255
    const-string v13, "FeliCaTest-IDM"

    #@257
    const-string v14, "Writing polling cmd"

    #@259
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@25c
    .line 1542
    sget-object v13, Lcom/lge/systemservice/service/FeliCaDevice;->CMD_POLLING_EXT:[B

    #@25e
    invoke-virtual {v11, v13}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_261
    .catchall {:try_start_255 .. :try_end_261} :catchall_147
    .catch Ljava/io/FileNotFoundException; {:try_start_255 .. :try_end_261} :catch_c0
    .catch Ljava/io/IOException; {:try_start_255 .. :try_end_261} :catch_114

    #@261
    .line 1545
    const-wide/16 v13, 0x64

    #@263
    :try_start_263
    invoke-static {v13, v14}, Ljava/lang/Thread;->sleep(J)V
    :try_end_266
    .catchall {:try_start_263 .. :try_end_266} :catchall_147
    .catch Ljava/lang/InterruptedException; {:try_start_263 .. :try_end_266} :catch_5d9
    .catch Ljava/io/FileNotFoundException; {:try_start_263 .. :try_end_266} :catch_c0
    .catch Ljava/io/IOException; {:try_start_263 .. :try_end_266} :catch_114

    #@266
    .line 1549
    :goto_266
    const/4 v13, 0x0

    #@267
    :try_start_267
    invoke-static {v7, v13}, Ljava/util/Arrays;->fill([BB)V

    #@26a
    .line 1553
    const-string v13, "FeliCaTest-IDM"

    #@26c
    const-string v14, "Reading Ack packet."

    #@26e
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@271
    .line 1555
    const/4 v13, 0x0

    #@272
    sget v14, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_ACK_LEN:I

    #@274
    invoke-virtual {v9, v7, v13, v14}, Ljava/io/FileInputStream;->read([BII)I

    #@277
    move-result v12

    #@278
    .line 1557
    const-string v13, "FeliCaTest-IDM"

    #@27a
    const-string v14, "Ack : 0x%x,0x%x,0x%x,0x%x"

    #@27c
    const/4 v15, 0x4

    #@27d
    new-array v15, v15, [Ljava/lang/Object;

    #@27f
    const/16 v16, 0x0

    #@281
    const/16 v17, 0x0

    #@283
    aget-byte v17, v7, v17

    #@285
    invoke-static/range {v17 .. v17}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@288
    move-result-object v17

    #@289
    aput-object v17, v15, v16

    #@28b
    const/16 v16, 0x1

    #@28d
    const/16 v17, 0x1

    #@28f
    aget-byte v17, v7, v17

    #@291
    invoke-static/range {v17 .. v17}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@294
    move-result-object v17

    #@295
    aput-object v17, v15, v16

    #@297
    const/16 v16, 0x2

    #@299
    const/16 v17, 0x2

    #@29b
    aget-byte v17, v7, v17

    #@29d
    invoke-static/range {v17 .. v17}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@2a0
    move-result-object v17

    #@2a1
    aput-object v17, v15, v16

    #@2a3
    const/16 v16, 0x3

    #@2a5
    const/16 v17, 0x3

    #@2a7
    aget-byte v17, v7, v17

    #@2a9
    invoke-static/range {v17 .. v17}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@2ac
    move-result-object v17

    #@2ad
    aput-object v17, v15, v16

    #@2af
    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@2b2
    move-result-object v14

    #@2b3
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2b6
    .line 1560
    sget v13, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_ACK_LEN:I

    #@2b8
    if-eq v12, v13, :cond_306

    #@2ba
    .line 1562
    const-string v13, "FeliCaTest-IDM"

    #@2bc
    new-instance v14, Ljava/lang/StringBuilder;

    #@2be
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@2c1
    const-string v15, "Read Length Mismatch : "

    #@2c3
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c6
    move-result-object v14

    #@2c7
    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2ca
    move-result-object v14

    #@2cb
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2ce
    move-result-object v14

    #@2cf
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2d2
    .catchall {:try_start_267 .. :try_end_2d2} :catchall_147
    .catch Ljava/io/FileNotFoundException; {:try_start_267 .. :try_end_2d2} :catch_c0
    .catch Ljava/io/IOException; {:try_start_267 .. :try_end_2d2} :catch_114

    #@2d2
    .line 1563
    const/4 v13, -0x1

    #@2d3
    .line 1646
    if-eqz v9, :cond_2d8

    #@2d5
    .line 1649
    :try_start_2d5
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_2d8
    .catch Ljava/io/IOException; {:try_start_2d5 .. :try_end_2d8} :catch_2fc

    #@2d8
    .line 1655
    :cond_2d8
    :goto_2d8
    if-eqz v11, :cond_2e4

    #@2da
    .line 1657
    const-string v14, "FeliCaTest-IDM"

    #@2dc
    const-string v15, "Closing FeliCa-UART."

    #@2de
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2e1
    .line 1661
    :try_start_2e1
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_2e4
    .catch Ljava/io/IOException; {:try_start_2e1 .. :try_end_2e4} :catch_301

    #@2e4
    .line 1668
    :cond_2e4
    :goto_2e4
    const-string v14, "FeliCaTest-IDM"

    #@2e6
    const-string v15, "Set FeliCa OFF."

    #@2e8
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2eb
    .line 1670
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@2ee
    move-result v14

    #@2ef
    if-gez v14, :cond_2f8

    #@2f1
    .line 1672
    const-string v14, "FeliCaTest-IDM"

    #@2f3
    const-string v15, "Set PON OFF failed"

    #@2f5
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2f8
    :cond_2f8
    move-object v10, v11

    #@2f9
    .end local v11           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v10       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v8, v9

    #@2fa
    .end local v9           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v8       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_64

    #@2fc
    .line 1650
    .end local v8           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v10           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v9       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v11       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_2fc
    move-exception v6

    #@2fd
    .line 1652
    .restart local v6       #e:Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    #@300
    goto :goto_2d8

    #@301
    .line 1662
    .end local v6           #e:Ljava/io/IOException;
    :catch_301
    move-exception v6

    #@302
    .line 1664
    .restart local v6       #e:Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    #@305
    goto :goto_2e4

    #@306
    .line 1567
    .end local v6           #e:Ljava/io/IOException;
    :cond_306
    :try_start_306
    sget v13, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_ACK_LEN:I

    #@308
    invoke-static {v7, v13}, Ljava/util/Arrays;->copyOf([BI)[B

    #@30b
    move-result-object v13

    #@30c
    sget-object v14, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_ACK:[B

    #@30e
    invoke-static {v13, v14}, Ljava/util/Arrays;->equals([B[B)Z

    #@311
    move-result v13

    #@312
    if-nez v13, :cond_34f

    #@314
    .line 1569
    const-string v13, "FeliCaTest-IDM"

    #@316
    const-string v14, "Ack Mismatch!"

    #@318
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_31b
    .catchall {:try_start_306 .. :try_end_31b} :catchall_147
    .catch Ljava/io/FileNotFoundException; {:try_start_306 .. :try_end_31b} :catch_c0
    .catch Ljava/io/IOException; {:try_start_306 .. :try_end_31b} :catch_114

    #@31b
    .line 1570
    const/4 v13, -0x1

    #@31c
    .line 1646
    if-eqz v9, :cond_321

    #@31e
    .line 1649
    :try_start_31e
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_321
    .catch Ljava/io/IOException; {:try_start_31e .. :try_end_321} :catch_345

    #@321
    .line 1655
    :cond_321
    :goto_321
    if-eqz v11, :cond_32d

    #@323
    .line 1657
    const-string v14, "FeliCaTest-IDM"

    #@325
    const-string v15, "Closing FeliCa-UART."

    #@327
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@32a
    .line 1661
    :try_start_32a
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_32d
    .catch Ljava/io/IOException; {:try_start_32a .. :try_end_32d} :catch_34a

    #@32d
    .line 1668
    :cond_32d
    :goto_32d
    const-string v14, "FeliCaTest-IDM"

    #@32f
    const-string v15, "Set FeliCa OFF."

    #@331
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@334
    .line 1670
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@337
    move-result v14

    #@338
    if-gez v14, :cond_341

    #@33a
    .line 1672
    const-string v14, "FeliCaTest-IDM"

    #@33c
    const-string v15, "Set PON OFF failed"

    #@33e
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@341
    :cond_341
    move-object v10, v11

    #@342
    .end local v11           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v10       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v8, v9

    #@343
    .end local v9           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v8       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_64

    #@345
    .line 1650
    .end local v8           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v10           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v9       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v11       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_345
    move-exception v6

    #@346
    .line 1652
    .restart local v6       #e:Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    #@349
    goto :goto_321

    #@34a
    .line 1662
    .end local v6           #e:Ljava/io/IOException;
    :catch_34a
    move-exception v6

    #@34b
    .line 1664
    .restart local v6       #e:Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    #@34e
    goto :goto_32d

    #@34f
    .line 1574
    .end local v6           #e:Ljava/io/IOException;
    :cond_34f
    const/4 v13, 0x0

    #@350
    :try_start_350
    invoke-static {v7, v13}, Ljava/util/Arrays;->fill([BB)V

    #@353
    .line 1576
    const-string v13, "FeliCaTest-IDM"

    #@355
    const-string v14, "Reading polling response."

    #@357
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@35a
    .line 1578
    const/4 v13, 0x0

    #@35b
    const/16 v14, 0x1c

    #@35d
    invoke-virtual {v9, v7, v13, v14}, Ljava/io/FileInputStream;->read([BII)I

    #@360
    move-result v12

    #@361
    .line 1586
    const-string v13, "FeliCaTest-IDM"

    #@363
    const-string v14, "response1 : 0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x"

    #@365
    const/16 v15, 0x8

    #@367
    new-array v15, v15, [Ljava/lang/Object;

    #@369
    const/16 v16, 0x0

    #@36b
    const/16 v17, 0x0

    #@36d
    aget-byte v17, v7, v17

    #@36f
    invoke-static/range {v17 .. v17}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@372
    move-result-object v17

    #@373
    aput-object v17, v15, v16

    #@375
    const/16 v16, 0x1

    #@377
    const/16 v17, 0x1

    #@379
    aget-byte v17, v7, v17

    #@37b
    invoke-static/range {v17 .. v17}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@37e
    move-result-object v17

    #@37f
    aput-object v17, v15, v16

    #@381
    const/16 v16, 0x2

    #@383
    const/16 v17, 0x2

    #@385
    aget-byte v17, v7, v17

    #@387
    invoke-static/range {v17 .. v17}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@38a
    move-result-object v17

    #@38b
    aput-object v17, v15, v16

    #@38d
    const/16 v16, 0x3

    #@38f
    const/16 v17, 0x3

    #@391
    aget-byte v17, v7, v17

    #@393
    invoke-static/range {v17 .. v17}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@396
    move-result-object v17

    #@397
    aput-object v17, v15, v16

    #@399
    const/16 v16, 0x4

    #@39b
    const/16 v17, 0x4

    #@39d
    aget-byte v17, v7, v17

    #@39f
    invoke-static/range {v17 .. v17}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@3a2
    move-result-object v17

    #@3a3
    aput-object v17, v15, v16

    #@3a5
    const/16 v16, 0x5

    #@3a7
    const/16 v17, 0x5

    #@3a9
    aget-byte v17, v7, v17

    #@3ab
    invoke-static/range {v17 .. v17}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@3ae
    move-result-object v17

    #@3af
    aput-object v17, v15, v16

    #@3b1
    const/16 v16, 0x6

    #@3b3
    const/16 v17, 0x6

    #@3b5
    aget-byte v17, v7, v17

    #@3b7
    invoke-static/range {v17 .. v17}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@3ba
    move-result-object v17

    #@3bb
    aput-object v17, v15, v16

    #@3bd
    const/16 v16, 0x7

    #@3bf
    const/16 v17, 0x7

    #@3c1
    aget-byte v17, v7, v17

    #@3c3
    invoke-static/range {v17 .. v17}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@3c6
    move-result-object v17

    #@3c7
    aput-object v17, v15, v16

    #@3c9
    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@3cc
    move-result-object v14

    #@3cd
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3d0
    .line 1588
    const-string v13, "FeliCaTest-IDM"

    #@3d2
    const-string v14, "response2 : 0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x"

    #@3d4
    const/16 v15, 0x8

    #@3d6
    new-array v15, v15, [Ljava/lang/Object;

    #@3d8
    const/16 v16, 0x0

    #@3da
    const/16 v17, 0x8

    #@3dc
    aget-byte v17, v7, v17

    #@3de
    invoke-static/range {v17 .. v17}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@3e1
    move-result-object v17

    #@3e2
    aput-object v17, v15, v16

    #@3e4
    const/16 v16, 0x1

    #@3e6
    const/16 v17, 0x9

    #@3e8
    aget-byte v17, v7, v17

    #@3ea
    invoke-static/range {v17 .. v17}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@3ed
    move-result-object v17

    #@3ee
    aput-object v17, v15, v16

    #@3f0
    const/16 v16, 0x2

    #@3f2
    const/16 v17, 0xa

    #@3f4
    aget-byte v17, v7, v17

    #@3f6
    invoke-static/range {v17 .. v17}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@3f9
    move-result-object v17

    #@3fa
    aput-object v17, v15, v16

    #@3fc
    const/16 v16, 0x3

    #@3fe
    const/16 v17, 0xb

    #@400
    aget-byte v17, v7, v17

    #@402
    invoke-static/range {v17 .. v17}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@405
    move-result-object v17

    #@406
    aput-object v17, v15, v16

    #@408
    const/16 v16, 0x4

    #@40a
    const/16 v17, 0xc

    #@40c
    aget-byte v17, v7, v17

    #@40e
    invoke-static/range {v17 .. v17}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@411
    move-result-object v17

    #@412
    aput-object v17, v15, v16

    #@414
    const/16 v16, 0x5

    #@416
    const/16 v17, 0xd

    #@418
    aget-byte v17, v7, v17

    #@41a
    invoke-static/range {v17 .. v17}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@41d
    move-result-object v17

    #@41e
    aput-object v17, v15, v16

    #@420
    const/16 v16, 0x6

    #@422
    const/16 v17, 0xe

    #@424
    aget-byte v17, v7, v17

    #@426
    invoke-static/range {v17 .. v17}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@429
    move-result-object v17

    #@42a
    aput-object v17, v15, v16

    #@42c
    const/16 v16, 0x7

    #@42e
    const/16 v17, 0xf

    #@430
    aget-byte v17, v7, v17

    #@432
    invoke-static/range {v17 .. v17}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@435
    move-result-object v17

    #@436
    aput-object v17, v15, v16

    #@438
    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@43b
    move-result-object v14

    #@43c
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@43f
    .line 1592
    const-string v13, "FeliCaTest-IDM"

    #@441
    const-string v14, "Validating polling response packet."

    #@443
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@446
    .line 1595
    const/16 v13, 0x1c

    #@448
    if-eq v12, v13, :cond_496

    #@44a
    .line 1597
    const-string v13, "FeliCaTest-IDM"

    #@44c
    new-instance v14, Ljava/lang/StringBuilder;

    #@44e
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@451
    const-string v15, "Length Mismatch : "

    #@453
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@456
    move-result-object v14

    #@457
    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@45a
    move-result-object v14

    #@45b
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45e
    move-result-object v14

    #@45f
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_462
    .catchall {:try_start_350 .. :try_end_462} :catchall_147
    .catch Ljava/io/FileNotFoundException; {:try_start_350 .. :try_end_462} :catch_c0
    .catch Ljava/io/IOException; {:try_start_350 .. :try_end_462} :catch_114

    #@462
    .line 1598
    const/4 v13, -0x1

    #@463
    .line 1646
    if-eqz v9, :cond_468

    #@465
    .line 1649
    :try_start_465
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_468
    .catch Ljava/io/IOException; {:try_start_465 .. :try_end_468} :catch_48c

    #@468
    .line 1655
    :cond_468
    :goto_468
    if-eqz v11, :cond_474

    #@46a
    .line 1657
    const-string v14, "FeliCaTest-IDM"

    #@46c
    const-string v15, "Closing FeliCa-UART."

    #@46e
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@471
    .line 1661
    :try_start_471
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_474
    .catch Ljava/io/IOException; {:try_start_471 .. :try_end_474} :catch_491

    #@474
    .line 1668
    :cond_474
    :goto_474
    const-string v14, "FeliCaTest-IDM"

    #@476
    const-string v15, "Set FeliCa OFF."

    #@478
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@47b
    .line 1670
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@47e
    move-result v14

    #@47f
    if-gez v14, :cond_488

    #@481
    .line 1672
    const-string v14, "FeliCaTest-IDM"

    #@483
    const-string v15, "Set PON OFF failed"

    #@485
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@488
    :cond_488
    move-object v10, v11

    #@489
    .end local v11           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v10       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v8, v9

    #@48a
    .end local v9           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v8       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_64

    #@48c
    .line 1650
    .end local v8           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v10           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v9       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v11       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_48c
    move-exception v6

    #@48d
    .line 1652
    .restart local v6       #e:Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    #@490
    goto :goto_468

    #@491
    .line 1662
    .end local v6           #e:Ljava/io/IOException;
    :catch_491
    move-exception v6

    #@492
    .line 1664
    .restart local v6       #e:Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    #@495
    goto :goto_474

    #@496
    .line 1602
    .end local v6           #e:Ljava/io/IOException;
    :cond_496
    const/4 v13, 0x0

    #@497
    :try_start_497
    aget-byte v13, v7, v13

    #@499
    if-nez v13, :cond_4a8

    #@49b
    const/4 v13, 0x1

    #@49c
    aget-byte v13, v7, v13

    #@49e
    const/4 v14, -0x6

    #@49f
    if-ne v13, v14, :cond_4a8

    #@4a1
    const/4 v13, 0x2

    #@4a2
    aget-byte v13, v7, v13

    #@4a4
    const/16 v14, 0x17

    #@4a6
    if-eq v13, v14, :cond_4e3

    #@4a8
    .line 1604
    :cond_4a8
    const-string v13, "FeliCaTest-IDM"

    #@4aa
    const-string v14, "Header Mismatch!"

    #@4ac
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4af
    .catchall {:try_start_497 .. :try_end_4af} :catchall_147
    .catch Ljava/io/FileNotFoundException; {:try_start_497 .. :try_end_4af} :catch_c0
    .catch Ljava/io/IOException; {:try_start_497 .. :try_end_4af} :catch_114

    #@4af
    .line 1605
    const/4 v13, -0x1

    #@4b0
    .line 1646
    if-eqz v9, :cond_4b5

    #@4b2
    .line 1649
    :try_start_4b2
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_4b5
    .catch Ljava/io/IOException; {:try_start_4b2 .. :try_end_4b5} :catch_4d9

    #@4b5
    .line 1655
    :cond_4b5
    :goto_4b5
    if-eqz v11, :cond_4c1

    #@4b7
    .line 1657
    const-string v14, "FeliCaTest-IDM"

    #@4b9
    const-string v15, "Closing FeliCa-UART."

    #@4bb
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4be
    .line 1661
    :try_start_4be
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_4c1
    .catch Ljava/io/IOException; {:try_start_4be .. :try_end_4c1} :catch_4de

    #@4c1
    .line 1668
    :cond_4c1
    :goto_4c1
    const-string v14, "FeliCaTest-IDM"

    #@4c3
    const-string v15, "Set FeliCa OFF."

    #@4c5
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4c8
    .line 1670
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@4cb
    move-result v14

    #@4cc
    if-gez v14, :cond_4d5

    #@4ce
    .line 1672
    const-string v14, "FeliCaTest-IDM"

    #@4d0
    const-string v15, "Set PON OFF failed"

    #@4d2
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4d5
    :cond_4d5
    move-object v10, v11

    #@4d6
    .end local v11           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v10       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v8, v9

    #@4d7
    .end local v9           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v8       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_64

    #@4d9
    .line 1650
    .end local v8           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v10           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v9       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v11       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_4d9
    move-exception v6

    #@4da
    .line 1652
    .restart local v6       #e:Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    #@4dd
    goto :goto_4b5

    #@4de
    .line 1662
    .end local v6           #e:Ljava/io/IOException;
    :catch_4de
    move-exception v6

    #@4df
    .line 1664
    .restart local v6       #e:Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    #@4e2
    goto :goto_4c1

    #@4e3
    .line 1609
    .end local v6           #e:Ljava/io/IOException;
    :cond_4e3
    const/4 v13, 0x3

    #@4e4
    :try_start_4e4
    aget-byte v13, v7, v13

    #@4e6
    and-int/lit16 v13, v13, 0xff

    #@4e8
    const/16 v14, 0xcd

    #@4ea
    if-ne v13, v14, :cond_4fa

    #@4ec
    const/4 v13, 0x4

    #@4ed
    aget-byte v13, v7, v13

    #@4ef
    const/16 v14, 0x11

    #@4f1
    if-ne v13, v14, :cond_4fa

    #@4f3
    const/4 v13, 0x7

    #@4f4
    aget-byte v13, v7, v13

    #@4f6
    const/16 v14, 0x12

    #@4f8
    if-eq v13, v14, :cond_535

    #@4fa
    .line 1613
    :cond_4fa
    const-string v13, "FeliCaTest-IDM"

    #@4fc
    const-string v14, "Header CommThru Mismatch!"

    #@4fe
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_501
    .catchall {:try_start_4e4 .. :try_end_501} :catchall_147
    .catch Ljava/io/FileNotFoundException; {:try_start_4e4 .. :try_end_501} :catch_c0
    .catch Ljava/io/IOException; {:try_start_4e4 .. :try_end_501} :catch_114

    #@501
    .line 1614
    const/4 v13, -0x1

    #@502
    .line 1646
    if-eqz v9, :cond_507

    #@504
    .line 1649
    :try_start_504
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_507
    .catch Ljava/io/IOException; {:try_start_504 .. :try_end_507} :catch_52b

    #@507
    .line 1655
    :cond_507
    :goto_507
    if-eqz v11, :cond_513

    #@509
    .line 1657
    const-string v14, "FeliCaTest-IDM"

    #@50b
    const-string v15, "Closing FeliCa-UART."

    #@50d
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@510
    .line 1661
    :try_start_510
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_513
    .catch Ljava/io/IOException; {:try_start_510 .. :try_end_513} :catch_530

    #@513
    .line 1668
    :cond_513
    :goto_513
    const-string v14, "FeliCaTest-IDM"

    #@515
    const-string v15, "Set FeliCa OFF."

    #@517
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@51a
    .line 1670
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@51d
    move-result v14

    #@51e
    if-gez v14, :cond_527

    #@520
    .line 1672
    const-string v14, "FeliCaTest-IDM"

    #@522
    const-string v15, "Set PON OFF failed"

    #@524
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@527
    :cond_527
    move-object v10, v11

    #@528
    .end local v11           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v10       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v8, v9

    #@529
    .end local v9           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v8       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_64

    #@52b
    .line 1650
    .end local v8           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v10           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v9       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v11       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_52b
    move-exception v6

    #@52c
    .line 1652
    .restart local v6       #e:Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    #@52f
    goto :goto_507

    #@530
    .line 1662
    .end local v6           #e:Ljava/io/IOException;
    :catch_530
    move-exception v6

    #@531
    .line 1664
    .restart local v6       #e:Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    #@534
    goto :goto_513

    #@535
    .line 1618
    .end local v6           #e:Ljava/io/IOException;
    :cond_535
    const/16 v13, 0x8

    #@537
    :try_start_537
    aget-byte v13, v7, v13

    #@539
    const/4 v14, 0x1

    #@53a
    if-eq v13, v14, :cond_577

    #@53c
    .line 1620
    const-string v13, "FeliCaTest-IDM"

    #@53e
    const-string v14, "Response Code Mismatch!"

    #@540
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_543
    .catchall {:try_start_537 .. :try_end_543} :catchall_147
    .catch Ljava/io/FileNotFoundException; {:try_start_537 .. :try_end_543} :catch_c0
    .catch Ljava/io/IOException; {:try_start_537 .. :try_end_543} :catch_114

    #@543
    .line 1621
    const/4 v13, -0x1

    #@544
    .line 1646
    if-eqz v9, :cond_549

    #@546
    .line 1649
    :try_start_546
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_549
    .catch Ljava/io/IOException; {:try_start_546 .. :try_end_549} :catch_56d

    #@549
    .line 1655
    :cond_549
    :goto_549
    if-eqz v11, :cond_555

    #@54b
    .line 1657
    const-string v14, "FeliCaTest-IDM"

    #@54d
    const-string v15, "Closing FeliCa-UART."

    #@54f
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@552
    .line 1661
    :try_start_552
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_555
    .catch Ljava/io/IOException; {:try_start_552 .. :try_end_555} :catch_572

    #@555
    .line 1668
    :cond_555
    :goto_555
    const-string v14, "FeliCaTest-IDM"

    #@557
    const-string v15, "Set FeliCa OFF."

    #@559
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@55c
    .line 1670
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@55f
    move-result v14

    #@560
    if-gez v14, :cond_569

    #@562
    .line 1672
    const-string v14, "FeliCaTest-IDM"

    #@564
    const-string v15, "Set PON OFF failed"

    #@566
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@569
    :cond_569
    move-object v10, v11

    #@56a
    .end local v11           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v10       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v8, v9

    #@56b
    .end local v9           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v8       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_64

    #@56d
    .line 1650
    .end local v8           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v10           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v9       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v11       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_56d
    move-exception v6

    #@56e
    .line 1652
    .restart local v6       #e:Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    #@571
    goto :goto_549

    #@572
    .line 1662
    .end local v6           #e:Ljava/io/IOException;
    :catch_572
    move-exception v6

    #@573
    .line 1664
    .restart local v6       #e:Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    #@576
    goto :goto_555

    #@577
    .line 1625
    .end local v6           #e:Ljava/io/IOException;
    :cond_577
    const/16 v13, 0x9

    #@579
    const/4 v14, 0x0

    #@57a
    :try_start_57a
    move-object/from16 v0, p1

    #@57c
    move/from16 v1, p2

    #@57e
    invoke-static {v7, v13, v0, v14, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_581
    .catchall {:try_start_57a .. :try_end_581} :catchall_147
    .catch Ljava/io/FileNotFoundException; {:try_start_57a .. :try_end_581} :catch_c0
    .catch Ljava/io/IOException; {:try_start_57a .. :try_end_581} :catch_114

    #@581
    .line 1627
    const/4 v13, 0x1

    #@582
    .line 1646
    if-eqz v9, :cond_587

    #@584
    .line 1649
    :try_start_584
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_587
    .catch Ljava/io/IOException; {:try_start_584 .. :try_end_587} :catch_5ab

    #@587
    .line 1655
    :cond_587
    :goto_587
    if-eqz v11, :cond_593

    #@589
    .line 1657
    const-string v14, "FeliCaTest-IDM"

    #@58b
    const-string v15, "Closing FeliCa-UART."

    #@58d
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@590
    .line 1661
    :try_start_590
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_593
    .catch Ljava/io/IOException; {:try_start_590 .. :try_end_593} :catch_5b0

    #@593
    .line 1668
    :cond_593
    :goto_593
    const-string v14, "FeliCaTest-IDM"

    #@595
    const-string v15, "Set FeliCa OFF."

    #@597
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@59a
    .line 1670
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@59d
    move-result v14

    #@59e
    if-gez v14, :cond_5a7

    #@5a0
    .line 1672
    const-string v14, "FeliCaTest-IDM"

    #@5a2
    const-string v15, "Set PON OFF failed"

    #@5a4
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5a7
    :cond_5a7
    move-object v10, v11

    #@5a8
    .end local v11           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v10       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v8, v9

    #@5a9
    .end local v9           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v8       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_64

    #@5ab
    .line 1650
    .end local v8           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v10           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v9       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v11       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_5ab
    move-exception v6

    #@5ac
    .line 1652
    .restart local v6       #e:Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    #@5af
    goto :goto_587

    #@5b0
    .line 1662
    .end local v6           #e:Ljava/io/IOException;
    :catch_5b0
    move-exception v6

    #@5b1
    .line 1664
    .restart local v6       #e:Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    #@5b4
    goto :goto_593

    #@5b5
    .line 1650
    .end local v2           #DEFAULT_TIMEOUT:I
    .end local v4           #current:J
    .end local v9           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v11           #fos_felica_uart:Ljava/io/FileOutputStream;
    .local v6, e:Ljava/io/FileNotFoundException;
    .restart local v8       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v10       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_5b5
    move-exception v6

    #@5b6
    .line 1652
    .local v6, e:Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    #@5b9
    goto/16 :goto_d3

    #@5bb
    .line 1662
    .end local v6           #e:Ljava/io/IOException;
    :catch_5bb
    move-exception v6

    #@5bc
    .line 1664
    .restart local v6       #e:Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    #@5bf
    goto/16 :goto_df

    #@5c1
    .line 1650
    :catch_5c1
    move-exception v6

    #@5c2
    .line 1652
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    #@5c5
    goto/16 :goto_120

    #@5c7
    .line 1662
    :catch_5c7
    move-exception v6

    #@5c8
    .line 1664
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    #@5cb
    goto/16 :goto_12c

    #@5cd
    .line 1650
    .end local v6           #e:Ljava/io/IOException;
    :catch_5cd
    move-exception v6

    #@5ce
    .line 1652
    .restart local v6       #e:Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    #@5d1
    goto/16 :goto_14f

    #@5d3
    .line 1662
    .end local v6           #e:Ljava/io/IOException;
    :catch_5d3
    move-exception v6

    #@5d4
    .line 1664
    .restart local v6       #e:Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    #@5d7
    goto/16 :goto_15b

    #@5d9
    .line 1546
    .end local v6           #e:Ljava/io/IOException;
    .end local v8           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v10           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v2       #DEFAULT_TIMEOUT:I
    .restart local v4       #current:J
    .restart local v9       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v11       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_5d9
    move-exception v13

    #@5da
    goto/16 :goto_266

    #@5dc
    .line 1646
    .end local v2           #DEFAULT_TIMEOUT:I
    .end local v4           #current:J
    .end local v9           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v11           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v8       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v10       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catchall_5dc
    move-exception v13

    #@5dd
    goto/16 :goto_14a

    #@5df
    .end local v8           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v9       #fis_felica_uart:Ljava/io/FileInputStream;
    :catchall_5df
    move-exception v13

    #@5e0
    move-object v8, v9

    #@5e1
    .end local v9           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v8       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_14a

    #@5e3
    .line 1637
    :catch_5e3
    move-exception v6

    #@5e4
    goto/16 :goto_117

    #@5e6
    .end local v8           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v9       #fis_felica_uart:Ljava/io/FileInputStream;
    :catch_5e6
    move-exception v6

    #@5e7
    move-object v8, v9

    #@5e8
    .end local v9           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v8       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_117

    #@5ea
    .line 1629
    :catch_5ea
    move-exception v6

    #@5eb
    goto/16 :goto_c3

    #@5ed
    .end local v8           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v9       #fis_felica_uart:Ljava/io/FileInputStream;
    :catch_5ed
    move-exception v6

    #@5ee
    move-object v8, v9

    #@5ef
    .end local v9           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v8       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_c3
.end method

.method static felica_read_idm([BI)I
    .registers 16
    .parameter "idm"
    .parameter "idm_buf_size"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v7, -0x1

    #@2
    const/4 v11, 0x0

    #@3
    .line 1208
    const/4 v6, 0x0

    #@4
    .line 1209
    .local v6, uart_result_len:I
    const/16 v9, 0x400

    #@6
    new-array v1, v9, [B

    #@8
    .line 1211
    .local v1, felica_uart_buff:[B
    const-string v9, "FeliCaDevice"

    #@a
    const-string v10, "Openning FeliCa-UART driver."

    #@c
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 1213
    invoke-static {v1, v11}, Ljava/util/Arrays;->fill([BB)V

    #@12
    .line 1214
    invoke-static {p0, v11}, Ljava/util/Arrays;->fill([BB)V

    #@15
    .line 1216
    const/4 v2, 0x0

    #@16
    .line 1217
    .local v2, fis_felica_uart:Ljava/io/FileInputStream;
    const/4 v4, 0x0

    #@17
    .line 1221
    .local v4, fos_felica_uart:Ljava/io/FileOutputStream;
    :try_start_17
    const-string v9, "FeliCaDevice"

    #@19
    const-string v10, "Openning in stream."

    #@1b
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 1222
    new-instance v3, Ljava/io/FileInputStream;

    #@20
    const-string v9, "/dev/felica"

    #@22
    invoke-direct {v3, v9}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_25
    .catchall {:try_start_17 .. :try_end_25} :catchall_49a
    .catch Ljava/io/FileNotFoundException; {:try_start_17 .. :try_end_25} :catch_429
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_25} :catch_465

    #@25
    .line 1223
    .end local v2           #fis_felica_uart:Ljava/io/FileInputStream;
    .local v3, fis_felica_uart:Ljava/io/FileInputStream;
    :try_start_25
    const-string v9, "FeliCaDevice"

    #@27
    const-string v10, "Openning out stream."

    #@29
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 1224
    new-instance v5, Ljava/io/FileOutputStream;

    #@2e
    const-string v9, "/dev/felica"

    #@30
    invoke-direct {v5, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_33
    .catchall {:try_start_25 .. :try_end_33} :catchall_4cb
    .catch Ljava/io/FileNotFoundException; {:try_start_25 .. :try_end_33} :catch_4d9
    .catch Ljava/io/IOException; {:try_start_25 .. :try_end_33} :catch_4d2

    #@33
    .line 1226
    .end local v4           #fos_felica_uart:Ljava/io/FileOutputStream;
    .local v5, fos_felica_uart:Ljava/io/FileOutputStream;
    :try_start_33
    const-string v9, "FeliCaDevice"

    #@35
    const-string v10, "Set FeliCa ON."

    #@37
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 1228
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_open()I

    #@3d
    move-result v9

    #@3e
    if-gez v9, :cond_79

    #@40
    .line 1229
    const-string v8, "FeliCaDevice"

    #@42
    const-string v9, "Set PON ON failed"

    #@44
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_47
    .catchall {:try_start_33 .. :try_end_47} :catchall_4ce
    .catch Ljava/io/FileNotFoundException; {:try_start_33 .. :try_end_47} :catch_4dd
    .catch Ljava/io/IOException; {:try_start_33 .. :try_end_47} :catch_4d5

    #@47
    .line 1413
    if-eqz v3, :cond_4c

    #@49
    .line 1415
    :try_start_49
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_4c
    .catch Ljava/io/IOException; {:try_start_49 .. :try_end_4c} :catch_6f

    #@4c
    .line 1420
    :cond_4c
    :goto_4c
    if-eqz v5, :cond_58

    #@4e
    .line 1421
    const-string v8, "FeliCaDevice"

    #@50
    const-string v9, "Closing FeilCa-UART."

    #@52
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@55
    .line 1424
    :try_start_55
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_58
    .catch Ljava/io/IOException; {:try_start_55 .. :try_end_58} :catch_74

    #@58
    .line 1430
    :cond_58
    :goto_58
    const-string v8, "FeliCaDevice"

    #@5a
    const-string v9, "Set FeliCa OFF."

    #@5c
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5f
    .line 1432
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@62
    move-result v8

    #@63
    if-gez v8, :cond_6c

    #@65
    .line 1433
    const-string v8, "FeliCaDevice"

    #@67
    const-string v9, "Set PON OFF failed"

    #@69
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6c
    :cond_6c
    move-object v4, v5

    #@6d
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v4       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v2, v3

    #@6e
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v2       #fis_felica_uart:Ljava/io/FileInputStream;
    :cond_6e
    :goto_6e
    return v7

    #@6f
    .line 1416
    .end local v2           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v4           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v5       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_6f
    move-exception v0

    #@70
    .line 1417
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@73
    goto :goto_4c

    #@74
    .line 1425
    .end local v0           #e:Ljava/io/IOException;
    :catch_74
    move-exception v0

    #@75
    .line 1426
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@78
    goto :goto_58

    #@79
    .line 1234
    .end local v0           #e:Ljava/io/IOException;
    :cond_79
    :try_start_79
    const-string v9, "FeliCaDevice"

    #@7b
    const-string v10, "Reading FeilCa UART port ready"

    #@7d
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@80
    .line 1236
    const/4 v9, 0x0

    #@81
    sget v10, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_PORT_READY_LEN:I

    #@83
    invoke-virtual {v3, v1, v9, v10}, Ljava/io/FileInputStream;->read([BII)I

    #@86
    move-result v6

    #@87
    .line 1239
    const-string v9, "FeliCaDevice"

    #@89
    const-string v10, "Port ready : 0x%x,0x%x,0x%x,0x%x"

    #@8b
    const/4 v11, 0x4

    #@8c
    new-array v11, v11, [Ljava/lang/Object;

    #@8e
    const/4 v12, 0x0

    #@8f
    const/4 v13, 0x0

    #@90
    aget-byte v13, v1, v13

    #@92
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@95
    move-result-object v13

    #@96
    aput-object v13, v11, v12

    #@98
    const/4 v12, 0x1

    #@99
    const/4 v13, 0x1

    #@9a
    aget-byte v13, v1, v13

    #@9c
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@9f
    move-result-object v13

    #@a0
    aput-object v13, v11, v12

    #@a2
    const/4 v12, 0x2

    #@a3
    const/4 v13, 0x2

    #@a4
    aget-byte v13, v1, v13

    #@a6
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@a9
    move-result-object v13

    #@aa
    aput-object v13, v11, v12

    #@ac
    const/4 v12, 0x3

    #@ad
    const/4 v13, 0x3

    #@ae
    aget-byte v13, v1, v13

    #@b0
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@b3
    move-result-object v13

    #@b4
    aput-object v13, v11, v12

    #@b6
    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@b9
    move-result-object v10

    #@ba
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@bd
    .line 1243
    sget v9, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_PORT_READY_LEN:I

    #@bf
    if-eq v6, v9, :cond_10c

    #@c1
    .line 1244
    const-string v8, "FeliCaDevice"

    #@c3
    new-instance v9, Ljava/lang/StringBuilder;

    #@c5
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@c8
    const-string v10, "Read Length Mismatch : "

    #@ca
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v9

    #@ce
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v9

    #@d2
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d5
    move-result-object v9

    #@d6
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_d9
    .catchall {:try_start_79 .. :try_end_d9} :catchall_4ce
    .catch Ljava/io/FileNotFoundException; {:try_start_79 .. :try_end_d9} :catch_4dd
    .catch Ljava/io/IOException; {:try_start_79 .. :try_end_d9} :catch_4d5

    #@d9
    .line 1413
    if-eqz v3, :cond_de

    #@db
    .line 1415
    :try_start_db
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_de
    .catch Ljava/io/IOException; {:try_start_db .. :try_end_de} :catch_102

    #@de
    .line 1420
    :cond_de
    :goto_de
    if-eqz v5, :cond_ea

    #@e0
    .line 1421
    const-string v8, "FeliCaDevice"

    #@e2
    const-string v9, "Closing FeilCa-UART."

    #@e4
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e7
    .line 1424
    :try_start_e7
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_ea
    .catch Ljava/io/IOException; {:try_start_e7 .. :try_end_ea} :catch_107

    #@ea
    .line 1430
    :cond_ea
    :goto_ea
    const-string v8, "FeliCaDevice"

    #@ec
    const-string v9, "Set FeliCa OFF."

    #@ee
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f1
    .line 1432
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@f4
    move-result v8

    #@f5
    if-gez v8, :cond_fe

    #@f7
    .line 1433
    const-string v8, "FeliCaDevice"

    #@f9
    const-string v9, "Set PON OFF failed"

    #@fb
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@fe
    :cond_fe
    move-object v4, v5

    #@ff
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v4       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v2, v3

    #@100
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v2       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_6e

    #@102
    .line 1416
    .end local v2           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v4           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v5       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_102
    move-exception v0

    #@103
    .line 1417
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@106
    goto :goto_de

    #@107
    .line 1425
    .end local v0           #e:Ljava/io/IOException;
    :catch_107
    move-exception v0

    #@108
    .line 1426
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@10b
    goto :goto_ea

    #@10c
    .line 1249
    .end local v0           #e:Ljava/io/IOException;
    :cond_10c
    :try_start_10c
    sget v9, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_PORT_READY_LEN:I

    #@10e
    invoke-static {v1, v9}, Ljava/util/Arrays;->copyOf([BI)[B

    #@111
    move-result-object v9

    #@112
    sget-object v10, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_PORT_READY:[B

    #@114
    invoke-static {v9, v10}, Ljava/util/Arrays;->equals([B[B)Z

    #@117
    move-result v9

    #@118
    if-nez v9, :cond_154

    #@11a
    .line 1252
    const-string v8, "FeliCaDevice"

    #@11c
    const-string v9, "Port-Ready Mismatch!"

    #@11e
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_121
    .catchall {:try_start_10c .. :try_end_121} :catchall_4ce
    .catch Ljava/io/FileNotFoundException; {:try_start_10c .. :try_end_121} :catch_4dd
    .catch Ljava/io/IOException; {:try_start_10c .. :try_end_121} :catch_4d5

    #@121
    .line 1413
    if-eqz v3, :cond_126

    #@123
    .line 1415
    :try_start_123
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_126
    .catch Ljava/io/IOException; {:try_start_123 .. :try_end_126} :catch_14a

    #@126
    .line 1420
    :cond_126
    :goto_126
    if-eqz v5, :cond_132

    #@128
    .line 1421
    const-string v8, "FeliCaDevice"

    #@12a
    const-string v9, "Closing FeilCa-UART."

    #@12c
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@12f
    .line 1424
    :try_start_12f
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_132
    .catch Ljava/io/IOException; {:try_start_12f .. :try_end_132} :catch_14f

    #@132
    .line 1430
    :cond_132
    :goto_132
    const-string v8, "FeliCaDevice"

    #@134
    const-string v9, "Set FeliCa OFF."

    #@136
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@139
    .line 1432
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@13c
    move-result v8

    #@13d
    if-gez v8, :cond_146

    #@13f
    .line 1433
    const-string v8, "FeliCaDevice"

    #@141
    const-string v9, "Set PON OFF failed"

    #@143
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@146
    :cond_146
    move-object v4, v5

    #@147
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v4       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v2, v3

    #@148
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v2       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_6e

    #@14a
    .line 1416
    .end local v2           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v4           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v5       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_14a
    move-exception v0

    #@14b
    .line 1417
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@14e
    goto :goto_126

    #@14f
    .line 1425
    .end local v0           #e:Ljava/io/IOException;
    :catch_14f
    move-exception v0

    #@150
    .line 1426
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@153
    goto :goto_132

    #@154
    .line 1257
    .end local v0           #e:Ljava/io/IOException;
    :cond_154
    :try_start_154
    const-string v9, "FeliCaDevice"

    #@156
    const-string v10, "Writing polling cmd"

    #@158
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@15b
    .line 1258
    sget-object v9, Lcom/lge/systemservice/service/FeliCaDevice;->CMD_POLLING:[B

    #@15d
    invoke-virtual {v5, v9}, Ljava/io/FileOutputStream;->write([B)V

    #@160
    .line 1262
    const-string v9, "FeliCaDevice"

    #@162
    const-string v10, "Reading Ack packet."

    #@164
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@167
    .line 1264
    const/4 v9, 0x0

    #@168
    sget v10, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_ACK_LEN:I

    #@16a
    invoke-virtual {v3, v1, v9, v10}, Ljava/io/FileInputStream;->read([BII)I

    #@16d
    move-result v6

    #@16e
    .line 1267
    const-string v9, "FeliCaDevice"

    #@170
    const-string v10, "Ack : 0x%x,0x%x,0x%x,0x%x"

    #@172
    const/4 v11, 0x4

    #@173
    new-array v11, v11, [Ljava/lang/Object;

    #@175
    const/4 v12, 0x0

    #@176
    const/4 v13, 0x0

    #@177
    aget-byte v13, v1, v13

    #@179
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@17c
    move-result-object v13

    #@17d
    aput-object v13, v11, v12

    #@17f
    const/4 v12, 0x1

    #@180
    const/4 v13, 0x1

    #@181
    aget-byte v13, v1, v13

    #@183
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@186
    move-result-object v13

    #@187
    aput-object v13, v11, v12

    #@189
    const/4 v12, 0x2

    #@18a
    const/4 v13, 0x2

    #@18b
    aget-byte v13, v1, v13

    #@18d
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@190
    move-result-object v13

    #@191
    aput-object v13, v11, v12

    #@193
    const/4 v12, 0x3

    #@194
    const/4 v13, 0x3

    #@195
    aget-byte v13, v1, v13

    #@197
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@19a
    move-result-object v13

    #@19b
    aput-object v13, v11, v12

    #@19d
    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@1a0
    move-result-object v10

    #@1a1
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a4
    .line 1271
    sget v9, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_ACK_LEN:I

    #@1a6
    if-eq v6, v9, :cond_1f3

    #@1a8
    .line 1272
    const-string v8, "FeliCaDevice"

    #@1aa
    new-instance v9, Ljava/lang/StringBuilder;

    #@1ac
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@1af
    const-string v10, "Read Length Mismatch : "

    #@1b1
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b4
    move-result-object v9

    #@1b5
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b8
    move-result-object v9

    #@1b9
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1bc
    move-result-object v9

    #@1bd
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1c0
    .catchall {:try_start_154 .. :try_end_1c0} :catchall_4ce
    .catch Ljava/io/FileNotFoundException; {:try_start_154 .. :try_end_1c0} :catch_4dd
    .catch Ljava/io/IOException; {:try_start_154 .. :try_end_1c0} :catch_4d5

    #@1c0
    .line 1413
    if-eqz v3, :cond_1c5

    #@1c2
    .line 1415
    :try_start_1c2
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_1c5
    .catch Ljava/io/IOException; {:try_start_1c2 .. :try_end_1c5} :catch_1e9

    #@1c5
    .line 1420
    :cond_1c5
    :goto_1c5
    if-eqz v5, :cond_1d1

    #@1c7
    .line 1421
    const-string v8, "FeliCaDevice"

    #@1c9
    const-string v9, "Closing FeilCa-UART."

    #@1cb
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1ce
    .line 1424
    :try_start_1ce
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_1d1
    .catch Ljava/io/IOException; {:try_start_1ce .. :try_end_1d1} :catch_1ee

    #@1d1
    .line 1430
    :cond_1d1
    :goto_1d1
    const-string v8, "FeliCaDevice"

    #@1d3
    const-string v9, "Set FeliCa OFF."

    #@1d5
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d8
    .line 1432
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@1db
    move-result v8

    #@1dc
    if-gez v8, :cond_1e5

    #@1de
    .line 1433
    const-string v8, "FeliCaDevice"

    #@1e0
    const-string v9, "Set PON OFF failed"

    #@1e2
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e5
    :cond_1e5
    move-object v4, v5

    #@1e6
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v4       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v2, v3

    #@1e7
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v2       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_6e

    #@1e9
    .line 1416
    .end local v2           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v4           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v5       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_1e9
    move-exception v0

    #@1ea
    .line 1417
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@1ed
    goto :goto_1c5

    #@1ee
    .line 1425
    .end local v0           #e:Ljava/io/IOException;
    :catch_1ee
    move-exception v0

    #@1ef
    .line 1426
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@1f2
    goto :goto_1d1

    #@1f3
    .line 1277
    .end local v0           #e:Ljava/io/IOException;
    :cond_1f3
    :try_start_1f3
    sget v9, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_ACK_LEN:I

    #@1f5
    invoke-static {v1, v9}, Ljava/util/Arrays;->copyOf([BI)[B

    #@1f8
    move-result-object v9

    #@1f9
    sget-object v10, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_ACK:[B

    #@1fb
    invoke-static {v9, v10}, Ljava/util/Arrays;->equals([B[B)Z

    #@1fe
    move-result v9

    #@1ff
    if-nez v9, :cond_23b

    #@201
    .line 1279
    const-string v8, "FeliCaDevice"

    #@203
    const-string v9, "Ack Mismatch!"

    #@205
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_208
    .catchall {:try_start_1f3 .. :try_end_208} :catchall_4ce
    .catch Ljava/io/FileNotFoundException; {:try_start_1f3 .. :try_end_208} :catch_4dd
    .catch Ljava/io/IOException; {:try_start_1f3 .. :try_end_208} :catch_4d5

    #@208
    .line 1413
    if-eqz v3, :cond_20d

    #@20a
    .line 1415
    :try_start_20a
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_20d
    .catch Ljava/io/IOException; {:try_start_20a .. :try_end_20d} :catch_231

    #@20d
    .line 1420
    :cond_20d
    :goto_20d
    if-eqz v5, :cond_219

    #@20f
    .line 1421
    const-string v8, "FeliCaDevice"

    #@211
    const-string v9, "Closing FeilCa-UART."

    #@213
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@216
    .line 1424
    :try_start_216
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_219
    .catch Ljava/io/IOException; {:try_start_216 .. :try_end_219} :catch_236

    #@219
    .line 1430
    :cond_219
    :goto_219
    const-string v8, "FeliCaDevice"

    #@21b
    const-string v9, "Set FeliCa OFF."

    #@21d
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@220
    .line 1432
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@223
    move-result v8

    #@224
    if-gez v8, :cond_22d

    #@226
    .line 1433
    const-string v8, "FeliCaDevice"

    #@228
    const-string v9, "Set PON OFF failed"

    #@22a
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22d
    :cond_22d
    move-object v4, v5

    #@22e
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v4       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v2, v3

    #@22f
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v2       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_6e

    #@231
    .line 1416
    .end local v2           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v4           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v5       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_231
    move-exception v0

    #@232
    .line 1417
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@235
    goto :goto_20d

    #@236
    .line 1425
    .end local v0           #e:Ljava/io/IOException;
    :catch_236
    move-exception v0

    #@237
    .line 1426
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@23a
    goto :goto_219

    #@23b
    .line 1346
    .end local v0           #e:Ljava/io/IOException;
    :cond_23b
    :try_start_23b
    const-string v9, "FeliCaDevice"

    #@23d
    const-string v10, "Reading polling response."

    #@23f
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@242
    .line 1348
    const/4 v9, 0x0

    #@243
    const/16 v10, 0x17

    #@245
    invoke-virtual {v3, v1, v9, v10}, Ljava/io/FileInputStream;->read([BII)I

    #@248
    move-result v6

    #@249
    .line 1357
    const-string v9, "FeliCaDevice"

    #@24b
    const-string v10, "response1 : 0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x"

    #@24d
    const/16 v11, 0x8

    #@24f
    new-array v11, v11, [Ljava/lang/Object;

    #@251
    const/4 v12, 0x0

    #@252
    const/4 v13, 0x0

    #@253
    aget-byte v13, v1, v13

    #@255
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@258
    move-result-object v13

    #@259
    aput-object v13, v11, v12

    #@25b
    const/4 v12, 0x1

    #@25c
    const/4 v13, 0x1

    #@25d
    aget-byte v13, v1, v13

    #@25f
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@262
    move-result-object v13

    #@263
    aput-object v13, v11, v12

    #@265
    const/4 v12, 0x2

    #@266
    const/4 v13, 0x2

    #@267
    aget-byte v13, v1, v13

    #@269
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@26c
    move-result-object v13

    #@26d
    aput-object v13, v11, v12

    #@26f
    const/4 v12, 0x3

    #@270
    const/4 v13, 0x3

    #@271
    aget-byte v13, v1, v13

    #@273
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@276
    move-result-object v13

    #@277
    aput-object v13, v11, v12

    #@279
    const/4 v12, 0x4

    #@27a
    const/4 v13, 0x4

    #@27b
    aget-byte v13, v1, v13

    #@27d
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@280
    move-result-object v13

    #@281
    aput-object v13, v11, v12

    #@283
    const/4 v12, 0x5

    #@284
    const/4 v13, 0x5

    #@285
    aget-byte v13, v1, v13

    #@287
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@28a
    move-result-object v13

    #@28b
    aput-object v13, v11, v12

    #@28d
    const/4 v12, 0x6

    #@28e
    const/4 v13, 0x6

    #@28f
    aget-byte v13, v1, v13

    #@291
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@294
    move-result-object v13

    #@295
    aput-object v13, v11, v12

    #@297
    const/4 v12, 0x7

    #@298
    const/4 v13, 0x7

    #@299
    aget-byte v13, v1, v13

    #@29b
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@29e
    move-result-object v13

    #@29f
    aput-object v13, v11, v12

    #@2a1
    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@2a4
    move-result-object v10

    #@2a5
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a8
    .line 1363
    const-string v9, "FeliCaDevice"

    #@2aa
    const-string v10, "response2 : 0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x"

    #@2ac
    const/16 v11, 0x8

    #@2ae
    new-array v11, v11, [Ljava/lang/Object;

    #@2b0
    const/4 v12, 0x0

    #@2b1
    const/16 v13, 0x8

    #@2b3
    aget-byte v13, v1, v13

    #@2b5
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@2b8
    move-result-object v13

    #@2b9
    aput-object v13, v11, v12

    #@2bb
    const/4 v12, 0x1

    #@2bc
    const/16 v13, 0x9

    #@2be
    aget-byte v13, v1, v13

    #@2c0
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@2c3
    move-result-object v13

    #@2c4
    aput-object v13, v11, v12

    #@2c6
    const/4 v12, 0x2

    #@2c7
    const/16 v13, 0xa

    #@2c9
    aget-byte v13, v1, v13

    #@2cb
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@2ce
    move-result-object v13

    #@2cf
    aput-object v13, v11, v12

    #@2d1
    const/4 v12, 0x3

    #@2d2
    const/16 v13, 0xb

    #@2d4
    aget-byte v13, v1, v13

    #@2d6
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@2d9
    move-result-object v13

    #@2da
    aput-object v13, v11, v12

    #@2dc
    const/4 v12, 0x4

    #@2dd
    const/16 v13, 0xc

    #@2df
    aget-byte v13, v1, v13

    #@2e1
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@2e4
    move-result-object v13

    #@2e5
    aput-object v13, v11, v12

    #@2e7
    const/4 v12, 0x5

    #@2e8
    const/16 v13, 0xd

    #@2ea
    aget-byte v13, v1, v13

    #@2ec
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@2ef
    move-result-object v13

    #@2f0
    aput-object v13, v11, v12

    #@2f2
    const/4 v12, 0x6

    #@2f3
    const/16 v13, 0xe

    #@2f5
    aget-byte v13, v1, v13

    #@2f7
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@2fa
    move-result-object v13

    #@2fb
    aput-object v13, v11, v12

    #@2fd
    const/4 v12, 0x7

    #@2fe
    const/16 v13, 0xf

    #@300
    aget-byte v13, v1, v13

    #@302
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@305
    move-result-object v13

    #@306
    aput-object v13, v11, v12

    #@308
    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@30b
    move-result-object v10

    #@30c
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@30f
    .line 1370
    const-string v9, "FeliCaDevice"

    #@311
    const-string v10, "Validating polling response packet."

    #@313
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@316
    .line 1373
    const/16 v9, 0x17

    #@318
    if-eq v6, v9, :cond_365

    #@31a
    .line 1374
    const-string v8, "FeliCaDevice"

    #@31c
    new-instance v9, Ljava/lang/StringBuilder;

    #@31e
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@321
    const-string v10, "Length Mismatch : "

    #@323
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@326
    move-result-object v9

    #@327
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32a
    move-result-object v9

    #@32b
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32e
    move-result-object v9

    #@32f
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_332
    .catchall {:try_start_23b .. :try_end_332} :catchall_4ce
    .catch Ljava/io/FileNotFoundException; {:try_start_23b .. :try_end_332} :catch_4dd
    .catch Ljava/io/IOException; {:try_start_23b .. :try_end_332} :catch_4d5

    #@332
    .line 1413
    if-eqz v3, :cond_337

    #@334
    .line 1415
    :try_start_334
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_337
    .catch Ljava/io/IOException; {:try_start_334 .. :try_end_337} :catch_35b

    #@337
    .line 1420
    :cond_337
    :goto_337
    if-eqz v5, :cond_343

    #@339
    .line 1421
    const-string v8, "FeliCaDevice"

    #@33b
    const-string v9, "Closing FeilCa-UART."

    #@33d
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@340
    .line 1424
    :try_start_340
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_343
    .catch Ljava/io/IOException; {:try_start_340 .. :try_end_343} :catch_360

    #@343
    .line 1430
    :cond_343
    :goto_343
    const-string v8, "FeliCaDevice"

    #@345
    const-string v9, "Set FeliCa OFF."

    #@347
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@34a
    .line 1432
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@34d
    move-result v8

    #@34e
    if-gez v8, :cond_357

    #@350
    .line 1433
    const-string v8, "FeliCaDevice"

    #@352
    const-string v9, "Set PON OFF failed"

    #@354
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@357
    :cond_357
    move-object v4, v5

    #@358
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v4       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v2, v3

    #@359
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v2       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_6e

    #@35b
    .line 1416
    .end local v2           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v4           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v5       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_35b
    move-exception v0

    #@35c
    .line 1417
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@35f
    goto :goto_337

    #@360
    .line 1425
    .end local v0           #e:Ljava/io/IOException;
    :catch_360
    move-exception v0

    #@361
    .line 1426
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@364
    goto :goto_343

    #@365
    .line 1381
    .end local v0           #e:Ljava/io/IOException;
    :cond_365
    const/4 v9, 0x0

    #@366
    :try_start_366
    aget-byte v9, v1, v9

    #@368
    if-nez v9, :cond_377

    #@36a
    const/4 v9, 0x1

    #@36b
    aget-byte v9, v1, v9

    #@36d
    const/4 v10, -0x6

    #@36e
    if-ne v9, v10, :cond_377

    #@370
    const/4 v9, 0x2

    #@371
    aget-byte v9, v1, v9

    #@373
    const/16 v10, 0x12

    #@375
    if-eq v9, v10, :cond_3b1

    #@377
    .line 1384
    :cond_377
    const-string v8, "FeliCaDevice"

    #@379
    const-string v9, "Header Mismatch!"

    #@37b
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_37e
    .catchall {:try_start_366 .. :try_end_37e} :catchall_4ce
    .catch Ljava/io/FileNotFoundException; {:try_start_366 .. :try_end_37e} :catch_4dd
    .catch Ljava/io/IOException; {:try_start_366 .. :try_end_37e} :catch_4d5

    #@37e
    .line 1413
    if-eqz v3, :cond_383

    #@380
    .line 1415
    :try_start_380
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_383
    .catch Ljava/io/IOException; {:try_start_380 .. :try_end_383} :catch_3a7

    #@383
    .line 1420
    :cond_383
    :goto_383
    if-eqz v5, :cond_38f

    #@385
    .line 1421
    const-string v8, "FeliCaDevice"

    #@387
    const-string v9, "Closing FeilCa-UART."

    #@389
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@38c
    .line 1424
    :try_start_38c
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_38f
    .catch Ljava/io/IOException; {:try_start_38c .. :try_end_38f} :catch_3ac

    #@38f
    .line 1430
    :cond_38f
    :goto_38f
    const-string v8, "FeliCaDevice"

    #@391
    const-string v9, "Set FeliCa OFF."

    #@393
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@396
    .line 1432
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@399
    move-result v8

    #@39a
    if-gez v8, :cond_3a3

    #@39c
    .line 1433
    const-string v8, "FeliCaDevice"

    #@39e
    const-string v9, "Set PON OFF failed"

    #@3a0
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a3
    :cond_3a3
    move-object v4, v5

    #@3a4
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v4       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v2, v3

    #@3a5
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v2       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_6e

    #@3a7
    .line 1416
    .end local v2           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v4           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v5       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_3a7
    move-exception v0

    #@3a8
    .line 1417
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@3ab
    goto :goto_383

    #@3ac
    .line 1425
    .end local v0           #e:Ljava/io/IOException;
    :catch_3ac
    move-exception v0

    #@3ad
    .line 1426
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@3b0
    goto :goto_38f

    #@3b1
    .line 1389
    .end local v0           #e:Ljava/io/IOException;
    :cond_3b1
    const/4 v9, 0x3

    #@3b2
    :try_start_3b2
    aget-byte v9, v1, v9

    #@3b4
    if-eq v9, v8, :cond_3f0

    #@3b6
    .line 1390
    const-string v8, "FeliCaDevice"

    #@3b8
    const-string v9, "Response Code Mismatch!"

    #@3ba
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3bd
    .catchall {:try_start_3b2 .. :try_end_3bd} :catchall_4ce
    .catch Ljava/io/FileNotFoundException; {:try_start_3b2 .. :try_end_3bd} :catch_4dd
    .catch Ljava/io/IOException; {:try_start_3b2 .. :try_end_3bd} :catch_4d5

    #@3bd
    .line 1413
    if-eqz v3, :cond_3c2

    #@3bf
    .line 1415
    :try_start_3bf
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_3c2
    .catch Ljava/io/IOException; {:try_start_3bf .. :try_end_3c2} :catch_3e6

    #@3c2
    .line 1420
    :cond_3c2
    :goto_3c2
    if-eqz v5, :cond_3ce

    #@3c4
    .line 1421
    const-string v8, "FeliCaDevice"

    #@3c6
    const-string v9, "Closing FeilCa-UART."

    #@3c8
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3cb
    .line 1424
    :try_start_3cb
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_3ce
    .catch Ljava/io/IOException; {:try_start_3cb .. :try_end_3ce} :catch_3eb

    #@3ce
    .line 1430
    :cond_3ce
    :goto_3ce
    const-string v8, "FeliCaDevice"

    #@3d0
    const-string v9, "Set FeliCa OFF."

    #@3d2
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3d5
    .line 1432
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@3d8
    move-result v8

    #@3d9
    if-gez v8, :cond_3e2

    #@3db
    .line 1433
    const-string v8, "FeliCaDevice"

    #@3dd
    const-string v9, "Set PON OFF failed"

    #@3df
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3e2
    :cond_3e2
    move-object v4, v5

    #@3e3
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v4       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v2, v3

    #@3e4
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v2       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_6e

    #@3e6
    .line 1416
    .end local v2           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v4           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v5       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_3e6
    move-exception v0

    #@3e7
    .line 1417
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@3ea
    goto :goto_3c2

    #@3eb
    .line 1425
    .end local v0           #e:Ljava/io/IOException;
    :catch_3eb
    move-exception v0

    #@3ec
    .line 1426
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@3ef
    goto :goto_3ce

    #@3f0
    .line 1395
    .end local v0           #e:Ljava/io/IOException;
    :cond_3f0
    const/4 v9, 0x4

    #@3f1
    const/4 v10, 0x0

    #@3f2
    :try_start_3f2
    invoke-static {v1, v9, p0, v10, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_3f5
    .catchall {:try_start_3f2 .. :try_end_3f5} :catchall_4ce
    .catch Ljava/io/FileNotFoundException; {:try_start_3f2 .. :try_end_3f5} :catch_4dd
    .catch Ljava/io/IOException; {:try_start_3f2 .. :try_end_3f5} :catch_4d5

    #@3f5
    .line 1413
    if-eqz v3, :cond_3fa

    #@3f7
    .line 1415
    :try_start_3f7
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_3fa
    .catch Ljava/io/IOException; {:try_start_3f7 .. :try_end_3fa} :catch_41f

    #@3fa
    .line 1420
    :cond_3fa
    :goto_3fa
    if-eqz v5, :cond_406

    #@3fc
    .line 1421
    const-string v7, "FeliCaDevice"

    #@3fe
    const-string v9, "Closing FeilCa-UART."

    #@400
    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@403
    .line 1424
    :try_start_403
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_406
    .catch Ljava/io/IOException; {:try_start_403 .. :try_end_406} :catch_424

    #@406
    .line 1430
    :cond_406
    :goto_406
    const-string v7, "FeliCaDevice"

    #@408
    const-string v9, "Set FeliCa OFF."

    #@40a
    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@40d
    .line 1432
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@410
    move-result v7

    #@411
    if-gez v7, :cond_41a

    #@413
    .line 1433
    const-string v7, "FeliCaDevice"

    #@415
    const-string v9, "Set PON OFF failed"

    #@417
    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@41a
    :cond_41a
    move-object v4, v5

    #@41b
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v4       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v2, v3

    #@41c
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v2       #fis_felica_uart:Ljava/io/FileInputStream;
    move v7, v8

    #@41d
    goto/16 :goto_6e

    #@41f
    .line 1416
    .end local v2           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v4           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v5       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_41f
    move-exception v0

    #@420
    .line 1417
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@423
    goto :goto_3fa

    #@424
    .line 1425
    .end local v0           #e:Ljava/io/IOException;
    :catch_424
    move-exception v0

    #@425
    .line 1426
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@428
    goto :goto_406

    #@429
    .line 1399
    .end local v0           #e:Ljava/io/IOException;
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v2       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v4       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_429
    move-exception v0

    #@42a
    .line 1400
    .local v0, e:Ljava/io/FileNotFoundException;
    :goto_42a
    :try_start_42a
    const-string v8, "FeliCaDevice"

    #@42c
    const-string v9, "Can\'t open FeliCa-UART !. Can\'t read IDM"

    #@42e
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@431
    .line 1402
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_434
    .catchall {:try_start_42a .. :try_end_434} :catchall_49a

    #@434
    .line 1413
    if-eqz v2, :cond_439

    #@436
    .line 1415
    :try_start_436
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_439
    .catch Ljava/io/IOException; {:try_start_436 .. :try_end_439} :catch_45b

    #@439
    .line 1420
    .end local v0           #e:Ljava/io/FileNotFoundException;
    :cond_439
    :goto_439
    if-eqz v4, :cond_445

    #@43b
    .line 1421
    const-string v8, "FeliCaDevice"

    #@43d
    const-string v9, "Closing FeilCa-UART."

    #@43f
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@442
    .line 1424
    :try_start_442
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_445
    .catch Ljava/io/IOException; {:try_start_442 .. :try_end_445} :catch_460

    #@445
    .line 1430
    :cond_445
    :goto_445
    const-string v8, "FeliCaDevice"

    #@447
    const-string v9, "Set FeliCa OFF."

    #@449
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@44c
    .line 1432
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@44f
    move-result v8

    #@450
    if-gez v8, :cond_6e

    #@452
    .line 1433
    const-string v8, "FeliCaDevice"

    #@454
    const-string v9, "Set PON OFF failed"

    #@456
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@459
    goto/16 :goto_6e

    #@45b
    .line 1416
    .restart local v0       #e:Ljava/io/FileNotFoundException;
    :catch_45b
    move-exception v0

    #@45c
    .line 1417
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@45f
    goto :goto_439

    #@460
    .line 1425
    .end local v0           #e:Ljava/io/IOException;
    :catch_460
    move-exception v0

    #@461
    .line 1426
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@464
    goto :goto_445

    #@465
    .line 1406
    .end local v0           #e:Ljava/io/IOException;
    :catch_465
    move-exception v0

    #@466
    .line 1408
    .restart local v0       #e:Ljava/io/IOException;
    :goto_466
    :try_start_466
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_469
    .catchall {:try_start_466 .. :try_end_469} :catchall_49a

    #@469
    .line 1413
    if-eqz v2, :cond_46e

    #@46b
    .line 1415
    :try_start_46b
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_46e
    .catch Ljava/io/IOException; {:try_start_46b .. :try_end_46e} :catch_490

    #@46e
    .line 1420
    :cond_46e
    :goto_46e
    if-eqz v4, :cond_47a

    #@470
    .line 1421
    const-string v8, "FeliCaDevice"

    #@472
    const-string v9, "Closing FeilCa-UART."

    #@474
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@477
    .line 1424
    :try_start_477
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_47a
    .catch Ljava/io/IOException; {:try_start_477 .. :try_end_47a} :catch_495

    #@47a
    .line 1430
    :cond_47a
    :goto_47a
    const-string v8, "FeliCaDevice"

    #@47c
    const-string v9, "Set FeliCa OFF."

    #@47e
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@481
    .line 1432
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@484
    move-result v8

    #@485
    if-gez v8, :cond_6e

    #@487
    .line 1433
    const-string v8, "FeliCaDevice"

    #@489
    const-string v9, "Set PON OFF failed"

    #@48b
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@48e
    goto/16 :goto_6e

    #@490
    .line 1416
    :catch_490
    move-exception v0

    #@491
    .line 1417
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@494
    goto :goto_46e

    #@495
    .line 1425
    :catch_495
    move-exception v0

    #@496
    .line 1426
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@499
    goto :goto_47a

    #@49a
    .line 1413
    .end local v0           #e:Ljava/io/IOException;
    :catchall_49a
    move-exception v7

    #@49b
    :goto_49b
    if-eqz v2, :cond_4a0

    #@49d
    .line 1415
    :try_start_49d
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_4a0
    .catch Ljava/io/IOException; {:try_start_49d .. :try_end_4a0} :catch_4c1

    #@4a0
    .line 1420
    :cond_4a0
    :goto_4a0
    if-eqz v4, :cond_4ac

    #@4a2
    .line 1421
    const-string v8, "FeliCaDevice"

    #@4a4
    const-string v9, "Closing FeilCa-UART."

    #@4a6
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4a9
    .line 1424
    :try_start_4a9
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_4ac
    .catch Ljava/io/IOException; {:try_start_4a9 .. :try_end_4ac} :catch_4c6

    #@4ac
    .line 1430
    :cond_4ac
    :goto_4ac
    const-string v8, "FeliCaDevice"

    #@4ae
    const-string v9, "Set FeliCa OFF."

    #@4b0
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4b3
    .line 1432
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@4b6
    move-result v8

    #@4b7
    if-gez v8, :cond_4c0

    #@4b9
    .line 1433
    const-string v8, "FeliCaDevice"

    #@4bb
    const-string v9, "Set PON OFF failed"

    #@4bd
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4c0
    :cond_4c0
    throw v7

    #@4c1
    .line 1416
    :catch_4c1
    move-exception v0

    #@4c2
    .line 1417
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@4c5
    goto :goto_4a0

    #@4c6
    .line 1425
    .end local v0           #e:Ljava/io/IOException;
    :catch_4c6
    move-exception v0

    #@4c7
    .line 1426
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@4ca
    goto :goto_4ac

    #@4cb
    .line 1413
    .end local v0           #e:Ljava/io/IOException;
    .end local v2           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    :catchall_4cb
    move-exception v7

    #@4cc
    move-object v2, v3

    #@4cd
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v2       #fis_felica_uart:Ljava/io/FileInputStream;
    goto :goto_49b

    #@4ce
    .end local v2           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v4           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v5       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catchall_4ce
    move-exception v7

    #@4cf
    move-object v4, v5

    #@4d0
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v4       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v2, v3

    #@4d1
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v2       #fis_felica_uart:Ljava/io/FileInputStream;
    goto :goto_49b

    #@4d2
    .line 1406
    .end local v2           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    :catch_4d2
    move-exception v0

    #@4d3
    move-object v2, v3

    #@4d4
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v2       #fis_felica_uart:Ljava/io/FileInputStream;
    goto :goto_466

    #@4d5
    .end local v2           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v4           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v5       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_4d5
    move-exception v0

    #@4d6
    move-object v4, v5

    #@4d7
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v4       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v2, v3

    #@4d8
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v2       #fis_felica_uart:Ljava/io/FileInputStream;
    goto :goto_466

    #@4d9
    .line 1399
    .end local v2           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    :catch_4d9
    move-exception v0

    #@4da
    move-object v2, v3

    #@4db
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v2       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_42a

    #@4dd
    .end local v2           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v4           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v5       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_4dd
    move-exception v0

    #@4de
    move-object v4, v5

    #@4df
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v4       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v2, v3

    #@4e0
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v2       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_42a
.end method

.method private static getCalFreqRemainder()I
    .registers 9

    #@0
    .prologue
    .line 2063
    const/4 v1, 0x0

    #@1
    .line 2064
    .local v1, fis:Ljava/io/FileInputStream;
    const/4 v3, 0x0

    #@2
    .line 2066
    .local v3, freq_remainder:I
    const-string v4, "FeliCaDevice"

    #@4
    const-string v5, "getCalFreqRemainder()"

    #@6
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 2069
    :try_start_9
    new-instance v2, Ljava/io/FileInputStream;

    #@b
    const-string v4, "/felica/knife/remainder"

    #@d
    invoke-direct {v2, v4}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_10
    .catchall {:try_start_9 .. :try_end_10} :catchall_64
    .catch Ljava/io/FileNotFoundException; {:try_start_9 .. :try_end_10} :catch_36
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_10} :catch_4d

    #@10
    .line 2071
    .end local v1           #fis:Ljava/io/FileInputStream;
    .local v2, fis:Ljava/io/FileInputStream;
    :try_start_10
    invoke-virtual {v2}, Ljava/io/FileInputStream;->read()I

    #@13
    move-result v3

    #@14
    .line 2073
    const-string v4, "FeliCaDevice"

    #@16
    const-string v5, "freq_remainder : %d"

    #@18
    const/4 v6, 0x1

    #@19
    new-array v6, v6, [Ljava/lang/Object;

    #@1b
    const/4 v7, 0x0

    #@1c
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1f
    move-result-object v8

    #@20
    aput-object v8, v6, v7

    #@22
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@25
    move-result-object v5

    #@26
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_29
    .catchall {:try_start_10 .. :try_end_29} :catchall_70
    .catch Ljava/io/FileNotFoundException; {:try_start_10 .. :try_end_29} :catch_76
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_29} :catch_73

    #@29
    .line 2089
    if-eqz v2, :cond_2e

    #@2b
    .line 2090
    :try_start_2b
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_2e
    .catch Ljava/io/IOException; {:try_start_2b .. :try_end_2e} :catch_30

    #@2e
    :cond_2e
    move-object v1, v2

    #@2f
    .line 2096
    .end local v2           #fis:Ljava/io/FileInputStream;
    .restart local v1       #fis:Ljava/io/FileInputStream;
    :cond_2f
    :goto_2f
    return v3

    #@30
    .line 2091
    .end local v1           #fis:Ljava/io/FileInputStream;
    .restart local v2       #fis:Ljava/io/FileInputStream;
    :catch_30
    move-exception v0

    #@31
    .line 2092
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@34
    move-object v1, v2

    #@35
    .line 2094
    .end local v2           #fis:Ljava/io/FileInputStream;
    .restart local v1       #fis:Ljava/io/FileInputStream;
    goto :goto_2f

    #@36
    .line 2075
    .end local v0           #e:Ljava/io/IOException;
    :catch_36
    move-exception v0

    #@37
    .line 2077
    .local v0, e:Ljava/io/FileNotFoundException;
    :goto_37
    :try_start_37
    const-string v4, "FeliCaDevice"

    #@39
    const-string v5, "ERROR - Open felica cal data file"

    #@3b
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 2079
    const/4 v3, -0x1

    #@3f
    .line 2080
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_42
    .catchall {:try_start_37 .. :try_end_42} :catchall_64

    #@42
    .line 2089
    if-eqz v1, :cond_2f

    #@44
    .line 2090
    :try_start_44
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_47
    .catch Ljava/io/IOException; {:try_start_44 .. :try_end_47} :catch_48

    #@47
    goto :goto_2f

    #@48
    .line 2091
    :catch_48
    move-exception v0

    #@49
    .line 2092
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@4c
    goto :goto_2f

    #@4d
    .line 2081
    .end local v0           #e:Ljava/io/IOException;
    :catch_4d
    move-exception v0

    #@4e
    .line 2083
    .restart local v0       #e:Ljava/io/IOException;
    :goto_4e
    :try_start_4e
    const-string v4, "FeliCaDevice"

    #@50
    const-string v5, "ERROR - read felica cal data file"

    #@52
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@55
    .line 2085
    const/4 v3, -0x1

    #@56
    .line 2086
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_59
    .catchall {:try_start_4e .. :try_end_59} :catchall_64

    #@59
    .line 2089
    if-eqz v1, :cond_2f

    #@5b
    .line 2090
    :try_start_5b
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_5e
    .catch Ljava/io/IOException; {:try_start_5b .. :try_end_5e} :catch_5f

    #@5e
    goto :goto_2f

    #@5f
    .line 2091
    :catch_5f
    move-exception v0

    #@60
    .line 2092
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@63
    goto :goto_2f

    #@64
    .line 2088
    .end local v0           #e:Ljava/io/IOException;
    :catchall_64
    move-exception v4

    #@65
    .line 2089
    :goto_65
    if-eqz v1, :cond_6a

    #@67
    .line 2090
    :try_start_67
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_6a
    .catch Ljava/io/IOException; {:try_start_67 .. :try_end_6a} :catch_6b

    #@6a
    .line 2093
    :cond_6a
    :goto_6a
    throw v4

    #@6b
    .line 2091
    :catch_6b
    move-exception v0

    #@6c
    .line 2092
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@6f
    goto :goto_6a

    #@70
    .line 2088
    .end local v0           #e:Ljava/io/IOException;
    .end local v1           #fis:Ljava/io/FileInputStream;
    .restart local v2       #fis:Ljava/io/FileInputStream;
    :catchall_70
    move-exception v4

    #@71
    move-object v1, v2

    #@72
    .end local v2           #fis:Ljava/io/FileInputStream;
    .restart local v1       #fis:Ljava/io/FileInputStream;
    goto :goto_65

    #@73
    .line 2081
    .end local v1           #fis:Ljava/io/FileInputStream;
    .restart local v2       #fis:Ljava/io/FileInputStream;
    :catch_73
    move-exception v0

    #@74
    move-object v1, v2

    #@75
    .end local v2           #fis:Ljava/io/FileInputStream;
    .restart local v1       #fis:Ljava/io/FileInputStream;
    goto :goto_4e

    #@76
    .line 2075
    .end local v1           #fis:Ljava/io/FileInputStream;
    .restart local v2       #fis:Ljava/io/FileInputStream;
    :catch_76
    move-exception v0

    #@77
    move-object v1, v2

    #@78
    .end local v2           #fis:Ljava/io/FileInputStream;
    .restart local v1       #fis:Ljava/io/FileInputStream;
    goto :goto_37
.end method

.method private static get_calibration_idx(I)I
    .registers 10
    .parameter "sw_val"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/4 v7, 0x1

    #@2
    .line 1721
    const/4 v0, 0x0

    #@3
    .line 1722
    .local v0, bMatch:I
    const/4 v1, 0x0

    #@4
    .line 1723
    .local v1, i:I
    const/4 v2, 0x0

    #@5
    .line 1729
    .local v2, sw_index:I
    const-string v3, "FeliCaDevice"

    #@7
    const-string v4, "searching sw1 %d, sw2 %d"

    #@9
    const/4 v5, 0x2

    #@a
    new-array v5, v5, [Ljava/lang/Object;

    #@c
    and-int/lit8 v6, p0, 0x7

    #@e
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11
    move-result-object v6

    #@12
    aput-object v6, v5, v8

    #@14
    and-int/lit8 v6, p0, 0x38

    #@16
    shr-int/lit8 v6, v6, 0x3

    #@18
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1b
    move-result-object v6

    #@1c
    aput-object v6, v5, v7

    #@1e
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@21
    move-result-object v4

    #@22
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 1732
    const/4 v1, 0x0

    #@26
    :goto_26
    sget v3, Lcom/lge/systemservice/service/FeliCaDevice;->CAL_ENTRY_COUNT:I

    #@28
    if-ge v1, v3, :cond_56

    #@2a
    .line 1733
    sget-object v3, Lcom/lge/systemservice/service/FeliCaDevice;->RF_CALIBRATION_TABLE:[[I

    #@2c
    aget-object v3, v3, v1

    #@2e
    aget v3, v3, v8

    #@30
    and-int/lit8 v4, p0, 0x7

    #@32
    if-ne v3, v4, :cond_5b

    #@34
    sget-object v3, Lcom/lge/systemservice/service/FeliCaDevice;->RF_CALIBRATION_TABLE:[[I

    #@36
    aget-object v3, v3, v1

    #@38
    aget v3, v3, v7

    #@3a
    and-int/lit8 v4, p0, 0x38

    #@3c
    shr-int/lit8 v4, v4, 0x3

    #@3e
    if-ne v3, v4, :cond_5b

    #@40
    .line 1735
    const/4 v0, 0x1

    #@41
    .line 1736
    const-string v3, "FeliCaDevice"

    #@43
    const-string v4, "matching idx found at %d"

    #@45
    new-array v5, v7, [Ljava/lang/Object;

    #@47
    add-int/lit8 v6, v1, 0x1

    #@49
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4c
    move-result-object v6

    #@4d
    aput-object v6, v5, v8

    #@4f
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@52
    move-result-object v4

    #@53
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    .line 1742
    :cond_56
    if-ne v0, v7, :cond_5e

    #@58
    .line 1743
    add-int/lit8 v2, v1, 0x1

    #@5a
    .line 1749
    :goto_5a
    return v2

    #@5b
    .line 1732
    :cond_5b
    add-int/lit8 v1, v1, 0x1

    #@5d
    goto :goto_26

    #@5e
    .line 1745
    :cond_5e
    const/4 v2, 0x1

    #@5f
    goto :goto_5a
.end method

.method private static get_calibration_value(I)I
    .registers 8
    .parameter "sw_index"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 1778
    const/4 v1, 0x0

    #@3
    .line 1779
    .local v1, sw_val:I
    const/4 v0, 0x0

    #@4
    .line 1781
    .local v0, arr_index:I
    const-string v2, "FeliCaDevice"

    #@6
    const-string v3, "get_calibration_value"

    #@8
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 1783
    add-int/lit8 v0, p0, -0x1

    #@d
    .line 1785
    const-string v2, "FeliCaDevice"

    #@f
    new-instance v3, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v4, "sw_index : "

    #@16
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v3

    #@22
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 1786
    const-string v2, "FeliCaDevice"

    #@27
    new-instance v3, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v4, "arr_index : "

    #@2e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v3

    #@32
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35
    move-result-object v3

    #@36
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v3

    #@3a
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 1788
    if-gez v0, :cond_47

    #@3f
    .line 1790
    const-string v2, "FeliCaDevice"

    #@41
    const-string v3, "out of bounds"

    #@43
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    .line 1791
    const/4 v0, 0x0

    #@47
    .line 1794
    :cond_47
    sget v2, Lcom/lge/systemservice/service/FeliCaDevice;->CAL_ENTRY_COUNT:I

    #@49
    if-gt v2, v0, :cond_56

    #@4b
    .line 1796
    const-string v2, "FeliCaDevice"

    #@4d
    const-string v3, "out of bounds"

    #@4f
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 1797
    sget v2, Lcom/lge/systemservice/service/FeliCaDevice;->CAL_ENTRY_COUNT:I

    #@54
    add-int/lit8 v0, v2, -0x1

    #@56
    .line 1800
    :cond_56
    const-string v2, "FeliCaDevice"

    #@58
    new-instance v3, Ljava/lang/StringBuilder;

    #@5a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5d
    const-string v4, "table[0]-"

    #@5f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v3

    #@63
    sget-object v4, Lcom/lge/systemservice/service/FeliCaDevice;->RF_CALIBRATION_TABLE:[[I

    #@65
    aget-object v4, v4, v0

    #@67
    aget v4, v4, v5

    #@69
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v3

    #@6d
    const-string v4, ", table[1]-"

    #@6f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v3

    #@73
    sget-object v4, Lcom/lge/systemservice/service/FeliCaDevice;->RF_CALIBRATION_TABLE:[[I

    #@75
    aget-object v4, v4, v0

    #@77
    aget v4, v4, v6

    #@79
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v3

    #@7d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@80
    move-result-object v3

    #@81
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@84
    .line 1803
    sget-object v2, Lcom/lge/systemservice/service/FeliCaDevice;->RF_CALIBRATION_TABLE:[[I

    #@86
    aget-object v2, v2, v0

    #@88
    aget v2, v2, v5

    #@8a
    sget-object v3, Lcom/lge/systemservice/service/FeliCaDevice;->RF_CALIBRATION_TABLE:[[I

    #@8c
    aget-object v3, v3, v0

    #@8e
    aget v3, v3, v6

    #@90
    mul-int/lit8 v3, v3, 0x8

    #@92
    add-int v1, v2, v3

    #@94
    .line 1807
    const-string v2, "FeliCaDevice"

    #@96
    new-instance v3, Ljava/lang/StringBuilder;

    #@98
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@9b
    const-string v4, "sw_val : "

    #@9d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v3

    #@a1
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v3

    #@a5
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a8
    move-result-object v3

    #@a9
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ac
    .line 1809
    return v1
.end method

.method private static initRFRarameters()Z
    .registers 16

    #@0
    .prologue
    const/4 v15, 0x4

    #@1
    const/4 v9, 0x1

    #@2
    const/4 v8, 0x0

    #@3
    .line 760
    const/4 v7, 0x0

    #@4
    .line 761
    .local v7, uart_result_len:I
    const/16 v10, 0x400

    #@6
    new-array v2, v10, [B

    #@8
    .line 763
    .local v2, felica_uart_buff:[B
    const-string v10, "FeliCaDevice"

    #@a
    const-string v11, "Openning FeliCa-UART driver."

    #@c
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 765
    invoke-static {v2, v8}, Ljava/util/Arrays;->fill([BB)V

    #@12
    .line 767
    const/4 v3, 0x0

    #@13
    .line 768
    .local v3, fis_felica_uart:Ljava/io/FileInputStream;
    const/4 v5, 0x0

    #@14
    .line 772
    .local v5, fos_felica_uart:Ljava/io/FileOutputStream;
    :try_start_14
    new-instance v4, Ljava/io/FileInputStream;

    #@16
    const-string v10, "/dev/felica"

    #@18
    invoke-direct {v4, v10}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_1b
    .catchall {:try_start_14 .. :try_end_1b} :catchall_4bc
    .catch Ljava/io/FileNotFoundException; {:try_start_14 .. :try_end_1b} :catch_44b
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_1b} :catch_487

    #@1b
    .line 773
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .local v4, fis_felica_uart:Ljava/io/FileInputStream;
    :try_start_1b
    new-instance v6, Ljava/io/FileOutputStream;

    #@1d
    const-string v10, "/dev/felica"

    #@1f
    invoke-direct {v6, v10}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_22
    .catchall {:try_start_1b .. :try_end_22} :catchall_4ed
    .catch Ljava/io/FileNotFoundException; {:try_start_1b .. :try_end_22} :catch_4fb
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_22} :catch_4f4

    #@22
    .line 775
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .local v6, fos_felica_uart:Ljava/io/FileOutputStream;
    :try_start_22
    const-string v10, "FeliCaDevice"

    #@24
    const-string v11, "Set FeliCa ON."

    #@26
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 776
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_open()I

    #@2c
    move-result v10

    #@2d
    if-gez v10, :cond_68

    #@2f
    .line 777
    const-string v9, "FeliCaDevice"

    #@31
    const-string v10, "Set PON ON failed"

    #@33
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_36
    .catchall {:try_start_22 .. :try_end_36} :catchall_4f0
    .catch Ljava/io/FileNotFoundException; {:try_start_22 .. :try_end_36} :catch_4ff
    .catch Ljava/io/IOException; {:try_start_22 .. :try_end_36} :catch_4f7

    #@36
    .line 920
    if-eqz v4, :cond_3b

    #@38
    .line 922
    :try_start_38
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_3b
    .catch Ljava/io/IOException; {:try_start_38 .. :try_end_3b} :catch_5e

    #@3b
    .line 927
    :cond_3b
    :goto_3b
    if-eqz v6, :cond_47

    #@3d
    .line 928
    const-string v9, "FeliCaDevice"

    #@3f
    const-string v10, "Closing FeilCa-UART."

    #@41
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 931
    :try_start_44
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_47
    .catch Ljava/io/IOException; {:try_start_44 .. :try_end_47} :catch_63

    #@47
    .line 937
    :cond_47
    :goto_47
    const-string v9, "FeliCaDevice"

    #@49
    const-string v10, "Set FeliCa OFF."

    #@4b
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 939
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@51
    move-result v9

    #@52
    if-gez v9, :cond_5b

    #@54
    .line 940
    const-string v9, "FeliCaDevice"

    #@56
    const-string v10, "Set PON OFF failed"

    #@58
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5b
    :cond_5b
    move-object v5, v6

    #@5c
    .end local v6           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v5       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v3, v4

    #@5d
    .end local v4           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    :cond_5d
    :goto_5d
    return v8

    #@5e
    .line 923
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v4       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v6       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_5e
    move-exception v1

    #@5f
    .line 924
    .local v1, e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@62
    goto :goto_3b

    #@63
    .line 932
    .end local v1           #e:Ljava/io/IOException;
    :catch_63
    move-exception v1

    #@64
    .line 933
    .restart local v1       #e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@67
    goto :goto_47

    #@68
    .line 782
    .end local v1           #e:Ljava/io/IOException;
    :cond_68
    :try_start_68
    const-string v10, "FeliCaDevice"

    #@6a
    const-string v11, "Reading FeilCa UART port ready"

    #@6c
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6f
    .line 784
    const/4 v10, 0x0

    #@70
    sget v11, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_PORT_READY_LEN:I

    #@72
    invoke-virtual {v4, v2, v10, v11}, Ljava/io/FileInputStream;->read([BII)I

    #@75
    move-result v7

    #@76
    .line 787
    const-string v10, "FeliCaDevice"

    #@78
    const-string v11, "Port ready : 0x%x,0x%x,0x%x,0x%x"

    #@7a
    const/4 v12, 0x4

    #@7b
    new-array v12, v12, [Ljava/lang/Object;

    #@7d
    const/4 v13, 0x0

    #@7e
    const/4 v14, 0x0

    #@7f
    aget-byte v14, v2, v14

    #@81
    invoke-static {v14}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@84
    move-result-object v14

    #@85
    aput-object v14, v12, v13

    #@87
    const/4 v13, 0x1

    #@88
    const/4 v14, 0x1

    #@89
    aget-byte v14, v2, v14

    #@8b
    invoke-static {v14}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@8e
    move-result-object v14

    #@8f
    aput-object v14, v12, v13

    #@91
    const/4 v13, 0x2

    #@92
    const/4 v14, 0x2

    #@93
    aget-byte v14, v2, v14

    #@95
    invoke-static {v14}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@98
    move-result-object v14

    #@99
    aput-object v14, v12, v13

    #@9b
    const/4 v13, 0x3

    #@9c
    const/4 v14, 0x3

    #@9d
    aget-byte v14, v2, v14

    #@9f
    invoke-static {v14}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@a2
    move-result-object v14

    #@a3
    aput-object v14, v12, v13

    #@a5
    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@a8
    move-result-object v11

    #@a9
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ac
    .line 791
    sget v10, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_PORT_READY_LEN:I

    #@ae
    if-eq v7, v10, :cond_fb

    #@b0
    .line 792
    const-string v9, "FeliCaDevice"

    #@b2
    new-instance v10, Ljava/lang/StringBuilder;

    #@b4
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@b7
    const-string v11, "Read Length Mismatch : "

    #@b9
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v10

    #@bd
    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v10

    #@c1
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c4
    move-result-object v10

    #@c5
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c8
    .catchall {:try_start_68 .. :try_end_c8} :catchall_4f0
    .catch Ljava/io/FileNotFoundException; {:try_start_68 .. :try_end_c8} :catch_4ff
    .catch Ljava/io/IOException; {:try_start_68 .. :try_end_c8} :catch_4f7

    #@c8
    .line 920
    if-eqz v4, :cond_cd

    #@ca
    .line 922
    :try_start_ca
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_cd
    .catch Ljava/io/IOException; {:try_start_ca .. :try_end_cd} :catch_f1

    #@cd
    .line 927
    :cond_cd
    :goto_cd
    if-eqz v6, :cond_d9

    #@cf
    .line 928
    const-string v9, "FeliCaDevice"

    #@d1
    const-string v10, "Closing FeilCa-UART."

    #@d3
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d6
    .line 931
    :try_start_d6
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_d9
    .catch Ljava/io/IOException; {:try_start_d6 .. :try_end_d9} :catch_f6

    #@d9
    .line 937
    :cond_d9
    :goto_d9
    const-string v9, "FeliCaDevice"

    #@db
    const-string v10, "Set FeliCa OFF."

    #@dd
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e0
    .line 939
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@e3
    move-result v9

    #@e4
    if-gez v9, :cond_ed

    #@e6
    .line 940
    const-string v9, "FeliCaDevice"

    #@e8
    const-string v10, "Set PON OFF failed"

    #@ea
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ed
    :cond_ed
    move-object v5, v6

    #@ee
    .end local v6           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v5       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v3, v4

    #@ef
    .end local v4           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_5d

    #@f1
    .line 923
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v4       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v6       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_f1
    move-exception v1

    #@f2
    .line 924
    .restart local v1       #e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@f5
    goto :goto_cd

    #@f6
    .line 932
    .end local v1           #e:Ljava/io/IOException;
    :catch_f6
    move-exception v1

    #@f7
    .line 933
    .restart local v1       #e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@fa
    goto :goto_d9

    #@fb
    .line 797
    .end local v1           #e:Ljava/io/IOException;
    :cond_fb
    :try_start_fb
    sget v10, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_PORT_READY_LEN:I

    #@fd
    invoke-static {v2, v10}, Ljava/util/Arrays;->copyOf([BI)[B

    #@100
    move-result-object v10

    #@101
    sget-object v11, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_PORT_READY:[B

    #@103
    invoke-static {v10, v11}, Ljava/util/Arrays;->equals([B[B)Z

    #@106
    move-result v10

    #@107
    if-nez v10, :cond_143

    #@109
    .line 800
    const-string v9, "FeliCaDevice"

    #@10b
    const-string v10, "Port-Ready Mismatch!"

    #@10d
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_110
    .catchall {:try_start_fb .. :try_end_110} :catchall_4f0
    .catch Ljava/io/FileNotFoundException; {:try_start_fb .. :try_end_110} :catch_4ff
    .catch Ljava/io/IOException; {:try_start_fb .. :try_end_110} :catch_4f7

    #@110
    .line 920
    if-eqz v4, :cond_115

    #@112
    .line 922
    :try_start_112
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_115
    .catch Ljava/io/IOException; {:try_start_112 .. :try_end_115} :catch_139

    #@115
    .line 927
    :cond_115
    :goto_115
    if-eqz v6, :cond_121

    #@117
    .line 928
    const-string v9, "FeliCaDevice"

    #@119
    const-string v10, "Closing FeilCa-UART."

    #@11b
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@11e
    .line 931
    :try_start_11e
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_121
    .catch Ljava/io/IOException; {:try_start_11e .. :try_end_121} :catch_13e

    #@121
    .line 937
    :cond_121
    :goto_121
    const-string v9, "FeliCaDevice"

    #@123
    const-string v10, "Set FeliCa OFF."

    #@125
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@128
    .line 939
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@12b
    move-result v9

    #@12c
    if-gez v9, :cond_135

    #@12e
    .line 940
    const-string v9, "FeliCaDevice"

    #@130
    const-string v10, "Set PON OFF failed"

    #@132
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@135
    :cond_135
    move-object v5, v6

    #@136
    .end local v6           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v5       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v3, v4

    #@137
    .end local v4           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_5d

    #@139
    .line 923
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v4       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v6       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_139
    move-exception v1

    #@13a
    .line 924
    .restart local v1       #e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@13d
    goto :goto_115

    #@13e
    .line 932
    .end local v1           #e:Ljava/io/IOException;
    :catch_13e
    move-exception v1

    #@13f
    .line 933
    .restart local v1       #e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@142
    goto :goto_121

    #@143
    .line 815
    .end local v1           #e:Ljava/io/IOException;
    :cond_143
    :try_start_143
    sget-object v10, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT:[B

    #@145
    const/4 v11, 0x0

    #@146
    sget-object v12, Lcom/lge/systemservice/service/FeliCaDevice;->CMD_SET_RF_PARAM:[B

    #@148
    const/4 v13, 0x5

    #@149
    sget-object v14, Lcom/lge/systemservice/service/FeliCaDevice;->RF_PARAMETER_DEFAULT:[B

    #@14b
    array-length v14, v14

    #@14c
    invoke-static {v10, v11, v12, v13, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@14f
    .line 818
    sget-object v10, Lcom/lge/systemservice/service/FeliCaDevice;->CMD_SET_RF_PARAM:[B

    #@151
    const/4 v11, 0x2

    #@152
    const/16 v12, 0x21

    #@154
    invoke-static {v10, v11, v12}, Lcom/lge/systemservice/service/FeliCaDevice;->calcCRC16([BII)I

    #@157
    move-result v0

    #@158
    .line 820
    .local v0, crc:I
    sget-object v10, Lcom/lge/systemservice/service/FeliCaDevice;->CMD_SET_RF_PARAM:[B

    #@15a
    sget-object v11, Lcom/lge/systemservice/service/FeliCaDevice;->CMD_SET_RF_PARAM:[B

    #@15c
    array-length v11, v11

    #@15d
    add-int/lit8 v11, v11, -0x1

    #@15f
    add-int/lit8 v11, v11, -0x2

    #@161
    ushr-int/lit8 v12, v0, 0x8

    #@163
    int-to-byte v12, v12

    #@164
    aput-byte v12, v10, v11

    #@166
    .line 821
    sget-object v10, Lcom/lge/systemservice/service/FeliCaDevice;->CMD_SET_RF_PARAM:[B

    #@168
    sget-object v11, Lcom/lge/systemservice/service/FeliCaDevice;->CMD_SET_RF_PARAM:[B

    #@16a
    array-length v11, v11

    #@16b
    add-int/lit8 v11, v11, -0x1

    #@16d
    add-int/lit8 v11, v11, -0x1

    #@16f
    and-int/lit16 v12, v0, 0xff

    #@171
    int-to-byte v12, v12

    #@172
    aput-byte v12, v10, v11

    #@174
    .line 825
    const-string v10, "FeliCaDevice"

    #@176
    const-string v11, "Writing read RF registers cmd"

    #@178
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@17b
    .line 826
    sget-object v10, Lcom/lge/systemservice/service/FeliCaDevice;->CMD_SET_RF_PARAM:[B

    #@17d
    invoke-virtual {v6, v10}, Ljava/io/FileOutputStream;->write([B)V

    #@180
    .line 830
    const-string v10, "FeliCaDevice"

    #@182
    const-string v11, "Reading Ack packet."

    #@184
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@187
    .line 832
    const/4 v10, 0x0

    #@188
    sget v11, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_ACK_LEN:I

    #@18a
    invoke-virtual {v4, v2, v10, v11}, Ljava/io/FileInputStream;->read([BII)I

    #@18d
    move-result v7

    #@18e
    .line 835
    const-string v10, "FeliCaDevice"

    #@190
    const-string v11, "Ack : 0x%x,0x%x,0x%x,0x%x"

    #@192
    const/4 v12, 0x4

    #@193
    new-array v12, v12, [Ljava/lang/Object;

    #@195
    const/4 v13, 0x0

    #@196
    const/4 v14, 0x0

    #@197
    aget-byte v14, v2, v14

    #@199
    invoke-static {v14}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@19c
    move-result-object v14

    #@19d
    aput-object v14, v12, v13

    #@19f
    const/4 v13, 0x1

    #@1a0
    const/4 v14, 0x1

    #@1a1
    aget-byte v14, v2, v14

    #@1a3
    invoke-static {v14}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@1a6
    move-result-object v14

    #@1a7
    aput-object v14, v12, v13

    #@1a9
    const/4 v13, 0x2

    #@1aa
    const/4 v14, 0x2

    #@1ab
    aget-byte v14, v2, v14

    #@1ad
    invoke-static {v14}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@1b0
    move-result-object v14

    #@1b1
    aput-object v14, v12, v13

    #@1b3
    const/4 v13, 0x3

    #@1b4
    const/4 v14, 0x3

    #@1b5
    aget-byte v14, v2, v14

    #@1b7
    invoke-static {v14}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@1ba
    move-result-object v14

    #@1bb
    aput-object v14, v12, v13

    #@1bd
    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@1c0
    move-result-object v11

    #@1c1
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c4
    .line 839
    sget v10, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_ACK_LEN:I

    #@1c6
    if-eq v7, v10, :cond_213

    #@1c8
    .line 840
    const-string v9, "FeliCaDevice"

    #@1ca
    new-instance v10, Ljava/lang/StringBuilder;

    #@1cc
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@1cf
    const-string v11, "Read Length Mismatch : "

    #@1d1
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d4
    move-result-object v10

    #@1d5
    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d8
    move-result-object v10

    #@1d9
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1dc
    move-result-object v10

    #@1dd
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1e0
    .catchall {:try_start_143 .. :try_end_1e0} :catchall_4f0
    .catch Ljava/io/FileNotFoundException; {:try_start_143 .. :try_end_1e0} :catch_4ff
    .catch Ljava/io/IOException; {:try_start_143 .. :try_end_1e0} :catch_4f7

    #@1e0
    .line 920
    if-eqz v4, :cond_1e5

    #@1e2
    .line 922
    :try_start_1e2
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_1e5
    .catch Ljava/io/IOException; {:try_start_1e2 .. :try_end_1e5} :catch_209

    #@1e5
    .line 927
    :cond_1e5
    :goto_1e5
    if-eqz v6, :cond_1f1

    #@1e7
    .line 928
    const-string v9, "FeliCaDevice"

    #@1e9
    const-string v10, "Closing FeilCa-UART."

    #@1eb
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1ee
    .line 931
    :try_start_1ee
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_1f1
    .catch Ljava/io/IOException; {:try_start_1ee .. :try_end_1f1} :catch_20e

    #@1f1
    .line 937
    :cond_1f1
    :goto_1f1
    const-string v9, "FeliCaDevice"

    #@1f3
    const-string v10, "Set FeliCa OFF."

    #@1f5
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f8
    .line 939
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@1fb
    move-result v9

    #@1fc
    if-gez v9, :cond_205

    #@1fe
    .line 940
    const-string v9, "FeliCaDevice"

    #@200
    const-string v10, "Set PON OFF failed"

    #@202
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@205
    :cond_205
    move-object v5, v6

    #@206
    .end local v6           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v5       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v3, v4

    #@207
    .end local v4           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_5d

    #@209
    .line 923
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v4       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v6       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_209
    move-exception v1

    #@20a
    .line 924
    .restart local v1       #e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@20d
    goto :goto_1e5

    #@20e
    .line 932
    .end local v1           #e:Ljava/io/IOException;
    :catch_20e
    move-exception v1

    #@20f
    .line 933
    .restart local v1       #e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@212
    goto :goto_1f1

    #@213
    .line 845
    .end local v1           #e:Ljava/io/IOException;
    :cond_213
    :try_start_213
    sget v10, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_ACK_LEN:I

    #@215
    invoke-static {v2, v10}, Ljava/util/Arrays;->copyOf([BI)[B

    #@218
    move-result-object v10

    #@219
    sget-object v11, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_ACK:[B

    #@21b
    invoke-static {v10, v11}, Ljava/util/Arrays;->equals([B[B)Z

    #@21e
    move-result v10

    #@21f
    if-nez v10, :cond_25b

    #@221
    .line 847
    const-string v9, "FeliCaDevice"

    #@223
    const-string v10, "Ack Mismatch!"

    #@225
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_228
    .catchall {:try_start_213 .. :try_end_228} :catchall_4f0
    .catch Ljava/io/FileNotFoundException; {:try_start_213 .. :try_end_228} :catch_4ff
    .catch Ljava/io/IOException; {:try_start_213 .. :try_end_228} :catch_4f7

    #@228
    .line 920
    if-eqz v4, :cond_22d

    #@22a
    .line 922
    :try_start_22a
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_22d
    .catch Ljava/io/IOException; {:try_start_22a .. :try_end_22d} :catch_251

    #@22d
    .line 927
    :cond_22d
    :goto_22d
    if-eqz v6, :cond_239

    #@22f
    .line 928
    const-string v9, "FeliCaDevice"

    #@231
    const-string v10, "Closing FeilCa-UART."

    #@233
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@236
    .line 931
    :try_start_236
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_239
    .catch Ljava/io/IOException; {:try_start_236 .. :try_end_239} :catch_256

    #@239
    .line 937
    :cond_239
    :goto_239
    const-string v9, "FeliCaDevice"

    #@23b
    const-string v10, "Set FeliCa OFF."

    #@23d
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@240
    .line 939
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@243
    move-result v9

    #@244
    if-gez v9, :cond_24d

    #@246
    .line 940
    const-string v9, "FeliCaDevice"

    #@248
    const-string v10, "Set PON OFF failed"

    #@24a
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24d
    :cond_24d
    move-object v5, v6

    #@24e
    .end local v6           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v5       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v3, v4

    #@24f
    .end local v4           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_5d

    #@251
    .line 923
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v4       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v6       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_251
    move-exception v1

    #@252
    .line 924
    .restart local v1       #e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@255
    goto :goto_22d

    #@256
    .line 932
    .end local v1           #e:Ljava/io/IOException;
    :catch_256
    move-exception v1

    #@257
    .line 933
    .restart local v1       #e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@25a
    goto :goto_239

    #@25b
    .line 852
    .end local v1           #e:Ljava/io/IOException;
    :cond_25b
    :try_start_25b
    const-string v10, "FeliCaDevice"

    #@25d
    const-string v11, "Reading set RF register result."

    #@25f
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@262
    .line 854
    const/4 v10, 0x0

    #@263
    const/16 v11, 0x27

    #@265
    invoke-virtual {v4, v2, v10, v11}, Ljava/io/FileInputStream;->read([BII)I

    #@268
    move-result v7

    #@269
    .line 863
    const-string v10, "FeliCaDevice"

    #@26b
    const-string v11, "response1 : 0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x"

    #@26d
    const/16 v12, 0x8

    #@26f
    new-array v12, v12, [Ljava/lang/Object;

    #@271
    const/4 v13, 0x0

    #@272
    const/4 v14, 0x0

    #@273
    aget-byte v14, v2, v14

    #@275
    invoke-static {v14}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@278
    move-result-object v14

    #@279
    aput-object v14, v12, v13

    #@27b
    const/4 v13, 0x1

    #@27c
    const/4 v14, 0x1

    #@27d
    aget-byte v14, v2, v14

    #@27f
    invoke-static {v14}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@282
    move-result-object v14

    #@283
    aput-object v14, v12, v13

    #@285
    const/4 v13, 0x2

    #@286
    const/4 v14, 0x2

    #@287
    aget-byte v14, v2, v14

    #@289
    invoke-static {v14}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@28c
    move-result-object v14

    #@28d
    aput-object v14, v12, v13

    #@28f
    const/4 v13, 0x3

    #@290
    const/4 v14, 0x3

    #@291
    aget-byte v14, v2, v14

    #@293
    invoke-static {v14}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@296
    move-result-object v14

    #@297
    aput-object v14, v12, v13

    #@299
    const/4 v13, 0x4

    #@29a
    const/4 v14, 0x4

    #@29b
    aget-byte v14, v2, v14

    #@29d
    invoke-static {v14}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@2a0
    move-result-object v14

    #@2a1
    aput-object v14, v12, v13

    #@2a3
    const/4 v13, 0x5

    #@2a4
    const/4 v14, 0x5

    #@2a5
    aget-byte v14, v2, v14

    #@2a7
    invoke-static {v14}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@2aa
    move-result-object v14

    #@2ab
    aput-object v14, v12, v13

    #@2ad
    const/4 v13, 0x6

    #@2ae
    const/4 v14, 0x6

    #@2af
    aget-byte v14, v2, v14

    #@2b1
    invoke-static {v14}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@2b4
    move-result-object v14

    #@2b5
    aput-object v14, v12, v13

    #@2b7
    const/4 v13, 0x7

    #@2b8
    const/4 v14, 0x7

    #@2b9
    aget-byte v14, v2, v14

    #@2bb
    invoke-static {v14}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@2be
    move-result-object v14

    #@2bf
    aput-object v14, v12, v13

    #@2c1
    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@2c4
    move-result-object v11

    #@2c5
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2c8
    .line 869
    const-string v10, "FeliCaDevice"

    #@2ca
    const-string v11, "response2 : 0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x"

    #@2cc
    const/16 v12, 0x8

    #@2ce
    new-array v12, v12, [Ljava/lang/Object;

    #@2d0
    const/4 v13, 0x0

    #@2d1
    const/16 v14, 0x8

    #@2d3
    aget-byte v14, v2, v14

    #@2d5
    invoke-static {v14}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@2d8
    move-result-object v14

    #@2d9
    aput-object v14, v12, v13

    #@2db
    const/4 v13, 0x1

    #@2dc
    const/16 v14, 0x9

    #@2de
    aget-byte v14, v2, v14

    #@2e0
    invoke-static {v14}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@2e3
    move-result-object v14

    #@2e4
    aput-object v14, v12, v13

    #@2e6
    const/4 v13, 0x2

    #@2e7
    const/16 v14, 0xa

    #@2e9
    aget-byte v14, v2, v14

    #@2eb
    invoke-static {v14}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@2ee
    move-result-object v14

    #@2ef
    aput-object v14, v12, v13

    #@2f1
    const/4 v13, 0x3

    #@2f2
    const/16 v14, 0xb

    #@2f4
    aget-byte v14, v2, v14

    #@2f6
    invoke-static {v14}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@2f9
    move-result-object v14

    #@2fa
    aput-object v14, v12, v13

    #@2fc
    const/4 v13, 0x4

    #@2fd
    const/16 v14, 0xc

    #@2ff
    aget-byte v14, v2, v14

    #@301
    invoke-static {v14}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@304
    move-result-object v14

    #@305
    aput-object v14, v12, v13

    #@307
    const/4 v13, 0x5

    #@308
    const/16 v14, 0xd

    #@30a
    aget-byte v14, v2, v14

    #@30c
    invoke-static {v14}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@30f
    move-result-object v14

    #@310
    aput-object v14, v12, v13

    #@312
    const/4 v13, 0x6

    #@313
    const/16 v14, 0xe

    #@315
    aget-byte v14, v2, v14

    #@317
    invoke-static {v14}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@31a
    move-result-object v14

    #@31b
    aput-object v14, v12, v13

    #@31d
    const/4 v13, 0x7

    #@31e
    const/16 v14, 0xf

    #@320
    aget-byte v14, v2, v14

    #@322
    invoke-static {v14}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@325
    move-result-object v14

    #@326
    aput-object v14, v12, v13

    #@328
    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@32b
    move-result-object v11

    #@32c
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@32f
    .line 876
    const-string v10, "FeliCaDevice"

    #@331
    const-string v11, "Validating set RF register response packet."

    #@333
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@336
    .line 879
    const/16 v10, 0x9

    #@338
    if-eq v7, v10, :cond_385

    #@33a
    .line 880
    const-string v9, "FeliCaDevice"

    #@33c
    new-instance v10, Ljava/lang/StringBuilder;

    #@33e
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@341
    const-string v11, "Length Mismatch : "

    #@343
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@346
    move-result-object v10

    #@347
    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34a
    move-result-object v10

    #@34b
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34e
    move-result-object v10

    #@34f
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_352
    .catchall {:try_start_25b .. :try_end_352} :catchall_4f0
    .catch Ljava/io/FileNotFoundException; {:try_start_25b .. :try_end_352} :catch_4ff
    .catch Ljava/io/IOException; {:try_start_25b .. :try_end_352} :catch_4f7

    #@352
    .line 920
    if-eqz v4, :cond_357

    #@354
    .line 922
    :try_start_354
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_357
    .catch Ljava/io/IOException; {:try_start_354 .. :try_end_357} :catch_37b

    #@357
    .line 927
    :cond_357
    :goto_357
    if-eqz v6, :cond_363

    #@359
    .line 928
    const-string v9, "FeliCaDevice"

    #@35b
    const-string v10, "Closing FeilCa-UART."

    #@35d
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@360
    .line 931
    :try_start_360
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_363
    .catch Ljava/io/IOException; {:try_start_360 .. :try_end_363} :catch_380

    #@363
    .line 937
    :cond_363
    :goto_363
    const-string v9, "FeliCaDevice"

    #@365
    const-string v10, "Set FeliCa OFF."

    #@367
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@36a
    .line 939
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@36d
    move-result v9

    #@36e
    if-gez v9, :cond_377

    #@370
    .line 940
    const-string v9, "FeliCaDevice"

    #@372
    const-string v10, "Set PON OFF failed"

    #@374
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@377
    :cond_377
    move-object v5, v6

    #@378
    .end local v6           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v5       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v3, v4

    #@379
    .end local v4           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_5d

    #@37b
    .line 923
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v4       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v6       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_37b
    move-exception v1

    #@37c
    .line 924
    .restart local v1       #e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@37f
    goto :goto_357

    #@380
    .line 932
    .end local v1           #e:Ljava/io/IOException;
    :catch_380
    move-exception v1

    #@381
    .line 933
    .restart local v1       #e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@384
    goto :goto_363

    #@385
    .line 885
    .end local v1           #e:Ljava/io/IOException;
    :cond_385
    const/4 v10, 0x0

    #@386
    :try_start_386
    aget-byte v10, v2, v10

    #@388
    if-nez v10, :cond_395

    #@38a
    const/4 v10, 0x1

    #@38b
    aget-byte v10, v2, v10

    #@38d
    const/4 v11, -0x6

    #@38e
    if-ne v10, v11, :cond_395

    #@390
    const/4 v10, 0x2

    #@391
    aget-byte v10, v2, v10

    #@393
    if-eq v10, v15, :cond_3cf

    #@395
    .line 888
    :cond_395
    const-string v9, "FeliCaDevice"

    #@397
    const-string v10, "Header Mismatch!"

    #@399
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_39c
    .catchall {:try_start_386 .. :try_end_39c} :catchall_4f0
    .catch Ljava/io/FileNotFoundException; {:try_start_386 .. :try_end_39c} :catch_4ff
    .catch Ljava/io/IOException; {:try_start_386 .. :try_end_39c} :catch_4f7

    #@39c
    .line 920
    if-eqz v4, :cond_3a1

    #@39e
    .line 922
    :try_start_39e
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_3a1
    .catch Ljava/io/IOException; {:try_start_39e .. :try_end_3a1} :catch_3c5

    #@3a1
    .line 927
    :cond_3a1
    :goto_3a1
    if-eqz v6, :cond_3ad

    #@3a3
    .line 928
    const-string v9, "FeliCaDevice"

    #@3a5
    const-string v10, "Closing FeilCa-UART."

    #@3a7
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3aa
    .line 931
    :try_start_3aa
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_3ad
    .catch Ljava/io/IOException; {:try_start_3aa .. :try_end_3ad} :catch_3ca

    #@3ad
    .line 937
    :cond_3ad
    :goto_3ad
    const-string v9, "FeliCaDevice"

    #@3af
    const-string v10, "Set FeliCa OFF."

    #@3b1
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3b4
    .line 939
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@3b7
    move-result v9

    #@3b8
    if-gez v9, :cond_3c1

    #@3ba
    .line 940
    const-string v9, "FeliCaDevice"

    #@3bc
    const-string v10, "Set PON OFF failed"

    #@3be
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3c1
    :cond_3c1
    move-object v5, v6

    #@3c2
    .end local v6           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v5       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v3, v4

    #@3c3
    .end local v4           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_5d

    #@3c5
    .line 923
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v4       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v6       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_3c5
    move-exception v1

    #@3c6
    .line 924
    .restart local v1       #e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@3c9
    goto :goto_3a1

    #@3ca
    .line 932
    .end local v1           #e:Ljava/io/IOException;
    :catch_3ca
    move-exception v1

    #@3cb
    .line 933
    .restart local v1       #e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@3ce
    goto :goto_3ad

    #@3cf
    .line 895
    .end local v1           #e:Ljava/io/IOException;
    :cond_3cf
    const/4 v10, 0x3

    #@3d0
    const/4 v11, 0x6

    #@3d1
    :try_start_3d1
    invoke-static {v2, v10, v11}, Ljava/util/Arrays;->copyOfRange([BII)[B

    #@3d4
    move-result-object v10

    #@3d5
    sget-object v11, Lcom/lge/systemservice/service/FeliCaDevice;->RES_SET_RF_REG:[B

    #@3d7
    invoke-static {v10, v11}, Ljava/util/Arrays;->equals([B[B)Z

    #@3da
    move-result v10

    #@3db
    if-nez v10, :cond_417

    #@3dd
    .line 897
    const-string v9, "FeliCaDevice"

    #@3df
    const-string v10, "Response Code Mismatch!"

    #@3e1
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3e4
    .catchall {:try_start_3d1 .. :try_end_3e4} :catchall_4f0
    .catch Ljava/io/FileNotFoundException; {:try_start_3d1 .. :try_end_3e4} :catch_4ff
    .catch Ljava/io/IOException; {:try_start_3d1 .. :try_end_3e4} :catch_4f7

    #@3e4
    .line 920
    if-eqz v4, :cond_3e9

    #@3e6
    .line 922
    :try_start_3e6
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_3e9
    .catch Ljava/io/IOException; {:try_start_3e6 .. :try_end_3e9} :catch_40d

    #@3e9
    .line 927
    :cond_3e9
    :goto_3e9
    if-eqz v6, :cond_3f5

    #@3eb
    .line 928
    const-string v9, "FeliCaDevice"

    #@3ed
    const-string v10, "Closing FeilCa-UART."

    #@3ef
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3f2
    .line 931
    :try_start_3f2
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_3f5
    .catch Ljava/io/IOException; {:try_start_3f2 .. :try_end_3f5} :catch_412

    #@3f5
    .line 937
    :cond_3f5
    :goto_3f5
    const-string v9, "FeliCaDevice"

    #@3f7
    const-string v10, "Set FeliCa OFF."

    #@3f9
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3fc
    .line 939
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@3ff
    move-result v9

    #@400
    if-gez v9, :cond_409

    #@402
    .line 940
    const-string v9, "FeliCaDevice"

    #@404
    const-string v10, "Set PON OFF failed"

    #@406
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@409
    :cond_409
    move-object v5, v6

    #@40a
    .end local v6           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v5       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v3, v4

    #@40b
    .end local v4           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_5d

    #@40d
    .line 923
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v4       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v6       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_40d
    move-exception v1

    #@40e
    .line 924
    .restart local v1       #e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@411
    goto :goto_3e9

    #@412
    .line 932
    .end local v1           #e:Ljava/io/IOException;
    :catch_412
    move-exception v1

    #@413
    .line 933
    .restart local v1       #e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@416
    goto :goto_3f5

    #@417
    .line 920
    .end local v1           #e:Ljava/io/IOException;
    :cond_417
    if-eqz v4, :cond_41c

    #@419
    .line 922
    :try_start_419
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_41c
    .catch Ljava/io/IOException; {:try_start_419 .. :try_end_41c} :catch_441

    #@41c
    .line 927
    :cond_41c
    :goto_41c
    if-eqz v6, :cond_428

    #@41e
    .line 928
    const-string v8, "FeliCaDevice"

    #@420
    const-string v10, "Closing FeilCa-UART."

    #@422
    invoke-static {v8, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@425
    .line 931
    :try_start_425
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_428
    .catch Ljava/io/IOException; {:try_start_425 .. :try_end_428} :catch_446

    #@428
    .line 937
    :cond_428
    :goto_428
    const-string v8, "FeliCaDevice"

    #@42a
    const-string v10, "Set FeliCa OFF."

    #@42c
    invoke-static {v8, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@42f
    .line 939
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@432
    move-result v8

    #@433
    if-gez v8, :cond_43c

    #@435
    .line 940
    const-string v8, "FeliCaDevice"

    #@437
    const-string v10, "Set PON OFF failed"

    #@439
    invoke-static {v8, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@43c
    :cond_43c
    move-object v5, v6

    #@43d
    .end local v6           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v5       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v3, v4

    #@43e
    .end local v4           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    move v8, v9

    #@43f
    goto/16 :goto_5d

    #@441
    .line 923
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v4       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v6       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_441
    move-exception v1

    #@442
    .line 924
    .restart local v1       #e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@445
    goto :goto_41c

    #@446
    .line 932
    .end local v1           #e:Ljava/io/IOException;
    :catch_446
    move-exception v1

    #@447
    .line 933
    .restart local v1       #e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@44a
    goto :goto_428

    #@44b
    .line 906
    .end local v0           #crc:I
    .end local v1           #e:Ljava/io/IOException;
    .end local v4           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v6           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v5       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_44b
    move-exception v1

    #@44c
    .line 907
    .local v1, e:Ljava/io/FileNotFoundException;
    :goto_44c
    :try_start_44c
    const-string v9, "FeliCaDevice"

    #@44e
    const-string v10, "Can\'t open FeliCa-UART !. Can\'t read IDM"

    #@450
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@453
    .line 909
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_456
    .catchall {:try_start_44c .. :try_end_456} :catchall_4bc

    #@456
    .line 920
    if-eqz v3, :cond_45b

    #@458
    .line 922
    :try_start_458
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_45b
    .catch Ljava/io/IOException; {:try_start_458 .. :try_end_45b} :catch_47d

    #@45b
    .line 927
    .end local v1           #e:Ljava/io/FileNotFoundException;
    :cond_45b
    :goto_45b
    if-eqz v5, :cond_467

    #@45d
    .line 928
    const-string v9, "FeliCaDevice"

    #@45f
    const-string v10, "Closing FeilCa-UART."

    #@461
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@464
    .line 931
    :try_start_464
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_467
    .catch Ljava/io/IOException; {:try_start_464 .. :try_end_467} :catch_482

    #@467
    .line 937
    :cond_467
    :goto_467
    const-string v9, "FeliCaDevice"

    #@469
    const-string v10, "Set FeliCa OFF."

    #@46b
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@46e
    .line 939
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@471
    move-result v9

    #@472
    if-gez v9, :cond_5d

    #@474
    .line 940
    const-string v9, "FeliCaDevice"

    #@476
    const-string v10, "Set PON OFF failed"

    #@478
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@47b
    goto/16 :goto_5d

    #@47d
    .line 923
    .restart local v1       #e:Ljava/io/FileNotFoundException;
    :catch_47d
    move-exception v1

    #@47e
    .line 924
    .local v1, e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@481
    goto :goto_45b

    #@482
    .line 932
    .end local v1           #e:Ljava/io/IOException;
    :catch_482
    move-exception v1

    #@483
    .line 933
    .restart local v1       #e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@486
    goto :goto_467

    #@487
    .line 913
    .end local v1           #e:Ljava/io/IOException;
    :catch_487
    move-exception v1

    #@488
    .line 915
    .restart local v1       #e:Ljava/io/IOException;
    :goto_488
    :try_start_488
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_48b
    .catchall {:try_start_488 .. :try_end_48b} :catchall_4bc

    #@48b
    .line 920
    if-eqz v3, :cond_490

    #@48d
    .line 922
    :try_start_48d
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_490
    .catch Ljava/io/IOException; {:try_start_48d .. :try_end_490} :catch_4b2

    #@490
    .line 927
    :cond_490
    :goto_490
    if-eqz v5, :cond_49c

    #@492
    .line 928
    const-string v9, "FeliCaDevice"

    #@494
    const-string v10, "Closing FeilCa-UART."

    #@496
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@499
    .line 931
    :try_start_499
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_49c
    .catch Ljava/io/IOException; {:try_start_499 .. :try_end_49c} :catch_4b7

    #@49c
    .line 937
    :cond_49c
    :goto_49c
    const-string v9, "FeliCaDevice"

    #@49e
    const-string v10, "Set FeliCa OFF."

    #@4a0
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4a3
    .line 939
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@4a6
    move-result v9

    #@4a7
    if-gez v9, :cond_5d

    #@4a9
    .line 940
    const-string v9, "FeliCaDevice"

    #@4ab
    const-string v10, "Set PON OFF failed"

    #@4ad
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4b0
    goto/16 :goto_5d

    #@4b2
    .line 923
    :catch_4b2
    move-exception v1

    #@4b3
    .line 924
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@4b6
    goto :goto_490

    #@4b7
    .line 932
    :catch_4b7
    move-exception v1

    #@4b8
    .line 933
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@4bb
    goto :goto_49c

    #@4bc
    .line 920
    .end local v1           #e:Ljava/io/IOException;
    :catchall_4bc
    move-exception v8

    #@4bd
    :goto_4bd
    if-eqz v3, :cond_4c2

    #@4bf
    .line 922
    :try_start_4bf
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_4c2
    .catch Ljava/io/IOException; {:try_start_4bf .. :try_end_4c2} :catch_4e3

    #@4c2
    .line 927
    :cond_4c2
    :goto_4c2
    if-eqz v5, :cond_4ce

    #@4c4
    .line 928
    const-string v9, "FeliCaDevice"

    #@4c6
    const-string v10, "Closing FeilCa-UART."

    #@4c8
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4cb
    .line 931
    :try_start_4cb
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_4ce
    .catch Ljava/io/IOException; {:try_start_4cb .. :try_end_4ce} :catch_4e8

    #@4ce
    .line 937
    :cond_4ce
    :goto_4ce
    const-string v9, "FeliCaDevice"

    #@4d0
    const-string v10, "Set FeliCa OFF."

    #@4d2
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4d5
    .line 939
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@4d8
    move-result v9

    #@4d9
    if-gez v9, :cond_4e2

    #@4db
    .line 940
    const-string v9, "FeliCaDevice"

    #@4dd
    const-string v10, "Set PON OFF failed"

    #@4df
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4e2
    :cond_4e2
    throw v8

    #@4e3
    .line 923
    :catch_4e3
    move-exception v1

    #@4e4
    .line 924
    .restart local v1       #e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@4e7
    goto :goto_4c2

    #@4e8
    .line 932
    .end local v1           #e:Ljava/io/IOException;
    :catch_4e8
    move-exception v1

    #@4e9
    .line 933
    .restart local v1       #e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@4ec
    goto :goto_4ce

    #@4ed
    .line 920
    .end local v1           #e:Ljava/io/IOException;
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v4       #fis_felica_uart:Ljava/io/FileInputStream;
    :catchall_4ed
    move-exception v8

    #@4ee
    move-object v3, v4

    #@4ef
    .end local v4           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    goto :goto_4bd

    #@4f0
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v4       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v6       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catchall_4f0
    move-exception v8

    #@4f1
    move-object v5, v6

    #@4f2
    .end local v6           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v5       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v3, v4

    #@4f3
    .end local v4           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    goto :goto_4bd

    #@4f4
    .line 913
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v4       #fis_felica_uart:Ljava/io/FileInputStream;
    :catch_4f4
    move-exception v1

    #@4f5
    move-object v3, v4

    #@4f6
    .end local v4           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    goto :goto_488

    #@4f7
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v4       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v6       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_4f7
    move-exception v1

    #@4f8
    move-object v5, v6

    #@4f9
    .end local v6           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v5       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v3, v4

    #@4fa
    .end local v4           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    goto :goto_488

    #@4fb
    .line 906
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v4       #fis_felica_uart:Ljava/io/FileInputStream;
    :catch_4fb
    move-exception v1

    #@4fc
    move-object v3, v4

    #@4fd
    .end local v4           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_44c

    #@4ff
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v4       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v6       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_4ff
    move-exception v1

    #@500
    move-object v5, v6

    #@501
    .end local v6           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v5       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v3, v4

    #@502
    .end local v4           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_44c
.end method

.method private static readRFRegisters([BI)Z
    .registers 16
    .parameter "rfRegs"
    .parameter "rfRegsBufSize"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 958
    const/4 v6, 0x0

    #@3
    .line 959
    .local v6, uart_result_len:I
    const/16 v9, 0x400

    #@5
    new-array v1, v9, [B

    #@7
    .line 961
    .local v1, felica_uart_buff:[B
    const-string v9, "FeliCaDevice"

    #@9
    const-string v10, "Openning FeliCa-UART driver."

    #@b
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 963
    invoke-static {v1, v7}, Ljava/util/Arrays;->fill([BB)V

    #@11
    .line 964
    invoke-static {p0, v7}, Ljava/util/Arrays;->fill([BB)V

    #@14
    .line 966
    const/4 v2, 0x0

    #@15
    .line 967
    .local v2, fis_felica_uart:Ljava/io/FileInputStream;
    const/4 v4, 0x0

    #@16
    .line 971
    .local v4, fos_felica_uart:Ljava/io/FileOutputStream;
    :try_start_16
    new-instance v3, Ljava/io/FileInputStream;

    #@18
    const-string v9, "/dev/felica"

    #@1a
    invoke-direct {v3, v9}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_1d
    .catchall {:try_start_16 .. :try_end_1d} :catchall_494
    .catch Ljava/io/FileNotFoundException; {:try_start_16 .. :try_end_1d} :catch_423
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_1d} :catch_45f

    #@1d
    .line 972
    .end local v2           #fis_felica_uart:Ljava/io/FileInputStream;
    .local v3, fis_felica_uart:Ljava/io/FileInputStream;
    :try_start_1d
    new-instance v5, Ljava/io/FileOutputStream;

    #@1f
    const-string v9, "/dev/felica"

    #@21
    invoke-direct {v5, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_24
    .catchall {:try_start_1d .. :try_end_24} :catchall_4c5
    .catch Ljava/io/FileNotFoundException; {:try_start_1d .. :try_end_24} :catch_4d3
    .catch Ljava/io/IOException; {:try_start_1d .. :try_end_24} :catch_4cc

    #@24
    .line 974
    .end local v4           #fos_felica_uart:Ljava/io/FileOutputStream;
    .local v5, fos_felica_uart:Ljava/io/FileOutputStream;
    :try_start_24
    const-string v9, "FeliCaDevice"

    #@26
    const-string v10, "Set FeliCa ON."

    #@28
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 975
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_open()I

    #@2e
    move-result v9

    #@2f
    if-gez v9, :cond_6a

    #@31
    .line 976
    const-string v8, "FeliCaDevice"

    #@33
    const-string v9, "Set PON ON failed"

    #@35
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_38
    .catchall {:try_start_24 .. :try_end_38} :catchall_4c8
    .catch Ljava/io/FileNotFoundException; {:try_start_24 .. :try_end_38} :catch_4d7
    .catch Ljava/io/IOException; {:try_start_24 .. :try_end_38} :catch_4cf

    #@38
    .line 1107
    if-eqz v3, :cond_3d

    #@3a
    .line 1109
    :try_start_3a
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_3d
    .catch Ljava/io/IOException; {:try_start_3a .. :try_end_3d} :catch_60

    #@3d
    .line 1114
    :cond_3d
    :goto_3d
    if-eqz v5, :cond_49

    #@3f
    .line 1115
    const-string v8, "FeliCaDevice"

    #@41
    const-string v9, "Closing FeilCa-UART."

    #@43
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    .line 1118
    :try_start_46
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_49
    .catch Ljava/io/IOException; {:try_start_46 .. :try_end_49} :catch_65

    #@49
    .line 1124
    :cond_49
    :goto_49
    const-string v8, "FeliCaDevice"

    #@4b
    const-string v9, "Set FeliCa OFF."

    #@4d
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@50
    .line 1126
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@53
    move-result v8

    #@54
    if-gez v8, :cond_5d

    #@56
    .line 1127
    const-string v8, "FeliCaDevice"

    #@58
    const-string v9, "Set PON OFF failed"

    #@5a
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    :cond_5d
    move-object v4, v5

    #@5e
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v4       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v2, v3

    #@5f
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v2       #fis_felica_uart:Ljava/io/FileInputStream;
    :cond_5f
    :goto_5f
    return v7

    #@60
    .line 1110
    .end local v2           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v4           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v5       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_60
    move-exception v0

    #@61
    .line 1111
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@64
    goto :goto_3d

    #@65
    .line 1119
    .end local v0           #e:Ljava/io/IOException;
    :catch_65
    move-exception v0

    #@66
    .line 1120
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@69
    goto :goto_49

    #@6a
    .line 981
    .end local v0           #e:Ljava/io/IOException;
    :cond_6a
    :try_start_6a
    const-string v9, "FeliCaDevice"

    #@6c
    const-string v10, "Reading FeilCa UART port ready"

    #@6e
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@71
    .line 983
    const/4 v9, 0x0

    #@72
    sget v10, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_PORT_READY_LEN:I

    #@74
    invoke-virtual {v3, v1, v9, v10}, Ljava/io/FileInputStream;->read([BII)I

    #@77
    move-result v6

    #@78
    .line 986
    const-string v9, "FeliCaDevice"

    #@7a
    const-string v10, "Port ready : 0x%x,0x%x,0x%x,0x%x"

    #@7c
    const/4 v11, 0x4

    #@7d
    new-array v11, v11, [Ljava/lang/Object;

    #@7f
    const/4 v12, 0x0

    #@80
    const/4 v13, 0x0

    #@81
    aget-byte v13, v1, v13

    #@83
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@86
    move-result-object v13

    #@87
    aput-object v13, v11, v12

    #@89
    const/4 v12, 0x1

    #@8a
    const/4 v13, 0x1

    #@8b
    aget-byte v13, v1, v13

    #@8d
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@90
    move-result-object v13

    #@91
    aput-object v13, v11, v12

    #@93
    const/4 v12, 0x2

    #@94
    const/4 v13, 0x2

    #@95
    aget-byte v13, v1, v13

    #@97
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@9a
    move-result-object v13

    #@9b
    aput-object v13, v11, v12

    #@9d
    const/4 v12, 0x3

    #@9e
    const/4 v13, 0x3

    #@9f
    aget-byte v13, v1, v13

    #@a1
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@a4
    move-result-object v13

    #@a5
    aput-object v13, v11, v12

    #@a7
    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@aa
    move-result-object v10

    #@ab
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ae
    .line 990
    sget v9, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_PORT_READY_LEN:I

    #@b0
    if-eq v6, v9, :cond_fd

    #@b2
    .line 991
    const-string v8, "FeliCaDevice"

    #@b4
    new-instance v9, Ljava/lang/StringBuilder;

    #@b6
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@b9
    const-string v10, "Read Length Mismatch : "

    #@bb
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v9

    #@bf
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v9

    #@c3
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c6
    move-result-object v9

    #@c7
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_ca
    .catchall {:try_start_6a .. :try_end_ca} :catchall_4c8
    .catch Ljava/io/FileNotFoundException; {:try_start_6a .. :try_end_ca} :catch_4d7
    .catch Ljava/io/IOException; {:try_start_6a .. :try_end_ca} :catch_4cf

    #@ca
    .line 1107
    if-eqz v3, :cond_cf

    #@cc
    .line 1109
    :try_start_cc
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_cf
    .catch Ljava/io/IOException; {:try_start_cc .. :try_end_cf} :catch_f3

    #@cf
    .line 1114
    :cond_cf
    :goto_cf
    if-eqz v5, :cond_db

    #@d1
    .line 1115
    const-string v8, "FeliCaDevice"

    #@d3
    const-string v9, "Closing FeilCa-UART."

    #@d5
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d8
    .line 1118
    :try_start_d8
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_db
    .catch Ljava/io/IOException; {:try_start_d8 .. :try_end_db} :catch_f8

    #@db
    .line 1124
    :cond_db
    :goto_db
    const-string v8, "FeliCaDevice"

    #@dd
    const-string v9, "Set FeliCa OFF."

    #@df
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e2
    .line 1126
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@e5
    move-result v8

    #@e6
    if-gez v8, :cond_ef

    #@e8
    .line 1127
    const-string v8, "FeliCaDevice"

    #@ea
    const-string v9, "Set PON OFF failed"

    #@ec
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ef
    :cond_ef
    move-object v4, v5

    #@f0
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v4       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v2, v3

    #@f1
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v2       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_5f

    #@f3
    .line 1110
    .end local v2           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v4           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v5       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_f3
    move-exception v0

    #@f4
    .line 1111
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@f7
    goto :goto_cf

    #@f8
    .line 1119
    .end local v0           #e:Ljava/io/IOException;
    :catch_f8
    move-exception v0

    #@f9
    .line 1120
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@fc
    goto :goto_db

    #@fd
    .line 996
    .end local v0           #e:Ljava/io/IOException;
    :cond_fd
    :try_start_fd
    sget v9, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_PORT_READY_LEN:I

    #@ff
    invoke-static {v1, v9}, Ljava/util/Arrays;->copyOf([BI)[B

    #@102
    move-result-object v9

    #@103
    sget-object v10, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_PORT_READY:[B

    #@105
    invoke-static {v9, v10}, Ljava/util/Arrays;->equals([B[B)Z

    #@108
    move-result v9

    #@109
    if-nez v9, :cond_145

    #@10b
    .line 999
    const-string v8, "FeliCaDevice"

    #@10d
    const-string v9, "Port-Ready Mismatch!"

    #@10f
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_112
    .catchall {:try_start_fd .. :try_end_112} :catchall_4c8
    .catch Ljava/io/FileNotFoundException; {:try_start_fd .. :try_end_112} :catch_4d7
    .catch Ljava/io/IOException; {:try_start_fd .. :try_end_112} :catch_4cf

    #@112
    .line 1107
    if-eqz v3, :cond_117

    #@114
    .line 1109
    :try_start_114
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_117
    .catch Ljava/io/IOException; {:try_start_114 .. :try_end_117} :catch_13b

    #@117
    .line 1114
    :cond_117
    :goto_117
    if-eqz v5, :cond_123

    #@119
    .line 1115
    const-string v8, "FeliCaDevice"

    #@11b
    const-string v9, "Closing FeilCa-UART."

    #@11d
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@120
    .line 1118
    :try_start_120
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_123
    .catch Ljava/io/IOException; {:try_start_120 .. :try_end_123} :catch_140

    #@123
    .line 1124
    :cond_123
    :goto_123
    const-string v8, "FeliCaDevice"

    #@125
    const-string v9, "Set FeliCa OFF."

    #@127
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@12a
    .line 1126
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@12d
    move-result v8

    #@12e
    if-gez v8, :cond_137

    #@130
    .line 1127
    const-string v8, "FeliCaDevice"

    #@132
    const-string v9, "Set PON OFF failed"

    #@134
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@137
    :cond_137
    move-object v4, v5

    #@138
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v4       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v2, v3

    #@139
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v2       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_5f

    #@13b
    .line 1110
    .end local v2           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v4           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v5       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_13b
    move-exception v0

    #@13c
    .line 1111
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@13f
    goto :goto_117

    #@140
    .line 1119
    .end local v0           #e:Ljava/io/IOException;
    :catch_140
    move-exception v0

    #@141
    .line 1120
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@144
    goto :goto_123

    #@145
    .line 1004
    .end local v0           #e:Ljava/io/IOException;
    :cond_145
    :try_start_145
    const-string v9, "FeliCaDevice"

    #@147
    const-string v10, "Writing read RF registers cmd"

    #@149
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@14c
    .line 1005
    sget-object v9, Lcom/lge/systemservice/service/FeliCaDevice;->CMD_READ_RF_REG:[B

    #@14e
    invoke-virtual {v5, v9}, Ljava/io/FileOutputStream;->write([B)V

    #@151
    .line 1009
    const-string v9, "FeliCaDevice"

    #@153
    const-string v10, "Reading Ack packet."

    #@155
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@158
    .line 1011
    const/4 v9, 0x0

    #@159
    sget v10, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_ACK_LEN:I

    #@15b
    invoke-virtual {v3, v1, v9, v10}, Ljava/io/FileInputStream;->read([BII)I

    #@15e
    move-result v6

    #@15f
    .line 1014
    const-string v9, "FeliCaDevice"

    #@161
    const-string v10, "Ack : 0x%x,0x%x,0x%x,0x%x"

    #@163
    const/4 v11, 0x4

    #@164
    new-array v11, v11, [Ljava/lang/Object;

    #@166
    const/4 v12, 0x0

    #@167
    const/4 v13, 0x0

    #@168
    aget-byte v13, v1, v13

    #@16a
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@16d
    move-result-object v13

    #@16e
    aput-object v13, v11, v12

    #@170
    const/4 v12, 0x1

    #@171
    const/4 v13, 0x1

    #@172
    aget-byte v13, v1, v13

    #@174
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@177
    move-result-object v13

    #@178
    aput-object v13, v11, v12

    #@17a
    const/4 v12, 0x2

    #@17b
    const/4 v13, 0x2

    #@17c
    aget-byte v13, v1, v13

    #@17e
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@181
    move-result-object v13

    #@182
    aput-object v13, v11, v12

    #@184
    const/4 v12, 0x3

    #@185
    const/4 v13, 0x3

    #@186
    aget-byte v13, v1, v13

    #@188
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@18b
    move-result-object v13

    #@18c
    aput-object v13, v11, v12

    #@18e
    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@191
    move-result-object v10

    #@192
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@195
    .line 1018
    sget v9, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_ACK_LEN:I

    #@197
    if-eq v6, v9, :cond_1e4

    #@199
    .line 1019
    const-string v8, "FeliCaDevice"

    #@19b
    new-instance v9, Ljava/lang/StringBuilder;

    #@19d
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@1a0
    const-string v10, "Read Length Mismatch : "

    #@1a2
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a5
    move-result-object v9

    #@1a6
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a9
    move-result-object v9

    #@1aa
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ad
    move-result-object v9

    #@1ae
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1b1
    .catchall {:try_start_145 .. :try_end_1b1} :catchall_4c8
    .catch Ljava/io/FileNotFoundException; {:try_start_145 .. :try_end_1b1} :catch_4d7
    .catch Ljava/io/IOException; {:try_start_145 .. :try_end_1b1} :catch_4cf

    #@1b1
    .line 1107
    if-eqz v3, :cond_1b6

    #@1b3
    .line 1109
    :try_start_1b3
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_1b6
    .catch Ljava/io/IOException; {:try_start_1b3 .. :try_end_1b6} :catch_1da

    #@1b6
    .line 1114
    :cond_1b6
    :goto_1b6
    if-eqz v5, :cond_1c2

    #@1b8
    .line 1115
    const-string v8, "FeliCaDevice"

    #@1ba
    const-string v9, "Closing FeilCa-UART."

    #@1bc
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1bf
    .line 1118
    :try_start_1bf
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_1c2
    .catch Ljava/io/IOException; {:try_start_1bf .. :try_end_1c2} :catch_1df

    #@1c2
    .line 1124
    :cond_1c2
    :goto_1c2
    const-string v8, "FeliCaDevice"

    #@1c4
    const-string v9, "Set FeliCa OFF."

    #@1c6
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c9
    .line 1126
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@1cc
    move-result v8

    #@1cd
    if-gez v8, :cond_1d6

    #@1cf
    .line 1127
    const-string v8, "FeliCaDevice"

    #@1d1
    const-string v9, "Set PON OFF failed"

    #@1d3
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d6
    :cond_1d6
    move-object v4, v5

    #@1d7
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v4       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v2, v3

    #@1d8
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v2       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_5f

    #@1da
    .line 1110
    .end local v2           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v4           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v5       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_1da
    move-exception v0

    #@1db
    .line 1111
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@1de
    goto :goto_1b6

    #@1df
    .line 1119
    .end local v0           #e:Ljava/io/IOException;
    :catch_1df
    move-exception v0

    #@1e0
    .line 1120
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@1e3
    goto :goto_1c2

    #@1e4
    .line 1024
    .end local v0           #e:Ljava/io/IOException;
    :cond_1e4
    :try_start_1e4
    sget v9, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_ACK_LEN:I

    #@1e6
    invoke-static {v1, v9}, Ljava/util/Arrays;->copyOf([BI)[B

    #@1e9
    move-result-object v9

    #@1ea
    sget-object v10, Lcom/lge/systemservice/service/FeliCaDevice;->PACKET_ACK:[B

    #@1ec
    invoke-static {v9, v10}, Ljava/util/Arrays;->equals([B[B)Z

    #@1ef
    move-result v9

    #@1f0
    if-nez v9, :cond_22c

    #@1f2
    .line 1026
    const-string v8, "FeliCaDevice"

    #@1f4
    const-string v9, "Ack Mismatch!"

    #@1f6
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1f9
    .catchall {:try_start_1e4 .. :try_end_1f9} :catchall_4c8
    .catch Ljava/io/FileNotFoundException; {:try_start_1e4 .. :try_end_1f9} :catch_4d7
    .catch Ljava/io/IOException; {:try_start_1e4 .. :try_end_1f9} :catch_4cf

    #@1f9
    .line 1107
    if-eqz v3, :cond_1fe

    #@1fb
    .line 1109
    :try_start_1fb
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_1fe
    .catch Ljava/io/IOException; {:try_start_1fb .. :try_end_1fe} :catch_222

    #@1fe
    .line 1114
    :cond_1fe
    :goto_1fe
    if-eqz v5, :cond_20a

    #@200
    .line 1115
    const-string v8, "FeliCaDevice"

    #@202
    const-string v9, "Closing FeilCa-UART."

    #@204
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@207
    .line 1118
    :try_start_207
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_20a
    .catch Ljava/io/IOException; {:try_start_207 .. :try_end_20a} :catch_227

    #@20a
    .line 1124
    :cond_20a
    :goto_20a
    const-string v8, "FeliCaDevice"

    #@20c
    const-string v9, "Set FeliCa OFF."

    #@20e
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@211
    .line 1126
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@214
    move-result v8

    #@215
    if-gez v8, :cond_21e

    #@217
    .line 1127
    const-string v8, "FeliCaDevice"

    #@219
    const-string v9, "Set PON OFF failed"

    #@21b
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@21e
    :cond_21e
    move-object v4, v5

    #@21f
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v4       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v2, v3

    #@220
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v2       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_5f

    #@222
    .line 1110
    .end local v2           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v4           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v5       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_222
    move-exception v0

    #@223
    .line 1111
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@226
    goto :goto_1fe

    #@227
    .line 1119
    .end local v0           #e:Ljava/io/IOException;
    :catch_227
    move-exception v0

    #@228
    .line 1120
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@22b
    goto :goto_20a

    #@22c
    .line 1031
    .end local v0           #e:Ljava/io/IOException;
    :cond_22c
    :try_start_22c
    const-string v9, "FeliCaDevice"

    #@22e
    const-string v10, "Reading RF register values."

    #@230
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@233
    .line 1033
    const/4 v9, 0x0

    #@234
    const/16 v10, 0x27

    #@236
    invoke-virtual {v3, v1, v9, v10}, Ljava/io/FileInputStream;->read([BII)I

    #@239
    move-result v6

    #@23a
    .line 1042
    const-string v9, "FeliCaDevice"

    #@23c
    const-string v10, "response1 : 0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x"

    #@23e
    const/16 v11, 0x8

    #@240
    new-array v11, v11, [Ljava/lang/Object;

    #@242
    const/4 v12, 0x0

    #@243
    const/4 v13, 0x0

    #@244
    aget-byte v13, v1, v13

    #@246
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@249
    move-result-object v13

    #@24a
    aput-object v13, v11, v12

    #@24c
    const/4 v12, 0x1

    #@24d
    const/4 v13, 0x1

    #@24e
    aget-byte v13, v1, v13

    #@250
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@253
    move-result-object v13

    #@254
    aput-object v13, v11, v12

    #@256
    const/4 v12, 0x2

    #@257
    const/4 v13, 0x2

    #@258
    aget-byte v13, v1, v13

    #@25a
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@25d
    move-result-object v13

    #@25e
    aput-object v13, v11, v12

    #@260
    const/4 v12, 0x3

    #@261
    const/4 v13, 0x3

    #@262
    aget-byte v13, v1, v13

    #@264
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@267
    move-result-object v13

    #@268
    aput-object v13, v11, v12

    #@26a
    const/4 v12, 0x4

    #@26b
    const/4 v13, 0x4

    #@26c
    aget-byte v13, v1, v13

    #@26e
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@271
    move-result-object v13

    #@272
    aput-object v13, v11, v12

    #@274
    const/4 v12, 0x5

    #@275
    const/4 v13, 0x5

    #@276
    aget-byte v13, v1, v13

    #@278
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@27b
    move-result-object v13

    #@27c
    aput-object v13, v11, v12

    #@27e
    const/4 v12, 0x6

    #@27f
    const/4 v13, 0x6

    #@280
    aget-byte v13, v1, v13

    #@282
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@285
    move-result-object v13

    #@286
    aput-object v13, v11, v12

    #@288
    const/4 v12, 0x7

    #@289
    const/4 v13, 0x7

    #@28a
    aget-byte v13, v1, v13

    #@28c
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@28f
    move-result-object v13

    #@290
    aput-object v13, v11, v12

    #@292
    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@295
    move-result-object v10

    #@296
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@299
    .line 1048
    const-string v9, "FeliCaDevice"

    #@29b
    const-string v10, "response2 : 0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x"

    #@29d
    const/16 v11, 0x8

    #@29f
    new-array v11, v11, [Ljava/lang/Object;

    #@2a1
    const/4 v12, 0x0

    #@2a2
    const/16 v13, 0x8

    #@2a4
    aget-byte v13, v1, v13

    #@2a6
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@2a9
    move-result-object v13

    #@2aa
    aput-object v13, v11, v12

    #@2ac
    const/4 v12, 0x1

    #@2ad
    const/16 v13, 0x9

    #@2af
    aget-byte v13, v1, v13

    #@2b1
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@2b4
    move-result-object v13

    #@2b5
    aput-object v13, v11, v12

    #@2b7
    const/4 v12, 0x2

    #@2b8
    const/16 v13, 0xa

    #@2ba
    aget-byte v13, v1, v13

    #@2bc
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@2bf
    move-result-object v13

    #@2c0
    aput-object v13, v11, v12

    #@2c2
    const/4 v12, 0x3

    #@2c3
    const/16 v13, 0xb

    #@2c5
    aget-byte v13, v1, v13

    #@2c7
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@2ca
    move-result-object v13

    #@2cb
    aput-object v13, v11, v12

    #@2cd
    const/4 v12, 0x4

    #@2ce
    const/16 v13, 0xc

    #@2d0
    aget-byte v13, v1, v13

    #@2d2
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@2d5
    move-result-object v13

    #@2d6
    aput-object v13, v11, v12

    #@2d8
    const/4 v12, 0x5

    #@2d9
    const/16 v13, 0xd

    #@2db
    aget-byte v13, v1, v13

    #@2dd
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@2e0
    move-result-object v13

    #@2e1
    aput-object v13, v11, v12

    #@2e3
    const/4 v12, 0x6

    #@2e4
    const/16 v13, 0xe

    #@2e6
    aget-byte v13, v1, v13

    #@2e8
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@2eb
    move-result-object v13

    #@2ec
    aput-object v13, v11, v12

    #@2ee
    const/4 v12, 0x7

    #@2ef
    const/16 v13, 0xf

    #@2f1
    aget-byte v13, v1, v13

    #@2f3
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@2f6
    move-result-object v13

    #@2f7
    aput-object v13, v11, v12

    #@2f9
    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@2fc
    move-result-object v10

    #@2fd
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@300
    .line 1055
    const-string v9, "FeliCaDevice"

    #@302
    const-string v10, "Validating read RF register response packet."

    #@304
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@307
    .line 1058
    const/16 v9, 0x27

    #@309
    if-eq v6, v9, :cond_356

    #@30b
    .line 1059
    const-string v8, "FeliCaDevice"

    #@30d
    new-instance v9, Ljava/lang/StringBuilder;

    #@30f
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@312
    const-string v10, "Length Mismatch : "

    #@314
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@317
    move-result-object v9

    #@318
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@31b
    move-result-object v9

    #@31c
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31f
    move-result-object v9

    #@320
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_323
    .catchall {:try_start_22c .. :try_end_323} :catchall_4c8
    .catch Ljava/io/FileNotFoundException; {:try_start_22c .. :try_end_323} :catch_4d7
    .catch Ljava/io/IOException; {:try_start_22c .. :try_end_323} :catch_4cf

    #@323
    .line 1107
    if-eqz v3, :cond_328

    #@325
    .line 1109
    :try_start_325
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_328
    .catch Ljava/io/IOException; {:try_start_325 .. :try_end_328} :catch_34c

    #@328
    .line 1114
    :cond_328
    :goto_328
    if-eqz v5, :cond_334

    #@32a
    .line 1115
    const-string v8, "FeliCaDevice"

    #@32c
    const-string v9, "Closing FeilCa-UART."

    #@32e
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@331
    .line 1118
    :try_start_331
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_334
    .catch Ljava/io/IOException; {:try_start_331 .. :try_end_334} :catch_351

    #@334
    .line 1124
    :cond_334
    :goto_334
    const-string v8, "FeliCaDevice"

    #@336
    const-string v9, "Set FeliCa OFF."

    #@338
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@33b
    .line 1126
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@33e
    move-result v8

    #@33f
    if-gez v8, :cond_348

    #@341
    .line 1127
    const-string v8, "FeliCaDevice"

    #@343
    const-string v9, "Set PON OFF failed"

    #@345
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@348
    :cond_348
    move-object v4, v5

    #@349
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v4       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v2, v3

    #@34a
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v2       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_5f

    #@34c
    .line 1110
    .end local v2           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v4           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v5       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_34c
    move-exception v0

    #@34d
    .line 1111
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@350
    goto :goto_328

    #@351
    .line 1119
    .end local v0           #e:Ljava/io/IOException;
    :catch_351
    move-exception v0

    #@352
    .line 1120
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@355
    goto :goto_334

    #@356
    .line 1066
    .end local v0           #e:Ljava/io/IOException;
    :cond_356
    const/4 v9, 0x0

    #@357
    :try_start_357
    aget-byte v9, v1, v9

    #@359
    if-nez v9, :cond_368

    #@35b
    const/4 v9, 0x1

    #@35c
    aget-byte v9, v1, v9

    #@35e
    const/4 v10, -0x6

    #@35f
    if-ne v9, v10, :cond_368

    #@361
    const/4 v9, 0x2

    #@362
    aget-byte v9, v1, v9

    #@364
    const/16 v10, 0x22

    #@366
    if-eq v9, v10, :cond_3a2

    #@368
    .line 1069
    :cond_368
    const-string v8, "FeliCaDevice"

    #@36a
    const-string v9, "Header Mismatch!"

    #@36c
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_36f
    .catchall {:try_start_357 .. :try_end_36f} :catchall_4c8
    .catch Ljava/io/FileNotFoundException; {:try_start_357 .. :try_end_36f} :catch_4d7
    .catch Ljava/io/IOException; {:try_start_357 .. :try_end_36f} :catch_4cf

    #@36f
    .line 1107
    if-eqz v3, :cond_374

    #@371
    .line 1109
    :try_start_371
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_374
    .catch Ljava/io/IOException; {:try_start_371 .. :try_end_374} :catch_398

    #@374
    .line 1114
    :cond_374
    :goto_374
    if-eqz v5, :cond_380

    #@376
    .line 1115
    const-string v8, "FeliCaDevice"

    #@378
    const-string v9, "Closing FeilCa-UART."

    #@37a
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@37d
    .line 1118
    :try_start_37d
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_380
    .catch Ljava/io/IOException; {:try_start_37d .. :try_end_380} :catch_39d

    #@380
    .line 1124
    :cond_380
    :goto_380
    const-string v8, "FeliCaDevice"

    #@382
    const-string v9, "Set FeliCa OFF."

    #@384
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@387
    .line 1126
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@38a
    move-result v8

    #@38b
    if-gez v8, :cond_394

    #@38d
    .line 1127
    const-string v8, "FeliCaDevice"

    #@38f
    const-string v9, "Set PON OFF failed"

    #@391
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@394
    :cond_394
    move-object v4, v5

    #@395
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v4       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v2, v3

    #@396
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v2       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_5f

    #@398
    .line 1110
    .end local v2           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v4           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v5       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_398
    move-exception v0

    #@399
    .line 1111
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@39c
    goto :goto_374

    #@39d
    .line 1119
    .end local v0           #e:Ljava/io/IOException;
    :catch_39d
    move-exception v0

    #@39e
    .line 1120
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@3a1
    goto :goto_380

    #@3a2
    .line 1076
    .end local v0           #e:Ljava/io/IOException;
    :cond_3a2
    const/4 v9, 0x3

    #@3a3
    const/4 v10, 0x6

    #@3a4
    :try_start_3a4
    invoke-static {v1, v9, v10}, Ljava/util/Arrays;->copyOfRange([BII)[B

    #@3a7
    move-result-object v9

    #@3a8
    sget-object v10, Lcom/lge/systemservice/service/FeliCaDevice;->RES_READ_RF_REG:[B

    #@3aa
    invoke-static {v9, v10}, Ljava/util/Arrays;->equals([B[B)Z

    #@3ad
    move-result v9

    #@3ae
    if-nez v9, :cond_3ea

    #@3b0
    .line 1078
    const-string v8, "FeliCaDevice"

    #@3b2
    const-string v9, "Response Code Mismatch!"

    #@3b4
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3b7
    .catchall {:try_start_3a4 .. :try_end_3b7} :catchall_4c8
    .catch Ljava/io/FileNotFoundException; {:try_start_3a4 .. :try_end_3b7} :catch_4d7
    .catch Ljava/io/IOException; {:try_start_3a4 .. :try_end_3b7} :catch_4cf

    #@3b7
    .line 1107
    if-eqz v3, :cond_3bc

    #@3b9
    .line 1109
    :try_start_3b9
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_3bc
    .catch Ljava/io/IOException; {:try_start_3b9 .. :try_end_3bc} :catch_3e0

    #@3bc
    .line 1114
    :cond_3bc
    :goto_3bc
    if-eqz v5, :cond_3c8

    #@3be
    .line 1115
    const-string v8, "FeliCaDevice"

    #@3c0
    const-string v9, "Closing FeilCa-UART."

    #@3c2
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3c5
    .line 1118
    :try_start_3c5
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_3c8
    .catch Ljava/io/IOException; {:try_start_3c5 .. :try_end_3c8} :catch_3e5

    #@3c8
    .line 1124
    :cond_3c8
    :goto_3c8
    const-string v8, "FeliCaDevice"

    #@3ca
    const-string v9, "Set FeliCa OFF."

    #@3cc
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3cf
    .line 1126
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@3d2
    move-result v8

    #@3d3
    if-gez v8, :cond_3dc

    #@3d5
    .line 1127
    const-string v8, "FeliCaDevice"

    #@3d7
    const-string v9, "Set PON OFF failed"

    #@3d9
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3dc
    :cond_3dc
    move-object v4, v5

    #@3dd
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v4       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v2, v3

    #@3de
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v2       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_5f

    #@3e0
    .line 1110
    .end local v2           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v4           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v5       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_3e0
    move-exception v0

    #@3e1
    .line 1111
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@3e4
    goto :goto_3bc

    #@3e5
    .line 1119
    .end local v0           #e:Ljava/io/IOException;
    :catch_3e5
    move-exception v0

    #@3e6
    .line 1120
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@3e9
    goto :goto_3c8

    #@3ea
    .line 1083
    .end local v0           #e:Ljava/io/IOException;
    :cond_3ea
    const/4 v9, 0x6

    #@3eb
    const/4 v10, 0x0

    #@3ec
    :try_start_3ec
    invoke-static {v1, v9, p0, v10, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_3ef
    .catchall {:try_start_3ec .. :try_end_3ef} :catchall_4c8
    .catch Ljava/io/FileNotFoundException; {:try_start_3ec .. :try_end_3ef} :catch_4d7
    .catch Ljava/io/IOException; {:try_start_3ec .. :try_end_3ef} :catch_4cf

    #@3ef
    .line 1107
    if-eqz v3, :cond_3f4

    #@3f1
    .line 1109
    :try_start_3f1
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_3f4
    .catch Ljava/io/IOException; {:try_start_3f1 .. :try_end_3f4} :catch_419

    #@3f4
    .line 1114
    :cond_3f4
    :goto_3f4
    if-eqz v5, :cond_400

    #@3f6
    .line 1115
    const-string v7, "FeliCaDevice"

    #@3f8
    const-string v9, "Closing FeilCa-UART."

    #@3fa
    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3fd
    .line 1118
    :try_start_3fd
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_400
    .catch Ljava/io/IOException; {:try_start_3fd .. :try_end_400} :catch_41e

    #@400
    .line 1124
    :cond_400
    :goto_400
    const-string v7, "FeliCaDevice"

    #@402
    const-string v9, "Set FeliCa OFF."

    #@404
    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@407
    .line 1126
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@40a
    move-result v7

    #@40b
    if-gez v7, :cond_414

    #@40d
    .line 1127
    const-string v7, "FeliCaDevice"

    #@40f
    const-string v9, "Set PON OFF failed"

    #@411
    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@414
    :cond_414
    move-object v4, v5

    #@415
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v4       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v2, v3

    #@416
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v2       #fis_felica_uart:Ljava/io/FileInputStream;
    move v7, v8

    #@417
    goto/16 :goto_5f

    #@419
    .line 1110
    .end local v2           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v4           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v5       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_419
    move-exception v0

    #@41a
    .line 1111
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@41d
    goto :goto_3f4

    #@41e
    .line 1119
    .end local v0           #e:Ljava/io/IOException;
    :catch_41e
    move-exception v0

    #@41f
    .line 1120
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@422
    goto :goto_400

    #@423
    .line 1093
    .end local v0           #e:Ljava/io/IOException;
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v2       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v4       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_423
    move-exception v0

    #@424
    .line 1094
    .local v0, e:Ljava/io/FileNotFoundException;
    :goto_424
    :try_start_424
    const-string v8, "FeliCaDevice"

    #@426
    const-string v9, "Can\'t open FeliCa-UART !. Can\'t read IDM"

    #@428
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@42b
    .line 1096
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_42e
    .catchall {:try_start_424 .. :try_end_42e} :catchall_494

    #@42e
    .line 1107
    if-eqz v2, :cond_433

    #@430
    .line 1109
    :try_start_430
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_433
    .catch Ljava/io/IOException; {:try_start_430 .. :try_end_433} :catch_455

    #@433
    .line 1114
    .end local v0           #e:Ljava/io/FileNotFoundException;
    :cond_433
    :goto_433
    if-eqz v4, :cond_43f

    #@435
    .line 1115
    const-string v8, "FeliCaDevice"

    #@437
    const-string v9, "Closing FeilCa-UART."

    #@439
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@43c
    .line 1118
    :try_start_43c
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_43f
    .catch Ljava/io/IOException; {:try_start_43c .. :try_end_43f} :catch_45a

    #@43f
    .line 1124
    :cond_43f
    :goto_43f
    const-string v8, "FeliCaDevice"

    #@441
    const-string v9, "Set FeliCa OFF."

    #@443
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@446
    .line 1126
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@449
    move-result v8

    #@44a
    if-gez v8, :cond_5f

    #@44c
    .line 1127
    const-string v8, "FeliCaDevice"

    #@44e
    const-string v9, "Set PON OFF failed"

    #@450
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@453
    goto/16 :goto_5f

    #@455
    .line 1110
    .restart local v0       #e:Ljava/io/FileNotFoundException;
    :catch_455
    move-exception v0

    #@456
    .line 1111
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@459
    goto :goto_433

    #@45a
    .line 1119
    .end local v0           #e:Ljava/io/IOException;
    :catch_45a
    move-exception v0

    #@45b
    .line 1120
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@45e
    goto :goto_43f

    #@45f
    .line 1100
    .end local v0           #e:Ljava/io/IOException;
    :catch_45f
    move-exception v0

    #@460
    .line 1102
    .restart local v0       #e:Ljava/io/IOException;
    :goto_460
    :try_start_460
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_463
    .catchall {:try_start_460 .. :try_end_463} :catchall_494

    #@463
    .line 1107
    if-eqz v2, :cond_468

    #@465
    .line 1109
    :try_start_465
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_468
    .catch Ljava/io/IOException; {:try_start_465 .. :try_end_468} :catch_48a

    #@468
    .line 1114
    :cond_468
    :goto_468
    if-eqz v4, :cond_474

    #@46a
    .line 1115
    const-string v8, "FeliCaDevice"

    #@46c
    const-string v9, "Closing FeilCa-UART."

    #@46e
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@471
    .line 1118
    :try_start_471
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_474
    .catch Ljava/io/IOException; {:try_start_471 .. :try_end_474} :catch_48f

    #@474
    .line 1124
    :cond_474
    :goto_474
    const-string v8, "FeliCaDevice"

    #@476
    const-string v9, "Set FeliCa OFF."

    #@478
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@47b
    .line 1126
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@47e
    move-result v8

    #@47f
    if-gez v8, :cond_5f

    #@481
    .line 1127
    const-string v8, "FeliCaDevice"

    #@483
    const-string v9, "Set PON OFF failed"

    #@485
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@488
    goto/16 :goto_5f

    #@48a
    .line 1110
    :catch_48a
    move-exception v0

    #@48b
    .line 1111
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@48e
    goto :goto_468

    #@48f
    .line 1119
    :catch_48f
    move-exception v0

    #@490
    .line 1120
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@493
    goto :goto_474

    #@494
    .line 1107
    .end local v0           #e:Ljava/io/IOException;
    :catchall_494
    move-exception v7

    #@495
    :goto_495
    if-eqz v2, :cond_49a

    #@497
    .line 1109
    :try_start_497
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_49a
    .catch Ljava/io/IOException; {:try_start_497 .. :try_end_49a} :catch_4bb

    #@49a
    .line 1114
    :cond_49a
    :goto_49a
    if-eqz v4, :cond_4a6

    #@49c
    .line 1115
    const-string v8, "FeliCaDevice"

    #@49e
    const-string v9, "Closing FeilCa-UART."

    #@4a0
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4a3
    .line 1118
    :try_start_4a3
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_4a6
    .catch Ljava/io/IOException; {:try_start_4a3 .. :try_end_4a6} :catch_4c0

    #@4a6
    .line 1124
    :cond_4a6
    :goto_4a6
    const-string v8, "FeliCaDevice"

    #@4a8
    const-string v9, "Set FeliCa OFF."

    #@4aa
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4ad
    .line 1126
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_pon_close()I

    #@4b0
    move-result v8

    #@4b1
    if-gez v8, :cond_4ba

    #@4b3
    .line 1127
    const-string v8, "FeliCaDevice"

    #@4b5
    const-string v9, "Set PON OFF failed"

    #@4b7
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4ba
    :cond_4ba
    throw v7

    #@4bb
    .line 1110
    :catch_4bb
    move-exception v0

    #@4bc
    .line 1111
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@4bf
    goto :goto_49a

    #@4c0
    .line 1119
    .end local v0           #e:Ljava/io/IOException;
    :catch_4c0
    move-exception v0

    #@4c1
    .line 1120
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@4c4
    goto :goto_4a6

    #@4c5
    .line 1107
    .end local v0           #e:Ljava/io/IOException;
    .end local v2           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    :catchall_4c5
    move-exception v7

    #@4c6
    move-object v2, v3

    #@4c7
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v2       #fis_felica_uart:Ljava/io/FileInputStream;
    goto :goto_495

    #@4c8
    .end local v2           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v4           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v5       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catchall_4c8
    move-exception v7

    #@4c9
    move-object v4, v5

    #@4ca
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v4       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v2, v3

    #@4cb
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v2       #fis_felica_uart:Ljava/io/FileInputStream;
    goto :goto_495

    #@4cc
    .line 1100
    .end local v2           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    :catch_4cc
    move-exception v0

    #@4cd
    move-object v2, v3

    #@4ce
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v2       #fis_felica_uart:Ljava/io/FileInputStream;
    goto :goto_460

    #@4cf
    .end local v2           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v4           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v5       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_4cf
    move-exception v0

    #@4d0
    move-object v4, v5

    #@4d1
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v4       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v2, v3

    #@4d2
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v2       #fis_felica_uart:Ljava/io/FileInputStream;
    goto :goto_460

    #@4d3
    .line 1093
    .end local v2           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    :catch_4d3
    move-exception v0

    #@4d4
    move-object v2, v3

    #@4d5
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v2       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_424

    #@4d7
    .end local v2           #fis_felica_uart:Ljava/io/FileInputStream;
    .end local v4           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v3       #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v5       #fos_felica_uart:Ljava/io/FileOutputStream;
    :catch_4d7
    move-exception v0

    #@4d8
    move-object v4, v5

    #@4d9
    .end local v5           #fos_felica_uart:Ljava/io/FileOutputStream;
    .restart local v4       #fos_felica_uart:Ljava/io/FileOutputStream;
    move-object v2, v3

    #@4da
    .end local v3           #fis_felica_uart:Ljava/io/FileInputStream;
    .restart local v2       #fis_felica_uart:Ljava/io/FileInputStream;
    goto/16 :goto_424
.end method

.method private static read_switch_idx()I
    .registers 7

    #@0
    .prologue
    .line 1753
    const/4 v0, 0x0

    #@1
    .line 1754
    .local v0, sw_index:I
    const/4 v1, 0x0

    #@2
    .line 1756
    .local v1, sw_val:I
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->read_switch_value()I

    #@5
    move-result v1

    #@6
    .line 1758
    if-gez v1, :cond_11

    #@8
    .line 1759
    const-string v2, "FeliCaDevice"

    #@a
    const-string v3, "read_switch_value failed"

    #@c
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 1761
    const/4 v0, -0x1

    #@10
    .line 1769
    .end local v0           #sw_index:I
    :goto_10
    return v0

    #@11
    .line 1764
    .restart local v0       #sw_index:I
    :cond_11
    invoke-static {v1}, Lcom/lge/systemservice/service/FeliCaDevice;->get_calibration_idx(I)I

    #@14
    move-result v0

    #@15
    .line 1766
    const-string v2, "FeliCaDevice"

    #@17
    const-string v3, "read result : reg - 0x%x, idx - %d"

    #@19
    const/4 v4, 0x2

    #@1a
    new-array v4, v4, [Ljava/lang/Object;

    #@1c
    const/4 v5, 0x0

    #@1d
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@20
    move-result-object v6

    #@21
    aput-object v6, v4, v5

    #@23
    const/4 v5, 0x1

    #@24
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@27
    move-result-object v6

    #@28
    aput-object v6, v4, v5

    #@2a
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@2d
    move-result-object v3

    #@2e
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    goto :goto_10
.end method

.method private static read_switch_value()I
    .registers 9

    #@0
    .prologue
    .line 1679
    const/4 v3, 0x0

    #@1
    .line 1680
    .local v3, sw_val:I
    const/4 v1, 0x0

    #@2
    .line 1684
    .local v1, fis:Ljava/io/FileInputStream;
    :try_start_2
    new-instance v2, Ljava/io/FileInputStream;

    #@4
    const-string v4, "/dev/felica_cal"

    #@6
    invoke-direct {v2, v4}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_9
    .catchall {:try_start_2 .. :try_end_9} :catchall_59
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_9} :catch_39
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_9} :catch_49

    #@9
    .line 1686
    .end local v1           #fis:Ljava/io/FileInputStream;
    .local v2, fis:Ljava/io/FileInputStream;
    :try_start_9
    invoke-virtual {v2}, Ljava/io/FileInputStream;->read()I

    #@c
    move-result v3

    #@d
    .line 1687
    const-string v4, "FeliCaDevice"

    #@f
    const-string v5, "felica_cal read byte result : 0x%x"

    #@11
    const/4 v6, 0x1

    #@12
    new-array v6, v6, [Ljava/lang/Object;

    #@14
    const/4 v7, 0x0

    #@15
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@18
    move-result-object v8

    #@19
    aput-object v8, v6, v7

    #@1b
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@1e
    move-result-object v5

    #@1f
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 1690
    if-gtz v3, :cond_2c

    #@24
    .line 1691
    const-string v4, "FeliCaDevice"

    #@26
    const-string v5, "read byte is not proper."

    #@28
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2b
    .catchall {:try_start_9 .. :try_end_2b} :catchall_65
    .catch Ljava/io/FileNotFoundException; {:try_start_9 .. :try_end_2b} :catch_6b
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_2b} :catch_68

    #@2b
    .line 1693
    const/4 v3, -0x1

    #@2c
    .line 1709
    :cond_2c
    if-eqz v2, :cond_31

    #@2e
    .line 1710
    :try_start_2e
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_31
    .catch Ljava/io/IOException; {:try_start_2e .. :try_end_31} :catch_33

    #@31
    :cond_31
    move-object v1, v2

    #@32
    .line 1716
    .end local v2           #fis:Ljava/io/FileInputStream;
    .restart local v1       #fis:Ljava/io/FileInputStream;
    :cond_32
    :goto_32
    return v3

    #@33
    .line 1711
    .end local v1           #fis:Ljava/io/FileInputStream;
    .restart local v2       #fis:Ljava/io/FileInputStream;
    :catch_33
    move-exception v0

    #@34
    .line 1712
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@37
    move-object v1, v2

    #@38
    .line 1714
    .end local v2           #fis:Ljava/io/FileInputStream;
    .restart local v1       #fis:Ljava/io/FileInputStream;
    goto :goto_32

    #@39
    .line 1696
    .end local v0           #e:Ljava/io/IOException;
    :catch_39
    move-exception v0

    #@3a
    .line 1698
    .local v0, e:Ljava/io/FileNotFoundException;
    :goto_3a
    const/4 v3, -0x1

    #@3b
    .line 1701
    :try_start_3b
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3e
    .catchall {:try_start_3b .. :try_end_3e} :catchall_59

    #@3e
    .line 1709
    if-eqz v1, :cond_32

    #@40
    .line 1710
    :try_start_40
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_43
    .catch Ljava/io/IOException; {:try_start_40 .. :try_end_43} :catch_44

    #@43
    goto :goto_32

    #@44
    .line 1711
    :catch_44
    move-exception v0

    #@45
    .line 1712
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@48
    goto :goto_32

    #@49
    .line 1702
    .end local v0           #e:Ljava/io/IOException;
    :catch_49
    move-exception v0

    #@4a
    .line 1704
    .restart local v0       #e:Ljava/io/IOException;
    :goto_4a
    const/4 v3, -0x1

    #@4b
    .line 1706
    :try_start_4b
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4e
    .catchall {:try_start_4b .. :try_end_4e} :catchall_59

    #@4e
    .line 1709
    if-eqz v1, :cond_32

    #@50
    .line 1710
    :try_start_50
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_53
    .catch Ljava/io/IOException; {:try_start_50 .. :try_end_53} :catch_54

    #@53
    goto :goto_32

    #@54
    .line 1711
    :catch_54
    move-exception v0

    #@55
    .line 1712
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@58
    goto :goto_32

    #@59
    .line 1708
    .end local v0           #e:Ljava/io/IOException;
    :catchall_59
    move-exception v4

    #@5a
    .line 1709
    :goto_5a
    if-eqz v1, :cond_5f

    #@5c
    .line 1710
    :try_start_5c
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_5f
    .catch Ljava/io/IOException; {:try_start_5c .. :try_end_5f} :catch_60

    #@5f
    .line 1713
    :cond_5f
    :goto_5f
    throw v4

    #@60
    .line 1711
    :catch_60
    move-exception v0

    #@61
    .line 1712
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@64
    goto :goto_5f

    #@65
    .line 1708
    .end local v0           #e:Ljava/io/IOException;
    .end local v1           #fis:Ljava/io/FileInputStream;
    .restart local v2       #fis:Ljava/io/FileInputStream;
    :catchall_65
    move-exception v4

    #@66
    move-object v1, v2

    #@67
    .end local v2           #fis:Ljava/io/FileInputStream;
    .restart local v1       #fis:Ljava/io/FileInputStream;
    goto :goto_5a

    #@68
    .line 1702
    .end local v1           #fis:Ljava/io/FileInputStream;
    .restart local v2       #fis:Ljava/io/FileInputStream;
    :catch_68
    move-exception v0

    #@69
    move-object v1, v2

    #@6a
    .end local v2           #fis:Ljava/io/FileInputStream;
    .restart local v1       #fis:Ljava/io/FileInputStream;
    goto :goto_4a

    #@6b
    .line 1696
    .end local v1           #fis:Ljava/io/FileInputStream;
    .restart local v2       #fis:Ljava/io/FileInputStream;
    :catch_6b
    move-exception v0

    #@6c
    move-object v1, v2

    #@6d
    .end local v2           #fis:Ljava/io/FileInputStream;
    .restart local v1       #fis:Ljava/io/FileInputStream;
    goto :goto_3a
.end method

.method private static setCalFilePermission()V
    .registers 4

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 2015
    new-instance v0, Ljava/io/File;

    #@4
    const-string v1, "/felica/knife/remainder"

    #@6
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@9
    .line 2018
    .local v0, file:Ljava/io/File;
    invoke-virtual {v0, v2, v3}, Ljava/io/File;->setReadable(ZZ)Z

    #@c
    .line 2019
    invoke-virtual {v0, v2, v2}, Ljava/io/File;->setWritable(ZZ)Z

    #@f
    .line 2020
    invoke-virtual {v0, v3}, Ljava/io/File;->setExecutable(Z)Z

    #@12
    .line 2021
    return-void
.end method

.method private static setCalFreqRemainder(I)I
    .registers 7
    .parameter "freq_remainder"

    #@0
    .prologue
    .line 2025
    const/4 v1, 0x0

    #@1
    .line 2027
    .local v1, fos:Ljava/io/FileOutputStream;
    const-string v3, "FeliCaDevice"

    #@3
    new-instance v4, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v5, "setCalFreqRemainder - "

    #@a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v4

    #@e
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v4

    #@12
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v4

    #@16
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 2030
    :try_start_19
    new-instance v2, Ljava/io/FileOutputStream;

    #@1b
    const-string v3, "/felica/knife/remainder"

    #@1d
    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_20
    .catchall {:try_start_19 .. :try_end_20} :catchall_68
    .catch Ljava/io/FileNotFoundException; {:try_start_19 .. :try_end_20} :catch_3a
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_20} :catch_51

    #@20
    .line 2032
    .end local v1           #fos:Ljava/io/FileOutputStream;
    .local v2, fos:Ljava/io/FileOutputStream;
    :try_start_20
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->setCalFilePermission()V

    #@23
    .line 2034
    invoke-virtual {v2, p0}, Ljava/io/FileOutputStream;->write(I)V

    #@26
    .line 2035
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    #@29
    move-result-object v3

    #@2a
    invoke-virtual {v3}, Ljava/io/FileDescriptor;->sync()V
    :try_end_2d
    .catchall {:try_start_20 .. :try_end_2d} :catchall_74
    .catch Ljava/io/FileNotFoundException; {:try_start_20 .. :try_end_2d} :catch_7a
    .catch Ljava/io/IOException; {:try_start_20 .. :try_end_2d} :catch_77

    #@2d
    .line 2051
    if-eqz v2, :cond_32

    #@2f
    .line 2052
    :try_start_2f
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_32
    .catch Ljava/io/IOException; {:try_start_2f .. :try_end_32} :catch_34

    #@32
    :cond_32
    move-object v1, v2

    #@33
    .line 2058
    .end local v2           #fos:Ljava/io/FileOutputStream;
    .restart local v1       #fos:Ljava/io/FileOutputStream;
    :cond_33
    :goto_33
    return p0

    #@34
    .line 2053
    .end local v1           #fos:Ljava/io/FileOutputStream;
    .restart local v2       #fos:Ljava/io/FileOutputStream;
    :catch_34
    move-exception v0

    #@35
    .line 2054
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@38
    move-object v1, v2

    #@39
    .line 2056
    .end local v2           #fos:Ljava/io/FileOutputStream;
    .restart local v1       #fos:Ljava/io/FileOutputStream;
    goto :goto_33

    #@3a
    .line 2037
    .end local v0           #e:Ljava/io/IOException;
    :catch_3a
    move-exception v0

    #@3b
    .line 2039
    .local v0, e:Ljava/io/FileNotFoundException;
    :goto_3b
    :try_start_3b
    const-string v3, "FeliCaDevice"

    #@3d
    const-string v4, "ERROR - Open felica cal data file"

    #@3f
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    .line 2041
    const/4 p0, -0x1

    #@43
    .line 2042
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_46
    .catchall {:try_start_3b .. :try_end_46} :catchall_68

    #@46
    .line 2051
    if-eqz v1, :cond_33

    #@48
    .line 2052
    :try_start_48
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4b
    .catch Ljava/io/IOException; {:try_start_48 .. :try_end_4b} :catch_4c

    #@4b
    goto :goto_33

    #@4c
    .line 2053
    :catch_4c
    move-exception v0

    #@4d
    .line 2054
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@50
    goto :goto_33

    #@51
    .line 2043
    .end local v0           #e:Ljava/io/IOException;
    :catch_51
    move-exception v0

    #@52
    .line 2045
    .restart local v0       #e:Ljava/io/IOException;
    :goto_52
    :try_start_52
    const-string v3, "FeliCaDevice"

    #@54
    const-string v4, "ERROR - write felica cal data file"

    #@56
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@59
    .line 2047
    const/4 p0, -0x1

    #@5a
    .line 2048
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5d
    .catchall {:try_start_52 .. :try_end_5d} :catchall_68

    #@5d
    .line 2051
    if-eqz v1, :cond_33

    #@5f
    .line 2052
    :try_start_5f
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_62
    .catch Ljava/io/IOException; {:try_start_5f .. :try_end_62} :catch_63

    #@62
    goto :goto_33

    #@63
    .line 2053
    :catch_63
    move-exception v0

    #@64
    .line 2054
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@67
    goto :goto_33

    #@68
    .line 2050
    .end local v0           #e:Ljava/io/IOException;
    :catchall_68
    move-exception v3

    #@69
    .line 2051
    :goto_69
    if-eqz v1, :cond_6e

    #@6b
    .line 2052
    :try_start_6b
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_6e
    .catch Ljava/io/IOException; {:try_start_6b .. :try_end_6e} :catch_6f

    #@6e
    .line 2055
    :cond_6e
    :goto_6e
    throw v3

    #@6f
    .line 2053
    :catch_6f
    move-exception v0

    #@70
    .line 2054
    .restart local v0       #e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@73
    goto :goto_6e

    #@74
    .line 2050
    .end local v0           #e:Ljava/io/IOException;
    .end local v1           #fos:Ljava/io/FileOutputStream;
    .restart local v2       #fos:Ljava/io/FileOutputStream;
    :catchall_74
    move-exception v3

    #@75
    move-object v1, v2

    #@76
    .end local v2           #fos:Ljava/io/FileOutputStream;
    .restart local v1       #fos:Ljava/io/FileOutputStream;
    goto :goto_69

    #@77
    .line 2043
    .end local v1           #fos:Ljava/io/FileOutputStream;
    .restart local v2       #fos:Ljava/io/FileOutputStream;
    :catch_77
    move-exception v0

    #@78
    move-object v1, v2

    #@79
    .end local v2           #fos:Ljava/io/FileOutputStream;
    .restart local v1       #fos:Ljava/io/FileOutputStream;
    goto :goto_52

    #@7a
    .line 2037
    .end local v1           #fos:Ljava/io/FileOutputStream;
    .restart local v2       #fos:Ljava/io/FileOutputStream;
    :catch_7a
    move-exception v0

    #@7b
    move-object v1, v2

    #@7c
    .end local v2           #fos:Ljava/io/FileOutputStream;
    .restart local v1       #fos:Ljava/io/FileOutputStream;
    goto :goto_3b
.end method

.method private static set_switch_by_idx(I)I
    .registers 4
    .parameter "sw_index"

    #@0
    .prologue
    .line 2006
    const-string v1, "FeliCaDevice"

    #@2
    const-string v2, "set_switch_by_idx"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2008
    invoke-static {p0}, Lcom/lge/systemservice/service/FeliCaDevice;->get_calibration_value(I)I

    #@a
    move-result v0

    #@b
    .line 2010
    .local v0, data:I
    invoke-static {v0}, Lcom/lge/systemservice/service/FeliCaDevice;->set_switch_by_val(I)I

    #@e
    move-result v1

    #@f
    return v1
.end method

.method private static set_switch_by_val(I)I
    .registers 8
    .parameter "data"

    #@0
    .prologue
    .line 1815
    const/4 v3, 0x0

    #@1
    .line 1828
    .local v3, result:I
    const/4 v1, 0x0

    #@2
    .line 1830
    .local v1, fos:Ljava/io/FileOutputStream;
    const-string v4, "FeliCaDevice"

    #@4
    const-string v5, "set_switch_by_val"

    #@6
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 1833
    :try_start_9
    new-instance v2, Ljava/io/FileOutputStream;

    #@b
    const-string v4, "/dev/felica_cal"

    #@d
    invoke-direct {v2, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_10
    .catchall {:try_start_9 .. :try_end_10} :catchall_de
    .catch Ljava/io/FileNotFoundException; {:try_start_9 .. :try_end_10} :catch_54
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_10} :catch_98

    #@10
    .line 1840
    .end local v1           #fos:Ljava/io/FileOutputStream;
    .local v2, fos:Ljava/io/FileOutputStream;
    :try_start_10
    const-string v4, "FeliCaDevice"

    #@12
    new-instance v5, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v6, "now writing cal value : "

    #@19
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v5

    #@1d
    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v5

    #@21
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v5

    #@25
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 1841
    invoke-virtual {v2, p0}, Ljava/io/FileOutputStream;->write(I)V
    :try_end_2b
    .catchall {:try_start_10 .. :try_end_2b} :catchall_106
    .catch Ljava/io/FileNotFoundException; {:try_start_10 .. :try_end_2b} :catch_10c
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_2b} :catch_109

    #@2b
    .line 1862
    :try_start_2b
    const-string v4, "FeliCaDevice"

    #@2d
    const-string v5, "close"

    #@2f
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 1863
    if-eqz v2, :cond_45

    #@34
    .line 1865
    const-string v4, "FeliCaDevice"

    #@36
    const-string v5, "now close\t\t!!!!!!!!!!!!!!!!!!!!!"

    #@38
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 1866
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    #@3e
    .line 1867
    const-string v4, "FeliCaDevice"

    #@40
    const-string v5, "closed"

    #@42
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_45
    .catch Ljava/io/IOException; {:try_start_2b .. :try_end_45} :catch_47

    #@45
    :cond_45
    move-object v1, v2

    #@46
    .line 1875
    .end local v2           #fos:Ljava/io/FileOutputStream;
    .restart local v1       #fos:Ljava/io/FileOutputStream;
    :cond_46
    :goto_46
    return v3

    #@47
    .line 1869
    .end local v1           #fos:Ljava/io/FileOutputStream;
    .restart local v2       #fos:Ljava/io/FileOutputStream;
    :catch_47
    move-exception v0

    #@48
    .line 1870
    .local v0, e:Ljava/io/IOException;
    const-string v4, "FeliCaDevice"

    #@4a
    const-string v5, "fuck close\t\t!!!!!!!!!!!!!!!!!!!!!"

    #@4c
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 1871
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@52
    move-object v1, v2

    #@53
    .line 1873
    .end local v2           #fos:Ljava/io/FileOutputStream;
    .restart local v1       #fos:Ljava/io/FileOutputStream;
    goto :goto_46

    #@54
    .line 1845
    .end local v0           #e:Ljava/io/IOException;
    :catch_54
    move-exception v0

    #@55
    .line 1846
    .local v0, e:Ljava/io/FileNotFoundException;
    :goto_55
    :try_start_55
    const-string v4, "FeliCaDevice"

    #@57
    new-instance v5, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    const-string v6, "file not exist"

    #@5e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v5

    #@62
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v5

    #@66
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v5

    #@6a
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6d
    .line 1848
    const/4 v3, -0x1

    #@6e
    .line 1851
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_71
    .catchall {:try_start_55 .. :try_end_71} :catchall_de

    #@71
    .line 1862
    :try_start_71
    const-string v4, "FeliCaDevice"

    #@73
    const-string v5, "close"

    #@75
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@78
    .line 1863
    if-eqz v1, :cond_46

    #@7a
    .line 1865
    const-string v4, "FeliCaDevice"

    #@7c
    const-string v5, "now close\t\t!!!!!!!!!!!!!!!!!!!!!"

    #@7e
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@81
    .line 1866
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    #@84
    .line 1867
    const-string v4, "FeliCaDevice"

    #@86
    const-string v5, "closed"

    #@88
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8b
    .catch Ljava/io/IOException; {:try_start_71 .. :try_end_8b} :catch_8c

    #@8b
    goto :goto_46

    #@8c
    .line 1869
    :catch_8c
    move-exception v0

    #@8d
    .line 1870
    .local v0, e:Ljava/io/IOException;
    const-string v4, "FeliCaDevice"

    #@8f
    const-string v5, "fuck close\t\t!!!!!!!!!!!!!!!!!!!!!"

    #@91
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@94
    .line 1871
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@97
    goto :goto_46

    #@98
    .line 1852
    .end local v0           #e:Ljava/io/IOException;
    :catch_98
    move-exception v0

    #@99
    .line 1853
    .restart local v0       #e:Ljava/io/IOException;
    :goto_99
    :try_start_99
    const-string v4, "FeliCaDevice"

    #@9b
    new-instance v5, Ljava/lang/StringBuilder;

    #@9d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@a0
    const-string v6, "IOException"

    #@a2
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v5

    #@a6
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v5

    #@aa
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ad
    move-result-object v5

    #@ae
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b1
    .line 1855
    const/4 v3, -0x1

    #@b2
    .line 1859
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_b5
    .catchall {:try_start_99 .. :try_end_b5} :catchall_de

    #@b5
    .line 1862
    :try_start_b5
    const-string v4, "FeliCaDevice"

    #@b7
    const-string v5, "close"

    #@b9
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@bc
    .line 1863
    if-eqz v1, :cond_46

    #@be
    .line 1865
    const-string v4, "FeliCaDevice"

    #@c0
    const-string v5, "now close\t\t!!!!!!!!!!!!!!!!!!!!!"

    #@c2
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c5
    .line 1866
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    #@c8
    .line 1867
    const-string v4, "FeliCaDevice"

    #@ca
    const-string v5, "closed"

    #@cc
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_cf
    .catch Ljava/io/IOException; {:try_start_b5 .. :try_end_cf} :catch_d1

    #@cf
    goto/16 :goto_46

    #@d1
    .line 1869
    :catch_d1
    move-exception v0

    #@d2
    .line 1870
    const-string v4, "FeliCaDevice"

    #@d4
    const-string v5, "fuck close\t\t!!!!!!!!!!!!!!!!!!!!!"

    #@d6
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d9
    .line 1871
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@dc
    goto/16 :goto_46

    #@de
    .line 1861
    .end local v0           #e:Ljava/io/IOException;
    :catchall_de
    move-exception v4

    #@df
    .line 1862
    :goto_df
    :try_start_df
    const-string v5, "FeliCaDevice"

    #@e1
    const-string v6, "close"

    #@e3
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e6
    .line 1863
    if-eqz v1, :cond_f9

    #@e8
    .line 1865
    const-string v5, "FeliCaDevice"

    #@ea
    const-string v6, "now close\t\t!!!!!!!!!!!!!!!!!!!!!"

    #@ec
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ef
    .line 1866
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    #@f2
    .line 1867
    const-string v5, "FeliCaDevice"

    #@f4
    const-string v6, "closed"

    #@f6
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_f9
    .catch Ljava/io/IOException; {:try_start_df .. :try_end_f9} :catch_fa

    #@f9
    .line 1872
    :cond_f9
    :goto_f9
    throw v4

    #@fa
    .line 1869
    :catch_fa
    move-exception v0

    #@fb
    .line 1870
    .restart local v0       #e:Ljava/io/IOException;
    const-string v5, "FeliCaDevice"

    #@fd
    const-string v6, "fuck close\t\t!!!!!!!!!!!!!!!!!!!!!"

    #@ff
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@102
    .line 1871
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@105
    goto :goto_f9

    #@106
    .line 1861
    .end local v0           #e:Ljava/io/IOException;
    .end local v1           #fos:Ljava/io/FileOutputStream;
    .restart local v2       #fos:Ljava/io/FileOutputStream;
    :catchall_106
    move-exception v4

    #@107
    move-object v1, v2

    #@108
    .end local v2           #fos:Ljava/io/FileOutputStream;
    .restart local v1       #fos:Ljava/io/FileOutputStream;
    goto :goto_df

    #@109
    .line 1852
    .end local v1           #fos:Ljava/io/FileOutputStream;
    .restart local v2       #fos:Ljava/io/FileOutputStream;
    :catch_109
    move-exception v0

    #@10a
    move-object v1, v2

    #@10b
    .end local v2           #fos:Ljava/io/FileOutputStream;
    .restart local v1       #fos:Ljava/io/FileOutputStream;
    goto :goto_99

    #@10c
    .line 1845
    .end local v1           #fos:Ljava/io/FileOutputStream;
    .restart local v2       #fos:Ljava/io/FileOutputStream;
    :catch_10c
    move-exception v0

    #@10d
    move-object v1, v2

    #@10e
    .end local v2           #fos:Ljava/io/FileOutputStream;
    .restart local v1       #fos:Ljava/io/FileOutputStream;
    goto/16 :goto_55
.end method


# virtual methods
.method public cmdEXTIDM([B)Z
    .registers 5
    .parameter "idm"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 473
    const/4 v0, 0x0

    #@2
    .line 475
    .local v0, result:I
    const/16 v2, 0x8

    #@4
    invoke-direct {p0, p1, v2}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_read_ext_idm([BI)I

    #@7
    move-result v0

    #@8
    .line 477
    if-ne v0, v1, :cond_b

    #@a
    .line 483
    :goto_a
    return v1

    #@b
    :cond_b
    const/4 v1, 0x0

    #@c
    goto :goto_a
.end method

.method public cmdFreqCalRange(Lcom/lge/systemservice/service/FeliCaDevice$FeliCaCmdResult;)Z
    .registers 8
    .parameter "cmdResult"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 615
    const-string v0, "%2.2f-%2.2f"

    #@3
    const/4 v1, 0x2

    #@4
    new-array v1, v1, [Ljava/lang/Object;

    #@6
    const/4 v2, 0x0

    #@7
    sget v3, Lcom/lge/systemservice/service/FeliCaDevice;->CALIBRATION_CENTER_FREQ:F

    #@9
    sget v4, Lcom/lge/systemservice/service/FeliCaDevice;->CALIBRATION_PASS_RANGE:F

    #@b
    sub-float/2addr v3, v4

    #@c
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@f
    move-result-object v3

    #@10
    aput-object v3, v1, v2

    #@12
    sget v2, Lcom/lge/systemservice/service/FeliCaDevice;->CALIBRATION_CENTER_FREQ:F

    #@14
    sget v3, Lcom/lge/systemservice/service/FeliCaDevice;->CALIBRATION_PASS_RANGE:F

    #@16
    add-float/2addr v2, v3

    #@17
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@1a
    move-result-object v2

    #@1b
    aput-object v2, v1, v5

    #@1d
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@20
    move-result-object v0

    #@21
    iput-object v0, p1, Lcom/lge/systemservice/service/FeliCaDevice$FeliCaCmdResult;->result:Ljava/lang/String;

    #@23
    .line 619
    return v5
.end method

.method public cmdFreqCalRead(Lcom/lge/systemservice/service/FeliCaDevice$FeliCaCmdResult;)Z
    .registers 12
    .parameter "cmdResult"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 586
    const/4 v1, 0x0

    #@3
    .line 589
    .local v1, freq_remainder_int:I
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->getCalFreqRemainder()I

    #@6
    move-result v1

    #@7
    .line 594
    if-gt v2, v1, :cond_31

    #@9
    const/16 v4, 0x63

    #@b
    if-gt v1, v4, :cond_31

    #@d
    .line 595
    const/4 v0, 0x0

    #@e
    .line 597
    .local v0, cal_freq:F
    const-string v4, "FeliCaDevice"

    #@10
    const-string v5, "saved freq_remainder is within normal range."

    #@12
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    .line 599
    const-wide/high16 v4, 0x402a

    #@17
    int-to-double v6, v1

    #@18
    const-wide v8, 0x3f847ae147ae147bL

    #@1d
    mul-double/2addr v6, v8

    #@1e
    add-double/2addr v4, v6

    #@1f
    double-to-float v0, v4

    #@20
    .line 601
    const-string v4, "%2.2f"

    #@22
    new-array v5, v2, [Ljava/lang/Object;

    #@24
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@27
    move-result-object v6

    #@28
    aput-object v6, v5, v3

    #@2a
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@2d
    move-result-object v3

    #@2e
    iput-object v3, p1, Lcom/lge/systemservice/service/FeliCaDevice$FeliCaCmdResult;->result:Ljava/lang/String;

    #@30
    .line 609
    .end local v0           #cal_freq:F
    :goto_30
    return v2

    #@31
    .line 607
    :cond_31
    const-string v2, "FeliCaDevice"

    #@33
    new-instance v4, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v5, "saved freq_remainder is invalid value : "

    #@3a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v4

    #@3e
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@41
    move-result-object v4

    #@42
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v4

    #@46
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@49
    move v2, v3

    #@4a
    .line 609
    goto :goto_30
.end method

.method public cmdFreqCalWrite(F)Z
    .registers 8
    .parameter "freq"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 545
    const/4 v0, 0x0

    #@3
    .line 547
    .local v0, cal_result:I
    invoke-direct {p0, p1}, Lcom/lge/systemservice/service/FeliCaDevice;->calibrate_frequency(F)I

    #@6
    move-result v0

    #@7
    .line 555
    if-ne v0, v2, :cond_33

    #@9
    .line 556
    const/4 v1, 0x0

    #@a
    .line 558
    .local v1, freq_cal:I
    const-string v3, "FeliCaDevice"

    #@c
    const-string v4, "FREQCALRESULT_CALOK"

    #@e
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 560
    const/high16 v3, 0x42c8

    #@13
    mul-float/2addr v3, p1

    #@14
    float-to-int v3, v3

    #@15
    add-int/lit16 v1, v3, -0x514

    #@17
    .line 562
    const-string v3, "FeliCaDevice"

    #@19
    new-instance v4, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v5, "frequency remainder is "

    #@20
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v4

    #@24
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@27
    move-result-object v4

    #@28
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v4

    #@2c
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 564
    invoke-static {v1}, Lcom/lge/systemservice/service/FeliCaDevice;->setCalFreqRemainder(I)I

    #@32
    .line 580
    .end local v1           #freq_cal:I
    :goto_32
    return v2

    #@33
    .line 568
    :cond_33
    if-nez v0, :cond_41

    #@35
    .line 569
    const-string v2, "FeliCaDevice"

    #@37
    const-string v4, "FREQCALRESULT_CALRETRY"

    #@39
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    .line 571
    invoke-static {v3}, Lcom/lge/systemservice/service/FeliCaDevice;->setCalFreqRemainder(I)I

    #@3f
    move v2, v3

    #@40
    .line 573
    goto :goto_32

    #@41
    .line 576
    :cond_41
    const-string v2, "FeliCaDevice"

    #@43
    const-string v4, "FREQCALRESULT_CALNG"

    #@45
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 578
    const/16 v2, 0xff

    #@4a
    invoke-static {v2}, Lcom/lge/systemservice/service/FeliCaDevice;->setCalFreqRemainder(I)I

    #@4d
    move v2, v3

    #@4e
    .line 580
    goto :goto_32
.end method

.method public cmdIDM([Ljava/lang/String;)Z
    .registers 11
    .parameter "response"

    #@0
    .prologue
    const/4 v8, 0x3

    #@1
    const/4 v7, 0x2

    #@2
    const/16 v5, 0x8

    #@4
    const/4 v2, 0x1

    #@5
    const/4 v3, 0x0

    #@6
    .line 448
    const/4 v1, 0x0

    #@7
    .line 449
    .local v1, result:I
    new-array v0, v5, [B

    #@9
    .line 451
    .local v0, idm:[B
    invoke-static {v0, v5}, Lcom/lge/systemservice/service/FeliCaDevice;->felica_read_idm([BI)I

    #@c
    move-result v1

    #@d
    .line 453
    if-ne v1, v2, :cond_62

    #@f
    .line 458
    const-string v4, "%02X%02X%02X%02X%02X%02X%02X%02X"

    #@11
    new-array v5, v5, [Ljava/lang/Object;

    #@13
    aget-byte v6, v0, v3

    #@15
    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@18
    move-result-object v6

    #@19
    aput-object v6, v5, v3

    #@1b
    aget-byte v6, v0, v2

    #@1d
    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@20
    move-result-object v6

    #@21
    aput-object v6, v5, v2

    #@23
    aget-byte v6, v0, v7

    #@25
    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@28
    move-result-object v6

    #@29
    aput-object v6, v5, v7

    #@2b
    aget-byte v6, v0, v8

    #@2d
    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@30
    move-result-object v6

    #@31
    aput-object v6, v5, v8

    #@33
    const/4 v6, 0x4

    #@34
    const/4 v7, 0x4

    #@35
    aget-byte v7, v0, v7

    #@37
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@3a
    move-result-object v7

    #@3b
    aput-object v7, v5, v6

    #@3d
    const/4 v6, 0x5

    #@3e
    const/4 v7, 0x5

    #@3f
    aget-byte v7, v0, v7

    #@41
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@44
    move-result-object v7

    #@45
    aput-object v7, v5, v6

    #@47
    const/4 v6, 0x6

    #@48
    const/4 v7, 0x6

    #@49
    aget-byte v7, v0, v7

    #@4b
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@4e
    move-result-object v7

    #@4f
    aput-object v7, v5, v6

    #@51
    const/4 v6, 0x7

    #@52
    const/4 v7, 0x7

    #@53
    aget-byte v7, v0, v7

    #@55
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@58
    move-result-object v7

    #@59
    aput-object v7, v5, v6

    #@5b
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@5e
    move-result-object v4

    #@5f
    aput-object v4, p1, v3

    #@61
    .line 466
    :goto_61
    return v2

    #@62
    :cond_62
    move v2, v3

    #@63
    goto :goto_61
.end method

.method public cmdRFIDCK()I
    .registers 10

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, -0x1

    #@2
    const/4 v2, 0x1

    #@3
    .line 419
    const/4 v1, 0x0

    #@4
    .line 420
    .local v1, result:I
    const/4 v0, 0x0

    #@5
    .line 422
    .local v0, freq_remainder_int:I
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->getCalFreqRemainder()I

    #@8
    move-result v0

    #@9
    .line 425
    const-string v5, "FeliCaDevice"

    #@b
    const-string v6, "freq_remainder : %d"

    #@d
    new-array v7, v2, [Ljava/lang/Object;

    #@f
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@12
    move-result-object v8

    #@13
    aput-object v8, v7, v4

    #@15
    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@18
    move-result-object v6

    #@19
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 427
    if-gt v2, v0, :cond_2a

    #@1e
    const/16 v5, 0x63

    #@20
    if-gt v0, v5, :cond_2a

    #@22
    .line 428
    const-string v3, "FeliCaDevice"

    #@24
    const-string v4, "cal done."

    #@26
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 442
    :goto_29
    return v2

    #@2a
    .line 433
    :cond_2a
    if-ne v0, v3, :cond_35

    #@2c
    .line 434
    const-string v2, "FeliCaDevice"

    #@2e
    const-string v4, "cal did, but failed."

    #@30
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    move v2, v3

    #@34
    .line 437
    goto :goto_29

    #@35
    .line 439
    :cond_35
    const-string v2, "FeliCaDevice"

    #@37
    const-string v3, "cal hasn\'t been, yet."

    #@39
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    move v2, v4

    #@3d
    .line 442
    goto :goto_29
.end method

.method public cmdRFRegCalCheck()Z
    .registers 3

    #@0
    .prologue
    .line 640
    invoke-direct {p0}, Lcom/lge/systemservice/service/FeliCaDevice;->checkRFRegCal()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_f

    #@6
    .line 642
    const-string v0, "FeliCaDevice"

    #@8
    const-string v1, "Check init values OK."

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 643
    const/4 v0, 0x1

    #@e
    .line 648
    :goto_e
    return v0

    #@f
    .line 647
    :cond_f
    const-string v0, "FeliCaDevice"

    #@11
    const-string v1, "Check init values FAIL."

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 648
    const/4 v0, 0x0

    #@17
    goto :goto_e
.end method

.method public cmdRFRegCalLoad()Z
    .registers 3

    #@0
    .prologue
    .line 625
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->initRFRarameters()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_f

    #@6
    .line 626
    const-string v0, "FeliCaDevice"

    #@8
    const-string v1, "Reset RF and SWITCH values OK."

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 628
    const/4 v0, 0x1

    #@e
    .line 633
    :goto_e
    return v0

    #@f
    .line 631
    :cond_f
    const-string v0, "FeliCaDevice"

    #@11
    const-string v1, "Reset RF and SWITCH values failed."

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 633
    const/4 v0, 0x0

    #@17
    goto :goto_e
.end method

.method public cmdSwitchRange(Lcom/lge/systemservice/service/FeliCaDevice$FeliCaCmdResult;)Z
    .registers 3
    .parameter "cmdResult"

    #@0
    .prologue
    .line 490
    sget v0, Lcom/lge/systemservice/service/FeliCaDevice;->CAL_ENTRY_COUNT:I

    #@2
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    iput-object v0, p1, Lcom/lge/systemservice/service/FeliCaDevice$FeliCaCmdResult;->result:Ljava/lang/String;

    #@8
    .line 492
    const/4 v0, 0x1

    #@9
    return v0
.end method

.method public cmdSwitchRead(Lcom/lge/systemservice/service/FeliCaDevice$FeliCaCmdResult;)Z
    .registers 5
    .parameter "cmdResult"

    #@0
    .prologue
    .line 528
    invoke-static {}, Lcom/lge/systemservice/service/FeliCaDevice;->read_switch_idx()I

    #@3
    move-result v0

    #@4
    .line 530
    .local v0, sw_index:I
    if-gez v0, :cond_f

    #@6
    .line 531
    const-string v1, "FeliCaDevice"

    #@8
    const-string v2, "READ SWITCH FAILED"

    #@a
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 533
    const/4 v1, 0x0

    #@e
    .line 539
    :goto_e
    return v1

    #@f
    .line 537
    :cond_f
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    iput-object v1, p1, Lcom/lge/systemservice/service/FeliCaDevice$FeliCaCmdResult;->result:Ljava/lang/String;

    #@15
    .line 539
    const/4 v1, 0x1

    #@16
    goto :goto_e
.end method

.method public cmdSwitchWrite(I)Z
    .registers 6
    .parameter "switchIdx"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 503
    if-gt v0, p1, :cond_19

    #@4
    sget v2, Lcom/lge/systemservice/service/FeliCaDevice;->CAL_ENTRY_COUNT:I

    #@6
    if-gt p1, v2, :cond_19

    #@8
    .line 505
    invoke-static {p1}, Lcom/lge/systemservice/service/FeliCaDevice;->set_switch_by_idx(I)I

    #@b
    move-result v2

    #@c
    const/4 v3, -0x1

    #@d
    if-eq v2, v3, :cond_10

    #@f
    .line 520
    :goto_f
    return v0

    #@10
    .line 511
    :cond_10
    const-string v0, "FeliCaDevice"

    #@12
    const-string v2, "WRITE SWITCH FAILED"

    #@14
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    move v0, v1

    #@18
    .line 513
    goto :goto_f

    #@19
    .line 518
    :cond_19
    const-string v0, "FeliCaDevice"

    #@1b
    const-string v2, "SWITCH index is out of range"

    #@1d
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    move v0, v1

    #@21
    .line 520
    goto :goto_f
.end method
