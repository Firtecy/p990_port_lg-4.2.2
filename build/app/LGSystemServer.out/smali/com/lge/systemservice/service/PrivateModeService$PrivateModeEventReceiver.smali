.class Lcom/lge/systemservice/service/PrivateModeService$PrivateModeEventReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PrivateModeService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/PrivateModeService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PrivateModeEventReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/PrivateModeService;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/PrivateModeService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 185
    iput-object p1, p0, Lcom/lge/systemservice/service/PrivateModeService$PrivateModeEventReceiver;->this$0:Lcom/lge/systemservice/service/PrivateModeService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 8
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 189
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 201
    .local v0, action:Ljava/lang/String;
    const-string v2, "android.intent.action.PHONE_STATE"

    #@6
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_5b

    #@c
    .line 202
    const-string v2, "state"

    #@e
    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@11
    move-result-object v1

    #@12
    .line 203
    .local v1, state:Ljava/lang/String;
    sget-object v2, Landroid/telephony/TelephonyManager;->EXTRA_STATE_RINGING:Ljava/lang/String;

    #@14
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v2

    #@18
    if-nez v2, :cond_22

    #@1a
    sget-object v2, Landroid/telephony/TelephonyManager;->EXTRA_STATE_OFFHOOK:Ljava/lang/String;

    #@1c
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v2

    #@20
    if-eqz v2, :cond_5c

    #@22
    .line 204
    :cond_22
    invoke-static {}, Lcom/lge/systemservice/service/PrivateModeService;->access$000()Z

    #@25
    move-result v2

    #@26
    if-eqz v2, :cond_48

    #@28
    invoke-static {}, Lcom/lge/systemservice/service/PrivateModeService;->access$100()Ljava/lang/String;

    #@2b
    move-result-object v2

    #@2c
    new-instance v3, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v4, "PrivateModeEventReceiver:: state in call..., mShown="

    #@33
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    iget-object v4, p0, Lcom/lge/systemservice/service/PrivateModeService$PrivateModeEventReceiver;->this$0:Lcom/lge/systemservice/service/PrivateModeService;

    #@39
    invoke-static {v4}, Lcom/lge/systemservice/service/PrivateModeService;->access$400(Lcom/lge/systemservice/service/PrivateModeService;)Z

    #@3c
    move-result v4

    #@3d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@40
    move-result-object v3

    #@41
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v3

    #@45
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 205
    :cond_48
    iget-object v2, p0, Lcom/lge/systemservice/service/PrivateModeService$PrivateModeEventReceiver;->this$0:Lcom/lge/systemservice/service/PrivateModeService;

    #@4a
    const/4 v3, 0x1

    #@4b
    invoke-static {v2, v3}, Lcom/lge/systemservice/service/PrivateModeService;->access$302(Lcom/lge/systemservice/service/PrivateModeService;Z)Z

    #@4e
    .line 206
    iget-object v2, p0, Lcom/lge/systemservice/service/PrivateModeService$PrivateModeEventReceiver;->this$0:Lcom/lge/systemservice/service/PrivateModeService;

    #@50
    invoke-static {v2}, Lcom/lge/systemservice/service/PrivateModeService;->access$400(Lcom/lge/systemservice/service/PrivateModeService;)Z

    #@53
    move-result v2

    #@54
    if-eqz v2, :cond_5b

    #@56
    .line 207
    iget-object v2, p0, Lcom/lge/systemservice/service/PrivateModeService$PrivateModeEventReceiver;->this$0:Lcom/lge/systemservice/service/PrivateModeService;

    #@58
    invoke-static {v2}, Lcom/lge/systemservice/service/PrivateModeService;->access$600(Lcom/lge/systemservice/service/PrivateModeService;)V

    #@5b
    .line 214
    .end local v1           #state:Ljava/lang/String;
    :cond_5b
    :goto_5b
    return-void

    #@5c
    .line 209
    .restart local v1       #state:Ljava/lang/String;
    :cond_5c
    sget-object v2, Landroid/telephony/TelephonyManager;->EXTRA_STATE_IDLE:Ljava/lang/String;

    #@5e
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@61
    move-result v2

    #@62
    if-eqz v2, :cond_5b

    #@64
    .line 210
    invoke-static {}, Lcom/lge/systemservice/service/PrivateModeService;->access$000()Z

    #@67
    move-result v2

    #@68
    if-eqz v2, :cond_73

    #@6a
    invoke-static {}, Lcom/lge/systemservice/service/PrivateModeService;->access$100()Ljava/lang/String;

    #@6d
    move-result-object v2

    #@6e
    const-string v3, "PrivateModeEventReceiver:: state idle..."

    #@70
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@73
    .line 211
    :cond_73
    iget-object v2, p0, Lcom/lge/systemservice/service/PrivateModeService$PrivateModeEventReceiver;->this$0:Lcom/lge/systemservice/service/PrivateModeService;

    #@75
    const/4 v3, 0x0

    #@76
    invoke-static {v2, v3}, Lcom/lge/systemservice/service/PrivateModeService;->access$302(Lcom/lge/systemservice/service/PrivateModeService;Z)Z

    #@79
    goto :goto_5b
.end method
