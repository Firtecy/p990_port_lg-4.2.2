.class Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService$1;
.super Landroid/content/BroadcastReceiver;
.source "LogCatcherService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 60
    iput-object p1, p0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService$1;->this$0:Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 8
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 63
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    .line 64
    .local v0, action:Ljava/lang/String;
    invoke-static {}, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->access$000()Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    new-instance v2, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v3, "received intent : "

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v2

    #@1c
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 66
    invoke-static {}, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->access$100()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@26
    move-result v1

    #@27
    if-eqz v1, :cond_4f

    #@29
    .line 67
    iget-object v1, p0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService$1;->this$0:Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;

    #@2b
    invoke-static {v1}, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->access$200(Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;)Ljava/lang/Boolean;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    #@32
    move-result v1

    #@33
    if-eqz v1, :cond_4b

    #@35
    .line 68
    invoke-static {}, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->access$000()Ljava/lang/String;

    #@38
    move-result-object v1

    #@39
    const-string v2, "QuickDump was triggered by LogCatcher Service"

    #@3b
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 69
    invoke-virtual {p0, p1, p2}, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService$1;->processQuickDumpTriggeredByAPICompleted(Landroid/content/Context;Landroid/content/Intent;)V

    #@41
    .line 70
    iget-object v1, p0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService$1;->this$0:Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;

    #@43
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@46
    move-result-object v2

    #@47
    invoke-static {v1, v2}, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->access$202(Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    #@4a
    .line 87
    :cond_4a
    :goto_4a
    return-void

    #@4b
    .line 72
    :cond_4b
    invoke-virtual {p0, p1, p2}, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService$1;->processQuickDumpTriggeredByInitServiceCompleted(Landroid/content/Context;Landroid/content/Intent;)V

    #@4e
    goto :goto_4a

    #@4f
    .line 74
    :cond_4f
    invoke-static {}, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->access$300()Ljava/lang/String;

    #@52
    move-result-object v1

    #@53
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@56
    move-result v1

    #@57
    if-eqz v1, :cond_7f

    #@59
    .line 75
    iget-object v1, p0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService$1;->this$0:Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;

    #@5b
    invoke-static {v1}, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->access$200(Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;)Ljava/lang/Boolean;

    #@5e
    move-result-object v1

    #@5f
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    #@62
    move-result v1

    #@63
    if-eqz v1, :cond_7b

    #@65
    .line 76
    invoke-static {}, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->access$000()Ljava/lang/String;

    #@68
    move-result-object v1

    #@69
    const-string v2, "QuickDump was triggered by LogCatcher Service"

    #@6b
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@6e
    .line 77
    invoke-virtual {p0, p1, p2}, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService$1;->processQuickDumpTriggeredByAPIFailed(Landroid/content/Context;Landroid/content/Intent;)V

    #@71
    .line 78
    iget-object v1, p0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService$1;->this$0:Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;

    #@73
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@76
    move-result-object v2

    #@77
    invoke-static {v1, v2}, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->access$202(Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    #@7a
    goto :goto_4a

    #@7b
    .line 80
    :cond_7b
    invoke-virtual {p0, p1, p2}, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService$1;->processQuickDumpTriggeredByInitServiceFailed(Landroid/content/Context;Landroid/content/Intent;)V

    #@7e
    goto :goto_4a

    #@7f
    .line 82
    :cond_7f
    invoke-static {}, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->access$400()Ljava/lang/String;

    #@82
    move-result-object v1

    #@83
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@86
    move-result v1

    #@87
    if-eqz v1, :cond_8d

    #@89
    .line 83
    invoke-virtual {p0, p1, p2}, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService$1;->processCopyLogsCompleted(Landroid/content/Context;Landroid/content/Intent;)V

    #@8c
    goto :goto_4a

    #@8d
    .line 84
    :cond_8d
    invoke-static {}, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->access$500()Ljava/lang/String;

    #@90
    move-result-object v1

    #@91
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@94
    move-result v1

    #@95
    if-eqz v1, :cond_4a

    #@97
    .line 85
    invoke-virtual {p0, p1, p2}, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService$1;->processCopyLogsFailed(Landroid/content/Context;Landroid/content/Intent;)V

    #@9a
    goto :goto_4a
.end method

.method public processCopyLogsCompleted(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 8
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 152
    new-instance v1, Landroid/content/Intent;

    #@2
    invoke-static {}, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->access$1100()Ljava/lang/String;

    #@5
    move-result-object v2

    #@6
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@9
    .line 153
    .local v1, sendingIntent:Landroid/content/Intent;
    const-string v2, "path"

    #@b
    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    .line 155
    .local v0, buffer:Ljava/lang/String;
    if-nez v0, :cond_36

    #@11
    .line 156
    const-string v2, "path"

    #@13
    const-string v3, ""

    #@15
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@18
    .line 161
    :goto_18
    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@1b
    .line 162
    invoke-static {}, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->access$000()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    new-instance v3, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v4, "Broadcast "

    #@26
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v3

    #@2a
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v3

    #@2e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v3

    #@32
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    .line 163
    return-void

    #@36
    .line 158
    :cond_36
    const-string v2, "path"

    #@38
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@3b
    goto :goto_18
.end method

.method public processCopyLogsFailed(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 8
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 166
    new-instance v1, Landroid/content/Intent;

    #@2
    invoke-static {}, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->access$1200()Ljava/lang/String;

    #@5
    move-result-object v2

    #@6
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@9
    .line 167
    .local v1, sendingIntent:Landroid/content/Intent;
    const-string v2, "error_code"

    #@b
    const/4 v3, 0x0

    #@c
    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@f
    move-result v0

    #@10
    .line 169
    .local v0, error_code:I
    const/4 v2, -0x2

    #@11
    if-ne v0, v2, :cond_3f

    #@13
    .line 170
    invoke-static {}, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->access$000()Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    const-string v3, "copy logs error -2: Unable to write"

    #@19
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 171
    const-string v2, "error_code"

    #@1e
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@21
    .line 180
    :goto_21
    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@24
    .line 181
    invoke-static {}, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->access$000()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    new-instance v3, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v4, "Broadcast "

    #@2f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v3

    #@3b
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 182
    return-void

    #@3f
    .line 172
    :cond_3f
    const/4 v2, -0x3

    #@40
    if-ne v0, v2, :cond_51

    #@42
    .line 173
    invoke-static {}, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->access$000()Ljava/lang/String;

    #@45
    move-result-object v2

    #@46
    const-string v3, "copy logs error -3: EXTERNAL_ADD_STORAGE is not declared."

    #@48
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    .line 174
    const-string v2, "error_code"

    #@4d
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@50
    goto :goto_21

    #@51
    .line 176
    :cond_51
    invoke-static {}, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->access$000()Ljava/lang/String;

    #@54
    move-result-object v2

    #@55
    const-string v3, "copy logs error -1: General error"

    #@57
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@5a
    .line 177
    const-string v2, "error_code"

    #@5c
    const/4 v3, -0x1

    #@5d
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@60
    goto :goto_21
.end method

.method public processQuickDumpTriggeredByAPICompleted(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 8
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 90
    new-instance v1, Landroid/content/Intent;

    #@2
    invoke-static {}, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->access$600()Ljava/lang/String;

    #@5
    move-result-object v2

    #@6
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@9
    .line 92
    .local v1, sendingIntent:Landroid/content/Intent;
    const-string v2, "path"

    #@b
    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    .line 93
    .local v0, buffer:Ljava/lang/String;
    if-nez v0, :cond_3c

    #@11
    .line 94
    const-string v2, "path"

    #@13
    const-string v3, ""

    #@15
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@18
    .line 99
    :goto_18
    iget-object v2, p0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService$1;->this$0:Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;

    #@1a
    invoke-static {v2}, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->access$700(Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;)Landroid/content/Context;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@21
    .line 100
    invoke-static {}, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->access$000()Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    new-instance v3, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v4, "Broadcast "

    #@2c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v3

    #@30
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v3

    #@38
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 101
    return-void

    #@3c
    .line 96
    :cond_3c
    const-string v2, "path"

    #@3e
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@41
    goto :goto_18
.end method

.method public processQuickDumpTriggeredByAPIFailed(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 7
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 104
    new-instance v0, Landroid/content/Intent;

    #@2
    invoke-static {}, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->access$800()Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@9
    .line 105
    .local v0, sendingIntent:Landroid/content/Intent;
    iget-object v1, p0, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService$1;->this$0:Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;

    #@b
    invoke-static {v1}, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->access$700(Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;)Landroid/content/Context;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@12
    .line 106
    invoke-static {}, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->access$000()Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    new-instance v2, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v3, "Broadcast "

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 107
    return-void
.end method

.method public processQuickDumpTriggeredByInitServiceCompleted(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 11
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 110
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@3
    move-result-object v3

    #@4
    .line 111
    .local v3, pm:Landroid/content/pm/PackageManager;
    new-instance v4, Landroid/content/Intent;

    #@6
    invoke-static {}, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->access$900()Ljava/lang/String;

    #@9
    move-result-object v5

    #@a
    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@d
    .line 113
    .local v4, sendingIntent:Landroid/content/Intent;
    const/high16 v5, 0x1

    #@f
    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    #@12
    move-result-object v2

    #@13
    .line 114
    .local v2, list:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v2, :cond_60

    #@15
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@18
    move-result v5

    #@19
    if-lez v5, :cond_60

    #@1b
    .line 115
    const/4 v5, 0x0

    #@1c
    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@1f
    move-result-object v1

    #@20
    check-cast v1, Landroid/content/pm/ResolveInfo;

    #@22
    .line 116
    .local v1, info:Landroid/content/pm/ResolveInfo;
    const-string v5, "path"

    #@24
    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@27
    move-result-object v0

    #@28
    .line 117
    .local v0, buffer:Ljava/lang/String;
    if-nez v0, :cond_61

    #@2a
    .line 118
    const-string v5, "path"

    #@2c
    const-string v6, ""

    #@2e
    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@31
    .line 122
    :goto_31
    iget-object v5, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@33
    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@35
    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@37
    iget-object v6, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@39
    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    #@3b
    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@3e
    .line 128
    const/high16 v5, 0x1400

    #@40
    invoke-virtual {v4, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@43
    .line 129
    invoke-static {}, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->access$000()Ljava/lang/String;

    #@46
    move-result-object v5

    #@47
    new-instance v6, Ljava/lang/StringBuilder;

    #@49
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    const-string v7, "Broadcast "

    #@4e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v6

    #@52
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v6

    #@56
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v6

    #@5a
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    .line 130
    invoke-virtual {p1, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@60
    .line 131
    .end local v0           #buffer:Ljava/lang/String;
    .end local v1           #info:Landroid/content/pm/ResolveInfo;
    :cond_60
    return-void

    #@61
    .line 120
    .restart local v0       #buffer:Ljava/lang/String;
    .restart local v1       #info:Landroid/content/pm/ResolveInfo;
    :cond_61
    const-string v5, "path"

    #@63
    invoke-virtual {v4, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@66
    goto :goto_31
.end method

.method public processQuickDumpTriggeredByInitServiceFailed(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 10
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 134
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@3
    move-result-object v2

    #@4
    .line 135
    .local v2, pm:Landroid/content/pm/PackageManager;
    new-instance v3, Landroid/content/Intent;

    #@6
    invoke-static {}, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->access$1000()Ljava/lang/String;

    #@9
    move-result-object v4

    #@a
    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@d
    .line 136
    .local v3, sendingIntent:Landroid/content/Intent;
    const/high16 v4, 0x1

    #@f
    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    #@12
    move-result-object v1

    #@13
    .line 138
    .local v1, list:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v1, :cond_51

    #@15
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@18
    move-result v4

    #@19
    if-lez v4, :cond_51

    #@1b
    .line 139
    const/4 v4, 0x0

    #@1c
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@1f
    move-result-object v0

    #@20
    check-cast v0, Landroid/content/pm/ResolveInfo;

    #@22
    .line 140
    .local v0, info:Landroid/content/pm/ResolveInfo;
    iget-object v4, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@24
    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@26
    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@28
    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@2a
    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    #@2c
    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@2f
    .line 146
    const/high16 v4, 0x1400

    #@31
    invoke-virtual {v3, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@34
    .line 147
    invoke-static {}, Lcom/lge/systemservice/service/logcatcherservice/LogCatcherService;->access$000()Ljava/lang/String;

    #@37
    move-result-object v4

    #@38
    new-instance v5, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v6, "Broadcast "

    #@3f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v5

    #@43
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v5

    #@47
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v5

    #@4b
    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 148
    invoke-virtual {p1, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@51
    .line 149
    .end local v0           #info:Landroid/content/pm/ResolveInfo;
    :cond_51
    return-void
.end method
