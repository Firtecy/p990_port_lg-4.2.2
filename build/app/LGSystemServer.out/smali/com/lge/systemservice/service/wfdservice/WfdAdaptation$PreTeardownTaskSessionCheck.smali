.class Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$PreTeardownTaskSessionCheck;
.super Landroid/os/AsyncTask;
.source "WfdAdaptation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PreTeardownTaskSessionCheck"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;


# direct methods
.method private constructor <init>(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 353
    iput-object p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$PreTeardownTaskSessionCheck;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@2
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 353
    invoke-direct {p0, p1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$PreTeardownTaskSessionCheck;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)V

    #@3
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .registers 7
    .parameter "params"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 357
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$PreTeardownTaskSessionCheck;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@3
    invoke-static {v1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->access$200(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;

    #@6
    move-result-object v1

    #@7
    if-eqz v1, :cond_61

    #@9
    .line 358
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$PreTeardownTaskSessionCheck;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@b
    invoke-static {v1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;->access$200(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;)Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1}, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->getStatus()Lcom/qualcomm/wfd/WfdStatus;

    #@12
    move-result-object v0

    #@13
    .line 359
    .local v0, wfdStatus:Lcom/qualcomm/wfd/WfdStatus;
    if-nez v0, :cond_21

    #@15
    .line 360
    const-string v1, "WfdAdaptation"

    #@17
    const-string v2, "wfdStatus is null!"

    #@19
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 361
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@1f
    move-result-object v1

    #@20
    .line 372
    .end local v0           #wfdStatus:Lcom/qualcomm/wfd/WfdStatus;
    :goto_20
    return-object v1

    #@21
    .line 363
    .restart local v0       #wfdStatus:Lcom/qualcomm/wfd/WfdStatus;
    :cond_21
    const-string v1, "WfdAdaptation"

    #@23
    new-instance v2, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v3, "wfdStatus.state= "

    #@2a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v2

    #@2e
    iget v3, v0, Lcom/qualcomm/wfd/WfdStatus;->state:I

    #@30
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v2

    #@38
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 364
    iget v1, v0, Lcom/qualcomm/wfd/WfdStatus;->state:I

    #@3d
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$SessionState;->INVALID:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@3f
    invoke-virtual {v2}, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ordinal()I

    #@42
    move-result v2

    #@43
    if-eq v1, v2, :cond_4f

    #@45
    iget v1, v0, Lcom/qualcomm/wfd/WfdStatus;->state:I

    #@47
    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$SessionState;->INITIALIZED:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    #@49
    invoke-virtual {v2}, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ordinal()I

    #@4c
    move-result v2

    #@4d
    if-ne v1, v2, :cond_5b

    #@4f
    .line 366
    :cond_4f
    const-string v1, "WfdAdaptation"

    #@51
    const-string v2, "Session not made"

    #@53
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    .line 367
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@59
    move-result-object v1

    #@5a
    goto :goto_20

    #@5b
    .line 369
    :cond_5b
    const/4 v1, 0x1

    #@5c
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@5f
    move-result-object v1

    #@60
    goto :goto_20

    #@61
    .line 372
    .end local v0           #wfdStatus:Lcom/qualcomm/wfd/WfdStatus;
    :cond_61
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@64
    move-result-object v1

    #@65
    goto :goto_20
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 353
    check-cast p1, [Ljava/lang/Void;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$PreTeardownTaskSessionCheck;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .registers 6
    .parameter "callTeardown"

    #@0
    .prologue
    .line 378
    const-string v1, "WfdAdaptation"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "PreTeardownTaskSessionCheck- callTeardown: "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 379
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    #@1b
    move-result v1

    #@1c
    if-eqz v1, :cond_34

    #@1e
    .line 380
    const-string v1, "WfdAdaptation"

    #@20
    const-string v2, "PreTeardownTaskSessionCheck- now calling teardown"

    #@22
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 381
    new-instance v0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$TeardownTask;

    #@27
    iget-object v1, p0, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$PreTeardownTaskSessionCheck;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;

    #@29
    const/4 v2, 0x0

    #@2a
    invoke-direct {v0, v1, v2}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$TeardownTask;-><init>(Lcom/lge/systemservice/service/wfdservice/WfdAdaptation;Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$1;)V

    #@2d
    .line 382
    .local v0, task:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$TeardownTask;
    const/4 v1, 0x0

    #@2e
    new-array v1, v1, [Ljava/lang/Void;

    #@30
    invoke-virtual {v0, v1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$TeardownTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    #@33
    .line 386
    .end local v0           #task:Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$TeardownTask;
    :goto_33
    return-void

    #@34
    .line 384
    :cond_34
    const-string v1, "WfdAdaptation"

    #@36
    const-string v2, "PreTeardownTaskSessionCheck- not in a WFD session, not going to call teardownOperation"

    #@38
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    goto :goto_33
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 353
    check-cast p1, Ljava/lang/Boolean;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Lcom/lge/systemservice/service/wfdservice/WfdAdaptation$PreTeardownTaskSessionCheck;->onPostExecute(Ljava/lang/Boolean;)V

    #@5
    return-void
.end method
