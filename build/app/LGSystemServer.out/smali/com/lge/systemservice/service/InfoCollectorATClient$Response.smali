.class public Lcom/lge/systemservice/service/InfoCollectorATClient$Response;
.super Ljava/lang/Object;
.source "InfoCollectorATClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/InfoCollectorATClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Response"
.end annotation


# instance fields
.field public data:[B

.field public length:I

.field public result:I

.field final synthetic this$0:Lcom/lge/systemservice/service/InfoCollectorATClient;


# direct methods
.method public constructor <init>(Lcom/lge/systemservice/service/InfoCollectorATClient;[B)V
    .registers 6
    .parameter
    .parameter "buffer"

    #@0
    .prologue
    .line 100
    iput-object p1, p0, Lcom/lge/systemservice/service/InfoCollectorATClient$Response;->this$0:Lcom/lge/systemservice/service/InfoCollectorATClient;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 103
    if-eqz p2, :cond_5b

    #@7
    .line 104
    const/4 v1, 0x0

    #@8
    aget-byte v1, p2, v1

    #@a
    and-int/lit16 v1, v1, 0xff

    #@c
    const/4 v2, 0x1

    #@d
    aget-byte v2, p2, v2

    #@f
    and-int/lit16 v2, v2, 0xff

    #@11
    shl-int/lit8 v2, v2, 0x8

    #@13
    add-int/2addr v1, v2

    #@14
    const/4 v2, 0x2

    #@15
    aget-byte v2, p2, v2

    #@17
    and-int/lit16 v2, v2, 0xff

    #@19
    shl-int/lit8 v2, v2, 0x10

    #@1b
    add-int/2addr v1, v2

    #@1c
    const/4 v2, 0x3

    #@1d
    aget-byte v2, p2, v2

    #@1f
    and-int/lit16 v2, v2, 0xff

    #@21
    shl-int/lit8 v2, v2, 0x18

    #@23
    add-int/2addr v1, v2

    #@24
    iput v1, p0, Lcom/lge/systemservice/service/InfoCollectorATClient$Response;->result:I

    #@26
    .line 107
    const/4 v1, 0x4

    #@27
    aget-byte v1, p2, v1

    #@29
    and-int/lit16 v1, v1, 0xff

    #@2b
    const/4 v2, 0x5

    #@2c
    aget-byte v2, p2, v2

    #@2e
    and-int/lit16 v2, v2, 0xff

    #@30
    shl-int/lit8 v2, v2, 0x8

    #@32
    add-int/2addr v1, v2

    #@33
    const/4 v2, 0x6

    #@34
    aget-byte v2, p2, v2

    #@36
    and-int/lit16 v2, v2, 0xff

    #@38
    shl-int/lit8 v2, v2, 0x10

    #@3a
    add-int/2addr v1, v2

    #@3b
    const/4 v2, 0x7

    #@3c
    aget-byte v2, p2, v2

    #@3e
    and-int/lit16 v2, v2, 0xff

    #@40
    shl-int/lit8 v2, v2, 0x18

    #@42
    add-int/2addr v1, v2

    #@43
    iput v1, p0, Lcom/lge/systemservice/service/InfoCollectorATClient$Response;->length:I

    #@45
    .line 110
    const/16 v1, 0x108

    #@47
    new-array v1, v1, [B

    #@49
    iput-object v1, p0, Lcom/lge/systemservice/service/InfoCollectorATClient$Response;->data:[B

    #@4b
    .line 112
    const/4 v0, 0x0

    #@4c
    .local v0, i:I
    :goto_4c
    iget v1, p0, Lcom/lge/systemservice/service/InfoCollectorATClient$Response;->length:I

    #@4e
    if-ge v0, v1, :cond_5b

    #@50
    .line 113
    iget-object v1, p0, Lcom/lge/systemservice/service/InfoCollectorATClient$Response;->data:[B

    #@52
    add-int/lit8 v2, v0, 0x8

    #@54
    aget-byte v2, p2, v2

    #@56
    aput-byte v2, v1, v0

    #@58
    .line 112
    add-int/lit8 v0, v0, 0x1

    #@5a
    goto :goto_4c

    #@5b
    .line 120
    .end local v0           #i:I
    :cond_5b
    return-void
.end method
