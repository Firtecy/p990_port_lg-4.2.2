.class final Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibratorHandler;
.super Landroid/os/Handler;
.source "VolumeVibratorService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "VolumeVibratorHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;


# direct methods
.method public constructor <init>(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;Landroid/os/Looper;)V
    .registers 3
    .parameter
    .parameter "looper"

    #@0
    .prologue
    .line 369
    iput-object p1, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibratorHandler;->this$0:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;

    #@2
    .line 370
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 371
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    .line 376
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v0, :pswitch_data_32

    #@5
    .line 387
    const-string v0, "VolumeVibrator"

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v2, "this is NOT support message"

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    iget v2, p1, Landroid/os/Message;->what:I

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 390
    :goto_1f
    return-void

    #@20
    .line 378
    :pswitch_20
    iget-object v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibratorHandler;->this$0:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;

    #@22
    invoke-static {v0, p1}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->access$700(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;Landroid/os/Message;)V

    #@25
    goto :goto_1f

    #@26
    .line 381
    :pswitch_26
    iget-object v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibratorHandler;->this$0:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;

    #@28
    invoke-static {v0, p1}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->access$800(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;Landroid/os/Message;)V

    #@2b
    goto :goto_1f

    #@2c
    .line 384
    :pswitch_2c
    iget-object v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibratorHandler;->this$0:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;

    #@2e
    invoke-static {v0, p1}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->access$900(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;Landroid/os/Message;)V

    #@31
    goto :goto_1f

    #@32
    .line 376
    :pswitch_data_32
    .packed-switch 0x0
        :pswitch_20
        :pswitch_26
        :pswitch_2c
    .end packed-switch
.end method
