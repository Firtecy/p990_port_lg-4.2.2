.class Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;
.super Ljava/lang/Object;
.source "CliptrayService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/CliptrayService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Cliptraycue"
.end annotation


# instance fields
.field private mCueBtn:Landroid/widget/ImageView;

.field private mCueEditWidth:I

.field private mCueHeight:I

.field private mCueState:I

.field private mCueWidth:I

.field final synthetic this$0:Lcom/lge/systemservice/service/CliptrayService;

.field private wmClipCueParams:Landroid/view/WindowManager$LayoutParams;


# direct methods
.method public constructor <init>(Lcom/lge/systemservice/service/CliptrayService;)V
    .registers 10
    .parameter

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v3, 0x0

    #@2
    .line 595
    iput-object p1, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    .line 588
    iput v3, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueState:I

    #@9
    .line 592
    iput-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueBtn:Landroid/widget/ImageView;

    #@b
    .line 593
    iput-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->wmClipCueParams:Landroid/view/WindowManager$LayoutParams;

    #@d
    .line 596
    invoke-virtual {p1}, Lcom/lge/systemservice/service/CliptrayService;->getResources()Landroid/content/res/Resources;

    #@10
    move-result-object v0

    #@11
    const v1, 0x20c0048

    #@14
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@17
    move-result v0

    #@18
    iput v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueWidth:I

    #@1a
    .line 597
    invoke-virtual {p1}, Lcom/lge/systemservice/service/CliptrayService;->getResources()Landroid/content/res/Resources;

    #@1d
    move-result-object v0

    #@1e
    const v1, 0x20c0049

    #@21
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@24
    move-result v0

    #@25
    iput v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueEditWidth:I

    #@27
    .line 598
    invoke-virtual {p1}, Lcom/lge/systemservice/service/CliptrayService;->getResources()Landroid/content/res/Resources;

    #@2a
    move-result-object v0

    #@2b
    const v1, 0x20c004a

    #@2e
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@31
    move-result v0

    #@32
    iput v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueHeight:I

    #@34
    .line 599
    new-instance v0, Lcom/lge/systemservice/service/CliptrayService$mCueBtnView;

    #@36
    invoke-direct {v0, p1, p1}, Lcom/lge/systemservice/service/CliptrayService$mCueBtnView;-><init>(Lcom/lge/systemservice/service/CliptrayService;Landroid/content/Context;)V

    #@39
    iput-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueBtn:Landroid/widget/ImageView;

    #@3b
    .line 601
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueBtn:Landroid/widget/ImageView;

    #@3d
    new-instance v1, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue$1;

    #@3f
    invoke-direct {v1, p0, p1}, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue$1;-><init>(Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;Lcom/lge/systemservice/service/CliptrayService;)V

    #@42
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@45
    .line 624
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueBtn:Landroid/widget/ImageView;

    #@47
    new-instance v1, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue$2;

    #@49
    invoke-direct {v1, p0, p1}, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue$2;-><init>(Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;Lcom/lge/systemservice/service/CliptrayService;)V

    #@4c
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    #@4f
    .line 657
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    #@51
    iget v1, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueWidth:I

    #@53
    iget v2, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueHeight:I

    #@55
    invoke-static {p1}, Lcom/lge/systemservice/service/CliptrayService;->access$100(Lcom/lge/systemservice/service/CliptrayService;)Landroid/view/View;

    #@58
    move-result-object v4

    #@59
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@5c
    move-result-object v4

    #@5d
    iget v4, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@5f
    const/16 v5, 0x7d8

    #@61
    const v6, 0x40228

    #@64
    const/4 v7, 0x1

    #@65
    invoke-direct/range {v0 .. v7}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIIIII)V

    #@68
    iput-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->wmClipCueParams:Landroid/view/WindowManager$LayoutParams;

    #@6a
    .line 665
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->wmClipCueParams:Landroid/view/WindowManager$LayoutParams;

    #@6c
    const/16 v1, 0x55

    #@6e
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@70
    .line 666
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->wmClipCueParams:Landroid/view/WindowManager$LayoutParams;

    #@72
    const/16 v1, 0x30

    #@74
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@76
    .line 667
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->wmClipCueParams:Landroid/view/WindowManager$LayoutParams;

    #@78
    const-string v1, "CliptrayCue"

    #@7a
    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    #@7d
    .line 669
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueBtn:Landroid/widget/ImageView;

    #@7f
    const/16 v1, 0x8

    #@81
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    #@84
    .line 670
    invoke-static {p1}, Lcom/lge/systemservice/service/CliptrayService;->access$1800(Lcom/lge/systemservice/service/CliptrayService;)Landroid/view/WindowManager;

    #@87
    move-result-object v0

    #@88
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueBtn:Landroid/widget/ImageView;

    #@8a
    iget-object v2, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->wmClipCueParams:Landroid/view/WindowManager$LayoutParams;

    #@8c
    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@8f
    .line 671
    return-void
.end method

.method static synthetic access$1300(Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 582
    invoke-direct {p0}, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->finishDeleteMode()V

    #@3
    return-void
.end method

.method static synthetic access$1600(Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 582
    iget v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueState:I

    #@2
    return v0
.end method

.method static synthetic access$1700(Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;)Landroid/widget/ImageView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 582
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueBtn:Landroid/widget/ImageView;

    #@2
    return-object v0
.end method

.method private finishDeleteMode()V
    .registers 4

    #@0
    .prologue
    .line 728
    const-string v0, "CliptrayService"

    #@2
    const-string v1, "finish Delete Mode"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 729
    const/4 v0, 0x2

    #@8
    iput v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueState:I

    #@a
    .line 730
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueBtn:Landroid/widget/ImageView;

    #@c
    const v1, 0x2020370

    #@f
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    #@12
    .line 731
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@14
    invoke-static {v0}, Lcom/lge/systemservice/service/CliptrayService;->access$400(Lcom/lge/systemservice/service/CliptrayService;)Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@17
    move-result-object v0

    #@18
    invoke-virtual {v0}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->onFinishDeleteMode()V

    #@1b
    .line 733
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->wmClipCueParams:Landroid/view/WindowManager$LayoutParams;

    #@1d
    iget v1, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueWidth:I

    #@1f
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@21
    .line 734
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@23
    invoke-static {v0}, Lcom/lge/systemservice/service/CliptrayService;->access$1800(Lcom/lge/systemservice/service/CliptrayService;)Landroid/view/WindowManager;

    #@26
    move-result-object v0

    #@27
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueBtn:Landroid/widget/ImageView;

    #@29
    iget-object v2, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->wmClipCueParams:Landroid/view/WindowManager$LayoutParams;

    #@2b
    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@2e
    .line 735
    return-void
.end method


# virtual methods
.method public changeToDeleteMode()V
    .registers 4

    #@0
    .prologue
    .line 719
    const-string v0, "CliptrayService"

    #@2
    const-string v1, "chnageToDeleteMode"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 720
    const/4 v0, 0x3

    #@8
    iput v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueState:I

    #@a
    .line 721
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueBtn:Landroid/widget/ImageView;

    #@c
    const v1, 0x2020372

    #@f
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    #@12
    .line 723
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->wmClipCueParams:Landroid/view/WindowManager$LayoutParams;

    #@14
    iget v1, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueEditWidth:I

    #@16
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@18
    .line 724
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@1a
    invoke-static {v0}, Lcom/lge/systemservice/service/CliptrayService;->access$1800(Lcom/lge/systemservice/service/CliptrayService;)Landroid/view/WindowManager;

    #@1d
    move-result-object v0

    #@1e
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueBtn:Landroid/widget/ImageView;

    #@20
    iget-object v2, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->wmClipCueParams:Landroid/view/WindowManager$LayoutParams;

    #@22
    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@25
    .line 725
    return-void
.end method

.method public hideCliptraycue()V
    .registers 6

    #@0
    .prologue
    const/16 v4, 0x8

    #@2
    const/4 v3, 0x3

    #@3
    const/4 v2, 0x0

    #@4
    .line 694
    iget v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueState:I

    #@6
    const/4 v1, 0x1

    #@7
    if-ne v0, v1, :cond_18

    #@9
    .line 695
    const-string v0, "CliptrayService"

    #@b
    const-string v1, " hideCliptraycue() >> mCueState == STATE_OPEN"

    #@d
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 696
    iput v2, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueState:I

    #@12
    .line 697
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueBtn:Landroid/widget/ImageView;

    #@14
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    #@17
    .line 707
    :cond_17
    :goto_17
    return-void

    #@18
    .line 698
    :cond_18
    iget v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueState:I

    #@1a
    const/4 v1, 0x2

    #@1b
    if-eq v0, v1, :cond_21

    #@1d
    iget v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueState:I

    #@1f
    if-ne v0, v3, :cond_17

    #@21
    .line 699
    :cond_21
    iget v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueState:I

    #@23
    if-ne v0, v3, :cond_28

    #@25
    .line 700
    invoke-direct {p0}, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->finishDeleteMode()V

    #@28
    .line 702
    :cond_28
    iput v2, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueState:I

    #@2a
    .line 703
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueBtn:Landroid/widget/ImageView;

    #@2c
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    #@2f
    .line 704
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@31
    invoke-static {v0}, Lcom/lge/systemservice/service/CliptrayService;->access$600(Lcom/lge/systemservice/service/CliptrayService;)V

    #@34
    .line 705
    const-string v0, "CliptrayService"

    #@36
    const-string v1, " hideCliptraycue() >> STATE_CLOSE||STATE_EDIT, hideCliptray()"

    #@38
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    goto :goto_17
.end method

.method public isShowing()Z
    .registers 2

    #@0
    .prologue
    .line 710
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueBtn:Landroid/widget/ImageView;

    #@2
    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public orientationChanged()V
    .registers 4

    #@0
    .prologue
    .line 714
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->wmClipCueParams:Landroid/view/WindowManager$LayoutParams;

    #@2
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@4
    invoke-static {v1}, Lcom/lge/systemservice/service/CliptrayService;->access$100(Lcom/lge/systemservice/service/CliptrayService;)Landroid/view/View;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@b
    move-result-object v1

    #@c
    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@e
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    #@10
    .line 715
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@12
    invoke-static {v0}, Lcom/lge/systemservice/service/CliptrayService;->access$1800(Lcom/lge/systemservice/service/CliptrayService;)Landroid/view/WindowManager;

    #@15
    move-result-object v0

    #@16
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueBtn:Landroid/widget/ImageView;

    #@18
    iget-object v2, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->wmClipCueParams:Landroid/view/WindowManager$LayoutParams;

    #@1a
    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@1d
    .line 716
    return-void
.end method

.method public showCliptraycue()V
    .registers 4

    #@0
    .prologue
    .line 674
    const-string v0, "CliptrayService"

    #@2
    const-string v1, "showCliptraycue"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 675
    const/4 v0, 0x1

    #@8
    iput v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueState:I

    #@a
    .line 676
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueBtn:Landroid/widget/ImageView;

    #@c
    const v1, 0x2020378

    #@f
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    #@12
    .line 677
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueBtn:Landroid/widget/ImageView;

    #@14
    const/4 v1, 0x0

    #@15
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    #@18
    .line 678
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->wmClipCueParams:Landroid/view/WindowManager$LayoutParams;

    #@1a
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@1c
    invoke-static {v1}, Lcom/lge/systemservice/service/CliptrayService;->access$100(Lcom/lge/systemservice/service/CliptrayService;)Landroid/view/View;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@23
    move-result-object v1

    #@24
    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@26
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    #@28
    .line 679
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@2a
    invoke-static {v0}, Lcom/lge/systemservice/service/CliptrayService;->access$1800(Lcom/lge/systemservice/service/CliptrayService;)Landroid/view/WindowManager;

    #@2d
    move-result-object v0

    #@2e
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueBtn:Landroid/widget/ImageView;

    #@30
    iget-object v2, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->wmClipCueParams:Landroid/view/WindowManager$LayoutParams;

    #@32
    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@35
    .line 680
    return-void
.end method

.method public showCliptraycueClose()V
    .registers 4

    #@0
    .prologue
    .line 683
    const-string v0, "CliptrayService"

    #@2
    const-string v1, "showCliptraycueClose()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 684
    const/4 v0, 0x2

    #@8
    iput v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueState:I

    #@a
    .line 685
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueBtn:Landroid/widget/ImageView;

    #@c
    const v1, 0x2020370

    #@f
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    #@12
    .line 686
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueBtn:Landroid/widget/ImageView;

    #@14
    const/4 v1, 0x0

    #@15
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    #@18
    .line 687
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->wmClipCueParams:Landroid/view/WindowManager$LayoutParams;

    #@1a
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@1c
    invoke-static {v1}, Lcom/lge/systemservice/service/CliptrayService;->access$100(Lcom/lge/systemservice/service/CliptrayService;)Landroid/view/View;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@23
    move-result-object v1

    #@24
    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@26
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    #@28
    .line 688
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@2a
    invoke-static {v0}, Lcom/lge/systemservice/service/CliptrayService;->access$1800(Lcom/lge/systemservice/service/CliptrayService;)Landroid/view/WindowManager;

    #@2d
    move-result-object v0

    #@2e
    iget-object v1, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->mCueBtn:Landroid/widget/ImageView;

    #@30
    iget-object v2, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->wmClipCueParams:Landroid/view/WindowManager$LayoutParams;

    #@32
    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@35
    .line 690
    iget-object v0, p0, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@37
    invoke-static {v0}, Lcom/lge/systemservice/service/CliptrayService;->access$500(Lcom/lge/systemservice/service/CliptrayService;)V

    #@3a
    .line 691
    return-void
.end method
