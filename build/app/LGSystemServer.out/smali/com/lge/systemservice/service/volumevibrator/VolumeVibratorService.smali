.class public Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;
.super Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager$Stub;
.source "VolumeVibratorService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibratorHandler;,
        Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;
    }
.end annotation


# static fields
.field private static final VOLUME_VIBRATE_ALL:[Ljava/lang/String;


# instance fields
.field private lock:Ljava/lang/Object;

.field private final mContext:Landroid/content/Context;

.field private mCurVibration:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;

.field private final mH:Landroid/os/Handler;

.field private mHandler:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibratorHandler;

.field mIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mJustVibrate:Z

.field private mOrgVolume:I

.field private mPatternIndex:I

.field private mPatternWorking:Z

.field private mPreference:Landroid/content/SharedPreferences;

.field private final mService:Landroid/os/IVibratorService;

.field private mVolumeVibratorHandlerThread:Landroid/os/HandlerThread;

.field private final mVolumes:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 37
    const/4 v0, 0x3

    #@1
    new-array v0, v0, [Ljava/lang/String;

    #@3
    const/4 v1, 0x0

    #@4
    const-string v2, "volume_vibrate_ring"

    #@6
    aput-object v2, v0, v1

    #@8
    const/4 v1, 0x1

    #@9
    const-string v2, "volume_vibrate_notification"

    #@b
    aput-object v2, v0, v1

    #@d
    const/4 v1, 0x2

    #@e
    const-string v2, "volume_vibrate_haptic"

    #@10
    aput-object v2, v0, v1

    #@12
    sput-object v0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->VOLUME_VIBRATE_ALL:[Ljava/lang/String;

    #@14
    .line 78
    const-string v0, "VolumeVibrator"

    #@16
    const-string v1, "Load jni lib Volume Vibrator..."

    #@18
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 79
    const-string v0, "volumevibrator_jni"

    #@1d
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@20
    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 84
    invoke-direct {p0}, Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager$Stub;-><init>()V

    #@4
    .line 41
    new-instance v1, Landroid/os/Handler;

    #@6
    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    #@9
    iput-object v1, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mH:Landroid/os/Handler;

    #@b
    .line 44
    iput-boolean v3, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPatternWorking:Z

    #@d
    .line 45
    iput-boolean v3, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mJustVibrate:Z

    #@f
    .line 47
    const/4 v1, -0x1

    #@10
    iput v1, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPatternIndex:I

    #@12
    .line 58
    new-instance v1, Landroid/os/HandlerThread;

    #@14
    const-string v2, "volumeHandlerThread"

    #@16
    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@19
    iput-object v1, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mVolumeVibratorHandlerThread:Landroid/os/HandlerThread;

    #@1b
    .line 357
    new-instance v1, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$1;

    #@1d
    invoke-direct {v1, p0}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$1;-><init>(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;)V

    #@20
    iput-object v1, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@22
    .line 85
    iput-object p1, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mContext:Landroid/content/Context;

    #@24
    .line 86
    const-string v1, "vibrator"

    #@26
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@29
    move-result-object v1

    #@2a
    invoke-static {v1}, Landroid/os/IVibratorService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/IVibratorService;

    #@2d
    move-result-object v1

    #@2e
    iput-object v1, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mService:Landroid/os/IVibratorService;

    #@30
    .line 87
    invoke-virtual {p0}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->getVibrateVolume()I

    #@33
    move-result v1

    #@34
    iput v1, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mOrgVolume:I

    #@36
    .line 88
    new-instance v1, Ljava/lang/Object;

    #@38
    invoke-direct/range {v1 .. v1}, Ljava/lang/Object;-><init>()V

    #@3b
    iput-object v1, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->lock:Ljava/lang/Object;

    #@3d
    .line 89
    new-instance v1, Ljava/util/LinkedList;

    #@3f
    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    #@42
    iput-object v1, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mVolumes:Ljava/util/LinkedList;

    #@44
    .line 90
    iget-object v1, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mContext:Landroid/content/Context;

    #@46
    const-string v2, "VolumeVibrator"

    #@48
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@4b
    move-result-object v1

    #@4c
    iput-object v1, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPreference:Landroid/content/SharedPreferences;

    #@4e
    .line 91
    new-instance v0, Landroid/content/IntentFilter;

    #@50
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@53
    .line 92
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SCREEN_OFF"

    #@55
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@58
    .line 93
    iget-object v1, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mContext:Landroid/content/Context;

    #@5a
    iget-object v2, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@5c
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@5f
    .line 95
    iget-object v1, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mVolumeVibratorHandlerThread:Landroid/os/HandlerThread;

    #@61
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    #@64
    .line 96
    new-instance v1, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibratorHandler;

    #@66
    iget-object v2, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mVolumeVibratorHandlerThread:Landroid/os/HandlerThread;

    #@68
    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@6b
    move-result-object v2

    #@6c
    invoke-direct {v1, p0, v2}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibratorHandler;-><init>(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;Landroid/os/Looper;)V

    #@6f
    iput-object v1, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mHandler:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibratorHandler;

    #@71
    .line 97
    return-void
.end method

.method static synthetic access$400(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 30
    iget-object v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->lock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;)Ljava/util/LinkedList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 30
    iget-object v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mVolumes:Ljava/util/LinkedList;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 30
    invoke-direct {p0}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->clearLocked()V

    #@3
    return-void
.end method

.method static synthetic access$700(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->handleVibrate(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$800(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->handleVibratePattern(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$900(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->handleVibrateCancel(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method private cancelVibrateInternal(Landroid/os/IBinder;)V
    .registers 4
    .parameter "token"

    #@0
    .prologue
    .line 265
    const/4 v0, 0x2

    #@1
    const/4 v1, 0x0

    #@2
    invoke-direct {p0, v0, v1, p1}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->sendMessage(ILandroid/os/Bundle;Ljava/lang/Object;)V

    #@5
    .line 266
    return-void
.end method

.method private clearLocked()V
    .registers 3

    #@0
    .prologue
    .line 107
    const-string v0, "VolumeVibrator"

    #@2
    const-string v1, "clear"

    #@4
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 108
    const/4 v0, -0x1

    #@8
    iput v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPatternIndex:I

    #@a
    .line 109
    const/4 v0, 0x0

    #@b
    iput-boolean v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPatternWorking:Z

    #@d
    .line 110
    const/4 v0, 0x0

    #@e
    iput-object v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mCurVibration:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;

    #@10
    .line 113
    return-void
.end method

.method private getSharedPreferences()Landroid/content/SharedPreferences;
    .registers 4

    #@0
    .prologue
    .line 100
    iget-object v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPreference:Landroid/content/SharedPreferences;

    #@2
    if-nez v0, :cond_f

    #@4
    .line 101
    iget-object v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mContext:Landroid/content/Context;

    #@6
    const-string v1, "VolumeVibrator"

    #@8
    const/4 v2, 0x0

    #@9
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@c
    move-result-object v0

    #@d
    iput-object v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPreference:Landroid/content/SharedPreferences;

    #@f
    .line 103
    :cond_f
    iget-object v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPreference:Landroid/content/SharedPreferences;

    #@11
    return-object v0
.end method

.method private handleVibrate(Landroid/os/Message;)V
    .registers 10
    .parameter "msg"

    #@0
    .prologue
    .line 394
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@3
    move-result-object v0

    #@4
    .line 395
    .local v0, args:Landroid/os/Bundle;
    const-string v6, "volume"

    #@6
    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@9
    move-result v5

    #@a
    .line 396
    .local v5, volume:I
    const-string v6, "milliseconds"

    #@c
    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    #@f
    move-result-wide v2

    #@10
    .line 397
    .local v2, milliseconds:J
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@12
    check-cast v4, Landroid/os/IBinder;

    #@14
    .line 399
    .local v4, token:Landroid/os/IBinder;
    iget-object v7, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->lock:Ljava/lang/Object;

    #@16
    monitor-enter v7

    #@17
    .line 400
    const/4 v6, 0x1

    #@18
    :try_start_18
    iput-boolean v6, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mJustVibrate:Z

    #@1a
    .line 401
    iput v5, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mOrgVolume:I

    #@1c
    .line 402
    monitor-exit v7
    :try_end_1d
    .catchall {:try_start_18 .. :try_end_1d} :catchall_23

    #@1d
    .line 405
    :try_start_1d
    iget-object v6, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mService:Landroid/os/IVibratorService;

    #@1f
    invoke-interface {v6, v2, v3, v4}, Landroid/os/IVibratorService;->vibrate(JLandroid/os/IBinder;)V
    :try_end_22
    .catch Landroid/os/RemoteException; {:try_start_1d .. :try_end_22} :catch_26

    #@22
    .line 410
    :goto_22
    return-void

    #@23
    .line 402
    :catchall_23
    move-exception v6

    #@24
    :try_start_24
    monitor-exit v7
    :try_end_25
    .catchall {:try_start_24 .. :try_end_25} :catchall_23

    #@25
    throw v6

    #@26
    .line 406
    :catch_26
    move-exception v1

    #@27
    .line 407
    .local v1, e:Landroid/os/RemoteException;
    const-string v6, "VolumeVibrator"

    #@29
    const-string v7, "Fail vibrate call"

    #@2b
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 408
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    #@31
    goto :goto_22
.end method

.method private handleVibrateCancel(Landroid/os/Message;)V
    .registers 7
    .parameter "msg"

    #@0
    .prologue
    .line 440
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2
    check-cast v1, Landroid/os/IBinder;

    #@4
    .line 442
    .local v1, token:Landroid/os/IBinder;
    iget-object v4, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->lock:Ljava/lang/Object;

    #@6
    monitor-enter v4

    #@7
    .line 443
    :try_start_7
    invoke-direct {p0, v1}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->removeVibrationLocked(Landroid/os/IBinder;)Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;

    #@a
    move-result-object v2

    #@b
    .line 444
    .local v2, vib:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;
    iget-object v3, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mCurVibration:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;

    #@d
    if-ne v2, v3, :cond_12

    #@f
    .line 445
    invoke-direct {p0}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->clearLocked()V

    #@12
    .line 447
    :cond_12
    monitor-exit v4
    :try_end_13
    .catchall {:try_start_7 .. :try_end_13} :catchall_19

    #@13
    .line 450
    :try_start_13
    iget-object v3, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mService:Landroid/os/IVibratorService;

    #@15
    invoke-interface {v3, v1}, Landroid/os/IVibratorService;->cancelVibrate(Landroid/os/IBinder;)V
    :try_end_18
    .catch Landroid/os/RemoteException; {:try_start_13 .. :try_end_18} :catch_1c

    #@18
    .line 455
    :goto_18
    return-void

    #@19
    .line 447
    .end local v2           #vib:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;
    :catchall_19
    move-exception v3

    #@1a
    :try_start_1a
    monitor-exit v4
    :try_end_1b
    .catchall {:try_start_1a .. :try_end_1b} :catchall_19

    #@1b
    throw v3

    #@1c
    .line 451
    .restart local v2       #vib:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;
    :catch_1c
    move-exception v0

    #@1d
    .line 452
    .local v0, e:Landroid/os/RemoteException;
    const-string v3, "VolumeVibrator"

    #@1f
    const-string v4, "Fail cancelVibrate"

    #@21
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 453
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@27
    goto :goto_18
.end method

.method private handleVibratePattern(Landroid/os/Message;)V
    .registers 11
    .parameter "msg"

    #@0
    .prologue
    .line 413
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@3
    move-result-object v6

    #@4
    .line 414
    .local v6, args:Landroid/os/Bundle;
    const-string v0, "pattern"

    #@6
    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    #@9
    move-result-object v2

    #@a
    .line 415
    .local v2, pattern:[J
    const-string v0, "repeat"

    #@c
    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@f
    move-result v3

    #@10
    .line 416
    .local v3, repeat:I
    const-string v0, "volumePattern"

    #@12
    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    #@15
    move-result-object v4

    #@16
    .line 417
    .local v4, volumePattern:[I
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@18
    check-cast v5, Landroid/os/IBinder;

    #@1a
    .line 419
    .local v5, token:Landroid/os/IBinder;
    iget-object v8, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->lock:Ljava/lang/Object;

    #@1c
    monitor-enter v8

    #@1d
    .line 420
    :try_start_1d
    invoke-direct {p0, v5}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->removeVibrationLocked(Landroid/os/IBinder;)Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;

    #@20
    .line 421
    invoke-direct {p0}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->clearLocked()V

    #@23
    .line 422
    const/4 v0, 0x1

    #@24
    iput-boolean v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPatternWorking:Z

    #@26
    .line 424
    const/4 v0, -0x1

    #@27
    iput v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPatternIndex:I

    #@29
    .line 425
    new-instance v0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;

    #@2b
    move-object v1, p0

    #@2c
    invoke-direct/range {v0 .. v5}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;-><init>(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;[JI[ILandroid/os/IBinder;)V

    #@2f
    iput-object v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mCurVibration:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;

    #@31
    .line 426
    if-ltz v3, :cond_3a

    #@33
    .line 427
    iget-object v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mVolumes:Ljava/util/LinkedList;

    #@35
    iget-object v1, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mCurVibration:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;

    #@37
    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    #@3a
    .line 429
    :cond_3a
    monitor-exit v8
    :try_end_3b
    .catchall {:try_start_1d .. :try_end_3b} :catchall_41

    #@3b
    .line 432
    :try_start_3b
    iget-object v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mService:Landroid/os/IVibratorService;

    #@3d
    invoke-interface {v0, v2, v3, v5}, Landroid/os/IVibratorService;->vibratePattern([JILandroid/os/IBinder;)V
    :try_end_40
    .catch Landroid/os/RemoteException; {:try_start_3b .. :try_end_40} :catch_44

    #@40
    .line 437
    :goto_40
    return-void

    #@41
    .line 429
    :catchall_41
    move-exception v0

    #@42
    :try_start_42
    monitor-exit v8
    :try_end_43
    .catchall {:try_start_42 .. :try_end_43} :catchall_41

    #@43
    throw v0

    #@44
    .line 433
    :catch_44
    move-exception v7

    #@45
    .line 434
    .local v7, e:Landroid/os/RemoteException;
    const-string v0, "VolumeVibrator"

    #@47
    const-string v1, "Fail vibratePattern call"

    #@49
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    .line 435
    invoke-virtual {v7}, Landroid/os/RemoteException;->printStackTrace()V

    #@4f
    goto :goto_40
.end method

.method private isAll0([J)Z
    .registers 8
    .parameter "pattern"

    #@0
    .prologue
    .line 235
    array-length v0, p1

    #@1
    .line 236
    .local v0, N:I
    const/4 v1, 0x0

    #@2
    .local v1, i:I
    :goto_2
    if-ge v1, v0, :cond_11

    #@4
    .line 237
    aget-wide v2, p1, v1

    #@6
    const-wide/16 v4, 0x0

    #@8
    cmp-long v2, v2, v4

    #@a
    if-eqz v2, :cond_e

    #@c
    .line 238
    const/4 v2, 0x0

    #@d
    .line 241
    :goto_d
    return v2

    #@e
    .line 236
    :cond_e
    add-int/lit8 v1, v1, 0x1

    #@10
    goto :goto_2

    #@11
    .line 241
    :cond_11
    const/4 v2, 0x1

    #@12
    goto :goto_d
.end method

.method private removeVibrationLocked(Landroid/os/IBinder;)Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;
    .registers 6
    .parameter "token"

    #@0
    .prologue
    .line 346
    iget-object v2, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mVolumes:Ljava/util/LinkedList;

    #@2
    const/4 v3, 0x0

    #@3
    invoke-virtual {v2, v3}, Ljava/util/LinkedList;->listIterator(I)Ljava/util/ListIterator;

    #@6
    move-result-object v0

    #@7
    .line 347
    .local v0, iter:Ljava/util/ListIterator;,"Ljava/util/ListIterator<Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;>;"
    :cond_7
    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    #@a
    move-result v2

    #@b
    if-eqz v2, :cond_1d

    #@d
    .line 348
    invoke-interface {v0}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    #@10
    move-result-object v1

    #@11
    check-cast v1, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;

    #@13
    .line 349
    .local v1, vib:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;
    invoke-static {v1}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;->access$300(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;)Landroid/os/IBinder;

    #@16
    move-result-object v2

    #@17
    if-ne v2, p1, :cond_7

    #@19
    .line 350
    invoke-interface {v0}, Ljava/util/ListIterator;->remove()V

    #@1c
    .line 354
    .end local v1           #vib:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;
    :goto_1c
    return-object v1

    #@1d
    :cond_1d
    const/4 v1, 0x0

    #@1e
    goto :goto_1c
.end method

.method private sendMessage(ILandroid/os/Bundle;Ljava/lang/Object;)V
    .registers 6
    .parameter "message"
    .parameter "args"
    .parameter "binderObj"

    #@0
    .prologue
    .line 459
    iget-object v1, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mHandler:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibratorHandler;

    #@2
    invoke-virtual {v1, p1, p3}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibratorHandler;->removeMessages(ILjava/lang/Object;)V

    #@5
    .line 460
    iget-object v1, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mHandler:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibratorHandler;

    #@7
    invoke-virtual {v1, p1, p3}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibratorHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@a
    move-result-object v0

    #@b
    .line 461
    .local v0, msg:Landroid/os/Message;
    invoke-virtual {v0, p2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@e
    .line 462
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@11
    .line 463
    return-void
.end method

.method static native setVibratorVolumeNative(I)V
.end method

.method private vibrateInternal(JILandroid/os/IBinder;)V
    .registers 7
    .parameter "milliseconds"
    .parameter "volume"
    .parameter "token"

    #@0
    .prologue
    .line 245
    new-instance v0, Landroid/os/Bundle;

    #@2
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@5
    .line 246
    .local v0, args:Landroid/os/Bundle;
    const-string v1, "milliseconds"

    #@7
    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@a
    .line 247
    const-string v1, "volume"

    #@c
    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@f
    .line 248
    const/4 v1, 0x0

    #@10
    invoke-direct {p0, v1, v0, p4}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->sendMessage(ILandroid/os/Bundle;Ljava/lang/Object;)V

    #@13
    .line 249
    return-void
.end method

.method private vibratePatternInternal([JI[ILandroid/os/IBinder;)V
    .registers 7
    .parameter "pattern"
    .parameter "repeat"
    .parameter "volumePattern"
    .parameter "token"

    #@0
    .prologue
    .line 252
    if-eqz p1, :cond_10

    #@2
    array-length v1, p1

    #@3
    if-eqz v1, :cond_10

    #@5
    invoke-direct {p0, p1}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->isAll0([J)Z

    #@8
    move-result v1

    #@9
    if-nez v1, :cond_10

    #@b
    array-length v1, p1

    #@c
    if-ge p2, v1, :cond_10

    #@e
    if-nez p4, :cond_11

    #@10
    .line 262
    :cond_10
    :goto_10
    return-void

    #@11
    .line 257
    :cond_11
    new-instance v0, Landroid/os/Bundle;

    #@13
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@16
    .line 258
    .local v0, args:Landroid/os/Bundle;
    const-string v1, "pattern"

    #@18
    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    #@1b
    .line 259
    const-string v1, "repeat"

    #@1d
    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@20
    .line 260
    const-string v1, "volumePattern"

    #@22
    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    #@25
    .line 261
    const/4 v1, 0x1

    #@26
    invoke-direct {p0, v1, v0, p4}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->sendMessage(ILandroid/os/Bundle;Ljava/lang/Object;)V

    #@29
    goto :goto_10
.end method


# virtual methods
.method public cancelVibrate(Landroid/os/IBinder;)V
    .registers 2
    .parameter "token"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 277
    invoke-direct {p0, p1}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->cancelVibrateInternal(Landroid/os/IBinder;)V

    #@3
    .line 278
    return-void
.end method

.method public getCurrentVibratorVolume(I)I
    .registers 10
    .parameter "milliseconds"

    #@0
    .prologue
    .line 134
    const/4 v0, -0x1

    #@1
    .line 137
    .local v0, ret:I
    const-string v2, "VolumeVibrator"

    #@3
    new-instance v3, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v4, "milliseconds : "

    #@a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v3

    #@e
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v3

    #@12
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v3

    #@16
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 140
    iget-object v3, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->lock:Ljava/lang/Object;

    #@1b
    monitor-enter v3

    #@1c
    .line 141
    :try_start_1c
    iget-boolean v2, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPatternWorking:Z

    #@1e
    if-eqz v2, :cond_2e

    #@20
    iget-object v2, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mCurVibration:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;

    #@22
    if-nez v2, :cond_2e

    #@24
    .line 142
    const-string v2, "VolumeVibrator"

    #@26
    const-string v4, "it is not volume vibration"

    #@28
    invoke-static {v2, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 143
    monitor-exit v3

    #@2c
    move v1, v0

    #@2d
    .line 198
    .end local v0           #ret:I
    .local v1, ret:I
    :goto_2d
    return v1

    #@2e
    .line 147
    .end local v1           #ret:I
    .restart local v0       #ret:I
    :cond_2e
    const-string v2, "VolumeVibrator"

    #@30
    new-instance v4, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v5, "getCurrent::mPatternworking : "

    #@37
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v4

    #@3b
    iget-boolean v5, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPatternWorking:Z

    #@3d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@40
    move-result-object v4

    #@41
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v4

    #@45
    invoke-static {v2, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 148
    const-string v2, "VolumeVibrator"

    #@4a
    new-instance v4, Ljava/lang/StringBuilder;

    #@4c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4f
    const-string v5, "getCurrent::mVolumes.size : "

    #@51
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v4

    #@55
    iget-object v5, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mVolumes:Ljava/util/LinkedList;

    #@57
    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    #@5a
    move-result v5

    #@5b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v4

    #@5f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v4

    #@63
    invoke-static {v2, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@66
    .line 149
    const-string v2, "VolumeVibrator"

    #@68
    new-instance v4, Ljava/lang/StringBuilder;

    #@6a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@6d
    const-string v5, "getCurrent::mJustVibrate "

    #@6f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v4

    #@73
    iget-boolean v5, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mJustVibrate:Z

    #@75
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@78
    move-result-object v4

    #@79
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v4

    #@7d
    invoke-static {v2, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@80
    .line 153
    iget-boolean v2, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPatternWorking:Z

    #@82
    if-eqz v2, :cond_13e

    #@84
    .line 154
    iget v2, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPatternIndex:I

    #@86
    add-int/lit8 v2, v2, 0x2

    #@88
    iput v2, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPatternIndex:I

    #@8a
    .line 155
    iget-object v2, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mCurVibration:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;

    #@8c
    invoke-static {v2}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;->access$000(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;)I

    #@8f
    move-result v2

    #@90
    if-ltz v2, :cond_a7

    #@92
    iget v2, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPatternIndex:I

    #@94
    iget-object v4, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mCurVibration:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;

    #@96
    invoke-static {v4}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;->access$100(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;)[J

    #@99
    move-result-object v4

    #@9a
    array-length v4, v4

    #@9b
    if-le v2, v4, :cond_a7

    #@9d
    .line 156
    iget-object v2, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mCurVibration:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;

    #@9f
    invoke-static {v2}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;->access$000(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;)I

    #@a2
    move-result v2

    #@a3
    add-int/lit8 v2, v2, 0x1

    #@a5
    iput v2, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPatternIndex:I

    #@a7
    .line 160
    :cond_a7
    const-string v2, "VolumeVibrator"

    #@a9
    new-instance v4, Ljava/lang/StringBuilder;

    #@ab
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@ae
    const-string v5, "mPatternIndex : "

    #@b0
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v4

    #@b4
    iget v5, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPatternIndex:I

    #@b6
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v4

    #@ba
    const-string v5, " mCurVibration.mPattern[mPatternIndex] : "

    #@bc
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v4

    #@c0
    iget-object v5, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mCurVibration:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;

    #@c2
    invoke-static {v5}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;->access$100(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;)[J

    #@c5
    move-result-object v5

    #@c6
    iget v6, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPatternIndex:I

    #@c8
    aget-wide v5, v5, v6

    #@ca
    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v4

    #@ce
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d1
    move-result-object v4

    #@d2
    invoke-static {v2, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d5
    .line 164
    int-to-long v4, p1

    #@d6
    iget-object v2, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mCurVibration:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;

    #@d8
    invoke-static {v2}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;->access$100(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;)[J

    #@db
    move-result-object v2

    #@dc
    iget v6, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPatternIndex:I

    #@de
    aget-wide v6, v2, v6

    #@e0
    cmp-long v2, v4, v6

    #@e2
    if-nez v2, :cond_f7

    #@e4
    iget-object v2, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mCurVibration:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;

    #@e6
    invoke-static {v2}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;->access$000(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;)I

    #@e9
    move-result v2

    #@ea
    if-gez v2, :cond_108

    #@ec
    iget v2, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPatternIndex:I

    #@ee
    iget-object v4, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mCurVibration:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;

    #@f0
    invoke-static {v4}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;->access$100(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;)[J

    #@f3
    move-result-object v4

    #@f4
    array-length v4, v4

    #@f5
    if-lt v2, v4, :cond_108

    #@f7
    .line 167
    :cond_f7
    iget-object v2, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mCurVibration:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;

    #@f9
    invoke-static {v2}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;->access$200(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;)[I

    #@fc
    move-result-object v2

    #@fd
    iget v4, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPatternIndex:I

    #@ff
    aget v0, v2, v4

    #@101
    .line 169
    invoke-direct {p0}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->clearLocked()V

    #@104
    .line 198
    :cond_104
    :goto_104
    monitor-exit v3

    #@105
    move v1, v0

    #@106
    .end local v0           #ret:I
    .restart local v1       #ret:I
    goto/16 :goto_2d

    #@108
    .line 171
    .end local v1           #ret:I
    .restart local v0       #ret:I
    :cond_108
    iget-object v2, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mCurVibration:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;

    #@10a
    invoke-static {v2}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;->access$200(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;)[I

    #@10d
    move-result-object v2

    #@10e
    iget v4, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPatternIndex:I

    #@110
    aget v0, v2, v4

    #@112
    .line 173
    const-string v2, "VolumeVibrator"

    #@114
    new-instance v4, Ljava/lang/StringBuilder;

    #@116
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@119
    const-string v5, "volume vibration pattern\'s value: "

    #@11b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11e
    move-result-object v4

    #@11f
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@122
    move-result-object v4

    #@123
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@126
    move-result-object v4

    #@127
    invoke-static {v2, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@12a
    .line 176
    iget v2, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPatternIndex:I

    #@12c
    iget-object v4, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mCurVibration:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;

    #@12e
    invoke-static {v4}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;->access$100(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;)[J

    #@131
    move-result-object v4

    #@132
    array-length v4, v4

    #@133
    add-int/lit8 v4, v4, -0x1

    #@135
    if-ne v2, v4, :cond_104

    #@137
    .line 177
    invoke-direct {p0}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->clearLocked()V

    #@13a
    goto :goto_104

    #@13b
    .line 199
    :catchall_13b
    move-exception v2

    #@13c
    monitor-exit v3
    :try_end_13d
    .catchall {:try_start_1c .. :try_end_13d} :catchall_13b

    #@13d
    throw v2

    #@13e
    .line 180
    :cond_13e
    :try_start_13e
    iget-object v2, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mVolumes:Ljava/util/LinkedList;

    #@140
    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    #@143
    move-result v2

    #@144
    if-lez v2, :cond_188

    #@146
    .line 181
    iget-object v2, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mVolumes:Ljava/util/LinkedList;

    #@148
    invoke-virtual {v2}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    #@14b
    move-result-object v2

    #@14c
    check-cast v2, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;

    #@14e
    iput-object v2, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mCurVibration:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;

    #@150
    .line 182
    iget-object v2, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mCurVibration:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;

    #@152
    invoke-static {v2}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;->access$100(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;)[J

    #@155
    move-result-object v2

    #@156
    const/4 v4, 0x1

    #@157
    aget-wide v4, v2, v4

    #@159
    int-to-long v6, p1

    #@15a
    cmp-long v2, v4, v6

    #@15c
    if-nez v2, :cond_104

    #@15e
    .line 183
    const/4 v2, 0x1

    #@15f
    iput-boolean v2, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPatternWorking:Z

    #@161
    .line 184
    const/4 v2, 0x1

    #@162
    iput v2, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPatternIndex:I

    #@164
    .line 185
    iget-object v2, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mCurVibration:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;

    #@166
    invoke-static {v2}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;->access$200(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;)[I

    #@169
    move-result-object v2

    #@16a
    iget v4, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPatternIndex:I

    #@16c
    aget v0, v2, v4

    #@16e
    .line 187
    const-string v2, "VolumeVibrator"

    #@170
    new-instance v4, Ljava/lang/StringBuilder;

    #@172
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@175
    const-string v5, "volume vibration pattern\'s value: "

    #@177
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17a
    move-result-object v4

    #@17b
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17e
    move-result-object v4

    #@17f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@182
    move-result-object v4

    #@183
    invoke-static {v2, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@186
    goto/16 :goto_104

    #@188
    .line 190
    :cond_188
    iget-boolean v2, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mJustVibrate:Z

    #@18a
    if-eqz v2, :cond_104

    #@18c
    .line 191
    iget v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mOrgVolume:I

    #@18e
    .line 192
    const/4 v2, 0x0

    #@18f
    iput-boolean v2, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mJustVibrate:Z

    #@191
    .line 194
    const-string v2, "VolumeVibrator"

    #@193
    new-instance v4, Ljava/lang/StringBuilder;

    #@195
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@198
    const-string v5, "getCurrent::normal volume vibration: "

    #@19a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19d
    move-result-object v4

    #@19e
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a1
    move-result-object v4

    #@1a2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a5
    move-result-object v4

    #@1a6
    invoke-static {v2, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1a9
    .catchall {:try_start_13e .. :try_end_1a9} :catchall_13b

    #@1a9
    goto/16 :goto_104
.end method

.method public getVibrateVolume()I
    .registers 4

    #@0
    .prologue
    .line 305
    invoke-direct {p0}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->getSharedPreferences()Landroid/content/SharedPreferences;

    #@3
    move-result-object v0

    #@4
    if-nez v0, :cond_f

    #@6
    .line 306
    const-string v0, "VolumeVibrator"

    #@8
    const-string v1, "getSharedPreferences is null"

    #@a
    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 307
    const/4 v0, -0x1

    #@e
    .line 309
    :goto_e
    return v0

    #@f
    :cond_f
    iget-object v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPreference:Landroid/content/SharedPreferences;

    #@11
    sget-object v1, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->VOLUME_VIBRATE_ALL:[Ljava/lang/String;

    #@13
    const/4 v2, 0x0

    #@14
    aget-object v1, v1, v2

    #@16
    const/4 v2, 0x5

    #@17
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    #@1a
    move-result v0

    #@1b
    goto :goto_e
.end method

.method public getVibrateVolumeType(I)I
    .registers 5
    .parameter "vibrateType"

    #@0
    .prologue
    .line 338
    invoke-direct {p0}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->getSharedPreferences()Landroid/content/SharedPreferences;

    #@3
    move-result-object v0

    #@4
    if-nez v0, :cond_f

    #@6
    .line 339
    const-string v0, "VolumeVibrator"

    #@8
    const-string v1, "getSharedPreferences is null"

    #@a
    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 340
    const/4 v0, -0x1

    #@e
    .line 342
    :goto_e
    return v0

    #@f
    :cond_f
    iget-object v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPreference:Landroid/content/SharedPreferences;

    #@11
    sget-object v1, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->VOLUME_VIBRATE_ALL:[Ljava/lang/String;

    #@13
    aget-object v1, v1, p1

    #@15
    const/4 v2, 0x5

    #@16
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    #@19
    move-result v0

    #@1a
    goto :goto_e
.end method

.method public onCancel()V
    .registers 1

    #@0
    .prologue
    .line 123
    return-void
.end method

.method public onVibratorExcuted(I)V
    .registers 8
    .parameter "milliseconds"

    #@0
    .prologue
    .line 203
    const-string v0, "VolumeVibrator"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "onVibratorExcuted milliseconds : "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 204
    iget-object v1, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->lock:Ljava/lang/Object;

    #@1a
    monitor-enter v1

    #@1b
    .line 205
    :try_start_1b
    iget-boolean v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPatternWorking:Z

    #@1d
    if-eqz v0, :cond_2c

    #@1f
    iget-object v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mCurVibration:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;

    #@21
    if-nez v0, :cond_2c

    #@23
    .line 206
    const-string v0, "VolumeVibrator"

    #@25
    const-string v2, "onVibratorExcuted::mCurVibration is null"

    #@27
    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 207
    monitor-exit v1

    #@2b
    .line 232
    :goto_2b
    return-void

    #@2c
    .line 210
    :cond_2c
    iget-boolean v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPatternWorking:Z

    #@2e
    if-eqz v0, :cond_bb

    #@30
    .line 211
    iget v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPatternIndex:I

    #@32
    add-int/lit8 v0, v0, 0x2

    #@34
    iput v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPatternIndex:I

    #@36
    .line 212
    iget-object v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mCurVibration:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;

    #@38
    invoke-static {v0}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;->access$000(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;)I

    #@3b
    move-result v0

    #@3c
    if-ltz v0, :cond_53

    #@3e
    iget v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPatternIndex:I

    #@40
    iget-object v2, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mCurVibration:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;

    #@42
    invoke-static {v2}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;->access$100(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;)[J

    #@45
    move-result-object v2

    #@46
    array-length v2, v2

    #@47
    if-lt v0, v2, :cond_53

    #@49
    .line 213
    iget-object v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mCurVibration:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;

    #@4b
    invoke-static {v0}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;->access$000(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;)I

    #@4e
    move-result v0

    #@4f
    add-int/lit8 v0, v0, 0x1

    #@51
    iput v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPatternIndex:I

    #@53
    .line 215
    :cond_53
    const-string v0, "VolumeVibrator"

    #@55
    new-instance v2, Ljava/lang/StringBuilder;

    #@57
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5a
    const-string v3, "mPatternIndex : "

    #@5c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v2

    #@60
    iget v3, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPatternIndex:I

    #@62
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@65
    move-result-object v2

    #@66
    const-string v3, " mCurVibration.mPattern[mPatternIndex] : "

    #@68
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v2

    #@6c
    iget-object v3, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mCurVibration:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;

    #@6e
    invoke-static {v3}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;->access$100(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;)[J

    #@71
    move-result-object v3

    #@72
    iget v4, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPatternIndex:I

    #@74
    aget-wide v3, v3, v4

    #@76
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@79
    move-result-object v2

    #@7a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v2

    #@7e
    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@81
    .line 216
    int-to-long v2, p1

    #@82
    iget-object v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mCurVibration:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;

    #@84
    invoke-static {v0}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;->access$100(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;)[J

    #@87
    move-result-object v0

    #@88
    iget v4, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPatternIndex:I

    #@8a
    aget-wide v4, v0, v4

    #@8c
    cmp-long v0, v2, v4

    #@8e
    if-nez v0, :cond_a5

    #@90
    iget-object v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mCurVibration:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;

    #@92
    invoke-static {v0}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;->access$000(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;)I

    #@95
    move-result v0

    #@96
    if-gez v0, :cond_ad

    #@98
    iget v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPatternIndex:I

    #@9a
    iget-object v2, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mCurVibration:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;

    #@9c
    invoke-static {v2}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;->access$100(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;)[J

    #@9f
    move-result-object v2

    #@a0
    array-length v2, v2

    #@a1
    add-int/lit8 v2, v2, -0x2

    #@a3
    if-lt v0, v2, :cond_ad

    #@a5
    .line 218
    :cond_a5
    invoke-direct {p0}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->clearLocked()V

    #@a8
    .line 231
    :cond_a8
    :goto_a8
    monitor-exit v1

    #@a9
    goto :goto_2b

    #@aa
    :catchall_aa
    move-exception v0

    #@ab
    monitor-exit v1
    :try_end_ac
    .catchall {:try_start_1b .. :try_end_ac} :catchall_aa

    #@ac
    throw v0

    #@ad
    .line 221
    :cond_ad
    :try_start_ad
    iget-object v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mCurVibration:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;

    #@af
    invoke-static {v0}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;->access$200(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;)[I

    #@b2
    move-result-object v0

    #@b3
    iget v2, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPatternIndex:I

    #@b5
    aget v0, v0, v2

    #@b7
    invoke-static {v0}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->setVibratorVolumeNative(I)V

    #@ba
    goto :goto_a8

    #@bb
    .line 224
    :cond_bb
    iget-object v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mVolumes:Ljava/util/LinkedList;

    #@bd
    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    #@c0
    move-result v0

    #@c1
    if-lez v0, :cond_a8

    #@c3
    .line 225
    iget-object v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mVolumes:Ljava/util/LinkedList;

    #@c5
    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    #@c8
    move-result-object v0

    #@c9
    check-cast v0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;

    #@cb
    iput-object v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mCurVibration:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;

    #@cd
    .line 226
    iget-object v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mCurVibration:Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;

    #@cf
    invoke-static {v0}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;->access$100(Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService$VolumeVibration;)[J

    #@d2
    move-result-object v0

    #@d3
    const/4 v2, 0x1

    #@d4
    aget-wide v2, v0, v2

    #@d6
    int-to-long v4, p1

    #@d7
    cmp-long v0, v2, v4

    #@d9
    if-nez v0, :cond_a8

    #@db
    .line 227
    const/4 v0, 0x1

    #@dc
    iput-boolean v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPatternWorking:Z

    #@de
    .line 228
    const/4 v0, 0x1

    #@df
    iput v0, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPatternIndex:I
    :try_end_e1
    .catchall {:try_start_ad .. :try_end_e1} :catchall_aa

    #@e1
    goto :goto_a8
.end method

.method public setVibrateVolume(I)V
    .registers 5
    .parameter "volumeIndex"

    #@0
    .prologue
    .line 288
    invoke-direct {p0}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->getSharedPreferences()Landroid/content/SharedPreferences;

    #@3
    move-result-object v1

    #@4
    if-nez v1, :cond_e

    #@6
    .line 289
    const-string v1, "VolumeVibrator"

    #@8
    const-string v2, "getSharedPreferences is null"

    #@a
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 295
    :goto_d
    return-void

    #@e
    .line 292
    :cond_e
    iget-object v1, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPreference:Landroid/content/SharedPreferences;

    #@10
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@13
    move-result-object v0

    #@14
    .line 293
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    sget-object v1, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->VOLUME_VIBRATE_ALL:[Ljava/lang/String;

    #@16
    const/4 v2, 0x0

    #@17
    aget-object v1, v1, v2

    #@19
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    #@1c
    .line 294
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    #@1f
    goto :goto_d
.end method

.method public setVibrateVolumeType(II)V
    .registers 6
    .parameter "vibrateType"
    .parameter "volumeIndex"

    #@0
    .prologue
    .line 321
    invoke-direct {p0}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->getSharedPreferences()Landroid/content/SharedPreferences;

    #@3
    move-result-object v1

    #@4
    if-nez v1, :cond_e

    #@6
    .line 322
    const-string v1, "VolumeVibrator"

    #@8
    const-string v2, "getSharedPreferences is null"

    #@a
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 328
    :goto_d
    return-void

    #@e
    .line 325
    :cond_e
    iget-object v1, p0, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->mPreference:Landroid/content/SharedPreferences;

    #@10
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@13
    move-result-object v0

    #@14
    .line 326
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    sget-object v1, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->VOLUME_VIBRATE_ALL:[Ljava/lang/String;

    #@16
    aget-object v1, v1, p1

    #@18
    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    #@1b
    .line 327
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    #@1e
    goto :goto_d
.end method

.method public vibrate(JILandroid/os/IBinder;)V
    .registers 5
    .parameter "milliseconds"
    .parameter "volume"
    .parameter "token"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 269
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->vibrateInternal(JILandroid/os/IBinder;)V

    #@3
    .line 270
    return-void
.end method

.method public vibratePattern([JI[ILandroid/os/IBinder;)V
    .registers 5
    .parameter "pattern"
    .parameter "repeat"
    .parameter "volumePattern"
    .parameter "token"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 273
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/lge/systemservice/service/volumevibrator/VolumeVibratorService;->vibratePatternInternal([JI[ILandroid/os/IBinder;)V

    #@3
    .line 274
    return-void
.end method
