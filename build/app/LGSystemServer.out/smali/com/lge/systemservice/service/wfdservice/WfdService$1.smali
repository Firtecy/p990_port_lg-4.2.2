.class Lcom/lge/systemservice/service/wfdservice/WfdService$1;
.super Landroid/content/BroadcastReceiver;
.source "WfdService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/wfdservice/WfdService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/wfdservice/WfdService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 75
    iput-object p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 18
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 78
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 80
    .local v1, action:Ljava/lang/String;
    const-string v12, "android.net.wifi.p2p.STATE_CHANGED"

    #@6
    invoke-virtual {v12, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v12

    #@a
    if-eqz v12, :cond_99

    #@c
    .line 81
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@e
    const-string v13, "wifi_p2p_state"

    #@10
    const/4 v14, 0x1

    #@11
    move-object/from16 v0, p2

    #@13
    invoke-virtual {v0, v13, v14}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@16
    move-result v13

    #@17
    invoke-static {v12, v13}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$002(Lcom/lge/systemservice/service/wfdservice/WfdService;I)I

    #@1a
    .line 83
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@1c
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$000(Lcom/lge/systemservice/service/wfdservice/WfdService;)I

    #@1f
    move-result v12

    #@20
    const/4 v13, 0x1

    #@21
    if-ne v12, v13, :cond_4a

    #@23
    .line 84
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@25
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$100(Lcom/lge/systemservice/service/wfdservice/WfdService;)I

    #@28
    move-result v12

    #@29
    const/4 v13, 0x1

    #@2a
    if-ne v12, v13, :cond_42

    #@2c
    .line 85
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@2e
    const/4 v13, 0x0

    #@2f
    invoke-static {v12, v13}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$202(Lcom/lge/systemservice/service/wfdservice/WfdService;I)I

    #@32
    .line 86
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@34
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdService;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@37
    move-result-object v12

    #@38
    iget-object v13, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@3a
    invoke-static {v13}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$200(Lcom/lge/systemservice/service/wfdservice/WfdService;)I

    #@3d
    move-result v13

    #@3e
    invoke-virtual {v12, v13}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->handleDirectStateChangedEvent(I)V

    #@41
    .line 237
    :cond_41
    :goto_41
    return-void

    #@42
    .line 88
    :cond_42
    const-string v12, "WfdService"

    #@44
    const-string v13, "Waiting for Wifi disabling"

    #@46
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@49
    goto :goto_41

    #@4a
    .line 90
    :cond_4a
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@4c
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$000(Lcom/lge/systemservice/service/wfdservice/WfdService;)I

    #@4f
    move-result v12

    #@50
    const/4 v13, 0x2

    #@51
    if-ne v12, v13, :cond_7a

    #@53
    .line 91
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@55
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$100(Lcom/lge/systemservice/service/wfdservice/WfdService;)I

    #@58
    move-result v12

    #@59
    const/4 v13, 0x3

    #@5a
    if-ne v12, v13, :cond_72

    #@5c
    .line 92
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@5e
    const/4 v13, 0x2

    #@5f
    invoke-static {v12, v13}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$202(Lcom/lge/systemservice/service/wfdservice/WfdService;I)I

    #@62
    .line 93
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@64
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdService;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@67
    move-result-object v12

    #@68
    iget-object v13, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@6a
    invoke-static {v13}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$200(Lcom/lge/systemservice/service/wfdservice/WfdService;)I

    #@6d
    move-result v13

    #@6e
    invoke-virtual {v12, v13}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->handleDirectStateChangedEvent(I)V

    #@71
    goto :goto_41

    #@72
    .line 95
    :cond_72
    const-string v12, "WfdService"

    #@74
    const-string v13, "Waiting for Wifi enabling"

    #@76
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@79
    goto :goto_41

    #@7a
    .line 98
    :cond_7a
    const-string v12, "WfdService"

    #@7c
    new-instance v13, Ljava/lang/StringBuilder;

    #@7e
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@81
    const-string v14, "Unexpected WifiP2pState Received: "

    #@83
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v13

    #@87
    iget-object v14, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@89
    invoke-static {v14}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$000(Lcom/lge/systemservice/service/wfdservice/WfdService;)I

    #@8c
    move-result v14

    #@8d
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@90
    move-result-object v13

    #@91
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v13

    #@95
    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@98
    goto :goto_41

    #@99
    .line 100
    :cond_99
    const-string v12, "android.net.wifi.WIFI_STATE_CHANGED"

    #@9b
    invoke-virtual {v12, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9e
    move-result v12

    #@9f
    if-eqz v12, :cond_152

    #@a1
    .line 101
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@a3
    const-string v13, "wifi_state"

    #@a5
    const/4 v14, 0x1

    #@a6
    move-object/from16 v0, p2

    #@a8
    invoke-virtual {v0, v13, v14}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@ab
    move-result v13

    #@ac
    invoke-static {v12, v13}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$102(Lcom/lge/systemservice/service/wfdservice/WfdService;I)I

    #@af
    .line 103
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@b1
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$100(Lcom/lge/systemservice/service/wfdservice/WfdService;)I

    #@b4
    move-result v12

    #@b5
    const/4 v13, 0x1

    #@b6
    if-ne v12, v13, :cond_e1

    #@b8
    .line 104
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@ba
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$000(Lcom/lge/systemservice/service/wfdservice/WfdService;)I

    #@bd
    move-result v12

    #@be
    const/4 v13, 0x1

    #@bf
    if-ne v12, v13, :cond_d8

    #@c1
    .line 105
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@c3
    const/4 v13, 0x0

    #@c4
    invoke-static {v12, v13}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$202(Lcom/lge/systemservice/service/wfdservice/WfdService;I)I

    #@c7
    .line 106
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@c9
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdService;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@cc
    move-result-object v12

    #@cd
    iget-object v13, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@cf
    invoke-static {v13}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$200(Lcom/lge/systemservice/service/wfdservice/WfdService;)I

    #@d2
    move-result v13

    #@d3
    invoke-virtual {v12, v13}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->handleDirectStateChangedEvent(I)V

    #@d6
    goto/16 :goto_41

    #@d8
    .line 108
    :cond_d8
    const-string v12, "WfdService"

    #@da
    const-string v13, "Waiting for WifiP2p disabling"

    #@dc
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@df
    goto/16 :goto_41

    #@e1
    .line 110
    :cond_e1
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@e3
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$100(Lcom/lge/systemservice/service/wfdservice/WfdService;)I

    #@e6
    move-result v12

    #@e7
    const/4 v13, 0x3

    #@e8
    if-ne v12, v13, :cond_113

    #@ea
    .line 111
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@ec
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$000(Lcom/lge/systemservice/service/wfdservice/WfdService;)I

    #@ef
    move-result v12

    #@f0
    const/4 v13, 0x2

    #@f1
    if-ne v12, v13, :cond_10a

    #@f3
    .line 112
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@f5
    const/4 v13, 0x2

    #@f6
    invoke-static {v12, v13}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$202(Lcom/lge/systemservice/service/wfdservice/WfdService;I)I

    #@f9
    .line 113
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@fb
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdService;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@fe
    move-result-object v12

    #@ff
    iget-object v13, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@101
    invoke-static {v13}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$200(Lcom/lge/systemservice/service/wfdservice/WfdService;)I

    #@104
    move-result v13

    #@105
    invoke-virtual {v12, v13}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->handleDirectStateChangedEvent(I)V

    #@108
    goto/16 :goto_41

    #@10a
    .line 115
    :cond_10a
    const-string v12, "WfdService"

    #@10c
    const-string v13, "Waiting for WifiP2p enabling"

    #@10e
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@111
    goto/16 :goto_41

    #@113
    .line 117
    :cond_113
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@115
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$100(Lcom/lge/systemservice/service/wfdservice/WfdService;)I

    #@118
    move-result v12

    #@119
    const/4 v13, 0x2

    #@11a
    if-ne v12, v13, :cond_133

    #@11c
    .line 118
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@11e
    const/4 v13, 0x1

    #@11f
    invoke-static {v12, v13}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$202(Lcom/lge/systemservice/service/wfdservice/WfdService;I)I

    #@122
    .line 119
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@124
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdService;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@127
    move-result-object v12

    #@128
    iget-object v13, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@12a
    invoke-static {v13}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$200(Lcom/lge/systemservice/service/wfdservice/WfdService;)I

    #@12d
    move-result v13

    #@12e
    invoke-virtual {v12, v13}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->handleDirectStateChangedEvent(I)V

    #@131
    goto/16 :goto_41

    #@133
    .line 120
    :cond_133
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@135
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$100(Lcom/lge/systemservice/service/wfdservice/WfdService;)I

    #@138
    move-result v12

    #@139
    if-nez v12, :cond_41

    #@13b
    .line 121
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@13d
    const/4 v13, 0x6

    #@13e
    invoke-static {v12, v13}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$202(Lcom/lge/systemservice/service/wfdservice/WfdService;I)I

    #@141
    .line 122
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@143
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdService;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@146
    move-result-object v12

    #@147
    iget-object v13, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@149
    invoke-static {v13}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$200(Lcom/lge/systemservice/service/wfdservice/WfdService;)I

    #@14c
    move-result v13

    #@14d
    invoke-virtual {v12, v13}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->handleDirectStateChangedEvent(I)V

    #@150
    goto/16 :goto_41

    #@152
    .line 124
    :cond_152
    const-string v12, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    #@154
    invoke-virtual {v12, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@157
    move-result v12

    #@158
    if-eqz v12, :cond_1ca

    #@15a
    .line 125
    const-string v12, "networkInfo"

    #@15c
    move-object/from16 v0, p2

    #@15e
    invoke-virtual {v0, v12}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@161
    move-result-object v7

    #@162
    check-cast v7, Landroid/net/NetworkInfo;

    #@164
    .line 127
    .local v7, networkInfo:Landroid/net/NetworkInfo;
    if-eqz v7, :cond_1a3

    #@166
    .line 128
    invoke-virtual {v7}, Landroid/net/NetworkInfo;->isConnected()Z

    #@169
    move-result v12

    #@16a
    if-eqz v12, :cond_183

    #@16c
    .line 129
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@16e
    const/4 v13, 0x4

    #@16f
    invoke-static {v12, v13}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$202(Lcom/lge/systemservice/service/wfdservice/WfdService;I)I

    #@172
    .line 130
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@174
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdService;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@177
    move-result-object v12

    #@178
    iget-object v13, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@17a
    invoke-static {v13}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$200(Lcom/lge/systemservice/service/wfdservice/WfdService;)I

    #@17d
    move-result v13

    #@17e
    invoke-virtual {v12, v13}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->handleDirectStateChangedEvent(I)V

    #@181
    goto/16 :goto_41

    #@183
    .line 132
    :cond_183
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@185
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$200(Lcom/lge/systemservice/service/wfdservice/WfdService;)I

    #@188
    move-result v12

    #@189
    const/4 v13, 0x4

    #@18a
    if-ne v12, v13, :cond_41

    #@18c
    .line 133
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@18e
    const/4 v13, 0x2

    #@18f
    invoke-static {v12, v13}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$202(Lcom/lge/systemservice/service/wfdservice/WfdService;I)I

    #@192
    .line 134
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@194
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdService;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@197
    move-result-object v12

    #@198
    iget-object v13, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@19a
    invoke-static {v13}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$200(Lcom/lge/systemservice/service/wfdservice/WfdService;)I

    #@19d
    move-result v13

    #@19e
    invoke-virtual {v12, v13}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->handleDirectStateChangedEvent(I)V

    #@1a1
    goto/16 :goto_41

    #@1a3
    .line 138
    :cond_1a3
    const-string v12, "WfdService"

    #@1a5
    const-string v13, "WIFI_P2P_CONNECTION_CHANGED_ACTION: networkInfo is null"

    #@1a7
    invoke-static {v12, v13}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1aa
    .line 139
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@1ac
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$200(Lcom/lge/systemservice/service/wfdservice/WfdService;)I

    #@1af
    move-result v12

    #@1b0
    const/4 v13, 0x4

    #@1b1
    if-ne v12, v13, :cond_41

    #@1b3
    .line 140
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@1b5
    const/4 v13, 0x2

    #@1b6
    invoke-static {v12, v13}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$202(Lcom/lge/systemservice/service/wfdservice/WfdService;I)I

    #@1b9
    .line 141
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@1bb
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdService;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@1be
    move-result-object v12

    #@1bf
    iget-object v13, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@1c1
    invoke-static {v13}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$200(Lcom/lge/systemservice/service/wfdservice/WfdService;)I

    #@1c4
    move-result v13

    #@1c5
    invoke-virtual {v12, v13}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->handleDirectStateChangedEvent(I)V

    #@1c8
    goto/16 :goto_41

    #@1ca
    .line 146
    .end local v7           #networkInfo:Landroid/net/NetworkInfo;
    :cond_1ca
    const-string v12, "android.net.wifi.p2p.THIS_DEVICE_CHANGED"

    #@1cc
    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1cf
    move-result v12

    #@1d0
    if-eqz v12, :cond_1e7

    #@1d2
    .line 147
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@1d4
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdService;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@1d7
    move-result-object v13

    #@1d8
    const-string v12, "wifiP2pDevice"

    #@1da
    move-object/from16 v0, p2

    #@1dc
    invoke-virtual {v0, v12}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@1df
    move-result-object v12

    #@1e0
    check-cast v12, Landroid/net/wifi/p2p/WifiP2pDevice;

    #@1e2
    invoke-virtual {v13, v12}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->handleP2pThisDeviceChanged(Landroid/net/wifi/p2p/WifiP2pDevice;)V

    #@1e5
    goto/16 :goto_41

    #@1e7
    .line 149
    :cond_1e7
    const-string v12, "com.lge.systemservice.core.wfdmanager.WFD_REQUEST_WIFI_ENABLED_ACTION"

    #@1e9
    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1ec
    move-result v12

    #@1ed
    if-eqz v12, :cond_203

    #@1ef
    .line 150
    const-string v12, "wifi_feature"

    #@1f1
    const/4 v13, 0x0

    #@1f2
    move-object/from16 v0, p2

    #@1f4
    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@1f7
    move-result v11

    #@1f8
    .line 151
    .local v11, wifiFeature:I
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@1fa
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdService;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@1fd
    move-result-object v12

    #@1fe
    invoke-virtual {v12, v11}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->handleRequestSetWifiEnabled(I)V

    #@201
    goto/16 :goto_41

    #@203
    .line 153
    .end local v11           #wifiFeature:I
    :cond_203
    const-string v12, "android.net.wifi.STATE_CHANGE"

    #@205
    invoke-virtual {v12, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@208
    move-result v12

    #@209
    if-eqz v12, :cond_251

    #@20b
    .line 154
    const-string v12, "networkInfo"

    #@20d
    move-object/from16 v0, p2

    #@20f
    invoke-virtual {v0, v12}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@212
    move-result-object v4

    #@213
    check-cast v4, Landroid/net/NetworkInfo;

    #@215
    .line 155
    .local v4, info:Landroid/net/NetworkInfo;
    if-eqz v4, :cond_41

    #@217
    .line 156
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isConnected()Z

    #@21a
    move-result v12

    #@21b
    if-eqz v12, :cond_237

    #@21d
    .line 157
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@21f
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$400(Lcom/lge/systemservice/service/wfdservice/WfdService;)Z

    #@222
    move-result v12

    #@223
    if-nez v12, :cond_41

    #@225
    .line 158
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@227
    const/4 v13, 0x1

    #@228
    invoke-static {v12, v13}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$402(Lcom/lge/systemservice/service/wfdservice/WfdService;Z)Z

    #@22b
    .line 159
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@22d
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdService;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@230
    move-result-object v12

    #@231
    const/4 v13, 0x1

    #@232
    invoke-virtual {v12, v13}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->handleWifiStateChangedEvent(I)V

    #@235
    goto/16 :goto_41

    #@237
    .line 162
    :cond_237
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@239
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$400(Lcom/lge/systemservice/service/wfdservice/WfdService;)Z

    #@23c
    move-result v12

    #@23d
    if-eqz v12, :cond_41

    #@23f
    .line 163
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@241
    const/4 v13, 0x0

    #@242
    invoke-static {v12, v13}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$402(Lcom/lge/systemservice/service/wfdservice/WfdService;Z)Z

    #@245
    .line 164
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@247
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdService;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@24a
    move-result-object v12

    #@24b
    const/4 v13, 0x0

    #@24c
    invoke-virtual {v12, v13}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->handleWifiStateChangedEvent(I)V

    #@24f
    goto/16 :goto_41

    #@251
    .line 168
    .end local v4           #info:Landroid/net/NetworkInfo;
    :cond_251
    const-string v12, "lge.wfd.switch.start"

    #@253
    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@256
    move-result v12

    #@257
    if-eqz v12, :cond_265

    #@259
    .line 169
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@25b
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdService;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@25e
    move-result-object v12

    #@25f
    const/4 v13, 0x1

    #@260
    invoke-virtual {v12, v13}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->handleDlnaTransitRequest(Z)V

    #@263
    goto/16 :goto_41

    #@265
    .line 170
    :cond_265
    const-string v12, "lge.wfd.switch.stop"

    #@267
    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@26a
    move-result v12

    #@26b
    if-eqz v12, :cond_279

    #@26d
    .line 171
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@26f
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdService;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@272
    move-result-object v12

    #@273
    const/4 v13, 0x0

    #@274
    invoke-virtual {v12, v13}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->handleDlnaTransitRequest(Z)V

    #@277
    goto/16 :goto_41

    #@279
    .line 172
    :cond_279
    const-string v12, "com.lge.systemservice.core.wfdmanager.WFD_ENABLE"

    #@27b
    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@27e
    move-result v12

    #@27f
    if-eqz v12, :cond_29d

    #@281
    .line 173
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@283
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdService;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@286
    move-result-object v12

    #@287
    const-string v13, "ignore_direct_connection"

    #@289
    const/4 v14, 0x0

    #@28a
    move-object/from16 v0, p2

    #@28c
    invoke-virtual {v0, v13, v14}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@28f
    move-result v13

    #@290
    const-string v14, "connected_udn"

    #@292
    move-object/from16 v0, p2

    #@294
    invoke-virtual {v0, v14}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@297
    move-result-object v14

    #@298
    invoke-virtual {v12, v13, v14}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWifiDisplayEnabledNoDisconnect(ZLjava/lang/String;)Z

    #@29b
    goto/16 :goto_41

    #@29d
    .line 176
    :cond_29d
    const-string v12, "com.lge.systemservice.core.wfdmanager.WFD_DISABLE"

    #@29f
    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2a2
    move-result v12

    #@2a3
    if-eqz v12, :cond_2b1

    #@2a5
    .line 177
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@2a7
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdService;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@2aa
    move-result-object v12

    #@2ab
    const/4 v13, 0x0

    #@2ac
    invoke-virtual {v12, v13}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setWifiDisplayEnabled(Z)Z

    #@2af
    goto/16 :goto_41

    #@2b1
    .line 179
    :cond_2b1
    const-string v12, "android.intent.action.SCREEN_ON"

    #@2b3
    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2b6
    move-result v12

    #@2b7
    if-eqz v12, :cond_2c6

    #@2b9
    .line 180
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@2bb
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdService;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@2be
    move-result-object v12

    #@2bf
    const/4 v13, 0x0

    #@2c0
    const/4 v14, 0x1

    #@2c1
    invoke-virtual {v12, v13, v14}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->handlePauseEvent(ZI)V

    #@2c4
    goto/16 :goto_41

    #@2c6
    .line 181
    :cond_2c6
    const-string v12, "android.intent.action.SCREEN_OFF"

    #@2c8
    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2cb
    move-result v12

    #@2cc
    if-eqz v12, :cond_2db

    #@2ce
    .line 182
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@2d0
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdService;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@2d3
    move-result-object v12

    #@2d4
    const/4 v13, 0x1

    #@2d5
    const/4 v14, 0x1

    #@2d6
    invoke-virtual {v12, v13, v14}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->handlePauseEvent(ZI)V

    #@2d9
    goto/16 :goto_41

    #@2db
    .line 183
    :cond_2db
    const-string v12, "android.intent.action.BATTERY_CHANGED"

    #@2dd
    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2e0
    move-result v12

    #@2e1
    if-eqz v12, :cond_333

    #@2e3
    .line 184
    const-string v12, "level"

    #@2e5
    const/4 v13, -0x1

    #@2e6
    move-object/from16 v0, p2

    #@2e8
    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@2eb
    move-result v2

    #@2ec
    .line 185
    .local v2, battLevel:I
    const-string v12, "scale"

    #@2ee
    const/4 v13, 0x0

    #@2ef
    move-object/from16 v0, p2

    #@2f1
    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@2f4
    move-result v3

    #@2f5
    .line 186
    .local v3, battScale:I
    if-lez v3, :cond_41

    #@2f7
    if-ltz v2, :cond_41

    #@2f9
    .line 187
    mul-int/lit8 v12, v2, 0x64

    #@2fb
    div-int/2addr v12, v3

    #@2fc
    const/4 v13, 0x5

    #@2fd
    if-gt v12, v13, :cond_319

    #@2ff
    .line 188
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@301
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$500(Lcom/lge/systemservice/service/wfdservice/WfdService;)Z

    #@304
    move-result v12

    #@305
    if-nez v12, :cond_41

    #@307
    .line 189
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@309
    const/4 v13, 0x1

    #@30a
    invoke-static {v12, v13}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$502(Lcom/lge/systemservice/service/wfdservice/WfdService;Z)Z

    #@30d
    .line 190
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@30f
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdService;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@312
    move-result-object v12

    #@313
    const/4 v13, 0x1

    #@314
    invoke-virtual {v12, v13}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->handleBatteryEvent(Z)V

    #@317
    goto/16 :goto_41

    #@319
    .line 193
    :cond_319
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@31b
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$500(Lcom/lge/systemservice/service/wfdservice/WfdService;)Z

    #@31e
    move-result v12

    #@31f
    if-eqz v12, :cond_41

    #@321
    .line 194
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@323
    const/4 v13, 0x0

    #@324
    invoke-static {v12, v13}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$502(Lcom/lge/systemservice/service/wfdservice/WfdService;Z)Z

    #@327
    .line 195
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@329
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdService;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@32c
    move-result-object v12

    #@32d
    const/4 v13, 0x0

    #@32e
    invoke-virtual {v12, v13}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->handleBatteryEvent(Z)V

    #@331
    goto/16 :goto_41

    #@333
    .line 199
    .end local v2           #battLevel:I
    .end local v3           #battScale:I
    :cond_333
    const-string v12, "com.lge.ims.action.VT_STARTED"

    #@335
    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@338
    move-result v12

    #@339
    if-eqz v12, :cond_348

    #@33b
    .line 200
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@33d
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdService;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@340
    move-result-object v12

    #@341
    const/4 v13, 0x1

    #@342
    const/4 v14, 0x4

    #@343
    invoke-virtual {v12, v13, v14}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->handlePauseEvent(ZI)V

    #@346
    goto/16 :goto_41

    #@348
    .line 201
    :cond_348
    const-string v12, "com.lge.ims.action.VT_ENDED"

    #@34a
    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@34d
    move-result v12

    #@34e
    if-eqz v12, :cond_35d

    #@350
    .line 202
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@352
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdService;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@355
    move-result-object v12

    #@356
    const/4 v13, 0x0

    #@357
    const/4 v14, 0x4

    #@358
    invoke-virtual {v12, v13, v14}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->handlePauseEvent(ZI)V

    #@35b
    goto/16 :goto_41

    #@35d
    .line 203
    :cond_35d
    const-string v12, "com.lge.vt.HDVT_CALL_STATE_CHANGED"

    #@35f
    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@362
    move-result v12

    #@363
    if-eqz v12, :cond_3a7

    #@365
    .line 204
    const-string v12, "HDVT_CALL_STATE"

    #@367
    const/4 v13, 0x0

    #@368
    move-object/from16 v0, p2

    #@36a
    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@36d
    move-result v10

    #@36e
    .line 205
    .local v10, vtState:I
    const-string v12, "WfdService"

    #@370
    new-instance v13, Ljava/lang/StringBuilder;

    #@372
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@375
    const-string v14, "HDVTcall received state: "

    #@377
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37a
    move-result-object v13

    #@37b
    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@37e
    move-result-object v13

    #@37f
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@382
    move-result-object v13

    #@383
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@386
    .line 206
    packed-switch v10, :pswitch_data_432

    #@389
    goto/16 :goto_41

    #@38b
    .line 212
    :pswitch_38b
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@38d
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdService;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@390
    move-result-object v12

    #@391
    const/4 v13, 0x0

    #@392
    const/16 v14, 0x8

    #@394
    invoke-virtual {v12, v13, v14}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->handlePauseEvent(ZI)V

    #@397
    goto/16 :goto_41

    #@399
    .line 209
    :pswitch_399
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@39b
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdService;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@39e
    move-result-object v12

    #@39f
    const/4 v13, 0x1

    #@3a0
    const/16 v14, 0x8

    #@3a2
    invoke-virtual {v12, v13, v14}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->handlePauseEvent(ZI)V

    #@3a5
    goto/16 :goto_41

    #@3a7
    .line 217
    .end local v10           #vtState:I
    :cond_3a7
    const-string v12, "com.lge.systemservice.core.wfdmanager.WFD_INFORM_DRM_STATUS"

    #@3a9
    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3ac
    move-result v12

    #@3ad
    if-eqz v12, :cond_3ce

    #@3af
    .line 218
    const-string v12, "drm_player"

    #@3b1
    move-object/from16 v0, p2

    #@3b3
    invoke-virtual {v0, v12}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@3b6
    move-result-object v6

    #@3b7
    .line 219
    .local v6, moduleName:Ljava/lang/String;
    const-string v12, "drm_pause_req"

    #@3b9
    const/4 v13, 0x3

    #@3ba
    move-object/from16 v0, p2

    #@3bc
    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@3bf
    move-result v8

    #@3c0
    .line 220
    .local v8, pauseReq:I
    if-eqz v6, :cond_41

    #@3c2
    .line 221
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@3c4
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdService;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@3c7
    move-result-object v12

    #@3c8
    const/4 v13, 0x2

    #@3c9
    invoke-virtual {v12, v6, v8, v13}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->handleDRMPauseEvent(Ljava/lang/String;II)V

    #@3cc
    goto/16 :goto_41

    #@3ce
    .line 223
    .end local v6           #moduleName:Ljava/lang/String;
    .end local v8           #pauseReq:I
    :cond_3ce
    const-string v12, "com.lge.oneseg.wifi.display.disable"

    #@3d0
    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3d3
    move-result v12

    #@3d4
    if-eqz v12, :cond_3e4

    #@3d6
    .line 224
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@3d8
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdService;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@3db
    move-result-object v12

    #@3dc
    const/4 v13, 0x1

    #@3dd
    const/16 v14, 0x10

    #@3df
    invoke-virtual {v12, v13, v14}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->handlePauseEvent(ZI)V

    #@3e2
    goto/16 :goto_41

    #@3e4
    .line 225
    :cond_3e4
    const-string v12, "com.lge.oneseg.wifi.display.enable"

    #@3e6
    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3e9
    move-result v12

    #@3ea
    if-eqz v12, :cond_3fa

    #@3ec
    .line 226
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@3ee
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdService;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@3f1
    move-result-object v12

    #@3f2
    const/4 v13, 0x0

    #@3f3
    const/16 v14, 0x10

    #@3f5
    invoke-virtual {v12, v13, v14}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->handlePauseEvent(ZI)V

    #@3f8
    goto/16 :goto_41

    #@3fa
    .line 227
    :cond_3fa
    const-string v12, "android.intent.action.HDMI_PLUGGED"

    #@3fc
    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3ff
    move-result v12

    #@400
    if-eqz v12, :cond_416

    #@402
    .line 228
    const-string v12, "state"

    #@404
    const/4 v13, 0x0

    #@405
    move-object/from16 v0, p2

    #@407
    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@40a
    move-result v5

    #@40b
    .line 229
    .local v5, isCableConnected:Z
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@40d
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdService;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@410
    move-result-object v12

    #@411
    invoke-virtual {v12, v5}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->handleHdmiConnected(Z)V

    #@414
    goto/16 :goto_41

    #@416
    .line 231
    .end local v5           #isCableConnected:Z
    :cond_416
    const-string v12, "android.intent.action.PHONE_STATE"

    #@418
    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@41b
    move-result v12

    #@41c
    if-eqz v12, :cond_41

    #@41e
    .line 232
    const-string v12, "state"

    #@420
    move-object/from16 v0, p2

    #@422
    invoke-virtual {v0, v12}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@425
    move-result-object v9

    #@426
    .line 233
    .local v9, phoneState:Ljava/lang/String;
    iget-object v12, p0, Lcom/lge/systemservice/service/wfdservice/WfdService$1;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdService;

    #@428
    invoke-static {v12}, Lcom/lge/systemservice/service/wfdservice/WfdService;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdService;)Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@42b
    move-result-object v12

    #@42c
    invoke-virtual {v12, v9}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->setCallState(Ljava/lang/String;)V

    #@42f
    goto/16 :goto_41

    #@431
    .line 206
    nop

    #@432
    :pswitch_data_432
    .packed-switch 0x0
        :pswitch_38b
        :pswitch_399
        :pswitch_399
    .end packed-switch
.end method
