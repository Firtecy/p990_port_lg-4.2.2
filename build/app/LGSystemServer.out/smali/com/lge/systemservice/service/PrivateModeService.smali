.class public Lcom/lge/systemservice/service/PrivateModeService;
.super Landroid/app/Service;
.source "PrivateModeService.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/systemservice/service/PrivateModeService$PrivateModeEventReceiver;,
        Lcom/lge/systemservice/service/PrivateModeService$SettingsObserver;
    }
.end annotation


# static fields
.field private static final DEBUG:Z

.field private static final FACTORY_TEST_MODE:Z

.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mBackgroundResId:I

.field private mDisplayMetrics:Landroid/util/DisplayMetrics;

.field private mEnabled:Z

.field private mHandler:Landroid/os/Handler;

.field private mInCall:Z

.field private mParams:Landroid/view/WindowManager$LayoutParams;

.field private mPhoneStateReceiver:Landroid/content/BroadcastReceiver;

.field private mProximity:Landroid/hardware/Sensor;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mSensorTime:J

.field private mSettingsObserver:Lcom/lge/systemservice/service/PrivateModeService$SettingsObserver;

.field private mShown:Z

.field private mView:Landroid/view/View;

.field private mWM:Landroid/view/WindowManager;

.field private mWindowHeight:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 41
    const-class v0, Lcom/lge/systemservice/service/PrivateModeService;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/lge/systemservice/service/PrivateModeService;->LOG_TAG:Ljava/lang/String;

    #@8
    .line 42
    const-string v0, "user"

    #@a
    const-string v1, "ro.build.type"

    #@c
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13
    move-result v0

    #@14
    if-nez v0, :cond_2a

    #@16
    const/4 v0, 0x1

    #@17
    :goto_17
    sput-boolean v0, Lcom/lge/systemservice/service/PrivateModeService;->DEBUG:Z

    #@19
    .line 71
    const-string v0, "1"

    #@1b
    const-string v1, "ro.factorytest"

    #@1d
    const-string v2, "0"

    #@1f
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@26
    move-result v0

    #@27
    sput-boolean v0, Lcom/lge/systemservice/service/PrivateModeService;->FACTORY_TEST_MODE:Z

    #@29
    return-void

    #@2a
    .line 42
    :cond_2a
    const/4 v0, 0x0

    #@2b
    goto :goto_17
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    .line 40
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    #@3
    .line 50
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    #@5
    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    #@8
    iput-object v0, p0, Lcom/lge/systemservice/service/PrivateModeService;->mParams:Landroid/view/WindowManager$LayoutParams;

    #@a
    .line 51
    new-instance v0, Landroid/util/DisplayMetrics;

    #@c
    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    #@f
    iput-object v0, p0, Lcom/lge/systemservice/service/PrivateModeService;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    #@11
    .line 62
    const-wide/16 v0, 0x0

    #@13
    iput-wide v0, p0, Lcom/lge/systemservice/service/PrivateModeService;->mSensorTime:J

    #@15
    .line 185
    return-void
.end method

.method static synthetic access$000()Z
    .registers 1

    #@0
    .prologue
    .line 40
    sget-boolean v0, Lcom/lge/systemservice/service/PrivateModeService;->DEBUG:Z

    #@2
    return v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 40
    sget-object v0, Lcom/lge/systemservice/service/PrivateModeService;->LOG_TAG:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/lge/systemservice/service/PrivateModeService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 40
    invoke-direct {p0}, Lcom/lge/systemservice/service/PrivateModeService;->updateSettings()V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/lge/systemservice/service/PrivateModeService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/lge/systemservice/service/PrivateModeService;->mInCall:Z

    #@2
    return v0
.end method

.method static synthetic access$302(Lcom/lge/systemservice/service/PrivateModeService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/lge/systemservice/service/PrivateModeService;->mInCall:Z

    #@2
    return p1
.end method

.method static synthetic access$400(Lcom/lge/systemservice/service/PrivateModeService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/lge/systemservice/service/PrivateModeService;->mShown:Z

    #@2
    return v0
.end method

.method static synthetic access$500(Lcom/lge/systemservice/service/PrivateModeService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 40
    invoke-direct {p0}, Lcom/lge/systemservice/service/PrivateModeService;->startPrivateMode()V

    #@3
    return-void
.end method

.method static synthetic access$600(Lcom/lge/systemservice/service/PrivateModeService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 40
    invoke-direct {p0}, Lcom/lge/systemservice/service/PrivateModeService;->stopPrivateMode()V

    #@3
    return-void
.end method

.method private startPrivateMode()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 223
    iget-boolean v2, p0, Lcom/lge/systemservice/service/PrivateModeService;->mEnabled:Z

    #@3
    if-nez v2, :cond_6

    #@5
    .line 247
    :goto_5
    return-void

    #@6
    .line 225
    :cond_6
    new-instance v2, Landroid/app/Notification;

    #@8
    invoke-direct {v2}, Landroid/app/Notification;-><init>()V

    #@b
    invoke-virtual {p0, v4, v2}, Lcom/lge/systemservice/service/PrivateModeService;->startForeground(ILandroid/app/Notification;)V

    #@e
    .line 227
    new-instance v2, Landroid/view/View;

    #@10
    invoke-direct {v2, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    #@13
    iput-object v2, p0, Lcom/lge/systemservice/service/PrivateModeService;->mView:Landroid/view/View;

    #@15
    .line 228
    iget-object v2, p0, Lcom/lge/systemservice/service/PrivateModeService;->mView:Landroid/view/View;

    #@17
    iget v3, p0, Lcom/lge/systemservice/service/PrivateModeService;->mBackgroundResId:I

    #@19
    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    #@1c
    .line 229
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    #@1e
    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    #@21
    .line 230
    .local v0, options:Landroid/graphics/BitmapFactory$Options;
    iput-boolean v4, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    #@23
    .line 231
    invoke-virtual {p0}, Lcom/lge/systemservice/service/PrivateModeService;->getResources()Landroid/content/res/Resources;

    #@26
    move-result-object v2

    #@27
    iget v3, p0, Lcom/lge/systemservice/service/PrivateModeService;->mBackgroundResId:I

    #@29
    invoke-static {v2, v3, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    #@2c
    .line 232
    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    #@2e
    iput v2, p0, Lcom/lge/systemservice/service/PrivateModeService;->mWindowHeight:I

    #@30
    .line 234
    iget-object v1, p0, Lcom/lge/systemservice/service/PrivateModeService;->mParams:Landroid/view/WindowManager$LayoutParams;

    #@32
    .line 235
    .local v1, params:Landroid/view/WindowManager$LayoutParams;
    const/16 v2, 0x7d7

    #@34
    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    #@36
    .line 236
    const/4 v2, -0x3

    #@37
    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->format:I

    #@39
    .line 237
    const/16 v2, 0x33

    #@3b
    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@3d
    .line 238
    iget-object v2, p0, Lcom/lge/systemservice/service/PrivateModeService;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    #@3f
    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    #@41
    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@43
    .line 239
    iget v2, p0, Lcom/lge/systemservice/service/PrivateModeService;->mWindowHeight:I

    #@45
    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@47
    .line 240
    const/16 v2, 0x18

    #@49
    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@4b
    .line 243
    const/4 v2, 0x0

    #@4c
    iput-object v2, v1, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    #@4e
    .line 244
    const-string v2, "PrivateMode"

    #@50
    invoke-virtual {v1, v2}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    #@53
    .line 245
    iget-object v2, p0, Lcom/lge/systemservice/service/PrivateModeService;->mWM:Landroid/view/WindowManager;

    #@55
    iget-object v3, p0, Lcom/lge/systemservice/service/PrivateModeService;->mView:Landroid/view/View;

    #@57
    invoke-interface {v2, v3, v1}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@5a
    .line 246
    iput-boolean v4, p0, Lcom/lge/systemservice/service/PrivateModeService;->mShown:Z

    #@5c
    goto :goto_5
.end method

.method private stopPrivateMode()V
    .registers 3

    #@0
    .prologue
    .line 218
    iget-object v0, p0, Lcom/lge/systemservice/service/PrivateModeService;->mWM:Landroid/view/WindowManager;

    #@2
    iget-object v1, p0, Lcom/lge/systemservice/service/PrivateModeService;->mView:Landroid/view/View;

    #@4
    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    #@7
    .line 219
    const/4 v0, 0x0

    #@8
    iput-boolean v0, p0, Lcom/lge/systemservice/service/PrivateModeService;->mShown:Z

    #@a
    .line 220
    return-void
.end method

.method private updateSettings()V
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 99
    sget-boolean v3, Lcom/lge/systemservice/service/PrivateModeService;->FACTORY_TEST_MODE:Z

    #@4
    if-eqz v3, :cond_7

    #@6
    .line 108
    :cond_6
    :goto_6
    return-void

    #@7
    .line 101
    :cond_7
    invoke-virtual {p0}, Lcom/lge/systemservice/service/PrivateModeService;->getContentResolver()Landroid/content/ContentResolver;

    #@a
    move-result-object v0

    #@b
    .line 102
    .local v0, resolver:Landroid/content/ContentResolver;
    const-string v3, "hide_display"

    #@d
    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@10
    move-result v3

    #@11
    if-ne v3, v1, :cond_3f

    #@13
    :goto_13
    iput-boolean v1, p0, Lcom/lge/systemservice/service/PrivateModeService;->mEnabled:Z

    #@15
    .line 104
    sget-boolean v1, Lcom/lge/systemservice/service/PrivateModeService;->DEBUG:Z

    #@17
    if-eqz v1, :cond_33

    #@19
    sget-object v1, Lcom/lge/systemservice/service/PrivateModeService;->LOG_TAG:Ljava/lang/String;

    #@1b
    new-instance v2, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v3, "PrivateMode : mEnabled = "

    #@22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v2

    #@26
    iget-boolean v3, p0, Lcom/lge/systemservice/service/PrivateModeService;->mEnabled:Z

    #@28
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v2

    #@2c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v2

    #@30
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 105
    :cond_33
    iget-boolean v1, p0, Lcom/lge/systemservice/service/PrivateModeService;->mShown:Z

    #@35
    if-eqz v1, :cond_6

    #@37
    iget-boolean v1, p0, Lcom/lge/systemservice/service/PrivateModeService;->mEnabled:Z

    #@39
    if-nez v1, :cond_6

    #@3b
    .line 106
    invoke-direct {p0}, Lcom/lge/systemservice/service/PrivateModeService;->stopPrivateMode()V

    #@3e
    goto :goto_6

    #@3f
    :cond_3f
    move v1, v2

    #@40
    .line 102
    goto :goto_13
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .registers 3
    .parameter "sensor"
    .parameter "accuracy"

    #@0
    .prologue
    .line 286
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 3
    .parameter "intent"

    #@0
    .prologue
    .line 182
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 3
    .parameter "newConfig"

    #@0
    .prologue
    .line 165
    invoke-super {p0, p1}, Landroid/app/Service;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    #@3
    .line 167
    iget-boolean v0, p0, Lcom/lge/systemservice/service/PrivateModeService;->mShown:Z

    #@5
    if-eqz v0, :cond_a

    #@7
    invoke-direct {p0}, Lcom/lge/systemservice/service/PrivateModeService;->stopPrivateMode()V

    #@a
    .line 168
    :cond_a
    return-void
.end method

.method public onCreate()V
    .registers 5

    #@0
    .prologue
    .line 112
    sget-object v1, Lcom/lge/systemservice/service/PrivateModeService;->LOG_TAG:Ljava/lang/String;

    #@2
    const-string v2, "PrivateMode service is created"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 113
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    #@a
    .line 114
    const-string v1, "sensor"

    #@c
    invoke-virtual {p0, v1}, Lcom/lge/systemservice/service/PrivateModeService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Landroid/hardware/SensorManager;

    #@12
    iput-object v1, p0, Lcom/lge/systemservice/service/PrivateModeService;->mSensorManager:Landroid/hardware/SensorManager;

    #@14
    .line 115
    iget-object v1, p0, Lcom/lge/systemservice/service/PrivateModeService;->mSensorManager:Landroid/hardware/SensorManager;

    #@16
    const/16 v2, 0x8

    #@18
    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    #@1b
    move-result-object v1

    #@1c
    iput-object v1, p0, Lcom/lge/systemservice/service/PrivateModeService;->mProximity:Landroid/hardware/Sensor;

    #@1e
    .line 117
    const-string v1, "window"

    #@20
    invoke-virtual {p0, v1}, Lcom/lge/systemservice/service/PrivateModeService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@23
    move-result-object v1

    #@24
    check-cast v1, Landroid/view/WindowManager;

    #@26
    iput-object v1, p0, Lcom/lge/systemservice/service/PrivateModeService;->mWM:Landroid/view/WindowManager;

    #@28
    .line 118
    iget-object v1, p0, Lcom/lge/systemservice/service/PrivateModeService;->mWM:Landroid/view/WindowManager;

    #@2a
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    #@2d
    move-result-object v1

    #@2e
    iget-object v2, p0, Lcom/lge/systemservice/service/PrivateModeService;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    #@30
    invoke-virtual {v1, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    #@33
    .line 120
    const v1, 0x7f020001

    #@36
    iput v1, p0, Lcom/lge/systemservice/service/PrivateModeService;->mBackgroundResId:I

    #@38
    .line 122
    new-instance v1, Lcom/lge/systemservice/service/PrivateModeService$PrivateModeEventReceiver;

    #@3a
    invoke-direct {v1, p0}, Lcom/lge/systemservice/service/PrivateModeService$PrivateModeEventReceiver;-><init>(Lcom/lge/systemservice/service/PrivateModeService;)V

    #@3d
    iput-object v1, p0, Lcom/lge/systemservice/service/PrivateModeService;->mPhoneStateReceiver:Landroid/content/BroadcastReceiver;

    #@3f
    .line 123
    new-instance v0, Landroid/content/IntentFilter;

    #@41
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@44
    .line 124
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.PHONE_STATE"

    #@46
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@49
    .line 129
    iget-object v1, p0, Lcom/lge/systemservice/service/PrivateModeService;->mPhoneStateReceiver:Landroid/content/BroadcastReceiver;

    #@4b
    invoke-virtual {p0, v1, v0}, Lcom/lge/systemservice/service/PrivateModeService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@4e
    .line 131
    iget-object v1, p0, Lcom/lge/systemservice/service/PrivateModeService;->mSensorManager:Landroid/hardware/SensorManager;

    #@50
    iget-object v2, p0, Lcom/lge/systemservice/service/PrivateModeService;->mProximity:Landroid/hardware/Sensor;

    #@52
    const/4 v3, 0x3

    #@53
    invoke-virtual {v1, p0, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    #@56
    .line 132
    new-instance v1, Lcom/lge/systemservice/service/PrivateModeService$1;

    #@58
    invoke-direct {v1, p0}, Lcom/lge/systemservice/service/PrivateModeService$1;-><init>(Lcom/lge/systemservice/service/PrivateModeService;)V

    #@5b
    iput-object v1, p0, Lcom/lge/systemservice/service/PrivateModeService;->mHandler:Landroid/os/Handler;

    #@5d
    .line 148
    new-instance v1, Lcom/lge/systemservice/service/PrivateModeService$SettingsObserver;

    #@5f
    iget-object v2, p0, Lcom/lge/systemservice/service/PrivateModeService;->mHandler:Landroid/os/Handler;

    #@61
    invoke-direct {v1, p0, v2}, Lcom/lge/systemservice/service/PrivateModeService$SettingsObserver;-><init>(Lcom/lge/systemservice/service/PrivateModeService;Landroid/os/Handler;)V

    #@64
    iput-object v1, p0, Lcom/lge/systemservice/service/PrivateModeService;->mSettingsObserver:Lcom/lge/systemservice/service/PrivateModeService$SettingsObserver;

    #@66
    .line 149
    iget-object v1, p0, Lcom/lge/systemservice/service/PrivateModeService;->mSettingsObserver:Lcom/lge/systemservice/service/PrivateModeService$SettingsObserver;

    #@68
    invoke-virtual {v1}, Lcom/lge/systemservice/service/PrivateModeService$SettingsObserver;->startObservation()V

    #@6b
    .line 150
    return-void
.end method

.method public onDestroy()V
    .registers 3

    #@0
    .prologue
    .line 154
    sget-object v0, Lcom/lge/systemservice/service/PrivateModeService;->LOG_TAG:Ljava/lang/String;

    #@2
    const-string v1, "PrivateMode is destroyed"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 155
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    #@a
    .line 156
    const/4 v0, 0x1

    #@b
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/service/PrivateModeService;->stopForeground(Z)V

    #@e
    .line 157
    iget-object v0, p0, Lcom/lge/systemservice/service/PrivateModeService;->mPhoneStateReceiver:Landroid/content/BroadcastReceiver;

    #@10
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/service/PrivateModeService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@13
    .line 158
    iget-object v0, p0, Lcom/lge/systemservice/service/PrivateModeService;->mSensorManager:Landroid/hardware/SensorManager;

    #@15
    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    #@18
    .line 159
    iget-object v0, p0, Lcom/lge/systemservice/service/PrivateModeService;->mSettingsObserver:Lcom/lge/systemservice/service/PrivateModeService$SettingsObserver;

    #@1a
    invoke-virtual {v0}, Lcom/lge/systemservice/service/PrivateModeService$SettingsObserver;->stopObservation()V

    #@1d
    .line 160
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .registers 14
    .parameter "event"

    #@0
    .prologue
    const-wide/16 v10, 0x3e8

    #@2
    const/4 v9, 0x1

    #@3
    const-wide/16 v7, 0x0

    #@5
    .line 262
    iget-boolean v3, p0, Lcom/lge/systemservice/service/PrivateModeService;->mEnabled:Z

    #@7
    if-nez v3, :cond_a

    #@9
    .line 281
    :cond_9
    :goto_9
    return-void

    #@a
    .line 264
    :cond_a
    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    #@c
    const/4 v4, 0x0

    #@d
    aget v2, v3, v4

    #@f
    .line 265
    .local v2, distance:F
    sget-boolean v3, Lcom/lge/systemservice/service/PrivateModeService;->DEBUG:Z

    #@11
    if-eqz v3, :cond_2b

    #@13
    sget-object v3, Lcom/lge/systemservice/service/PrivateModeService;->LOG_TAG:Ljava/lang/String;

    #@15
    new-instance v4, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v5, "PrivateMode:: onSensorChanged(), distance="

    #@1c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v4

    #@20
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@23
    move-result-object v4

    #@24
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v4

    #@28
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 266
    :cond_2b
    const/high16 v3, 0x4000

    #@2d
    cmpg-float v3, v2, v3

    #@2f
    if-gez v3, :cond_54

    #@31
    .line 267
    iget-wide v3, p0, Lcom/lge/systemservice/service/PrivateModeService;->mSensorTime:J

    #@33
    cmp-long v3, v3, v7

    #@35
    if-nez v3, :cond_3d

    #@37
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@3a
    move-result-wide v3

    #@3b
    iput-wide v3, p0, Lcom/lge/systemservice/service/PrivateModeService;->mSensorTime:J

    #@3d
    .line 268
    :cond_3d
    iget-object v3, p0, Lcom/lge/systemservice/service/PrivateModeService;->mHandler:Landroid/os/Handler;

    #@3f
    iget-object v4, p0, Lcom/lge/systemservice/service/PrivateModeService;->mHandler:Landroid/os/Handler;

    #@41
    invoke-virtual {v4, v9}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@44
    move-result-object v4

    #@45
    invoke-virtual {v3, v4, v10, v11}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@48
    .line 270
    sget-boolean v3, Lcom/lge/systemservice/service/PrivateModeService;->DEBUG:Z

    #@4a
    if-eqz v3, :cond_9

    #@4c
    sget-object v3, Lcom/lge/systemservice/service/PrivateModeService;->LOG_TAG:Ljava/lang/String;

    #@4e
    const-string v4, "PrivateMode:: onSensorChanged(), send message(TOGGLE_PRIVATE_MODE)"

    #@50
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    goto :goto_9

    #@54
    .line 272
    :cond_54
    iget-wide v3, p0, Lcom/lge/systemservice/service/PrivateModeService;->mSensorTime:J

    #@56
    cmp-long v3, v3, v7

    #@58
    if-eqz v3, :cond_9

    #@5a
    .line 273
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@5d
    move-result-wide v3

    #@5e
    iget-wide v5, p0, Lcom/lge/systemservice/service/PrivateModeService;->mSensorTime:J

    #@60
    sub-long v0, v3, v5

    #@62
    .line 274
    .local v0, diff:J
    cmp-long v3, v0, v10

    #@64
    if-gez v3, :cond_9

    #@66
    .line 275
    sget-boolean v3, Lcom/lge/systemservice/service/PrivateModeService;->DEBUG:Z

    #@68
    if-eqz v3, :cond_71

    #@6a
    sget-object v3, Lcom/lge/systemservice/service/PrivateModeService;->LOG_TAG:Ljava/lang/String;

    #@6c
    const-string v4, "PrivateMode:: onSensorChanged(), remove message(TOGGLE_PRIVATE_MODE)"

    #@6e
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@71
    .line 276
    :cond_71
    iget-object v3, p0, Lcom/lge/systemservice/service/PrivateModeService;->mHandler:Landroid/os/Handler;

    #@73
    invoke-virtual {v3, v9}, Landroid/os/Handler;->removeMessages(I)V

    #@76
    .line 277
    iput-wide v7, p0, Lcom/lge/systemservice/service/PrivateModeService;->mSensorTime:J

    #@78
    goto :goto_9
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .registers 5
    .parameter "intent"
    .parameter "flags"
    .parameter "startId"

    #@0
    .prologue
    .line 172
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public onTaskRemoved(Landroid/content/Intent;)V
    .registers 2
    .parameter "rootIntent"

    #@0
    .prologue
    .line 177
    invoke-super {p0, p1}, Landroid/app/Service;->onTaskRemoved(Landroid/content/Intent;)V

    #@3
    .line 178
    return-void
.end method
