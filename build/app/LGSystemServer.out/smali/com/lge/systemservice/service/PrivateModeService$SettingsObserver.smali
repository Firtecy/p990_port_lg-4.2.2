.class Lcom/lge/systemservice/service/PrivateModeService$SettingsObserver;
.super Landroid/database/ContentObserver;
.source "PrivateModeService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/PrivateModeService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/PrivateModeService;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/PrivateModeService;Landroid/os/Handler;)V
    .registers 3
    .parameter
    .parameter "handler"

    #@0
    .prologue
    .line 74
    iput-object p1, p0, Lcom/lge/systemservice/service/PrivateModeService$SettingsObserver;->this$0:Lcom/lge/systemservice/service/PrivateModeService;

    #@2
    .line 75
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    #@5
    .line 76
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .registers 3
    .parameter "selfChange"

    #@0
    .prologue
    .line 94
    iget-object v0, p0, Lcom/lge/systemservice/service/PrivateModeService$SettingsObserver;->this$0:Lcom/lge/systemservice/service/PrivateModeService;

    #@2
    invoke-static {v0}, Lcom/lge/systemservice/service/PrivateModeService;->access$200(Lcom/lge/systemservice/service/PrivateModeService;)V

    #@5
    .line 95
    return-void
.end method

.method startObservation()V
    .registers 4

    #@0
    .prologue
    .line 79
    invoke-static {}, Lcom/lge/systemservice/service/PrivateModeService;->access$000()Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_f

    #@6
    invoke-static {}, Lcom/lge/systemservice/service/PrivateModeService;->access$100()Ljava/lang/String;

    #@9
    move-result-object v1

    #@a
    const-string v2, "SettingsObserver:: startObservation()"

    #@c
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 80
    :cond_f
    iget-object v1, p0, Lcom/lge/systemservice/service/PrivateModeService$SettingsObserver;->this$0:Lcom/lge/systemservice/service/PrivateModeService;

    #@11
    invoke-virtual {v1}, Lcom/lge/systemservice/service/PrivateModeService;->getContentResolver()Landroid/content/ContentResolver;

    #@14
    move-result-object v0

    #@15
    .line 81
    .local v0, resolver:Landroid/content/ContentResolver;
    const-string v1, "hide_display"

    #@17
    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@1a
    move-result-object v1

    #@1b
    const/4 v2, 0x0

    #@1c
    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@1f
    .line 83
    iget-object v1, p0, Lcom/lge/systemservice/service/PrivateModeService$SettingsObserver;->this$0:Lcom/lge/systemservice/service/PrivateModeService;

    #@21
    invoke-static {v1}, Lcom/lge/systemservice/service/PrivateModeService;->access$200(Lcom/lge/systemservice/service/PrivateModeService;)V

    #@24
    .line 84
    return-void
.end method

.method stopObservation()V
    .registers 4

    #@0
    .prologue
    .line 87
    invoke-static {}, Lcom/lge/systemservice/service/PrivateModeService;->access$000()Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_f

    #@6
    invoke-static {}, Lcom/lge/systemservice/service/PrivateModeService;->access$100()Ljava/lang/String;

    #@9
    move-result-object v1

    #@a
    const-string v2, "SettingsObserver:: stopObservation()"

    #@c
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 88
    :cond_f
    iget-object v1, p0, Lcom/lge/systemservice/service/PrivateModeService$SettingsObserver;->this$0:Lcom/lge/systemservice/service/PrivateModeService;

    #@11
    invoke-virtual {v1}, Lcom/lge/systemservice/service/PrivateModeService;->getContentResolver()Landroid/content/ContentResolver;

    #@14
    move-result-object v0

    #@15
    .line 89
    .local v0, resolver:Landroid/content/ContentResolver;
    invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    #@18
    .line 90
    return-void
.end method
