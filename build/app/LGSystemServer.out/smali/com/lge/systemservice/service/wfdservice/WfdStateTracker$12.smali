.class Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$12;
.super Ljava/lang/Object;
.source "WfdStateTracker.java"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->handleRtspStateChange(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1353
    iput-object p1, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$12;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onFailure(I)V
    .registers 5
    .parameter "reason"

    #@0
    .prologue
    .line 1358
    const-string v0, "WfdStateTracker"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Direct disconnect fail, reason: "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1359
    return-void
.end method

.method public onSuccess()V
    .registers 3

    #@0
    .prologue
    .line 1355
    iget-object v0, p0, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker$12;->this$0:Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;

    #@2
    const/4 v1, 0x2

    #@3
    invoke-static {v0, v1}, Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;->access$300(Lcom/lge/systemservice/service/wfdservice/WfdStateTracker;I)V

    #@6
    .line 1356
    return-void
.end method
