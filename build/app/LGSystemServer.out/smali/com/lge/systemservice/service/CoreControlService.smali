.class public Lcom/lge/systemservice/service/CoreControlService;
.super Lcom/lge/systemservice/core/corecontrolmanager/ICoreControlManager$Stub;
.source "CoreControlService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/systemservice/service/CoreControlService$CoreControlFailedException;,
        Lcom/lge/systemservice/service/CoreControlService$LPMPolicy;
    }
.end annotation


# static fields
.field private static coreMode:I


# instance fields
.field private final ECO_MODE:Ljava/lang/String;

.field private final ECO_MODE_URI:Landroid/net/Uri;

.field private final mContext:Landroid/content/Context;

.field private mCurPowerSaveBias:I

.field private mEcoModeObserver:Landroid/database/ContentObserver;

.field private mFreqLowLimit:I

.field private final mHandler:Landroid/os/Handler;

.field private mPolicyProperties:[Lcom/lge/systemservice/service/CoreControlService$LPMPolicy;

.field private mSetLowPowerModeTask:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 48
    const/4 v0, 0x4

    #@1
    sput v0, Lcom/lge/systemservice/service/CoreControlService;->coreMode:I

    #@3
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 79
    invoke-direct {p0}, Lcom/lge/systemservice/core/corecontrolmanager/ICoreControlManager$Stub;-><init>()V

    #@3
    .line 76
    new-instance v0, Landroid/os/Handler;

    #@5
    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    #@8
    iput-object v0, p0, Lcom/lge/systemservice/service/CoreControlService;->mHandler:Landroid/os/Handler;

    #@a
    .line 526
    const-string v0, "eco_mode"

    #@c
    iput-object v0, p0, Lcom/lge/systemservice/service/CoreControlService;->ECO_MODE:Ljava/lang/String;

    #@e
    .line 527
    const-string v0, "eco_mode"

    #@10
    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@13
    move-result-object v0

    #@14
    iput-object v0, p0, Lcom/lge/systemservice/service/CoreControlService;->ECO_MODE_URI:Landroid/net/Uri;

    #@16
    .line 744
    new-instance v0, Lcom/lge/systemservice/service/CoreControlService$2;

    #@18
    invoke-direct {v0, p0}, Lcom/lge/systemservice/service/CoreControlService$2;-><init>(Lcom/lge/systemservice/service/CoreControlService;)V

    #@1b
    iput-object v0, p0, Lcom/lge/systemservice/service/CoreControlService;->mSetLowPowerModeTask:Ljava/lang/Runnable;

    #@1d
    .line 80
    iput-object p1, p0, Lcom/lge/systemservice/service/CoreControlService;->mContext:Landroid/content/Context;

    #@1f
    .line 81
    invoke-direct {p0}, Lcom/lge/systemservice/service/CoreControlService;->initLowPowerMode()V

    #@22
    .line 82
    return-void
.end method

.method static synthetic access$000(Lcom/lge/systemservice/service/CoreControlService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 19
    invoke-direct {p0}, Lcom/lge/systemservice/service/CoreControlService;->isEcoModeEnabled()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$100(Lcom/lge/systemservice/service/CoreControlService;)Ljava/lang/Runnable;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 19
    iget-object v0, p0, Lcom/lge/systemservice/service/CoreControlService;->mSetLowPowerModeTask:Ljava/lang/Runnable;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/lge/systemservice/service/CoreControlService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 19
    invoke-direct {p0}, Lcom/lge/systemservice/service/CoreControlService;->setLowPowerMode()V

    #@3
    return-void
.end method

.method private forcedToNormal()Z
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 245
    const/4 v2, 0x0

    #@2
    .line 247
    .local v2, succeeded:Z
    const-wide/16 v3, 0x7d0

    #@4
    :try_start_4
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V

    #@7
    .line 248
    invoke-direct {p0}, Lcom/lge/systemservice/service/CoreControlService;->setAsQuadMode()Z
    :try_end_a
    .catch Lcom/lge/systemservice/service/CoreControlService$CoreControlFailedException; {:try_start_4 .. :try_end_a} :catch_28
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_a} :catch_4a

    #@a
    move-result v2

    #@b
    .line 270
    :goto_b
    const-string v4, "CoreControlService"

    #@d
    new-instance v3, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v5, "forcedToNormal(), "

    #@14
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v5

    #@18
    if-eqz v2, :cond_55

    #@1a
    const-string v3, "SUCCEEDED"

    #@1c
    :goto_1c
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-static {v4, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 272
    return v2

    #@28
    .line 250
    :catch_28
    move-exception v0

    #@29
    .line 252
    .local v0, e:Lcom/lge/systemservice/service/CoreControlService$CoreControlFailedException;
    const-string v3, "CoreControlService"

    #@2b
    invoke-virtual {v0}, Lcom/lge/systemservice/service/CoreControlService$CoreControlFailedException;->getMessage()Ljava/lang/String;

    #@2e
    move-result-object v4

    #@2f
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 254
    const/4 v3, 0x0

    #@33
    :try_start_33
    invoke-direct {p0, v3}, Lcom/lge/systemservice/service/CoreControlService;->setCpuManualControl(Z)Z
    :try_end_36
    .catchall {:try_start_33 .. :try_end_36} :catchall_46
    .catch Lcom/lge/systemservice/service/CoreControlService$CoreControlFailedException; {:try_start_33 .. :try_end_36} :catch_39

    #@36
    .line 262
    sput v5, Lcom/lge/systemservice/service/CoreControlService;->coreMode:I

    #@38
    goto :goto_b

    #@39
    .line 256
    :catch_39
    move-exception v1

    #@3a
    .line 258
    .local v1, ee:Lcom/lge/systemservice/service/CoreControlService$CoreControlFailedException;
    :try_start_3a
    const-string v3, "CoreControlService"

    #@3c
    invoke-virtual {v1}, Lcom/lge/systemservice/service/CoreControlService$CoreControlFailedException;->getMessage()Ljava/lang/String;

    #@3f
    move-result-object v4

    #@40
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_43
    .catchall {:try_start_3a .. :try_end_43} :catchall_46

    #@43
    .line 262
    sput v5, Lcom/lge/systemservice/service/CoreControlService;->coreMode:I

    #@45
    goto :goto_b

    #@46
    .end local v1           #ee:Lcom/lge/systemservice/service/CoreControlService$CoreControlFailedException;
    :catchall_46
    move-exception v3

    #@47
    sput v5, Lcom/lge/systemservice/service/CoreControlService;->coreMode:I

    #@49
    throw v3

    #@4a
    .line 265
    .end local v0           #e:Lcom/lge/systemservice/service/CoreControlService$CoreControlFailedException;
    :catch_4a
    move-exception v0

    #@4b
    .line 267
    .local v0, e:Ljava/lang/InterruptedException;
    const-string v3, "CoreControlService"

    #@4d
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    #@50
    move-result-object v4

    #@51
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    goto :goto_b

    #@55
    .line 270
    .end local v0           #e:Ljava/lang/InterruptedException;
    :cond_55
    const-string v3, "FAILED"

    #@57
    goto :goto_1c
.end method

.method private getPsbLimit()I
    .registers 12

    #@0
    .prologue
    const/16 v1, 0x3e8

    #@2
    .line 810
    iget v3, p0, Lcom/lge/systemservice/service/CoreControlService;->mFreqLowLimit:I

    #@4
    if-gtz v3, :cond_7

    #@6
    .line 827
    :cond_6
    :goto_6
    return v1

    #@7
    .line 814
    :cond_7
    const-string v3, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq"

    #@9
    invoke-direct {p0, v3}, Lcom/lge/systemservice/service/CoreControlService;->readCpuSysFile(Ljava/lang/String;)Ljava/lang/String;

    #@c
    move-result-object v2

    #@d
    .line 815
    .local v2, strMaxFreq:Ljava/lang/String;
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    #@14
    move-result v0

    #@15
    .line 817
    .local v0, maxFreq:I
    iget v3, p0, Lcom/lge/systemservice/service/CoreControlService;->mFreqLowLimit:I

    #@17
    if-le v0, v3, :cond_6

    #@19
    if-lez v0, :cond_6

    #@1b
    .line 818
    const-wide v3, 0x408f400000000000L

    #@20
    const-wide/high16 v5, 0x3ff0

    #@22
    iget v7, p0, Lcom/lge/systemservice/service/CoreControlService;->mFreqLowLimit:I

    #@24
    int-to-double v7, v7

    #@25
    int-to-double v9, v0

    #@26
    div-double/2addr v7, v9

    #@27
    sub-double/2addr v5, v7

    #@28
    mul-double/2addr v3, v5

    #@29
    const-wide v5, 0x3feccccccccccccdL

    #@2e
    add-double/2addr v3, v5

    #@2f
    double-to-int v1, v3

    #@30
    .line 824
    .local v1, psb:I
    goto :goto_6
.end method

.method private initLowPowerMode()V
    .registers 11

    #@0
    .prologue
    const/4 v9, 0x3

    #@1
    const/4 v8, 0x1

    #@2
    const/4 v7, 0x0

    #@3
    const/4 v6, 0x2

    #@4
    .line 757
    iget-object v4, p0, Lcom/lge/systemservice/service/CoreControlService;->mPolicyProperties:[Lcom/lge/systemservice/service/CoreControlService$LPMPolicy;

    #@6
    if-nez v4, :cond_9d

    #@8
    .line 759
    iput v7, p0, Lcom/lge/systemservice/service/CoreControlService;->mCurPowerSaveBias:I

    #@a
    .line 762
    const-string v4, "ro.lge.ps.freqlowlimit"

    #@c
    const-string v5, ""

    #@e
    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@11
    move-result-object v1

    #@12
    .line 763
    .local v1, freqLowLimit:Ljava/lang/String;
    if-eqz v1, :cond_2f

    #@14
    const-string v4, ""

    #@16
    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@19
    move-result v4

    #@1a
    if-nez v4, :cond_2f

    #@1c
    .line 764
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    #@1f
    move-result-object v4

    #@20
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    #@23
    move-result v4

    #@24
    iput v4, p0, Lcom/lge/systemservice/service/CoreControlService;->mFreqLowLimit:I

    #@26
    .line 770
    :goto_26
    new-array v4, v9, [Lcom/lge/systemservice/service/CoreControlService$LPMPolicy;

    #@28
    iput-object v4, p0, Lcom/lge/systemservice/service/CoreControlService;->mPolicyProperties:[Lcom/lge/systemservice/service/CoreControlService$LPMPolicy;

    #@2a
    .line 771
    iget-object v4, p0, Lcom/lge/systemservice/service/CoreControlService;->mPolicyProperties:[Lcom/lge/systemservice/service/CoreControlService$LPMPolicy;

    #@2c
    if-nez v4, :cond_33

    #@2e
    .line 802
    .end local v1           #freqLowLimit:Ljava/lang/String;
    :goto_2e
    return-void

    #@2f
    .line 766
    .restart local v1       #freqLowLimit:Ljava/lang/String;
    :cond_2f
    const/4 v4, -0x1

    #@30
    iput v4, p0, Lcom/lge/systemservice/service/CoreControlService;->mFreqLowLimit:I

    #@32
    goto :goto_26

    #@33
    .line 774
    :cond_33
    const/4 v2, 0x0

    #@34
    .local v2, i:I
    :goto_34
    if-ge v2, v9, :cond_42

    #@36
    .line 775
    iget-object v4, p0, Lcom/lge/systemservice/service/CoreControlService;->mPolicyProperties:[Lcom/lge/systemservice/service/CoreControlService$LPMPolicy;

    #@38
    new-instance v5, Lcom/lge/systemservice/service/CoreControlService$LPMPolicy;

    #@3a
    invoke-direct {v5}, Lcom/lge/systemservice/service/CoreControlService$LPMPolicy;-><init>()V

    #@3d
    aput-object v5, v4, v2

    #@3f
    .line 774
    add-int/lit8 v2, v2, 0x1

    #@41
    goto :goto_34

    #@42
    .line 778
    :cond_42
    iget-object v4, p0, Lcom/lge/systemservice/service/CoreControlService;->mPolicyProperties:[Lcom/lge/systemservice/service/CoreControlService$LPMPolicy;

    #@44
    aget-object v4, v4, v7

    #@46
    const/16 v5, 0x3e8

    #@48
    iput v5, v4, Lcom/lge/systemservice/service/CoreControlService$LPMPolicy;->mPowerSaveBias:I

    #@4a
    .line 779
    iget-object v4, p0, Lcom/lge/systemservice/service/CoreControlService;->mPolicyProperties:[Lcom/lge/systemservice/service/CoreControlService$LPMPolicy;

    #@4c
    aget-object v4, v4, v7

    #@4e
    iput v8, v4, Lcom/lge/systemservice/service/CoreControlService$LPMPolicy;->mCoreMode:I

    #@50
    .line 781
    const-string v4, "ro.lge.ps.ime.pv"

    #@52
    const-string v5, ""

    #@54
    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@57
    move-result-object v3

    #@58
    .line 782
    .local v3, imePowerSaveBias:Ljava/lang/String;
    if-eqz v3, :cond_70

    #@5a
    const-string v4, ""

    #@5c
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5f
    move-result v4

    #@60
    if-nez v4, :cond_70

    #@62
    .line 783
    iget-object v4, p0, Lcom/lge/systemservice/service/CoreControlService;->mPolicyProperties:[Lcom/lge/systemservice/service/CoreControlService$LPMPolicy;

    #@64
    aget-object v4, v4, v8

    #@66
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    #@69
    move-result-object v5

    #@6a
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    #@6d
    move-result v5

    #@6e
    iput v5, v4, Lcom/lge/systemservice/service/CoreControlService$LPMPolicy;->mPowerSaveBias:I

    #@70
    .line 785
    :cond_70
    iget-object v4, p0, Lcom/lge/systemservice/service/CoreControlService;->mPolicyProperties:[Lcom/lge/systemservice/service/CoreControlService$LPMPolicy;

    #@72
    aget-object v4, v4, v8

    #@74
    const/4 v5, 0x4

    #@75
    iput v5, v4, Lcom/lge/systemservice/service/CoreControlService$LPMPolicy;->mCoreMode:I

    #@77
    .line 787
    const-string v4, "ro.lge.ps.eco.pv"

    #@79
    const-string v5, ""

    #@7b
    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7e
    move-result-object v0

    #@7f
    .line 788
    .local v0, ecoModePowerSaveBias:Ljava/lang/String;
    if-eqz v0, :cond_97

    #@81
    const-string v4, ""

    #@83
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@86
    move-result v4

    #@87
    if-nez v4, :cond_97

    #@89
    .line 789
    iget-object v4, p0, Lcom/lge/systemservice/service/CoreControlService;->mPolicyProperties:[Lcom/lge/systemservice/service/CoreControlService$LPMPolicy;

    #@8b
    aget-object v4, v4, v6

    #@8d
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    #@90
    move-result-object v5

    #@91
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    #@94
    move-result v5

    #@95
    iput v5, v4, Lcom/lge/systemservice/service/CoreControlService$LPMPolicy;->mPowerSaveBias:I

    #@97
    .line 791
    :cond_97
    iget-object v4, p0, Lcom/lge/systemservice/service/CoreControlService;->mPolicyProperties:[Lcom/lge/systemservice/service/CoreControlService$LPMPolicy;

    #@99
    aget-object v4, v4, v6

    #@9b
    iput v6, v4, Lcom/lge/systemservice/service/CoreControlService$LPMPolicy;->mCoreMode:I

    #@9d
    .line 800
    .end local v0           #ecoModePowerSaveBias:Ljava/lang/String;
    .end local v1           #freqLowLimit:Ljava/lang/String;
    .end local v2           #i:I
    .end local v3           #imePowerSaveBias:Ljava/lang/String;
    :cond_9d
    iget-object v4, p0, Lcom/lge/systemservice/service/CoreControlService;->mPolicyProperties:[Lcom/lge/systemservice/service/CoreControlService$LPMPolicy;

    #@9f
    aget-object v4, v4, v6

    #@a1
    invoke-direct {p0}, Lcom/lge/systemservice/service/CoreControlService;->isEcoModeEnabled()Z

    #@a4
    move-result v5

    #@a5
    iput-boolean v5, v4, Lcom/lge/systemservice/service/CoreControlService$LPMPolicy;->mEnabled:Z

    #@a7
    .line 801
    invoke-direct {p0}, Lcom/lge/systemservice/service/CoreControlService;->setLowPowerMode()V

    #@aa
    goto :goto_2e
.end method

.method private isEcoModeEnabled()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 530
    const/4 v0, 0x0

    #@2
    .line 531
    .local v0, enabled:I
    iget-object v2, p0, Lcom/lge/systemservice/service/CoreControlService;->ECO_MODE_URI:Landroid/net/Uri;

    #@4
    if-eqz v2, :cond_12

    #@6
    .line 533
    iget-object v2, p0, Lcom/lge/systemservice/service/CoreControlService;->mContext:Landroid/content/Context;

    #@8
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@b
    move-result-object v2

    #@c
    const-string v3, "eco_mode"

    #@e
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@11
    move-result v0

    #@12
    .line 541
    :cond_12
    if-lez v0, :cond_15

    #@14
    .line 543
    const/4 v1, 0x1

    #@15
    .line 546
    :cond_15
    return v1
.end method

.method private readCpuSysFile(Ljava/lang/String;)Ljava/lang/String;
    .registers 9
    .parameter "filePath"

    #@0
    .prologue
    .line 277
    const/4 v1, 0x0

    #@1
    .line 278
    .local v1, inReader:Ljava/io/BufferedReader;
    const-string v3, ""

    #@3
    .line 280
    .local v3, lineStr:Ljava/lang/String;
    :try_start_3
    new-instance v2, Ljava/io/BufferedReader;

    #@5
    new-instance v4, Ljava/io/FileReader;

    #@7
    invoke-direct {v4, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    #@a
    invoke-direct {v2, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_4a
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_d} :catch_1b

    #@d
    .line 281
    .end local v1           #inReader:Ljava/io/BufferedReader;
    .local v2, inReader:Ljava/io/BufferedReader;
    :try_start_d
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_10
    .catchall {:try_start_d .. :try_end_10} :catchall_53
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_10} :catch_56

    #@10
    move-result-object v3

    #@11
    .line 291
    if-eqz v2, :cond_16

    #@13
    .line 293
    :try_start_13
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_16} :catch_18

    #@16
    :cond_16
    move-object v1, v2

    #@17
    .line 298
    .end local v2           #inReader:Ljava/io/BufferedReader;
    .restart local v1       #inReader:Ljava/io/BufferedReader;
    :cond_17
    :goto_17
    return-object v3

    #@18
    .line 295
    .end local v1           #inReader:Ljava/io/BufferedReader;
    .restart local v2       #inReader:Ljava/io/BufferedReader;
    :catch_18
    move-exception v4

    #@19
    move-object v1, v2

    #@1a
    .line 296
    .end local v2           #inReader:Ljava/io/BufferedReader;
    .restart local v1       #inReader:Ljava/io/BufferedReader;
    goto :goto_17

    #@1b
    .line 287
    :catch_1b
    move-exception v0

    #@1c
    .line 288
    .local v0, exception:Ljava/lang/Exception;
    :goto_1c
    :try_start_1c
    const-string v4, "CoreControlService"

    #@1e
    new-instance v5, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v6, "cpu sys file read error: "

    #@25
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v5

    #@29
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v5

    #@2d
    const-string v6, " "

    #@2f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v5

    #@33
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@36
    move-result-object v6

    #@37
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v5

    #@3b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v5

    #@3f
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_42
    .catchall {:try_start_1c .. :try_end_42} :catchall_4a

    #@42
    .line 291
    if-eqz v1, :cond_17

    #@44
    .line 293
    :try_start_44
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_47
    .catch Ljava/lang/Exception; {:try_start_44 .. :try_end_47} :catch_48

    #@47
    goto :goto_17

    #@48
    .line 295
    :catch_48
    move-exception v4

    #@49
    goto :goto_17

    #@4a
    .line 290
    .end local v0           #exception:Ljava/lang/Exception;
    :catchall_4a
    move-exception v4

    #@4b
    .line 291
    :goto_4b
    if-eqz v1, :cond_50

    #@4d
    .line 293
    :try_start_4d
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_50
    .catch Ljava/lang/Exception; {:try_start_4d .. :try_end_50} :catch_51

    #@50
    .line 295
    :cond_50
    :goto_50
    throw v4

    #@51
    :catch_51
    move-exception v5

    #@52
    goto :goto_50

    #@53
    .line 290
    .end local v1           #inReader:Ljava/io/BufferedReader;
    .restart local v2       #inReader:Ljava/io/BufferedReader;
    :catchall_53
    move-exception v4

    #@54
    move-object v1, v2

    #@55
    .end local v2           #inReader:Ljava/io/BufferedReader;
    .restart local v1       #inReader:Ljava/io/BufferedReader;
    goto :goto_4b

    #@56
    .line 287
    .end local v1           #inReader:Ljava/io/BufferedReader;
    .restart local v2       #inReader:Ljava/io/BufferedReader;
    :catch_56
    move-exception v0

    #@57
    move-object v1, v2

    #@58
    .end local v2           #inReader:Ljava/io/BufferedReader;
    .restart local v1       #inReader:Ljava/io/BufferedReader;
    goto :goto_1c
.end method

.method private resetLowPowerModePolicyState()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 805
    iput v0, p0, Lcom/lge/systemservice/service/CoreControlService;->mCurPowerSaveBias:I

    #@3
    .line 806
    sput v0, Lcom/lge/systemservice/service/CoreControlService;->coreMode:I

    #@5
    .line 807
    return-void
.end method

.method private setAsDualMode()Z
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lge/systemservice/service/CoreControlService$CoreControlFailedException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x2

    #@1
    .line 215
    invoke-direct {p0, v0}, Lcom/lge/systemservice/service/CoreControlService;->setEcoModeMaxAvailableCpu(I)Z

    #@4
    .line 218
    sput v0, Lcom/lge/systemservice/service/CoreControlService;->coreMode:I

    #@6
    .line 220
    const/4 v0, 0x1

    #@7
    return v0
.end method

.method private setAsQuadMode()Z
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lge/systemservice/service/CoreControlService$CoreControlFailedException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x4

    #@1
    .line 235
    invoke-direct {p0, v0}, Lcom/lge/systemservice/service/CoreControlService;->setEcoModeMaxAvailableCpu(I)Z

    #@4
    .line 238
    sput v0, Lcom/lge/systemservice/service/CoreControlService;->coreMode:I

    #@6
    .line 240
    const/4 v0, 0x1

    #@7
    return v0
.end method

.method private setAsSingleMode()Z
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lge/systemservice/service/CoreControlService$CoreControlFailedException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 194
    invoke-direct {p0, v0}, Lcom/lge/systemservice/service/CoreControlService;->setEcoModeMaxAvailableCpu(I)Z

    #@4
    .line 197
    sput v0, Lcom/lge/systemservice/service/CoreControlService;->coreMode:I

    #@6
    .line 199
    return v0
.end method

.method private setCpuManualControl(Z)Z
    .registers 4
    .parameter "enable"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lge/systemservice/service/CoreControlService$CoreControlFailedException;
        }
    .end annotation

    #@0
    .prologue
    .line 334
    const-string v0, "CoreControlService"

    #@2
    const-string v1, "setCpuManualControl() failed. Set MANUAL_CPU_CONTROL as TRUE first."

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 335
    const/4 v0, 0x0

    #@8
    return v0
.end method

.method private setEcoModeMaxAvailableCpu(I)Z
    .registers 10
    .parameter "maxCpu"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lge/systemservice/service/CoreControlService$CoreControlFailedException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v7, 0xa

    #@2
    .line 418
    const/16 v0, 0xa

    #@4
    .line 419
    .local v0, MAX_TRY:I
    const/4 v4, 0x0

    #@5
    .line 421
    .local v4, tryCnt:I
    new-instance v3, Ljava/io/File;

    #@7
    const-string v5, "/sys/devices/platform/lge_kernel_driver/eco_cpu"

    #@9
    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@c
    .line 422
    .local v3, onlineFile:Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    #@f
    move-result v5

    #@10
    if-eqz v5, :cond_32

    #@12
    .line 425
    const/4 v4, 0x0

    #@13
    :goto_13
    if-ge v4, v7, :cond_21

    #@15
    .line 427
    const-string v5, "/sys/devices/platform/lge_kernel_driver/eco_cpu"

    #@17
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@1a
    move-result-object v6

    #@1b
    invoke-direct {p0, v5, v6}, Lcom/lge/systemservice/service/CoreControlService;->writeCpuSysFile(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    move-result v5

    #@1f
    if-eqz v5, :cond_25

    #@21
    .line 438
    :cond_21
    if-ge v4, v7, :cond_32

    #@23
    .line 440
    const/4 v5, 0x1

    #@24
    return v5

    #@25
    .line 432
    :cond_25
    add-int/lit8 v5, v4, 0x1

    #@27
    mul-int/lit8 v5, v5, 0x64

    #@29
    :try_start_29
    rem-int/lit16 v5, v5, 0x1f4

    #@2b
    int-to-long v1, v5

    #@2c
    .line 433
    .local v1, interval:J
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2f
    .catch Ljava/lang/InterruptedException; {:try_start_29 .. :try_end_2f} :catch_4b

    #@2f
    .line 425
    .end local v1           #interval:J
    :goto_2f
    add-int/lit8 v4, v4, 0x1

    #@31
    goto :goto_13

    #@32
    .line 468
    :cond_32
    new-instance v5, Lcom/lge/systemservice/service/CoreControlService$CoreControlFailedException;

    #@34
    new-instance v6, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v7, "Failed to setEcoModeMaxAvailableCpu as "

    #@3b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v6

    #@3f
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@42
    move-result-object v6

    #@43
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v6

    #@47
    invoke-direct {v5, p0, v6}, Lcom/lge/systemservice/service/CoreControlService$CoreControlFailedException;-><init>(Lcom/lge/systemservice/service/CoreControlService;Ljava/lang/String;)V

    #@4a
    throw v5

    #@4b
    .line 434
    :catch_4b
    move-exception v5

    #@4c
    goto :goto_2f
.end method

.method private setLowPowerMode()V
    .registers 9

    #@0
    .prologue
    .line 697
    const/4 v6, 0x0

    #@1
    .line 698
    .local v6, psbValue:I
    const/4 v0, 0x4

    #@2
    .line 699
    .local v0, coreMode:I
    const/4 v3, 0x0

    #@3
    .line 700
    .local v3, policyEnabled:Z
    const/4 v4, 0x0

    #@4
    .line 703
    .local v4, psbCanceled:Z
    invoke-direct {p0}, Lcom/lge/systemservice/service/CoreControlService;->getPsbLimit()I

    #@7
    move-result v5

    #@8
    .line 705
    .local v5, psbLimit:I
    const/4 v1, 0x0

    #@9
    .local v1, i:I
    :goto_9
    const/4 v7, 0x3

    #@a
    if-ge v1, v7, :cond_2e

    #@c
    .line 706
    iget-object v7, p0, Lcom/lge/systemservice/service/CoreControlService;->mPolicyProperties:[Lcom/lge/systemservice/service/CoreControlService$LPMPolicy;

    #@e
    aget-object v2, v7, v1

    #@10
    .line 707
    .local v2, policy:Lcom/lge/systemservice/service/CoreControlService$LPMPolicy;
    iget-boolean v7, v2, Lcom/lge/systemservice/service/CoreControlService$LPMPolicy;->mEnabled:Z

    #@12
    if-eqz v7, :cond_29

    #@14
    .line 708
    const/4 v3, 0x1

    #@15
    .line 710
    iget v7, v2, Lcom/lge/systemservice/service/CoreControlService$LPMPolicy;->mPowerSaveBias:I

    #@17
    if-le v7, v6, :cond_1f

    #@19
    .line 711
    iget v7, v2, Lcom/lge/systemservice/service/CoreControlService$LPMPolicy;->mPowerSaveBias:I

    #@1b
    if-gt v7, v5, :cond_2c

    #@1d
    .line 712
    iget v6, v2, Lcom/lge/systemservice/service/CoreControlService$LPMPolicy;->mPowerSaveBias:I

    #@1f
    .line 721
    :cond_1f
    :goto_1f
    iget v7, v2, Lcom/lge/systemservice/service/CoreControlService$LPMPolicy;->mCoreMode:I

    #@21
    if-lez v7, :cond_29

    #@23
    iget v7, v2, Lcom/lge/systemservice/service/CoreControlService$LPMPolicy;->mCoreMode:I

    #@25
    if-ge v7, v0, :cond_29

    #@27
    .line 722
    iget v0, v2, Lcom/lge/systemservice/service/CoreControlService$LPMPolicy;->mCoreMode:I

    #@29
    .line 705
    :cond_29
    add-int/lit8 v1, v1, 0x1

    #@2b
    goto :goto_9

    #@2c
    .line 714
    :cond_2c
    const/4 v4, 0x1

    #@2d
    goto :goto_1f

    #@2e
    .line 727
    .end local v2           #policy:Lcom/lge/systemservice/service/CoreControlService$LPMPolicy;
    :cond_2e
    if-eqz v3, :cond_3d

    #@30
    .line 729
    if-nez v4, :cond_36

    #@32
    iget v7, p0, Lcom/lge/systemservice/service/CoreControlService;->mCurPowerSaveBias:I

    #@34
    if-eq v6, v7, :cond_39

    #@36
    .line 730
    :cond_36
    invoke-direct {p0, v6}, Lcom/lge/systemservice/service/CoreControlService;->setOndemandPowerSaveBias(I)V

    #@39
    .line 737
    :cond_39
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/service/CoreControlService;->changeCoreState(I)V

    #@3c
    .line 743
    :goto_3c
    return-void

    #@3d
    .line 740
    :cond_3d
    const/4 v7, 0x0

    #@3e
    invoke-direct {p0, v7}, Lcom/lge/systemservice/service/CoreControlService;->setOndemandPowerSaveBias(I)V

    #@41
    .line 741
    const/4 v7, 0x4

    #@42
    invoke-virtual {p0, v7}, Lcom/lge/systemservice/service/CoreControlService;->changeCoreState(I)V

    #@45
    goto :goto_3c
.end method

.method private setOndemandPowerSaveBias(I)V
    .registers 5
    .parameter "psbValue"

    #@0
    .prologue
    .line 832
    const-string v1, "/sys/devices/system/cpu/cpufreq/ondemand/powersave_bias"

    #@2
    invoke-direct {p0, v1}, Lcom/lge/systemservice/service/CoreControlService;->readCpuSysFile(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 833
    .local v0, curPsb:Ljava/lang/String;
    if-eqz v0, :cond_10

    #@8
    const-string v1, "-1000"

    #@a
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v1

    #@e
    if-nez v1, :cond_1b

    #@10
    .line 834
    :cond_10
    const-string v1, "/sys/devices/system/cpu/cpufreq/ondemand/powersave_bias"

    #@12
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-direct {p0, v1, v2}, Lcom/lge/systemservice/service/CoreControlService;->writeCpuSysFile(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 835
    iput p1, p0, Lcom/lge/systemservice/service/CoreControlService;->mCurPowerSaveBias:I

    #@1b
    .line 837
    :cond_1b
    return-void
.end method

.method private writeCpuSysFile(Ljava/lang/String;Ljava/lang/String;)I
    .registers 10
    .parameter "filePath"
    .parameter "stringValue"

    #@0
    .prologue
    .line 303
    const/4 v3, 0x0

    #@1
    .line 304
    .local v3, ret:I
    const/4 v1, 0x0

    #@2
    .line 307
    .local v1, outWriter:Ljava/io/BufferedWriter;
    :try_start_2
    new-instance v2, Ljava/io/BufferedWriter;

    #@4
    new-instance v4, Ljava/io/FileWriter;

    #@6
    invoke-direct {v4, p1}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    #@9
    invoke-direct {v2, v4}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_c
    .catchall {:try_start_2 .. :try_end_c} :catchall_59
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_c} :catch_20

    #@c
    .line 308
    .end local v1           #outWriter:Ljava/io/BufferedWriter;
    .local v2, outWriter:Ljava/io/BufferedWriter;
    :try_start_c
    invoke-virtual {v2, p2}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@f
    .line 309
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->flush()V

    #@12
    .line 310
    invoke-virtual {p2}, Ljava/lang/String;->length()I
    :try_end_15
    .catchall {:try_start_c .. :try_end_15} :catchall_62
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_15} :catch_65

    #@15
    move-result v3

    #@16
    .line 320
    if-eqz v2, :cond_1b

    #@18
    .line 322
    :try_start_18
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->close()V
    :try_end_1b
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_1b} :catch_1d

    #@1b
    :cond_1b
    move-object v1, v2

    #@1c
    .line 327
    .end local v2           #outWriter:Ljava/io/BufferedWriter;
    .restart local v1       #outWriter:Ljava/io/BufferedWriter;
    :cond_1c
    :goto_1c
    return v3

    #@1d
    .line 324
    .end local v1           #outWriter:Ljava/io/BufferedWriter;
    .restart local v2       #outWriter:Ljava/io/BufferedWriter;
    :catch_1d
    move-exception v4

    #@1e
    move-object v1, v2

    #@1f
    .line 325
    .end local v2           #outWriter:Ljava/io/BufferedWriter;
    .restart local v1       #outWriter:Ljava/io/BufferedWriter;
    goto :goto_1c

    #@20
    .line 316
    :catch_20
    move-exception v0

    #@21
    .line 317
    .local v0, exception:Ljava/lang/Exception;
    :goto_21
    :try_start_21
    const-string v4, "CoreControlService"

    #@23
    new-instance v5, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v6, "cpu sys file write error: "

    #@2a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v5

    #@2e
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v5

    #@32
    const-string v6, " as "

    #@34
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v5

    #@38
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v5

    #@3c
    const-string v6, ", "

    #@3e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v5

    #@42
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@45
    move-result-object v6

    #@46
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v5

    #@4a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v5

    #@4e
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_51
    .catchall {:try_start_21 .. :try_end_51} :catchall_59

    #@51
    .line 320
    if-eqz v1, :cond_1c

    #@53
    .line 322
    :try_start_53
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_56
    .catch Ljava/lang/Exception; {:try_start_53 .. :try_end_56} :catch_57

    #@56
    goto :goto_1c

    #@57
    .line 324
    :catch_57
    move-exception v4

    #@58
    goto :goto_1c

    #@59
    .line 319
    .end local v0           #exception:Ljava/lang/Exception;
    :catchall_59
    move-exception v4

    #@5a
    .line 320
    :goto_5a
    if-eqz v1, :cond_5f

    #@5c
    .line 322
    :try_start_5c
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_5f
    .catch Ljava/lang/Exception; {:try_start_5c .. :try_end_5f} :catch_60

    #@5f
    .line 324
    :cond_5f
    :goto_5f
    throw v4

    #@60
    :catch_60
    move-exception v5

    #@61
    goto :goto_5f

    #@62
    .line 319
    .end local v1           #outWriter:Ljava/io/BufferedWriter;
    .restart local v2       #outWriter:Ljava/io/BufferedWriter;
    :catchall_62
    move-exception v4

    #@63
    move-object v1, v2

    #@64
    .end local v2           #outWriter:Ljava/io/BufferedWriter;
    .restart local v1       #outWriter:Ljava/io/BufferedWriter;
    goto :goto_5a

    #@65
    .line 316
    .end local v1           #outWriter:Ljava/io/BufferedWriter;
    .restart local v2       #outWriter:Ljava/io/BufferedWriter;
    :catch_65
    move-exception v0

    #@66
    move-object v1, v2

    #@67
    .end local v2           #outWriter:Ljava/io/BufferedWriter;
    .restart local v1       #outWriter:Ljava/io/BufferedWriter;
    goto :goto_21
.end method


# virtual methods
.method public changeCoreState(I)V
    .registers 8
    .parameter "state"

    #@0
    .prologue
    .line 113
    const-string v3, "CoreControlService"

    #@2
    new-instance v4, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v5, "Trying to change CoreState, "

    #@9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v4

    #@d
    sget v5, Lcom/lge/systemservice/service/CoreControlService;->coreMode:I

    #@f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v4

    #@13
    const-string v5, " -> "

    #@15
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v4

    #@19
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v4

    #@1d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v4

    #@21
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 115
    sget v3, Lcom/lge/systemservice/service/CoreControlService;->coreMode:I

    #@26
    if-ne v3, p1, :cond_29

    #@28
    .line 154
    :goto_28
    return-void

    #@29
    .line 118
    :cond_29
    sget v0, Lcom/lge/systemservice/service/CoreControlService;->coreMode:I

    #@2b
    .line 120
    .local v0, beforeCoreMode:I
    const/4 v2, 0x0

    #@2c
    .line 122
    .local v2, succeeded:Z
    packed-switch p1, :pswitch_data_9e

    #@2f
    .line 140
    :goto_2f
    :pswitch_2f
    if-nez v2, :cond_78

    #@31
    .line 142
    const-string v3, "CoreControlService"

    #@33
    new-instance v4, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v5, "Failed to change CoreState,"

    #@3a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v4

    #@3e
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@41
    move-result-object v4

    #@42
    const-string v5, " -> "

    #@44
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v4

    #@48
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v4

    #@4c
    const-string v5, ", forcedToNormal()"

    #@4e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v4

    #@52
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v4

    #@56
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@59
    .line 143
    invoke-direct {p0}, Lcom/lge/systemservice/service/CoreControlService;->forcedToNormal()Z

    #@5c
    goto :goto_28

    #@5d
    .line 124
    :pswitch_5d
    :try_start_5d
    invoke-direct {p0}, Lcom/lge/systemservice/service/CoreControlService;->setAsSingleMode()Z

    #@60
    move-result v2

    #@61
    .line 125
    goto :goto_2f

    #@62
    .line 128
    :pswitch_62
    invoke-direct {p0}, Lcom/lge/systemservice/service/CoreControlService;->setAsDualMode()Z

    #@65
    move-result v2

    #@66
    .line 129
    goto :goto_2f

    #@67
    .line 132
    :pswitch_67
    invoke-direct {p0}, Lcom/lge/systemservice/service/CoreControlService;->setAsQuadMode()Z
    :try_end_6a
    .catch Lcom/lge/systemservice/service/CoreControlService$CoreControlFailedException; {:try_start_5d .. :try_end_6a} :catch_6c

    #@6a
    move-result v2

    #@6b
    goto :goto_2f

    #@6c
    .line 135
    :catch_6c
    move-exception v1

    #@6d
    .line 136
    .local v1, e:Lcom/lge/systemservice/service/CoreControlService$CoreControlFailedException;
    const-string v3, "CoreControlService"

    #@6f
    invoke-virtual {v1}, Lcom/lge/systemservice/service/CoreControlService$CoreControlFailedException;->getMessage()Ljava/lang/String;

    #@72
    move-result-object v4

    #@73
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@76
    .line 137
    const/4 v2, 0x0

    #@77
    goto :goto_2f

    #@78
    .line 147
    .end local v1           #e:Lcom/lge/systemservice/service/CoreControlService$CoreControlFailedException;
    :cond_78
    const-string v3, "CoreControlService"

    #@7a
    new-instance v4, Ljava/lang/StringBuilder;

    #@7c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7f
    const-string v5, "Succeeded to change CoreState,"

    #@81
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v4

    #@85
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@88
    move-result-object v4

    #@89
    const-string v5, " -> "

    #@8b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v4

    #@8f
    sget v5, Lcom/lge/systemservice/service/CoreControlService;->coreMode:I

    #@91
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@94
    move-result-object v4

    #@95
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@98
    move-result-object v4

    #@99
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@9c
    goto :goto_28

    #@9d
    .line 122
    nop

    #@9e
    :pswitch_data_9e
    .packed-switch 0x1
        :pswitch_5d
        :pswitch_62
        :pswitch_2f
        :pswitch_67
    .end packed-switch
.end method

.method public changeDisableEDP(I)V
    .registers 2
    .parameter "disable"

    #@0
    .prologue
    .line 166
    return-void
.end method

.method public changeMaxFrequency(I)V
    .registers 5
    .parameter "state"

    #@0
    .prologue
    .line 158
    const-string v0, "CoreControlService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "changeMaxFrequency state "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 160
    const-string v0, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq"

    #@1a
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-direct {p0, v0, v1}, Lcom/lge/systemservice/service/CoreControlService;->writeCpuSysFile(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 161
    return-void
.end method

.method public getCoreState()I
    .registers 4

    #@0
    .prologue
    .line 106
    const-string v0, "CoreControlService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "getCoreState state: "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    sget v2, Lcom/lge/systemservice/service/CoreControlService;->coreMode:I

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 108
    sget v0, Lcom/lge/systemservice/service/CoreControlService;->coreMode:I

    #@1c
    return v0
.end method

.method public nativeCoreControlNotification(I)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 89
    packed-switch p1, :pswitch_data_10

    #@3
    .line 101
    :goto_3
    return-void

    #@4
    .line 94
    :pswitch_4
    invoke-direct {p0}, Lcom/lge/systemservice/service/CoreControlService;->isEcoModeEnabled()Z

    #@7
    move-result v0

    #@8
    .line 95
    .local v0, ecoMode:Z
    invoke-direct {p0}, Lcom/lge/systemservice/service/CoreControlService;->resetLowPowerModePolicyState()V

    #@b
    .line 96
    const/4 v1, 0x2

    #@c
    invoke-virtual {p0, v1, v0}, Lcom/lge/systemservice/service/CoreControlService;->setLowPowerModePolicy(IZ)V

    #@f
    goto :goto_3

    #@10
    .line 89
    :pswitch_data_10
    .packed-switch 0x0
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public registerEcoModeObserver()V
    .registers 5

    #@0
    .prologue
    .line 554
    iget-object v0, p0, Lcom/lge/systemservice/service/CoreControlService;->mEcoModeObserver:Landroid/database/ContentObserver;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 555
    invoke-virtual {p0}, Lcom/lge/systemservice/service/CoreControlService;->unregisterEcoModeObserver()V

    #@7
    .line 558
    :cond_7
    new-instance v0, Lcom/lge/systemservice/service/CoreControlService$1;

    #@9
    iget-object v1, p0, Lcom/lge/systemservice/service/CoreControlService;->mHandler:Landroid/os/Handler;

    #@b
    invoke-direct {v0, p0, v1}, Lcom/lge/systemservice/service/CoreControlService$1;-><init>(Lcom/lge/systemservice/service/CoreControlService;Landroid/os/Handler;)V

    #@e
    iput-object v0, p0, Lcom/lge/systemservice/service/CoreControlService;->mEcoModeObserver:Landroid/database/ContentObserver;

    #@10
    .line 575
    iget-object v0, p0, Lcom/lge/systemservice/service/CoreControlService;->mContext:Landroid/content/Context;

    #@12
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@15
    move-result-object v0

    #@16
    iget-object v1, p0, Lcom/lge/systemservice/service/CoreControlService;->ECO_MODE_URI:Landroid/net/Uri;

    #@18
    const/4 v2, 0x1

    #@19
    iget-object v3, p0, Lcom/lge/systemservice/service/CoreControlService;->mEcoModeObserver:Landroid/database/ContentObserver;

    #@1b
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@1e
    .line 576
    return-void
.end method

.method public setLowPowerModePolicy(IZ)V
    .registers 8
    .parameter "policy"
    .parameter "enabled"

    #@0
    .prologue
    .line 171
    iget-object v0, p0, Lcom/lge/systemservice/service/CoreControlService;->mPolicyProperties:[Lcom/lge/systemservice/service/CoreControlService$LPMPolicy;

    #@2
    if-eqz v0, :cond_9

    #@4
    if-ltz p1, :cond_9

    #@6
    const/4 v0, 0x3

    #@7
    if-lt p1, v0, :cond_a

    #@9
    .line 180
    :cond_9
    :goto_9
    return-void

    #@a
    .line 175
    :cond_a
    iget-object v1, p0, Lcom/lge/systemservice/service/CoreControlService;->mSetLowPowerModeTask:Ljava/lang/Runnable;

    #@c
    monitor-enter v1

    #@d
    .line 176
    :try_start_d
    iget-object v0, p0, Lcom/lge/systemservice/service/CoreControlService;->mHandler:Landroid/os/Handler;

    #@f
    iget-object v2, p0, Lcom/lge/systemservice/service/CoreControlService;->mSetLowPowerModeTask:Ljava/lang/Runnable;

    #@11
    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@14
    .line 177
    iget-object v0, p0, Lcom/lge/systemservice/service/CoreControlService;->mPolicyProperties:[Lcom/lge/systemservice/service/CoreControlService$LPMPolicy;

    #@16
    aget-object v0, v0, p1

    #@18
    iput-boolean p2, v0, Lcom/lge/systemservice/service/CoreControlService$LPMPolicy;->mEnabled:Z

    #@1a
    .line 178
    iget-object v0, p0, Lcom/lge/systemservice/service/CoreControlService;->mHandler:Landroid/os/Handler;

    #@1c
    iget-object v2, p0, Lcom/lge/systemservice/service/CoreControlService;->mSetLowPowerModeTask:Ljava/lang/Runnable;

    #@1e
    const-wide/16 v3, 0x0

    #@20
    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@23
    .line 179
    monitor-exit v1

    #@24
    goto :goto_9

    #@25
    :catchall_25
    move-exception v0

    #@26
    monitor-exit v1
    :try_end_27
    .catchall {:try_start_d .. :try_end_27} :catchall_25

    #@27
    throw v0
.end method

.method public toString()Ljava/lang/String;
    .registers 9

    #@0
    .prologue
    .line 474
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@3
    move-result-object v4

    #@4
    .line 476
    .local v4, text:Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v5

    #@d
    const-string v6, "\nCore mode: "

    #@f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v5

    #@13
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v4

    #@17
    .line 477
    sget v5, Lcom/lge/systemservice/service/CoreControlService;->coreMode:I

    #@19
    packed-switch v5, :pswitch_data_fa

    #@1c
    .line 492
    :pswitch_1c
    new-instance v5, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v5

    #@25
    const-string v6, "unknown core mode :"

    #@27
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v5

    #@2b
    sget v6, Lcom/lge/systemservice/service/CoreControlService;->coreMode:I

    #@2d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30
    move-result-object v5

    #@31
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v4

    #@35
    .line 496
    :goto_35
    new-instance v5, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v5

    #@3e
    const-string v6, "\nCore state:"

    #@40
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v5

    #@44
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v4

    #@48
    .line 497
    const/4 v1, 0x0

    #@49
    .local v1, i:I
    :goto_49
    const/4 v5, 0x4

    #@4a
    if-ge v1, v5, :cond_da

    #@4c
    .line 499
    new-instance v5, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v6, "/sys/devices/system/cpu/cpu"

    #@53
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v5

    #@57
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@5a
    move-result-object v6

    #@5b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v5

    #@5f
    const-string v6, "/online"

    #@61
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v5

    #@65
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v3

    #@69
    .line 500
    .local v3, onlineFilePath:Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    #@6b
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@6e
    .line 501
    .local v2, onlineFile:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    #@71
    move-result v5

    #@72
    if-eqz v5, :cond_99

    #@74
    .line 503
    invoke-direct {p0, v3}, Lcom/lge/systemservice/service/CoreControlService;->readCpuSysFile(Ljava/lang/String;)Ljava/lang/String;

    #@77
    move-result-object v0

    #@78
    .line 504
    .local v0, currentState:Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    #@7a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@7d
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v5

    #@81
    const-string v6, "\n\t"

    #@83
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v5

    #@87
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v5

    #@8b
    const-string v6, ":"

    #@8d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v5

    #@91
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v5

    #@95
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@98
    move-result-object v4

    #@99
    .line 497
    .end local v0           #currentState:Ljava/lang/String;
    :cond_99
    add-int/lit8 v1, v1, 0x1

    #@9b
    goto :goto_49

    #@9c
    .line 480
    .end local v1           #i:I
    .end local v2           #onlineFile:Ljava/io/File;
    .end local v3           #onlineFilePath:Ljava/lang/String;
    :pswitch_9c
    new-instance v5, Ljava/lang/StringBuilder;

    #@9e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@a1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v5

    #@a5
    const-string v6, "CORE_MODE_SINGLE"

    #@a7
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v5

    #@ab
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ae
    move-result-object v4

    #@af
    .line 481
    goto :goto_35

    #@b0
    .line 484
    :pswitch_b0
    new-instance v5, Ljava/lang/StringBuilder;

    #@b2
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@b5
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v5

    #@b9
    const-string v6, "CORE_MODE_DUAL"

    #@bb
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v5

    #@bf
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c2
    move-result-object v4

    #@c3
    .line 485
    goto/16 :goto_35

    #@c5
    .line 488
    :pswitch_c5
    new-instance v5, Ljava/lang/StringBuilder;

    #@c7
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@ca
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v5

    #@ce
    const-string v6, "CORE_MODE_QUAD"

    #@d0
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d3
    move-result-object v5

    #@d4
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d7
    move-result-object v4

    #@d8
    .line 489
    goto/16 :goto_35

    #@da
    .line 508
    .restart local v1       #i:I
    :cond_da
    new-instance v5, Ljava/lang/StringBuilder;

    #@dc
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@df
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v5

    #@e3
    const-string v6, "\nsys.lge.core.manual: "

    #@e5
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e8
    move-result-object v5

    #@e9
    const-string v6, "sys.lge.core.manual"

    #@eb
    const-string v7, "0"

    #@ed
    invoke-static {v6, v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@f0
    move-result-object v6

    #@f1
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v5

    #@f5
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f8
    move-result-object v4

    #@f9
    .line 509
    return-object v4

    #@fa
    .line 477
    :pswitch_data_fa
    .packed-switch 0x1
        :pswitch_9c
        :pswitch_b0
        :pswitch_1c
        :pswitch_c5
    .end packed-switch
.end method

.method public unregisterEcoModeObserver()V
    .registers 3

    #@0
    .prologue
    .line 580
    iget-object v0, p0, Lcom/lge/systemservice/service/CoreControlService;->mEcoModeObserver:Landroid/database/ContentObserver;

    #@2
    if-eqz v0, :cond_12

    #@4
    .line 581
    iget-object v0, p0, Lcom/lge/systemservice/service/CoreControlService;->mContext:Landroid/content/Context;

    #@6
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@9
    move-result-object v0

    #@a
    iget-object v1, p0, Lcom/lge/systemservice/service/CoreControlService;->mEcoModeObserver:Landroid/database/ContentObserver;

    #@c
    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    #@f
    .line 582
    const/4 v0, 0x0

    #@10
    iput-object v0, p0, Lcom/lge/systemservice/service/CoreControlService;->mEcoModeObserver:Landroid/database/ContentObserver;

    #@12
    .line 584
    :cond_12
    return-void
.end method
