.class Lcom/lge/systemservice/service/CliptrayService$cliptrayEventReceiver;
.super Landroid/content/BroadcastReceiver;
.source "CliptrayService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/CliptrayService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "cliptrayEventReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/service/CliptrayService;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/service/CliptrayService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 513
    iput-object p1, p0, Lcom/lge/systemservice/service/CliptrayService$cliptrayEventReceiver;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 9
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 517
    const-string v3, "CliptrayService"

    #@3
    const-string v4, "cliptrayEventReceiver :onReceive"

    #@5
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 518
    iget-object v3, p0, Lcom/lge/systemservice/service/CliptrayService$cliptrayEventReceiver;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@a
    invoke-static {v3}, Lcom/lge/systemservice/service/CliptrayService;->access$200(Lcom/lge/systemservice/service/CliptrayService;)Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;

    #@d
    move-result-object v3

    #@e
    if-nez v3, :cond_11

    #@10
    .line 563
    :cond_10
    :goto_10
    return-void

    #@11
    .line 521
    :cond_11
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    const-string v4, "android.intent.action.CONFIGURATION_CHANGED"

    #@17
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1a
    move-result v3

    #@1b
    if-eqz v3, :cond_97

    #@1d
    .line 522
    iget-object v3, p0, Lcom/lge/systemservice/service/CliptrayService$cliptrayEventReceiver;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@1f
    invoke-static {v3}, Lcom/lge/systemservice/service/CliptrayService;->access$200(Lcom/lge/systemservice/service/CliptrayService;)Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3}, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->isShowing()Z

    #@26
    move-result v3

    #@27
    if-eqz v3, :cond_3e

    #@29
    iget-object v3, p0, Lcom/lge/systemservice/service/CliptrayService$cliptrayEventReceiver;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@2b
    invoke-static {v3}, Lcom/lge/systemservice/service/CliptrayService;->access$100(Lcom/lge/systemservice/service/CliptrayService;)Landroid/view/View;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    #@32
    move-result v3

    #@33
    if-nez v3, :cond_3e

    #@35
    .line 523
    iget-object v3, p0, Lcom/lge/systemservice/service/CliptrayService$cliptrayEventReceiver;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@37
    invoke-static {v3}, Lcom/lge/systemservice/service/CliptrayService;->access$200(Lcom/lge/systemservice/service/CliptrayService;)Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;

    #@3a
    move-result-object v3

    #@3b
    invoke-virtual {v3}, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->hideCliptraycue()V

    #@3e
    .line 525
    :cond_3e
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@41
    move-result-object v3

    #@42
    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@45
    move-result-object v3

    #@46
    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    #@48
    if-ne v3, v5, :cond_90

    #@4a
    .line 526
    iget-object v3, p0, Lcom/lge/systemservice/service/CliptrayService$cliptrayEventReceiver;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@4c
    invoke-static {v3, v5}, Lcom/lge/systemservice/service/CliptrayService;->access$1000(Lcom/lge/systemservice/service/CliptrayService;Z)V

    #@4f
    .line 530
    :goto_4f
    iget-object v3, p0, Lcom/lge/systemservice/service/CliptrayService$cliptrayEventReceiver;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@51
    invoke-static {v3}, Lcom/lge/systemservice/service/CliptrayService;->access$200(Lcom/lge/systemservice/service/CliptrayService;)Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;

    #@54
    move-result-object v3

    #@55
    invoke-virtual {v3}, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->orientationChanged()V

    #@58
    .line 533
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@5b
    move-result-object v3

    #@5c
    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@5f
    move-result-object v3

    #@60
    iget-object v3, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@62
    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    #@65
    move-result-object v0

    #@66
    .line 534
    .local v0, currLocale:Ljava/lang/String;
    iget-object v3, p0, Lcom/lge/systemservice/service/CliptrayService$cliptrayEventReceiver;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@68
    invoke-static {v3}, Lcom/lge/systemservice/service/CliptrayService;->access$1400(Lcom/lge/systemservice/service/CliptrayService;)Ljava/lang/String;

    #@6b
    move-result-object v3

    #@6c
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6f
    move-result v3

    #@70
    if-nez v3, :cond_80

    #@72
    .line 535
    iget-object v3, p0, Lcom/lge/systemservice/service/CliptrayService$cliptrayEventReceiver;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@74
    invoke-static {v3, v0}, Lcom/lge/systemservice/service/CliptrayService;->access$1402(Lcom/lge/systemservice/service/CliptrayService;Ljava/lang/String;)Ljava/lang/String;

    #@77
    .line 536
    iget-object v3, p0, Lcom/lge/systemservice/service/CliptrayService$cliptrayEventReceiver;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@79
    invoke-static {v3}, Lcom/lge/systemservice/service/CliptrayService;->access$400(Lcom/lge/systemservice/service/CliptrayService;)Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@7c
    move-result-object v3

    #@7d
    invoke-virtual {v3}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->updateLocaleInfo()V

    #@80
    .line 539
    :cond_80
    iget-object v3, p0, Lcom/lge/systemservice/service/CliptrayService$cliptrayEventReceiver;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@82
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@85
    move-result-object v4

    #@86
    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@89
    move-result-object v4

    #@8a
    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    #@8c
    invoke-static {v3, v4}, Lcom/lge/systemservice/service/CliptrayService;->access$1502(Lcom/lge/systemservice/service/CliptrayService;I)I

    #@8f
    goto :goto_10

    #@90
    .line 528
    .end local v0           #currLocale:Ljava/lang/String;
    :cond_90
    iget-object v3, p0, Lcom/lge/systemservice/service/CliptrayService$cliptrayEventReceiver;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@92
    const/4 v4, 0x0

    #@93
    invoke-static {v3, v4}, Lcom/lge/systemservice/service/CliptrayService;->access$1000(Lcom/lge/systemservice/service/CliptrayService;Z)V

    #@96
    goto :goto_4f

    #@97
    .line 542
    :cond_97
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@9a
    move-result-object v3

    #@9b
    const-string v4, "android.intent.action.PHONE_STATE"

    #@9d
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a0
    move-result v3

    #@a1
    if-eqz v3, :cond_d9

    #@a3
    .line 543
    const-string v3, "state"

    #@a5
    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@a8
    move-result-object v2

    #@a9
    .line 544
    .local v2, state:Ljava/lang/String;
    if-eqz v2, :cond_10

    #@ab
    .line 545
    sget-object v3, Landroid/telephony/TelephonyManager;->EXTRA_STATE_RINGING:Ljava/lang/String;

    #@ad
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b0
    move-result v3

    #@b1
    if-nez v3, :cond_bb

    #@b3
    sget-object v3, Landroid/telephony/TelephonyManager;->EXTRA_STATE_OFFHOOK:Ljava/lang/String;

    #@b5
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b8
    move-result v3

    #@b9
    if-eqz v3, :cond_c6

    #@bb
    .line 547
    :cond_bb
    iget-object v3, p0, Lcom/lge/systemservice/service/CliptrayService$cliptrayEventReceiver;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@bd
    invoke-static {v3}, Lcom/lge/systemservice/service/CliptrayService;->access$200(Lcom/lge/systemservice/service/CliptrayService;)Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;

    #@c0
    move-result-object v3

    #@c1
    invoke-virtual {v3}, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->hideCliptraycue()V

    #@c4
    goto/16 :goto_10

    #@c6
    .line 549
    :cond_c6
    sget-object v3, Landroid/telephony/TelephonyManager;->EXTRA_STATE_IDLE:Ljava/lang/String;

    #@c8
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@cb
    move-result v3

    #@cc
    if-eqz v3, :cond_10

    #@ce
    .line 550
    iget-object v3, p0, Lcom/lge/systemservice/service/CliptrayService$cliptrayEventReceiver;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@d0
    invoke-static {v3}, Lcom/lge/systemservice/service/CliptrayService;->access$200(Lcom/lge/systemservice/service/CliptrayService;)Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;

    #@d3
    move-result-object v3

    #@d4
    invoke-virtual {v3}, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->hideCliptraycue()V

    #@d7
    goto/16 :goto_10

    #@d9
    .line 554
    .end local v2           #state:Ljava/lang/String;
    :cond_d9
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@dc
    move-result-object v3

    #@dd
    const-string v4, "com.lge.camera.action.LGE_CAMERA_STARTED"

    #@df
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e2
    move-result v3

    #@e3
    if-eqz v3, :cond_10

    #@e5
    .line 555
    const-string v3, "isStart"

    #@e7
    invoke-virtual {p2, v3, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@ea
    move-result v1

    #@eb
    .line 556
    .local v1, isStart:Z
    if-eqz v1, :cond_f8

    #@ed
    .line 557
    iget-object v3, p0, Lcom/lge/systemservice/service/CliptrayService$cliptrayEventReceiver;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@ef
    invoke-static {v3}, Lcom/lge/systemservice/service/CliptrayService;->access$200(Lcom/lge/systemservice/service/CliptrayService;)Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;

    #@f2
    move-result-object v3

    #@f3
    invoke-virtual {v3}, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->hideCliptraycue()V

    #@f6
    goto/16 :goto_10

    #@f8
    .line 560
    :cond_f8
    iget-object v3, p0, Lcom/lge/systemservice/service/CliptrayService$cliptrayEventReceiver;->this$0:Lcom/lge/systemservice/service/CliptrayService;

    #@fa
    invoke-static {v3}, Lcom/lge/systemservice/service/CliptrayService;->access$200(Lcom/lge/systemservice/service/CliptrayService;)Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;

    #@fd
    move-result-object v3

    #@fe
    invoke-virtual {v3}, Lcom/lge/systemservice/service/CliptrayService$Cliptraycue;->showCliptraycueClose()V

    #@101
    goto/16 :goto_10
.end method
