.class Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;
.super Ljava/lang/Object;
.source "WatchNetStorageService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/service/WatchNetStorageService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "WatchMountDevice"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice$DeleteGabageFilesThread;,
        Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice$UnmountThread;
    }
.end annotation


# instance fields
.field aliveCheckTime:[J

.field localMountlist:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field localSession:I

.field mStopFlag:Z

.field final synthetic this$0:Lcom/lge/systemservice/service/WatchNetStorageService;


# direct methods
.method public constructor <init>(Lcom/lge/systemservice/service/WatchNetStorageService;ILjava/util/List;)V
    .registers 8
    .parameter
    .parameter "session"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 448
    .local p3, tempMountList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->this$0:Lcom/lge/systemservice/service/WatchNetStorageService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 442
    const/4 v1, 0x0

    #@6
    iput-boolean v1, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->mStopFlag:Z

    #@8
    .line 449
    iput p2, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->localSession:I

    #@a
    .line 450
    iput-object p3, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->localMountlist:Ljava/util/List;

    #@c
    .line 451
    invoke-interface {p3}, Ljava/util/List;->size()I

    #@f
    move-result v1

    #@10
    new-array v1, v1, [J

    #@12
    iput-object v1, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->aliveCheckTime:[J

    #@14
    .line 452
    const/4 v0, 0x0

    #@15
    .local v0, i:I
    :goto_15
    iget-object v1, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->aliveCheckTime:[J

    #@17
    array-length v1, v1

    #@18
    if-ge v0, v1, :cond_23

    #@1a
    .line 453
    iget-object v1, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->aliveCheckTime:[J

    #@1c
    const-wide/16 v2, 0x0

    #@1e
    aput-wide v2, v1, v0

    #@20
    .line 452
    add-int/lit8 v0, v0, 0x1

    #@22
    goto :goto_15

    #@23
    .line 454
    :cond_23
    return-void
.end method


# virtual methods
.method public disconnect()V
    .registers 15

    #@0
    .prologue
    const/4 v13, 0x1

    #@1
    .line 546
    const/4 v4, 0x0

    #@2
    .line 548
    .local v4, mountPoint:Ljava/lang/String;
    const/4 v0, 0x0

    #@3
    .local v0, i:I
    iget-object v10, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->aliveCheckTime:[J

    #@5
    array-length v10, v10

    #@6
    if-ge v0, v10, :cond_38

    #@8
    .line 550
    iget-object v10, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->localMountlist:Ljava/util/List;

    #@a
    invoke-interface {v10, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@d
    move-result-object v4

    #@e
    .end local v4           #mountPoint:Ljava/lang/String;
    check-cast v4, Ljava/lang/String;

    #@10
    .line 552
    .restart local v4       #mountPoint:Ljava/lang/String;
    iget-object v10, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->this$0:Lcom/lge/systemservice/service/WatchNetStorageService;

    #@12
    invoke-static {v10, v4}, Lcom/lge/systemservice/service/WatchNetStorageService;->access$200(Lcom/lge/systemservice/service/WatchNetStorageService;Ljava/lang/String;)Z

    #@15
    move-result v10

    #@16
    if-nez v10, :cond_54

    #@18
    .line 554
    const-string v10, "WatchNetStorageService"

    #@1a
    new-instance v11, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v12, "[checkAliveTimeThread FINISH] "

    #@21
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v11

    #@25
    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v11

    #@29
    const-string v12, " is not mounted."

    #@2b
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v11

    #@2f
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v11

    #@33
    invoke-static {v10, v11}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 555
    iput-boolean v13, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->mStopFlag:Z

    #@38
    .line 598
    :cond_38
    :goto_38
    iget-object v10, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->localMountlist:Ljava/util/List;

    #@3a
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@3d
    move-result-object v2

    #@3e
    .line 599
    .local v2, iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_3e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@41
    move-result v10

    #@42
    if-eqz v10, :cond_b2

    #@44
    .line 600
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@47
    move-result-object v9

    #@48
    check-cast v9, Ljava/lang/String;

    #@4a
    .line 601
    .local v9, unmountPoint:Ljava/lang/String;
    new-instance v10, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice$DeleteGabageFilesThread;

    #@4c
    const/4 v11, 0x3

    #@4d
    invoke-direct {v10, p0, v9, v11}, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice$DeleteGabageFilesThread;-><init>(Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;Ljava/lang/String;I)V

    #@50
    invoke-virtual {v10}, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice$DeleteGabageFilesThread;->start()V

    #@53
    goto :goto_3e

    #@54
    .line 558
    .end local v2           #iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v9           #unmountPoint:Ljava/lang/String;
    :cond_54
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@57
    move-result-wide v5

    #@58
    .line 566
    .local v5, now:J
    iget-object v10, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->localMountlist:Ljava/util/List;

    #@5a
    invoke-interface {v10}, Ljava/util/List;->size()I

    #@5d
    move-result v7

    #@5e
    .line 567
    .local v7, thread_count:I
    new-array v8, v7, [Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice$UnmountThread;

    #@60
    .line 568
    .local v8, threads:[Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice$UnmountThread;
    iget-object v10, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->localMountlist:Ljava/util/List;

    #@62
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@65
    move-result-object v2

    #@66
    .line 569
    .restart local v2       #iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    const/4 v3, 0x0

    #@67
    .line 570
    .local v3, iterCount:I
    :goto_67
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@6a
    move-result v10

    #@6b
    if-eqz v10, :cond_8b

    #@6d
    .line 571
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@70
    move-result-object v9

    #@71
    check-cast v9, Ljava/lang/String;

    #@73
    .line 573
    .restart local v9       #unmountPoint:Ljava/lang/String;
    new-instance v10, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice$UnmountThread;

    #@75
    iget v11, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->localSession:I

    #@77
    invoke-direct {v10, p0, v9, v11}, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice$UnmountThread;-><init>(Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;Ljava/lang/String;I)V

    #@7a
    aput-object v10, v8, v3

    #@7c
    .line 574
    aget-object v10, v8, v3

    #@7e
    invoke-virtual {v10}, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice$UnmountThread;->start()V

    #@81
    .line 575
    add-int/lit8 v3, v3, 0x1

    #@83
    .line 578
    const-wide/16 v10, 0x64

    #@85
    :try_start_85
    invoke-static {v10, v11}, Ljava/lang/Thread;->sleep(J)V
    :try_end_88
    .catch Ljava/lang/InterruptedException; {:try_start_85 .. :try_end_88} :catch_89

    #@88
    goto :goto_67

    #@89
    .line 579
    :catch_89
    move-exception v10

    #@8a
    goto :goto_67

    #@8b
    .line 582
    .end local v9           #unmountPoint:Ljava/lang/String;
    :cond_8b
    const/4 v1, 0x0

    #@8c
    .local v1, idx:I
    :goto_8c
    if-ge v1, v3, :cond_98

    #@8e
    .line 585
    :try_start_8e
    aget-object v10, v8, v1

    #@90
    const-wide/16 v11, 0xfa0

    #@92
    invoke-virtual {v10, v11, v12}, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice$UnmountThread;->join(J)V
    :try_end_95
    .catch Ljava/lang/InterruptedException; {:try_start_8e .. :try_end_95} :catch_b3

    #@95
    .line 582
    :goto_95
    add-int/lit8 v1, v1, 0x1

    #@97
    goto :goto_8c

    #@98
    .line 588
    :cond_98
    iget-object v10, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->this$0:Lcom/lge/systemservice/service/WatchNetStorageService;

    #@9a
    iget-object v10, v10, Lcom/lge/systemservice/service/WatchNetStorageService;->mWatchThreadList:Ljava/util/Map;

    #@9c
    iget v11, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->localSession:I

    #@9e
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a1
    move-result-object v11

    #@a2
    invoke-interface {v10, v11}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@a5
    .line 589
    invoke-virtual {p0}, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->stopThread()V

    #@a8
    .line 590
    iget-object v10, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->this$0:Lcom/lge/systemservice/service/WatchNetStorageService;

    #@aa
    iget v11, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->localSession:I

    #@ac
    invoke-static {v10, v11}, Lcom/lge/systemservice/service/WatchNetStorageService;->access$400(Lcom/lge/systemservice/service/WatchNetStorageService;I)V

    #@af
    .line 591
    iput-boolean v13, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->mStopFlag:Z

    #@b1
    goto :goto_38

    #@b2
    .line 607
    .end local v1           #idx:I
    .end local v3           #iterCount:I
    .end local v5           #now:J
    .end local v7           #thread_count:I
    .end local v8           #threads:[Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice$UnmountThread;
    :cond_b2
    return-void

    #@b3
    .line 586
    .restart local v1       #idx:I
    .restart local v3       #iterCount:I
    .restart local v5       #now:J
    .restart local v7       #thread_count:I
    .restart local v8       #threads:[Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice$UnmountThread;
    :catch_b3
    move-exception v10

    #@b4
    goto :goto_95
.end method

.method public startThread()V
    .registers 3

    #@0
    .prologue
    .line 458
    new-instance v0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice$1;

    #@2
    const-string v1, "updateAliveTimeThread"

    #@4
    invoke-direct {v0, p0, v1}, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice$1;-><init>(Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;Ljava/lang/String;)V

    #@7
    invoke-virtual {v0}, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice$1;->start()V

    #@a
    .line 466
    return-void
.end method

.method public stopThread()V
    .registers 2

    #@0
    .prologue
    .line 470
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->mStopFlag:Z

    #@3
    .line 471
    return-void
.end method

.method updateAliveTime()V
    .registers 12

    #@0
    .prologue
    const-wide/16 v9, 0x0

    #@2
    const/4 v8, 0x1

    #@3
    .line 475
    const/4 v2, 0x0

    #@4
    .line 482
    .local v2, mountPoint:Ljava/lang/String;
    :cond_4
    :goto_4
    iget-boolean v5, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->mStopFlag:Z

    #@6
    if-nez v5, :cond_92

    #@8
    iget-object v5, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->localMountlist:Ljava/util/List;

    #@a
    invoke-interface {v5}, Ljava/util/List;->size()I

    #@d
    move-result v5

    #@e
    if-eqz v5, :cond_92

    #@10
    .line 485
    iget-boolean v5, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->mStopFlag:Z

    #@12
    if-nez v5, :cond_92

    #@14
    .line 488
    const-wide/16 v5, 0x2710

    #@16
    :try_start_16
    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_19
    .catch Ljava/lang/InterruptedException; {:try_start_16 .. :try_end_19} :catch_90

    #@19
    .line 495
    :goto_19
    const/4 v0, 0x0

    #@1a
    .local v0, i:I
    :goto_1a
    iget-object v5, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->localMountlist:Ljava/util/List;

    #@1c
    invoke-interface {v5}, Ljava/util/List;->size()I

    #@1f
    move-result v5

    #@20
    if-ge v0, v5, :cond_4

    #@22
    .line 496
    iget-boolean v5, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->mStopFlag:Z

    #@24
    if-nez v5, :cond_4

    #@26
    .line 499
    iget-object v5, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->localMountlist:Ljava/util/List;

    #@28
    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@2b
    move-result-object v2

    #@2c
    .end local v2           #mountPoint:Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    #@2e
    .line 501
    .restart local v2       #mountPoint:Ljava/lang/String;
    iget-object v5, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->this$0:Lcom/lge/systemservice/service/WatchNetStorageService;

    #@30
    invoke-static {v5, v2}, Lcom/lge/systemservice/service/WatchNetStorageService;->access$200(Lcom/lge/systemservice/service/WatchNetStorageService;Ljava/lang/String;)Z

    #@33
    move-result v5

    #@34
    if-nez v5, :cond_5b

    #@36
    .line 503
    iget-object v5, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->aliveCheckTime:[J

    #@38
    aput-wide v9, v5, v0

    #@3a
    .line 504
    const-string v5, "WatchNetStorageService"

    #@3c
    new-instance v6, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v7, "[updateAliveTimeThread FINISH] "

    #@43
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v6

    #@47
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v6

    #@4b
    const-string v7, " is not mounted."

    #@4d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v6

    #@51
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v6

    #@55
    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@58
    .line 505
    iput-boolean v8, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->mStopFlag:Z

    #@5a
    goto :goto_4

    #@5b
    .line 510
    :cond_5b
    move-object v1, v2

    #@5c
    .line 511
    .local v1, mMountPoint:Ljava/lang/String;
    iget-boolean v5, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->mStopFlag:Z

    #@5e
    if-nez v5, :cond_4

    #@60
    .line 514
    iget-object v5, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->aliveCheckTime:[J

    #@62
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@65
    move-result-wide v6

    #@66
    aput-wide v6, v5, v0

    #@68
    .line 515
    const-wide/16 v3, 0x0

    #@6a
    .line 516
    .local v3, now:J
    iget-object v5, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->this$0:Lcom/lge/systemservice/service/WatchNetStorageService;

    #@6c
    const-wide/16 v6, 0xa

    #@6e
    invoke-static {v5, v1, v6, v7}, Lcom/lge/systemservice/service/WatchNetStorageService;->access$300(Lcom/lge/systemservice/service/WatchNetStorageService;Ljava/lang/String;J)Z

    #@71
    move-result v5

    #@72
    if-nez v5, :cond_8a

    #@74
    .line 517
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@77
    move-result-wide v3

    #@78
    .line 520
    iget-object v5, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->aliveCheckTime:[J

    #@7a
    aput-wide v9, v5, v0

    #@7c
    .line 521
    new-instance v5, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice$2;

    #@7e
    const-string v6, "DisconnectThread"

    #@80
    invoke-direct {v5, p0, v6}, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice$2;-><init>(Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;Ljava/lang/String;)V

    #@83
    invoke-virtual {v5}, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice$2;->start()V

    #@86
    .line 528
    iput-boolean v8, p0, Lcom/lge/systemservice/service/WatchNetStorageService$WatchMountDevice;->mStopFlag:Z

    #@88
    goto/16 :goto_4

    #@8a
    .line 532
    :cond_8a
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@8d
    .line 495
    add-int/lit8 v0, v0, 0x1

    #@8f
    goto :goto_1a

    #@90
    .line 489
    .end local v0           #i:I
    .end local v1           #mMountPoint:Ljava/lang/String;
    .end local v3           #now:J
    :catch_90
    move-exception v5

    #@91
    goto :goto_19

    #@92
    .line 541
    :cond_92
    return-void
.end method
