.class public Lcom/lge/systemservice/service/LGSystemServerApplication;
.super Landroid/app/Application;
.source "LGSystemServerApplication.java"


# static fields
.field private static final DEBUG:Z


# instance fields
.field private mServiceContext:Lcom/lge/systemservice/core/LGContext;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 17
    sget-object v0, Landroid/os/Build;->TYPE:Ljava/lang/String;

    #@2
    const-string v1, "user"

    #@4
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_e

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    sput-boolean v0, Lcom/lge/systemservice/service/LGSystemServerApplication;->DEBUG:Z

    #@d
    return-void

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_b
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 13
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    #@3
    .line 18
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Lcom/lge/systemservice/service/LGSystemServerApplication;->mServiceContext:Lcom/lge/systemservice/core/LGContext;

    #@6
    return-void
.end method


# virtual methods
.method public onCreate()V
    .registers 2

    #@0
    .prologue
    .line 23
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    #@3
    .line 25
    iget-object v0, p0, Lcom/lge/systemservice/service/LGSystemServerApplication;->mServiceContext:Lcom/lge/systemservice/core/LGContext;

    #@5
    if-nez v0, :cond_e

    #@7
    .line 26
    new-instance v0, Lcom/lge/systemservice/core/LGContextImpl;

    #@9
    invoke-direct {v0, p0}, Lcom/lge/systemservice/core/LGContextImpl;-><init>(Landroid/content/Context;)V

    #@c
    iput-object v0, p0, Lcom/lge/systemservice/service/LGSystemServerApplication;->mServiceContext:Lcom/lge/systemservice/core/LGContext;

    #@e
    .line 28
    :cond_e
    iget-object v0, p0, Lcom/lge/systemservice/service/LGSystemServerApplication;->mServiceContext:Lcom/lge/systemservice/core/LGContext;

    #@10
    invoke-virtual {v0}, Lcom/lge/systemservice/core/LGContext;->startServer()Z

    #@13
    .line 30
    return-void
.end method

.method public onLowMemory()V
    .registers 1

    #@0
    .prologue
    .line 35
    invoke-super {p0}, Landroid/app/Application;->onLowMemory()V

    #@3
    .line 36
    return-void
.end method

.method public onTerminate()V
    .registers 1

    #@0
    .prologue
    .line 41
    invoke-super {p0}, Landroid/app/Application;->onTerminate()V

    #@3
    .line 42
    return-void
.end method
