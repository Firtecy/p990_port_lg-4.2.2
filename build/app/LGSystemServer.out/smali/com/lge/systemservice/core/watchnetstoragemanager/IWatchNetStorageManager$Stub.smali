.class public abstract Lcom/lge/systemservice/core/watchnetstoragemanager/IWatchNetStorageManager$Stub;
.super Landroid/os/Binder;
.source "IWatchNetStorageManager.java"

# interfaces
.implements Lcom/lge/systemservice/core/watchnetstoragemanager/IWatchNetStorageManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/core/watchnetstoragemanager/IWatchNetStorageManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/systemservice/core/watchnetstoragemanager/IWatchNetStorageManager$Stub$Proxy;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 17
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 18
    const-string v0, "com.lge.systemservice.core.watchnetstoragemanager.IWatchNetStorageManager"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/lge/systemservice/core/watchnetstoragemanager/IWatchNetStorageManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 19
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/lge/systemservice/core/watchnetstoragemanager/IWatchNetStorageManager;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 26
    if-nez p0, :cond_4

    #@2
    .line 27
    const/4 v0, 0x0

    #@3
    .line 33
    :goto_3
    return-object v0

    #@4
    .line 29
    :cond_4
    const-string v1, "com.lge.systemservice.core.watchnetstoragemanager.IWatchNetStorageManager"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 30
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/lge/systemservice/core/watchnetstoragemanager/IWatchNetStorageManager;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 31
    check-cast v0, Lcom/lge/systemservice/core/watchnetstoragemanager/IWatchNetStorageManager;

    #@12
    goto :goto_3

    #@13
    .line 33
    :cond_13
    new-instance v0, Lcom/lge/systemservice/core/watchnetstoragemanager/IWatchNetStorageManager$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/lge/systemservice/core/watchnetstoragemanager/IWatchNetStorageManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 37
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 11
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 41
    sparse-switch p1, :sswitch_data_9c

    #@5
    .line 109
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v4

    #@9
    :goto_9
    return v4

    #@a
    .line 45
    :sswitch_a
    const-string v3, "com.lge.systemservice.core.watchnetstoragemanager.IWatchNetStorageManager"

    #@c
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 50
    :sswitch_10
    const-string v5, "com.lge.systemservice.core.watchnetstoragemanager.IWatchNetStorageManager"

    #@12
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 52
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    .line 54
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1c
    move-result v1

    #@1d
    .line 55
    .local v1, _arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/lge/systemservice/core/watchnetstoragemanager/IWatchNetStorageManager$Stub;->requestMediaScanFile(Ljava/lang/String;I)Z

    #@20
    move-result v2

    #@21
    .line 56
    .local v2, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@24
    .line 57
    if-eqz v2, :cond_27

    #@26
    move v3, v4

    #@27
    :cond_27
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@2a
    goto :goto_9

    #@2b
    .line 62
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:I
    .end local v2           #_result:Z
    :sswitch_2b
    const-string v5, "com.lge.systemservice.core.watchnetstoragemanager.IWatchNetStorageManager"

    #@2d
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@30
    .line 64
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@33
    move-result-object v0

    #@34
    .line 65
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/watchnetstoragemanager/IWatchNetStorageManager$Stub;->addNetStorage(Ljava/lang/String;)Z

    #@37
    move-result v2

    #@38
    .line 66
    .restart local v2       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3b
    .line 67
    if-eqz v2, :cond_3e

    #@3d
    move v3, v4

    #@3e
    :cond_3e
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@41
    goto :goto_9

    #@42
    .line 72
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v2           #_result:Z
    :sswitch_42
    const-string v5, "com.lge.systemservice.core.watchnetstoragemanager.IWatchNetStorageManager"

    #@44
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@47
    .line 74
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@4a
    move-result-object v0

    #@4b
    .line 75
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/watchnetstoragemanager/IWatchNetStorageManager$Stub;->removeNetStorage(Ljava/lang/String;)Z

    #@4e
    move-result v2

    #@4f
    .line 76
    .restart local v2       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@52
    .line 77
    if-eqz v2, :cond_55

    #@54
    move v3, v4

    #@55
    :cond_55
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@58
    goto :goto_9

    #@59
    .line 82
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v2           #_result:Z
    :sswitch_59
    const-string v5, "com.lge.systemservice.core.watchnetstoragemanager.IWatchNetStorageManager"

    #@5b
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5e
    .line 84
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@61
    move-result v0

    #@62
    .line 85
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/watchnetstoragemanager/IWatchNetStorageManager$Stub;->startThread(I)Z

    #@65
    move-result v2

    #@66
    .line 86
    .restart local v2       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@69
    .line 87
    if-eqz v2, :cond_6c

    #@6b
    move v3, v4

    #@6c
    :cond_6c
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@6f
    goto :goto_9

    #@70
    .line 92
    .end local v0           #_arg0:I
    .end local v2           #_result:Z
    :sswitch_70
    const-string v5, "com.lge.systemservice.core.watchnetstoragemanager.IWatchNetStorageManager"

    #@72
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@75
    .line 94
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@78
    move-result v0

    #@79
    .line 95
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/watchnetstoragemanager/IWatchNetStorageManager$Stub;->stopThread(I)Z

    #@7c
    move-result v2

    #@7d
    .line 96
    .restart local v2       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@80
    .line 97
    if-eqz v2, :cond_83

    #@82
    move v3, v4

    #@83
    :cond_83
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@86
    goto :goto_9

    #@87
    .line 102
    .end local v0           #_arg0:I
    .end local v2           #_result:Z
    :sswitch_87
    const-string v5, "com.lge.systemservice.core.watchnetstoragemanager.IWatchNetStorageManager"

    #@89
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@8c
    .line 103
    invoke-virtual {p0}, Lcom/lge/systemservice/core/watchnetstoragemanager/IWatchNetStorageManager$Stub;->checkFirstRunAfterBoot()Z

    #@8f
    move-result v2

    #@90
    .line 104
    .restart local v2       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@93
    .line 105
    if-eqz v2, :cond_96

    #@95
    move v3, v4

    #@96
    :cond_96
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@99
    goto/16 :goto_9

    #@9b
    .line 41
    nop

    #@9c
    :sswitch_data_9c
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_2b
        0x3 -> :sswitch_42
        0x4 -> :sswitch_59
        0x5 -> :sswitch_70
        0x6 -> :sswitch_87
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
