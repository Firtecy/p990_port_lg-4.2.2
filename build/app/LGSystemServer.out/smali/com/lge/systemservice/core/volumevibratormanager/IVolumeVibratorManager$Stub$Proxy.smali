.class Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager$Stub$Proxy;
.super Ljava/lang/Object;
.source "IVolumeVibratorManager.java"

# interfaces
.implements Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 153
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 154
    iput-object p1, p0, Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 155
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 158
    iget-object v0, p0, Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public cancelVibrate(Landroid/os/IBinder;)V
    .registers 7
    .parameter "token"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 234
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 235
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 237
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.systemservice.core.volumevibratormanager.IVolumeVibratorManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 238
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@10
    .line 239
    iget-object v2, p0, Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v3, 0x5

    #@13
    const/4 v4, 0x0

    #@14
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 240
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_21

    #@1a
    .line 243
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1d
    .line 244
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@20
    .line 246
    return-void

    #@21
    .line 243
    :catchall_21
    move-exception v2

    #@22
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 244
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    throw v2
.end method

.method public getCurrentVibratorVolume(I)I
    .registers 8
    .parameter "milliseconds"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 315
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 316
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 319
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.systemservice.core.volumevibratormanager.IVolumeVibratorManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 320
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 321
    iget-object v3, p0, Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v4, 0xa

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 322
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 323
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1e
    .catchall {:try_start_8 .. :try_end_1e} :catchall_26

    #@1e
    move-result v2

    #@1f
    .line 326
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 327
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 329
    return v2

    #@26
    .line 326
    .end local v2           #_result:I
    :catchall_26
    move-exception v3

    #@27
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 327
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    throw v3
.end method

.method public getVibrateVolume()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 264
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 265
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 268
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.systemservice.core.volumevibratormanager.IVolumeVibratorManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 269
    iget-object v3, p0, Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x7

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 270
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 271
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_22

    #@1a
    move-result v2

    #@1b
    .line 274
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 275
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 277
    return v2

    #@22
    .line 274
    .end local v2           #_result:I
    :catchall_22
    move-exception v3

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 275
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v3
.end method

.method public getVibrateVolumeType(I)I
    .registers 8
    .parameter "vibrateType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 297
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 298
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 301
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.systemservice.core.volumevibratormanager.IVolumeVibratorManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 302
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 303
    iget-object v3, p0, Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v4, 0x9

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 304
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 305
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1e
    .catchall {:try_start_8 .. :try_end_1e} :catchall_26

    #@1e
    move-result v2

    #@1f
    .line 308
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 309
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 311
    return v2

    #@26
    .line 308
    .end local v2           #_result:I
    :catchall_26
    move-exception v3

    #@27
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 309
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    throw v3
.end method

.method public onCancel()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 183
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 184
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 186
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.systemservice.core.volumevibratormanager.IVolumeVibratorManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 187
    iget-object v2, p0, Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v3, 0x2

    #@10
    const/4 v4, 0x0

    #@11
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 188
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_17
    .catchall {:try_start_8 .. :try_end_17} :catchall_1e

    #@17
    .line 191
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 192
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1d
    .line 194
    return-void

    #@1e
    .line 191
    :catchall_1e
    move-exception v2

    #@1f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 192
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    throw v2
.end method

.method public onVibratorExcuted(I)V
    .registers 7
    .parameter "milliseconds"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 166
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 167
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 169
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.systemservice.core.volumevibratormanager.IVolumeVibratorManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 170
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 171
    iget-object v2, p0, Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v3, 0x1

    #@13
    const/4 v4, 0x0

    #@14
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 172
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_21

    #@1a
    .line 175
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1d
    .line 176
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@20
    .line 178
    return-void

    #@21
    .line 175
    :catchall_21
    move-exception v2

    #@22
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 176
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    throw v2
.end method

.method public setVibrateVolume(I)V
    .registers 7
    .parameter "volumeIndex"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 249
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 250
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 252
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.systemservice.core.volumevibratormanager.IVolumeVibratorManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 253
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 254
    iget-object v2, p0, Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v3, 0x6

    #@13
    const/4 v4, 0x0

    #@14
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 255
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_21

    #@1a
    .line 258
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1d
    .line 259
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@20
    .line 261
    return-void

    #@21
    .line 258
    :catchall_21
    move-exception v2

    #@22
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 259
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    throw v2
.end method

.method public setVibrateVolumeType(II)V
    .registers 8
    .parameter "vibrateType"
    .parameter "volumeIndex"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 281
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 282
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 284
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.systemservice.core.volumevibratormanager.IVolumeVibratorManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 285
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 286
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 287
    iget-object v2, p0, Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/16 v3, 0x8

    #@17
    const/4 v4, 0x0

    #@18
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 288
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1e
    .catchall {:try_start_8 .. :try_end_1e} :catchall_25

    #@1e
    .line 291
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 292
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 294
    return-void

    #@25
    .line 291
    :catchall_25
    move-exception v2

    #@26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 292
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    throw v2
.end method

.method public vibrate(JILandroid/os/IBinder;)V
    .registers 10
    .parameter "milliseconds"
    .parameter "volumeIndex"
    .parameter "token"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 199
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 200
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 202
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.systemservice.core.volumevibratormanager.IVolumeVibratorManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 203
    invoke-virtual {v0, p1, p2}, Landroid/os/Parcel;->writeLong(J)V

    #@10
    .line 204
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 205
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 206
    iget-object v2, p0, Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/4 v3, 0x3

    #@19
    const/4 v4, 0x0

    #@1a
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1d
    .line 207
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_20
    .catchall {:try_start_8 .. :try_end_20} :catchall_27

    #@20
    .line 210
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 211
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 213
    return-void

    #@27
    .line 210
    :catchall_27
    move-exception v2

    #@28
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 211
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    throw v2
.end method

.method public vibratePattern([JI[ILandroid/os/IBinder;)V
    .registers 10
    .parameter "pattern"
    .parameter "repeat"
    .parameter "volumeIndex"
    .parameter "token"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 216
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 217
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 219
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.systemservice.core.volumevibratormanager.IVolumeVibratorManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 220
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeLongArray([J)V

    #@10
    .line 221
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 222
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeIntArray([I)V

    #@16
    .line 223
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@19
    .line 224
    iget-object v2, p0, Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1b
    const/4 v3, 0x4

    #@1c
    const/4 v4, 0x0

    #@1d
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@20
    .line 225
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_23
    .catchall {:try_start_8 .. :try_end_23} :catchall_2a

    #@23
    .line 228
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 229
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 231
    return-void

    #@2a
    .line 228
    :catchall_2a
    move-exception v2

    #@2b
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 229
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    throw v2
.end method
