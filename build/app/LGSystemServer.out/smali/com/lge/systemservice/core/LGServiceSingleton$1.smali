.class Lcom/lge/systemservice/core/LGServiceSingleton$1;
.super Ljava/lang/Object;
.source "LGServiceSingleton.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/core/LGServiceSingleton;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/core/LGServiceSingleton;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/core/LGServiceSingleton;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 58
    .local p0, this:Lcom/lge/systemservice/core/LGServiceSingleton$1;,"Lcom/lge/systemservice/core/LGServiceSingleton.1;"
    iput-object p1, p0, Lcom/lge/systemservice/core/LGServiceSingleton$1;->this$0:Lcom/lge/systemservice/core/LGServiceSingleton;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public binderDied()V
    .registers 4

    #@0
    .prologue
    .line 62
    .local p0, this:Lcom/lge/systemservice/core/LGServiceSingleton$1;,"Lcom/lge/systemservice/core/LGServiceSingleton.1;"
    const-string v0, "LGServiceSingleton"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    iget-object v2, p0, Lcom/lge/systemservice/core/LGServiceSingleton$1;->this$0:Lcom/lge/systemservice/core/LGServiceSingleton;

    #@9
    invoke-static {v2}, Lcom/lge/systemservice/core/LGServiceSingleton;->access$000(Lcom/lge/systemservice/core/LGServiceSingleton;)Ljava/lang/String;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, " service binder is death and clean-up"

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 63
    iget-object v0, p0, Lcom/lge/systemservice/core/LGServiceSingleton$1;->this$0:Lcom/lge/systemservice/core/LGServiceSingleton;

    #@20
    const/4 v1, 0x0

    #@21
    invoke-static {v0, v1}, Lcom/lge/systemservice/core/LGServiceSingleton;->access$102(Lcom/lge/systemservice/core/LGServiceSingleton;Ljava/lang/Object;)Ljava/lang/Object;

    #@24
    .line 64
    iget-object v0, p0, Lcom/lge/systemservice/core/LGServiceSingleton$1;->this$0:Lcom/lge/systemservice/core/LGServiceSingleton;

    #@26
    invoke-virtual {v0}, Lcom/lge/systemservice/core/LGServiceSingleton;->reset()V

    #@29
    .line 65
    return-void
.end method
