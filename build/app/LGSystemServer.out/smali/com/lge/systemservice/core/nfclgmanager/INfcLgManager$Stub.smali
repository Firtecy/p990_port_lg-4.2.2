.class public abstract Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager$Stub;
.super Landroid/os/Binder;
.source "INfcLgManager.java"

# interfaces
.implements Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager$Stub$Proxy;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 15
    const-string v0, "com.lge.systemservice.core.nfclgmanager.INfcLgManager"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 23
    if-nez p0, :cond_4

    #@2
    .line 24
    const/4 v0, 0x0

    #@3
    .line 30
    :goto_3
    return-object v0

    #@4
    .line 26
    :cond_4
    const-string v1, "com.lge.systemservice.core.nfclgmanager.INfcLgManager"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 27
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 28
    check-cast v0, Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager;

    #@12
    goto :goto_3

    #@13
    .line 30
    :cond_13
    new-instance v0, Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 11
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 38
    sparse-switch p1, :sswitch_data_5c

    #@5
    .line 80
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v4

    #@9
    :goto_9
    return v4

    #@a
    .line 42
    :sswitch_a
    const-string v3, "com.lge.systemservice.core.nfclgmanager.INfcLgManager"

    #@c
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 47
    :sswitch_10
    const-string v3, "com.lge.systemservice.core.nfclgmanager.INfcLgManager"

    #@12
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@18
    move-result v0

    #@19
    .line 51
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@1c
    move-result-object v1

    #@1d
    .line 52
    .local v1, _arg1:[B
    invoke-virtual {p0, v0, v1}, Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager$Stub;->handleNfcFactory(I[B)Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    .line 53
    .local v2, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@24
    .line 54
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@27
    .line 55
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeByteArray([B)V

    #@2a
    goto :goto_9

    #@2b
    .line 60
    .end local v0           #_arg0:I
    .end local v1           #_arg1:[B
    .end local v2           #_result:Ljava/lang/String;
    :sswitch_2b
    const-string v5, "com.lge.systemservice.core.nfclgmanager.INfcLgManager"

    #@2d
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@30
    .line 61
    invoke-virtual {p0}, Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager$Stub;->createNfcFactoryObj()Z

    #@33
    move-result v2

    #@34
    .line 62
    .local v2, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@37
    .line 63
    if-eqz v2, :cond_3a

    #@39
    move v3, v4

    #@3a
    :cond_3a
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@3d
    goto :goto_9

    #@3e
    .line 68
    .end local v2           #_result:Z
    :sswitch_3e
    const-string v5, "com.lge.systemservice.core.nfclgmanager.INfcLgManager"

    #@40
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@43
    .line 70
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@46
    move-result v0

    #@47
    .line 72
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@4a
    move-result-object v1

    #@4b
    .line 73
    .restart local v1       #_arg1:[B
    invoke-virtual {p0, v0, v1}, Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager$Stub;->sendNfcTestCommand(I[B)Z

    #@4e
    move-result v2

    #@4f
    .line 74
    .restart local v2       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@52
    .line 75
    if-eqz v2, :cond_55

    #@54
    move v3, v4

    #@55
    :cond_55
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@58
    .line 76
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeByteArray([B)V

    #@5b
    goto :goto_9

    #@5c
    .line 38
    :sswitch_data_5c
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_2b
        0x3 -> :sswitch_3e
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
