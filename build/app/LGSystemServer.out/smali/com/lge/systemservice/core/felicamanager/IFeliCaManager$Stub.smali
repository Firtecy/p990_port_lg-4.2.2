.class public abstract Lcom/lge/systemservice/core/felicamanager/IFeliCaManager$Stub;
.super Landroid/os/Binder;
.source "IFeliCaManager.java"

# interfaces
.implements Lcom/lge/systemservice/core/felicamanager/IFeliCaManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/core/felicamanager/IFeliCaManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/systemservice/core/felicamanager/IFeliCaManager$Stub$Proxy;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 15
    const-string v0, "com.lge.systemservice.core.felicamanager.IFeliCaManager"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/lge/systemservice/core/felicamanager/IFeliCaManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/lge/systemservice/core/felicamanager/IFeliCaManager;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 23
    if-nez p0, :cond_4

    #@2
    .line 24
    const/4 v0, 0x0

    #@3
    .line 30
    :goto_3
    return-object v0

    #@4
    .line 26
    :cond_4
    const-string v1, "com.lge.systemservice.core.felicamanager.IFeliCaManager"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 27
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/lge/systemservice/core/felicamanager/IFeliCaManager;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 28
    check-cast v0, Lcom/lge/systemservice/core/felicamanager/IFeliCaManager;

    #@12
    goto :goto_3

    #@13
    .line 30
    :cond_13
    new-instance v0, Lcom/lge/systemservice/core/felicamanager/IFeliCaManager$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/lge/systemservice/core/felicamanager/IFeliCaManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 11
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 38
    sparse-switch p1, :sswitch_data_13c

    #@5
    .line 192
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v4

    #@9
    :goto_9
    return v4

    #@a
    .line 42
    :sswitch_a
    const-string v3, "com.lge.systemservice.core.felicamanager.IFeliCaManager"

    #@c
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 47
    :sswitch_10
    const-string v5, "com.lge.systemservice.core.felicamanager.IFeliCaManager"

    #@12
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@18
    move-result v1

    #@19
    .line 50
    .local v1, _arg0_length:I
    if-gez v1, :cond_2d

    #@1b
    .line 51
    const/4 v0, 0x0

    #@1c
    .line 56
    .local v0, _arg0:[B
    :goto_1c
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/felicamanager/IFeliCaManager$Stub;->cmdEXTIDM([B)Z

    #@1f
    move-result v2

    #@20
    .line 57
    .local v2, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@23
    .line 58
    if-eqz v2, :cond_26

    #@25
    move v3, v4

    #@26
    :cond_26
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@29
    .line 59
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    #@2c
    goto :goto_9

    #@2d
    .line 54
    .end local v0           #_arg0:[B
    .end local v2           #_result:Z
    :cond_2d
    new-array v0, v1, [B

    #@2f
    .restart local v0       #_arg0:[B
    goto :goto_1c

    #@30
    .line 64
    .end local v0           #_arg0:[B
    .end local v1           #_arg0_length:I
    :sswitch_30
    const-string v5, "com.lge.systemservice.core.felicamanager.IFeliCaManager"

    #@32
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@35
    .line 66
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@38
    move-result v1

    #@39
    .line 67
    .restart local v1       #_arg0_length:I
    if-gez v1, :cond_4d

    #@3b
    .line 68
    const/4 v0, 0x0

    #@3c
    .line 73
    .local v0, _arg0:[Ljava/lang/String;
    :goto_3c
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/felicamanager/IFeliCaManager$Stub;->cmdIDM([Ljava/lang/String;)Z

    #@3f
    move-result v2

    #@40
    .line 74
    .restart local v2       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@43
    .line 75
    if-eqz v2, :cond_46

    #@45
    move v3, v4

    #@46
    :cond_46
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@49
    .line 76
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@4c
    goto :goto_9

    #@4d
    .line 71
    .end local v0           #_arg0:[Ljava/lang/String;
    .end local v2           #_result:Z
    :cond_4d
    new-array v0, v1, [Ljava/lang/String;

    #@4f
    .restart local v0       #_arg0:[Ljava/lang/String;
    goto :goto_3c

    #@50
    .line 81
    .end local v0           #_arg0:[Ljava/lang/String;
    .end local v1           #_arg0_length:I
    :sswitch_50
    const-string v3, "com.lge.systemservice.core.felicamanager.IFeliCaManager"

    #@52
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@55
    .line 82
    invoke-virtual {p0}, Lcom/lge/systemservice/core/felicamanager/IFeliCaManager$Stub;->cmdRFIDCK()I

    #@58
    move-result v2

    #@59
    .line 83
    .local v2, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5c
    .line 84
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@5f
    goto :goto_9

    #@60
    .line 89
    .end local v2           #_result:I
    :sswitch_60
    const-string v5, "com.lge.systemservice.core.felicamanager.IFeliCaManager"

    #@62
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@65
    .line 91
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@68
    move-result v1

    #@69
    .line 92
    .restart local v1       #_arg0_length:I
    if-gez v1, :cond_7d

    #@6b
    .line 93
    const/4 v0, 0x0

    #@6c
    .line 98
    .restart local v0       #_arg0:[Ljava/lang/String;
    :goto_6c
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/felicamanager/IFeliCaManager$Stub;->cmdSwitchRange([Ljava/lang/String;)Z

    #@6f
    move-result v2

    #@70
    .line 99
    .local v2, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@73
    .line 100
    if-eqz v2, :cond_76

    #@75
    move v3, v4

    #@76
    :cond_76
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@79
    .line 101
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@7c
    goto :goto_9

    #@7d
    .line 96
    .end local v0           #_arg0:[Ljava/lang/String;
    .end local v2           #_result:Z
    :cond_7d
    new-array v0, v1, [Ljava/lang/String;

    #@7f
    .restart local v0       #_arg0:[Ljava/lang/String;
    goto :goto_6c

    #@80
    .line 106
    .end local v0           #_arg0:[Ljava/lang/String;
    .end local v1           #_arg0_length:I
    :sswitch_80
    const-string v5, "com.lge.systemservice.core.felicamanager.IFeliCaManager"

    #@82
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@85
    .line 108
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@88
    move-result v0

    #@89
    .line 109
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/felicamanager/IFeliCaManager$Stub;->cmdSwitchWrite(I)Z

    #@8c
    move-result v2

    #@8d
    .line 110
    .restart local v2       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@90
    .line 111
    if-eqz v2, :cond_93

    #@92
    move v3, v4

    #@93
    :cond_93
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@96
    goto/16 :goto_9

    #@98
    .line 116
    .end local v0           #_arg0:I
    .end local v2           #_result:Z
    :sswitch_98
    const-string v5, "com.lge.systemservice.core.felicamanager.IFeliCaManager"

    #@9a
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9d
    .line 118
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@a0
    move-result v1

    #@a1
    .line 119
    .restart local v1       #_arg0_length:I
    if-gez v1, :cond_b6

    #@a3
    .line 120
    const/4 v0, 0x0

    #@a4
    .line 125
    .local v0, _arg0:[Ljava/lang/String;
    :goto_a4
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/felicamanager/IFeliCaManager$Stub;->cmdSwitchRead([Ljava/lang/String;)Z

    #@a7
    move-result v2

    #@a8
    .line 126
    .restart local v2       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@ab
    .line 127
    if-eqz v2, :cond_ae

    #@ad
    move v3, v4

    #@ae
    :cond_ae
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@b1
    .line 128
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@b4
    goto/16 :goto_9

    #@b6
    .line 123
    .end local v0           #_arg0:[Ljava/lang/String;
    .end local v2           #_result:Z
    :cond_b6
    new-array v0, v1, [Ljava/lang/String;

    #@b8
    .restart local v0       #_arg0:[Ljava/lang/String;
    goto :goto_a4

    #@b9
    .line 133
    .end local v0           #_arg0:[Ljava/lang/String;
    .end local v1           #_arg0_length:I
    :sswitch_b9
    const-string v5, "com.lge.systemservice.core.felicamanager.IFeliCaManager"

    #@bb
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@be
    .line 135
    invoke-virtual {p2}, Landroid/os/Parcel;->readFloat()F

    #@c1
    move-result v0

    #@c2
    .line 136
    .local v0, _arg0:F
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/felicamanager/IFeliCaManager$Stub;->cmdFreqCalWrite(F)Z

    #@c5
    move-result v2

    #@c6
    .line 137
    .restart local v2       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@c9
    .line 138
    if-eqz v2, :cond_cc

    #@cb
    move v3, v4

    #@cc
    :cond_cc
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@cf
    goto/16 :goto_9

    #@d1
    .line 143
    .end local v0           #_arg0:F
    .end local v2           #_result:Z
    :sswitch_d1
    const-string v5, "com.lge.systemservice.core.felicamanager.IFeliCaManager"

    #@d3
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d6
    .line 145
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@d9
    move-result v1

    #@da
    .line 146
    .restart local v1       #_arg0_length:I
    if-gez v1, :cond_ef

    #@dc
    .line 147
    const/4 v0, 0x0

    #@dd
    .line 152
    .local v0, _arg0:[Ljava/lang/String;
    :goto_dd
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/felicamanager/IFeliCaManager$Stub;->cmdFreqCalRead([Ljava/lang/String;)Z

    #@e0
    move-result v2

    #@e1
    .line 153
    .restart local v2       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@e4
    .line 154
    if-eqz v2, :cond_e7

    #@e6
    move v3, v4

    #@e7
    :cond_e7
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@ea
    .line 155
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@ed
    goto/16 :goto_9

    #@ef
    .line 150
    .end local v0           #_arg0:[Ljava/lang/String;
    .end local v2           #_result:Z
    :cond_ef
    new-array v0, v1, [Ljava/lang/String;

    #@f1
    .restart local v0       #_arg0:[Ljava/lang/String;
    goto :goto_dd

    #@f2
    .line 160
    .end local v0           #_arg0:[Ljava/lang/String;
    .end local v1           #_arg0_length:I
    :sswitch_f2
    const-string v5, "com.lge.systemservice.core.felicamanager.IFeliCaManager"

    #@f4
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f7
    .line 162
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@fa
    move-result v1

    #@fb
    .line 163
    .restart local v1       #_arg0_length:I
    if-gez v1, :cond_110

    #@fd
    .line 164
    const/4 v0, 0x0

    #@fe
    .line 169
    .restart local v0       #_arg0:[Ljava/lang/String;
    :goto_fe
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/felicamanager/IFeliCaManager$Stub;->cmdFreqCalRange([Ljava/lang/String;)Z

    #@101
    move-result v2

    #@102
    .line 170
    .restart local v2       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@105
    .line 171
    if-eqz v2, :cond_108

    #@107
    move v3, v4

    #@108
    :cond_108
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@10b
    .line 172
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@10e
    goto/16 :goto_9

    #@110
    .line 167
    .end local v0           #_arg0:[Ljava/lang/String;
    .end local v2           #_result:Z
    :cond_110
    new-array v0, v1, [Ljava/lang/String;

    #@112
    .restart local v0       #_arg0:[Ljava/lang/String;
    goto :goto_fe

    #@113
    .line 177
    .end local v0           #_arg0:[Ljava/lang/String;
    .end local v1           #_arg0_length:I
    :sswitch_113
    const-string v5, "com.lge.systemservice.core.felicamanager.IFeliCaManager"

    #@115
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@118
    .line 178
    invoke-virtual {p0}, Lcom/lge/systemservice/core/felicamanager/IFeliCaManager$Stub;->cmdRFRegCalLoad()Z

    #@11b
    move-result v2

    #@11c
    .line 179
    .restart local v2       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@11f
    .line 180
    if-eqz v2, :cond_122

    #@121
    move v3, v4

    #@122
    :cond_122
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@125
    goto/16 :goto_9

    #@127
    .line 185
    .end local v2           #_result:Z
    :sswitch_127
    const-string v5, "com.lge.systemservice.core.felicamanager.IFeliCaManager"

    #@129
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@12c
    .line 186
    invoke-virtual {p0}, Lcom/lge/systemservice/core/felicamanager/IFeliCaManager$Stub;->cmdRFRegCalCheck()Z

    #@12f
    move-result v2

    #@130
    .line 187
    .restart local v2       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@133
    .line 188
    if-eqz v2, :cond_136

    #@135
    move v3, v4

    #@136
    :cond_136
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@139
    goto/16 :goto_9

    #@13b
    .line 38
    nop

    #@13c
    :sswitch_data_13c
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_30
        0x3 -> :sswitch_50
        0x4 -> :sswitch_60
        0x5 -> :sswitch_80
        0x6 -> :sswitch_98
        0x7 -> :sswitch_b9
        0x8 -> :sswitch_d1
        0x9 -> :sswitch_f2
        0xa -> :sswitch_113
        0xb -> :sswitch_127
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
