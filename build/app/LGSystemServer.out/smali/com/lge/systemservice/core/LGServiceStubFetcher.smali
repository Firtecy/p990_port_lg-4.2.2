.class public abstract Lcom/lge/systemservice/core/LGServiceStubFetcher;
.super Ljava/lang/Object;
.source "LGServiceStubFetcher.java"


# instance fields
.field private mFeatureName:Ljava/lang/String;

.field private mIsSupportFeature:Z

.field private mServiceName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 3
    .parameter "serviceName"

    #@0
    .prologue
    .line 18
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 19
    iput-object p1, p0, Lcom/lge/systemservice/core/LGServiceStubFetcher;->mServiceName:Ljava/lang/String;

    #@5
    .line 20
    const/4 v0, 0x0

    #@6
    iput-boolean v0, p0, Lcom/lge/systemservice/core/LGServiceStubFetcher;->mIsSupportFeature:Z

    #@8
    .line 21
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "serviceName"
    .parameter "featureName"

    #@0
    .prologue
    .line 12
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 13
    iput-object p1, p0, Lcom/lge/systemservice/core/LGServiceStubFetcher;->mServiceName:Ljava/lang/String;

    #@5
    .line 14
    iput-object p2, p0, Lcom/lge/systemservice/core/LGServiceStubFetcher;->mFeatureName:Ljava/lang/String;

    #@7
    .line 15
    const/4 v0, 0x1

    #@8
    iput-boolean v0, p0, Lcom/lge/systemservice/core/LGServiceStubFetcher;->mIsSupportFeature:Z

    #@a
    .line 16
    return-void
.end method


# virtual methods
.method public abstract createServiceStub(Landroid/content/Context;)Landroid/os/IBinder;
.end method

.method public newInstance(Landroid/content/Context;)V
    .registers 7
    .parameter "ctx"

    #@0
    .prologue
    .line 24
    iget-boolean v2, p0, Lcom/lge/systemservice/core/LGServiceStubFetcher;->mIsSupportFeature:Z

    #@2
    if-eqz v2, :cond_39

    #@4
    iget-object v2, p0, Lcom/lge/systemservice/core/LGServiceStubFetcher;->mFeatureName:Ljava/lang/String;

    #@6
    invoke-static {p1, v2}, Lcom/lge/systemservice/core/LGServiceManager;->hasLGSystemServiceFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@9
    move-result v2

    #@a
    if-nez v2, :cond_39

    #@c
    .line 26
    const-string v2, "LGFeaturingServiceStubRegister"

    #@e
    new-instance v3, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v4, "["

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    iget-object v4, p0, Lcom/lge/systemservice/core/LGServiceStubFetcher;->mFeatureName:Ljava/lang/String;

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    const-string v4, "] feture don\'t support, and "

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    iget-object v4, p0, Lcom/lge/systemservice/core/LGServiceStubFetcher;->mServiceName:Ljava/lang/String;

    #@27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    const-string v4, " service don\'t work"

    #@2d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v3

    #@35
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 51
    :goto_38
    return-void

    #@39
    .line 31
    :cond_39
    const/4 v0, 0x0

    #@3a
    .line 32
    .local v0, b:Landroid/os/IBinder;
    iget-object v2, p0, Lcom/lge/systemservice/core/LGServiceStubFetcher;->mServiceName:Ljava/lang/String;

    #@3c
    if-eqz v2, :cond_89

    #@3e
    .line 33
    invoke-virtual {p0, p1}, Lcom/lge/systemservice/core/LGServiceStubFetcher;->createServiceStub(Landroid/content/Context;)Landroid/os/IBinder;

    #@41
    move-result-object v0

    #@42
    .line 34
    if-eqz v0, :cond_6a

    #@44
    .line 35
    iget-object v2, p0, Lcom/lge/systemservice/core/LGServiceStubFetcher;->mServiceName:Ljava/lang/String;

    #@46
    invoke-static {v0, v2}, Lcom/lge/systemservice/core/LGServiceManager;->addServiceAsDefault(Landroid/os/IBinder;Ljava/lang/String;)V

    #@49
    .line 36
    const-string v2, "LGServiceStubRegister"

    #@4b
    new-instance v3, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    const-string v4, "LG "

    #@52
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v3

    #@56
    iget-object v4, p0, Lcom/lge/systemservice/core/LGServiceStubFetcher;->mServiceName:Ljava/lang/String;

    #@58
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v3

    #@5c
    const-string v4, " system service starts"

    #@5e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v3

    #@62
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65
    move-result-object v3

    #@66
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@69
    goto :goto_38

    #@6a
    .line 39
    :cond_6a
    const-string v2, "LGServiceStubRegister"

    #@6c
    new-instance v3, Ljava/lang/StringBuilder;

    #@6e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@71
    iget-object v4, p0, Lcom/lge/systemservice/core/LGServiceStubFetcher;->mServiceName:Ljava/lang/String;

    #@73
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v3

    #@77
    const-string v4, " service don\'t work by binder "

    #@79
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v3

    #@7d
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v3

    #@81
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@84
    move-result-object v3

    #@85
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@88
    goto :goto_38

    #@89
    .line 44
    :cond_89
    :try_start_89
    new-instance v2, Ljava/lang/Exception;

    #@8b
    new-instance v3, Ljava/lang/StringBuilder;

    #@8d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@90
    const-string v4, "ServiceName value: "

    #@92
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v3

    #@96
    iget-object v4, p0, Lcom/lge/systemservice/core/LGServiceStubFetcher;->mServiceName:Ljava/lang/String;

    #@98
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v3

    #@9c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9f
    move-result-object v3

    #@a0
    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    #@a3
    throw v2
    :try_end_a4
    .catch Ljava/lang/Exception; {:try_start_89 .. :try_end_a4} :catch_a4

    #@a4
    .line 45
    :catch_a4
    move-exception v1

    #@a5
    .line 47
    .local v1, e:Ljava/lang/Exception;
    const-string v2, "LGServiceStubRegister"

    #@a7
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@aa
    move-result-object v3

    #@ab
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ae
    .line 48
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    #@b1
    goto :goto_38
.end method
