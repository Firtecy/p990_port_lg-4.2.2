.class public abstract Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager$Stub;
.super Landroid/os/Binder;
.source "IVolumeVibratorManager.java"

# interfaces
.implements Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager$Stub$Proxy;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 15
    const-string v0, "com.lge.systemservice.core.volumevibratormanager.IVolumeVibratorManager"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 23
    if-nez p0, :cond_4

    #@2
    .line 24
    const/4 v0, 0x0

    #@3
    .line 30
    :goto_3
    return-object v0

    #@4
    .line 26
    :cond_4
    const-string v1, "com.lge.systemservice.core.volumevibratormanager.IVolumeVibratorManager"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 27
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 28
    check-cast v0, Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager;

    #@12
    goto :goto_3

    #@13
    .line 30
    :cond_13
    new-instance v0, Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 13
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 38
    sparse-switch p1, :sswitch_data_d0

    #@4
    .line 147
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v6

    #@8
    :goto_8
    return v6

    #@9
    .line 42
    :sswitch_9
    const-string v7, "com.lge.systemservice.core.volumevibratormanager.IVolumeVibratorManager"

    #@b
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 47
    :sswitch_f
    const-string v7, "com.lge.systemservice.core.volumevibratormanager.IVolumeVibratorManager"

    #@11
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v0

    #@18
    .line 50
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager$Stub;->onVibratorExcuted(I)V

    #@1b
    .line 51
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1e
    goto :goto_8

    #@1f
    .line 56
    .end local v0           #_arg0:I
    :sswitch_1f
    const-string v7, "com.lge.systemservice.core.volumevibratormanager.IVolumeVibratorManager"

    #@21
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@24
    .line 57
    invoke-virtual {p0}, Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager$Stub;->onCancel()V

    #@27
    .line 58
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2a
    goto :goto_8

    #@2b
    .line 63
    :sswitch_2b
    const-string v7, "com.lge.systemservice.core.volumevibratormanager.IVolumeVibratorManager"

    #@2d
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@30
    .line 65
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    #@33
    move-result-wide v0

    #@34
    .line 67
    .local v0, _arg0:J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@37
    move-result v2

    #@38
    .line 69
    .local v2, _arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@3b
    move-result-object v3

    #@3c
    .line 70
    .local v3, _arg2:Landroid/os/IBinder;
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager$Stub;->vibrate(JILandroid/os/IBinder;)V

    #@3f
    .line 71
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@42
    goto :goto_8

    #@43
    .line 76
    .end local v0           #_arg0:J
    .end local v2           #_arg1:I
    .end local v3           #_arg2:Landroid/os/IBinder;
    :sswitch_43
    const-string v7, "com.lge.systemservice.core.volumevibratormanager.IVolumeVibratorManager"

    #@45
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@48
    .line 78
    invoke-virtual {p2}, Landroid/os/Parcel;->createLongArray()[J

    #@4b
    move-result-object v0

    #@4c
    .line 80
    .local v0, _arg0:[J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@4f
    move-result v2

    #@50
    .line 82
    .restart local v2       #_arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->createIntArray()[I

    #@53
    move-result-object v3

    #@54
    .line 84
    .local v3, _arg2:[I
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@57
    move-result-object v4

    #@58
    .line 85
    .local v4, _arg3:Landroid/os/IBinder;
    invoke-virtual {p0, v0, v2, v3, v4}, Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager$Stub;->vibratePattern([JI[ILandroid/os/IBinder;)V

    #@5b
    .line 86
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5e
    goto :goto_8

    #@5f
    .line 91
    .end local v0           #_arg0:[J
    .end local v2           #_arg1:I
    .end local v3           #_arg2:[I
    .end local v4           #_arg3:Landroid/os/IBinder;
    :sswitch_5f
    const-string v7, "com.lge.systemservice.core.volumevibratormanager.IVolumeVibratorManager"

    #@61
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@64
    .line 93
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@67
    move-result-object v0

    #@68
    .line 94
    .local v0, _arg0:Landroid/os/IBinder;
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager$Stub;->cancelVibrate(Landroid/os/IBinder;)V

    #@6b
    .line 95
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@6e
    goto :goto_8

    #@6f
    .line 100
    .end local v0           #_arg0:Landroid/os/IBinder;
    :sswitch_6f
    const-string v7, "com.lge.systemservice.core.volumevibratormanager.IVolumeVibratorManager"

    #@71
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@74
    .line 102
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@77
    move-result v0

    #@78
    .line 103
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager$Stub;->setVibrateVolume(I)V

    #@7b
    .line 104
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@7e
    goto :goto_8

    #@7f
    .line 109
    .end local v0           #_arg0:I
    :sswitch_7f
    const-string v7, "com.lge.systemservice.core.volumevibratormanager.IVolumeVibratorManager"

    #@81
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@84
    .line 110
    invoke-virtual {p0}, Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager$Stub;->getVibrateVolume()I

    #@87
    move-result v5

    #@88
    .line 111
    .local v5, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@8b
    .line 112
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@8e
    goto/16 :goto_8

    #@90
    .line 117
    .end local v5           #_result:I
    :sswitch_90
    const-string v7, "com.lge.systemservice.core.volumevibratormanager.IVolumeVibratorManager"

    #@92
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@95
    .line 119
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@98
    move-result v0

    #@99
    .line 121
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@9c
    move-result v2

    #@9d
    .line 122
    .restart local v2       #_arg1:I
    invoke-virtual {p0, v0, v2}, Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager$Stub;->setVibrateVolumeType(II)V

    #@a0
    .line 123
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@a3
    goto/16 :goto_8

    #@a5
    .line 128
    .end local v0           #_arg0:I
    .end local v2           #_arg1:I
    :sswitch_a5
    const-string v7, "com.lge.systemservice.core.volumevibratormanager.IVolumeVibratorManager"

    #@a7
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@aa
    .line 130
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@ad
    move-result v0

    #@ae
    .line 131
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager$Stub;->getVibrateVolumeType(I)I

    #@b1
    move-result v5

    #@b2
    .line 132
    .restart local v5       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@b5
    .line 133
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@b8
    goto/16 :goto_8

    #@ba
    .line 138
    .end local v0           #_arg0:I
    .end local v5           #_result:I
    :sswitch_ba
    const-string v7, "com.lge.systemservice.core.volumevibratormanager.IVolumeVibratorManager"

    #@bc
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@bf
    .line 140
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@c2
    move-result v0

    #@c3
    .line 141
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager$Stub;->getCurrentVibratorVolume(I)I

    #@c6
    move-result v5

    #@c7
    .line 142
    .restart local v5       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@ca
    .line 143
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@cd
    goto/16 :goto_8

    #@cf
    .line 38
    nop

    #@d0
    :sswitch_data_d0
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_1f
        0x3 -> :sswitch_2b
        0x4 -> :sswitch_43
        0x5 -> :sswitch_5f
        0x6 -> :sswitch_6f
        0x7 -> :sswitch_7f
        0x8 -> :sswitch_90
        0x9 -> :sswitch_a5
        0xa -> :sswitch_ba
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
