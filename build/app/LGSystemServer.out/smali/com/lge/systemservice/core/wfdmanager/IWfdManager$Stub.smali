.class public abstract Lcom/lge/systemservice/core/wfdmanager/IWfdManager$Stub;
.super Landroid/os/Binder;
.source "IWfdManager.java"

# interfaces
.implements Lcom/lge/systemservice/core/wfdmanager/IWfdManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/core/wfdmanager/IWfdManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/systemservice/core/wfdmanager/IWfdManager$Stub$Proxy;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 20
    const-string v0, "com.lge.systemservice.core.wfdmanager.IWfdManager"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/lge/systemservice/core/wfdmanager/IWfdManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 21
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/lge/systemservice/core/wfdmanager/IWfdManager;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 28
    if-nez p0, :cond_4

    #@2
    .line 29
    const/4 v0, 0x0

    #@3
    .line 35
    :goto_3
    return-object v0

    #@4
    .line 31
    :cond_4
    const-string v1, "com.lge.systemservice.core.wfdmanager.IWfdManager"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 32
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/lge/systemservice/core/wfdmanager/IWfdManager;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 33
    check-cast v0, Lcom/lge/systemservice/core/wfdmanager/IWfdManager;

    #@12
    goto :goto_3

    #@13
    .line 35
    :cond_13
    new-instance v0, Lcom/lge/systemservice/core/wfdmanager/IWfdManager$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/lge/systemservice/core/wfdmanager/IWfdManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 10
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 43
    sparse-switch p1, :sswitch_data_154

    #@5
    .line 196
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v2

    #@9
    :goto_9
    return v2

    #@a
    .line 47
    :sswitch_a
    const-string v3, "com.lge.systemservice.core.wfdmanager.IWfdManager"

    #@c
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 52
    :sswitch_10
    const-string v4, "com.lge.systemservice.core.wfdmanager.IWfdManager"

    #@12
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 54
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@18
    move-result v4

    #@19
    if-eqz v4, :cond_2a

    #@1b
    move v0, v2

    #@1c
    .line 55
    .local v0, _arg0:Z
    :goto_1c
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/wfdmanager/IWfdManager$Stub;->setWifiDisplayEnabled(Z)Z

    #@1f
    move-result v1

    #@20
    .line 56
    .local v1, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@23
    .line 57
    if-eqz v1, :cond_26

    #@25
    move v3, v2

    #@26
    :cond_26
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@29
    goto :goto_9

    #@2a
    .end local v0           #_arg0:Z
    .end local v1           #_result:Z
    :cond_2a
    move v0, v3

    #@2b
    .line 54
    goto :goto_1c

    #@2c
    .line 62
    :sswitch_2c
    const-string v4, "com.lge.systemservice.core.wfdmanager.IWfdManager"

    #@2e
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@31
    .line 64
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@34
    move-result v4

    #@35
    if-eqz v4, :cond_46

    #@37
    move v0, v2

    #@38
    .line 65
    .restart local v0       #_arg0:Z
    :goto_38
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/wfdmanager/IWfdManager$Stub;->setWifiDisplayEnabledWithPopUp(Z)Z

    #@3b
    move-result v1

    #@3c
    .line 66
    .restart local v1       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3f
    .line 67
    if-eqz v1, :cond_42

    #@41
    move v3, v2

    #@42
    :cond_42
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@45
    goto :goto_9

    #@46
    .end local v0           #_arg0:Z
    .end local v1           #_result:Z
    :cond_46
    move v0, v3

    #@47
    .line 64
    goto :goto_38

    #@48
    .line 72
    :sswitch_48
    const-string v4, "com.lge.systemservice.core.wfdmanager.IWfdManager"

    #@4a
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4d
    .line 73
    invoke-virtual {p0}, Lcom/lge/systemservice/core/wfdmanager/IWfdManager$Stub;->cancelWifiDisplayConnect()Z

    #@50
    move-result v1

    #@51
    .line 74
    .restart local v1       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@54
    .line 75
    if-eqz v1, :cond_57

    #@56
    move v3, v2

    #@57
    :cond_57
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@5a
    goto :goto_9

    #@5b
    .line 80
    .end local v1           #_result:Z
    :sswitch_5b
    const-string v3, "com.lge.systemservice.core.wfdmanager.IWfdManager"

    #@5d
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@60
    .line 82
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@63
    move-result-object v0

    #@64
    .line 83
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/wfdmanager/IWfdManager$Stub;->informConnectionRequstedUdn(Ljava/lang/String;)V

    #@67
    .line 84
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@6a
    goto :goto_9

    #@6b
    .line 89
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_6b
    const-string v3, "com.lge.systemservice.core.wfdmanager.IWfdManager"

    #@6d
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@70
    .line 90
    invoke-virtual {p0}, Lcom/lge/systemservice/core/wfdmanager/IWfdManager$Stub;->getRtspAddress()Ljava/lang/String;

    #@73
    move-result-object v1

    #@74
    .line 91
    .local v1, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@77
    .line 92
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@7a
    goto :goto_9

    #@7b
    .line 97
    .end local v1           #_result:Ljava/lang/String;
    :sswitch_7b
    const-string v3, "com.lge.systemservice.core.wfdmanager.IWfdManager"

    #@7d
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@80
    .line 99
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@83
    move-result v0

    #@84
    .line 100
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/wfdmanager/IWfdManager$Stub;->setWfdMode(I)V

    #@87
    .line 101
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@8a
    goto/16 :goto_9

    #@8c
    .line 106
    .end local v0           #_arg0:I
    :sswitch_8c
    const-string v3, "com.lge.systemservice.core.wfdmanager.IWfdManager"

    #@8e
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@91
    .line 107
    invoke-virtual {p0}, Lcom/lge/systemservice/core/wfdmanager/IWfdManager$Stub;->getWfdMode()I

    #@94
    move-result v1

    #@95
    .line 108
    .local v1, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@98
    .line 109
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@9b
    goto/16 :goto_9

    #@9d
    .line 114
    .end local v1           #_result:I
    :sswitch_9d
    const-string v3, "com.lge.systemservice.core.wfdmanager.IWfdManager"

    #@9f
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a2
    .line 116
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@a5
    move-result-object v0

    #@a6
    .line 117
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/wfdmanager/IWfdManager$Stub;->setTargetUrl(Ljava/lang/String;)V

    #@a9
    .line 118
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@ac
    goto/16 :goto_9

    #@ae
    .line 123
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_ae
    const-string v3, "com.lge.systemservice.core.wfdmanager.IWfdManager"

    #@b0
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b3
    .line 124
    invoke-virtual {p0}, Lcom/lge/systemservice/core/wfdmanager/IWfdManager$Stub;->getTargetUrl()Ljava/lang/String;

    #@b6
    move-result-object v1

    #@b7
    .line 125
    .local v1, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@ba
    .line 126
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@bd
    goto/16 :goto_9

    #@bf
    .line 131
    .end local v1           #_result:Ljava/lang/String;
    :sswitch_bf
    const-string v3, "com.lge.systemservice.core.wfdmanager.IWfdManager"

    #@c1
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c4
    .line 133
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@c7
    move-result v0

    #@c8
    .line 134
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/wfdmanager/IWfdManager$Stub;->setTargetRtspPort(I)V

    #@cb
    .line 135
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@ce
    goto/16 :goto_9

    #@d0
    .line 140
    .end local v0           #_arg0:I
    :sswitch_d0
    const-string v3, "com.lge.systemservice.core.wfdmanager.IWfdManager"

    #@d2
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d5
    .line 142
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@d8
    move-result v0

    #@d9
    .line 143
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/wfdmanager/IWfdManager$Stub;->setTargetIpAddress_Url(I)V

    #@dc
    .line 144
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@df
    goto/16 :goto_9

    #@e1
    .line 149
    .end local v0           #_arg0:I
    :sswitch_e1
    const-string v4, "com.lge.systemservice.core.wfdmanager.IWfdManager"

    #@e3
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@e6
    .line 150
    invoke-virtual {p0}, Lcom/lge/systemservice/core/wfdmanager/IWfdManager$Stub;->clientConnect()Z

    #@e9
    move-result v1

    #@ea
    .line 151
    .local v1, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@ed
    .line 152
    if-eqz v1, :cond_f0

    #@ef
    move v3, v2

    #@f0
    :cond_f0
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@f3
    goto/16 :goto_9

    #@f5
    .line 157
    .end local v1           #_result:Z
    :sswitch_f5
    const-string v4, "com.lge.systemservice.core.wfdmanager.IWfdManager"

    #@f7
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@fa
    .line 158
    invoke-virtual {p0}, Lcom/lge/systemservice/core/wfdmanager/IWfdManager$Stub;->play()Z

    #@fd
    move-result v1

    #@fe
    .line 159
    .restart local v1       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@101
    .line 160
    if-eqz v1, :cond_104

    #@103
    move v3, v2

    #@104
    :cond_104
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@107
    goto/16 :goto_9

    #@109
    .line 165
    .end local v1           #_result:Z
    :sswitch_109
    const-string v4, "com.lge.systemservice.core.wfdmanager.IWfdManager"

    #@10b
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@10e
    .line 166
    invoke-virtual {p0}, Lcom/lge/systemservice/core/wfdmanager/IWfdManager$Stub;->pause()Z

    #@111
    move-result v1

    #@112
    .line 167
    .restart local v1       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@115
    .line 168
    if-eqz v1, :cond_118

    #@117
    move v3, v2

    #@118
    :cond_118
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@11b
    goto/16 :goto_9

    #@11d
    .line 173
    .end local v1           #_result:Z
    :sswitch_11d
    const-string v4, "com.lge.systemservice.core.wfdmanager.IWfdManager"

    #@11f
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@122
    .line 174
    invoke-virtual {p0}, Lcom/lge/systemservice/core/wfdmanager/IWfdManager$Stub;->teardown()Z

    #@125
    move-result v1

    #@126
    .line 175
    .restart local v1       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@129
    .line 176
    if-eqz v1, :cond_12c

    #@12b
    move v3, v2

    #@12c
    :cond_12c
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@12f
    goto/16 :goto_9

    #@131
    .line 181
    .end local v1           #_result:Z
    :sswitch_131
    const-string v3, "com.lge.systemservice.core.wfdmanager.IWfdManager"

    #@133
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@136
    .line 182
    invoke-virtual {p0}, Lcom/lge/systemservice/core/wfdmanager/IWfdManager$Stub;->getWfdState()I

    #@139
    move-result v1

    #@13a
    .line 183
    .local v1, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@13d
    .line 184
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@140
    goto/16 :goto_9

    #@142
    .line 189
    .end local v1           #_result:I
    :sswitch_142
    const-string v3, "com.lge.systemservice.core.wfdmanager.IWfdManager"

    #@144
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@147
    .line 190
    invoke-virtual {p0}, Lcom/lge/systemservice/core/wfdmanager/IWfdManager$Stub;->getRtspState()I

    #@14a
    move-result v1

    #@14b
    .line 191
    .restart local v1       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@14e
    .line 192
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@151
    goto/16 :goto_9

    #@153
    .line 43
    nop

    #@154
    :sswitch_data_154
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_2c
        0x3 -> :sswitch_48
        0x4 -> :sswitch_5b
        0x5 -> :sswitch_6b
        0x6 -> :sswitch_7b
        0x7 -> :sswitch_8c
        0x8 -> :sswitch_9d
        0x9 -> :sswitch_ae
        0xa -> :sswitch_bf
        0xb -> :sswitch_d0
        0xc -> :sswitch_e1
        0xd -> :sswitch_f5
        0xe -> :sswitch_109
        0xf -> :sswitch_11d
        0x10 -> :sswitch_131
        0x11 -> :sswitch_142
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
