.class public Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;
.super Lcom/lge/systemservice/core/LGServiceManager;
.source "CliptrayManager.java"

# interfaces
.implements Lcom/lge/loader/cliptray/ICliptrayManagerLoader;


# static fields
.field private static gDefault:Lcom/lge/systemservice/core/LGServiceSingleton;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/lge/systemservice/core/LGServiceSingleton",
            "<",
            "Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mClipManager:Landroid/content/ClipboardManager;

.field private mConnected:Z

.field private mContext:Landroid/content/Context;

.field private mCurrPasteListener:Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;

.field private mCurrentInputType:I

.field private mHandler:Landroid/os/Handler;

.field private mIsShowing:Z

.field private final mPasteListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mPasteServiceListener:Lcom/lge/systemservice/core/cliptraymanager/IOnPasteListener$Stub;

.field private thumbnailHeight:I

.field private thumbnailWidth:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 96
    new-instance v0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager$1;

    #@2
    const-string v1, "cliptray"

    #@4
    invoke-direct {v0, v1}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager$1;-><init>(Ljava/lang/String;)V

    #@7
    sput-object v0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->gDefault:Lcom/lge/systemservice/core/LGServiceSingleton;

    #@9
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 112
    invoke-direct {p0}, Lcom/lge/systemservice/core/LGServiceManager;-><init>()V

    #@5
    .line 86
    iput-object v3, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mContext:Landroid/content/Context;

    #@7
    .line 88
    iput-boolean v2, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mIsShowing:Z

    #@9
    .line 89
    iput-boolean v2, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mConnected:Z

    #@b
    .line 90
    iput v2, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mCurrentInputType:I

    #@d
    .line 94
    iput-object v3, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mCurrPasteListener:Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;

    #@f
    .line 388
    new-instance v2, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager$2;

    #@11
    invoke-direct {v2, p0}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager$2;-><init>(Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;)V

    #@14
    iput-object v2, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mHandler:Landroid/os/Handler;

    #@16
    .line 540
    new-instance v2, Ljava/util/ArrayList;

    #@18
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@1b
    iput-object v2, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mPasteListeners:Ljava/util/ArrayList;

    #@1d
    .line 543
    new-instance v2, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager$3;

    #@1f
    invoke-direct {v2, p0}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager$3;-><init>(Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;)V

    #@22
    iput-object v2, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mPasteServiceListener:Lcom/lge/systemservice/core/cliptraymanager/IOnPasteListener$Stub;

    #@24
    .line 113
    iput-object p1, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mContext:Landroid/content/Context;

    #@26
    .line 114
    iget-object v2, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mContext:Landroid/content/Context;

    #@28
    const-string v3, "clipboard"

    #@2a
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@2d
    move-result-object v2

    #@2e
    check-cast v2, Landroid/content/ClipboardManager;

    #@30
    iput-object v2, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mClipManager:Landroid/content/ClipboardManager;

    #@32
    .line 116
    iget-object v2, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mContext:Landroid/content/Context;

    #@34
    const-string v3, "window"

    #@36
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@39
    move-result-object v1

    #@3a
    check-cast v1, Landroid/view/WindowManager;

    #@3c
    .line 117
    .local v1, wm:Landroid/view/WindowManager;
    new-instance v0, Landroid/util/DisplayMetrics;

    #@3e
    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    #@41
    .line 118
    .local v0, metrics:Landroid/util/DisplayMetrics;
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    #@44
    move-result-object v2

    #@45
    invoke-virtual {v2, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    #@48
    .line 120
    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    #@4a
    iput v2, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->thumbnailWidth:I

    #@4c
    .line 121
    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    #@4e
    iput v2, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->thumbnailHeight:I

    #@50
    .line 122
    const-string v2, "Cliptray Manager"

    #@52
    const-string v3, "new CliptrayManager"

    #@54
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@57
    .line 124
    return-void
.end method

.method static synthetic access$000(Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 67
    invoke-direct {p0}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->reportPaste()V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 67
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method private calculateInSampleSize(II)I
    .registers 9
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 825
    const/4 v1, 0x1

    #@1
    .line 827
    .local v1, inSampleSize:I
    iget v3, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->thumbnailHeight:I

    #@3
    if-gt p2, v3, :cond_9

    #@5
    iget v3, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->thumbnailWidth:I

    #@7
    if-le p1, v3, :cond_1e

    #@9
    .line 828
    :cond_9
    int-to-float v3, p2

    #@a
    iget v4, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->thumbnailHeight:I

    #@c
    int-to-float v4, v4

    #@d
    div-float/2addr v3, v4

    #@e
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    #@11
    move-result v0

    #@12
    .line 829
    .local v0, heightRatio:I
    int-to-float v3, p1

    #@13
    iget v4, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->thumbnailWidth:I

    #@15
    int-to-float v4, v4

    #@16
    div-float/2addr v3, v4

    #@17
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    #@1a
    move-result v2

    #@1b
    .line 831
    .local v2, widthRatio:I
    if-ge v0, v2, :cond_37

    #@1d
    move v1, v0

    #@1e
    .line 833
    .end local v0           #heightRatio:I
    .end local v2           #widthRatio:I
    :cond_1e
    :goto_1e
    const-string v3, "Cliptray Manager"

    #@20
    new-instance v4, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v5, "calculateInSampleSize: "

    #@27
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v4

    #@2b
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v4

    #@2f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v4

    #@33
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 834
    return v1

    #@37
    .restart local v0       #heightRatio:I
    .restart local v2       #widthRatio:I
    :cond_37
    move v1, v2

    #@38
    .line 831
    goto :goto_1e
.end method

.method private checkIsPNGfile(Landroid/net/Uri;)Z
    .registers 7
    .parameter "imgUri"

    #@0
    .prologue
    .line 1058
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    #@3
    move-result-object v2

    #@4
    .line 1059
    .local v2, lastPathSegment:Ljava/lang/String;
    const-string v3, "."

    #@6
    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    #@9
    move-result v1

    #@a
    .line 1060
    .local v1, index:I
    add-int/lit8 v3, v1, 0x1

    #@c
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@f
    move-result v4

    #@10
    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@13
    move-result-object v0

    #@14
    .line 1061
    .local v0, fileFormat:Ljava/lang/String;
    const-string v3, "PNG"

    #@16
    invoke-virtual {v0, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@19
    move-result v3

    #@1a
    if-nez v3, :cond_1e

    #@1c
    const/4 v3, 0x1

    #@1d
    :goto_1d
    return v3

    #@1e
    :cond_1e
    const/4 v3, 0x0

    #@1f
    goto :goto_1d
.end method

.method private copy(Ljava/lang/CharSequence;)V
    .registers 13
    .parameter

    #@0
    .prologue
    const/4 v10, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 402
    const/4 v1, 0x0

    #@3
    .line 404
    instance-of v0, p1, Landroid/text/Spanned;

    #@5
    if-eqz v0, :cond_30

    #@7
    .line 406
    instance-of v0, p1, Landroid/text/Spannable;

    #@9
    if-eqz v0, :cond_45

    #@b
    move-object v0, p1

    #@c
    .line 407
    check-cast v0, Landroid/text/Spannable;

    #@e
    move-object v2, v0

    #@f
    .line 414
    :goto_f
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@12
    move-result v0

    #@13
    const-class v3, Landroid/text/style/DynamicDrawableSpan;

    #@15
    invoke-interface {v2, v5, v0, v3}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@18
    move-result-object v0

    #@19
    check-cast v0, [Landroid/text/style/DynamicDrawableSpan;

    #@1b
    .line 416
    array-length v3, v0

    #@1c
    if-gtz v3, :cond_4d

    #@1e
    .line 418
    new-instance v0, Landroid/content/ClipData$Item;

    #@20
    invoke-direct {v0, p1}, Landroid/content/ClipData$Item;-><init>(Ljava/lang/CharSequence;)V

    #@23
    .line 419
    new-instance v1, Landroid/content/ClipData;

    #@25
    const-string v2, "text"

    #@27
    new-array v3, v10, [Ljava/lang/String;

    #@29
    const-string v4, "text/plain"

    #@2b
    aput-object v4, v3, v5

    #@2d
    invoke-direct {v1, v2, v3, v0}, Landroid/content/ClipData;-><init>(Ljava/lang/CharSequence;[Ljava/lang/String;Landroid/content/ClipData$Item;)V

    #@30
    .line 484
    :cond_30
    :goto_30
    if-eqz v1, :cond_44

    #@32
    .line 485
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@35
    move-result-object v0

    #@36
    if-eqz v0, :cond_3f

    #@38
    .line 487
    :try_start_38
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@3b
    move-result-object v0

    #@3c
    invoke-interface {v0}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->doCopyAnimation()V
    :try_end_3f
    .catch Landroid/os/RemoteException; {:try_start_38 .. :try_end_3f} :catch_105

    #@3f
    .line 493
    :cond_3f
    :goto_3f
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mClipManager:Landroid/content/ClipboardManager;

    #@41
    invoke-virtual {v0, v1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    #@44
    .line 495
    :cond_44
    return-void

    #@45
    .line 409
    :cond_45
    new-instance v0, Landroid/text/SpannableString;

    #@47
    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    #@4a
    move-object v2, v0

    #@4b
    move-object p1, v0

    #@4c
    .line 410
    goto :goto_f

    #@4d
    :cond_4d
    move v4, v5

    #@4e
    move v3, v5

    #@4f
    .line 424
    :goto_4f
    array-length v6, v0

    #@50
    if-ge v4, v6, :cond_dc

    #@52
    .line 425
    aget-object v6, v0, v4

    #@54
    invoke-interface {v2, v6}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    #@57
    move-result v6

    #@58
    .line 427
    invoke-static {v3, v6}, Ljava/lang/Math;->min(II)I

    #@5b
    move-result v3

    #@5c
    invoke-static {v5, v3}, Ljava/lang/Math;->max(II)I

    #@5f
    move-result v3

    #@60
    .line 428
    invoke-static {v3, v6}, Ljava/lang/Math;->max(II)I

    #@63
    move-result v6

    #@64
    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    #@67
    move-result v6

    #@68
    .line 430
    if-eq v3, v6, :cond_ce

    #@6a
    .line 431
    invoke-interface {p1, v3, v6}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    #@6d
    move-result-object v3

    #@6e
    .line 433
    new-instance v6, Landroid/content/ClipData$Item;

    #@70
    invoke-direct {v6, v3}, Landroid/content/ClipData$Item;-><init>(Ljava/lang/CharSequence;)V

    #@73
    .line 435
    if-nez v1, :cond_cb

    #@75
    .line 437
    new-instance v1, Landroid/content/ClipData;

    #@77
    const-string v3, "text"

    #@79
    new-array v7, v10, [Ljava/lang/String;

    #@7b
    const-string v8, "text/plain"

    #@7d
    aput-object v8, v7, v5

    #@7f
    invoke-direct {v1, v3, v7, v6}, Landroid/content/ClipData;-><init>(Ljava/lang/CharSequence;[Ljava/lang/String;Landroid/content/ClipData$Item;)V

    #@82
    move-object v3, v1

    #@83
    .line 443
    :goto_83
    aget-object v1, v0, v4

    #@85
    invoke-interface {v2, v1}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    #@88
    move-result v6

    #@89
    .line 446
    aget-object v1, v0, v4

    #@8b
    invoke-virtual {v1}, Landroid/text/style/DynamicDrawableSpan;->getDrawable()Landroid/graphics/drawable/Drawable;

    #@8e
    move-result-object v1

    #@8f
    .line 447
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@92
    move-result-object v1

    #@93
    const-class v7, Landroid/graphics/drawable/BitmapDrawable;

    #@95
    invoke-virtual {v1, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@98
    move-result v1

    #@99
    .line 450
    if-eqz v1, :cond_c5

    #@9b
    .line 451
    aget-object v1, v0, v4

    #@9d
    invoke-virtual {v1}, Landroid/text/style/DynamicDrawableSpan;->getDrawable()Landroid/graphics/drawable/Drawable;

    #@a0
    move-result-object v1

    #@a1
    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    #@a3
    .line 452
    if-eqz v1, :cond_d4

    #@a5
    .line 453
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    #@a8
    move-result-object v1

    #@a9
    .line 454
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->hasAlpha()Z

    #@ac
    move-result v7

    #@ad
    .line 455
    invoke-virtual {p0, v1, v7}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->insertImage(Landroid/graphics/Bitmap;Z)Landroid/net/Uri;

    #@b0
    move-result-object v1

    #@b1
    .line 456
    new-instance v7, Landroid/content/ClipData$Item;

    #@b3
    invoke-direct {v7, v1}, Landroid/content/ClipData$Item;-><init>(Landroid/net/Uri;)V

    #@b6
    .line 459
    if-nez v3, :cond_d0

    #@b8
    .line 460
    new-instance v3, Landroid/content/ClipData;

    #@ba
    const-string v1, "uri"

    #@bc
    new-array v8, v10, [Ljava/lang/String;

    #@be
    const-string v9, "text/uri-list"

    #@c0
    aput-object v9, v8, v5

    #@c2
    invoke-direct {v3, v1, v8, v7}, Landroid/content/ClipData;-><init>(Ljava/lang/CharSequence;[Ljava/lang/String;Landroid/content/ClipData$Item;)V

    #@c5
    .line 424
    :cond_c5
    :goto_c5
    add-int/lit8 v1, v4, 0x1

    #@c7
    move v4, v1

    #@c8
    move-object v1, v3

    #@c9
    move v3, v6

    #@ca
    goto :goto_4f

    #@cb
    .line 440
    :cond_cb
    invoke-virtual {v1, v6}, Landroid/content/ClipData;->addItem(Landroid/content/ClipData$Item;)V

    #@ce
    :cond_ce
    move-object v3, v1

    #@cf
    goto :goto_83

    #@d0
    .line 462
    :cond_d0
    invoke-virtual {v3, v7}, Landroid/content/ClipData;->addItem(Landroid/content/ClipData$Item;)V

    #@d3
    goto :goto_c5

    #@d4
    .line 465
    :cond_d4
    const-string v1, "Cliptray Manager"

    #@d6
    const-string v7, "failed to copy image!!"

    #@d8
    invoke-static {v1, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@db
    goto :goto_c5

    #@dc
    .line 471
    :cond_dc
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@df
    move-result v0

    #@e0
    if-eq v3, v0, :cond_30

    #@e2
    .line 472
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@e5
    move-result v0

    #@e6
    invoke-interface {p1, v3, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    #@e9
    move-result-object v0

    #@ea
    .line 473
    new-instance v2, Landroid/content/ClipData$Item;

    #@ec
    invoke-direct {v2, v0}, Landroid/content/ClipData$Item;-><init>(Ljava/lang/CharSequence;)V

    #@ef
    .line 475
    if-nez v1, :cond_100

    #@f1
    .line 476
    new-instance v1, Landroid/content/ClipData;

    #@f3
    const-string v0, "text"

    #@f5
    new-array v3, v10, [Ljava/lang/String;

    #@f7
    const-string v4, "text/plain"

    #@f9
    aput-object v4, v3, v5

    #@fb
    invoke-direct {v1, v0, v3, v2}, Landroid/content/ClipData;-><init>(Ljava/lang/CharSequence;[Ljava/lang/String;Landroid/content/ClipData$Item;)V

    #@fe
    goto/16 :goto_30

    #@100
    .line 478
    :cond_100
    invoke-virtual {v1, v2}, Landroid/content/ClipData;->addItem(Landroid/content/ClipData$Item;)V

    #@103
    goto/16 :goto_30

    #@105
    .line 488
    :catch_105
    move-exception v0

    #@106
    .line 489
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@109
    goto/16 :goto_3f
.end method

.method private copyClipData(Landroid/content/ClipData;)Z
    .registers 16
    .parameter "clip"

    #@0
    .prologue
    .line 666
    const/4 v8, 0x0

    #@1
    .line 668
    .local v8, newclip:Landroid/content/ClipData;
    invoke-virtual {p1}, Landroid/content/ClipData;->getItemCount()I

    #@4
    move-result v2

    #@5
    .line 669
    .local v2, count:I
    const/4 v4, 0x0

    #@6
    .local v4, i:I
    :goto_6
    if-ge v4, v2, :cond_b0

    #@8
    .line 670
    invoke-virtual {p1, v4}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    #@b
    move-result-object v6

    #@c
    .line 671
    .local v6, item:Landroid/content/ClipData$Item;
    invoke-virtual {v6}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    #@f
    move-result-object v1

    #@10
    .line 672
    .local v1, clipUri:Landroid/net/Uri;
    const-string v10, "Cliptray Manager"

    #@12
    new-instance v11, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v12, "copy clipdata uri = "

    #@19
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v11

    #@1d
    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v11

    #@21
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v11

    #@25
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 673
    if-eqz v1, :cond_9a

    #@2a
    const-string v10, "file"

    #@2c
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@2f
    move-result-object v11

    #@30
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@33
    move-result v10

    #@34
    if-eqz v10, :cond_9a

    #@36
    .line 675
    invoke-direct {p0, v1}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getSampledBitmapFromUri(Landroid/net/Uri;)Landroid/graphics/Bitmap;

    #@39
    move-result-object v0

    #@3a
    .line 676
    .local v0, bitmap:Landroid/graphics/Bitmap;
    if-nez v0, :cond_56

    #@3c
    .line 677
    const-string v10, "Cliptray Manager"

    #@3e
    new-instance v11, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v12, "image copy failed.. cannot decode from URI: "

    #@45
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v11

    #@49
    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v11

    #@4d
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v11

    #@51
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    .line 678
    const/4 v10, 0x0

    #@55
    .line 718
    .end local v0           #bitmap:Landroid/graphics/Bitmap;
    .end local v1           #clipUri:Landroid/net/Uri;
    .end local v6           #item:Landroid/content/ClipData$Item;
    :goto_55
    return v10

    #@56
    .line 681
    .restart local v0       #bitmap:Landroid/graphics/Bitmap;
    .restart local v1       #clipUri:Landroid/net/Uri;
    .restart local v6       #item:Landroid/content/ClipData$Item;
    :cond_56
    invoke-direct {p0, v1}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->checkIsPNGfile(Landroid/net/Uri;)Z

    #@59
    move-result v5

    #@5a
    .line 683
    .local v5, isPNG:Z
    if-eqz v5, :cond_78

    #@5c
    .line 684
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->hasAlpha()Z

    #@5f
    move-result v5

    #@60
    .line 685
    const-string v10, "test"

    #@62
    new-instance v11, Ljava/lang/StringBuilder;

    #@64
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@67
    const-string v12, "save as png? "

    #@69
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v11

    #@6d
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@70
    move-result-object v11

    #@71
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v11

    #@75
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@78
    .line 688
    :cond_78
    invoke-virtual {p0, v0, v5}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->insertImage(Landroid/graphics/Bitmap;Z)Landroid/net/Uri;

    #@7b
    move-result-object v7

    #@7c
    .line 689
    .local v7, newUri:Landroid/net/Uri;
    new-instance v9, Landroid/content/ClipData$Item;

    #@7e
    invoke-direct {v9, v7}, Landroid/content/ClipData$Item;-><init>(Landroid/net/Uri;)V

    #@81
    .line 691
    .local v9, newitem:Landroid/content/ClipData$Item;
    if-nez v8, :cond_96

    #@83
    .line 692
    new-instance v8, Landroid/content/ClipData;

    #@85
    .end local v8           #newclip:Landroid/content/ClipData;
    const-string v10, "uri"

    #@87
    const/4 v11, 0x1

    #@88
    new-array v11, v11, [Ljava/lang/String;

    #@8a
    const/4 v12, 0x0

    #@8b
    const-string v13, "text/uri-list"

    #@8d
    aput-object v13, v11, v12

    #@8f
    invoke-direct {v8, v10, v11, v9}, Landroid/content/ClipData;-><init>(Ljava/lang/CharSequence;[Ljava/lang/String;Landroid/content/ClipData$Item;)V

    #@92
    .line 669
    .end local v0           #bitmap:Landroid/graphics/Bitmap;
    .end local v5           #isPNG:Z
    .end local v7           #newUri:Landroid/net/Uri;
    .end local v9           #newitem:Landroid/content/ClipData$Item;
    .restart local v8       #newclip:Landroid/content/ClipData;
    :goto_92
    add-int/lit8 v4, v4, 0x1

    #@94
    goto/16 :goto_6

    #@96
    .line 694
    .restart local v0       #bitmap:Landroid/graphics/Bitmap;
    .restart local v5       #isPNG:Z
    .restart local v7       #newUri:Landroid/net/Uri;
    .restart local v9       #newitem:Landroid/content/ClipData$Item;
    :cond_96
    invoke-virtual {v8, v9}, Landroid/content/ClipData;->addItem(Landroid/content/ClipData$Item;)V

    #@99
    goto :goto_92

    #@9a
    .line 698
    .end local v0           #bitmap:Landroid/graphics/Bitmap;
    .end local v5           #isPNG:Z
    .end local v7           #newUri:Landroid/net/Uri;
    .end local v9           #newitem:Landroid/content/ClipData$Item;
    :cond_9a
    if-nez v8, :cond_ac

    #@9c
    .line 699
    new-instance v8, Landroid/content/ClipData;

    #@9e
    .end local v8           #newclip:Landroid/content/ClipData;
    const-string v10, "text"

    #@a0
    const/4 v11, 0x1

    #@a1
    new-array v11, v11, [Ljava/lang/String;

    #@a3
    const/4 v12, 0x0

    #@a4
    const-string v13, "text/plain"

    #@a6
    aput-object v13, v11, v12

    #@a8
    invoke-direct {v8, v10, v11, v6}, Landroid/content/ClipData;-><init>(Ljava/lang/CharSequence;[Ljava/lang/String;Landroid/content/ClipData$Item;)V

    #@ab
    .restart local v8       #newclip:Landroid/content/ClipData;
    goto :goto_92

    #@ac
    .line 701
    :cond_ac
    invoke-virtual {v8, v6}, Landroid/content/ClipData;->addItem(Landroid/content/ClipData$Item;)V

    #@af
    goto :goto_92

    #@b0
    .line 706
    .end local v1           #clipUri:Landroid/net/Uri;
    .end local v6           #item:Landroid/content/ClipData$Item;
    :cond_b0
    if-eqz v8, :cond_cb

    #@b2
    .line 707
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@b5
    move-result-object v10

    #@b6
    if-eqz v10, :cond_bf

    #@b8
    .line 709
    :try_start_b8
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@bb
    move-result-object v10

    #@bc
    invoke-interface {v10}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->doCopyAnimation()V
    :try_end_bf
    .catch Landroid/os/RemoteException; {:try_start_b8 .. :try_end_bf} :catch_c6

    #@bf
    .line 715
    :cond_bf
    :goto_bf
    iget-object v10, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mClipManager:Landroid/content/ClipboardManager;

    #@c1
    invoke-virtual {v10, v8}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    #@c4
    .line 716
    const/4 v10, 0x1

    #@c5
    goto :goto_55

    #@c6
    .line 710
    :catch_c6
    move-exception v3

    #@c7
    .line 711
    .local v3, e:Landroid/os/RemoteException;
    invoke-virtual {v3}, Landroid/os/RemoteException;->printStackTrace()V

    #@ca
    goto :goto_bf

    #@cb
    .line 718
    .end local v3           #e:Landroid/os/RemoteException;
    :cond_cb
    const/4 v10, 0x0

    #@cc
    goto :goto_55
.end method

.method private copyImageBitmap(Landroid/graphics/Bitmap;)Z
    .registers 12
    .parameter "bitmap"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 643
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->hasAlpha()Z

    #@5
    move-result v2

    #@6
    .line 645
    .local v2, hasAlpha:Z
    invoke-virtual {p0, p1, v2}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->insertImage(Landroid/graphics/Bitmap;Z)Landroid/net/Uri;

    #@9
    move-result-object v3

    #@a
    .line 647
    .local v3, imgUri:Landroid/net/Uri;
    new-instance v4, Landroid/content/ClipData$Item;

    #@c
    invoke-direct {v4, v3}, Landroid/content/ClipData$Item;-><init>(Landroid/net/Uri;)V

    #@f
    .line 648
    .local v4, item:Landroid/content/ClipData$Item;
    new-instance v0, Landroid/content/ClipData;

    #@11
    const-string v7, "uri"

    #@13
    new-array v8, v5, [Ljava/lang/String;

    #@15
    const-string v9, "text/uri-list"

    #@17
    aput-object v9, v8, v6

    #@19
    invoke-direct {v0, v7, v8, v4}, Landroid/content/ClipData;-><init>(Ljava/lang/CharSequence;[Ljava/lang/String;Landroid/content/ClipData$Item;)V

    #@1c
    .line 650
    .local v0, clip:Landroid/content/ClipData;
    if-eqz v0, :cond_36

    #@1e
    .line 651
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@21
    move-result-object v6

    #@22
    if-eqz v6, :cond_2b

    #@24
    .line 653
    :try_start_24
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@27
    move-result-object v6

    #@28
    invoke-interface {v6}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->doCopyAnimation()V
    :try_end_2b
    .catch Landroid/os/RemoteException; {:try_start_24 .. :try_end_2b} :catch_31

    #@2b
    .line 659
    :cond_2b
    :goto_2b
    iget-object v6, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mClipManager:Landroid/content/ClipboardManager;

    #@2d
    invoke-virtual {v6, v0}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    #@30
    .line 662
    :goto_30
    return v5

    #@31
    .line 654
    :catch_31
    move-exception v1

    #@32
    .line 655
    .local v1, e:Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    #@35
    goto :goto_2b

    #@36
    .end local v1           #e:Landroid/os/RemoteException;
    :cond_36
    move v5, v6

    #@37
    .line 662
    goto :goto_30
.end method

.method private copyImageUri(Landroid/net/Uri;)Z
    .registers 13
    .parameter "imgUri"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 603
    const-string v8, "Cliptray Manager"

    #@4
    new-instance v9, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v10, "copy image uri = "

    #@b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v9

    #@f
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v9

    #@13
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v9

    #@17
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 604
    if-eqz p1, :cond_28

    #@1c
    const-string v8, "file"

    #@1e
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@21
    move-result-object v9

    #@22
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25
    move-result v8

    #@26
    if-nez v8, :cond_30

    #@28
    .line 605
    :cond_28
    const-string v7, "Cliptray Manager"

    #@2a
    const-string v8, "image uri is null or type is wrong"

    #@2c
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 639
    :cond_2f
    :goto_2f
    return v6

    #@30
    .line 609
    :cond_30
    invoke-direct {p0, p1}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->checkIsPNGfile(Landroid/net/Uri;)Z

    #@33
    move-result v3

    #@34
    .line 612
    .local v3, isPNG:Z
    invoke-direct {p0, p1}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getSampledBitmapFromUri(Landroid/net/Uri;)Landroid/graphics/Bitmap;

    #@37
    move-result-object v0

    #@38
    .line 613
    .local v0, bitmap:Landroid/graphics/Bitmap;
    if-nez v0, :cond_53

    #@3a
    .line 614
    const-string v7, "Cliptray Manager"

    #@3c
    new-instance v8, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v9, "image copy failed.. cannot decode from URI: "

    #@43
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v8

    #@47
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v8

    #@4b
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v8

    #@4f
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    goto :goto_2f

    #@53
    .line 618
    :cond_53
    if-eqz v3, :cond_71

    #@55
    .line 619
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->hasAlpha()Z

    #@58
    move-result v3

    #@59
    .line 620
    const-string v8, "test"

    #@5b
    new-instance v9, Ljava/lang/StringBuilder;

    #@5d
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@60
    const-string v10, "save as png? "

    #@62
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v9

    #@66
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@69
    move-result-object v9

    #@6a
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d
    move-result-object v9

    #@6e
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@71
    .line 622
    :cond_71
    invoke-virtual {p0, v0, v3}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->insertImage(Landroid/graphics/Bitmap;Z)Landroid/net/Uri;

    #@74
    move-result-object v5

    #@75
    .line 624
    .local v5, newUri:Landroid/net/Uri;
    new-instance v4, Landroid/content/ClipData$Item;

    #@77
    invoke-direct {v4, v5}, Landroid/content/ClipData$Item;-><init>(Landroid/net/Uri;)V

    #@7a
    .line 625
    .local v4, item:Landroid/content/ClipData$Item;
    new-instance v1, Landroid/content/ClipData;

    #@7c
    const-string v8, "uri"

    #@7e
    new-array v9, v7, [Ljava/lang/String;

    #@80
    const-string v10, "text/uri-list"

    #@82
    aput-object v10, v9, v6

    #@84
    invoke-direct {v1, v8, v9, v4}, Landroid/content/ClipData;-><init>(Ljava/lang/CharSequence;[Ljava/lang/String;Landroid/content/ClipData$Item;)V

    #@87
    .line 627
    .local v1, clip:Landroid/content/ClipData;
    if-eqz v1, :cond_2f

    #@89
    .line 628
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@8c
    move-result-object v6

    #@8d
    if-eqz v6, :cond_96

    #@8f
    .line 630
    :try_start_8f
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@92
    move-result-object v6

    #@93
    invoke-interface {v6}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->doCopyAnimation()V
    :try_end_96
    .catch Landroid/os/RemoteException; {:try_start_8f .. :try_end_96} :catch_9d

    #@96
    .line 636
    :cond_96
    :goto_96
    iget-object v6, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mClipManager:Landroid/content/ClipboardManager;

    #@98
    invoke-virtual {v6, v1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    #@9b
    move v6, v7

    #@9c
    .line 637
    goto :goto_2f

    #@9d
    .line 631
    :catch_9d
    move-exception v2

    #@9e
    .line 632
    .local v2, e:Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    #@a1
    goto :goto_96
.end method

.method private copyScreenCapture(Landroid/net/Uri;)Z
    .registers 13
    .parameter "imgUri"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 722
    const-string v8, "Cliptray Manager"

    #@4
    new-instance v9, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v10, "copy image uri = "

    #@b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v9

    #@f
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v9

    #@13
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v9

    #@17
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 723
    if-eqz p1, :cond_28

    #@1c
    const-string v8, "file"

    #@1e
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@21
    move-result-object v9

    #@22
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25
    move-result v8

    #@26
    if-nez v8, :cond_30

    #@28
    .line 724
    :cond_28
    const-string v7, "Cliptray Manager"

    #@2a
    const-string v8, "image uri is null or type is wrong"

    #@2c
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 758
    :cond_2f
    :goto_2f
    return v6

    #@30
    .line 728
    :cond_30
    invoke-direct {p0, p1}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->checkIsPNGfile(Landroid/net/Uri;)Z

    #@33
    move-result v3

    #@34
    .line 731
    .local v3, isPNG:Z
    invoke-direct {p0, p1}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getSampledBitmapFromUri(Landroid/net/Uri;)Landroid/graphics/Bitmap;

    #@37
    move-result-object v0

    #@38
    .line 732
    .local v0, bitmap:Landroid/graphics/Bitmap;
    if-nez v0, :cond_53

    #@3a
    .line 733
    const-string v7, "Cliptray Manager"

    #@3c
    new-instance v8, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v9, "image copy failed.. cannot decode from URI: "

    #@43
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v8

    #@47
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v8

    #@4b
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v8

    #@4f
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    goto :goto_2f

    #@53
    .line 737
    :cond_53
    if-eqz v3, :cond_71

    #@55
    .line 738
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->hasAlpha()Z

    #@58
    move-result v3

    #@59
    .line 739
    const-string v8, "test"

    #@5b
    new-instance v9, Ljava/lang/StringBuilder;

    #@5d
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@60
    const-string v10, "save as png? "

    #@62
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v9

    #@66
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@69
    move-result-object v9

    #@6a
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d
    move-result-object v9

    #@6e
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@71
    .line 741
    :cond_71
    invoke-virtual {p0, v0, v3}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->insertImage(Landroid/graphics/Bitmap;Z)Landroid/net/Uri;

    #@74
    move-result-object v5

    #@75
    .line 743
    .local v5, newUri:Landroid/net/Uri;
    new-instance v4, Landroid/content/ClipData$Item;

    #@77
    invoke-direct {v4, v5}, Landroid/content/ClipData$Item;-><init>(Landroid/net/Uri;)V

    #@7a
    .line 744
    .local v4, item:Landroid/content/ClipData$Item;
    new-instance v1, Landroid/content/ClipData;

    #@7c
    const-string v8, "uri"

    #@7e
    new-array v9, v7, [Ljava/lang/String;

    #@80
    const-string v10, "text/uri-list"

    #@82
    aput-object v10, v9, v6

    #@84
    invoke-direct {v1, v8, v9, v4}, Landroid/content/ClipData;-><init>(Ljava/lang/CharSequence;[Ljava/lang/String;Landroid/content/ClipData$Item;)V

    #@87
    .line 746
    .local v1, clip:Landroid/content/ClipData;
    if-eqz v1, :cond_2f

    #@89
    .line 747
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@8c
    move-result-object v6

    #@8d
    if-eqz v6, :cond_9c

    #@8f
    invoke-virtual {p0}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getVisibility()I

    #@92
    move-result v6

    #@93
    if-nez v6, :cond_9c

    #@95
    .line 749
    :try_start_95
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@98
    move-result-object v6

    #@99
    invoke-interface {v6}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->doCopyAnimation()V
    :try_end_9c
    .catch Landroid/os/RemoteException; {:try_start_95 .. :try_end_9c} :catch_a3

    #@9c
    .line 755
    :cond_9c
    :goto_9c
    iget-object v6, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mClipManager:Landroid/content/ClipboardManager;

    #@9e
    invoke-virtual {v6, v1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    #@a1
    move v6, v7

    #@a2
    .line 756
    goto :goto_2f

    #@a3
    .line 750
    :catch_a3
    move-exception v2

    #@a4
    .line 751
    .local v2, e:Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    #@a7
    goto :goto_9c
.end method

.method private doCopyToCliptray(ILjava/lang/Object;)V
    .registers 10
    .parameter "type"
    .parameter "obj"

    #@0
    .prologue
    const/4 v6, 0x2

    #@1
    const/4 v5, 0x5

    #@2
    .line 977
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@5
    move-result-object v3

    #@6
    if-nez v3, :cond_9

    #@8
    .line 1055
    .end local p2
    :cond_8
    :goto_8
    return-void

    #@9
    .line 980
    .restart local p2
    :cond_9
    const-string v3, "Cliptray Manager"

    #@b
    const-string v4, "doCopyToCliptray"

    #@d
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 982
    invoke-virtual {p0}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getVisibility()I

    #@13
    move-result v3

    #@14
    if-nez v3, :cond_4c

    #@16
    const/4 v2, 0x1

    #@17
    .line 983
    .local v2, isShowingCliptray:Z
    :goto_17
    iget v3, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mCurrentInputType:I

    #@19
    if-eq v3, v6, :cond_27

    #@1b
    if-eq p1, v5, :cond_27

    #@1d
    if-nez v2, :cond_27

    #@1f
    .line 985
    :try_start_1f
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@22
    move-result-object v3

    #@23
    const/4 v4, 0x2

    #@24
    invoke-interface {v3, v4}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->setInputType(I)V
    :try_end_27
    .catch Landroid/os/RemoteException; {:try_start_1f .. :try_end_27} :catch_4e

    #@27
    .line 991
    :cond_27
    :goto_27
    const/4 v0, 0x1

    #@28
    .line 992
    .local v0, copy:Z
    packed-switch p1, :pswitch_data_b6

    #@2b
    .line 1011
    const/4 v0, 0x0

    #@2c
    .line 1015
    .end local p2
    :goto_2c
    :pswitch_2c
    if-nez v0, :cond_7a

    #@2e
    .line 1017
    :try_start_2e
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@31
    move-result-object v3

    #@32
    invoke-interface {v3}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->showDecodeErrorToast()V
    :try_end_35
    .catch Landroid/os/RemoteException; {:try_start_2e .. :try_end_35} :catch_75

    #@35
    .line 1048
    :cond_35
    :goto_35
    iget v3, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mCurrentInputType:I

    #@37
    if-eq v3, v6, :cond_8

    #@39
    if-eq p1, v5, :cond_8

    #@3b
    if-nez v2, :cond_8

    #@3d
    .line 1050
    :try_start_3d
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@40
    move-result-object v3

    #@41
    iget v4, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mCurrentInputType:I

    #@43
    invoke-interface {v3, v4}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->setInputType(I)V
    :try_end_46
    .catch Landroid/os/RemoteException; {:try_start_3d .. :try_end_46} :catch_47

    #@46
    goto :goto_8

    #@47
    .line 1051
    :catch_47
    move-exception v1

    #@48
    .line 1052
    .local v1, e:Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    #@4b
    goto :goto_8

    #@4c
    .line 982
    .end local v0           #copy:Z
    .end local v1           #e:Landroid/os/RemoteException;
    .end local v2           #isShowingCliptray:Z
    .restart local p2
    :cond_4c
    const/4 v2, 0x0

    #@4d
    goto :goto_17

    #@4e
    .line 986
    .restart local v2       #isShowingCliptray:Z
    :catch_4e
    move-exception v1

    #@4f
    .line 987
    .restart local v1       #e:Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    #@52
    goto :goto_27

    #@53
    .line 994
    .end local v1           #e:Landroid/os/RemoteException;
    .restart local v0       #copy:Z
    :pswitch_53
    check-cast p2, Ljava/lang/CharSequence;

    #@55
    .end local p2
    invoke-direct {p0, p2}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->copy(Ljava/lang/CharSequence;)V

    #@58
    goto :goto_2c

    #@59
    .line 999
    .restart local p2
    :pswitch_59
    check-cast p2, Landroid/net/Uri;

    #@5b
    .end local p2
    invoke-direct {p0, p2}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->copyImageUri(Landroid/net/Uri;)Z

    #@5e
    move-result v0

    #@5f
    .line 1000
    goto :goto_2c

    #@60
    .line 1002
    .restart local p2
    :pswitch_60
    check-cast p2, Landroid/graphics/Bitmap;

    #@62
    .end local p2
    invoke-direct {p0, p2}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->copyImageBitmap(Landroid/graphics/Bitmap;)Z

    #@65
    move-result v0

    #@66
    .line 1003
    goto :goto_2c

    #@67
    .line 1005
    .restart local p2
    :pswitch_67
    check-cast p2, Landroid/content/ClipData;

    #@69
    .end local p2
    invoke-direct {p0, p2}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->copyClipData(Landroid/content/ClipData;)Z

    #@6c
    move-result v0

    #@6d
    .line 1006
    goto :goto_2c

    #@6e
    .line 1008
    .restart local p2
    :pswitch_6e
    check-cast p2, Landroid/net/Uri;

    #@70
    .end local p2
    invoke-direct {p0, p2}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->copyScreenCapture(Landroid/net/Uri;)Z

    #@73
    move-result v0

    #@74
    .line 1009
    goto :goto_2c

    #@75
    .line 1018
    :catch_75
    move-exception v1

    #@76
    .line 1019
    .restart local v1       #e:Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    #@79
    goto :goto_35

    #@7a
    .line 1021
    .end local v1           #e:Landroid/os/RemoteException;
    :cond_7a
    invoke-virtual {p0}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getVisibility()I

    #@7d
    move-result v3

    #@7e
    const/16 v4, 0x8

    #@80
    if-ne v3, v4, :cond_a7

    #@82
    if-eq p1, v5, :cond_a7

    #@84
    .line 1023
    :try_start_84
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@87
    move-result-object v3

    #@88
    invoke-interface {v3}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->getPeek()V
    :try_end_8b
    .catch Landroid/os/RemoteException; {:try_start_84 .. :try_end_8b} :catch_9d

    #@8b
    .line 1029
    :goto_8b
    const-wide/16 v3, 0x320

    #@8d
    :try_start_8d
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_90
    .catch Ljava/lang/InterruptedException; {:try_start_8d .. :try_end_90} :catch_a2

    #@90
    .line 1035
    :goto_90
    :try_start_90
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@93
    move-result-object v3

    #@94
    invoke-interface {v3}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->getClose()V
    :try_end_97
    .catch Landroid/os/RemoteException; {:try_start_90 .. :try_end_97} :catch_98

    #@97
    goto :goto_35

    #@98
    .line 1036
    :catch_98
    move-exception v1

    #@99
    .line 1037
    .restart local v1       #e:Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    #@9c
    goto :goto_35

    #@9d
    .line 1024
    .end local v1           #e:Landroid/os/RemoteException;
    :catch_9d
    move-exception v1

    #@9e
    .line 1025
    .restart local v1       #e:Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    #@a1
    goto :goto_8b

    #@a2
    .line 1030
    .end local v1           #e:Landroid/os/RemoteException;
    :catch_a2
    move-exception v1

    #@a3
    .line 1031
    .local v1, e:Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    #@a6
    goto :goto_90

    #@a7
    .line 1039
    .end local v1           #e:Ljava/lang/InterruptedException;
    :cond_a7
    if-ne p1, v5, :cond_35

    #@a9
    .line 1041
    :try_start_a9
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@ac
    move-result-object v3

    #@ad
    invoke-interface {v3}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->showCliptrayCopiedToast()V
    :try_end_b0
    .catch Landroid/os/RemoteException; {:try_start_a9 .. :try_end_b0} :catch_b1

    #@b0
    goto :goto_35

    #@b1
    .line 1042
    :catch_b1
    move-exception v1

    #@b2
    .line 1043
    .local v1, e:Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    #@b5
    goto :goto_35

    #@b6
    .line 992
    :pswitch_data_b6
    .packed-switch 0x0
        :pswitch_53
        :pswitch_2c
        :pswitch_59
        :pswitch_60
        :pswitch_67
        :pswitch_6e
    .end packed-switch
.end method

.method private ensureFileExists(Ljava/lang/String;)Z
    .registers 11
    .parameter "path"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 901
    new-instance v2, Ljava/io/File;

    #@4
    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@7
    .line 902
    .local v2, file:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    #@a
    move-result v8

    #@b
    if-eqz v8, :cond_e

    #@d
    .line 927
    :goto_d
    return v6

    #@e
    .line 907
    :cond_e
    const/16 v8, 0x2f

    #@10
    invoke-virtual {p1, v8, v6}, Ljava/lang/String;->indexOf(II)I

    #@13
    move-result v5

    #@14
    .line 908
    .local v5, secondSlash:I
    if-ge v5, v6, :cond_18

    #@16
    move v6, v7

    #@17
    .line 909
    goto :goto_d

    #@18
    .line 911
    :cond_18
    invoke-virtual {p1, v7, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    .line 912
    .local v1, directoryPath:Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    #@1e
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@21
    .line 913
    .local v0, directory:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@24
    move-result v6

    #@25
    if-nez v6, :cond_29

    #@27
    move v6, v7

    #@28
    .line 914
    goto :goto_d

    #@29
    .line 917
    :cond_29
    invoke-virtual {v2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    #@2c
    move-result-object v4

    #@2d
    .line 918
    .local v4, parentFile:Ljava/io/File;
    if-eqz v4, :cond_32

    #@2f
    .line 919
    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    #@32
    .line 923
    :cond_32
    :try_start_32
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z
    :try_end_35
    .catch Ljava/io/IOException; {:try_start_32 .. :try_end_35} :catch_37

    #@35
    move-result v6

    #@36
    goto :goto_d

    #@37
    .line 924
    :catch_37
    move-exception v3

    #@38
    .line 925
    .local v3, ioe:Ljava/io/IOException;
    const-string v6, "Cliptray Manager"

    #@3a
    const-string v8, "File creation failed"

    #@3c
    invoke-static {v6, v8, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3f
    move v6, v7

    #@40
    .line 927
    goto :goto_d
.end method

.method private generateFileName(Z)Ljava/lang/String;
    .registers 5
    .parameter "isPNG"

    #@0
    .prologue
    .line 890
    if-eqz p1, :cond_1e

    #@2
    .line 891
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@a
    move-result-wide v1

    #@b
    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    const-string v1, ".png"

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    .line 893
    :goto_1d
    return-object v0

    #@1e
    :cond_1e
    new-instance v0, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@26
    move-result-wide v1

    #@27
    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    const-string v1, ".jpg"

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v0

    #@39
    goto :goto_1d
.end method

.method private getOrientation(Ljava/lang/String;)I
    .registers 8
    .parameter "path"

    #@0
    .prologue
    const/4 v3, -0x1

    #@1
    .line 1067
    :try_start_1
    new-instance v1, Landroid/media/ExifInterface;

    #@3
    invoke-direct {v1, p1}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_6} :catch_11

    #@6
    .line 1073
    .local v1, exif:Landroid/media/ExifInterface;
    const-string v4, "Orientation"

    #@8
    const/4 v5, 0x1

    #@9
    invoke-virtual {v1, v4, v5}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    #@c
    move-result v2

    #@d
    .line 1074
    .local v2, orientation:I
    packed-switch v2, :pswitch_data_22

    #@10
    .line 1081
    .end local v1           #exif:Landroid/media/ExifInterface;
    .end local v2           #orientation:I
    :goto_10
    :pswitch_10
    return v3

    #@11
    .line 1068
    :catch_11
    move-exception v0

    #@12
    .line 1069
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@15
    goto :goto_10

    #@16
    .line 1075
    .end local v0           #e:Ljava/io/IOException;
    .restart local v1       #exif:Landroid/media/ExifInterface;
    .restart local v2       #orientation:I
    :pswitch_16
    const/4 v3, 0x0

    #@17
    goto :goto_10

    #@18
    .line 1076
    :pswitch_18
    const/16 v3, 0x5a

    #@1a
    goto :goto_10

    #@1b
    .line 1077
    :pswitch_1b
    const/16 v3, 0xb4

    #@1d
    goto :goto_10

    #@1e
    .line 1078
    :pswitch_1e
    const/16 v3, 0x10e

    #@20
    goto :goto_10

    #@21
    .line 1074
    nop

    #@22
    :pswitch_data_22
    .packed-switch 0x1
        :pswitch_16
        :pswitch_10
        :pswitch_1b
        :pswitch_10
        :pswitch_10
        :pswitch_18
        :pswitch_10
        :pswitch_1e
    .end packed-switch
.end method

.method private getOutputUri(Ljava/lang/String;)Landroid/net/Uri;
    .registers 9
    .parameter "filename"

    #@0
    .prologue
    .line 935
    const-string v0, ".cliptray"

    #@2
    .line 938
    .local v0, foldername:Ljava/lang/String;
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    #@5
    move-result-object v4

    #@6
    const-string v5, "mounted"

    #@8
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v4

    #@c
    if-eqz v4, :cond_cb

    #@e
    .line 939
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    #@11
    move-result-object v4

    #@12
    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@15
    move-result-object v3

    #@16
    .line 941
    .local v3, path:Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v4

    #@1f
    const-string v5, "/Android/data"

    #@21
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v4

    #@25
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v3

    #@29
    .line 942
    new-instance v4, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v4

    #@32
    const-string v5, "/"

    #@34
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v4

    #@38
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v4

    #@3c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v3

    #@40
    .line 943
    new-instance v4, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v4

    #@49
    const-string v5, "/"

    #@4b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v4

    #@4f
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v4

    #@53
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v3

    #@57
    .line 944
    new-instance v1, Ljava/io/File;

    #@59
    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@5c
    .line 945
    .local v1, imageFile:Ljava/io/File;
    const-string v4, "Cliptray Manager"

    #@5e
    new-instance v5, Ljava/lang/StringBuilder;

    #@60
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@63
    const-string v6, "image save path = "

    #@65
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v5

    #@69
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v5

    #@6d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@70
    move-result-object v5

    #@71
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@74
    .line 947
    invoke-direct {p0, v3}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->ensureFileExists(Ljava/lang/String;)Z

    #@77
    move-result v4

    #@78
    if-nez v4, :cond_93

    #@7a
    .line 948
    new-instance v4, Ljava/lang/IllegalStateException;

    #@7c
    new-instance v5, Ljava/lang/StringBuilder;

    #@7e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@81
    const-string v6, "Unable to create new file: "

    #@83
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v5

    #@87
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v5

    #@8b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8e
    move-result-object v5

    #@8f
    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@92
    throw v4

    #@93
    .line 951
    :cond_93
    new-instance v2, Ljava/io/File;

    #@95
    new-instance v4, Ljava/lang/StringBuilder;

    #@97
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@9a
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    #@9d
    move-result-object v5

    #@9e
    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@a1
    move-result-object v5

    #@a2
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v4

    #@a6
    const-string v5, "/"

    #@a8
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v4

    #@ac
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af
    move-result-object v4

    #@b0
    const-string v5, "/.nomedia"

    #@b2
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v4

    #@b6
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b9
    move-result-object v4

    #@ba
    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@bd
    .line 952
    .local v2, nomediaFile:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    #@c0
    move-result v4

    #@c1
    if-nez v4, :cond_c6

    #@c3
    .line 953
    invoke-virtual {v2}, Ljava/io/File;->mkdir()Z

    #@c6
    .line 956
    :cond_c6
    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    #@c9
    move-result-object v4

    #@ca
    .line 959
    .end local v1           #imageFile:Ljava/io/File;
    .end local v2           #nomediaFile:Ljava/io/File;
    .end local v3           #path:Ljava/lang/String;
    :goto_ca
    return-object v4

    #@cb
    :cond_cb
    const/4 v4, 0x0

    #@cc
    goto :goto_ca
.end method

.method private getSampledBitmapFromUri(Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .registers 13
    .parameter "uri"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    const/4 v0, 0x0

    #@3
    .line 781
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    #@6
    move-result-object v2

    #@7
    invoke-direct {p0, v2}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getOrientation(Ljava/lang/String;)I

    #@a
    move-result v10

    #@b
    .line 782
    .local v10, orientation:I
    const-string v2, "Cliptray Manager"

    #@d
    new-instance v3, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v4, "image is rotated by : "

    #@14
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v3

    #@20
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 785
    iget-object v2, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mContext:Landroid/content/Context;

    #@25
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@28
    move-result-object v7

    #@29
    .line 786
    .local v7, cr:Landroid/content/ContentResolver;
    if-nez v7, :cond_33

    #@2b
    .line 787
    const-string v1, "Cliptray Manager"

    #@2d
    const-string v2, "getSampledBitmapFromUri: content resolver is null, cannot copy image"

    #@2f
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 821
    :cond_32
    :goto_32
    return-object v0

    #@33
    .line 795
    :cond_33
    new-instance v9, Landroid/graphics/BitmapFactory$Options;

    #@35
    invoke-direct {v9}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    #@38
    .line 796
    .local v9, options:Landroid/graphics/BitmapFactory$Options;
    iput-boolean v6, v9, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    #@3a
    .line 798
    :try_start_3a
    invoke-virtual {v7, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    #@3d
    move-result-object v2

    #@3e
    const/4 v3, 0x0

    #@3f
    invoke-static {v2, v3, v9}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_42
    .catch Ljava/io/FileNotFoundException; {:try_start_3a .. :try_end_42} :catch_71

    #@42
    .line 804
    :goto_42
    iget v2, v9, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    #@44
    iget v3, v9, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    #@46
    invoke-direct {p0, v2, v3}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->calculateInSampleSize(II)I

    #@49
    move-result v2

    #@4a
    iput v2, v9, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    #@4c
    .line 807
    const/4 v2, 0x0

    #@4d
    :try_start_4d
    iput-boolean v2, v9, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    #@4f
    .line 808
    invoke-virtual {v7, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    #@52
    move-result-object v2

    #@53
    const/4 v3, 0x0

    #@54
    invoke-static {v2, v3, v9}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_57
    .catch Ljava/io/FileNotFoundException; {:try_start_4d .. :try_end_57} :catch_76

    #@57
    move-result-object v0

    #@58
    .line 816
    .local v0, bitmap:Landroid/graphics/Bitmap;
    if-lez v10, :cond_32

    #@5a
    .line 817
    new-instance v5, Landroid/graphics/Matrix;

    #@5c
    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    #@5f
    .line 818
    .local v5, matrix:Landroid/graphics/Matrix;
    int-to-float v2, v10

    #@60
    invoke-virtual {v5, v2}, Landroid/graphics/Matrix;->postRotate(F)Z

    #@63
    .line 819
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    #@66
    move-result v3

    #@67
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    #@6a
    move-result v4

    #@6b
    move v2, v1

    #@6c
    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    #@6f
    move-result-object v0

    #@70
    goto :goto_32

    #@71
    .line 799
    .end local v0           #bitmap:Landroid/graphics/Bitmap;
    .end local v5           #matrix:Landroid/graphics/Matrix;
    :catch_71
    move-exception v8

    #@72
    .line 800
    .local v8, e:Ljava/io/FileNotFoundException;
    invoke-virtual {v8}, Ljava/io/FileNotFoundException;->printStackTrace()V

    #@75
    goto :goto_42

    #@76
    .line 809
    .end local v8           #e:Ljava/io/FileNotFoundException;
    :catch_76
    move-exception v8

    #@77
    .line 810
    .restart local v8       #e:Ljava/io/FileNotFoundException;
    invoke-virtual {v8}, Ljava/io/FileNotFoundException;->printStackTrace()V

    #@7a
    goto :goto_32
.end method

.method private static getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;
    .registers 1

    #@0
    .prologue
    .line 131
    sget-object v0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->gDefault:Lcom/lge/systemservice/core/LGServiceSingleton;

    #@2
    invoke-virtual {v0}, Lcom/lge/systemservice/core/LGServiceSingleton;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@8
    return-object v0
.end method

.method private reportPaste()V
    .registers 8

    #@0
    .prologue
    .line 521
    const-string v4, "Cliptray Manager"

    #@2
    const-string v5, "reportPaste"

    #@4
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 522
    iget-object v4, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mClipManager:Landroid/content/ClipboardManager;

    #@9
    iget-object v5, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mClipManager:Landroid/content/ClipboardManager;

    #@b
    invoke-virtual {v5}, Landroid/content/ClipboardManager;->getPrimaryClipCount()I

    #@e
    move-result v5

    #@f
    add-int/lit8 v5, v5, -0x1

    #@11
    invoke-virtual {v4, v5}, Landroid/content/ClipboardManager;->getPrimaryClipAt(I)Landroid/content/ClipData;

    #@14
    move-result-object v1

    #@15
    .line 526
    .local v1, clipdata:Landroid/content/ClipData;
    iget-object v5, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mPasteListeners:Ljava/util/ArrayList;

    #@17
    monitor-enter v5

    #@18
    .line 527
    :try_start_18
    iget-object v4, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mPasteListeners:Ljava/util/ArrayList;

    #@1a
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@1d
    move-result v0

    #@1e
    .line 528
    .local v0, N:I
    if-gtz v0, :cond_29

    #@20
    .line 529
    const-string v4, "Cliptray Manager"

    #@22
    const-string v6, "paste listener is not registered"

    #@24
    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 530
    monitor-exit v5

    #@28
    .line 538
    :cond_28
    return-void

    #@29
    .line 532
    :cond_29
    iget-object v4, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mPasteListeners:Ljava/util/ArrayList;

    #@2b
    invoke-virtual {v4}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    #@2e
    move-result-object v3

    #@2f
    .line 533
    .local v3, listeners:[Ljava/lang/Object;
    monitor-exit v5
    :try_end_30
    .catchall {:try_start_18 .. :try_end_30} :catchall_3e

    #@30
    .line 535
    const/4 v2, 0x0

    #@31
    .local v2, i:I
    :goto_31
    array-length v4, v3

    #@32
    if-ge v2, v4, :cond_28

    #@34
    .line 536
    aget-object v4, v3, v2

    #@36
    check-cast v4, Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;

    #@38
    invoke-interface {v4, v1}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;->onPaste(Landroid/content/ClipData;)V

    #@3b
    .line 535
    add-int/lit8 v2, v2, 0x1

    #@3d
    goto :goto_31

    #@3e
    .line 533
    .end local v0           #N:I
    .end local v2           #i:I
    .end local v3           #listeners:[Ljava/lang/Object;
    :catchall_3e
    move-exception v4

    #@3f
    :try_start_3f
    monitor-exit v5
    :try_end_40
    .catchall {:try_start_3f .. :try_end_40} :catchall_3e

    #@40
    throw v4
.end method

.method private saveBitmap(Landroid/graphics/Bitmap;Landroid/net/Uri;Z)V
    .registers 16
    .parameter "bitmap"
    .parameter "uri"
    .parameter "isPNG"

    #@0
    .prologue
    .line 842
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    #@3
    move-result v7

    #@4
    .line 843
    .local v7, width:I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    #@7
    move-result v3

    #@8
    .line 844
    .local v3, height:I
    iget v1, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->thumbnailWidth:I

    #@a
    .line 845
    .local v1, dstWidth:I
    iget v0, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->thumbnailHeight:I

    #@c
    .line 847
    .local v0, dstHeight:I
    iget v9, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->thumbnailWidth:I

    #@e
    if-gt v7, v9, :cond_14

    #@10
    iget v9, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->thumbnailHeight:I

    #@12
    if-le v3, v9, :cond_2f

    #@14
    .line 848
    :cond_14
    int-to-float v9, v3

    #@15
    iget v10, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->thumbnailHeight:I

    #@17
    int-to-float v10, v10

    #@18
    div-float v4, v9, v10

    #@1a
    .line 849
    .local v4, heightRatio:F
    int-to-float v9, v7

    #@1b
    iget v10, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->thumbnailWidth:I

    #@1d
    int-to-float v10, v10

    #@1e
    div-float v8, v9, v10

    #@20
    .line 850
    .local v8, widthRatio:F
    cmpl-float v9, v4, v8

    #@22
    if-lez v9, :cond_4c

    #@24
    .line 851
    int-to-float v9, v7

    #@25
    div-float/2addr v9, v4

    #@26
    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    #@29
    move-result v1

    #@2a
    .line 855
    :goto_2a
    const/4 v9, 0x1

    #@2b
    invoke-static {p1, v1, v0, v9}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    #@2e
    move-result-object p1

    #@2f
    .line 858
    .end local v4           #heightRatio:F
    .end local v8           #widthRatio:F
    :cond_2f
    const/4 v6, 0x0

    #@30
    .line 859
    .local v6, success:Z
    const/4 v5, 0x0

    #@31
    .line 862
    .local v5, outstream:Ljava/io/OutputStream;
    :try_start_31
    iget-object v9, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mContext:Landroid/content/Context;

    #@33
    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@36
    move-result-object v9

    #@37
    invoke-virtual {v9, p2}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;)Ljava/io/OutputStream;

    #@3a
    move-result-object v5

    #@3b
    .line 863
    if-eqz p3, :cond_53

    #@3d
    .line 864
    sget-object v9, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    #@3f
    const/4 v10, 0x0

    #@40
    invoke-virtual {p1, v9, v10, v5}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_43
    .catchall {:try_start_31 .. :try_end_43} :catchall_7c
    .catch Ljava/io/FileNotFoundException; {:try_start_31 .. :try_end_43} :catch_65

    #@43
    move-result v6

    #@44
    .line 871
    :goto_44
    if-eqz v5, :cond_49

    #@46
    .line 873
    :try_start_46
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V
    :try_end_49
    .catch Ljava/io/IOException; {:try_start_46 .. :try_end_49} :catch_5c

    #@49
    .line 881
    :cond_49
    :goto_49
    if-nez v6, :cond_4b

    #@4b
    .line 884
    :cond_4b
    return-void

    #@4c
    .line 853
    .end local v5           #outstream:Ljava/io/OutputStream;
    .end local v6           #success:Z
    .restart local v4       #heightRatio:F
    .restart local v8       #widthRatio:F
    :cond_4c
    int-to-float v9, v3

    #@4d
    div-float/2addr v9, v8

    #@4e
    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    #@51
    move-result v0

    #@52
    goto :goto_2a

    #@53
    .line 866
    .end local v4           #heightRatio:F
    .end local v8           #widthRatio:F
    .restart local v5       #outstream:Ljava/io/OutputStream;
    .restart local v6       #success:Z
    :cond_53
    :try_start_53
    sget-object v9, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    #@55
    const/16 v10, 0x50

    #@57
    invoke-virtual {p1, v9, v10, v5}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_5a
    .catchall {:try_start_53 .. :try_end_5a} :catchall_7c
    .catch Ljava/io/FileNotFoundException; {:try_start_53 .. :try_end_5a} :catch_65

    #@5a
    move-result v6

    #@5b
    goto :goto_44

    #@5c
    .line 875
    :catch_5c
    move-exception v2

    #@5d
    .line 876
    .local v2, ex:Ljava/io/IOException;
    const-string v9, "Cliptray Manager"

    #@5f
    const-string v10, "error closing outstream"

    #@61
    invoke-static {v9, v10, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@64
    goto :goto_49

    #@65
    .line 868
    .end local v2           #ex:Ljava/io/IOException;
    :catch_65
    move-exception v2

    #@66
    .line 869
    .local v2, ex:Ljava/io/FileNotFoundException;
    :try_start_66
    const-string v9, "Cliptray Manager"

    #@68
    const-string v10, "error creating file"

    #@6a
    invoke-static {v9, v10, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6d
    .catchall {:try_start_66 .. :try_end_6d} :catchall_7c

    #@6d
    .line 871
    if-eqz v5, :cond_49

    #@6f
    .line 873
    :try_start_6f
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V
    :try_end_72
    .catch Ljava/io/IOException; {:try_start_6f .. :try_end_72} :catch_73

    #@72
    goto :goto_49

    #@73
    .line 875
    :catch_73
    move-exception v2

    #@74
    .line 876
    .local v2, ex:Ljava/io/IOException;
    const-string v9, "Cliptray Manager"

    #@76
    const-string v10, "error closing outstream"

    #@78
    invoke-static {v9, v10, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@7b
    goto :goto_49

    #@7c
    .line 871
    .end local v2           #ex:Ljava/io/IOException;
    :catchall_7c
    move-exception v9

    #@7d
    if-eqz v5, :cond_82

    #@7f
    .line 873
    :try_start_7f
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V
    :try_end_82
    .catch Ljava/io/IOException; {:try_start_7f .. :try_end_82} :catch_83

    #@82
    .line 877
    :cond_82
    :goto_82
    throw v9

    #@83
    .line 875
    :catch_83
    move-exception v2

    #@84
    .line 876
    .restart local v2       #ex:Ljava/io/IOException;
    const-string v10, "Cliptray Manager"

    #@86
    const-string v11, "error closing outstream"

    #@88
    invoke-static {v10, v11, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@8b
    goto :goto_82
.end method


# virtual methods
.method public cliptrayPaste()V
    .registers 8

    #@0
    .prologue
    .line 369
    const-string v4, "Cliptray Manager"

    #@2
    const-string v5, "reportPaste"

    #@4
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 370
    iget-object v4, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mClipManager:Landroid/content/ClipboardManager;

    #@9
    iget-object v5, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mClipManager:Landroid/content/ClipboardManager;

    #@b
    invoke-virtual {v5}, Landroid/content/ClipboardManager;->getPrimaryClipCount()I

    #@e
    move-result v5

    #@f
    add-int/lit8 v5, v5, -0x1

    #@11
    invoke-virtual {v4, v5}, Landroid/content/ClipboardManager;->getPrimaryClipAt(I)Landroid/content/ClipData;

    #@14
    move-result-object v1

    #@15
    .line 374
    .local v1, clipdata:Landroid/content/ClipData;
    iget-object v5, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mPasteListeners:Ljava/util/ArrayList;

    #@17
    monitor-enter v5

    #@18
    .line 375
    :try_start_18
    iget-object v4, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mPasteListeners:Ljava/util/ArrayList;

    #@1a
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@1d
    move-result v0

    #@1e
    .line 376
    .local v0, N:I
    if-gtz v0, :cond_29

    #@20
    .line 377
    const-string v4, "Cliptray Manager"

    #@22
    const-string v6, "paste listener is not registered"

    #@24
    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 378
    monitor-exit v5

    #@28
    .line 386
    :cond_28
    return-void

    #@29
    .line 380
    :cond_29
    iget-object v4, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mPasteListeners:Ljava/util/ArrayList;

    #@2b
    invoke-virtual {v4}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    #@2e
    move-result-object v3

    #@2f
    .line 381
    .local v3, listeners:[Ljava/lang/Object;
    monitor-exit v5
    :try_end_30
    .catchall {:try_start_18 .. :try_end_30} :catchall_3e

    #@30
    .line 383
    const/4 v2, 0x0

    #@31
    .local v2, i:I
    :goto_31
    array-length v4, v3

    #@32
    if-ge v2, v4, :cond_28

    #@34
    .line 384
    aget-object v4, v3, v2

    #@36
    check-cast v4, Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;

    #@38
    invoke-interface {v4, v1}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;->onPaste(Landroid/content/ClipData;)V

    #@3b
    .line 383
    add-int/lit8 v2, v2, 0x1

    #@3d
    goto :goto_31

    #@3e
    .line 381
    .end local v0           #N:I
    .end local v2           #i:I
    .end local v3           #listeners:[Ljava/lang/Object;
    :catchall_3e
    move-exception v4

    #@3f
    :try_start_3f
    monitor-exit v5
    :try_end_40
    .catchall {:try_start_3f .. :try_end_40} :catchall_3e

    #@40
    throw v4
.end method

.method public copyImageToCliptray(Landroid/net/Uri;)V
    .registers 3
    .parameter "imgUri"

    #@0
    .prologue
    .line 226
    const/4 v0, 0x2

    #@1
    invoke-direct {p0, v0, p1}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->doCopyToCliptray(ILjava/lang/Object;)V

    #@4
    .line 227
    return-void
.end method

.method public copyScreenCaptureToCliptray(Landroid/net/Uri;)V
    .registers 3
    .parameter "imgUri"

    #@0
    .prologue
    .line 236
    const/4 v0, 0x5

    #@1
    invoke-direct {p0, v0, p1}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->doCopyToCliptray(ILjava/lang/Object;)V

    #@4
    .line 237
    return-void
.end method

.method public copyToCliptray(Landroid/content/ClipData;)V
    .registers 3
    .parameter "clip"

    #@0
    .prologue
    .line 231
    const/4 v0, 0x4

    #@1
    invoke-direct {p0, v0, p1}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->doCopyToCliptray(ILjava/lang/Object;)V

    #@4
    .line 232
    return-void
.end method

.method public copyToCliptray(Landroid/widget/TextView;)V
    .registers 11
    .parameter "text"

    #@0
    .prologue
    const/4 v8, 0x2

    #@1
    .line 170
    iget v5, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mCurrentInputType:I

    #@3
    if-eq v5, v8, :cond_d

    #@5
    .line 172
    :try_start_5
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@8
    move-result-object v5

    #@9
    const/4 v6, 0x2

    #@a
    invoke-interface {v5, v6}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->setInputType(I)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_d} :catch_20

    #@d
    .line 178
    :cond_d
    :goto_d
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@10
    move-result-object v5

    #@11
    invoke-static {v5}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@14
    move-result v4

    #@15
    .line 179
    .local v4, start:I
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@18
    move-result-object v5

    #@19
    invoke-static {v5}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@1c
    move-result v2

    #@1d
    .line 181
    .local v2, end:I
    if-ne v4, v2, :cond_25

    #@1f
    .line 217
    :cond_1f
    :goto_1f
    return-void

    #@20
    .line 173
    .end local v2           #end:I
    .end local v4           #start:I
    :catch_20
    move-exception v1

    #@21
    .line 174
    .local v1, e1:Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    #@24
    goto :goto_d

    #@25
    .line 185
    .end local v1           #e1:Landroid/os/RemoteException;
    .restart local v2       #end:I
    .restart local v4       #start:I
    :cond_25
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@28
    move-result-object v5

    #@29
    invoke-interface {v5, v4, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    #@2c
    move-result-object v3

    #@2d
    .line 186
    .local v3, selectedText:Ljava/lang/CharSequence;
    const-string v5, "Cliptray Manager"

    #@2f
    new-instance v6, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v7, "copy text = "

    #@36
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v6

    #@3a
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v6

    #@3e
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v6

    #@42
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 187
    invoke-direct {p0, v3}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->copy(Ljava/lang/CharSequence;)V

    #@48
    .line 189
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@4b
    move-result-object v5

    #@4c
    if-eqz v5, :cond_69

    #@4e
    invoke-virtual {p0}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getVisibility()I

    #@51
    move-result v5

    #@52
    const/16 v6, 0x8

    #@54
    if-ne v5, v6, :cond_69

    #@56
    .line 191
    :try_start_56
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@59
    move-result-object v5

    #@5a
    invoke-interface {v5}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->getPeek()V
    :try_end_5d
    .catch Landroid/os/RemoteException; {:try_start_56 .. :try_end_5d} :catch_7c

    #@5d
    .line 197
    :goto_5d
    const-wide/16 v5, 0x320

    #@5f
    :try_start_5f
    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_62
    .catch Ljava/lang/InterruptedException; {:try_start_5f .. :try_end_62} :catch_81

    #@62
    .line 203
    :goto_62
    :try_start_62
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@65
    move-result-object v5

    #@66
    invoke-interface {v5}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->getClose()V
    :try_end_69
    .catch Landroid/os/RemoteException; {:try_start_62 .. :try_end_69} :catch_86

    #@69
    .line 210
    :cond_69
    :goto_69
    iget v5, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mCurrentInputType:I

    #@6b
    if-eq v5, v8, :cond_1f

    #@6d
    .line 212
    :try_start_6d
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@70
    move-result-object v5

    #@71
    iget v6, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mCurrentInputType:I

    #@73
    invoke-interface {v5, v6}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->setInputType(I)V
    :try_end_76
    .catch Landroid/os/RemoteException; {:try_start_6d .. :try_end_76} :catch_77

    #@76
    goto :goto_1f

    #@77
    .line 213
    :catch_77
    move-exception v1

    #@78
    .line 214
    .restart local v1       #e1:Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    #@7b
    goto :goto_1f

    #@7c
    .line 192
    .end local v1           #e1:Landroid/os/RemoteException;
    :catch_7c
    move-exception v1

    #@7d
    .line 193
    .restart local v1       #e1:Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    #@80
    goto :goto_5d

    #@81
    .line 198
    .end local v1           #e1:Landroid/os/RemoteException;
    :catch_81
    move-exception v0

    #@82
    .line 199
    .local v0, e:Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    #@85
    goto :goto_62

    #@86
    .line 204
    .end local v0           #e:Ljava/lang/InterruptedException;
    :catch_86
    move-exception v1

    #@87
    .line 205
    .restart local v1       #e1:Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    #@8a
    goto :goto_69
.end method

.method public copyToCliptray(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "selectedText"

    #@0
    .prologue
    .line 221
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0, p1}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->doCopyToCliptray(ILjava/lang/Object;)V

    #@4
    .line 222
    return-void
.end method

.method public finishCliptrayService()V
    .registers 1

    #@0
    .prologue
    .line 136
    return-void
.end method

.method public getVisibility()I
    .registers 4

    #@0
    .prologue
    .line 241
    const/4 v1, -0x1

    #@1
    .line 242
    .local v1, visibility:I
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@4
    move-result-object v2

    #@5
    if-eqz v2, :cond_f

    #@7
    .line 244
    :try_start_7
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@a
    move-result-object v2

    #@b
    invoke-interface {v2}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->getVisibility()I
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_e} :catch_10

    #@e
    move-result v1

    #@f
    .line 249
    :cond_f
    :goto_f
    return v1

    #@10
    .line 245
    :catch_10
    move-exception v0

    #@11
    .line 246
    .local v0, e1:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@14
    goto :goto_f
.end method

.method public hideCliptray()V
    .registers 3

    #@0
    .prologue
    .line 157
    iget-object v1, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mContext:Landroid/content/Context;

    #@2
    if-eqz v1, :cond_a

    #@4
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@7
    move-result-object v1

    #@8
    if-nez v1, :cond_b

    #@a
    .line 166
    :cond_a
    :goto_a
    return-void

    #@b
    .line 162
    :cond_b
    :try_start_b
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@e
    move-result-object v1

    #@f
    invoke-interface {v1}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->hideCliptraycue()V
    :try_end_12
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_12} :catch_13

    #@12
    goto :goto_a

    #@13
    .line 163
    :catch_13
    move-exception v0

    #@14
    .line 164
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@17
    goto :goto_a
.end method

.method public hideCliptraycue()V
    .registers 3

    #@0
    .prologue
    .line 309
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@3
    move-result-object v1

    #@4
    if-eqz v1, :cond_d

    #@6
    .line 311
    :try_start_6
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@9
    move-result-object v1

    #@a
    invoke-interface {v1}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->hideCliptraycue()V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_d} :catch_e

    #@d
    .line 316
    :cond_d
    :goto_d
    return-void

    #@e
    .line 312
    :catch_e
    move-exception v0

    #@f
    .line 313
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@12
    goto :goto_d
.end method

.method public insertImage(Landroid/graphics/Bitmap;Z)Landroid/net/Uri;
    .registers 5
    .parameter "bitmap"
    .parameter "isPNG"

    #@0
    .prologue
    .line 967
    invoke-direct {p0, p2}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->generateFileName(Z)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 968
    .local v0, filename:Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getOutputUri(Ljava/lang/String;)Landroid/net/Uri;

    #@7
    move-result-object v1

    #@8
    .line 970
    .local v1, newUri:Landroid/net/Uri;
    if-eqz v1, :cond_d

    #@a
    .line 971
    invoke-direct {p0, p1, v1, p2}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->saveBitmap(Landroid/graphics/Bitmap;Landroid/net/Uri;Z)V

    #@d
    .line 973
    :cond_d
    return-object v1
.end method

.method public isCliptraycueShowing()Z
    .registers 4

    #@0
    .prologue
    .line 320
    const/4 v1, 0x0

    #@1
    .line 321
    .local v1, isShowing:Z
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@4
    move-result-object v2

    #@5
    if-eqz v2, :cond_f

    #@7
    .line 323
    :try_start_7
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@a
    move-result-object v2

    #@b
    invoke-interface {v2}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->isCliptraycueShowing()Z
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_e} :catch_10

    #@e
    move-result v1

    #@f
    .line 328
    :cond_f
    :goto_f
    return v1

    #@10
    .line 324
    :catch_10
    move-exception v0

    #@11
    .line 325
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@14
    goto :goto_f
.end method

.method public isServiceConnected()Z
    .registers 3

    #@0
    .prologue
    .line 357
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@3
    move-result-object v1

    #@4
    if-eqz v1, :cond_10

    #@6
    .line 359
    :try_start_6
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@9
    move-result-object v1

    #@a
    invoke-interface {v1}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->getServiceConnected()Z

    #@d
    move-result v1

    #@e
    iput-boolean v1, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mConnected:Z
    :try_end_10
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_10} :catch_13

    #@10
    .line 364
    :cond_10
    :goto_10
    iget-boolean v1, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mConnected:Z

    #@12
    return v1

    #@13
    .line 360
    :catch_13
    move-exception v0

    #@14
    .line 361
    .local v0, e1:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@17
    goto :goto_10
.end method

.method public orientationChanged(Z)V
    .registers 4
    .parameter "portrait"

    #@0
    .prologue
    .line 346
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@3
    move-result-object v1

    #@4
    if-eqz v1, :cond_d

    #@6
    .line 348
    :try_start_6
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@9
    move-result-object v1

    #@a
    invoke-interface {v1, p1}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->orientationChanged(Z)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_d} :catch_e

    #@d
    .line 353
    :cond_d
    :goto_d
    return-void

    #@e
    .line 349
    :catch_e
    move-exception v0

    #@f
    .line 350
    .local v0, e1:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@12
    goto :goto_d
.end method

.method public removePasteListener(Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;)V
    .registers 5
    .parameter "what"

    #@0
    .prologue
    .line 271
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@3
    move-result-object v0

    #@4
    if-eqz v0, :cond_20

    #@6
    .line 272
    iget-object v1, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mPasteListeners:Ljava/util/ArrayList;

    #@8
    monitor-enter v1

    #@9
    .line 273
    :try_start_9
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mPasteListeners:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@e
    .line 274
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mPasteListeners:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
    :try_end_13
    .catchall {:try_start_9 .. :try_end_13} :catchall_21

    #@13
    move-result v0

    #@14
    if-nez v0, :cond_1f

    #@16
    .line 276
    :try_start_16
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@19
    move-result-object v0

    #@1a
    iget-object v2, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mPasteServiceListener:Lcom/lge/systemservice/core/cliptraymanager/IOnPasteListener$Stub;

    #@1c
    invoke-interface {v0, v2}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->removePasteListener(Lcom/lge/systemservice/core/cliptraymanager/IOnPasteListener;)V
    :try_end_1f
    .catchall {:try_start_16 .. :try_end_1f} :catchall_21
    .catch Landroid/os/RemoteException; {:try_start_16 .. :try_end_1f} :catch_24

    #@1f
    .line 281
    :cond_1f
    :goto_1f
    :try_start_1f
    monitor-exit v1

    #@20
    .line 283
    :cond_20
    return-void

    #@21
    .line 281
    :catchall_21
    move-exception v0

    #@22
    monitor-exit v1
    :try_end_23
    .catchall {:try_start_1f .. :try_end_23} :catchall_21

    #@23
    throw v0

    #@24
    .line 277
    :catch_24
    move-exception v0

    #@25
    goto :goto_1f
.end method

.method public setInputType(I)V
    .registers 6
    .parameter "type"

    #@0
    .prologue
    .line 333
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@3
    move-result-object v1

    #@4
    if-eqz v1, :cond_27

    #@6
    .line 335
    :try_start_6
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@9
    move-result-object v1

    #@a
    invoke-interface {v1, p1}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->setInputType(I)V

    #@d
    .line 336
    iput p1, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mCurrentInputType:I

    #@f
    .line 337
    const-string v1, "Cliptray Manager"

    #@11
    new-instance v2, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v3, "setInputType : "

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_27
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_27} :catch_28

    #@27
    .line 342
    :cond_27
    :goto_27
    return-void

    #@28
    .line 338
    :catch_28
    move-exception v0

    #@29
    .line 339
    .local v0, e1:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@2c
    goto :goto_27
.end method

.method public setPasteListener(Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;)V
    .registers 5
    .parameter "what"

    #@0
    .prologue
    .line 254
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@3
    move-result-object v0

    #@4
    if-eqz v0, :cond_25

    #@6
    .line 255
    iget-object v1, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mPasteListeners:Ljava/util/ArrayList;

    #@8
    monitor-enter v1

    #@9
    .line 256
    :try_start_9
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mPasteListeners:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@e
    move-result v0

    #@f
    if-eqz v0, :cond_16

    #@11
    .line 257
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mPasteListeners:Ljava/util/ArrayList;

    #@13
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
    :try_end_16
    .catchall {:try_start_9 .. :try_end_16} :catchall_26

    #@16
    .line 260
    :cond_16
    :try_start_16
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@19
    move-result-object v0

    #@1a
    iget-object v2, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mPasteServiceListener:Lcom/lge/systemservice/core/cliptraymanager/IOnPasteListener$Stub;

    #@1c
    invoke-interface {v0, v2}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->setPasteListener(Lcom/lge/systemservice/core/cliptraymanager/IOnPasteListener;)V
    :try_end_1f
    .catchall {:try_start_16 .. :try_end_1f} :catchall_26
    .catch Landroid/os/RemoteException; {:try_start_16 .. :try_end_1f} :catch_29

    #@1f
    .line 264
    :goto_1f
    :try_start_1f
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mPasteListeners:Ljava/util/ArrayList;

    #@21
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@24
    .line 265
    monitor-exit v1

    #@25
    .line 267
    :cond_25
    return-void

    #@26
    .line 265
    :catchall_26
    move-exception v0

    #@27
    monitor-exit v1
    :try_end_28
    .catchall {:try_start_1f .. :try_end_28} :catchall_26

    #@28
    throw v0

    #@29
    .line 261
    :catch_29
    move-exception v0

    #@2a
    goto :goto_1f
.end method

.method public showCliptray()V
    .registers 4

    #@0
    .prologue
    .line 140
    iget-object v1, p0, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->mContext:Landroid/content/Context;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 153
    :goto_4
    return-void

    #@5
    .line 144
    :cond_5
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@8
    move-result-object v1

    #@9
    if-eqz v1, :cond_18

    #@b
    .line 146
    :try_start_b
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@e
    move-result-object v1

    #@f
    invoke-interface {v1}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->showCliptraycueClose()V
    :try_end_12
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_12} :catch_13

    #@12
    goto :goto_4

    #@13
    .line 147
    :catch_13
    move-exception v0

    #@14
    .line 148
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@17
    goto :goto_4

    #@18
    .line 151
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_18
    const-string v1, "Cliptray Manager"

    #@1a
    const-string v2, "Cliptray Service Connection is null"

    #@1c
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    goto :goto_4
.end method

.method public showCliptraycue()V
    .registers 3

    #@0
    .prologue
    .line 287
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@3
    move-result-object v1

    #@4
    if-eqz v1, :cond_d

    #@6
    .line 289
    :try_start_6
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@9
    move-result-object v1

    #@a
    invoke-interface {v1}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->showCliptraycue()V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_d} :catch_e

    #@d
    .line 294
    :cond_d
    :goto_d
    return-void

    #@e
    .line 290
    :catch_e
    move-exception v0

    #@f
    .line 291
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@12
    goto :goto_d
.end method

.method public showCliptraycueClose()V
    .registers 3

    #@0
    .prologue
    .line 298
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@3
    move-result-object v1

    #@4
    if-eqz v1, :cond_d

    #@6
    .line 300
    :try_start_6
    invoke-static {}, Lcom/lge/systemservice/core/cliptraymanager/CliptrayManager;->getService()Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@9
    move-result-object v1

    #@a
    invoke-interface {v1}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;->showCliptraycueClose()V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_d} :catch_e

    #@d
    .line 305
    :cond_d
    :goto_d
    return-void

    #@e
    .line 301
    :catch_e
    move-exception v0

    #@f
    .line 302
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@12
    goto :goto_d
.end method
