.class Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager$Stub$Proxy;
.super Ljava/lang/Object;
.source "INfcLgManager.java"

# interfaces
.implements Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 86
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 87
    iput-object p1, p0, Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 88
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 91
    iget-object v0, p0, Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public createNfcFactoryObj()Z
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 119
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 120
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 123
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.lge.systemservice.core.nfclgmanager.INfcLgManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 124
    iget-object v3, p0, Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@10
    const/4 v4, 0x2

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 125
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 126
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1b
    .catchall {:try_start_9 .. :try_end_1b} :catchall_26

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_1f

    #@1e
    const/4 v2, 0x1

    #@1f
    .line 129
    .local v2, _result:Z
    :cond_1f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 130
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 132
    return v2

    #@26
    .line 129
    .end local v2           #_result:Z
    :catchall_26
    move-exception v3

    #@27
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 130
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    throw v3
.end method

.method public handleNfcFactory(I[B)Ljava/lang/String;
    .registers 9
    .parameter "command"
    .parameter "retData"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 99
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 100
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 103
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.systemservice.core.nfclgmanager.INfcLgManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 104
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 105
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeByteArray([B)V

    #@13
    .line 106
    iget-object v3, p0, Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/4 v4, 0x1

    #@16
    const/4 v5, 0x0

    #@17
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1a
    .line 107
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1d
    .line 108
    invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    .line 109
    .local v2, _result:Ljava/lang/String;
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->readByteArray([B)V
    :try_end_24
    .catchall {:try_start_8 .. :try_end_24} :catchall_2b

    #@24
    .line 112
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 113
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 115
    return-object v2

    #@2b
    .line 112
    .end local v2           #_result:Ljava/lang/String;
    :catchall_2b
    move-exception v3

    #@2c
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 113
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@32
    throw v3
.end method

.method public sendNfcTestCommand(I[B)Z
    .registers 9
    .parameter "command"
    .parameter "response"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 136
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 137
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 140
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.lge.systemservice.core.nfclgmanager.INfcLgManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 141
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 142
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeByteArray([B)V

    #@14
    .line 143
    iget-object v3, p0, Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/4 v4, 0x3

    #@17
    const/4 v5, 0x0

    #@18
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 144
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1e
    .line 145
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@21
    move-result v3

    #@22
    if-eqz v3, :cond_25

    #@24
    const/4 v2, 0x1

    #@25
    .line 146
    .local v2, _result:Z
    :cond_25
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->readByteArray([B)V
    :try_end_28
    .catchall {:try_start_9 .. :try_end_28} :catchall_2f

    #@28
    .line 149
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 150
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 152
    return v2

    #@2f
    .line 149
    .end local v2           #_result:Z
    :catchall_2f
    move-exception v3

    #@30
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@33
    .line 150
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@36
    throw v3
.end method
