.class public Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;
.super Ljava/lang/Object;
.source "ClipTrayWindow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnNoContent;,
        Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnDeleteMode;,
        Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnPasteInterface;
    }
.end annotation


# static fields
.field private static mCell_norm_land_height:I

.field private static mCell_norm_land_width:I

.field private static mCell_norm_port_height:I

.field private static mCell_norm_port_width:I


# instance fields
.field private clipManager:Landroid/content/ClipboardManager;

.field private cliptrayHeight:I

.field private cliptrayView:Landroid/widget/LinearLayout;

.field private cliptrayWidth:I

.field private current_paste_time:J

.field private displayPortHeight:I

.field private displayPortWidth:I

.field private doCopyAnimation:Z

.field private duration:J

.field private gridlayout:Landroid/widget/GridLayout;

.field handler:Landroid/os/Handler;

.field private inputType:I

.field private isAfterPaste:Z

.field private isDeleteMode:Z

.field private isPortraitMode:Z

.field private last_paste_time:J

.field private mAnimSet:Landroid/view/animation/AnimationSet;

.field private mAnimView:Landroid/widget/ImageView;

.field private mContext:Landroid/content/Context;

.field private mHScroll:Landroid/widget/HorizontalScrollView;

.field private mIOnDeleteMode:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnDeleteMode;

.field private mIOnNoContent:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnNoContent;

.field private mIOnPasteInterface:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnPasteInterface;

.field private mNoContentLandView:Landroid/view/View;

.field private mNoContentPortView:Landroid/view/View;

.field private mRes:Landroid/content/res/Resources;

.field private onClipItemClick:Landroid/view/View$OnClickListener;

.field private onClipItemLongClick:Landroid/view/View$OnLongClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 10
    .parameter "context"

    #@0
    .prologue
    const-wide/16 v5, 0x0

    #@2
    const/4 v4, 0x1

    #@3
    const/4 v3, 0x0

    #@4
    const/4 v7, 0x0

    #@5
    .line 119
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@8
    .line 66
    iput-boolean v3, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->isDeleteMode:Z

    #@a
    .line 67
    iput-boolean v4, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->isPortraitMode:Z

    #@c
    .line 68
    iput-boolean v3, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->isAfterPaste:Z

    #@e
    .line 73
    iput v3, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->inputType:I

    #@10
    .line 77
    iput-object v7, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mAnimView:Landroid/widget/ImageView;

    #@12
    .line 78
    iput-object v7, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mAnimSet:Landroid/view/animation/AnimationSet;

    #@14
    .line 81
    iput-object v7, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mNoContentLandView:Landroid/view/View;

    #@16
    .line 82
    iput-object v7, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mNoContentPortView:Landroid/view/View;

    #@18
    .line 84
    iput-boolean v3, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->doCopyAnimation:Z

    #@1a
    .line 86
    iput-wide v5, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->duration:J

    #@1c
    .line 87
    iput-wide v5, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->current_paste_time:J

    #@1e
    .line 88
    const-wide/16 v5, -0x1

    #@20
    iput-wide v5, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->last_paste_time:J

    #@22
    .line 91
    iput-object v7, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mIOnPasteInterface:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnPasteInterface;

    #@24
    .line 100
    iput-object v7, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mIOnDeleteMode:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnDeleteMode;

    #@26
    .line 109
    iput-object v7, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mIOnNoContent:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnNoContent;

    #@28
    .line 219
    new-instance v2, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;

    #@2a
    invoke-direct {v2, p0}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;-><init>(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)V

    #@2d
    iput-object v2, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->onClipItemClick:Landroid/view/View$OnClickListener;

    #@2f
    .line 364
    new-instance v2, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$2;

    #@31
    invoke-direct {v2, p0}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$2;-><init>(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)V

    #@34
    iput-object v2, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->onClipItemLongClick:Landroid/view/View$OnLongClickListener;

    #@36
    .line 824
    new-instance v2, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$3;

    #@38
    invoke-direct {v2, p0}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$3;-><init>(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)V

    #@3b
    iput-object v2, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->handler:Landroid/os/Handler;

    #@3d
    .line 121
    iput-object p1, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mContext:Landroid/content/Context;

    #@3f
    .line 122
    iget-object v2, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mContext:Landroid/content/Context;

    #@41
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@44
    move-result-object v2

    #@45
    iput-object v2, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mRes:Landroid/content/res/Resources;

    #@47
    .line 123
    iget-object v2, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mContext:Landroid/content/Context;

    #@49
    const-string v5, "clipboard"

    #@4b
    invoke-virtual {v2, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@4e
    move-result-object v2

    #@4f
    check-cast v2, Landroid/content/ClipboardManager;

    #@51
    iput-object v2, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->clipManager:Landroid/content/ClipboardManager;

    #@53
    .line 125
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@56
    move-result-object v2

    #@57
    const v5, 0x2030005

    #@5a
    invoke-virtual {v2, v5, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@5d
    move-result-object v2

    #@5e
    check-cast v2, Landroid/widget/LinearLayout;

    #@60
    iput-object v2, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->cliptrayView:Landroid/widget/LinearLayout;

    #@62
    .line 127
    iget-object v2, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->cliptrayView:Landroid/widget/LinearLayout;

    #@64
    const v5, 0x20d0043

    #@67
    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    #@6a
    move-result-object v2

    #@6b
    iput-object v2, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mNoContentPortView:Landroid/view/View;

    #@6d
    .line 128
    iget-object v2, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->cliptrayView:Landroid/widget/LinearLayout;

    #@6f
    const v5, 0x20d0045

    #@72
    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    #@75
    move-result-object v2

    #@76
    iput-object v2, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mNoContentLandView:Landroid/view/View;

    #@78
    .line 130
    iget-object v2, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->cliptrayView:Landroid/widget/LinearLayout;

    #@7a
    const v5, 0x20d0042

    #@7d
    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    #@80
    move-result-object v2

    #@81
    check-cast v2, Landroid/widget/GridLayout;

    #@83
    iput-object v2, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->gridlayout:Landroid/widget/GridLayout;

    #@85
    .line 131
    iget-object v2, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->cliptrayView:Landroid/widget/LinearLayout;

    #@87
    const v5, 0x20d0040

    #@8a
    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    #@8d
    move-result-object v2

    #@8e
    check-cast v2, Landroid/widget/HorizontalScrollView;

    #@90
    iput-object v2, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mHScroll:Landroid/widget/HorizontalScrollView;

    #@92
    .line 134
    new-instance v0, Landroid/util/DisplayMetrics;

    #@94
    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    #@97
    .line 135
    .local v0, metrics:Landroid/util/DisplayMetrics;
    iget-object v2, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mContext:Landroid/content/Context;

    #@99
    const-string v5, "window"

    #@9b
    invoke-virtual {v2, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@9e
    move-result-object v1

    #@9f
    check-cast v1, Landroid/view/WindowManager;

    #@a1
    .line 136
    .local v1, wm:Landroid/view/WindowManager;
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    #@a4
    move-result-object v2

    #@a5
    invoke-virtual {v2, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    #@a8
    .line 139
    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    #@aa
    iget v5, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    #@ac
    if-le v2, v5, :cond_c1

    #@ae
    move v2, v3

    #@af
    :goto_af
    iput-boolean v2, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->isPortraitMode:Z

    #@b1
    .line 140
    iget-boolean v2, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->isPortraitMode:Z

    #@b3
    if-eqz v2, :cond_c3

    #@b5
    .line 141
    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    #@b7
    iput v2, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->displayPortWidth:I

    #@b9
    .line 142
    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    #@bb
    iput v2, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->displayPortHeight:I

    #@bd
    .line 147
    :goto_bd
    invoke-direct {p0}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->init_cell_sizes()V

    #@c0
    .line 148
    return-void

    #@c1
    :cond_c1
    move v2, v4

    #@c2
    .line 139
    goto :goto_af

    #@c3
    .line 144
    :cond_c3
    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    #@c5
    iput v2, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->displayPortWidth:I

    #@c7
    .line 145
    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    #@c9
    iput v2, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->displayPortHeight:I

    #@cb
    goto :goto_bd
.end method

.method static synthetic access$000(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->isDeleteMode:Z

    #@2
    return v0
.end method

.method static synthetic access$002(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->isDeleteMode:Z

    #@2
    return p1
.end method

.method static synthetic access$100(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Landroid/widget/GridLayout;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 47
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->gridlayout:Landroid/widget/GridLayout;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 47
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mNoContentLandView:Landroid/view/View;

    #@2
    return-object v0
.end method

.method static synthetic access$1100(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnNoContent;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 47
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mIOnNoContent:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnNoContent;

    #@2
    return-object v0
.end method

.method static synthetic access$1200(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnPasteInterface;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 47
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mIOnPasteInterface:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnPasteInterface;

    #@2
    return-object v0
.end method

.method static synthetic access$1300(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 47
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1400(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 47
    iget v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->inputType:I

    #@2
    return v0
.end method

.method static synthetic access$1502(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->isAfterPaste:Z

    #@2
    return p1
.end method

.method static synthetic access$1600(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Landroid/content/res/Resources;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 47
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mRes:Landroid/content/res/Resources;

    #@2
    return-object v0
.end method

.method static synthetic access$1700(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnDeleteMode;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 47
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mIOnDeleteMode:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnDeleteMode;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Landroid/content/ClipboardManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 47
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->clipManager:Landroid/content/ClipboardManager;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;I)Ljava/util/ArrayList;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->getImageList(I)Ljava/util/ArrayList;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$400(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 47
    invoke-direct {p0}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->removeFiles()V

    #@3
    return-void
.end method

.method static synthetic access$500(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 47
    invoke-direct {p0}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->updateClipboard()V

    #@3
    return-void
.end method

.method static synthetic access$600(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Landroid/widget/HorizontalScrollView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 47
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mHScroll:Landroid/widget/HorizontalScrollView;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->isPortraitMode:Z

    #@2
    return v0
.end method

.method static synthetic access$800(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Landroid/widget/LinearLayout;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 47
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->cliptrayView:Landroid/widget/LinearLayout;

    #@2
    return-object v0
.end method

.method static synthetic access$900(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 47
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mNoContentPortView:Landroid/view/View;

    #@2
    return-object v0
.end method

.method private getCellView(ILjava/lang/String;Landroid/net/Uri;)Landroid/view/View;
    .registers 21
    .parameter "datatype"
    .parameter "text"
    .parameter "imgUri"

    #@0
    .prologue
    .line 489
    packed-switch p1, :pswitch_data_d8

    #@3
    .line 524
    const/4 v9, 0x0

    #@4
    .line 530
    :goto_4
    return-object v9

    #@5
    .line 491
    :pswitch_5
    move-object/from16 v0, p0

    #@7
    iget-object v14, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mContext:Landroid/content/Context;

    #@9
    invoke-static {v14}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@c
    move-result-object v14

    #@d
    const v15, 0x2030009

    #@10
    const/16 v16, 0x0

    #@12
    invoke-virtual/range {v14 .. v16}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@15
    move-result-object v9

    #@16
    .line 492
    .local v9, childview:Landroid/view/View;
    new-instance v14, Landroid/view/ViewGroup$LayoutParams;

    #@18
    const/16 v15, 0x64

    #@1a
    const/16 v16, 0x64

    #@1c
    invoke-direct/range {v14 .. v16}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@1f
    invoke-virtual {v9, v14}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@22
    .line 494
    const v14, 0x20d004b

    #@25
    invoke-virtual {v9, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@28
    move-result-object v8

    #@29
    check-cast v8, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;

    #@2b
    .line 495
    .local v8, cellTextPort:Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;
    move/from16 v0, p1

    #@2d
    move-object/from16 v1, p2

    #@2f
    invoke-virtual {v8, v0, v1}, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->setChildData(ILjava/lang/String;)V

    #@32
    .line 497
    const v14, 0x20d004d

    #@35
    invoke-virtual {v9, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@38
    move-result-object v7

    #@39
    check-cast v7, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;

    #@3b
    .line 498
    .local v7, cellTextLand:Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;
    move/from16 v0, p1

    #@3d
    move-object/from16 v1, p2

    #@3f
    invoke-virtual {v7, v0, v1}, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->setChildData(ILjava/lang/String;)V

    #@42
    .line 527
    .end local v7           #cellTextLand:Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;
    .end local v8           #cellTextPort:Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;
    :cond_42
    move-object/from16 v0, p0

    #@44
    iget-object v14, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->onClipItemClick:Landroid/view/View$OnClickListener;

    #@46
    invoke-virtual {v9, v14}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@49
    .line 528
    move-object/from16 v0, p0

    #@4b
    iget-object v14, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->onClipItemLongClick:Landroid/view/View$OnLongClickListener;

    #@4d
    invoke-virtual {v9, v14}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    #@50
    goto :goto_4

    #@51
    .line 501
    .end local v9           #childview:Landroid/view/View;
    :pswitch_51
    move-object/from16 v0, p0

    #@53
    iget-object v14, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mContext:Landroid/content/Context;

    #@55
    invoke-static {v14}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@58
    move-result-object v14

    #@59
    const v15, 0x2030008

    #@5c
    const/16 v16, 0x0

    #@5e
    invoke-virtual/range {v14 .. v16}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@61
    move-result-object v9

    #@62
    .line 502
    .restart local v9       #childview:Landroid/view/View;
    const v14, 0x20d004b

    #@65
    invoke-virtual {v9, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@68
    move-result-object v6

    #@69
    check-cast v6, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;

    #@6b
    .line 503
    .local v6, cellImagePort:Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;
    move/from16 v0, p1

    #@6d
    move-object/from16 v1, p3

    #@6f
    invoke-virtual {v6, v0, v1}, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->setChildData(ILandroid/net/Uri;)Z

    #@72
    move-result v12

    #@73
    .line 505
    .local v12, successPort:Z
    const v14, 0x20d004d

    #@76
    invoke-virtual {v9, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@79
    move-result-object v5

    #@7a
    check-cast v5, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;

    #@7c
    .line 506
    .local v5, cellImageLand:Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;
    move/from16 v0, p1

    #@7e
    move-object/from16 v1, p3

    #@80
    invoke-virtual {v5, v0, v1}, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->setChildData(ILandroid/net/Uri;)Z

    #@83
    move-result v10

    #@84
    .line 507
    .local v10, successLand:Z
    if-eqz v12, :cond_88

    #@86
    if-nez v10, :cond_42

    #@88
    .line 508
    :cond_88
    const-string v14, "Cliptraywindow"

    #@8a
    const-string v15, "getCellView: cannot load image, view is removed!!"

    #@8c
    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8f
    .line 509
    const/4 v9, 0x0

    #@90
    goto/16 :goto_4

    #@92
    .line 513
    .end local v5           #cellImageLand:Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;
    .end local v6           #cellImagePort:Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;
    .end local v9           #childview:Landroid/view/View;
    .end local v10           #successLand:Z
    .end local v12           #successPort:Z
    :pswitch_92
    move-object/from16 v0, p0

    #@94
    iget-object v14, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mContext:Landroid/content/Context;

    #@96
    invoke-static {v14}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@99
    move-result-object v14

    #@9a
    const v15, 0x2030007

    #@9d
    const/16 v16, 0x0

    #@9f
    invoke-virtual/range {v14 .. v16}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@a2
    move-result-object v9

    #@a3
    .line 514
    .restart local v9       #childview:Landroid/view/View;
    const v14, 0x20d004b

    #@a6
    invoke-virtual {v9, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@a9
    move-result-object v4

    #@aa
    check-cast v4, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;

    #@ac
    .line 515
    .local v4, cellComplexPort:Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;
    move/from16 v0, p1

    #@ae
    move-object/from16 v1, p2

    #@b0
    move-object/from16 v2, p3

    #@b2
    invoke-virtual {v4, v0, v1, v2}, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->setChildData(ILjava/lang/String;Landroid/net/Uri;)Z

    #@b5
    move-result v13

    #@b6
    .line 517
    .local v13, successPort2:Z
    const v14, 0x20d004d

    #@b9
    invoke-virtual {v9, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@bc
    move-result-object v3

    #@bd
    check-cast v3, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;

    #@bf
    .line 518
    .local v3, cellComplexLand:Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;
    move/from16 v0, p1

    #@c1
    move-object/from16 v1, p2

    #@c3
    move-object/from16 v2, p3

    #@c5
    invoke-virtual {v3, v0, v1, v2}, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->setChildData(ILjava/lang/String;Landroid/net/Uri;)Z

    #@c8
    move-result v11

    #@c9
    .line 519
    .local v11, successLand2:Z
    if-eqz v13, :cond_cd

    #@cb
    if-nez v11, :cond_42

    #@cd
    .line 520
    :cond_cd
    const-string v14, "Cliptraywindow"

    #@cf
    const-string v15, "getCellView: cannot load image, view is removed!!"

    #@d1
    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d4
    .line 521
    const/4 v9, 0x0

    #@d5
    goto/16 :goto_4

    #@d7
    .line 489
    nop

    #@d8
    :pswitch_data_d8
    .packed-switch 0x0
        :pswitch_5
        :pswitch_51
        :pswitch_92
    .end packed-switch
.end method

.method private getImageList(I)Ljava/util/ArrayList;
    .registers 6
    .parameter "index"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 351
    new-instance v2, Ljava/util/ArrayList;

    #@2
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 352
    .local v2, lists:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/net/Uri;>;"
    iget-object v3, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->clipManager:Landroid/content/ClipboardManager;

    #@7
    invoke-virtual {v3, p1}, Landroid/content/ClipboardManager;->getPrimaryClipAt(I)Landroid/content/ClipData;

    #@a
    move-result-object v0

    #@b
    .line 354
    .local v0, clip:Landroid/content/ClipData;
    if-eqz v0, :cond_2c

    #@d
    .line 355
    const/4 v1, 0x0

    #@e
    .local v1, i:I
    :goto_e
    invoke-virtual {v0}, Landroid/content/ClipData;->getItemCount()I

    #@11
    move-result v3

    #@12
    if-ge v1, v3, :cond_2c

    #@14
    .line 356
    invoke-virtual {v0, v1}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v3}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    #@1b
    move-result-object v3

    #@1c
    if-eqz v3, :cond_29

    #@1e
    .line 357
    invoke-virtual {v0, v1}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v3}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@29
    .line 355
    :cond_29
    add-int/lit8 v1, v1, 0x1

    #@2b
    goto :goto_e

    #@2c
    .line 361
    .end local v1           #i:I
    :cond_2c
    return-object v2
.end method

.method private init()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 195
    iget-boolean v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->isPortraitMode:Z

    #@3
    if-eqz v0, :cond_5b

    #@5
    iget v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->displayPortWidth:I

    #@7
    :goto_7
    iput v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->cliptrayWidth:I

    #@9
    .line 196
    iget-boolean v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->isPortraitMode:Z

    #@b
    if-eqz v0, :cond_60

    #@d
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mRes:Landroid/content/res/Resources;

    #@f
    const v1, 0x20c004b

    #@12
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@15
    move-result v0

    #@16
    :goto_16
    iput v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->cliptrayHeight:I

    #@18
    .line 199
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->cliptrayView:Landroid/widget/LinearLayout;

    #@1a
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    #@1c
    iget v2, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->cliptrayWidth:I

    #@1e
    iget v3, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->cliptrayHeight:I

    #@20
    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    #@23
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@26
    .line 200
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->gridlayout:Landroid/widget/GridLayout;

    #@28
    iget v1, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->cliptrayWidth:I

    #@2a
    invoke-virtual {v0, v1}, Landroid/widget/GridLayout;->setMinimumWidth(I)V

    #@2d
    .line 202
    iget-boolean v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->isPortraitMode:Z

    #@2f
    if-eqz v0, :cond_6a

    #@31
    .line 203
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->gridlayout:Landroid/widget/GridLayout;

    #@33
    const/4 v1, 0x2

    #@34
    invoke-virtual {v0, v1}, Landroid/widget/GridLayout;->setRowCount(I)V

    #@37
    .line 204
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->cliptrayView:Landroid/widget/LinearLayout;

    #@39
    iget-object v1, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mRes:Landroid/content/res/Resources;

    #@3b
    const v2, 0x20c004f

    #@3e
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@41
    move-result v1

    #@42
    iget-object v2, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mRes:Landroid/content/res/Resources;

    #@44
    const v3, 0x20c004d

    #@47
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@4a
    move-result v2

    #@4b
    iget-object v3, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mRes:Landroid/content/res/Resources;

    #@4d
    const v4, 0x20c004e

    #@50
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@53
    move-result v3

    #@54
    invoke-virtual {v0, v1, v2, v5, v3}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    #@57
    .line 216
    :goto_57
    invoke-direct {p0}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->updateClipboard()V

    #@5a
    .line 217
    return-void

    #@5b
    .line 195
    :cond_5b
    iget v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->displayPortHeight:I

    #@5d
    add-int/lit8 v0, v0, 0xf

    #@5f
    goto :goto_7

    #@60
    .line 196
    :cond_60
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mRes:Landroid/content/res/Resources;

    #@62
    const v1, 0x20c004c

    #@65
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@68
    move-result v0

    #@69
    goto :goto_16

    #@6a
    .line 208
    :cond_6a
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->gridlayout:Landroid/widget/GridLayout;

    #@6c
    const/4 v1, 0x1

    #@6d
    invoke-virtual {v0, v1}, Landroid/widget/GridLayout;->setRowCount(I)V

    #@70
    .line 209
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->cliptrayView:Landroid/widget/LinearLayout;

    #@72
    iget-object v1, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mRes:Landroid/content/res/Resources;

    #@74
    const v2, 0x20c0057

    #@77
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@7a
    move-result v1

    #@7b
    iget-object v2, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mRes:Landroid/content/res/Resources;

    #@7d
    const v3, 0x20c0055

    #@80
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@83
    move-result v2

    #@84
    iget-object v3, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mRes:Landroid/content/res/Resources;

    #@86
    const v4, 0x20c0056

    #@89
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@8c
    move-result v3

    #@8d
    invoke-virtual {v0, v1, v2, v5, v3}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    #@90
    goto :goto_57
.end method

.method private init_cell_sizes()V
    .registers 3

    #@0
    .prologue
    .line 186
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mRes:Landroid/content/res/Resources;

    #@2
    const v1, 0x20c005d

    #@5
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@8
    move-result v0

    #@9
    sput v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mCell_norm_port_width:I

    #@b
    .line 187
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mRes:Landroid/content/res/Resources;

    #@d
    const v1, 0x20c005c

    #@10
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@13
    move-result v0

    #@14
    sput v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mCell_norm_port_height:I

    #@16
    .line 189
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mRes:Landroid/content/res/Resources;

    #@18
    const v1, 0x20c005f

    #@1b
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@1e
    move-result v0

    #@1f
    sput v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mCell_norm_land_width:I

    #@21
    .line 190
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mRes:Landroid/content/res/Resources;

    #@23
    const v1, 0x20c005e

    #@26
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@29
    move-result v0

    #@2a
    sput v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mCell_norm_land_height:I

    #@2c
    .line 191
    return-void
.end method

.method private removeFiles()V
    .registers 9

    #@0
    .prologue
    .line 805
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    #@3
    move-result-object v6

    #@4
    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@7
    move-result-object v5

    #@8
    .line 806
    .local v5, path:Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v6

    #@11
    const-string v7, "/Android/data"

    #@13
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v6

    #@17
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v5

    #@1b
    .line 807
    new-instance v6, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v6

    #@24
    const-string v7, "/.cliptray"

    #@26
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v6

    #@2a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v5

    #@2e
    .line 809
    new-instance v1, Ljava/io/File;

    #@30
    invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@33
    .line 811
    .local v1, dir:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    #@36
    move-result-object v0

    #@37
    .line 812
    .local v0, child:[Ljava/lang/String;
    if-eqz v0, :cond_61

    #@39
    .line 813
    const/4 v4, 0x0

    #@3a
    .local v4, i:I
    :goto_3a
    array-length v6, v0

    #@3b
    if-ge v4, v6, :cond_61

    #@3d
    .line 814
    aget-object v3, v0, v4

    #@3f
    .line 815
    .local v3, filename:Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    #@41
    new-instance v6, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v6

    #@4a
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v6

    #@4e
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v6

    #@52
    invoke-direct {v2, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@55
    .line 817
    .local v2, file:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    #@58
    move-result v6

    #@59
    if-eqz v6, :cond_5e

    #@5b
    .line 818
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    #@5e
    .line 813
    :cond_5e
    add-int/lit8 v4, v4, 0x1

    #@60
    goto :goto_3a

    #@61
    .line 822
    .end local v2           #file:Ljava/io/File;
    .end local v3           #filename:Ljava/lang/String;
    .end local v4           #i:I
    :cond_61
    return-void
.end method

.method private updateClipDataCellAt(I)V
    .registers 16
    .parameter "index"

    #@0
    .prologue
    const v13, 0x20d004a

    #@3
    const/high16 v12, 0x3f80

    #@5
    const v9, 0x3f666666

    #@8
    const/16 v11, 0x8

    #@a
    const/4 v10, 0x0

    #@b
    .line 394
    iget-object v7, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->gridlayout:Landroid/widget/GridLayout;

    #@d
    invoke-virtual {v7}, Landroid/widget/GridLayout;->getChildCount()I

    #@10
    move-result v1

    #@11
    .line 395
    .local v1, childCnt:I
    iget-object v7, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->clipManager:Landroid/content/ClipboardManager;

    #@13
    invoke-virtual {v7}, Landroid/content/ClipboardManager;->getPrimaryClipCount()I

    #@16
    move-result v3

    #@17
    .line 397
    .local v3, clipCnt:I
    if-ge p1, v1, :cond_1b

    #@19
    if-lt p1, v3, :cond_22

    #@1b
    .line 398
    :cond_1b
    const-string v7, "Cliptraywindow"

    #@1d
    const-string v8, "error: clip index exceed clip count!!"

    #@1f
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 402
    :cond_22
    iget-object v7, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->gridlayout:Landroid/widget/GridLayout;

    #@24
    invoke-virtual {v7, p1}, Landroid/widget/GridLayout;->getChildAt(I)Landroid/view/View;

    #@27
    move-result-object v2

    #@28
    .line 403
    .local v2, childview:Landroid/view/View;
    if-nez v2, :cond_49

    #@2a
    .line 404
    const-string v7, "Cliptraywindow"

    #@2c
    new-instance v8, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v9, "error: cannot get child view at = "

    #@33
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v8

    #@37
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v8

    #@3b
    const-string v9, " / failed to updateCliptray!!"

    #@3d
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v8

    #@41
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v8

    #@45
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 469
    :goto_48
    return-void

    #@49
    .line 409
    :cond_49
    iget-boolean v7, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->isDeleteMode:Z

    #@4b
    if-eqz v7, :cond_7e

    #@4d
    .line 411
    new-instance v4, Landroid/widget/GridLayout$LayoutParams;

    #@4f
    invoke-direct {v4}, Landroid/widget/GridLayout$LayoutParams;-><init>()V

    #@52
    .line 412
    .local v4, glp:Landroid/widget/GridLayout$LayoutParams;
    iget-boolean v7, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->isPortraitMode:Z

    #@54
    if-eqz v7, :cond_72

    #@56
    .line 413
    sget v7, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mCell_norm_port_width:I

    #@58
    iput v7, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@5a
    .line 414
    sget v7, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mCell_norm_port_height:I

    #@5c
    iput v7, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@5e
    .line 415
    invoke-virtual {v4, v10, v10, v10, v10}, Landroid/widget/GridLayout$LayoutParams;->setMargins(IIII)V

    #@61
    .line 421
    :goto_61
    invoke-virtual {v2, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@64
    .line 424
    invoke-virtual {v2, v9}, Landroid/view/View;->setScaleX(F)V

    #@67
    .line 425
    invoke-virtual {v2, v9}, Landroid/view/View;->setScaleY(F)V

    #@6a
    .line 428
    invoke-virtual {v2, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@6d
    move-result-object v0

    #@6e
    .line 429
    .local v0, btn_delete:Landroid/view/View;
    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    #@71
    goto :goto_48

    #@72
    .line 417
    .end local v0           #btn_delete:Landroid/view/View;
    :cond_72
    sget v7, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mCell_norm_land_width:I

    #@74
    iput v7, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@76
    .line 418
    sget v7, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mCell_norm_land_height:I

    #@78
    iput v7, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@7a
    .line 419
    invoke-virtual {v4, v10, v10, v10, v10}, Landroid/widget/GridLayout$LayoutParams;->setMargins(IIII)V

    #@7d
    goto :goto_61

    #@7e
    .line 432
    .end local v4           #glp:Landroid/widget/GridLayout$LayoutParams;
    :cond_7e
    new-instance v4, Landroid/widget/GridLayout$LayoutParams;

    #@80
    invoke-direct {v4}, Landroid/widget/GridLayout$LayoutParams;-><init>()V

    #@83
    .line 433
    .restart local v4       #glp:Landroid/widget/GridLayout$LayoutParams;
    iget-boolean v7, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->isPortraitMode:Z

    #@85
    if-eqz v7, :cond_d3

    #@87
    .line 434
    sget v7, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mCell_norm_port_width:I

    #@89
    iput v7, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@8b
    .line 435
    sget v7, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mCell_norm_port_height:I

    #@8d
    iput v7, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@8f
    .line 436
    iget-object v7, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mRes:Landroid/content/res/Resources;

    #@91
    const v8, 0x20c0051

    #@94
    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@97
    move-result v7

    #@98
    iget-object v8, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mRes:Landroid/content/res/Resources;

    #@9a
    const v9, 0x20c0050

    #@9d
    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@a0
    move-result v8

    #@a1
    invoke-virtual {v4, v10, v10, v7, v8}, Landroid/widget/GridLayout$LayoutParams;->setMargins(IIII)V

    #@a4
    .line 440
    const v7, 0x20d004b

    #@a7
    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@aa
    move-result-object v6

    #@ab
    check-cast v6, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;

    #@ad
    .line 441
    .local v6, portview:Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;
    invoke-virtual {v6, v10}, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->setVisibility(I)V

    #@b0
    .line 442
    iget v7, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->inputType:I

    #@b2
    invoke-virtual {v6, v7}, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->setDim(I)V

    #@b5
    .line 444
    const v7, 0x20d004d

    #@b8
    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@bb
    move-result-object v5

    #@bc
    check-cast v5, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;

    #@be
    .line 445
    .local v5, landview:Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;
    invoke-virtual {v5, v11}, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->setVisibility(I)V

    #@c1
    .line 459
    :goto_c1
    invoke-virtual {v2, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@c4
    .line 462
    invoke-virtual {v2, v12}, Landroid/view/View;->setScaleX(F)V

    #@c7
    .line 463
    invoke-virtual {v2, v12}, Landroid/view/View;->setScaleY(F)V

    #@ca
    .line 466
    invoke-virtual {v2, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@cd
    move-result-object v0

    #@ce
    .line 467
    .restart local v0       #btn_delete:Landroid/view/View;
    invoke-virtual {v0, v11}, Landroid/view/View;->setVisibility(I)V

    #@d1
    goto/16 :goto_48

    #@d3
    .line 447
    .end local v0           #btn_delete:Landroid/view/View;
    .end local v5           #landview:Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;
    .end local v6           #portview:Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;
    :cond_d3
    sget v7, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mCell_norm_land_width:I

    #@d5
    iput v7, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@d7
    .line 448
    sget v7, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mCell_norm_land_height:I

    #@d9
    iput v7, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@db
    .line 449
    iget-object v7, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mRes:Landroid/content/res/Resources;

    #@dd
    const v8, 0x20c0058

    #@e0
    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@e3
    move-result v7

    #@e4
    invoke-virtual {v4, v10, v10, v7, v10}, Landroid/widget/GridLayout$LayoutParams;->setMargins(IIII)V

    #@e7
    .line 452
    const v7, 0x20d004b

    #@ea
    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@ed
    move-result-object v6

    #@ee
    check-cast v6, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;

    #@f0
    .line 453
    .restart local v6       #portview:Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;
    invoke-virtual {v6, v11}, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->setVisibility(I)V

    #@f3
    .line 455
    const v7, 0x20d004d

    #@f6
    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@f9
    move-result-object v5

    #@fa
    check-cast v5, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;

    #@fc
    .line 456
    .restart local v5       #landview:Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;
    invoke-virtual {v5, v10}, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->setVisibility(I)V

    #@ff
    .line 457
    iget v7, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->inputType:I

    #@101
    invoke-virtual {v5, v7}, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->setDim(I)V

    #@104
    goto :goto_c1
.end method

.method private updateClipboard()V
    .registers 6

    #@0
    .prologue
    .line 472
    iget-object v3, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->gridlayout:Landroid/widget/GridLayout;

    #@2
    invoke-virtual {v3}, Landroid/widget/GridLayout;->getChildCount()I

    #@5
    move-result v0

    #@6
    .line 473
    .local v0, childCnt:I
    iget-object v3, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->clipManager:Landroid/content/ClipboardManager;

    #@8
    invoke-virtual {v3}, Landroid/content/ClipboardManager;->getPrimaryClipCount()I

    #@b
    move-result v1

    #@c
    .line 475
    .local v1, clipCnt:I
    if-eq v0, v1, :cond_15

    #@e
    .line 476
    const-string v3, "Cliptraywindow"

    #@10
    const-string v4, "error: clip count does not match child view count!!"

    #@12
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    .line 479
    :cond_15
    const/4 v2, 0x0

    #@16
    .local v2, i:I
    :goto_16
    if-ge v2, v0, :cond_1e

    #@18
    .line 480
    invoke-direct {p0, v2}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->updateClipDataCellAt(I)V

    #@1b
    .line 479
    add-int/lit8 v2, v2, 0x1

    #@1d
    goto :goto_16

    #@1e
    .line 483
    :cond_1e
    iget-object v3, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->cliptrayView:Landroid/widget/LinearLayout;

    #@20
    invoke-virtual {v3}, Landroid/widget/LinearLayout;->postInvalidate()V

    #@23
    .line 484
    return-void
.end method


# virtual methods
.method public add(Landroid/content/ClipData;)Z
    .registers 16
    .parameter "clip"

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    .line 572
    const/4 v10, 0x0

    #@2
    .line 573
    .local v10, txt:Ljava/lang/String;
    const/4 v7, 0x0

    #@3
    .line 574
    .local v7, imgUri:Landroid/net/Uri;
    invoke-virtual {p1}, Landroid/content/ClipData;->getItemCount()I

    #@6
    move-result v9

    #@7
    .line 576
    .local v9, itemCount:I
    const/4 v6, 0x0

    #@8
    .local v6, i:I
    :goto_8
    if-ge v6, v9, :cond_6b

    #@a
    .line 577
    invoke-virtual {p1, v6}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    #@d
    move-result-object v8

    #@e
    .line 578
    .local v8, item:Landroid/content/ClipData$Item;
    invoke-virtual {v8}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    #@11
    move-result-object v4

    #@12
    .line 579
    .local v4, clipUri:Landroid/net/Uri;
    iget-object v12, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mContext:Landroid/content/Context;

    #@14
    invoke-virtual {v8, v12}, Landroid/content/ClipData$Item;->coerceToStyledText(Landroid/content/Context;)Ljava/lang/CharSequence;

    #@17
    move-result-object v12

    #@18
    invoke-virtual {v12}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1b
    move-result-object v3

    #@1c
    .line 581
    .local v3, clipTxt:Ljava/lang/String;
    if-eqz v4, :cond_30

    #@1e
    const-string v12, "file"

    #@20
    invoke-virtual {v4}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@23
    move-result-object v13

    #@24
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@27
    move-result v12

    #@28
    if-eqz v12, :cond_30

    #@2a
    .line 582
    if-nez v7, :cond_2d

    #@2c
    .line 583
    move-object v7, v4

    #@2d
    .line 576
    :cond_2d
    :goto_2d
    add-int/lit8 v6, v6, 0x1

    #@2f
    goto :goto_8

    #@30
    .line 585
    :cond_30
    if-eqz v3, :cond_2d

    #@32
    .line 587
    const-string v12, "\n"

    #@34
    const-string v13, ""

    #@36
    invoke-virtual {v3, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@39
    move-result-object v3

    #@3a
    .line 588
    const-string v12, ""

    #@3c
    invoke-virtual {v3, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3f
    move-result v12

    #@40
    if-nez v12, :cond_2d

    #@42
    .line 589
    if-nez v10, :cond_46

    #@44
    .line 590
    move-object v10, v3

    #@45
    goto :goto_2d

    #@46
    .line 592
    :cond_46
    new-instance v12, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v12

    #@4f
    const-string v13, "\n"

    #@51
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v12

    #@55
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v10

    #@59
    .line 593
    new-instance v12, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v12

    #@62
    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v12

    #@66
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v10

    #@6a
    goto :goto_2d

    #@6b
    .line 600
    .end local v3           #clipTxt:Ljava/lang/String;
    .end local v4           #clipUri:Landroid/net/Uri;
    .end local v8           #item:Landroid/content/ClipData$Item;
    :cond_6b
    if-eqz v10, :cond_77

    #@6d
    if-eqz v7, :cond_77

    #@6f
    .line 601
    const/4 v5, 0x2

    #@70
    .line 610
    .local v5, datatype:I
    :goto_70
    invoke-direct {p0, v5, v10, v7}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->getCellView(ILjava/lang/String;Landroid/net/Uri;)Landroid/view/View;

    #@73
    move-result-object v2

    #@74
    .line 611
    .local v2, childview:Landroid/view/View;
    if-nez v2, :cond_83

    #@76
    .line 627
    .end local v2           #childview:Landroid/view/View;
    .end local v5           #datatype:I
    :cond_76
    :goto_76
    return v11

    #@77
    .line 602
    :cond_77
    if-eqz v10, :cond_7d

    #@79
    if-nez v7, :cond_7d

    #@7b
    .line 603
    const/4 v5, 0x0

    #@7c
    .restart local v5       #datatype:I
    goto :goto_70

    #@7d
    .line 604
    .end local v5           #datatype:I
    :cond_7d
    if-nez v10, :cond_76

    #@7f
    if-eqz v7, :cond_76

    #@81
    .line 605
    const/4 v5, 0x1

    #@82
    .restart local v5       #datatype:I
    goto :goto_70

    #@83
    .line 617
    .restart local v2       #childview:Landroid/view/View;
    :cond_83
    iget-boolean v12, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->isPortraitMode:Z

    #@85
    if-eqz v12, :cond_a8

    #@87
    .line 618
    iget-object v12, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mRes:Landroid/content/res/Resources;

    #@89
    const v13, 0x20c005d

    #@8c
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@8f
    move-result v1

    #@90
    .line 619
    .local v1, cellWidth:I
    iget-object v12, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mRes:Landroid/content/res/Resources;

    #@92
    const v13, 0x20c005c

    #@95
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@98
    move-result v0

    #@99
    .line 624
    .local v0, cellHeight:I
    :goto_99
    iget-object v12, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->gridlayout:Landroid/widget/GridLayout;

    #@9b
    new-instance v13, Landroid/widget/LinearLayout$LayoutParams;

    #@9d
    invoke-direct {v13, v1, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    #@a0
    invoke-virtual {v12, v2, v11, v13}, Landroid/widget/GridLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    #@a3
    .line 625
    invoke-direct {p0, v11}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->updateClipDataCellAt(I)V

    #@a6
    .line 627
    const/4 v11, 0x1

    #@a7
    goto :goto_76

    #@a8
    .line 621
    .end local v0           #cellHeight:I
    .end local v1           #cellWidth:I
    :cond_a8
    iget-object v12, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mRes:Landroid/content/res/Resources;

    #@aa
    const v13, 0x20c005f

    #@ad
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@b0
    move-result v1

    #@b1
    .line 622
    .restart local v1       #cellWidth:I
    iget-object v12, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mRes:Landroid/content/res/Resources;

    #@b3
    const v13, 0x20c005e

    #@b6
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@b9
    move-result v0

    #@ba
    .restart local v0       #cellHeight:I
    goto :goto_99
.end method

.method public addNewClipData()V
    .registers 29

    #@0
    .prologue
    .line 631
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->clipManager:Landroid/content/ClipboardManager;

    #@4
    move-object/from16 v25, v0

    #@6
    invoke-virtual/range {v25 .. v25}, Landroid/content/ClipboardManager;->getPrimaryClipCount()I

    #@9
    move-result v11

    #@a
    .line 632
    .local v11, count:I
    const/16 v25, 0x1

    #@c
    move/from16 v0, v25

    #@e
    if-ge v11, v0, :cond_18

    #@10
    .line 633
    const-string v25, "Cliptraywindow"

    #@12
    const-string v26, "addNewClipData::clip count is 0!!"

    #@14
    invoke-static/range {v25 .. v26}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    .line 735
    :cond_17
    :goto_17
    return-void

    #@18
    .line 636
    :cond_18
    move-object/from16 v0, p0

    #@1a
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->clipManager:Landroid/content/ClipboardManager;

    #@1c
    move-object/from16 v25, v0

    #@1e
    add-int/lit8 v26, v11, -0x1

    #@20
    invoke-virtual/range {v25 .. v26}, Landroid/content/ClipboardManager;->getPrimaryClipAt(I)Landroid/content/ClipData;

    #@23
    move-result-object v7

    #@24
    .line 637
    .local v7, clip:Landroid/content/ClipData;
    if-nez v7, :cond_41

    #@26
    .line 638
    const-string v25, "Cliptraywindow"

    #@28
    new-instance v26, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v27, "addNewClipData::clip data is null at "

    #@2f
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v26

    #@33
    add-int/lit8 v27, v11, -0x1

    #@35
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38
    move-result-object v26

    #@39
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v26

    #@3d
    invoke-static/range {v25 .. v26}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    goto :goto_17

    #@41
    .line 641
    :cond_41
    invoke-virtual {v7}, Landroid/content/ClipData;->getItemCount()I

    #@44
    move-result v18

    #@45
    .line 643
    .local v18, itemCount:I
    const/16 v24, 0x0

    #@47
    .line 644
    .local v24, txt:Ljava/lang/String;
    const/16 v16, 0x0

    #@49
    .line 645
    .local v16, imgUri:Landroid/net/Uri;
    const/4 v15, 0x0

    #@4a
    .local v15, i:I
    :goto_4a
    move/from16 v0, v18

    #@4c
    if-ge v15, v0, :cond_c9

    #@4e
    .line 646
    invoke-virtual {v7, v15}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    #@51
    move-result-object v17

    #@52
    .line 647
    .local v17, item:Landroid/content/ClipData$Item;
    invoke-virtual/range {v17 .. v17}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    #@55
    move-result-object v10

    #@56
    .line 648
    .local v10, clipUri:Landroid/net/Uri;
    move-object/from16 v0, p0

    #@58
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mContext:Landroid/content/Context;

    #@5a
    move-object/from16 v25, v0

    #@5c
    move-object/from16 v0, v17

    #@5e
    move-object/from16 v1, v25

    #@60
    invoke-virtual {v0, v1}, Landroid/content/ClipData$Item;->coerceToStyledText(Landroid/content/Context;)Ljava/lang/CharSequence;

    #@63
    move-result-object v25

    #@64
    invoke-virtual/range {v25 .. v25}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@67
    move-result-object v9

    #@68
    .line 650
    .local v9, clipTxt:Ljava/lang/String;
    if-eqz v10, :cond_7d

    #@6a
    const-string v25, "file"

    #@6c
    invoke-virtual {v10}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@6f
    move-result-object v26

    #@70
    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@73
    move-result v25

    #@74
    if-eqz v25, :cond_7d

    #@76
    .line 651
    if-nez v16, :cond_7a

    #@78
    .line 652
    move-object/from16 v16, v10

    #@7a
    .line 645
    :cond_7a
    :goto_7a
    add-int/lit8 v15, v15, 0x1

    #@7c
    goto :goto_4a

    #@7d
    .line 654
    :cond_7d
    if-eqz v9, :cond_7a

    #@7f
    .line 656
    const-string v25, "\n"

    #@81
    const-string v26, ""

    #@83
    move-object/from16 v0, v25

    #@85
    move-object/from16 v1, v26

    #@87
    invoke-virtual {v9, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@8a
    move-result-object v9

    #@8b
    .line 657
    const-string v25, ""

    #@8d
    move-object/from16 v0, v25

    #@8f
    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@92
    move-result v25

    #@93
    if-nez v25, :cond_7a

    #@95
    .line 658
    if-nez v24, :cond_9a

    #@97
    .line 659
    move-object/from16 v24, v9

    #@99
    goto :goto_7a

    #@9a
    .line 661
    :cond_9a
    new-instance v25, Ljava/lang/StringBuilder;

    #@9c
    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    #@9f
    move-object/from16 v0, v25

    #@a1
    move-object/from16 v1, v24

    #@a3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v25

    #@a7
    const-string v26, "\n"

    #@a9
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v25

    #@ad
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b0
    move-result-object v24

    #@b1
    .line 662
    new-instance v25, Ljava/lang/StringBuilder;

    #@b3
    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    #@b6
    move-object/from16 v0, v25

    #@b8
    move-object/from16 v1, v24

    #@ba
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v25

    #@be
    move-object/from16 v0, v25

    #@c0
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v25

    #@c4
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c7
    move-result-object v24

    #@c8
    goto :goto_7a

    #@c9
    .line 670
    .end local v9           #clipTxt:Ljava/lang/String;
    .end local v10           #clipUri:Landroid/net/Uri;
    .end local v17           #item:Landroid/content/ClipData$Item;
    :cond_c9
    if-eqz v24, :cond_e3

    #@cb
    if-eqz v16, :cond_e3

    #@cd
    .line 671
    const/4 v12, 0x2

    #@ce
    .line 681
    .local v12, datatype:I
    :goto_ce
    move-object/from16 v0, p0

    #@d0
    move-object/from16 v1, v24

    #@d2
    move-object/from16 v2, v16

    #@d4
    invoke-direct {v0, v12, v1, v2}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->getCellView(ILjava/lang/String;Landroid/net/Uri;)Landroid/view/View;

    #@d7
    move-result-object v6

    #@d8
    .line 682
    .local v6, cellview:Landroid/view/View;
    if-nez v6, :cond_ef

    #@da
    .line 683
    const-string v25, "Cliptraywindow"

    #@dc
    const-string v26, "add new clip data : nothing to add!!"

    #@de
    invoke-static/range {v25 .. v26}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e1
    goto/16 :goto_17

    #@e3
    .line 672
    .end local v6           #cellview:Landroid/view/View;
    .end local v12           #datatype:I
    :cond_e3
    if-eqz v24, :cond_e9

    #@e5
    if-nez v16, :cond_e9

    #@e7
    .line 673
    const/4 v12, 0x0

    #@e8
    .restart local v12       #datatype:I
    goto :goto_ce

    #@e9
    .line 674
    .end local v12           #datatype:I
    :cond_e9
    if-nez v24, :cond_17

    #@eb
    if-eqz v16, :cond_17

    #@ed
    .line 675
    const/4 v12, 0x1

    #@ee
    .restart local v12       #datatype:I
    goto :goto_ce

    #@ef
    .line 690
    .restart local v6       #cellview:Landroid/view/View;
    :cond_ef
    move-object/from16 v0, p0

    #@f1
    iget-boolean v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->isPortraitMode:Z

    #@f3
    move/from16 v25, v0

    #@f5
    if-eqz v25, :cond_1b7

    #@f7
    .line 691
    move-object/from16 v0, p0

    #@f9
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mRes:Landroid/content/res/Resources;

    #@fb
    move-object/from16 v25, v0

    #@fd
    const v26, 0x20c005d

    #@100
    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@103
    move-result v5

    #@104
    .line 692
    .local v5, cellWidth:I
    move-object/from16 v0, p0

    #@106
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mRes:Landroid/content/res/Resources;

    #@108
    move-object/from16 v25, v0

    #@10a
    const v26, 0x20c005c

    #@10d
    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@110
    move-result v4

    #@111
    .line 697
    .local v4, cellHeight:I
    :goto_111
    move-object/from16 v0, p0

    #@113
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->gridlayout:Landroid/widget/GridLayout;

    #@115
    move-object/from16 v25, v0

    #@117
    const/16 v26, 0x0

    #@119
    new-instance v27, Landroid/widget/LinearLayout$LayoutParams;

    #@11b
    move-object/from16 v0, v27

    #@11d
    invoke-direct {v0, v5, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    #@120
    move-object/from16 v0, v25

    #@122
    move/from16 v1, v26

    #@124
    move-object/from16 v2, v27

    #@126
    invoke-virtual {v0, v6, v1, v2}, Landroid/widget/GridLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    #@129
    .line 698
    const/16 v25, 0x0

    #@12b
    move-object/from16 v0, p0

    #@12d
    move/from16 v1, v25

    #@12f
    invoke-direct {v0, v1}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->updateClipDataCellAt(I)V

    #@132
    .line 702
    move-object/from16 v0, p0

    #@134
    iget-boolean v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->doCopyAnimation:Z

    #@136
    move/from16 v25, v0

    #@138
    if-eqz v25, :cond_151

    #@13a
    .line 703
    move-object/from16 v0, p0

    #@13c
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mContext:Landroid/content/Context;

    #@13e
    move-object/from16 v25, v0

    #@140
    const/high16 v26, 0x204

    #@142
    invoke-static/range {v25 .. v26}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    #@145
    move-result-object v3

    #@146
    .line 704
    .local v3, animSet:Landroid/view/animation/Animation;
    invoke-virtual {v6, v3}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    #@149
    .line 705
    const/16 v25, 0x0

    #@14b
    move/from16 v0, v25

    #@14d
    move-object/from16 v1, p0

    #@14f
    iput-boolean v0, v1, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->doCopyAnimation:Z

    #@151
    .line 708
    .end local v3           #animSet:Landroid/view/animation/Animation;
    :cond_151
    const/16 v25, 0x14

    #@153
    move/from16 v0, v25

    #@155
    if-le v11, v0, :cond_17

    #@157
    .line 710
    const/4 v8, 0x0

    #@158
    .line 712
    .local v8, clipIndex:I
    move-object/from16 v0, p0

    #@15a
    invoke-direct {v0, v8}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->getImageList(I)Ljava/util/ArrayList;

    #@15d
    move-result-object v20

    #@15e
    .line 713
    .local v20, lists:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v15, 0x0

    #@15f
    :goto_15f
    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    #@162
    move-result v25

    #@163
    move/from16 v0, v25

    #@165
    if-ge v15, v0, :cond_1d3

    #@167
    .line 714
    move-object/from16 v0, v20

    #@169
    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@16c
    move-result-object v25

    #@16d
    check-cast v25, Landroid/net/Uri;

    #@16f
    invoke-virtual/range {v25 .. v25}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    #@172
    move-result-object v21

    #@173
    .line 715
    .local v21, path:Ljava/lang/String;
    const-string v25, "Cliptraywindow"

    #@175
    new-instance v26, Ljava/lang/StringBuilder;

    #@177
    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    #@17a
    const-string v27, "remove clip image at = "

    #@17c
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17f
    move-result-object v26

    #@180
    move-object/from16 v0, v26

    #@182
    move-object/from16 v1, v21

    #@184
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@187
    move-result-object v26

    #@188
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18b
    move-result-object v26

    #@18c
    invoke-static/range {v25 .. v26}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18f
    .line 717
    new-instance v14, Ljava/io/File;

    #@191
    move-object/from16 v0, v21

    #@193
    invoke-direct {v14, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@196
    .line 718
    .local v14, file:Ljava/io/File;
    invoke-virtual {v14}, Ljava/io/File;->delete()Z

    #@199
    move-result v13

    #@19a
    .line 720
    .local v13, deleted:Z
    const-string v25, "Cliptraywindow"

    #@19c
    new-instance v26, Ljava/lang/StringBuilder;

    #@19e
    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    #@1a1
    const-string v27, "file removed : "

    #@1a3
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a6
    move-result-object v26

    #@1a7
    move-object/from16 v0, v26

    #@1a9
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1ac
    move-result-object v26

    #@1ad
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b0
    move-result-object v26

    #@1b1
    invoke-static/range {v25 .. v26}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1b4
    .line 713
    add-int/lit8 v15, v15, 0x1

    #@1b6
    goto :goto_15f

    #@1b7
    .line 694
    .end local v4           #cellHeight:I
    .end local v5           #cellWidth:I
    .end local v8           #clipIndex:I
    .end local v13           #deleted:Z
    .end local v14           #file:Ljava/io/File;
    .end local v20           #lists:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/net/Uri;>;"
    .end local v21           #path:Ljava/lang/String;
    :cond_1b7
    move-object/from16 v0, p0

    #@1b9
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mRes:Landroid/content/res/Resources;

    #@1bb
    move-object/from16 v25, v0

    #@1bd
    const v26, 0x20c005f

    #@1c0
    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@1c3
    move-result v5

    #@1c4
    .line 695
    .restart local v5       #cellWidth:I
    move-object/from16 v0, p0

    #@1c6
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mRes:Landroid/content/res/Resources;

    #@1c8
    move-object/from16 v25, v0

    #@1ca
    const v26, 0x20c005e

    #@1cd
    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@1d0
    move-result v4

    #@1d1
    .restart local v4       #cellHeight:I
    goto/16 :goto_111

    #@1d3
    .line 723
    .restart local v8       #clipIndex:I
    .restart local v20       #lists:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_1d3
    move-object/from16 v0, p0

    #@1d5
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->clipManager:Landroid/content/ClipboardManager;

    #@1d7
    move-object/from16 v25, v0

    #@1d9
    move-object/from16 v0, v25

    #@1db
    invoke-virtual {v0, v8}, Landroid/content/ClipboardManager;->removePrimaryClipAt(I)Z

    #@1de
    .line 726
    move-object/from16 v0, p0

    #@1e0
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->gridlayout:Landroid/widget/GridLayout;

    #@1e2
    move-object/from16 v25, v0

    #@1e4
    add-int/lit8 v26, v11, -0x1

    #@1e6
    invoke-virtual/range {v25 .. v26}, Landroid/widget/GridLayout;->getChildAt(I)Landroid/view/View;

    #@1e9
    move-result-object v23

    #@1ea
    .line 727
    .local v23, removeview:Landroid/view/View;
    if-eqz v23, :cond_20c

    #@1ec
    .line 728
    const v25, 0x20d004b

    #@1ef
    move-object/from16 v0, v23

    #@1f1
    move/from16 v1, v25

    #@1f3
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@1f6
    move-result-object v22

    #@1f7
    check-cast v22, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;

    #@1f9
    .line 729
    .local v22, portview:Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;
    const v25, 0x20d004d

    #@1fc
    move-object/from16 v0, v23

    #@1fe
    move/from16 v1, v25

    #@200
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@203
    move-result-object v19

    #@204
    check-cast v19, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;

    #@206
    .line 730
    .local v19, landview:Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;
    invoke-virtual/range {v22 .. v22}, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->remove()V

    #@209
    .line 731
    invoke-virtual/range {v19 .. v19}, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->remove()V

    #@20c
    .line 733
    .end local v19           #landview:Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;
    .end local v22           #portview:Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;
    :cond_20c
    move-object/from16 v0, p0

    #@20e
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->gridlayout:Landroid/widget/GridLayout;

    #@210
    move-object/from16 v25, v0

    #@212
    add-int/lit8 v26, v11, -0x1

    #@214
    invoke-virtual/range {v25 .. v26}, Landroid/widget/GridLayout;->removeViewAt(I)V

    #@217
    goto/16 :goto_17
.end method

.method public changeViewMode(Z)Z
    .registers 4
    .parameter "portrait"

    #@0
    .prologue
    .line 762
    iget-boolean v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->isPortraitMode:Z

    #@2
    if-ne v0, p1, :cond_6

    #@4
    .line 763
    const/4 v0, 0x0

    #@5
    .line 777
    :goto_5
    return v0

    #@6
    .line 766
    :cond_6
    iput-boolean p1, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->isPortraitMode:Z

    #@8
    .line 767
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->gridlayout:Landroid/widget/GridLayout;

    #@a
    invoke-virtual {v0}, Landroid/widget/GridLayout;->getChildCount()I

    #@d
    move-result v0

    #@e
    if-lez v0, :cond_15

    #@10
    .line 768
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->gridlayout:Landroid/widget/GridLayout;

    #@12
    invoke-virtual {v0}, Landroid/widget/GridLayout;->removeAllViews()V

    #@15
    .line 772
    :cond_15
    iget-boolean v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->isPortraitMode:Z

    #@17
    if-eqz v0, :cond_31

    #@19
    iget v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->displayPortWidth:I

    #@1b
    :goto_1b
    iput v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->cliptrayWidth:I

    #@1d
    .line 773
    iget-boolean v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->isPortraitMode:Z

    #@1f
    if-eqz v0, :cond_34

    #@21
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mRes:Landroid/content/res/Resources;

    #@23
    const v1, 0x20c004b

    #@26
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@29
    move-result v0

    #@2a
    :goto_2a
    iput v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->cliptrayHeight:I

    #@2c
    .line 776
    invoke-direct {p0}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->init()V

    #@2f
    .line 777
    const/4 v0, 0x1

    #@30
    goto :goto_5

    #@31
    .line 772
    :cond_31
    iget v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->displayPortHeight:I

    #@33
    goto :goto_1b

    #@34
    .line 773
    :cond_34
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mRes:Landroid/content/res/Resources;

    #@36
    const v1, 0x20c004c

    #@39
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@3c
    move-result v0

    #@3d
    goto :goto_2a
.end method

.method public getClipTrayView(II)Landroid/view/View;
    .registers 4
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 151
    if-le p2, p1, :cond_b

    #@2
    const/4 v0, 0x1

    #@3
    :goto_3
    iput-boolean v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->isPortraitMode:Z

    #@5
    .line 153
    invoke-direct {p0}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->init()V

    #@8
    .line 154
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->cliptrayView:Landroid/widget/LinearLayout;

    #@a
    return-object v0

    #@b
    .line 151
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_3
.end method

.method public getCurrentCellWidth()I
    .registers 4

    #@0
    .prologue
    .line 789
    iget-boolean v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->isPortraitMode:Z

    #@2
    if-eqz v0, :cond_11

    #@4
    sget v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mCell_norm_port_width:I

    #@6
    iget-object v1, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mRes:Landroid/content/res/Resources;

    #@8
    const v2, 0x20c004f

    #@b
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@e
    move-result v1

    #@f
    add-int/2addr v0, v1

    #@10
    :goto_10
    return v0

    #@11
    :cond_11
    sget v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mCell_norm_land_width:I

    #@13
    iget-object v1, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mRes:Landroid/content/res/Resources;

    #@15
    const v2, 0x20c0057

    #@18
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@1b
    move-result v1

    #@1c
    add-int/2addr v0, v1

    #@1d
    goto :goto_10
.end method

.method public getInputType()I
    .registers 2

    #@0
    .prologue
    .line 754
    iget v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->inputType:I

    #@2
    return v0
.end method

.method public hideClipTray()V
    .registers 4

    #@0
    .prologue
    const/16 v2, 0x8

    #@2
    const/4 v1, 0x0

    #@3
    .line 553
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->cliptrayView:Landroid/widget/LinearLayout;

    #@5
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    #@8
    .line 555
    iget-boolean v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->isDeleteMode:Z

    #@a
    if-eqz v0, :cond_11

    #@c
    .line 556
    iput-boolean v1, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->isDeleteMode:Z

    #@e
    .line 557
    invoke-direct {p0}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->updateClipboard()V

    #@11
    .line 561
    :cond_11
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mHScroll:Landroid/widget/HorizontalScrollView;

    #@13
    invoke-virtual {v0}, Landroid/widget/HorizontalScrollView;->getVisibility()I

    #@16
    move-result v0

    #@17
    if-ne v0, v2, :cond_28

    #@19
    .line 562
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mHScroll:Landroid/widget/HorizontalScrollView;

    #@1b
    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    #@1e
    .line 563
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mNoContentPortView:Landroid/view/View;

    #@20
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    #@23
    .line 564
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mNoContentLandView:Landroid/view/View;

    #@25
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    #@28
    .line 568
    :cond_28
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mHScroll:Landroid/widget/HorizontalScrollView;

    #@2a
    invoke-virtual {v0, v1, v1}, Landroid/widget/HorizontalScrollView;->scrollTo(II)V

    #@2d
    .line 569
    return-void
.end method

.method public isDeleteMode()Z
    .registers 2

    #@0
    .prologue
    .line 167
    iget-boolean v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->isDeleteMode:Z

    #@2
    return v0
.end method

.method public log(Ljava/lang/String;)V
    .registers 3
    .parameter "str"

    #@0
    .prologue
    .line 742
    const-string v0, "cliptray service"

    #@2
    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 743
    return-void
.end method

.method public onClipChanged()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 158
    iget-boolean v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->isAfterPaste:Z

    #@3
    if-eqz v0, :cond_8

    #@5
    .line 159
    iput-boolean v2, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->isAfterPaste:Z

    #@7
    .line 164
    :goto_7
    return-void

    #@8
    .line 161
    :cond_8
    const-string v0, "Cliptraywindow"

    #@a
    const-string v1, "onPrimaryClipChanged!! Update CliptrayWindow"

    #@c
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 162
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->handler:Landroid/os/Handler;

    #@11
    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@14
    goto :goto_7
.end method

.method public onFinishDeleteMode()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 171
    iput-boolean v5, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->isDeleteMode:Z

    #@3
    .line 172
    iget-boolean v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->isPortraitMode:Z

    #@5
    if-eqz v0, :cond_2b

    #@7
    .line 173
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->cliptrayView:Landroid/widget/LinearLayout;

    #@9
    iget-object v1, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mRes:Landroid/content/res/Resources;

    #@b
    const v2, 0x20c004f

    #@e
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@11
    move-result v1

    #@12
    iget-object v2, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mRes:Landroid/content/res/Resources;

    #@14
    const v3, 0x20c004d

    #@17
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@1a
    move-result v2

    #@1b
    iget-object v3, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mRes:Landroid/content/res/Resources;

    #@1d
    const v4, 0x20c004e

    #@20
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@23
    move-result v3

    #@24
    invoke-virtual {v0, v1, v2, v5, v3}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    #@27
    .line 182
    :goto_27
    invoke-direct {p0}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->updateClipboard()V

    #@2a
    .line 183
    return-void

    #@2b
    .line 177
    :cond_2b
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->cliptrayView:Landroid/widget/LinearLayout;

    #@2d
    iget-object v1, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mRes:Landroid/content/res/Resources;

    #@2f
    const v2, 0x20c0057

    #@32
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@35
    move-result v1

    #@36
    iget-object v2, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mRes:Landroid/content/res/Resources;

    #@38
    const v3, 0x20c0055

    #@3b
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@3e
    move-result v2

    #@3f
    iget-object v3, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mRes:Landroid/content/res/Resources;

    #@41
    const v4, 0x20c0056

    #@44
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@47
    move-result v3

    #@48
    invoke-virtual {v0, v1, v2, v5, v3}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    #@4b
    goto :goto_27
.end method

.method public onPreCopyToCliptray()V
    .registers 4

    #@0
    .prologue
    const/16 v2, 0x8

    #@2
    .line 781
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->clipManager:Landroid/content/ClipboardManager;

    #@4
    invoke-virtual {v0}, Landroid/content/ClipboardManager;->getPrimaryClipCount()I

    #@7
    move-result v0

    #@8
    const/4 v1, 0x1

    #@9
    if-ne v0, v1, :cond_1b

    #@b
    .line 782
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mHScroll:Landroid/widget/HorizontalScrollView;

    #@d
    const/4 v1, 0x0

    #@e
    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    #@11
    .line 783
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mNoContentPortView:Landroid/view/View;

    #@13
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    #@16
    .line 784
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mNoContentLandView:Landroid/view/View;

    #@18
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    #@1b
    .line 786
    :cond_1b
    return-void
.end method

.method public setDoCopyAnimation(Z)V
    .registers 2
    .parameter "doAnim"

    #@0
    .prologue
    .line 794
    iput-boolean p1, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->doCopyAnimation:Z

    #@2
    .line 795
    return-void
.end method

.method public setInputType(I)V
    .registers 3
    .parameter "type"

    #@0
    .prologue
    .line 746
    iget v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->inputType:I

    #@2
    if-ne v0, p1, :cond_5

    #@4
    .line 751
    :goto_4
    return-void

    #@5
    .line 749
    :cond_5
    iput p1, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->inputType:I

    #@7
    .line 750
    invoke-direct {p0}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->updateClipboard()V

    #@a
    goto :goto_4
.end method

.method public setOnDeleteModeListener(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnDeleteMode;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 102
    iput-object p1, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mIOnDeleteMode:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnDeleteMode;

    #@2
    .line 103
    return-void
.end method

.method public setOnNoContentListener(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnNoContent;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 111
    iput-object p1, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mIOnNoContent:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnNoContent;

    #@2
    .line 112
    return-void
.end method

.method public setOnPasteInterface(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnPasteInterface;)V
    .registers 2
    .parameter "onPasteInterface"

    #@0
    .prologue
    .line 93
    iput-object p1, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mIOnPasteInterface:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnPasteInterface;

    #@2
    .line 94
    return-void
.end method

.method public showClipTray()V
    .registers 5

    #@0
    .prologue
    const/16 v3, 0x8

    #@2
    const/4 v2, 0x0

    #@3
    .line 534
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->cliptrayView:Landroid/widget/LinearLayout;

    #@5
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    #@8
    .line 535
    const-string v0, "Cliptraywindow"

    #@a
    const-string v1, "showClipTray(), setVisibility(View.VISIBLE)"

    #@c
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 536
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->clipManager:Landroid/content/ClipboardManager;

    #@11
    invoke-virtual {v0}, Landroid/content/ClipboardManager;->getPrimaryClipCount()I

    #@14
    move-result v0

    #@15
    if-nez v0, :cond_2c

    #@17
    .line 537
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mHScroll:Landroid/widget/HorizontalScrollView;

    #@19
    invoke-virtual {v0, v3}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    #@1c
    .line 538
    iget-boolean v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->isPortraitMode:Z

    #@1e
    if-eqz v0, :cond_26

    #@20
    .line 539
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mNoContentPortView:Landroid/view/View;

    #@22
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    #@25
    .line 550
    :cond_25
    :goto_25
    return-void

    #@26
    .line 541
    :cond_26
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mNoContentLandView:Landroid/view/View;

    #@28
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    #@2b
    goto :goto_25

    #@2c
    .line 544
    :cond_2c
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mHScroll:Landroid/widget/HorizontalScrollView;

    #@2e
    invoke-virtual {v0}, Landroid/widget/HorizontalScrollView;->getVisibility()I

    #@31
    move-result v0

    #@32
    if-ne v0, v3, :cond_25

    #@34
    .line 545
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mHScroll:Landroid/widget/HorizontalScrollView;

    #@36
    invoke-virtual {v0, v2}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    #@39
    .line 546
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mNoContentPortView:Landroid/view/View;

    #@3b
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    #@3e
    .line 547
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->mNoContentLandView:Landroid/view/View;

    #@40
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    #@43
    goto :goto_25
.end method

.method public updateLocaleInfo()V
    .registers 6

    #@0
    .prologue
    const v4, 0x20902a3

    #@3
    .line 798
    iget-object v2, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->cliptrayView:Landroid/widget/LinearLayout;

    #@5
    const v3, 0x20d0044

    #@8
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    #@b
    move-result-object v1

    #@c
    check-cast v1, Landroid/widget/TextView;

    #@e
    .line 799
    .local v1, txtNoContentPort:Landroid/widget/TextView;
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    #@11
    .line 800
    iget-object v2, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->cliptrayView:Landroid/widget/LinearLayout;

    #@13
    const v3, 0x20d0046

    #@16
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    #@19
    move-result-object v0

    #@1a
    check-cast v0, Landroid/widget/TextView;

    #@1c
    .line 801
    .local v0, txtNoContentLand:Landroid/widget/TextView;
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    #@1f
    .line 802
    return-void
.end method
