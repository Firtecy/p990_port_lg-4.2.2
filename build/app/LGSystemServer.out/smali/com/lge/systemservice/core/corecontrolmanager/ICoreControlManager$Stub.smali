.class public abstract Lcom/lge/systemservice/core/corecontrolmanager/ICoreControlManager$Stub;
.super Landroid/os/Binder;
.source "ICoreControlManager.java"

# interfaces
.implements Lcom/lge/systemservice/core/corecontrolmanager/ICoreControlManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/core/corecontrolmanager/ICoreControlManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 15
    const-string v0, "com.lge.systemservice.core.corecontrolmanager.ICoreControlManager"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/lge/systemservice/core/corecontrolmanager/ICoreControlManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 16
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 10
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 38
    sparse-switch p1, :sswitch_data_78

    #@4
    .line 101
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v3

    #@8
    :goto_8
    return v3

    #@9
    .line 42
    :sswitch_9
    const-string v4, "com.lge.systemservice.core.corecontrolmanager.ICoreControlManager"

    #@b
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 47
    :sswitch_f
    const-string v4, "com.lge.systemservice.core.corecontrolmanager.ICoreControlManager"

    #@11
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v0

    #@18
    .line 50
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/corecontrolmanager/ICoreControlManager$Stub;->nativeCoreControlNotification(I)V

    #@1b
    .line 51
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1e
    goto :goto_8

    #@1f
    .line 56
    .end local v0           #_arg0:I
    :sswitch_1f
    const-string v4, "com.lge.systemservice.core.corecontrolmanager.ICoreControlManager"

    #@21
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@24
    .line 57
    invoke-virtual {p0}, Lcom/lge/systemservice/core/corecontrolmanager/ICoreControlManager$Stub;->getCoreState()I

    #@27
    move-result v2

    #@28
    .line 58
    .local v2, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2b
    .line 59
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@2e
    goto :goto_8

    #@2f
    .line 64
    .end local v2           #_result:I
    :sswitch_2f
    const-string v4, "com.lge.systemservice.core.corecontrolmanager.ICoreControlManager"

    #@31
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@34
    .line 66
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@37
    move-result v0

    #@38
    .line 67
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/corecontrolmanager/ICoreControlManager$Stub;->changeCoreState(I)V

    #@3b
    .line 68
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3e
    goto :goto_8

    #@3f
    .line 73
    .end local v0           #_arg0:I
    :sswitch_3f
    const-string v4, "com.lge.systemservice.core.corecontrolmanager.ICoreControlManager"

    #@41
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@44
    .line 75
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@47
    move-result v0

    #@48
    .line 76
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/corecontrolmanager/ICoreControlManager$Stub;->changeMaxFrequency(I)V

    #@4b
    .line 77
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4e
    goto :goto_8

    #@4f
    .line 82
    .end local v0           #_arg0:I
    :sswitch_4f
    const-string v4, "com.lge.systemservice.core.corecontrolmanager.ICoreControlManager"

    #@51
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@54
    .line 84
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@57
    move-result v0

    #@58
    .line 85
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/corecontrolmanager/ICoreControlManager$Stub;->changeDisableEDP(I)V

    #@5b
    .line 86
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5e
    goto :goto_8

    #@5f
    .line 91
    .end local v0           #_arg0:I
    :sswitch_5f
    const-string v4, "com.lge.systemservice.core.corecontrolmanager.ICoreControlManager"

    #@61
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@64
    .line 93
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@67
    move-result v0

    #@68
    .line 95
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@6b
    move-result v4

    #@6c
    if-eqz v4, :cond_76

    #@6e
    move v1, v3

    #@6f
    .line 96
    .local v1, _arg1:Z
    :goto_6f
    invoke-virtual {p0, v0, v1}, Lcom/lge/systemservice/core/corecontrolmanager/ICoreControlManager$Stub;->setLowPowerModePolicy(IZ)V

    #@72
    .line 97
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@75
    goto :goto_8

    #@76
    .line 95
    .end local v1           #_arg1:Z
    :cond_76
    const/4 v1, 0x0

    #@77
    goto :goto_6f

    #@78
    .line 38
    :sswitch_data_78
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_1f
        0x3 -> :sswitch_2f
        0x4 -> :sswitch_3f
        0x5 -> :sswitch_4f
        0x6 -> :sswitch_5f
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
