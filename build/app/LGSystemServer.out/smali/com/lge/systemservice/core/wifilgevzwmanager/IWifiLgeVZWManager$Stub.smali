.class public abstract Lcom/lge/systemservice/core/wifilgevzwmanager/IWifiLgeVZWManager$Stub;
.super Landroid/os/Binder;
.source "IWifiLgeVZWManager.java"

# interfaces
.implements Lcom/lge/systemservice/core/wifilgevzwmanager/IWifiLgeVZWManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/core/wifilgevzwmanager/IWifiLgeVZWManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 17
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 18
    const-string v0, "com.lge.systemservice.core.wifilgevzwmanager.IWifiLgeVZWManager"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/lge/systemservice/core/wifilgevzwmanager/IWifiLgeVZWManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 19
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 37
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 6
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 41
    packed-switch p1, :pswitch_data_10

    #@3
    .line 49
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@6
    move-result v0

    #@7
    :goto_7
    return v0

    #@8
    .line 45
    :pswitch_8
    const-string v0, "com.lge.systemservice.core.wifilgevzwmanager.IWifiLgeVZWManager"

    #@a
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@d
    .line 46
    const/4 v0, 0x1

    #@e
    goto :goto_7

    #@f
    .line 41
    nop

    #@10
    :pswitch_data_10
    .packed-switch 0x5f4e5446
        :pswitch_8
    .end packed-switch
.end method
