.class public Lcom/lge/systemservice/core/felicamanager/FeliCaManager;
.super Ljava/lang/Object;
.source "FeliCaManager.java"


# static fields
.field private static deathBinderNotificator:Landroid/os/IBinder$DeathRecipient;

.field private static mService:Lcom/lge/systemservice/core/felicamanager/IFeliCaManager;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 18
    const/4 v0, 0x0

    #@1
    sput-object v0, Lcom/lge/systemservice/core/felicamanager/FeliCaManager;->mService:Lcom/lge/systemservice/core/felicamanager/IFeliCaManager;

    #@3
    .line 28
    new-instance v0, Lcom/lge/systemservice/core/felicamanager/FeliCaManager$1;

    #@5
    invoke-direct {v0}, Lcom/lge/systemservice/core/felicamanager/FeliCaManager$1;-><init>()V

    #@8
    sput-object v0, Lcom/lge/systemservice/core/felicamanager/FeliCaManager;->deathBinderNotificator:Landroid/os/IBinder$DeathRecipient;

    #@a
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 36
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 37
    return-void
.end method

.method static synthetic access$002(Lcom/lge/systemservice/core/felicamanager/IFeliCaManager;)Lcom/lge/systemservice/core/felicamanager/IFeliCaManager;
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 15
    sput-object p0, Lcom/lge/systemservice/core/felicamanager/FeliCaManager;->mService:Lcom/lge/systemservice/core/felicamanager/IFeliCaManager;

    #@2
    return-object p0
.end method

.method private static getService()Lcom/lge/systemservice/core/felicamanager/IFeliCaManager;
    .registers 5

    #@0
    .prologue
    .line 45
    sget-object v2, Lcom/lge/systemservice/core/felicamanager/FeliCaManager;->mService:Lcom/lge/systemservice/core/felicamanager/IFeliCaManager;

    #@2
    if-eqz v2, :cond_7

    #@4
    .line 46
    sget-object v2, Lcom/lge/systemservice/core/felicamanager/FeliCaManager;->mService:Lcom/lge/systemservice/core/felicamanager/IFeliCaManager;

    #@6
    .line 74
    .local v0, b:Landroid/os/IBinder;
    :goto_6
    return-object v2

    #@7
    .line 49
    .end local v0           #b:Landroid/os/IBinder;
    :cond_7
    const-string v2, "FeliCaService"

    #@9
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@c
    move-result-object v0

    #@d
    .line 51
    .restart local v0       #b:Landroid/os/IBinder;
    invoke-static {v0}, Lcom/lge/systemservice/core/felicamanager/IFeliCaManager$Stub;->asInterface(Landroid/os/IBinder;)Lcom/lge/systemservice/core/felicamanager/IFeliCaManager;

    #@10
    move-result-object v2

    #@11
    sput-object v2, Lcom/lge/systemservice/core/felicamanager/FeliCaManager;->mService:Lcom/lge/systemservice/core/felicamanager/IFeliCaManager;

    #@13
    .line 54
    if-nez v0, :cond_1c

    #@15
    .line 55
    const-string v2, "FeliCaManager"

    #@17
    const-string v3, "fucking b nil      ```````````````````````````````"

    #@19
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 56
    :cond_1c
    sget-object v2, Lcom/lge/systemservice/core/felicamanager/FeliCaManager;->mService:Lcom/lge/systemservice/core/felicamanager/IFeliCaManager;

    #@1e
    if-nez v2, :cond_27

    #@20
    .line 57
    const-string v2, "FeliCaManager"

    #@22
    const-string v3, "fucking mService nil      ```````````````````````````````"

    #@24
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 67
    :cond_27
    :try_start_27
    sget-object v2, Lcom/lge/systemservice/core/felicamanager/FeliCaManager;->mService:Lcom/lge/systemservice/core/felicamanager/IFeliCaManager;

    #@29
    if-eqz v2, :cond_37

    #@2b
    .line 69
    sget-object v2, Lcom/lge/systemservice/core/felicamanager/FeliCaManager;->mService:Lcom/lge/systemservice/core/felicamanager/IFeliCaManager;

    #@2d
    invoke-interface {v2}, Lcom/lge/systemservice/core/felicamanager/IFeliCaManager;->asBinder()Landroid/os/IBinder;

    #@30
    move-result-object v2

    #@31
    sget-object v3, Lcom/lge/systemservice/core/felicamanager/FeliCaManager;->deathBinderNotificator:Landroid/os/IBinder$DeathRecipient;

    #@33
    const/4 v4, 0x0

    #@34
    invoke-interface {v2, v3, v4}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_37
    .catch Landroid/os/RemoteException; {:try_start_27 .. :try_end_37} :catch_3a

    #@37
    .line 74
    :cond_37
    :goto_37
    sget-object v2, Lcom/lge/systemservice/core/felicamanager/FeliCaManager;->mService:Lcom/lge/systemservice/core/felicamanager/IFeliCaManager;

    #@39
    goto :goto_6

    #@3a
    .line 71
    :catch_3a
    move-exception v1

    #@3b
    .line 72
    .local v1, e:Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    #@3e
    goto :goto_37
.end method


# virtual methods
.method public cmdEXTIDM([B)Z
    .registers 5
    .parameter "idm"

    #@0
    .prologue
    .line 80
    :try_start_0
    const-string v1, "FeliCaManager"

    #@2
    const-string v2, "cmdEXTIDM is called."

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 82
    invoke-static {}, Lcom/lge/systemservice/core/felicamanager/FeliCaManager;->getService()Lcom/lge/systemservice/core/felicamanager/IFeliCaManager;

    #@a
    move-result-object v1

    #@b
    invoke-interface {v1, p1}, Lcom/lge/systemservice/core/felicamanager/IFeliCaManager;->cmdEXTIDM([B)Z
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_e} :catch_10

    #@e
    move-result v1

    #@f
    .line 87
    :goto_f
    return v1

    #@10
    .line 84
    :catch_10
    move-exception v0

    #@11
    .line 85
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "FeliCaManager"

    #@13
    const-string v2, "cmdEXTIDM is failed."

    #@15
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 86
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@1b
    .line 87
    const/4 v1, 0x0

    #@1c
    goto :goto_f
.end method
