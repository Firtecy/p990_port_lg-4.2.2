.class public abstract Lcom/lge/systemservice/core/LGServiceSingleton;
.super Lcom/lge/systemservice/core/LGSingleton;
.source "LGServiceSingleton.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/lge/systemservice/core/LGSingleton",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private deathBinderNotificator:Landroid/os/IBinder$DeathRecipient;

.field private mService:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private mServiceName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 3
    .parameter "ServiceName"

    #@0
    .prologue
    .line 18
    .local p0, this:Lcom/lge/systemservice/core/LGServiceSingleton;,"Lcom/lge/systemservice/core/LGServiceSingleton<TT;>;"
    invoke-direct {p0}, Lcom/lge/systemservice/core/LGSingleton;-><init>()V

    #@3
    .line 58
    new-instance v0, Lcom/lge/systemservice/core/LGServiceSingleton$1;

    #@5
    invoke-direct {v0, p0}, Lcom/lge/systemservice/core/LGServiceSingleton$1;-><init>(Lcom/lge/systemservice/core/LGServiceSingleton;)V

    #@8
    iput-object v0, p0, Lcom/lge/systemservice/core/LGServiceSingleton;->deathBinderNotificator:Landroid/os/IBinder$DeathRecipient;

    #@a
    .line 19
    iput-object p1, p0, Lcom/lge/systemservice/core/LGServiceSingleton;->mServiceName:Ljava/lang/String;

    #@c
    .line 20
    return-void
.end method

.method static synthetic access$000(Lcom/lge/systemservice/core/LGServiceSingleton;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 14
    iget-object v0, p0, Lcom/lge/systemservice/core/LGServiceSingleton;->mServiceName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$102(Lcom/lge/systemservice/core/LGServiceSingleton;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 14
    iput-object p1, p0, Lcom/lge/systemservice/core/LGServiceSingleton;->mService:Ljava/lang/Object;

    #@2
    return-object p1
.end method


# virtual methods
.method protected create()Ljava/lang/Object;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    #@0
    .prologue
    .local p0, this:Lcom/lge/systemservice/core/LGServiceSingleton;,"Lcom/lge/systemservice/core/LGServiceSingleton<TT;>;"
    const/4 v2, 0x0

    #@1
    .line 31
    iget-object v3, p0, Lcom/lge/systemservice/core/LGServiceSingleton;->mServiceName:Ljava/lang/String;

    #@3
    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@6
    move-result-object v0

    #@7
    .line 33
    .local v0, b:Landroid/os/IBinder;
    if-nez v0, :cond_24

    #@9
    .line 34
    const-string v3, "LGServiceSingleton"

    #@b
    new-instance v4, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v5, "Failure getting binder service: "

    #@12
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v4

    #@16
    iget-object v5, p0, Lcom/lge/systemservice/core/LGServiceSingleton;->mServiceName:Ljava/lang/String;

    #@18
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v4

    #@20
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 51
    :goto_23
    return-object v2

    #@24
    .line 39
    :cond_24
    :try_start_24
    iget-object v3, p0, Lcom/lge/systemservice/core/LGServiceSingleton;->deathBinderNotificator:Landroid/os/IBinder$DeathRecipient;

    #@26
    const/4 v4, 0x0

    #@27
    invoke-interface {v0, v3, v4}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_2a
    .catch Landroid/os/RemoteException; {:try_start_24 .. :try_end_2a} :catch_37

    #@2a
    .line 45
    :goto_2a
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/LGServiceSingleton;->onCallBackProxyInterface(Landroid/os/IBinder;)Ljava/lang/Object;

    #@2d
    move-result-object v3

    #@2e
    iput-object v3, p0, Lcom/lge/systemservice/core/LGServiceSingleton;->mService:Ljava/lang/Object;

    #@30
    .line 47
    iget-object v3, p0, Lcom/lge/systemservice/core/LGServiceSingleton;->mService:Ljava/lang/Object;

    #@32
    if-eqz v3, :cond_3c

    #@34
    .line 48
    iget-object v2, p0, Lcom/lge/systemservice/core/LGServiceSingleton;->mService:Ljava/lang/Object;

    #@36
    goto :goto_23

    #@37
    .line 40
    :catch_37
    move-exception v1

    #@38
    .line 42
    .local v1, e:Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    #@3b
    goto :goto_2a

    #@3c
    .line 50
    .end local v1           #e:Landroid/os/RemoteException;
    :cond_3c
    const-string v3, "LGServiceSingleton"

    #@3e
    new-instance v4, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v5, "Failure getting Service proxy: "

    #@45
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v4

    #@49
    iget-object v5, p0, Lcom/lge/systemservice/core/LGServiceSingleton;->mServiceName:Ljava/lang/String;

    #@4b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v4

    #@4f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v4

    #@53
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    goto :goto_23
.end method

.method protected abstract onCallBackProxyInterface(Landroid/os/IBinder;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/IBinder;",
            ")TT;"
        }
    .end annotation
.end method
