.class public Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;
.super Ljava/lang/Object;
.source "FmRadioManager.java"

# interfaces
.implements Lcom/broadcom/fm/fmreceiver/IFmProxyCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager$FmReceiverEventHandler;
    }
.end annotation


# instance fields
.field mContext:Landroid/content/Context;

.field private mFmReceiverEventHandler:Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager$FmReceiverEventHandler;

.field private mReceiver:Lcom/broadcom/fm/fmreceiver/FmProxy;

.field private nState:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 38
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->mFmReceiverEventHandler:Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager$FmReceiverEventHandler;

    #@6
    .line 41
    const/16 v0, 0xb

    #@8
    iput v0, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->nState:I

    #@a
    .line 46
    const-string v0, "FmRadioManager"

    #@c
    const-string v1, "Create FmRadioManager"

    #@e
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 48
    iput-object p1, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->mContext:Landroid/content/Context;

    #@13
    .line 51
    iget-object v0, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->mReceiver:Lcom/broadcom/fm/fmreceiver/FmProxy;

    #@15
    if-nez v0, :cond_23

    #@17
    .line 52
    const-string v0, "FmRadioManager"

    #@19
    const-string v1, "Getting FmProxy proxy..."

    #@1b
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 53
    iget-object v0, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->mContext:Landroid/content/Context;

    #@20
    invoke-static {v0, p0}, Lcom/broadcom/fm/fmreceiver/FmProxy;->getProxy(Landroid/content/Context;Lcom/broadcom/fm/fmreceiver/IFmProxyCallback;)Z

    #@23
    .line 56
    :cond_23
    iget-object v0, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->mFmReceiverEventHandler:Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager$FmReceiverEventHandler;

    #@25
    if-nez v0, :cond_2e

    #@27
    .line 57
    new-instance v0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager$FmReceiverEventHandler;

    #@29
    invoke-direct {v0, p0}, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager$FmReceiverEventHandler;-><init>(Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;)V

    #@2c
    iput-object v0, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->mFmReceiverEventHandler:Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager$FmReceiverEventHandler;

    #@2e
    .line 59
    :cond_2e
    return-void
.end method

.method static synthetic access$000(Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 19
    iget v0, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->nState:I

    #@2
    return v0
.end method

.method static synthetic access$002(Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 19
    iput p1, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->nState:I

    #@2
    return p1
.end method

.method static synthetic access$100(Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;)Lcom/broadcom/fm/fmreceiver/FmProxy;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 19
    iget-object v0, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->mReceiver:Lcom/broadcom/fm/fmreceiver/FmProxy;

    #@2
    return-object v0
.end method


# virtual methods
.method getAudioManager(Landroid/content/Context;)Landroid/media/AudioManager;
    .registers 3
    .parameter "c"

    #@0
    .prologue
    .line 62
    const-string v0, "audio"

    #@2
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/media/AudioManager;

    #@8
    return-object v0
.end method

.method public onProxyAvailable(Ljava/lang/Object;)V
    .registers 5
    .parameter "ProxyObject"

    #@0
    .prologue
    .line 182
    const-string v0, "FmRadioManager"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "onProxyAvailable ( "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, " )"

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 183
    iget-object v0, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->mReceiver:Lcom/broadcom/fm/fmreceiver/FmProxy;

    #@20
    if-nez v0, :cond_26

    #@22
    .line 184
    check-cast p1, Lcom/broadcom/fm/fmreceiver/FmProxy;

    #@24
    .end local p1
    iput-object p1, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->mReceiver:Lcom/broadcom/fm/fmreceiver/FmProxy;

    #@26
    .line 185
    :cond_26
    iget-object v0, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->mReceiver:Lcom/broadcom/fm/fmreceiver/FmProxy;

    #@28
    if-nez v0, :cond_32

    #@2a
    .line 186
    const-string v0, "FmRadioManager"

    #@2c
    const-string v1, "Error : Get Proxy"

    #@2e
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    .line 204
    :cond_31
    :goto_31
    return-void

    #@32
    .line 198
    :cond_32
    iget-object v0, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->mReceiver:Lcom/broadcom/fm/fmreceiver/FmProxy;

    #@34
    invoke-virtual {v0}, Lcom/broadcom/fm/fmreceiver/FmProxy;->getRadioIsOn()Z

    #@37
    move-result v0

    #@38
    if-eqz v0, :cond_31

    #@3a
    .line 202
    iget-object v0, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->mReceiver:Lcom/broadcom/fm/fmreceiver/FmProxy;

    #@3c
    invoke-virtual {v0}, Lcom/broadcom/fm/fmreceiver/FmProxy;->getStatus()I

    #@3f
    goto :goto_31
.end method

.method public turnOff()Z
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/16 v7, 0xb

    #@3
    const/4 v2, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 131
    const-string v4, "FmRadioManager"

    #@7
    new-instance v5, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v6, "turnOff : nState="

    #@e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v5

    #@12
    iget v6, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->nState:I

    #@14
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v5

    #@18
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v5

    #@1c
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 134
    iget v4, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->nState:I

    #@21
    if-ne v4, v7, :cond_3e

    #@23
    .line 135
    const-string v3, "FmRadioManager"

    #@25
    new-instance v4, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v5, "turnOff : No normal nState="

    #@2c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v4

    #@30
    iget v5, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->nState:I

    #@32
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35
    move-result-object v4

    #@36
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v4

    #@3a
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 162
    :goto_3d
    return v2

    #@3e
    .line 138
    :cond_3e
    iget-object v4, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->mContext:Landroid/content/Context;

    #@40
    invoke-virtual {p0, v4}, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->getAudioManager(Landroid/content/Context;)Landroid/media/AudioManager;

    #@43
    move-result-object v4

    #@44
    const-string v5, "fm_audio=inactive"

    #@46
    invoke-virtual {v4, v5}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    #@49
    .line 141
    iget-object v4, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->mReceiver:Lcom/broadcom/fm/fmreceiver/FmProxy;

    #@4b
    if-eqz v4, :cond_ad

    #@4d
    .line 142
    iget-object v4, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->mReceiver:Lcom/broadcom/fm/fmreceiver/FmProxy;

    #@4f
    invoke-virtual {v4}, Lcom/broadcom/fm/fmreceiver/FmProxy;->turnOffRadio()I

    #@52
    move-result v1

    #@53
    .line 144
    .local v1, nStatus:I
    if-nez v1, :cond_8c

    #@55
    .line 145
    const/4 v0, 0x0

    #@56
    .local v0, i:I
    :goto_56
    const/16 v3, 0x1e

    #@58
    if-ge v0, v3, :cond_7c

    #@5a
    .line 146
    const-string v3, "FmRadioManager"

    #@5c
    new-instance v4, Ljava/lang/StringBuilder;

    #@5e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@61
    const-string v5, "Waiting turnOffRadio = "

    #@63
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v4

    #@67
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v4

    #@6b
    const-string v5, " time"

    #@6d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v4

    #@71
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v4

    #@75
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@78
    .line 147
    iget v3, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->nState:I

    #@7a
    if-ne v3, v7, :cond_84

    #@7c
    .line 152
    :cond_7c
    iget-object v3, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->mReceiver:Lcom/broadcom/fm/fmreceiver/FmProxy;

    #@7e
    invoke-virtual {v3}, Lcom/broadcom/fm/fmreceiver/FmProxy;->unregisterEventHandler()V

    #@81
    .line 153
    iput-object v8, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->mFmReceiverEventHandler:Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager$FmReceiverEventHandler;

    #@83
    goto :goto_3d

    #@84
    .line 150
    :cond_84
    const-wide/16 v3, 0x64

    #@86
    invoke-static {v3, v4}, Landroid/os/SystemClock;->sleep(J)V

    #@89
    .line 145
    add-int/lit8 v0, v0, 0x1

    #@8b
    goto :goto_56

    #@8c
    .line 156
    .end local v0           #i:I
    :cond_8c
    const-string v2, "FmRadioManager"

    #@8e
    new-instance v4, Ljava/lang/StringBuilder;

    #@90
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@93
    const-string v5, "turnOffRadio() returned error :"

    #@95
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v4

    #@99
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v4

    #@9d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a0
    move-result-object v4

    #@a1
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a4
    .line 157
    iget-object v2, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->mReceiver:Lcom/broadcom/fm/fmreceiver/FmProxy;

    #@a6
    invoke-virtual {v2}, Lcom/broadcom/fm/fmreceiver/FmProxy;->unregisterEventHandler()V

    #@a9
    .line 158
    iput-object v8, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->mFmReceiverEventHandler:Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager$FmReceiverEventHandler;

    #@ab
    move v2, v3

    #@ac
    .line 159
    goto :goto_3d

    #@ad
    .end local v1           #nStatus:I
    :cond_ad
    move v2, v3

    #@ae
    .line 162
    goto :goto_3d
.end method

.method public turnOn()Z
    .registers 10

    #@0
    .prologue
    const-wide/16 v7, 0x64

    #@2
    const/4 v2, 0x1

    #@3
    const/4 v3, 0x0

    #@4
    .line 71
    const/4 v0, 0x0

    #@5
    .local v0, i:I
    const/4 v1, 0x0

    #@6
    .line 72
    .local v1, result:I
    const-string v4, "FmRadioManager"

    #@8
    const-string v5, "turnOn"

    #@a
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 73
    iget-object v4, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->mReceiver:Lcom/broadcom/fm/fmreceiver/FmProxy;

    #@f
    if-nez v4, :cond_5c

    #@11
    .line 74
    :goto_11
    const/4 v4, 0x5

    #@12
    if-ge v0, v4, :cond_56

    #@14
    .line 75
    iget-object v4, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->mReceiver:Lcom/broadcom/fm/fmreceiver/FmProxy;

    #@16
    if-nez v4, :cond_3c

    #@18
    .line 76
    const-string v4, "FmRadioManager"

    #@1a
    new-instance v5, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v6, "Waiting FM Receiver Proxy!!!! +"

    #@21
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v5

    #@25
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v5

    #@29
    const-string v6, " time"

    #@2b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v5

    #@2f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v5

    #@33
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 77
    invoke-static {v7, v8}, Landroid/os/SystemClock;->sleep(J)V

    #@39
    .line 78
    add-int/lit8 v0, v0, 0x1

    #@3b
    .line 79
    goto :goto_11

    #@3c
    .line 81
    :cond_3c
    const-string v4, "FmRadioManager"

    #@3e
    new-instance v5, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v6, "Got FM Receiver Proxy!!!! +"

    #@45
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v5

    #@49
    iget-object v6, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->mReceiver:Lcom/broadcom/fm/fmreceiver/FmProxy;

    #@4b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v5

    #@4f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v5

    #@53
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    .line 86
    :cond_56
    iget-object v4, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->mReceiver:Lcom/broadcom/fm/fmreceiver/FmProxy;

    #@58
    if-nez v4, :cond_5c

    #@5a
    move v2, v3

    #@5b
    .line 120
    :cond_5b
    :goto_5b
    return v2

    #@5c
    .line 92
    :cond_5c
    iget-object v4, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->mReceiver:Lcom/broadcom/fm/fmreceiver/FmProxy;

    #@5e
    if-eqz v4, :cond_b2

    #@60
    .line 93
    const-string v4, "FmRadioManager"

    #@62
    new-instance v5, Ljava/lang/StringBuilder;

    #@64
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@67
    const-string v6, "mReceiver.registerEventHandler( "

    #@69
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v5

    #@6d
    iget-object v6, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->mFmReceiverEventHandler:Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager$FmReceiverEventHandler;

    #@6f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v5

    #@73
    const-string v6, " )"

    #@75
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v5

    #@79
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v5

    #@7d
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@80
    .line 94
    iget-object v4, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->mFmReceiverEventHandler:Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager$FmReceiverEventHandler;

    #@82
    if-nez v4, :cond_8b

    #@84
    .line 95
    new-instance v4, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager$FmReceiverEventHandler;

    #@86
    invoke-direct {v4, p0}, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager$FmReceiverEventHandler;-><init>(Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;)V

    #@89
    iput-object v4, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->mFmReceiverEventHandler:Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager$FmReceiverEventHandler;

    #@8b
    .line 97
    :cond_8b
    const-string v4, "FmRadioManager"

    #@8d
    new-instance v5, Ljava/lang/StringBuilder;

    #@8f
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@92
    const-string v6, "mReceiver.registerEventHandler( "

    #@94
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v5

    #@98
    iget-object v6, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->mFmReceiverEventHandler:Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager$FmReceiverEventHandler;

    #@9a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v5

    #@9e
    const-string v6, " )"

    #@a0
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v5

    #@a4
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a7
    move-result-object v5

    #@a8
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ab
    .line 98
    iget-object v4, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->mReceiver:Lcom/broadcom/fm/fmreceiver/FmProxy;

    #@ad
    iget-object v5, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->mFmReceiverEventHandler:Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager$FmReceiverEventHandler;

    #@af
    invoke-virtual {v4, v5}, Lcom/broadcom/fm/fmreceiver/FmProxy;->registerEventHandler(Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;)V

    #@b2
    .line 103
    :cond_b2
    const-string v4, "FmRadioManager"

    #@b4
    const-string v5, "mReceiver.turnOnRadio()"

    #@b6
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b9
    .line 104
    iput v2, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->nState:I

    #@bb
    .line 105
    iget-object v4, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->mReceiver:Lcom/broadcom/fm/fmreceiver/FmProxy;

    #@bd
    const/16 v5, 0x50

    #@bf
    const-string v6, "com.lge.fmradio"

    #@c1
    invoke-virtual {v4, v5, v6}, Lcom/broadcom/fm/fmreceiver/FmProxy;->turnOnRadio(ILjava/lang/String;)I

    #@c4
    move-result v1

    #@c5
    .line 108
    if-nez v1, :cond_f6

    #@c7
    .line 109
    const/4 v0, 0x0

    #@c8
    :goto_c8
    const/16 v3, 0x28

    #@ca
    if-ge v0, v3, :cond_5b

    #@cc
    .line 110
    const-string v3, "FmRadioManager"

    #@ce
    new-instance v4, Ljava/lang/StringBuilder;

    #@d0
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@d3
    const-string v5, "Waiting to end turnOnRadio = "

    #@d5
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v4

    #@d9
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v4

    #@dd
    const-string v5, " time"

    #@df
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v4

    #@e3
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e6
    move-result-object v4

    #@e7
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ea
    .line 111
    iget v3, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->nState:I

    #@ec
    const/16 v4, 0xa

    #@ee
    if-eq v3, v4, :cond_5b

    #@f0
    .line 114
    invoke-static {v7, v8}, Landroid/os/SystemClock;->sleep(J)V

    #@f3
    .line 109
    add-int/lit8 v0, v0, 0x1

    #@f5
    goto :goto_c8

    #@f6
    .line 117
    :cond_f6
    const-string v2, "FmRadioManager"

    #@f8
    new-instance v4, Ljava/lang/StringBuilder;

    #@fa
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@fd
    const-string v5, "turnOnRadio() returned error :"

    #@ff
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@102
    move-result-object v4

    #@103
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@106
    move-result-object v4

    #@107
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10a
    move-result-object v4

    #@10b
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@10e
    move v2, v3

    #@10f
    .line 118
    goto/16 :goto_5b
.end method
