.class public abstract Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager$Stub;
.super Landroid/os/Binder;
.source "ILGSDEncManager.java"

# interfaces
.implements Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager$Stub$Proxy;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 18
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 19
    const-string v0, "com.lge.systemservice.core.lgsdencmanager.ILGSDEncManager"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 20
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 27
    if-nez p0, :cond_4

    #@2
    .line 28
    const/4 v0, 0x0

    #@3
    .line 34
    :goto_3
    return-object v0

    #@4
    .line 30
    :cond_4
    const-string v1, "com.lge.systemservice.core.lgsdencmanager.ILGSDEncManager"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 31
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 32
    check-cast v0, Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager;

    #@12
    goto :goto_3

    #@13
    .line 34
    :cond_13
    new-instance v0, Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 38
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 11
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 42
    sparse-switch p1, :sswitch_data_106

    #@5
    .line 166
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v4

    #@9
    :goto_9
    return v4

    #@a
    .line 46
    :sswitch_a
    const-string v3, "com.lge.systemservice.core.lgsdencmanager.ILGSDEncManager"

    #@c
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 51
    :sswitch_10
    const-string v3, "com.lge.systemservice.core.lgsdencmanager.ILGSDEncManager"

    #@12
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 53
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    .line 54
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager$Stub;->externalSDCardEnableEncryption(Ljava/lang/String;)I

    #@1c
    move-result v2

    #@1d
    .line 55
    .local v2, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@20
    .line 56
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@23
    goto :goto_9

    #@24
    .line 61
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v2           #_result:I
    :sswitch_24
    const-string v3, "com.lge.systemservice.core.lgsdencmanager.ILGSDEncManager"

    #@26
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@29
    .line 63
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2c
    move-result-object v0

    #@2d
    .line 64
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager$Stub;->externalSDCardDisableEncryption(Ljava/lang/String;)I

    #@30
    move-result v2

    #@31
    .line 65
    .restart local v2       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@34
    .line 66
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@37
    goto :goto_9

    #@38
    .line 71
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v2           #_result:I
    :sswitch_38
    const-string v3, "com.lge.systemservice.core.lgsdencmanager.ILGSDEncManager"

    #@3a
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3d
    .line 73
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@40
    move-result-object v0

    #@41
    .line 74
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager$Stub;->externalSDCardFullEnableEncryption(Ljava/lang/String;)I

    #@44
    move-result v2

    #@45
    .line 75
    .restart local v2       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@48
    .line 76
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@4b
    goto :goto_9

    #@4c
    .line 81
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v2           #_result:I
    :sswitch_4c
    const-string v3, "com.lge.systemservice.core.lgsdencmanager.ILGSDEncManager"

    #@4e
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@51
    .line 83
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@54
    move-result-object v0

    #@55
    .line 84
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager$Stub;->externalSDCardFullDisableEncryption(Ljava/lang/String;)I

    #@58
    move-result v2

    #@59
    .line 85
    .restart local v2       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5c
    .line 86
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@5f
    goto :goto_9

    #@60
    .line 91
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v2           #_result:I
    :sswitch_60
    const-string v3, "com.lge.systemservice.core.lgsdencmanager.ILGSDEncManager"

    #@62
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@65
    .line 93
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    #@68
    move-result-wide v0

    #@69
    .line 94
    .local v0, _arg0:J
    invoke-virtual {p0, v0, v1}, Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager$Stub;->externalSDCardFullTotalMemory(J)I

    #@6c
    move-result v2

    #@6d
    .line 95
    .restart local v2       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@70
    .line 96
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@73
    goto :goto_9

    #@74
    .line 101
    .end local v0           #_arg0:J
    .end local v2           #_result:I
    :sswitch_74
    const-string v3, "com.lge.systemservice.core.lgsdencmanager.ILGSDEncManager"

    #@76
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@79
    .line 102
    invoke-virtual {p0}, Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager$Stub;->externalSDCardMediaEnableEncryption()I

    #@7c
    move-result v2

    #@7d
    .line 103
    .restart local v2       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@80
    .line 104
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@83
    goto :goto_9

    #@84
    .line 109
    .end local v2           #_result:I
    :sswitch_84
    const-string v3, "com.lge.systemservice.core.lgsdencmanager.ILGSDEncManager"

    #@86
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@89
    .line 110
    invoke-virtual {p0}, Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager$Stub;->externalSDCardMediaDisableEncryption()I

    #@8c
    move-result v2

    #@8d
    .line 111
    .restart local v2       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@90
    .line 112
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@93
    goto/16 :goto_9

    #@95
    .line 117
    .end local v2           #_result:I
    :sswitch_95
    const-string v3, "com.lge.systemservice.core.lgsdencmanager.ILGSDEncManager"

    #@97
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9a
    .line 119
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@9d
    move-result-object v0

    #@9e
    .line 120
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager$Stub;->externalSDCardMountComplete(Ljava/lang/String;)I

    #@a1
    move-result v2

    #@a2
    .line 121
    .restart local v2       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@a5
    .line 122
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@a8
    goto/16 :goto_9

    #@aa
    .line 127
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v2           #_result:I
    :sswitch_aa
    const-string v3, "com.lge.systemservice.core.lgsdencmanager.ILGSDEncManager"

    #@ac
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@af
    .line 128
    invoke-virtual {p0}, Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager$Stub;->StorageEncryptionStatus()I

    #@b2
    move-result v2

    #@b3
    .line 129
    .restart local v2       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@b6
    .line 130
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@b9
    goto/16 :goto_9

    #@bb
    .line 135
    .end local v2           #_result:I
    :sswitch_bb
    const-string v3, "com.lge.systemservice.core.lgsdencmanager.ILGSDEncManager"

    #@bd
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c0
    .line 136
    invoke-virtual {p0}, Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager$Stub;->getInternalSDCardMountPath()Ljava/lang/String;

    #@c3
    move-result-object v2

    #@c4
    .line 137
    .local v2, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@c7
    .line 138
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@ca
    goto/16 :goto_9

    #@cc
    .line 143
    .end local v2           #_result:Ljava/lang/String;
    :sswitch_cc
    const-string v3, "com.lge.systemservice.core.lgsdencmanager.ILGSDEncManager"

    #@ce
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d1
    .line 144
    invoke-virtual {p0}, Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager$Stub;->getExternalSDCardMountPath()Ljava/lang/String;

    #@d4
    move-result-object v2

    #@d5
    .line 145
    .restart local v2       #_result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@d8
    .line 146
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@db
    goto/16 :goto_9

    #@dd
    .line 151
    .end local v2           #_result:Ljava/lang/String;
    :sswitch_dd
    const-string v5, "com.lge.systemservice.core.lgsdencmanager.ILGSDEncManager"

    #@df
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@e2
    .line 152
    invoke-virtual {p0}, Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager$Stub;->MDMStorageEncryptionStatus()Z

    #@e5
    move-result v2

    #@e6
    .line 153
    .local v2, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@e9
    .line 154
    if-eqz v2, :cond_ec

    #@eb
    move v3, v4

    #@ec
    :cond_ec
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@ef
    goto/16 :goto_9

    #@f1
    .line 159
    .end local v2           #_result:Z
    :sswitch_f1
    const-string v5, "com.lge.systemservice.core.lgsdencmanager.ILGSDEncManager"

    #@f3
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f6
    .line 160
    invoke-virtual {p0}, Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager$Stub;->getSDEncSupportStatus()Z

    #@f9
    move-result v2

    #@fa
    .line 161
    .restart local v2       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@fd
    .line 162
    if-eqz v2, :cond_100

    #@ff
    move v3, v4

    #@100
    :cond_100
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@103
    goto/16 :goto_9

    #@105
    .line 42
    nop

    #@106
    :sswitch_data_106
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_24
        0x3 -> :sswitch_38
        0x4 -> :sswitch_4c
        0x5 -> :sswitch_60
        0x6 -> :sswitch_74
        0x7 -> :sswitch_84
        0x8 -> :sswitch_95
        0x9 -> :sswitch_aa
        0xa -> :sswitch_bb
        0xb -> :sswitch_cc
        0xc -> :sswitch_dd
        0xd -> :sswitch_f1
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
