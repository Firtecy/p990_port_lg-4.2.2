.class Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub$Proxy;
.super Ljava/lang/Object;
.source "IBtLgeExtManager.java"

# interfaces
.implements Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 184
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 185
    iput-object p1, p0, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 186
    return-void
.end method


# virtual methods
.method public HCIDumpDisable()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 455
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 456
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 459
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.systemservice.core.btlgeextmanager.IBtLgeExtManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 460
    iget-object v3, p0, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0xf

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 461
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 462
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_23

    #@1b
    move-result v2

    #@1c
    .line 465
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 466
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 468
    return v2

    #@23
    .line 465
    .end local v2           #_result:I
    :catchall_23
    move-exception v3

    #@24
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 466
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    throw v3
.end method

.method public HCIDumpEnable()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 438
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 439
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 442
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.systemservice.core.btlgeextmanager.IBtLgeExtManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 443
    iget-object v3, p0, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0xe

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 444
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 445
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_23

    #@1b
    move-result v2

    #@1c
    .line 448
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 449
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 451
    return v2

    #@23
    .line 448
    .end local v2           #_result:I
    :catchall_23
    move-exception v3

    #@24
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 449
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    throw v3
.end method

.method public SSPDebugModeDisable()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 343
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 344
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 347
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.systemservice.core.btlgeextmanager.IBtLgeExtManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 348
    iget-object v3, p0, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x9

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 349
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 350
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_23

    #@1b
    move-result v2

    #@1c
    .line 353
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 354
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 356
    return v2

    #@23
    .line 353
    .end local v2           #_result:I
    :catchall_23
    move-exception v3

    #@24
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 354
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    throw v3
.end method

.method public SSPDebugModeEnable()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 326
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 327
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 330
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.systemservice.core.btlgeextmanager.IBtLgeExtManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 331
    iget-object v3, p0, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x8

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 332
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 333
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_23

    #@1b
    move-result v2

    #@1c
    .line 336
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 337
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 339
    return v2

    #@23
    .line 336
    .end local v2           #_result:I
    :catchall_23
    move-exception v3

    #@24
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 337
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    throw v3
.end method

.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 189
    iget-object v0, p0, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public endLeRxTest()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 418
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 419
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 422
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.systemservice.core.btlgeextmanager.IBtLgeExtManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 423
    iget-object v3, p0, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0xd

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 424
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 425
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_23

    #@1b
    move-result v2

    #@1c
    .line 428
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 429
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 431
    return v2

    #@23
    .line 428
    .end local v2           #_result:I
    :catchall_23
    move-exception v3

    #@24
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 429
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    throw v3
.end method

.method public endLeTxTest()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 401
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 402
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 405
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.systemservice.core.btlgeextmanager.IBtLgeExtManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 406
    iget-object v3, p0, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0xc

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 407
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 408
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_23

    #@1b
    move-result v2

    #@1c
    .line 411
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 412
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 414
    return v2

    #@23
    .line 411
    .end local v2           #_result:I
    :catchall_23
    move-exception v3

    #@24
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 412
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    throw v3
.end method

.method public enterBluetoothDUTMode(I)I
    .registers 8
    .parameter "index"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 219
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 220
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 223
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.systemservice.core.btlgeextmanager.IBtLgeExtManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 224
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 225
    iget-object v3, p0, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v4, 0x2

    #@13
    const/4 v5, 0x0

    #@14
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 226
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1a
    .line 227
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1d
    .catchall {:try_start_8 .. :try_end_1d} :catchall_25

    #@1d
    move-result v2

    #@1e
    .line 230
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 231
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 233
    return v2

    #@25
    .line 230
    .end local v2           #_result:I
    :catchall_25
    move-exception v3

    #@26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 231
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    throw v3
.end method

.method public enterBluetoothDUTModeBle(I)I
    .registers 8
    .parameter "channel"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 271
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 272
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 275
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.systemservice.core.btlgeextmanager.IBtLgeExtManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 276
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 277
    iget-object v3, p0, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v4, 0x5

    #@13
    const/4 v5, 0x0

    #@14
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 278
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1a
    .line 279
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1d
    .catchall {:try_start_8 .. :try_end_1d} :catchall_25

    #@1d
    move-result v2

    #@1e
    .line 282
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 283
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 285
    return v2

    #@25
    .line 282
    .end local v2           #_result:I
    :catchall_25
    move-exception v3

    #@26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 283
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    throw v3
.end method

.method public exitBluetoothDUTMode()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 237
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 238
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 241
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.systemservice.core.btlgeextmanager.IBtLgeExtManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 242
    iget-object v3, p0, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x3

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 243
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 244
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_22

    #@1a
    move-result v2

    #@1b
    .line 247
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 248
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 250
    return v2

    #@22
    .line 247
    .end local v2           #_result:I
    :catchall_22
    move-exception v3

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 248
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v3
.end method

.method public exitBluetoothDUTModeBle()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 289
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 290
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 293
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.systemservice.core.btlgeextmanager.IBtLgeExtManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 294
    iget-object v3, p0, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x6

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 295
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 296
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_22

    #@1a
    move-result v2

    #@1b
    .line 299
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 300
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 302
    return v2

    #@22
    .line 299
    .end local v2           #_result:I
    :catchall_22
    move-exception v3

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 300
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v3
.end method

.method public isBluetoothBleDUTMode()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 306
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 307
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 310
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.systemservice.core.btlgeextmanager.IBtLgeExtManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 311
    iget-object v3, p0, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x7

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 312
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 313
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_22

    #@1a
    move-result v2

    #@1b
    .line 316
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 317
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 319
    return v2

    #@22
    .line 316
    .end local v2           #_result:I
    :catchall_22
    move-exception v3

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 317
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v3
.end method

.method public isBluetoothDUTMode()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 254
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 255
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 258
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.systemservice.core.btlgeextmanager.IBtLgeExtManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 259
    iget-object v3, p0, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x4

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 260
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 261
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_22

    #@1a
    move-result v2

    #@1b
    .line 264
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 265
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 267
    return v2

    #@22
    .line 264
    .end local v2           #_result:I
    :catchall_22
    move-exception v3

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 265
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v3
.end method

.method public loadBluetoothAddress()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 199
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 200
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 203
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.systemservice.core.btlgeextmanager.IBtLgeExtManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 204
    iget-object v3, p0, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x1

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 205
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 206
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_22

    #@1a
    move-result v2

    #@1b
    .line 209
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 210
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 212
    return v2

    #@22
    .line 209
    .end local v2           #_result:I
    :catchall_22
    move-exception v3

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 210
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v3
.end method

.method public startLeReceiverTest(I)I
    .registers 8
    .parameter "rx_freq"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 383
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 384
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 387
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.systemservice.core.btlgeextmanager.IBtLgeExtManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 388
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 389
    iget-object v3, p0, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v4, 0xb

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 390
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 391
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1e
    .catchall {:try_start_8 .. :try_end_1e} :catchall_26

    #@1e
    move-result v2

    #@1f
    .line 394
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 395
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 397
    return v2

    #@26
    .line 394
    .end local v2           #_result:I
    :catchall_26
    move-exception v3

    #@27
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 395
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    throw v3
.end method

.method public startLeTransmitterTest(III)I
    .registers 10
    .parameter "tx_freq"
    .parameter "data_len"
    .parameter "pkt_type"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 363
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 364
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 367
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.systemservice.core.btlgeextmanager.IBtLgeExtManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 368
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 369
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 370
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 371
    iget-object v3, p0, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/16 v4, 0xa

    #@1a
    const/4 v5, 0x0

    #@1b
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 372
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@21
    .line 373
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_24
    .catchall {:try_start_8 .. :try_end_24} :catchall_2c

    #@24
    move-result v2

    #@25
    .line 376
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 377
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 379
    return v2

    #@2c
    .line 376
    .end local v2           #_result:I
    :catchall_2c
    move-exception v3

    #@2d
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 377
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@33
    throw v3
.end method
