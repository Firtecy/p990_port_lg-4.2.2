.class public Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;
.super Ljava/lang/Object;
.source "BtLgeExtManager.java"


# static fields
.field private static deathBinderNotificator:Landroid/os/IBinder$DeathRecipient;

.field private static final mLock:Ljava/lang/Object;

.field private static mService:Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager;


# instance fields
.field private mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private final mHandler:Landroid/os/Handler;

.field private mWaitTime:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 25
    const/4 v0, 0x0

    #@1
    sput-object v0, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->mService:Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager;

    #@3
    .line 27
    new-instance v0, Ljava/lang/Object;

    #@5
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@8
    sput-object v0, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->mLock:Ljava/lang/Object;

    #@a
    .line 505
    new-instance v0, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager$2;

    #@c
    invoke-direct {v0}, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager$2;-><init>()V

    #@f
    sput-object v0, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->deathBinderNotificator:Landroid/os/IBinder$DeathRecipient;

    #@11
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 39
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 43
    new-instance v0, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager$1;

    #@5
    invoke-direct {v0, p0}, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager$1;-><init>(Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;)V

    #@8
    iput-object v0, p0, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->mHandler:Landroid/os/Handler;

    #@a
    .line 40
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@d
    move-result-object v0

    #@e
    iput-object v0, p0, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@10
    .line 41
    return-void
.end method

.method static synthetic access$000()Ljava/lang/Object;
    .registers 1

    #@0
    .prologue
    .line 22
    sget-object v0, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->mLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;)Landroid/bluetooth/BluetoothAdapter;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 22
    iget-object v0, p0, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 22
    iget-object v0, p0, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 22
    iget v0, p0, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->mWaitTime:I

    #@2
    return v0
.end method

.method static synthetic access$310(Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;)I
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 22
    iget v0, p0, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->mWaitTime:I

    #@2
    add-int/lit8 v1, v0, -0x1

    #@4
    iput v1, p0, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->mWaitTime:I

    #@6
    return v0
.end method

.method static synthetic access$402(Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager;)Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager;
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 22
    sput-object p0, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->mService:Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager;

    #@2
    return-object p0
.end method

.method private static getService()Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager;
    .registers 5

    #@0
    .prologue
    .line 91
    sget-object v2, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->mService:Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager;

    #@2
    if-eqz v2, :cond_7

    #@4
    .line 92
    sget-object v2, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->mService:Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager;

    #@6
    .line 113
    .local v0, b:Landroid/os/IBinder;
    :goto_6
    return-object v2

    #@7
    .line 95
    .end local v0           #b:Landroid/os/IBinder;
    :cond_7
    const-string v2, "BtLgeExt"

    #@9
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@c
    move-result-object v0

    #@d
    .line 96
    .restart local v0       #b:Landroid/os/IBinder;
    invoke-static {v0}, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub;->asInterface(Landroid/os/IBinder;)Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager;

    #@10
    move-result-object v2

    #@11
    sput-object v2, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->mService:Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager;

    #@13
    .line 98
    sget-object v2, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->mService:Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager;

    #@15
    if-nez v2, :cond_19

    #@17
    .line 99
    const/4 v2, 0x0

    #@18
    goto :goto_6

    #@19
    .line 108
    :cond_19
    :try_start_19
    sget-object v2, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->mService:Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager;

    #@1b
    invoke-interface {v2}, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager;->asBinder()Landroid/os/IBinder;

    #@1e
    move-result-object v2

    #@1f
    sget-object v3, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->deathBinderNotificator:Landroid/os/IBinder$DeathRecipient;

    #@21
    const/4 v4, 0x0

    #@22
    invoke-interface {v2, v3, v4}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_25
    .catch Landroid/os/RemoteException; {:try_start_19 .. :try_end_25} :catch_28

    #@25
    .line 113
    :goto_25
    sget-object v2, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->mService:Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager;

    #@27
    goto :goto_6

    #@28
    .line 109
    :catch_28
    move-exception v1

    #@29
    .line 111
    .local v1, e:Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    #@2c
    goto :goto_25
.end method


# virtual methods
.method checkIfBluetoothIsOn(II)Z
    .registers 7
    .parameter "mode"
    .parameter "index"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 75
    iget-object v1, p0, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@3
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_27

    #@9
    .line 76
    const-string v1, "BtLgeExtManager"

    #@b
    const-string v2, "[BTUI] checkIfBluetoothIsOn : oops.. BT OFF first"

    #@d
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 77
    const/16 v1, 0x14

    #@12
    iput v1, p0, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->mWaitTime:I

    #@14
    .line 78
    iget-object v1, p0, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@16
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    #@19
    .line 79
    iget-object v1, p0, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->mHandler:Landroid/os/Handler;

    #@1b
    iget-object v2, p0, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->mHandler:Landroid/os/Handler;

    #@1d
    invoke-virtual {v2, v0, p1, p2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    #@20
    move-result-object v0

    #@21
    const-wide/16 v2, 0x12c

    #@23
    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@26
    .line 81
    const/4 v0, 0x0

    #@27
    .line 83
    :cond_27
    return v0
.end method

.method public enterBluetoothDUTMode(I)I
    .registers 7
    .parameter "index"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 153
    const/16 v3, 0xa

    #@3
    invoke-virtual {p0, v3, p1}, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->checkIfBluetoothIsOn(II)Z

    #@6
    move-result v3

    #@7
    if-nez v3, :cond_b

    #@9
    .line 154
    const/4 v2, 0x0

    #@a
    .line 173
    :goto_a
    return v2

    #@b
    .line 159
    :cond_b
    :try_start_b
    invoke-static {}, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->getService()Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager;

    #@e
    move-result-object v1

    #@f
    .line 160
    .local v1, mCurService:Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager;
    if-eqz v1, :cond_16

    #@11
    .line 161
    invoke-interface {v1, p1}, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager;->enterBluetoothDUTMode(I)I

    #@14
    move-result v2

    #@15
    goto :goto_a

    #@16
    .line 163
    :cond_16
    const-string v3, "BtLgeExtManager"

    #@18
    const-string v4, "[BTUI] enterBluetoothDUTMode fail. mCurService is null"

    #@1a
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1d
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_1d} :catch_1e

    #@1d
    goto :goto_a

    #@1e
    .line 170
    .end local v1           #mCurService:Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager;
    :catch_1e
    move-exception v0

    #@1f
    .line 171
    .local v0, e:Landroid/os/RemoteException;
    const-string v3, "BtLgeExtManager"

    #@21
    const-string v4, "[BTUI] enterBluetoothDUTMode fail"

    #@23
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    goto :goto_a
.end method

.method public enterBluetoothDUTModeBle(I)I
    .registers 7
    .parameter "channel"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 226
    const/16 v3, 0xb

    #@3
    invoke-virtual {p0, v3, p1}, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->checkIfBluetoothIsOn(II)Z

    #@6
    move-result v3

    #@7
    if-nez v3, :cond_b

    #@9
    .line 227
    const/4 v2, 0x0

    #@a
    .line 246
    :goto_a
    return v2

    #@b
    .line 232
    :cond_b
    :try_start_b
    invoke-static {}, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->getService()Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager;

    #@e
    move-result-object v1

    #@f
    .line 233
    .local v1, mCurService:Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager;
    if-eqz v1, :cond_16

    #@11
    .line 234
    invoke-interface {v1, p1}, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager;->enterBluetoothDUTModeBle(I)I

    #@14
    move-result v2

    #@15
    goto :goto_a

    #@16
    .line 236
    :cond_16
    const-string v3, "BtLgeExtManager"

    #@18
    const-string v4, "[BTUI] enterBluetoothDUTModeBle fail. mCurService is null"

    #@1a
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1d
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_1d} :catch_1e

    #@1d
    goto :goto_a

    #@1e
    .line 243
    .end local v1           #mCurService:Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager;
    :catch_1e
    move-exception v0

    #@1f
    .line 244
    .local v0, e:Landroid/os/RemoteException;
    const-string v3, "BtLgeExtManager"

    #@21
    const-string v4, "[BTUI] enterBluetoothDUTModeBle fail"

    #@23
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    goto :goto_a
.end method
