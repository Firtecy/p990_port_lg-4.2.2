.class synthetic Lcom/lge/systemservice/core/LGContextImpl$19;
.super Ljava/lang/Object;
.source "LGContextImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/core/LGContextImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$lge$systemservice$core$LGContext$ServiceList:[I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 89
    invoke-static {}, Lcom/lge/systemservice/core/LGContext$ServiceList;->values()[Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@3
    move-result-object v0

    #@4
    array-length v0, v0

    #@5
    new-array v0, v0, [I

    #@7
    sput-object v0, Lcom/lge/systemservice/core/LGContextImpl$19;->$SwitchMap$com$lge$systemservice$core$LGContext$ServiceList:[I

    #@9
    :try_start_9
    sget-object v0, Lcom/lge/systemservice/core/LGContextImpl$19;->$SwitchMap$com$lge$systemservice$core$LGContext$ServiceList:[I

    #@b
    sget-object v1, Lcom/lge/systemservice/core/LGContext$ServiceList;->BYEWORLD_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@d
    invoke-virtual {v1}, Lcom/lge/systemservice/core/LGContext$ServiceList;->ordinal()I

    #@10
    move-result v1

    #@11
    const/4 v2, 0x1

    #@12
    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_14} :catch_e6

    #@14
    :goto_14
    :try_start_14
    sget-object v0, Lcom/lge/systemservice/core/LGContextImpl$19;->$SwitchMap$com$lge$systemservice$core$LGContext$ServiceList:[I

    #@16
    sget-object v1, Lcom/lge/systemservice/core/LGContext$ServiceList;->WATCHNETSTORAGE_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@18
    invoke-virtual {v1}, Lcom/lge/systemservice/core/LGContext$ServiceList;->ordinal()I

    #@1b
    move-result v1

    #@1c
    const/4 v2, 0x2

    #@1d
    aput v2, v0, v1
    :try_end_1f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_1f} :catch_e3

    #@1f
    :goto_1f
    :try_start_1f
    sget-object v0, Lcom/lge/systemservice/core/LGContextImpl$19;->$SwitchMap$com$lge$systemservice$core$LGContext$ServiceList:[I

    #@21
    sget-object v1, Lcom/lge/systemservice/core/LGContext$ServiceList;->EMOTIONALLED_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@23
    invoke-virtual {v1}, Lcom/lge/systemservice/core/LGContext$ServiceList;->ordinal()I

    #@26
    move-result v1

    #@27
    const/4 v2, 0x3

    #@28
    aput v2, v0, v1
    :try_end_2a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1f .. :try_end_2a} :catch_e0

    #@2a
    :goto_2a
    :try_start_2a
    sget-object v0, Lcom/lge/systemservice/core/LGContextImpl$19;->$SwitchMap$com$lge$systemservice$core$LGContext$ServiceList:[I

    #@2c
    sget-object v1, Lcom/lge/systemservice/core/LGContext$ServiceList;->LGSDENC_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@2e
    invoke-virtual {v1}, Lcom/lge/systemservice/core/LGContext$ServiceList;->ordinal()I

    #@31
    move-result v1

    #@32
    const/4 v2, 0x4

    #@33
    aput v2, v0, v1
    :try_end_35
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2a .. :try_end_35} :catch_dd

    #@35
    :goto_35
    :try_start_35
    sget-object v0, Lcom/lge/systemservice/core/LGContextImpl$19;->$SwitchMap$com$lge$systemservice$core$LGContext$ServiceList:[I

    #@37
    sget-object v1, Lcom/lge/systemservice/core/LGContext$ServiceList;->AAT_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@39
    invoke-virtual {v1}, Lcom/lge/systemservice/core/LGContext$ServiceList;->ordinal()I

    #@3c
    move-result v1

    #@3d
    const/4 v2, 0x5

    #@3e
    aput v2, v0, v1
    :try_end_40
    .catch Ljava/lang/NoSuchFieldError; {:try_start_35 .. :try_end_40} :catch_da

    #@40
    :goto_40
    :try_start_40
    sget-object v0, Lcom/lge/systemservice/core/LGContextImpl$19;->$SwitchMap$com$lge$systemservice$core$LGContext$ServiceList:[I

    #@42
    sget-object v1, Lcom/lge/systemservice/core/LGContext$ServiceList;->VOLUMEVIBRATOR_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@44
    invoke-virtual {v1}, Lcom/lge/systemservice/core/LGContext$ServiceList;->ordinal()I

    #@47
    move-result v1

    #@48
    const/4 v2, 0x6

    #@49
    aput v2, v0, v1
    :try_end_4b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_40 .. :try_end_4b} :catch_d7

    #@4b
    :goto_4b
    :try_start_4b
    sget-object v0, Lcom/lge/systemservice/core/LGContextImpl$19;->$SwitchMap$com$lge$systemservice$core$LGContext$ServiceList:[I

    #@4d
    sget-object v1, Lcom/lge/systemservice/core/LGContext$ServiceList;->WIFILGEEXT_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@4f
    invoke-virtual {v1}, Lcom/lge/systemservice/core/LGContext$ServiceList;->ordinal()I

    #@52
    move-result v1

    #@53
    const/4 v2, 0x7

    #@54
    aput v2, v0, v1
    :try_end_56
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4b .. :try_end_56} :catch_d5

    #@56
    :goto_56
    :try_start_56
    sget-object v0, Lcom/lge/systemservice/core/LGContextImpl$19;->$SwitchMap$com$lge$systemservice$core$LGContext$ServiceList:[I

    #@58
    sget-object v1, Lcom/lge/systemservice/core/LGContext$ServiceList;->BLUETOOTH_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@5a
    invoke-virtual {v1}, Lcom/lge/systemservice/core/LGContext$ServiceList;->ordinal()I

    #@5d
    move-result v1

    #@5e
    const/16 v2, 0x8

    #@60
    aput v2, v0, v1
    :try_end_62
    .catch Ljava/lang/NoSuchFieldError; {:try_start_56 .. :try_end_62} :catch_d3

    #@62
    :goto_62
    :try_start_62
    sget-object v0, Lcom/lge/systemservice/core/LGContextImpl$19;->$SwitchMap$com$lge$systemservice$core$LGContext$ServiceList:[I

    #@64
    sget-object v1, Lcom/lge/systemservice/core/LGContext$ServiceList;->FELICA_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@66
    invoke-virtual {v1}, Lcom/lge/systemservice/core/LGContext$ServiceList;->ordinal()I

    #@69
    move-result v1

    #@6a
    const/16 v2, 0x9

    #@6c
    aput v2, v0, v1
    :try_end_6e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_62 .. :try_end_6e} :catch_d1

    #@6e
    :goto_6e
    :try_start_6e
    sget-object v0, Lcom/lge/systemservice/core/LGContextImpl$19;->$SwitchMap$com$lge$systemservice$core$LGContext$ServiceList:[I

    #@70
    sget-object v1, Lcom/lge/systemservice/core/LGContext$ServiceList;->LGEWIFIDISPLAY_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@72
    invoke-virtual {v1}, Lcom/lge/systemservice/core/LGContext$ServiceList;->ordinal()I

    #@75
    move-result v1

    #@76
    const/16 v2, 0xa

    #@78
    aput v2, v0, v1
    :try_end_7a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6e .. :try_end_7a} :catch_cf

    #@7a
    :goto_7a
    :try_start_7a
    sget-object v0, Lcom/lge/systemservice/core/LGContextImpl$19;->$SwitchMap$com$lge$systemservice$core$LGContext$ServiceList:[I

    #@7c
    sget-object v1, Lcom/lge/systemservice/core/LGContext$ServiceList;->QCTWIFIDISPLAY_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@7e
    invoke-virtual {v1}, Lcom/lge/systemservice/core/LGContext$ServiceList;->ordinal()I

    #@81
    move-result v1

    #@82
    const/16 v2, 0xb

    #@84
    aput v2, v0, v1
    :try_end_86
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7a .. :try_end_86} :catch_cd

    #@86
    :goto_86
    :try_start_86
    sget-object v0, Lcom/lge/systemservice/core/LGContextImpl$19;->$SwitchMap$com$lge$systemservice$core$LGContext$ServiceList:[I

    #@88
    sget-object v1, Lcom/lge/systemservice/core/LGContext$ServiceList;->NFCLG_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@8a
    invoke-virtual {v1}, Lcom/lge/systemservice/core/LGContext$ServiceList;->ordinal()I

    #@8d
    move-result v1

    #@8e
    const/16 v2, 0xc

    #@90
    aput v2, v0, v1
    :try_end_92
    .catch Ljava/lang/NoSuchFieldError; {:try_start_86 .. :try_end_92} :catch_cb

    #@92
    :goto_92
    :try_start_92
    sget-object v0, Lcom/lge/systemservice/core/LGContextImpl$19;->$SwitchMap$com$lge$systemservice$core$LGContext$ServiceList:[I

    #@94
    sget-object v1, Lcom/lge/systemservice/core/LGContext$ServiceList;->SECURECLOCK_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@96
    invoke-virtual {v1}, Lcom/lge/systemservice/core/LGContext$ServiceList;->ordinal()I

    #@99
    move-result v1

    #@9a
    const/16 v2, 0xd

    #@9c
    aput v2, v0, v1
    :try_end_9e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_92 .. :try_end_9e} :catch_c9

    #@9e
    :goto_9e
    :try_start_9e
    sget-object v0, Lcom/lge/systemservice/core/LGContextImpl$19;->$SwitchMap$com$lge$systemservice$core$LGContext$ServiceList:[I

    #@a0
    sget-object v1, Lcom/lge/systemservice/core/LGContext$ServiceList;->CORECONTROL_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@a2
    invoke-virtual {v1}, Lcom/lge/systemservice/core/LGContext$ServiceList;->ordinal()I

    #@a5
    move-result v1

    #@a6
    const/16 v2, 0xe

    #@a8
    aput v2, v0, v1
    :try_end_aa
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9e .. :try_end_aa} :catch_c7

    #@aa
    :goto_aa
    :try_start_aa
    sget-object v0, Lcom/lge/systemservice/core/LGContextImpl$19;->$SwitchMap$com$lge$systemservice$core$LGContext$ServiceList:[I

    #@ac
    sget-object v1, Lcom/lge/systemservice/core/LGContext$ServiceList;->WIFILGEVZW_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@ae
    invoke-virtual {v1}, Lcom/lge/systemservice/core/LGContext$ServiceList;->ordinal()I

    #@b1
    move-result v1

    #@b2
    const/16 v2, 0xf

    #@b4
    aput v2, v0, v1
    :try_end_b6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_aa .. :try_end_b6} :catch_c5

    #@b6
    :goto_b6
    :try_start_b6
    sget-object v0, Lcom/lge/systemservice/core/LGContextImpl$19;->$SwitchMap$com$lge$systemservice$core$LGContext$ServiceList:[I

    #@b8
    sget-object v1, Lcom/lge/systemservice/core/LGContext$ServiceList;->LGECopyLog_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@ba
    invoke-virtual {v1}, Lcom/lge/systemservice/core/LGContext$ServiceList;->ordinal()I

    #@bd
    move-result v1

    #@be
    const/16 v2, 0x10

    #@c0
    aput v2, v0, v1
    :try_end_c2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b6 .. :try_end_c2} :catch_c3

    #@c2
    :goto_c2
    return-void

    #@c3
    :catch_c3
    move-exception v0

    #@c4
    goto :goto_c2

    #@c5
    :catch_c5
    move-exception v0

    #@c6
    goto :goto_b6

    #@c7
    :catch_c7
    move-exception v0

    #@c8
    goto :goto_aa

    #@c9
    :catch_c9
    move-exception v0

    #@ca
    goto :goto_9e

    #@cb
    :catch_cb
    move-exception v0

    #@cc
    goto :goto_92

    #@cd
    :catch_cd
    move-exception v0

    #@ce
    goto :goto_86

    #@cf
    :catch_cf
    move-exception v0

    #@d0
    goto :goto_7a

    #@d1
    :catch_d1
    move-exception v0

    #@d2
    goto :goto_6e

    #@d3
    :catch_d3
    move-exception v0

    #@d4
    goto :goto_62

    #@d5
    :catch_d5
    move-exception v0

    #@d6
    goto :goto_56

    #@d7
    :catch_d7
    move-exception v0

    #@d8
    goto/16 :goto_4b

    #@da
    :catch_da
    move-exception v0

    #@db
    goto/16 :goto_40

    #@dd
    :catch_dd
    move-exception v0

    #@de
    goto/16 :goto_35

    #@e0
    :catch_e0
    move-exception v0

    #@e1
    goto/16 :goto_2a

    #@e3
    :catch_e3
    move-exception v0

    #@e4
    goto/16 :goto_1f

    #@e6
    :catch_e6
    move-exception v0

    #@e7
    goto/16 :goto_14
.end method
