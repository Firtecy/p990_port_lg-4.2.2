.class Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;
.super Ljava/lang/Thread;
.source "NfcLgFactoryAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ProgressThread"
.end annotation


# instance fields
.field public mResult:Z

.field mThreadHandler:Landroid/os/Handler;

.field final mainLock:Ljava/util/concurrent/locks/Lock;

.field final mainblockingCondition:Ljava/util/concurrent/locks/Condition;

.field final synthetic this$0:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;


# direct methods
.method private constructor <init>(Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 819
    iput-object p1, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->this$0:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;

    #@2
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@5
    .line 820
    const/4 v0, 0x0

    #@6
    iput-boolean v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->mResult:Z

    #@8
    .line 821
    const/4 v0, 0x0

    #@9
    iput-object v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->mThreadHandler:Landroid/os/Handler;

    #@b
    .line 822
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    #@d
    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    #@10
    iput-object v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->mainLock:Ljava/util/concurrent/locks/Lock;

    #@12
    .line 823
    iget-object v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->mainLock:Ljava/util/concurrent/locks/Lock;

    #@14
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    #@17
    move-result-object v0

    #@18
    iput-object v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->mainblockingCondition:Ljava/util/concurrent/locks/Condition;

    #@1a
    return-void
.end method

.method synthetic constructor <init>(Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 819
    invoke-direct {p0, p1}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;-><init>(Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;)V

    #@3
    return-void
.end method


# virtual methods
.method public run()V
    .registers 3

    #@0
    .prologue
    .line 826
    invoke-static {}, Landroid/os/Looper;->prepare()V

    #@3
    .line 828
    const-string v0, "NfcFactoryAdapter"

    #@5
    const-string v1, "@@NfcTestThread::Thread Run"

    #@7
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 829
    new-instance v0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread$1;

    #@c
    invoke-direct {v0, p0}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread$1;-><init>(Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;)V

    #@f
    iput-object v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->mThreadHandler:Landroid/os/Handler;

    #@11
    .line 912
    iget-object v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->mThreadHandler:Landroid/os/Handler;

    #@13
    if-eqz v0, :cond_18

    #@15
    .line 913
    invoke-static {}, Landroid/os/Looper;->loop()V

    #@18
    .line 915
    :cond_18
    const-string v0, "NfcFactoryAdapter"

    #@1a
    const-string v1, "ProgressThread::Thread stop"

    #@1c
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 916
    return-void
.end method

.method public final declared-synchronized sendMsg2Thread(I[B)I
    .registers 12
    .parameter "what"
    .parameter "arg"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, -0x1

    #@2
    .line 919
    monitor-enter p0

    #@3
    :try_start_3
    const-string v5, "NfcFactoryAdapter"

    #@5
    new-instance v6, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v7, "sendMsg2Thread::In==>"

    #@c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v6

    #@10
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v6

    #@14
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v6

    #@18
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 921
    iget-object v5, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->mThreadHandler:Landroid/os/Handler;

    #@1d
    if-nez v5, :cond_28

    #@1f
    .line 922
    const-string v4, "NfcFactoryAdapter"

    #@21
    const-string v5, "sendMsg2Thread::mThreadHandler is NULL"

    #@23
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_26
    .catchall {:try_start_3 .. :try_end_26} :catchall_88

    #@26
    .line 956
    :goto_26
    monitor-exit p0

    #@27
    return v3

    #@28
    .line 925
    :cond_28
    :try_start_28
    iget-object v5, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->this$0:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;

    #@2a
    invoke-virtual {v5}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->ResetRetVal()V

    #@2d
    .line 926
    new-instance v0, Landroid/os/Bundle;

    #@2f
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@32
    .line 927
    .local v0, bundle:Landroid/os/Bundle;
    const-string v5, "param1"

    #@34
    const/4 v6, 0x0

    #@35
    aget-byte v6, p2, v6

    #@37
    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putByte(Ljava/lang/String;B)V

    #@3a
    .line 928
    const-string v5, "param2"

    #@3c
    const/4 v6, 0x1

    #@3d
    aget-byte v6, p2, v6

    #@3f
    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putByte(Ljava/lang/String;B)V

    #@42
    .line 929
    const-string v5, "param3"

    #@44
    const/4 v6, 0x2

    #@45
    aget-byte v6, p2, v6

    #@47
    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putByte(Ljava/lang/String;B)V

    #@4a
    .line 930
    const-string v5, "param4"

    #@4c
    const/4 v6, 0x3

    #@4d
    aget-byte v6, p2, v6

    #@4f
    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putByte(Ljava/lang/String;B)V

    #@52
    .line 931
    const-string v5, "param5"

    #@54
    const/4 v6, 0x4

    #@55
    aget-byte v6, p2, v6

    #@57
    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putByte(Ljava/lang/String;B)V

    #@5a
    .line 933
    iget-object v5, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->mThreadHandler:Landroid/os/Handler;

    #@5c
    invoke-virtual {v5, p1, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@5f
    move-result-object v2

    #@60
    .line 934
    .local v2, msg:Landroid/os/Message;
    if-eqz v2, :cond_8b

    #@62
    .line 935
    invoke-virtual {v2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@65
    .line 936
    iget-object v5, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->mThreadHandler:Landroid/os/Handler;

    #@67
    invoke-virtual {v5, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@6a
    .line 942
    iget-object v5, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->mainLock:Ljava/util/concurrent/locks/Lock;

    #@6c
    invoke-interface {v5}, Ljava/util/concurrent/locks/Lock;->lock()V
    :try_end_6f
    .catchall {:try_start_28 .. :try_end_6f} :catchall_88

    #@6f
    .line 944
    :try_start_6f
    iget-object v5, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->mainblockingCondition:Ljava/util/concurrent/locks/Condition;

    #@71
    const-wide/16 v6, 0x2710

    #@73
    sget-object v8, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    #@75
    invoke-interface {v5, v6, v7, v8}, Ljava/util/concurrent/locks/Condition;->await(JLjava/util/concurrent/TimeUnit;)Z

    #@78
    move-result v5

    #@79
    if-nez v5, :cond_93

    #@7b
    .line 945
    const-string v5, "NfcFactoryAdapter"

    #@7d
    const-string v6, "blockingCondition.await timeout"

    #@7f
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_82
    .catchall {:try_start_6f .. :try_end_82} :catchall_b9
    .catch Ljava/lang/InterruptedException; {:try_start_6f .. :try_end_82} :catch_9a

    #@82
    .line 953
    :try_start_82
    iget-object v4, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->mainLock:Ljava/util/concurrent/locks/Lock;

    #@84
    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V
    :try_end_87
    .catchall {:try_start_82 .. :try_end_87} :catchall_88

    #@87
    goto :goto_26

    #@88
    .line 919
    .end local v0           #bundle:Landroid/os/Bundle;
    .end local v2           #msg:Landroid/os/Message;
    :catchall_88
    move-exception v3

    #@89
    monitor-exit p0

    #@8a
    throw v3

    #@8b
    .line 939
    .restart local v0       #bundle:Landroid/os/Bundle;
    .restart local v2       #msg:Landroid/os/Message;
    :cond_8b
    :try_start_8b
    const-string v4, "NfcFactoryAdapter"

    #@8d
    const-string v5, "sendMsg2Thread::Handler is NULL"

    #@8f
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@92
    goto :goto_26

    #@93
    .line 953
    :cond_93
    iget-object v3, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->mainLock:Ljava/util/concurrent/locks/Lock;

    #@95
    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->unlock()V
    :try_end_98
    .catchall {:try_start_8b .. :try_end_98} :catchall_88

    #@98
    :goto_98
    move v3, v4

    #@99
    .line 956
    goto :goto_26

    #@9a
    .line 949
    :catch_9a
    move-exception v1

    #@9b
    .line 950
    .local v1, e:Ljava/lang/InterruptedException;
    :try_start_9b
    const-string v3, "NfcFactoryAdapter"

    #@9d
    new-instance v5, Ljava/lang/StringBuilder;

    #@9f
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@a2
    const-string v6, "sendMsg2Thread::InterruptedException !!"

    #@a4
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v5

    #@a8
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v5

    #@ac
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@af
    move-result-object v5

    #@b0
    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b3
    .catchall {:try_start_9b .. :try_end_b3} :catchall_b9

    #@b3
    .line 953
    :try_start_b3
    iget-object v3, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->mainLock:Ljava/util/concurrent/locks/Lock;

    #@b5
    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->unlock()V

    #@b8
    goto :goto_98

    #@b9
    .end local v1           #e:Ljava/lang/InterruptedException;
    :catchall_b9
    move-exception v3

    #@ba
    iget-object v4, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->mainLock:Ljava/util/concurrent/locks/Lock;

    #@bc
    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V

    #@bf
    throw v3
    :try_end_c0
    .catchall {:try_start_b3 .. :try_end_c0} :catchall_88
.end method

.method public final declared-synchronized stopThread()V
    .registers 3

    #@0
    .prologue
    .line 960
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->mThreadHandler:Landroid/os/Handler;

    #@3
    if-eqz v0, :cond_18

    #@5
    .line 961
    const-string v0, "NfcFactoryAdapter"

    #@7
    const-string v1, "stopThread::request looper.quit"

    #@9
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 962
    iget-object v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->mThreadHandler:Landroid/os/Handler;

    #@e
    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    #@15
    .line 963
    const/4 v0, 0x0

    #@16
    iput-object v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->mThreadHandler:Landroid/os/Handler;
    :try_end_18
    .catchall {:try_start_1 .. :try_end_18} :catchall_1a

    #@18
    .line 965
    :cond_18
    monitor-exit p0

    #@19
    return-void

    #@1a
    .line 960
    :catchall_1a
    move-exception v0

    #@1b
    monitor-exit p0

    #@1c
    throw v0
.end method
