.class Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager$Stub$Proxy;
.super Ljava/lang/Object;
.source "ILGSDEncManager.java"

# interfaces
.implements Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 172
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 173
    iput-object p1, p0, Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 174
    return-void
.end method


# virtual methods
.method public MDMStorageEncryptionStatus()Z
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 385
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 386
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 389
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.lge.systemservice.core.lgsdencmanager.ILGSDEncManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 390
    iget-object v3, p0, Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@10
    const/16 v4, 0xc

    #@12
    const/4 v5, 0x0

    #@13
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@16
    .line 391
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@19
    .line 392
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1c
    .catchall {:try_start_9 .. :try_end_1c} :catchall_27

    #@1c
    move-result v3

    #@1d
    if-eqz v3, :cond_20

    #@1f
    const/4 v2, 0x1

    #@20
    .line 395
    .local v2, _result:Z
    :cond_20
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 396
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 398
    return v2

    #@27
    .line 395
    .end local v2           #_result:Z
    :catchall_27
    move-exception v3

    #@28
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 396
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    throw v3
.end method

.method public StorageEncryptionStatus()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 334
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 335
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 338
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.systemservice.core.lgsdencmanager.ILGSDEncManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 339
    iget-object v3, p0, Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x9

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 340
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 341
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_23

    #@1b
    move-result v2

    #@1c
    .line 344
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 345
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 347
    return v2

    #@23
    .line 344
    .end local v2           #_result:I
    :catchall_23
    move-exception v3

    #@24
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 345
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    throw v3
.end method

.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 177
    iget-object v0, p0, Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public externalSDCardDisableEncryption(Ljava/lang/String;)I
    .registers 8
    .parameter "path"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 203
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 204
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 207
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.systemservice.core.lgsdencmanager.ILGSDEncManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 208
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 209
    iget-object v3, p0, Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v4, 0x2

    #@13
    const/4 v5, 0x0

    #@14
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 210
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1a
    .line 211
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1d
    .catchall {:try_start_8 .. :try_end_1d} :catchall_25

    #@1d
    move-result v2

    #@1e
    .line 214
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 215
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 217
    return v2

    #@25
    .line 214
    .end local v2           #_result:I
    :catchall_25
    move-exception v3

    #@26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 215
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    throw v3
.end method

.method public externalSDCardEnableEncryption(Ljava/lang/String;)I
    .registers 8
    .parameter "path"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 185
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 186
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 189
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.systemservice.core.lgsdencmanager.ILGSDEncManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 190
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 191
    iget-object v3, p0, Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v4, 0x1

    #@13
    const/4 v5, 0x0

    #@14
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 192
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1a
    .line 193
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1d
    .catchall {:try_start_8 .. :try_end_1d} :catchall_25

    #@1d
    move-result v2

    #@1e
    .line 196
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 197
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 199
    return v2

    #@25
    .line 196
    .end local v2           #_result:I
    :catchall_25
    move-exception v3

    #@26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 197
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    throw v3
.end method

.method public externalSDCardFullDisableEncryption(Ljava/lang/String;)I
    .registers 8
    .parameter "path"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 241
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 242
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 245
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.systemservice.core.lgsdencmanager.ILGSDEncManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 246
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 247
    iget-object v3, p0, Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v4, 0x4

    #@13
    const/4 v5, 0x0

    #@14
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 248
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1a
    .line 249
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1d
    .catchall {:try_start_8 .. :try_end_1d} :catchall_25

    #@1d
    move-result v2

    #@1e
    .line 252
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 253
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 255
    return v2

    #@25
    .line 252
    .end local v2           #_result:I
    :catchall_25
    move-exception v3

    #@26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 253
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    throw v3
.end method

.method public externalSDCardFullEnableEncryption(Ljava/lang/String;)I
    .registers 8
    .parameter "path"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 223
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 224
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 227
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.systemservice.core.lgsdencmanager.ILGSDEncManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 228
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 229
    iget-object v3, p0, Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v4, 0x3

    #@13
    const/4 v5, 0x0

    #@14
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 230
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1a
    .line 231
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1d
    .catchall {:try_start_8 .. :try_end_1d} :catchall_25

    #@1d
    move-result v2

    #@1e
    .line 234
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 235
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 237
    return v2

    #@25
    .line 234
    .end local v2           #_result:I
    :catchall_25
    move-exception v3

    #@26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 235
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    throw v3
.end method

.method public externalSDCardFullTotalMemory(J)I
    .registers 9
    .parameter "totalMemory"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 259
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 260
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 263
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.systemservice.core.lgsdencmanager.ILGSDEncManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 264
    invoke-virtual {v0, p1, p2}, Landroid/os/Parcel;->writeLong(J)V

    #@10
    .line 265
    iget-object v3, p0, Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v4, 0x5

    #@13
    const/4 v5, 0x0

    #@14
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 266
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1a
    .line 267
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1d
    .catchall {:try_start_8 .. :try_end_1d} :catchall_25

    #@1d
    move-result v2

    #@1e
    .line 270
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 271
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 273
    return v2

    #@25
    .line 270
    .end local v2           #_result:I
    :catchall_25
    move-exception v3

    #@26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 271
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    throw v3
.end method

.method public externalSDCardMediaDisableEncryption()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 297
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 298
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 301
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.systemservice.core.lgsdencmanager.ILGSDEncManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 302
    iget-object v3, p0, Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x7

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 303
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 304
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_22

    #@1a
    move-result v2

    #@1b
    .line 307
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 308
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 310
    return v2

    #@22
    .line 307
    .end local v2           #_result:I
    :catchall_22
    move-exception v3

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 308
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v3
.end method

.method public externalSDCardMediaEnableEncryption()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 280
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 281
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 284
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.systemservice.core.lgsdencmanager.ILGSDEncManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 285
    iget-object v3, p0, Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x6

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 286
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 287
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_22

    #@1a
    move-result v2

    #@1b
    .line 290
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 291
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 293
    return v2

    #@22
    .line 290
    .end local v2           #_result:I
    :catchall_22
    move-exception v3

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 291
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v3
.end method

.method public externalSDCardMountComplete(Ljava/lang/String;)I
    .registers 8
    .parameter "path"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 316
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 317
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 320
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.systemservice.core.lgsdencmanager.ILGSDEncManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 321
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 322
    iget-object v3, p0, Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v4, 0x8

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 323
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 324
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1e
    .catchall {:try_start_8 .. :try_end_1e} :catchall_26

    #@1e
    move-result v2

    #@1f
    .line 327
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 328
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 330
    return v2

    #@26
    .line 327
    .end local v2           #_result:I
    :catchall_26
    move-exception v3

    #@27
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 328
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    throw v3
.end method

.method public getExternalSDCardMountPath()Ljava/lang/String;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 368
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 369
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 372
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.systemservice.core.lgsdencmanager.ILGSDEncManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 373
    iget-object v3, p0, Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0xb

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 374
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 375
    invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_23

    #@1b
    move-result-object v2

    #@1c
    .line 378
    .local v2, _result:Ljava/lang/String;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 379
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 381
    return-object v2

    #@23
    .line 378
    .end local v2           #_result:Ljava/lang/String;
    :catchall_23
    move-exception v3

    #@24
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 379
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    throw v3
.end method

.method public getInternalSDCardMountPath()Ljava/lang/String;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 351
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 352
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 355
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.systemservice.core.lgsdencmanager.ILGSDEncManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 356
    iget-object v3, p0, Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0xa

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 357
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 358
    invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_23

    #@1b
    move-result-object v2

    #@1c
    .line 361
    .local v2, _result:Ljava/lang/String;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 362
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 364
    return-object v2

    #@23
    .line 361
    .end local v2           #_result:Ljava/lang/String;
    :catchall_23
    move-exception v3

    #@24
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 362
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    throw v3
.end method

.method public getSDEncSupportStatus()Z
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 402
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 403
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 406
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.lge.systemservice.core.lgsdencmanager.ILGSDEncManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 407
    iget-object v3, p0, Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@10
    const/16 v4, 0xd

    #@12
    const/4 v5, 0x0

    #@13
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@16
    .line 408
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@19
    .line 409
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1c
    .catchall {:try_start_9 .. :try_end_1c} :catchall_27

    #@1c
    move-result v3

    #@1d
    if-eqz v3, :cond_20

    #@1f
    const/4 v2, 0x1

    #@20
    .line 412
    .local v2, _result:Z
    :cond_20
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 413
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 415
    return v2

    #@27
    .line 412
    .end local v2           #_result:Z
    :catchall_27
    move-exception v3

    #@28
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 413
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    throw v3
.end method
