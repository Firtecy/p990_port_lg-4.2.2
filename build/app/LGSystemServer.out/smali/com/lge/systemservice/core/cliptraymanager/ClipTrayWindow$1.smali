.class Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;
.super Ljava/lang/Object;
.source "ClipTrayWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 219
    iput-object p1, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;->this$0:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 25
    .parameter "v"

    #@0
    .prologue
    .line 223
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;->this$0:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@4
    move-object/from16 v20, v0

    #@6
    invoke-static/range {v20 .. v20}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->access$000(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Z

    #@9
    move-result v20

    #@a
    if-eqz v20, :cond_1e9

    #@c
    .line 225
    move-object/from16 v0, p0

    #@e
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;->this$0:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@10
    move-object/from16 v20, v0

    #@12
    invoke-static/range {v20 .. v20}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->access$100(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Landroid/widget/GridLayout;

    #@15
    move-result-object v20

    #@16
    move-object/from16 v0, v20

    #@18
    move-object/from16 v1, p1

    #@1a
    invoke-virtual {v0, v1}, Landroid/widget/GridLayout;->indexOfChild(Landroid/view/View;)I

    #@1d
    move-result v9

    #@1e
    .line 226
    .local v9, index:I
    const-string v20, "Cliptraywindow"

    #@20
    new-instance v21, Ljava/lang/StringBuilder;

    #@22
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v22, "item index = "

    #@27
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v21

    #@2b
    move-object/from16 v0, v21

    #@2d
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30
    move-result-object v21

    #@31
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v21

    #@35
    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 229
    move-object/from16 v0, p0

    #@3a
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;->this$0:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@3c
    move-object/from16 v20, v0

    #@3e
    invoke-static/range {v20 .. v20}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->access$100(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Landroid/widget/GridLayout;

    #@41
    move-result-object v20

    #@42
    move-object/from16 v0, v20

    #@44
    invoke-virtual {v0, v9}, Landroid/widget/GridLayout;->getChildAt(I)Landroid/view/View;

    #@47
    move-result-object v17

    #@48
    .line 230
    .local v17, removeview:Landroid/view/View;
    if-nez v17, :cond_65

    #@4a
    .line 231
    const-string v20, "Cliptraywindow"

    #@4c
    new-instance v21, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v22, "error : cannot remove child view at = "

    #@53
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v21

    #@57
    move-object/from16 v0, v21

    #@59
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v21

    #@5d
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v21

    #@61
    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@64
    .line 347
    .end local v9           #index:I
    .end local v17           #removeview:Landroid/view/View;
    :cond_64
    :goto_64
    return-void

    #@65
    .line 235
    .restart local v9       #index:I
    .restart local v17       #removeview:Landroid/view/View;
    :cond_65
    const v20, 0x20d004b

    #@68
    move-object/from16 v0, v17

    #@6a
    move/from16 v1, v20

    #@6c
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@6f
    move-result-object v16

    #@70
    check-cast v16, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;

    #@72
    .line 236
    .local v16, portview:Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;
    const v20, 0x20d004d

    #@75
    move-object/from16 v0, v17

    #@77
    move/from16 v1, v20

    #@79
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@7c
    move-result-object v12

    #@7d
    check-cast v12, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;

    #@7f
    .line 237
    .local v12, landview:Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;
    invoke-virtual/range {v16 .. v16}, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->remove()V

    #@82
    .line 238
    invoke-virtual {v12}, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->remove()V

    #@85
    .line 239
    move-object/from16 v0, p0

    #@87
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;->this$0:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@89
    move-object/from16 v20, v0

    #@8b
    invoke-static/range {v20 .. v20}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->access$100(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Landroid/widget/GridLayout;

    #@8e
    move-result-object v20

    #@8f
    move-object/from16 v0, v20

    #@91
    invoke-virtual {v0, v9}, Landroid/widget/GridLayout;->removeViewAt(I)V

    #@94
    .line 242
    move-object/from16 v0, p0

    #@96
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;->this$0:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@98
    move-object/from16 v20, v0

    #@9a
    invoke-static/range {v20 .. v20}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->access$200(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Landroid/content/ClipboardManager;

    #@9d
    move-result-object v20

    #@9e
    invoke-virtual/range {v20 .. v20}, Landroid/content/ClipboardManager;->getPrimaryClipCount()I

    #@a1
    move-result v20

    #@a2
    sub-int v20, v20, v9

    #@a4
    add-int/lit8 v3, v20, -0x1

    #@a6
    .line 244
    .local v3, clipIndex:I
    move-object/from16 v0, p0

    #@a8
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;->this$0:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@aa
    move-object/from16 v20, v0

    #@ac
    move-object/from16 v0, v20

    #@ae
    invoke-static {v0, v3}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->access$300(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;I)Ljava/util/ArrayList;

    #@b1
    move-result-object v13

    #@b2
    .line 245
    .local v13, lists:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v7, 0x0

    #@b3
    .local v7, i:I
    :goto_b3
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    #@b6
    move-result v20

    #@b7
    move/from16 v0, v20

    #@b9
    if-ge v7, v0, :cond_105

    #@bb
    .line 246
    invoke-virtual {v13, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@be
    move-result-object v20

    #@bf
    check-cast v20, Landroid/net/Uri;

    #@c1
    invoke-virtual/range {v20 .. v20}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    #@c4
    move-result-object v15

    #@c5
    .line 247
    .local v15, path:Ljava/lang/String;
    const-string v20, "Cliptraywindow"

    #@c7
    new-instance v21, Ljava/lang/StringBuilder;

    #@c9
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@cc
    const-string v22, "remove clip image at = "

    #@ce
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v21

    #@d2
    move-object/from16 v0, v21

    #@d4
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v21

    #@d8
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@db
    move-result-object v21

    #@dc
    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@df
    .line 249
    new-instance v6, Ljava/io/File;

    #@e1
    invoke-direct {v6, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@e4
    .line 250
    .local v6, file:Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    #@e7
    move-result v5

    #@e8
    .line 252
    .local v5, deleted:Z
    const-string v20, "Cliptraywindow"

    #@ea
    new-instance v21, Ljava/lang/StringBuilder;

    #@ec
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@ef
    const-string v22, "file removed : "

    #@f1
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v21

    #@f5
    move-object/from16 v0, v21

    #@f7
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@fa
    move-result-object v21

    #@fb
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fe
    move-result-object v21

    #@ff
    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@102
    .line 245
    add-int/lit8 v7, v7, 0x1

    #@104
    goto :goto_b3

    #@105
    .line 255
    .end local v5           #deleted:Z
    .end local v6           #file:Ljava/io/File;
    .end local v15           #path:Ljava/lang/String;
    :cond_105
    move-object/from16 v0, p0

    #@107
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;->this$0:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@109
    move-object/from16 v20, v0

    #@10b
    invoke-static/range {v20 .. v20}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->access$200(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Landroid/content/ClipboardManager;

    #@10e
    move-result-object v20

    #@10f
    invoke-virtual/range {v20 .. v20}, Landroid/content/ClipboardManager;->getPrimaryClipCount()I

    #@112
    move-result v20

    #@113
    const/16 v21, 0x1

    #@115
    move/from16 v0, v20

    #@117
    move/from16 v1, v21

    #@119
    if-ne v0, v1, :cond_1af

    #@11b
    .line 256
    move-object/from16 v0, p0

    #@11d
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;->this$0:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@11f
    move-object/from16 v20, v0

    #@121
    invoke-static/range {v20 .. v20}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->access$200(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Landroid/content/ClipboardManager;

    #@124
    move-result-object v20

    #@125
    invoke-virtual/range {v20 .. v20}, Landroid/content/ClipboardManager;->removeAllPrimaryClips()Z

    #@128
    .line 259
    move-object/from16 v0, p0

    #@12a
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;->this$0:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@12c
    move-object/from16 v20, v0

    #@12e
    invoke-static/range {v20 .. v20}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->access$400(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)V

    #@131
    .line 265
    :goto_131
    move-object/from16 v0, p0

    #@133
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;->this$0:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@135
    move-object/from16 v20, v0

    #@137
    invoke-static/range {v20 .. v20}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->access$500(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)V

    #@13a
    .line 267
    move-object/from16 v0, p0

    #@13c
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;->this$0:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@13e
    move-object/from16 v20, v0

    #@140
    invoke-static/range {v20 .. v20}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->access$200(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Landroid/content/ClipboardManager;

    #@143
    move-result-object v20

    #@144
    invoke-virtual/range {v20 .. v20}, Landroid/content/ClipboardManager;->getPrimaryClipCount()I

    #@147
    move-result v20

    #@148
    if-nez v20, :cond_64

    #@14a
    .line 268
    const-string v20, "test"

    #@14c
    const-string v21, "clip count is 0"

    #@14e
    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@151
    .line 269
    move-object/from16 v0, p0

    #@153
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;->this$0:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@155
    move-object/from16 v20, v0

    #@157
    invoke-static/range {v20 .. v20}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->access$600(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Landroid/widget/HorizontalScrollView;

    #@15a
    move-result-object v20

    #@15b
    const/16 v21, 0x8

    #@15d
    invoke-virtual/range {v20 .. v21}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    #@160
    .line 270
    move-object/from16 v0, p0

    #@162
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;->this$0:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@164
    move-object/from16 v20, v0

    #@166
    invoke-static/range {v20 .. v20}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->access$700(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Z

    #@169
    move-result v20

    #@16a
    if-eqz v20, :cond_1c0

    #@16c
    .line 271
    move-object/from16 v0, p0

    #@16e
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;->this$0:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@170
    move-object/from16 v20, v0

    #@172
    invoke-static/range {v20 .. v20}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->access$800(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Landroid/widget/LinearLayout;

    #@175
    move-result-object v20

    #@176
    const v21, 0x20d0044

    #@179
    invoke-virtual/range {v20 .. v21}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    #@17c
    move-result-object v19

    #@17d
    check-cast v19, Landroid/widget/TextView;

    #@17f
    .line 272
    .local v19, txtNoContent:Landroid/widget/TextView;
    const v20, 0x20902a3

    #@182
    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setText(I)V

    #@185
    .line 273
    move-object/from16 v0, p0

    #@187
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;->this$0:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@189
    move-object/from16 v20, v0

    #@18b
    invoke-static/range {v20 .. v20}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->access$900(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Landroid/view/View;

    #@18e
    move-result-object v20

    #@18f
    const/16 v21, 0x0

    #@191
    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->setVisibility(I)V

    #@194
    .line 279
    :goto_194
    move-object/from16 v0, p0

    #@196
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;->this$0:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@198
    move-object/from16 v20, v0

    #@19a
    invoke-static/range {v20 .. v20}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->access$1100(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnNoContent;

    #@19d
    move-result-object v20

    #@19e
    if-eqz v20, :cond_64

    #@1a0
    .line 280
    move-object/from16 v0, p0

    #@1a2
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;->this$0:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@1a4
    move-object/from16 v20, v0

    #@1a6
    invoke-static/range {v20 .. v20}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->access$1100(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnNoContent;

    #@1a9
    move-result-object v20

    #@1aa
    invoke-interface/range {v20 .. v20}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnNoContent;->onFinishDeleteMode()V

    #@1ad
    goto/16 :goto_64

    #@1af
    .line 261
    .end local v19           #txtNoContent:Landroid/widget/TextView;
    :cond_1af
    move-object/from16 v0, p0

    #@1b1
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;->this$0:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@1b3
    move-object/from16 v20, v0

    #@1b5
    invoke-static/range {v20 .. v20}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->access$200(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Landroid/content/ClipboardManager;

    #@1b8
    move-result-object v20

    #@1b9
    move-object/from16 v0, v20

    #@1bb
    invoke-virtual {v0, v3}, Landroid/content/ClipboardManager;->removePrimaryClipAt(I)Z

    #@1be
    goto/16 :goto_131

    #@1c0
    .line 275
    :cond_1c0
    move-object/from16 v0, p0

    #@1c2
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;->this$0:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@1c4
    move-object/from16 v20, v0

    #@1c6
    invoke-static/range {v20 .. v20}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->access$800(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Landroid/widget/LinearLayout;

    #@1c9
    move-result-object v20

    #@1ca
    const v21, 0x20d0046

    #@1cd
    invoke-virtual/range {v20 .. v21}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    #@1d0
    move-result-object v19

    #@1d1
    check-cast v19, Landroid/widget/TextView;

    #@1d3
    .line 276
    .restart local v19       #txtNoContent:Landroid/widget/TextView;
    const v20, 0x20902a3

    #@1d6
    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setText(I)V

    #@1d9
    .line 277
    move-object/from16 v0, p0

    #@1db
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;->this$0:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@1dd
    move-object/from16 v20, v0

    #@1df
    invoke-static/range {v20 .. v20}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->access$1000(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Landroid/view/View;

    #@1e2
    move-result-object v20

    #@1e3
    const/16 v21, 0x0

    #@1e5
    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->setVisibility(I)V

    #@1e8
    goto :goto_194

    #@1e9
    .line 286
    .end local v3           #clipIndex:I
    .end local v7           #i:I
    .end local v9           #index:I
    .end local v12           #landview:Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;
    .end local v13           #lists:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/net/Uri;>;"
    .end local v16           #portview:Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;
    .end local v17           #removeview:Landroid/view/View;
    .end local v19           #txtNoContent:Landroid/widget/TextView;
    :cond_1e9
    move-object/from16 v0, p0

    #@1eb
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;->this$0:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@1ed
    move-object/from16 v20, v0

    #@1ef
    invoke-static/range {v20 .. v20}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->access$1200(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnPasteInterface;

    #@1f2
    move-result-object v20

    #@1f3
    if-eqz v20, :cond_64

    #@1f5
    .line 290
    move-object/from16 v0, p0

    #@1f7
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;->this$0:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@1f9
    move-object/from16 v20, v0

    #@1fb
    invoke-static/range {v20 .. v20}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->access$100(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Landroid/widget/GridLayout;

    #@1fe
    move-result-object v20

    #@1ff
    move-object/from16 v0, v20

    #@201
    move-object/from16 v1, p1

    #@203
    invoke-virtual {v0, v1}, Landroid/widget/GridLayout;->indexOfChild(Landroid/view/View;)I

    #@206
    move-result v9

    #@207
    .line 291
    .restart local v9       #index:I
    if-gez v9, :cond_225

    #@209
    .line 292
    const-string v20, "Cliptraywindow"

    #@20b
    new-instance v21, Ljava/lang/StringBuilder;

    #@20d
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@210
    const-string v22, "error : failed to paste item at = "

    #@212
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@215
    move-result-object v21

    #@216
    move-object/from16 v0, v21

    #@218
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21b
    move-result-object v21

    #@21c
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21f
    move-result-object v21

    #@220
    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@223
    goto/16 :goto_64

    #@225
    .line 297
    :cond_225
    move-object/from16 v0, p0

    #@227
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;->this$0:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@229
    move-object/from16 v20, v0

    #@22b
    invoke-static/range {v20 .. v20}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->access$200(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Landroid/content/ClipboardManager;

    #@22e
    move-result-object v20

    #@22f
    invoke-virtual/range {v20 .. v20}, Landroid/content/ClipboardManager;->getPrimaryClipCount()I

    #@232
    move-result v20

    #@233
    sub-int v20, v20, v9

    #@235
    add-int/lit8 v3, v20, -0x1

    #@237
    .line 298
    .restart local v3       #clipIndex:I
    move-object/from16 v0, p0

    #@239
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;->this$0:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@23b
    move-object/from16 v20, v0

    #@23d
    invoke-static/range {v20 .. v20}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->access$200(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Landroid/content/ClipboardManager;

    #@240
    move-result-object v20

    #@241
    move-object/from16 v0, v20

    #@243
    invoke-virtual {v0, v3}, Landroid/content/ClipboardManager;->getPrimaryClipAt(I)Landroid/content/ClipData;

    #@246
    move-result-object v2

    #@247
    .line 299
    .local v2, clip:Landroid/content/ClipData;
    if-nez v2, :cond_265

    #@249
    .line 300
    const-string v20, "Cliptraywindow"

    #@24b
    new-instance v21, Ljava/lang/StringBuilder;

    #@24d
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@250
    const-string v22, "error : failed to get copied item at = "

    #@252
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@255
    move-result-object v21

    #@256
    move-object/from16 v0, v21

    #@258
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25b
    move-result-object v21

    #@25c
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25f
    move-result-object v21

    #@260
    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@263
    goto/16 :goto_64

    #@265
    .line 304
    :cond_265
    invoke-virtual {v2}, Landroid/content/ClipData;->getItemCount()I

    #@268
    move-result v11

    #@269
    .line 305
    .local v11, itemCount:I
    const/16 v18, 0x0

    #@26b
    .line 306
    .local v18, textCount:I
    const/4 v8, 0x0

    #@26c
    .line 307
    .local v8, imgCount:I
    const/4 v7, 0x0

    #@26d
    .restart local v7       #i:I
    :goto_26d
    if-ge v7, v11, :cond_29f

    #@26f
    .line 308
    invoke-virtual {v2, v7}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    #@272
    move-result-object v10

    #@273
    .line 309
    .local v10, item:Landroid/content/ClipData$Item;
    invoke-virtual {v10}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    #@276
    move-result-object v4

    #@277
    .line 310
    .local v4, clipUri:Landroid/net/Uri;
    if-eqz v4, :cond_28a

    #@279
    const-string v20, "file"

    #@27b
    invoke-virtual {v4}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@27e
    move-result-object v21

    #@27f
    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@282
    move-result v20

    #@283
    if-eqz v20, :cond_28a

    #@285
    .line 311
    add-int/lit8 v8, v8, 0x1

    #@287
    .line 307
    :cond_287
    :goto_287
    add-int/lit8 v7, v7, 0x1

    #@289
    goto :goto_26d

    #@28a
    .line 313
    :cond_28a
    move-object/from16 v0, p0

    #@28c
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;->this$0:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@28e
    move-object/from16 v20, v0

    #@290
    invoke-static/range {v20 .. v20}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->access$1300(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Landroid/content/Context;

    #@293
    move-result-object v20

    #@294
    move-object/from16 v0, v20

    #@296
    invoke-virtual {v10, v0}, Landroid/content/ClipData$Item;->coerceToStyledText(Landroid/content/Context;)Ljava/lang/CharSequence;

    #@299
    move-result-object v20

    #@29a
    if-eqz v20, :cond_287

    #@29c
    .line 314
    add-int/lit8 v18, v18, 0x1

    #@29e
    goto :goto_287

    #@29f
    .line 320
    .end local v4           #clipUri:Landroid/net/Uri;
    .end local v10           #item:Landroid/content/ClipData$Item;
    :cond_29f
    move-object/from16 v0, p0

    #@2a1
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;->this$0:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@2a3
    move-object/from16 v20, v0

    #@2a5
    invoke-static/range {v20 .. v20}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->access$1400(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)I

    #@2a8
    move-result v20

    #@2a9
    if-nez v20, :cond_2ad

    #@2ab
    if-eqz v18, :cond_64

    #@2ad
    .line 325
    :cond_2ad
    move-object/from16 v0, p0

    #@2af
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;->this$0:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@2b1
    move-object/from16 v20, v0

    #@2b3
    invoke-static/range {v20 .. v20}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->access$1400(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)I

    #@2b6
    move-result v20

    #@2b7
    const/16 v21, 0x1

    #@2b9
    move/from16 v0, v20

    #@2bb
    move/from16 v1, v21

    #@2bd
    if-ne v0, v1, :cond_2c1

    #@2bf
    if-eqz v8, :cond_64

    #@2c1
    .line 330
    :cond_2c1
    move-object/from16 v0, p0

    #@2c3
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;->this$0:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@2c5
    move-object/from16 v20, v0

    #@2c7
    invoke-static/range {v20 .. v20}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->access$200(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Landroid/content/ClipboardManager;

    #@2ca
    move-result-object v20

    #@2cb
    move-object/from16 v0, v20

    #@2cd
    invoke-virtual {v0, v3}, Landroid/content/ClipboardManager;->removePrimaryClipAt(I)Z

    #@2d0
    .line 331
    move-object/from16 v0, p0

    #@2d2
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;->this$0:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@2d4
    move-object/from16 v20, v0

    #@2d6
    invoke-static/range {v20 .. v20}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->access$200(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Landroid/content/ClipboardManager;

    #@2d9
    move-result-object v20

    #@2da
    move-object/from16 v0, v20

    #@2dc
    invoke-virtual {v0, v2}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    #@2df
    .line 334
    move-object/from16 v0, p0

    #@2e1
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;->this$0:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@2e3
    move-object/from16 v20, v0

    #@2e5
    invoke-static/range {v20 .. v20}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->access$100(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Landroid/widget/GridLayout;

    #@2e8
    move-result-object v20

    #@2e9
    move-object/from16 v0, v20

    #@2eb
    invoke-virtual {v0, v9}, Landroid/widget/GridLayout;->getChildAt(I)Landroid/view/View;

    #@2ee
    move-result-object v14

    #@2ef
    .line 335
    .local v14, moveview:Landroid/view/View;
    if-nez v14, :cond_2fa

    #@2f1
    .line 336
    const-string v20, "Cliptraywindow"

    #@2f3
    const-string v21, "error : cannot move clicked item to front!!"

    #@2f5
    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2f8
    goto/16 :goto_64

    #@2fa
    .line 339
    :cond_2fa
    move-object/from16 v0, p0

    #@2fc
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;->this$0:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@2fe
    move-object/from16 v20, v0

    #@300
    invoke-static/range {v20 .. v20}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->access$100(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Landroid/widget/GridLayout;

    #@303
    move-result-object v20

    #@304
    move-object/from16 v0, v20

    #@306
    invoke-virtual {v0, v9}, Landroid/widget/GridLayout;->removeViewAt(I)V

    #@309
    .line 340
    move-object/from16 v0, p0

    #@30b
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;->this$0:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@30d
    move-object/from16 v20, v0

    #@30f
    invoke-static/range {v20 .. v20}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->access$100(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Landroid/widget/GridLayout;

    #@312
    move-result-object v20

    #@313
    const/16 v21, 0x0

    #@315
    move-object/from16 v0, v20

    #@317
    move/from16 v1, v21

    #@319
    invoke-virtual {v0, v14, v1}, Landroid/widget/GridLayout;->addView(Landroid/view/View;I)V

    #@31c
    .line 342
    move-object/from16 v0, p0

    #@31e
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;->this$0:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@320
    move-object/from16 v20, v0

    #@322
    invoke-static/range {v20 .. v20}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->access$500(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)V

    #@325
    .line 344
    move-object/from16 v0, p0

    #@327
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;->this$0:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@329
    move-object/from16 v20, v0

    #@32b
    const/16 v21, 0x1

    #@32d
    invoke-static/range {v20 .. v21}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->access$1502(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;Z)Z

    #@330
    .line 345
    move-object/from16 v0, p0

    #@332
    iget-object v0, v0, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$1;->this$0:Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;

    #@334
    move-object/from16 v20, v0

    #@336
    invoke-static/range {v20 .. v20}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;->access$1200(Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow;)Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnPasteInterface;

    #@339
    move-result-object v20

    #@33a
    invoke-interface/range {v20 .. v20}, Lcom/lge/systemservice/core/cliptraymanager/ClipTrayWindow$IOnPasteInterface;->onPasteClipData()V

    #@33d
    goto/16 :goto_64
.end method
