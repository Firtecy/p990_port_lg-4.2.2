.class public abstract Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;
.super Landroid/os/Binder;
.source "IAATManager.java"

# interfaces
.implements Lcom/lge/systemservice/core/aatmanager/IAATManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/core/aatmanager/IAATManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 15
    const-string v0, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 16
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 13
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    .line 38
    sparse-switch p1, :sswitch_data_574

    #@5
    .line 681
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v6

    #@9
    :goto_9
    return v6

    #@a
    .line 42
    :sswitch_a
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@c
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 47
    :sswitch_10
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@12
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 48
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->AATInitialize()V

    #@18
    .line 49
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1b
    goto :goto_9

    #@1c
    .line 54
    :sswitch_1c
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@1e
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@21
    .line 55
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->AATFinalize()V

    #@24
    .line 56
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@27
    goto :goto_9

    #@28
    .line 61
    :sswitch_28
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@2a
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2d
    .line 63
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@30
    move-result v0

    #@31
    .line 65
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@34
    move-result-object v2

    #@35
    .line 66
    .local v2, _arg1:Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->requestToService(ILjava/lang/String;)Ljava/lang/String;

    #@38
    move-result-object v4

    #@39
    .line 67
    .local v4, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3c
    .line 68
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@3f
    goto :goto_9

    #@40
    .line 73
    .end local v0           #_arg0:I
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v4           #_result:Ljava/lang/String;
    :sswitch_40
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@42
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@45
    .line 74
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->initializeLoopback()V

    #@48
    .line 75
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4b
    goto :goto_9

    #@4c
    .line 80
    :sswitch_4c
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@4e
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@51
    .line 81
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->finalizeLoopback()V

    #@54
    .line 82
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@57
    goto :goto_9

    #@58
    .line 87
    :sswitch_58
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@5a
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5d
    .line 89
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@60
    move-result v0

    #@61
    .line 90
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->SetLoopbackParam(I)Ljava/lang/String;

    #@64
    move-result-object v4

    #@65
    .line 91
    .restart local v4       #_result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@68
    .line 92
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@6b
    goto :goto_9

    #@6c
    .line 97
    .end local v0           #_arg0:I
    .end local v4           #_result:Ljava/lang/String;
    :sswitch_6c
    const-string v7, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@6e
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@71
    .line 98
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->IsSupportGLOTestGps()Z

    #@74
    move-result v4

    #@75
    .line 99
    .local v4, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@78
    .line 100
    if-eqz v4, :cond_7b

    #@7a
    move v5, v6

    #@7b
    :cond_7b
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@7e
    goto :goto_9

    #@7f
    .line 105
    .end local v4           #_result:Z
    :sswitch_7f
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@81
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@84
    .line 106
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->initializeGps()V

    #@87
    .line 107
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@8a
    goto/16 :goto_9

    #@8c
    .line 112
    :sswitch_8c
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@8e
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@91
    .line 113
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->finalizeGps()V

    #@94
    .line 114
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@97
    goto/16 :goto_9

    #@99
    .line 119
    :sswitch_99
    const-string v7, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@9b
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9e
    .line 120
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->NFC_Enable()Z

    #@a1
    move-result v4

    #@a2
    .line 121
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@a5
    .line 122
    if-eqz v4, :cond_a8

    #@a7
    move v5, v6

    #@a8
    :cond_a8
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@ab
    goto/16 :goto_9

    #@ad
    .line 127
    .end local v4           #_result:Z
    :sswitch_ad
    const-string v7, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@af
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b2
    .line 128
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->NFC_Disable()Z

    #@b5
    move-result v4

    #@b6
    .line 129
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@b9
    .line 130
    if-eqz v4, :cond_bc

    #@bb
    move v5, v6

    #@bc
    :cond_bc
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@bf
    goto/16 :goto_9

    #@c1
    .line 135
    .end local v4           #_result:Z
    :sswitch_c1
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@c3
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c6
    .line 137
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@c9
    move-result-object v0

    #@ca
    .line 138
    .local v0, _arg0:[B
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->NFC_On([B)I

    #@cd
    move-result v4

    #@ce
    .line 139
    .local v4, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@d1
    .line 140
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@d4
    .line 141
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    #@d7
    goto/16 :goto_9

    #@d9
    .line 146
    .end local v0           #_arg0:[B
    .end local v4           #_result:I
    :sswitch_d9
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@db
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@de
    .line 148
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@e1
    move-result-object v0

    #@e2
    .line 149
    .restart local v0       #_arg0:[B
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->NFC_Off([B)I

    #@e5
    move-result v4

    #@e6
    .line 150
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@e9
    .line 151
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@ec
    .line 152
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    #@ef
    goto/16 :goto_9

    #@f1
    .line 157
    .end local v0           #_arg0:[B
    .end local v4           #_result:I
    :sswitch_f1
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@f3
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f6
    .line 159
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@f9
    move-result-object v0

    #@fa
    .line 160
    .restart local v0       #_arg0:[B
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->NFC_Swp([B)I

    #@fd
    move-result v4

    #@fe
    .line 161
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@101
    .line 162
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@104
    .line 163
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    #@107
    goto/16 :goto_9

    #@109
    .line 168
    .end local v0           #_arg0:[B
    .end local v4           #_result:I
    :sswitch_109
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@10b
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@10e
    .line 170
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@111
    move-result-object v0

    #@112
    .line 171
    .restart local v0       #_arg0:[B
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->NFC_SmartMX([B)I

    #@115
    move-result v4

    #@116
    .line 172
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@119
    .line 173
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@11c
    .line 174
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    #@11f
    goto/16 :goto_9

    #@121
    .line 179
    .end local v0           #_arg0:[B
    .end local v4           #_result:I
    :sswitch_121
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@123
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@126
    .line 181
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@129
    move-result-object v0

    #@12a
    .line 182
    .restart local v0       #_arg0:[B
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->NFC_Reader([B)I

    #@12d
    move-result v4

    #@12e
    .line 183
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@131
    .line 184
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@134
    .line 185
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    #@137
    goto/16 :goto_9

    #@139
    .line 190
    .end local v0           #_arg0:[B
    .end local v4           #_result:I
    :sswitch_139
    const-string v7, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@13b
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@13e
    .line 192
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@141
    move-result v7

    #@142
    if-eqz v7, :cond_14d

    #@144
    move v0, v6

    #@145
    .line 193
    .local v0, _arg0:Z
    :goto_145
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->camcorder_submic(Z)V

    #@148
    .line 194
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@14b
    goto/16 :goto_9

    #@14d
    .end local v0           #_arg0:Z
    :cond_14d
    move v0, v5

    #@14e
    .line 192
    goto :goto_145

    #@14f
    .line 199
    :sswitch_14f
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@151
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@154
    .line 200
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->ThresholdALC()F

    #@157
    move-result v4

    #@158
    .line 201
    .local v4, _result:F
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@15b
    .line 202
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeFloat(F)V

    #@15e
    goto/16 :goto_9

    #@160
    .line 207
    .end local v4           #_result:F
    :sswitch_160
    const-string v7, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@162
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@165
    .line 209
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@168
    move-result v7

    #@169
    if-eqz v7, :cond_174

    #@16b
    move v0, v6

    #@16c
    .line 210
    .restart local v0       #_arg0:Z
    :goto_16c
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->setRotationSensor(Z)V

    #@16f
    .line 211
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@172
    goto/16 :goto_9

    #@174
    .end local v0           #_arg0:Z
    :cond_174
    move v0, v5

    #@175
    .line 209
    goto :goto_16c

    #@176
    .line 216
    :sswitch_176
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@178
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@17b
    .line 218
    invoke-virtual {p2}, Landroid/os/Parcel;->createFloatArray()[F

    #@17e
    move-result-object v0

    #@17f
    .line 219
    .local v0, _arg0:[F
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->Start_AccCalibration([F)I

    #@182
    move-result v4

    #@183
    .line 220
    .local v4, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@186
    .line 221
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@189
    .line 222
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeFloatArray([F)V

    #@18c
    goto/16 :goto_9

    #@18e
    .line 227
    .end local v0           #_arg0:[F
    .end local v4           #_result:I
    :sswitch_18e
    const-string v7, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@190
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@193
    .line 228
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->IsSupportProximityCalibration()Z

    #@196
    move-result v4

    #@197
    .line 229
    .local v4, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@19a
    .line 230
    if-eqz v4, :cond_19d

    #@19c
    move v5, v6

    #@19d
    :cond_19d
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@1a0
    goto/16 :goto_9

    #@1a2
    .line 235
    .end local v4           #_result:Z
    :sswitch_1a2
    const-string v7, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@1a4
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1a7
    .line 236
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->IsSupportBarometer()Z

    #@1aa
    move-result v4

    #@1ab
    .line 237
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1ae
    .line 238
    if-eqz v4, :cond_1b1

    #@1b0
    move v5, v6

    #@1b1
    :cond_1b1
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@1b4
    goto/16 :goto_9

    #@1b6
    .line 243
    .end local v4           #_result:Z
    :sswitch_1b6
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@1b8
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1bb
    .line 244
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->Start_ProximityCalibration()I

    #@1be
    move-result v4

    #@1bf
    .line 245
    .local v4, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1c2
    .line 246
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1c5
    goto/16 :goto_9

    #@1c7
    .line 251
    .end local v4           #_result:I
    :sswitch_1c7
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@1c9
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1cc
    .line 252
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->getCameraResolution()[I

    #@1cf
    move-result-object v4

    #@1d0
    .line 253
    .local v4, _result:[I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1d3
    .line 254
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeIntArray([I)V

    #@1d6
    goto/16 :goto_9

    #@1d8
    .line 259
    .end local v4           #_result:[I
    :sswitch_1d8
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@1da
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1dd
    .line 260
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->QwertyLedOn()V

    #@1e0
    .line 261
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1e3
    goto/16 :goto_9

    #@1e5
    .line 266
    :sswitch_1e5
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@1e7
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1ea
    .line 267
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->QwertyLedOff()V

    #@1ed
    .line 268
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1f0
    goto/16 :goto_9

    #@1f2
    .line 273
    :sswitch_1f2
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@1f4
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1f7
    .line 274
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->getQwertyStatusValue()Ljava/lang/String;

    #@1fa
    move-result-object v4

    #@1fb
    .line 275
    .local v4, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1fe
    .line 276
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@201
    goto/16 :goto_9

    #@203
    .line 281
    .end local v4           #_result:Ljava/lang/String;
    :sswitch_203
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@205
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@208
    .line 283
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@20b
    move-result-object v0

    #@20c
    .line 285
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@20f
    move-result-object v2

    #@210
    .line 287
    .restart local v2       #_arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@213
    move-result-object v3

    #@214
    .line 288
    .local v3, _arg2:Ljava/lang/String;
    invoke-virtual {p0, v0, v2, v3}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->setLedVal(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@217
    .line 289
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@21a
    goto/16 :goto_9

    #@21c
    .line 294
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v3           #_arg2:Ljava/lang/String;
    :sswitch_21c
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@21e
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@221
    .line 295
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->getLedVal()[Ljava/lang/String;

    #@224
    move-result-object v4

    #@225
    .line 296
    .local v4, _result:[Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@228
    .line 297
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@22b
    goto/16 :goto_9

    #@22d
    .line 302
    .end local v4           #_result:[Ljava/lang/String;
    :sswitch_22d
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@22f
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@232
    .line 303
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->Start_SmartFactoryReset()V

    #@235
    .line 304
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@238
    goto/16 :goto_9

    #@23a
    .line 309
    :sswitch_23a
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@23c
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@23f
    .line 311
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@242
    move-result v0

    #@243
    .line 313
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@246
    move-result v2

    #@247
    .line 314
    .local v2, _arg1:I
    invoke-virtual {p0, v0, v2}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->RebootforModeChange(II)V

    #@24a
    .line 315
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@24d
    goto/16 :goto_9

    #@24f
    .line 320
    .end local v0           #_arg0:I
    .end local v2           #_arg1:I
    :sswitch_24f
    const-string v7, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@251
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@254
    .line 321
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->isDualSIM()Z

    #@257
    move-result v4

    #@258
    .line 322
    .local v4, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@25b
    .line 323
    if-eqz v4, :cond_25e

    #@25d
    move v5, v6

    #@25e
    :cond_25e
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@261
    goto/16 :goto_9

    #@263
    .line 328
    .end local v4           #_result:Z
    :sswitch_263
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@265
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@268
    .line 329
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->getCountSuppotedSIM()I

    #@26b
    move-result v4

    #@26c
    .line 330
    .local v4, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@26f
    .line 331
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@272
    goto/16 :goto_9

    #@274
    .line 336
    .end local v4           #_result:I
    :sswitch_274
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@276
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@279
    .line 337
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->testDualUSIM1()I

    #@27c
    move-result v4

    #@27d
    .line 338
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@280
    .line 339
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@283
    goto/16 :goto_9

    #@285
    .line 344
    .end local v4           #_result:I
    :sswitch_285
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@287
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@28a
    .line 345
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->testDualUSIM2()I

    #@28d
    move-result v4

    #@28e
    .line 346
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@291
    .line 347
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@294
    goto/16 :goto_9

    #@296
    .line 352
    .end local v4           #_result:I
    :sswitch_296
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@298
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@29b
    .line 353
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->testDualUSIM3()I

    #@29e
    move-result v4

    #@29f
    .line 354
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2a2
    .line 355
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@2a5
    goto/16 :goto_9

    #@2a7
    .line 360
    .end local v4           #_result:I
    :sswitch_2a7
    const-string v7, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@2a9
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2ac
    .line 361
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->IsSupportUSIM()Z

    #@2af
    move-result v4

    #@2b0
    .line 362
    .local v4, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2b3
    .line 363
    if-eqz v4, :cond_2b6

    #@2b5
    move v5, v6

    #@2b6
    :cond_2b6
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@2b9
    goto/16 :goto_9

    #@2bb
    .line 368
    .end local v4           #_result:Z
    :sswitch_2bb
    const-string v7, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@2bd
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2c0
    .line 369
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->IsSupportMura()Z

    #@2c3
    move-result v4

    #@2c4
    .line 370
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2c7
    .line 371
    if-eqz v4, :cond_2ca

    #@2c9
    move v5, v6

    #@2ca
    :cond_2ca
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@2cd
    goto/16 :goto_9

    #@2cf
    .line 376
    .end local v4           #_result:Z
    :sswitch_2cf
    const-string v7, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@2d1
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2d4
    .line 377
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->IsFMRadio()Z

    #@2d7
    move-result v4

    #@2d8
    .line 378
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2db
    .line 379
    if-eqz v4, :cond_2de

    #@2dd
    move v5, v6

    #@2de
    :cond_2de
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@2e1
    goto/16 :goto_9

    #@2e3
    .line 384
    .end local v4           #_result:Z
    :sswitch_2e3
    const-string v7, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@2e5
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2e8
    .line 385
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->turnOnFMRadio()Z

    #@2eb
    move-result v4

    #@2ec
    .line 386
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2ef
    .line 387
    if-eqz v4, :cond_2f2

    #@2f1
    move v5, v6

    #@2f2
    :cond_2f2
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@2f5
    goto/16 :goto_9

    #@2f7
    .line 392
    .end local v4           #_result:Z
    :sswitch_2f7
    const-string v7, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@2f9
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2fc
    .line 393
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->turnOffFMRadio()Z

    #@2ff
    move-result v4

    #@300
    .line 394
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@303
    .line 395
    if-eqz v4, :cond_306

    #@305
    move v5, v6

    #@306
    :cond_306
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@309
    goto/16 :goto_9

    #@30b
    .line 400
    .end local v4           #_result:Z
    :sswitch_30b
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@30d
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@310
    .line 401
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->createFmRadioMgrFMRadio()V

    #@313
    .line 402
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@316
    goto/16 :goto_9

    #@318
    .line 407
    :sswitch_318
    const-string v7, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@31a
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@31d
    .line 408
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->startAutoScanFMRadio()Z

    #@320
    move-result v4

    #@321
    .line 409
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@324
    .line 410
    if-eqz v4, :cond_327

    #@326
    move v5, v6

    #@327
    :cond_327
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@32a
    goto/16 :goto_9

    #@32c
    .line 415
    .end local v4           #_result:Z
    :sswitch_32c
    const-string v7, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@32e
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@331
    .line 416
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->startBackwardScanFMRadio()Z

    #@334
    move-result v4

    #@335
    .line 417
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@338
    .line 418
    if-eqz v4, :cond_33b

    #@33a
    move v5, v6

    #@33b
    :cond_33b
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@33e
    goto/16 :goto_9

    #@340
    .line 423
    .end local v4           #_result:Z
    :sswitch_340
    const-string v7, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@342
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@345
    .line 424
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->startForwardScanFMRadio()Z

    #@348
    move-result v4

    #@349
    .line 425
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@34c
    .line 426
    if-eqz v4, :cond_34f

    #@34e
    move v5, v6

    #@34f
    :cond_34f
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@352
    goto/16 :goto_9

    #@354
    .line 431
    .end local v4           #_result:Z
    :sswitch_354
    const-string v7, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@356
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@359
    .line 433
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@35c
    move-result v0

    #@35d
    .line 434
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->tuneFrequencyFMRadio(I)Z

    #@360
    move-result v4

    #@361
    .line 435
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@364
    .line 436
    if-eqz v4, :cond_367

    #@366
    move v5, v6

    #@367
    :cond_367
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@36a
    goto/16 :goto_9

    #@36c
    .line 441
    .end local v0           #_arg0:I
    .end local v4           #_result:Z
    :sswitch_36c
    const-string v7, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@36e
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@371
    .line 443
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@374
    move-result v1

    #@375
    .line 444
    .local v1, _arg0_length:I
    if-gez v1, :cond_38a

    #@377
    .line 445
    const/4 v0, 0x0

    #@378
    .line 450
    .local v0, _arg0:[B
    :goto_378
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->felicacmdEXTIDM([B)Z

    #@37b
    move-result v4

    #@37c
    .line 451
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@37f
    .line 452
    if-eqz v4, :cond_382

    #@381
    move v5, v6

    #@382
    :cond_382
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@385
    .line 453
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    #@388
    goto/16 :goto_9

    #@38a
    .line 448
    .end local v0           #_arg0:[B
    .end local v4           #_result:Z
    :cond_38a
    new-array v0, v1, [B

    #@38c
    .restart local v0       #_arg0:[B
    goto :goto_378

    #@38d
    .line 458
    .end local v0           #_arg0:[B
    .end local v1           #_arg0_length:I
    :sswitch_38d
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@38f
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@392
    .line 459
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->getSupportVideoEncorder()Ljava/lang/String;

    #@395
    move-result-object v4

    #@396
    .line 460
    .local v4, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@399
    .line 461
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@39c
    goto/16 :goto_9

    #@39e
    .line 466
    .end local v4           #_result:Ljava/lang/String;
    :sswitch_39e
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@3a0
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3a3
    .line 467
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->getAccelerometerCalOption()Ljava/lang/String;

    #@3a6
    move-result-object v4

    #@3a7
    .line 468
    .restart local v4       #_result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3aa
    .line 469
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@3ad
    goto/16 :goto_9

    #@3af
    .line 474
    .end local v4           #_result:Ljava/lang/String;
    :sswitch_3af
    const-string v7, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@3b1
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3b4
    .line 475
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->getAccelCalSleep()Z

    #@3b7
    move-result v4

    #@3b8
    .line 476
    .local v4, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3bb
    .line 477
    if-eqz v4, :cond_3be

    #@3bd
    move v5, v6

    #@3be
    :cond_3be
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@3c1
    goto/16 :goto_9

    #@3c3
    .line 482
    .end local v4           #_result:Z
    :sswitch_3c3
    const-string v7, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@3c5
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3c8
    .line 484
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3cb
    move-result v7

    #@3cc
    if-eqz v7, :cond_3d7

    #@3ce
    move v0, v6

    #@3cf
    .line 485
    .local v0, _arg0:Z
    :goto_3cf
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->AATsetLCDOnOff(Z)V

    #@3d2
    .line 486
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3d5
    goto/16 :goto_9

    #@3d7
    .end local v0           #_arg0:Z
    :cond_3d7
    move v0, v5

    #@3d8
    .line 484
    goto :goto_3cf

    #@3d9
    .line 491
    :sswitch_3d9
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@3db
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3de
    .line 492
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->getQuickMemoKeyCodeValue()I

    #@3e1
    move-result v4

    #@3e2
    .line 493
    .local v4, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3e5
    .line 494
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@3e8
    goto/16 :goto_9

    #@3ea
    .line 499
    .end local v4           #_result:I
    :sswitch_3ea
    const-string v7, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@3ec
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3ef
    .line 500
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->IsSupportAutoFocus()Z

    #@3f2
    move-result v4

    #@3f3
    .line 501
    .local v4, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3f6
    .line 502
    if-eqz v4, :cond_3f9

    #@3f8
    move v5, v6

    #@3f9
    :cond_3f9
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@3fc
    goto/16 :goto_9

    #@3fe
    .line 507
    .end local v4           #_result:Z
    :sswitch_3fe
    const-string v7, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@400
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@403
    .line 508
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->IsSupportSubMic()Z

    #@406
    move-result v4

    #@407
    .line 509
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@40a
    .line 510
    if-eqz v4, :cond_40d

    #@40c
    move v5, v6

    #@40d
    :cond_40d
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@410
    goto/16 :goto_9

    #@412
    .line 515
    .end local v4           #_result:Z
    :sswitch_412
    const-string v7, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@414
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@417
    .line 516
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->isFactoryTestMode()Z

    #@41a
    move-result v4

    #@41b
    .line 517
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@41e
    .line 518
    if-eqz v4, :cond_421

    #@420
    move v5, v6

    #@421
    :cond_421
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@424
    goto/16 :goto_9

    #@426
    .line 523
    .end local v4           #_result:Z
    :sswitch_426
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@428
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@42b
    .line 524
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->FolderTest_GetSupportedMenuList()[Z

    #@42e
    move-result-object v4

    #@42f
    .line 525
    .local v4, _result:[Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@432
    .line 526
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    #@435
    goto/16 :goto_9

    #@437
    .line 531
    .end local v4           #_result:[Z
    :sswitch_437
    const-string v7, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@439
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@43c
    .line 533
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@43f
    move-result v0

    #@440
    .line 534
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->FolderTest_GetDetailTestSupportValue(I)Z

    #@443
    move-result v4

    #@444
    .line 535
    .local v4, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@447
    .line 536
    if-eqz v4, :cond_44a

    #@449
    move v5, v6

    #@44a
    :cond_44a
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@44d
    goto/16 :goto_9

    #@44f
    .line 541
    .end local v0           #_arg0:I
    .end local v4           #_result:Z
    :sswitch_44f
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@451
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@454
    .line 543
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@457
    move-result v0

    #@458
    .line 544
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->FolderTest_GetFilePath(I)Ljava/lang/String;

    #@45b
    move-result-object v4

    #@45c
    .line 545
    .local v4, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@45f
    .line 546
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@462
    goto/16 :goto_9

    #@464
    .line 551
    .end local v0           #_arg0:I
    .end local v4           #_result:Ljava/lang/String;
    :sswitch_464
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@466
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@469
    .line 552
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->FolderTest_GetDimOnBacklightValue()F

    #@46c
    move-result v4

    #@46d
    .line 553
    .local v4, _result:F
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@470
    .line 554
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeFloat(F)V

    #@473
    goto/16 :goto_9

    #@475
    .line 559
    .end local v4           #_result:F
    :sswitch_475
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@477
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@47a
    .line 561
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@47d
    move-result v0

    #@47e
    .line 562
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->switchUSBMode(I)V

    #@481
    .line 563
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@484
    goto/16 :goto_9

    #@486
    .line 568
    .end local v0           #_arg0:I
    :sswitch_486
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@488
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@48b
    .line 569
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->GetUsbOnOffValue()I

    #@48e
    move-result v4

    #@48f
    .line 570
    .local v4, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@492
    .line 571
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@495
    goto/16 :goto_9

    #@497
    .line 576
    .end local v4           #_result:I
    :sswitch_497
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@499
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@49c
    .line 577
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->getTestOrderNumber()Ljava/lang/String;

    #@49f
    move-result-object v4

    #@4a0
    .line 578
    .local v4, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4a3
    .line 579
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@4a6
    goto/16 :goto_9

    #@4a8
    .line 584
    .end local v4           #_result:Ljava/lang/String;
    :sswitch_4a8
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@4aa
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4ad
    .line 585
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->getTestOrderLength()I

    #@4b0
    move-result v4

    #@4b1
    .line 586
    .local v4, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4b4
    .line 587
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@4b7
    goto/16 :goto_9

    #@4b9
    .line 592
    .end local v4           #_result:I
    :sswitch_4b9
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@4bb
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4be
    .line 593
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->resetData()V

    #@4c1
    .line 594
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4c4
    goto/16 :goto_9

    #@4c6
    .line 599
    :sswitch_4c6
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@4c8
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4cb
    .line 600
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->getHallSensorResult()[Ljava/lang/String;

    #@4ce
    move-result-object v4

    #@4cf
    .line 601
    .local v4, _result:[Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4d2
    .line 602
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@4d5
    goto/16 :goto_9

    #@4d7
    .line 607
    .end local v4           #_result:[Ljava/lang/String;
    :sswitch_4d7
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@4d9
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4dc
    .line 609
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@4df
    move-result-object v0

    #@4e0
    .line 610
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->setSpeakerState(Ljava/lang/String;)V

    #@4e3
    .line 611
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4e6
    goto/16 :goto_9

    #@4e8
    .line 616
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_4e8
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@4ea
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4ed
    .line 617
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->getBatteryCapacityFilePath()Ljava/lang/String;

    #@4f0
    move-result-object v4

    #@4f1
    .line 618
    .local v4, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4f4
    .line 619
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@4f7
    goto/16 :goto_9

    #@4f9
    .line 624
    .end local v4           #_result:Ljava/lang/String;
    :sswitch_4f9
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@4fb
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4fe
    .line 625
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->getBatteryIDFilePath()Ljava/lang/String;

    #@501
    move-result-object v4

    #@502
    .line 626
    .restart local v4       #_result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@505
    .line 627
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@508
    goto/16 :goto_9

    #@50a
    .line 632
    .end local v4           #_result:Ljava/lang/String;
    :sswitch_50a
    const-string v7, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@50c
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@50f
    .line 633
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->IsSupportHookKeyTest()Z

    #@512
    move-result v4

    #@513
    .line 634
    .local v4, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@516
    .line 635
    if-eqz v4, :cond_519

    #@518
    move v5, v6

    #@519
    :cond_519
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@51c
    goto/16 :goto_9

    #@51e
    .line 640
    .end local v4           #_result:Z
    :sswitch_51e
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@520
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@523
    .line 641
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->getAATSWversion()Ljava/lang/String;

    #@526
    move-result-object v4

    #@527
    .line 642
    .local v4, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@52a
    .line 643
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@52d
    goto/16 :goto_9

    #@52f
    .line 648
    .end local v4           #_result:Ljava/lang/String;
    :sswitch_52f
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@531
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@534
    .line 650
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@537
    move-result v0

    #@538
    .line 651
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->getSaveVolume(I)I

    #@53b
    move-result v4

    #@53c
    .line 652
    .local v4, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@53f
    .line 653
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@542
    goto/16 :goto_9

    #@544
    .line 658
    .end local v0           #_arg0:I
    .end local v4           #_result:I
    :sswitch_544
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@546
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@549
    .line 660
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@54c
    move-result v0

    #@54d
    .line 661
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->getMaxVolume(I)I

    #@550
    move-result v4

    #@551
    .line 662
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@554
    .line 663
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@557
    goto/16 :goto_9

    #@559
    .line 668
    .end local v0           #_arg0:I
    .end local v4           #_result:I
    :sswitch_559
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@55b
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@55e
    .line 669
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->enableOisProp()V

    #@561
    .line 670
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@564
    goto/16 :goto_9

    #@566
    .line 675
    :sswitch_566
    const-string v5, "com.lge.systemservice.core.aatmanager.IAATManager"

    #@568
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@56b
    .line 676
    invoke-virtual {p0}, Lcom/lge/systemservice/core/aatmanager/IAATManager$Stub;->disableOisProp()V

    #@56e
    .line 677
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@571
    goto/16 :goto_9

    #@573
    .line 38
    nop

    #@574
    :sswitch_data_574
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_1c
        0x3 -> :sswitch_28
        0x4 -> :sswitch_40
        0x5 -> :sswitch_4c
        0x6 -> :sswitch_58
        0x7 -> :sswitch_6c
        0x8 -> :sswitch_7f
        0x9 -> :sswitch_8c
        0xa -> :sswitch_99
        0xb -> :sswitch_ad
        0xc -> :sswitch_c1
        0xd -> :sswitch_d9
        0xe -> :sswitch_f1
        0xf -> :sswitch_109
        0x10 -> :sswitch_121
        0x11 -> :sswitch_139
        0x12 -> :sswitch_14f
        0x13 -> :sswitch_160
        0x14 -> :sswitch_176
        0x15 -> :sswitch_18e
        0x16 -> :sswitch_1a2
        0x17 -> :sswitch_1b6
        0x18 -> :sswitch_1c7
        0x19 -> :sswitch_1d8
        0x1a -> :sswitch_1e5
        0x1b -> :sswitch_1f2
        0x1c -> :sswitch_203
        0x1d -> :sswitch_21c
        0x1e -> :sswitch_22d
        0x1f -> :sswitch_23a
        0x20 -> :sswitch_24f
        0x21 -> :sswitch_263
        0x22 -> :sswitch_274
        0x23 -> :sswitch_285
        0x24 -> :sswitch_296
        0x25 -> :sswitch_2a7
        0x26 -> :sswitch_2bb
        0x27 -> :sswitch_2cf
        0x28 -> :sswitch_2e3
        0x29 -> :sswitch_2f7
        0x2a -> :sswitch_30b
        0x2b -> :sswitch_318
        0x2c -> :sswitch_32c
        0x2d -> :sswitch_340
        0x2e -> :sswitch_354
        0x2f -> :sswitch_36c
        0x30 -> :sswitch_38d
        0x31 -> :sswitch_39e
        0x32 -> :sswitch_3af
        0x33 -> :sswitch_3c3
        0x34 -> :sswitch_3d9
        0x35 -> :sswitch_3ea
        0x36 -> :sswitch_3fe
        0x37 -> :sswitch_412
        0x38 -> :sswitch_426
        0x39 -> :sswitch_437
        0x3a -> :sswitch_44f
        0x3b -> :sswitch_464
        0x3c -> :sswitch_475
        0x3d -> :sswitch_486
        0x3e -> :sswitch_497
        0x3f -> :sswitch_4a8
        0x40 -> :sswitch_4b9
        0x41 -> :sswitch_4c6
        0x42 -> :sswitch_4d7
        0x43 -> :sswitch_4e8
        0x44 -> :sswitch_4f9
        0x45 -> :sswitch_50a
        0x46 -> :sswitch_51e
        0x47 -> :sswitch_52f
        0x48 -> :sswitch_544
        0x49 -> :sswitch_559
        0x4a -> :sswitch_566
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
