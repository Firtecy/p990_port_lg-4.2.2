.class public abstract Lcom/lge/systemservice/core/LGSingleton;
.super Ljava/lang/Object;
.source "LGSingleton.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private volatile mInstance:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 3
    .local p0, this:Lcom/lge/systemservice/core/LGSingleton;,"Lcom/lge/systemservice/core/LGSingleton<TT;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method protected abstract create()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public final get()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    #@0
    .prologue
    .line 13
    .local p0, this:Lcom/lge/systemservice/core/LGSingleton;,"Lcom/lge/systemservice/core/LGSingleton<TT;>;"
    iget-object v0, p0, Lcom/lge/systemservice/core/LGSingleton;->mInstance:Ljava/lang/Object;

    #@2
    if-nez v0, :cond_10

    #@4
    .line 14
    monitor-enter p0

    #@5
    .line 15
    :try_start_5
    iget-object v0, p0, Lcom/lge/systemservice/core/LGSingleton;->mInstance:Ljava/lang/Object;

    #@7
    if-nez v0, :cond_f

    #@9
    .line 16
    invoke-virtual {p0}, Lcom/lge/systemservice/core/LGSingleton;->create()Ljava/lang/Object;

    #@c
    move-result-object v0

    #@d
    iput-object v0, p0, Lcom/lge/systemservice/core/LGSingleton;->mInstance:Ljava/lang/Object;

    #@f
    .line 18
    :cond_f
    monitor-exit p0
    :try_end_10
    .catchall {:try_start_5 .. :try_end_10} :catchall_13

    #@10
    .line 20
    :cond_10
    iget-object v0, p0, Lcom/lge/systemservice/core/LGSingleton;->mInstance:Ljava/lang/Object;

    #@12
    return-object v0

    #@13
    .line 18
    :catchall_13
    move-exception v0

    #@14
    :try_start_14
    monitor-exit p0
    :try_end_15
    .catchall {:try_start_14 .. :try_end_15} :catchall_13

    #@15
    throw v0
.end method

.method protected final reset()V
    .registers 2

    #@0
    .prologue
    .line 9
    .local p0, this:Lcom/lge/systemservice/core/LGSingleton;,"Lcom/lge/systemservice/core/LGSingleton<TT;>;"
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Lcom/lge/systemservice/core/LGSingleton;->mInstance:Ljava/lang/Object;

    #@3
    .line 10
    return-void
.end method
