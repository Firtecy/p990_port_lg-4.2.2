.class public Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;
.super Landroid/widget/LinearLayout;
.source "ClipDataCellView.java"


# static fields
.field private static THUMBNAIL_HEIGHT:I

.field private static THUMBNAIL_WIDTH:I


# instance fields
.field private mCellImageView:Landroid/widget/ImageView;

.field private mCellText:Ljava/lang/String;

.field private mCellTextView:Landroid/widget/TextView;

.field private mCellType:I

.field private mCellUri:Landroid/net/Uri;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 37
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    #@4
    .line 32
    iput-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mCellTextView:Landroid/widget/TextView;

    #@6
    .line 33
    iput-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mCellImageView:Landroid/widget/ImageView;

    #@8
    .line 38
    iput-object p1, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mContext:Landroid/content/Context;

    #@a
    .line 40
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mContext:Landroid/content/Context;

    #@c
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@f
    move-result-object v0

    #@10
    const v1, 0x20c0065

    #@13
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@16
    move-result v0

    #@17
    sput v0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->THUMBNAIL_HEIGHT:I

    #@19
    .line 41
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mContext:Landroid/content/Context;

    #@1b
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1e
    move-result-object v0

    #@1f
    const v1, 0x20c0064

    #@22
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@25
    move-result v0

    #@26
    sput v0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->THUMBNAIL_WIDTH:I

    #@28
    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 5
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 45
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 32
    iput-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mCellTextView:Landroid/widget/TextView;

    #@6
    .line 33
    iput-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mCellImageView:Landroid/widget/ImageView;

    #@8
    .line 46
    iput-object p1, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mContext:Landroid/content/Context;

    #@a
    .line 48
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mContext:Landroid/content/Context;

    #@c
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@f
    move-result-object v0

    #@10
    const v1, 0x20c0065

    #@13
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@16
    move-result v0

    #@17
    sput v0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->THUMBNAIL_HEIGHT:I

    #@19
    .line 49
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mContext:Landroid/content/Context;

    #@1b
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1e
    move-result-object v0

    #@1f
    const v1, 0x20c0064

    #@22
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@25
    move-result v0

    #@26
    sput v0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->THUMBNAIL_WIDTH:I

    #@28
    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 6
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 53
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 32
    iput-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mCellTextView:Landroid/widget/TextView;

    #@6
    .line 33
    iput-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mCellImageView:Landroid/widget/ImageView;

    #@8
    .line 54
    iput-object p1, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mContext:Landroid/content/Context;

    #@a
    .line 56
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mContext:Landroid/content/Context;

    #@c
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@f
    move-result-object v0

    #@10
    const v1, 0x20c0065

    #@13
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@16
    move-result v0

    #@17
    sput v0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->THUMBNAIL_HEIGHT:I

    #@19
    .line 57
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mContext:Landroid/content/Context;

    #@1b
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1e
    move-result-object v0

    #@1f
    const v1, 0x20c0064

    #@22
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@25
    move-result v0

    #@26
    sput v0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->THUMBNAIL_WIDTH:I

    #@28
    .line 58
    return-void
.end method

.method private calculateInSampleSize(II)I
    .registers 8
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 173
    const/4 v1, 0x1

    #@1
    .line 174
    .local v1, inSampleSize:I
    sget v3, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->THUMBNAIL_HEIGHT:I

    #@3
    if-gt p2, v3, :cond_9

    #@5
    sget v3, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->THUMBNAIL_WIDTH:I

    #@7
    if-le p1, v3, :cond_1e

    #@9
    .line 175
    :cond_9
    int-to-float v3, p2

    #@a
    sget v4, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->THUMBNAIL_HEIGHT:I

    #@c
    int-to-float v4, v4

    #@d
    div-float/2addr v3, v4

    #@e
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    #@11
    move-result v0

    #@12
    .line 176
    .local v0, heightRatio:I
    int-to-float v3, p1

    #@13
    sget v4, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->THUMBNAIL_WIDTH:I

    #@15
    int-to-float v4, v4

    #@16
    div-float/2addr v3, v4

    #@17
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    #@1a
    move-result v2

    #@1b
    .line 178
    .local v2, widthRatio:I
    if-ge v0, v2, :cond_1f

    #@1d
    move v1, v0

    #@1e
    .line 180
    .end local v0           #heightRatio:I
    .end local v2           #widthRatio:I
    :cond_1e
    :goto_1e
    return v1

    #@1f
    .restart local v0       #heightRatio:I
    .restart local v2       #widthRatio:I
    :cond_1f
    move v1, v2

    #@20
    .line 178
    goto :goto_1e
.end method

.method private getThumbnailFromUri(Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .registers 8
    .parameter "uri"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 149
    const/4 v1, 0x0

    #@2
    .line 152
    .local v1, input:Ljava/io/InputStream;
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    #@4
    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    #@7
    .line 153
    .local v2, options:Landroid/graphics/BitmapFactory$Options;
    const/4 v4, 0x1

    #@8
    iput-boolean v4, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    #@a
    .line 155
    :try_start_a
    iget-object v4, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mContext:Landroid/content/Context;

    #@c
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@f
    move-result-object v4

    #@10
    invoke-virtual {v4, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    #@13
    move-result-object v4

    #@14
    const/4 v5, 0x0

    #@15
    invoke-static {v4, v5, v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_18
    .catch Ljava/io/FileNotFoundException; {:try_start_a .. :try_end_18} :catch_39

    #@18
    .line 161
    :goto_18
    iget v4, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    #@1a
    iget v5, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    #@1c
    invoke-direct {p0, v4, v5}, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->calculateInSampleSize(II)I

    #@1f
    move-result v4

    #@20
    iput v4, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    #@22
    .line 162
    sget-object v4, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    #@24
    iput-object v4, v2, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    #@26
    .line 164
    const/4 v4, 0x0

    #@27
    :try_start_27
    iput-boolean v4, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    #@29
    .line 165
    iget-object v4, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mContext:Landroid/content/Context;

    #@2b
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2e
    move-result-object v4

    #@2f
    invoke-virtual {v4, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    #@32
    move-result-object v4

    #@33
    const/4 v5, 0x0

    #@34
    invoke-static {v4, v5, v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_37
    .catch Ljava/io/FileNotFoundException; {:try_start_27 .. :try_end_37} :catch_3e

    #@37
    move-result-object v3

    #@38
    .line 168
    :goto_38
    return-object v3

    #@39
    .line 156
    :catch_39
    move-exception v0

    #@3a
    .line 157
    .local v0, e:Ljava/io/FileNotFoundException;
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    #@3d
    goto :goto_18

    #@3e
    .line 166
    .end local v0           #e:Ljava/io/FileNotFoundException;
    :catch_3e
    move-exception v0

    #@3f
    .line 167
    .restart local v0       #e:Ljava/io/FileNotFoundException;
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    #@42
    goto :goto_38
.end method


# virtual methods
.method public getChildType()I
    .registers 2

    #@0
    .prologue
    .line 109
    iget v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mCellType:I

    #@2
    return v0
.end method

.method public remove()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 137
    invoke-virtual {p0}, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->getChildType()I

    #@4
    move-result v1

    #@5
    if-eqz v1, :cond_1c

    #@7
    iget-object v1, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mCellImageView:Landroid/widget/ImageView;

    #@9
    if-eqz v1, :cond_1c

    #@b
    .line 138
    iget-object v1, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mCellImageView:Landroid/widget/ImageView;

    #@d
    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    #@10
    move-result-object v0

    #@11
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    #@13
    .line 139
    .local v0, bd:Landroid/graphics/drawable/BitmapDrawable;
    if-eqz v0, :cond_1c

    #@15
    .line 140
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    #@1c
    .line 144
    .end local v0           #bd:Landroid/graphics/drawable/BitmapDrawable;
    :cond_1c
    iput-object v2, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mCellImageView:Landroid/widget/ImageView;

    #@1e
    .line 145
    iput-object v2, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mCellTextView:Landroid/widget/TextView;

    #@20
    .line 146
    return-void
.end method

.method public setChildData(ILjava/lang/String;)V
    .registers 4
    .parameter "datatype"
    .parameter "text"

    #@0
    .prologue
    .line 61
    iput p1, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mCellType:I

    #@2
    .line 62
    iput-object p2, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mCellText:Ljava/lang/String;

    #@4
    .line 64
    const v0, 0x20d0049

    #@7
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->findViewById(I)Landroid/view/View;

    #@a
    move-result-object v0

    #@b
    check-cast v0, Landroid/widget/TextView;

    #@d
    iput-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mCellTextView:Landroid/widget/TextView;

    #@f
    .line 65
    iget-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mCellTextView:Landroid/widget/TextView;

    #@11
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@14
    .line 67
    const/4 v0, 0x0

    #@15
    iput-object v0, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mCellImageView:Landroid/widget/ImageView;

    #@17
    .line 68
    return-void
.end method

.method public setChildData(ILandroid/net/Uri;)Z
    .registers 5
    .parameter "datatype"
    .parameter "uri"

    #@0
    .prologue
    .line 71
    iput p1, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mCellType:I

    #@2
    .line 72
    iput-object p2, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mCellUri:Landroid/net/Uri;

    #@4
    .line 74
    const/4 v1, 0x0

    #@5
    iput-object v1, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mCellTextView:Landroid/widget/TextView;

    #@7
    .line 76
    const v1, 0x20d0048

    #@a
    invoke-virtual {p0, v1}, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->findViewById(I)Landroid/view/View;

    #@d
    move-result-object v1

    #@e
    check-cast v1, Landroid/widget/ImageView;

    #@10
    iput-object v1, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mCellImageView:Landroid/widget/ImageView;

    #@12
    .line 78
    invoke-direct {p0, p2}, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->getThumbnailFromUri(Landroid/net/Uri;)Landroid/graphics/Bitmap;

    #@15
    move-result-object v0

    #@16
    .line 79
    .local v0, img:Landroid/graphics/Bitmap;
    if-nez v0, :cond_1a

    #@18
    .line 80
    const/4 v1, 0x0

    #@19
    .line 85
    :goto_19
    return v1

    #@1a
    .line 83
    :cond_1a
    iget-object v1, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mCellImageView:Landroid/widget/ImageView;

    #@1c
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    #@1f
    .line 84
    const/4 v0, 0x0

    #@20
    .line 85
    const/4 v1, 0x1

    #@21
    goto :goto_19
.end method

.method public setChildData(ILjava/lang/String;Landroid/net/Uri;)Z
    .registers 6
    .parameter "datatype"
    .parameter "text"
    .parameter "uri"

    #@0
    .prologue
    .line 89
    iput p1, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mCellType:I

    #@2
    .line 90
    iput-object p2, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mCellText:Ljava/lang/String;

    #@4
    .line 91
    iput-object p3, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mCellUri:Landroid/net/Uri;

    #@6
    .line 93
    const v1, 0x20d0049

    #@9
    invoke-virtual {p0, v1}, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->findViewById(I)Landroid/view/View;

    #@c
    move-result-object v1

    #@d
    check-cast v1, Landroid/widget/TextView;

    #@f
    iput-object v1, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mCellTextView:Landroid/widget/TextView;

    #@11
    .line 94
    iget-object v1, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mCellTextView:Landroid/widget/TextView;

    #@13
    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@16
    .line 96
    const v1, 0x20d0048

    #@19
    invoke-virtual {p0, v1}, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->findViewById(I)Landroid/view/View;

    #@1c
    move-result-object v1

    #@1d
    check-cast v1, Landroid/widget/ImageView;

    #@1f
    iput-object v1, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mCellImageView:Landroid/widget/ImageView;

    #@21
    .line 98
    invoke-direct {p0, p3}, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->getThumbnailFromUri(Landroid/net/Uri;)Landroid/graphics/Bitmap;

    #@24
    move-result-object v0

    #@25
    .line 99
    .local v0, img:Landroid/graphics/Bitmap;
    if-nez v0, :cond_29

    #@27
    .line 100
    const/4 v1, 0x0

    #@28
    .line 105
    :goto_28
    return v1

    #@29
    .line 103
    :cond_29
    iget-object v1, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mCellImageView:Landroid/widget/ImageView;

    #@2b
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    #@2e
    .line 104
    const/4 v0, 0x0

    #@2f
    .line 105
    const/4 v1, 0x1

    #@30
    goto :goto_28
.end method

.method public setDim(I)V
    .registers 7
    .parameter "datatype"

    #@0
    .prologue
    const v4, 0x20d004c

    #@3
    const/4 v3, 0x0

    #@4
    .line 113
    iget-object v1, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mCellTextView:Landroid/widget/TextView;

    #@6
    if-eqz v1, :cond_13

    #@8
    .line 114
    const/4 v1, 0x1

    #@9
    if-ne p1, v1, :cond_25

    #@b
    .line 115
    iget-object v1, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mCellTextView:Landroid/widget/TextView;

    #@d
    const v2, -0xbbbbbc

    #@10
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackgroundColor(I)V

    #@13
    .line 121
    :cond_13
    :goto_13
    iget-object v1, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mCellImageView:Landroid/widget/ImageView;

    #@15
    if-eqz v1, :cond_24

    #@17
    .line 122
    if-nez p1, :cond_2b

    #@19
    .line 123
    invoke-virtual {p0, v4}, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->findViewById(I)Landroid/view/View;

    #@1c
    move-result-object v0

    #@1d
    check-cast v0, Landroid/widget/ImageView;

    #@1f
    .line 124
    .local v0, dim:Landroid/widget/ImageView;
    if-eqz v0, :cond_24

    #@21
    .line 125
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    #@24
    .line 134
    .end local v0           #dim:Landroid/widget/ImageView;
    :cond_24
    :goto_24
    return-void

    #@25
    .line 117
    :cond_25
    iget-object v1, p0, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->mCellTextView:Landroid/widget/TextView;

    #@27
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setBackgroundColor(I)V

    #@2a
    goto :goto_13

    #@2b
    .line 128
    :cond_2b
    invoke-virtual {p0, v4}, Lcom/lge/systemservice/core/cliptraymanager/ClipDataCellView;->findViewById(I)Landroid/view/View;

    #@2e
    move-result-object v0

    #@2f
    check-cast v0, Landroid/widget/ImageView;

    #@31
    .line 129
    .restart local v0       #dim:Landroid/widget/ImageView;
    if-eqz v0, :cond_24

    #@33
    .line 130
    const/16 v1, 0x8

    #@35
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    #@38
    goto :goto_24
.end method
