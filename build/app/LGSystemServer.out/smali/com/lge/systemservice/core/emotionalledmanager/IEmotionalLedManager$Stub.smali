.class public abstract Lcom/lge/systemservice/core/emotionalledmanager/IEmotionalLedManager$Stub;
.super Landroid/os/Binder;
.source "IEmotionalLedManager.java"

# interfaces
.implements Lcom/lge/systemservice/core/emotionalledmanager/IEmotionalLedManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/core/emotionalledmanager/IEmotionalLedManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/systemservice/core/emotionalledmanager/IEmotionalLedManager$Stub$Proxy;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 15
    const-string v0, "com.lge.systemservice.core.emotionalledmanager.IEmotionalLedManager"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/lge/systemservice/core/emotionalledmanager/IEmotionalLedManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/lge/systemservice/core/emotionalledmanager/IEmotionalLedManager;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 23
    if-nez p0, :cond_4

    #@2
    .line 24
    const/4 v0, 0x0

    #@3
    .line 30
    :goto_3
    return-object v0

    #@4
    .line 26
    :cond_4
    const-string v1, "com.lge.systemservice.core.emotionalledmanager.IEmotionalLedManager"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 27
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/lge/systemservice/core/emotionalledmanager/IEmotionalLedManager;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 28
    check-cast v0, Lcom/lge/systemservice/core/emotionalledmanager/IEmotionalLedManager;

    #@12
    goto :goto_3

    #@13
    .line 30
    :cond_13
    new-instance v0, Lcom/lge/systemservice/core/emotionalledmanager/IEmotionalLedManager$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/lge/systemservice/core/emotionalledmanager/IEmotionalLedManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 15
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    .line 38
    sparse-switch p1, :sswitch_data_b4

    #@4
    .line 127
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v0

    #@8
    :goto_8
    return v0

    #@9
    .line 42
    :sswitch_9
    const-string v0, "com.lge.systemservice.core.emotionalledmanager.IEmotionalLedManager"

    #@b
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    move v0, v9

    #@f
    .line 43
    goto :goto_8

    #@10
    .line 47
    :sswitch_10
    const-string v0, "com.lge.systemservice.core.emotionalledmanager.IEmotionalLedManager"

    #@12
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    .line 51
    .local v1, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1c
    move-result v2

    #@1d
    .line 53
    .local v2, _arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@20
    move-result v0

    #@21
    if-eqz v0, :cond_33

    #@23
    .line 54
    sget-object v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->CREATOR:Landroid/os/Parcelable$Creator;

    #@25
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@28
    move-result-object v3

    #@29
    check-cast v3, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@2b
    .line 59
    .local v3, _arg2:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
    :goto_2b
    invoke-virtual {p0, v1, v2, v3}, Lcom/lge/systemservice/core/emotionalledmanager/IEmotionalLedManager$Stub;->start(Ljava/lang/String;ILcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;)V

    #@2e
    .line 60
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@31
    move v0, v9

    #@32
    .line 61
    goto :goto_8

    #@33
    .line 57
    .end local v3           #_arg2:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
    :cond_33
    const/4 v3, 0x0

    #@34
    .restart local v3       #_arg2:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
    goto :goto_2b

    #@35
    .line 65
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:I
    .end local v3           #_arg2:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
    :sswitch_35
    const-string v0, "com.lge.systemservice.core.emotionalledmanager.IEmotionalLedManager"

    #@37
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3a
    .line 67
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3d
    move-result-object v1

    #@3e
    .line 69
    .restart local v1       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@41
    move-result v2

    #@42
    .line 70
    .restart local v2       #_arg1:I
    invoke-virtual {p0, v1, v2}, Lcom/lge/systemservice/core/emotionalledmanager/IEmotionalLedManager$Stub;->stop(Ljava/lang/String;I)V

    #@45
    .line 71
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@48
    move v0, v9

    #@49
    .line 72
    goto :goto_8

    #@4a
    .line 76
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:I
    :sswitch_4a
    const-string v0, "com.lge.systemservice.core.emotionalledmanager.IEmotionalLedManager"

    #@4c
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4f
    .line 77
    invoke-virtual {p0}, Lcom/lge/systemservice/core/emotionalledmanager/IEmotionalLedManager$Stub;->clearAllLeds()V

    #@52
    .line 78
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@55
    move v0, v9

    #@56
    .line 79
    goto :goto_8

    #@57
    .line 83
    :sswitch_57
    const-string v0, "com.lge.systemservice.core.emotionalledmanager.IEmotionalLedManager"

    #@59
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5c
    .line 84
    invoke-virtual {p0}, Lcom/lge/systemservice/core/emotionalledmanager/IEmotionalLedManager$Stub;->restart()V

    #@5f
    .line 85
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@62
    move v0, v9

    #@63
    .line 86
    goto :goto_8

    #@64
    .line 90
    :sswitch_64
    const-string v0, "com.lge.systemservice.core.emotionalledmanager.IEmotionalLedManager"

    #@66
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@69
    .line 92
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@6c
    move-result v1

    #@6d
    .line 94
    .local v1, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@70
    move-result v2

    #@71
    .line 96
    .restart local v2       #_arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@74
    move-result v3

    #@75
    .line 98
    .local v3, _arg2:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@78
    move-result v4

    #@79
    .line 100
    .local v4, _arg3:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@7c
    move-result v5

    #@7d
    .line 102
    .local v5, _arg4:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@80
    move-result v6

    #@81
    .line 104
    .local v6, _arg5:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@84
    move-result-object v7

    #@85
    .local v7, _arg6:Ljava/lang/String;
    move-object v0, p0

    #@86
    .line 105
    invoke-virtual/range {v0 .. v7}, Lcom/lge/systemservice/core/emotionalledmanager/IEmotionalLedManager$Stub;->updateLightList(IIIIIILjava/lang/String;)V

    #@89
    .line 106
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@8c
    move v0, v9

    #@8d
    .line 107
    goto/16 :goto_8

    #@8f
    .line 111
    .end local v1           #_arg0:I
    .end local v2           #_arg1:I
    .end local v3           #_arg2:I
    .end local v4           #_arg3:I
    .end local v5           #_arg4:I
    .end local v6           #_arg5:I
    .end local v7           #_arg6:Ljava/lang/String;
    :sswitch_8f
    const-string v0, "com.lge.systemservice.core.emotionalledmanager.IEmotionalLedManager"

    #@91
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@94
    .line 113
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@97
    move-result v1

    #@98
    .line 114
    .restart local v1       #_arg0:I
    invoke-virtual {p0, v1}, Lcom/lge/systemservice/core/emotionalledmanager/IEmotionalLedManager$Stub;->setBrightness(I)V

    #@9b
    .line 115
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@9e
    move v0, v9

    #@9f
    .line 116
    goto/16 :goto_8

    #@a1
    .line 120
    .end local v1           #_arg0:I
    :sswitch_a1
    const-string v0, "com.lge.systemservice.core.emotionalledmanager.IEmotionalLedManager"

    #@a3
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a6
    .line 121
    invoke-virtual {p0}, Lcom/lge/systemservice/core/emotionalledmanager/IEmotionalLedManager$Stub;->getEmotionalLedType()I

    #@a9
    move-result v8

    #@aa
    .line 122
    .local v8, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@ad
    .line 123
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@b0
    move v0, v9

    #@b1
    .line 124
    goto/16 :goto_8

    #@b3
    .line 38
    nop

    #@b4
    :sswitch_data_b4
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_35
        0x3 -> :sswitch_4a
        0x4 -> :sswitch_57
        0x5 -> :sswitch_64
        0x6 -> :sswitch_8f
        0x7 -> :sswitch_a1
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
