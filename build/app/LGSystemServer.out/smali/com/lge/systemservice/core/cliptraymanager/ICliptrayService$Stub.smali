.class public abstract Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService$Stub;
.super Landroid/os/Binder;
.source "ICliptrayService.java"

# interfaces
.implements Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService$Stub$Proxy;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 15
    const-string v0, "com.lge.systemservice.core.cliptraymanager.ICliptrayService"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 23
    if-nez p0, :cond_4

    #@2
    .line 24
    const/4 v0, 0x0

    #@3
    .line 30
    :goto_3
    return-object v0

    #@4
    .line 26
    :cond_4
    const-string v1, "com.lge.systemservice.core.cliptraymanager.ICliptrayService"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 27
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 28
    check-cast v0, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService;

    #@12
    goto :goto_3

    #@13
    .line 30
    :cond_13
    new-instance v0, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 9
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 38
    sparse-switch p1, :sswitch_data_126

    #@5
    .line 185
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v2

    #@9
    :goto_9
    return v2

    #@a
    .line 42
    :sswitch_a
    const-string v3, "com.lge.systemservice.core.cliptraymanager.ICliptrayService"

    #@c
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 47
    :sswitch_10
    const-string v3, "com.lge.systemservice.core.cliptraymanager.ICliptrayService"

    #@12
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 48
    invoke-virtual {p0}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService$Stub;->getShow()V

    #@18
    .line 49
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1b
    goto :goto_9

    #@1c
    .line 54
    :sswitch_1c
    const-string v3, "com.lge.systemservice.core.cliptraymanager.ICliptrayService"

    #@1e
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@21
    .line 55
    invoke-virtual {p0}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService$Stub;->getClose()V

    #@24
    .line 56
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@27
    goto :goto_9

    #@28
    .line 61
    :sswitch_28
    const-string v3, "com.lge.systemservice.core.cliptraymanager.ICliptrayService"

    #@2a
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2d
    .line 62
    invoke-virtual {p0}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService$Stub;->addNewClipData()V

    #@30
    .line 63
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@33
    goto :goto_9

    #@34
    .line 68
    :sswitch_34
    const-string v3, "com.lge.systemservice.core.cliptraymanager.ICliptrayService"

    #@36
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@39
    .line 69
    invoke-virtual {p0}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService$Stub;->getPeek()V

    #@3c
    .line 70
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3f
    goto :goto_9

    #@40
    .line 75
    :sswitch_40
    const-string v3, "com.lge.systemservice.core.cliptraymanager.ICliptrayService"

    #@42
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@45
    .line 77
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@48
    move-result-object v3

    #@49
    invoke-static {v3}, Lcom/lge/systemservice/core/cliptraymanager/IOnPasteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/lge/systemservice/core/cliptraymanager/IOnPasteListener;

    #@4c
    move-result-object v0

    #@4d
    .line 78
    .local v0, _arg0:Lcom/lge/systemservice/core/cliptraymanager/IOnPasteListener;
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService$Stub;->setPasteListener(Lcom/lge/systemservice/core/cliptraymanager/IOnPasteListener;)V

    #@50
    .line 79
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@53
    goto :goto_9

    #@54
    .line 84
    .end local v0           #_arg0:Lcom/lge/systemservice/core/cliptraymanager/IOnPasteListener;
    :sswitch_54
    const-string v3, "com.lge.systemservice.core.cliptraymanager.ICliptrayService"

    #@56
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@59
    .line 86
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@5c
    move-result-object v3

    #@5d
    invoke-static {v3}, Lcom/lge/systemservice/core/cliptraymanager/IOnPasteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/lge/systemservice/core/cliptraymanager/IOnPasteListener;

    #@60
    move-result-object v0

    #@61
    .line 87
    .restart local v0       #_arg0:Lcom/lge/systemservice/core/cliptraymanager/IOnPasteListener;
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService$Stub;->removePasteListener(Lcom/lge/systemservice/core/cliptraymanager/IOnPasteListener;)V

    #@64
    .line 88
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@67
    goto :goto_9

    #@68
    .line 93
    .end local v0           #_arg0:Lcom/lge/systemservice/core/cliptraymanager/IOnPasteListener;
    :sswitch_68
    const-string v3, "com.lge.systemservice.core.cliptraymanager.ICliptrayService"

    #@6a
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6d
    .line 94
    invoke-virtual {p0}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService$Stub;->getVisibility()I

    #@70
    move-result v1

    #@71
    .line 95
    .local v1, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@74
    .line 96
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@77
    goto :goto_9

    #@78
    .line 101
    .end local v1           #_result:I
    :sswitch_78
    const-string v3, "com.lge.systemservice.core.cliptraymanager.ICliptrayService"

    #@7a
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@7d
    .line 103
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@80
    move-result v0

    #@81
    .line 104
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService$Stub;->setInputType(I)V

    #@84
    .line 105
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@87
    goto :goto_9

    #@88
    .line 110
    .end local v0           #_arg0:I
    :sswitch_88
    const-string v3, "com.lge.systemservice.core.cliptraymanager.ICliptrayService"

    #@8a
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@8d
    .line 112
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@90
    move-result v3

    #@91
    if-eqz v3, :cond_94

    #@93
    move v0, v2

    #@94
    .line 113
    .local v0, _arg0:Z
    :cond_94
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService$Stub;->orientationChanged(Z)V

    #@97
    .line 114
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@9a
    goto/16 :goto_9

    #@9c
    .line 119
    .end local v0           #_arg0:Z
    :sswitch_9c
    const-string v3, "com.lge.systemservice.core.cliptraymanager.ICliptrayService"

    #@9e
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a1
    .line 120
    invoke-virtual {p0}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService$Stub;->showCliptraycue()V

    #@a4
    .line 121
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@a7
    goto/16 :goto_9

    #@a9
    .line 126
    :sswitch_a9
    const-string v3, "com.lge.systemservice.core.cliptraymanager.ICliptrayService"

    #@ab
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ae
    .line 127
    invoke-virtual {p0}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService$Stub;->showCliptraycueClose()V

    #@b1
    .line 128
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@b4
    goto/16 :goto_9

    #@b6
    .line 133
    :sswitch_b6
    const-string v3, "com.lge.systemservice.core.cliptraymanager.ICliptrayService"

    #@b8
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@bb
    .line 134
    invoke-virtual {p0}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService$Stub;->hideCliptraycue()V

    #@be
    .line 135
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@c1
    goto/16 :goto_9

    #@c3
    .line 140
    :sswitch_c3
    const-string v3, "com.lge.systemservice.core.cliptraymanager.ICliptrayService"

    #@c5
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c8
    .line 141
    invoke-virtual {p0}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService$Stub;->isCliptraycueShowing()Z

    #@cb
    move-result v1

    #@cc
    .line 142
    .local v1, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@cf
    .line 143
    if-eqz v1, :cond_d2

    #@d1
    move v0, v2

    #@d2
    :cond_d2
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@d5
    goto/16 :goto_9

    #@d7
    .line 148
    .end local v1           #_result:Z
    :sswitch_d7
    const-string v3, "com.lge.systemservice.core.cliptraymanager.ICliptrayService"

    #@d9
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@dc
    .line 149
    invoke-virtual {p0}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService$Stub;->showDecodeErrorToast()V

    #@df
    .line 150
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@e2
    goto/16 :goto_9

    #@e4
    .line 155
    :sswitch_e4
    const-string v3, "com.lge.systemservice.core.cliptraymanager.ICliptrayService"

    #@e6
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@e9
    .line 157
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@ec
    move-result v3

    #@ed
    if-eqz v3, :cond_f0

    #@ef
    move v0, v2

    #@f0
    .line 158
    .restart local v0       #_arg0:Z
    :cond_f0
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService$Stub;->setServiceConnected(Z)V

    #@f3
    .line 159
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@f6
    goto/16 :goto_9

    #@f8
    .line 164
    .end local v0           #_arg0:Z
    :sswitch_f8
    const-string v3, "com.lge.systemservice.core.cliptraymanager.ICliptrayService"

    #@fa
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@fd
    .line 165
    invoke-virtual {p0}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService$Stub;->getServiceConnected()Z

    #@100
    move-result v1

    #@101
    .line 166
    .restart local v1       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@104
    .line 167
    if-eqz v1, :cond_107

    #@106
    move v0, v2

    #@107
    :cond_107
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@10a
    goto/16 :goto_9

    #@10c
    .line 172
    .end local v1           #_result:Z
    :sswitch_10c
    const-string v3, "com.lge.systemservice.core.cliptraymanager.ICliptrayService"

    #@10e
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@111
    .line 173
    invoke-virtual {p0}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService$Stub;->doCopyAnimation()V

    #@114
    .line 174
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@117
    goto/16 :goto_9

    #@119
    .line 179
    :sswitch_119
    const-string v3, "com.lge.systemservice.core.cliptraymanager.ICliptrayService"

    #@11b
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@11e
    .line 180
    invoke-virtual {p0}, Lcom/lge/systemservice/core/cliptraymanager/ICliptrayService$Stub;->showCliptrayCopiedToast()V

    #@121
    .line 181
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@124
    goto/16 :goto_9

    #@126
    .line 38
    :sswitch_data_126
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_1c
        0x3 -> :sswitch_28
        0x4 -> :sswitch_34
        0x5 -> :sswitch_40
        0x6 -> :sswitch_54
        0x7 -> :sswitch_68
        0x8 -> :sswitch_78
        0x9 -> :sswitch_88
        0xa -> :sswitch_9c
        0xb -> :sswitch_a9
        0xc -> :sswitch_b6
        0xd -> :sswitch_c3
        0xe -> :sswitch_d7
        0xf -> :sswitch_e4
        0x10 -> :sswitch_f8
        0x11 -> :sswitch_10c
        0x12 -> :sswitch_119
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
