.class Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread$1;
.super Landroid/os/Handler;
.source "NfcLgFactoryAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 829
    iput-object p1, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread$1;->this$1:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 11
    .parameter "msg"

    #@0
    .prologue
    const-wide/16 v7, 0x64

    #@2
    const/4 v6, 0x0

    #@3
    .line 831
    const/16 v3, 0x80

    #@5
    new-array v1, v3, [B

    #@7
    .line 833
    .local v1, resData:[B
    const/4 v2, 0x0

    #@8
    .line 834
    .local v2, result:I
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@b
    move-result-object v0

    #@c
    .line 836
    .local v0, b:Landroid/os/Bundle;
    const-string v3, "NfcFactoryAdapter"

    #@e
    new-instance v4, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v5, "NfcTestThread::handler In!==>"

    #@15
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v4

    #@19
    iget v5, p1, Landroid/os/Message;->what:I

    #@1b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v4

    #@1f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v4

    #@23
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 838
    iget-object v3, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread$1;->this$1:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@28
    iget-object v3, v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->this$0:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;

    #@2a
    invoke-static {v3}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->access$500(Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;)Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@2d
    move-result-object v3

    #@2e
    if-nez v3, :cond_38

    #@30
    .line 839
    const-string v3, "NfcFactoryAdapter"

    #@32
    const-string v4, "@@NfcTestThread::mNfcLgManagerLegacy Null"

    #@34
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 909
    :goto_37
    return-void

    #@38
    .line 842
    :cond_38
    iget v3, p1, Landroid/os/Message;->what:I

    #@3a
    const/16 v4, 0x14

    #@3c
    if-ne v3, v4, :cond_a0

    #@3e
    .line 843
    iget-object v3, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread$1;->this$1:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@40
    iget-object v4, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread$1;->this$1:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@42
    iget-object v4, v4, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->this$0:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;

    #@44
    invoke-static {v4}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->access$600(Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;)Z

    #@47
    move-result v4

    #@48
    iput-boolean v4, v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->mResult:Z

    #@4a
    .line 864
    :goto_4a
    iget v3, p1, Landroid/os/Message;->what:I

    #@4c
    packed-switch v3, :pswitch_data_124

    #@4f
    .line 895
    :pswitch_4f
    const-string v3, "NfcFactoryAdapter"

    #@51
    const-string v4, "Invalid run"

    #@53
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    .line 898
    :goto_56
    const-string v3, "NfcFactoryAdapter"

    #@58
    new-instance v4, Ljava/lang/StringBuilder;

    #@5a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5d
    const-string v5, "mThreadHandler:: "

    #@5f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v4

    #@63
    iget v5, p1, Landroid/os/Message;->what:I

    #@65
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@68
    move-result-object v4

    #@69
    const-string v5, " handled"

    #@6b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v4

    #@6f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@72
    move-result-object v4

    #@73
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@76
    .line 900
    iget-object v3, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread$1;->this$1:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@78
    iget-object v3, v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->this$0:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;

    #@7a
    iget-object v4, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread$1;->this$1:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@7c
    iget-boolean v4, v4, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->mResult:Z

    #@7e
    iget v5, p1, Landroid/os/Message;->what:I

    #@80
    invoke-static {v3, v4, v5, v1}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->access$800(Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;ZI[B)V

    #@83
    .line 901
    iget-object v3, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread$1;->this$1:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@85
    iget-object v3, v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->mainLock:Ljava/util/concurrent/locks/Lock;

    #@87
    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->lock()V

    #@8a
    .line 903
    :try_start_8a
    const-string v3, "NfcFactoryAdapter"

    #@8c
    const-string v4, "blockingCondition.signalAll()"

    #@8e
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@91
    .line 904
    iget-object v3, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread$1;->this$1:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@93
    iget-object v3, v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->mainblockingCondition:Ljava/util/concurrent/locks/Condition;

    #@95
    invoke-interface {v3}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_98
    .catchall {:try_start_8a .. :try_end_98} :catchall_11a

    #@98
    .line 907
    iget-object v3, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread$1;->this$1:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@9a
    iget-object v3, v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->mainLock:Ljava/util/concurrent/locks/Lock;

    #@9c
    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->unlock()V

    #@9f
    goto :goto_37

    #@a0
    .line 845
    :cond_a0
    iget v3, p1, Landroid/os/Message;->what:I

    #@a2
    const/16 v4, 0x15

    #@a4
    if-ne v3, v4, :cond_b3

    #@a6
    .line 846
    iget-object v3, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread$1;->this$1:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@a8
    iget-object v4, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread$1;->this$1:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@aa
    iget-object v4, v4, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->this$0:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;

    #@ac
    invoke-static {v4}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->access$700(Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;)Z

    #@af
    move-result v4

    #@b0
    iput-boolean v4, v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->mResult:Z

    #@b2
    goto :goto_4a

    #@b3
    .line 848
    :cond_b3
    iget v3, p1, Landroid/os/Message;->what:I

    #@b5
    const/16 v4, 0xd

    #@b7
    if-eq v3, v4, :cond_bf

    #@b9
    iget v3, p1, Landroid/os/Message;->what:I

    #@bb
    const/16 v4, 0xc

    #@bd
    if-ne v3, v4, :cond_f5

    #@bf
    .line 850
    :cond_bf
    if-eqz v0, :cond_ef

    #@c1
    .line 851
    const-string v3, "param2"

    #@c3
    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    #@c6
    move-result v3

    #@c7
    aput-byte v3, v1, v6

    #@c9
    .line 852
    const/4 v3, 0x1

    #@ca
    const-string v4, "param3"

    #@cc
    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    #@cf
    move-result v4

    #@d0
    aput-byte v4, v1, v3

    #@d2
    .line 853
    const/4 v3, 0x2

    #@d3
    const-string v4, "param4"

    #@d5
    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    #@d8
    move-result v4

    #@d9
    aput-byte v4, v1, v3

    #@db
    .line 854
    iget-object v3, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread$1;->this$1:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@dd
    iget-object v4, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread$1;->this$1:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@df
    iget-object v4, v4, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->this$0:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;

    #@e1
    invoke-static {v4}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->access$500(Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;)Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@e4
    move-result-object v4

    #@e5
    iget v5, p1, Landroid/os/Message;->what:I

    #@e7
    invoke-virtual {v4, v5, v1}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;->sendNfcTestCommand(I[B)Z

    #@ea
    move-result v4

    #@eb
    iput-boolean v4, v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->mResult:Z

    #@ed
    goto/16 :goto_4a

    #@ef
    .line 857
    :cond_ef
    iget-object v3, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread$1;->this$1:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@f1
    iput-boolean v6, v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->mResult:Z

    #@f3
    goto/16 :goto_4a

    #@f5
    .line 861
    :cond_f5
    iget-object v3, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread$1;->this$1:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@f7
    iget-object v4, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread$1;->this$1:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@f9
    iget-object v4, v4, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->this$0:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;

    #@fb
    invoke-static {v4}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->access$500(Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;)Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@fe
    move-result-object v4

    #@ff
    iget v5, p1, Landroid/os/Message;->what:I

    #@101
    invoke-virtual {v4, v5, v1}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;->sendNfcTestCommand(I[B)Z

    #@104
    move-result v4

    #@105
    iput-boolean v4, v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->mResult:Z

    #@107
    goto/16 :goto_4a

    #@109
    .line 873
    :pswitch_109
    const-wide/16 v3, 0x1f4

    #@10b
    invoke-static {v3, v4}, Landroid/os/SystemClock;->sleep(J)V

    #@10e
    goto/16 :goto_56

    #@110
    .line 882
    :pswitch_110
    invoke-static {v7, v8}, Landroid/os/SystemClock;->sleep(J)V

    #@113
    goto/16 :goto_56

    #@115
    .line 892
    :pswitch_115
    invoke-static {v7, v8}, Landroid/os/SystemClock;->sleep(J)V

    #@118
    goto/16 :goto_56

    #@11a
    .line 907
    :catchall_11a
    move-exception v3

    #@11b
    iget-object v4, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread$1;->this$1:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@11d
    iget-object v4, v4, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->mainLock:Ljava/util/concurrent/locks/Lock;

    #@11f
    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V

    #@122
    throw v3

    #@123
    .line 864
    nop

    #@124
    :pswitch_data_124
    .packed-switch 0x0
        :pswitch_109
        :pswitch_109
        :pswitch_110
        :pswitch_109
        :pswitch_110
        :pswitch_109
        :pswitch_110
        :pswitch_109
        :pswitch_110
        :pswitch_110
        :pswitch_115
        :pswitch_115
        :pswitch_110
        :pswitch_110
        :pswitch_109
        :pswitch_4f
        :pswitch_4f
        :pswitch_4f
        :pswitch_4f
        :pswitch_4f
        :pswitch_109
        :pswitch_109
    .end packed-switch
.end method
