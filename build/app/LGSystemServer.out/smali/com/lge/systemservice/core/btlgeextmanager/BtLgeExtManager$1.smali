.class Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager$1;
.super Landroid/os/Handler;
.source "BtLgeExtManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 43
    iput-object p1, p0, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager$1;->this$0:Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 8
    .parameter "msg"

    #@0
    .prologue
    .line 46
    invoke-static {}, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->access$000()Ljava/lang/Object;

    #@3
    move-result-object v1

    #@4
    monitor-enter v1

    #@5
    .line 47
    :try_start_5
    iget v0, p1, Landroid/os/Message;->what:I

    #@7
    packed-switch v0, :pswitch_data_98

    #@a
    .line 70
    :goto_a
    monitor-exit v1

    #@b
    .line 71
    return-void

    #@c
    .line 50
    :pswitch_c
    const-string v0, "BtLgeExtManager"

    #@e
    new-instance v2, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v3, "[BTUI] ENTER_DUT_MODE : check state = "

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    iget-object v3, p0, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager$1;->this$0:Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;

    #@1b
    invoke-static {v3}, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->access$100(Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;)Landroid/bluetooth/BluetoothAdapter;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    #@22
    move-result v3

    #@23
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v2

    #@2b
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 51
    iget-object v0, p0, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager$1;->this$0:Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;

    #@30
    invoke-static {v0}, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->access$100(Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;)Landroid/bluetooth/BluetoothAdapter;

    #@33
    move-result-object v0

    #@34
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    #@37
    move-result v0

    #@38
    if-nez v0, :cond_5d

    #@3a
    .line 52
    iget-object v0, p0, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager$1;->this$0:Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;

    #@3c
    invoke-static {v0}, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->access$200(Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;)Landroid/os/Handler;

    #@3f
    move-result-object v0

    #@40
    const/4 v2, 0x1

    #@41
    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@44
    .line 53
    iget v0, p1, Landroid/os/Message;->arg1:I

    #@46
    const/16 v2, 0xa

    #@48
    if-ne v0, v2, :cond_55

    #@4a
    .line 54
    iget-object v0, p0, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager$1;->this$0:Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;

    #@4c
    iget v2, p1, Landroid/os/Message;->arg2:I

    #@4e
    invoke-virtual {v0, v2}, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->enterBluetoothDUTMode(I)I

    #@51
    goto :goto_a

    #@52
    .line 70
    :catchall_52
    move-exception v0

    #@53
    monitor-exit v1
    :try_end_54
    .catchall {:try_start_5 .. :try_end_54} :catchall_52

    #@54
    throw v0

    #@55
    .line 56
    :cond_55
    :try_start_55
    iget-object v0, p0, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager$1;->this$0:Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;

    #@57
    iget v2, p1, Landroid/os/Message;->arg2:I

    #@59
    invoke-virtual {v0, v2}, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->enterBluetoothDUTModeBle(I)I

    #@5c
    goto :goto_a

    #@5d
    .line 60
    :cond_5d
    iget-object v0, p0, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager$1;->this$0:Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;

    #@5f
    invoke-static {v0}, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->access$310(Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;)I

    #@62
    .line 61
    iget-object v0, p0, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager$1;->this$0:Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;

    #@64
    invoke-static {v0}, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->access$300(Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;)I

    #@67
    move-result v0

    #@68
    if-nez v0, :cond_7c

    #@6a
    .line 62
    iget-object v0, p0, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager$1;->this$0:Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;

    #@6c
    invoke-static {v0}, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->access$200(Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;)Landroid/os/Handler;

    #@6f
    move-result-object v0

    #@70
    const/4 v2, 0x1

    #@71
    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@74
    .line 63
    const-string v0, "BtLgeExtManager"

    #@76
    const-string v2, "[BTUI] BT OFF fail for good"

    #@78
    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7b
    goto :goto_a

    #@7c
    .line 65
    :cond_7c
    iget-object v0, p0, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager$1;->this$0:Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;

    #@7e
    invoke-static {v0}, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->access$200(Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;)Landroid/os/Handler;

    #@81
    move-result-object v0

    #@82
    iget-object v2, p0, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager$1;->this$0:Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;

    #@84
    invoke-static {v2}, Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;->access$200(Lcom/lge/systemservice/core/btlgeextmanager/BtLgeExtManager;)Landroid/os/Handler;

    #@87
    move-result-object v2

    #@88
    const/4 v3, 0x1

    #@89
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@8b
    iget v5, p1, Landroid/os/Message;->arg2:I

    #@8d
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    #@90
    move-result-object v2

    #@91
    const-wide/16 v3, 0x12c

    #@93
    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_96
    .catchall {:try_start_55 .. :try_end_96} :catchall_52

    #@96
    goto/16 :goto_a

    #@98
    .line 47
    :pswitch_data_98
    .packed-switch 0x1
        :pswitch_c
    .end packed-switch
.end method
