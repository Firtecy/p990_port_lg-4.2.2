.class Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$1;
.super Landroid/content/BroadcastReceiver;
.source "NfcLgFactoryAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;


# direct methods
.method constructor <init>(Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 61
    iput-object p1, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$1;->this$0:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 7
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/16 v3, 0x29

    #@2
    .line 64
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 65
    .local v0, action:Ljava/lang/String;
    const-string v1, "lge.nfc.action.ADAPTER_STATE_CHANGED"

    #@8
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_3a

    #@e
    .line 66
    iget-object v1, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$1;->this$0:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;

    #@10
    const-string v2, "com.lge.nfcaddon.extra.ADAPTER_SYSTEM_STATE"

    #@12
    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@15
    move-result v2

    #@16
    invoke-static {v1, v2}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->access$000(Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;I)V

    #@19
    .line 67
    iget-object v1, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$1;->this$0:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;

    #@1b
    const-string v2, "com.lge.nfcaddon.extra.ADAPTER_CARD_STATE"

    #@1d
    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@20
    move-result v2

    #@21
    invoke-static {v1, v2}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->access$100(Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;I)V

    #@24
    .line 68
    iget-object v1, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$1;->this$0:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;

    #@26
    const-string v2, "com.lge.nfcaddon.extra.ADAPTER_DISCOVERY_STATE"

    #@28
    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@2b
    move-result v2

    #@2c
    invoke-static {v1, v2}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->access$200(Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;I)V

    #@2f
    .line 69
    iget-object v1, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$1;->this$0:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;

    #@31
    const-string v2, "com.lge.nfcaddon.extra.ADAPTER_P2P_STATE"

    #@33
    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@36
    move-result v2

    #@37
    invoke-static {v1, v2}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->access$300(Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;I)V

    #@3a
    .line 71
    :cond_3a
    return-void
.end method
