.class public final enum Lcom/lge/systemservice/core/LGContext$ServiceList;
.super Ljava/lang/Enum;
.source "LGContext.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/core/LGContext;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ServiceList"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/lge/systemservice/core/LGContext$ServiceList;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/lge/systemservice/core/LGContext$ServiceList;

.field public static final enum AAT_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

.field public static final enum BLUETOOTH_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

.field public static final enum BYEWORLD_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

.field public static final enum CORECONTROL_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

.field public static final enum EMOTIONALLED_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

.field public static final enum FELICA_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

.field public static final enum HELLOWORLD_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

.field public static final enum LGECopyLog_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

.field public static final enum LGEWIFIDISPLAY_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

.field public static final enum LGSDENC_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

.field public static final enum NFCLG_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

.field public static final enum QCTWIFIDISPLAY_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

.field public static final enum SECURECLOCK_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

.field public static final enum VOLUMEVIBRATOR_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

.field public static final enum WATCHNETSTORAGE_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

.field public static final enum WIFILGEEXT_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

.field public static final enum WIFILGEVZW_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 36
    new-instance v0, Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@7
    const-string v1, "HELLOWORLD_SERVICE"

    #@9
    invoke-direct {v0, v1, v3}, Lcom/lge/systemservice/core/LGContext$ServiceList;-><init>(Ljava/lang/String;I)V

    #@c
    sput-object v0, Lcom/lge/systemservice/core/LGContext$ServiceList;->HELLOWORLD_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@e
    .line 37
    new-instance v0, Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@10
    const-string v1, "BYEWORLD_SERVICE"

    #@12
    invoke-direct {v0, v1, v4}, Lcom/lge/systemservice/core/LGContext$ServiceList;-><init>(Ljava/lang/String;I)V

    #@15
    sput-object v0, Lcom/lge/systemservice/core/LGContext$ServiceList;->BYEWORLD_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@17
    .line 38
    new-instance v0, Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@19
    const-string v1, "WATCHNETSTORAGE_SERVICE"

    #@1b
    invoke-direct {v0, v1, v5}, Lcom/lge/systemservice/core/LGContext$ServiceList;-><init>(Ljava/lang/String;I)V

    #@1e
    sput-object v0, Lcom/lge/systemservice/core/LGContext$ServiceList;->WATCHNETSTORAGE_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@20
    .line 40
    new-instance v0, Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@22
    const-string v1, "AAT_SERVICE"

    #@24
    invoke-direct {v0, v1, v6}, Lcom/lge/systemservice/core/LGContext$ServiceList;-><init>(Ljava/lang/String;I)V

    #@27
    sput-object v0, Lcom/lge/systemservice/core/LGContext$ServiceList;->AAT_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@29
    .line 43
    new-instance v0, Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@2b
    const-string v1, "LGEWIFIDISPLAY_SERVICE"

    #@2d
    invoke-direct {v0, v1, v7}, Lcom/lge/systemservice/core/LGContext$ServiceList;-><init>(Ljava/lang/String;I)V

    #@30
    sput-object v0, Lcom/lge/systemservice/core/LGContext$ServiceList;->LGEWIFIDISPLAY_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@32
    .line 44
    new-instance v0, Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@34
    const-string v1, "QCTWIFIDISPLAY_SERVICE"

    #@36
    const/4 v2, 0x5

    #@37
    invoke-direct {v0, v1, v2}, Lcom/lge/systemservice/core/LGContext$ServiceList;-><init>(Ljava/lang/String;I)V

    #@3a
    sput-object v0, Lcom/lge/systemservice/core/LGContext$ServiceList;->QCTWIFIDISPLAY_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@3c
    .line 46
    new-instance v0, Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@3e
    const-string v1, "SECURECLOCK_SERVICE"

    #@40
    const/4 v2, 0x6

    #@41
    invoke-direct {v0, v1, v2}, Lcom/lge/systemservice/core/LGContext$ServiceList;-><init>(Ljava/lang/String;I)V

    #@44
    sput-object v0, Lcom/lge/systemservice/core/LGContext$ServiceList;->SECURECLOCK_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@46
    .line 47
    new-instance v0, Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@48
    const-string v1, "EMOTIONALLED_SERVICE"

    #@4a
    const/4 v2, 0x7

    #@4b
    invoke-direct {v0, v1, v2}, Lcom/lge/systemservice/core/LGContext$ServiceList;-><init>(Ljava/lang/String;I)V

    #@4e
    sput-object v0, Lcom/lge/systemservice/core/LGContext$ServiceList;->EMOTIONALLED_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@50
    .line 48
    new-instance v0, Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@52
    const-string v1, "LGSDENC_SERVICE"

    #@54
    const/16 v2, 0x8

    #@56
    invoke-direct {v0, v1, v2}, Lcom/lge/systemservice/core/LGContext$ServiceList;-><init>(Ljava/lang/String;I)V

    #@59
    sput-object v0, Lcom/lge/systemservice/core/LGContext$ServiceList;->LGSDENC_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@5b
    .line 49
    new-instance v0, Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@5d
    const-string v1, "VOLUMEVIBRATOR_SERVICE"

    #@5f
    const/16 v2, 0x9

    #@61
    invoke-direct {v0, v1, v2}, Lcom/lge/systemservice/core/LGContext$ServiceList;-><init>(Ljava/lang/String;I)V

    #@64
    sput-object v0, Lcom/lge/systemservice/core/LGContext$ServiceList;->VOLUMEVIBRATOR_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@66
    .line 50
    new-instance v0, Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@68
    const-string v1, "WIFILGEEXT_SERVICE"

    #@6a
    const/16 v2, 0xa

    #@6c
    invoke-direct {v0, v1, v2}, Lcom/lge/systemservice/core/LGContext$ServiceList;-><init>(Ljava/lang/String;I)V

    #@6f
    sput-object v0, Lcom/lge/systemservice/core/LGContext$ServiceList;->WIFILGEEXT_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@71
    .line 52
    new-instance v0, Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@73
    const-string v1, "BLUETOOTH_SERVICE"

    #@75
    const/16 v2, 0xb

    #@77
    invoke-direct {v0, v1, v2}, Lcom/lge/systemservice/core/LGContext$ServiceList;-><init>(Ljava/lang/String;I)V

    #@7a
    sput-object v0, Lcom/lge/systemservice/core/LGContext$ServiceList;->BLUETOOTH_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@7c
    .line 54
    new-instance v0, Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@7e
    const-string v1, "FELICA_SERVICE"

    #@80
    const/16 v2, 0xc

    #@82
    invoke-direct {v0, v1, v2}, Lcom/lge/systemservice/core/LGContext$ServiceList;-><init>(Ljava/lang/String;I)V

    #@85
    sput-object v0, Lcom/lge/systemservice/core/LGContext$ServiceList;->FELICA_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@87
    .line 55
    new-instance v0, Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@89
    const-string v1, "NFCLG_SERVICE"

    #@8b
    const/16 v2, 0xd

    #@8d
    invoke-direct {v0, v1, v2}, Lcom/lge/systemservice/core/LGContext$ServiceList;-><init>(Ljava/lang/String;I)V

    #@90
    sput-object v0, Lcom/lge/systemservice/core/LGContext$ServiceList;->NFCLG_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@92
    .line 57
    new-instance v0, Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@94
    const-string v1, "CORECONTROL_SERVICE"

    #@96
    const/16 v2, 0xe

    #@98
    invoke-direct {v0, v1, v2}, Lcom/lge/systemservice/core/LGContext$ServiceList;-><init>(Ljava/lang/String;I)V

    #@9b
    sput-object v0, Lcom/lge/systemservice/core/LGContext$ServiceList;->CORECONTROL_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@9d
    .line 61
    new-instance v0, Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@9f
    const-string v1, "WIFILGEVZW_SERVICE"

    #@a1
    const/16 v2, 0xf

    #@a3
    invoke-direct {v0, v1, v2}, Lcom/lge/systemservice/core/LGContext$ServiceList;-><init>(Ljava/lang/String;I)V

    #@a6
    sput-object v0, Lcom/lge/systemservice/core/LGContext$ServiceList;->WIFILGEVZW_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@a8
    .line 65
    new-instance v0, Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@aa
    const-string v1, "LGECopyLog_SERVICE"

    #@ac
    const/16 v2, 0x10

    #@ae
    invoke-direct {v0, v1, v2}, Lcom/lge/systemservice/core/LGContext$ServiceList;-><init>(Ljava/lang/String;I)V

    #@b1
    sput-object v0, Lcom/lge/systemservice/core/LGContext$ServiceList;->LGECopyLog_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@b3
    .line 34
    const/16 v0, 0x11

    #@b5
    new-array v0, v0, [Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@b7
    sget-object v1, Lcom/lge/systemservice/core/LGContext$ServiceList;->HELLOWORLD_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@b9
    aput-object v1, v0, v3

    #@bb
    sget-object v1, Lcom/lge/systemservice/core/LGContext$ServiceList;->BYEWORLD_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@bd
    aput-object v1, v0, v4

    #@bf
    sget-object v1, Lcom/lge/systemservice/core/LGContext$ServiceList;->WATCHNETSTORAGE_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@c1
    aput-object v1, v0, v5

    #@c3
    sget-object v1, Lcom/lge/systemservice/core/LGContext$ServiceList;->AAT_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@c5
    aput-object v1, v0, v6

    #@c7
    sget-object v1, Lcom/lge/systemservice/core/LGContext$ServiceList;->LGEWIFIDISPLAY_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@c9
    aput-object v1, v0, v7

    #@cb
    const/4 v1, 0x5

    #@cc
    sget-object v2, Lcom/lge/systemservice/core/LGContext$ServiceList;->QCTWIFIDISPLAY_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@ce
    aput-object v2, v0, v1

    #@d0
    const/4 v1, 0x6

    #@d1
    sget-object v2, Lcom/lge/systemservice/core/LGContext$ServiceList;->SECURECLOCK_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@d3
    aput-object v2, v0, v1

    #@d5
    const/4 v1, 0x7

    #@d6
    sget-object v2, Lcom/lge/systemservice/core/LGContext$ServiceList;->EMOTIONALLED_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@d8
    aput-object v2, v0, v1

    #@da
    const/16 v1, 0x8

    #@dc
    sget-object v2, Lcom/lge/systemservice/core/LGContext$ServiceList;->LGSDENC_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@de
    aput-object v2, v0, v1

    #@e0
    const/16 v1, 0x9

    #@e2
    sget-object v2, Lcom/lge/systemservice/core/LGContext$ServiceList;->VOLUMEVIBRATOR_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@e4
    aput-object v2, v0, v1

    #@e6
    const/16 v1, 0xa

    #@e8
    sget-object v2, Lcom/lge/systemservice/core/LGContext$ServiceList;->WIFILGEEXT_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@ea
    aput-object v2, v0, v1

    #@ec
    const/16 v1, 0xb

    #@ee
    sget-object v2, Lcom/lge/systemservice/core/LGContext$ServiceList;->BLUETOOTH_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@f0
    aput-object v2, v0, v1

    #@f2
    const/16 v1, 0xc

    #@f4
    sget-object v2, Lcom/lge/systemservice/core/LGContext$ServiceList;->FELICA_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@f6
    aput-object v2, v0, v1

    #@f8
    const/16 v1, 0xd

    #@fa
    sget-object v2, Lcom/lge/systemservice/core/LGContext$ServiceList;->NFCLG_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@fc
    aput-object v2, v0, v1

    #@fe
    const/16 v1, 0xe

    #@100
    sget-object v2, Lcom/lge/systemservice/core/LGContext$ServiceList;->CORECONTROL_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@102
    aput-object v2, v0, v1

    #@104
    const/16 v1, 0xf

    #@106
    sget-object v2, Lcom/lge/systemservice/core/LGContext$ServiceList;->WIFILGEVZW_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@108
    aput-object v2, v0, v1

    #@10a
    const/16 v1, 0x10

    #@10c
    sget-object v2, Lcom/lge/systemservice/core/LGContext$ServiceList;->LGECopyLog_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@10e
    aput-object v2, v0, v1

    #@110
    sput-object v0, Lcom/lge/systemservice/core/LGContext$ServiceList;->$VALUES:[Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@112
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/lge/systemservice/core/LGContext$ServiceList;
    .registers 2
    .parameter

    #@0
    .prologue
    .line 34
    const-class v0, Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/lge/systemservice/core/LGContext$ServiceList;
    .registers 1

    #@0
    .prologue
    .line 34
    sget-object v0, Lcom/lge/systemservice/core/LGContext$ServiceList;->$VALUES:[Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@2
    invoke-virtual {v0}, [Lcom/lge/systemservice/core/LGContext$ServiceList;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@8
    return-object v0
.end method
