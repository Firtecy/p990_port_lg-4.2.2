.class public Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;
.super Ljava/lang/Object;
.source "NfcLgManager.java"


# static fields
.field private static deathBinderNotificator:Landroid/os/IBinder$DeathRecipient;

.field private static mService:Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 18
    const/4 v0, 0x0

    #@1
    sput-object v0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;->mService:Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager;

    #@3
    .line 119
    new-instance v0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager$1;

    #@5
    invoke-direct {v0}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager$1;-><init>()V

    #@8
    sput-object v0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;->deathBinderNotificator:Landroid/os/IBinder$DeathRecipient;

    #@a
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 25
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 26
    return-void
.end method

.method static synthetic access$002(Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager;)Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager;
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 16
    sput-object p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;->mService:Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager;

    #@2
    return-object p0
.end method

.method private static getService()Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager;
    .registers 5

    #@0
    .prologue
    .line 32
    sget-object v2, Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;->mService:Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager;

    #@2
    if-eqz v2, :cond_7

    #@4
    .line 33
    sget-object v2, Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;->mService:Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager;

    #@6
    .line 57
    .local v0, b:Landroid/os/IBinder;
    :goto_6
    return-object v2

    #@7
    .line 36
    .end local v0           #b:Landroid/os/IBinder;
    :cond_7
    const-string v2, "nfcLgService"

    #@9
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@c
    move-result-object v0

    #@d
    .line 37
    .restart local v0       #b:Landroid/os/IBinder;
    invoke-static {v0}, Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager$Stub;->asInterface(Landroid/os/IBinder;)Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager;

    #@10
    move-result-object v2

    #@11
    sput-object v2, Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;->mService:Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager;

    #@13
    .line 39
    sget-object v2, Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;->mService:Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager;

    #@15
    if-nez v2, :cond_20

    #@17
    .line 40
    const-string v2, "NfcLgManager"

    #@19
    const-string v3, "Failed to get NfcLgService"

    #@1b
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 41
    const/4 v2, 0x0

    #@1f
    goto :goto_6

    #@20
    .line 51
    :cond_20
    :try_start_20
    sget-object v2, Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;->mService:Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager;

    #@22
    invoke-interface {v2}, Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager;->asBinder()Landroid/os/IBinder;

    #@25
    move-result-object v2

    #@26
    sget-object v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;->deathBinderNotificator:Landroid/os/IBinder$DeathRecipient;

    #@28
    const/4 v4, 0x0

    #@29
    invoke-interface {v2, v3, v4}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_2c
    .catch Landroid/os/RemoteException; {:try_start_20 .. :try_end_2c} :catch_2f

    #@2c
    .line 57
    :goto_2c
    sget-object v2, Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;->mService:Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager;

    #@2e
    goto :goto_6

    #@2f
    .line 53
    :catch_2f
    move-exception v1

    #@30
    .line 55
    .local v1, e:Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    #@33
    goto :goto_2c
.end method


# virtual methods
.method public createNfcFactoryObj()Z
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 101
    invoke-static {}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;->getService()Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager;

    #@4
    move-result-object v1

    #@5
    .line 103
    .local v1, nm:Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager;
    if-nez v1, :cond_8

    #@7
    .line 112
    :goto_7
    return v2

    #@8
    .line 108
    :cond_8
    :try_start_8
    invoke-interface {v1}, Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager;->createNfcFactoryObj()Z
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_b} :catch_d

    #@b
    move-result v2

    #@c
    goto :goto_7

    #@d
    .line 110
    :catch_d
    move-exception v0

    #@e
    .line 111
    .local v0, e:Landroid/os/RemoteException;
    const-string v3, "NfcLgManager"

    #@10
    const-string v4, "sendNfcTestCommand::RemoteException"

    #@12
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    goto :goto_7
.end method

.method public handleNfcTest(I[B)Ljava/lang/String;
    .registers 10
    .parameter "command"
    .parameter "retData"

    #@0
    .prologue
    .line 81
    const-string v2, "Fail"

    #@2
    .line 82
    .local v2, result:Ljava/lang/String;
    invoke-static {}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;->getService()Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager;

    #@5
    move-result-object v1

    #@6
    .line 84
    .local v1, nm:Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager;
    if-nez v1, :cond_a

    #@8
    move-object v3, v2

    #@9
    .line 96
    .end local v2           #result:Ljava/lang/String;
    .local v3, result:Ljava/lang/String;
    :goto_9
    return-object v3

    #@a
    .line 89
    .end local v3           #result:Ljava/lang/String;
    .restart local v2       #result:Ljava/lang/String;
    :cond_a
    :try_start_a
    const-string v4, "NfcLgManager"

    #@c
    new-instance v5, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v6, "NfcLgManager handleNfcTest::"

    #@13
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v5

    #@17
    const/4 v6, 0x0

    #@18
    aget-byte v6, p2, v6

    #@1a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v5

    #@1e
    const/4 v6, 0x1

    #@1f
    aget-byte v6, p2, v6

    #@21
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v5

    #@25
    const/4 v6, 0x2

    #@26
    aget-byte v6, p2, v6

    #@28
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v5

    #@2c
    const/4 v6, 0x3

    #@2d
    aget-byte v6, p2, v6

    #@2f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v5

    #@33
    const/4 v6, 0x4

    #@34
    aget-byte v6, p2, v6

    #@36
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@39
    move-result-object v5

    #@3a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v5

    #@3e
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    .line 90
    invoke-interface {v1, p1, p2}, Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager;->handleNfcFactory(I[B)Ljava/lang/String;
    :try_end_44
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_44} :catch_47

    #@44
    move-result-object v2

    #@45
    move-object v3, v2

    #@46
    .line 91
    .end local v2           #result:Ljava/lang/String;
    .restart local v3       #result:Ljava/lang/String;
    goto :goto_9

    #@47
    .line 94
    .end local v3           #result:Ljava/lang/String;
    .restart local v2       #result:Ljava/lang/String;
    :catch_47
    move-exception v0

    #@48
    .line 95
    .local v0, e:Landroid/os/RemoteException;
    const-string v4, "NfcLgManager"

    #@4a
    const-string v5, "handleNfcTest::RemoteException"

    #@4c
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    move-object v3, v2

    #@50
    .line 96
    .end local v2           #result:Ljava/lang/String;
    .restart local v3       #result:Ljava/lang/String;
    goto :goto_9
.end method

.method public sendNfcTestCommand(I[B)Z
    .registers 6
    .parameter "command"
    .parameter "response"

    #@0
    .prologue
    .line 64
    const/4 v2, 0x0

    #@1
    .line 65
    .local v2, result:Z
    invoke-static {}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;->getService()Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager;

    #@4
    move-result-object v1

    #@5
    .line 67
    .local v1, nm:Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager;
    if-nez v1, :cond_8

    #@7
    .line 76
    .end local v2           #result:Z
    :goto_7
    return v2

    #@8
    .line 72
    .restart local v2       #result:Z
    :cond_8
    :try_start_8
    invoke-interface {v1, p1, p2}, Lcom/lge/systemservice/core/nfclgmanager/INfcLgManager;->sendNfcTestCommand(I[B)Z
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_b} :catch_d

    #@b
    move-result v2

    #@c
    goto :goto_7

    #@d
    .line 74
    :catch_d
    move-exception v0

    #@e
    .line 75
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@11
    goto :goto_7
.end method
