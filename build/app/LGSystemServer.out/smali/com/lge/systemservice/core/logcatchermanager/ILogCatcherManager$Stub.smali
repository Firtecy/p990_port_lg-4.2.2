.class public abstract Lcom/lge/systemservice/core/logcatchermanager/ILogCatcherManager$Stub;
.super Landroid/os/Binder;
.source "ILogCatcherManager.java"

# interfaces
.implements Lcom/lge/systemservice/core/logcatchermanager/ILogCatcherManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/core/logcatchermanager/ILogCatcherManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/systemservice/core/logcatchermanager/ILogCatcherManager$Stub$Proxy;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 15
    const-string v0, "com.lge.systemservice.core.logcatchermanager.ILogCatcherManager"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/lge/systemservice/core/logcatchermanager/ILogCatcherManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/lge/systemservice/core/logcatchermanager/ILogCatcherManager;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 23
    if-nez p0, :cond_4

    #@2
    .line 24
    const/4 v0, 0x0

    #@3
    .line 30
    :goto_3
    return-object v0

    #@4
    .line 26
    :cond_4
    const-string v1, "com.lge.systemservice.core.logcatchermanager.ILogCatcherManager"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 27
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/lge/systemservice/core/logcatchermanager/ILogCatcherManager;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 28
    check-cast v0, Lcom/lge/systemservice/core/logcatchermanager/ILogCatcherManager;

    #@12
    goto :goto_3

    #@13
    .line 30
    :cond_13
    new-instance v0, Lcom/lge/systemservice/core/logcatchermanager/ILogCatcherManager$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/lge/systemservice/core/logcatchermanager/ILogCatcherManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 8
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 38
    sparse-switch p1, :sswitch_data_70

    #@4
    .line 94
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v1

    #@8
    :goto_8
    return v1

    #@9
    .line 42
    :sswitch_9
    const-string v2, "com.lge.systemservice.core.logcatchermanager.ILogCatcherManager"

    #@b
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 47
    :sswitch_f
    const-string v2, "com.lge.systemservice.core.logcatchermanager.ILogCatcherManager"

    #@11
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 48
    invoke-virtual {p0}, Lcom/lge/systemservice/core/logcatchermanager/ILogCatcherManager$Stub;->copyLogsToInternalStorage()I

    #@17
    move-result v0

    #@18
    .line 49
    .local v0, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1b
    .line 50
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1e
    goto :goto_8

    #@1f
    .line 55
    .end local v0           #_result:I
    :sswitch_1f
    const-string v2, "com.lge.systemservice.core.logcatchermanager.ILogCatcherManager"

    #@21
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@24
    .line 56
    invoke-virtual {p0}, Lcom/lge/systemservice/core/logcatchermanager/ILogCatcherManager$Stub;->copyLogsToExternalStorage()I

    #@27
    move-result v0

    #@28
    .line 57
    .restart local v0       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2b
    .line 58
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@2e
    goto :goto_8

    #@2f
    .line 63
    .end local v0           #_result:I
    :sswitch_2f
    const-string v2, "com.lge.systemservice.core.logcatchermanager.ILogCatcherManager"

    #@31
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@34
    .line 64
    invoke-virtual {p0}, Lcom/lge/systemservice/core/logcatchermanager/ILogCatcherManager$Stub;->startQuickDump()I

    #@37
    move-result v0

    #@38
    .line 65
    .restart local v0       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3b
    .line 66
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@3e
    goto :goto_8

    #@3f
    .line 71
    .end local v0           #_result:I
    :sswitch_3f
    const-string v2, "com.lge.systemservice.core.logcatchermanager.ILogCatcherManager"

    #@41
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@44
    .line 72
    invoke-virtual {p0}, Lcom/lge/systemservice/core/logcatchermanager/ILogCatcherManager$Stub;->cancelQuickDump()I

    #@47
    move-result v0

    #@48
    .line 73
    .restart local v0       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4b
    .line 74
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@4e
    goto :goto_8

    #@4f
    .line 79
    .end local v0           #_result:I
    :sswitch_4f
    const-string v2, "com.lge.systemservice.core.logcatchermanager.ILogCatcherManager"

    #@51
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@54
    .line 80
    invoke-virtual {p0}, Lcom/lge/systemservice/core/logcatchermanager/ILogCatcherManager$Stub;->cancelCopyLogsToInternalStorage()I

    #@57
    move-result v0

    #@58
    .line 81
    .restart local v0       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5b
    .line 82
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5e
    goto :goto_8

    #@5f
    .line 87
    .end local v0           #_result:I
    :sswitch_5f
    const-string v2, "com.lge.systemservice.core.logcatchermanager.ILogCatcherManager"

    #@61
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@64
    .line 88
    invoke-virtual {p0}, Lcom/lge/systemservice/core/logcatchermanager/ILogCatcherManager$Stub;->cancelCopyLogsToExternalStorage()I

    #@67
    move-result v0

    #@68
    .line 89
    .restart local v0       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@6b
    .line 90
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@6e
    goto :goto_8

    #@6f
    .line 38
    nop

    #@70
    :sswitch_data_70
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_1f
        0x3 -> :sswitch_2f
        0x4 -> :sswitch_3f
        0x5 -> :sswitch_4f
        0x6 -> :sswitch_5f
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
