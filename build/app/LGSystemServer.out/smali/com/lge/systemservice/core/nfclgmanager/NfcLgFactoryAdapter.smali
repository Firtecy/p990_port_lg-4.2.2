.class public Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;
.super Ljava/lang/Object;
.source "NfcLgFactoryAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;
    }
.end annotation


# static fields
.field private static bSenseMTFirst:Z

.field private static bSenseMTStarted:Z

.field private static bTestStarted:Z

.field private static detectcount:I

.field private static retArr:[B

.field private static retChar:B

.field private static retString:Ljava/lang/String;


# instance fields
.field private final MAX_BLOCK_TIMEOUT_FOR_DISABLE_NATIVE:I

.field private final MAX_BLOCK_TIMEOUT_FOR_ENABLE_NATIVE:I

.field private final STABILIZATION_TIME_AFTER_NATIVE_OPER:I

.field private mContext:Landroid/content/Context;

.field private final mIntentFilterAddon:Landroid/content/IntentFilter;

.field private mIsNfcNativeServiceOn:Z

.field private mNfcAdapterAddon:Lcom/lge/nfcaddon/NfcAdapterAddon;

.field private mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

.field private mProgressThread:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mServiceContext:Lcom/lge/systemservice/core/LGContext;

.field mainHandler:Landroid/os/Handler;

.field private final onOffblockingCondition:Ljava/util/concurrent/locks/Condition;

.field private final onOfflock:Ljava/util/concurrent/locks/Lock;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 35
    sput v1, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->detectcount:I

    #@3
    .line 36
    sput-boolean v1, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->bTestStarted:Z

    #@5
    .line 37
    const-string v0, "\u0002FAIL\u0003"

    #@7
    sput-object v0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retString:Ljava/lang/String;

    #@9
    .line 39
    sput-byte v1, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retChar:B

    #@b
    .line 621
    sput-boolean v1, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->bSenseMTStarted:Z

    #@d
    .line 622
    const/4 v0, 0x1

    #@e
    sput-boolean v0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->bSenseMTFirst:Z

    #@10
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 31
    iput-object v2, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mContext:Landroid/content/Context;

    #@6
    .line 33
    iput-object v2, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mNfcAdapterAddon:Lcom/lge/nfcaddon/NfcAdapterAddon;

    #@8
    .line 41
    iput-object v2, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mainHandler:Landroid/os/Handler;

    #@a
    .line 44
    iput-object v2, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@c
    .line 45
    iput-object v2, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mServiceContext:Lcom/lge/systemservice/core/LGContext;

    #@e
    .line 47
    const/4 v0, 0x0

    #@f
    iput-boolean v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mIsNfcNativeServiceOn:Z

    #@11
    .line 51
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    #@13
    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    #@16
    iput-object v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->onOfflock:Ljava/util/concurrent/locks/Lock;

    #@18
    .line 52
    iget-object v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->onOfflock:Ljava/util/concurrent/locks/Lock;

    #@1a
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    #@1d
    move-result-object v0

    #@1e
    iput-object v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->onOffblockingCondition:Ljava/util/concurrent/locks/Condition;

    #@20
    .line 53
    const/16 v0, 0x2710

    #@22
    iput v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->MAX_BLOCK_TIMEOUT_FOR_ENABLE_NATIVE:I

    #@24
    .line 54
    const/16 v0, 0x1388

    #@26
    iput v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->MAX_BLOCK_TIMEOUT_FOR_DISABLE_NATIVE:I

    #@28
    .line 55
    const/16 v0, 0x1f4

    #@2a
    iput v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->STABILIZATION_TIME_AFTER_NATIVE_OPER:I

    #@2c
    .line 61
    new-instance v0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$1;

    #@2e
    invoke-direct {v0, p0}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$1;-><init>(Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;)V

    #@31
    iput-object v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mReceiver:Landroid/content/BroadcastReceiver;

    #@33
    .line 74
    iput-object p1, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mContext:Landroid/content/Context;

    #@35
    .line 76
    invoke-static {}, Lcom/lge/nfcaddon/NfcAdapterAddon;->getNfcAdapterAddon()Lcom/lge/nfcaddon/NfcAdapterAddon;

    #@38
    move-result-object v0

    #@39
    iput-object v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mNfcAdapterAddon:Lcom/lge/nfcaddon/NfcAdapterAddon;

    #@3b
    .line 82
    iget-object v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mNfcAdapterAddon:Lcom/lge/nfcaddon/NfcAdapterAddon;

    #@3d
    if-nez v0, :cond_46

    #@3f
    .line 83
    const-string v0, "NfcFactoryAdapter"

    #@41
    const-string v1, "@@mNfcAdapterAddon null"

    #@43
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    .line 86
    :cond_46
    iget-object v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mProgressThread:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@48
    if-nez v0, :cond_5d

    #@4a
    .line 87
    const-string v0, "NfcFactoryAdapter"

    #@4c
    const-string v1, "@@mProgressThread null"

    #@4e
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@51
    .line 89
    new-instance v0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@53
    invoke-direct {v0, p0, v2}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;-><init>(Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$1;)V

    #@56
    iput-object v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mProgressThread:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@58
    .line 90
    iget-object v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mProgressThread:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@5a
    invoke-virtual {v0}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->start()V

    #@5d
    .line 93
    :cond_5d
    const-string v0, "NfcFactoryAdapter"

    #@5f
    const-string v1, "@@LGContextImpl"

    #@61
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@64
    .line 94
    iget-object v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mServiceContext:Lcom/lge/systemservice/core/LGContext;

    #@66
    if-nez v0, :cond_71

    #@68
    .line 95
    new-instance v0, Lcom/lge/systemservice/core/LGContextImpl;

    #@6a
    iget-object v1, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mContext:Landroid/content/Context;

    #@6c
    invoke-direct {v0, v1}, Lcom/lge/systemservice/core/LGContextImpl;-><init>(Landroid/content/Context;)V

    #@6f
    iput-object v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mServiceContext:Lcom/lge/systemservice/core/LGContext;

    #@71
    .line 98
    :cond_71
    iget-object v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mServiceContext:Lcom/lge/systemservice/core/LGContext;

    #@73
    if-eqz v0, :cond_88

    #@75
    .line 99
    iget-object v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mServiceContext:Lcom/lge/systemservice/core/LGContext;

    #@77
    sget-object v1, Lcom/lge/systemservice/core/LGContext$ServiceList;->NFCLG_SERVICE:Lcom/lge/systemservice/core/LGContext$ServiceList;

    #@79
    invoke-virtual {v0, v1}, Lcom/lge/systemservice/core/LGContext;->getLegacyService(Lcom/lge/systemservice/core/LGContext$ServiceList;)Ljava/lang/Object;

    #@7c
    move-result-object v0

    #@7d
    check-cast v0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@7f
    iput-object v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@81
    .line 101
    const-string v0, "NfcFactoryAdapter"

    #@83
    const-string v1, "Create NFCDevice mServiceContext"

    #@85
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@88
    .line 105
    :cond_88
    iget-object v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mNfcAdapterAddon:Lcom/lge/nfcaddon/NfcAdapterAddon;

    #@8a
    if-eqz v0, :cond_bb

    #@8c
    .line 106
    new-instance v0, Landroid/content/IntentFilter;

    #@8e
    const-string v1, "lge.nfc.action.ADAPTER_STATE_CHANGED"

    #@90
    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@93
    iput-object v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mIntentFilterAddon:Landroid/content/IntentFilter;

    #@95
    .line 107
    const-string v0, "NfcLgFactoryAdapter"

    #@97
    const-string v1, "Intent Filter Created!"

    #@99
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@9c
    .line 114
    :goto_9c
    iget-object v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mContext:Landroid/content/Context;

    #@9e
    if-eqz v0, :cond_b4

    #@a0
    iget-object v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mIntentFilterAddon:Landroid/content/IntentFilter;

    #@a2
    if-eqz v0, :cond_b4

    #@a4
    .line 115
    iget-object v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mContext:Landroid/content/Context;

    #@a6
    iget-object v1, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mReceiver:Landroid/content/BroadcastReceiver;

    #@a8
    iget-object v2, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mIntentFilterAddon:Landroid/content/IntentFilter;

    #@aa
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@ad
    .line 116
    const-string v0, "NfcLgFactoryAdapter"

    #@af
    const-string v1, "Broadcast Receiver Registered!"

    #@b1
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b4
    .line 119
    :cond_b4
    const/16 v0, 0x80

    #@b6
    new-array v0, v0, [B

    #@b8
    sput-object v0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retArr:[B

    #@ba
    .line 121
    return-void

    #@bb
    .line 110
    :cond_bb
    iput-object v2, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mIntentFilterAddon:Landroid/content/IntentFilter;

    #@bd
    goto :goto_9c
.end method

.method static synthetic access$000(Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->handleNfcSystemStateChanged(I)V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->handleNfcCardStateChanged(I)V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->handleNfcRwStateChanged(I)V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->handleNfcP2pStateChanged(I)V

    #@3
    return-void
.end method

.method static synthetic access$500(Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;)Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 28
    iget-object v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mNfcLgManagerLegacy:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 28
    invoke-direct {p0}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->nativeNfcEnableBlocking()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$700(Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 28
    invoke-direct {p0}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->nativeNfcDisableBlocking()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$800(Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;ZI[B)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 28
    invoke-direct {p0, p1, p2, p3}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->set_retval(ZI[B)V

    #@3
    return-void
.end method

.method private handleNfcCardStateChanged(I)V
    .registers 4
    .parameter "newState"

    #@0
    .prologue
    .line 169
    packed-switch p1, :pswitch_data_2c

    #@3
    .line 184
    const-string v0, "NfcLgFactoryAdapter"

    #@5
    const-string v1, "NFC Card Mode State is Invalid"

    #@7
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 187
    :goto_a
    return-void

    #@b
    .line 172
    :pswitch_b
    const-string v0, "NfcLgFactoryAdapter"

    #@d
    const-string v1, "NFC Card is Turned Off!"

    #@f
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    goto :goto_a

    #@13
    .line 175
    :pswitch_13
    const-string v0, "NfcLgFactoryAdapter"

    #@15
    const-string v1, "NFC Card is Turned On!"

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    goto :goto_a

    #@1b
    .line 178
    :pswitch_1b
    const-string v0, "NfcLgFactoryAdapter"

    #@1d
    const-string v1, "NFC Card Mode is Turning On..."

    #@1f
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    goto :goto_a

    #@23
    .line 181
    :pswitch_23
    const-string v0, "NfcLgFactoryAdapter"

    #@25
    const-string v1, "NFC Card Mode is Turning Off..."

    #@27
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    goto :goto_a

    #@2b
    .line 169
    nop

    #@2c
    :pswitch_data_2c
    .packed-switch 0xb
        :pswitch_b
        :pswitch_1b
        :pswitch_13
        :pswitch_23
    .end packed-switch
.end method

.method private handleNfcP2pStateChanged(I)V
    .registers 4
    .parameter "newState"

    #@0
    .prologue
    .line 212
    packed-switch p1, :pswitch_data_2c

    #@3
    .line 227
    const-string v0, "NfcLgFactoryAdapter"

    #@5
    const-string v1, "NFC P2P State is Invalid"

    #@7
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 230
    :goto_a
    return-void

    #@b
    .line 215
    :pswitch_b
    const-string v0, "NfcLgFactoryAdapter"

    #@d
    const-string v1, "NFC P2P is Turned Off!"

    #@f
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    goto :goto_a

    #@13
    .line 218
    :pswitch_13
    const-string v0, "NfcLgFactoryAdapter"

    #@15
    const-string v1, "NFC P2P is Turned On!"

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    goto :goto_a

    #@1b
    .line 221
    :pswitch_1b
    const-string v0, "NfcLgFactoryAdapter"

    #@1d
    const-string v1, "NFC P2P is Turning On..."

    #@1f
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    goto :goto_a

    #@23
    .line 224
    :pswitch_23
    const-string v0, "NfcLgFactoryAdapter"

    #@25
    const-string v1, "NFC P2P is Turning Off..."

    #@27
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    goto :goto_a

    #@2b
    .line 212
    nop

    #@2c
    :pswitch_data_2c
    .packed-switch 0x1f
        :pswitch_b
        :pswitch_1b
        :pswitch_13
        :pswitch_23
    .end packed-switch
.end method

.method private handleNfcRwStateChanged(I)V
    .registers 4
    .parameter "newState"

    #@0
    .prologue
    .line 191
    packed-switch p1, :pswitch_data_2c

    #@3
    .line 206
    const-string v0, "NfcLgFactoryAdapter"

    #@5
    const-string v1, "NFC RW Mode State is Invalid"

    #@7
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 209
    :goto_a
    return-void

    #@b
    .line 194
    :pswitch_b
    const-string v0, "NfcLgFactoryAdapter"

    #@d
    const-string v1, "NFC RW is Turned Off!"

    #@f
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    goto :goto_a

    #@13
    .line 197
    :pswitch_13
    const-string v0, "NfcLgFactoryAdapter"

    #@15
    const-string v1, "NFC RW is Turned On!"

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    goto :goto_a

    #@1b
    .line 200
    :pswitch_1b
    const-string v0, "NfcLgFactoryAdapter"

    #@1d
    const-string v1, "NFC RW is Turning On..."

    #@1f
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    goto :goto_a

    #@23
    .line 203
    :pswitch_23
    const-string v0, "NfcLgFactoryAdapter"

    #@25
    const-string v1, "NFC RW is Turning Off..."

    #@27
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    goto :goto_a

    #@2b
    .line 191
    nop

    #@2c
    :pswitch_data_2c
    .packed-switch 0x15
        :pswitch_b
        :pswitch_1b
        :pswitch_13
        :pswitch_23
    .end packed-switch
.end method

.method private handleNfcSystemStateChanged(I)V
    .registers 7
    .parameter "newState"

    #@0
    .prologue
    const-wide/16 v3, 0x1f4

    #@2
    const/4 v2, 0x1

    #@3
    .line 124
    packed-switch p1, :pswitch_data_68

    #@6
    .line 163
    const-string v0, "NfcLgFactoryAdapter"

    #@8
    const-string v1, "NFC System State is Invalid"

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 166
    :cond_d
    :goto_d
    return-void

    #@e
    .line 127
    :pswitch_e
    const-string v0, "NfcLgFactoryAdapter"

    #@10
    const-string v1, "NFC Native Service is Turned Off!"

    #@12
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    .line 130
    iget-boolean v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mIsNfcNativeServiceOn:Z

    #@17
    if-ne v0, v2, :cond_d

    #@19
    .line 131
    invoke-static {v3, v4}, Landroid/os/SystemClock;->sleep(J)V

    #@1c
    .line 132
    iget-object v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->onOfflock:Ljava/util/concurrent/locks/Lock;

    #@1e
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    #@21
    .line 134
    :try_start_21
    iget-object v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->onOffblockingCondition:Ljava/util/concurrent/locks/Condition;

    #@23
    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_26
    .catchall {:try_start_21 .. :try_end_26} :catchall_2c

    #@26
    .line 137
    iget-object v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->onOfflock:Ljava/util/concurrent/locks/Lock;

    #@28
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    #@2b
    goto :goto_d

    #@2c
    :catchall_2c
    move-exception v0

    #@2d
    iget-object v1, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->onOfflock:Ljava/util/concurrent/locks/Lock;

    #@2f
    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    #@32
    throw v0

    #@33
    .line 142
    :pswitch_33
    const-string v0, "NfcLgFactoryAdapter"

    #@35
    const-string v1, "NFC Native Service is Turned On!"

    #@37
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 145
    iget-boolean v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mIsNfcNativeServiceOn:Z

    #@3c
    if-ne v0, v2, :cond_d

    #@3e
    .line 146
    invoke-static {v3, v4}, Landroid/os/SystemClock;->sleep(J)V

    #@41
    .line 147
    iget-object v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->onOfflock:Ljava/util/concurrent/locks/Lock;

    #@43
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    #@46
    .line 149
    :try_start_46
    iget-object v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->onOffblockingCondition:Ljava/util/concurrent/locks/Condition;

    #@48
    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_4b
    .catchall {:try_start_46 .. :try_end_4b} :catchall_51

    #@4b
    .line 152
    iget-object v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->onOfflock:Ljava/util/concurrent/locks/Lock;

    #@4d
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    #@50
    goto :goto_d

    #@51
    :catchall_51
    move-exception v0

    #@52
    iget-object v1, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->onOfflock:Ljava/util/concurrent/locks/Lock;

    #@54
    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    #@57
    throw v0

    #@58
    .line 157
    :pswitch_58
    const-string v0, "NfcLgFactoryAdapter"

    #@5a
    const-string v1, "NFC system is Turning On..."

    #@5c
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@5f
    goto :goto_d

    #@60
    .line 160
    :pswitch_60
    const-string v0, "NfcLgFactoryAdapter"

    #@62
    const-string v1, "NFC system is Turning Off..."

    #@64
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@67
    goto :goto_d

    #@68
    .line 124
    :pswitch_data_68
    .packed-switch 0x29
        :pswitch_e
        :pswitch_58
        :pswitch_33
        :pswitch_60
    .end packed-switch
.end method

.method public static makeCPLCKey(Ljava/lang/String;)Ljava/lang/String;
    .registers 9
    .parameter "fullResult"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 329
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@4
    move-result v3

    #@5
    .line 330
    .local v3, resultLength:I
    const-string v5, "NfcLgFactoryAdapter"

    #@7
    new-instance v6, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v7, "makeCPLCKey : resultLength = "

    #@e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v6

    #@12
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v6

    #@16
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v6

    #@1a
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 331
    const/16 v5, 0x5e

    #@1f
    if-ge v3, v5, :cond_22

    #@21
    .line 346
    :cond_21
    :goto_21
    return-object v4

    #@22
    .line 335
    :cond_22
    add-int/lit8 v5, v3, -0x4

    #@24
    invoke-virtual {p0, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    .line 336
    .local v1, commandResult:Ljava/lang/String;
    const-string v5, "NfcLgFactoryAdapter"

    #@2a
    new-instance v6, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v7, "makeCPLCKey : commandResult = "

    #@31
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v6

    #@35
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v6

    #@39
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v6

    #@3d
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 337
    const-string v5, "9000"

    #@42
    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@45
    move-result v5

    #@46
    if-eqz v5, :cond_21

    #@48
    .line 341
    const/4 v4, 0x6

    #@49
    const/16 v5, 0xa

    #@4b
    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@4e
    move-result-object v2

    #@4f
    .line 342
    .local v2, fabricatorValue:Ljava/lang/String;
    const/16 v4, 0x1a

    #@51
    const/16 v5, 0x2a

    #@53
    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@56
    move-result-object v0

    #@57
    .line 343
    .local v0, chipInfoValue:Ljava/lang/String;
    const-string v4, "NfcLgFactoryAdapter"

    #@59
    new-instance v5, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v6, "makeCPLCKey : fabricatorValue = "

    #@60
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v5

    #@64
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v5

    #@68
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v5

    #@6c
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6f
    .line 344
    const-string v4, "NfcLgFactoryAdapter"

    #@71
    new-instance v5, Ljava/lang/StringBuilder;

    #@73
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    const-string v6, "makeCPLCKey : chipInfoValue = "

    #@78
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v5

    #@7c
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v5

    #@80
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@83
    move-result-object v5

    #@84
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@87
    .line 346
    new-instance v4, Ljava/lang/String;

    #@89
    new-instance v5, Ljava/lang/StringBuilder;

    #@8b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@8e
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v5

    #@92
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v5

    #@96
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@99
    move-result-object v5

    #@9a
    invoke-direct {v4, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    #@9d
    goto :goto_21
.end method

.method private nativeNfcDisableBlocking()Z
    .registers 7

    #@0
    .prologue
    .line 235
    const/4 v2, 0x1

    #@1
    iput-boolean v2, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mIsNfcNativeServiceOn:Z

    #@3
    .line 236
    const/4 v1, 0x1

    #@4
    .line 238
    .local v1, ret:Z
    iget-object v2, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mNfcAdapterAddon:Lcom/lge/nfcaddon/NfcAdapterAddon;

    #@6
    if-nez v2, :cond_11

    #@8
    .line 239
    const-string v2, "NfcLgFactoryAdapter"

    #@a
    const-string v3, "Null Nfc Adapter or Nfc Addon Adapter"

    #@c
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 240
    const/4 v2, 0x0

    #@10
    .line 270
    :goto_10
    return v2

    #@11
    .line 243
    :cond_11
    iget-object v2, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mNfcAdapterAddon:Lcom/lge/nfcaddon/NfcAdapterAddon;

    #@13
    invoke-virtual {v2}, Lcom/lge/nfcaddon/NfcAdapterAddon;->isNfcSystemEnabled()Z

    #@16
    move-result v2

    #@17
    if-nez v2, :cond_22

    #@19
    .line 244
    const-string v2, "NfcLgFactoryAdapter"

    #@1b
    const-string v3, "NFC is Already Disabled!"

    #@1d
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    move v2, v1

    #@21
    .line 245
    goto :goto_10

    #@22
    .line 250
    :cond_22
    :try_start_22
    iget-object v2, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mNfcAdapterAddon:Lcom/lge/nfcaddon/NfcAdapterAddon;

    #@24
    invoke-virtual {v2}, Lcom/lge/nfcaddon/NfcAdapterAddon;->disableNfcP2p()Z

    #@27
    .line 251
    iget-object v2, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mNfcAdapterAddon:Lcom/lge/nfcaddon/NfcAdapterAddon;

    #@29
    invoke-virtual {v2}, Lcom/lge/nfcaddon/NfcAdapterAddon;->disableNfcDiscovery()Z

    #@2c
    .line 252
    iget-object v2, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mNfcAdapterAddon:Lcom/lge/nfcaddon/NfcAdapterAddon;

    #@2e
    invoke-virtual {v2}, Lcom/lge/nfcaddon/NfcAdapterAddon;->disableNfcCard()Z

    #@31
    .line 253
    iget-object v2, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mNfcAdapterAddon:Lcom/lge/nfcaddon/NfcAdapterAddon;

    #@33
    invoke-virtual {v2}, Lcom/lge/nfcaddon/NfcAdapterAddon;->deinitNfcSystem()Z

    #@36
    .line 256
    iget-object v2, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->onOfflock:Ljava/util/concurrent/locks/Lock;

    #@38
    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->lock()V

    #@3b
    .line 258
    iget-object v2, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->onOffblockingCondition:Ljava/util/concurrent/locks/Condition;

    #@3d
    const-wide/16 v3, 0x2710

    #@3f
    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    #@41
    invoke-interface {v2, v3, v4, v5}, Ljava/util/concurrent/locks/Condition;->await(JLjava/util/concurrent/TimeUnit;)Z

    #@44
    move-result v2

    #@45
    if-nez v2, :cond_4f

    #@47
    .line 259
    const-string v2, "NfcLgFactoryAdapter"

    #@49
    const-string v3, "blockingCondition.await timeout"

    #@4b
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4e
    .catchall {:try_start_22 .. :try_end_4e} :catchall_75
    .catch Ljava/lang/InterruptedException; {:try_start_22 .. :try_end_4e} :catch_56

    #@4e
    .line 260
    const/4 v1, 0x0

    #@4f
    .line 267
    :cond_4f
    iget-object v2, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->onOfflock:Ljava/util/concurrent/locks/Lock;

    #@51
    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    #@54
    :goto_54
    move v2, v1

    #@55
    .line 270
    goto :goto_10

    #@56
    .line 263
    :catch_56
    move-exception v0

    #@57
    .line 264
    .local v0, e:Ljava/lang/InterruptedException;
    :try_start_57
    const-string v2, "NfcLgFactoryAdapter"

    #@59
    new-instance v3, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v4, "atCmd_NFC_On : InterruptedException !!"

    #@60
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v3

    #@64
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v3

    #@68
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v3

    #@6c
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6f
    .catchall {:try_start_57 .. :try_end_6f} :catchall_75

    #@6f
    .line 267
    iget-object v2, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->onOfflock:Ljava/util/concurrent/locks/Lock;

    #@71
    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    #@74
    goto :goto_54

    #@75
    .end local v0           #e:Ljava/lang/InterruptedException;
    :catchall_75
    move-exception v2

    #@76
    iget-object v3, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->onOfflock:Ljava/util/concurrent/locks/Lock;

    #@78
    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->unlock()V

    #@7b
    throw v2
.end method

.method private nativeNfcEnableBlocking()Z
    .registers 8

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 276
    iget-object v3, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mNfcAdapterAddon:Lcom/lge/nfcaddon/NfcAdapterAddon;

    #@4
    if-nez v3, :cond_e

    #@6
    .line 277
    const-string v2, "NfcLgFactoryAdapter"

    #@8
    const-string v3, "Null Nfc Adapter"

    #@a
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 311
    :goto_d
    return v1

    #@e
    .line 281
    :cond_e
    iget-object v3, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mNfcAdapterAddon:Lcom/lge/nfcaddon/NfcAdapterAddon;

    #@10
    invoke-virtual {v3}, Lcom/lge/nfcaddon/NfcAdapterAddon;->isNfcSystemEnabled()Z

    #@13
    move-result v3

    #@14
    if-ne v3, v2, :cond_1f

    #@16
    .line 282
    const-string v1, "NfcLgFactoryAdapter"

    #@18
    const-string v3, "NFC is Already Enabled!"

    #@1a
    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    move v1, v2

    #@1e
    .line 283
    goto :goto_d

    #@1f
    .line 286
    :cond_1f
    iget-boolean v3, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mIsNfcNativeServiceOn:Z

    #@21
    if-ne v3, v2, :cond_57

    #@23
    .line 288
    iget-object v3, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mNfcAdapterAddon:Lcom/lge/nfcaddon/NfcAdapterAddon;

    #@25
    invoke-virtual {v3}, Lcom/lge/nfcaddon/NfcAdapterAddon;->initNfcSystem()Z

    #@28
    .line 289
    iget-object v3, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mNfcAdapterAddon:Lcom/lge/nfcaddon/NfcAdapterAddon;

    #@2a
    invoke-virtual {v3}, Lcom/lge/nfcaddon/NfcAdapterAddon;->enableNfcCard()Z

    #@2d
    .line 290
    iget-object v3, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mNfcAdapterAddon:Lcom/lge/nfcaddon/NfcAdapterAddon;

    #@2f
    invoke-virtual {v3}, Lcom/lge/nfcaddon/NfcAdapterAddon;->enableNfcDiscovery()Z

    #@32
    .line 294
    iget-object v3, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->onOfflock:Ljava/util/concurrent/locks/Lock;

    #@34
    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->lock()V

    #@37
    .line 296
    :try_start_37
    iget-object v3, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->onOffblockingCondition:Ljava/util/concurrent/locks/Condition;

    #@39
    const-wide/16 v4, 0x1388

    #@3b
    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    #@3d
    invoke-interface {v3, v4, v5, v6}, Ljava/util/concurrent/locks/Condition;->await(JLjava/util/concurrent/TimeUnit;)Z

    #@40
    move-result v3

    #@41
    if-nez v3, :cond_50

    #@43
    .line 297
    const-string v3, "NfcLgFactoryAdapter"

    #@45
    const-string v4, "blockingCondition.await timeout"

    #@47
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4a
    .catchall {:try_start_37 .. :try_end_4a} :catchall_78
    .catch Ljava/lang/InterruptedException; {:try_start_37 .. :try_end_4a} :catch_59

    #@4a
    .line 305
    iget-object v2, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->onOfflock:Ljava/util/concurrent/locks/Lock;

    #@4c
    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    #@4f
    goto :goto_d

    #@50
    :cond_50
    iget-object v3, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->onOfflock:Ljava/util/concurrent/locks/Lock;

    #@52
    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->unlock()V

    #@55
    .line 308
    :goto_55
    iput-boolean v1, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mIsNfcNativeServiceOn:Z

    #@57
    :cond_57
    move v1, v2

    #@58
    .line 311
    goto :goto_d

    #@59
    .line 301
    :catch_59
    move-exception v0

    #@5a
    .line 302
    .local v0, e:Ljava/lang/InterruptedException;
    :try_start_5a
    const-string v3, "NfcLgFactoryAdapter"

    #@5c
    new-instance v4, Ljava/lang/StringBuilder;

    #@5e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@61
    const-string v5, "atCmd_NFC_On : InterruptedException !!"

    #@63
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v4

    #@67
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v4

    #@6b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6e
    move-result-object v4

    #@6f
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_72
    .catchall {:try_start_5a .. :try_end_72} :catchall_78

    #@72
    .line 305
    iget-object v3, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->onOfflock:Ljava/util/concurrent/locks/Lock;

    #@74
    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->unlock()V

    #@77
    goto :goto_55

    #@78
    .end local v0           #e:Ljava/lang/InterruptedException;
    :catchall_78
    move-exception v1

    #@79
    iget-object v2, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->onOfflock:Ljava/util/concurrent/locks/Lock;

    #@7b
    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    #@7e
    throw v1
.end method

.method private set_retval(ZI[B)V
    .registers 13
    .parameter "result"
    .parameter "command"
    .parameter "resp"

    #@0
    .prologue
    const/4 v8, 0x4

    #@1
    const/4 v4, 0x0

    #@2
    const/4 v7, 0x3

    #@3
    const/4 v6, 0x1

    #@4
    const/4 v5, 0x2

    #@5
    .line 351
    if-nez p1, :cond_18

    #@7
    .line 352
    const/4 v3, 0x6

    #@8
    if-ne p2, v3, :cond_13

    #@a
    .line 353
    const-string v3, "NFC UPDATE FAIL"

    #@c
    sput-object v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retString:Ljava/lang/String;

    #@e
    .line 358
    :goto_e
    sget-object v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retArr:[B

    #@10
    aput-byte v4, v3, v4

    #@12
    .line 455
    :goto_12
    return-void

    #@13
    .line 356
    :cond_13
    const-string v3, "FAIL"

    #@15
    sput-object v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retString:Ljava/lang/String;

    #@17
    goto :goto_e

    #@18
    .line 361
    :cond_18
    sget-object v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retArr:[B

    #@1a
    aput-byte v6, v3, v4

    #@1c
    .line 363
    packed-switch p2, :pswitch_data_1ba

    #@1f
    .line 451
    :pswitch_1f
    const-string v3, "NfcFactoryAdapter"

    #@21
    const-string v4, "@@set_retval Called Invalid"

    #@23
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 454
    :cond_26
    :goto_26
    const-string v3, "NfcFactoryAdapter"

    #@28
    const-string v4, "@@set_retval Called"

    #@2a
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    goto :goto_12

    #@2e
    .line 365
    :pswitch_2e
    const-string v3, "NFC OFF"

    #@30
    sput-object v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retString:Ljava/lang/String;

    #@32
    goto :goto_26

    #@33
    .line 368
    :pswitch_33
    const-string v3, "Service Disabled"

    #@35
    sput-object v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retString:Ljava/lang/String;

    #@37
    goto :goto_26

    #@38
    .line 371
    :pswitch_38
    const-string v3, "NFC ON"

    #@3a
    sput-object v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retString:Ljava/lang/String;

    #@3c
    goto :goto_26

    #@3d
    .line 374
    :pswitch_3d
    const-string v3, "NFC OFF"

    #@3f
    sput-object v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retString:Ljava/lang/String;

    #@41
    goto :goto_26

    #@42
    .line 377
    :pswitch_42
    aget-byte v3, p3, v5

    #@44
    packed-switch v3, :pswitch_data_1ea

    #@47
    .line 389
    const-string v3, "SWIO FAIL, PMUVCC FAIL"

    #@49
    sput-object v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retString:Ljava/lang/String;

    #@4b
    .line 392
    :goto_4b
    sget-object v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retArr:[B

    #@4d
    aget-byte v4, p3, v5

    #@4f
    aput-byte v4, v3, v6

    #@51
    goto :goto_26

    #@52
    .line 379
    :pswitch_52
    const-string v3, "SWIO PASS, PMUVCC PASS"

    #@54
    sput-object v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retString:Ljava/lang/String;

    #@56
    goto :goto_4b

    #@57
    .line 382
    :pswitch_57
    const-string v3, "SWIO FAIL, PMUVCC PASS"

    #@59
    sput-object v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retString:Ljava/lang/String;

    #@5b
    goto :goto_4b

    #@5c
    .line 385
    :pswitch_5c
    const-string v3, "SWIO PASS, PMUVCC FAIL"

    #@5e
    sput-object v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retString:Ljava/lang/String;

    #@60
    goto :goto_4b

    #@61
    .line 396
    :pswitch_61
    new-instance v3, Ljava/lang/StringBuilder;

    #@63
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@66
    const-string v4, "ANT "

    #@68
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v3

    #@6c
    aget-byte v4, p3, v5

    #@6e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@71
    move-result-object v3

    #@72
    const-string v4, ":"

    #@74
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v3

    #@78
    aget-byte v4, p3, v7

    #@7a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v3

    #@7e
    const-string v4, ":"

    #@80
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v3

    #@84
    aget-byte v4, p3, v8

    #@86
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@89
    move-result-object v3

    #@8a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8d
    move-result-object v3

    #@8e
    sput-object v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retString:Ljava/lang/String;

    #@90
    .line 397
    sget-object v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retArr:[B

    #@92
    aget-byte v4, p3, v5

    #@94
    aput-byte v4, v3, v6

    #@96
    .line 398
    sget-object v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retArr:[B

    #@98
    aget-byte v4, p3, v7

    #@9a
    aput-byte v4, v3, v5

    #@9c
    .line 399
    sget-object v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retArr:[B

    #@9e
    aget-byte v4, p3, v8

    #@a0
    aput-byte v4, v3, v7

    #@a2
    goto :goto_26

    #@a3
    .line 402
    :pswitch_a3
    const-string v3, "PASS"

    #@a5
    sput-object v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retString:Ljava/lang/String;

    #@a7
    goto/16 :goto_26

    #@a9
    .line 405
    :pswitch_a9
    aget-byte v3, p3, v5

    #@ab
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@ae
    move-result-object v1

    #@af
    .line 406
    .local v1, sf:Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    #@b1
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@b4
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@b7
    move-result v4

    #@b8
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v3

    #@bc
    const-string v4, ""

    #@be
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v3

    #@c2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c5
    move-result-object v3

    #@c6
    sput-object v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retString:Ljava/lang/String;

    #@c8
    .line 407
    sget-object v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retArr:[B

    #@ca
    aget-byte v4, p3, v5

    #@cc
    aput-byte v4, v3, v6

    #@ce
    .line 408
    sget-object v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retArr:[B

    #@d0
    aget-byte v4, p3, v7

    #@d2
    aput-byte v4, v3, v5

    #@d4
    goto/16 :goto_26

    #@d6
    .line 411
    .end local v1           #sf:Ljava/lang/String;
    :pswitch_d6
    const-string v3, "NFC UPDATE PASS"

    #@d8
    sput-object v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retString:Ljava/lang/String;

    #@da
    goto/16 :goto_26

    #@dc
    .line 418
    :pswitch_dc
    new-instance v3, Ljava/lang/StringBuilder;

    #@de
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@e1
    const-string v4, "FW : "

    #@e3
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e6
    move-result-object v3

    #@e7
    aget-byte v4, p3, v5

    #@e9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ec
    move-result-object v3

    #@ed
    const-string v4, "_"

    #@ef
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f2
    move-result-object v3

    #@f3
    aget-byte v4, p3, v7

    #@f5
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f8
    move-result-object v3

    #@f9
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fc
    move-result-object v3

    #@fd
    sput-object v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retString:Ljava/lang/String;

    #@ff
    .line 419
    sget-object v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retArr:[B

    #@101
    aget-byte v4, p3, v5

    #@103
    aput-byte v4, v3, v6

    #@105
    .line 420
    sget-object v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retArr:[B

    #@107
    aget-byte v4, p3, v7

    #@109
    aput-byte v4, v3, v5

    #@10b
    goto/16 :goto_26

    #@10d
    .line 424
    :pswitch_10d
    const-string v3, "CE Test Start"

    #@10f
    sput-object v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retString:Ljava/lang/String;

    #@111
    goto/16 :goto_26

    #@113
    .line 427
    :pswitch_113
    const-string v3, "CE Test Stop"

    #@115
    sput-object v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retString:Ljava/lang/String;

    #@117
    goto/16 :goto_26

    #@119
    .line 430
    :pswitch_119
    sget v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->detectcount:I

    #@11b
    add-int/lit8 v3, v3, 0x1

    #@11d
    sput v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->detectcount:I

    #@11f
    .line 431
    new-instance v3, Ljava/lang/StringBuilder;

    #@121
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@124
    sget v4, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->detectcount:I

    #@126
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@129
    move-result-object v3

    #@12a
    const-string v4, "Tags Detected"

    #@12c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12f
    move-result-object v3

    #@130
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@133
    move-result-object v3

    #@134
    sput-object v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retString:Ljava/lang/String;

    #@136
    .line 432
    sget-boolean v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->bSenseMTFirst:Z

    #@138
    if-nez v3, :cond_26

    #@13a
    .line 433
    invoke-virtual {p0, v6}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->SenseMultiTagStart(I)V

    #@13d
    goto/16 :goto_26

    #@13f
    .line 437
    :pswitch_13f
    const-string v3, "Register Write"

    #@141
    sput-object v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retString:Ljava/lang/String;

    #@143
    goto/16 :goto_26

    #@145
    .line 440
    :pswitch_145
    new-instance v0, Ljava/lang/Byte;

    #@147
    aget-byte v3, p3, v5

    #@149
    invoke-direct {v0, v3}, Ljava/lang/Byte;-><init>(B)V

    #@14c
    .line 441
    .local v0, bt:Ljava/lang/Byte;
    new-instance v3, Ljava/lang/StringBuilder;

    #@14e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@151
    const-string v4, "Value : "

    #@153
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@156
    move-result-object v3

    #@157
    invoke-virtual {v0}, Ljava/lang/Byte;->intValue()I

    #@15a
    move-result v4

    #@15b
    and-int/lit16 v4, v4, 0xff

    #@15d
    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@160
    move-result-object v4

    #@161
    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@164
    move-result-object v4

    #@165
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@168
    move-result-object v3

    #@169
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16c
    move-result-object v3

    #@16d
    sput-object v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retString:Ljava/lang/String;

    #@16f
    .line 442
    sget-object v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retArr:[B

    #@171
    aget-byte v4, p3, v5

    #@173
    aput-byte v4, v3, v6

    #@175
    .line 443
    const-string v3, "NfcFactoryAdapter"

    #@177
    new-instance v4, Ljava/lang/StringBuilder;

    #@179
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@17c
    const-string v5, "getRegister Return Array:"

    #@17e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@181
    move-result-object v4

    #@182
    sget-object v5, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retArr:[B

    #@184
    aget-byte v5, v5, v6

    #@186
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@189
    move-result-object v4

    #@18a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18d
    move-result-object v4

    #@18e
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@191
    goto/16 :goto_26

    #@193
    .line 446
    .end local v0           #bt:Ljava/lang/Byte;
    :pswitch_193
    invoke-static {p3}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->tmpToHex([B)Ljava/lang/String;

    #@196
    move-result-object v2

    #@197
    .line 447
    .local v2, tmpCPLCResult:Ljava/lang/String;
    invoke-static {v2}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->makeCPLCKey(Ljava/lang/String;)Ljava/lang/String;

    #@19a
    move-result-object v3

    #@19b
    sput-object v3, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retString:Ljava/lang/String;

    #@19d
    .line 448
    const-string v3, "NfcFactoryAdapter"

    #@19f
    new-instance v4, Ljava/lang/StringBuilder;

    #@1a1
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1a4
    const-string v5, "getCPLC Return Array:"

    #@1a6
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a9
    move-result-object v4

    #@1aa
    sget-object v5, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retString:Ljava/lang/String;

    #@1ac
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1af
    move-result-object v4

    #@1b0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b3
    move-result-object v4

    #@1b4
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1b7
    goto/16 :goto_26

    #@1b9
    .line 363
    nop

    #@1ba
    :pswitch_data_1ba
    .packed-switch 0x0
        :pswitch_38
        :pswitch_3d
        :pswitch_42
        :pswitch_61
        :pswitch_a3
        :pswitch_a9
        :pswitch_d6
        :pswitch_dc
        :pswitch_10d
        :pswitch_113
        :pswitch_119
        :pswitch_1f
        :pswitch_13f
        :pswitch_145
        :pswitch_193
        :pswitch_1f
        :pswitch_1f
        :pswitch_1f
        :pswitch_1f
        :pswitch_1f
        :pswitch_2e
        :pswitch_33
    .end packed-switch

    #@1ea
    .line 377
    :pswitch_data_1ea
    .packed-switch 0x1
        :pswitch_52
        :pswitch_57
        :pswitch_5c
    .end packed-switch
.end method

.method public static tmpToHex([B)Ljava/lang/String;
    .registers 8
    .parameter "array"

    #@0
    .prologue
    .line 315
    const/4 v5, 0x0

    #@1
    aget-byte v1, p0, v5

    #@3
    .line 316
    .local v1, arraySize:I
    mul-int/lit8 v5, v1, 0x2

    #@5
    new-array v2, v5, [C

    #@7
    .line 317
    .local v2, chars:[C
    array-length v5, p0

    #@8
    mul-int/lit8 v4, v5, 0x2

    #@a
    .line 318
    .local v4, local_length:I
    const-string v5, "0123456789abcdef"

    #@c
    invoke-virtual {v5}, Ljava/lang/String;->toCharArray()[C

    #@f
    move-result-object v0

    #@10
    .line 320
    .local v0, HEX_CHARS:[C
    const/4 v3, 0x0

    #@11
    .local v3, j:I
    :goto_11
    if-ge v3, v1, :cond_32

    #@13
    .line 321
    mul-int/lit8 v5, v3, 0x2

    #@15
    add-int/lit8 v6, v3, 0x1

    #@17
    aget-byte v6, p0, v6

    #@19
    and-int/lit16 v6, v6, 0xf0

    #@1b
    ushr-int/lit8 v6, v6, 0x4

    #@1d
    aget-char v6, v0, v6

    #@1f
    aput-char v6, v2, v5

    #@21
    .line 322
    mul-int/lit8 v5, v3, 0x2

    #@23
    add-int/lit8 v5, v5, 0x1

    #@25
    add-int/lit8 v6, v3, 0x1

    #@27
    aget-byte v6, p0, v6

    #@29
    and-int/lit8 v6, v6, 0xf

    #@2b
    aget-char v6, v0, v6

    #@2d
    aput-char v6, v2, v5

    #@2f
    .line 320
    add-int/lit8 v3, v3, 0x1

    #@31
    goto :goto_11

    #@32
    .line 325
    :cond_32
    new-instance v5, Ljava/lang/String;

    #@34
    invoke-direct {v5, v2}, Ljava/lang/String;-><init>([C)V

    #@37
    return-object v5
.end method


# virtual methods
.method public ActAntSelfTest([B)V
    .registers 7
    .parameter "data"

    #@0
    .prologue
    .line 499
    const/4 v0, 0x0

    #@1
    .line 502
    .local v0, ret:I
    invoke-virtual {p0}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->CheckTestStatus()Z

    #@4
    move-result v1

    #@5
    .line 504
    .local v1, status:Z
    if-nez v1, :cond_8

    #@7
    .line 511
    :goto_7
    return-void

    #@8
    .line 508
    :cond_8
    const-string v2, "NfcFactoryAdapter"

    #@a
    const-string v3, "ActAntSelfTest::In"

    #@c
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 509
    iget-object v2, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mProgressThread:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@11
    const/4 v3, 0x3

    #@12
    invoke-virtual {v2, v3, p1}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->sendMsg2Thread(I[B)I

    #@15
    move-result v0

    #@16
    .line 510
    const-string v2, "NfcFactoryAdapter"

    #@18
    new-instance v3, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v4, "ActAntSelfTest called["

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    const-string v4, "]"

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v3

    #@31
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    goto :goto_7
.end method

.method public CheckSwpStatus([B)V
    .registers 7
    .parameter "data"

    #@0
    .prologue
    .line 483
    const/4 v0, 0x0

    #@1
    .line 486
    .local v0, ret:I
    invoke-virtual {p0}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->CheckTestStatus()Z

    #@4
    move-result v1

    #@5
    .line 488
    .local v1, status:Z
    if-nez v1, :cond_8

    #@7
    .line 496
    :goto_7
    return-void

    #@8
    .line 492
    :cond_8
    const-string v2, "NfcFactoryAdapter"

    #@a
    const-string v3, "CheckSwpStatus::In"

    #@c
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 493
    iget-object v2, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mProgressThread:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@11
    const/4 v3, 0x2

    #@12
    invoke-virtual {v2, v3, p1}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->sendMsg2Thread(I[B)I

    #@15
    move-result v0

    #@16
    .line 495
    const-string v2, "NfcFactoryAdapter"

    #@18
    new-instance v3, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v4, "CheckSwpStatus called["

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    const-string v4, "]"

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v3

    #@31
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    goto :goto_7
.end method

.method public CheckTestStatus()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 696
    sget-boolean v1, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->bTestStarted:Z

    #@3
    if-ne v1, v0, :cond_6

    #@5
    .line 700
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public DisableNfsService([B)V
    .registers 6
    .parameter "data"

    #@0
    .prologue
    .line 465
    const/4 v0, 0x0

    #@1
    .line 466
    .local v0, ret:I
    const-string v1, "NfcFactoryAdapter"

    #@3
    const-string v2, "DisableNfsService::In"

    #@5
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 467
    iget-object v1, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mProgressThread:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@a
    const/16 v2, 0x15

    #@c
    invoke-virtual {v1, v2, p1}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->sendMsg2Thread(I[B)I

    #@f
    move-result v0

    #@10
    .line 468
    const-string v1, "NfcFactoryAdapter"

    #@12
    new-instance v2, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v3, "DisableNfsService called["

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    const-string v3, "]"

    #@23
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v2

    #@2b
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 469
    return-void
.end method

.method public EnableNfsService([B)V
    .registers 6
    .parameter "data"

    #@0
    .prologue
    .line 458
    const/4 v0, 0x0

    #@1
    .line 459
    .local v0, ret:I
    const-string v1, "NfcFactoryAdapter"

    #@3
    const-string v2, "EnableNfsService::In"

    #@5
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 460
    iget-object v1, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mProgressThread:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@a
    const/16 v2, 0x14

    #@c
    invoke-virtual {v1, v2, p1}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->sendMsg2Thread(I[B)I

    #@f
    move-result v0

    #@10
    .line 461
    const-string v1, "NfcFactoryAdapter"

    #@12
    new-instance v2, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v3, "EnableNfsService called["

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    const-string v3, "]"

    #@23
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v2

    #@2b
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 462
    return-void
.end method

.method public FinshTestEnv([B)V
    .registers 7
    .parameter "data"

    #@0
    .prologue
    .line 579
    invoke-virtual {p0}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->CheckTestStatus()Z

    #@3
    move-result v1

    #@4
    .line 581
    .local v1, status:Z
    if-nez v1, :cond_7

    #@6
    .line 589
    :goto_6
    return-void

    #@7
    .line 585
    :cond_7
    iget-object v2, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mProgressThread:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@9
    const/4 v3, 0x1

    #@a
    invoke-virtual {v2, v3, p1}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->sendMsg2Thread(I[B)I

    #@d
    move-result v0

    #@e
    .line 587
    .local v0, ret:I
    const/4 v2, 0x0

    #@f
    sput-boolean v2, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->bTestStarted:Z

    #@11
    .line 588
    const-string v2, "NfcFactoryAdapter"

    #@13
    new-instance v3, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v4, "FinshTestEnv called["

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    const-string v4, "]"

    #@24
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v3

    #@28
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v3

    #@2c
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    goto :goto_6
.end method

.method public GenConsistentFieldStart([B)V
    .registers 7
    .parameter "data"

    #@0
    .prologue
    .line 595
    invoke-virtual {p0}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->CheckTestStatus()Z

    #@3
    move-result v1

    #@4
    .line 597
    .local v1, status:Z
    if-nez v1, :cond_7

    #@6
    .line 604
    :goto_6
    return-void

    #@7
    .line 601
    :cond_7
    iget-object v2, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mProgressThread:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@9
    const/16 v3, 0x8

    #@b
    invoke-virtual {v2, v3, p1}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->sendMsg2Thread(I[B)I

    #@e
    move-result v0

    #@f
    .line 603
    .local v0, ret:I
    const-string v2, "NfcFactoryAdapter"

    #@11
    new-instance v3, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v4, "GenConsistentFieldStart called["

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    const-string v4, "]"

    #@22
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    goto :goto_6
.end method

.method public GenConsistentFieldStop([B)V
    .registers 7
    .parameter "data"

    #@0
    .prologue
    .line 610
    invoke-virtual {p0}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->CheckTestStatus()Z

    #@3
    move-result v1

    #@4
    .line 612
    .local v1, status:Z
    if-nez v1, :cond_7

    #@6
    .line 619
    :goto_6
    return-void

    #@7
    .line 616
    :cond_7
    iget-object v2, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mProgressThread:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@9
    const/16 v3, 0x9

    #@b
    invoke-virtual {v2, v3, p1}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->sendMsg2Thread(I[B)I

    #@e
    move-result v0

    #@f
    .line 618
    .local v0, ret:I
    const-string v2, "NfcFactoryAdapter"

    #@11
    new-instance v3, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v4, "GenConsistentFieldStop called["

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    const-string v4, "]"

    #@22
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    goto :goto_6
.end method

.method public GetCPLCValue([B)V
    .registers 7
    .parameter "reg"

    #@0
    .prologue
    .line 681
    const/4 v0, 0x0

    #@1
    .line 684
    .local v0, ret:I
    invoke-virtual {p0}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->CheckTestStatus()Z

    #@4
    move-result v1

    #@5
    .line 685
    .local v1, status:Z
    if-nez v1, :cond_8

    #@7
    .line 693
    :goto_7
    return-void

    #@8
    .line 689
    :cond_8
    const-string v2, "NfcFactoryAdapter"

    #@a
    const-string v3, "GetCPLCValue::In"

    #@c
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 690
    iget-object v2, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mProgressThread:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@11
    const/16 v3, 0xe

    #@13
    invoke-virtual {v2, v3, p1}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->sendMsg2Thread(I[B)I

    #@16
    move-result v0

    #@17
    .line 692
    const-string v2, "NfcFactoryAdapter"

    #@19
    new-instance v3, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v4, "GetCPLCValue called["

    #@20
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@27
    move-result-object v3

    #@28
    const-string v4, "]"

    #@2a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v3

    #@2e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v3

    #@32
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    goto :goto_7
.end method

.method public GetFileVersion([B)V
    .registers 7
    .parameter "data"

    #@0
    .prologue
    .line 529
    const/4 v0, 0x0

    #@1
    .line 532
    .local v0, ret:I
    invoke-virtual {p0}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->CheckTestStatus()Z

    #@4
    move-result v1

    #@5
    .line 534
    .local v1, status:Z
    if-nez v1, :cond_8

    #@7
    .line 541
    :goto_7
    return-void

    #@8
    .line 538
    :cond_8
    const-string v2, "NfcFactoryAdapter"

    #@a
    const-string v3, "GetFileVersion::In"

    #@c
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 539
    iget-object v2, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mProgressThread:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@11
    const/4 v3, 0x5

    #@12
    invoke-virtual {v2, v3, p1}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->sendMsg2Thread(I[B)I

    #@15
    move-result v0

    #@16
    .line 540
    const-string v2, "NfcFactoryAdapter"

    #@18
    new-instance v3, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v4, "GetFileVersion called["

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    const-string v4, "]"

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v3

    #@31
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    goto :goto_7
.end method

.method public GetRegValue([B)V
    .registers 7
    .parameter "reg"

    #@0
    .prologue
    .line 672
    invoke-virtual {p0}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->CheckTestStatus()Z

    #@3
    move-result v1

    #@4
    .line 673
    .local v1, status:Z
    if-nez v1, :cond_7

    #@6
    .line 679
    :goto_6
    return-void

    #@7
    .line 677
    :cond_7
    iget-object v2, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mProgressThread:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@9
    const/16 v3, 0xd

    #@b
    invoke-virtual {v2, v3, p1}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->sendMsg2Thread(I[B)I

    #@e
    move-result v0

    #@f
    .line 678
    .local v0, ret:I
    const-string v2, "NfcFactoryAdapter"

    #@11
    new-instance v3, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v4, "GetRegValue called["

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    const-string v4, "]"

    #@22
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    goto :goto_6
.end method

.method public GetVersion([B)V
    .registers 7
    .parameter "data"

    #@0
    .prologue
    .line 560
    const/4 v0, 0x0

    #@1
    .line 563
    .local v0, ret:I
    invoke-virtual {p0}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->CheckTestStatus()Z

    #@4
    move-result v1

    #@5
    .line 565
    .local v1, status:Z
    if-nez v1, :cond_8

    #@7
    .line 572
    :goto_7
    return-void

    #@8
    .line 569
    :cond_8
    const-string v2, "NfcFactoryAdapter"

    #@a
    const-string v3, "GetVersion::In"

    #@c
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 570
    iget-object v2, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mProgressThread:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@11
    const/4 v3, 0x7

    #@12
    invoke-virtual {v2, v3, p1}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->sendMsg2Thread(I[B)I

    #@15
    move-result v0

    #@16
    .line 571
    const-string v2, "NfcFactoryAdapter"

    #@18
    new-instance v3, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v4, "GetVersion called["

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    const-string v4, "]"

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v3

    #@31
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    goto :goto_7
.end method

.method public NfcTestCmd(I[B)Ljava/lang/String;
    .registers 13
    .parameter "cmd"
    .parameter "resval"

    #@0
    .prologue
    const/16 v9, 0x80

    #@2
    const/16 v8, 0x1f

    #@4
    const/4 v7, 0x2

    #@5
    const/4 v6, 0x0

    #@6
    const/4 v5, 0x1

    #@7
    .line 705
    new-array v1, v9, [B

    #@9
    .line 706
    .local v1, ret:[B
    const/4 v0, 0x0

    #@a
    .line 707
    .local v0, i:I
    const-string v2, "NfcFactoryAdapter"

    #@c
    new-instance v3, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v4, "NfcTestCmd : "

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 709
    packed-switch p1, :pswitch_data_fe

    #@25
    .line 776
    :pswitch_25
    const-string v2, "NfcFactoryAdapter"

    #@27
    const-string v3, "Invalid Command Received"

    #@29
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 777
    aput-byte v6, p2, v5

    #@2e
    .line 778
    const/16 v2, 0xf

    #@30
    invoke-direct {p0, v6, v2, v1}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->set_retval(ZI[B)V

    #@33
    .line 779
    const/4 v2, 0x0

    #@34
    .line 786
    :goto_34
    return-object v2

    #@35
    .line 711
    :pswitch_35
    invoke-virtual {p0, p2}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->SetTestEnv([B)V

    #@38
    .line 782
    :goto_38
    const/4 v0, 0x0

    #@39
    :goto_39
    if-ge v0, v9, :cond_f9

    #@3b
    .line 783
    sget-object v2, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retArr:[B

    #@3d
    aget-byte v2, v2, v0

    #@3f
    aput-byte v2, p2, v0

    #@41
    .line 782
    add-int/lit8 v0, v0, 0x1

    #@43
    goto :goto_39

    #@44
    .line 714
    :pswitch_44
    invoke-virtual {p0, p2}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->FinshTestEnv([B)V

    #@47
    goto :goto_38

    #@48
    .line 717
    :pswitch_48
    invoke-virtual {p0, p2}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->CheckSwpStatus([B)V

    #@4b
    goto :goto_38

    #@4c
    .line 720
    :pswitch_4c
    invoke-virtual {p0, p2}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->ActAntSelfTest([B)V

    #@4f
    goto :goto_38

    #@50
    .line 723
    :pswitch_50
    invoke-virtual {p0, p2}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->TestReaderMode([B)V

    #@53
    goto :goto_38

    #@54
    .line 726
    :pswitch_54
    invoke-virtual {p0, p2}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->GetFileVersion([B)V

    #@57
    goto :goto_38

    #@58
    .line 729
    :pswitch_58
    invoke-virtual {p0, p2}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->UpgradeFirmeware([B)V

    #@5b
    goto :goto_38

    #@5c
    .line 732
    :pswitch_5c
    invoke-virtual {p0, p2}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->GetVersion([B)V

    #@5f
    goto :goto_38

    #@60
    .line 735
    :pswitch_60
    invoke-virtual {p0, p2}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->GenConsistentFieldStart([B)V

    #@63
    goto :goto_38

    #@64
    .line 738
    :pswitch_64
    invoke-virtual {p0, p2}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->GenConsistentFieldStop([B)V

    #@67
    goto :goto_38

    #@68
    .line 741
    :pswitch_68
    const-string v2, "NfcFactoryAdapter"

    #@6a
    new-instance v3, Ljava/lang/StringBuilder;

    #@6c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@6f
    const-string v4, "Set register 0x"

    #@71
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v3

    #@75
    aget-byte v4, p2, v5

    #@77
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v3

    #@7b
    aget-byte v4, p2, v7

    #@7d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@80
    move-result-object v3

    #@81
    const-string v4, " return 0x"

    #@83
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v3

    #@87
    const/4 v4, 0x3

    #@88
    aget-byte v4, p2, v4

    #@8a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v3

    #@8e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@91
    move-result-object v3

    #@92
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@95
    .line 742
    invoke-virtual {p0, p2}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->SetRegValue([B)V

    #@98
    goto :goto_38

    #@99
    .line 745
    :pswitch_99
    const-string v2, "NfcFactoryAdapter"

    #@9b
    new-instance v3, Ljava/lang/StringBuilder;

    #@9d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@a0
    const-string v4, "Get register 0x"

    #@a2
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v3

    #@a6
    aget-byte v4, p2, v5

    #@a8
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v3

    #@ac
    aget-byte v4, p2, v7

    #@ae
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v3

    #@b2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b5
    move-result-object v3

    #@b6
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b9
    .line 746
    invoke-virtual {p0, p2}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->GetRegValue([B)V

    #@bc
    goto/16 :goto_38

    #@be
    .line 749
    :pswitch_be
    invoke-virtual {p0}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->CheckTestStatus()Z

    #@c1
    move-result v2

    #@c2
    if-ne v2, v5, :cond_cf

    #@c4
    .line 750
    const-string v2, "\u0002NFC ON\u0003"

    #@c6
    sput-object v2, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retString:Ljava/lang/String;

    #@c8
    .line 751
    aput-byte v5, v1, v5

    #@ca
    .line 752
    invoke-direct {p0, v5, v8, v1}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->set_retval(ZI[B)V

    #@cd
    goto/16 :goto_38

    #@cf
    .line 755
    :cond_cf
    const-string v2, "\u0002NFC OFF\u0003"

    #@d1
    sput-object v2, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retString:Ljava/lang/String;

    #@d3
    .line 756
    aput-byte v6, v1, v5

    #@d5
    .line 757
    invoke-direct {p0, v5, v8, v1}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->set_retval(ZI[B)V

    #@d8
    goto/16 :goto_38

    #@da
    .line 761
    :pswitch_da
    invoke-virtual {p0}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->StopThread()V

    #@dd
    .line 762
    const-string v2, "Thread Off"

    #@df
    sput-object v2, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retString:Ljava/lang/String;

    #@e1
    .line 763
    aput-byte v5, v1, v5

    #@e3
    .line 764
    const/16 v2, 0x1e

    #@e5
    invoke-direct {p0, v5, v2, v1}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->set_retval(ZI[B)V

    #@e8
    goto/16 :goto_38

    #@ea
    .line 767
    :pswitch_ea
    invoke-virtual {p0, p2}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->EnableNfsService([B)V

    #@ed
    goto/16 :goto_38

    #@ef
    .line 770
    :pswitch_ef
    invoke-virtual {p0, p2}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->DisableNfsService([B)V

    #@f2
    goto/16 :goto_38

    #@f4
    .line 773
    :pswitch_f4
    invoke-virtual {p0, p2}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->GetCPLCValue([B)V

    #@f7
    goto/16 :goto_38

    #@f9
    .line 786
    :cond_f9
    sget-object v2, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retString:Ljava/lang/String;

    #@fb
    goto/16 :goto_34

    #@fd
    .line 709
    nop

    #@fe
    :pswitch_data_fe
    .packed-switch 0x0
        :pswitch_35
        :pswitch_44
        :pswitch_48
        :pswitch_4c
        :pswitch_50
        :pswitch_54
        :pswitch_58
        :pswitch_5c
        :pswitch_60
        :pswitch_64
        :pswitch_25
        :pswitch_25
        :pswitch_68
        :pswitch_99
        :pswitch_f4
        :pswitch_25
        :pswitch_25
        :pswitch_25
        :pswitch_25
        :pswitch_25
        :pswitch_ea
        :pswitch_ef
        :pswitch_25
        :pswitch_25
        :pswitch_25
        :pswitch_25
        :pswitch_25
        :pswitch_25
        :pswitch_25
        :pswitch_25
        :pswitch_da
        :pswitch_be
    .end packed-switch
.end method

.method public ResetRetVal()V
    .registers 2

    #@0
    .prologue
    .line 790
    const/4 v0, 0x0

    #@1
    sput-object v0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retString:Ljava/lang/String;

    #@3
    .line 791
    const/4 v0, 0x0

    #@4
    sput-byte v0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retChar:B

    #@6
    .line 792
    const/16 v0, 0x80

    #@8
    new-array v0, v0, [B

    #@a
    sput-object v0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->retArr:[B

    #@c
    .line 793
    return-void
.end method

.method public SenseMultiTagStart(I)V
    .registers 8
    .parameter "mode"

    #@0
    .prologue
    .line 638
    const/4 v3, 0x5

    #@1
    new-array v0, v3, [B

    #@3
    .line 640
    .local v0, data:[B
    invoke-virtual {p0}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->CheckTestStatus()Z

    #@6
    move-result v2

    #@7
    .line 642
    .local v2, status:Z
    if-nez v2, :cond_a

    #@9
    .line 653
    :goto_9
    return-void

    #@a
    .line 646
    :cond_a
    const/4 v3, 0x1

    #@b
    if-ne p1, v3, :cond_34

    #@d
    .line 647
    iget-object v3, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mProgressThread:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@f
    const/16 v4, 0xa

    #@11
    invoke-virtual {v3, v4, v0}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->sendMsg2Thread(I[B)I

    #@14
    move-result v1

    #@15
    .line 652
    .local v1, ret:I
    :goto_15
    const-string v3, "NfcFactoryAdapter"

    #@17
    new-instance v4, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v5, "SenseMultiTag called["

    #@1e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v4

    #@22
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25
    move-result-object v4

    #@26
    const-string v5, "]"

    #@28
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v4

    #@2c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v4

    #@30
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    goto :goto_9

    #@34
    .line 650
    .end local v1           #ret:I
    :cond_34
    iget-object v3, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mProgressThread:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@36
    const/16 v4, 0xb

    #@38
    invoke-virtual {v3, v4, v0}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->sendMsg2Thread(I[B)I

    #@3b
    move-result v1

    #@3c
    .restart local v1       #ret:I
    goto :goto_15
.end method

.method public SetRegValue([B)V
    .registers 7
    .parameter "reg"

    #@0
    .prologue
    .line 659
    invoke-virtual {p0}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->CheckTestStatus()Z

    #@3
    move-result v1

    #@4
    .line 660
    .local v1, status:Z
    if-nez v1, :cond_7

    #@6
    .line 666
    :goto_6
    return-void

    #@7
    .line 664
    :cond_7
    iget-object v2, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mProgressThread:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@9
    const/16 v3, 0xc

    #@b
    invoke-virtual {v2, v3, p1}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->sendMsg2Thread(I[B)I

    #@e
    move-result v0

    #@f
    .line 665
    .local v0, ret:I
    const-string v2, "NfcFactoryAdapter"

    #@11
    new-instance v3, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v4, "SetRegValue called["

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    const-string v4, "]"

    #@22
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    goto :goto_6
.end method

.method public SetTestEnv([B)V
    .registers 6
    .parameter "data"

    #@0
    .prologue
    .line 472
    const/4 v0, 0x0

    #@1
    .line 473
    .local v0, ret:I
    const-string v1, "NfcFactoryAdapter"

    #@3
    const-string v2, "SetTestEnv::In"

    #@5
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 474
    iget-object v1, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mProgressThread:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@a
    const/16 v2, 0x15

    #@c
    invoke-virtual {v1, v2, p1}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->sendMsg2Thread(I[B)I

    #@f
    move-result v0

    #@10
    .line 475
    const-wide/16 v1, 0x7d0

    #@12
    invoke-static {v1, v2}, Landroid/os/SystemClock;->sleep(J)V

    #@15
    .line 476
    iget-object v1, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mProgressThread:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@17
    const/4 v2, 0x0

    #@18
    invoke-virtual {v1, v2, p1}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->sendMsg2Thread(I[B)I

    #@1b
    move-result v0

    #@1c
    .line 478
    const/4 v1, 0x1

    #@1d
    sput-boolean v1, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->bTestStarted:Z

    #@1f
    .line 479
    const-string v1, "NfcFactoryAdapter"

    #@21
    new-instance v2, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v3, "SetTestEnv called["

    #@28
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v2

    #@2c
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    const-string v3, "]"

    #@32
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v2

    #@36
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v2

    #@3a
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 480
    return-void
.end method

.method public StopThread()V
    .registers 3

    #@0
    .prologue
    .line 815
    const-string v0, "NfcFactoryAdapter"

    #@2
    const-string v1, "StopThread"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 816
    iget-object v0, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mProgressThread:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@9
    invoke-virtual {v0}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->stopThread()V

    #@c
    .line 817
    return-void
.end method

.method public TestReaderMode([B)V
    .registers 7
    .parameter "data"

    #@0
    .prologue
    .line 514
    const/4 v0, 0x0

    #@1
    .line 517
    .local v0, ret:I
    invoke-virtual {p0}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->CheckTestStatus()Z

    #@4
    move-result v1

    #@5
    .line 519
    .local v1, status:Z
    if-nez v1, :cond_8

    #@7
    .line 526
    :goto_7
    return-void

    #@8
    .line 523
    :cond_8
    const-string v2, "NfcFactoryAdapter"

    #@a
    const-string v3, "TestReaderMode::In"

    #@c
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 524
    iget-object v2, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mProgressThread:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@11
    const/4 v3, 0x4

    #@12
    invoke-virtual {v2, v3, p1}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->sendMsg2Thread(I[B)I

    #@15
    move-result v0

    #@16
    .line 525
    const-string v2, "NfcFactoryAdapter"

    #@18
    new-instance v3, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v4, "TestReaderMode called["

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    const-string v4, "]"

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v3

    #@31
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    goto :goto_7
.end method

.method public UpgradeFirmeware([B)V
    .registers 7
    .parameter "data"

    #@0
    .prologue
    .line 545
    const/4 v0, 0x0

    #@1
    .line 548
    .local v0, ret:I
    invoke-virtual {p0}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->CheckTestStatus()Z

    #@4
    move-result v1

    #@5
    .line 550
    .local v1, status:Z
    if-nez v1, :cond_8

    #@7
    .line 557
    :goto_7
    return-void

    #@8
    .line 554
    :cond_8
    const-string v2, "NfcFactoryAdapter"

    #@a
    const-string v3, "UpgradeFirmeware::In"

    #@c
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 555
    iget-object v2, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mProgressThread:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@11
    const/4 v3, 0x6

    #@12
    invoke-virtual {v2, v3, p1}, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;->sendMsg2Thread(I[B)I

    #@15
    move-result v0

    #@16
    .line 556
    const-string v2, "NfcFactoryAdapter"

    #@18
    new-instance v3, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v4, "UpgradeFirmeware called["

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    const-string v4, "]"

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v3

    #@31
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    goto :goto_7
.end method

.method public isReady()Z
    .registers 4

    #@0
    .prologue
    .line 796
    const/4 v0, 0x0

    #@1
    .line 798
    .local v0, result:Z
    iget-object v1, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mNfcAdapterAddon:Lcom/lge/nfcaddon/NfcAdapterAddon;

    #@3
    if-eqz v1, :cond_1d

    #@5
    iget-object v1, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mProgressThread:Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter$ProgressThread;

    #@7
    if-eqz v1, :cond_1d

    #@9
    iget-object v1, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mServiceContext:Lcom/lge/systemservice/core/LGContext;

    #@b
    if-eqz v1, :cond_1d

    #@d
    iget-object v1, p0, Lcom/lge/systemservice/core/nfclgmanager/NfcLgFactoryAdapter;->mIntentFilterAddon:Landroid/content/IntentFilter;

    #@f
    if-eqz v1, :cond_1d

    #@11
    .line 799
    const/4 v0, 0x1

    #@12
    .line 805
    :goto_12
    const/4 v1, 0x1

    #@13
    if-ne v0, v1, :cond_1f

    #@15
    .line 806
    const-string v1, "NfcFactoryAdapter"

    #@17
    const-string v2, "@@isReady::Ready"

    #@19
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 811
    :goto_1c
    return v0

    #@1d
    .line 802
    :cond_1d
    const/4 v0, 0x0

    #@1e
    goto :goto_12

    #@1f
    .line 809
    :cond_1f
    const-string v1, "NfcFactoryAdapter"

    #@21
    const-string v2, "@@isReady::Not Ready"

    #@23
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    goto :goto_1c
.end method
