.class public abstract Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub;
.super Landroid/os/Binder;
.source "IBtLgeExtManager.java"

# interfaces
.implements Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub$Proxy;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 15
    const-string v0, "com.lge.systemservice.core.btlgeextmanager.IBtLgeExtManager"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 23
    if-nez p0, :cond_4

    #@2
    .line 24
    const/4 v0, 0x0

    #@3
    .line 30
    :goto_3
    return-object v0

    #@4
    .line 26
    :cond_4
    const-string v1, "com.lge.systemservice.core.btlgeextmanager.IBtLgeExtManager"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 27
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 28
    check-cast v0, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager;

    #@12
    goto :goto_3

    #@13
    .line 30
    :cond_13
    new-instance v0, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 11
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 38
    sparse-switch p1, :sswitch_data_120

    #@4
    .line 178
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v4

    #@8
    :goto_8
    return v4

    #@9
    .line 42
    :sswitch_9
    const-string v5, "com.lge.systemservice.core.btlgeextmanager.IBtLgeExtManager"

    #@b
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 47
    :sswitch_f
    const-string v5, "com.lge.systemservice.core.btlgeextmanager.IBtLgeExtManager"

    #@11
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 48
    invoke-virtual {p0}, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub;->loadBluetoothAddress()I

    #@17
    move-result v3

    #@18
    .line 49
    .local v3, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1b
    .line 50
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1e
    goto :goto_8

    #@1f
    .line 55
    .end local v3           #_result:I
    :sswitch_1f
    const-string v5, "com.lge.systemservice.core.btlgeextmanager.IBtLgeExtManager"

    #@21
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@24
    .line 57
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@27
    move-result v0

    #@28
    .line 58
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub;->enterBluetoothDUTMode(I)I

    #@2b
    move-result v3

    #@2c
    .line 59
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2f
    .line 60
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@32
    goto :goto_8

    #@33
    .line 65
    .end local v0           #_arg0:I
    .end local v3           #_result:I
    :sswitch_33
    const-string v5, "com.lge.systemservice.core.btlgeextmanager.IBtLgeExtManager"

    #@35
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@38
    .line 66
    invoke-virtual {p0}, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub;->exitBluetoothDUTMode()I

    #@3b
    move-result v3

    #@3c
    .line 67
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3f
    .line 68
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@42
    goto :goto_8

    #@43
    .line 73
    .end local v3           #_result:I
    :sswitch_43
    const-string v5, "com.lge.systemservice.core.btlgeextmanager.IBtLgeExtManager"

    #@45
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@48
    .line 74
    invoke-virtual {p0}, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub;->isBluetoothDUTMode()I

    #@4b
    move-result v3

    #@4c
    .line 75
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4f
    .line 76
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@52
    goto :goto_8

    #@53
    .line 81
    .end local v3           #_result:I
    :sswitch_53
    const-string v5, "com.lge.systemservice.core.btlgeextmanager.IBtLgeExtManager"

    #@55
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@58
    .line 83
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@5b
    move-result v0

    #@5c
    .line 84
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub;->enterBluetoothDUTModeBle(I)I

    #@5f
    move-result v3

    #@60
    .line 85
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@63
    .line 86
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@66
    goto :goto_8

    #@67
    .line 91
    .end local v0           #_arg0:I
    .end local v3           #_result:I
    :sswitch_67
    const-string v5, "com.lge.systemservice.core.btlgeextmanager.IBtLgeExtManager"

    #@69
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6c
    .line 92
    invoke-virtual {p0}, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub;->exitBluetoothDUTModeBle()I

    #@6f
    move-result v3

    #@70
    .line 93
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@73
    .line 94
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@76
    goto :goto_8

    #@77
    .line 99
    .end local v3           #_result:I
    :sswitch_77
    const-string v5, "com.lge.systemservice.core.btlgeextmanager.IBtLgeExtManager"

    #@79
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@7c
    .line 100
    invoke-virtual {p0}, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub;->isBluetoothBleDUTMode()I

    #@7f
    move-result v3

    #@80
    .line 101
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@83
    .line 102
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@86
    goto :goto_8

    #@87
    .line 107
    .end local v3           #_result:I
    :sswitch_87
    const-string v5, "com.lge.systemservice.core.btlgeextmanager.IBtLgeExtManager"

    #@89
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@8c
    .line 108
    invoke-virtual {p0}, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub;->SSPDebugModeEnable()I

    #@8f
    move-result v3

    #@90
    .line 109
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@93
    .line 110
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@96
    goto/16 :goto_8

    #@98
    .line 115
    .end local v3           #_result:I
    :sswitch_98
    const-string v5, "com.lge.systemservice.core.btlgeextmanager.IBtLgeExtManager"

    #@9a
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9d
    .line 116
    invoke-virtual {p0}, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub;->SSPDebugModeDisable()I

    #@a0
    move-result v3

    #@a1
    .line 117
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@a4
    .line 118
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@a7
    goto/16 :goto_8

    #@a9
    .line 123
    .end local v3           #_result:I
    :sswitch_a9
    const-string v5, "com.lge.systemservice.core.btlgeextmanager.IBtLgeExtManager"

    #@ab
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ae
    .line 125
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@b1
    move-result v0

    #@b2
    .line 127
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@b5
    move-result v1

    #@b6
    .line 129
    .local v1, _arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@b9
    move-result v2

    #@ba
    .line 130
    .local v2, _arg2:I
    invoke-virtual {p0, v0, v1, v2}, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub;->startLeTransmitterTest(III)I

    #@bd
    move-result v3

    #@be
    .line 131
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@c1
    .line 132
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@c4
    goto/16 :goto_8

    #@c6
    .line 137
    .end local v0           #_arg0:I
    .end local v1           #_arg1:I
    .end local v2           #_arg2:I
    .end local v3           #_result:I
    :sswitch_c6
    const-string v5, "com.lge.systemservice.core.btlgeextmanager.IBtLgeExtManager"

    #@c8
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@cb
    .line 139
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@ce
    move-result v0

    #@cf
    .line 140
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub;->startLeReceiverTest(I)I

    #@d2
    move-result v3

    #@d3
    .line 141
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@d6
    .line 142
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@d9
    goto/16 :goto_8

    #@db
    .line 147
    .end local v0           #_arg0:I
    .end local v3           #_result:I
    :sswitch_db
    const-string v5, "com.lge.systemservice.core.btlgeextmanager.IBtLgeExtManager"

    #@dd
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@e0
    .line 148
    invoke-virtual {p0}, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub;->endLeTxTest()I

    #@e3
    move-result v3

    #@e4
    .line 149
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@e7
    .line 150
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@ea
    goto/16 :goto_8

    #@ec
    .line 155
    .end local v3           #_result:I
    :sswitch_ec
    const-string v5, "com.lge.systemservice.core.btlgeextmanager.IBtLgeExtManager"

    #@ee
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f1
    .line 156
    invoke-virtual {p0}, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub;->endLeRxTest()I

    #@f4
    move-result v3

    #@f5
    .line 157
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@f8
    .line 158
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@fb
    goto/16 :goto_8

    #@fd
    .line 163
    .end local v3           #_result:I
    :sswitch_fd
    const-string v5, "com.lge.systemservice.core.btlgeextmanager.IBtLgeExtManager"

    #@ff
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@102
    .line 164
    invoke-virtual {p0}, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub;->HCIDumpEnable()I

    #@105
    move-result v3

    #@106
    .line 165
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@109
    .line 166
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@10c
    goto/16 :goto_8

    #@10e
    .line 171
    .end local v3           #_result:I
    :sswitch_10e
    const-string v5, "com.lge.systemservice.core.btlgeextmanager.IBtLgeExtManager"

    #@110
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@113
    .line 172
    invoke-virtual {p0}, Lcom/lge/systemservice/core/btlgeextmanager/IBtLgeExtManager$Stub;->HCIDumpDisable()I

    #@116
    move-result v3

    #@117
    .line 173
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@11a
    .line 174
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@11d
    goto/16 :goto_8

    #@11f
    .line 38
    nop

    #@120
    :sswitch_data_120
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_1f
        0x3 -> :sswitch_33
        0x4 -> :sswitch_43
        0x5 -> :sswitch_53
        0x6 -> :sswitch_67
        0x7 -> :sswitch_77
        0x8 -> :sswitch_87
        0x9 -> :sswitch_98
        0xa -> :sswitch_a9
        0xb -> :sswitch_c6
        0xc -> :sswitch_db
        0xd -> :sswitch_ec
        0xe -> :sswitch_fd
        0xf -> :sswitch_10e
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
