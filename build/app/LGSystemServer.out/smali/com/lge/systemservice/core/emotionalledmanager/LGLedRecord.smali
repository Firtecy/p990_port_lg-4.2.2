.class public Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
.super Ljava/lang/Object;
.source "LGLedRecord.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public color:I

.field public flags:I

.field public mExceptional:Z

.field public mNativeNotification:Z

.field public offMS:I

.field public onMS:I

.field public patternId:I

.field public pkgName:Ljava/lang/String;

.field public priority:I

.field public recordId:I

.field public whichLedPlay:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 442
    new-instance v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord$1;

    #@2
    invoke-direct {v0}, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord$1;-><init>()V

    #@5
    sput-object v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x0

    #@2
    .line 364
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 322
    iput v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->flags:I

    #@7
    .line 324
    iput v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@9
    .line 343
    iput v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->priority:I

    #@b
    .line 345
    iput v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->recordId:I

    #@d
    .line 347
    iput-object v1, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->pkgName:Ljava/lang/String;

    #@f
    .line 353
    iput-boolean v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->mExceptional:Z

    #@11
    .line 359
    iput-boolean v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->mNativeNotification:Z

    #@13
    .line 361
    iput v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->whichLedPlay:I

    #@15
    .line 365
    iput v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->flags:I

    #@17
    .line 366
    iput v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@19
    .line 367
    iput v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->color:I

    #@1b
    .line 368
    iput v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->onMS:I

    #@1d
    .line 369
    iput v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->offMS:I

    #@1f
    .line 370
    iput v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->priority:I

    #@21
    .line 371
    iput v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->recordId:I

    #@23
    .line 372
    iput-object v1, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->pkgName:Ljava/lang/String;

    #@25
    .line 373
    iput-boolean v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->mExceptional:Z

    #@27
    .line 374
    iput-boolean v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->mNativeNotification:Z

    #@29
    .line 375
    iput v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->whichLedPlay:I

    #@2b
    .line 376
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 5
    .parameter "parcel"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 378
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 322
    iput v2, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->flags:I

    #@7
    .line 324
    iput v2, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@9
    .line 343
    iput v2, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->priority:I

    #@b
    .line 345
    iput v2, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->recordId:I

    #@d
    .line 347
    const/4 v0, 0x0

    #@e
    iput-object v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->pkgName:Ljava/lang/String;

    #@10
    .line 353
    iput-boolean v2, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->mExceptional:Z

    #@12
    .line 359
    iput-boolean v2, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->mNativeNotification:Z

    #@14
    .line 361
    iput v2, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->whichLedPlay:I

    #@16
    .line 379
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@19
    move-result v0

    #@1a
    iput v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->flags:I

    #@1c
    .line 380
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1f
    move-result v0

    #@20
    iput v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@22
    .line 381
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@25
    move-result v0

    #@26
    iput v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->color:I

    #@28
    .line 382
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@2b
    move-result v0

    #@2c
    iput v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->onMS:I

    #@2e
    .line 383
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@31
    move-result v0

    #@32
    iput v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->offMS:I

    #@34
    .line 384
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@37
    move-result v0

    #@38
    iput v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->priority:I

    #@3a
    .line 385
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3d
    move-result v0

    #@3e
    iput v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->recordId:I

    #@40
    .line 386
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@43
    move-result-object v0

    #@44
    iput-object v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->pkgName:Ljava/lang/String;

    #@46
    .line 387
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@49
    move-result v0

    #@4a
    if-ne v0, v1, :cond_5e

    #@4c
    move v0, v1

    #@4d
    :goto_4d
    iput-boolean v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->mExceptional:Z

    #@4f
    .line 388
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@52
    move-result v0

    #@53
    if-ne v0, v1, :cond_60

    #@55
    :goto_55
    iput-boolean v1, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->mNativeNotification:Z

    #@57
    .line 389
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@5a
    move-result v0

    #@5b
    iput v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->whichLedPlay:I

    #@5d
    .line 390
    return-void

    #@5e
    :cond_5e
    move v0, v2

    #@5f
    .line 387
    goto :goto_4d

    #@60
    :cond_60
    move v1, v2

    #@61
    .line 388
    goto :goto_55
.end method


# virtual methods
.method public clone()Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
    .registers 3

    #@0
    .prologue
    .line 394
    new-instance v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@2
    invoke-direct {v0}, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;-><init>()V

    #@5
    .line 395
    .local v0, that:Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;
    iget v1, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->flags:I

    #@7
    iput v1, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->flags:I

    #@9
    .line 396
    iget v1, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@b
    iput v1, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@d
    .line 397
    iget v1, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->color:I

    #@f
    iput v1, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->color:I

    #@11
    .line 398
    iget v1, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->onMS:I

    #@13
    iput v1, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->onMS:I

    #@15
    .line 399
    iget v1, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->offMS:I

    #@17
    iput v1, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->offMS:I

    #@19
    .line 400
    iget v1, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->priority:I

    #@1b
    iput v1, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->priority:I

    #@1d
    .line 401
    iget v1, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->recordId:I

    #@1f
    iput v1, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->recordId:I

    #@21
    .line 402
    iget-object v1, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->pkgName:Ljava/lang/String;

    #@23
    iput-object v1, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->pkgName:Ljava/lang/String;

    #@25
    .line 403
    iget-boolean v1, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->mExceptional:Z

    #@27
    iput-boolean v1, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->mExceptional:Z

    #@29
    .line 404
    iget-boolean v1, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->mNativeNotification:Z

    #@2b
    iput-boolean v1, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->mNativeNotification:Z

    #@2d
    .line 405
    iget v1, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->whichLedPlay:I

    #@2f
    iput v1, v0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->whichLedPlay:I

    #@31
    .line 407
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->clone()Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 425
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;)Z
    .registers 4
    .parameter "r"

    #@0
    .prologue
    .line 411
    iget v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->flags:I

    #@2
    iget v1, p1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->flags:I

    #@4
    if-ne v0, v1, :cond_48

    #@6
    iget v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@8
    iget v1, p1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@a
    if-ne v0, v1, :cond_48

    #@c
    iget v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->color:I

    #@e
    iget v1, p1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->color:I

    #@10
    if-ne v0, v1, :cond_48

    #@12
    iget v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->onMS:I

    #@14
    iget v1, p1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->onMS:I

    #@16
    if-ne v0, v1, :cond_48

    #@18
    iget v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->offMS:I

    #@1a
    iget v1, p1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->offMS:I

    #@1c
    if-ne v0, v1, :cond_48

    #@1e
    iget v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->priority:I

    #@20
    iget v1, p1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->priority:I

    #@22
    if-ne v0, v1, :cond_48

    #@24
    iget v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->recordId:I

    #@26
    iget v1, p1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->recordId:I

    #@28
    if-ne v0, v1, :cond_48

    #@2a
    iget-object v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->pkgName:Ljava/lang/String;

    #@2c
    iget-object v1, p1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->pkgName:Ljava/lang/String;

    #@2e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@31
    move-result v0

    #@32
    if-eqz v0, :cond_48

    #@34
    iget-boolean v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->mExceptional:Z

    #@36
    iget-boolean v1, p1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->mExceptional:Z

    #@38
    if-ne v0, v1, :cond_48

    #@3a
    iget-boolean v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->mNativeNotification:Z

    #@3c
    iget-boolean v1, p1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->mNativeNotification:Z

    #@3e
    if-ne v0, v1, :cond_48

    #@40
    iget v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->whichLedPlay:I

    #@42
    iget v1, p1, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->whichLedPlay:I

    #@44
    if-ne v0, v1, :cond_48

    #@46
    const/4 v0, 0x1

    #@47
    :goto_47
    return v0

    #@48
    :cond_48
    const/4 v0, 0x0

    #@49
    goto :goto_47
.end method

.method public final toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 455
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "LGLedRecord{"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@e
    move-result v1

    #@f
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    const-string v1, " flags="

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    iget v1, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->flags:I

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    const-string v1, " patternId="

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    iget v1, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    const-string v1, " color="

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    iget v1, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->color:I

    #@37
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@3a
    move-result-object v1

    #@3b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v0

    #@3f
    const-string v1, " priority="

    #@41
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v0

    #@45
    iget v1, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->priority:I

    #@47
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v0

    #@4b
    const-string v1, " recordId="

    #@4d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v0

    #@51
    iget v1, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->recordId:I

    #@53
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@56
    move-result-object v0

    #@57
    const-string v1, " pkg="

    #@59
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v0

    #@5d
    iget-object v1, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->pkgName:Ljava/lang/String;

    #@5f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v0

    #@63
    const-string v1, " mExceptional="

    #@65
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v0

    #@69
    iget-boolean v1, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->mExceptional:Z

    #@6b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v0

    #@6f
    const-string v1, " mNativeNotification="

    #@71
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v0

    #@75
    iget-boolean v1, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->mNativeNotification:Z

    #@77
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v0

    #@7b
    const-string v1, " whichLedPlay="

    #@7d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v0

    #@81
    iget v1, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->whichLedPlay:I

    #@83
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@86
    move-result-object v0

    #@87
    const-string v1, "}"

    #@89
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v0

    #@8d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@90
    move-result-object v0

    #@91
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter "parcel"
    .parameter "flags"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 429
    iget v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->flags:I

    #@4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@7
    .line 430
    iget v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->patternId:I

    #@9
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 431
    iget v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->color:I

    #@e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 432
    iget v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->onMS:I

    #@13
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 433
    iget v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->offMS:I

    #@18
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1b
    .line 434
    iget v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->priority:I

    #@1d
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@20
    .line 435
    iget v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->recordId:I

    #@22
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@25
    .line 436
    iget-object v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->pkgName:Ljava/lang/String;

    #@27
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@2a
    .line 437
    iget-boolean v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->mExceptional:Z

    #@2c
    if-eqz v0, :cond_3f

    #@2e
    move v0, v1

    #@2f
    :goto_2f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@32
    .line 438
    iget-boolean v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->mNativeNotification:Z

    #@34
    if-eqz v0, :cond_41

    #@36
    :goto_36
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@39
    .line 439
    iget v0, p0, Lcom/lge/systemservice/core/emotionalledmanager/LGLedRecord;->whichLedPlay:I

    #@3b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@3e
    .line 440
    return-void

    #@3f
    :cond_3f
    move v0, v2

    #@40
    .line 437
    goto :goto_2f

    #@41
    :cond_41
    move v1, v2

    #@42
    .line 438
    goto :goto_36
.end method
