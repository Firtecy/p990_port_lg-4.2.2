.class public Lcom/lge/systemservice/core/emotionalledmanager/EmotionalLedManager;
.super Ljava/lang/Object;
.source "EmotionalLedManager.java"


# static fields
.field private static final DEBUG:Z

.field private static gDefault:Lcom/lge/systemservice/core/LGServiceSingleton;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/lge/systemservice/core/LGServiceSingleton",
            "<",
            "Lcom/lge/systemservice/core/emotionalledmanager/IEmotionalLedManager;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 22
    sget-object v0, Landroid/os/Build;->TYPE:Ljava/lang/String;

    #@2
    const-string v1, "user"

    #@4
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_17

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    sput-boolean v0, Lcom/lge/systemservice/core/emotionalledmanager/EmotionalLedManager;->DEBUG:Z

    #@d
    .line 28
    new-instance v0, Lcom/lge/systemservice/core/emotionalledmanager/EmotionalLedManager$1;

    #@f
    const-string v1, "emotionled"

    #@11
    invoke-direct {v0, v1}, Lcom/lge/systemservice/core/emotionalledmanager/EmotionalLedManager$1;-><init>(Ljava/lang/String;)V

    #@14
    sput-object v0, Lcom/lge/systemservice/core/emotionalledmanager/EmotionalLedManager;->gDefault:Lcom/lge/systemservice/core/LGServiceSingleton;

    #@16
    return-void

    #@17
    .line 22
    :cond_17
    const/4 v0, 0x0

    #@18
    goto :goto_b
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 36
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 37
    iput-object p1, p0, Lcom/lge/systemservice/core/emotionalledmanager/EmotionalLedManager;->mContext:Landroid/content/Context;

    #@5
    .line 38
    return-void
.end method
