.class Lcom/lge/systemservice/core/watchnetstoragemanager/IWatchNetStorageManager$Stub$Proxy;
.super Ljava/lang/Object;
.source "IWatchNetStorageManager.java"

# interfaces
.implements Lcom/lge/systemservice/core/watchnetstoragemanager/IWatchNetStorageManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/core/watchnetstoragemanager/IWatchNetStorageManager$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 115
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 116
    iput-object p1, p0, Lcom/lge/systemservice/core/watchnetstoragemanager/IWatchNetStorageManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 117
    return-void
.end method


# virtual methods
.method public addNetStorage(Ljava/lang/String;)Z
    .registers 8
    .parameter "mountPoint"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 147
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 148
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 151
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.lge.systemservice.core.watchnetstoragemanager.IWatchNetStorageManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 152
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@11
    .line 153
    iget-object v3, p0, Lcom/lge/systemservice/core/watchnetstoragemanager/IWatchNetStorageManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@13
    const/4 v4, 0x2

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 154
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 155
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1e
    .catchall {:try_start_9 .. :try_end_1e} :catchall_29

    #@1e
    move-result v3

    #@1f
    if-eqz v3, :cond_22

    #@21
    const/4 v2, 0x1

    #@22
    .line 158
    .local v2, _result:Z
    :cond_22
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 159
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 161
    return v2

    #@29
    .line 158
    .end local v2           #_result:Z
    :catchall_29
    move-exception v3

    #@2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 159
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    throw v3
.end method

.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 120
    iget-object v0, p0, Lcom/lge/systemservice/core/watchnetstoragemanager/IWatchNetStorageManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public checkFirstRunAfterBoot()Z
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 219
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 220
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 223
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.lge.systemservice.core.watchnetstoragemanager.IWatchNetStorageManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 224
    iget-object v3, p0, Lcom/lge/systemservice/core/watchnetstoragemanager/IWatchNetStorageManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@10
    const/4 v4, 0x6

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 225
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 226
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1b
    .catchall {:try_start_9 .. :try_end_1b} :catchall_26

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_1f

    #@1e
    const/4 v2, 0x1

    #@1f
    .line 229
    .local v2, _result:Z
    :cond_1f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 230
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 232
    return v2

    #@26
    .line 229
    .end local v2           #_result:Z
    :catchall_26
    move-exception v3

    #@27
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 230
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    throw v3
.end method

.method public removeNetStorage(Ljava/lang/String;)Z
    .registers 8
    .parameter "mountPoint"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 165
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 166
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 169
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.lge.systemservice.core.watchnetstoragemanager.IWatchNetStorageManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 170
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@11
    .line 171
    iget-object v3, p0, Lcom/lge/systemservice/core/watchnetstoragemanager/IWatchNetStorageManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@13
    const/4 v4, 0x3

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 172
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 173
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1e
    .catchall {:try_start_9 .. :try_end_1e} :catchall_29

    #@1e
    move-result v3

    #@1f
    if-eqz v3, :cond_22

    #@21
    const/4 v2, 0x1

    #@22
    .line 176
    .local v2, _result:Z
    :cond_22
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 177
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 179
    return v2

    #@29
    .line 176
    .end local v2           #_result:Z
    :catchall_29
    move-exception v3

    #@2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 177
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    throw v3
.end method

.method public requestMediaScanFile(Ljava/lang/String;I)Z
    .registers 10
    .parameter "path"
    .parameter "timeout"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 128
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 129
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 132
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "com.lge.systemservice.core.watchnetstoragemanager.IWatchNetStorageManager"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 133
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@12
    .line 134
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 135
    iget-object v4, p0, Lcom/lge/systemservice/core/watchnetstoragemanager/IWatchNetStorageManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@17
    const/4 v5, 0x1

    #@18
    const/4 v6, 0x0

    #@19
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1c
    .line 136
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1f
    .line 137
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_22
    .catchall {:try_start_a .. :try_end_22} :catchall_2e

    #@22
    move-result v4

    #@23
    if-eqz v4, :cond_2c

    #@25
    .line 140
    .local v2, _result:Z
    :goto_25
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 141
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 143
    return v2

    #@2c
    .end local v2           #_result:Z
    :cond_2c
    move v2, v3

    #@2d
    .line 137
    goto :goto_25

    #@2e
    .line 140
    :catchall_2e
    move-exception v3

    #@2f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 141
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@35
    throw v3
.end method

.method public startThread(I)Z
    .registers 8
    .parameter "session"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 183
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 184
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 187
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.lge.systemservice.core.watchnetstoragemanager.IWatchNetStorageManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 188
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 189
    iget-object v3, p0, Lcom/lge/systemservice/core/watchnetstoragemanager/IWatchNetStorageManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@13
    const/4 v4, 0x4

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 190
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 191
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1e
    .catchall {:try_start_9 .. :try_end_1e} :catchall_29

    #@1e
    move-result v3

    #@1f
    if-eqz v3, :cond_22

    #@21
    const/4 v2, 0x1

    #@22
    .line 194
    .local v2, _result:Z
    :cond_22
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 195
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 197
    return v2

    #@29
    .line 194
    .end local v2           #_result:Z
    :catchall_29
    move-exception v3

    #@2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 195
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    throw v3
.end method

.method public stopThread(I)Z
    .registers 8
    .parameter "session"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 201
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 202
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 205
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.lge.systemservice.core.watchnetstoragemanager.IWatchNetStorageManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 206
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 207
    iget-object v3, p0, Lcom/lge/systemservice/core/watchnetstoragemanager/IWatchNetStorageManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@13
    const/4 v4, 0x5

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 208
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 209
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1e
    .catchall {:try_start_9 .. :try_end_1e} :catchall_29

    #@1e
    move-result v3

    #@1f
    if-eqz v3, :cond_22

    #@21
    const/4 v2, 0x1

    #@22
    .line 212
    .local v2, _result:Z
    :cond_22
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 213
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 215
    return v2

    #@29
    .line 212
    .end local v2           #_result:Z
    :catchall_29
    move-exception v3

    #@2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 213
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    throw v3
.end method
