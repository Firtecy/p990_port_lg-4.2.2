.class public Lcom/lge/systemservice/core/lgsdencmanager/LGSDEncManager;
.super Ljava/lang/Object;
.source "LGSDEncManager.java"


# static fields
.field private static gDefault:Lcom/lge/systemservice/core/LGServiceSingleton;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/lge/systemservice/core/LGServiceSingleton",
            "<",
            "Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager;",
            ">;"
        }
    .end annotation
.end field

.field private static mService:Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 24
    new-instance v0, Lcom/lge/systemservice/core/lgsdencmanager/LGSDEncManager$1;

    #@2
    const-string v1, "lgsdencryption"

    #@4
    invoke-direct {v0, v1}, Lcom/lge/systemservice/core/lgsdencmanager/LGSDEncManager$1;-><init>(Ljava/lang/String;)V

    #@7
    sput-object v0, Lcom/lge/systemservice/core/lgsdencmanager/LGSDEncManager;->gDefault:Lcom/lge/systemservice/core/LGServiceSingleton;

    #@9
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 32
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 33
    invoke-static {}, Lcom/lge/systemservice/core/lgsdencmanager/LGSDEncManager;->getService()Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager;

    #@6
    .line 34
    return-void
.end method

.method private static getService()Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager;
    .registers 1

    #@0
    .prologue
    .line 38
    sget-object v0, Lcom/lge/systemservice/core/lgsdencmanager/LGSDEncManager;->gDefault:Lcom/lge/systemservice/core/LGServiceSingleton;

    #@2
    invoke-virtual {v0}, Lcom/lge/systemservice/core/LGServiceSingleton;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager;

    #@8
    sput-object v0, Lcom/lge/systemservice/core/lgsdencmanager/LGSDEncManager;->mService:Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager;

    #@a
    .line 39
    sget-object v0, Lcom/lge/systemservice/core/lgsdencmanager/LGSDEncManager;->mService:Lcom/lge/systemservice/core/lgsdencmanager/ILGSDEncManager;

    #@c
    return-object v0
.end method
