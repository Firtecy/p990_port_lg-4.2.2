.class public Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager$FmReceiverEventHandler;
.super Ljava/lang/Object;
.source "FmRadioManager.java"

# interfaces
.implements Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "FmReceiverEventHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;


# direct methods
.method protected constructor <init>(Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 210
    iput-object p1, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager$FmReceiverEventHandler;->this$0:Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;

    #@2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onAudioModeEvent(I)V
    .registers 2
    .parameter "audioMode"

    #@0
    .prologue
    .line 233
    return-void
.end method

.method public onAudioPathEvent(I)V
    .registers 5
    .parameter "audioPath"

    #@0
    .prologue
    .line 228
    const-string v0, "FmRadioManager"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "onAudioPathEvent : audioPath="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 229
    iget-object v0, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager$FmReceiverEventHandler;->this$0:Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;

    #@1a
    const/16 v1, 0x8

    #@1c
    #setter for: Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->nState:I
    invoke-static {v0, v1}, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->access$002(Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;I)I

    #@1f
    .line 230
    iget-object v0, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager$FmReceiverEventHandler;->this$0:Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;

    #@21
    #getter for: Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->mReceiver:Lcom/broadcom/fm/fmreceiver/FmProxy;
    invoke-static {v0}, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->access$100(Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;)Lcom/broadcom/fm/fmreceiver/FmProxy;

    #@24
    move-result-object v0

    #@25
    const/16 v1, 0x222e

    #@27
    invoke-virtual {v0, v1}, Lcom/broadcom/fm/fmreceiver/FmProxy;->tuneRadio(I)I

    #@2a
    .line 231
    return-void
.end method

.method public onEstimateNoiseFloorLevelEvent(I)V
    .registers 2
    .parameter "nfl"

    #@0
    .prologue
    .line 224
    return-void
.end method

.method public onLiveAudioQualityEvent(II)V
    .registers 3
    .parameter "rssi"
    .parameter "snr"

    #@0
    .prologue
    .line 222
    return-void
.end method

.method public onRdsDataEvent(IILjava/lang/String;)V
    .registers 4
    .parameter "rdsDataType"
    .parameter "rdsIndex"
    .parameter "rdsText"

    #@0
    .prologue
    .line 236
    return-void
.end method

.method public onRdsModeEvent(II)V
    .registers 6
    .parameter "rdsMode"
    .parameter "alternateFreqHopEnabled"

    #@0
    .prologue
    .line 238
    const-string v0, "FmRadioManager"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "onRdsModeEvent : rdsMode="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, "alternateFreqHopEnabled="

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 239
    iget-object v0, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager$FmReceiverEventHandler;->this$0:Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;

    #@24
    const/16 v1, 0xa

    #@26
    #setter for: Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->nState:I
    invoke-static {v0, v1}, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->access$002(Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;I)I

    #@29
    .line 240
    iget-object v0, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager$FmReceiverEventHandler;->this$0:Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;

    #@2b
    iget-object v1, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager$FmReceiverEventHandler;->this$0:Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;

    #@2d
    iget-object v1, v1, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->mContext:Landroid/content/Context;

    #@2f
    invoke-virtual {v0, v1}, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->getAudioManager(Landroid/content/Context;)Landroid/media/AudioManager;

    #@32
    move-result-object v0

    #@33
    const-string v1, "fm_audio=active;fm_speakerphone=off;fm_mute=loud"

    #@35
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    #@38
    .line 241
    return-void
.end method

.method public onSeekCompleteEvent(IIIZ)V
    .registers 5
    .parameter "freq"
    .parameter "rssi"
    .parameter "snr"
    .parameter "seeksuccess"

    #@0
    .prologue
    .line 244
    return-void
.end method

.method public onStatusEvent(IIIZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 14
    .parameter "freq"
    .parameter "rssi"
    .parameter "snr"
    .parameter "radioIsOn"
    .parameter "rdsProgramType"
    .parameter "rdsProgramService"
    .parameter "rdsRadioText"
    .parameter "rdsProgramTypeName"
    .parameter "isMute"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 249
    const-string v0, "FmRadioManager"

    #@3
    new-instance v1, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v2, "onStatusEvent (nState="

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    iget-object v2, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager$FmReceiverEventHandler;->this$0:Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;

    #@10
    #getter for: Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->nState:I
    invoke-static {v2}, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->access$000(Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;)I

    #@13
    move-result v2

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    const-string v2, ")(freq="

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    const-string v2, ",rssi="

    #@24
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v1

    #@28
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v1

    #@2c
    const-string v2, ",snr="

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35
    move-result-object v1

    #@36
    const-string v2, ",radioIsOn="

    #@38
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v1

    #@3c
    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v1

    #@40
    const-string v2, ",rdsProgramType="

    #@42
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v1

    #@46
    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@49
    move-result-object v1

    #@4a
    const-string v2, ",rdsProgramService="

    #@4c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v1

    #@50
    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v1

    #@54
    const-string v2, ",rdsRadioText="

    #@56
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v1

    #@5a
    invoke-virtual {v1, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v1

    #@5e
    const-string v2, ",rdsProgramTypeName="

    #@60
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v1

    #@64
    invoke-virtual {v1, p8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v1

    #@68
    const-string v2, ",isMute="

    #@6a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v1

    #@6e
    invoke-virtual {v1, p9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@71
    move-result-object v1

    #@72
    const-string v2, ")"

    #@74
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v1

    #@78
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v1

    #@7c
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7f
    .line 254
    if-ne p4, v3, :cond_ac

    #@81
    .line 255
    iget-object v0, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager$FmReceiverEventHandler;->this$0:Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;

    #@83
    #getter for: Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->nState:I
    invoke-static {v0}, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->access$000(Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;)I

    #@86
    move-result v0

    #@87
    if-ne v0, v3, :cond_9a

    #@89
    .line 256
    iget-object v0, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager$FmReceiverEventHandler;->this$0:Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;

    #@8b
    const/4 v1, 0x2

    #@8c
    #setter for: Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->nState:I
    invoke-static {v0, v1}, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->access$002(Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;I)I

    #@8f
    .line 257
    iget-object v0, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager$FmReceiverEventHandler;->this$0:Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;

    #@91
    #getter for: Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->mReceiver:Lcom/broadcom/fm/fmreceiver/FmProxy;
    invoke-static {v0}, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->access$100(Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;)Lcom/broadcom/fm/fmreceiver/FmProxy;

    #@94
    move-result-object v0

    #@95
    const/4 v1, 0x3

    #@96
    invoke-virtual {v0, v1}, Lcom/broadcom/fm/fmreceiver/FmProxy;->setAudioPath(I)I

    #@99
    .line 265
    :cond_99
    :goto_99
    return-void

    #@9a
    .line 259
    :cond_9a
    iget-object v0, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager$FmReceiverEventHandler;->this$0:Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;

    #@9c
    const/4 v1, 0x4

    #@9d
    #setter for: Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->nState:I
    invoke-static {v0, v1}, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->access$002(Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;I)I

    #@a0
    .line 260
    iget-object v0, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager$FmReceiverEventHandler;->this$0:Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;

    #@a2
    #getter for: Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->mReceiver:Lcom/broadcom/fm/fmreceiver/FmProxy;
    invoke-static {v0}, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->access$100(Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;)Lcom/broadcom/fm/fmreceiver/FmProxy;

    #@a5
    move-result-object v0

    #@a6
    const/16 v1, 0xff

    #@a8
    invoke-virtual {v0, v1}, Lcom/broadcom/fm/fmreceiver/FmProxy;->setFMVolume(I)I

    #@ab
    goto :goto_99

    #@ac
    .line 262
    :cond_ac
    if-nez p4, :cond_99

    #@ae
    .line 263
    iget-object v0, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager$FmReceiverEventHandler;->this$0:Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;

    #@b0
    const/16 v1, 0xb

    #@b2
    #setter for: Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->nState:I
    invoke-static {v0, v1}, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->access$002(Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;I)I

    #@b5
    goto :goto_99
.end method

.method public onVolumeEvent(II)V
    .registers 7
    .parameter "status"
    .parameter "volume"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 212
    const-string v0, "FmRadioManager"

    #@3
    new-instance v1, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v2, "onVolumeEvent : status="

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    const-string v2, ", volume="

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 213
    iget-object v0, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager$FmReceiverEventHandler;->this$0:Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;

    #@25
    const/4 v1, 0x6

    #@26
    #setter for: Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->nState:I
    invoke-static {v0, v1}, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->access$002(Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;I)I

    #@29
    .line 215
    iget-object v0, p0, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager$FmReceiverEventHandler;->this$0:Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;

    #@2b
    #getter for: Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->mReceiver:Lcom/broadcom/fm/fmreceiver/FmProxy;
    invoke-static {v0}, Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;->access$100(Lcom/lge/systemservice/core/fmradiomanager/FmRadioManager;)Lcom/broadcom/fm/fmreceiver/FmProxy;

    #@2e
    move-result-object v0

    #@2f
    const/4 v1, 0x1

    #@30
    const/16 v2, 0x4c

    #@32
    invoke-virtual {v0, v1, v2, v3, v3}, Lcom/broadcom/fm/fmreceiver/FmProxy;->setRdsMode(IIII)I

    #@35
    .line 220
    return-void
.end method

.method public onWorldRegionEvent(I)V
    .registers 2
    .parameter "worldRegion"

    #@0
    .prologue
    .line 226
    return-void
.end method
