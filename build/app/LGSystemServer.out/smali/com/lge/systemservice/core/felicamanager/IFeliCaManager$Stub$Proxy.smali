.class Lcom/lge/systemservice/core/felicamanager/IFeliCaManager$Stub$Proxy;
.super Ljava/lang/Object;
.source "IFeliCaManager.java"

# interfaces
.implements Lcom/lge/systemservice/core/felicamanager/IFeliCaManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/systemservice/core/felicamanager/IFeliCaManager$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 198
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 199
    iput-object p1, p0, Lcom/lge/systemservice/core/felicamanager/IFeliCaManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 200
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 203
    iget-object v0, p0, Lcom/lge/systemservice/core/felicamanager/IFeliCaManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public cmdEXTIDM([B)Z
    .registers 9
    .parameter "idm"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 211
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 212
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 215
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "com.lge.systemservice.core.felicamanager.IFeliCaManager"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 216
    if-nez p1, :cond_2f

    #@11
    .line 217
    const/4 v4, -0x1

    #@12
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 222
    :goto_15
    iget-object v4, p0, Lcom/lge/systemservice/core/felicamanager/IFeliCaManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@17
    const/4 v5, 0x1

    #@18
    const/4 v6, 0x0

    #@19
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1c
    .line 223
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1f
    .line 224
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@22
    move-result v4

    #@23
    if-eqz v4, :cond_3c

    #@25
    .line 225
    .local v2, _result:Z
    :goto_25
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->readByteArray([B)V
    :try_end_28
    .catchall {:try_start_a .. :try_end_28} :catchall_34

    #@28
    .line 228
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 229
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 231
    return v2

    #@2f
    .line 220
    .end local v2           #_result:Z
    :cond_2f
    :try_start_2f
    array-length v4, p1

    #@30
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_33
    .catchall {:try_start_2f .. :try_end_33} :catchall_34

    #@33
    goto :goto_15

    #@34
    .line 228
    :catchall_34
    move-exception v3

    #@35
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@38
    .line 229
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3b
    throw v3

    #@3c
    :cond_3c
    move v2, v3

    #@3d
    .line 224
    goto :goto_25
.end method

.method public cmdFreqCalRange([Ljava/lang/String;)Z
    .registers 8
    .parameter "response"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 384
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 385
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 388
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.lge.systemservice.core.felicamanager.IFeliCaManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 389
    if-nez p1, :cond_30

    #@10
    .line 390
    const/4 v3, -0x1

    #@11
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 395
    :goto_14
    iget-object v3, p0, Lcom/lge/systemservice/core/felicamanager/IFeliCaManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/16 v4, 0x9

    #@18
    const/4 v5, 0x0

    #@19
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1c
    .line 396
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1f
    .line 397
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@22
    move-result v3

    #@23
    if-eqz v3, :cond_26

    #@25
    const/4 v2, 0x1

    #@26
    .line 398
    .local v2, _result:Z
    :cond_26
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->readStringArray([Ljava/lang/String;)V
    :try_end_29
    .catchall {:try_start_9 .. :try_end_29} :catchall_35

    #@29
    .line 401
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 402
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 404
    return v2

    #@30
    .line 393
    .end local v2           #_result:Z
    :cond_30
    :try_start_30
    array-length v3, p1

    #@31
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_34
    .catchall {:try_start_30 .. :try_end_34} :catchall_35

    #@34
    goto :goto_14

    #@35
    .line 401
    :catchall_35
    move-exception v3

    #@36
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@39
    .line 402
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3c
    throw v3
.end method

.method public cmdFreqCalRead([Ljava/lang/String;)Z
    .registers 8
    .parameter "response"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 360
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 361
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 364
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.lge.systemservice.core.felicamanager.IFeliCaManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 365
    if-nez p1, :cond_30

    #@10
    .line 366
    const/4 v3, -0x1

    #@11
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 371
    :goto_14
    iget-object v3, p0, Lcom/lge/systemservice/core/felicamanager/IFeliCaManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/16 v4, 0x8

    #@18
    const/4 v5, 0x0

    #@19
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1c
    .line 372
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1f
    .line 373
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@22
    move-result v3

    #@23
    if-eqz v3, :cond_26

    #@25
    const/4 v2, 0x1

    #@26
    .line 374
    .local v2, _result:Z
    :cond_26
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->readStringArray([Ljava/lang/String;)V
    :try_end_29
    .catchall {:try_start_9 .. :try_end_29} :catchall_35

    #@29
    .line 377
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 378
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 380
    return v2

    #@30
    .line 369
    .end local v2           #_result:Z
    :cond_30
    :try_start_30
    array-length v3, p1

    #@31
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_34
    .catchall {:try_start_30 .. :try_end_34} :catchall_35

    #@34
    goto :goto_14

    #@35
    .line 377
    :catchall_35
    move-exception v3

    #@36
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@39
    .line 378
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3c
    throw v3
.end method

.method public cmdFreqCalWrite(F)Z
    .registers 8
    .parameter "freq"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 342
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 343
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 346
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.lge.systemservice.core.felicamanager.IFeliCaManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 347
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeFloat(F)V

    #@11
    .line 348
    iget-object v3, p0, Lcom/lge/systemservice/core/felicamanager/IFeliCaManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@13
    const/4 v4, 0x7

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 349
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 350
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1e
    .catchall {:try_start_9 .. :try_end_1e} :catchall_29

    #@1e
    move-result v3

    #@1f
    if-eqz v3, :cond_22

    #@21
    const/4 v2, 0x1

    #@22
    .line 353
    .local v2, _result:Z
    :cond_22
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 354
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 356
    return v2

    #@29
    .line 353
    .end local v2           #_result:Z
    :catchall_29
    move-exception v3

    #@2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 354
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    throw v3
.end method

.method public cmdIDM([Ljava/lang/String;)Z
    .registers 8
    .parameter "response"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 235
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 236
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 239
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.lge.systemservice.core.felicamanager.IFeliCaManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 240
    if-nez p1, :cond_2f

    #@10
    .line 241
    const/4 v3, -0x1

    #@11
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 246
    :goto_14
    iget-object v3, p0, Lcom/lge/systemservice/core/felicamanager/IFeliCaManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/4 v4, 0x2

    #@17
    const/4 v5, 0x0

    #@18
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 247
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1e
    .line 248
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@21
    move-result v3

    #@22
    if-eqz v3, :cond_25

    #@24
    const/4 v2, 0x1

    #@25
    .line 249
    .local v2, _result:Z
    :cond_25
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->readStringArray([Ljava/lang/String;)V
    :try_end_28
    .catchall {:try_start_9 .. :try_end_28} :catchall_34

    #@28
    .line 252
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 253
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 255
    return v2

    #@2f
    .line 244
    .end local v2           #_result:Z
    :cond_2f
    :try_start_2f
    array-length v3, p1

    #@30
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_33
    .catchall {:try_start_2f .. :try_end_33} :catchall_34

    #@33
    goto :goto_14

    #@34
    .line 252
    :catchall_34
    move-exception v3

    #@35
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@38
    .line 253
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3b
    throw v3
.end method

.method public cmdRFIDCK()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 259
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 260
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 263
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.systemservice.core.felicamanager.IFeliCaManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 264
    iget-object v3, p0, Lcom/lge/systemservice/core/felicamanager/IFeliCaManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x3

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 265
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 266
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_22

    #@1a
    move-result v2

    #@1b
    .line 269
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 270
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 272
    return v2

    #@22
    .line 269
    .end local v2           #_result:I
    :catchall_22
    move-exception v3

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 270
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v3
.end method

.method public cmdRFRegCalCheck()Z
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 425
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 426
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 429
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.lge.systemservice.core.felicamanager.IFeliCaManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 430
    iget-object v3, p0, Lcom/lge/systemservice/core/felicamanager/IFeliCaManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@10
    const/16 v4, 0xb

    #@12
    const/4 v5, 0x0

    #@13
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@16
    .line 431
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@19
    .line 432
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1c
    .catchall {:try_start_9 .. :try_end_1c} :catchall_27

    #@1c
    move-result v3

    #@1d
    if-eqz v3, :cond_20

    #@1f
    const/4 v2, 0x1

    #@20
    .line 435
    .local v2, _result:Z
    :cond_20
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 436
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 438
    return v2

    #@27
    .line 435
    .end local v2           #_result:Z
    :catchall_27
    move-exception v3

    #@28
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 436
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    throw v3
.end method

.method public cmdRFRegCalLoad()Z
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 408
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 409
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 412
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.lge.systemservice.core.felicamanager.IFeliCaManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 413
    iget-object v3, p0, Lcom/lge/systemservice/core/felicamanager/IFeliCaManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@10
    const/16 v4, 0xa

    #@12
    const/4 v5, 0x0

    #@13
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@16
    .line 414
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@19
    .line 415
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1c
    .catchall {:try_start_9 .. :try_end_1c} :catchall_27

    #@1c
    move-result v3

    #@1d
    if-eqz v3, :cond_20

    #@1f
    const/4 v2, 0x1

    #@20
    .line 418
    .local v2, _result:Z
    :cond_20
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 419
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 421
    return v2

    #@27
    .line 418
    .end local v2           #_result:Z
    :catchall_27
    move-exception v3

    #@28
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 419
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    throw v3
.end method

.method public cmdSwitchRange([Ljava/lang/String;)Z
    .registers 8
    .parameter "response"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 276
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 277
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 280
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.lge.systemservice.core.felicamanager.IFeliCaManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 281
    if-nez p1, :cond_2f

    #@10
    .line 282
    const/4 v3, -0x1

    #@11
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 287
    :goto_14
    iget-object v3, p0, Lcom/lge/systemservice/core/felicamanager/IFeliCaManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/4 v4, 0x4

    #@17
    const/4 v5, 0x0

    #@18
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 288
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1e
    .line 289
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@21
    move-result v3

    #@22
    if-eqz v3, :cond_25

    #@24
    const/4 v2, 0x1

    #@25
    .line 290
    .local v2, _result:Z
    :cond_25
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->readStringArray([Ljava/lang/String;)V
    :try_end_28
    .catchall {:try_start_9 .. :try_end_28} :catchall_34

    #@28
    .line 293
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 294
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 296
    return v2

    #@2f
    .line 285
    .end local v2           #_result:Z
    :cond_2f
    :try_start_2f
    array-length v3, p1

    #@30
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_33
    .catchall {:try_start_2f .. :try_end_33} :catchall_34

    #@33
    goto :goto_14

    #@34
    .line 293
    :catchall_34
    move-exception v3

    #@35
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@38
    .line 294
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3b
    throw v3
.end method

.method public cmdSwitchRead([Ljava/lang/String;)Z
    .registers 8
    .parameter "response"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 318
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 319
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 322
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.lge.systemservice.core.felicamanager.IFeliCaManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 323
    if-nez p1, :cond_2f

    #@10
    .line 324
    const/4 v3, -0x1

    #@11
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 329
    :goto_14
    iget-object v3, p0, Lcom/lge/systemservice/core/felicamanager/IFeliCaManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/4 v4, 0x6

    #@17
    const/4 v5, 0x0

    #@18
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 330
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1e
    .line 331
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@21
    move-result v3

    #@22
    if-eqz v3, :cond_25

    #@24
    const/4 v2, 0x1

    #@25
    .line 332
    .local v2, _result:Z
    :cond_25
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->readStringArray([Ljava/lang/String;)V
    :try_end_28
    .catchall {:try_start_9 .. :try_end_28} :catchall_34

    #@28
    .line 335
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 336
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 338
    return v2

    #@2f
    .line 327
    .end local v2           #_result:Z
    :cond_2f
    :try_start_2f
    array-length v3, p1

    #@30
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_33
    .catchall {:try_start_2f .. :try_end_33} :catchall_34

    #@33
    goto :goto_14

    #@34
    .line 335
    :catchall_34
    move-exception v3

    #@35
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@38
    .line 336
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3b
    throw v3
.end method

.method public cmdSwitchWrite(I)Z
    .registers 8
    .parameter "switchIdx"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 300
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 301
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 304
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.lge.systemservice.core.felicamanager.IFeliCaManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 305
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 306
    iget-object v3, p0, Lcom/lge/systemservice/core/felicamanager/IFeliCaManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@13
    const/4 v4, 0x5

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 307
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 308
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1e
    .catchall {:try_start_9 .. :try_end_1e} :catchall_29

    #@1e
    move-result v3

    #@1f
    if-eqz v3, :cond_22

    #@21
    const/4 v2, 0x1

    #@22
    .line 311
    .local v2, _result:Z
    :cond_22
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 312
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 314
    return v2

    #@29
    .line 311
    .end local v2           #_result:Z
    :catchall_29
    move-exception v3

    #@2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 312
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    throw v3
.end method
