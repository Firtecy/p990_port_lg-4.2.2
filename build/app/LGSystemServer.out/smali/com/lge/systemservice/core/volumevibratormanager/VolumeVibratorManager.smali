.class public Lcom/lge/systemservice/core/volumevibratormanager/VolumeVibratorManager;
.super Ljava/lang/Object;
.source "VolumeVibratorManager.java"

# interfaces
.implements Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;


# static fields
.field private static gDefault:Lcom/lge/systemservice/core/LGServiceSingleton;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/lge/systemservice/core/LGServiceSingleton",
            "<",
            "Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mToken:Landroid/os/Binder;

.field private mVolumeValue:[I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 47
    new-instance v0, Lcom/lge/systemservice/core/volumevibratormanager/VolumeVibratorManager$1;

    #@2
    const-string v1, "volumevibrator"

    #@4
    invoke-direct {v0, v1}, Lcom/lge/systemservice/core/volumevibratormanager/VolumeVibratorManager$1;-><init>(Ljava/lang/String;)V

    #@7
    sput-object v0, Lcom/lge/systemservice/core/volumevibratormanager/VolumeVibratorManager;->gDefault:Lcom/lge/systemservice/core/LGServiceSingleton;

    #@9
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 61
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Lcom/lge/systemservice/core/volumevibratormanager/VolumeVibratorManager;-><init>(Landroid/content/Context;)V

    #@4
    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 8
    .parameter "context"

    #@0
    .prologue
    .line 64
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 45
    new-instance v3, Landroid/os/Binder;

    #@5
    invoke-direct {v3}, Landroid/os/Binder;-><init>()V

    #@8
    iput-object v3, p0, Lcom/lge/systemservice/core/volumevibratormanager/VolumeVibratorManager;->mToken:Landroid/os/Binder;

    #@a
    .line 56
    const/16 v3, 0x8

    #@c
    new-array v3, v3, [I

    #@e
    fill-array-data v3, :array_78

    #@11
    iput-object v3, p0, Lcom/lge/systemservice/core/volumevibratormanager/VolumeVibratorManager;->mVolumeValue:[I

    #@13
    .line 65
    const-string v3, "ro.lge.vib_magnitude_index"

    #@15
    const/4 v4, 0x0

    #@16
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    .line 66
    .local v1, vibrateVolume:Ljava/lang/String;
    if-eqz v1, :cond_77

    #@1c
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@1f
    move-result v3

    #@20
    if-lez v3, :cond_77

    #@22
    .line 67
    const-string v3, "VolumeVibratorManager"

    #@24
    new-instance v4, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v5, "ro.lge.vib_magnitude_index = "

    #@2b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v4

    #@2f
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v4

    #@37
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 68
    const-string v3, ","

    #@3c
    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@3f
    move-result-object v2

    #@40
    .line 69
    .local v2, vibteteVolumeIndex:[Ljava/lang/String;
    const/4 v0, 0x0

    #@41
    .local v0, i:I
    :goto_41
    array-length v3, v2

    #@42
    if-ge v0, v3, :cond_77

    #@44
    .line 70
    iget-object v3, p0, Lcom/lge/systemservice/core/volumevibratormanager/VolumeVibratorManager;->mVolumeValue:[I

    #@46
    aget-object v4, v2, v0

    #@48
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@4b
    move-result v4

    #@4c
    aput v4, v3, v0

    #@4e
    .line 71
    const-string v3, "VolumeVibratorManager"

    #@50
    new-instance v4, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v5, "Initialize mVolumeValue["

    #@57
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v4

    #@5b
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v4

    #@5f
    const-string v5, "] to "

    #@61
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v4

    #@65
    iget-object v5, p0, Lcom/lge/systemservice/core/volumevibratormanager/VolumeVibratorManager;->mVolumeValue:[I

    #@67
    aget v5, v5, v0

    #@69
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v4

    #@6d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@70
    move-result-object v4

    #@71
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@74
    .line 69
    add-int/lit8 v0, v0, 0x1

    #@76
    goto :goto_41

    #@77
    .line 74
    .end local v0           #i:I
    .end local v2           #vibteteVolumeIndex:[Ljava/lang/String;
    :cond_77
    return-void

    #@78
    .line 56
    :array_78
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0xd0t 0x7t 0x0t 0x0t
        0xb8t 0xbt 0x0t 0x0t
        0xa0t 0xft 0x0t 0x0t
        0x88t 0x13t 0x0t 0x0t
        0xedt 0x17t 0x0t 0x0t
        0x6bt 0x1dt 0x0t 0x0t
        0x80t 0x38t 0x1t 0x0t
    .end array-data
.end method

.method private static getService()Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager;
    .registers 1

    #@0
    .prologue
    .line 81
    sget-object v0, Lcom/lge/systemservice/core/volumevibratormanager/VolumeVibratorManager;->gDefault:Lcom/lge/systemservice/core/LGServiceSingleton;

    #@2
    invoke-virtual {v0}, Lcom/lge/systemservice/core/LGServiceSingleton;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager;

    #@8
    return-object v0
.end method


# virtual methods
.method public cancel()V
    .registers 4

    #@0
    .prologue
    .line 145
    invoke-static {}, Lcom/lge/systemservice/core/volumevibratormanager/VolumeVibratorManager;->getService()Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager;

    #@3
    move-result-object v1

    #@4
    if-nez v1, :cond_7

    #@6
    .line 153
    :goto_6
    return-void

    #@7
    .line 149
    :cond_7
    :try_start_7
    invoke-static {}, Lcom/lge/systemservice/core/volumevibratormanager/VolumeVibratorManager;->getService()Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager;

    #@a
    move-result-object v1

    #@b
    iget-object v2, p0, Lcom/lge/systemservice/core/volumevibratormanager/VolumeVibratorManager;->mToken:Landroid/os/Binder;

    #@d
    invoke-interface {v1, v2}, Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager;->cancelVibrate(Landroid/os/IBinder;)V
    :try_end_10
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_10} :catch_11

    #@10
    goto :goto_6

    #@11
    .line 150
    :catch_11
    move-exception v0

    #@12
    .line 151
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "VolumeVibratorManager"

    #@14
    const-string v2, "Failed to cancel vibration."

    #@16
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@19
    goto :goto_6
.end method

.method public getVibrateVolume()I
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 186
    invoke-static {}, Lcom/lge/systemservice/core/volumevibratormanager/VolumeVibratorManager;->getService()Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager;

    #@4
    move-result-object v2

    #@5
    if-nez v2, :cond_8

    #@7
    .line 194
    :goto_7
    return v1

    #@8
    .line 190
    :cond_8
    :try_start_8
    const-string v2, "VolumeVibratorManager"

    #@a
    const-string v3, "getVibrateVolume"

    #@c
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 191
    invoke-static {}, Lcom/lge/systemservice/core/volumevibratormanager/VolumeVibratorManager;->getService()Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager;

    #@12
    move-result-object v2

    #@13
    invoke-interface {v2}, Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager;->getVibrateVolume()I
    :try_end_16
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_16} :catch_18

    #@16
    move-result v1

    #@17
    goto :goto_7

    #@18
    .line 192
    :catch_18
    move-exception v0

    #@19
    .line 193
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "VolumeVibratorManager"

    #@1b
    const-string v3, "Failed to getVibrateVolume."

    #@1d
    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@20
    goto :goto_7
.end method

.method public getVibrateVolume(I)I
    .registers 7
    .parameter "vibrateType"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 230
    invoke-static {}, Lcom/lge/systemservice/core/volumevibratormanager/VolumeVibratorManager;->getService()Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager;

    #@4
    move-result-object v3

    #@5
    if-nez v3, :cond_8

    #@7
    .line 241
    :goto_7
    return v2

    #@8
    .line 234
    :cond_8
    const/4 v1, 0x5

    #@9
    .line 235
    .local v1, mVolume:I
    if-ltz p1, :cond_27

    #@b
    const/4 v3, 0x3

    #@c
    if-ge p1, v3, :cond_27

    #@e
    .line 237
    :try_start_e
    const-string v3, "VolumeVibratorManager"

    #@10
    const-string v4, "getVibrateVolume"

    #@12
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    .line 238
    invoke-static {}, Lcom/lge/systemservice/core/volumevibratormanager/VolumeVibratorManager;->getService()Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager;

    #@18
    move-result-object v3

    #@19
    invoke-interface {v3, p1}, Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager;->getVibrateVolumeType(I)I
    :try_end_1c
    .catch Landroid/os/RemoteException; {:try_start_e .. :try_end_1c} :catch_1e

    #@1c
    move-result v2

    #@1d
    goto :goto_7

    #@1e
    .line 239
    :catch_1e
    move-exception v0

    #@1f
    .line 240
    .local v0, e:Landroid/os/RemoteException;
    const-string v3, "VolumeVibratorManager"

    #@21
    const-string v4, "Failed to setVibrateVolume."

    #@23
    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@26
    goto :goto_7

    #@27
    .line 244
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_27
    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@29
    invoke-direct {v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@2c
    throw v2
.end method

.method public setVibrateVolume(I)V
    .registers 5
    .parameter "volumeIndex"

    #@0
    .prologue
    .line 163
    invoke-static {}, Lcom/lge/systemservice/core/volumevibratormanager/VolumeVibratorManager;->getService()Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager;

    #@3
    move-result-object v1

    #@4
    if-nez v1, :cond_7

    #@6
    .line 176
    :goto_6
    return-void

    #@7
    .line 166
    :cond_7
    if-ltz p1, :cond_24

    #@9
    const/4 v1, 0x7

    #@a
    if-gt p1, v1, :cond_24

    #@c
    .line 168
    :try_start_c
    invoke-static {}, Lcom/lge/systemservice/core/volumevibratormanager/VolumeVibratorManager;->getService()Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager;

    #@f
    move-result-object v1

    #@10
    invoke-interface {v1, p1}, Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager;->setVibrateVolume(I)V

    #@13
    .line 169
    const-string v1, "VolumeVibratorManager"

    #@15
    const-string v2, "setVibrateVolume"

    #@17
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1a
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_1a} :catch_1b

    #@1a
    goto :goto_6

    #@1b
    .line 170
    :catch_1b
    move-exception v0

    #@1c
    .line 171
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "VolumeVibratorManager"

    #@1e
    const-string v2, "Failed to setVibrateVolume."

    #@20
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@23
    goto :goto_6

    #@24
    .line 174
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_24
    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@26
    invoke-direct {v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@29
    throw v1
.end method

.method public setVibrateVolume(II)V
    .registers 6
    .parameter "vibrateType"
    .parameter "volumeIndex"

    #@0
    .prologue
    .line 206
    invoke-static {}, Lcom/lge/systemservice/core/volumevibratormanager/VolumeVibratorManager;->getService()Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager;

    #@3
    move-result-object v1

    #@4
    if-nez v1, :cond_7

    #@6
    .line 220
    :goto_6
    return-void

    #@7
    .line 209
    :cond_7
    if-ltz p2, :cond_29

    #@9
    const/4 v1, 0x7

    #@a
    if-gt p2, v1, :cond_29

    #@c
    if-ltz p1, :cond_29

    #@e
    const/4 v1, 0x3

    #@f
    if-ge p1, v1, :cond_29

    #@11
    .line 212
    :try_start_11
    invoke-static {}, Lcom/lge/systemservice/core/volumevibratormanager/VolumeVibratorManager;->getService()Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager;

    #@14
    move-result-object v1

    #@15
    invoke-interface {v1, p1, p2}, Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager;->setVibrateVolumeType(II)V

    #@18
    .line 213
    const-string v1, "VolumeVibratorManager"

    #@1a
    const-string v2, "setVibrateVolume"

    #@1c
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1f
    .catch Landroid/os/RemoteException; {:try_start_11 .. :try_end_1f} :catch_20

    #@1f
    goto :goto_6

    #@20
    .line 214
    :catch_20
    move-exception v0

    #@21
    .line 215
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "VolumeVibratorManager"

    #@23
    const-string v2, "Failed to setVibrateVolume."

    #@25
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@28
    goto :goto_6

    #@29
    .line 218
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_29
    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@2b
    invoke-direct {v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@2e
    throw v1
.end method

.method public vibrate(JI)V
    .registers 8
    .parameter "milliseconds"
    .parameter "volumeIndex"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ArrayIndexOutOfBoundsException;
        }
    .end annotation

    #@0
    .prologue
    .line 92
    invoke-static {}, Lcom/lge/systemservice/core/volumevibratormanager/VolumeVibratorManager;->getService()Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager;

    #@3
    move-result-object v1

    #@4
    if-nez v1, :cond_7

    #@6
    .line 105
    :goto_6
    return-void

    #@7
    .line 95
    :cond_7
    if-ltz p3, :cond_23

    #@9
    const/4 v1, 0x7

    #@a
    if-gt p3, v1, :cond_23

    #@c
    .line 97
    :try_start_c
    invoke-static {}, Lcom/lge/systemservice/core/volumevibratormanager/VolumeVibratorManager;->getService()Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager;

    #@f
    move-result-object v1

    #@10
    iget-object v2, p0, Lcom/lge/systemservice/core/volumevibratormanager/VolumeVibratorManager;->mVolumeValue:[I

    #@12
    aget v2, v2, p3

    #@14
    iget-object v3, p0, Lcom/lge/systemservice/core/volumevibratormanager/VolumeVibratorManager;->mToken:Landroid/os/Binder;

    #@16
    invoke-interface {v1, p1, p2, v2, v3}, Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager;->vibrate(JILandroid/os/IBinder;)V
    :try_end_19
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_19} :catch_1a

    #@19
    goto :goto_6

    #@1a
    .line 99
    :catch_1a
    move-exception v0

    #@1b
    .line 100
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "VolumeVibratorManager"

    #@1d
    const-string v2, "Failed to vibrate."

    #@1f
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@22
    goto :goto_6

    #@23
    .line 103
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_23
    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@25
    invoke-direct {v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@28
    throw v1
.end method

.method public vibrate([JI[I)V
    .registers 9
    .parameter "pattern"
    .parameter "repeat"
    .parameter "volumeIndex"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ArrayIndexOutOfBoundsException;
        }
    .end annotation

    #@0
    .prologue
    .line 123
    invoke-static {}, Lcom/lge/systemservice/core/volumevibratormanager/VolumeVibratorManager;->getService()Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager;

    #@3
    move-result-object v3

    #@4
    if-nez v3, :cond_7

    #@6
    .line 142
    :goto_6
    return-void

    #@7
    .line 129
    :cond_7
    array-length v3, p1

    #@8
    if-ge p2, v3, :cond_33

    #@a
    array-length v3, p1

    #@b
    array-length v4, p3

    #@c
    if-ne v3, v4, :cond_33

    #@e
    .line 131
    :try_start_e
    array-length v3, p3

    #@f
    new-array v0, v3, [I

    #@11
    .line 132
    .local v0, NaturalVolumeIndex:[I
    const/4 v2, 0x0

    #@12
    .local v2, i:I
    :goto_12
    array-length v3, p3

    #@13
    if-ge v2, v3, :cond_20

    #@15
    .line 133
    iget-object v3, p0, Lcom/lge/systemservice/core/volumevibratormanager/VolumeVibratorManager;->mVolumeValue:[I

    #@17
    aget v4, p3, v2

    #@19
    aget v3, v3, v4

    #@1b
    aput v3, v0, v2

    #@1d
    .line 132
    add-int/lit8 v2, v2, 0x1

    #@1f
    goto :goto_12

    #@20
    .line 135
    :cond_20
    invoke-static {}, Lcom/lge/systemservice/core/volumevibratormanager/VolumeVibratorManager;->getService()Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager;

    #@23
    move-result-object v3

    #@24
    iget-object v4, p0, Lcom/lge/systemservice/core/volumevibratormanager/VolumeVibratorManager;->mToken:Landroid/os/Binder;

    #@26
    invoke-interface {v3, p1, p2, v0, v4}, Lcom/lge/systemservice/core/volumevibratormanager/IVolumeVibratorManager;->vibratePattern([JI[ILandroid/os/IBinder;)V
    :try_end_29
    .catch Landroid/os/RemoteException; {:try_start_e .. :try_end_29} :catch_2a

    #@29
    goto :goto_6

    #@2a
    .line 136
    .end local v0           #NaturalVolumeIndex:[I
    .end local v2           #i:I
    :catch_2a
    move-exception v1

    #@2b
    .line 137
    .local v1, e:Landroid/os/RemoteException;
    const-string v3, "VolumeVibratorManager"

    #@2d
    const-string v4, "Failed to vibrate."

    #@2f
    invoke-static {v3, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@32
    goto :goto_6

    #@33
    .line 140
    .end local v1           #e:Landroid/os/RemoteException;
    :cond_33
    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@35
    invoke-direct {v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@38
    throw v3
.end method
