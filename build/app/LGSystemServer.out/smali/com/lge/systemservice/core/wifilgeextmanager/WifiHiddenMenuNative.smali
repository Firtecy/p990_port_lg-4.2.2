.class public Lcom/lge/systemservice/core/wifilgeextmanager/WifiHiddenMenu;
.super Ljava/lang/Object;
.source "WifiHiddenMenu.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 8
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static Channel(II)Z
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method public static Channel5G(II)Z
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method public static CloseDUT()Z
	.registers 1
	const/4 v0, 0x1
	return v0
.end method

.method public static FRError()I
	.registers 1
	const/4 v0, 0x0
	return v0
.end method

.method public static FRGood()I
	.registers 1
	const/4 v0, 0x1
	return v0
.end method

.method public static FRTotal()I
	.registers 1
	const/4 v0, 0x1
	return v0
.end method

.method public static FrequencyAccuracy(Ljava/lang/String;I)Z
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method public static FrequencyAccuracy5G(Ljava/lang/String;I)Z
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method public static IsRunning()Z
	.registers 1
	const/4 v0, 0x1
	return v0
.end method

.method public static NoModTxStart()Z
	.registers 1
	const/4 v0, 0x1
	return v0
.end method

.method public static NoModTxStartBCM(I)Z
	.registers 2
	const/4 v0, 0x1
	return v0
.end method

.method public static NoModTxStop()Z
	.registers 1
	const/4 v0, 0x1
	return v0
.end method

.method public static OpenDUT()Z
	.registers 1
	const/4 v0, 0x1
	return v0
.end method

.method public static OtaDisable()Z
	.registers 1
	const/4 v0, 0x1
	return v0
.end method

.method public static OtaEnable()Z
	.registers 1
	const/4 v0, 0x1
	return v0
.end method

.method public static RSSI()I
	.registers 1
	const/4 v0, 0x1
	return v0
.end method

.method public static RxPER(Ljava/lang/String;)I
	.registers 2
	const/4 v0, 0x1
	return v0
.end method

.method public static RxStart()Z
	.registers 1
	const/4 v0, 0x1
	return v0
.end method

.method public static RxStop()Z
	.registers 1
	const/4 v0, 0x1
	return v0
.end method

.method public static Set11nPreamble(I)Z
	.registers 2
	const/4 v0, 0x1
	return v0
.end method

.method public static SetPreamble(I)Z
	.registers 2
	const/4 v0, 0x1
	return v0
.end method

.method public static TXBW_40M(I)Z
	.registers 2
	const/4 v0, 0x1
	return v0
.end method

.method public static TXBW_80M(I)Z
	.registers 2
	const/4 v0, 0x1
	return v0
.end method

.method public static TxBurstFrames(I)Z
	.registers 2
	const/4 v0, 0x1
	return v0
.end method

.method public static TxBurstInterval(I)Z
	.registers 2
	const/4 v0, 0x1
	return v0
.end method

.method public static TxDataRate(Ljava/lang/String;)Z
	.registers 2
	const/4 v0, 0x1
	return v0
.end method

.method public static TxDataRate11ac(IIII)Z
	.registers 5
	const/4 v0, 0x1
	return v0
.end method

.method public static TxDataRate11n(III)Z
	.registers 4
	const/4 v0, 0x1
	return v0
.end method

.method public static TxDataRate11n5G(III)Z
	.registers 4
	const/4 v0, 0x1
	return v0
.end method

.method public static TxDataRate11n5G40M(III)Z
	.registers 4
	const/4 v0, 0x1
	return v0
.end method

.method public static TxDataRate5G(Ljava/lang/String;)Z
	.registers 2
	const/4 v0, 0x1
	return v0
.end method

.method public static TxDestAddress(Ljava/lang/String;)Z
	.registers 2
	const/4 v0, 0x1
	return v0
.end method

.method public static TxGain(I)Z
	.registers 2
	const/4 v0, 0x1
	return v0
.end method

.method public static TxPER(Ljava/lang/String;)I
	.registers 2
	const/4 v0, 0x1
	return v0
.end method

.method public static TxPayloadLength(I)Z
	.registers 2
	const/4 v0, 0x1
	return v0
.end method

.method public static TxStart()Z
	.registers 1
	const/4 v0, 0x1
	return v0
.end method

.method public static TxStop()Z
	.registers 1
	const/4 v0, 0x1
	return v0
.end method
