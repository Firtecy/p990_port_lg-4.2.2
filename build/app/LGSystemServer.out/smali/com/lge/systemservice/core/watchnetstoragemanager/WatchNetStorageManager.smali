.class public Lcom/lge/systemservice/core/watchnetstoragemanager/WatchNetStorageManager;
.super Ljava/lang/Object;
.source "WatchNetStorageManager.java"


# static fields
.field private static gDefault:Lcom/lge/systemservice/core/LGServiceSingleton;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/lge/systemservice/core/LGServiceSingleton",
            "<",
            "Lcom/lge/systemservice/core/watchnetstoragemanager/IWatchNetStorageManager;",
            ">;"
        }
    .end annotation
.end field

.field private static mFirstCall:Z

.field private static mService:Lcom/lge/systemservice/core/watchnetstoragemanager/IWatchNetStorageManager;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 22
    const/4 v0, 0x0

    #@1
    sput-object v0, Lcom/lge/systemservice/core/watchnetstoragemanager/WatchNetStorageManager;->mService:Lcom/lge/systemservice/core/watchnetstoragemanager/IWatchNetStorageManager;

    #@3
    .line 23
    const/4 v0, 0x1

    #@4
    sput-boolean v0, Lcom/lge/systemservice/core/watchnetstoragemanager/WatchNetStorageManager;->mFirstCall:Z

    #@6
    .line 27
    new-instance v0, Lcom/lge/systemservice/core/watchnetstoragemanager/WatchNetStorageManager$1;

    #@8
    const-string v1, "watchnetstorage"

    #@a
    invoke-direct {v0, v1}, Lcom/lge/systemservice/core/watchnetstoragemanager/WatchNetStorageManager$1;-><init>(Ljava/lang/String;)V

    #@d
    sput-object v0, Lcom/lge/systemservice/core/watchnetstoragemanager/WatchNetStorageManager;->gDefault:Lcom/lge/systemservice/core/LGServiceSingleton;

    #@f
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 36
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 37
    return-void
.end method
