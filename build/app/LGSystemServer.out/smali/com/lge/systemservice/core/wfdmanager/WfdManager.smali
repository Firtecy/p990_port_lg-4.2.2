.class public Lcom/lge/systemservice/core/wfdmanager/WfdManager;
.super Ljava/lang/Object;
.source "WfdManager.java"


# static fields
.field private static deathBinderNotificator:Landroid/os/IBinder$DeathRecipient;

.field private static mService:Lcom/lge/systemservice/core/wfdmanager/IWfdManager;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 33
    new-instance v0, Lcom/lge/systemservice/core/wfdmanager/WfdManager$1;

    #@2
    invoke-direct {v0}, Lcom/lge/systemservice/core/wfdmanager/WfdManager$1;-><init>()V

    #@5
    sput-object v0, Lcom/lge/systemservice/core/wfdmanager/WfdManager;->deathBinderNotificator:Landroid/os/IBinder$DeathRecipient;

    #@7
    .line 145
    const/4 v0, 0x0

    #@8
    sput-object v0, Lcom/lge/systemservice/core/wfdmanager/WfdManager;->mService:Lcom/lge/systemservice/core/wfdmanager/IWfdManager;

    #@a
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 148
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 149
    invoke-static {}, Lcom/lge/systemservice/core/wfdmanager/WfdManager;->getService()Lcom/lge/systemservice/core/wfdmanager/IWfdManager;

    #@6
    move-result-object v0

    #@7
    sput-object v0, Lcom/lge/systemservice/core/wfdmanager/WfdManager;->mService:Lcom/lge/systemservice/core/wfdmanager/IWfdManager;

    #@9
    .line 151
    return-void
.end method

.method static synthetic access$002(Lcom/lge/systemservice/core/wfdmanager/IWfdManager;)Lcom/lge/systemservice/core/wfdmanager/IWfdManager;
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 21
    sput-object p0, Lcom/lge/systemservice/core/wfdmanager/WfdManager;->mService:Lcom/lge/systemservice/core/wfdmanager/IWfdManager;

    #@2
    return-object p0
.end method

.method private static getService()Lcom/lge/systemservice/core/wfdmanager/IWfdManager;
    .registers 5

    #@0
    .prologue
    .line 159
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdManager;->mService:Lcom/lge/systemservice/core/wfdmanager/IWfdManager;

    #@2
    if-eqz v2, :cond_7

    #@4
    .line 160
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdManager;->mService:Lcom/lge/systemservice/core/wfdmanager/IWfdManager;

    #@6
    .line 182
    .local v0, b:Landroid/os/IBinder;
    :goto_6
    return-object v2

    #@7
    .line 163
    .end local v0           #b:Landroid/os/IBinder;
    :cond_7
    const-string v2, "wfdService"

    #@9
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@c
    move-result-object v0

    #@d
    .line 164
    .restart local v0       #b:Landroid/os/IBinder;
    invoke-static {v0}, Lcom/lge/systemservice/core/wfdmanager/IWfdManager$Stub;->asInterface(Landroid/os/IBinder;)Lcom/lge/systemservice/core/wfdmanager/IWfdManager;

    #@10
    move-result-object v2

    #@11
    sput-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdManager;->mService:Lcom/lge/systemservice/core/wfdmanager/IWfdManager;

    #@13
    .line 166
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdManager;->mService:Lcom/lge/systemservice/core/wfdmanager/IWfdManager;

    #@15
    if-nez v2, :cond_19

    #@17
    .line 167
    const/4 v2, 0x0

    #@18
    goto :goto_6

    #@19
    .line 177
    :cond_19
    :try_start_19
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdManager;->mService:Lcom/lge/systemservice/core/wfdmanager/IWfdManager;

    #@1b
    invoke-interface {v2}, Lcom/lge/systemservice/core/wfdmanager/IWfdManager;->asBinder()Landroid/os/IBinder;

    #@1e
    move-result-object v2

    #@1f
    sget-object v3, Lcom/lge/systemservice/core/wfdmanager/WfdManager;->deathBinderNotificator:Landroid/os/IBinder$DeathRecipient;

    #@21
    const/4 v4, 0x0

    #@22
    invoke-interface {v2, v3, v4}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_25
    .catch Landroid/os/RemoteException; {:try_start_19 .. :try_end_25} :catch_28

    #@25
    .line 182
    :goto_25
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdManager;->mService:Lcom/lge/systemservice/core/wfdmanager/IWfdManager;

    #@27
    goto :goto_6

    #@28
    .line 178
    :catch_28
    move-exception v1

    #@29
    .line 180
    .local v1, e:Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    #@2c
    goto :goto_25
.end method
