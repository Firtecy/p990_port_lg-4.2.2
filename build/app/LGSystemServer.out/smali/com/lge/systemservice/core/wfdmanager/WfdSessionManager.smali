.class public Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;
.super Ljava/lang/Object;
.source "WfdSessionManager.java"


# static fields
.field private static deathBinderNotificator:Landroid/os/IBinder$DeathRecipient;

.field private static mService:Lcom/qualcomm/wfd/service/ISessionManagerService;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 39
    const/4 v0, 0x0

    #@1
    sput-object v0, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->mService:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@3
    .line 44
    new-instance v0, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager$1;

    #@5
    invoke-direct {v0}, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager$1;-><init>()V

    #@8
    sput-object v0, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->deathBinderNotificator:Landroid/os/IBinder$DeathRecipient;

    #@a
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 53
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 54
    invoke-static {}, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->getService()Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@6
    move-result-object v0

    #@7
    sput-object v0, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->mService:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@9
    .line 55
    return-void
.end method

.method static synthetic access$002(Lcom/qualcomm/wfd/service/ISessionManagerService;)Lcom/qualcomm/wfd/service/ISessionManagerService;
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 30
    sput-object p0, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->mService:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@2
    return-object p0
.end method

.method private static getService()Lcom/qualcomm/wfd/service/ISessionManagerService;
    .registers 5

    #@0
    .prologue
    .line 63
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->mService:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@2
    if-eqz v2, :cond_7

    #@4
    .line 64
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->mService:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@6
    .line 86
    .local v0, b:Landroid/os/IBinder;
    :goto_6
    return-object v2

    #@7
    .line 67
    .end local v0           #b:Landroid/os/IBinder;
    :cond_7
    const-string v2, "wfdSessionService"

    #@9
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@c
    move-result-object v0

    #@d
    .line 68
    .restart local v0       #b:Landroid/os/IBinder;
    invoke-static {v0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@10
    move-result-object v2

    #@11
    sput-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->mService:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@13
    .line 70
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->mService:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@15
    if-nez v2, :cond_19

    #@17
    .line 71
    const/4 v2, 0x0

    #@18
    goto :goto_6

    #@19
    .line 81
    :cond_19
    :try_start_19
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->mService:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@1b
    invoke-interface {v2}, Lcom/qualcomm/wfd/service/ISessionManagerService;->asBinder()Landroid/os/IBinder;

    #@1e
    move-result-object v2

    #@1f
    sget-object v3, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->deathBinderNotificator:Landroid/os/IBinder$DeathRecipient;

    #@21
    const/4 v4, 0x0

    #@22
    invoke-interface {v2, v3, v4}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_25
    .catch Landroid/os/RemoteException; {:try_start_19 .. :try_end_25} :catch_28

    #@25
    .line 86
    :goto_25
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->mService:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@27
    goto :goto_6

    #@28
    .line 82
    :catch_28
    move-exception v1

    #@29
    .line 84
    .local v1, e:Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    #@2c
    goto :goto_25
.end method


# virtual methods
.method public deinit()I
    .registers 4

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 196
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->mService:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 201
    :goto_5
    return v1

    #@6
    .line 199
    :cond_6
    :try_start_6
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->mService:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@8
    invoke-interface {v2}, Lcom/qualcomm/wfd/service/ISessionManagerService;->deinit()I
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_b} :catch_d

    #@b
    move-result v1

    #@c
    goto :goto_5

    #@d
    .line 200
    :catch_d
    move-exception v0

    #@e
    .line 201
    .local v0, e:Landroid/os/RemoteException;
    goto :goto_5
.end method

.method public getStatus()Lcom/qualcomm/wfd/WfdStatus;
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 139
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->mService:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 144
    :goto_5
    return-object v1

    #@6
    .line 142
    :cond_6
    :try_start_6
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->mService:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@8
    invoke-interface {v2}, Lcom/qualcomm/wfd/service/ISessionManagerService;->getStatus()Lcom/qualcomm/wfd/WfdStatus;
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_b} :catch_d

    #@b
    move-result-object v1

    #@c
    goto :goto_5

    #@d
    .line 143
    :catch_d
    move-exception v0

    #@e
    .line 144
    .local v0, e:Landroid/os/RemoteException;
    goto :goto_5
.end method

.method public init(Lcom/qualcomm/wfd/service/IWfdActionListener;Lcom/qualcomm/wfd/WfdDevice;)I
    .registers 6
    .parameter "listener"
    .parameter "myDevice"

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 160
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->mService:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 165
    :goto_5
    return v1

    #@6
    .line 163
    :cond_6
    :try_start_6
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->mService:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@8
    invoke-interface {v2, p1, p2}, Lcom/qualcomm/wfd/service/ISessionManagerService;->init(Lcom/qualcomm/wfd/service/IWfdActionListener;Lcom/qualcomm/wfd/WfdDevice;)I
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_b} :catch_d

    #@b
    move-result v1

    #@c
    goto :goto_5

    #@d
    .line 164
    :catch_d
    move-exception v0

    #@e
    .line 165
    .local v0, e:Landroid/os/RemoteException;
    goto :goto_5
.end method

.method public pause()I
    .registers 4

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 342
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->mService:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 347
    :goto_5
    return v1

    #@6
    .line 345
    :cond_6
    :try_start_6
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->mService:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@8
    invoke-interface {v2}, Lcom/qualcomm/wfd/service/ISessionManagerService;->pause()I
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_b} :catch_d

    #@b
    move-result v1

    #@c
    goto :goto_5

    #@d
    .line 346
    :catch_d
    move-exception v0

    #@e
    .line 347
    .local v0, e:Landroid/os/RemoteException;
    goto :goto_5
.end method

.method public pause_transit()I
    .registers 4

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 369
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->mService:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 374
    :goto_5
    return v1

    #@6
    .line 372
    :cond_6
    :try_start_6
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->mService:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@8
    invoke-interface {v2}, Lcom/qualcomm/wfd/service/ISessionManagerService;->pause_transit()I
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_b} :catch_d

    #@b
    move-result v1

    #@c
    goto :goto_5

    #@d
    .line 373
    :catch_d
    move-exception v0

    #@e
    .line 374
    .local v0, e:Landroid/os/RemoteException;
    goto :goto_5
.end method

.method public play()I
    .registers 4

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 329
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->mService:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 334
    :goto_5
    return v1

    #@6
    .line 332
    :cond_6
    :try_start_6
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->mService:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@8
    invoke-interface {v2}, Lcom/qualcomm/wfd/service/ISessionManagerService;->play()I
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_b} :catch_d

    #@b
    move-result v1

    #@c
    goto :goto_5

    #@d
    .line 333
    :catch_d
    move-exception v0

    #@e
    .line 334
    .local v0, e:Landroid/os/RemoteException;
    goto :goto_5
.end method

.method public play_transit()I
    .registers 4

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 356
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->mService:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 361
    :goto_5
    return v1

    #@6
    .line 359
    :cond_6
    :try_start_6
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->mService:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@8
    invoke-interface {v2}, Lcom/qualcomm/wfd/service/ISessionManagerService;->play_transit()I
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_b} :catch_d

    #@b
    move-result v1

    #@c
    goto :goto_5

    #@d
    .line 360
    :catch_d
    move-exception v0

    #@e
    .line 361
    .local v0, e:Landroid/os/RemoteException;
    goto :goto_5
.end method

.method public setDeviceType(I)I
    .registers 5
    .parameter "type"

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 105
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->mService:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 110
    :goto_5
    return v1

    #@6
    .line 108
    :cond_6
    :try_start_6
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->mService:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@8
    invoke-interface {v2, p1}, Lcom/qualcomm/wfd/service/ISessionManagerService;->setDeviceType(I)I
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_b} :catch_d

    #@b
    move-result v1

    #@c
    goto :goto_5

    #@d
    .line 109
    :catch_d
    move-exception v0

    #@e
    .line 110
    .local v0, e:Landroid/os/RemoteException;
    goto :goto_5
.end method

.method public standby()I
    .registers 4

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 397
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->mService:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 402
    :goto_5
    return v1

    #@6
    .line 400
    :cond_6
    :try_start_6
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->mService:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@8
    invoke-interface {v2}, Lcom/qualcomm/wfd/service/ISessionManagerService;->standby()I
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_b} :catch_d

    #@b
    move-result v1

    #@c
    goto :goto_5

    #@d
    .line 401
    :catch_d
    move-exception v0

    #@e
    .line 402
    .local v0, e:Landroid/os/RemoteException;
    goto :goto_5
.end method

.method public startUibcSession()I
    .registers 4

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 415
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->mService:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 420
    :goto_5
    return v1

    #@6
    .line 418
    :cond_6
    :try_start_6
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->mService:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@8
    invoke-interface {v2}, Lcom/qualcomm/wfd/service/ISessionManagerService;->startUibcSession()I
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_b} :catch_d

    #@b
    move-result v1

    #@c
    goto :goto_5

    #@d
    .line 419
    :catch_d
    move-exception v0

    #@e
    .line 420
    .local v0, e:Landroid/os/RemoteException;
    goto :goto_5
.end method

.method public startWfdSession(Lcom/qualcomm/wfd/WfdDevice;)I
    .registers 5
    .parameter "device"

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 217
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->mService:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 222
    :goto_5
    return v1

    #@6
    .line 220
    :cond_6
    :try_start_6
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->mService:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@8
    invoke-interface {v2, p1}, Lcom/qualcomm/wfd/service/ISessionManagerService;->startWfdSession(Lcom/qualcomm/wfd/WfdDevice;)I
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_b} :catch_d

    #@b
    move-result v1

    #@c
    goto :goto_5

    #@d
    .line 221
    :catch_d
    move-exception v0

    #@e
    .line 222
    .local v0, e:Landroid/os/RemoteException;
    goto :goto_5
.end method

.method public stopUibcSession()I
    .registers 4

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 433
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->mService:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 438
    :goto_5
    return v1

    #@6
    .line 436
    :cond_6
    :try_start_6
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->mService:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@8
    invoke-interface {v2}, Lcom/qualcomm/wfd/service/ISessionManagerService;->stopUibcSession()I
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_b} :catch_d

    #@b
    move-result v1

    #@c
    goto :goto_5

    #@d
    .line 437
    :catch_d
    move-exception v0

    #@e
    .line 438
    .local v0, e:Landroid/os/RemoteException;
    goto :goto_5
.end method

.method public stopWfdSession()I
    .registers 4

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 236
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->mService:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 241
    :goto_5
    return v1

    #@6
    .line 239
    :cond_6
    :try_start_6
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->mService:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@8
    invoke-interface {v2}, Lcom/qualcomm/wfd/service/ISessionManagerService;->stopWfdSession()I
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_b} :catch_d

    #@b
    move-result v1

    #@c
    goto :goto_5

    #@d
    .line 240
    :catch_d
    move-exception v0

    #@e
    .line 241
    .local v0, e:Landroid/os/RemoteException;
    goto :goto_5
.end method

.method public teardown()I
    .registers 4

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 384
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->mService:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 389
    :goto_5
    return v1

    #@6
    .line 387
    :cond_6
    :try_start_6
    sget-object v2, Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;->mService:Lcom/qualcomm/wfd/service/ISessionManagerService;

    #@8
    invoke-interface {v2}, Lcom/qualcomm/wfd/service/ISessionManagerService;->teardown()I
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_b} :catch_d

    #@b
    move-result v1

    #@c
    goto :goto_5

    #@d
    .line 388
    :catch_d
    move-exception v0

    #@e
    .line 389
    .local v0, e:Landroid/os/RemoteException;
    goto :goto_5
.end method
