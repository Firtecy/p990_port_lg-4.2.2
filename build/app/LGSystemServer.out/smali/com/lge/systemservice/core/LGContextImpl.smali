.class public Lcom/lge/systemservice/core/LGContextImpl;
.super Lcom/lge/systemservice/core/LGContext;
.source "LGContextImpl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/systemservice/core/LGContextImpl$19;
    }
.end annotation


# static fields
.field private static final SERVICE_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/lge/systemservice/core/LGServiceManagerFetcher;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private cliptrayFeature:Z

.field private mAATManager:Lcom/lge/systemservice/core/aatmanager/AATManager;

.field private mContext:Landroid/content/Context;

.field private mCoreControlManager:Lcom/lge/systemservice/core/corecontrolmanager/CoreControlManager;

.field private mFeliCaManager:Lcom/lge/systemservice/core/felicamanager/FeliCaManager;

.field private mNfcLgManager:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

.field private mPrivateModeFeature:Z

.field private mSecureClockManager:Lcom/lge/systemservice/core/secureclockmanager/SecureClockManager;

.field private mWfdManager:Lcom/lge/systemservice/core/wfdmanager/WfdManager;

.field private mWfdSessionManager:Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 207
    new-instance v0, Ljava/util/HashMap;

    #@2
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@5
    sput-object v0, Lcom/lge/systemservice/core/LGContextImpl;->SERVICE_MAP:Ljava/util/HashMap;

    #@7
    .line 221
    const-string v0, "byeworld"

    #@9
    new-instance v1, Lcom/lge/systemservice/core/LGContextImpl$1;

    #@b
    const-string v2, "com.lge.software.byeworld"

    #@d
    invoke-direct {v1, v2}, Lcom/lge/systemservice/core/LGContextImpl$1;-><init>(Ljava/lang/String;)V

    #@10
    invoke-static {v0, v1}, Lcom/lge/systemservice/core/LGContextImpl;->registerService(Ljava/lang/String;Lcom/lge/systemservice/core/LGServiceManagerFetcher;)V

    #@1f
    .line 237
    const-string v0, "volumevibrator"

    #@21
    new-instance v1, Lcom/lge/systemservice/core/LGContextImpl$3;

    #@23
    const-string v2, "com.lge.software.volumevibrator"

    #@25
    invoke-direct {v1, v2}, Lcom/lge/systemservice/core/LGContextImpl$3;-><init>(Ljava/lang/String;)V

    #@28
    invoke-static {v0, v1}, Lcom/lge/systemservice/core/LGContextImpl;->registerService(Ljava/lang/String;Lcom/lge/systemservice/core/LGServiceManagerFetcher;)V

    #@2b
    .line 245
    const-string v0, "lgsdencryption"

    #@2d
    new-instance v1, Lcom/lge/systemservice/core/LGContextImpl$4;

    #@2f
    const-string v2, "com.lge.software.sdencryption"

    #@31
    invoke-direct {v1, v2}, Lcom/lge/systemservice/core/LGContextImpl$4;-><init>(Ljava/lang/String;)V

    #@34
    invoke-static {v0, v1}, Lcom/lge/systemservice/core/LGContextImpl;->registerService(Ljava/lang/String;Lcom/lge/systemservice/core/LGServiceManagerFetcher;)V

    #@37
    .line 253
    const-string v0, "watchnetstorage"

    #@39
    new-instance v1, Lcom/lge/systemservice/core/LGContextImpl$5;

    #@3b
    const-string v2, "com.lge.software.nfs"

    #@3d
    invoke-direct {v1, v2}, Lcom/lge/systemservice/core/LGContextImpl$5;-><init>(Ljava/lang/String;)V

    #@40
    invoke-static {v0, v1}, Lcom/lge/systemservice/core/LGContextImpl;->registerService(Ljava/lang/String;Lcom/lge/systemservice/core/LGServiceManagerFetcher;)V

    #@43
    .line 261
    const-string v0, "AAT"

    #@45
    new-instance v1, Lcom/lge/systemservice/core/LGContextImpl$6;

    #@47
    invoke-direct {v1}, Lcom/lge/systemservice/core/LGContextImpl$6;-><init>()V

    #@4a
    invoke-static {v0, v1}, Lcom/lge/systemservice/core/LGContextImpl;->registerService(Ljava/lang/String;Lcom/lge/systemservice/core/LGServiceManagerFetcher;)V

    #@4d
    .line 270
    const-string v0, "wifiLgeExtService"

    #@4f
    new-instance v1, Lcom/lge/systemservice/core/LGContextImpl$7;

    #@51
    invoke-direct {v1}, Lcom/lge/systemservice/core/LGContextImpl$7;-><init>()V

    #@54
    invoke-static {v0, v1}, Lcom/lge/systemservice/core/LGContextImpl;->registerService(Ljava/lang/String;Lcom/lge/systemservice/core/LGServiceManagerFetcher;)V

    #@57
    .line 278
    const-string v0, "cliptray"

    #@59
    new-instance v1, Lcom/lge/systemservice/core/LGContextImpl$8;

    #@5b
    const-string v2, "com.lge.software.cliptray"

    #@5d
    invoke-direct {v1, v2}, Lcom/lge/systemservice/core/LGContextImpl$8;-><init>(Ljava/lang/String;)V

    #@60
    invoke-static {v0, v1}, Lcom/lge/systemservice/core/LGContextImpl;->registerService(Ljava/lang/String;Lcom/lge/systemservice/core/LGServiceManagerFetcher;)V

    #@63
    .line 286
    const-string v0, "BtLgeExt"

    #@65
    new-instance v1, Lcom/lge/systemservice/core/LGContextImpl$9;

    #@67
    invoke-direct {v1}, Lcom/lge/systemservice/core/LGContextImpl$9;-><init>()V

    #@6a
    invoke-static {v0, v1}, Lcom/lge/systemservice/core/LGContextImpl;->registerService(Ljava/lang/String;Lcom/lge/systemservice/core/LGServiceManagerFetcher;)V

    #@6d
    .line 295
    const-string v0, "FeliCaService"

    #@6f
    new-instance v1, Lcom/lge/systemservice/core/LGContextImpl$10;

    #@71
    invoke-direct {v1}, Lcom/lge/systemservice/core/LGContextImpl$10;-><init>()V

    #@74
    invoke-static {v0, v1}, Lcom/lge/systemservice/core/LGContextImpl;->registerService(Ljava/lang/String;Lcom/lge/systemservice/core/LGServiceManagerFetcher;)V

    #@77
    .line 303
    const-string v0, "wfdService"

    #@79
    new-instance v1, Lcom/lge/systemservice/core/LGContextImpl$11;

    #@7b
    invoke-direct {v1}, Lcom/lge/systemservice/core/LGContextImpl$11;-><init>()V

    #@7e
    invoke-static {v0, v1}, Lcom/lge/systemservice/core/LGContextImpl;->registerService(Ljava/lang/String;Lcom/lge/systemservice/core/LGServiceManagerFetcher;)V

    #@81
    .line 311
    const-string v0, "wfdSessionService"

    #@83
    new-instance v1, Lcom/lge/systemservice/core/LGContextImpl$12;

    #@85
    invoke-direct {v1}, Lcom/lge/systemservice/core/LGContextImpl$12;-><init>()V

    #@88
    invoke-static {v0, v1}, Lcom/lge/systemservice/core/LGContextImpl;->registerService(Ljava/lang/String;Lcom/lge/systemservice/core/LGServiceManagerFetcher;)V

    #@95
    .line 326
    const-string v0, "secureclock"

    #@97
    new-instance v1, Lcom/lge/systemservice/core/LGContextImpl$14;

    #@99
    invoke-direct {v1}, Lcom/lge/systemservice/core/LGContextImpl$14;-><init>()V

    #@9c
    invoke-static {v0, v1}, Lcom/lge/systemservice/core/LGContextImpl;->registerService(Ljava/lang/String;Lcom/lge/systemservice/core/LGServiceManagerFetcher;)V

    #@9f
    .line 339
    const-string v0, "corecontrol"

    #@a1
    new-instance v1, Lcom/lge/systemservice/core/LGContextImpl$15;

    #@a3
    invoke-direct {v1}, Lcom/lge/systemservice/core/LGContextImpl$15;-><init>()V

    #@a6
    invoke-static {v0, v1}, Lcom/lge/systemservice/core/LGContextImpl;->registerService(Ljava/lang/String;Lcom/lge/systemservice/core/LGServiceManagerFetcher;)V

    #@a9
    .line 348
    const-string v0, "ro.build.target_operator"

    #@ab
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@ae
    move-result-object v0

    #@af
    const-string v1, "VZW"

    #@b1
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b4
    move-result v0

    #@b5
    if-eqz v0, :cond_c1

    #@b7
    .line 349
    const-string v0, "wifiLgeVZWService"

    #@b9
    new-instance v1, Lcom/lge/systemservice/core/LGContextImpl$16;

    #@bb
    invoke-direct {v1}, Lcom/lge/systemservice/core/LGContextImpl$16;-><init>()V

    #@be
    invoke-static {v0, v1}, Lcom/lge/systemservice/core/LGContextImpl;->registerService(Ljava/lang/String;Lcom/lge/systemservice/core/LGServiceManagerFetcher;)V

    #@c1
    .line 357
    :cond_c1
    const-string v0, "logcatcher"

    #@c3
    new-instance v1, Lcom/lge/systemservice/core/LGContextImpl$17;

    #@c5
    invoke-direct {v1}, Lcom/lge/systemservice/core/LGContextImpl$17;-><init>()V

    #@c8
    invoke-static {v0, v1}, Lcom/lge/systemservice/core/LGContextImpl;->registerService(Ljava/lang/String;Lcom/lge/systemservice/core/LGServiceManagerFetcher;)V

    #@cb
    .line 364
    const-string v0, "infocollector"

    #@cd
    new-instance v1, Lcom/lge/systemservice/core/LGContextImpl$18;

    #@cf
    const-string v2, "com.lge.software.infocollector"

    #@d1
    invoke-direct {v1, v2}, Lcom/lge/systemservice/core/LGContextImpl$18;-><init>(Ljava/lang/String;)V

    #@d4
    invoke-static {v0, v1}, Lcom/lge/systemservice/core/LGContextImpl;->registerService(Ljava/lang/String;Lcom/lge/systemservice/core/LGServiceManagerFetcher;)V

    #@d7
    .line 370
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x0

    #@2
    .line 78
    invoke-direct {p0}, Lcom/lge/systemservice/core/LGContext;-><init>()V

    #@5
    .line 55
    iput-object v0, p0, Lcom/lge/systemservice/core/LGContextImpl;->mContext:Landroid/content/Context;

    #@7
    .line 57
    iput-boolean v1, p0, Lcom/lge/systemservice/core/LGContextImpl;->cliptrayFeature:Z

    #@9
    .line 58
    iput-boolean v1, p0, Lcom/lge/systemservice/core/LGContextImpl;->mPrivateModeFeature:Z

    #@b
    .line 60
    iput-object v0, p0, Lcom/lge/systemservice/core/LGContextImpl;->mWfdManager:Lcom/lge/systemservice/core/wfdmanager/WfdManager;

    #@d
    .line 61
    iput-object v0, p0, Lcom/lge/systemservice/core/LGContextImpl;->mWfdSessionManager:Lcom/lge/systemservice/core/wfdmanager/WfdSessionManager;

    #@f
    .line 65
    iput-object v0, p0, Lcom/lge/systemservice/core/LGContextImpl;->mAATManager:Lcom/lge/systemservice/core/aatmanager/AATManager;

    #@11
    .line 66
    iput-object v0, p0, Lcom/lge/systemservice/core/LGContextImpl;->mFeliCaManager:Lcom/lge/systemservice/core/felicamanager/FeliCaManager;

    #@13
    .line 69
    iput-object v0, p0, Lcom/lge/systemservice/core/LGContextImpl;->mNfcLgManager:Lcom/lge/systemservice/core/nfclgmanager/NfcLgManager;

    #@15
    .line 71
    iput-object v0, p0, Lcom/lge/systemservice/core/LGContextImpl;->mSecureClockManager:Lcom/lge/systemservice/core/secureclockmanager/SecureClockManager;

    #@17
    .line 75
    iput-object v0, p0, Lcom/lge/systemservice/core/LGContextImpl;->mCoreControlManager:Lcom/lge/systemservice/core/corecontrolmanager/CoreControlManager;

    #@19
    .line 79
    iput-object p1, p0, Lcom/lge/systemservice/core/LGContextImpl;->mContext:Landroid/content/Context;

    #@1b
    .line 80
    iget-object v0, p0, Lcom/lge/systemservice/core/LGContextImpl;->mContext:Landroid/content/Context;

    #@1d
    const-string v1, "com.lge.software.cliptray"

    #@1f
    invoke-static {v0, v1}, Lcom/lge/systemservice/core/LGServiceManager;->hasLGSystemServiceFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@22
    move-result v0

    #@23
    iput-boolean v0, p0, Lcom/lge/systemservice/core/LGContextImpl;->cliptrayFeature:Z

    #@25
    .line 81
    iget-object v0, p0, Lcom/lge/systemservice/core/LGContextImpl;->mContext:Landroid/content/Context;

    #@27
    const-string v1, "com.lge.software.privatemode"

    #@29
    invoke-static {v0, v1}, Lcom/lge/systemservice/core/LGServiceManager;->hasLGSystemServiceFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@2c
    move-result v0

    #@2d
    iput-boolean v0, p0, Lcom/lge/systemservice/core/LGContextImpl;->mPrivateModeFeature:Z

    #@2f
    .line 82
    return-void
.end method

.method private getLegacyServiceInteralForCompatiable(Lcom/lge/systemservice/core/LGContext$ServiceList;)Ljava/lang/Object;
    .registers 5
    .parameter "serviceName"

    #@0
    .prologue
    .line 88
    iget-object v0, p0, Lcom/lge/systemservice/core/LGContextImpl;->mContext:Landroid/content/Context;

    #@2
    if-eqz v0, :cond_31

    #@4
    .line 89
    sget-object v0, Lcom/lge/systemservice/core/LGContextImpl$19;->$SwitchMap$com$lge$systemservice$core$LGContext$ServiceList:[I

    #@6
    invoke-virtual {p1}, Lcom/lge/systemservice/core/LGContext$ServiceList;->ordinal()I

    #@9
    move-result v1

    #@a
    aget v0, v0, v1

    #@c
    packed-switch v0, :pswitch_data_a4

    #@f
    .line 137
    const-string v0, "LGEContextImpl"

    #@11
    new-instance v1, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v2, "we don\'t support "

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {p1}, Lcom/lge/systemservice/core/LGContext$ServiceList;->toString()Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    const-string v2, " service"

    #@26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v1

    #@2e
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    .line 140
    :cond_31
    const/4 v0, 0x0

    #@32
    :goto_32
    return-object v0

    #@33
    .line 91
    :pswitch_33
    const-string v0, "byeworld"

    #@35
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/LGContextImpl;->getLGSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@38
    move-result-object v0

    #@39
    goto :goto_32

    #@3a
    .line 93
    :pswitch_3a
    const-string v0, "watchnetstorage"

    #@3c
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/LGContextImpl;->getLGSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@3f
    move-result-object v0

    #@40
    goto :goto_32

    #@41
    .line 95
    :pswitch_41
    const-string v0, "emotionled"

    #@43
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/LGContextImpl;->getLGSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@46
    move-result-object v0

    #@47
    goto :goto_32

    #@48
    .line 97
    :pswitch_48
    const-string v0, "lgsdencryption"

    #@4a
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/LGContextImpl;->getLGSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@4d
    move-result-object v0

    #@4e
    goto :goto_32

    #@4f
    .line 100
    :pswitch_4f
    const-string v0, "AAT"

    #@51
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/LGContextImpl;->getLegacyService(Ljava/lang/String;)Ljava/lang/Object;

    #@54
    move-result-object v0

    #@55
    goto :goto_32

    #@56
    .line 103
    :pswitch_56
    const-string v0, "volumevibrator"

    #@58
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/LGContextImpl;->getLGSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@5b
    move-result-object v0

    #@5c
    goto :goto_32

    #@5d
    .line 105
    :pswitch_5d
    const-string v0, "wifiLgeExtService"

    #@5f
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/LGContextImpl;->getLegacyService(Ljava/lang/String;)Ljava/lang/Object;

    #@62
    move-result-object v0

    #@63
    goto :goto_32

    #@64
    .line 108
    :pswitch_64
    const-string v0, "BtLgeExt"

    #@66
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/LGContextImpl;->getLegacyService(Ljava/lang/String;)Ljava/lang/Object;

    #@69
    move-result-object v0

    #@6a
    goto :goto_32

    #@6b
    .line 111
    :pswitch_6b
    const-string v0, "FeliCaService"

    #@6d
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/LGContextImpl;->getLegacyService(Ljava/lang/String;)Ljava/lang/Object;

    #@70
    move-result-object v0

    #@71
    goto :goto_32

    #@72
    .line 114
    :pswitch_72
    const-string v0, "wfdService"

    #@74
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/LGContextImpl;->getLegacyService(Ljava/lang/String;)Ljava/lang/Object;

    #@77
    move-result-object v0

    #@78
    goto :goto_32

    #@79
    .line 116
    :pswitch_79
    const-string v0, "wfdSessionService"

    #@7b
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/LGContextImpl;->getLegacyService(Ljava/lang/String;)Ljava/lang/Object;

    #@7e
    move-result-object v0

    #@7f
    goto :goto_32

    #@80
    .line 119
    :pswitch_80
    const-string v0, "nfcLgService"

    #@82
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/LGContextImpl;->getLegacyService(Ljava/lang/String;)Ljava/lang/Object;

    #@85
    move-result-object v0

    #@86
    goto :goto_32

    #@87
    .line 122
    :pswitch_87
    const-string v0, "secureclock"

    #@89
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/LGContextImpl;->getLegacyService(Ljava/lang/String;)Ljava/lang/Object;

    #@8c
    move-result-object v0

    #@8d
    goto :goto_32

    #@8e
    .line 126
    :pswitch_8e
    const-string v0, "corecontrol"

    #@90
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/LGContextImpl;->getLegacyService(Ljava/lang/String;)Ljava/lang/Object;

    #@93
    move-result-object v0

    #@94
    goto :goto_32

    #@95
    .line 131
    :pswitch_95
    const-string v0, "wifiLgeVZWService"

    #@97
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/LGContextImpl;->getLegacyService(Ljava/lang/String;)Ljava/lang/Object;

    #@9a
    move-result-object v0

    #@9b
    goto :goto_32

    #@9c
    .line 135
    :pswitch_9c
    const-string v0, "logcatcher"

    #@9e
    invoke-virtual {p0, v0}, Lcom/lge/systemservice/core/LGContextImpl;->getLegacyService(Ljava/lang/String;)Ljava/lang/Object;

    #@a1
    move-result-object v0

    #@a2
    goto :goto_32

    #@a3
    .line 89
    nop

    #@a4
    :pswitch_data_a4
    .packed-switch 0x1
        :pswitch_33
        :pswitch_3a
        :pswitch_41
        :pswitch_48
        :pswitch_4f
        :pswitch_56
        :pswitch_5d
        :pswitch_64
        :pswitch_6b
        :pswitch_72
        :pswitch_79
        :pswitch_80
        :pswitch_87
        :pswitch_8e
        :pswitch_95
        :pswitch_9c
    .end packed-switch
.end method

.method private static registerService(Ljava/lang/String;Lcom/lge/systemservice/core/LGServiceManagerFetcher;)V
    .registers 3
    .parameter "serviceName"
    .parameter "fetcher"

    #@0
    .prologue
    .line 211
    sget-object v0, Lcom/lge/systemservice/core/LGContextImpl;->SERVICE_MAP:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    .line 212
    return-void
.end method


# virtual methods
.method public getLGSystemService(Ljava/lang/String;)Ljava/lang/Object;
    .registers 4
    .parameter "serviceName"

    #@0
    .prologue
    .line 163
    sget-object v1, Lcom/lge/systemservice/core/LGContextImpl;->SERVICE_MAP:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/lge/systemservice/core/LGServiceManagerFetcher;

    #@8
    .line 164
    .local v0, fetcher:Lcom/lge/systemservice/core/LGServiceManagerFetcher;
    if-nez v0, :cond_c

    #@a
    const/4 v1, 0x0

    #@b
    :goto_b
    return-object v1

    #@c
    :cond_c
    iget-object v1, p0, Lcom/lge/systemservice/core/LGContextImpl;->mContext:Landroid/content/Context;

    #@e
    invoke-virtual {v0, v1}, Lcom/lge/systemservice/core/LGServiceManagerFetcher;->getService(Landroid/content/Context;)Ljava/lang/Object;

    #@11
    move-result-object v1

    #@12
    goto :goto_b
.end method

.method public getLegacyService(Lcom/lge/systemservice/core/LGContext$ServiceList;)Ljava/lang/Object;
    .registers 3
    .parameter "serviceName"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 148
    invoke-direct {p0, p1}, Lcom/lge/systemservice/core/LGContextImpl;->getLegacyServiceInteralForCompatiable(Lcom/lge/systemservice/core/LGContext$ServiceList;)Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getLegacyService(Ljava/lang/String;)Ljava/lang/Object;
    .registers 3
    .parameter "serviceName"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 156
    invoke-virtual {p0, p1}, Lcom/lge/systemservice/core/LGContextImpl;->getLGSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public startServer()Z
    .registers 4

    #@0
    .prologue
    .line 168
    iget-object v0, p0, Lcom/lge/systemservice/core/LGContextImpl;->mContext:Landroid/content/Context;

    #@2
    if-eqz v0, :cond_3e

    #@4
    .line 169
    iget-object v0, p0, Lcom/lge/systemservice/core/LGContextImpl;->mContext:Landroid/content/Context;

    #@6
    new-instance v1, Landroid/content/Intent;

    #@8
    const-string v2, "lge.android.intent.action.LGSYSTEM_SERVICE"

    #@a
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@d
    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    #@10
    .line 170
    iget-object v0, p0, Lcom/lge/systemservice/core/LGContextImpl;->mContext:Landroid/content/Context;

    #@12
    new-instance v1, Landroid/content/Intent;

    #@14
    const-string v2, "lge.android.intent.action.FACTORY_SERVICE"

    #@16
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@19
    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    #@1c
    .line 171
    iget-boolean v0, p0, Lcom/lge/systemservice/core/LGContextImpl;->cliptrayFeature:Z

    #@1e
    if-eqz v0, :cond_2c

    #@20
    .line 172
    iget-object v0, p0, Lcom/lge/systemservice/core/LGContextImpl;->mContext:Landroid/content/Context;

    #@22
    new-instance v1, Landroid/content/Intent;

    #@24
    const-string v2, "lge.android.intent.action.CLIPTRAY_SERVICE"

    #@26
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@29
    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    #@2c
    .line 174
    :cond_2c
    iget-boolean v0, p0, Lcom/lge/systemservice/core/LGContextImpl;->mPrivateModeFeature:Z

    #@2e
    if-eqz v0, :cond_3c

    #@30
    .line 175
    iget-object v0, p0, Lcom/lge/systemservice/core/LGContextImpl;->mContext:Landroid/content/Context;

    #@32
    new-instance v1, Landroid/content/Intent;

    #@34
    const-string v2, "com.lge.intent.action.PRIVATE_MODE_SERVICE"

    #@36
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@39
    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    #@3c
    .line 177
    :cond_3c
    const/4 v0, 0x1

    #@3d
    .line 180
    :goto_3d
    return v0

    #@3e
    .line 179
    :cond_3e
    const-string v0, "LGEContextImpl"

    #@40
    const-string v1, "Context value is null"

    #@42
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 180
    const/4 v0, 0x0

    #@46
    goto :goto_3d
.end method
