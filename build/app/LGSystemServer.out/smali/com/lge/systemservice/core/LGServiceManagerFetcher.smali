.class public abstract Lcom/lge/systemservice/core/LGServiceManagerFetcher;
.super Ljava/lang/Object;
.source "LGServiceManagerFetcher.java"


# instance fields
.field private mCachedInstance:Ljava/lang/Object;

.field private mFeatureName:Ljava/lang/String;

.field private mIsFeatureChecked:Z

.field private mIsSupportedFeature:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 23
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 25
    iput-boolean v0, p0, Lcom/lge/systemservice/core/LGServiceManagerFetcher;->mIsFeatureChecked:Z

    #@6
    .line 26
    iput-boolean v0, p0, Lcom/lge/systemservice/core/LGServiceManagerFetcher;->mIsSupportedFeature:Z

    #@8
    .line 27
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 3
    .parameter "featureName"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 17
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 18
    iput-object p1, p0, Lcom/lge/systemservice/core/LGServiceManagerFetcher;->mFeatureName:Ljava/lang/String;

    #@6
    .line 19
    iput-boolean v0, p0, Lcom/lge/systemservice/core/LGServiceManagerFetcher;->mIsFeatureChecked:Z

    #@8
    .line 20
    iput-boolean v0, p0, Lcom/lge/systemservice/core/LGServiceManagerFetcher;->mIsSupportedFeature:Z

    #@a
    .line 21
    return-void
.end method


# virtual methods
.method public abstract createServiceManager(Landroid/content/Context;)Ljava/lang/Object;
.end method

.method public getService(Landroid/content/Context;)Ljava/lang/Object;
    .registers 4
    .parameter "ctx"

    #@0
    .prologue
    .line 30
    monitor-enter p0

    #@1
    .line 31
    :try_start_1
    iget-boolean v1, p0, Lcom/lge/systemservice/core/LGServiceManagerFetcher;->mIsFeatureChecked:Z

    #@3
    if-nez v1, :cond_10

    #@5
    .line 32
    const/4 v1, 0x1

    #@6
    iput-boolean v1, p0, Lcom/lge/systemservice/core/LGServiceManagerFetcher;->mIsFeatureChecked:Z

    #@8
    .line 33
    iget-object v1, p0, Lcom/lge/systemservice/core/LGServiceManagerFetcher;->mFeatureName:Ljava/lang/String;

    #@a
    invoke-static {p1, v1}, Lcom/lge/systemservice/core/LGServiceManager;->hasLGSystemServiceFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@d
    move-result v1

    #@e
    iput-boolean v1, p0, Lcom/lge/systemservice/core/LGServiceManagerFetcher;->mIsSupportedFeature:Z

    #@10
    .line 36
    :cond_10
    iget-boolean v1, p0, Lcom/lge/systemservice/core/LGServiceManagerFetcher;->mIsSupportedFeature:Z

    #@12
    if-eqz v1, :cond_25

    #@14
    .line 37
    iget-object v0, p0, Lcom/lge/systemservice/core/LGServiceManagerFetcher;->mCachedInstance:Ljava/lang/Object;

    #@16
    .line 38
    .local v0, service:Ljava/lang/Object;
    if-eqz v0, :cond_1a

    #@18
    .line 39
    monitor-exit p0

    #@19
    .line 44
    .end local v0           #service:Ljava/lang/Object;
    :goto_19
    return-object v0

    #@1a
    .line 42
    .restart local v0       #service:Ljava/lang/Object;
    :cond_1a
    invoke-virtual {p0, p1}, Lcom/lge/systemservice/core/LGServiceManagerFetcher;->createServiceManager(Landroid/content/Context;)Ljava/lang/Object;

    #@1d
    move-result-object v0

    #@1e
    .end local v0           #service:Ljava/lang/Object;
    iput-object v0, p0, Lcom/lge/systemservice/core/LGServiceManagerFetcher;->mCachedInstance:Ljava/lang/Object;

    #@20
    monitor-exit p0

    #@21
    goto :goto_19

    #@22
    .line 46
    :catchall_22
    move-exception v1

    #@23
    monitor-exit p0
    :try_end_24
    .catchall {:try_start_1 .. :try_end_24} :catchall_22

    #@24
    throw v1

    #@25
    .line 44
    :cond_25
    const/4 v0, 0x0

    #@26
    :try_start_26
    monitor-exit p0
    :try_end_27
    .catchall {:try_start_26 .. :try_end_27} :catchall_22

    #@27
    goto :goto_19
.end method
