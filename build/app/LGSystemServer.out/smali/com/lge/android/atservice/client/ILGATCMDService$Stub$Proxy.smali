.class Lcom/lge/android/atservice/client/ILGATCMDService$Stub$Proxy;
.super Ljava/lang/Object;
.source "ILGATCMDService.java"

# interfaces
.implements Lcom/lge/android/atservice/client/ILGATCMDService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/android/atservice/client/ILGATCMDService$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 72
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 74
    iput-object p1, p0, Lcom/lge/android/atservice/client/ILGATCMDService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 75
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 78
    iget-object v0, p0, Lcom/lge/android/atservice/client/ILGATCMDService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public request(Landroid/os/Bundle;)Landroid/os/Bundle;
    .registers 8
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 86
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 87
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 90
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.android.atservice.client.ILGATCMDService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 91
    if-eqz p1, :cond_36

    #@f
    .line 92
    const/4 v3, 0x1

    #@10
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 93
    const/4 v3, 0x0

    #@14
    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 98
    :goto_17
    iget-object v3, p0, Lcom/lge/android/atservice/client/ILGATCMDService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/4 v4, 0x1

    #@1a
    const/4 v5, 0x0

    #@1b
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 99
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@21
    .line 100
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@24
    move-result v3

    #@25
    if-eqz v3, :cond_43

    #@27
    .line 101
    sget-object v3, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@29
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2c
    move-result-object v2

    #@2d
    check-cast v2, Landroid/os/Bundle;
    :try_end_2f
    .catchall {:try_start_8 .. :try_end_2f} :catchall_3b

    #@2f
    .line 108
    .local v2, _result:Landroid/os/Bundle;
    :goto_2f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 109
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 111
    return-object v2

    #@36
    .line 96
    .end local v2           #_result:Landroid/os/Bundle;
    :cond_36
    const/4 v3, 0x0

    #@37
    :try_start_37
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_3a
    .catchall {:try_start_37 .. :try_end_3a} :catchall_3b

    #@3a
    goto :goto_17

    #@3b
    .line 107
    :catchall_3b
    move-exception v3

    #@3c
    .line 108
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3f
    .line 109
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@42
    .line 110
    throw v3

    #@43
    .line 104
    :cond_43
    const/4 v2, 0x0

    #@44
    .line 106
    .restart local v2       #_result:Landroid/os/Bundle;
    goto :goto_2f
.end method
