.class Lcom/qualcomm/location/DeviceContext$1;
.super Landroid/content/BroadcastReceiver;
.source "DeviceContext.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/location/DeviceContext;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/location/DeviceContext;


# direct methods
.method constructor <init>(Lcom/qualcomm/location/DeviceContext;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 29
    iput-object p1, p0, Lcom/qualcomm/location/DeviceContext$1;->this$0:Lcom/qualcomm/location/DeviceContext;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 6
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 31
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 33
    .local v0, action:Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v2, "mBroadcastReceiver - "

    #@b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v1}, Lcom/qualcomm/location/DeviceContext;->access$000(Ljava/lang/String;)V

    #@1a
    .line 35
    const-string v1, "android.intent.action.ACTION_POWER_DISCONNECTED"

    #@1c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v1

    #@20
    if-eqz v1, :cond_29

    #@22
    .line 36
    iget-object v1, p0, Lcom/qualcomm/location/DeviceContext$1;->this$0:Lcom/qualcomm/location/DeviceContext;

    #@24
    const/4 v2, 0x0

    #@25
    invoke-static {v1, v2}, Lcom/qualcomm/location/DeviceContext;->access$100(Lcom/qualcomm/location/DeviceContext;I)V

    #@28
    .line 40
    :cond_28
    :goto_28
    return-void

    #@29
    .line 37
    :cond_29
    const-string v1, "android.intent.action.ACTION_POWER_CONNECTED"

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2e
    move-result v1

    #@2f
    if-eqz v1, :cond_28

    #@31
    .line 38
    iget-object v1, p0, Lcom/qualcomm/location/DeviceContext$1;->this$0:Lcom/qualcomm/location/DeviceContext;

    #@33
    const/4 v2, 0x1

    #@34
    invoke-static {v1, v2}, Lcom/qualcomm/location/DeviceContext;->access$100(Lcom/qualcomm/location/DeviceContext;I)V

    #@37
    goto :goto_28
.end method
