.class final Lcom/qualcomm/location/ulp/UlpEngine$GlobalSettingsObserver;
.super Ljava/lang/Object;
.source "UlpEngine.java"

# interfaces
.implements Ljava/util/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/location/ulp/UlpEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "GlobalSettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/location/ulp/UlpEngine;


# direct methods
.method private constructor <init>(Lcom/qualcomm/location/ulp/UlpEngine;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 798
    iput-object p1, p0, Lcom/qualcomm/location/ulp/UlpEngine$GlobalSettingsObserver;->this$0:Lcom/qualcomm/location/ulp/UlpEngine;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/qualcomm/location/ulp/UlpEngine;Lcom/qualcomm/location/ulp/UlpEngine$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 798
    invoke-direct {p0, p1}, Lcom/qualcomm/location/ulp/UlpEngine$GlobalSettingsObserver;-><init>(Lcom/qualcomm/location/ulp/UlpEngine;)V

    #@3
    return-void
.end method


# virtual methods
.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .registers 10
    .parameter "o"
    .parameter "arg"

    #@0
    .prologue
    .line 800
    invoke-static {}, Lcom/qualcomm/location/ulp/UlpEngine;->access$200()Z

    #@3
    move-result v4

    #@4
    if-eqz v4, :cond_d

    #@6
    const-string v4, "UlpEngine"

    #@8
    const-string v5, "GlobalSettingsObserver.update invoked "

    #@a
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 801
    :cond_d
    const/4 v3, 0x0

    #@e
    .local v3, wifiSetting:Z
    const/4 v0, 0x0

    #@f
    .line 803
    .local v0, agpsSetting:Z
    check-cast p1, Landroid/content/ContentQueryMap;

    #@11
    .end local p1
    invoke-virtual {p1}, Landroid/content/ContentQueryMap;->getRows()Ljava/util/Map;

    #@14
    move-result-object v1

    #@15
    .line 804
    .local v1, kvs:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Landroid/content/ContentValues;>;"
    if-eqz v1, :cond_85

    #@17
    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    #@1a
    move-result v4

    #@1b
    if-nez v4, :cond_85

    #@1d
    .line 808
    :try_start_1d
    const-string v4, "wifi_on"

    #@1f
    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@22
    move-result-object v4

    #@23
    check-cast v4, Landroid/content/ContentValues;

    #@25
    invoke-virtual {v4}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    #@28
    move-result-object v4

    #@29
    const-string v5, "1"

    #@2b
    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@2e
    move-result v4

    #@2f
    if-nez v4, :cond_45

    #@31
    const-string v4, "wifi_on"

    #@33
    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@36
    move-result-object v4

    #@37
    check-cast v4, Landroid/content/ContentValues;

    #@39
    invoke-virtual {v4}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    #@3c
    move-result-object v4

    #@3d
    const-string v5, "2"

    #@3f
    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_42
    .catch Ljava/lang/NullPointerException; {:try_start_1d .. :try_end_42} :catch_88

    #@42
    move-result v4

    #@43
    if-eqz v4, :cond_86

    #@45
    :cond_45
    const/4 v3, 0x1

    #@46
    .line 815
    :cond_46
    :goto_46
    :try_start_46
    const-string v4, "assisted_gps_enabled"

    #@48
    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@4b
    move-result-object v4

    #@4c
    check-cast v4, Landroid/content/ContentValues;

    #@4e
    invoke-virtual {v4}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    #@51
    move-result-object v4

    #@52
    const-string v5, "1"

    #@54
    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_57
    .catch Ljava/lang/NullPointerException; {:try_start_46 .. :try_end_57} :catch_97

    #@57
    move-result v0

    #@58
    .line 820
    :cond_58
    :goto_58
    invoke-static {}, Lcom/qualcomm/location/ulp/UlpEngine;->access$200()Z

    #@5b
    move-result v4

    #@5c
    if-eqz v4, :cond_80

    #@5e
    .line 821
    const-string v4, "UlpEngine"

    #@60
    new-instance v5, Ljava/lang/StringBuilder;

    #@62
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@65
    const-string v6, "setting values. WiFi:"

    #@67
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v5

    #@6b
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v5

    #@6f
    const-string v6, " Agps:"

    #@71
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v5

    #@75
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@78
    move-result-object v5

    #@79
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v5

    #@7d
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@80
    .line 823
    :cond_80
    iget-object v4, p0, Lcom/qualcomm/location/ulp/UlpEngine$GlobalSettingsObserver;->this$0:Lcom/qualcomm/location/ulp/UlpEngine;

    #@82
    invoke-static {v4, v3, v0}, Lcom/qualcomm/location/ulp/UlpEngine;->access$400(Lcom/qualcomm/location/ulp/UlpEngine;ZZ)Z

    #@85
    .line 825
    :cond_85
    return-void

    #@86
    .line 808
    :cond_86
    const/4 v3, 0x0

    #@87
    goto :goto_46

    #@88
    .line 810
    :catch_88
    move-exception v2

    #@89
    .line 811
    .local v2, npe:Ljava/lang/NullPointerException;
    invoke-static {}, Lcom/qualcomm/location/ulp/UlpEngine;->access$200()Z

    #@8c
    move-result v4

    #@8d
    if-eqz v4, :cond_46

    #@8f
    const-string v4, "UlpEngine"

    #@91
    const-string v5, "no WIFI_ON in the DB"

    #@93
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@96
    goto :goto_46

    #@97
    .line 816
    .end local v2           #npe:Ljava/lang/NullPointerException;
    :catch_97
    move-exception v2

    #@98
    .line 817
    .restart local v2       #npe:Ljava/lang/NullPointerException;
    invoke-static {}, Lcom/qualcomm/location/ulp/UlpEngine;->access$200()Z

    #@9b
    move-result v4

    #@9c
    if-eqz v4, :cond_58

    #@9e
    const-string v4, "UlpEngine"

    #@a0
    const-string v5, "no ASSISTED_GPS_ENABLED in the DB"

    #@a2
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@a5
    goto :goto_58
.end method
