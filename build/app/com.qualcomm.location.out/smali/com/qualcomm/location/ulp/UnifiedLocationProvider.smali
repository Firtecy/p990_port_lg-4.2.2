.class public Lcom/qualcomm/location/ulp/UnifiedLocationProvider;
.super Lcom/android/location/provider/LocationProviderBase;
.source "UnifiedLocationProvider.java"

# interfaces
.implements Lcom/qualcomm/location/ulp/UlpEngine$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/location/ulp/UnifiedLocationProvider$RequestWrapper;
    }
.end annotation


# static fields
.field private static final MSG_DISABLE:I = 0x2

.field private static final MSG_ENABLE:I = 0x1

.field private static final MSG_SET_REQUEST:I = 0x3

.field private static PROPERTIES:Lcom/android/location/provider/ProviderPropertiesUnbundled; = null

.field private static final TAG:Ljava/lang/String; = "UnifiedLocationProvider"

.field private static final VERBOSE_DBG:Z


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mEngine:Lcom/qualcomm/location/ulp/UlpEngine;

.field private mHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .registers 9

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 28
    const-string v1, "UnifiedLocationProvider"

    #@4
    const/4 v2, 0x2

    #@5
    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@8
    move-result v1

    #@9
    sput-boolean v1, Lcom/qualcomm/location/ulp/UnifiedLocationProvider;->VERBOSE_DBG:Z

    #@b
    move v1, v0

    #@c
    move v2, v0

    #@d
    move v3, v0

    #@e
    move v5, v4

    #@f
    move v6, v4

    #@10
    move v7, v4

    #@11
    move v8, v4

    #@12
    .line 30
    invoke-static/range {v0 .. v8}, Lcom/android/location/provider/ProviderPropertiesUnbundled;->create(ZZZZZZZII)Lcom/android/location/provider/ProviderPropertiesUnbundled;

    #@15
    move-result-object v0

    #@16
    sput-object v0, Lcom/qualcomm/location/ulp/UnifiedLocationProvider;->PROPERTIES:Lcom/android/location/provider/ProviderPropertiesUnbundled;

    #@18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 51
    const-string v0, "UnifiedLocationProvider"

    #@2
    sget-object v1, Lcom/qualcomm/location/ulp/UnifiedLocationProvider;->PROPERTIES:Lcom/android/location/provider/ProviderPropertiesUnbundled;

    #@4
    invoke-direct {p0, v0, v1}, Lcom/android/location/provider/LocationProviderBase;-><init>(Ljava/lang/String;Lcom/android/location/provider/ProviderPropertiesUnbundled;)V

    #@7
    .line 59
    new-instance v0, Lcom/qualcomm/location/ulp/UnifiedLocationProvider$1;

    #@9
    invoke-direct {v0, p0}, Lcom/qualcomm/location/ulp/UnifiedLocationProvider$1;-><init>(Lcom/qualcomm/location/ulp/UnifiedLocationProvider;)V

    #@c
    iput-object v0, p0, Lcom/qualcomm/location/ulp/UnifiedLocationProvider;->mHandler:Landroid/os/Handler;

    #@e
    .line 52
    iput-object p1, p0, Lcom/qualcomm/location/ulp/UnifiedLocationProvider;->mContext:Landroid/content/Context;

    #@10
    .line 53
    new-instance v0, Lcom/qualcomm/location/ulp/UlpEngine;

    #@12
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@15
    move-result-object v1

    #@16
    invoke-direct {v0, p1, v1}, Lcom/qualcomm/location/ulp/UlpEngine;-><init>(Landroid/content/Context;Landroid/os/Looper;)V

    #@19
    iput-object v0, p0, Lcom/qualcomm/location/ulp/UnifiedLocationProvider;->mEngine:Lcom/qualcomm/location/ulp/UlpEngine;

    #@1b
    .line 54
    return-void
.end method

.method static synthetic access$000(Lcom/qualcomm/location/ulp/UnifiedLocationProvider;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/qualcomm/location/ulp/UnifiedLocationProvider;->logv(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/qualcomm/location/ulp/UnifiedLocationProvider;)Lcom/qualcomm/location/ulp/UlpEngine;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 26
    iget-object v0, p0, Lcom/qualcomm/location/ulp/UnifiedLocationProvider;->mEngine:Lcom/qualcomm/location/ulp/UlpEngine;

    #@2
    return-object v0
.end method

.method private logv(Ljava/lang/String;)V
    .registers 3
    .parameter "s"

    #@0
    .prologue
    .line 113
    sget-boolean v0, Lcom/qualcomm/location/ulp/UnifiedLocationProvider;->VERBOSE_DBG:Z

    #@2
    if-eqz v0, :cond_9

    #@4
    const-string v0, "UnifiedLocationProvider"

    #@6
    invoke-static {v0, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 114
    :cond_9
    return-void
.end method


# virtual methods
.method public onDisable()V
    .registers 3

    #@0
    .prologue
    .line 88
    iget-object v0, p0, Lcom/qualcomm/location/ulp/UnifiedLocationProvider;->mHandler:Landroid/os/Handler;

    #@2
    const/4 v1, 0x2

    #@3
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@6
    .line 89
    return-void
.end method

.method public onDump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 5
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 99
    iget-object v0, p0, Lcom/qualcomm/location/ulp/UnifiedLocationProvider;->mEngine:Lcom/qualcomm/location/ulp/UlpEngine;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/location/ulp/UlpEngine;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@5
    .line 100
    return-void
.end method

.method public onEnable()V
    .registers 3

    #@0
    .prologue
    .line 83
    iget-object v0, p0, Lcom/qualcomm/location/ulp/UnifiedLocationProvider;->mHandler:Landroid/os/Handler;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@6
    .line 84
    return-void
.end method

.method public onGetStatus(Landroid/os/Bundle;)I
    .registers 3
    .parameter "extras"

    #@0
    .prologue
    .line 104
    const/4 v0, 0x2

    #@1
    return v0
.end method

.method public onGetStatusUpdateTime()J
    .registers 3

    #@0
    .prologue
    .line 109
    const-wide/16 v0, 0x0

    #@2
    return-wide v0
.end method

.method public onSetRequest(Lcom/android/location/provider/ProviderRequestUnbundled;Landroid/os/WorkSource;)V
    .registers 6
    .parameter "request"
    .parameter "source"

    #@0
    .prologue
    .line 93
    iget-object v0, p0, Lcom/qualcomm/location/ulp/UnifiedLocationProvider;->mHandler:Landroid/os/Handler;

    #@2
    const/4 v1, 0x3

    #@3
    new-instance v2, Lcom/qualcomm/location/ulp/UnifiedLocationProvider$RequestWrapper;

    #@5
    invoke-direct {v2, p1, p2}, Lcom/qualcomm/location/ulp/UnifiedLocationProvider$RequestWrapper;-><init>(Lcom/android/location/provider/ProviderRequestUnbundled;Landroid/os/WorkSource;)V

    #@8
    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@f
    .line 94
    return-void
.end method
