.class public Lcom/qualcomm/location/ulp/UlpEngine;
.super Ljava/lang/Object;
.source "UlpEngine.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/location/ulp/UlpEngine$UlpEngineHandler;,
        Lcom/qualcomm/location/ulp/UlpEngine$GlobalSettingsObserver;,
        Lcom/qualcomm/location/ulp/UlpEngine$SecureSettingsObserver;,
        Lcom/qualcomm/location/ulp/UlpEngine$Callback;
    }
.end annotation


# static fields
.field private static final ENABLE:I = 0x1

.field public static final ENH_LOCATION_SERVICES_ENABLED:Ljava/lang/String; = "enhLocationServices_on"

.field public static final INDEX_ZERO:I = 0x0

.field private static final LOCATION_HAS_ACCURACY:I = 0x10

.field private static final LOCATION_HAS_ALTITUDE:I = 0x2

.field private static final LOCATION_HAS_BEARING:I = 0x8

.field private static final LOCATION_HAS_FLOOR_NUMBER:I = 0x80

.field private static final LOCATION_HAS_IS_INDOOR:I = 0x40

.field private static final LOCATION_HAS_LAT_LONG:I = 0x1

.field private static final LOCATION_HAS_MAP_INDEX:I = 0x200

.field private static final LOCATION_HAS_MAP_URL:I = 0x100

.field private static final LOCATION_HAS_SOURCE_INFO:I = 0x20

.field private static final LOCATION_HAS_SPEED:I = 0x4

.field private static final LOCATION_INVALID:I = 0x0

.field private static final REQUEST_NETWORK_LOCATION:I = 0x5

.field private static final REQUEST_PHONE_CONTEXT_SETTINGS:I = 0x3

.field private static final SET_REQUEST:I = 0x2

.field private static final TAG:Ljava/lang/String; = "UlpEngine"

.field public static final ULP_ADD_CRITERIA:I = 0x1

.field private static final ULP_LOCATION_IS_FROM_GNSS:I = 0x2

.field private static final ULP_LOCATION_IS_FROM_HYBRID:I = 0x1

.field private static final ULP_NETWORK_POSITION_SRC_CELL:I = 0x2

.field private static final ULP_NETWORK_POSITION_SRC_UNKNOWN:I = 0xff

.field private static final ULP_NETWORK_POSITION_SRC_WIFI:I = 0x1

.field private static final ULP_NETWORK_POS_GET_LAST_KNOWN_LOCATION_REQUEST:I = 0x3

.field private static final ULP_NETWORK_POS_START_PERIODIC_REQUEST:I = 0x2

.field private static final ULP_NETWORK_POS_STATUS_REQUEST:I = 0x1

.field private static final ULP_NETWORK_POS_STOP_REQUEST:I = 0x4

.field private static final ULP_PHONE_CONTEXT_AGPS_OFF:I = 0x8

.field private static final ULP_PHONE_CONTEXT_AGPS_ON:I = 0x4

.field private static final ULP_PHONE_CONTEXT_AGPS_SETTING:I = 0x10

.field private static final ULP_PHONE_CONTEXT_BATTERY_CHARGING_STATE:I = 0x8

.field private static final ULP_PHONE_CONTEXT_CELL_BASED_POSITION_OFF:I = 0x20

.field private static final ULP_PHONE_CONTEXT_CELL_BASED_POSITION_ON:I = 0x10

.field private static final ULP_PHONE_CONTEXT_ENH_LOCATION_SERVICES_OFF:I = 0x800

.field private static final ULP_PHONE_CONTEXT_ENH_LOCATION_SERVICES_ON:I = 0x400

.field private static final ULP_PHONE_CONTEXT_ENH_LOCATION_SERVICES_SETTING:I = 0x20

.field private static final ULP_PHONE_CONTEXT_GPS_OFF:I = 0x2

.field private static final ULP_PHONE_CONTEXT_GPS_ON:I = 0x1

.field private static final ULP_PHONE_CONTEXT_GPS_SETTING:I = 0x1

.field private static final ULP_PHONE_CONTEXT_NETWORK_POSITION_SETTING:I = 0x2

.field private static final ULP_PHONE_CONTEXT_REQUEST_TYPE_ONCHANGE:I = 0x2

.field private static final ULP_PHONE_CONTEXT_REQUEST_TYPE_SINGLE:I = 0x1

.field private static final ULP_PHONE_CONTEXT_UPDATE_TYPE_ONCHANGE:I = 0x2

.field private static final ULP_PHONE_CONTEXT_UPDATE_TYPE_SINGLE:I = 0x1

.field private static final ULP_PHONE_CONTEXT_WIFI_OFF:I = 0x80

.field private static final ULP_PHONE_CONTEXT_WIFI_ON:I = 0x40

.field private static final ULP_PHONE_CONTEXT_WIFI_SETTING:I = 0x4

.field private static final ULP_PROVIDER_SOURCE_GNSS:I = 0x1

.field private static final ULP_PROVIDER_SOURCE_HYBRID:I = 0x2

.field public static final ULP_REMOVE_CRITERIA:I = 0x2

.field private static final UPDATE_NATIVE_PHONE_CONTEXT_SETTINGS:I = 0x4

.field private static final UPDATE_NETWORK_LOCATION:I = 0x6

.field private static final VERBOSE_DBG:Z


# instance fields
.field private volatile mAgpsSetting:Z

.field private mCallback:Lcom/qualcomm/location/ulp/UlpEngine$Callback;

.field private final mContext:Landroid/content/Context;

.field private mEnabled:Z

.field private volatile mEnhServicesSetting:Z

.field private mGlobalSettings:Landroid/content/ContentQueryMap;

.field private volatile mGpsSetting:Z

.field private mHandler:Landroid/os/Handler;

.field private mLocMgr:Landroid/location/LocationManager;

.field private final mLooper:Landroid/os/Looper;

.field private mNetworkLocationListener:Landroid/location/LocationListener;

.field private volatile mNetworkProvSetting:Z

.field private mRequest:Lcom/android/location/provider/ProviderRequestUnbundled;

.field private mRequestActive:Lcom/android/location/provider/ProviderRequestUnbundled;

.field private volatile mRequestContextType:I

.field private volatile mRequestType:I

.field private mSecureSettings:Landroid/content/ContentQueryMap;

.field private volatile mWifiSetting:Z


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 69
    const-string v0, "UlpEngine"

    #@2
    const/4 v1, 0x2

    #@3
    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v0

    #@7
    sput-boolean v0, Lcom/qualcomm/location/ulp/UlpEngine;->VERBOSE_DBG:Z

    #@11
    .line 884
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;)V
    .registers 14
    .parameter "context"
    .parameter "looper"

    #@0
    .prologue
    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 108
    const/4 v1, 0x0

    #@4
    iput-boolean v1, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mGpsSetting:Z

    #@6
    .line 109
    const/4 v1, 0x0

    #@7
    iput-boolean v1, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mAgpsSetting:Z

    #@9
    .line 110
    const/4 v1, 0x0

    #@a
    iput-boolean v1, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mNetworkProvSetting:Z

    #@c
    .line 111
    const/4 v1, 0x0

    #@d
    iput-boolean v1, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mWifiSetting:Z

    #@f
    .line 112
    const/4 v1, 0x0

    #@10
    iput-boolean v1, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mEnhServicesSetting:Z

    #@12
    .line 141
    const/4 v1, 0x0

    #@13
    iput v1, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequestType:I

    #@15
    .line 142
    const/4 v1, 0x0

    #@16
    iput v1, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequestContextType:I

    #@18
    .line 459
    new-instance v1, Lcom/qualcomm/location/ulp/UlpEngine$1;

    #@1a
    invoke-direct {v1, p0}, Lcom/qualcomm/location/ulp/UlpEngine$1;-><init>(Lcom/qualcomm/location/ulp/UlpEngine;)V

    #@1d
    iput-object v1, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mNetworkLocationListener:Landroid/location/LocationListener;

    #@1f
    .line 155
    iput-object p1, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mContext:Landroid/content/Context;

    #@21
    .line 156
    iput-object p2, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mLooper:Landroid/os/Looper;

    #@23
    .line 157
    sget-boolean v1, Lcom/qualcomm/location/ulp/UlpEngine;->VERBOSE_DBG:Z

    #@25
    if-eqz v1, :cond_2e

    #@27
    const-string v1, "UlpEngine"

    #@29
    const-string v2, "Create UlpEngine"

    #@2b
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 159
    :cond_2e
    new-instance v1, Lcom/qualcomm/location/ulp/UlpEngine$UlpEngineHandler;

    #@30
    iget-object v2, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mLooper:Landroid/os/Looper;

    #@32
    invoke-direct {v1, p0, v2}, Lcom/qualcomm/location/ulp/UlpEngine$UlpEngineHandler;-><init>(Lcom/qualcomm/location/ulp/UlpEngine;Landroid/os/Looper;)V

    #@35
    iput-object v1, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mHandler:Landroid/os/Handler;

    #@37
    .line 160
    iget-object v1, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mContext:Landroid/content/Context;

    #@39
    const-string v2, "location"

    #@3b
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@3e
    move-result-object v1

    #@3f
    check-cast v1, Landroid/location/LocationManager;

    #@41
    iput-object v1, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mLocMgr:Landroid/location/LocationManager;

    #@43
    .line 162
    iget-object v1, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mContext:Landroid/content/Context;

    #@45
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@48
    move-result-object v0

    #@49
    .line 163
    .local v0, resolver:Landroid/content/ContentResolver;
    sget-object v1, Landroid/provider/Settings$Secure;->CONTENT_URI:Landroid/net/Uri;

    #@4b
    const/4 v2, 0x2

    #@4c
    new-array v2, v2, [Ljava/lang/String;

    #@4e
    const/4 v3, 0x0

    #@4f
    const-string v4, "name"

    #@51
    aput-object v4, v2, v3

    #@53
    const/4 v3, 0x1

    #@54
    const-string v4, "value"

    #@56
    aput-object v4, v2, v3

    #@58
    const-string v3, "(name=?) or (name=?) "

    #@5a
    const/4 v4, 0x2

    #@5b
    new-array v4, v4, [Ljava/lang/String;

    #@5d
    const/4 v5, 0x0

    #@5e
    const-string v10, "location_providers_allowed"

    #@60
    aput-object v10, v4, v5

    #@62
    const/4 v5, 0x1

    #@63
    const-string v10, "enhLocationServices_on"

    #@65
    aput-object v10, v4, v5

    #@67
    const/4 v5, 0x0

    #@68
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@6b
    move-result-object v8

    #@6c
    .line 170
    .local v8, secureSettingsCursor:Landroid/database/Cursor;
    new-instance v1, Landroid/content/ContentQueryMap;

    #@6e
    const-string v2, "name"

    #@70
    const/4 v3, 0x1

    #@71
    iget-object v4, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mHandler:Landroid/os/Handler;

    #@73
    invoke-direct {v1, v8, v2, v3, v4}, Landroid/content/ContentQueryMap;-><init>(Landroid/database/Cursor;Ljava/lang/String;ZLandroid/os/Handler;)V

    #@76
    iput-object v1, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mSecureSettings:Landroid/content/ContentQueryMap;

    #@78
    .line 171
    new-instance v9, Lcom/qualcomm/location/ulp/UlpEngine$SecureSettingsObserver;

    #@7a
    const/4 v1, 0x0

    #@7b
    invoke-direct {v9, p0, v1}, Lcom/qualcomm/location/ulp/UlpEngine$SecureSettingsObserver;-><init>(Lcom/qualcomm/location/ulp/UlpEngine;Lcom/qualcomm/location/ulp/UlpEngine$1;)V

    #@7e
    .line 172
    .local v9, secureSettingsObserver:Lcom/qualcomm/location/ulp/UlpEngine$SecureSettingsObserver;
    iget-object v1, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mSecureSettings:Landroid/content/ContentQueryMap;

    #@80
    invoke-virtual {v1, v9}, Landroid/content/ContentQueryMap;->addObserver(Ljava/util/Observer;)V

    #@83
    .line 174
    sget-object v1, Landroid/provider/Settings$Global;->CONTENT_URI:Landroid/net/Uri;

    #@85
    const/4 v2, 0x2

    #@86
    new-array v2, v2, [Ljava/lang/String;

    #@88
    const/4 v3, 0x0

    #@89
    const-string v4, "name"

    #@8b
    aput-object v4, v2, v3

    #@8d
    const/4 v3, 0x1

    #@8e
    const-string v4, "value"

    #@90
    aput-object v4, v2, v3

    #@92
    const-string v3, "(name=?) or (name=?) "

    #@94
    const/4 v4, 0x2

    #@95
    new-array v4, v4, [Ljava/lang/String;

    #@97
    const/4 v5, 0x0

    #@98
    const-string v10, "wifi_on"

    #@9a
    aput-object v10, v4, v5

    #@9c
    const/4 v5, 0x1

    #@9d
    const-string v10, "assisted_gps_enabled"

    #@9f
    aput-object v10, v4, v5

    #@a1
    const/4 v5, 0x0

    #@a2
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@a5
    move-result-object v6

    #@a6
    .line 180
    .local v6, globalSettingsCursor:Landroid/database/Cursor;
    new-instance v1, Landroid/content/ContentQueryMap;

    #@a8
    const-string v2, "name"

    #@aa
    const/4 v3, 0x1

    #@ab
    iget-object v4, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mHandler:Landroid/os/Handler;

    #@ad
    invoke-direct {v1, v6, v2, v3, v4}, Landroid/content/ContentQueryMap;-><init>(Landroid/database/Cursor;Ljava/lang/String;ZLandroid/os/Handler;)V

    #@b0
    iput-object v1, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mGlobalSettings:Landroid/content/ContentQueryMap;

    #@b2
    .line 181
    new-instance v7, Lcom/qualcomm/location/ulp/UlpEngine$GlobalSettingsObserver;

    #@b4
    const/4 v1, 0x0

    #@b5
    invoke-direct {v7, p0, v1}, Lcom/qualcomm/location/ulp/UlpEngine$GlobalSettingsObserver;-><init>(Lcom/qualcomm/location/ulp/UlpEngine;Lcom/qualcomm/location/ulp/UlpEngine$1;)V

    #@b8
    .line 182
    .local v7, globalSettingsObserver:Lcom/qualcomm/location/ulp/UlpEngine$GlobalSettingsObserver;
    iget-object v1, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mGlobalSettings:Landroid/content/ContentQueryMap;

    #@ba
    invoke-virtual {v1, v7}, Landroid/content/ContentQueryMap;->addObserver(Ljava/util/Observer;)V

    #@bd
    .line 183
    return-void
.end method

.method static synthetic access$200()Z
    .registers 1

    #@0
    .prologue
    .line 61
    sget-boolean v0, Lcom/qualcomm/location/ulp/UlpEngine;->VERBOSE_DBG:Z

    #@2
    return v0
.end method

.method static synthetic access$300(Lcom/qualcomm/location/ulp/UlpEngine;ZZZ)Z
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 61
    invoke-direct {p0, p1, p2, p3}, Lcom/qualcomm/location/ulp/UlpEngine;->updateSecureSettings(ZZZ)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$400(Lcom/qualcomm/location/ulp/UlpEngine;ZZ)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Lcom/qualcomm/location/ulp/UlpEngine;->updateGlobalSettings(ZZ)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$500(Lcom/qualcomm/location/ulp/UlpEngine;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 61
    invoke-direct {p0}, Lcom/qualcomm/location/ulp/UlpEngine;->handleEnable()V

    #@3
    return-void
.end method

.method static synthetic access$600(Lcom/qualcomm/location/ulp/UlpEngine;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Lcom/qualcomm/location/ulp/UlpEngine;->handleNativePhoneContextRequest(II)V

    #@3
    return-void
.end method

.method static synthetic access$700(Lcom/qualcomm/location/ulp/UlpEngine;ILandroid/os/Bundle;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Lcom/qualcomm/location/ulp/UlpEngine;->handleNativePhoneContextUpdate(ILandroid/os/Bundle;)V

    #@3
    return-void
.end method

.method static synthetic access$800(Lcom/qualcomm/location/ulp/UlpEngine;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Lcom/qualcomm/location/ulp/UlpEngine;->handleNativeNetworkLocationRequest(II)V

    #@3
    return-void
.end method

.method static synthetic access$900(Lcom/qualcomm/location/ulp/UlpEngine;Landroid/location/Location;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/qualcomm/location/ulp/UlpEngine;->handleNetworkLocationUpdate(Landroid/location/Location;)V

    #@3
    return-void
.end method

.method private handleEnable()V
    .registers 3

    #@0
    .prologue
    .line 212
    sget-boolean v0, Lcom/qualcomm/location/ulp/UlpEngine;->VERBOSE_DBG:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    const-string v0, "UlpEngine"

    #@6
    const-string v1, "handleEnable"

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 213
    :cond_b
    iget-boolean v0, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mEnabled:Z

    #@d
    if-eqz v0, :cond_10

    #@f
    .line 216
    :goto_f
    return-void

    #@10
    .line 214
    :cond_10
    const/4 v0, 0x1

    #@11
    iput-boolean v0, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mEnabled:Z

    #@13
    .line 215
    invoke-direct {p0}, Lcom/qualcomm/location/ulp/UlpEngine;->updateRequirements()V

    #@16
    goto :goto_f
.end method

.method private handleNativeNetworkLocationRequest(II)V
    .registers 10
    .parameter "type"
    .parameter "interval"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 478
    packed-switch p1, :pswitch_data_70

    #@4
    .line 493
    const-string v0, "UlpEngine"

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "requestNetworkLocation. Inccorect request sent in: "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 497
    :goto_1c
    return-void

    #@1d
    .line 480
    :pswitch_1d
    sget-boolean v0, Lcom/qualcomm/location/ulp/UlpEngine;->VERBOSE_DBG:Z

    #@1f
    if-eqz v0, :cond_28

    #@21
    const-string v0, "UlpEngine"

    #@23
    const-string v1, "requestNetworkLocation NLP start from GP"

    #@25
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 481
    :cond_28
    iget-object v0, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mLocMgr:Landroid/location/LocationManager;

    #@2a
    const-string v1, "network"

    #@2c
    int-to-long v2, p2

    #@2d
    const/4 v4, 0x0

    #@2e
    iget-object v5, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mNetworkLocationListener:Landroid/location/LocationListener;

    #@30
    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    #@33
    goto :goto_1c

    #@34
    .line 484
    :pswitch_34
    iget-object v0, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mLocMgr:Landroid/location/LocationManager;

    #@36
    const-string v1, "LocationManager.NETWORK_PROVIDER"

    #@38
    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    #@3b
    move-result-object v6

    #@3c
    .line 485
    .local v6, location:Landroid/location/Location;
    sget-boolean v0, Lcom/qualcomm/location/ulp/UlpEngine;->VERBOSE_DBG:Z

    #@3e
    if-eqz v0, :cond_58

    #@40
    const-string v0, "UlpEngine"

    #@42
    new-instance v1, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v2, "requestNetworkLocation NLP last known location from GP:"

    #@49
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v1

    #@4d
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v1

    #@51
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v1

    #@55
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@58
    .line 486
    :cond_58
    const/4 v0, 0x6

    #@59
    invoke-virtual {p0, v0, v3, v3, v6}, Lcom/qualcomm/location/ulp/UlpEngine;->sendMessage(IIILjava/lang/Object;)V

    #@5c
    goto :goto_1c

    #@5d
    .line 489
    .end local v6           #location:Landroid/location/Location;
    :pswitch_5d
    sget-boolean v0, Lcom/qualcomm/location/ulp/UlpEngine;->VERBOSE_DBG:Z

    #@5f
    if-eqz v0, :cond_68

    #@61
    const-string v0, "UlpEngine"

    #@63
    const-string v1, "requestNetworkLocation NLP stop from GP"

    #@65
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@68
    .line 490
    :cond_68
    iget-object v0, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mLocMgr:Landroid/location/LocationManager;

    #@6a
    iget-object v1, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mNetworkLocationListener:Landroid/location/LocationListener;

    #@6c
    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    #@6f
    goto :goto_1c

    #@70
    .line 478
    :pswitch_data_70
    .packed-switch 0x2
        :pswitch_1d
        :pswitch_34
        :pswitch_5d
    .end packed-switch
.end method

.method private handleNativePhoneContextRequest(II)V
    .registers 6
    .parameter "contextType"
    .parameter "requestType"

    #@0
    .prologue
    .line 512
    iput p1, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequestContextType:I

    #@2
    .line 513
    iput p2, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequestType:I

    #@4
    .line 514
    sget-boolean v0, Lcom/qualcomm/location/ulp/UlpEngine;->VERBOSE_DBG:Z

    #@6
    if-eqz v0, :cond_2e

    #@8
    const-string v0, "UlpEngine"

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "handleNativePhoneContextRequest invoked from native layer with mRequestContextType: "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    iget v2, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequestContextType:I

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    const-string v2, " mRequestType:"

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    iget v2, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequestType:I

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 516
    :cond_2e
    const/4 v0, 0x1

    #@2f
    const/4 v1, 0x0

    #@30
    invoke-direct {p0, v0, v1}, Lcom/qualcomm/location/ulp/UlpEngine;->handleNativePhoneContextUpdate(ILandroid/os/Bundle;)V

    #@33
    .line 517
    return-void
.end method

.method private handleNativePhoneContextUpdate(ILandroid/os/Bundle;)V
    .registers 26
    .parameter "updateType"
    .parameter "settingsValues"

    #@0
    .prologue
    .line 521
    const/4 v3, 0x0

    #@1
    .line 523
    .local v3, currentContextType:I
    const/4 v5, 0x0

    #@2
    .local v5, currentAgpsSetting:Z
    const/4 v7, 0x0

    #@3
    .line 524
    .local v7, currentWifiSetting:Z
    const/4 v4, 0x0

    #@4
    .local v4, currentGpsSetting:Z
    const/4 v6, 0x0

    #@5
    .line 525
    .local v6, currentNetworkProvSetting:Z
    const/4 v8, 0x0

    #@6
    .local v8, currentBatteryCharging:Z
    const/4 v9, 0x0

    #@7
    .line 526
    .local v9, currentEnhLocationServicesSetting:Z
    const/4 v15, 0x0

    #@8
    .local v15, wasAgpsSettingAvailable:Z
    const/16 v20, 0x0

    #@a
    .line 527
    .local v20, wasWifiSettingAvailable:Z
    const/16 v18, 0x0

    #@c
    .local v18, wasGpsSettingAvailable:Z
    const/16 v19, 0x0

    #@e
    .line 528
    .local v19, wasNetworkProviderSettingAvailable:Z
    const/16 v16, 0x0

    #@10
    .local v16, wasBatteryChargingAvailable:Z
    const/16 v17, 0x0

    #@12
    .line 530
    .local v17, wasEnhLocationServicesSettingAvailable:Z
    move-object/from16 v0, p0

    #@14
    iget-object v2, v0, Lcom/qualcomm/location/ulp/UlpEngine;->mContext:Landroid/content/Context;

    #@16
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@19
    move-result-object v14

    #@1a
    .line 531
    .local v14, resolver:Landroid/content/ContentResolver;
    sget-boolean v2, Lcom/qualcomm/location/ulp/UlpEngine;->VERBOSE_DBG:Z

    #@1c
    if-eqz v2, :cond_5c

    #@1e
    .line 532
    const-string v2, "UlpEngine"

    #@20
    new-instance v21, Ljava/lang/StringBuilder;

    #@22
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v22, "handleNativePhoneContextUpdate called. updateType: "

    #@27
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v21

    #@2b
    move-object/from16 v0, v21

    #@2d
    move/from16 v1, p1

    #@2f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v21

    #@33
    const-string v22, " mRequestContextType: "

    #@35
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v21

    #@39
    move-object/from16 v0, p0

    #@3b
    iget v0, v0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequestContextType:I

    #@3d
    move/from16 v22, v0

    #@3f
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@42
    move-result-object v21

    #@43
    const-string v22, " mRequestType: "

    #@45
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v21

    #@49
    move-object/from16 v0, p0

    #@4b
    iget v0, v0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequestType:I

    #@4d
    move/from16 v22, v0

    #@4f
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@52
    move-result-object v21

    #@53
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v21

    #@57
    move-object/from16 v0, v21

    #@59
    invoke-static {v2, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@5c
    .line 536
    :cond_5c
    move-object/from16 v0, p0

    #@5e
    iget v2, v0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequestContextType:I

    #@60
    if-nez v2, :cond_70

    #@62
    .line 537
    sget-boolean v2, Lcom/qualcomm/location/ulp/UlpEngine;->VERBOSE_DBG:Z

    #@64
    if-eqz v2, :cond_6f

    #@66
    const-string v2, "UlpEngine"

    #@68
    const-string v21, "handleNativePhoneContextUpdate. Update obtained before request. Ignoring"

    #@6a
    move-object/from16 v0, v21

    #@6c
    invoke-static {v2, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@6f
    .line 714
    :cond_6f
    :goto_6f
    return-void

    #@70
    .line 542
    :cond_70
    :try_start_70
    move-object/from16 v0, p0

    #@72
    iget v2, v0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequestContextType:I

    #@74
    and-int/lit8 v2, v2, 0x1

    #@76
    const/16 v21, 0x1

    #@78
    move/from16 v0, v21

    #@7a
    if-ne v2, v0, :cond_89

    #@7c
    .line 544
    const/4 v2, 0x1

    #@7d
    move/from16 v0, p1

    #@7f
    if-ne v0, v2, :cond_1b1

    #@81
    .line 545
    const-string v2, "gps"

    #@83
    invoke-static {v14, v2}, Landroid/provider/Settings$Secure;->isLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    #@86
    move-result v4

    #@87
    .line 547
    const/16 v18, 0x1

    #@89
    .line 558
    :cond_89
    :goto_89
    move-object/from16 v0, p0

    #@8b
    iget v2, v0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequestContextType:I

    #@8d
    and-int/lit8 v2, v2, 0x10

    #@8f
    const/16 v21, 0x10

    #@91
    move/from16 v0, v21

    #@93
    if-ne v2, v0, :cond_a8

    #@95
    .line 560
    const/4 v2, 0x1

    #@96
    move/from16 v0, p1

    #@98
    if-ne v0, v2, :cond_1ca

    #@9a
    .line 561
    const-string v2, "assisted_gps_enabled"

    #@9c
    invoke-static {v14, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    #@9f
    move-result v2

    #@a0
    const/16 v21, 0x1

    #@a2
    move/from16 v0, v21

    #@a4
    if-ne v2, v0, :cond_1c7

    #@a6
    const/4 v5, 0x1

    #@a7
    .line 563
    :goto_a7
    const/4 v15, 0x1

    #@a8
    .line 575
    :cond_a8
    :goto_a8
    move-object/from16 v0, p0

    #@aa
    iget v2, v0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequestContextType:I

    #@ac
    and-int/lit8 v2, v2, 0x2

    #@ae
    const/16 v21, 0x2

    #@b0
    move/from16 v0, v21

    #@b2
    if-ne v2, v0, :cond_db

    #@b4
    .line 578
    move-object/from16 v0, p0

    #@b6
    iget-object v2, v0, Lcom/qualcomm/location/ulp/UlpEngine;->mLocMgr:Landroid/location/LocationManager;

    #@b8
    invoke-virtual {v2}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    #@bb
    move-result-object v13

    #@bc
    .line 579
    .local v13, providers:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "network"

    #@be
    invoke-interface {v13, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@c1
    move-result v2

    #@c2
    const/16 v21, 0x1

    #@c4
    move/from16 v0, v21

    #@c6
    if-ne v2, v0, :cond_1df

    #@c8
    const/4 v12, 0x1

    #@c9
    .line 580
    .local v12, networkLocProvAvailable:Z
    :goto_c9
    const/4 v2, 0x1

    #@ca
    move/from16 v0, p1

    #@cc
    if-ne v0, v2, :cond_1e5

    #@ce
    .line 581
    const-string v2, "network"

    #@d0
    invoke-static {v14, v2}, Landroid/provider/Settings$Secure;->isLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    #@d3
    move-result v2

    #@d4
    if-eqz v2, :cond_1e2

    #@d6
    if-eqz v12, :cond_1e2

    #@d8
    const/4 v6, 0x1

    #@d9
    .line 584
    :goto_d9
    const/16 v19, 0x1

    #@db
    .line 597
    .end local v12           #networkLocProvAvailable:Z
    .end local v13           #providers:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :cond_db
    :goto_db
    move-object/from16 v0, p0

    #@dd
    iget v2, v0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequestContextType:I

    #@df
    and-int/lit8 v2, v2, 0x4

    #@e1
    const/16 v21, 0x4

    #@e3
    move/from16 v0, v21

    #@e5
    if-ne v2, v0, :cond_107

    #@e7
    .line 600
    const/4 v2, 0x1

    #@e8
    move/from16 v0, p1

    #@ea
    if-ne v0, v2, :cond_205

    #@ec
    .line 602
    const-string v2, "wifi_on"

    #@ee
    invoke-static {v14, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    #@f1
    move-result v2

    #@f2
    const/16 v21, 0x1

    #@f4
    move/from16 v0, v21

    #@f6
    if-eq v2, v0, :cond_104

    #@f8
    const-string v2, "wifi_on"

    #@fa
    invoke-static {v14, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    #@fd
    move-result v2

    #@fe
    const/16 v21, 0x2

    #@100
    move/from16 v0, v21

    #@102
    if-ne v2, v0, :cond_202

    #@104
    :cond_104
    const/4 v7, 0x1

    #@105
    .line 604
    :goto_105
    const/16 v20, 0x1

    #@107
    .line 616
    :cond_107
    :goto_107
    move-object/from16 v0, p0

    #@109
    iget v2, v0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequestContextType:I

    #@10b
    and-int/lit8 v2, v2, 0x20

    #@10d
    const/16 v21, 0x20

    #@10f
    move/from16 v0, v21

    #@111
    if-ne v2, v0, :cond_128

    #@113
    .line 619
    const/4 v2, 0x1

    #@114
    move/from16 v0, p1

    #@116
    if-ne v0, v2, :cond_232

    #@118
    .line 620
    const-string v2, "enhLocationServices_on"

    #@11a
    invoke-static {v14, v2}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@11d
    move-result-object v10

    #@11e
    .line 623
    .local v10, currentEnhLocationServicesSettingString:Ljava/lang/String;
    if-eqz v10, :cond_21b

    #@120
    .line 624
    const-string v2, "1"

    #@122
    invoke-virtual {v10, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_125
    .catch Ljava/lang/Exception; {:try_start_70 .. :try_end_125} :catch_226

    #@125
    move-result v9

    #@126
    .line 629
    :goto_126
    const/16 v17, 0x1

    #@128
    .line 644
    .end local v10           #currentEnhLocationServicesSettingString:Ljava/lang/String;
    :cond_128
    :goto_128
    move-object/from16 v0, p0

    #@12a
    iget v3, v0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequestContextType:I

    #@12c
    .line 648
    if-nez v18, :cond_248

    #@12e
    .line 649
    and-int/lit8 v3, v3, -0x2

    #@130
    .line 659
    :goto_130
    if-nez v15, :cond_25b

    #@132
    .line 660
    and-int/lit8 v3, v3, -0x11

    #@134
    .line 670
    :goto_134
    if-nez v19, :cond_26e

    #@136
    .line 671
    and-int/lit8 v3, v3, -0x3

    #@138
    .line 680
    :goto_138
    if-nez v20, :cond_281

    #@13a
    .line 681
    and-int/lit8 v3, v3, -0x5

    #@13c
    .line 690
    :goto_13c
    if-nez v17, :cond_294

    #@13e
    .line 691
    and-int/lit8 v3, v3, -0x21

    #@140
    .line 702
    :goto_140
    and-int/lit8 v3, v3, -0x9

    #@142
    move-object/from16 v2, p0

    #@144
    .line 704
    invoke-direct/range {v2 .. v9}, Lcom/qualcomm/location/ulp/UlpEngine;->native_ue_update_settings(IZZZZZZ)Z

    #@147
    .line 707
    sget-boolean v2, Lcom/qualcomm/location/ulp/UlpEngine;->VERBOSE_DBG:Z

    #@149
    if-eqz v2, :cond_6f

    #@14b
    .line 708
    const-string v2, "UlpEngine"

    #@14d
    new-instance v21, Ljava/lang/StringBuilder;

    #@14f
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@152
    const-string v22, "After calling native_ue_update_settings. currentContextType: "

    #@154
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@157
    move-result-object v21

    #@158
    move-object/from16 v0, v21

    #@15a
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15d
    move-result-object v21

    #@15e
    const-string v22, " sGpsSetting: "

    #@160
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@163
    move-result-object v21

    #@164
    move-object/from16 v0, v21

    #@166
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@169
    move-result-object v21

    #@16a
    const-string v22, "currentAgpsSetting: "

    #@16c
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16f
    move-result-object v21

    #@170
    move-object/from16 v0, v21

    #@172
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@175
    move-result-object v21

    #@176
    const-string v22, " currentNetworkProvSetting: "

    #@178
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17b
    move-result-object v21

    #@17c
    move-object/from16 v0, v21

    #@17e
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@181
    move-result-object v21

    #@182
    const-string v22, "currentWifiSetting: "

    #@184
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@187
    move-result-object v21

    #@188
    move-object/from16 v0, v21

    #@18a
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@18d
    move-result-object v21

    #@18e
    const-string v22, " currentBatteryCharging: "

    #@190
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@193
    move-result-object v21

    #@194
    move-object/from16 v0, v21

    #@196
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@199
    move-result-object v21

    #@19a
    const-string v22, " currentEnhLocationServicesSetting: "

    #@19c
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19f
    move-result-object v21

    #@1a0
    move-object/from16 v0, v21

    #@1a2
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1a5
    move-result-object v21

    #@1a6
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a9
    move-result-object v21

    #@1aa
    move-object/from16 v0, v21

    #@1ac
    invoke-static {v2, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1af
    goto/16 :goto_6f

    #@1b1
    .line 550
    :cond_1b1
    :try_start_1b1
    const-string v2, "gpsSetting"

    #@1b3
    move-object/from16 v0, p2

    #@1b5
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    #@1b8
    move-result v2

    #@1b9
    if-eqz v2, :cond_89

    #@1bb
    .line 552
    const/16 v18, 0x1

    #@1bd
    .line 553
    const-string v2, "gpsSetting"

    #@1bf
    move-object/from16 v0, p2

    #@1c1
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@1c4
    move-result v4

    #@1c5
    goto/16 :goto_89

    #@1c7
    .line 561
    :cond_1c7
    const/4 v5, 0x0

    #@1c8
    goto/16 :goto_a7

    #@1ca
    .line 566
    :cond_1ca
    const-string v2, "agpsSetting"

    #@1cc
    move-object/from16 v0, p2

    #@1ce
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    #@1d1
    move-result v2

    #@1d2
    if-eqz v2, :cond_a8

    #@1d4
    .line 568
    const/4 v15, 0x1

    #@1d5
    .line 569
    const-string v2, "agpsSetting"

    #@1d7
    move-object/from16 v0, p2

    #@1d9
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@1dc
    move-result v5

    #@1dd
    goto/16 :goto_a8

    #@1df
    .line 579
    .restart local v13       #providers:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :cond_1df
    const/4 v12, 0x0

    #@1e0
    goto/16 :goto_c9

    #@1e2
    .line 581
    .restart local v12       #networkLocProvAvailable:Z
    :cond_1e2
    const/4 v6, 0x0

    #@1e3
    goto/16 :goto_d9

    #@1e5
    .line 587
    :cond_1e5
    const-string v2, "networkProvSetting"

    #@1e7
    move-object/from16 v0, p2

    #@1e9
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    #@1ec
    move-result v2

    #@1ed
    if-eqz v2, :cond_db

    #@1ef
    .line 589
    const/16 v19, 0x1

    #@1f1
    .line 590
    const-string v2, "networkProvSetting"

    #@1f3
    move-object/from16 v0, p2

    #@1f5
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@1f8
    move-result v2

    #@1f9
    if-eqz v2, :cond_200

    #@1fb
    if-eqz v12, :cond_200

    #@1fd
    const/4 v6, 0x1

    #@1fe
    :goto_1fe
    goto/16 :goto_db

    #@200
    :cond_200
    const/4 v6, 0x0

    #@201
    goto :goto_1fe

    #@202
    .line 602
    .end local v12           #networkLocProvAvailable:Z
    .end local v13           #providers:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :cond_202
    const/4 v7, 0x0

    #@203
    goto/16 :goto_105

    #@205
    .line 607
    :cond_205
    const-string v2, "wifiSetting"

    #@207
    move-object/from16 v0, p2

    #@209
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    #@20c
    move-result v2

    #@20d
    if-eqz v2, :cond_107

    #@20f
    .line 609
    const/16 v20, 0x1

    #@211
    .line 610
    const-string v2, "wifiSetting"

    #@213
    move-object/from16 v0, p2

    #@215
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@218
    move-result v7

    #@219
    goto/16 :goto_107

    #@21b
    .line 626
    .restart local v10       #currentEnhLocationServicesSettingString:Ljava/lang/String;
    :cond_21b
    const-string v2, "UlpEngine"

    #@21d
    const-string v21, "Got null pinter for call to Settings.Secure.getString(resolver,ENH_LOCATION_SERVICES_ENABLED)"

    #@21f
    move-object/from16 v0, v21

    #@221
    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_224
    .catch Ljava/lang/Exception; {:try_start_1b1 .. :try_end_224} :catch_226

    #@224
    goto/16 :goto_126

    #@226
    .line 639
    .end local v10           #currentEnhLocationServicesSettingString:Ljava/lang/String;
    :catch_226
    move-exception v11

    #@227
    .line 640
    .local v11, e:Ljava/lang/Exception;
    const-string v2, "UlpEngine"

    #@229
    const-string v21, "Exception in handleNativePhoneContextUpdate:"

    #@22b
    move-object/from16 v0, v21

    #@22d
    invoke-static {v2, v0, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@230
    goto/16 :goto_128

    #@232
    .line 632
    .end local v11           #e:Ljava/lang/Exception;
    :cond_232
    :try_start_232
    const-string v2, "enhLocationServicesSetting"

    #@234
    move-object/from16 v0, p2

    #@236
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    #@239
    move-result v2

    #@23a
    if-eqz v2, :cond_128

    #@23c
    .line 634
    const/16 v17, 0x1

    #@23e
    .line 635
    const-string v2, "enhLocationServicesSetting"

    #@240
    move-object/from16 v0, p2

    #@242
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z
    :try_end_245
    .catch Ljava/lang/Exception; {:try_start_232 .. :try_end_245} :catch_226

    #@245
    move-result v9

    #@246
    goto/16 :goto_128

    #@248
    .line 652
    :cond_248
    const/4 v2, 0x2

    #@249
    move/from16 v0, p1

    #@24b
    if-ne v0, v2, :cond_255

    #@24d
    move-object/from16 v0, p0

    #@24f
    iget-boolean v2, v0, Lcom/qualcomm/location/ulp/UlpEngine;->mGpsSetting:Z

    #@251
    if-ne v4, v2, :cond_255

    #@253
    .line 654
    and-int/lit8 v3, v3, -0x2

    #@255
    .line 656
    :cond_255
    move-object/from16 v0, p0

    #@257
    iput-boolean v4, v0, Lcom/qualcomm/location/ulp/UlpEngine;->mGpsSetting:Z

    #@259
    goto/16 :goto_130

    #@25b
    .line 663
    :cond_25b
    const/4 v2, 0x2

    #@25c
    move/from16 v0, p1

    #@25e
    if-ne v0, v2, :cond_268

    #@260
    move-object/from16 v0, p0

    #@262
    iget-boolean v2, v0, Lcom/qualcomm/location/ulp/UlpEngine;->mAgpsSetting:Z

    #@264
    if-ne v5, v2, :cond_268

    #@266
    .line 665
    and-int/lit8 v3, v3, -0x11

    #@268
    .line 667
    :cond_268
    move-object/from16 v0, p0

    #@26a
    iput-boolean v5, v0, Lcom/qualcomm/location/ulp/UlpEngine;->mAgpsSetting:Z

    #@26c
    goto/16 :goto_134

    #@26e
    .line 674
    :cond_26e
    const/4 v2, 0x2

    #@26f
    move/from16 v0, p1

    #@271
    if-ne v0, v2, :cond_27b

    #@273
    move-object/from16 v0, p0

    #@275
    iget-boolean v2, v0, Lcom/qualcomm/location/ulp/UlpEngine;->mNetworkProvSetting:Z

    #@277
    if-ne v6, v2, :cond_27b

    #@279
    .line 676
    and-int/lit8 v3, v3, -0x3

    #@27b
    .line 678
    :cond_27b
    move-object/from16 v0, p0

    #@27d
    iput-boolean v6, v0, Lcom/qualcomm/location/ulp/UlpEngine;->mNetworkProvSetting:Z

    #@27f
    goto/16 :goto_138

    #@281
    .line 684
    :cond_281
    const/4 v2, 0x2

    #@282
    move/from16 v0, p1

    #@284
    if-ne v0, v2, :cond_28e

    #@286
    move-object/from16 v0, p0

    #@288
    iget-boolean v2, v0, Lcom/qualcomm/location/ulp/UlpEngine;->mWifiSetting:Z

    #@28a
    if-ne v7, v2, :cond_28e

    #@28c
    .line 686
    and-int/lit8 v3, v3, -0x5

    #@28e
    .line 688
    :cond_28e
    move-object/from16 v0, p0

    #@290
    iput-boolean v7, v0, Lcom/qualcomm/location/ulp/UlpEngine;->mWifiSetting:Z

    #@292
    goto/16 :goto_13c

    #@294
    .line 694
    :cond_294
    const/4 v2, 0x2

    #@295
    move/from16 v0, p1

    #@297
    if-ne v0, v2, :cond_2a1

    #@299
    move-object/from16 v0, p0

    #@29b
    iget-boolean v2, v0, Lcom/qualcomm/location/ulp/UlpEngine;->mEnhServicesSetting:Z

    #@29d
    if-ne v9, v2, :cond_2a1

    #@29f
    .line 696
    and-int/lit8 v3, v3, -0x21

    #@2a1
    .line 698
    :cond_2a1
    move-object/from16 v0, p0

    #@2a3
    iput-boolean v9, v0, Lcom/qualcomm/location/ulp/UlpEngine;->mEnhServicesSetting:Z

    #@2a5
    goto/16 :goto_140
.end method

.method private handleNetworkLocationUpdate(Landroid/location/Location;)V
    .registers 8
    .parameter "location"

    #@0
    .prologue
    .line 446
    const-string v0, "UlpEngine"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "handleNetworkLocationUpdate. lat"

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    #@10
    move-result-wide v2

    #@11
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    const-string v2, "lon"

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    #@1e
    move-result-wide v2

    #@1f
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    const-string v2, "accurancy "

    #@25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    #@2c
    move-result v2

    #@2d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@30
    move-result-object v1

    #@31
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v1

    #@35
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 448
    invoke-virtual {p1}, Landroid/location/Location;->hasAccuracy()Z

    #@3b
    move-result v0

    #@3c
    if-eqz v0, :cond_4e

    #@3e
    .line 450
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    #@41
    move-result-wide v1

    #@42
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    #@45
    move-result-wide v3

    #@46
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    #@49
    move-result v5

    #@4a
    move-object v0, p0

    #@4b
    invoke-direct/range {v0 .. v5}, Lcom/qualcomm/location/ulp/UlpEngine;->native_ue_send_network_location(DDF)V

    #@4e
    .line 453
    :cond_4e
    return-void
.end method

.method private isEqual(Lcom/android/location/provider/LocationRequestUnbundled;Lcom/android/location/provider/LocationRequestUnbundled;)Z
    .registers 7
    .parameter "locReq1"
    .parameter "locReq2"

    #@0
    .prologue
    .line 787
    sget-boolean v0, Lcom/qualcomm/location/ulp/UlpEngine;->VERBOSE_DBG:Z

    #@2
    if-eqz v0, :cond_26

    #@4
    .line 789
    const-string v0, "UlpEngine"

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "isEqual invoked. values. locReq1: "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    const-string v2, " locReq2: "

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 792
    :cond_26
    invoke-virtual {p1}, Lcom/android/location/provider/LocationRequestUnbundled;->getFastestInterval()J

    #@29
    move-result-wide v0

    #@2a
    invoke-virtual {p2}, Lcom/android/location/provider/LocationRequestUnbundled;->getFastestInterval()J

    #@2d
    move-result-wide v2

    #@2e
    cmp-long v0, v0, v2

    #@30
    if-nez v0, :cond_56

    #@32
    invoke-virtual {p1}, Lcom/android/location/provider/LocationRequestUnbundled;->getInterval()J

    #@35
    move-result-wide v0

    #@36
    invoke-virtual {p2}, Lcom/android/location/provider/LocationRequestUnbundled;->getInterval()J

    #@39
    move-result-wide v2

    #@3a
    cmp-long v0, v0, v2

    #@3c
    if-nez v0, :cond_56

    #@3e
    invoke-virtual {p1}, Lcom/android/location/provider/LocationRequestUnbundled;->getQuality()I

    #@41
    move-result v0

    #@42
    invoke-virtual {p2}, Lcom/android/location/provider/LocationRequestUnbundled;->getQuality()I

    #@45
    move-result v1

    #@46
    if-ne v0, v1, :cond_56

    #@48
    invoke-virtual {p1}, Lcom/android/location/provider/LocationRequestUnbundled;->getSmallestDisplacement()F

    #@4b
    move-result v0

    #@4c
    invoke-virtual {p2}, Lcom/android/location/provider/LocationRequestUnbundled;->getSmallestDisplacement()F

    #@4f
    move-result v1

    #@50
    cmpl-float v0, v0, v1

    #@52
    if-nez v0, :cond_56

    #@54
    const/4 v0, 0x1

    #@55
    :goto_55
    return v0

    #@56
    :cond_56
    const/4 v0, 0x0

    #@57
    goto :goto_55
.end method

.method private static native native_ue_class_init()V
.end method

.method private native_ue_init()V
	.registers 1
	return-void
.end method

.method private native_ue_inject_raw_command([BI)Z
	.registers 4
	const/4 v0, 0x1
	return v0
.end method

.method private native_ue_send_network_location(DDF)V
	.registers 6
	return-void
.end method

.method private native_ue_start()Z
	.registers 2
	const/4 v0, 0x1
	return v0
.end method

.method private native_ue_stop()Z
	.registers 2
	const/4 v0, 0x1
	return v0
.end method

.method private native_ue_update_criteria(IIJFZII)Z
	.registers 9
	const/4 v0, 0x1
	return v0
.end method

.method private native_ue_update_settings(IZZZZZZ)Z
	.registers 9
	const/4 v0, 0x1
	return v0
.end method

.method private reportLocation(IDDDFFFJI[BZFLjava/lang/String;Ljava/lang/String;)V
    .registers 26
    .parameter "flags"
    .parameter "latitude"
    .parameter "longitude"
    .parameter "altitude"
    .parameter "speed"
    .parameter "bearing"
    .parameter "accuracy"
    .parameter "timestamp"
    .parameter "positionSource"
    .parameter "rawData"
    .parameter "isIndoor"
    .parameter "floorNumber"
    .parameter "mapUrl"
    .parameter "mapIndex"

    #@0
    .prologue
    .line 366
    sget-boolean v4, Lcom/qualcomm/location/ulp/UlpEngine;->VERBOSE_DBG:Z

    #@2
    if-eqz v4, :cond_3e

    #@4
    .line 367
    const-string v4, "UlpEngine"

    #@6
    new-instance v5, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v6, "reportLocation lat: "

    #@d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v5

    #@11
    invoke-virtual {v5, p2, p3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    #@14
    move-result-object v5

    #@15
    const-string v6, " long: "

    #@17
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v5

    #@1b
    invoke-virtual {v5, p4, p5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v5

    #@1f
    const-string v6, " timestamp: "

    #@21
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v5

    #@25
    move-wide/from16 v0, p11

    #@27
    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v5

    #@2b
    const-string v6, " positionSource: "

    #@2d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v5

    #@31
    move/from16 v0, p13

    #@33
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v5

    #@37
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v5

    #@3b
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 371
    :cond_3e
    new-instance v2, Landroid/location/Location;

    #@40
    const-string v4, "fused"

    #@42
    invoke-direct {v2, v4}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    #@45
    .line 372
    .local v2, location:Landroid/location/Location;
    new-instance v3, Landroid/os/Bundle;

    #@47
    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    #@4a
    .line 374
    .local v3, locationExtras:Landroid/os/Bundle;
    and-int/lit8 v4, p1, 0x1

    #@4c
    const/4 v5, 0x1

    #@4d
    if-ne v4, v5, :cond_5a

    #@4f
    .line 375
    invoke-virtual {v2, p2, p3}, Landroid/location/Location;->setLatitude(D)V

    #@52
    .line 376
    invoke-virtual {v2, p4, p5}, Landroid/location/Location;->setLongitude(D)V

    #@55
    .line 377
    move-wide/from16 v0, p11

    #@57
    invoke-virtual {v2, v0, v1}, Landroid/location/Location;->setTime(J)V

    #@5a
    .line 379
    :cond_5a
    and-int/lit8 v4, p1, 0x2

    #@5c
    const/4 v5, 0x2

    #@5d
    if-ne v4, v5, :cond_e8

    #@5f
    .line 380
    invoke-virtual {v2, p6, p7}, Landroid/location/Location;->setAltitude(D)V

    #@62
    .line 384
    :goto_62
    and-int/lit8 v4, p1, 0x4

    #@64
    const/4 v5, 0x4

    #@65
    if-ne v4, v5, :cond_ed

    #@67
    .line 385
    invoke-virtual {v2, p8}, Landroid/location/Location;->setSpeed(F)V

    #@6a
    .line 389
    :goto_6a
    and-int/lit8 v4, p1, 0x8

    #@6c
    const/16 v5, 0x8

    #@6e
    if-ne v4, v5, :cond_f2

    #@70
    .line 390
    move/from16 v0, p9

    #@72
    invoke-virtual {v2, v0}, Landroid/location/Location;->setBearing(F)V

    #@75
    .line 394
    :goto_75
    and-int/lit8 v4, p1, 0x10

    #@77
    const/16 v5, 0x10

    #@79
    if-ne v4, v5, :cond_f6

    #@7b
    .line 395
    move/from16 v0, p10

    #@7d
    invoke-virtual {v2, v0}, Landroid/location/Location;->setAccuracy(F)V

    #@80
    .line 399
    :goto_80
    sget-boolean v4, Lcom/qualcomm/location/ulp/UlpEngine;->VERBOSE_DBG:Z

    #@82
    if-eqz v4, :cond_9c

    #@84
    const-string v4, "UlpEngine"

    #@86
    new-instance v5, Ljava/lang/StringBuilder;

    #@88
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@8b
    const-string v6, "reportLocation.flag:"

    #@8d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v5

    #@91
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@94
    move-result-object v5

    #@95
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@98
    move-result-object v5

    #@99
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@9c
    .line 401
    :cond_9c
    move-object/from16 v0, p14

    #@9e
    array-length v4, v0

    #@9f
    if-lez v4, :cond_fa

    #@a1
    .line 402
    const-string v4, "RawData"

    #@a3
    move-object/from16 v0, p14

    #@a5
    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    #@a8
    .line 407
    :goto_a8
    and-int/lit8 v4, p1, 0x40

    #@aa
    const/16 v5, 0x40

    #@ac
    if-ne v4, v5, :cond_100

    #@ae
    .line 408
    const-string v4, "isIndoor"

    #@b0
    move/from16 v0, p15

    #@b2
    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@b5
    .line 413
    :goto_b5
    and-int/lit16 v4, p1, 0x80

    #@b7
    const/16 v5, 0x80

    #@b9
    if-ne v4, v5, :cond_106

    #@bb
    .line 414
    const-string v4, "floorNumber"

    #@bd
    move/from16 v0, p16

    #@bf
    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    #@c2
    .line 419
    :goto_c2
    and-int/lit16 v4, p1, 0x100

    #@c4
    const/16 v5, 0x100

    #@c6
    if-ne v4, v5, :cond_10c

    #@c8
    .line 420
    const-string v4, "mapUrl"

    #@ca
    move-object/from16 v0, p17

    #@cc
    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    #@cf
    .line 425
    :goto_cf
    and-int/lit16 v4, p1, 0x200

    #@d1
    const/16 v5, 0x200

    #@d3
    if-ne v4, v5, :cond_112

    #@d5
    .line 426
    const-string v4, "mapIndex"

    #@d7
    move-object/from16 v0, p18

    #@d9
    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    #@dc
    .line 431
    :goto_dc
    invoke-virtual {v2, v3}, Landroid/location/Location;->setExtras(Landroid/os/Bundle;)V

    #@df
    .line 432
    invoke-virtual {v2}, Landroid/location/Location;->makeComplete()V

    #@e2
    .line 433
    iget-object v4, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mCallback:Lcom/qualcomm/location/ulp/UlpEngine$Callback;

    #@e4
    invoke-interface {v4, v2}, Lcom/qualcomm/location/ulp/UlpEngine$Callback;->reportLocation(Landroid/location/Location;)V

    #@e7
    .line 434
    return-void

    #@e8
    .line 382
    :cond_e8
    invoke-virtual {v2}, Landroid/location/Location;->removeAltitude()V

    #@eb
    goto/16 :goto_62

    #@ed
    .line 387
    :cond_ed
    invoke-virtual {v2}, Landroid/location/Location;->removeSpeed()V

    #@f0
    goto/16 :goto_6a

    #@f2
    .line 392
    :cond_f2
    invoke-virtual {v2}, Landroid/location/Location;->removeBearing()V

    #@f5
    goto :goto_75

    #@f6
    .line 397
    :cond_f6
    invoke-virtual {v2}, Landroid/location/Location;->removeAccuracy()V

    #@f9
    goto :goto_80

    #@fa
    .line 404
    :cond_fa
    const-string v4, "RawData"

    #@fc
    invoke-virtual {v3, v4}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    #@ff
    goto :goto_a8

    #@100
    .line 410
    :cond_100
    const-string v4, "isIndoor"

    #@102
    invoke-virtual {v3, v4}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    #@105
    goto :goto_b5

    #@106
    .line 416
    :cond_106
    const-string v4, "floorNumber"

    #@108
    invoke-virtual {v3, v4}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    #@10b
    goto :goto_c2

    #@10c
    .line 422
    :cond_10c
    const-string v4, "mapUrl"

    #@10e
    invoke-virtual {v3, v4}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    #@111
    goto :goto_cf

    #@112
    .line 428
    :cond_112
    const-string v4, "mapIndex"

    #@114
    invoke-virtual {v3, v4}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    #@117
    goto :goto_dc
.end method

.method private requestNetworkLocation(III)V
    .registers 7
    .parameter "type"
    .parameter "interval"
    .parameter "source"

    #@0
    .prologue
    .line 441
    sget-boolean v0, Lcom/qualcomm/location/ulp/UlpEngine;->VERBOSE_DBG:Z

    #@2
    if-eqz v0, :cond_30

    #@4
    const-string v0, "UlpEngine"

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "requestNetworkLocation. type: "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    const-string v2, "interval: "

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    const-string v2, "source "

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v1

    #@2d
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 442
    :cond_30
    const/4 v0, 0x5

    #@31
    const/4 v1, 0x0

    #@32
    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/qualcomm/location/ulp/UlpEngine;->sendMessage(IIILjava/lang/Object;)V

    #@35
    .line 443
    return-void
.end method

.method private requestPhoneContext(II)V
    .registers 6
    .parameter "context_type"
    .parameter "request_type"

    #@0
    .prologue
    .line 504
    sget-boolean v0, Lcom/qualcomm/location/ulp/UlpEngine;->VERBOSE_DBG:Z

    #@2
    if-eqz v0, :cond_26

    #@4
    const-string v0, "UlpEngine"

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "requestPhoneContext from native layer.context_type: "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    const-string v2, " request_type:"

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 506
    :cond_26
    const/4 v0, 0x3

    #@27
    const/4 v1, 0x0

    #@28
    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/qualcomm/location/ulp/UlpEngine;->sendMessage(IIILjava/lang/Object;)V

    #@2b
    .line 507
    return-void
.end method

.method private updateCriteria(ILcom/android/location/provider/LocationRequestUnbundled;)V
    .registers 16
    .parameter "action"
    .parameter "request"

    #@0
    .prologue
    const/4 v7, 0x3

    #@1
    const/4 v1, 0x2

    #@2
    const/4 v12, 0x1

    #@3
    .line 233
    const/4 v6, 0x0

    #@4
    .line 236
    .local v6, singleShot:Z
    :try_start_4
    const-class v0, Lcom/android/location/provider/LocationRequestUnbundled;

    #@6
    const-string v2, "delegate"

    #@8
    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@b
    move-result-object v11

    #@c
    .line 237
    .local v11, locationRequestField:Ljava/lang/reflect/Field;
    const/4 v0, 0x1

    #@d
    invoke-virtual {v11, v0}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    #@10
    .line 238
    invoke-virtual {v11, p2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@13
    move-result-object v10

    #@14
    check-cast v10, Landroid/location/LocationRequest;

    #@16
    .line 239
    .local v10, locationRequest:Landroid/location/LocationRequest;
    sget-boolean v0, Lcom/qualcomm/location/ulp/UlpEngine;->VERBOSE_DBG:Z

    #@18
    if-eqz v0, :cond_36

    #@1a
    const-string v0, "UlpEngine"

    #@1c
    new-instance v2, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v3, "locationRequest "

    #@23
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v10}, Landroid/location/LocationRequest;->toString()Ljava/lang/String;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v2

    #@33
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 240
    :cond_36
    invoke-virtual {v10}, Landroid/location/LocationRequest;->getNumUpdates()I
    :try_end_39
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_39} :catch_7c

    #@39
    move-result v0

    #@3a
    if-ne v0, v12, :cond_7a

    #@3c
    move v6, v12

    #@3d
    .line 245
    .end local v10           #locationRequest:Landroid/location/LocationRequest;
    .end local v11           #locationRequestField:Ljava/lang/reflect/Field;
    :goto_3d
    sget-boolean v0, Lcom/qualcomm/location/ulp/UlpEngine;->VERBOSE_DBG:Z

    #@3f
    if-eqz v0, :cond_6b

    #@41
    const-string v0, "UlpEngine"

    #@43
    new-instance v2, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v3, "Request Received with quality: "

    #@4a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v2

    #@4e
    invoke-virtual {p2}, Lcom/android/location/provider/LocationRequestUnbundled;->getQuality()I

    #@51
    move-result v3

    #@52
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@55
    move-result-object v2

    #@56
    const-string v3, " & interval: "

    #@58
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v2

    #@5c
    invoke-virtual {p2}, Lcom/android/location/provider/LocationRequestUnbundled;->getInterval()J

    #@5f
    move-result-wide v3

    #@60
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@63
    move-result-object v2

    #@64
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@67
    move-result-object v2

    #@68
    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@6b
    .line 246
    :cond_6b
    invoke-virtual {p2}, Lcom/android/location/provider/LocationRequestUnbundled;->getQuality()I

    #@6e
    move-result v0

    #@6f
    sparse-switch v0, :sswitch_data_a4

    #@72
    .line 264
    const-string v0, "UlpEngine"

    #@74
    const-string v1, "Invalid quality value received"

    #@76
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@79
    .line 267
    :goto_79
    return-void

    #@7a
    .line 240
    .restart local v10       #locationRequest:Landroid/location/LocationRequest;
    .restart local v11       #locationRequestField:Ljava/lang/reflect/Field;
    :cond_7a
    const/4 v6, 0x0

    #@7b
    goto :goto_3d

    #@7c
    .line 241
    .end local v10           #locationRequest:Landroid/location/LocationRequest;
    .end local v11           #locationRequestField:Ljava/lang/reflect/Field;
    :catch_7c
    move-exception v9

    #@7d
    .line 242
    .local v9, e:Ljava/lang/Exception;
    const-string v0, "UlpEngine"

    #@7f
    const-string v2, "Exception "

    #@81
    invoke-static {v0, v2, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@84
    goto :goto_3d

    #@85
    .line 251
    .end local v9           #e:Ljava/lang/Exception;
    :sswitch_85
    invoke-virtual {p2}, Lcom/android/location/provider/LocationRequestUnbundled;->getInterval()J

    #@88
    move-result-wide v3

    #@89
    invoke-virtual {p2}, Lcom/android/location/provider/LocationRequestUnbundled;->getSmallestDisplacement()F

    #@8c
    move-result v5

    #@8d
    move-object v0, p0

    #@8e
    move v2, p1

    #@8f
    move v8, v7

    #@90
    invoke-direct/range {v0 .. v8}, Lcom/qualcomm/location/ulp/UlpEngine;->native_ue_update_criteria(IIJFZII)Z

    #@93
    goto :goto_79

    #@94
    .line 259
    :sswitch_94
    invoke-virtual {p2}, Lcom/android/location/provider/LocationRequestUnbundled;->getInterval()J

    #@97
    move-result-wide v3

    #@98
    invoke-virtual {p2}, Lcom/android/location/provider/LocationRequestUnbundled;->getSmallestDisplacement()F

    #@9b
    move-result v5

    #@9c
    move-object v0, p0

    #@9d
    move v2, p1

    #@9e
    move v7, v12

    #@9f
    move v8, v12

    #@a0
    invoke-direct/range {v0 .. v8}, Lcom/qualcomm/location/ulp/UlpEngine;->native_ue_update_criteria(IIJFZII)Z

    #@a3
    goto :goto_79

    #@a4
    .line 246
    :sswitch_data_a4
    .sparse-switch
        0x64 -> :sswitch_85
        0x66 -> :sswitch_94
        0x68 -> :sswitch_94
        0xc9 -> :sswitch_94
        0xcb -> :sswitch_85
    .end sparse-switch
.end method

.method private updateGlobalSettings(ZZ)Z
    .registers 7
    .parameter "wifiSetting"
    .parameter "agpsSetting"

    #@0
    .prologue
    .line 772
    sget-boolean v1, Lcom/qualcomm/location/ulp/UlpEngine;->VERBOSE_DBG:Z

    #@2
    if-eqz v1, :cond_26

    #@4
    .line 774
    const-string v1, "UlpEngine"

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "updateGlobalSettings invoked. values. WiFi:"

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    const-string v3, " Agps:"

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 777
    :cond_26
    new-instance v0, Landroid/os/Bundle;

    #@28
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@2b
    .line 778
    .local v0, contextBundle:Landroid/os/Bundle;
    const-string v1, "wifiSetting"

    #@2d
    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@30
    .line 779
    const-string v1, "agpsSetting"

    #@32
    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@35
    .line 780
    const/4 v1, 0x4

    #@36
    const/4 v2, 0x2

    #@37
    const/4 v3, 0x0

    #@38
    invoke-virtual {p0, v1, v2, v3, v0}, Lcom/qualcomm/location/ulp/UlpEngine;->sendMessage(IIILjava/lang/Object;)V

    #@3b
    .line 782
    const/4 v1, 0x1

    #@3c
    return v1
.end method

.method private updateRequirements()V
    .registers 13

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    const/4 v10, 0x1

    #@2
    .line 271
    sget-boolean v7, Lcom/qualcomm/location/ulp/UlpEngine;->VERBOSE_DBG:Z

    #@4
    if-eqz v7, :cond_2c

    #@6
    const-string v7, "UlpEngine"

    #@8
    new-instance v8, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v9, "updateRequirements. mEnabled: "

    #@f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v8

    #@13
    iget-boolean v9, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mEnabled:Z

    #@15
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@18
    move-result-object v8

    #@19
    const-string v9, " mRequest : "

    #@1b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v8

    #@1f
    iget-object v9, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequest:Lcom/android/location/provider/ProviderRequestUnbundled;

    #@21
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v8

    #@25
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v8

    #@29
    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 273
    :cond_2c
    iget-boolean v7, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mEnabled:Z

    #@2e
    if-eqz v7, :cond_34

    #@30
    iget-object v7, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequest:Lcom/android/location/provider/ProviderRequestUnbundled;

    #@32
    if-nez v7, :cond_6b

    #@34
    .line 274
    :cond_34
    iput-object v11, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequest:Lcom/android/location/provider/ProviderRequestUnbundled;

    #@36
    .line 275
    iget-object v7, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequestActive:Lcom/android/location/provider/ProviderRequestUnbundled;

    #@38
    if-eqz v7, :cond_4d

    #@3a
    .line 278
    const/4 v8, 0x2

    #@3b
    :try_start_3b
    iget-object v7, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequestActive:Lcom/android/location/provider/ProviderRequestUnbundled;

    #@3d
    invoke-virtual {v7}, Lcom/android/location/provider/ProviderRequestUnbundled;->getLocationRequests()Ljava/util/List;

    #@40
    move-result-object v7

    #@41
    const/4 v9, 0x0

    #@42
    invoke-interface {v7, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@45
    move-result-object v7

    #@46
    check-cast v7, Lcom/android/location/provider/LocationRequestUnbundled;

    #@48
    invoke-direct {p0, v8, v7}, Lcom/qualcomm/location/ulp/UlpEngine;->updateCriteria(ILcom/android/location/provider/LocationRequestUnbundled;)V
    :try_end_4b
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_3b .. :try_end_4b} :catch_51

    #@4b
    .line 282
    :goto_4b
    iput-object v11, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequestActive:Lcom/android/location/provider/ProviderRequestUnbundled;

    #@4d
    .line 284
    :cond_4d
    invoke-direct {p0}, Lcom/qualcomm/location/ulp/UlpEngine;->native_ue_stop()Z

    #@50
    .line 342
    :goto_50
    return-void

    #@51
    .line 279
    :catch_51
    move-exception v0

    #@52
    .line 280
    .local v0, e:Ljava/lang/IndexOutOfBoundsException;
    const-string v7, "UlpEngine"

    #@54
    new-instance v8, Ljava/lang/StringBuilder;

    #@56
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@59
    const-string v9, "in updateRequirements-last removal. Got IndexOutOfBoundsException: "

    #@5b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v8

    #@5f
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v8

    #@63
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66
    move-result-object v8

    #@67
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6a
    goto :goto_4b

    #@6b
    .line 288
    .end local v0           #e:Ljava/lang/IndexOutOfBoundsException;
    :cond_6b
    sget-boolean v7, Lcom/qualcomm/location/ulp/UlpEngine;->VERBOSE_DBG:Z

    #@6d
    if-eqz v7, :cond_b7

    #@6f
    .line 289
    const-string v7, "UlpEngine"

    #@71
    new-instance v8, Ljava/lang/StringBuilder;

    #@73
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    const-string v9, "updateRequirements. mRequest.getLocationRequests().size() :"

    #@78
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v8

    #@7c
    iget-object v9, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequest:Lcom/android/location/provider/ProviderRequestUnbundled;

    #@7e
    invoke-virtual {v9}, Lcom/android/location/provider/ProviderRequestUnbundled;->getLocationRequests()Ljava/util/List;

    #@81
    move-result-object v9

    #@82
    invoke-interface {v9}, Ljava/util/List;->size()I

    #@85
    move-result v9

    #@86
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@89
    move-result-object v8

    #@8a
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8d
    move-result-object v8

    #@8e
    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@91
    .line 290
    iget-object v7, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequestActive:Lcom/android/location/provider/ProviderRequestUnbundled;

    #@93
    if-eqz v7, :cond_b7

    #@95
    .line 291
    const-string v7, "UlpEngine"

    #@97
    new-instance v8, Ljava/lang/StringBuilder;

    #@99
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@9c
    const-string v9, "mRequestActive.getLocationRequests().size(): "

    #@9e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v8

    #@a2
    iget-object v9, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequestActive:Lcom/android/location/provider/ProviderRequestUnbundled;

    #@a4
    invoke-virtual {v9}, Lcom/android/location/provider/ProviderRequestUnbundled;->getLocationRequests()Ljava/util/List;

    #@a7
    move-result-object v9

    #@a8
    invoke-interface {v9}, Ljava/util/List;->size()I

    #@ab
    move-result v9

    #@ac
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@af
    move-result-object v8

    #@b0
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b3
    move-result-object v8

    #@b4
    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b7
    .line 296
    :cond_b7
    iget-object v7, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequestActive:Lcom/android/location/provider/ProviderRequestUnbundled;

    #@b9
    if-nez v7, :cond_de

    #@bb
    .line 297
    iget-object v7, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequest:Lcom/android/location/provider/ProviderRequestUnbundled;

    #@bd
    invoke-virtual {v7}, Lcom/android/location/provider/ProviderRequestUnbundled;->getLocationRequests()Ljava/util/List;

    #@c0
    move-result-object v7

    #@c1
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@c4
    move-result-object v2

    #@c5
    .line 298
    .local v2, iter:Ljava/util/Iterator;
    :goto_c5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@c8
    move-result v7

    #@c9
    if-eqz v7, :cond_d5

    #@cb
    .line 299
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@ce
    move-result-object v6

    #@cf
    check-cast v6, Lcom/android/location/provider/LocationRequestUnbundled;

    #@d1
    .line 300
    .local v6, locRequest:Lcom/android/location/provider/LocationRequestUnbundled;
    invoke-direct {p0, v10, v6}, Lcom/qualcomm/location/ulp/UlpEngine;->updateCriteria(ILcom/android/location/provider/LocationRequestUnbundled;)V

    #@d4
    goto :goto_c5

    #@d5
    .line 302
    .end local v6           #locRequest:Lcom/android/location/provider/LocationRequestUnbundled;
    :cond_d5
    invoke-direct {p0}, Lcom/qualcomm/location/ulp/UlpEngine;->native_ue_start()Z

    #@d8
    .line 341
    .end local v2           #iter:Ljava/util/Iterator;
    :cond_d8
    :goto_d8
    iget-object v7, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequest:Lcom/android/location/provider/ProviderRequestUnbundled;

    #@da
    iput-object v7, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequestActive:Lcom/android/location/provider/ProviderRequestUnbundled;

    #@dc
    goto/16 :goto_50

    #@de
    .line 303
    :cond_de
    iget-object v7, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequest:Lcom/android/location/provider/ProviderRequestUnbundled;

    #@e0
    invoke-virtual {v7}, Lcom/android/location/provider/ProviderRequestUnbundled;->getLocationRequests()Ljava/util/List;

    #@e3
    move-result-object v7

    #@e4
    invoke-interface {v7}, Ljava/util/List;->size()I

    #@e7
    move-result v7

    #@e8
    iget-object v8, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequestActive:Lcom/android/location/provider/ProviderRequestUnbundled;

    #@ea
    invoke-virtual {v8}, Lcom/android/location/provider/ProviderRequestUnbundled;->getLocationRequests()Ljava/util/List;

    #@ed
    move-result-object v8

    #@ee
    invoke-interface {v8}, Ljava/util/List;->size()I

    #@f1
    move-result v8

    #@f2
    if-le v7, v8, :cond_160

    #@f4
    .line 307
    iget-object v7, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequest:Lcom/android/location/provider/ProviderRequestUnbundled;

    #@f6
    invoke-virtual {v7}, Lcom/android/location/provider/ProviderRequestUnbundled;->getLocationRequests()Ljava/util/List;

    #@f9
    move-result-object v7

    #@fa
    invoke-interface {v7}, Ljava/util/List;->size()I

    #@fd
    move-result v7

    #@fe
    add-int/lit8 v5, v7, -0x1

    #@100
    .line 308
    .local v5, lastElementIndexCurrentReq:I
    iget-object v7, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequestActive:Lcom/android/location/provider/ProviderRequestUnbundled;

    #@102
    invoke-virtual {v7}, Lcom/android/location/provider/ProviderRequestUnbundled;->getLocationRequests()Ljava/util/List;

    #@105
    move-result-object v7

    #@106
    invoke-interface {v7}, Ljava/util/List;->size()I

    #@109
    move-result v7

    #@10a
    add-int/lit8 v4, v7, -0x1

    #@10c
    .line 309
    .local v4, lastElementIndexCachedReq:I
    const-string v7, "UlpEngine"

    #@10e
    new-instance v8, Ljava/lang/StringBuilder;

    #@110
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@113
    const-string v9, "in updateRequirements-addition. lastElementIndexCurrentReq: "

    #@115
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@118
    move-result-object v8

    #@119
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11c
    move-result-object v8

    #@11d
    const-string v9, "lastElementIndexCachedReq "

    #@11f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@122
    move-result-object v8

    #@123
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@126
    move-result-object v8

    #@127
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12a
    move-result-object v8

    #@12b
    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@12e
    .line 313
    add-int/lit8 v1, v4, 0x1

    #@130
    .local v1, i:I
    :goto_130
    if-gt v1, v5, :cond_d8

    #@132
    .line 315
    const/4 v8, 0x1

    #@133
    :try_start_133
    iget-object v7, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequest:Lcom/android/location/provider/ProviderRequestUnbundled;

    #@135
    invoke-virtual {v7}, Lcom/android/location/provider/ProviderRequestUnbundled;->getLocationRequests()Ljava/util/List;

    #@138
    move-result-object v7

    #@139
    invoke-interface {v7, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@13c
    move-result-object v7

    #@13d
    check-cast v7, Lcom/android/location/provider/LocationRequestUnbundled;

    #@13f
    invoke-direct {p0, v8, v7}, Lcom/qualcomm/location/ulp/UlpEngine;->updateCriteria(ILcom/android/location/provider/LocationRequestUnbundled;)V
    :try_end_142
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_133 .. :try_end_142} :catch_145

    #@142
    .line 314
    add-int/lit8 v1, v1, 0x1

    #@144
    goto :goto_130

    #@145
    .line 318
    :catch_145
    move-exception v0

    #@146
    .line 319
    .restart local v0       #e:Ljava/lang/IndexOutOfBoundsException;
    const-string v7, "UlpEngine"

    #@148
    new-instance v8, Ljava/lang/StringBuilder;

    #@14a
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@14d
    const-string v9, "in updateRequirements-addition. Got IndexOutOfBoundsException: "

    #@14f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@152
    move-result-object v8

    #@153
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@156
    move-result-object v8

    #@157
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15a
    move-result-object v8

    #@15b
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@15e
    goto/16 :goto_d8

    #@160
    .line 321
    .end local v0           #e:Ljava/lang/IndexOutOfBoundsException;
    .end local v1           #i:I
    .end local v4           #lastElementIndexCachedReq:I
    .end local v5           #lastElementIndexCurrentReq:I
    :cond_160
    iget-object v7, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequest:Lcom/android/location/provider/ProviderRequestUnbundled;

    #@162
    invoke-virtual {v7}, Lcom/android/location/provider/ProviderRequestUnbundled;->getLocationRequests()Ljava/util/List;

    #@165
    move-result-object v7

    #@166
    invoke-interface {v7}, Ljava/util/List;->size()I

    #@169
    move-result v7

    #@16a
    iget-object v8, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequestActive:Lcom/android/location/provider/ProviderRequestUnbundled;

    #@16c
    invoke-virtual {v8}, Lcom/android/location/provider/ProviderRequestUnbundled;->getLocationRequests()Ljava/util/List;

    #@16f
    move-result-object v8

    #@170
    invoke-interface {v8}, Ljava/util/List;->size()I

    #@173
    move-result v8

    #@174
    if-ge v7, v8, :cond_d8

    #@176
    .line 324
    const/4 v1, 0x0

    #@177
    .restart local v1       #i:I
    :goto_177
    :try_start_177
    iget-object v7, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequestActive:Lcom/android/location/provider/ProviderRequestUnbundled;

    #@179
    invoke-virtual {v7}, Lcom/android/location/provider/ProviderRequestUnbundled;->getLocationRequests()Ljava/util/List;

    #@17c
    move-result-object v7

    #@17d
    invoke-interface {v7}, Ljava/util/List;->size()I

    #@180
    move-result v7

    #@181
    if-ge v1, v7, :cond_d8

    #@183
    .line 326
    const/4 v3, 0x0

    #@184
    .local v3, j:I
    :goto_184
    iget-object v7, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequest:Lcom/android/location/provider/ProviderRequestUnbundled;

    #@186
    invoke-virtual {v7}, Lcom/android/location/provider/ProviderRequestUnbundled;->getLocationRequests()Ljava/util/List;

    #@189
    move-result-object v7

    #@18a
    invoke-interface {v7}, Ljava/util/List;->size()I

    #@18d
    move-result v7

    #@18e
    if-ge v3, v7, :cond_1c1

    #@190
    .line 328
    iget-object v7, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequestActive:Lcom/android/location/provider/ProviderRequestUnbundled;

    #@192
    invoke-virtual {v7}, Lcom/android/location/provider/ProviderRequestUnbundled;->getLocationRequests()Ljava/util/List;

    #@195
    move-result-object v7

    #@196
    invoke-interface {v7, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@199
    move-result-object v7

    #@19a
    check-cast v7, Lcom/android/location/provider/LocationRequestUnbundled;

    #@19c
    iget-object v8, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequest:Lcom/android/location/provider/ProviderRequestUnbundled;

    #@19e
    invoke-virtual {v8}, Lcom/android/location/provider/ProviderRequestUnbundled;->getLocationRequests()Ljava/util/List;

    #@1a1
    move-result-object v8

    #@1a2
    invoke-interface {v8, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@1a5
    move-result-object v8

    #@1a6
    check-cast v8, Lcom/android/location/provider/LocationRequestUnbundled;

    #@1a8
    invoke-direct {p0, v7, v8}, Lcom/qualcomm/location/ulp/UlpEngine;->isEqual(Lcom/android/location/provider/LocationRequestUnbundled;Lcom/android/location/provider/LocationRequestUnbundled;)Z

    #@1ab
    move-result v7

    #@1ac
    if-nez v7, :cond_1be

    #@1ae
    .line 331
    const/4 v8, 0x2

    #@1af
    iget-object v7, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequestActive:Lcom/android/location/provider/ProviderRequestUnbundled;

    #@1b1
    invoke-virtual {v7}, Lcom/android/location/provider/ProviderRequestUnbundled;->getLocationRequests()Ljava/util/List;

    #@1b4
    move-result-object v7

    #@1b5
    invoke-interface {v7, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@1b8
    move-result-object v7

    #@1b9
    check-cast v7, Lcom/android/location/provider/LocationRequestUnbundled;

    #@1bb
    invoke-direct {p0, v8, v7}, Lcom/qualcomm/location/ulp/UlpEngine;->updateCriteria(ILcom/android/location/provider/LocationRequestUnbundled;)V
    :try_end_1be
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_177 .. :try_end_1be} :catch_1c4

    #@1be
    .line 327
    :cond_1be
    add-int/lit8 v3, v3, 0x1

    #@1c0
    goto :goto_184

    #@1c1
    .line 325
    :cond_1c1
    add-int/lit8 v1, v1, 0x1

    #@1c3
    goto :goto_177

    #@1c4
    .line 336
    .end local v3           #j:I
    :catch_1c4
    move-exception v0

    #@1c5
    .line 337
    .restart local v0       #e:Ljava/lang/IndexOutOfBoundsException;
    const-string v7, "UlpEngine"

    #@1c7
    new-instance v8, Ljava/lang/StringBuilder;

    #@1c9
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@1cc
    const-string v9, "in updateRequirements-removal. Got IndexOutOfBoundsException: "

    #@1ce
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d1
    move-result-object v8

    #@1d2
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1d5
    move-result-object v8

    #@1d6
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d9
    move-result-object v8

    #@1da
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1dd
    goto/16 :goto_d8
.end method

.method private updateSecureSettings(ZZZ)Z
    .registers 8
    .parameter "gpsSetting"
    .parameter "networkProvSetting"
    .parameter "enhLocationServicesSetting"

    #@0
    .prologue
    .line 718
    sget-boolean v1, Lcom/qualcomm/location/ulp/UlpEngine;->VERBOSE_DBG:Z

    #@2
    if-eqz v1, :cond_30

    #@4
    .line 720
    const-string v1, "UlpEngine"

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "updateSettings invoked and setting values. Gps:"

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    const-string v3, " GNP:"

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    const-string v3, "enhLocationServicesSetting: "

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v2

    #@2d
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 724
    :cond_30
    new-instance v0, Landroid/os/Bundle;

    #@32
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@35
    .line 725
    .local v0, contextBundle:Landroid/os/Bundle;
    const-string v1, "gpsSetting"

    #@37
    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@3a
    .line 726
    const-string v1, "networkProvSetting"

    #@3c
    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@3f
    .line 727
    const-string v1, "enhLocationServicesSetting"

    #@41
    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@44
    .line 728
    const/4 v1, 0x4

    #@45
    const/4 v2, 0x2

    #@46
    const/4 v3, 0x0

    #@47
    invoke-virtual {p0, v1, v2, v3, v0}, Lcom/qualcomm/location/ulp/UlpEngine;->sendMessage(IIILjava/lang/Object;)V

    #@4a
    .line 730
    const/4 v1, 0x1

    #@4b
    return v1
.end method


# virtual methods
.method public deinit()V
    .registers 4

    #@0
    .prologue
    .line 199
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequest:Lcom/android/location/provider/ProviderRequestUnbundled;

    #@3
    .line 200
    invoke-virtual {p0}, Lcom/qualcomm/location/ulp/UlpEngine;->disable()V

    #@6
    .line 201
    const-string v0, "UlpEngine"

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "engine stopped ("

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    iget-object v2, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mContext:Landroid/content/Context;

    #@15
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    const-string v2, ")"

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 202
    return-void
.end method

.method public disable()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 219
    const/4 v0, 0x1

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {p0, v0, v2, v2, v1}, Lcom/qualcomm/location/ulp/UlpEngine;->sendMessage(IIILjava/lang/Object;)V

    #@6
    .line 220
    return-void
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 4
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 357
    return-void
.end method

.method public enable()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 208
    const/4 v0, 0x0

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {p0, v2, v2, v0, v1}, Lcom/qualcomm/location/ulp/UlpEngine;->sendMessage(IIILjava/lang/Object;)V

    #@6
    .line 209
    return-void
.end method

.method public handleDisable()V
    .registers 2

    #@0
    .prologue
    .line 223
    iget-boolean v0, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mEnabled:Z

    #@2
    if-nez v0, :cond_5

    #@4
    .line 229
    :goto_4
    return-void

    #@5
    .line 227
    :cond_5
    const/4 v0, 0x0

    #@6
    iput-boolean v0, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mEnabled:Z

    #@8
    .line 228
    invoke-direct {p0}, Lcom/qualcomm/location/ulp/UlpEngine;->updateRequirements()V

    #@b
    goto :goto_4
.end method

.method public handleSetRequest(Lcom/android/location/provider/ProviderRequestUnbundled;)V
    .registers 3
    .parameter "request"

    #@0
    .prologue
    .line 350
    iput-object p1, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mRequest:Lcom/android/location/provider/ProviderRequestUnbundled;

    #@2
    .line 351
    invoke-virtual {p1}, Lcom/android/location/provider/ProviderRequestUnbundled;->getReportLocation()Z

    #@5
    move-result v0

    #@6
    iput-boolean v0, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mEnabled:Z

    #@8
    .line 352
    invoke-direct {p0}, Lcom/qualcomm/location/ulp/UlpEngine;->updateRequirements()V

    #@b
    .line 353
    return-void
.end method

.method public init()V
    .registers 1

    #@0
    .prologue
    .line 192
    return-void
.end method

.method public init(Lcom/qualcomm/location/ulp/UlpEngine$Callback;)V
    .registers 5
    .parameter "callback"

    #@0
    .prologue
    .line 194
    const-string v0, "UlpEngine"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "engine started ("

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-object v2, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mContext:Landroid/content/Context;

    #@f
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, ")"

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 195
    iput-object p1, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mCallback:Lcom/qualcomm/location/ulp/UlpEngine$Callback;

    #@26
    .line 196
    invoke-direct {p0}, Lcom/qualcomm/location/ulp/UlpEngine;->native_ue_init()V

    #@29
    .line 197
    return-void
.end method

.method public final sendMessage(IIILjava/lang/Object;)V
    .registers 7
    .parameter "what"
    .parameter "arg1"
    .parameter "arg2"
    .parameter "obj"

    #@0
    .prologue
    .line 186
    iget-object v1, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mHandler:Landroid/os/Handler;

    #@2
    invoke-virtual {v1, p1}, Landroid/os/Handler;->removeMessages(I)V

    #@5
    .line 187
    iget-object v1, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mHandler:Landroid/os/Handler;

    #@7
    invoke-static {v1, p1, p2, p3, p4}, Landroid/os/Message;->obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;

    #@a
    move-result-object v0

    #@b
    .line 188
    .local v0, m:Landroid/os/Message;
    iget-object v1, p0, Lcom/qualcomm/location/ulp/UlpEngine;->mHandler:Landroid/os/Handler;

    #@d
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@10
    .line 189
    return-void
.end method

.method public setRequest(Lcom/android/location/provider/ProviderRequestUnbundled;Landroid/os/WorkSource;)V
    .registers 7
    .parameter "request"
    .parameter "source"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 345
    sget-boolean v0, Lcom/qualcomm/location/ulp/UlpEngine;->VERBOSE_DBG:Z

    #@3
    if-eqz v0, :cond_21

    #@5
    const-string v0, "UlpEngine"

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v2, "setRequest received. mEnabled: "

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {p1}, Lcom/android/location/provider/ProviderRequestUnbundled;->getReportLocation()Z

    #@15
    move-result v2

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 346
    :cond_21
    const/4 v0, 0x2

    #@22
    invoke-virtual {p0, v0, v3, v3, p1}, Lcom/qualcomm/location/ulp/UlpEngine;->sendMessage(IIILjava/lang/Object;)V

    #@25
    .line 347
    return-void
.end method
