.class final Lcom/qualcomm/location/ulp/UlpEngine$SecureSettingsObserver;
.super Ljava/lang/Object;
.source "UlpEngine.java"

# interfaces
.implements Ljava/util/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/location/ulp/UlpEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SecureSettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/location/ulp/UlpEngine;


# direct methods
.method private constructor <init>(Lcom/qualcomm/location/ulp/UlpEngine;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 733
    iput-object p1, p0, Lcom/qualcomm/location/ulp/UlpEngine$SecureSettingsObserver;->this$0:Lcom/qualcomm/location/ulp/UlpEngine;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/qualcomm/location/ulp/UlpEngine;Lcom/qualcomm/location/ulp/UlpEngine$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 733
    invoke-direct {p0, p1}, Lcom/qualcomm/location/ulp/UlpEngine$SecureSettingsObserver;-><init>(Lcom/qualcomm/location/ulp/UlpEngine;)V

    #@3
    return-void
.end method


# virtual methods
.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .registers 13
    .parameter "o"
    .parameter "arg"

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    .line 735
    invoke-static {}, Lcom/qualcomm/location/ulp/UlpEngine;->access$200()Z

    #@4
    move-result v6

    #@5
    if-eqz v6, :cond_e

    #@7
    const-string v6, "UlpEngine"

    #@9
    const-string v7, "SecureSettingsObserver.update invoked "

    #@b
    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 736
    :cond_e
    const/4 v0, 0x0

    #@f
    .local v0, enhLocationServicesSetting:Z
    const/4 v2, 0x0

    #@10
    .local v2, gpsSetting:Z
    const/4 v4, 0x0

    #@11
    .line 737
    .local v4, networkProvSetting:Z
    const/4 v1, 0x0

    #@12
    .line 739
    .local v1, enhLocationServicesSettingString:Ljava/lang/String;
    check-cast p1, Landroid/content/ContentQueryMap;

    #@14
    .end local p1
    invoke-virtual {p1}, Landroid/content/ContentQueryMap;->getRows()Ljava/util/Map;

    #@17
    move-result-object v3

    #@18
    .line 740
    .local v3, kvs:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Landroid/content/ContentValues;>;"
    if-eqz v3, :cond_bd

    #@1a
    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    #@1d
    move-result v6

    #@1e
    if-nez v6, :cond_bd

    #@20
    .line 741
    const-string v6, "location_providers_allowed"

    #@22
    invoke-interface {v3, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    #@25
    move-result v6

    #@26
    if-ne v6, v9, :cond_6a

    #@28
    .line 742
    invoke-static {}, Lcom/qualcomm/location/ulp/UlpEngine;->access$200()Z

    #@2b
    move-result v6

    #@2c
    if-eqz v6, :cond_52

    #@2e
    const-string v7, "UlpEngine"

    #@30
    new-instance v6, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v8, "in Settings.Secure.LOCATION_PROVIDERS_ALLOWED - "

    #@37
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v8

    #@3b
    const-string v6, "location_providers_allowed"

    #@3d
    invoke-interface {v3, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@40
    move-result-object v6

    #@41
    check-cast v6, Landroid/content/ContentValues;

    #@43
    invoke-virtual {v6}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    #@46
    move-result-object v6

    #@47
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v6

    #@4b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v6

    #@4f
    invoke-static {v7, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 744
    :cond_52
    const-string v6, "location_providers_allowed"

    #@54
    invoke-interface {v3, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@57
    move-result-object v6

    #@58
    check-cast v6, Landroid/content/ContentValues;

    #@5a
    invoke-virtual {v6}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    #@5d
    move-result-object v5

    #@5e
    .line 745
    .local v5, providers:Ljava/lang/String;
    const-string v6, "gps"

    #@60
    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@63
    move-result v2

    #@64
    .line 746
    const-string v6, "network"

    #@66
    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@69
    move-result v4

    #@6a
    .line 749
    .end local v5           #providers:Ljava/lang/String;
    :cond_6a
    const-string v6, "enhLocationServices_on"

    #@6c
    invoke-interface {v3, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    #@6f
    move-result v6

    #@70
    if-ne v6, v9, :cond_c6

    #@72
    .line 750
    const-string v6, "enhLocationServices_on"

    #@74
    invoke-interface {v3, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@77
    move-result-object v6

    #@78
    check-cast v6, Landroid/content/ContentValues;

    #@7a
    invoke-virtual {v6}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    #@7d
    move-result-object v1

    #@7e
    .line 752
    if-eqz v1, :cond_be

    #@80
    .line 753
    const-string v6, "1"

    #@82
    invoke-virtual {v1, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@85
    move-result v0

    #@86
    .line 761
    :goto_86
    invoke-static {}, Lcom/qualcomm/location/ulp/UlpEngine;->access$200()Z

    #@89
    move-result v6

    #@8a
    if-eqz v6, :cond_b8

    #@8c
    .line 762
    const-string v6, "UlpEngine"

    #@8e
    new-instance v7, Ljava/lang/StringBuilder;

    #@90
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@93
    const-string v8, "SettingsObserver.update invoked and setting values. Gps:"

    #@95
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v7

    #@99
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v7

    #@9d
    const-string v8, " GNP:"

    #@9f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v7

    #@a3
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v7

    #@a7
    const-string v8, " enhLocationServicesSettingString: "

    #@a9
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v7

    #@ad
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v7

    #@b1
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b4
    move-result-object v7

    #@b5
    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b8
    .line 766
    :cond_b8
    iget-object v6, p0, Lcom/qualcomm/location/ulp/UlpEngine$SecureSettingsObserver;->this$0:Lcom/qualcomm/location/ulp/UlpEngine;

    #@ba
    invoke-static {v6, v2, v4, v0}, Lcom/qualcomm/location/ulp/UlpEngine;->access$300(Lcom/qualcomm/location/ulp/UlpEngine;ZZZ)Z

    #@bd
    .line 768
    :cond_bd
    return-void

    #@be
    .line 756
    :cond_be
    const-string v6, "UlpEngine"

    #@c0
    const-string v7, "Got null pinter for call to kvs.get(ENH_LOCATION_SERVICES_ENABLED)"

    #@c2
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c5
    goto :goto_86

    #@c6
    .line 759
    :cond_c6
    const-string v6, "UlpEngine"

    #@c8
    const-string v7, "kvs.containsKey(ENH_LOCATION_SERVICES_ENABLED) returned false"

    #@ca
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@cd
    goto :goto_86
.end method
