.class final Lcom/qualcomm/location/ulp/UlpEngine$UlpEngineHandler;
.super Landroid/os/Handler;
.source "UlpEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/location/ulp/UlpEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "UlpEngineHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/location/ulp/UlpEngine;


# direct methods
.method public constructor <init>(Lcom/qualcomm/location/ulp/UlpEngine;Landroid/os/Looper;)V
    .registers 3
    .parameter
    .parameter "looper"

    #@0
    .prologue
    .line 830
    iput-object p1, p0, Lcom/qualcomm/location/ulp/UlpEngine$UlpEngineHandler;->this$0:Lcom/qualcomm/location/ulp/UlpEngine;

    #@2
    .line 831
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 832
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 6
    .parameter "msg"

    #@0
    .prologue
    .line 836
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    .line 837
    .local v0, message:I
    invoke-static {}, Lcom/qualcomm/location/ulp/UlpEngine;->access$200()Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_20

    #@8
    const-string v1, "UlpEngine"

    #@a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "handleMessage what - "

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 838
    :cond_20
    packed-switch v0, :pswitch_data_6a

    #@23
    .line 863
    :goto_23
    return-void

    #@24
    .line 840
    :pswitch_24
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@26
    const/4 v2, 0x1

    #@27
    if-ne v1, v2, :cond_2f

    #@29
    .line 841
    iget-object v1, p0, Lcom/qualcomm/location/ulp/UlpEngine$UlpEngineHandler;->this$0:Lcom/qualcomm/location/ulp/UlpEngine;

    #@2b
    #calls: Lcom/qualcomm/location/ulp/UlpEngine;->handleEnable()V
    invoke-static {v1}, Lcom/qualcomm/location/ulp/UlpEngine;->access$500(Lcom/qualcomm/location/ulp/UlpEngine;)V

    #@2e
    goto :goto_23

    #@2f
    .line 843
    :cond_2f
    iget-object v1, p0, Lcom/qualcomm/location/ulp/UlpEngine$UlpEngineHandler;->this$0:Lcom/qualcomm/location/ulp/UlpEngine;

    #@31
    invoke-virtual {v1}, Lcom/qualcomm/location/ulp/UlpEngine;->handleDisable()V

    #@34
    goto :goto_23

    #@35
    .line 847
    :pswitch_35
    iget-object v2, p0, Lcom/qualcomm/location/ulp/UlpEngine$UlpEngineHandler;->this$0:Lcom/qualcomm/location/ulp/UlpEngine;

    #@37
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@39
    check-cast v1, Lcom/android/location/provider/ProviderRequestUnbundled;

    #@3b
    invoke-virtual {v2, v1}, Lcom/qualcomm/location/ulp/UlpEngine;->handleSetRequest(Lcom/android/location/provider/ProviderRequestUnbundled;)V

    #@3e
    goto :goto_23

    #@3f
    .line 850
    :pswitch_3f
    iget-object v1, p0, Lcom/qualcomm/location/ulp/UlpEngine$UlpEngineHandler;->this$0:Lcom/qualcomm/location/ulp/UlpEngine;

    #@41
    iget v2, p1, Landroid/os/Message;->arg1:I

    #@43
    iget v3, p1, Landroid/os/Message;->arg2:I

    #@45
    #calls: Lcom/qualcomm/location/ulp/UlpEngine;->handleNativePhoneContextRequest(II)V
    invoke-static {v1, v2, v3}, Lcom/qualcomm/location/ulp/UlpEngine;->access$600(Lcom/qualcomm/location/ulp/UlpEngine;II)V

    #@48
    goto :goto_23

    #@49
    .line 853
    :pswitch_49
    iget-object v2, p0, Lcom/qualcomm/location/ulp/UlpEngine$UlpEngineHandler;->this$0:Lcom/qualcomm/location/ulp/UlpEngine;

    #@4b
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@4d
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4f
    check-cast v1, Landroid/os/Bundle;

    #@51
    #calls: Lcom/qualcomm/location/ulp/UlpEngine;->handleNativePhoneContextUpdate(ILandroid/os/Bundle;)V
    invoke-static {v2, v3, v1}, Lcom/qualcomm/location/ulp/UlpEngine;->access$700(Lcom/qualcomm/location/ulp/UlpEngine;ILandroid/os/Bundle;)V

    #@54
    goto :goto_23

    #@55
    .line 856
    :pswitch_55
    iget-object v1, p0, Lcom/qualcomm/location/ulp/UlpEngine$UlpEngineHandler;->this$0:Lcom/qualcomm/location/ulp/UlpEngine;

    #@57
    iget v2, p1, Landroid/os/Message;->arg1:I

    #@59
    iget v3, p1, Landroid/os/Message;->arg2:I

    #@5b
    #calls: Lcom/qualcomm/location/ulp/UlpEngine;->handleNativeNetworkLocationRequest(II)V
    invoke-static {v1, v2, v3}, Lcom/qualcomm/location/ulp/UlpEngine;->access$800(Lcom/qualcomm/location/ulp/UlpEngine;II)V

    #@5e
    goto :goto_23

    #@5f
    .line 859
    :pswitch_5f
    iget-object v2, p0, Lcom/qualcomm/location/ulp/UlpEngine$UlpEngineHandler;->this$0:Lcom/qualcomm/location/ulp/UlpEngine;

    #@61
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@63
    check-cast v1, Landroid/location/Location;

    #@65
    #calls: Lcom/qualcomm/location/ulp/UlpEngine;->handleNetworkLocationUpdate(Landroid/location/Location;)V
    invoke-static {v2, v1}, Lcom/qualcomm/location/ulp/UlpEngine;->access$900(Lcom/qualcomm/location/ulp/UlpEngine;Landroid/location/Location;)V

    #@68
    goto :goto_23

    #@69
    .line 838
    nop

    #@6a
    :pswitch_data_6a
    .packed-switch 0x1
        :pswitch_24
        :pswitch_35
        :pswitch_3f
        :pswitch_49
        :pswitch_55
        :pswitch_5f
    .end packed-switch
.end method
