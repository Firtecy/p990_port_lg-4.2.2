.class Lcom/qualcomm/location/ulp/UnifiedLocationProvider$1;
.super Landroid/os/Handler;
.source "UnifiedLocationProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/location/ulp/UnifiedLocationProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/location/ulp/UnifiedLocationProvider;


# direct methods
.method constructor <init>(Lcom/qualcomm/location/ulp/UnifiedLocationProvider;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 59
    iput-object p1, p0, Lcom/qualcomm/location/ulp/UnifiedLocationProvider$1;->this$0:Lcom/qualcomm/location/ulp/UnifiedLocationProvider;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 7
    .parameter "msg"

    #@0
    .prologue
    .line 62
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    .line 63
    .local v0, msgID:I
    iget-object v2, p0, Lcom/qualcomm/location/ulp/UnifiedLocationProvider$1;->this$0:Lcom/qualcomm/location/ulp/UnifiedLocationProvider;

    #@4
    new-instance v3, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v4, "handleMessage what - "

    #@b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v3

    #@f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v3

    #@17
    #calls: Lcom/qualcomm/location/ulp/UnifiedLocationProvider;->logv(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/qualcomm/location/ulp/UnifiedLocationProvider;->access$000(Lcom/qualcomm/location/ulp/UnifiedLocationProvider;Ljava/lang/String;)V

    #@1a
    .line 64
    packed-switch v0, :pswitch_data_46

    #@1d
    .line 78
    :goto_1d
    return-void

    #@1e
    .line 66
    :pswitch_1e
    iget-object v2, p0, Lcom/qualcomm/location/ulp/UnifiedLocationProvider$1;->this$0:Lcom/qualcomm/location/ulp/UnifiedLocationProvider;

    #@20
    #getter for: Lcom/qualcomm/location/ulp/UnifiedLocationProvider;->mEngine:Lcom/qualcomm/location/ulp/UlpEngine;
    invoke-static {v2}, Lcom/qualcomm/location/ulp/UnifiedLocationProvider;->access$100(Lcom/qualcomm/location/ulp/UnifiedLocationProvider;)Lcom/qualcomm/location/ulp/UlpEngine;

    #@23
    move-result-object v2

    #@24
    iget-object v3, p0, Lcom/qualcomm/location/ulp/UnifiedLocationProvider$1;->this$0:Lcom/qualcomm/location/ulp/UnifiedLocationProvider;

    #@26
    invoke-virtual {v2, v3}, Lcom/qualcomm/location/ulp/UlpEngine;->init(Lcom/qualcomm/location/ulp/UlpEngine$Callback;)V

    #@29
    goto :goto_1d

    #@2a
    .line 69
    :pswitch_2a
    iget-object v2, p0, Lcom/qualcomm/location/ulp/UnifiedLocationProvider$1;->this$0:Lcom/qualcomm/location/ulp/UnifiedLocationProvider;

    #@2c
    #getter for: Lcom/qualcomm/location/ulp/UnifiedLocationProvider;->mEngine:Lcom/qualcomm/location/ulp/UlpEngine;
    invoke-static {v2}, Lcom/qualcomm/location/ulp/UnifiedLocationProvider;->access$100(Lcom/qualcomm/location/ulp/UnifiedLocationProvider;)Lcom/qualcomm/location/ulp/UlpEngine;

    #@2f
    move-result-object v2

    #@30
    invoke-virtual {v2}, Lcom/qualcomm/location/ulp/UlpEngine;->deinit()V

    #@33
    goto :goto_1d

    #@34
    .line 73
    :pswitch_34
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@36
    check-cast v1, Lcom/qualcomm/location/ulp/UnifiedLocationProvider$RequestWrapper;

    #@38
    .line 74
    .local v1, wrapper:Lcom/qualcomm/location/ulp/UnifiedLocationProvider$RequestWrapper;
    iget-object v2, p0, Lcom/qualcomm/location/ulp/UnifiedLocationProvider$1;->this$0:Lcom/qualcomm/location/ulp/UnifiedLocationProvider;

    #@3a
    #getter for: Lcom/qualcomm/location/ulp/UnifiedLocationProvider;->mEngine:Lcom/qualcomm/location/ulp/UlpEngine;
    invoke-static {v2}, Lcom/qualcomm/location/ulp/UnifiedLocationProvider;->access$100(Lcom/qualcomm/location/ulp/UnifiedLocationProvider;)Lcom/qualcomm/location/ulp/UlpEngine;

    #@3d
    move-result-object v2

    #@3e
    iget-object v3, v1, Lcom/qualcomm/location/ulp/UnifiedLocationProvider$RequestWrapper;->request:Lcom/android/location/provider/ProviderRequestUnbundled;

    #@40
    iget-object v4, v1, Lcom/qualcomm/location/ulp/UnifiedLocationProvider$RequestWrapper;->source:Landroid/os/WorkSource;

    #@42
    invoke-virtual {v2, v3, v4}, Lcom/qualcomm/location/ulp/UlpEngine;->setRequest(Lcom/android/location/provider/ProviderRequestUnbundled;Landroid/os/WorkSource;)V

    #@45
    goto :goto_1d

    #@46
    .line 64
    :pswitch_data_46
    .packed-switch 0x1
        :pswitch_1e
        :pswitch_2a
        :pswitch_34
    .end packed-switch
.end method
