.class Lcom/qualcomm/location/ulp/UlpEngine$1;
.super Ljava/lang/Object;
.source "UlpEngine.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/location/ulp/UlpEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/location/ulp/UlpEngine;


# direct methods
.method constructor <init>(Lcom/qualcomm/location/ulp/UlpEngine;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 459
    iput-object p1, p0, Lcom/qualcomm/location/ulp/UlpEngine$1;->this$0:Lcom/qualcomm/location/ulp/UlpEngine;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .registers 7
    .parameter "location"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 461
    invoke-static {}, Lcom/qualcomm/location/ulp/UlpEngine;->access$200()Z

    #@4
    move-result v0

    #@5
    if-eqz v0, :cond_3f

    #@7
    const-string v0, "UlpEngine"

    #@9
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v2, "onLocationChanged for NLP lat"

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    #@17
    move-result-wide v2

    #@18
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    const-string v2, "lon"

    #@1e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    #@25
    move-result-wide v2

    #@26
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    const-string v2, "accurancy "

    #@2c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    #@33
    move-result v2

    #@34
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v1

    #@3c
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    .line 463
    :cond_3f
    iget-object v0, p0, Lcom/qualcomm/location/ulp/UlpEngine$1;->this$0:Lcom/qualcomm/location/ulp/UlpEngine;

    #@41
    const/4 v1, 0x6

    #@42
    invoke-virtual {v0, v1, v4, v4, p1}, Lcom/qualcomm/location/ulp/UlpEngine;->sendMessage(IIILjava/lang/Object;)V

    #@45
    .line 464
    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .registers 5
    .parameter "arg0"

    #@0
    .prologue
    .line 472
    invoke-static {}, Lcom/qualcomm/location/ulp/UlpEngine;->access$200()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_1e

    #@6
    const-string v0, "UlpEngine"

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "onProviderEnabled for NLP.state "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 473
    :cond_1e
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .registers 5
    .parameter "arg0"

    #@0
    .prologue
    .line 469
    invoke-static {}, Lcom/qualcomm/location/ulp/UlpEngine;->access$200()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_1e

    #@6
    const-string v0, "UlpEngine"

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "onProviderEnabled for NLP.state "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 470
    :cond_1e
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .registers 7
    .parameter "arg0"
    .parameter "arg1"
    .parameter "arg2"

    #@0
    .prologue
    .line 466
    invoke-static {}, Lcom/qualcomm/location/ulp/UlpEngine;->access$200()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_1e

    #@6
    const-string v0, "UlpEngine"

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "Status update for NLP"

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 467
    :cond_1e
    return-void
.end method
