.class public Lcom/qualcomm/location/ulp/UlpService;
.super Landroid/app/Service;
.source "UlpService.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "UlpService"

.field private static final VERBOSE_DBG:Z


# instance fields
.field private mProvider:Lcom/qualcomm/location/ulp/UnifiedLocationProvider;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 16
    const-string v0, "UlpService"

    #@2
    const/4 v1, 0x2

    #@3
    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v0

    #@7
    sput-boolean v0, Lcom/qualcomm/location/ulp/UlpService;->VERBOSE_DBG:Z

    #@9
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 4
    .parameter "intent"

    #@0
    .prologue
    .line 22
    sget-boolean v0, Lcom/qualcomm/location/ulp/UlpService;->VERBOSE_DBG:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 23
    const-string v0, "UlpService"

    #@6
    const-string v1, "onBind Event"

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 24
    :cond_b
    iget-object v0, p0, Lcom/qualcomm/location/ulp/UlpService;->mProvider:Lcom/qualcomm/location/ulp/UnifiedLocationProvider;

    #@d
    if-nez v0, :cond_1a

    #@f
    .line 25
    new-instance v0, Lcom/qualcomm/location/ulp/UnifiedLocationProvider;

    #@11
    invoke-virtual {p0}, Lcom/qualcomm/location/ulp/UlpService;->getApplicationContext()Landroid/content/Context;

    #@14
    move-result-object v1

    #@15
    invoke-direct {v0, v1}, Lcom/qualcomm/location/ulp/UnifiedLocationProvider;-><init>(Landroid/content/Context;)V

    #@18
    iput-object v0, p0, Lcom/qualcomm/location/ulp/UlpService;->mProvider:Lcom/qualcomm/location/ulp/UnifiedLocationProvider;

    #@1a
    .line 27
    :cond_1a
    iget-object v0, p0, Lcom/qualcomm/location/ulp/UlpService;->mProvider:Lcom/qualcomm/location/ulp/UnifiedLocationProvider;

    #@1c
    invoke-virtual {v0}, Lcom/qualcomm/location/ulp/UnifiedLocationProvider;->getBinder()Landroid/os/IBinder;

    #@1f
    move-result-object v0

    #@20
    return-object v0
.end method

.method public onDestroy()V
    .registers 3

    #@0
    .prologue
    .line 43
    sget-boolean v0, Lcom/qualcomm/location/ulp/UlpService;->VERBOSE_DBG:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 44
    const-string v0, "UlpService"

    #@6
    const-string v1, "onDestroy Event"

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 45
    :cond_b
    const/4 v0, 0x0

    #@c
    iput-object v0, p0, Lcom/qualcomm/location/ulp/UlpService;->mProvider:Lcom/qualcomm/location/ulp/UnifiedLocationProvider;

    #@e
    .line 46
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .registers 4
    .parameter "intent"

    #@0
    .prologue
    .line 32
    sget-boolean v0, Lcom/qualcomm/location/ulp/UlpService;->VERBOSE_DBG:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 33
    const-string v0, "UlpService"

    #@6
    const-string v1, "onUnbind Event"

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 35
    :cond_b
    iget-object v0, p0, Lcom/qualcomm/location/ulp/UlpService;->mProvider:Lcom/qualcomm/location/ulp/UnifiedLocationProvider;

    #@d
    if-eqz v0, :cond_14

    #@f
    .line 36
    iget-object v0, p0, Lcom/qualcomm/location/ulp/UlpService;->mProvider:Lcom/qualcomm/location/ulp/UnifiedLocationProvider;

    #@11
    invoke-virtual {v0}, Lcom/qualcomm/location/ulp/UnifiedLocationProvider;->onDisable()V

    #@14
    .line 38
    :cond_14
    const/4 v0, 0x0

    #@15
    return v0
.end method
