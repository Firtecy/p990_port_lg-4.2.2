.class Lcom/qualcomm/location/RilInfoMonitor$1;
.super Landroid/content/BroadcastReceiver;
.source "RilInfoMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/location/RilInfoMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/location/RilInfoMonitor;


# direct methods
.method constructor <init>(Lcom/qualcomm/location/RilInfoMonitor;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 41
    iput-object p1, p0, Lcom/qualcomm/location/RilInfoMonitor$1;->this$0:Lcom/qualcomm/location/RilInfoMonitor;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 11
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v7, 0x2

    #@1
    const/4 v6, 0x0

    #@2
    .line 43
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 45
    .local v0, action:Ljava/lang/String;
    invoke-static {}, Lcom/qualcomm/location/RilInfoMonitor;->access$000()Z

    #@9
    move-result v3

    #@a
    if-eqz v3, :cond_24

    #@c
    .line 46
    const-string v3, "RilInfoMonitor"

    #@e
    new-instance v4, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v5, "mBroadcastReceiver - "

    #@15
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v4

    #@19
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v4

    #@1d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v4

    #@21
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 48
    :cond_24
    const-string v3, "android.intent.action.DATA_SMS_RECEIVED"

    #@26
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@29
    move-result v3

    #@2a
    if-eqz v3, :cond_42

    #@2c
    .line 49
    invoke-static {p2}, Landroid/provider/Telephony$Sms$Intents;->getMessagesFromIntent(Landroid/content/Intent;)[Landroid/telephony/SmsMessage;

    #@2f
    move-result-object v2

    #@30
    .line 50
    .local v2, messages:[Landroid/telephony/SmsMessage;
    const/4 v1, 0x0

    #@31
    .local v1, i:I
    :goto_31
    array-length v3, v2

    #@32
    if-ge v1, v3, :cond_55

    #@34
    .line 51
    iget-object v3, p0, Lcom/qualcomm/location/RilInfoMonitor$1;->this$0:Lcom/qualcomm/location/RilInfoMonitor;

    #@36
    aget-object v4, v2, v1

    #@38
    invoke-virtual {v4}, Landroid/telephony/SmsMessage;->getUserData()[B

    #@3b
    move-result-object v4

    #@3c
    invoke-virtual {v3, v7, v6, v6, v4}, Lcom/qualcomm/location/RilInfoMonitor;->sendMessage(IIILjava/lang/Object;)V

    #@3f
    .line 50
    add-int/lit8 v1, v1, 0x1

    #@41
    goto :goto_31

    #@42
    .line 53
    .end local v1           #i:I
    .end local v2           #messages:[Landroid/telephony/SmsMessage;
    :cond_42
    const-string v3, "android.provider.Telephony.WAP_PUSH_RECEIVED"

    #@44
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@47
    move-result v3

    #@48
    if-eqz v3, :cond_55

    #@4a
    .line 54
    iget-object v3, p0, Lcom/qualcomm/location/RilInfoMonitor$1;->this$0:Lcom/qualcomm/location/RilInfoMonitor;

    #@4c
    const-string v4, "data"

    #@4e
    invoke-virtual {p2, v4}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    #@51
    move-result-object v4

    #@52
    invoke-virtual {v3, v7, v6, v6, v4}, Lcom/qualcomm/location/RilInfoMonitor;->sendMessage(IIILjava/lang/Object;)V

    #@55
    .line 56
    :cond_55
    return-void
.end method
