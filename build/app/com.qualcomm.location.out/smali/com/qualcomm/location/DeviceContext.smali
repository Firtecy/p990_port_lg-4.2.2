.class public Lcom/qualcomm/location/DeviceContext;
.super Lcom/qualcomm/location/MonitorInterface$Monitor;
.source "DeviceContext.java"


# static fields
.field private static final CHARGER_OFF:I = 0x0

.field private static final CHARGER_ON:I = 0x1

.field private static final MSG_CHARGER_STATE_INJECT:I = 0x1

.field private static final MSG_MAX:I = 0x2

.field private static final MSG_START:I = 0x0

.field private static final TAG:Ljava/lang/String; = "DeviceContext"

.field private static final VERBOSE_DBG:Z


# instance fields
.field private final mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mChargeState:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 21
    const-string v0, "DeviceContext"

    #@2
    const/4 v1, 0x2

    #@3
    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v0

    #@7
    sput-boolean v0, Lcom/qualcomm/location/DeviceContext;->VERBOSE_DBG:Z

    #@9
    return-void
.end method

.method public constructor <init>(Lcom/qualcomm/location/MonitorInterface;I)V
    .registers 5
    .parameter "service"
    .parameter "msgIdBase"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 53
    invoke-direct {p0, p1, p2}, Lcom/qualcomm/location/MonitorInterface$Monitor;-><init>(Lcom/qualcomm/location/MonitorInterface;I)V

    #@4
    .line 29
    new-instance v0, Lcom/qualcomm/location/DeviceContext$1;

    #@6
    invoke-direct {v0, p0}, Lcom/qualcomm/location/DeviceContext$1;-><init>(Lcom/qualcomm/location/DeviceContext;)V

    #@9
    iput-object v0, p0, Lcom/qualcomm/location/DeviceContext;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@b
    .line 54
    iput v1, p0, Lcom/qualcomm/location/DeviceContext;->mChargeState:I

    #@d
    .line 55
    const/4 v0, 0x0

    #@e
    invoke-virtual {p0, v1, v1, v1, v0}, Lcom/qualcomm/location/DeviceContext;->sendMessage(IIILjava/lang/Object;)V

    #@11
    .line 56
    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 19
    invoke-static {p0}, Lcom/qualcomm/location/DeviceContext;->logv(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/qualcomm/location/DeviceContext;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/qualcomm/location/DeviceContext;->checkChargerIntent(I)V

    #@3
    return-void
.end method

.method private checkChargerIntent(I)V
    .registers 5
    .parameter "chargerState"

    #@0
    .prologue
    .line 44
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "mChargeState - "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Lcom/qualcomm/location/DeviceContext;->mChargeState:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, "; chargerState - "

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    invoke-static {v0}, Lcom/qualcomm/location/DeviceContext;->logv(Ljava/lang/String;)V

    #@22
    .line 45
    iget v0, p0, Lcom/qualcomm/location/DeviceContext;->mChargeState:I

    #@24
    if-eq v0, p1, :cond_2e

    #@26
    .line 46
    iput p1, p0, Lcom/qualcomm/location/DeviceContext;->mChargeState:I

    #@28
    .line 48
    const/4 v0, 0x1

    #@29
    const/4 v1, 0x0

    #@2a
    const/4 v2, 0x0

    #@2b
    invoke-virtual {p0, v0, p1, v1, v2}, Lcom/qualcomm/location/DeviceContext;->sendMessage(IIILjava/lang/Object;)V

    #@2e
    .line 50
    :cond_2e
    return-void
.end method

.method private static logv(Ljava/lang/String;)V
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 108
    sget-boolean v0, Lcom/qualcomm/location/DeviceContext;->VERBOSE_DBG:Z

    #@2
    if-eqz v0, :cond_9

    #@4
    const-string v0, "DeviceContext"

    #@6
    invoke-static {v0, p0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 109
    :cond_9
    return-void
.end method

.method private start()V
    .registers 11

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const/4 v7, 0x0

    #@2
    const/4 v6, 0x1

    #@3
    .line 61
    new-instance v3, Landroid/content/IntentFilter;

    #@5
    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    #@8
    .line 62
    .local v3, intentFilter:Landroid/content/IntentFilter;
    const-string v5, "android.intent.action.BATTERY_CHANGED"

    #@a
    invoke-virtual {v3, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@d
    .line 63
    iget-object v5, p0, Lcom/qualcomm/location/MonitorInterface$Monitor;->mMoniterService:Lcom/qualcomm/location/MonitorInterface;

    #@f
    invoke-interface {v5}, Lcom/qualcomm/location/MonitorInterface;->getContext()Landroid/content/Context;

    #@12
    move-result-object v5

    #@13
    invoke-virtual {v5, v9, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@16
    move-result-object v0

    #@17
    .line 65
    .local v0, batteryIntent:Landroid/content/Intent;
    const/4 v4, -0x1

    #@18
    .line 66
    .local v4, plugged:I
    const-string v5, "plugged"

    #@1a
    const/4 v8, -0x1

    #@1b
    invoke-virtual {v0, v5, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@1e
    move-result v4

    #@1f
    .line 67
    if-eq v4, v6, :cond_24

    #@21
    const/4 v5, 0x2

    #@22
    if-ne v4, v5, :cond_62

    #@24
    :cond_24
    move v1, v6

    #@25
    .line 69
    .local v1, currentBatteryCharging:Z
    :goto_25
    if-eqz v1, :cond_64

    #@27
    move v5, v6

    #@28
    :goto_28
    iput v5, p0, Lcom/qualcomm/location/DeviceContext;->mChargeState:I

    #@2a
    .line 70
    iget v5, p0, Lcom/qualcomm/location/DeviceContext;->mChargeState:I

    #@2c
    invoke-virtual {p0, v6, v5, v7, v9}, Lcom/qualcomm/location/DeviceContext;->sendMessage(IIILjava/lang/Object;)V

    #@2f
    .line 71
    new-instance v5, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v6, "In start(). mChargeState - "

    #@36
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v5

    #@3a
    iget v6, p0, Lcom/qualcomm/location/DeviceContext;->mChargeState:I

    #@3c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v5

    #@40
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v5

    #@44
    invoke-static {v5}, Lcom/qualcomm/location/DeviceContext;->logv(Ljava/lang/String;)V

    #@47
    .line 73
    new-instance v2, Landroid/content/IntentFilter;

    #@49
    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    #@4c
    .line 74
    .local v2, filter:Landroid/content/IntentFilter;
    const-string v5, "android.intent.action.ACTION_POWER_CONNECTED"

    #@4e
    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@51
    .line 75
    const-string v5, "android.intent.action.ACTION_POWER_DISCONNECTED"

    #@53
    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@56
    .line 76
    iget-object v5, p0, Lcom/qualcomm/location/MonitorInterface$Monitor;->mMoniterService:Lcom/qualcomm/location/MonitorInterface;

    #@58
    invoke-interface {v5}, Lcom/qualcomm/location/MonitorInterface;->getContext()Landroid/content/Context;

    #@5b
    move-result-object v5

    #@5c
    iget-object v6, p0, Lcom/qualcomm/location/DeviceContext;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@5e
    invoke-virtual {v5, v6, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@61
    .line 78
    return-void

    #@62
    .end local v1           #currentBatteryCharging:Z
    .end local v2           #filter:Landroid/content/IntentFilter;
    :cond_62
    move v1, v7

    #@63
    .line 67
    goto :goto_25

    #@64
    .restart local v1       #currentBatteryCharging:Z
    :cond_64
    move v5, v7

    #@65
    .line 69
    goto :goto_28
.end method


# virtual methods
.method public getNumOfMessages()I
    .registers 2

    #@0
    .prologue
    .line 104
    const/4 v0, 0x2

    #@1
    return v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 7
    .parameter "msg"

    #@0
    .prologue
    .line 82
    iget v1, p1, Landroid/os/Message;->what:I

    #@2
    .line 83
    .local v1, message:I
    const-string v2, "DeviceContext"

    #@4
    new-instance v3, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v4, "handleMessage what - "

    #@b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v3

    #@f
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v3

    #@17
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 86
    packed-switch v1, :pswitch_data_48

    #@1d
    .line 100
    :goto_1d
    return-void

    #@1e
    .line 88
    :pswitch_1e
    :try_start_1e
    invoke-direct {p0}, Lcom/qualcomm/location/DeviceContext;->start()V
    :try_end_21
    .catch Ljava/lang/ClassCastException; {:try_start_1e .. :try_end_21} :catch_22

    #@21
    goto :goto_1d

    #@22
    .line 97
    :catch_22
    move-exception v0

    #@23
    .line 98
    .local v0, cce:Ljava/lang/ClassCastException;
    const-string v2, "DeviceContext"

    #@25
    new-instance v3, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v4, "ClassCastException on "

    #@2c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v3

    #@30
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v3

    #@38
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    goto :goto_1d

    #@3c
    .line 91
    .end local v0           #cce:Ljava/lang/ClassCastException;
    :pswitch_3c
    :try_start_3c
    iget-object v2, p0, Lcom/qualcomm/location/MonitorInterface$Monitor;->mMoniterService:Lcom/qualcomm/location/MonitorInterface;

    #@3e
    invoke-interface {v2}, Lcom/qualcomm/location/MonitorInterface;->getLBSHal()Lcom/qualcomm/location/LBSHal;

    #@41
    iget v2, p1, Landroid/os/Message;->arg1:I

    #@43
    invoke-static {v2}, Lcom/qualcomm/location/LBSHal;->native_lh_charger_status_inject(I)V
    :try_end_46
    .catch Ljava/lang/ClassCastException; {:try_start_3c .. :try_end_46} :catch_22

    #@46
    goto :goto_1d

    #@47
    .line 86
    nop

    #@48
    :pswitch_data_48
    .packed-switch 0x0
        :pswitch_1e
        :pswitch_3c
    .end packed-switch
.end method
