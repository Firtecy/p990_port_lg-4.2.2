.class public abstract Lcom/qualcomm/location/MonitorInterface$Monitor;
.super Ljava/lang/Object;
.source "MonitorInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/location/MonitorInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Monitor"
.end annotation


# instance fields
.field protected final mMoniterService:Lcom/qualcomm/location/MonitorInterface;

.field private final mMsgIdBase:I


# direct methods
.method public constructor <init>(Lcom/qualcomm/location/MonitorInterface;I)V
    .registers 3
    .parameter "service"
    .parameter "msgIdBase"

    #@0
    .prologue
    .line 27
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 28
    iput-object p1, p0, Lcom/qualcomm/location/MonitorInterface$Monitor;->mMoniterService:Lcom/qualcomm/location/MonitorInterface;

    #@5
    .line 29
    iput p2, p0, Lcom/qualcomm/location/MonitorInterface$Monitor;->mMsgIdBase:I

    #@7
    .line 30
    return-void
.end method


# virtual methods
.method public final composeMessage(IIILjava/lang/Object;)Landroid/os/Message;
    .registers 8
    .parameter "what"
    .parameter "arg1"
    .parameter "arg2"
    .parameter "obj"

    #@0
    .prologue
    .line 37
    iget-object v2, p0, Lcom/qualcomm/location/MonitorInterface$Monitor;->mMoniterService:Lcom/qualcomm/location/MonitorInterface;

    #@2
    invoke-interface {v2}, Lcom/qualcomm/location/MonitorInterface;->getHandler()Landroid/os/Handler;

    #@5
    move-result-object v0

    #@6
    .line 38
    .local v0, handler:Landroid/os/Handler;
    iget v2, p0, Lcom/qualcomm/location/MonitorInterface$Monitor;->mMsgIdBase:I

    #@8
    add-int/2addr v2, p1

    #@9
    invoke-static {v0, v2, p2, p3, p4}, Landroid/os/Message;->obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;

    #@c
    move-result-object v1

    #@d
    .line 39
    .local v1, msg:Landroid/os/Message;
    return-object v1
.end method

.method public final getMsgIdBase()I
    .registers 2

    #@0
    .prologue
    .line 33
    iget v0, p0, Lcom/qualcomm/location/MonitorInterface$Monitor;->mMsgIdBase:I

    #@2
    return v0
.end method

.method public abstract getNumOfMessages()I
.end method

.method public abstract handleMessage(Landroid/os/Message;)V
.end method

.method public final sendMessage(IIILjava/lang/Object;)V
    .registers 8
    .parameter "what"
    .parameter "arg1"
    .parameter "arg2"
    .parameter "obj"

    #@0
    .prologue
    .line 43
    iget-object v2, p0, Lcom/qualcomm/location/MonitorInterface$Monitor;->mMoniterService:Lcom/qualcomm/location/MonitorInterface;

    #@2
    invoke-interface {v2}, Lcom/qualcomm/location/MonitorInterface;->getHandler()Landroid/os/Handler;

    #@5
    move-result-object v0

    #@6
    .line 44
    .local v0, handler:Landroid/os/Handler;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/qualcomm/location/MonitorInterface$Monitor;->composeMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    .line 45
    .local v1, msg:Landroid/os/Message;
    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeMessages(I)V

    #@d
    .line 46
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@10
    .line 47
    return-void
.end method
