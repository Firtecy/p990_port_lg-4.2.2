.class public Lcom/qualcomm/location/LBSSystemMonitorService;
.super Landroid/app/Service;
.source "LBSSystemMonitorService.java"

# interfaces
.implements Lcom/qualcomm/location/MonitorInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/location/LBSSystemMonitorService$1;,
        Lcom/qualcomm/location/LBSSystemMonitorService$MyHandler;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "LBSSystemMonitorService"

.field private static final VERBOSE_DBG:Z


# instance fields
.field private mHandler:Lcom/qualcomm/location/LBSSystemMonitorService$MyHandler;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mLBSHal:Lcom/qualcomm/location/LBSHal;

.field private mMonitors:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/qualcomm/location/MonitorInterface$Monitor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 28
    const-string v0, "LBSSystemMonitorService"

    #@2
    const/4 v1, 0x2

    #@3
    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v0

    #@7
    sput-boolean v0, Lcom/qualcomm/location/LBSSystemMonitorService;->VERBOSE_DBG:Z

    #@9
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 26
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    #@3
    .line 81
    return-void
.end method

.method static synthetic access$100(Lcom/qualcomm/location/LBSSystemMonitorService;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 26
    iget-object v0, p0, Lcom/qualcomm/location/LBSSystemMonitorService;->mMonitors:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method


# virtual methods
.method public getContext()Landroid/content/Context;
    .registers 1

    #@0
    .prologue
    .line 125
    return-object p0
.end method

.method public getHandler()Landroid/os/Handler;
    .registers 2

    #@0
    .prologue
    .line 130
    iget-object v0, p0, Lcom/qualcomm/location/LBSSystemMonitorService;->mHandler:Lcom/qualcomm/location/LBSSystemMonitorService$MyHandler;

    #@2
    return-object v0
.end method

.method public getLBSHal()Lcom/qualcomm/location/LBSHal;
    .registers 2

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/qualcomm/location/LBSSystemMonitorService;->mLBSHal:Lcom/qualcomm/location/LBSHal;

    #@2
    return-object v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 3
    .parameter "intent"

    #@0
    .prologue
    .line 120
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public onCreate()V
    .registers 6

    #@0
    .prologue
    .line 38
    new-instance v2, Ljava/util/ArrayList;

    #@2
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@5
    iput-object v2, p0, Lcom/qualcomm/location/LBSSystemMonitorService;->mMonitors:Ljava/util/ArrayList;

    #@7
    .line 39
    sget-boolean v2, Lcom/qualcomm/location/LBSSystemMonitorService;->VERBOSE_DBG:Z

    #@9
    if-eqz v2, :cond_12

    #@b
    .line 40
    const-string v2, "LBSSystemMonitorService"

    #@d
    const-string v3, "onCreate()"

    #@f
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 41
    :cond_12
    new-instance v2, Landroid/os/HandlerThread;

    #@14
    const-string v3, "LBSSystemMonitorService"

    #@16
    invoke-direct {v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@19
    iput-object v2, p0, Lcom/qualcomm/location/LBSSystemMonitorService;->mHandlerThread:Landroid/os/HandlerThread;

    #@1b
    .line 42
    iget-object v2, p0, Lcom/qualcomm/location/LBSSystemMonitorService;->mHandlerThread:Landroid/os/HandlerThread;

    #@1d
    invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V

    #@20
    .line 43
    new-instance v2, Lcom/qualcomm/location/LBSSystemMonitorService$MyHandler;

    #@22
    iget-object v3, p0, Lcom/qualcomm/location/LBSSystemMonitorService;->mHandlerThread:Landroid/os/HandlerThread;

    #@24
    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@27
    move-result-object v3

    #@28
    const/4 v4, 0x0

    #@29
    invoke-direct {v2, p0, v3, p0, v4}, Lcom/qualcomm/location/LBSSystemMonitorService$MyHandler;-><init>(Lcom/qualcomm/location/LBSSystemMonitorService;Landroid/os/Looper;Lcom/qualcomm/location/LBSSystemMonitorService;Lcom/qualcomm/location/LBSSystemMonitorService$1;)V

    #@2c
    iput-object v2, p0, Lcom/qualcomm/location/LBSSystemMonitorService;->mHandler:Lcom/qualcomm/location/LBSSystemMonitorService$MyHandler;

    #@2e
    .line 45
    const/4 v1, 0x1

    #@2f
    .line 50
    .local v1, msgBase:I
    invoke-static {p0, v1}, Lcom/qualcomm/location/LBSHal;->getLBSHal(Lcom/qualcomm/location/MonitorInterface;I)Lcom/qualcomm/location/LBSHal;

    #@32
    move-result-object v2

    #@33
    iput-object v2, p0, Lcom/qualcomm/location/LBSSystemMonitorService;->mLBSHal:Lcom/qualcomm/location/LBSHal;

    #@35
    .line 51
    iget-object v2, p0, Lcom/qualcomm/location/LBSSystemMonitorService;->mLBSHal:Lcom/qualcomm/location/LBSHal;

    #@37
    invoke-virtual {p0, v2}, Lcom/qualcomm/location/LBSSystemMonitorService;->subscribe(Lcom/qualcomm/location/MonitorInterface$Monitor;)V

    #@3a
    .line 52
    iget-object v2, p0, Lcom/qualcomm/location/LBSSystemMonitorService;->mLBSHal:Lcom/qualcomm/location/LBSHal;

    #@3c
    invoke-virtual {v2}, Lcom/qualcomm/location/LBSHal;->getNumOfMessages()I

    #@3f
    move-result v2

    #@40
    add-int/2addr v1, v2

    #@41
    .line 54
    const-string v2, "ro.baseband"

    #@43
    const-string v3, ""

    #@45
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@48
    move-result-object v2

    #@49
    const-string v3, "sglte"

    #@4b
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4e
    move-result v2

    #@4f
    if-eqz v2, :cond_5e

    #@51
    .line 55
    new-instance v0, Lcom/qualcomm/location/RilInfoMonitor;

    #@53
    invoke-direct {v0, p0, v1}, Lcom/qualcomm/location/RilInfoMonitor;-><init>(Lcom/qualcomm/location/MonitorInterface;I)V

    #@56
    .line 56
    .local v0, m:Lcom/qualcomm/location/MonitorInterface$Monitor;
    invoke-virtual {p0, v0}, Lcom/qualcomm/location/LBSSystemMonitorService;->subscribe(Lcom/qualcomm/location/MonitorInterface$Monitor;)V

    #@59
    .line 57
    invoke-virtual {v0}, Lcom/qualcomm/location/RilInfoMonitor;->getNumOfMessages()I

    #@5c
    move-result v2

    #@5d
    add-int/2addr v1, v2

    #@5e
    .line 60
    .end local v0           #m:Lcom/qualcomm/location/MonitorInterface$Monitor;
    :cond_5e
    new-instance v0, Lcom/qualcomm/location/DeviceContext;

    #@60
    invoke-direct {v0, p0, v1}, Lcom/qualcomm/location/DeviceContext;-><init>(Lcom/qualcomm/location/MonitorInterface;I)V

    #@63
    .line 61
    .restart local v0       #m:Lcom/qualcomm/location/MonitorInterface$Monitor;
    invoke-virtual {p0, v0}, Lcom/qualcomm/location/LBSSystemMonitorService;->subscribe(Lcom/qualcomm/location/MonitorInterface$Monitor;)V

    #@66
    .line 62
    invoke-virtual {v0}, Lcom/qualcomm/location/DeviceContext;->getNumOfMessages()I

    #@69
    move-result v2

    #@6a
    add-int/2addr v1, v2

    #@6b
    .line 65
    return-void
.end method

.method public onDestroy()V
    .registers 3

    #@0
    .prologue
    .line 69
    const-string v0, "LBSSystemMonitorService"

    #@2
    const-string v1, "onDestroy"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 70
    iget-object v1, p0, Lcom/qualcomm/location/LBSSystemMonitorService;->mHandlerThread:Landroid/os/HandlerThread;

    #@9
    monitor-enter v1

    #@a
    .line 71
    :try_start_a
    iget-object v0, p0, Lcom/qualcomm/location/LBSSystemMonitorService;->mHandlerThread:Landroid/os/HandlerThread;

    #@c
    if-eqz v0, :cond_16

    #@e
    .line 72
    iget-object v0, p0, Lcom/qualcomm/location/LBSSystemMonitorService;->mHandlerThread:Landroid/os/HandlerThread;

    #@10
    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    #@13
    .line 73
    const/4 v0, 0x0

    #@14
    iput-object v0, p0, Lcom/qualcomm/location/LBSSystemMonitorService;->mHandlerThread:Landroid/os/HandlerThread;

    #@16
    .line 75
    :cond_16
    monitor-exit v1

    #@17
    .line 79
    return-void

    #@18
    .line 75
    :catchall_18
    move-exception v0

    #@19
    monitor-exit v1
    :try_end_1a
    .catchall {:try_start_a .. :try_end_1a} :catchall_18

    #@1a
    throw v0
.end method

.method public subscribe(Lcom/qualcomm/location/MonitorInterface$Monitor;)V
    .registers 4
    .parameter "m"

    #@0
    .prologue
    .line 140
    iget-object v1, p0, Lcom/qualcomm/location/LBSSystemMonitorService;->mMonitors:Ljava/util/ArrayList;

    #@2
    monitor-enter v1

    #@3
    .line 141
    :try_start_3
    iget-object v0, p0, Lcom/qualcomm/location/LBSSystemMonitorService;->mMonitors:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@8
    .line 142
    monitor-exit v1

    #@9
    .line 143
    return-void

    #@a
    .line 142
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method public unsubscribe(Lcom/qualcomm/location/MonitorInterface$Monitor;)V
    .registers 4
    .parameter "m"

    #@0
    .prologue
    .line 147
    iget-object v1, p0, Lcom/qualcomm/location/LBSSystemMonitorService;->mMonitors:Ljava/util/ArrayList;

    #@2
    monitor-enter v1

    #@3
    .line 148
    :try_start_3
    iget-object v0, p0, Lcom/qualcomm/location/LBSSystemMonitorService;->mMonitors:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@8
    .line 149
    monitor-exit v1

    #@9
    .line 150
    return-void

    #@a
    .line 149
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method
