.class public Lcom/qualcomm/location/nlp/NlpProxyProvider;
.super Lcom/android/location/provider/LocationProviderBase;
.source "NlpProxyProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/location/nlp/NlpProxyProvider$RequestWrapper;
    }
.end annotation


# static fields
.field private static final D:Z = false

.field private static final MSG_DISABLE:I = 0x2

.field private static final MSG_ENABLE:I = 0x1

.field private static final MSG_SET_REQUEST:I = 0x3

.field private static ORG_NLP_PROPERTIES:Lcom/android/location/provider/ProviderPropertiesUnbundled; = null

.field private static PROPERTIES:Lcom/android/location/provider/ProviderPropertiesUnbundled; = null

.field private static final TAG:Ljava/lang/String; = "NlpProxy"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mEnableOnConnected:Z

.field private mHandler:Landroid/os/Handler;

.field private mLock:Ljava/lang/Object;

.field private mNlpBinder:Landroid/os/IBinder;

.field private mNlpService:Lcom/android/internal/location/ILocationProvider;

.field private mNlpServiceConnection:Landroid/content/ServiceConnection;

.field private mProviderRequestOnConnected:Lcom/android/location/provider/ProviderRequestUnbundled;

.field private mSetRequestOnConnected:Z

.field private mWorkSourceOnConnected:Landroid/os/WorkSource;


# direct methods
.method static constructor <clinit>()V
    .registers 9

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    move v2, v0

    #@3
    move v3, v1

    #@4
    move v4, v1

    #@5
    move v5, v1

    #@6
    move v6, v1

    #@7
    move v7, v0

    #@8
    move v8, v0

    #@9
    .line 63
    invoke-static/range {v0 .. v8}, Lcom/android/location/provider/ProviderPropertiesUnbundled;->create(ZZZZZZZII)Lcom/android/location/provider/ProviderPropertiesUnbundled;

    #@c
    move-result-object v2

    #@d
    sput-object v2, Lcom/qualcomm/location/nlp/NlpProxyProvider;->PROPERTIES:Lcom/android/location/provider/ProviderPropertiesUnbundled;

    #@f
    move v2, v0

    #@10
    move v3, v1

    #@11
    move v4, v1

    #@12
    move v5, v1

    #@13
    move v6, v1

    #@14
    move v7, v0

    #@15
    move v8, v0

    #@16
    .line 70
    invoke-static/range {v0 .. v8}, Lcom/android/location/provider/ProviderPropertiesUnbundled;->create(ZZZZZZZII)Lcom/android/location/provider/ProviderPropertiesUnbundled;

    #@19
    move-result-object v0

    #@1a
    sput-object v0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->ORG_NLP_PROPERTIES:Lcom/android/location/provider/ProviderPropertiesUnbundled;

    #@1c
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 100
    const-string v0, "NlpProxy"

    #@3
    sget-object v1, Lcom/qualcomm/location/nlp/NlpProxyProvider;->PROPERTIES:Lcom/android/location/provider/ProviderPropertiesUnbundled;

    #@5
    invoke-direct {p0, v0, v1}, Lcom/android/location/provider/LocationProviderBase;-><init>(Ljava/lang/String;Lcom/android/location/provider/ProviderPropertiesUnbundled;)V

    #@8
    .line 80
    new-instance v0, Ljava/lang/Object;

    #@a
    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    #@d
    iput-object v0, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mLock:Ljava/lang/Object;

    #@f
    .line 141
    new-instance v0, Lcom/qualcomm/location/nlp/NlpProxyProvider$1;

    #@11
    invoke-direct {v0, p0}, Lcom/qualcomm/location/nlp/NlpProxyProvider$1;-><init>(Lcom/qualcomm/location/nlp/NlpProxyProvider;)V

    #@14
    iput-object v0, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mNlpServiceConnection:Landroid/content/ServiceConnection;

    #@16
    .line 178
    new-instance v0, Lcom/qualcomm/location/nlp/NlpProxyProvider$2;

    #@18
    invoke-direct {v0, p0}, Lcom/qualcomm/location/nlp/NlpProxyProvider$2;-><init>(Lcom/qualcomm/location/nlp/NlpProxyProvider;)V

    #@1b
    iput-object v0, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mHandler:Landroid/os/Handler;

    #@1d
    .line 101
    iput-object p1, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mContext:Landroid/content/Context;

    #@1f
    .line 102
    iput-boolean v2, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mEnableOnConnected:Z

    #@21
    .line 103
    iput-boolean v2, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mSetRequestOnConnected:Z

    #@23
    .line 104
    invoke-direct {p0}, Lcom/qualcomm/location/nlp/NlpProxyProvider;->bind()V

    #@26
    .line 105
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .registers 6
    .parameter "context"
    .parameter "operator"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 108
    const-string v0, "NlpProxy"

    #@3
    sget-object v1, Lcom/qualcomm/location/nlp/NlpProxyProvider;->ORG_NLP_PROPERTIES:Lcom/android/location/provider/ProviderPropertiesUnbundled;

    #@5
    invoke-direct {p0, v0, v1}, Lcom/android/location/provider/LocationProviderBase;-><init>(Ljava/lang/String;Lcom/android/location/provider/ProviderPropertiesUnbundled;)V

    #@8
    .line 80
    new-instance v0, Ljava/lang/Object;

    #@a
    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    #@d
    iput-object v0, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mLock:Ljava/lang/Object;

    #@f
    .line 141
    new-instance v0, Lcom/qualcomm/location/nlp/NlpProxyProvider$1;

    #@11
    invoke-direct {v0, p0}, Lcom/qualcomm/location/nlp/NlpProxyProvider$1;-><init>(Lcom/qualcomm/location/nlp/NlpProxyProvider;)V

    #@14
    iput-object v0, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mNlpServiceConnection:Landroid/content/ServiceConnection;

    #@16
    .line 178
    new-instance v0, Lcom/qualcomm/location/nlp/NlpProxyProvider$2;

    #@18
    invoke-direct {v0, p0}, Lcom/qualcomm/location/nlp/NlpProxyProvider$2;-><init>(Lcom/qualcomm/location/nlp/NlpProxyProvider;)V

    #@1b
    iput-object v0, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mHandler:Landroid/os/Handler;

    #@1d
    .line 109
    iput-object p1, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mContext:Landroid/content/Context;

    #@1f
    .line 110
    iput-boolean v2, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mEnableOnConnected:Z

    #@21
    .line 111
    iput-boolean v2, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mSetRequestOnConnected:Z

    #@23
    .line 112
    invoke-direct {p0}, Lcom/qualcomm/location/nlp/NlpProxyProvider;->bind()V

    #@26
    .line 113
    return-void
.end method

.method static synthetic access$000(Lcom/qualcomm/location/nlp/NlpProxyProvider;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 59
    iget-object v0, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/qualcomm/location/nlp/NlpProxyProvider;)Landroid/os/IBinder;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 59
    iget-object v0, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mNlpBinder:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method static synthetic access$102(Lcom/qualcomm/location/nlp/NlpProxyProvider;Landroid/os/IBinder;)Landroid/os/IBinder;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 59
    iput-object p1, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mNlpBinder:Landroid/os/IBinder;

    #@2
    return-object p1
.end method

.method static synthetic access$200(Lcom/qualcomm/location/nlp/NlpProxyProvider;)Lcom/android/internal/location/ILocationProvider;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 59
    iget-object v0, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mNlpService:Lcom/android/internal/location/ILocationProvider;

    #@2
    return-object v0
.end method

.method static synthetic access$202(Lcom/qualcomm/location/nlp/NlpProxyProvider;Lcom/android/internal/location/ILocationProvider;)Lcom/android/internal/location/ILocationProvider;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 59
    iput-object p1, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mNlpService:Lcom/android/internal/location/ILocationProvider;

    #@2
    return-object p1
.end method

.method static synthetic access$300(Lcom/qualcomm/location/nlp/NlpProxyProvider;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mEnableOnConnected:Z

    #@2
    return v0
.end method

.method static synthetic access$302(Lcom/qualcomm/location/nlp/NlpProxyProvider;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mEnableOnConnected:Z

    #@2
    return p1
.end method

.method static synthetic access$400(Lcom/qualcomm/location/nlp/NlpProxyProvider;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 59
    iget-object v0, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Lcom/qualcomm/location/nlp/NlpProxyProvider;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mSetRequestOnConnected:Z

    #@2
    return v0
.end method

.method static synthetic access$502(Lcom/qualcomm/location/nlp/NlpProxyProvider;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mSetRequestOnConnected:Z

    #@2
    return p1
.end method

.method static synthetic access$600(Lcom/qualcomm/location/nlp/NlpProxyProvider;)Lcom/android/location/provider/ProviderRequestUnbundled;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 59
    iget-object v0, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mProviderRequestOnConnected:Lcom/android/location/provider/ProviderRequestUnbundled;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Lcom/qualcomm/location/nlp/NlpProxyProvider;)Landroid/os/WorkSource;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 59
    iget-object v0, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mWorkSourceOnConnected:Landroid/os/WorkSource;

    #@2
    return-object v0
.end method

.method private bind()V
    .registers 10

    #@0
    .prologue
    .line 116
    const-string v3, "com.google.android.location"

    #@2
    .line 117
    .local v3, packageName:Ljava/lang/String;
    const-string v0, "com.android.location.service.v2.NetworkLocationProvider"

    #@4
    .line 119
    .local v0, actionName:Ljava/lang/String;
    const-string v5, "persist.loc.nlp_name"

    #@6
    const-string v6, ""

    #@8
    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v2

    #@c
    .line 120
    .local v2, oem_nlp_package_name:Ljava/lang/String;
    const-string v5, ""

    #@e
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v5

    #@12
    if-nez v5, :cond_17

    #@14
    .line 121
    move-object v3, v2

    #@15
    .line 122
    const-string v0, "com.qualcomm.location.service.v2.NetworkLocationProvider"

    #@17
    .line 125
    :cond_17
    new-instance v1, Landroid/content/Intent;

    #@19
    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@1c
    .line 126
    .local v1, intent:Landroid/content/Intent;
    invoke-virtual {v1, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    #@1f
    .line 129
    iget-object v5, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mContext:Landroid/content/Context;

    #@21
    iget-object v6, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mNlpServiceConnection:Landroid/content/ServiceConnection;

    #@23
    const v7, 0x40000015

    #@26
    const/4 v8, -0x2

    #@27
    invoke-virtual {v5, v1, v6, v7, v8}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z

    #@2a
    move-result v4

    #@2b
    .line 134
    .local v4, success:Z
    if-eqz v4, :cond_2e

    #@2d
    .line 139
    :goto_2d
    return-void

    #@2e
    .line 137
    :cond_2e
    const-string v5, "NlpProxy"

    #@30
    new-instance v6, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v7, "bind to "

    #@37
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v6

    #@3b
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v6

    #@3f
    const-string v7, " failed!"

    #@41
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v6

    #@45
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v6

    #@49
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    goto :goto_2d
.end method


# virtual methods
.method public onDisable()V
    .registers 4

    #@0
    .prologue
    .line 254
    iget-object v1, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 255
    :try_start_3
    iget-object v0, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mNlpService:Lcom/android/internal/location/ILocationProvider;

    #@5
    if-nez v0, :cond_c

    #@7
    .line 257
    const/4 v0, 0x0

    #@8
    iput-boolean v0, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mEnableOnConnected:Z

    #@a
    .line 258
    monitor-exit v1

    #@b
    .line 263
    :goto_b
    return-void

    #@c
    .line 261
    :cond_c
    iget-object v0, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mHandler:Landroid/os/Handler;

    #@e
    const/4 v2, 0x2

    #@f
    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@12
    .line 262
    monitor-exit v1

    #@13
    goto :goto_b

    #@14
    :catchall_14
    move-exception v0

    #@15
    monitor-exit v1
    :try_end_16
    .catchall {:try_start_3 .. :try_end_16} :catchall_14

    #@16
    throw v0
.end method

.method public onEnable()V
    .registers 4

    #@0
    .prologue
    .line 240
    iget-object v1, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 241
    :try_start_3
    iget-object v0, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mNlpService:Lcom/android/internal/location/ILocationProvider;

    #@5
    if-nez v0, :cond_c

    #@7
    .line 243
    const/4 v0, 0x1

    #@8
    iput-boolean v0, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mEnableOnConnected:Z

    #@a
    .line 244
    monitor-exit v1

    #@b
    .line 249
    :goto_b
    return-void

    #@c
    .line 247
    :cond_c
    iget-object v0, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mHandler:Landroid/os/Handler;

    #@e
    const/4 v2, 0x1

    #@f
    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@12
    .line 248
    monitor-exit v1

    #@13
    goto :goto_b

    #@14
    :catchall_14
    move-exception v0

    #@15
    monitor-exit v1
    :try_end_16
    .catchall {:try_start_3 .. :try_end_16} :catchall_14

    #@16
    throw v0
.end method

.method public onGetStatus(Landroid/os/Bundle;)I
    .registers 3
    .parameter "extras"

    #@0
    .prologue
    .line 282
    const/4 v0, 0x2

    #@1
    return v0
.end method

.method public onGetStatusUpdateTime()J
    .registers 3

    #@0
    .prologue
    .line 287
    const-wide/16 v0, 0x0

    #@2
    return-wide v0
.end method

.method public onSetRequest(Lcom/android/location/provider/ProviderRequestUnbundled;Landroid/os/WorkSource;)V
    .registers 7
    .parameter "request"
    .parameter "source"

    #@0
    .prologue
    .line 268
    iget-object v1, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 269
    :try_start_3
    iget-object v0, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mNlpService:Lcom/android/internal/location/ILocationProvider;

    #@5
    if-nez v0, :cond_10

    #@7
    .line 270
    const/4 v0, 0x1

    #@8
    iput-boolean v0, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mSetRequestOnConnected:Z

    #@a
    .line 271
    iput-object p1, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mProviderRequestOnConnected:Lcom/android/location/provider/ProviderRequestUnbundled;

    #@c
    .line 272
    iput-object p2, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mWorkSourceOnConnected:Landroid/os/WorkSource;

    #@e
    .line 273
    monitor-exit v1

    #@f
    .line 278
    :goto_f
    return-void

    #@10
    .line 276
    :cond_10
    iget-object v0, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider;->mHandler:Landroid/os/Handler;

    #@12
    const/4 v2, 0x3

    #@13
    new-instance v3, Lcom/qualcomm/location/nlp/NlpProxyProvider$RequestWrapper;

    #@15
    invoke-direct {v3, p1, p2}, Lcom/qualcomm/location/nlp/NlpProxyProvider$RequestWrapper;-><init>(Lcom/android/location/provider/ProviderRequestUnbundled;Landroid/os/WorkSource;)V

    #@18
    invoke-virtual {v0, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@1b
    move-result-object v0

    #@1c
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@1f
    .line 277
    monitor-exit v1

    #@20
    goto :goto_f

    #@21
    :catchall_21
    move-exception v0

    #@22
    monitor-exit v1
    :try_end_23
    .catchall {:try_start_3 .. :try_end_23} :catchall_21

    #@23
    throw v0
.end method
