.class Lcom/qualcomm/location/nlp/NlpProxyProvider$1;
.super Ljava/lang/Object;
.source "NlpProxyProvider.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/location/nlp/NlpProxyProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/location/nlp/NlpProxyProvider;


# direct methods
.method constructor <init>(Lcom/qualcomm/location/nlp/NlpProxyProvider;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 141
    iput-object p1, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider$1;->this$0:Lcom/qualcomm/location/nlp/NlpProxyProvider;

    #@2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .registers 9
    .parameter "name"
    .parameter "binder"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider$1;->this$0:Lcom/qualcomm/location/nlp/NlpProxyProvider;

    #@2
    #getter for: Lcom/qualcomm/location/nlp/NlpProxyProvider;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/qualcomm/location/nlp/NlpProxyProvider;->access$000(Lcom/qualcomm/location/nlp/NlpProxyProvider;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    monitor-enter v1

    #@7
    .line 147
    :try_start_7
    iget-object v0, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider$1;->this$0:Lcom/qualcomm/location/nlp/NlpProxyProvider;

    #@9
    #setter for: Lcom/qualcomm/location/nlp/NlpProxyProvider;->mNlpBinder:Landroid/os/IBinder;
    invoke-static {v0, p2}, Lcom/qualcomm/location/nlp/NlpProxyProvider;->access$102(Lcom/qualcomm/location/nlp/NlpProxyProvider;Landroid/os/IBinder;)Landroid/os/IBinder;

    #@c
    .line 148
    iget-object v0, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider$1;->this$0:Lcom/qualcomm/location/nlp/NlpProxyProvider;

    #@e
    iget-object v2, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider$1;->this$0:Lcom/qualcomm/location/nlp/NlpProxyProvider;

    #@10
    #getter for: Lcom/qualcomm/location/nlp/NlpProxyProvider;->mNlpBinder:Landroid/os/IBinder;
    invoke-static {v2}, Lcom/qualcomm/location/nlp/NlpProxyProvider;->access$100(Lcom/qualcomm/location/nlp/NlpProxyProvider;)Landroid/os/IBinder;

    #@13
    move-result-object v2

    #@14
    invoke-static {v2}, Lcom/android/internal/location/ILocationProvider$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/location/ILocationProvider;

    #@17
    move-result-object v2

    #@18
    #setter for: Lcom/qualcomm/location/nlp/NlpProxyProvider;->mNlpService:Lcom/android/internal/location/ILocationProvider;
    invoke-static {v0, v2}, Lcom/qualcomm/location/nlp/NlpProxyProvider;->access$202(Lcom/qualcomm/location/nlp/NlpProxyProvider;Lcom/android/internal/location/ILocationProvider;)Lcom/android/internal/location/ILocationProvider;

    #@1b
    .line 150
    iget-object v0, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider$1;->this$0:Lcom/qualcomm/location/nlp/NlpProxyProvider;

    #@1d
    #getter for: Lcom/qualcomm/location/nlp/NlpProxyProvider;->mEnableOnConnected:Z
    invoke-static {v0}, Lcom/qualcomm/location/nlp/NlpProxyProvider;->access$300(Lcom/qualcomm/location/nlp/NlpProxyProvider;)Z

    #@20
    move-result v0

    #@21
    if-eqz v0, :cond_33

    #@23
    .line 152
    iget-object v0, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider$1;->this$0:Lcom/qualcomm/location/nlp/NlpProxyProvider;

    #@25
    #getter for: Lcom/qualcomm/location/nlp/NlpProxyProvider;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/qualcomm/location/nlp/NlpProxyProvider;->access$400(Lcom/qualcomm/location/nlp/NlpProxyProvider;)Landroid/os/Handler;

    #@28
    move-result-object v0

    #@29
    const/4 v2, 0x1

    #@2a
    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@2d
    .line 153
    iget-object v0, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider$1;->this$0:Lcom/qualcomm/location/nlp/NlpProxyProvider;

    #@2f
    const/4 v2, 0x0

    #@30
    #setter for: Lcom/qualcomm/location/nlp/NlpProxyProvider;->mEnableOnConnected:Z
    invoke-static {v0, v2}, Lcom/qualcomm/location/nlp/NlpProxyProvider;->access$302(Lcom/qualcomm/location/nlp/NlpProxyProvider;Z)Z

    #@33
    .line 156
    :cond_33
    iget-object v0, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider$1;->this$0:Lcom/qualcomm/location/nlp/NlpProxyProvider;

    #@35
    #getter for: Lcom/qualcomm/location/nlp/NlpProxyProvider;->mSetRequestOnConnected:Z
    invoke-static {v0}, Lcom/qualcomm/location/nlp/NlpProxyProvider;->access$500(Lcom/qualcomm/location/nlp/NlpProxyProvider;)Z

    #@38
    move-result v0

    #@39
    if-eqz v0, :cond_60

    #@3b
    .line 158
    iget-object v0, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider$1;->this$0:Lcom/qualcomm/location/nlp/NlpProxyProvider;

    #@3d
    #getter for: Lcom/qualcomm/location/nlp/NlpProxyProvider;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/qualcomm/location/nlp/NlpProxyProvider;->access$400(Lcom/qualcomm/location/nlp/NlpProxyProvider;)Landroid/os/Handler;

    #@40
    move-result-object v0

    #@41
    const/4 v2, 0x3

    #@42
    new-instance v3, Lcom/qualcomm/location/nlp/NlpProxyProvider$RequestWrapper;

    #@44
    iget-object v4, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider$1;->this$0:Lcom/qualcomm/location/nlp/NlpProxyProvider;

    #@46
    #getter for: Lcom/qualcomm/location/nlp/NlpProxyProvider;->mProviderRequestOnConnected:Lcom/android/location/provider/ProviderRequestUnbundled;
    invoke-static {v4}, Lcom/qualcomm/location/nlp/NlpProxyProvider;->access$600(Lcom/qualcomm/location/nlp/NlpProxyProvider;)Lcom/android/location/provider/ProviderRequestUnbundled;

    #@49
    move-result-object v4

    #@4a
    iget-object v5, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider$1;->this$0:Lcom/qualcomm/location/nlp/NlpProxyProvider;

    #@4c
    #getter for: Lcom/qualcomm/location/nlp/NlpProxyProvider;->mWorkSourceOnConnected:Landroid/os/WorkSource;
    invoke-static {v5}, Lcom/qualcomm/location/nlp/NlpProxyProvider;->access$700(Lcom/qualcomm/location/nlp/NlpProxyProvider;)Landroid/os/WorkSource;

    #@4f
    move-result-object v5

    #@50
    invoke-direct {v3, v4, v5}, Lcom/qualcomm/location/nlp/NlpProxyProvider$RequestWrapper;-><init>(Lcom/android/location/provider/ProviderRequestUnbundled;Landroid/os/WorkSource;)V

    #@53
    invoke-virtual {v0, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@56
    move-result-object v0

    #@57
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@5a
    .line 160
    iget-object v0, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider$1;->this$0:Lcom/qualcomm/location/nlp/NlpProxyProvider;

    #@5c
    const/4 v2, 0x0

    #@5d
    #setter for: Lcom/qualcomm/location/nlp/NlpProxyProvider;->mSetRequestOnConnected:Z
    invoke-static {v0, v2}, Lcom/qualcomm/location/nlp/NlpProxyProvider;->access$502(Lcom/qualcomm/location/nlp/NlpProxyProvider;Z)Z

    #@60
    .line 162
    :cond_60
    monitor-exit v1

    #@61
    .line 163
    return-void

    #@62
    .line 162
    :catchall_62
    move-exception v0

    #@63
    monitor-exit v1
    :try_end_64
    .catchall {:try_start_7 .. :try_end_64} :catchall_62

    #@64
    throw v0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .registers 5
    .parameter "name"

    #@0
    .prologue
    .line 168
    iget-object v0, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider$1;->this$0:Lcom/qualcomm/location/nlp/NlpProxyProvider;

    #@2
    #getter for: Lcom/qualcomm/location/nlp/NlpProxyProvider;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/qualcomm/location/nlp/NlpProxyProvider;->access$000(Lcom/qualcomm/location/nlp/NlpProxyProvider;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    monitor-enter v1

    #@7
    .line 169
    :try_start_7
    iget-object v0, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider$1;->this$0:Lcom/qualcomm/location/nlp/NlpProxyProvider;

    #@9
    const/4 v2, 0x0

    #@a
    #setter for: Lcom/qualcomm/location/nlp/NlpProxyProvider;->mNlpBinder:Landroid/os/IBinder;
    invoke-static {v0, v2}, Lcom/qualcomm/location/nlp/NlpProxyProvider;->access$102(Lcom/qualcomm/location/nlp/NlpProxyProvider;Landroid/os/IBinder;)Landroid/os/IBinder;

    #@d
    .line 170
    iget-object v0, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider$1;->this$0:Lcom/qualcomm/location/nlp/NlpProxyProvider;

    #@f
    const/4 v2, 0x0

    #@10
    #setter for: Lcom/qualcomm/location/nlp/NlpProxyProvider;->mNlpService:Lcom/android/internal/location/ILocationProvider;
    invoke-static {v0, v2}, Lcom/qualcomm/location/nlp/NlpProxyProvider;->access$202(Lcom/qualcomm/location/nlp/NlpProxyProvider;Lcom/android/internal/location/ILocationProvider;)Lcom/android/internal/location/ILocationProvider;

    #@13
    .line 171
    monitor-exit v1

    #@14
    .line 172
    return-void

    #@15
    .line 171
    :catchall_15
    move-exception v0

    #@16
    monitor-exit v1
    :try_end_17
    .catchall {:try_start_7 .. :try_end_17} :catchall_15

    #@17
    throw v0
.end method
