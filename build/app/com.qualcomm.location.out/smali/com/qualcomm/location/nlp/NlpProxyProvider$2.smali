.class Lcom/qualcomm/location/nlp/NlpProxyProvider$2;
.super Landroid/os/Handler;
.source "NlpProxyProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/location/nlp/NlpProxyProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/location/nlp/NlpProxyProvider;


# direct methods
.method constructor <init>(Lcom/qualcomm/location/nlp/NlpProxyProvider;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 178
    iput-object p1, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider$2;->this$0:Lcom/qualcomm/location/nlp/NlpProxyProvider;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 9
    .parameter "msg"

    #@0
    .prologue
    .line 181
    iget v4, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v4, :pswitch_data_90

    #@5
    .line 234
    :goto_5
    return-void

    #@6
    .line 185
    :pswitch_6
    iget-object v4, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider$2;->this$0:Lcom/qualcomm/location/nlp/NlpProxyProvider;

    #@8
    #getter for: Lcom/qualcomm/location/nlp/NlpProxyProvider;->mLock:Ljava/lang/Object;
    invoke-static {v4}, Lcom/qualcomm/location/nlp/NlpProxyProvider;->access$000(Lcom/qualcomm/location/nlp/NlpProxyProvider;)Ljava/lang/Object;

    #@b
    move-result-object v5

    #@c
    monitor-enter v5

    #@d
    .line 187
    :try_start_d
    iget-object v4, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider$2;->this$0:Lcom/qualcomm/location/nlp/NlpProxyProvider;

    #@f
    #getter for: Lcom/qualcomm/location/nlp/NlpProxyProvider;->mNlpService:Lcom/android/internal/location/ILocationProvider;
    invoke-static {v4}, Lcom/qualcomm/location/nlp/NlpProxyProvider;->access$200(Lcom/qualcomm/location/nlp/NlpProxyProvider;)Lcom/android/internal/location/ILocationProvider;

    #@12
    move-result-object v4

    #@13
    invoke-interface {v4}, Lcom/android/internal/location/ILocationProvider;->enable()V
    :try_end_16
    .catchall {:try_start_d .. :try_end_16} :catchall_18
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_16} :catch_1b
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_16} :catch_22

    #@16
    .line 194
    :goto_16
    :try_start_16
    monitor-exit v5

    #@17
    goto :goto_5

    #@18
    :catchall_18
    move-exception v4

    #@19
    monitor-exit v5
    :try_end_1a
    .catchall {:try_start_16 .. :try_end_1a} :catchall_18

    #@1a
    throw v4

    #@1b
    .line 188
    :catch_1b
    move-exception v0

    #@1c
    .line 189
    .local v0, e:Landroid/os/RemoteException;
    :try_start_1c
    const-string v4, "NlpProxy"

    #@1e
    invoke-static {v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    #@21
    goto :goto_16

    #@22
    .line 190
    .end local v0           #e:Landroid/os/RemoteException;
    :catch_22
    move-exception v0

    #@23
    .line 192
    .local v0, e:Ljava/lang/Exception;
    const-string v4, "NlpProxy"

    #@25
    const-string v6, "Exception "

    #@27
    invoke-static {v4, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2a
    .catchall {:try_start_1c .. :try_end_2a} :catchall_18

    #@2a
    goto :goto_16

    #@2b
    .line 200
    .end local v0           #e:Ljava/lang/Exception;
    :pswitch_2b
    iget-object v4, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider$2;->this$0:Lcom/qualcomm/location/nlp/NlpProxyProvider;

    #@2d
    #getter for: Lcom/qualcomm/location/nlp/NlpProxyProvider;->mLock:Ljava/lang/Object;
    invoke-static {v4}, Lcom/qualcomm/location/nlp/NlpProxyProvider;->access$000(Lcom/qualcomm/location/nlp/NlpProxyProvider;)Ljava/lang/Object;

    #@30
    move-result-object v5

    #@31
    monitor-enter v5

    #@32
    .line 202
    :try_start_32
    iget-object v4, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider$2;->this$0:Lcom/qualcomm/location/nlp/NlpProxyProvider;

    #@34
    #getter for: Lcom/qualcomm/location/nlp/NlpProxyProvider;->mNlpService:Lcom/android/internal/location/ILocationProvider;
    invoke-static {v4}, Lcom/qualcomm/location/nlp/NlpProxyProvider;->access$200(Lcom/qualcomm/location/nlp/NlpProxyProvider;)Lcom/android/internal/location/ILocationProvider;

    #@37
    move-result-object v4

    #@38
    invoke-interface {v4}, Lcom/android/internal/location/ILocationProvider;->disable()V
    :try_end_3b
    .catchall {:try_start_32 .. :try_end_3b} :catchall_3d
    .catch Landroid/os/RemoteException; {:try_start_32 .. :try_end_3b} :catch_40
    .catch Ljava/lang/Exception; {:try_start_32 .. :try_end_3b} :catch_47

    #@3b
    .line 209
    :goto_3b
    :try_start_3b
    monitor-exit v5

    #@3c
    goto :goto_5

    #@3d
    :catchall_3d
    move-exception v4

    #@3e
    monitor-exit v5
    :try_end_3f
    .catchall {:try_start_3b .. :try_end_3f} :catchall_3d

    #@3f
    throw v4

    #@40
    .line 203
    :catch_40
    move-exception v0

    #@41
    .line 204
    .local v0, e:Landroid/os/RemoteException;
    :try_start_41
    const-string v4, "NlpProxy"

    #@43
    invoke-static {v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    #@46
    goto :goto_3b

    #@47
    .line 205
    .end local v0           #e:Landroid/os/RemoteException;
    :catch_47
    move-exception v0

    #@48
    .line 207
    .local v0, e:Ljava/lang/Exception;
    const-string v4, "NlpProxy"

    #@4a
    const-string v6, "Exception "

    #@4c
    invoke-static {v4, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4f
    .catchall {:try_start_41 .. :try_end_4f} :catchall_3d

    #@4f
    goto :goto_3b

    #@50
    .line 215
    .end local v0           #e:Ljava/lang/Exception;
    :pswitch_50
    iget-object v4, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider$2;->this$0:Lcom/qualcomm/location/nlp/NlpProxyProvider;

    #@52
    #getter for: Lcom/qualcomm/location/nlp/NlpProxyProvider;->mLock:Ljava/lang/Object;
    invoke-static {v4}, Lcom/qualcomm/location/nlp/NlpProxyProvider;->access$000(Lcom/qualcomm/location/nlp/NlpProxyProvider;)Ljava/lang/Object;

    #@55
    move-result-object v5

    #@56
    monitor-enter v5

    #@57
    .line 216
    :try_start_57
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@59
    check-cast v3, Lcom/qualcomm/location/nlp/NlpProxyProvider$RequestWrapper;
    :try_end_5b
    .catchall {:try_start_57 .. :try_end_5b} :catchall_7c

    #@5b
    .line 218
    .local v3, wrapper:Lcom/qualcomm/location/nlp/NlpProxyProvider$RequestWrapper;
    :try_start_5b
    const-class v4, Lcom/android/location/provider/ProviderRequestUnbundled;

    #@5d
    const-string v6, "mRequest"

    #@5f
    invoke-virtual {v4, v6}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@62
    move-result-object v2

    #@63
    .line 219
    .local v2, providerRequestField:Ljava/lang/reflect/Field;
    const/4 v4, 0x1

    #@64
    invoke-virtual {v2, v4}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    #@67
    .line 220
    iget-object v4, v3, Lcom/qualcomm/location/nlp/NlpProxyProvider$RequestWrapper;->request:Lcom/android/location/provider/ProviderRequestUnbundled;

    #@69
    invoke-virtual {v2, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@6c
    move-result-object v1

    #@6d
    check-cast v1, Lcom/android/internal/location/ProviderRequest;

    #@6f
    .line 223
    .local v1, providerRequest:Lcom/android/internal/location/ProviderRequest;
    iget-object v4, p0, Lcom/qualcomm/location/nlp/NlpProxyProvider$2;->this$0:Lcom/qualcomm/location/nlp/NlpProxyProvider;

    #@71
    #getter for: Lcom/qualcomm/location/nlp/NlpProxyProvider;->mNlpService:Lcom/android/internal/location/ILocationProvider;
    invoke-static {v4}, Lcom/qualcomm/location/nlp/NlpProxyProvider;->access$200(Lcom/qualcomm/location/nlp/NlpProxyProvider;)Lcom/android/internal/location/ILocationProvider;

    #@74
    move-result-object v4

    #@75
    iget-object v6, v3, Lcom/qualcomm/location/nlp/NlpProxyProvider$RequestWrapper;->source:Landroid/os/WorkSource;

    #@77
    invoke-interface {v4, v1, v6}, Lcom/android/internal/location/ILocationProvider;->setRequest(Lcom/android/internal/location/ProviderRequest;Landroid/os/WorkSource;)V
    :try_end_7a
    .catchall {:try_start_5b .. :try_end_7a} :catchall_7c
    .catch Landroid/os/RemoteException; {:try_start_5b .. :try_end_7a} :catch_7f
    .catch Ljava/lang/Exception; {:try_start_5b .. :try_end_7a} :catch_86

    #@7a
    .line 230
    .end local v1           #providerRequest:Lcom/android/internal/location/ProviderRequest;
    .end local v2           #providerRequestField:Ljava/lang/reflect/Field;
    :goto_7a
    :try_start_7a
    monitor-exit v5

    #@7b
    goto :goto_5

    #@7c
    .end local v3           #wrapper:Lcom/qualcomm/location/nlp/NlpProxyProvider$RequestWrapper;
    :catchall_7c
    move-exception v4

    #@7d
    monitor-exit v5
    :try_end_7e
    .catchall {:try_start_7a .. :try_end_7e} :catchall_7c

    #@7e
    throw v4

    #@7f
    .line 224
    .restart local v3       #wrapper:Lcom/qualcomm/location/nlp/NlpProxyProvider$RequestWrapper;
    :catch_7f
    move-exception v0

    #@80
    .line 225
    .local v0, e:Landroid/os/RemoteException;
    :try_start_80
    const-string v4, "NlpProxy"

    #@82
    invoke-static {v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    #@85
    goto :goto_7a

    #@86
    .line 226
    .end local v0           #e:Landroid/os/RemoteException;
    :catch_86
    move-exception v0

    #@87
    .line 228
    .local v0, e:Ljava/lang/Exception;
    const-string v4, "NlpProxy"

    #@89
    const-string v6, "Exception "

    #@8b
    invoke-static {v4, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_8e
    .catchall {:try_start_80 .. :try_end_8e} :catchall_7c

    #@8e
    goto :goto_7a

    #@8f
    .line 181
    nop

    #@90
    :pswitch_data_90
    .packed-switch 0x1
        :pswitch_6
        :pswitch_2b
        :pswitch_50
    .end packed-switch
.end method
