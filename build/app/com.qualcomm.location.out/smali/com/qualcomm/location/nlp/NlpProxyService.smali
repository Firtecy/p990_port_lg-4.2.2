.class public Lcom/qualcomm/location/nlp/NlpProxyService;
.super Landroid/app/Service;
.source "NlpProxyService.java"


# instance fields
.field private mProvider:Lcom/qualcomm/location/nlp/NlpProxyProvider;


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 34
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 4
    .parameter "intent"

    #@0
    .prologue
    .line 39
    iget-object v0, p0, Lcom/qualcomm/location/nlp/NlpProxyService;->mProvider:Lcom/qualcomm/location/nlp/NlpProxyProvider;

    #@2
    if-nez v0, :cond_1b

    #@4
    .line 41
    const-string v0, "ro.build.target_operator"

    #@6
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    const-string v1, "KT"

    #@c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_22

    #@12
    .line 42
    new-instance v0, Lcom/qualcomm/location/nlp/NlpProxyProvider;

    #@14
    const-string v1, "KT"

    #@16
    invoke-direct {v0, p0, v1}, Lcom/qualcomm/location/nlp/NlpProxyProvider;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    #@19
    iput-object v0, p0, Lcom/qualcomm/location/nlp/NlpProxyService;->mProvider:Lcom/qualcomm/location/nlp/NlpProxyProvider;

    #@1b
    .line 48
    :cond_1b
    :goto_1b
    iget-object v0, p0, Lcom/qualcomm/location/nlp/NlpProxyService;->mProvider:Lcom/qualcomm/location/nlp/NlpProxyProvider;

    #@1d
    invoke-virtual {v0}, Lcom/qualcomm/location/nlp/NlpProxyProvider;->getBinder()Landroid/os/IBinder;

    #@20
    move-result-object v0

    #@21
    return-object v0

    #@22
    .line 44
    :cond_22
    new-instance v0, Lcom/qualcomm/location/nlp/NlpProxyProvider;

    #@24
    invoke-direct {v0, p0}, Lcom/qualcomm/location/nlp/NlpProxyProvider;-><init>(Landroid/content/Context;)V

    #@27
    iput-object v0, p0, Lcom/qualcomm/location/nlp/NlpProxyService;->mProvider:Lcom/qualcomm/location/nlp/NlpProxyProvider;

    #@29
    goto :goto_1b
.end method

.method public onDestroy()V
    .registers 2

    #@0
    .prologue
    .line 62
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Lcom/qualcomm/location/nlp/NlpProxyService;->mProvider:Lcom/qualcomm/location/nlp/NlpProxyProvider;

    #@3
    .line 63
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .registers 3
    .parameter "intent"

    #@0
    .prologue
    .line 54
    iget-object v0, p0, Lcom/qualcomm/location/nlp/NlpProxyService;->mProvider:Lcom/qualcomm/location/nlp/NlpProxyProvider;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 55
    iget-object v0, p0, Lcom/qualcomm/location/nlp/NlpProxyService;->mProvider:Lcom/qualcomm/location/nlp/NlpProxyProvider;

    #@6
    invoke-virtual {v0}, Lcom/qualcomm/location/nlp/NlpProxyProvider;->onDisable()V

    #@9
    .line 57
    :cond_9
    const/4 v0, 0x0

    #@a
    return v0
.end method
