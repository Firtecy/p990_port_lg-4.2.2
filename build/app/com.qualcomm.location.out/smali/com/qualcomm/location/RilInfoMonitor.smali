.class public Lcom/qualcomm/location/RilInfoMonitor;
.super Lcom/qualcomm/location/MonitorInterface$Monitor;
.source "RilInfoMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/location/RilInfoMonitor$MyPhoneStateListener;
    }
.end annotation


# static fields
.field private static final MSG_CID_INJECT:I = 0x0

.field private static final MSG_MAX:I = 0x4

.field private static final MSG_OOS_INJECT:I = 0x1

.field private static final MSG_SMS_INJECT:I = 0x2

.field private static final MSG_SMS_MULTI_INJECT:I = 0x3

.field private static final TAG:Ljava/lang/String; = "RilInfoMonitor"

.field private static final VERBOSE_DBG:Z


# instance fields
.field private final HOME_OPERATOR:Ljava/lang/String;

.field private final mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private final mListener:Lcom/qualcomm/location/RilInfoMonitor$MyPhoneStateListener;

.field private final mTelMgr:Landroid/telephony/TelephonyManager;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 31
    const-string v0, "RilInfoMonitor"

    #@2
    const/4 v1, 0x2

    #@3
    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v0

    #@7
    sput-boolean v0, Lcom/qualcomm/location/RilInfoMonitor;->VERBOSE_DBG:Z

    #@9
    return-void
.end method

.method public constructor <init>(Lcom/qualcomm/location/MonitorInterface;I)V
    .registers 5
    .parameter "service"
    .parameter "msgIdBase"

    #@0
    .prologue
    .line 94
    invoke-direct {p0, p1, p2}, Lcom/qualcomm/location/MonitorInterface$Monitor;-><init>(Lcom/qualcomm/location/MonitorInterface;I)V

    #@3
    .line 41
    new-instance v0, Lcom/qualcomm/location/RilInfoMonitor$1;

    #@5
    invoke-direct {v0, p0}, Lcom/qualcomm/location/RilInfoMonitor$1;-><init>(Lcom/qualcomm/location/RilInfoMonitor;)V

    #@8
    iput-object v0, p0, Lcom/qualcomm/location/RilInfoMonitor;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@a
    .line 95
    iget-object v0, p0, Lcom/qualcomm/location/MonitorInterface$Monitor;->mMoniterService:Lcom/qualcomm/location/MonitorInterface;

    #@c
    invoke-interface {v0}, Lcom/qualcomm/location/MonitorInterface;->getContext()Landroid/content/Context;

    #@f
    move-result-object v0

    #@10
    const-string v1, "phone"

    #@12
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@15
    move-result-object v0

    #@16
    check-cast v0, Landroid/telephony/TelephonyManager;

    #@18
    iput-object v0, p0, Lcom/qualcomm/location/RilInfoMonitor;->mTelMgr:Landroid/telephony/TelephonyManager;

    #@1a
    .line 97
    new-instance v0, Lcom/qualcomm/location/RilInfoMonitor$MyPhoneStateListener;

    #@1c
    const/4 v1, 0x0

    #@1d
    invoke-direct {v0, p0, v1}, Lcom/qualcomm/location/RilInfoMonitor$MyPhoneStateListener;-><init>(Lcom/qualcomm/location/RilInfoMonitor;Lcom/qualcomm/location/RilInfoMonitor$1;)V

    #@20
    iput-object v0, p0, Lcom/qualcomm/location/RilInfoMonitor;->mListener:Lcom/qualcomm/location/RilInfoMonitor$MyPhoneStateListener;

    #@22
    .line 99
    iget-object v0, p0, Lcom/qualcomm/location/RilInfoMonitor;->mTelMgr:Landroid/telephony/TelephonyManager;

    #@24
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    #@27
    move-result-object v0

    #@28
    iput-object v0, p0, Lcom/qualcomm/location/RilInfoMonitor;->HOME_OPERATOR:Ljava/lang/String;

    #@2a
    .line 101
    invoke-direct {p0}, Lcom/qualcomm/location/RilInfoMonitor;->startMonitor()V

    #@2d
    .line 102
    return-void
.end method

.method static synthetic access$000()Z
    .registers 1

    #@0
    .prologue
    .line 29
    sget-boolean v0, Lcom/qualcomm/location/RilInfoMonitor;->VERBOSE_DBG:Z

    #@2
    return v0
.end method

.method private static getMcc(I)I
    .registers 5
    .parameter "mncmccCombo"

    #@0
    .prologue
    .line 181
    div-int/lit8 v0, p0, 0x64

    #@2
    .line 182
    .local v0, mcc:I
    sget-boolean v1, Lcom/qualcomm/location/RilInfoMonitor;->VERBOSE_DBG:Z

    #@4
    if-eqz v1, :cond_1e

    #@6
    .line 183
    const-string v1, "RilInfoMonitor"

    #@8
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v3, "getMcc() - "

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v2

    #@1b
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 184
    :cond_1e
    return v0
.end method

.method private static getMnc(I)I
    .registers 5
    .parameter "mncmccCombo"

    #@0
    .prologue
    .line 174
    rem-int/lit8 v0, p0, 0x64

    #@2
    .line 175
    .local v0, mnc:I
    sget-boolean v1, Lcom/qualcomm/location/RilInfoMonitor;->VERBOSE_DBG:Z

    #@4
    if-eqz v1, :cond_1e

    #@6
    .line 176
    const-string v1, "RilInfoMonitor"

    #@8
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v3, "getMnc() - "

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v2

    #@1b
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 177
    :cond_1e
    return v0
.end method

.method private getMncMccCombo()I
    .registers 6

    #@0
    .prologue
    .line 162
    iget-object v2, p0, Lcom/qualcomm/location/RilInfoMonitor;->mTelMgr:Landroid/telephony/TelephonyManager;

    #@2
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 163
    .local v0, networkOperator:Ljava/lang/String;
    sget-boolean v2, Lcom/qualcomm/location/RilInfoMonitor;->VERBOSE_DBG:Z

    #@8
    if-eqz v2, :cond_2e

    #@a
    .line 164
    const-string v2, "RilInfoMonitor"

    #@c
    new-instance v3, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v4, "getMncMccCombo() - "

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    const-string v4, "; Sim Operator - "

    #@1d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    iget-object v4, p0, Lcom/qualcomm/location/RilInfoMonitor;->HOME_OPERATOR:Ljava/lang/String;

    #@23
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v3

    #@2b
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 167
    :cond_2e
    :try_start_2e
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_31
    .catch Ljava/lang/NumberFormatException; {:try_start_2e .. :try_end_31} :catch_33

    #@31
    move-result v2

    #@32
    .line 169
    :goto_32
    return v2

    #@33
    .line 168
    :catch_33
    move-exception v1

    #@34
    .line 169
    .local v1, nfe:Ljava/lang/NumberFormatException;
    const/4 v2, -0x1

    #@35
    goto :goto_32
.end method

.method private getRoaming()Z
    .registers 5

    #@0
    .prologue
    .line 155
    iget-object v1, p0, Lcom/qualcomm/location/RilInfoMonitor;->mTelMgr:Landroid/telephony/TelephonyManager;

    #@2
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    #@5
    move-result v0

    #@6
    .line 156
    .local v0, roaming:Z
    sget-boolean v1, Lcom/qualcomm/location/RilInfoMonitor;->VERBOSE_DBG:Z

    #@8
    if-eqz v1, :cond_22

    #@a
    .line 157
    const-string v1, "RilInfoMonitor"

    #@c
    new-instance v2, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v3, "getRoaming() - "

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 158
    :cond_22
    return v0
.end method

.method private startMonitor()V
    .registers 6

    #@0
    .prologue
    .line 105
    iget-object v2, p0, Lcom/qualcomm/location/RilInfoMonitor;->mTelMgr:Landroid/telephony/TelephonyManager;

    #@2
    iget-object v3, p0, Lcom/qualcomm/location/RilInfoMonitor;->mListener:Lcom/qualcomm/location/RilInfoMonitor$MyPhoneStateListener;

    #@4
    const/16 v4, 0x11

    #@6
    invoke-virtual {v2, v3, v4}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    #@9
    .line 107
    new-instance v1, Landroid/content/IntentFilter;

    #@b
    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    #@e
    .line 108
    .local v1, filter:Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.DATA_SMS_RECEIVED"

    #@10
    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@13
    .line 109
    const-string v2, "sms"

    #@15
    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    #@18
    .line 110
    const-string v2, "localhost"

    #@1a
    const-string v3, "7275"

    #@1c
    invoke-virtual {v1, v2, v3}, Landroid/content/IntentFilter;->addDataAuthority(Ljava/lang/String;Ljava/lang/String;)V

    #@1f
    .line 111
    iget-object v2, p0, Lcom/qualcomm/location/MonitorInterface$Monitor;->mMoniterService:Lcom/qualcomm/location/MonitorInterface;

    #@21
    invoke-interface {v2}, Lcom/qualcomm/location/MonitorInterface;->getContext()Landroid/content/Context;

    #@24
    move-result-object v2

    #@25
    iget-object v3, p0, Lcom/qualcomm/location/RilInfoMonitor;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@27
    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@2a
    .line 113
    new-instance v1, Landroid/content/IntentFilter;

    #@2c
    .end local v1           #filter:Landroid/content/IntentFilter;
    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    #@2f
    .line 114
    .restart local v1       #filter:Landroid/content/IntentFilter;
    const-string v2, "android.provider.Telephony.WAP_PUSH_RECEIVED"

    #@31
    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@34
    .line 116
    :try_start_34
    const-string v2, "application/vnd.omaloc-supl-init"

    #@36
    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addDataType(Ljava/lang/String;)V
    :try_end_39
    .catch Landroid/content/IntentFilter$MalformedMimeTypeException; {:try_start_34 .. :try_end_39} :catch_45

    #@39
    .line 120
    :goto_39
    iget-object v2, p0, Lcom/qualcomm/location/MonitorInterface$Monitor;->mMoniterService:Lcom/qualcomm/location/MonitorInterface;

    #@3b
    invoke-interface {v2}, Lcom/qualcomm/location/MonitorInterface;->getContext()Landroid/content/Context;

    #@3e
    move-result-object v2

    #@3f
    iget-object v3, p0, Lcom/qualcomm/location/RilInfoMonitor;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@41
    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@44
    .line 121
    return-void

    #@45
    .line 117
    :catch_45
    move-exception v0

    #@46
    .line 118
    .local v0, e:Landroid/content/IntentFilter$MalformedMimeTypeException;
    const-string v2, "RilInfoMonitor"

    #@48
    const-string v3, "Malformed SUPL init mime type"

    #@4a
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    goto :goto_39
.end method


# virtual methods
.method public getNumOfMessages()I
    .registers 2

    #@0
    .prologue
    .line 151
    const/4 v0, 0x4

    #@1
    return v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 11
    .parameter "msg"

    #@0
    .prologue
    .line 125
    iget v7, p1, Landroid/os/Message;->what:I

    #@2
    .line 126
    .local v7, message:I
    const-string v0, "RilInfoMonitor"

    #@4
    new-instance v1, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v2, "handleMessage what - "

    #@b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 128
    packed-switch v7, :pswitch_data_5a

    #@1d
    .line 147
    :cond_1d
    :goto_1d
    :pswitch_1d
    return-void

    #@1e
    .line 130
    :pswitch_1e
    invoke-direct {p0}, Lcom/qualcomm/location/RilInfoMonitor;->getMncMccCombo()I

    #@21
    move-result v8

    #@22
    .line 131
    .local v8, mncMcc:I
    if-ltz v8, :cond_1d

    #@24
    .line 132
    iget-object v0, p0, Lcom/qualcomm/location/MonitorInterface$Monitor;->mMoniterService:Lcom/qualcomm/location/MonitorInterface;

    #@26
    invoke-interface {v0}, Lcom/qualcomm/location/MonitorInterface;->getLBSHal()Lcom/qualcomm/location/LBSHal;

    #@29
    move-result-object v0

    #@2a
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@2c
    iget v2, p1, Landroid/os/Message;->arg2:I

    #@2e
    invoke-static {v8}, Lcom/qualcomm/location/RilInfoMonitor;->getMnc(I)I

    #@31
    move-result v3

    #@32
    invoke-static {v8}, Lcom/qualcomm/location/RilInfoMonitor;->getMcc(I)I

    #@35
    move-result v4

    #@36
    invoke-direct {p0}, Lcom/qualcomm/location/RilInfoMonitor;->getRoaming()Z

    #@39
    move-result v5

    #@3a
    invoke-virtual/range {v0 .. v5}, Lcom/qualcomm/location/LBSHal;->native_lh_cinfo_inject(IIIIZ)V

    #@3d
    goto :goto_1d

    #@3e
    .line 136
    .end local v8           #mncMcc:I
    :pswitch_3e
    iget-object v0, p0, Lcom/qualcomm/location/MonitorInterface$Monitor;->mMoniterService:Lcom/qualcomm/location/MonitorInterface;

    #@40
    invoke-interface {v0}, Lcom/qualcomm/location/MonitorInterface;->getLBSHal()Lcom/qualcomm/location/LBSHal;

    #@43
    move-result-object v0

    #@44
    invoke-virtual {v0}, Lcom/qualcomm/location/LBSHal;->native_lh_oos_inform()V

    #@47
    goto :goto_1d

    #@48
    .line 139
    :pswitch_48
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4a
    check-cast v0, [B

    #@4c
    move-object v6, v0

    #@4d
    check-cast v6, [B

    #@4f
    .line 140
    .local v6, data:[B
    iget-object v0, p0, Lcom/qualcomm/location/MonitorInterface$Monitor;->mMoniterService:Lcom/qualcomm/location/MonitorInterface;

    #@51
    invoke-interface {v0}, Lcom/qualcomm/location/MonitorInterface;->getLBSHal()Lcom/qualcomm/location/LBSHal;

    #@54
    move-result-object v0

    #@55
    array-length v1, v6

    #@56
    invoke-virtual {v0, v6, v1}, Lcom/qualcomm/location/LBSHal;->native_lh_ni_supl_init([BI)V

    #@59
    goto :goto_1d

    #@5a
    .line 128
    :pswitch_data_5a
    .packed-switch 0x0
        :pswitch_1e
        :pswitch_3e
        :pswitch_48
        :pswitch_1d
    .end packed-switch
.end method
