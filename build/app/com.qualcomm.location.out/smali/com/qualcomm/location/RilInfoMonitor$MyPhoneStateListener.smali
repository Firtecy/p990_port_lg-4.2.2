.class final Lcom/qualcomm/location/RilInfoMonitor$MyPhoneStateListener;
.super Landroid/telephony/PhoneStateListener;
.source "RilInfoMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/location/RilInfoMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MyPhoneStateListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/location/RilInfoMonitor;


# direct methods
.method private constructor <init>(Lcom/qualcomm/location/RilInfoMonitor;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 59
    iput-object p1, p0, Lcom/qualcomm/location/RilInfoMonitor$MyPhoneStateListener;->this$0:Lcom/qualcomm/location/RilInfoMonitor;

    #@2
    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/qualcomm/location/RilInfoMonitor;Lcom/qualcomm/location/RilInfoMonitor$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/qualcomm/location/RilInfoMonitor$MyPhoneStateListener;-><init>(Lcom/qualcomm/location/RilInfoMonitor;)V

    #@3
    return-void
.end method


# virtual methods
.method public onCellLocationChanged(Landroid/telephony/CellLocation;)V
    .registers 8
    .parameter "location"

    #@0
    .prologue
    .line 62
    instance-of v3, p1, Landroid/telephony/gsm/GsmCellLocation;

    #@2
    if-eqz v3, :cond_69

    #@4
    move-object v1, p1

    #@5
    .line 63
    check-cast v1, Landroid/telephony/gsm/GsmCellLocation;

    #@7
    .line 64
    .local v1, gsmCell:Landroid/telephony/gsm/GsmCellLocation;
    const/4 v0, -0x1

    #@8
    .line 65
    .local v0, cid:I
    const/4 v2, -0x1

    #@9
    .line 66
    .local v2, lac:I
    invoke-static {}, Lcom/qualcomm/location/RilInfoMonitor;->access$000()Z

    #@c
    move-result v3

    #@d
    if-eqz v3, :cond_2b

    #@f
    .line 67
    const-string v3, "RilInfoMonitor"

    #@11
    new-instance v4, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v5, "onCellLocationChanged: psc - "

    #@18
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {v1}, Landroid/telephony/gsm/GsmCellLocation;->getPsc()I

    #@1f
    move-result v5

    #@20
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23
    move-result-object v4

    #@24
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v4

    #@28
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 68
    :cond_2b
    invoke-virtual {v1}, Landroid/telephony/gsm/GsmCellLocation;->getPsc()I

    #@2e
    move-result v3

    #@2f
    const/4 v4, -0x1

    #@30
    if-ne v3, v4, :cond_3a

    #@32
    .line 69
    invoke-virtual {v1}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    #@35
    move-result v0

    #@36
    .line 70
    invoke-virtual {v1}, Landroid/telephony/gsm/GsmCellLocation;->getLac()I

    #@39
    move-result v2

    #@3a
    .line 73
    :cond_3a
    invoke-static {}, Lcom/qualcomm/location/RilInfoMonitor;->access$000()Z

    #@3d
    move-result v3

    #@3e
    if-eqz v3, :cond_62

    #@40
    .line 74
    const-string v3, "RilInfoMonitor"

    #@42
    new-instance v4, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v5, "onCellLocationChanged: cid - "

    #@49
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v4

    #@4d
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@50
    move-result-object v4

    #@51
    const-string v5, "; lac - "

    #@53
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v4

    #@57
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v4

    #@5b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v4

    #@5f
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@62
    .line 77
    :cond_62
    iget-object v3, p0, Lcom/qualcomm/location/RilInfoMonitor$MyPhoneStateListener;->this$0:Lcom/qualcomm/location/RilInfoMonitor;

    #@64
    const/4 v4, 0x0

    #@65
    const/4 v5, 0x0

    #@66
    invoke-virtual {v3, v4, v0, v2, v5}, Lcom/qualcomm/location/RilInfoMonitor;->sendMessage(IIILjava/lang/Object;)V

    #@69
    .line 79
    .end local v0           #cid:I
    .end local v1           #gsmCell:Landroid/telephony/gsm/GsmCellLocation;
    .end local v2           #lac:I
    :cond_69
    return-void
.end method

.method public onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .registers 6
    .parameter "serviceState"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 83
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getState()I

    #@5
    move-result v0

    #@6
    if-ne v2, v0, :cond_21

    #@8
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getDataState()I

    #@b
    move-result v0

    #@c
    if-ne v2, v0, :cond_21

    #@e
    .line 85
    invoke-static {}, Lcom/qualcomm/location/RilInfoMonitor;->access$000()Z

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_1b

    #@14
    .line 86
    const-string v0, "RilInfoMonitor"

    #@16
    const-string v1, "onServiceStateChanged - OOS"

    #@18
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 88
    :cond_1b
    iget-object v0, p0, Lcom/qualcomm/location/RilInfoMonitor$MyPhoneStateListener;->this$0:Lcom/qualcomm/location/RilInfoMonitor;

    #@1d
    const/4 v1, 0x0

    #@1e
    invoke-virtual {v0, v2, v3, v3, v1}, Lcom/qualcomm/location/RilInfoMonitor;->sendMessage(IIILjava/lang/Object;)V

    #@21
    .line 90
    :cond_21
    return-void
.end method
