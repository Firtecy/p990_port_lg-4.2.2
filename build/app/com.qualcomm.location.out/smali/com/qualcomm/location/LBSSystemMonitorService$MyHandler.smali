.class Lcom/qualcomm/location/LBSSystemMonitorService$MyHandler;
.super Landroid/os/Handler;
.source "LBSSystemMonitorService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/location/LBSSystemMonitorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyHandler"
.end annotation


# instance fields
.field private final mService:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/qualcomm/location/LBSSystemMonitorService;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/qualcomm/location/LBSSystemMonitorService;


# direct methods
.method private constructor <init>(Lcom/qualcomm/location/LBSSystemMonitorService;Landroid/os/Looper;Lcom/qualcomm/location/LBSSystemMonitorService;)V
    .registers 5
    .parameter
    .parameter "looper"
    .parameter "ms"

    #@0
    .prologue
    .line 84
    iput-object p1, p0, Lcom/qualcomm/location/LBSSystemMonitorService$MyHandler;->this$0:Lcom/qualcomm/location/LBSSystemMonitorService;

    #@2
    .line 85
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 86
    new-instance v0, Ljava/lang/ref/WeakReference;

    #@7
    invoke-direct {v0, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@a
    iput-object v0, p0, Lcom/qualcomm/location/LBSSystemMonitorService$MyHandler;->mService:Ljava/lang/ref/WeakReference;

    #@c
    .line 87
    return-void
.end method

.method synthetic constructor <init>(Lcom/qualcomm/location/LBSSystemMonitorService;Landroid/os/Looper;Lcom/qualcomm/location/LBSSystemMonitorService;Lcom/qualcomm/location/LBSSystemMonitorService$1;)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 81
    invoke-direct {p0, p1, p2, p3}, Lcom/qualcomm/location/LBSSystemMonitorService$MyHandler;-><init>(Lcom/qualcomm/location/LBSSystemMonitorService;Landroid/os/Looper;Lcom/qualcomm/location/LBSSystemMonitorService;)V

    #@3
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .registers 11
    .parameter "msg"

    #@0
    .prologue
    .line 91
    iget v5, p1, Landroid/os/Message;->what:I

    #@2
    .line 92
    .local v5, msgID:I
    const-string v6, "LBSSystemMonitorService"

    #@4
    new-instance v7, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v8, "handleMessage what - "

    #@b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v7

    #@f
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v7

    #@13
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v7

    #@17
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 94
    iget-object v6, p0, Lcom/qualcomm/location/LBSSystemMonitorService$MyHandler;->mService:Ljava/lang/ref/WeakReference;

    #@1c
    invoke-virtual {v6}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@1f
    move-result-object v6

    #@20
    if-eqz v6, :cond_5a

    #@22
    .line 95
    const/4 v4, 0x0

    #@23
    .line 96
    .local v4, monitorHandler:Lcom/qualcomm/location/MonitorInterface$Monitor;
    iget-object v6, p0, Lcom/qualcomm/location/LBSSystemMonitorService$MyHandler;->this$0:Lcom/qualcomm/location/LBSSystemMonitorService;

    #@25
    invoke-static {v6}, Lcom/qualcomm/location/LBSSystemMonitorService;->access$100(Lcom/qualcomm/location/LBSSystemMonitorService;)Ljava/util/ArrayList;

    #@28
    move-result-object v7

    #@29
    monitor-enter v7

    #@2a
    .line 97
    :try_start_2a
    iget-object v6, p0, Lcom/qualcomm/location/LBSSystemMonitorService$MyHandler;->this$0:Lcom/qualcomm/location/LBSSystemMonitorService;

    #@2c
    invoke-static {v6}, Lcom/qualcomm/location/LBSSystemMonitorService;->access$100(Lcom/qualcomm/location/LBSSystemMonitorService;)Ljava/util/ArrayList;

    #@2f
    move-result-object v6

    #@30
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@33
    move-result-object v2

    #@34
    .local v2, i$:Ljava/util/Iterator;
    :cond_34
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@37
    move-result v6

    #@38
    if-eqz v6, :cond_54

    #@3a
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@3d
    move-result-object v3

    #@3e
    check-cast v3, Lcom/qualcomm/location/MonitorInterface$Monitor;

    #@40
    .line 98
    .local v3, monitor:Lcom/qualcomm/location/MonitorInterface$Monitor;
    invoke-virtual {v3}, Lcom/qualcomm/location/MonitorInterface$Monitor;->getMsgIdBase()I

    #@43
    move-result v1

    #@44
    .line 99
    .local v1, handlerStart:I
    invoke-virtual {v3}, Lcom/qualcomm/location/MonitorInterface$Monitor;->getNumOfMessages()I

    #@47
    move-result v6

    #@48
    add-int v0, v1, v6

    #@4a
    .line 100
    .local v0, handlerEnd:I
    if-ge v5, v0, :cond_34

    #@4c
    if-lt v5, v1, :cond_34

    #@4e
    .line 104
    iget v6, p1, Landroid/os/Message;->what:I

    #@50
    sub-int/2addr v6, v1

    #@51
    iput v6, p1, Landroid/os/Message;->what:I

    #@53
    .line 105
    move-object v4, v3

    #@54
    .line 109
    .end local v0           #handlerEnd:I
    .end local v1           #handlerStart:I
    .end local v3           #monitor:Lcom/qualcomm/location/MonitorInterface$Monitor;
    :cond_54
    monitor-exit v7
    :try_end_55
    .catchall {:try_start_2a .. :try_end_55} :catchall_5b

    #@55
    .line 111
    if-eqz v4, :cond_5a

    #@57
    .line 112
    invoke-virtual {v4, p1}, Lcom/qualcomm/location/MonitorInterface$Monitor;->handleMessage(Landroid/os/Message;)V

    #@5a
    .line 115
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v4           #monitorHandler:Lcom/qualcomm/location/MonitorInterface$Monitor;
    :cond_5a
    return-void

    #@5b
    .line 109
    .restart local v4       #monitorHandler:Lcom/qualcomm/location/MonitorInterface$Monitor;
    :catchall_5b
    move-exception v6

    #@5c
    :try_start_5c
    monitor-exit v7
    :try_end_5d
    .catchall {:try_start_5c .. :try_end_5d} :catchall_5b

    #@5d
    throw v6
.end method
