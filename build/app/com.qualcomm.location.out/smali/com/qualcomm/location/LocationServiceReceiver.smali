.class public Lcom/qualcomm/location/LocationServiceReceiver;
.super Landroid/content/BroadcastReceiver;
.source "LocationServiceReceiver.java"


# static fields
.field private static final MAX_SHUTDOWN_TIME_MS:I = 0xc8

.field private static final TAG:Ljava/lang/String; = "LocationServiceReceiver"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 23
    const-string v0, "locationservice"

    #@2
    #invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@5
    .line 24
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 18
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@3
    return-void
.end method

.method static synthetic access$000(Lcom/qualcomm/location/LocationServiceReceiver;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 18
    invoke-direct {p0}, Lcom/qualcomm/location/LocationServiceReceiver;->nativeShutdown()V

    #@3
    return-void
.end method

.method private nativeShutdown()V
	.registers 1
	return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 10
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 30
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v2

    #@4
    .line 31
    .local v2, intentAction:Ljava/lang/String;
    if-eqz v2, :cond_46

    #@6
    .line 32
    const-string v4, "android.intent.action.ACTION_SHUTDOWN"

    #@8
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v4

    #@c
    if-eqz v4, :cond_63

    #@e
    .line 33
    const-string v4, "LocationServiceReceiver"

    #@10
    const-string v5, "nativeShutdown start."

    #@12
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    .line 34
    new-instance v3, Ljava/lang/Thread;

    #@17
    new-instance v4, Lcom/qualcomm/location/LocationServiceReceiver$1;

    #@19
    invoke-direct {v4, p0}, Lcom/qualcomm/location/LocationServiceReceiver$1;-><init>(Lcom/qualcomm/location/LocationServiceReceiver;)V

    #@1c
    invoke-direct {v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    #@1f
    .line 40
    .local v3, threadShutdown:Ljava/lang/Thread;
    :try_start_1f
    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    #@22
    .line 43
    const-wide/16 v4, 0xc8

    #@24
    invoke-virtual {v3, v4, v5}, Ljava/lang/Thread;->join(J)V

    #@27
    .line 44
    const-string v5, "LocationServiceReceiver"

    #@29
    new-instance v4, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v6, "nativeShutdown done. Completed within time limit is "

    #@30
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v6

    #@34
    invoke-virtual {v3}, Ljava/lang/Thread;->isAlive()Z

    #@37
    move-result v4

    #@38
    if-nez v4, :cond_47

    #@3a
    const/4 v4, 0x1

    #@3b
    :goto_3b
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v4

    #@3f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v4

    #@43
    invoke-static {v5, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_46
    .catch Ljava/lang/InterruptedException; {:try_start_1f .. :try_end_46} :catch_49

    #@46
    .line 53
    .end local v3           #threadShutdown:Ljava/lang/Thread;
    :cond_46
    :goto_46
    return-void

    #@47
    .line 44
    .restart local v3       #threadShutdown:Ljava/lang/Thread;
    :cond_47
    const/4 v4, 0x0

    #@48
    goto :goto_3b

    #@49
    .line 45
    :catch_49
    move-exception v0

    #@4a
    .line 46
    .local v0, ex:Ljava/lang/InterruptedException;
    const-string v4, "LocationServiceReceiver"

    #@4c
    new-instance v5, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v6, "InterruptedException "

    #@53
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v5

    #@57
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v5

    #@5b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v5

    #@5f
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@62
    goto :goto_46

    #@63
    .line 48
    .end local v0           #ex:Ljava/lang/InterruptedException;
    .end local v3           #threadShutdown:Ljava/lang/Thread;
    :cond_63
    const-string v4, "android.intent.action.BOOT_COMPLETED"

    #@65
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@68
    move-result v4

    #@69
    if-eqz v4, :cond_46

    #@6b
    .line 49
    new-instance v1, Landroid/content/Intent;

    #@6d
    const-class v4, Lcom/qualcomm/location/LBSSystemMonitorService;

    #@6f
    invoke-direct {v1, p1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    #@72
    .line 50
    .local v1, i:Landroid/content/Intent;
    invoke-virtual {p1, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    #@75
    goto :goto_46
.end method
