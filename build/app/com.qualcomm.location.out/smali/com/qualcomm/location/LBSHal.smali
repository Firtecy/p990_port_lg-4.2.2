.class public Lcom/qualcomm/location/LBSHal;
.super Lcom/qualcomm/location/MonitorInterface$Monitor;
.source "LBSHal.java"


# static fields
.field private static final MSG_HAL_SSR:I = 0x0

.field private static final MSG_MAX:I = 0x1

.field private static final TAG:Ljava/lang/String; = "LBSHal"

.field private static final VERBOSE_DBG:Z

.field private static mLBSHal:Lcom/qualcomm/location/LBSHal;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 19
    const-string v0, "LBSHal"

    #@2
    const/4 v1, 0x2

    #@3
    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v0

    #@7
    sput-boolean v0, Lcom/qualcomm/location/LBSHal;->VERBOSE_DBG:Z

    #@9
    .line 70
    const-string v0, "locationservice"

    #@b
    #invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@e
    .line 71
    invoke-static {}, Lcom/qualcomm/location/LBSHal;->native_lh_class_init()V

    #@11
    .line 72
    return-void
.end method

.method private constructor <init>(Lcom/qualcomm/location/MonitorInterface;I)V
    .registers 3
    .parameter "service"
    .parameter "msgIdBase"

    #@0
    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/qualcomm/location/MonitorInterface$Monitor;-><init>(Lcom/qualcomm/location/MonitorInterface;I)V

    #@3
    .line 26
    invoke-direct {p0}, Lcom/qualcomm/location/LBSHal;->ssrestart()V

    #@6
    .line 27
    return-void
.end method

.method public static getLBSHal(Lcom/qualcomm/location/MonitorInterface;I)Lcom/qualcomm/location/LBSHal;
    .registers 4
    .parameter "service"
    .parameter "msgIdBase"

    #@0
    .prologue
    .line 30
    sget-boolean v0, Lcom/qualcomm/location/LBSHal;->VERBOSE_DBG:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 31
    const-string v0, "LBSHal"

    #@6
    const-string v1, "getLBSHal()"

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 33
    :cond_b
    sget-object v0, Lcom/qualcomm/location/LBSHal;->mLBSHal:Lcom/qualcomm/location/LBSHal;

    #@d
    if-nez v0, :cond_16

    #@f
    .line 34
    new-instance v0, Lcom/qualcomm/location/LBSHal;

    #@11
    invoke-direct {v0, p0, p1}, Lcom/qualcomm/location/LBSHal;-><init>(Lcom/qualcomm/location/MonitorInterface;I)V

    #@14
    sput-object v0, Lcom/qualcomm/location/LBSHal;->mLBSHal:Lcom/qualcomm/location/LBSHal;

    #@16
    .line 36
    :cond_16
    sget-object v0, Lcom/qualcomm/location/LBSHal;->mLBSHal:Lcom/qualcomm/location/LBSHal;

    #@18
    return-object v0
.end method

.method public static native_lh_charger_status_inject(I)V
	.registers 1
	return-void
.end method

.method private static native_lh_class_init()V
	.registers 0
	return-void
.end method

.method private native_lh_init()V
	.registers 1
	return-void
.end method

.method private ssrestart()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 40
    const/4 v0, 0x0

    #@2
    invoke-virtual {p0, v1, v1, v1, v0}, Lcom/qualcomm/location/LBSHal;->sendMessage(IIILjava/lang/Object;)V

    #@5
    .line 41
    return-void
.end method


# virtual methods
.method public getNumOfMessages()I
    .registers 2

    #@0
    .prologue
    .line 52
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 6
    .parameter "msg"

    #@0
    .prologue
    .line 57
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    .line 58
    .local v0, message:I
    const-string v1, "LBSHal"

    #@4
    new-instance v2, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v3, "handleMessage what - "

    #@b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 60
    packed-switch v0, :pswitch_data_22

    #@1d
    .line 67
    :goto_1d
    return-void

    #@1e
    .line 62
    :pswitch_1e
    invoke-direct {p0}, Lcom/qualcomm/location/LBSHal;->native_lh_init()V

    #@21
    goto :goto_1d

    #@22
    .line 60
    :pswitch_data_22
    .packed-switch 0x0
        :pswitch_1e
    .end packed-switch
.end method

.method public native_lh_cinfo_inject(IIIIZ)V
	.registers 6
	return-void
.end method

.method public native_lh_ni_supl_init([BI)V
	.registers 3
	return-void
.end method

.method public native_lh_oos_inform()V
	.registers 1
	return-void
.end method
