.class public Lcom/lge/ims/SystemServiceManager;
.super Ljava/lang/Object;
.source "SystemServiceManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/SystemServiceManager$SystemServiceReceiver;,
        Lcom/lge/ims/SystemServiceManager$SystemServiceConnector;
    }
.end annotation


# static fields
.field private static final ACTION_IMS_PHONE_STARTED:Ljava/lang/String; = "com.lge.ims.action.IMS_PHONE_STARTED"

.field private static final IMS_PHONE:Ljava/lang/String; = "com.lge.ims.phone"

.field private static final TAG:Ljava/lang/String; = "SystemServiceManager"

.field private static final TELEPHONY_REGISTRY:Ljava/lang/String; = "telephony.registry"

.field private static mSSMngr:Lcom/lge/ims/SystemServiceManager;


# instance fields
.field private mAlarmManager:Landroid/app/AlarmManager;

.field private mConnectivityManager:Landroid/net/ConnectivityManager;

.field private mContext:Landroid/content/Context;

.field private mIMSPhone:Lcom/android/internal/telephony/IIMSPhone;

.field private mIntentFilter:Landroid/content/IntentFilter;

.field private mPowerManager:Landroid/os/PowerManager;

.field private mSSReceiver:Lcom/lge/ims/SystemServiceManager$SystemServiceReceiver;

.field private mSysMonitor:Lcom/android/internal/telephony/ISysMonitor;

.field private mSystemServiceRestartedRegistrants:Landroid/os/RegistrantList;

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;

.field private mTelephonyRegistry:Lcom/android/internal/telephony/ITelephonyRegistry;

.field private mTelephonyService:Lcom/android/internal/telephony/ITelephony;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 33
    new-instance v0, Lcom/lge/ims/SystemServiceManager;

    #@2
    invoke-direct {v0}, Lcom/lge/ims/SystemServiceManager;-><init>()V

    #@5
    sput-object v0, Lcom/lge/ims/SystemServiceManager;->mSSMngr:Lcom/lge/ims/SystemServiceManager;

    #@7
    return-void
.end method

.method private constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 50
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 35
    iput-object v1, p0, Lcom/lge/ims/SystemServiceManager;->mContext:Landroid/content/Context;

    #@6
    .line 36
    iput-object v1, p0, Lcom/lge/ims/SystemServiceManager;->mAlarmManager:Landroid/app/AlarmManager;

    #@8
    .line 37
    iput-object v1, p0, Lcom/lge/ims/SystemServiceManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    #@a
    .line 38
    iput-object v1, p0, Lcom/lge/ims/SystemServiceManager;->mPowerManager:Landroid/os/PowerManager;

    #@c
    .line 39
    iput-object v1, p0, Lcom/lge/ims/SystemServiceManager;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@e
    .line 40
    iput-object v1, p0, Lcom/lge/ims/SystemServiceManager;->mTelephonyService:Lcom/android/internal/telephony/ITelephony;

    #@10
    .line 41
    iput-object v1, p0, Lcom/lge/ims/SystemServiceManager;->mTelephonyRegistry:Lcom/android/internal/telephony/ITelephonyRegistry;

    #@12
    .line 42
    new-instance v0, Landroid/content/IntentFilter;

    #@14
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@17
    iput-object v0, p0, Lcom/lge/ims/SystemServiceManager;->mIntentFilter:Landroid/content/IntentFilter;

    #@19
    .line 43
    new-instance v0, Landroid/os/RegistrantList;

    #@1b
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@1e
    iput-object v0, p0, Lcom/lge/ims/SystemServiceManager;->mSystemServiceRestartedRegistrants:Landroid/os/RegistrantList;

    #@20
    .line 44
    iput-object v1, p0, Lcom/lge/ims/SystemServiceManager;->mIMSPhone:Lcom/android/internal/telephony/IIMSPhone;

    #@22
    .line 45
    iput-object v1, p0, Lcom/lge/ims/SystemServiceManager;->mSysMonitor:Lcom/android/internal/telephony/ISysMonitor;

    #@24
    .line 46
    new-instance v0, Lcom/lge/ims/SystemServiceManager$SystemServiceReceiver;

    #@26
    invoke-direct {v0, p0}, Lcom/lge/ims/SystemServiceManager$SystemServiceReceiver;-><init>(Lcom/lge/ims/SystemServiceManager;)V

    #@29
    iput-object v0, p0, Lcom/lge/ims/SystemServiceManager;->mSSReceiver:Lcom/lge/ims/SystemServiceManager$SystemServiceReceiver;

    #@2b
    .line 52
    iget-object v0, p0, Lcom/lge/ims/SystemServiceManager;->mIntentFilter:Landroid/content/IntentFilter;

    #@2d
    if-eqz v0, :cond_36

    #@2f
    .line 53
    iget-object v0, p0, Lcom/lge/ims/SystemServiceManager;->mIntentFilter:Landroid/content/IntentFilter;

    #@31
    const-string v1, "com.lge.ims.action.IMS_PHONE_STARTED"

    #@33
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@36
    .line 56
    :cond_36
    const-string v0, "SystemServiceManager"

    #@38
    const-string v1, "SystemServiceManager is created"

    #@3a
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@3d
    .line 57
    return-void
.end method

.method static synthetic access$000(Lcom/lge/ims/SystemServiceManager;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 26
    invoke-direct {p0}, Lcom/lge/ims/SystemServiceManager;->getSystemServices()V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/lge/ims/SystemServiceManager;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 26
    invoke-direct {p0}, Lcom/lge/ims/SystemServiceManager;->bindTelephonyRegistry()V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/lge/ims/SystemServiceManager;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 26
    invoke-direct {p0}, Lcom/lge/ims/SystemServiceManager;->bindTelephonyService()V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/lge/ims/SystemServiceManager;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 26
    invoke-direct {p0}, Lcom/lge/ims/SystemServiceManager;->bindIMSPhone()V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/lge/ims/SystemServiceManager;)Landroid/os/RegistrantList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 26
    iget-object v0, p0, Lcom/lge/ims/SystemServiceManager;->mSystemServiceRestartedRegistrants:Landroid/os/RegistrantList;

    #@2
    return-object v0
.end method

.method private bindIMSPhone()V
    .registers 6

    #@0
    .prologue
    .line 276
    const-string v2, "com.lge.ims.phone"

    #@2
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@5
    move-result-object v2

    #@6
    invoke-static {v2}, Lcom/android/internal/telephony/IIMSPhone$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/IIMSPhone;

    #@9
    move-result-object v2

    #@a
    iput-object v2, p0, Lcom/lge/ims/SystemServiceManager;->mIMSPhone:Lcom/android/internal/telephony/IIMSPhone;

    #@c
    .line 278
    iget-object v2, p0, Lcom/lge/ims/SystemServiceManager;->mIMSPhone:Lcom/android/internal/telephony/IIMSPhone;

    #@e
    if-nez v2, :cond_11

    #@10
    .line 293
    :cond_10
    :goto_10
    return-void

    #@11
    .line 283
    :cond_11
    :try_start_11
    iget-object v2, p0, Lcom/lge/ims/SystemServiceManager;->mIMSPhone:Lcom/android/internal/telephony/IIMSPhone;

    #@13
    iget-object v3, p0, Lcom/lge/ims/SystemServiceManager;->mSysMonitor:Lcom/android/internal/telephony/ISysMonitor;

    #@15
    invoke-interface {v2, v3}, Lcom/android/internal/telephony/IIMSPhone;->setListener(Lcom/android/internal/telephony/ISysMonitor;)V
    :try_end_18
    .catch Landroid/os/RemoteException; {:try_start_11 .. :try_end_18} :catch_19

    #@18
    goto :goto_10

    #@19
    .line 284
    :catch_19
    move-exception v1

    #@1a
    .line 285
    .local v1, e:Landroid/os/RemoteException;
    const-string v2, "SystemServiceManager"

    #@1c
    new-instance v3, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v4, "setListener :: "

    #@23
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v3

    #@2f
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@32
    .line 287
    invoke-virtual {v1}, Landroid/os/RemoteException;->getCause()Ljava/lang/Throwable;

    #@35
    move-result-object v0

    #@36
    .line 289
    .local v0, cause:Ljava/lang/Throwable;
    instance-of v2, v0, Landroid/os/DeadObjectException;

    #@38
    if-eqz v2, :cond_10

    #@3a
    .line 290
    const/4 v2, 0x0

    #@3b
    iput-object v2, p0, Lcom/lge/ims/SystemServiceManager;->mIMSPhone:Lcom/android/internal/telephony/IIMSPhone;

    #@3d
    goto :goto_10
.end method

.method private bindTelephonyRegistry()V
    .registers 2

    #@0
    .prologue
    .line 296
    const-string v0, "telephony.registry"

    #@2
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@5
    move-result-object v0

    #@6
    invoke-static {v0}, Lcom/android/internal/telephony/ITelephonyRegistry$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephonyRegistry;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Lcom/lge/ims/SystemServiceManager;->mTelephonyRegistry:Lcom/android/internal/telephony/ITelephonyRegistry;

    #@c
    .line 297
    return-void
.end method

.method private bindTelephonyService()V
    .registers 2

    #@0
    .prologue
    .line 300
    const-string v0, "phone"

    #@2
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@5
    move-result-object v0

    #@6
    invoke-static {v0}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Lcom/lge/ims/SystemServiceManager;->mTelephonyService:Lcom/android/internal/telephony/ITelephony;

    #@c
    .line 301
    return-void
.end method

.method public static getAlarmManager()Landroid/app/AlarmManager;
    .registers 3

    #@0
    .prologue
    .line 64
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getInstance()Lcom/lge/ims/SystemServiceManager;

    #@3
    move-result-object v0

    #@4
    .line 66
    .local v0, ssm:Lcom/lge/ims/SystemServiceManager;
    if-eqz v0, :cond_1d

    #@6
    .line 67
    iget-object v1, v0, Lcom/lge/ims/SystemServiceManager;->mContext:Landroid/content/Context;

    #@8
    if-eqz v1, :cond_1a

    #@a
    iget-object v1, v0, Lcom/lge/ims/SystemServiceManager;->mAlarmManager:Landroid/app/AlarmManager;

    #@c
    if-nez v1, :cond_1a

    #@e
    .line 68
    iget-object v1, v0, Lcom/lge/ims/SystemServiceManager;->mContext:Landroid/content/Context;

    #@10
    const-string v2, "alarm"

    #@12
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@15
    move-result-object v1

    #@16
    check-cast v1, Landroid/app/AlarmManager;

    #@18
    iput-object v1, v0, Lcom/lge/ims/SystemServiceManager;->mAlarmManager:Landroid/app/AlarmManager;

    #@1a
    .line 71
    :cond_1a
    iget-object v1, v0, Lcom/lge/ims/SystemServiceManager;->mAlarmManager:Landroid/app/AlarmManager;

    #@1c
    .line 74
    :goto_1c
    return-object v1

    #@1d
    :cond_1d
    const/4 v1, 0x0

    #@1e
    goto :goto_1c
.end method

.method public static getConnectivityManager()Landroid/net/ConnectivityManager;
    .registers 3

    #@0
    .prologue
    .line 78
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getInstance()Lcom/lge/ims/SystemServiceManager;

    #@3
    move-result-object v0

    #@4
    .line 80
    .local v0, ssm:Lcom/lge/ims/SystemServiceManager;
    if-eqz v0, :cond_1d

    #@6
    .line 81
    iget-object v1, v0, Lcom/lge/ims/SystemServiceManager;->mContext:Landroid/content/Context;

    #@8
    if-eqz v1, :cond_1a

    #@a
    iget-object v1, v0, Lcom/lge/ims/SystemServiceManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    #@c
    if-nez v1, :cond_1a

    #@e
    .line 82
    iget-object v1, v0, Lcom/lge/ims/SystemServiceManager;->mContext:Landroid/content/Context;

    #@10
    const-string v2, "connectivity"

    #@12
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@15
    move-result-object v1

    #@16
    check-cast v1, Landroid/net/ConnectivityManager;

    #@18
    iput-object v1, v0, Lcom/lge/ims/SystemServiceManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    #@1a
    .line 85
    :cond_1a
    iget-object v1, v0, Lcom/lge/ims/SystemServiceManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    #@1c
    .line 88
    :goto_1c
    return-object v1

    #@1d
    :cond_1d
    const/4 v1, 0x0

    #@1e
    goto :goto_1c
.end method

.method public static getIMSPhone()Lcom/android/internal/telephony/IIMSPhone;
    .registers 2

    #@0
    .prologue
    .line 148
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getInstance()Lcom/lge/ims/SystemServiceManager;

    #@3
    move-result-object v0

    #@4
    .line 150
    .local v0, ssm:Lcom/lge/ims/SystemServiceManager;
    if-eqz v0, :cond_19

    #@6
    .line 151
    iget-object v1, v0, Lcom/lge/ims/SystemServiceManager;->mIMSPhone:Lcom/android/internal/telephony/IIMSPhone;

    #@8
    if-nez v1, :cond_16

    #@a
    .line 152
    const-string v1, "com.lge.ims.phone"

    #@c
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@f
    move-result-object v1

    #@10
    invoke-static {v1}, Lcom/android/internal/telephony/IIMSPhone$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/IIMSPhone;

    #@13
    move-result-object v1

    #@14
    iput-object v1, v0, Lcom/lge/ims/SystemServiceManager;->mIMSPhone:Lcom/android/internal/telephony/IIMSPhone;

    #@16
    .line 155
    :cond_16
    iget-object v1, v0, Lcom/lge/ims/SystemServiceManager;->mIMSPhone:Lcom/android/internal/telephony/IIMSPhone;

    #@18
    .line 158
    :goto_18
    return-object v1

    #@19
    :cond_19
    const/4 v1, 0x0

    #@1a
    goto :goto_18
.end method

.method public static getInstance()Lcom/lge/ims/SystemServiceManager;
    .registers 1

    #@0
    .prologue
    .line 60
    sget-object v0, Lcom/lge/ims/SystemServiceManager;->mSSMngr:Lcom/lge/ims/SystemServiceManager;

    #@2
    return-object v0
.end method

.method public static getPowerManager()Landroid/os/PowerManager;
    .registers 3

    #@0
    .prologue
    .line 92
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getInstance()Lcom/lge/ims/SystemServiceManager;

    #@3
    move-result-object v0

    #@4
    .line 94
    .local v0, ssm:Lcom/lge/ims/SystemServiceManager;
    if-eqz v0, :cond_1d

    #@6
    .line 95
    iget-object v1, v0, Lcom/lge/ims/SystemServiceManager;->mContext:Landroid/content/Context;

    #@8
    if-eqz v1, :cond_1a

    #@a
    iget-object v1, v0, Lcom/lge/ims/SystemServiceManager;->mPowerManager:Landroid/os/PowerManager;

    #@c
    if-nez v1, :cond_1a

    #@e
    .line 96
    iget-object v1, v0, Lcom/lge/ims/SystemServiceManager;->mContext:Landroid/content/Context;

    #@10
    const-string v2, "power"

    #@12
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@15
    move-result-object v1

    #@16
    check-cast v1, Landroid/os/PowerManager;

    #@18
    iput-object v1, v0, Lcom/lge/ims/SystemServiceManager;->mPowerManager:Landroid/os/PowerManager;

    #@1a
    .line 99
    :cond_1a
    iget-object v1, v0, Lcom/lge/ims/SystemServiceManager;->mPowerManager:Landroid/os/PowerManager;

    #@1c
    .line 102
    :goto_1c
    return-object v1

    #@1d
    :cond_1d
    const/4 v1, 0x0

    #@1e
    goto :goto_1c
.end method

.method private getSystemServices()V
    .registers 3

    #@0
    .prologue
    .line 305
    iget-object v0, p0, Lcom/lge/ims/SystemServiceManager;->mContext:Landroid/content/Context;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 306
    const-string v0, "SystemServiceManager"

    #@6
    const-string v1, "Context is null"

    #@8
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@b
    .line 337
    :cond_b
    :goto_b
    return-void

    #@c
    .line 311
    :cond_c
    iget-object v0, p0, Lcom/lge/ims/SystemServiceManager;->mContext:Landroid/content/Context;

    #@e
    const-string v1, "alarm"

    #@10
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@13
    move-result-object v0

    #@14
    check-cast v0, Landroid/app/AlarmManager;

    #@16
    iput-object v0, p0, Lcom/lge/ims/SystemServiceManager;->mAlarmManager:Landroid/app/AlarmManager;

    #@18
    .line 313
    iget-object v0, p0, Lcom/lge/ims/SystemServiceManager;->mAlarmManager:Landroid/app/AlarmManager;

    #@1a
    if-nez v0, :cond_23

    #@1c
    .line 314
    const-string v0, "SystemServiceManager"

    #@1e
    const-string v1, "AlarmManager is null"

    #@20
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@23
    .line 318
    :cond_23
    iget-object v0, p0, Lcom/lge/ims/SystemServiceManager;->mContext:Landroid/content/Context;

    #@25
    const-string v1, "connectivity"

    #@27
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@2a
    move-result-object v0

    #@2b
    check-cast v0, Landroid/net/ConnectivityManager;

    #@2d
    iput-object v0, p0, Lcom/lge/ims/SystemServiceManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    #@2f
    .line 320
    iget-object v0, p0, Lcom/lge/ims/SystemServiceManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    #@31
    if-nez v0, :cond_3a

    #@33
    .line 321
    const-string v0, "SystemServiceManager"

    #@35
    const-string v1, "ConnectivityManager is null"

    #@37
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@3a
    .line 325
    :cond_3a
    iget-object v0, p0, Lcom/lge/ims/SystemServiceManager;->mContext:Landroid/content/Context;

    #@3c
    const-string v1, "power"

    #@3e
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@41
    move-result-object v0

    #@42
    check-cast v0, Landroid/os/PowerManager;

    #@44
    iput-object v0, p0, Lcom/lge/ims/SystemServiceManager;->mPowerManager:Landroid/os/PowerManager;

    #@46
    .line 327
    iget-object v0, p0, Lcom/lge/ims/SystemServiceManager;->mPowerManager:Landroid/os/PowerManager;

    #@48
    if-nez v0, :cond_51

    #@4a
    .line 328
    const-string v0, "SystemServiceManager"

    #@4c
    const-string v1, "PowerManager is null"

    #@4e
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@51
    .line 332
    :cond_51
    iget-object v0, p0, Lcom/lge/ims/SystemServiceManager;->mContext:Landroid/content/Context;

    #@53
    const-string v1, "phone"

    #@55
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@58
    move-result-object v0

    #@59
    check-cast v0, Landroid/telephony/TelephonyManager;

    #@5b
    iput-object v0, p0, Lcom/lge/ims/SystemServiceManager;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@5d
    .line 334
    iget-object v0, p0, Lcom/lge/ims/SystemServiceManager;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@5f
    if-nez v0, :cond_b

    #@61
    .line 335
    const-string v0, "SystemServiceManager"

    #@63
    const-string v1, "TelephonyManager is null"

    #@65
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@68
    goto :goto_b
.end method

.method public static getTelephonyManager()Landroid/telephony/TelephonyManager;
    .registers 3

    #@0
    .prologue
    .line 106
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getInstance()Lcom/lge/ims/SystemServiceManager;

    #@3
    move-result-object v0

    #@4
    .line 108
    .local v0, ssm:Lcom/lge/ims/SystemServiceManager;
    if-eqz v0, :cond_1d

    #@6
    .line 109
    iget-object v1, v0, Lcom/lge/ims/SystemServiceManager;->mContext:Landroid/content/Context;

    #@8
    if-eqz v1, :cond_1a

    #@a
    iget-object v1, v0, Lcom/lge/ims/SystemServiceManager;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@c
    if-nez v1, :cond_1a

    #@e
    .line 110
    iget-object v1, v0, Lcom/lge/ims/SystemServiceManager;->mContext:Landroid/content/Context;

    #@10
    const-string v2, "phone"

    #@12
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@15
    move-result-object v1

    #@16
    check-cast v1, Landroid/telephony/TelephonyManager;

    #@18
    iput-object v1, v0, Lcom/lge/ims/SystemServiceManager;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@1a
    .line 113
    :cond_1a
    iget-object v1, v0, Lcom/lge/ims/SystemServiceManager;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@1c
    .line 116
    :goto_1c
    return-object v1

    #@1d
    :cond_1d
    const/4 v1, 0x0

    #@1e
    goto :goto_1c
.end method

.method public static getTelephonyRegistry()Lcom/android/internal/telephony/ITelephonyRegistry;
    .registers 2

    #@0
    .prologue
    .line 120
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getInstance()Lcom/lge/ims/SystemServiceManager;

    #@3
    move-result-object v0

    #@4
    .line 122
    .local v0, ssm:Lcom/lge/ims/SystemServiceManager;
    if-eqz v0, :cond_19

    #@6
    .line 123
    iget-object v1, v0, Lcom/lge/ims/SystemServiceManager;->mTelephonyRegistry:Lcom/android/internal/telephony/ITelephonyRegistry;

    #@8
    if-nez v1, :cond_16

    #@a
    .line 124
    const-string v1, "telephony.registry"

    #@c
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@f
    move-result-object v1

    #@10
    invoke-static {v1}, Lcom/android/internal/telephony/ITelephonyRegistry$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephonyRegistry;

    #@13
    move-result-object v1

    #@14
    iput-object v1, v0, Lcom/lge/ims/SystemServiceManager;->mTelephonyRegistry:Lcom/android/internal/telephony/ITelephonyRegistry;

    #@16
    .line 127
    :cond_16
    iget-object v1, v0, Lcom/lge/ims/SystemServiceManager;->mTelephonyRegistry:Lcom/android/internal/telephony/ITelephonyRegistry;

    #@18
    .line 130
    :goto_18
    return-object v1

    #@19
    :cond_19
    const/4 v1, 0x0

    #@1a
    goto :goto_18
.end method

.method public static getTelephonyService()Lcom/android/internal/telephony/ITelephony;
    .registers 2

    #@0
    .prologue
    .line 134
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getInstance()Lcom/lge/ims/SystemServiceManager;

    #@3
    move-result-object v0

    #@4
    .line 136
    .local v0, ssm:Lcom/lge/ims/SystemServiceManager;
    if-eqz v0, :cond_19

    #@6
    .line 137
    iget-object v1, v0, Lcom/lge/ims/SystemServiceManager;->mTelephonyService:Lcom/android/internal/telephony/ITelephony;

    #@8
    if-nez v1, :cond_16

    #@a
    .line 138
    const-string v1, "phone"

    #@c
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@f
    move-result-object v1

    #@10
    invoke-static {v1}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    #@13
    move-result-object v1

    #@14
    iput-object v1, v0, Lcom/lge/ims/SystemServiceManager;->mTelephonyService:Lcom/android/internal/telephony/ITelephony;

    #@16
    .line 141
    :cond_16
    iget-object v1, v0, Lcom/lge/ims/SystemServiceManager;->mTelephonyService:Lcom/android/internal/telephony/ITelephony;

    #@18
    .line 144
    :goto_18
    return-object v1

    #@19
    :cond_19
    const/4 v1, 0x0

    #@1a
    goto :goto_18
.end method

.method public static rebindIMSPhone()V
    .registers 1

    #@0
    .prologue
    .line 162
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getInstance()Lcom/lge/ims/SystemServiceManager;

    #@3
    move-result-object v0

    #@4
    .line 164
    .local v0, ssm:Lcom/lge/ims/SystemServiceManager;
    if-eqz v0, :cond_9

    #@6
    .line 165
    invoke-direct {v0}, Lcom/lge/ims/SystemServiceManager;->bindIMSPhone()V

    #@9
    .line 167
    :cond_9
    return-void
.end method

.method public static rebindTelephonyRegistry()V
    .registers 1

    #@0
    .prologue
    .line 170
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getInstance()Lcom/lge/ims/SystemServiceManager;

    #@3
    move-result-object v0

    #@4
    .line 172
    .local v0, ssm:Lcom/lge/ims/SystemServiceManager;
    if-eqz v0, :cond_9

    #@6
    .line 173
    invoke-direct {v0}, Lcom/lge/ims/SystemServiceManager;->bindTelephonyRegistry()V

    #@9
    .line 175
    :cond_9
    return-void
.end method

.method public static rebindTelephonyService()V
    .registers 1

    #@0
    .prologue
    .line 178
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getInstance()Lcom/lge/ims/SystemServiceManager;

    #@3
    move-result-object v0

    #@4
    .line 180
    .local v0, ssm:Lcom/lge/ims/SystemServiceManager;
    if-eqz v0, :cond_9

    #@6
    .line 181
    invoke-direct {v0}, Lcom/lge/ims/SystemServiceManager;->bindTelephonyService()V

    #@9
    .line 183
    :cond_9
    return-void
.end method


# virtual methods
.method public destroy()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 186
    iget-object v2, p0, Lcom/lge/ims/SystemServiceManager;->mContext:Landroid/content/Context;

    #@3
    if-eqz v2, :cond_c

    #@5
    .line 187
    iget-object v2, p0, Lcom/lge/ims/SystemServiceManager;->mContext:Landroid/content/Context;

    #@7
    iget-object v3, p0, Lcom/lge/ims/SystemServiceManager;->mSSReceiver:Lcom/lge/ims/SystemServiceManager$SystemServiceReceiver;

    #@9
    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@c
    .line 190
    :cond_c
    iget-object v2, p0, Lcom/lge/ims/SystemServiceManager;->mIMSPhone:Lcom/android/internal/telephony/IIMSPhone;

    #@e
    if-eqz v2, :cond_16

    #@10
    .line 192
    :try_start_10
    iget-object v2, p0, Lcom/lge/ims/SystemServiceManager;->mIMSPhone:Lcom/android/internal/telephony/IIMSPhone;

    #@12
    const/4 v3, 0x0

    #@13
    invoke-interface {v2, v3}, Lcom/android/internal/telephony/IIMSPhone;->setListener(Lcom/android/internal/telephony/ISysMonitor;)V
    :try_end_16
    .catch Landroid/os/RemoteException; {:try_start_10 .. :try_end_16} :catch_17

    #@16
    .line 203
    :cond_16
    :goto_16
    return-void

    #@17
    .line 193
    :catch_17
    move-exception v1

    #@18
    .line 194
    .local v1, e:Landroid/os/RemoteException;
    const-string v2, "SystemServiceManager"

    #@1a
    new-instance v3, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v4, "destroy() :: "

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v3

    #@2d
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@30
    .line 196
    invoke-virtual {v1}, Landroid/os/RemoteException;->getCause()Ljava/lang/Throwable;

    #@33
    move-result-object v0

    #@34
    .line 198
    .local v0, cause:Ljava/lang/Throwable;
    instance-of v2, v0, Landroid/os/DeadObjectException;

    #@36
    if-eqz v2, :cond_16

    #@38
    .line 199
    iput-object v5, p0, Lcom/lge/ims/SystemServiceManager;->mIMSPhone:Lcom/android/internal/telephony/IIMSPhone;

    #@3a
    goto :goto_16
.end method

.method public getSysInfo(IILjava/lang/String;)[Ljava/lang/String;
    .registers 11
    .parameter "item"
    .parameter "param"
    .parameter "paramEx"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 206
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getIMSPhone()Lcom/android/internal/telephony/IIMSPhone;

    #@4
    .line 208
    iget-object v4, p0, Lcom/lge/ims/SystemServiceManager;->mIMSPhone:Lcom/android/internal/telephony/IIMSPhone;

    #@6
    if-nez v4, :cond_11

    #@8
    .line 209
    const-string v4, "SystemServiceManager"

    #@a
    const-string v5, "IMSPhone is null"

    #@c
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@f
    move-object v2, v3

    #@10
    .line 227
    :cond_10
    :goto_10
    return-object v2

    #@11
    .line 213
    :cond_11
    const/4 v2, 0x0

    #@12
    .line 216
    .local v2, sysInfo:[Ljava/lang/String;
    :try_start_12
    iget-object v4, p0, Lcom/lge/ims/SystemServiceManager;->mIMSPhone:Lcom/android/internal/telephony/IIMSPhone;

    #@14
    invoke-interface {v4, p1, p2, p3}, Lcom/android/internal/telephony/IIMSPhone;->getSysInfo(IILjava/lang/String;)[Ljava/lang/String;
    :try_end_17
    .catch Landroid/os/RemoteException; {:try_start_12 .. :try_end_17} :catch_19

    #@17
    move-result-object v2

    #@18
    goto :goto_10

    #@19
    .line 217
    :catch_19
    move-exception v1

    #@1a
    .line 218
    .local v1, e:Landroid/os/RemoteException;
    const-string v4, "SystemServiceManager"

    #@1c
    new-instance v5, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v6, "getSysInfo() :: "

    #@23
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v5

    #@27
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v5

    #@2b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v5

    #@2f
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@32
    .line 220
    invoke-virtual {v1}, Landroid/os/RemoteException;->getCause()Ljava/lang/Throwable;

    #@35
    move-result-object v0

    #@36
    .line 222
    .local v0, cause:Ljava/lang/Throwable;
    instance-of v4, v0, Landroid/os/DeadObjectException;

    #@38
    if-eqz v4, :cond_10

    #@3a
    .line 223
    iput-object v3, p0, Lcom/lge/ims/SystemServiceManager;->mIMSPhone:Lcom/android/internal/telephony/IIMSPhone;

    #@3c
    goto :goto_10
.end method

.method public registerForSystemServiceRestarted(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 252
    iget-object v0, p0, Lcom/lge/ims/SystemServiceManager;->mSystemServiceRestartedRegistrants:Landroid/os/RegistrantList;

    #@2
    new-instance v1, Landroid/os/Registrant;

    #@4
    invoke-direct {v1, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@7
    invoke-virtual {v0, v1}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 253
    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 260
    iget-object v0, p0, Lcom/lge/ims/SystemServiceManager;->mContext:Landroid/content/Context;

    #@2
    if-eq v0, p1, :cond_16

    #@4
    .line 261
    iput-object p1, p0, Lcom/lge/ims/SystemServiceManager;->mContext:Landroid/content/Context;

    #@6
    .line 263
    invoke-direct {p0}, Lcom/lge/ims/SystemServiceManager;->getSystemServices()V

    #@9
    .line 265
    iget-object v0, p0, Lcom/lge/ims/SystemServiceManager;->mContext:Landroid/content/Context;

    #@b
    if-eqz v0, :cond_16

    #@d
    .line 266
    iget-object v0, p0, Lcom/lge/ims/SystemServiceManager;->mContext:Landroid/content/Context;

    #@f
    iget-object v1, p0, Lcom/lge/ims/SystemServiceManager;->mSSReceiver:Lcom/lge/ims/SystemServiceManager$SystemServiceReceiver;

    #@11
    iget-object v2, p0, Lcom/lge/ims/SystemServiceManager;->mIntentFilter:Landroid/content/IntentFilter;

    #@13
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@16
    .line 269
    :cond_16
    return-void
.end method

.method public setSysInfo(IIILjava/lang/String;)V
    .registers 10
    .parameter "item"
    .parameter "param1"
    .parameter "param2"
    .parameter "paramEx"

    #@0
    .prologue
    .line 231
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getIMSPhone()Lcom/android/internal/telephony/IIMSPhone;

    #@3
    .line 233
    iget-object v2, p0, Lcom/lge/ims/SystemServiceManager;->mIMSPhone:Lcom/android/internal/telephony/IIMSPhone;

    #@5
    if-nez v2, :cond_f

    #@7
    .line 234
    const-string v2, "SystemServiceManager"

    #@9
    const-string v3, "IMSPhone is null"

    #@b
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@e
    .line 249
    :cond_e
    :goto_e
    return-void

    #@f
    .line 239
    :cond_f
    :try_start_f
    iget-object v2, p0, Lcom/lge/ims/SystemServiceManager;->mIMSPhone:Lcom/android/internal/telephony/IIMSPhone;

    #@11
    invoke-interface {v2, p1, p2, p3, p4}, Lcom/android/internal/telephony/IIMSPhone;->setSysInfo(IIILjava/lang/String;)V
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_14} :catch_15

    #@14
    goto :goto_e

    #@15
    .line 240
    :catch_15
    move-exception v1

    #@16
    .line 241
    .local v1, e:Landroid/os/RemoteException;
    const-string v2, "SystemServiceManager"

    #@18
    new-instance v3, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v4, "setSysInfo() :: "

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v3

    #@2b
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@2e
    .line 243
    invoke-virtual {v1}, Landroid/os/RemoteException;->getCause()Ljava/lang/Throwable;

    #@31
    move-result-object v0

    #@32
    .line 245
    .local v0, cause:Ljava/lang/Throwable;
    instance-of v2, v0, Landroid/os/DeadObjectException;

    #@34
    if-eqz v2, :cond_e

    #@36
    .line 246
    const/4 v2, 0x0

    #@37
    iput-object v2, p0, Lcom/lge/ims/SystemServiceManager;->mIMSPhone:Lcom/android/internal/telephony/IIMSPhone;

    #@39
    goto :goto_e
.end method

.method public setSysMonitor(Lcom/android/internal/telephony/ISysMonitor;)V
    .registers 2
    .parameter "sysMonitor"

    #@0
    .prologue
    .line 272
    iput-object p1, p0, Lcom/lge/ims/SystemServiceManager;->mSysMonitor:Lcom/android/internal/telephony/ISysMonitor;

    #@2
    .line 273
    return-void
.end method

.method public unregisterForSystemServiceRestarted(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 256
    iget-object v0, p0, Lcom/lge/ims/SystemServiceManager;->mSystemServiceRestartedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 257
    return-void
.end method
