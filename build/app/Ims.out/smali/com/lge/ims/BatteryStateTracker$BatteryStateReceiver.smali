.class final Lcom/lge/ims/BatteryStateTracker$BatteryStateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BatteryStateTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/BatteryStateTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "BatteryStateReceiver"
.end annotation


# instance fields
.field mIntentFilter:Landroid/content/IntentFilter;

.field final synthetic this$0:Lcom/lge/ims/BatteryStateTracker;


# direct methods
.method public constructor <init>(Lcom/lge/ims/BatteryStateTracker;)V
    .registers 4
    .parameter

    #@0
    .prologue
    .line 358
    iput-object p1, p0, Lcom/lge/ims/BatteryStateTracker$BatteryStateReceiver;->this$0:Lcom/lge/ims/BatteryStateTracker;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    .line 356
    new-instance v0, Landroid/content/IntentFilter;

    #@7
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@a
    iput-object v0, p0, Lcom/lge/ims/BatteryStateTracker$BatteryStateReceiver;->mIntentFilter:Landroid/content/IntentFilter;

    #@c
    .line 359
    iget-object v0, p0, Lcom/lge/ims/BatteryStateTracker$BatteryStateReceiver;->mIntentFilter:Landroid/content/IntentFilter;

    #@e
    if-eqz v0, :cond_2c

    #@10
    .line 360
    iget-object v0, p0, Lcom/lge/ims/BatteryStateTracker$BatteryStateReceiver;->mIntentFilter:Landroid/content/IntentFilter;

    #@12
    const-string v1, "android.intent.action.BATTERY_LOW"

    #@14
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@17
    .line 361
    iget-object v0, p0, Lcom/lge/ims/BatteryStateTracker$BatteryStateReceiver;->mIntentFilter:Landroid/content/IntentFilter;

    #@19
    const-string v1, "android.intent.action.ACTION_POWER_CONNECTED"

    #@1b
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1e
    .line 362
    iget-object v0, p0, Lcom/lge/ims/BatteryStateTracker$BatteryStateReceiver;->mIntentFilter:Landroid/content/IntentFilter;

    #@20
    const-string v1, "android.intent.action.ACTION_POWER_DISCONNECTED"

    #@22
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@25
    .line 363
    iget-object v0, p0, Lcom/lge/ims/BatteryStateTracker$BatteryStateReceiver;->mIntentFilter:Landroid/content/IntentFilter;

    #@27
    const-string v1, "com.lge.ims.action.BATTERY_POLLING_TIMER"

    #@29
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@2c
    .line 365
    :cond_2c
    return-void
.end method


# virtual methods
.method public getFilter()Landroid/content/IntentFilter;
    .registers 2

    #@0
    .prologue
    .line 368
    iget-object v0, p0, Lcom/lge/ims/BatteryStateTracker$BatteryStateReceiver;->mIntentFilter:Landroid/content/IntentFilter;

    #@2
    return-object v0
.end method

.method public declared-synchronized onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 8
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 373
    monitor-enter p0

    #@1
    :try_start_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    .line 375
    .local v0, action:Ljava/lang/String;
    const-string v2, "BatteryStateTracker"

    #@7
    new-instance v3, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v4, "BatteryStateReceiver - "

    #@e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v3

    #@12
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v3

    #@1a
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@1d
    .line 377
    const-string v2, "android.intent.action.BATTERY_LOW"

    #@1f
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22
    move-result v2

    #@23
    if-eqz v2, :cond_2c

    #@25
    .line 378
    iget-object v2, p0, Lcom/lge/ims/BatteryStateTracker$BatteryStateReceiver;->this$0:Lcom/lge/ims/BatteryStateTracker;

    #@27
    invoke-static {v2}, Lcom/lge/ims/BatteryStateTracker;->access$700(Lcom/lge/ims/BatteryStateTracker;)V
    :try_end_2a
    .catchall {:try_start_1 .. :try_end_2a} :catchall_46

    #@2a
    .line 406
    :cond_2a
    :goto_2a
    monitor-exit p0

    #@2b
    return-void

    #@2c
    .line 379
    :cond_2c
    :try_start_2c
    const-string v2, "android.intent.action.ACTION_POWER_CONNECTED"

    #@2e
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@31
    move-result v2

    #@32
    if-eqz v2, :cond_4f

    #@34
    .line 380
    iget-object v2, p0, Lcom/lge/ims/BatteryStateTracker$BatteryStateReceiver;->this$0:Lcom/lge/ims/BatteryStateTracker;

    #@36
    invoke-virtual {v2}, Lcom/lge/ims/BatteryStateTracker;->getBatteryLevel()I

    #@39
    move-result v2

    #@3a
    invoke-static {v2}, Lcom/lge/ims/BatteryStateTracker;->access$400(I)I

    #@3d
    move-result v1

    #@3e
    .line 382
    .local v1, interval:I
    if-nez v1, :cond_49

    #@40
    .line 383
    iget-object v2, p0, Lcom/lge/ims/BatteryStateTracker$BatteryStateReceiver;->this$0:Lcom/lge/ims/BatteryStateTracker;

    #@42
    invoke-static {v2}, Lcom/lge/ims/BatteryStateTracker;->access$700(Lcom/lge/ims/BatteryStateTracker;)V
    :try_end_45
    .catchall {:try_start_2c .. :try_end_45} :catchall_46

    #@45
    goto :goto_2a

    #@46
    .line 373
    .end local v0           #action:Ljava/lang/String;
    .end local v1           #interval:I
    :catchall_46
    move-exception v2

    #@47
    monitor-exit p0

    #@48
    throw v2

    #@49
    .line 385
    .restart local v0       #action:Ljava/lang/String;
    .restart local v1       #interval:I
    :cond_49
    :try_start_49
    iget-object v2, p0, Lcom/lge/ims/BatteryStateTracker$BatteryStateReceiver;->this$0:Lcom/lge/ims/BatteryStateTracker;

    #@4b
    invoke-static {v2, v1}, Lcom/lge/ims/BatteryStateTracker;->access$600(Lcom/lge/ims/BatteryStateTracker;I)V

    #@4e
    goto :goto_2a

    #@4f
    .line 387
    .end local v1           #interval:I
    :cond_4f
    const-string v2, "android.intent.action.ACTION_POWER_DISCONNECTED"

    #@51
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@54
    move-result v2

    #@55
    if-eqz v2, :cond_72

    #@57
    .line 388
    iget-object v2, p0, Lcom/lge/ims/BatteryStateTracker$BatteryStateReceiver;->this$0:Lcom/lge/ims/BatteryStateTracker;

    #@59
    invoke-static {v2}, Lcom/lge/ims/BatteryStateTracker;->access$800(Lcom/lge/ims/BatteryStateTracker;)V

    #@5c
    .line 390
    iget-object v2, p0, Lcom/lge/ims/BatteryStateTracker$BatteryStateReceiver;->this$0:Lcom/lge/ims/BatteryStateTracker;

    #@5e
    invoke-virtual {v2}, Lcom/lge/ims/BatteryStateTracker;->getBatteryLevel()I

    #@61
    move-result v2

    #@62
    const/16 v3, 0x10

    #@64
    if-gt v2, v3, :cond_6c

    #@66
    .line 391
    iget-object v2, p0, Lcom/lge/ims/BatteryStateTracker$BatteryStateReceiver;->this$0:Lcom/lge/ims/BatteryStateTracker;

    #@68
    invoke-static {v2}, Lcom/lge/ims/BatteryStateTracker;->access$700(Lcom/lge/ims/BatteryStateTracker;)V

    #@6b
    goto :goto_2a

    #@6c
    .line 393
    :cond_6c
    iget-object v2, p0, Lcom/lge/ims/BatteryStateTracker$BatteryStateReceiver;->this$0:Lcom/lge/ims/BatteryStateTracker;

    #@6e
    invoke-static {v2}, Lcom/lge/ims/BatteryStateTracker;->access$500(Lcom/lge/ims/BatteryStateTracker;)V

    #@71
    goto :goto_2a

    #@72
    .line 395
    :cond_72
    const-string v2, "com.lge.ims.action.BATTERY_POLLING_TIMER"

    #@74
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@77
    move-result v2

    #@78
    if-eqz v2, :cond_2a

    #@7a
    .line 396
    iget-object v2, p0, Lcom/lge/ims/BatteryStateTracker$BatteryStateReceiver;->this$0:Lcom/lge/ims/BatteryStateTracker;

    #@7c
    const/4 v3, 0x0

    #@7d
    invoke-static {v2, v3}, Lcom/lge/ims/BatteryStateTracker;->access$902(Lcom/lge/ims/BatteryStateTracker;Z)Z

    #@80
    .line 398
    iget-object v2, p0, Lcom/lge/ims/BatteryStateTracker$BatteryStateReceiver;->this$0:Lcom/lge/ims/BatteryStateTracker;

    #@82
    invoke-virtual {v2}, Lcom/lge/ims/BatteryStateTracker;->getBatteryLevel()I

    #@85
    move-result v2

    #@86
    invoke-static {v2}, Lcom/lge/ims/BatteryStateTracker;->access$400(I)I

    #@89
    move-result v1

    #@8a
    .line 400
    .restart local v1       #interval:I
    if-nez v1, :cond_92

    #@8c
    .line 401
    iget-object v2, p0, Lcom/lge/ims/BatteryStateTracker$BatteryStateReceiver;->this$0:Lcom/lge/ims/BatteryStateTracker;

    #@8e
    invoke-static {v2}, Lcom/lge/ims/BatteryStateTracker;->access$700(Lcom/lge/ims/BatteryStateTracker;)V

    #@91
    goto :goto_2a

    #@92
    .line 403
    :cond_92
    iget-object v2, p0, Lcom/lge/ims/BatteryStateTracker$BatteryStateReceiver;->this$0:Lcom/lge/ims/BatteryStateTracker;

    #@94
    invoke-static {v2, v1}, Lcom/lge/ims/BatteryStateTracker;->access$600(Lcom/lge/ims/BatteryStateTracker;I)V
    :try_end_97
    .catchall {:try_start_49 .. :try_end_97} :catchall_46

    #@97
    goto :goto_2a
.end method
