.class public Lcom/lge/ims/AlarmTimerManager;
.super Ljava/lang/Object;
.source "AlarmTimerManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/AlarmTimerManager$AlarmTimerReceiver;
    }
.end annotation


# static fields
.field private static final ACTION_ALARM_NATIVE_TIMER:Ljava/lang/String; = "com.lge.ims.action.ALARM_NATIVE_TIMER"

.field private static final ACTION_ALARM_TIMER:Ljava/lang/String; = "com.lge.ims.action.ALARM_TIMER"

.field private static final EXTRA_PARAM_TID:Ljava/lang/String; = "tid"

.field private static final LONG_TIMER_INTERVAL:I = 0x7530

.field private static final MAX_TIMER_ID:I = 0x7fffffff

.field private static final TAG:Ljava/lang/String; = "AlarmTimerManager"

.field private static mAlarmTimerManager:Lcom/lge/ims/AlarmTimerManager;


# instance fields
.field private mAlarmTimerReceiver:Lcom/lge/ims/AlarmTimerManager$AlarmTimerReceiver;

.field private mContext:Landroid/content/Context;

.field private mLongTimers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mNativeTimerExpiredRegistrant:Landroid/os/Registrant;

.field private mTimerExpiredListeners:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/os/Registrant;",
            ">;"
        }
    .end annotation
.end field

.field private mTimerIdForUniqueness:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 31
    new-instance v0, Lcom/lge/ims/AlarmTimerManager;

    #@2
    invoke-direct {v0}, Lcom/lge/ims/AlarmTimerManager;-><init>()V

    #@5
    sput-object v0, Lcom/lge/ims/AlarmTimerManager;->mAlarmTimerManager:Lcom/lge/ims/AlarmTimerManager;

    #@7
    return-void
.end method

.method private constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 45
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 33
    iput-object v0, p0, Lcom/lge/ims/AlarmTimerManager;->mContext:Landroid/content/Context;

    #@6
    .line 34
    iput-object v0, p0, Lcom/lge/ims/AlarmTimerManager;->mNativeTimerExpiredRegistrant:Landroid/os/Registrant;

    #@8
    .line 35
    new-instance v0, Ljava/util/Hashtable;

    #@a
    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    #@d
    iput-object v0, p0, Lcom/lge/ims/AlarmTimerManager;->mTimerExpiredListeners:Ljava/util/Hashtable;

    #@f
    .line 36
    new-instance v0, Lcom/lge/ims/AlarmTimerManager$AlarmTimerReceiver;

    #@11
    invoke-direct {v0, p0}, Lcom/lge/ims/AlarmTimerManager$AlarmTimerReceiver;-><init>(Lcom/lge/ims/AlarmTimerManager;)V

    #@14
    iput-object v0, p0, Lcom/lge/ims/AlarmTimerManager;->mAlarmTimerReceiver:Lcom/lge/ims/AlarmTimerManager$AlarmTimerReceiver;

    #@16
    .line 39
    new-instance v0, Ljava/util/ArrayList;

    #@18
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@1b
    iput-object v0, p0, Lcom/lge/ims/AlarmTimerManager;->mLongTimers:Ljava/util/ArrayList;

    #@1d
    .line 41
    const/4 v0, 0x1

    #@1e
    iput v0, p0, Lcom/lge/ims/AlarmTimerManager;->mTimerIdForUniqueness:I

    #@20
    .line 46
    const-string v0, "AlarmTimerManager"

    #@22
    const-string v1, "AlarmTimerManager is created"

    #@24
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@27
    .line 47
    return-void
.end method

.method static synthetic access$000(Lcom/lge/ims/AlarmTimerManager;I)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/lge/ims/AlarmTimerManager;->checkAndRemoveLongTimer(I)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$100(Lcom/lge/ims/AlarmTimerManager;)Landroid/os/Registrant;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 18
    iget-object v0, p0, Lcom/lge/ims/AlarmTimerManager;->mNativeTimerExpiredRegistrant:Landroid/os/Registrant;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/lge/ims/AlarmTimerManager;)Ljava/util/Hashtable;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 18
    iget-object v0, p0, Lcom/lge/ims/AlarmTimerManager;->mTimerExpiredListeners:Ljava/util/Hashtable;

    #@2
    return-object v0
.end method

.method private checkAndRemoveLongTimer(I)Z
    .registers 6
    .parameter "tid"

    #@0
    .prologue
    .line 225
    const/4 v0, 0x0

    #@1
    .line 227
    .local v0, found:Z
    iget-object v2, p0, Lcom/lge/ims/AlarmTimerManager;->mLongTimers:Ljava/util/ArrayList;

    #@3
    monitor-enter v2

    #@4
    .line 228
    :try_start_4
    iget-object v1, p0, Lcom/lge/ims/AlarmTimerManager;->mLongTimers:Ljava/util/ArrayList;

    #@6
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@d
    move-result v0

    #@e
    .line 229
    monitor-exit v2

    #@f
    .line 231
    return v0

    #@10
    .line 229
    :catchall_10
    move-exception v1

    #@11
    monitor-exit v2
    :try_end_12
    .catchall {:try_start_4 .. :try_end_12} :catchall_10

    #@12
    throw v1
.end method

.method public static getInstance()Lcom/lge/ims/AlarmTimerManager;
    .registers 1

    #@0
    .prologue
    .line 50
    sget-object v0, Lcom/lge/ims/AlarmTimerManager;->mAlarmTimerManager:Lcom/lge/ims/AlarmTimerManager;

    #@2
    return-object v0
.end method

.method public static getTimerId()I
    .registers 7

    #@0
    .prologue
    const v6, 0x7fffffff

    #@3
    .line 55
    invoke-static {}, Lcom/lge/ims/AlarmTimerManager;->getInstance()Lcom/lge/ims/AlarmTimerManager;

    #@6
    move-result-object v1

    #@7
    .line 57
    .local v1, atm:Lcom/lge/ims/AlarmTimerManager;
    if-nez v1, :cond_b

    #@9
    .line 58
    const/4 v0, -0x1

    #@a
    .line 91
    :cond_a
    :goto_a
    return v0

    #@b
    .line 61
    :cond_b
    const/4 v0, -0x1

    #@c
    .line 62
    .local v0, allocatedTid:I
    iget v3, v1, Lcom/lge/ims/AlarmTimerManager;->mTimerIdForUniqueness:I

    #@e
    .line 63
    .local v3, tid:I
    const/4 v2, 0x0

    #@f
    .line 66
    .local v2, r:Landroid/os/Registrant;
    :cond_f
    :goto_f
    iget-object v4, v1, Lcom/lge/ims/AlarmTimerManager;->mTimerExpiredListeners:Ljava/util/Hashtable;

    #@11
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@14
    move-result-object v5

    #@15
    invoke-virtual {v4, v5}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@18
    move-result-object v2

    #@19
    .end local v2           #r:Landroid/os/Registrant;
    check-cast v2, Landroid/os/Registrant;

    #@1b
    .line 68
    .restart local v2       #r:Landroid/os/Registrant;
    if-nez v2, :cond_2c

    #@1d
    .line 69
    move v0, v3

    #@1e
    .line 83
    :goto_1e
    if-lez v0, :cond_a

    #@20
    .line 84
    add-int/lit8 v4, v0, 0x1

    #@22
    iput v4, v1, Lcom/lge/ims/AlarmTimerManager;->mTimerIdForUniqueness:I

    #@24
    .line 86
    iget v4, v1, Lcom/lge/ims/AlarmTimerManager;->mTimerIdForUniqueness:I

    #@26
    if-ne v4, v6, :cond_a

    #@28
    .line 87
    const/4 v4, 0x1

    #@29
    iput v4, v1, Lcom/lge/ims/AlarmTimerManager;->mTimerIdForUniqueness:I

    #@2b
    goto :goto_a

    #@2c
    .line 73
    :cond_2c
    add-int/lit8 v3, v3, 0x1

    #@2e
    .line 75
    if-ne v3, v6, :cond_32

    #@30
    .line 76
    const/4 v3, 0x1

    #@31
    goto :goto_f

    #@32
    .line 77
    :cond_32
    iget v4, v1, Lcom/lge/ims/AlarmTimerManager;->mTimerIdForUniqueness:I

    #@34
    if-ne v3, v4, :cond_f

    #@36
    .line 78
    const-string v4, "AlarmTimerManager"

    #@38
    const-string v5, "There is no available timer id"

    #@3a
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@3d
    goto :goto_1e
.end method


# virtual methods
.method public destroy()V
    .registers 3

    #@0
    .prologue
    .line 95
    iget-object v0, p0, Lcom/lge/ims/AlarmTimerManager;->mContext:Landroid/content/Context;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 96
    iget-object v0, p0, Lcom/lge/ims/AlarmTimerManager;->mContext:Landroid/content/Context;

    #@6
    iget-object v1, p0, Lcom/lge/ims/AlarmTimerManager;->mAlarmTimerReceiver:Lcom/lge/ims/AlarmTimerManager$AlarmTimerReceiver;

    #@8
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@b
    .line 98
    :cond_b
    return-void
.end method

.method public registerForNativeTimerExpired(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 102
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    iput-object v0, p0, Lcom/lge/ims/AlarmTimerManager;->mNativeTimerExpiredRegistrant:Landroid/os/Registrant;

    #@7
    .line 103
    return-void
.end method

.method public registerForTimerExpired(ILandroid/os/Handler;ILjava/lang/Object;)V
    .registers 9
    .parameter "tid"
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 110
    iget-object v1, p0, Lcom/lge/ims/AlarmTimerManager;->mTimerExpiredListeners:Ljava/util/Hashtable;

    #@2
    monitor-enter v1

    #@3
    .line 111
    :try_start_3
    iget-object v0, p0, Lcom/lge/ims/AlarmTimerManager;->mTimerExpiredListeners:Ljava/util/Hashtable;

    #@5
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8
    move-result-object v2

    #@9
    new-instance v3, Landroid/os/Registrant;

    #@b
    invoke-direct {v3, p2, p3, p4}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@e
    invoke-virtual {v0, v2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@11
    .line 112
    monitor-exit v1

    #@12
    .line 113
    return-void

    #@13
    .line 112
    :catchall_13
    move-exception v0

    #@14
    monitor-exit v1
    :try_end_15
    .catchall {:try_start_3 .. :try_end_15} :catchall_13

    #@15
    throw v0
.end method

.method public setContext(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 122
    iget-object v0, p0, Lcom/lge/ims/AlarmTimerManager;->mContext:Landroid/content/Context;

    #@2
    if-eq v0, p1, :cond_15

    #@4
    .line 124
    iget-object v0, p0, Lcom/lge/ims/AlarmTimerManager;->mContext:Landroid/content/Context;

    #@6
    if-eqz v0, :cond_f

    #@8
    .line 125
    iget-object v0, p0, Lcom/lge/ims/AlarmTimerManager;->mContext:Landroid/content/Context;

    #@a
    iget-object v1, p0, Lcom/lge/ims/AlarmTimerManager;->mAlarmTimerReceiver:Lcom/lge/ims/AlarmTimerManager$AlarmTimerReceiver;

    #@c
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@f
    .line 128
    :cond_f
    iput-object p1, p0, Lcom/lge/ims/AlarmTimerManager;->mContext:Landroid/content/Context;

    #@11
    .line 130
    iget-object v0, p0, Lcom/lge/ims/AlarmTimerManager;->mContext:Landroid/content/Context;

    #@13
    if-nez v0, :cond_16

    #@15
    .line 138
    :cond_15
    :goto_15
    return-void

    #@16
    .line 134
    :cond_16
    iget-object v0, p0, Lcom/lge/ims/AlarmTimerManager;->mAlarmTimerReceiver:Lcom/lge/ims/AlarmTimerManager$AlarmTimerReceiver;

    #@18
    if-eqz v0, :cond_15

    #@1a
    .line 135
    iget-object v0, p0, Lcom/lge/ims/AlarmTimerManager;->mContext:Landroid/content/Context;

    #@1c
    iget-object v1, p0, Lcom/lge/ims/AlarmTimerManager;->mAlarmTimerReceiver:Lcom/lge/ims/AlarmTimerManager$AlarmTimerReceiver;

    #@1e
    iget-object v2, p0, Lcom/lge/ims/AlarmTimerManager;->mAlarmTimerReceiver:Lcom/lge/ims/AlarmTimerManager$AlarmTimerReceiver;

    #@20
    invoke-virtual {v2}, Lcom/lge/ims/AlarmTimerManager$AlarmTimerReceiver;->getFilter()Landroid/content/IntentFilter;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@27
    goto :goto_15
.end method

.method public startNativeTimer(II)V
    .registers 11
    .parameter "tid"
    .parameter "duration"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 141
    const-string v3, "AlarmTimerManager"

    #@3
    new-instance v4, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v5, "startNativeTimer :: tid="

    #@a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v4

    #@e
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v4

    #@12
    const-string v5, ", duration="

    #@14
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v4

    #@18
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v4

    #@20
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@23
    .line 143
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getAlarmManager()Landroid/app/AlarmManager;

    #@26
    move-result-object v0

    #@27
    .line 145
    .local v0, am:Landroid/app/AlarmManager;
    if-nez v0, :cond_31

    #@29
    .line 146
    const-string v3, "AlarmTimerManager"

    #@2b
    const-string v4, "AlarmManager is null"

    #@2d
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@30
    .line 163
    :goto_30
    return-void

    #@31
    .line 151
    :cond_31
    const/16 v3, 0x7530

    #@33
    if-lt p2, v3, :cond_42

    #@35
    .line 152
    iget-object v4, p0, Lcom/lge/ims/AlarmTimerManager;->mLongTimers:Ljava/util/ArrayList;

    #@37
    monitor-enter v4

    #@38
    .line 153
    :try_start_38
    iget-object v3, p0, Lcom/lge/ims/AlarmTimerManager;->mLongTimers:Ljava/util/ArrayList;

    #@3a
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3d
    move-result-object v5

    #@3e
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@41
    .line 154
    monitor-exit v4
    :try_end_42
    .catchall {:try_start_38 .. :try_end_42} :catchall_5e

    #@42
    .line 157
    :cond_42
    new-instance v1, Landroid/content/Intent;

    #@44
    const-string v3, "com.lge.ims.action.ALARM_NATIVE_TIMER"

    #@46
    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@49
    .line 158
    .local v1, intent:Landroid/content/Intent;
    const-string v3, "tid"

    #@4b
    invoke-virtual {v1, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@4e
    .line 160
    iget-object v3, p0, Lcom/lge/ims/AlarmTimerManager;->mContext:Landroid/content/Context;

    #@50
    invoke-static {v3, p1, v1, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@53
    move-result-object v2

    #@54
    .line 162
    .local v2, sender:Landroid/app/PendingIntent;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@57
    move-result-wide v3

    #@58
    int-to-long v5, p2

    #@59
    add-long/2addr v3, v5

    #@5a
    invoke-virtual {v0, v7, v3, v4, v2}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    #@5d
    goto :goto_30

    #@5e
    .line 154
    .end local v1           #intent:Landroid/content/Intent;
    .end local v2           #sender:Landroid/app/PendingIntent;
    :catchall_5e
    move-exception v3

    #@5f
    :try_start_5f
    monitor-exit v4
    :try_end_60
    .catchall {:try_start_5f .. :try_end_60} :catchall_5e

    #@60
    throw v3
.end method

.method public startTimer(II)Z
    .registers 11
    .parameter "tid"
    .parameter "duration"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 187
    const-string v4, "AlarmTimerManager"

    #@3
    new-instance v5, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v6, "startTimer :: tid="

    #@a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v5

    #@e
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v5

    #@12
    const-string v6, ", duration="

    #@14
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v5

    #@18
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v5

    #@1c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v5

    #@20
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@23
    .line 189
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getAlarmManager()Landroid/app/AlarmManager;

    #@26
    move-result-object v0

    #@27
    .line 191
    .local v0, am:Landroid/app/AlarmManager;
    if-nez v0, :cond_31

    #@29
    .line 192
    const-string v4, "AlarmTimerManager"

    #@2b
    const-string v5, "AlarmManager is null"

    #@2d
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@30
    .line 203
    :goto_30
    return v3

    #@31
    .line 196
    :cond_31
    new-instance v1, Landroid/content/Intent;

    #@33
    const-string v4, "com.lge.ims.action.ALARM_TIMER"

    #@35
    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@38
    .line 197
    .local v1, intent:Landroid/content/Intent;
    const-string v4, "tid"

    #@3a
    invoke-virtual {v1, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@3d
    .line 199
    iget-object v4, p0, Lcom/lge/ims/AlarmTimerManager;->mContext:Landroid/content/Context;

    #@3f
    invoke-static {v4, p1, v1, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@42
    move-result-object v2

    #@43
    .line 201
    .local v2, sender:Landroid/app/PendingIntent;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@46
    move-result-wide v4

    #@47
    int-to-long v6, p2

    #@48
    add-long/2addr v4, v6

    #@49
    invoke-virtual {v0, v3, v4, v5, v2}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    #@4c
    .line 203
    const/4 v3, 0x1

    #@4d
    goto :goto_30
.end method

.method public stopNativeTimer(I)V
    .registers 8
    .parameter "tid"

    #@0
    .prologue
    .line 166
    const-string v3, "AlarmTimerManager"

    #@2
    new-instance v4, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v5, "stopNativeTimer :: tid="

    #@9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v4

    #@d
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v4

    #@11
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v4

    #@15
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 168
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getAlarmManager()Landroid/app/AlarmManager;

    #@1b
    move-result-object v0

    #@1c
    .line 170
    .local v0, am:Landroid/app/AlarmManager;
    if-nez v0, :cond_26

    #@1e
    .line 171
    const-string v3, "AlarmTimerManager"

    #@20
    const-string v4, "AlarmManager is null"

    #@22
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@25
    .line 184
    :goto_25
    return-void

    #@26
    .line 176
    :cond_26
    invoke-direct {p0, p1}, Lcom/lge/ims/AlarmTimerManager;->checkAndRemoveLongTimer(I)Z

    #@29
    .line 178
    new-instance v1, Landroid/content/Intent;

    #@2b
    const-string v3, "com.lge.ims.action.ALARM_NATIVE_TIMER"

    #@2d
    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@30
    .line 179
    .local v1, intent:Landroid/content/Intent;
    const-string v3, "tid"

    #@32
    invoke-virtual {v1, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@35
    .line 181
    iget-object v3, p0, Lcom/lge/ims/AlarmTimerManager;->mContext:Landroid/content/Context;

    #@37
    const/4 v4, 0x0

    #@38
    invoke-static {v3, p1, v1, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@3b
    move-result-object v2

    #@3c
    .line 183
    .local v2, sender:Landroid/app/PendingIntent;
    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    #@3f
    goto :goto_25
.end method

.method public stopTimer(I)V
    .registers 8
    .parameter "tid"

    #@0
    .prologue
    .line 207
    const-string v3, "AlarmTimerManager"

    #@2
    new-instance v4, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v5, "stopTimer :: tid="

    #@9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v4

    #@d
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v4

    #@11
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v4

    #@15
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 209
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getAlarmManager()Landroid/app/AlarmManager;

    #@1b
    move-result-object v0

    #@1c
    .line 211
    .local v0, am:Landroid/app/AlarmManager;
    if-nez v0, :cond_26

    #@1e
    .line 212
    const-string v3, "AlarmTimerManager"

    #@20
    const-string v4, "AlarmManager is null"

    #@22
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@25
    .line 222
    :goto_25
    return-void

    #@26
    .line 216
    :cond_26
    new-instance v1, Landroid/content/Intent;

    #@28
    const-string v3, "com.lge.ims.action.ALARM_TIMER"

    #@2a
    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@2d
    .line 217
    .local v1, intent:Landroid/content/Intent;
    const-string v3, "tid"

    #@2f
    invoke-virtual {v1, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@32
    .line 219
    iget-object v3, p0, Lcom/lge/ims/AlarmTimerManager;->mContext:Landroid/content/Context;

    #@34
    const/4 v4, 0x0

    #@35
    invoke-static {v3, p1, v1, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@38
    move-result-object v2

    #@39
    .line 221
    .local v2, sender:Landroid/app/PendingIntent;
    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    #@3c
    goto :goto_25
.end method

.method public unregisterForNativeTimerExpired(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 106
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Lcom/lge/ims/AlarmTimerManager;->mNativeTimerExpiredRegistrant:Landroid/os/Registrant;

    #@3
    .line 107
    return-void
.end method

.method public unregisterForTimerExpired(ILandroid/os/Handler;)V
    .registers 6
    .parameter "tid"
    .parameter "h"

    #@0
    .prologue
    .line 116
    iget-object v1, p0, Lcom/lge/ims/AlarmTimerManager;->mTimerExpiredListeners:Ljava/util/Hashtable;

    #@2
    monitor-enter v1

    #@3
    .line 117
    :try_start_3
    iget-object v0, p0, Lcom/lge/ims/AlarmTimerManager;->mTimerExpiredListeners:Ljava/util/Hashtable;

    #@5
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8
    move-result-object v2

    #@9
    invoke-virtual {v0, v2}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@c
    .line 118
    monitor-exit v1

    #@d
    .line 119
    return-void

    #@e
    .line 118
    :catchall_e
    move-exception v0

    #@f
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_e

    #@10
    throw v0
.end method
