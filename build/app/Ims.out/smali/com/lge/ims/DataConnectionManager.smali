.class public Lcom/lge/ims/DataConnectionManager;
.super Ljava/lang/Object;
.source "DataConnectionManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/DataConnectionManager$1;,
        Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;,
        Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;
    }
.end annotation


# static fields
.field public static final APN_EMERGENCY:I = 0x9

.field public static final APN_IMS:I = 0x1

.field public static final APN_INTERNET:I = 0x2

.field private static final APN_REQUEST_BLOCKED:I = 0x3

.field private static final APN_REQUEST_DONE:I = 0x2

.field private static final APN_REQUEST_IDLE:I = 0x0

.field private static final APN_REQUEST_MAX_RETRIES:I = 0x5

.field private static final APN_REQUEST_MAX_RETRIES_FOR_EMERGENCY:I = 0x3

.field private static final APN_REQUEST_PENDING:I = 0x1

.field private static final APN_REQUEST_RETRY_INTERVAL:I = 0x7d0

.field private static final APN_REQUEST_RETRY_INTERVAL_FOR_EMERGENCY:I = 0x3e8

.field private static final APN_REQUEST_RETRY_LONG_INTERVAL:I = 0x2710

.field public static final APN_STR_EMERGENCY:Ljava/lang/String; = "mobile_emergency"

.field public static final APN_STR_IMS:Ljava/lang/String; = "mobile_ims"

.field public static final APN_STR_INTERNET:Ljava/lang/String; = "mobile_internet"

.field public static final DATA_STATE_CONNECTED:I = 0x0

.field public static final DATA_STATE_CONNECT_FAILED:I = 0x2

.field public static final DATA_STATE_DISCONNECTED:I = 0x1

.field public static final DATA_STATE_IP_CHANGED:I = 0x3

.field private static final EVENT_DATA_SERVICE_STATE_CHANGED:I = 0x3ea

.field private static final EVENT_IMS_PHONE_RESTARTED:I = 0x3e9

.field private static final EVENT_IMS_SERVICE_STARTED:I = 0x7d1

.field private static final EVENT_NOTIFY_DATA_STATE_CHANGED:I = 0x7db

.field private static final EVENT_RAT_CHANGED:I = 0x3eb

.field private static final EVENT_RETRY_EMERGENCY_APN_REQUEST:I = 0x7d3

.field private static final EVENT_RETRY_IMS_APN_REQUEST:I = 0x7d2

.field private static final EVENT_ROAMING_STATE_CHANGED:I = 0x3ec

.field private static final EVENT_UNBLOCK_IMS_APN_REQUEST:I = 0x7d5

.field private static final POLICY_APN_MULTIPLE:I = 0x2

.field private static final POLICY_APN_SINGLE:I = 0x1

.field private static final POLICY_APN_SINGLE_AND_IMS:I = 0x4

.field private static final POLICY_IMS_APN_BLOCK_REQUIRED:I = 0x20

.field private static final POLICY_INTERNET_APN_REQUEST_REQUIRED:I = 0x10

.field private static final POLICY_RAT_2G:I = 0x800

.field private static final POLICY_RAT_3G:I = 0x200

.field private static final POLICY_RAT_4G:I = 0x400

.field private static final POLICY_RAT_EHRPD:I = 0x100

.field private static final POLICY_ROAMING_ALLOWED:I = 0x40

.field private static final TAG:Ljava/lang/String; = "DataConnectionManager"

.field private static mDataConnectionManager:Lcom/lge/ims/DataConnectionManager;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDataConnectionHandler:Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;

.field private mDataConnectionReceiver:Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;

.field private mDataStateChangedRegistrants:Landroid/os/RegistrantList;

.field private mDefaultApn:Ljava/lang/String;

.field private mDefaultDataState:I

.field private mEmcFailCause:Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

.field private mEmergencyApn:Ljava/lang/String;

.field private mEmergencyApnRequestRetries:I

.field private mEmergencyApnRequestState:I

.field private mEmergencyDataState:I

.field private mImsApn:Ljava/lang/String;

.field private mImsApnRequestRetries:I

.field private mImsApnRequestState:I

.field private mImsDataState:I

.field private mImsServiceStarted:Z

.field private mLGfeature:Lcom/android/internal/telephony/LGfeature;

.field private mPolicy:I

.field private mWiFiConnected:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 93
    new-instance v0, Lcom/lge/ims/DataConnectionManager;

    #@2
    invoke-direct {v0}, Lcom/lge/ims/DataConnectionManager;-><init>()V

    #@5
    sput-object v0, Lcom/lge/ims/DataConnectionManager;->mDataConnectionManager:Lcom/lge/ims/DataConnectionManager;

    #@7
    return-void
.end method

.method private constructor <init>()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 123
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 95
    new-instance v0, Landroid/os/RegistrantList;

    #@7
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@a
    iput-object v0, p0, Lcom/lge/ims/DataConnectionManager;->mDataStateChangedRegistrants:Landroid/os/RegistrantList;

    #@c
    .line 96
    new-instance v0, Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;

    #@e
    invoke-direct {v0, p0, v2}, Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;-><init>(Lcom/lge/ims/DataConnectionManager;Lcom/lge/ims/DataConnectionManager$1;)V

    #@11
    iput-object v0, p0, Lcom/lge/ims/DataConnectionManager;->mDataConnectionHandler:Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;

    #@13
    .line 97
    new-instance v0, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;

    #@15
    invoke-direct {v0, p0}, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;-><init>(Lcom/lge/ims/DataConnectionManager;)V

    #@18
    iput-object v0, p0, Lcom/lge/ims/DataConnectionManager;->mDataConnectionReceiver:Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;

    #@1a
    .line 99
    iput-object v2, p0, Lcom/lge/ims/DataConnectionManager;->mContext:Landroid/content/Context;

    #@1c
    .line 101
    iput-object v2, p0, Lcom/lge/ims/DataConnectionManager;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@1e
    .line 103
    iput v1, p0, Lcom/lge/ims/DataConnectionManager;->mPolicy:I

    #@20
    .line 105
    iput-boolean v1, p0, Lcom/lge/ims/DataConnectionManager;->mWiFiConnected:Z

    #@22
    .line 107
    const/4 v0, 0x1

    #@23
    iput-boolean v0, p0, Lcom/lge/ims/DataConnectionManager;->mImsServiceStarted:Z

    #@25
    .line 109
    const-string v0, ""

    #@27
    iput-object v0, p0, Lcom/lge/ims/DataConnectionManager;->mEmergencyApn:Ljava/lang/String;

    #@29
    .line 110
    const-string v0, ""

    #@2b
    iput-object v0, p0, Lcom/lge/ims/DataConnectionManager;->mImsApn:Ljava/lang/String;

    #@2d
    .line 111
    const-string v0, ""

    #@2f
    iput-object v0, p0, Lcom/lge/ims/DataConnectionManager;->mDefaultApn:Ljava/lang/String;

    #@31
    .line 112
    iput v1, p0, Lcom/lge/ims/DataConnectionManager;->mEmergencyDataState:I

    #@33
    .line 113
    iput v1, p0, Lcom/lge/ims/DataConnectionManager;->mImsDataState:I

    #@35
    .line 114
    iput v1, p0, Lcom/lge/ims/DataConnectionManager;->mDefaultDataState:I

    #@37
    .line 115
    iput v1, p0, Lcom/lge/ims/DataConnectionManager;->mEmergencyApnRequestState:I

    #@39
    .line 116
    iput v1, p0, Lcom/lge/ims/DataConnectionManager;->mEmergencyApnRequestRetries:I

    #@3b
    .line 117
    iput v1, p0, Lcom/lge/ims/DataConnectionManager;->mImsApnRequestState:I

    #@3d
    .line 118
    iput v1, p0, Lcom/lge/ims/DataConnectionManager;->mImsApnRequestRetries:I

    #@3f
    .line 119
    invoke-static {v1}, Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;->fromInt(I)Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

    #@42
    move-result-object v0

    #@43
    iput-object v0, p0, Lcom/lge/ims/DataConnectionManager;->mEmcFailCause:Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

    #@45
    .line 124
    const-string v0, "DataConnectionManager"

    #@47
    const-string v1, "DataConnectionManager is created"

    #@49
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@4c
    .line 125
    return-void
.end method

.method static synthetic access$1002(Lcom/lge/ims/DataConnectionManager;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 35
    iput-object p1, p0, Lcom/lge/ims/DataConnectionManager;->mEmergencyApn:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$102(Lcom/lge/ims/DataConnectionManager;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/lge/ims/DataConnectionManager;->mWiFiConnected:Z

    #@2
    return p1
.end method

.method static synthetic access$1102(Lcom/lge/ims/DataConnectionManager;Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;)Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 35
    iput-object p1, p0, Lcom/lge/ims/DataConnectionManager;->mEmcFailCause:Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

    #@2
    return-object p1
.end method

.method static synthetic access$1202(Lcom/lge/ims/DataConnectionManager;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/lge/ims/DataConnectionManager;->mImsServiceStarted:Z

    #@2
    return p1
.end method

.method static synthetic access$200(Lcom/lge/ims/DataConnectionManager;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 35
    iget v0, p0, Lcom/lge/ims/DataConnectionManager;->mDefaultDataState:I

    #@2
    return v0
.end method

.method static synthetic access$202(Lcom/lge/ims/DataConnectionManager;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 35
    iput p1, p0, Lcom/lge/ims/DataConnectionManager;->mDefaultDataState:I

    #@2
    return p1
.end method

.method static synthetic access$302(Lcom/lge/ims/DataConnectionManager;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 35
    iput-object p1, p0, Lcom/lge/ims/DataConnectionManager;->mDefaultApn:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$400(Lcom/lge/ims/DataConnectionManager;)Landroid/os/RegistrantList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 35
    iget-object v0, p0, Lcom/lge/ims/DataConnectionManager;->mDataStateChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Lcom/lge/ims/DataConnectionManager;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 35
    iget v0, p0, Lcom/lge/ims/DataConnectionManager;->mImsApnRequestState:I

    #@2
    return v0
.end method

.method static synthetic access$502(Lcom/lge/ims/DataConnectionManager;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 35
    iput p1, p0, Lcom/lge/ims/DataConnectionManager;->mImsApnRequestState:I

    #@2
    return p1
.end method

.method static synthetic access$600(Lcom/lge/ims/DataConnectionManager;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 35
    iget v0, p0, Lcom/lge/ims/DataConnectionManager;->mImsDataState:I

    #@2
    return v0
.end method

.method static synthetic access$602(Lcom/lge/ims/DataConnectionManager;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 35
    iput p1, p0, Lcom/lge/ims/DataConnectionManager;->mImsDataState:I

    #@2
    return p1
.end method

.method static synthetic access$702(Lcom/lge/ims/DataConnectionManager;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 35
    iput-object p1, p0, Lcom/lge/ims/DataConnectionManager;->mImsApn:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$800(Lcom/lge/ims/DataConnectionManager;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 35
    iget v0, p0, Lcom/lge/ims/DataConnectionManager;->mEmergencyApnRequestState:I

    #@2
    return v0
.end method

.method static synthetic access$802(Lcom/lge/ims/DataConnectionManager;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 35
    iput p1, p0, Lcom/lge/ims/DataConnectionManager;->mEmergencyApnRequestState:I

    #@2
    return p1
.end method

.method static synthetic access$900(Lcom/lge/ims/DataConnectionManager;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 35
    iget v0, p0, Lcom/lge/ims/DataConnectionManager;->mEmergencyDataState:I

    #@2
    return v0
.end method

.method static synthetic access$902(Lcom/lge/ims/DataConnectionManager;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 35
    iput p1, p0, Lcom/lge/ims/DataConnectionManager;->mEmergencyDataState:I

    #@2
    return p1
.end method

.method public static convertImsDataState(I)I
    .registers 3
    .parameter "state"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 766
    const/4 v1, 0x2

    #@2
    if-ne p0, v1, :cond_5

    #@4
    .line 771
    :cond_4
    :goto_4
    return v0

    #@5
    .line 768
    :cond_5
    const/4 v1, 0x3

    #@6
    if-eq p0, v1, :cond_4

    #@8
    .line 771
    const/4 v0, 0x1

    #@9
    goto :goto_4
.end method

.method public static getApnName(I)Ljava/lang/String;
    .registers 2
    .parameter "apnType"

    #@0
    .prologue
    .line 776
    const/4 v0, 0x1

    #@1
    if-ne p0, v0, :cond_6

    #@3
    .line 777
    const-string v0, "mobile_ims"

    #@5
    .line 783
    :goto_5
    return-object v0

    #@6
    .line 778
    :cond_6
    const/4 v0, 0x2

    #@7
    if-ne p0, v0, :cond_c

    #@9
    .line 779
    const-string v0, "mobile_internet"

    #@b
    goto :goto_5

    #@c
    .line 780
    :cond_c
    const/16 v0, 0x9

    #@e
    if-ne p0, v0, :cond_13

    #@10
    .line 781
    const-string v0, "mobile_emergency"

    #@12
    goto :goto_5

    #@13
    .line 783
    :cond_13
    const/4 v0, 0x0

    #@14
    goto :goto_5
.end method

.method public static getApnTypeFromDataStateChanged(I)I
    .registers 3
    .parameter "result"

    #@0
    .prologue
    .line 804
    shr-int/lit8 v0, p0, 0x10

    #@2
    const v1, 0xffff

    #@5
    and-int/2addr v0, v1

    #@6
    return v0
.end method

.method public static getApnTypeFromPhone(I)Ljava/lang/String;
    .registers 3
    .parameter "apnType"

    #@0
    .prologue
    .line 788
    const/4 v0, 0x1

    #@1
    if-ne p0, v0, :cond_6

    #@3
    .line 789
    const-string v0, "ims"

    #@5
    .line 799
    :goto_5
    return-object v0

    #@6
    .line 790
    :cond_6
    const/4 v0, 0x2

    #@7
    if-ne p0, v0, :cond_19

    #@9
    .line 791
    sget-object v0, Lcom/lge/ims/Configuration;->OPERATOR:Ljava/lang/String;

    #@b
    const-string v1, "VZW"

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10
    move-result v0

    #@11
    if-eqz v0, :cond_16

    #@13
    .line 792
    const-string v0, "internet"

    #@15
    goto :goto_5

    #@16
    .line 794
    :cond_16
    const-string v0, "default"

    #@18
    goto :goto_5

    #@19
    .line 796
    :cond_19
    const/16 v0, 0x9

    #@1b
    if-ne p0, v0, :cond_20

    #@1d
    .line 797
    const-string v0, "emergency"

    #@1f
    goto :goto_5

    #@20
    .line 799
    :cond_20
    const/4 v0, 0x0

    #@21
    goto :goto_5
.end method

.method public static getDataStateFromDataStateChanged(I)I
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 808
    const v0, 0xffff

    #@3
    and-int/2addr v0, p0

    #@4
    return v0
.end method

.method private getIPAddress(Ljava/util/Collection;I)Ljava/lang/String;
    .registers 10
    .parameter
    .parameter "ipVersion"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Landroid/net/LinkAddress;",
            ">;I)",
            "Ljava/lang/String;"
        }
    .end annotation

    #@0
    .prologue
    .local p1, linkAddresses:Ljava/util/Collection;,"Ljava/util/Collection<Landroid/net/LinkAddress;>;"
    const/4 v0, 0x0

    #@1
    .line 1235
    if-nez p1, :cond_4

    #@3
    .line 1289
    :cond_3
    :goto_3
    return-object v0

    #@4
    .line 1239
    :cond_4
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@7
    move-result-object v1

    #@8
    .line 1241
    .local v1, iterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/net/LinkAddress;>;"
    if-eqz v1, :cond_3

    #@a
    .line 1245
    const/4 v4, 0x6

    #@b
    if-ne p2, v4, :cond_5f

    #@d
    .line 1246
    :cond_d
    :goto_d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@10
    move-result v4

    #@11
    if-eqz v4, :cond_3

    #@13
    .line 1247
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@16
    move-result-object v2

    #@17
    check-cast v2, Landroid/net/LinkAddress;

    #@19
    .line 1249
    .local v2, linkAddress:Landroid/net/LinkAddress;
    if-eqz v2, :cond_d

    #@1b
    .line 1250
    invoke-virtual {v2}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;

    #@1e
    move-result-object v3

    #@1f
    .line 1252
    .local v3, netAddress:Ljava/net/InetAddress;
    if-nez v3, :cond_3e

    #@21
    .line 1253
    const-string v4, "DataConnectionManager"

    #@23
    new-instance v5, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v6, "Resolving InetAddress failed - "

    #@2a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v5

    #@2e
    invoke-virtual {v2}, Landroid/net/LinkAddress;->toString()Ljava/lang/String;

    #@31
    move-result-object v6

    #@32
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v5

    #@36
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v5

    #@3a
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@3d
    goto :goto_d

    #@3e
    .line 1257
    :cond_3e
    instance-of v4, v3, Ljava/net/Inet6Address;

    #@40
    if-eqz v4, :cond_d

    #@42
    .line 1258
    invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@45
    move-result-object v0

    #@46
    .line 1260
    .local v0, ipAddress:Ljava/lang/String;
    const-string v4, "DataConnectionManager"

    #@48
    new-instance v5, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v6, "Mobile :: ip6Address="

    #@4f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v5

    #@53
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v5

    #@57
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v5

    #@5b
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@5e
    goto :goto_3

    #@5f
    .line 1266
    .end local v0           #ipAddress:Ljava/lang/String;
    .end local v2           #linkAddress:Landroid/net/LinkAddress;
    .end local v3           #netAddress:Ljava/net/InetAddress;
    :cond_5f
    const/4 v4, 0x4

    #@60
    if-ne p2, v4, :cond_3

    #@62
    .line 1267
    :cond_62
    :goto_62
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@65
    move-result v4

    #@66
    if-eqz v4, :cond_3

    #@68
    .line 1268
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@6b
    move-result-object v2

    #@6c
    check-cast v2, Landroid/net/LinkAddress;

    #@6e
    .line 1270
    .restart local v2       #linkAddress:Landroid/net/LinkAddress;
    if-eqz v2, :cond_62

    #@70
    .line 1271
    invoke-virtual {v2}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;

    #@73
    move-result-object v3

    #@74
    .line 1273
    .restart local v3       #netAddress:Ljava/net/InetAddress;
    if-nez v3, :cond_93

    #@76
    .line 1274
    const-string v4, "DataConnectionManager"

    #@78
    new-instance v5, Ljava/lang/StringBuilder;

    #@7a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@7d
    const-string v6, "Resolving InetAddress failed - "

    #@7f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v5

    #@83
    invoke-virtual {v2}, Landroid/net/LinkAddress;->toString()Ljava/lang/String;

    #@86
    move-result-object v6

    #@87
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v5

    #@8b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8e
    move-result-object v5

    #@8f
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@92
    goto :goto_62

    #@93
    .line 1278
    :cond_93
    instance-of v4, v3, Ljava/net/Inet4Address;

    #@95
    if-eqz v4, :cond_62

    #@97
    .line 1279
    invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@9a
    move-result-object v0

    #@9b
    .line 1281
    .restart local v0       #ipAddress:Ljava/lang/String;
    const-string v4, "DataConnectionManager"

    #@9d
    new-instance v5, Ljava/lang/StringBuilder;

    #@9f
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@a2
    const-string v6, "Mobile :: ip4Address="

    #@a4
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v5

    #@a8
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v5

    #@ac
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@af
    move-result-object v5

    #@b0
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@b3
    goto/16 :goto_3
.end method

.method public static getInstance()Lcom/lge/ims/DataConnectionManager;
    .registers 1

    #@0
    .prologue
    .line 128
    sget-object v0, Lcom/lge/ims/DataConnectionManager;->mDataConnectionManager:Lcom/lge/ims/DataConnectionManager;

    #@2
    return-object v0
.end method

.method private getValidAddresses([Ljava/lang/String;)[Ljava/lang/String;
    .registers 14
    .parameter "addresses"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v11, 0x1

    #@2
    .line 1293
    if-nez p1, :cond_c

    #@4
    .line 1294
    const-string v8, "DataConnectionManager"

    #@6
    const-string v9, "Addresses is null"

    #@8
    invoke-static {v8, v9}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@b
    .line 1351
    :goto_b
    return-object v5

    #@c
    .line 1298
    :cond_c
    array-length v8, p1

    #@d
    new-array v7, v8, [Z

    #@f
    .line 1300
    .local v7, validities:[Z
    const/4 v1, 0x0

    #@10
    .local v1, i:I
    :goto_10
    array-length v8, p1

    #@11
    if-ge v1, v8, :cond_19

    #@13
    .line 1301
    const/4 v8, 0x0

    #@14
    aput-boolean v8, v7, v1

    #@16
    .line 1300
    add-int/lit8 v1, v1, 0x1

    #@18
    goto :goto_10

    #@19
    .line 1304
    :cond_19
    const/4 v6, 0x0

    #@1a
    .line 1306
    .local v6, validCount:I
    const/4 v1, 0x0

    #@1b
    :goto_1b
    array-length v8, p1

    #@1c
    if-ge v1, v8, :cond_ac

    #@1e
    .line 1307
    const/4 v4, 0x0

    #@1f
    .line 1309
    .local v4, result:Z
    aget-object v8, p1, v1

    #@21
    if-nez v8, :cond_44

    #@23
    .line 1310
    const-string v8, "DataConnectionManager"

    #@25
    new-instance v9, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v10, "Address at index("

    #@2c
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v9

    #@30
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@33
    move-result-object v9

    #@34
    const-string v10, ") is null"

    #@36
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v9

    #@3a
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v9

    #@3e
    invoke-static {v8, v9}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@41
    .line 1306
    :cond_41
    :goto_41
    add-int/lit8 v1, v1, 0x1

    #@43
    goto :goto_1b

    #@44
    .line 1314
    :cond_44
    const/4 v2, 0x0

    #@45
    .line 1317
    .local v2, inetAddress:Ljava/net/InetAddress;
    :try_start_45
    aget-object v8, p1, v1

    #@47
    invoke-static {v8}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;
    :try_end_4a
    .catch Ljava/net/UnknownHostException; {:try_start_45 .. :try_end_4a} :catch_7c

    #@4a
    move-result-object v2

    #@4b
    .line 1322
    :goto_4b
    if-eqz v2, :cond_41

    #@4d
    .line 1326
    const-string v8, "DataConnectionManager"

    #@4f
    new-instance v9, Ljava/lang/StringBuilder;

    #@51
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@54
    const-string v10, "Address("

    #@56
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v9

    #@5a
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v9

    #@5e
    const-string v10, ") >> "

    #@60
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v9

    #@64
    aget-object v10, p1, v1

    #@66
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v9

    #@6a
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d
    move-result-object v9

    #@6e
    invoke-static {v8, v9}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@71
    .line 1328
    invoke-virtual {v2}, Ljava/net/InetAddress;->isAnyLocalAddress()Z

    #@74
    move-result v8

    #@75
    if-nez v8, :cond_41

    #@77
    .line 1329
    aput-boolean v11, v7, v1

    #@79
    .line 1330
    add-int/lit8 v6, v6, 0x1

    #@7b
    goto :goto_41

    #@7c
    .line 1318
    :catch_7c
    move-exception v0

    #@7d
    .line 1319
    .local v0, e:Ljava/net/UnknownHostException;
    const-string v8, "DataConnectionManager"

    #@7f
    new-instance v9, Ljava/lang/StringBuilder;

    #@81
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@84
    const-string v10, "Exception :: "

    #@86
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v9

    #@8a
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v9

    #@8e
    const-string v10, ", Address("

    #@90
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v9

    #@94
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@97
    move-result-object v9

    #@98
    const-string v10, ") >> "

    #@9a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v9

    #@9e
    aget-object v10, p1, v1

    #@a0
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v9

    #@a4
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a7
    move-result-object v9

    #@a8
    invoke-static {v8, v9}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@ab
    goto :goto_4b

    #@ac
    .line 1334
    .end local v0           #e:Ljava/net/UnknownHostException;
    .end local v2           #inetAddress:Ljava/net/InetAddress;
    .end local v4           #result:Z
    :cond_ac
    if-nez v6, :cond_b7

    #@ae
    .line 1335
    const-string v8, "DataConnectionManager"

    #@b0
    const-string v9, "No valid addresses"

    #@b2
    invoke-static {v8, v9}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@b5
    goto/16 :goto_b

    #@b7
    .line 1339
    :cond_b7
    new-array v5, v6, [Ljava/lang/String;

    #@b9
    .line 1340
    .local v5, validAddresses:[Ljava/lang/String;
    const/4 v3, 0x0

    #@ba
    .line 1342
    .local v3, j:I
    const/4 v1, 0x0

    #@bb
    :goto_bb
    array-length v8, p1

    #@bc
    if-ge v1, v8, :cond_cd

    #@be
    if-ge v3, v6, :cond_cd

    #@c0
    .line 1343
    aget-boolean v8, v7, v1

    #@c2
    if-ne v8, v11, :cond_ca

    #@c4
    .line 1344
    aget-object v8, p1, v1

    #@c6
    aput-object v8, v5, v3

    #@c8
    .line 1345
    add-int/lit8 v3, v3, 0x1

    #@ca
    .line 1342
    :cond_ca
    add-int/lit8 v1, v1, 0x1

    #@cc
    goto :goto_bb

    #@cd
    .line 1349
    :cond_cd
    const-string v8, "DataConnectionManager"

    #@cf
    new-instance v9, Ljava/lang/StringBuilder;

    #@d1
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@d4
    const-string v10, "Count of valid addresses :: "

    #@d6
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v9

    #@da
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v9

    #@de
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e1
    move-result-object v9

    #@e2
    invoke-static {v8, v9}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@e5
    goto/16 :goto_b
.end method


# virtual methods
.method protected checkOperatorSpecificAvailability()Z
    .registers 8

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1062
    sget-object v5, Lcom/lge/ims/Configuration;->OPERATOR:Ljava/lang/String;

    #@3
    const-string v6, "LGU"

    #@5
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8
    move-result v5

    #@9
    if-eqz v5, :cond_93

    #@b
    .line 1063
    invoke-static {}, Lcom/lge/ims/PhoneStateTracker;->getInstance()Lcom/lge/ims/PhoneStateTracker;

    #@e
    move-result-object v1

    #@f
    .line 1065
    .local v1, pst:Lcom/lge/ims/PhoneStateTracker;
    if-eqz v1, :cond_2d

    #@11
    invoke-virtual {v1}, Lcom/lge/ims/PhoneStateTracker;->iseHRPD()Z

    #@14
    move-result v5

    #@15
    if-eqz v5, :cond_2d

    #@17
    .line 1066
    const-string v5, "true"

    #@19
    const-string v6, "ril.cdma.authlock"

    #@1b
    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@1e
    move-result-object v6

    #@1f
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22
    move-result v5

    #@23
    if-eqz v5, :cond_2d

    #@25
    .line 1067
    const-string v5, "DataConnectionManager"

    #@27
    const-string v6, "Data service is auth-locked, so do not request data connection"

    #@29
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@2c
    .line 1187
    .end local v1           #pst:Lcom/lge/ims/PhoneStateTracker;
    :cond_2c
    :goto_2c
    return v4

    #@2d
    .line 1072
    .restart local v1       #pst:Lcom/lge/ims/PhoneStateTracker;
    :cond_2d
    invoke-virtual {p0}, Lcom/lge/ims/DataConnectionManager;->isMultiplePdnSupported()Z

    #@30
    move-result v5

    #@31
    if-eqz v5, :cond_6e

    #@33
    .line 1074
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getTelephonyManager()Landroid/telephony/TelephonyManager;

    #@36
    move-result-object v3

    #@37
    .line 1076
    .local v3, tm:Landroid/telephony/TelephonyManager;
    if-eqz v3, :cond_70

    #@39
    .line 1077
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    #@3c
    move-result-object v0

    #@3d
    .line 1079
    .local v0, operator:Ljava/lang/String;
    const-string v5, "45006"

    #@3f
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@42
    move-result v5

    #@43
    if-nez v5, :cond_6e

    #@45
    .line 1080
    const-string v5, "DataConnectionManager"

    #@47
    const-string v6, "Network is not LGU+"

    #@49
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@4c
    .line 1083
    const-string v5, "gsm.sim.type"

    #@4e
    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@51
    move-result-object v2

    #@52
    .line 1085
    .local v2, simType:Ljava/lang/String;
    const-string v5, "lgu"

    #@54
    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@57
    move-result v5

    #@58
    if-nez v5, :cond_6e

    #@5a
    .line 1086
    const-string v5, "DataConnectionManager"

    #@5c
    const-string v6, "SIM type is not LGU+"

    #@5e
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@61
    .line 1088
    invoke-static {}, Lcom/lge/ims/Configuration;->isTestMode()Z

    #@64
    move-result v5

    #@65
    if-eqz v5, :cond_2c

    #@67
    .line 1089
    const-string v4, "DataConnectionManager"

    #@69
    const-string v5, "Testmode is enabled"

    #@6b
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@6e
    .line 1187
    .end local v0           #operator:Ljava/lang/String;
    .end local v1           #pst:Lcom/lge/ims/PhoneStateTracker;
    .end local v2           #simType:Ljava/lang/String;
    .end local v3           #tm:Landroid/telephony/TelephonyManager;
    :cond_6e
    :goto_6e
    const/4 v4, 0x1

    #@6f
    goto :goto_2c

    #@70
    .line 1096
    .restart local v1       #pst:Lcom/lge/ims/PhoneStateTracker;
    .restart local v3       #tm:Landroid/telephony/TelephonyManager;
    :cond_70
    const-string v5, "gsm.sim.type"

    #@72
    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@75
    move-result-object v2

    #@76
    .line 1098
    .restart local v2       #simType:Ljava/lang/String;
    const-string v5, "lgu"

    #@78
    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@7b
    move-result v5

    #@7c
    if-nez v5, :cond_6e

    #@7e
    .line 1099
    const-string v5, "DataConnectionManager"

    #@80
    const-string v6, "SIM type is not LGU+"

    #@82
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@85
    .line 1101
    invoke-static {}, Lcom/lge/ims/Configuration;->isTestMode()Z

    #@88
    move-result v5

    #@89
    if-eqz v5, :cond_2c

    #@8b
    .line 1102
    const-string v4, "DataConnectionManager"

    #@8d
    const-string v5, "Testmode is enabled"

    #@8f
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@92
    goto :goto_6e

    #@93
    .line 1109
    .end local v1           #pst:Lcom/lge/ims/PhoneStateTracker;
    .end local v2           #simType:Ljava/lang/String;
    .end local v3           #tm:Landroid/telephony/TelephonyManager;
    :cond_93
    sget-object v5, Lcom/lge/ims/Configuration;->OPERATOR:Ljava/lang/String;

    #@95
    const-string v6, "KT"

    #@97
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9a
    move-result v5

    #@9b
    if-eqz v5, :cond_103

    #@9d
    .line 1110
    invoke-virtual {p0}, Lcom/lge/ims/DataConnectionManager;->isMultiplePdnSupported()Z

    #@a0
    move-result v5

    #@a1
    if-eqz v5, :cond_6e

    #@a3
    .line 1112
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getTelephonyManager()Landroid/telephony/TelephonyManager;

    #@a6
    move-result-object v3

    #@a7
    .line 1114
    .restart local v3       #tm:Landroid/telephony/TelephonyManager;
    if-eqz v3, :cond_df

    #@a9
    .line 1115
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    #@ac
    move-result-object v0

    #@ad
    .line 1117
    .restart local v0       #operator:Ljava/lang/String;
    const-string v5, "45008"

    #@af
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b2
    move-result v5

    #@b3
    if-nez v5, :cond_6e

    #@b5
    .line 1118
    const-string v5, "DataConnectionManager"

    #@b7
    const-string v6, "Network is not KT"

    #@b9
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@bc
    .line 1121
    const-string v5, "gsm.sim.type"

    #@be
    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@c1
    move-result-object v2

    #@c2
    .line 1123
    .restart local v2       #simType:Ljava/lang/String;
    const-string v5, "kt"

    #@c4
    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@c7
    move-result v5

    #@c8
    if-nez v5, :cond_6e

    #@ca
    .line 1124
    const-string v5, "DataConnectionManager"

    #@cc
    const-string v6, "SIM type is not KT"

    #@ce
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@d1
    .line 1126
    invoke-static {}, Lcom/lge/ims/Configuration;->isTestMode()Z

    #@d4
    move-result v5

    #@d5
    if-eqz v5, :cond_2c

    #@d7
    .line 1127
    const-string v4, "DataConnectionManager"

    #@d9
    const-string v5, "Testmode is enabled"

    #@db
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@de
    goto :goto_6e

    #@df
    .line 1134
    .end local v0           #operator:Ljava/lang/String;
    .end local v2           #simType:Ljava/lang/String;
    :cond_df
    const-string v5, "gsm.sim.type"

    #@e1
    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@e4
    move-result-object v2

    #@e5
    .line 1136
    .restart local v2       #simType:Ljava/lang/String;
    const-string v5, "kt"

    #@e7
    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@ea
    move-result v5

    #@eb
    if-nez v5, :cond_6e

    #@ed
    .line 1137
    const-string v5, "DataConnectionManager"

    #@ef
    const-string v6, "SIM type is not KT"

    #@f1
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@f4
    .line 1139
    invoke-static {}, Lcom/lge/ims/Configuration;->isTestMode()Z

    #@f7
    move-result v5

    #@f8
    if-eqz v5, :cond_2c

    #@fa
    .line 1140
    const-string v4, "DataConnectionManager"

    #@fc
    const-string v5, "Testmode is enabled"

    #@fe
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@101
    goto/16 :goto_6e

    #@103
    .line 1147
    .end local v2           #simType:Ljava/lang/String;
    .end local v3           #tm:Landroid/telephony/TelephonyManager;
    :cond_103
    sget-object v5, Lcom/lge/ims/Configuration;->OPERATOR:Ljava/lang/String;

    #@105
    const-string v6, "SKT"

    #@107
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10a
    move-result v5

    #@10b
    if-eqz v5, :cond_6e

    #@10d
    .line 1148
    invoke-virtual {p0}, Lcom/lge/ims/DataConnectionManager;->isMultiplePdnSupported()Z

    #@110
    move-result v5

    #@111
    if-eqz v5, :cond_6e

    #@113
    .line 1150
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getTelephonyManager()Landroid/telephony/TelephonyManager;

    #@116
    move-result-object v3

    #@117
    .line 1152
    .restart local v3       #tm:Landroid/telephony/TelephonyManager;
    if-eqz v3, :cond_150

    #@119
    .line 1153
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    #@11c
    move-result-object v0

    #@11d
    .line 1155
    .restart local v0       #operator:Ljava/lang/String;
    const-string v5, "45005"

    #@11f
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@122
    move-result v5

    #@123
    if-nez v5, :cond_6e

    #@125
    .line 1156
    const-string v5, "DataConnectionManager"

    #@127
    const-string v6, "Network is not SKT"

    #@129
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@12c
    .line 1159
    const-string v5, "gsm.sim.type"

    #@12e
    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@131
    move-result-object v2

    #@132
    .line 1161
    .restart local v2       #simType:Ljava/lang/String;
    const-string v5, "skt"

    #@134
    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@137
    move-result v5

    #@138
    if-nez v5, :cond_6e

    #@13a
    .line 1162
    const-string v5, "DataConnectionManager"

    #@13c
    const-string v6, "SIM type is not SKT"

    #@13e
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@141
    .line 1164
    invoke-static {}, Lcom/lge/ims/Configuration;->isTestMode()Z

    #@144
    move-result v5

    #@145
    if-eqz v5, :cond_2c

    #@147
    .line 1165
    const-string v4, "DataConnectionManager"

    #@149
    const-string v5, "Testmode is enabled"

    #@14b
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@14e
    goto/16 :goto_6e

    #@150
    .line 1172
    .end local v0           #operator:Ljava/lang/String;
    .end local v2           #simType:Ljava/lang/String;
    :cond_150
    const-string v5, "gsm.sim.type"

    #@152
    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@155
    move-result-object v2

    #@156
    .line 1174
    .restart local v2       #simType:Ljava/lang/String;
    const-string v5, "skt"

    #@158
    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@15b
    move-result v5

    #@15c
    if-nez v5, :cond_6e

    #@15e
    .line 1175
    const-string v5, "DataConnectionManager"

    #@160
    const-string v6, "SIM type is not SKT"

    #@162
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@165
    .line 1177
    invoke-static {}, Lcom/lge/ims/Configuration;->isTestMode()Z

    #@168
    move-result v5

    #@169
    if-eqz v5, :cond_2c

    #@16b
    .line 1178
    const-string v4, "DataConnectionManager"

    #@16d
    const-string v5, "Testmode is enabled"

    #@16f
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@172
    goto/16 :goto_6e
.end method

.method protected checkRadioTechnology()Z
    .registers 6

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1191
    invoke-static {}, Lcom/lge/ims/PhoneStateTracker;->getInstance()Lcom/lge/ims/PhoneStateTracker;

    #@4
    move-result-object v0

    #@5
    .line 1193
    .local v0, pst:Lcom/lge/ims/PhoneStateTracker;
    if-eqz v0, :cond_54

    #@7
    .line 1194
    const-string v2, "DataConnectionManager"

    #@9
    new-instance v3, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v4, "RAT="

    #@10
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v0}, Lcom/lge/ims/PhoneStateTracker;->getNetworkType()I

    #@17
    move-result v4

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v3

    #@20
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@23
    .line 1196
    iget v2, p0, Lcom/lge/ims/DataConnectionManager;->mPolicy:I

    #@25
    and-int/lit16 v2, v2, 0x400

    #@27
    if-eqz v2, :cond_30

    #@29
    invoke-virtual {v0}, Lcom/lge/ims/PhoneStateTracker;->is4G()Z

    #@2c
    move-result v2

    #@2d
    if-eqz v2, :cond_30

    #@2f
    .line 1213
    :cond_2f
    :goto_2f
    return v1

    #@30
    .line 1200
    :cond_30
    iget v2, p0, Lcom/lge/ims/DataConnectionManager;->mPolicy:I

    #@32
    and-int/lit16 v2, v2, 0x200

    #@34
    if-eqz v2, :cond_3c

    #@36
    invoke-virtual {v0}, Lcom/lge/ims/PhoneStateTracker;->is3G()Z

    #@39
    move-result v2

    #@3a
    if-nez v2, :cond_2f

    #@3c
    .line 1204
    :cond_3c
    iget v2, p0, Lcom/lge/ims/DataConnectionManager;->mPolicy:I

    #@3e
    and-int/lit16 v2, v2, 0x100

    #@40
    if-eqz v2, :cond_48

    #@42
    invoke-virtual {v0}, Lcom/lge/ims/PhoneStateTracker;->iseHRPD()Z

    #@45
    move-result v2

    #@46
    if-nez v2, :cond_2f

    #@48
    .line 1208
    :cond_48
    iget v2, p0, Lcom/lge/ims/DataConnectionManager;->mPolicy:I

    #@4a
    and-int/lit16 v2, v2, 0x800

    #@4c
    if-eqz v2, :cond_54

    #@4e
    invoke-virtual {v0}, Lcom/lge/ims/PhoneStateTracker;->is2G()Z

    #@51
    move-result v2

    #@52
    if-nez v2, :cond_2f

    #@54
    .line 1213
    :cond_54
    const/4 v1, 0x0

    #@55
    goto :goto_2f
.end method

.method protected checkRoaming()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1217
    iget v2, p0, Lcom/lge/ims/DataConnectionManager;->mPolicy:I

    #@3
    and-int/lit8 v2, v2, 0x40

    #@5
    if-eqz v2, :cond_f

    #@7
    .line 1218
    const-string v2, "DataConnectionManager"

    #@9
    const-string v3, "Roaming is allowed"

    #@b
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@e
    .line 1231
    :cond_e
    :goto_e
    return v1

    #@f
    .line 1222
    :cond_f
    invoke-static {}, Lcom/lge/ims/PhoneStateTracker;->getInstance()Lcom/lge/ims/PhoneStateTracker;

    #@12
    move-result-object v0

    #@13
    .line 1224
    .local v0, pst:Lcom/lge/ims/PhoneStateTracker;
    if-eqz v0, :cond_e

    #@15
    .line 1225
    invoke-virtual {v0}, Lcom/lge/ims/PhoneStateTracker;->isRoaming()Z

    #@18
    move-result v2

    #@19
    if-eqz v2, :cond_e

    #@1b
    .line 1226
    const-string v1, "DataConnectionManager"

    #@1d
    const-string v2, "Network is roaming"

    #@1f
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@22
    .line 1227
    const/4 v1, 0x0

    #@23
    goto :goto_e
.end method

.method public connect(I)Z
    .registers 6
    .parameter "apnType"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 228
    const-string v1, "DataConnectionManager"

    #@3
    new-instance v2, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v3, "connect - apnType="

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@19
    .line 230
    invoke-static {}, Lcom/lge/ims/Configuration;->isImsOn()Z

    #@1c
    move-result v1

    #@1d
    if-nez v1, :cond_27

    #@1f
    .line 231
    const-string v1, "DataConnectionManager"

    #@21
    const-string v2, "IMS is disabled; so do not request data connection"

    #@23
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@26
    .line 267
    :goto_26
    return v0

    #@27
    .line 235
    :cond_27
    invoke-virtual {p0}, Lcom/lge/ims/DataConnectionManager;->isImsServiceStarted()Z

    #@2a
    move-result v1

    #@2b
    if-nez v1, :cond_35

    #@2d
    .line 236
    const-string v1, "DataConnectionManager"

    #@2f
    const-string v2, "Do not request data connection; waits for the service readiness ..."

    #@31
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@34
    goto :goto_26

    #@35
    .line 240
    :cond_35
    if-ne p1, v0, :cond_67

    #@37
    .line 241
    invoke-virtual {p0}, Lcom/lge/ims/DataConnectionManager;->isMultipleApnSupported()Z

    #@3a
    move-result v1

    #@3b
    if-eqz v1, :cond_54

    #@3d
    .line 242
    invoke-static {v0}, Lcom/lge/ims/DataConnectionManager;->getApnName(I)Ljava/lang/String;

    #@40
    move-result-object v1

    #@41
    invoke-static {v1}, Lcom/lge/ims/Configuration;->isApnSupported(Ljava/lang/String;)Z

    #@44
    move-result v1

    #@45
    if-nez v1, :cond_4f

    #@47
    .line 243
    const-string v1, "DataConnectionManager"

    #@49
    const-string v2, "APN (mobile_ims) is not supported"

    #@4b
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@4e
    goto :goto_26

    #@4f
    .line 247
    :cond_4f
    invoke-virtual {p0}, Lcom/lge/ims/DataConnectionManager;->connectImsApn()Z

    #@52
    move-result v0

    #@53
    goto :goto_26

    #@54
    .line 249
    :cond_54
    invoke-virtual {p0}, Lcom/lge/ims/DataConnectionManager;->isInternetApnRequestRequired()Z

    #@57
    move-result v1

    #@58
    if-nez v1, :cond_62

    #@5a
    .line 250
    const-string v1, "DataConnectionManager"

    #@5c
    const-string v2, "No need to request data connection"

    #@5e
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@61
    goto :goto_26

    #@62
    .line 254
    :cond_62
    invoke-virtual {p0}, Lcom/lge/ims/DataConnectionManager;->connectInternetApn()Z

    #@65
    move-result v0

    #@66
    goto :goto_26

    #@67
    .line 256
    :cond_67
    const/4 v1, 0x2

    #@68
    if-ne p1, v1, :cond_7d

    #@6a
    .line 257
    invoke-virtual {p0}, Lcom/lge/ims/DataConnectionManager;->isInternetApnRequestRequired()Z

    #@6d
    move-result v1

    #@6e
    if-nez v1, :cond_78

    #@70
    .line 258
    const-string v1, "DataConnectionManager"

    #@72
    const-string v2, "No need to request data connection"

    #@74
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@77
    goto :goto_26

    #@78
    .line 262
    :cond_78
    invoke-virtual {p0}, Lcom/lge/ims/DataConnectionManager;->connectInternetApn()Z

    #@7b
    move-result v0

    #@7c
    goto :goto_26

    #@7d
    .line 263
    :cond_7d
    const/16 v0, 0x9

    #@7f
    if-ne p1, v0, :cond_86

    #@81
    .line 264
    invoke-virtual {p0}, Lcom/lge/ims/DataConnectionManager;->connectEmergencyApn()Z

    #@84
    move-result v0

    #@85
    goto :goto_26

    #@86
    .line 267
    :cond_86
    const/4 v0, 0x0

    #@87
    goto :goto_26
.end method

.method protected connectEmergencyApn()Z
    .registers 11

    #@0
    .prologue
    const/4 v9, 0x3

    #@1
    const/4 v8, 0x2

    #@2
    const/4 v4, 0x0

    #@3
    const/4 v3, 0x1

    #@4
    .line 812
    iget v5, p0, Lcom/lge/ims/DataConnectionManager;->mEmergencyApnRequestState:I

    #@6
    if-ne v5, v8, :cond_10

    #@8
    .line 813
    const-string v4, "DataConnectionManager"

    #@a
    const-string v5, "Data connection(mobile_emergency) request is already done"

    #@c
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@f
    .line 858
    :cond_f
    :goto_f
    return v3

    #@10
    .line 817
    :cond_10
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getConnectivityManager()Landroid/net/ConnectivityManager;

    #@13
    move-result-object v0

    #@14
    .line 819
    .local v0, cm:Landroid/net/ConnectivityManager;
    if-nez v0, :cond_1f

    #@16
    .line 820
    const-string v3, "DataConnectionManager"

    #@18
    const-string v5, "ConnectivityManager is null"

    #@1a
    invoke-static {v3, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@1d
    move v3, v4

    #@1e
    .line 821
    goto :goto_f

    #@1f
    .line 824
    :cond_1f
    const-string v5, "enableEMERGENCY"

    #@21
    invoke-virtual {v0, v4, v5}, Landroid/net/ConnectivityManager;->startUsingNetworkFeature(ILjava/lang/String;)I

    #@24
    move-result v2

    #@25
    .line 826
    .local v2, result:I
    const-string v5, "DataConnectionManager"

    #@27
    new-instance v6, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v7, "startUsingNetworkFeature :: result="

    #@2e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v6

    #@32
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35
    move-result-object v6

    #@36
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v6

    #@3a
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@3d
    .line 828
    if-eqz v2, :cond_41

    #@3f
    if-ne v2, v3, :cond_4d

    #@41
    .line 829
    :cond_41
    iput v8, p0, Lcom/lge/ims/DataConnectionManager;->mEmergencyApnRequestState:I

    #@43
    .line 830
    iput v4, p0, Lcom/lge/ims/DataConnectionManager;->mEmergencyApnRequestRetries:I

    #@45
    .line 832
    if-nez v2, :cond_f

    #@47
    .line 833
    const/16 v4, 0x9

    #@49
    invoke-virtual {p0, v4}, Lcom/lge/ims/DataConnectionManager;->getDataStateEx(I)I

    #@4c
    goto :goto_f

    #@4d
    .line 836
    :cond_4d
    iget v4, p0, Lcom/lge/ims/DataConnectionManager;->mEmergencyApnRequestState:I

    #@4f
    if-eq v4, v3, :cond_92

    #@51
    .line 837
    iget v4, p0, Lcom/lge/ims/DataConnectionManager;->mEmergencyApnRequestRetries:I

    #@53
    add-int/lit8 v4, v4, 0x1

    #@55
    iput v4, p0, Lcom/lge/ims/DataConnectionManager;->mEmergencyApnRequestRetries:I

    #@57
    .line 839
    const-string v4, "DataConnectionManager"

    #@59
    new-instance v5, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v6, "Data connection(mobile_emergency) request :: APN_REQUEST_PENDING - "

    #@60
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v5

    #@64
    iget v6, p0, Lcom/lge/ims/DataConnectionManager;->mEmergencyApnRequestRetries:I

    #@66
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@69
    move-result-object v5

    #@6a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d
    move-result-object v5

    #@6e
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@71
    .line 841
    iget v4, p0, Lcom/lge/ims/DataConnectionManager;->mEmergencyApnRequestRetries:I

    #@73
    if-gt v4, v9, :cond_7e

    #@75
    .line 842
    iget-object v4, p0, Lcom/lge/ims/DataConnectionManager;->mDataConnectionHandler:Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;

    #@77
    const/16 v5, 0x7d3

    #@79
    const-wide/16 v6, 0x3e8

    #@7b
    invoke-virtual {v4, v5, v6, v7}, Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;->sendEmptyMessageDelayed(IJ)Z

    #@7e
    .line 846
    :cond_7e
    iput v3, p0, Lcom/lge/ims/DataConnectionManager;->mEmergencyApnRequestState:I

    #@80
    .line 852
    :goto_80
    iget v4, p0, Lcom/lge/ims/DataConnectionManager;->mEmergencyApnRequestRetries:I

    #@82
    if-le v4, v9, :cond_f

    #@84
    .line 853
    const v1, 0x90002

    #@87
    .line 854
    .local v1, connectionResult:I
    iget-object v4, p0, Lcom/lge/ims/DataConnectionManager;->mDataStateChangedRegistrants:Landroid/os/RegistrantList;

    #@89
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8c
    move-result-object v5

    #@8d
    invoke-virtual {v4, v5}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@90
    goto/16 :goto_f

    #@92
    .line 848
    .end local v1           #connectionResult:I
    :cond_92
    const-string v4, "DataConnectionManager"

    #@94
    const-string v5, "Data connection(mobile_emergency) request :: already APN_REQUEST_PENDING"

    #@96
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@99
    goto :goto_80
.end method

.method protected connectImsApn()Z
    .registers 10

    #@0
    .prologue
    const/16 v8, 0x7d2

    #@2
    const/4 v7, 0x2

    #@3
    const/4 v2, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 883
    iget v4, p0, Lcom/lge/ims/DataConnectionManager;->mImsApnRequestState:I

    #@7
    if-ne v4, v7, :cond_11

    #@9
    .line 884
    const-string v3, "DataConnectionManager"

    #@b
    const-string v4, "Data connection(mobile_ims) request is already done"

    #@d
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@10
    .line 984
    :cond_10
    :goto_10
    return v2

    #@11
    .line 888
    :cond_11
    invoke-virtual {p0}, Lcom/lge/ims/DataConnectionManager;->isImsApnBlockRequired()Z

    #@14
    move-result v4

    #@15
    if-eqz v4, :cond_24

    #@17
    .line 889
    iget v4, p0, Lcom/lge/ims/DataConnectionManager;->mImsApnRequestState:I

    #@19
    const/4 v5, 0x3

    #@1a
    if-ne v4, v5, :cond_24

    #@1c
    .line 890
    const-string v3, "DataConnectionManager"

    #@1e
    const-string v4, "APN is blocked; try to connect when the blocking is released"

    #@20
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@23
    goto :goto_10

    #@24
    .line 895
    :cond_24
    invoke-virtual {p0}, Lcom/lge/ims/DataConnectionManager;->isDataEmergencyOnly()Z

    #@27
    move-result v4

    #@28
    if-eqz v4, :cond_32

    #@2a
    .line 896
    const-string v3, "DataConnectionManager"

    #@2c
    const-string v4, "APN is blocked; LTE only supports the emergency service"

    #@2e
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@31
    goto :goto_10

    #@32
    .line 900
    :cond_32
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getConnectivityManager()Landroid/net/ConnectivityManager;

    #@35
    move-result-object v0

    #@36
    .line 902
    .local v0, cm:Landroid/net/ConnectivityManager;
    if-nez v0, :cond_41

    #@38
    .line 903
    const-string v2, "DataConnectionManager"

    #@3a
    const-string v4, "ConnectivityManager is null"

    #@3c
    invoke-static {v2, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@3f
    move v2, v3

    #@40
    .line 904
    goto :goto_10

    #@41
    .line 907
    :cond_41
    invoke-virtual {p0}, Lcom/lge/ims/DataConnectionManager;->isSingleAndImsApnSupported()Z

    #@44
    move-result v4

    #@45
    if-eqz v4, :cond_56

    #@47
    .line 908
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getMobileDataEnabled()Z

    #@4a
    move-result v4

    #@4b
    if-nez v4, :cond_56

    #@4d
    .line 909
    const-string v2, "DataConnectionManager"

    #@4f
    const-string v4, "Mobile data setting is disabled"

    #@51
    invoke-static {v2, v4}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@54
    move v2, v3

    #@55
    .line 910
    goto :goto_10

    #@56
    .line 914
    :cond_56
    invoke-virtual {p0}, Lcom/lge/ims/DataConnectionManager;->checkRoaming()Z

    #@59
    move-result v4

    #@5a
    if-nez v4, :cond_65

    #@5c
    .line 915
    const-string v2, "DataConnectionManager"

    #@5e
    const-string v4, "Roaming is not allowed"

    #@60
    invoke-static {v2, v4}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@63
    move v2, v3

    #@64
    .line 916
    goto :goto_10

    #@65
    .line 919
    :cond_65
    invoke-virtual {p0}, Lcom/lge/ims/DataConnectionManager;->checkRadioTechnology()Z

    #@68
    move-result v4

    #@69
    if-nez v4, :cond_74

    #@6b
    .line 920
    const-string v2, "DataConnectionManager"

    #@6d
    const-string v4, "RAT is not valid"

    #@6f
    invoke-static {v2, v4}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@72
    move v2, v3

    #@73
    .line 921
    goto :goto_10

    #@74
    .line 924
    :cond_74
    invoke-virtual {p0}, Lcom/lge/ims/DataConnectionManager;->isAirplaneMode()Z

    #@77
    move-result v4

    #@78
    if-eqz v4, :cond_83

    #@7a
    .line 925
    const-string v2, "DataConnectionManager"

    #@7c
    const-string v4, "Airplane mode is ON"

    #@7e
    invoke-static {v2, v4}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@81
    move v2, v3

    #@82
    .line 926
    goto :goto_10

    #@83
    .line 929
    :cond_83
    invoke-virtual {p0}, Lcom/lge/ims/DataConnectionManager;->isSingleAndImsApnSupported()Z

    #@86
    move-result v4

    #@87
    if-eqz v4, :cond_9e

    #@89
    .line 930
    invoke-virtual {p0}, Lcom/lge/ims/DataConnectionManager;->isWiFiConnected()Z

    #@8c
    move-result v4

    #@8d
    if-eqz v4, :cond_d7

    #@8f
    .line 931
    const-string v4, "DataConnectionManager"

    #@91
    const-string v5, "Data state :: WiFi is connected"

    #@93
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@96
    .line 933
    iget v4, p0, Lcom/lge/ims/DataConnectionManager;->mImsDataState:I

    #@98
    if-nez v4, :cond_ae

    #@9a
    iget v4, p0, Lcom/lge/ims/DataConnectionManager;->mDefaultDataState:I

    #@9c
    if-nez v4, :cond_ae

    #@9e
    .line 950
    :cond_9e
    invoke-virtual {p0}, Lcom/lge/ims/DataConnectionManager;->checkOperatorSpecificAvailability()Z

    #@a1
    move-result v4

    #@a2
    if-nez v4, :cond_ec

    #@a4
    .line 951
    const-string v2, "DataConnectionManager"

    #@a6
    const-string v4, "Operator specific availability is failed"

    #@a8
    invoke-static {v2, v4}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@ab
    move v2, v3

    #@ac
    .line 952
    goto/16 :goto_10

    #@ae
    .line 937
    :cond_ae
    const-string v2, "DataConnectionManager"

    #@b0
    new-instance v4, Ljava/lang/StringBuilder;

    #@b2
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@b5
    const-string v5, "Data state :: mobile_ims="

    #@b7
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v4

    #@bb
    iget v5, p0, Lcom/lge/ims/DataConnectionManager;->mImsDataState:I

    #@bd
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v4

    #@c1
    const-string v5, ", mobile="

    #@c3
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v4

    #@c7
    iget v5, p0, Lcom/lge/ims/DataConnectionManager;->mDefaultDataState:I

    #@c9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@cc
    move-result-object v4

    #@cd
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d0
    move-result-object v4

    #@d1
    invoke-static {v2, v4}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@d4
    move v2, v3

    #@d5
    .line 938
    goto/16 :goto_10

    #@d7
    .line 941
    :cond_d7
    const-string v4, "DataConnectionManager"

    #@d9
    const-string v5, "Data state :: WiFi is not connected"

    #@db
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@de
    .line 943
    iget v4, p0, Lcom/lge/ims/DataConnectionManager;->mDefaultDataState:I

    #@e0
    if-nez v4, :cond_9e

    #@e2
    .line 944
    const-string v2, "DataConnectionManager"

    #@e4
    const-string v4, "Data state :: mobile is not connected"

    #@e6
    invoke-static {v2, v4}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@e9
    move v2, v3

    #@ea
    .line 945
    goto/16 :goto_10

    #@ec
    .line 955
    :cond_ec
    const-string v4, "enableIMS"

    #@ee
    invoke-virtual {v0, v3, v4}, Landroid/net/ConnectivityManager;->startUsingNetworkFeature(ILjava/lang/String;)I

    #@f1
    move-result v1

    #@f2
    .line 957
    .local v1, result:I
    const-string v4, "DataConnectionManager"

    #@f4
    new-instance v5, Ljava/lang/StringBuilder;

    #@f6
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@f9
    const-string v6, "startUsingNetworkFeature :: result="

    #@fb
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fe
    move-result-object v5

    #@ff
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@102
    move-result-object v5

    #@103
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@106
    move-result-object v5

    #@107
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@10a
    .line 959
    if-eqz v1, :cond_10e

    #@10c
    if-ne v1, v2, :cond_119

    #@10e
    .line 960
    :cond_10e
    iput v7, p0, Lcom/lge/ims/DataConnectionManager;->mImsApnRequestState:I

    #@110
    .line 961
    iput v3, p0, Lcom/lge/ims/DataConnectionManager;->mImsApnRequestRetries:I

    #@112
    .line 963
    if-nez v1, :cond_10

    #@114
    .line 964
    invoke-virtual {p0, v2}, Lcom/lge/ims/DataConnectionManager;->getDataStateEx(I)I

    #@117
    goto/16 :goto_10

    #@119
    .line 967
    :cond_119
    iget v3, p0, Lcom/lge/ims/DataConnectionManager;->mImsApnRequestState:I

    #@11b
    if-eq v3, v2, :cond_155

    #@11d
    .line 968
    iget v3, p0, Lcom/lge/ims/DataConnectionManager;->mImsApnRequestRetries:I

    #@11f
    add-int/lit8 v3, v3, 0x1

    #@121
    iput v3, p0, Lcom/lge/ims/DataConnectionManager;->mImsApnRequestRetries:I

    #@123
    .line 970
    const-string v3, "DataConnectionManager"

    #@125
    new-instance v4, Ljava/lang/StringBuilder;

    #@127
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@12a
    const-string v5, "Data connection(mobile_ims) request :: APN_REQUEST_PENDING - "

    #@12c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12f
    move-result-object v4

    #@130
    iget v5, p0, Lcom/lge/ims/DataConnectionManager;->mImsApnRequestRetries:I

    #@132
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@135
    move-result-object v4

    #@136
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@139
    move-result-object v4

    #@13a
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@13d
    .line 972
    iget v3, p0, Lcom/lge/ims/DataConnectionManager;->mImsApnRequestRetries:I

    #@13f
    const/4 v4, 0x5

    #@140
    if-ge v3, v4, :cond_14d

    #@142
    .line 973
    iget-object v3, p0, Lcom/lge/ims/DataConnectionManager;->mDataConnectionHandler:Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;

    #@144
    const-wide/16 v4, 0x7d0

    #@146
    invoke-virtual {v3, v8, v4, v5}, Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;->sendEmptyMessageDelayed(IJ)Z

    #@149
    .line 978
    :goto_149
    iput v2, p0, Lcom/lge/ims/DataConnectionManager;->mImsApnRequestState:I

    #@14b
    goto/16 :goto_10

    #@14d
    .line 975
    :cond_14d
    iget-object v3, p0, Lcom/lge/ims/DataConnectionManager;->mDataConnectionHandler:Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;

    #@14f
    const-wide/16 v4, 0x2710

    #@151
    invoke-virtual {v3, v8, v4, v5}, Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;->sendEmptyMessageDelayed(IJ)Z

    #@154
    goto :goto_149

    #@155
    .line 980
    :cond_155
    const-string v3, "DataConnectionManager"

    #@157
    const-string v4, "Data connection(mobile_ims) request :: already APN_REQUEST_PENDING"

    #@159
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@15c
    goto/16 :goto_10
.end method

.method protected connectInternetApn()Z
    .registers 2

    #@0
    .prologue
    .line 1017
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public create(Landroid/content/Context;)V
    .registers 9
    .parameter "context"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 132
    const-string v3, "ro.afwdata.LGfeatureset"

    #@3
    const-string v4, "none"

    #@5
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@8
    move-result-object v3

    #@9
    invoke-static {p1, v3}, Lcom/android/internal/telephony/LGfeature;->getInstance(Landroid/content/Context;Ljava/lang/String;)Lcom/android/internal/telephony/LGfeature;

    #@c
    move-result-object v3

    #@d
    iput-object v3, p0, Lcom/lge/ims/DataConnectionManager;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@f
    .line 134
    const-string v3, "DataConnectionManager"

    #@11
    new-instance v4, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v5, "Data feature :: "

    #@18
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v4

    #@1c
    iget-object v5, p0, Lcom/lge/ims/DataConnectionManager;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@1e
    invoke-virtual {v5}, Lcom/android/internal/telephony/LGfeature;->toString()Ljava/lang/String;

    #@21
    move-result-object v5

    #@22
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v4

    #@26
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v4

    #@2a
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@2d
    .line 136
    const/4 v3, 0x0

    #@2e
    iput v3, p0, Lcom/lge/ims/DataConnectionManager;->mPolicy:I

    #@30
    .line 138
    iget-object v3, p0, Lcom/lge/ims/DataConnectionManager;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@32
    iget v3, v3, Lcom/android/internal/telephony/LGfeature;->MPDNset:I

    #@34
    if-eqz v3, :cond_3c

    #@36
    .line 139
    iget v3, p0, Lcom/lge/ims/DataConnectionManager;->mPolicy:I

    #@38
    or-int/lit8 v3, v3, 0x2

    #@3a
    iput v3, p0, Lcom/lge/ims/DataConnectionManager;->mPolicy:I

    #@3c
    .line 142
    :cond_3c
    iget v3, p0, Lcom/lge/ims/DataConnectionManager;->mPolicy:I

    #@3e
    and-int/lit8 v3, v3, 0x2

    #@40
    if-nez v3, :cond_52

    #@42
    .line 143
    sget-object v3, Lcom/lge/ims/Configuration;->OPERATOR:Ljava/lang/String;

    #@44
    const-string v4, "LGU"

    #@46
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@49
    move-result v3

    #@4a
    if-eqz v3, :cond_10c

    #@4c
    .line 144
    iget v3, p0, Lcom/lge/ims/DataConnectionManager;->mPolicy:I

    #@4e
    or-int/lit8 v3, v3, 0x4

    #@50
    iput v3, p0, Lcom/lge/ims/DataConnectionManager;->mPolicy:I

    #@52
    .line 154
    :cond_52
    :goto_52
    const/4 v3, 0x1

    #@53
    invoke-static {v3}, Lcom/lge/ims/DataConnectionManager;->getApnName(I)Ljava/lang/String;

    #@56
    move-result-object v3

    #@57
    invoke-static {v3}, Lcom/lge/ims/Configuration;->getAccessPolicy(Ljava/lang/String;)I

    #@5a
    move-result v0

    #@5b
    .line 156
    .local v0, accessPolicy:I
    if-nez v0, :cond_6a

    #@5d
    .line 157
    const/4 v3, 0x2

    #@5e
    invoke-static {v3}, Lcom/lge/ims/DataConnectionManager;->getApnName(I)Ljava/lang/String;

    #@61
    move-result-object v3

    #@62
    invoke-static {v3}, Lcom/lge/ims/Configuration;->getAccessPolicy(Ljava/lang/String;)I

    #@65
    move-result v0

    #@66
    .line 159
    if-nez v0, :cond_6a

    #@68
    .line 160
    const/high16 v0, 0x2020

    #@6a
    .line 164
    :cond_6a
    const/high16 v3, 0x2000

    #@6c
    and-int/2addr v3, v0

    #@6d
    if-eqz v3, :cond_75

    #@6f
    .line 165
    iget v3, p0, Lcom/lge/ims/DataConnectionManager;->mPolicy:I

    #@71
    or-int/lit16 v3, v3, 0x400

    #@73
    iput v3, p0, Lcom/lge/ims/DataConnectionManager;->mPolicy:I

    #@75
    .line 168
    :cond_75
    const/high16 v3, 0x20

    #@77
    and-int/2addr v3, v0

    #@78
    if-eqz v3, :cond_80

    #@7a
    .line 169
    iget v3, p0, Lcom/lge/ims/DataConnectionManager;->mPolicy:I

    #@7c
    or-int/lit16 v3, v3, 0x200

    #@7e
    iput v3, p0, Lcom/lge/ims/DataConnectionManager;->mPolicy:I

    #@80
    .line 172
    :cond_80
    const/high16 v3, 0x1000

    #@82
    and-int/2addr v3, v0

    #@83
    if-eqz v3, :cond_8b

    #@85
    .line 173
    iget v3, p0, Lcom/lge/ims/DataConnectionManager;->mPolicy:I

    #@87
    or-int/lit16 v3, v3, 0x100

    #@89
    iput v3, p0, Lcom/lge/ims/DataConnectionManager;->mPolicy:I

    #@8b
    .line 176
    :cond_8b
    const/high16 v3, 0x8

    #@8d
    and-int/2addr v3, v0

    #@8e
    if-eqz v3, :cond_96

    #@90
    .line 177
    iget v3, p0, Lcom/lge/ims/DataConnectionManager;->mPolicy:I

    #@92
    or-int/lit16 v3, v3, 0x800

    #@94
    iput v3, p0, Lcom/lge/ims/DataConnectionManager;->mPolicy:I

    #@96
    .line 181
    :cond_96
    sget-object v3, Lcom/lge/ims/Configuration;->OPERATOR:Ljava/lang/String;

    #@98
    const-string v4, "VZW"

    #@9a
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9d
    move-result v3

    #@9e
    if-eqz v3, :cond_a6

    #@a0
    .line 182
    iget v3, p0, Lcom/lge/ims/DataConnectionManager;->mPolicy:I

    #@a2
    or-int/lit8 v3, v3, 0x40

    #@a4
    iput v3, p0, Lcom/lge/ims/DataConnectionManager;->mPolicy:I

    #@a6
    .line 185
    :cond_a6
    const-string v3, "DataConnectionManager"

    #@a8
    new-instance v4, Ljava/lang/StringBuilder;

    #@aa
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@ad
    const-string v5, "Policy=0x"

    #@af
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b2
    move-result-object v4

    #@b3
    iget v5, p0, Lcom/lge/ims/DataConnectionManager;->mPolicy:I

    #@b5
    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@b8
    move-result-object v5

    #@b9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v4

    #@bd
    const-string v5, ", MPDN="

    #@bf
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v4

    #@c3
    invoke-virtual {p0}, Lcom/lge/ims/DataConnectionManager;->isMultiplePdnSupported()Z

    #@c6
    move-result v5

    #@c7
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v4

    #@cb
    const-string v5, ", MAPN="

    #@cd
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d0
    move-result-object v4

    #@d1
    invoke-virtual {p0}, Lcom/lge/ims/DataConnectionManager;->isMultipleApnSupported()Z

    #@d4
    move-result v5

    #@d5
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v4

    #@d9
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@dc
    move-result-object v4

    #@dd
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@e0
    .line 189
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getInstance()Lcom/lge/ims/SystemServiceManager;

    #@e3
    move-result-object v2

    #@e4
    .line 191
    .local v2, ssm:Lcom/lge/ims/SystemServiceManager;
    if-eqz v2, :cond_ed

    #@e6
    .line 192
    iget-object v3, p0, Lcom/lge/ims/DataConnectionManager;->mDataConnectionHandler:Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;

    #@e8
    const/16 v4, 0x3e9

    #@ea
    invoke-virtual {v2, v3, v4, v6}, Lcom/lge/ims/SystemServiceManager;->registerForSystemServiceRestarted(Landroid/os/Handler;ILjava/lang/Object;)V

    #@ed
    .line 195
    :cond_ed
    invoke-static {}, Lcom/lge/ims/PhoneStateTracker;->getInstance()Lcom/lge/ims/PhoneStateTracker;

    #@f0
    move-result-object v1

    #@f1
    .line 197
    .local v1, pst:Lcom/lge/ims/PhoneStateTracker;
    if-eqz v1, :cond_10b

    #@f3
    .line 199
    invoke-virtual {v1, v0}, Lcom/lge/ims/PhoneStateTracker;->setAccessPolicy(I)V

    #@f6
    .line 201
    iget-object v3, p0, Lcom/lge/ims/DataConnectionManager;->mDataConnectionHandler:Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;

    #@f8
    const/16 v4, 0x3ea

    #@fa
    invoke-virtual {v1, v3, v4, v6}, Lcom/lge/ims/PhoneStateTracker;->registerForDataServiceStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@fd
    .line 202
    iget-object v3, p0, Lcom/lge/ims/DataConnectionManager;->mDataConnectionHandler:Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;

    #@ff
    const/16 v4, 0x3eb

    #@101
    invoke-virtual {v1, v3, v4, v6}, Lcom/lge/ims/PhoneStateTracker;->registerForRATChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@104
    .line 203
    iget-object v3, p0, Lcom/lge/ims/DataConnectionManager;->mDataConnectionHandler:Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;

    #@106
    const/16 v4, 0x3ec

    #@108
    invoke-virtual {v1, v3, v4, v6}, Lcom/lge/ims/PhoneStateTracker;->registerForRoamingStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@10b
    .line 205
    :cond_10b
    return-void

    #@10c
    .line 146
    .end local v0           #accessPolicy:I
    .end local v1           #pst:Lcom/lge/ims/PhoneStateTracker;
    .end local v2           #ssm:Lcom/lge/ims/SystemServiceManager;
    :cond_10c
    iget v3, p0, Lcom/lge/ims/DataConnectionManager;->mPolicy:I

    #@10e
    or-int/lit8 v3, v3, 0x1

    #@110
    iput v3, p0, Lcom/lge/ims/DataConnectionManager;->mPolicy:I

    #@112
    goto/16 :goto_52
.end method

.method public destroy()V
    .registers 5

    #@0
    .prologue
    .line 208
    iget-object v2, p0, Lcom/lge/ims/DataConnectionManager;->mContext:Landroid/content/Context;

    #@2
    if-eqz v2, :cond_b

    #@4
    .line 209
    iget-object v2, p0, Lcom/lge/ims/DataConnectionManager;->mContext:Landroid/content/Context;

    #@6
    iget-object v3, p0, Lcom/lge/ims/DataConnectionManager;->mDataConnectionReceiver:Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;

    #@8
    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@b
    .line 212
    :cond_b
    invoke-static {}, Lcom/lge/ims/PhoneStateTracker;->getInstance()Lcom/lge/ims/PhoneStateTracker;

    #@e
    move-result-object v0

    #@f
    .line 214
    .local v0, pst:Lcom/lge/ims/PhoneStateTracker;
    if-eqz v0, :cond_20

    #@11
    .line 215
    iget-object v2, p0, Lcom/lge/ims/DataConnectionManager;->mDataConnectionHandler:Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;

    #@13
    invoke-virtual {v0, v2}, Lcom/lge/ims/PhoneStateTracker;->unregisterForDataServiceStateChanged(Landroid/os/Handler;)V

    #@16
    .line 216
    iget-object v2, p0, Lcom/lge/ims/DataConnectionManager;->mDataConnectionHandler:Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;

    #@18
    invoke-virtual {v0, v2}, Lcom/lge/ims/PhoneStateTracker;->unregisterForRATChanged(Landroid/os/Handler;)V

    #@1b
    .line 217
    iget-object v2, p0, Lcom/lge/ims/DataConnectionManager;->mDataConnectionHandler:Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;

    #@1d
    invoke-virtual {v0, v2}, Lcom/lge/ims/PhoneStateTracker;->unregisterForRoamingStateChanged(Landroid/os/Handler;)V

    #@20
    .line 220
    :cond_20
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getInstance()Lcom/lge/ims/SystemServiceManager;

    #@23
    move-result-object v1

    #@24
    .line 222
    .local v1, ssm:Lcom/lge/ims/SystemServiceManager;
    if-eqz v1, :cond_2b

    #@26
    .line 223
    iget-object v2, p0, Lcom/lge/ims/DataConnectionManager;->mDataConnectionHandler:Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;

    #@28
    invoke-virtual {v1, v2}, Lcom/lge/ims/SystemServiceManager;->unregisterForSystemServiceRestarted(Landroid/os/Handler;)V

    #@2b
    .line 225
    :cond_2b
    return-void
.end method

.method public disconnect(II)V
    .registers 6
    .parameter "apnType"
    .parameter "intervalForApnBlock"

    #@0
    .prologue
    .line 272
    const-string v0, "DataConnectionManager"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "disconnect - apnType="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 274
    invoke-static {}, Lcom/lge/ims/Configuration;->isImsOn()Z

    #@1b
    move-result v0

    #@1c
    if-nez v0, :cond_1f

    #@1e
    .line 297
    :cond_1e
    :goto_1e
    return-void

    #@1f
    .line 278
    :cond_1f
    const/4 v0, 0x1

    #@20
    if-ne p1, v0, :cond_36

    #@22
    .line 279
    invoke-virtual {p0}, Lcom/lge/ims/DataConnectionManager;->isMultipleApnSupported()Z

    #@25
    move-result v0

    #@26
    if-eqz v0, :cond_2c

    #@28
    .line 280
    invoke-virtual {p0, p2}, Lcom/lge/ims/DataConnectionManager;->disconnectImsApn(I)V

    #@2b
    goto :goto_1e

    #@2c
    .line 282
    :cond_2c
    invoke-virtual {p0}, Lcom/lge/ims/DataConnectionManager;->isInternetApnRequestRequired()Z

    #@2f
    move-result v0

    #@30
    if-eqz v0, :cond_1e

    #@32
    .line 286
    invoke-virtual {p0, p2}, Lcom/lge/ims/DataConnectionManager;->disconnectInternetApn(I)V

    #@35
    goto :goto_1e

    #@36
    .line 288
    :cond_36
    const/4 v0, 0x2

    #@37
    if-ne p1, v0, :cond_43

    #@39
    .line 289
    invoke-virtual {p0}, Lcom/lge/ims/DataConnectionManager;->isInternetApnRequestRequired()Z

    #@3c
    move-result v0

    #@3d
    if-eqz v0, :cond_1e

    #@3f
    .line 293
    invoke-virtual {p0, p2}, Lcom/lge/ims/DataConnectionManager;->disconnectInternetApn(I)V

    #@42
    goto :goto_1e

    #@43
    .line 294
    :cond_43
    const/16 v0, 0x9

    #@45
    if-ne p1, v0, :cond_1e

    #@47
    .line 295
    invoke-virtual {p0}, Lcom/lge/ims/DataConnectionManager;->disconnectEmergencyApn()V

    #@4a
    goto :goto_1e
.end method

.method protected disconnectEmergencyApn()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 862
    iget v2, p0, Lcom/lge/ims/DataConnectionManager;->mEmergencyApnRequestState:I

    #@3
    const/4 v3, 0x2

    #@4
    if-eq v2, v3, :cond_e

    #@6
    .line 863
    const-string v2, "DataConnectionManager"

    #@8
    const-string v3, "Data connection(mobile_emergency) request is not done"

    #@a
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    .line 880
    :goto_d
    return-void

    #@e
    .line 867
    :cond_e
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getConnectivityManager()Landroid/net/ConnectivityManager;

    #@11
    move-result-object v0

    #@12
    .line 869
    .local v0, cm:Landroid/net/ConnectivityManager;
    if-nez v0, :cond_1c

    #@14
    .line 870
    const-string v2, "DataConnectionManager"

    #@16
    const-string v3, "ConnectivityManager is null"

    #@18
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@1b
    goto :goto_d

    #@1c
    .line 874
    :cond_1c
    const-string v2, "enableEMERGENCY"

    #@1e
    invoke-virtual {v0, v5, v2}, Landroid/net/ConnectivityManager;->stopUsingNetworkFeature(ILjava/lang/String;)I

    #@21
    move-result v1

    #@22
    .line 876
    .local v1, result:I
    const-string v2, "DataConnectionManager"

    #@24
    new-instance v3, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v4, "stopUsingNetworkFeature :: result="

    #@2b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v3

    #@37
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@3a
    .line 878
    iput v5, p0, Lcom/lge/ims/DataConnectionManager;->mEmergencyApnRequestState:I

    #@3c
    .line 879
    invoke-static {v5}, Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;->fromInt(I)Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

    #@3f
    move-result-object v2

    #@40
    iput-object v2, p0, Lcom/lge/ims/DataConnectionManager;->mEmcFailCause:Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

    #@42
    goto :goto_d
.end method

.method protected disconnectImsApn(I)V
    .registers 8
    .parameter "intervalForApnBlock"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 988
    iget v2, p0, Lcom/lge/ims/DataConnectionManager;->mImsApnRequestState:I

    #@3
    const/4 v3, 0x2

    #@4
    if-eq v2, v3, :cond_e

    #@6
    .line 989
    const-string v2, "DataConnectionManager"

    #@8
    const-string v3, "Data connection(mobile_ims) request is not done"

    #@a
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    .line 1013
    :cond_d
    :goto_d
    return-void

    #@e
    .line 993
    :cond_e
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getConnectivityManager()Landroid/net/ConnectivityManager;

    #@11
    move-result-object v0

    #@12
    .line 995
    .local v0, cm:Landroid/net/ConnectivityManager;
    if-nez v0, :cond_1c

    #@14
    .line 996
    const-string v2, "DataConnectionManager"

    #@16
    const-string v3, "ConnectivityManager is null"

    #@18
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@1b
    goto :goto_d

    #@1c
    .line 1000
    :cond_1c
    const-string v2, "enableIMS"

    #@1e
    invoke-virtual {v0, v5, v2}, Landroid/net/ConnectivityManager;->stopUsingNetworkFeature(ILjava/lang/String;)I

    #@21
    move-result v1

    #@22
    .line 1002
    .local v1, result:I
    const-string v2, "DataConnectionManager"

    #@24
    new-instance v3, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v4, "stopUsingNetworkFeature :: result="

    #@2b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v3

    #@37
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@3a
    .line 1004
    iput v5, p0, Lcom/lge/ims/DataConnectionManager;->mImsApnRequestState:I

    #@3c
    .line 1006
    invoke-virtual {p0}, Lcom/lge/ims/DataConnectionManager;->isImsApnBlockRequired()Z

    #@3f
    move-result v2

    #@40
    if-eqz v2, :cond_d

    #@42
    .line 1007
    if-lez p1, :cond_d

    #@44
    .line 1008
    const/4 v2, 0x3

    #@45
    iput v2, p0, Lcom/lge/ims/DataConnectionManager;->mImsApnRequestState:I

    #@47
    .line 1009
    iget-object v2, p0, Lcom/lge/ims/DataConnectionManager;->mDataConnectionHandler:Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;

    #@49
    const/16 v3, 0x7d5

    #@4b
    int-to-long v4, p1

    #@4c
    invoke-virtual {v2, v3, v4, v5}, Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;->sendEmptyMessageDelayed(IJ)Z

    #@4f
    .line 1010
    const-string v2, "DataConnectionManager"

    #@51
    new-instance v3, Ljava/lang/StringBuilder;

    #@53
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@56
    const-string v4, "Data connection(mobile_ims) will be blocked during "

    #@58
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v3

    #@5c
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v3

    #@60
    const-string v4, "ms"

    #@62
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v3

    #@66
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v3

    #@6a
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@6d
    goto :goto_d
.end method

.method protected disconnectInternetApn(I)V
    .registers 2
    .parameter "intervalForApnBlock"

    #@0
    .prologue
    .line 1022
    return-void
.end method

.method public getApn(I)Ljava/lang/String;
    .registers 12
    .parameter "apnType"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v9, 0x2

    #@2
    const/4 v8, 0x1

    #@3
    const/16 v7, 0x9

    #@5
    const/4 v6, 0x0

    #@6
    .line 300
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getInstance()Lcom/lge/ims/SystemServiceManager;

    #@9
    move-result-object v1

    #@a
    .line 302
    .local v1, ssm:Lcom/lge/ims/SystemServiceManager;
    if-nez v1, :cond_1b

    #@c
    .line 303
    if-ne p1, v8, :cond_11

    #@e
    .line 304
    iget-object v2, p0, Lcom/lge/ims/DataConnectionManager;->mImsApn:Ljava/lang/String;

    #@10
    .line 331
    :cond_10
    :goto_10
    return-object v2

    #@11
    .line 305
    :cond_11
    if-ne p1, v9, :cond_16

    #@13
    .line 306
    iget-object v2, p0, Lcom/lge/ims/DataConnectionManager;->mDefaultApn:Ljava/lang/String;

    #@15
    goto :goto_10

    #@16
    .line 307
    :cond_16
    if-ne p1, v7, :cond_10

    #@18
    .line 308
    iget-object v2, p0, Lcom/lge/ims/DataConnectionManager;->mEmergencyApn:Ljava/lang/String;

    #@1a
    goto :goto_10

    #@1b
    .line 314
    :cond_1b
    invoke-static {p1}, Lcom/lge/ims/DataConnectionManager;->getApnTypeFromPhone(I)Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v1, v7, v6, v3}, Lcom/lge/ims/SystemServiceManager;->getSysInfo(IILjava/lang/String;)[Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    .line 316
    .local v0, apns:[Ljava/lang/String;
    if-eqz v0, :cond_51

    #@25
    array-length v3, v0

    #@26
    if-lez v3, :cond_51

    #@28
    .line 317
    const-string v3, "DataConnectionManager"

    #@2a
    new-instance v4, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v5, "getApn="

    #@31
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v4

    #@35
    aget-object v5, v0, v6

    #@37
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v4

    #@3b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v4

    #@3f
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@42
    .line 319
    aget-object v3, v0, v6

    #@44
    if-eqz v3, :cond_51

    #@46
    aget-object v3, v0, v6

    #@48
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    #@4b
    move-result v3

    #@4c
    if-nez v3, :cond_51

    #@4e
    .line 320
    aget-object v2, v0, v6

    #@50
    goto :goto_10

    #@51
    .line 324
    :cond_51
    if-ne p1, v8, :cond_56

    #@53
    .line 325
    iget-object v2, p0, Lcom/lge/ims/DataConnectionManager;->mImsApn:Ljava/lang/String;

    #@55
    goto :goto_10

    #@56
    .line 326
    :cond_56
    if-ne p1, v9, :cond_5b

    #@58
    .line 327
    iget-object v2, p0, Lcom/lge/ims/DataConnectionManager;->mDefaultApn:Ljava/lang/String;

    #@5a
    goto :goto_10

    #@5b
    .line 328
    :cond_5b
    if-ne p1, v7, :cond_10

    #@5d
    .line 329
    iget-object v2, p0, Lcom/lge/ims/DataConnectionManager;->mEmergencyApn:Ljava/lang/String;

    #@5f
    goto :goto_10
.end method

.method public getDataState()I
    .registers 2

    #@0
    .prologue
    .line 337
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, v0}, Lcom/lge/ims/DataConnectionManager;->getDataState(I)I

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public getDataState(I)I
    .registers 5
    .parameter "apnType"

    #@0
    .prologue
    .line 342
    const-string v0, "DataConnectionManager"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "getDataState - apnType="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 344
    const/4 v0, 0x1

    #@19
    if-ne p1, v0, :cond_27

    #@1b
    .line 345
    invoke-virtual {p0}, Lcom/lge/ims/DataConnectionManager;->isMultipleApnSupported()Z

    #@1e
    move-result v0

    #@1f
    if-eqz v0, :cond_24

    #@21
    .line 346
    iget v0, p0, Lcom/lge/ims/DataConnectionManager;->mImsDataState:I

    #@23
    .line 355
    :goto_23
    return v0

    #@24
    .line 348
    :cond_24
    iget v0, p0, Lcom/lge/ims/DataConnectionManager;->mDefaultDataState:I

    #@26
    goto :goto_23

    #@27
    .line 350
    :cond_27
    const/4 v0, 0x2

    #@28
    if-ne p1, v0, :cond_2d

    #@2a
    .line 351
    iget v0, p0, Lcom/lge/ims/DataConnectionManager;->mDefaultDataState:I

    #@2c
    goto :goto_23

    #@2d
    .line 352
    :cond_2d
    const/16 v0, 0x9

    #@2f
    if-ne p1, v0, :cond_34

    #@31
    .line 353
    iget v0, p0, Lcom/lge/ims/DataConnectionManager;->mEmergencyDataState:I

    #@33
    goto :goto_23

    #@34
    .line 355
    :cond_34
    const/4 v0, -0x1

    #@35
    goto :goto_23
.end method

.method public getDataStateEx()I
    .registers 2

    #@0
    .prologue
    .line 361
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, v0}, Lcom/lge/ims/DataConnectionManager;->getDataStateEx(I)I

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public getDataStateEx(I)I
    .registers 14
    .parameter "apnType"

    #@0
    .prologue
    const/high16 v11, 0x2

    #@2
    const/16 v10, 0x9

    #@4
    const/4 v9, 0x2

    #@5
    const/4 v8, 0x1

    #@6
    const/4 v1, 0x0

    #@7
    .line 366
    const-string v5, "DataConnectionManager"

    #@9
    new-instance v6, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v7, "getDataStateEx - apnType="

    #@10
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v6

    #@14
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v6

    #@18
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v6

    #@1c
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@1f
    .line 368
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getConnectivityManager()Landroid/net/ConnectivityManager;

    #@22
    move-result-object v0

    #@23
    .line 370
    .local v0, cm:Landroid/net/ConnectivityManager;
    if-nez v0, :cond_2d

    #@25
    .line 371
    const-string v5, "DataConnectionManager"

    #@27
    const-string v6, "ConnectivityManager is null"

    #@29
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@2c
    .line 441
    :cond_2c
    :goto_2c
    return v1

    #@2d
    .line 375
    :cond_2d
    const/4 v2, 0x0

    #@2e
    .line 377
    .local v2, netInfo:Landroid/net/NetworkInfo;
    if-ne p1, v8, :cond_4b

    #@30
    .line 378
    invoke-virtual {p0}, Lcom/lge/ims/DataConnectionManager;->isMultipleApnSupported()Z

    #@33
    move-result v5

    #@34
    if-eqz v5, :cond_46

    #@36
    .line 379
    const/16 v5, 0xb

    #@38
    invoke-virtual {v0, v5}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@3b
    move-result-object v2

    #@3c
    .line 389
    :cond_3c
    :goto_3c
    if-nez v2, :cond_5b

    #@3e
    .line 390
    const-string v5, "DataConnectionManager"

    #@40
    const-string v6, "NetworkInfo is null"

    #@42
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@45
    goto :goto_2c

    #@46
    .line 381
    :cond_46
    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@49
    move-result-object v2

    #@4a
    goto :goto_3c

    #@4b
    .line 383
    :cond_4b
    if-ne p1, v9, :cond_52

    #@4d
    .line 384
    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@50
    move-result-object v2

    #@51
    goto :goto_3c

    #@52
    .line 385
    :cond_52
    if-ne p1, v10, :cond_3c

    #@54
    .line 386
    const/16 v5, 0x17

    #@56
    invoke-virtual {v0, v5}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@59
    move-result-object v2

    #@5a
    goto :goto_3c

    #@5b
    .line 394
    :cond_5b
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    #@5e
    move-result-object v3

    #@5f
    .line 395
    .local v3, niState:Landroid/net/NetworkInfo$State;
    const/4 v1, 0x0

    #@60
    .line 397
    .local v1, dataState:I
    sget-object v5, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    #@62
    if-eq v3, v5, :cond_68

    #@64
    sget-object v5, Landroid/net/NetworkInfo$State;->SUSPENDED:Landroid/net/NetworkInfo$State;

    #@66
    if-ne v3, v5, :cond_69

    #@68
    .line 398
    :cond_68
    const/4 v1, 0x2

    #@69
    .line 401
    :cond_69
    if-ne p1, v8, :cond_e8

    #@6b
    .line 402
    invoke-virtual {p0}, Lcom/lge/ims/DataConnectionManager;->isMultipleApnSupported()Z

    #@6e
    move-result v5

    #@6f
    if-eqz v5, :cond_ad

    #@71
    .line 403
    iget v5, p0, Lcom/lge/ims/DataConnectionManager;->mImsDataState:I

    #@73
    if-eq v5, v1, :cond_2c

    #@75
    .line 404
    const-string v5, "DataConnectionManager"

    #@77
    new-instance v6, Ljava/lang/StringBuilder;

    #@79
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@7c
    const-string v7, "Data state :: "

    #@7e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v6

    #@82
    iget v7, p0, Lcom/lge/ims/DataConnectionManager;->mImsDataState:I

    #@84
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@87
    move-result-object v6

    #@88
    const-string v7, " >> "

    #@8a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v6

    #@8e
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@91
    move-result-object v6

    #@92
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@95
    move-result-object v6

    #@96
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@99
    .line 405
    iput v1, p0, Lcom/lge/ims/DataConnectionManager;->mImsDataState:I

    #@9b
    .line 408
    const/high16 v5, 0x1

    #@9d
    invoke-static {v1}, Lcom/lge/ims/DataConnectionManager;->convertImsDataState(I)I

    #@a0
    move-result v6

    #@a1
    or-int v4, v5, v6

    #@a3
    .line 409
    .local v4, result:I
    iget-object v5, p0, Lcom/lge/ims/DataConnectionManager;->mDataStateChangedRegistrants:Landroid/os/RegistrantList;

    #@a5
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a8
    move-result-object v6

    #@a9
    invoke-virtual {v5, v6}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@ac
    goto :goto_2c

    #@ad
    .line 412
    .end local v4           #result:I
    :cond_ad
    iget v5, p0, Lcom/lge/ims/DataConnectionManager;->mDefaultDataState:I

    #@af
    if-eq v5, v1, :cond_2c

    #@b1
    .line 413
    const-string v5, "DataConnectionManager"

    #@b3
    new-instance v6, Ljava/lang/StringBuilder;

    #@b5
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@b8
    const-string v7, "Data state :: "

    #@ba
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v6

    #@be
    iget v7, p0, Lcom/lge/ims/DataConnectionManager;->mDefaultDataState:I

    #@c0
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v6

    #@c4
    const-string v7, " >> "

    #@c6
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v6

    #@ca
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v6

    #@ce
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d1
    move-result-object v6

    #@d2
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@d5
    .line 414
    iput v1, p0, Lcom/lge/ims/DataConnectionManager;->mDefaultDataState:I

    #@d7
    .line 417
    invoke-static {v1}, Lcom/lge/ims/DataConnectionManager;->convertImsDataState(I)I

    #@da
    move-result v5

    #@db
    or-int v4, v11, v5

    #@dd
    .line 418
    .restart local v4       #result:I
    iget-object v5, p0, Lcom/lge/ims/DataConnectionManager;->mDataStateChangedRegistrants:Landroid/os/RegistrantList;

    #@df
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@e2
    move-result-object v6

    #@e3
    invoke-virtual {v5, v6}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@e6
    goto/16 :goto_2c

    #@e8
    .line 421
    .end local v4           #result:I
    :cond_e8
    if-ne p1, v9, :cond_125

    #@ea
    .line 422
    iget v5, p0, Lcom/lge/ims/DataConnectionManager;->mDefaultDataState:I

    #@ec
    if-eq v5, v1, :cond_2c

    #@ee
    .line 423
    const-string v5, "DataConnectionManager"

    #@f0
    new-instance v6, Ljava/lang/StringBuilder;

    #@f2
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@f5
    const-string v7, "Data state :: "

    #@f7
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fa
    move-result-object v6

    #@fb
    iget v7, p0, Lcom/lge/ims/DataConnectionManager;->mDefaultDataState:I

    #@fd
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@100
    move-result-object v6

    #@101
    const-string v7, " >> "

    #@103
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@106
    move-result-object v6

    #@107
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10a
    move-result-object v6

    #@10b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10e
    move-result-object v6

    #@10f
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@112
    .line 424
    iput v1, p0, Lcom/lge/ims/DataConnectionManager;->mDefaultDataState:I

    #@114
    .line 427
    invoke-static {v1}, Lcom/lge/ims/DataConnectionManager;->convertImsDataState(I)I

    #@117
    move-result v5

    #@118
    or-int v4, v11, v5

    #@11a
    .line 428
    .restart local v4       #result:I
    iget-object v5, p0, Lcom/lge/ims/DataConnectionManager;->mDataStateChangedRegistrants:Landroid/os/RegistrantList;

    #@11c
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11f
    move-result-object v6

    #@120
    invoke-virtual {v5, v6}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@123
    goto/16 :goto_2c

    #@125
    .line 430
    .end local v4           #result:I
    :cond_125
    if-ne p1, v10, :cond_2c

    #@127
    .line 431
    iget v5, p0, Lcom/lge/ims/DataConnectionManager;->mEmergencyDataState:I

    #@129
    if-eq v5, v1, :cond_2c

    #@12b
    .line 432
    const-string v5, "DataConnectionManager"

    #@12d
    new-instance v6, Ljava/lang/StringBuilder;

    #@12f
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@132
    const-string v7, "Data state :: "

    #@134
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@137
    move-result-object v6

    #@138
    iget v7, p0, Lcom/lge/ims/DataConnectionManager;->mEmergencyDataState:I

    #@13a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13d
    move-result-object v6

    #@13e
    const-string v7, " >> "

    #@140
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@143
    move-result-object v6

    #@144
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@147
    move-result-object v6

    #@148
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14b
    move-result-object v6

    #@14c
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@14f
    .line 433
    iput v1, p0, Lcom/lge/ims/DataConnectionManager;->mEmergencyDataState:I

    #@151
    .line 436
    const/high16 v5, 0x9

    #@153
    invoke-static {v1}, Lcom/lge/ims/DataConnectionManager;->convertImsDataState(I)I

    #@156
    move-result v6

    #@157
    or-int v4, v5, v6

    #@159
    .line 437
    .restart local v4       #result:I
    iget-object v5, p0, Lcom/lge/ims/DataConnectionManager;->mDataStateChangedRegistrants:Landroid/os/RegistrantList;

    #@15b
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@15e
    move-result-object v6

    #@15f
    invoke-virtual {v5, v6}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@162
    goto/16 :goto_2c
.end method

.method public getFailCause(I)I
    .registers 3
    .parameter "apnType"

    #@0
    .prologue
    .line 445
    const/4 v0, 0x1

    #@1
    if-ne p1, v0, :cond_5

    #@3
    .line 453
    :cond_3
    :goto_3
    const/4 v0, 0x0

    #@4
    :goto_4
    return v0

    #@5
    .line 447
    :cond_5
    const/16 v0, 0x9

    #@7
    if-ne p1, v0, :cond_10

    #@9
    .line 448
    iget-object v0, p0, Lcom/lge/ims/DataConnectionManager;->mEmcFailCause:Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

    #@b
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;->getCode()I

    #@e
    move-result v0

    #@f
    goto :goto_4

    #@10
    .line 449
    :cond_10
    const/4 v0, 0x2

    #@11
    if-ne p1, v0, :cond_3

    #@13
    goto :goto_3
.end method

.method public getLocalAddress(II)Ljava/lang/String;
    .registers 12
    .parameter "apnType"
    .parameter "ipVersion"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v8, 0x6

    #@2
    const/4 v7, 0x4

    #@3
    const/4 v1, 0x0

    #@4
    .line 457
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getConnectivityManager()Landroid/net/ConnectivityManager;

    #@7
    move-result-object v0

    #@8
    .line 459
    .local v0, cm:Landroid/net/ConnectivityManager;
    if-nez v0, :cond_12

    #@a
    .line 460
    const-string v4, "DataConnectionManager"

    #@c
    const-string v5, "ConnectivityManager is null"

    #@e
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 524
    :cond_11
    :goto_11
    return-object v1

    #@12
    .line 464
    :cond_12
    const/4 v3, 0x0

    #@13
    .line 466
    .local v3, linkProperties:Landroid/net/LinkProperties;
    invoke-virtual {p0}, Lcom/lge/ims/DataConnectionManager;->isMultipleApnSupported()Z

    #@16
    move-result v4

    #@17
    if-eqz v4, :cond_3f

    #@19
    .line 467
    const/4 v4, 0x1

    #@1a
    if-ne p1, v4, :cond_2c

    #@1c
    .line 468
    const/16 v4, 0xb

    #@1e
    invoke-virtual {v0, v4}, Landroid/net/ConnectivityManager;->getLinkProperties(I)Landroid/net/LinkProperties;

    #@21
    move-result-object v3

    #@22
    .line 478
    :cond_22
    :goto_22
    if-nez v3, :cond_44

    #@24
    .line 479
    const-string v4, "DataConnectionManager"

    #@26
    const-string v5, "LinkProperties is null"

    #@28
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@2b
    goto :goto_11

    #@2c
    .line 469
    :cond_2c
    const/4 v4, 0x2

    #@2d
    if-ne p1, v4, :cond_34

    #@2f
    .line 470
    invoke-virtual {v0, v5}, Landroid/net/ConnectivityManager;->getLinkProperties(I)Landroid/net/LinkProperties;

    #@32
    move-result-object v3

    #@33
    goto :goto_22

    #@34
    .line 471
    :cond_34
    const/16 v4, 0x9

    #@36
    if-ne p1, v4, :cond_22

    #@38
    .line 472
    const/16 v4, 0x17

    #@3a
    invoke-virtual {v0, v4}, Landroid/net/ConnectivityManager;->getLinkProperties(I)Landroid/net/LinkProperties;

    #@3d
    move-result-object v3

    #@3e
    goto :goto_22

    #@3f
    .line 475
    :cond_3f
    invoke-virtual {v0, v5}, Landroid/net/ConnectivityManager;->getLinkProperties(I)Landroid/net/LinkProperties;

    #@42
    move-result-object v3

    #@43
    goto :goto_22

    #@44
    .line 483
    :cond_44
    invoke-virtual {v3}, Landroid/net/LinkProperties;->getLinkAddresses()Ljava/util/Collection;

    #@47
    move-result-object v2

    #@48
    .line 485
    .local v2, linkAddresses:Ljava/util/Collection;,"Ljava/util/Collection<Landroid/net/LinkAddress;>;"
    if-nez v2, :cond_52

    #@4a
    .line 486
    const-string v4, "DataConnectionManager"

    #@4c
    const-string v5, "LinkAddresses is null"

    #@4e
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@51
    goto :goto_11

    #@52
    .line 490
    :cond_52
    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    #@55
    move-result v4

    #@56
    if-eqz v4, :cond_60

    #@58
    .line 491
    const-string v4, "DataConnectionManager"

    #@5a
    const-string v5, "LinkAddresses is empty"

    #@5c
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@5f
    goto :goto_11

    #@60
    .line 495
    :cond_60
    const-string v4, "DataConnectionManager"

    #@62
    new-instance v5, Ljava/lang/StringBuilder;

    #@64
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@67
    const-string v6, "LinkAddress :: apnType="

    #@69
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v5

    #@6d
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@70
    move-result-object v5

    #@71
    const-string v6, ", size="

    #@73
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v5

    #@77
    invoke-interface {v2}, Ljava/util/Collection;->size()I

    #@7a
    move-result v6

    #@7b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v5

    #@7f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@82
    move-result-object v5

    #@83
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@86
    .line 497
    if-nez p2, :cond_90

    #@88
    .line 499
    invoke-static {p1}, Lcom/lge/ims/DataConnectionManager;->getApnName(I)Ljava/lang/String;

    #@8b
    move-result-object v4

    #@8c
    invoke-static {v4}, Lcom/lge/ims/Configuration;->getIPVersion(Ljava/lang/String;)I

    #@8f
    move-result p2

    #@90
    .line 502
    :cond_90
    if-ne p2, v7, :cond_98

    #@92
    .line 503
    invoke-direct {p0, v2, v7}, Lcom/lge/ims/DataConnectionManager;->getIPAddress(Ljava/util/Collection;I)Ljava/lang/String;

    #@95
    move-result-object v1

    #@96
    goto/16 :goto_11

    #@98
    .line 504
    :cond_98
    if-ne p2, v8, :cond_a0

    #@9a
    .line 505
    invoke-direct {p0, v2, v8}, Lcom/lge/ims/DataConnectionManager;->getIPAddress(Ljava/util/Collection;I)Ljava/lang/String;

    #@9d
    move-result-object v1

    #@9e
    goto/16 :goto_11

    #@a0
    .line 506
    :cond_a0
    const/16 v4, 0x2e

    #@a2
    if-ne p2, v4, :cond_b0

    #@a4
    .line 507
    invoke-direct {p0, v2, v7}, Lcom/lge/ims/DataConnectionManager;->getIPAddress(Ljava/util/Collection;I)Ljava/lang/String;

    #@a7
    move-result-object v1

    #@a8
    .line 509
    .local v1, ipAddress:Ljava/lang/String;
    if-nez v1, :cond_11

    #@aa
    .line 513
    invoke-direct {p0, v2, v8}, Lcom/lge/ims/DataConnectionManager;->getIPAddress(Ljava/util/Collection;I)Ljava/lang/String;

    #@ad
    move-result-object v1

    #@ae
    goto/16 :goto_11

    #@b0
    .line 514
    .end local v1           #ipAddress:Ljava/lang/String;
    :cond_b0
    const/16 v4, 0x40

    #@b2
    if-ne p2, v4, :cond_11

    #@b4
    .line 515
    invoke-direct {p0, v2, v8}, Lcom/lge/ims/DataConnectionManager;->getIPAddress(Ljava/util/Collection;I)Ljava/lang/String;

    #@b7
    move-result-object v1

    #@b8
    .line 517
    .restart local v1       #ipAddress:Ljava/lang/String;
    if-nez v1, :cond_11

    #@ba
    .line 521
    invoke-direct {p0, v2, v7}, Lcom/lge/ims/DataConnectionManager;->getIPAddress(Ljava/util/Collection;I)Ljava/lang/String;

    #@bd
    move-result-object v1

    #@be
    goto/16 :goto_11
.end method

.method public getPcscfAddress(II)[Ljava/lang/String;
    .registers 13
    .parameter "apnType"
    .parameter "ipVersion"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v9, 0x6

    #@2
    const/4 v8, 0x4

    #@3
    const/4 v7, 0x2

    #@4
    .line 528
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getInstance()Lcom/lge/ims/SystemServiceManager;

    #@7
    move-result-object v3

    #@8
    .line 530
    .local v3, ssm:Lcom/lge/ims/SystemServiceManager;
    if-nez v3, :cond_12

    #@a
    .line 531
    const-string v4, "DataConnectionManager"

    #@c
    const-string v5, "SystemServiceManager is null"

    #@e
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 577
    :cond_11
    :goto_11
    return-object v2

    #@12
    .line 535
    :cond_12
    invoke-static {p1}, Lcom/lge/ims/DataConnectionManager;->getApnTypeFromPhone(I)Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    .line 537
    .local v1, apnTypeFromPhone:Ljava/lang/String;
    if-nez v1, :cond_37

    #@18
    .line 538
    const-string v4, "DataConnectionManager"

    #@1a
    new-instance v5, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v6, "apnType("

    #@21
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v5

    #@25
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v5

    #@29
    const-string v6, ") is not supported"

    #@2b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v5

    #@2f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v5

    #@33
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@36
    goto :goto_11

    #@37
    .line 542
    :cond_37
    const/4 v2, 0x0

    #@38
    .line 544
    .local v2, pcscfAddresses:[Ljava/lang/String;
    if-nez p2, :cond_42

    #@3a
    .line 546
    invoke-static {p1}, Lcom/lge/ims/DataConnectionManager;->getApnName(I)Ljava/lang/String;

    #@3d
    move-result-object v4

    #@3e
    invoke-static {v4}, Lcom/lge/ims/Configuration;->getIPVersion(Ljava/lang/String;)I

    #@41
    move-result p2

    #@42
    .line 549
    :cond_42
    const-string v4, "DataConnectionManager"

    #@44
    new-instance v5, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    const-string v6, "P-CSCF address (PCO) :: ipVersion="

    #@4b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v5

    #@4f
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@52
    move-result-object v5

    #@53
    const-string v6, ", apnType="

    #@55
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v5

    #@59
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v5

    #@5d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v5

    #@61
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@64
    .line 551
    if-ne p2, v8, :cond_6f

    #@66
    .line 552
    invoke-virtual {v3, v7, v8, v1}, Lcom/lge/ims/SystemServiceManager;->getSysInfo(IILjava/lang/String;)[Ljava/lang/String;

    #@69
    move-result-object v2

    #@6a
    .line 577
    :cond_6a
    :goto_6a
    invoke-direct {p0, v2}, Lcom/lge/ims/DataConnectionManager;->getValidAddresses([Ljava/lang/String;)[Ljava/lang/String;

    #@6d
    move-result-object v2

    #@6e
    goto :goto_11

    #@6f
    .line 553
    :cond_6f
    if-ne p2, v9, :cond_76

    #@71
    .line 554
    invoke-virtual {v3, v7, v9, v1}, Lcom/lge/ims/SystemServiceManager;->getSysInfo(IILjava/lang/String;)[Ljava/lang/String;

    #@74
    move-result-object v2

    #@75
    goto :goto_6a

    #@76
    .line 555
    :cond_76
    const/16 v4, 0x2e

    #@78
    if-ne p2, v4, :cond_8c

    #@7a
    .line 556
    invoke-virtual {v3, v7, v8, v1}, Lcom/lge/ims/SystemServiceManager;->getSysInfo(IILjava/lang/String;)[Ljava/lang/String;

    #@7d
    move-result-object v0

    #@7e
    .line 558
    .local v0, addresses:[Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/lge/ims/DataConnectionManager;->getValidAddresses([Ljava/lang/String;)[Ljava/lang/String;

    #@81
    move-result-object v2

    #@82
    .line 560
    if-eqz v2, :cond_87

    #@84
    array-length v4, v2

    #@85
    if-gtz v4, :cond_11

    #@87
    .line 564
    :cond_87
    invoke-virtual {v3, v7, v9, v1}, Lcom/lge/ims/SystemServiceManager;->getSysInfo(IILjava/lang/String;)[Ljava/lang/String;

    #@8a
    move-result-object v2

    #@8b
    .line 565
    goto :goto_6a

    #@8c
    .end local v0           #addresses:[Ljava/lang/String;
    :cond_8c
    const/16 v4, 0x40

    #@8e
    if-ne p2, v4, :cond_6a

    #@90
    .line 566
    invoke-virtual {v3, v7, v9, v1}, Lcom/lge/ims/SystemServiceManager;->getSysInfo(IILjava/lang/String;)[Ljava/lang/String;

    #@93
    move-result-object v0

    #@94
    .line 568
    .restart local v0       #addresses:[Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/lge/ims/DataConnectionManager;->getValidAddresses([Ljava/lang/String;)[Ljava/lang/String;

    #@97
    move-result-object v2

    #@98
    .line 570
    if-eqz v2, :cond_9d

    #@9a
    array-length v4, v2

    #@9b
    if-gtz v4, :cond_11

    #@9d
    .line 574
    :cond_9d
    invoke-virtual {v3, v7, v8, v1}, Lcom/lge/ims/SystemServiceManager;->getSysInfo(IILjava/lang/String;)[Ljava/lang/String;

    #@a0
    move-result-object v2

    #@a1
    goto :goto_6a
.end method

.method protected isAirplaneMode()Z
    .registers 3

    #@0
    .prologue
    .line 1029
    invoke-static {}, Lcom/lge/ims/PhoneStateTracker;->getInstance()Lcom/lge/ims/PhoneStateTracker;

    #@3
    move-result-object v0

    #@4
    .line 1031
    .local v0, pst:Lcom/lge/ims/PhoneStateTracker;
    if-eqz v0, :cond_b

    #@6
    .line 1032
    invoke-virtual {v0}, Lcom/lge/ims/PhoneStateTracker;->isAirplaneMode()Z

    #@9
    move-result v1

    #@a
    .line 1035
    :goto_a
    return v1

    #@b
    :cond_b
    const/4 v1, 0x0

    #@c
    goto :goto_a
.end method

.method public isConnected()Z
    .registers 2

    #@0
    .prologue
    .line 582
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, v0}, Lcom/lge/ims/DataConnectionManager;->isConnected(I)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public isConnected(I)Z
    .registers 7
    .parameter "apnType"

    #@0
    .prologue
    const/4 v4, 0x3

    #@1
    const/4 v1, 0x0

    #@2
    const/4 v3, 0x2

    #@3
    const/4 v0, 0x1

    #@4
    .line 586
    if-ne p1, v0, :cond_1f

    #@6
    .line 587
    invoke-virtual {p0}, Lcom/lge/ims/DataConnectionManager;->isMultipleApnSupported()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_15

    #@c
    .line 588
    iget v2, p0, Lcom/lge/ims/DataConnectionManager;->mImsDataState:I

    #@e
    if-eq v2, v3, :cond_14

    #@10
    iget v2, p0, Lcom/lge/ims/DataConnectionManager;->mImsDataState:I

    #@12
    if-ne v2, v4, :cond_1d

    #@14
    .line 615
    :cond_14
    :goto_14
    return v0

    #@15
    .line 593
    :cond_15
    iget v2, p0, Lcom/lge/ims/DataConnectionManager;->mDefaultDataState:I

    #@17
    if-eq v2, v3, :cond_14

    #@19
    iget v2, p0, Lcom/lge/ims/DataConnectionManager;->mDefaultDataState:I

    #@1b
    if-eq v2, v4, :cond_14

    #@1d
    :cond_1d
    move v0, v1

    #@1e
    .line 599
    goto :goto_14

    #@1f
    .line 600
    :cond_1f
    if-ne p1, v3, :cond_2b

    #@21
    .line 601
    iget v2, p0, Lcom/lge/ims/DataConnectionManager;->mDefaultDataState:I

    #@23
    if-eq v2, v3, :cond_14

    #@25
    iget v2, p0, Lcom/lge/ims/DataConnectionManager;->mDefaultDataState:I

    #@27
    if-eq v2, v4, :cond_14

    #@29
    move v0, v1

    #@2a
    .line 606
    goto :goto_14

    #@2b
    .line 607
    :cond_2b
    const/16 v2, 0x9

    #@2d
    if-ne p1, v2, :cond_39

    #@2f
    .line 608
    iget v2, p0, Lcom/lge/ims/DataConnectionManager;->mEmergencyDataState:I

    #@31
    if-eq v2, v3, :cond_14

    #@33
    iget v2, p0, Lcom/lge/ims/DataConnectionManager;->mEmergencyDataState:I

    #@35
    if-eq v2, v4, :cond_14

    #@37
    move v0, v1

    #@38
    .line 613
    goto :goto_14

    #@39
    :cond_39
    move v0, v1

    #@3a
    .line 615
    goto :goto_14
.end method

.method protected isDataEmergencyOnly()Z
    .registers 3

    #@0
    .prologue
    .line 1039
    invoke-static {}, Lcom/lge/ims/PhoneStateTracker;->getInstance()Lcom/lge/ims/PhoneStateTracker;

    #@3
    move-result-object v0

    #@4
    .line 1041
    .local v0, pst:Lcom/lge/ims/PhoneStateTracker;
    if-nez v0, :cond_8

    #@6
    .line 1042
    const/4 v1, 0x0

    #@7
    .line 1045
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0}, Lcom/lge/ims/PhoneStateTracker;->isLteEmergencyOnly()Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method protected isImsApnBlockRequired()Z
    .registers 2

    #@0
    .prologue
    .line 1049
    iget v0, p0, Lcom/lge/ims/DataConnectionManager;->mPolicy:I

    #@2
    and-int/lit8 v0, v0, 0x20

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method protected isImsServiceStarted()Z
    .registers 2

    #@0
    .prologue
    .line 1053
    iget-boolean v0, p0, Lcom/lge/ims/DataConnectionManager;->mImsServiceStarted:Z

    #@2
    return v0
.end method

.method protected isInternetApnRequestRequired()Z
    .registers 2

    #@0
    .prologue
    .line 1025
    iget v0, p0, Lcom/lge/ims/DataConnectionManager;->mPolicy:I

    #@2
    and-int/lit8 v0, v0, 0x10

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public isMultipleApnSupported()Z
    .registers 2

    #@0
    .prologue
    .line 621
    iget v0, p0, Lcom/lge/ims/DataConnectionManager;->mPolicy:I

    #@2
    and-int/lit8 v0, v0, 0x1

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x0

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x1

    #@9
    goto :goto_7
.end method

.method public isMultiplePdnSupported()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 625
    iget-object v1, p0, Lcom/lge/ims/DataConnectionManager;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@3
    if-nez v1, :cond_6

    #@5
    .line 629
    :cond_5
    :goto_5
    return v0

    #@6
    :cond_6
    iget-object v1, p0, Lcom/lge/ims/DataConnectionManager;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@8
    iget v1, v1, Lcom/android/internal/telephony/LGfeature;->MPDNset:I

    #@a
    if-eqz v1, :cond_5

    #@c
    const/4 v0, 0x1

    #@d
    goto :goto_5
.end method

.method public isSingleAndImsApnSupported()Z
    .registers 2

    #@0
    .prologue
    .line 633
    iget v0, p0, Lcom/lge/ims/DataConnectionManager;->mPolicy:I

    #@2
    and-int/lit8 v0, v0, 0x4

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method protected isWiFiConnected()Z
    .registers 2

    #@0
    .prologue
    .line 1057
    iget-boolean v0, p0, Lcom/lge/ims/DataConnectionManager;->mWiFiConnected:Z

    #@2
    return v0
.end method

.method public notifyImsServiceStarted()V
    .registers 3

    #@0
    .prologue
    .line 637
    invoke-virtual {p0}, Lcom/lge/ims/DataConnectionManager;->isImsServiceStarted()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_d

    #@6
    .line 638
    iget-object v0, p0, Lcom/lge/ims/DataConnectionManager;->mDataConnectionHandler:Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;

    #@8
    const/16 v1, 0x7d1

    #@a
    invoke-virtual {v0, v1}, Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;->sendEmptyMessage(I)Z

    #@d
    .line 640
    :cond_d
    return-void
.end method

.method public registerForDataStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 726
    iget-object v0, p0, Lcom/lge/ims/DataConnectionManager;->mDataStateChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    new-instance v1, Landroid/os/Registrant;

    #@4
    invoke-direct {v1, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@7
    invoke-virtual {v0, v1}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 727
    return-void
.end method

.method public removeRouteToHostAddress(ILjava/lang/String;)Z
    .registers 11
    .parameter "apnType"
    .parameter "hostAddress"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 643
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getConnectivityManager()Landroid/net/ConnectivityManager;

    #@5
    move-result-object v1

    #@6
    .line 645
    .local v1, cm:Landroid/net/ConnectivityManager;
    if-nez v1, :cond_10

    #@8
    .line 646
    const-string v4, "DataConnectionManager"

    #@a
    const-string v5, "ConnectivityManager is null"

    #@c
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@f
    .line 680
    :cond_f
    :goto_f
    return v3

    #@10
    .line 650
    :cond_10
    const-string v5, "DataConnectionManager"

    #@12
    new-instance v6, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v7, "removeRouteToHostAddress :: apnType="

    #@19
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v6

    #@1d
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v6

    #@21
    const-string v7, ", hostAddress="

    #@23
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v6

    #@27
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v6

    #@2b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v6

    #@2f
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@32
    .line 652
    const/4 v0, 0x0

    #@33
    .line 655
    .local v0, address:Ljava/net/InetAddress;
    :try_start_33
    invoke-static {p2}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;
    :try_end_36
    .catch Ljava/net/UnknownHostException; {:try_start_33 .. :try_end_36} :catch_41

    #@36
    move-result-object v0

    #@37
    .line 662
    :goto_37
    if-nez v0, :cond_5f

    #@39
    .line 663
    const-string v4, "DataConnectionManager"

    #@3b
    const-string v5, "InetAddress is null"

    #@3d
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@40
    goto :goto_f

    #@41
    .line 656
    :catch_41
    move-exception v2

    #@42
    .line 657
    .local v2, e:Ljava/net/UnknownHostException;
    const-string v5, "DataConnectionManager"

    #@44
    new-instance v6, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    const-string v7, "Unknown Host :: "

    #@4b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v6

    #@4f
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v6

    #@53
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v6

    #@57
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@5a
    .line 658
    invoke-virtual {v2}, Ljava/net/UnknownHostException;->printStackTrace()V

    #@5d
    .line 659
    const/4 v0, 0x0

    #@5e
    goto :goto_37

    #@5f
    .line 667
    .end local v2           #e:Ljava/net/UnknownHostException;
    :cond_5f
    invoke-virtual {p0}, Lcom/lge/ims/DataConnectionManager;->isMultipleApnSupported()Z

    #@62
    move-result v5

    #@63
    if-eqz v5, :cond_81

    #@65
    .line 668
    if-ne p1, v4, :cond_6e

    #@67
    .line 669
    const/16 v3, 0xb

    #@69
    invoke-virtual {v1, v3, v0}, Landroid/net/ConnectivityManager;->requestRemRouteToHostAddress(ILjava/net/InetAddress;)Z

    #@6c
    move-result v3

    #@6d
    goto :goto_f

    #@6e
    .line 670
    :cond_6e
    const/4 v4, 0x2

    #@6f
    if-ne p1, v4, :cond_76

    #@71
    .line 671
    invoke-virtual {v1, v3, v0}, Landroid/net/ConnectivityManager;->requestRemRouteToHostAddress(ILjava/net/InetAddress;)Z

    #@74
    move-result v3

    #@75
    goto :goto_f

    #@76
    .line 672
    :cond_76
    const/16 v4, 0x9

    #@78
    if-ne p1, v4, :cond_f

    #@7a
    .line 673
    const/16 v3, 0x17

    #@7c
    invoke-virtual {v1, v3, v0}, Landroid/net/ConnectivityManager;->requestRemRouteToHostAddress(ILjava/net/InetAddress;)Z

    #@7f
    move-result v3

    #@80
    goto :goto_f

    #@81
    :cond_81
    move v3, v4

    #@82
    .line 677
    goto :goto_f
.end method

.method public requestRouteToHostAddress(ILjava/lang/String;)Z
    .registers 11
    .parameter "apnType"
    .parameter "hostAddress"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 684
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getConnectivityManager()Landroid/net/ConnectivityManager;

    #@5
    move-result-object v1

    #@6
    .line 686
    .local v1, cm:Landroid/net/ConnectivityManager;
    if-nez v1, :cond_10

    #@8
    .line 687
    const-string v4, "DataConnectionManager"

    #@a
    const-string v5, "ConnectivityManager is null"

    #@c
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@f
    .line 721
    :cond_f
    :goto_f
    return v3

    #@10
    .line 691
    :cond_10
    const-string v5, "DataConnectionManager"

    #@12
    new-instance v6, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v7, "requestRouteToHostAddress :: apnType="

    #@19
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v6

    #@1d
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v6

    #@21
    const-string v7, ", hostAddress="

    #@23
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v6

    #@27
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v6

    #@2b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v6

    #@2f
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@32
    .line 693
    const/4 v0, 0x0

    #@33
    .line 696
    .local v0, address:Ljava/net/InetAddress;
    :try_start_33
    invoke-static {p2}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;
    :try_end_36
    .catch Ljava/net/UnknownHostException; {:try_start_33 .. :try_end_36} :catch_41

    #@36
    move-result-object v0

    #@37
    .line 703
    :goto_37
    if-nez v0, :cond_5f

    #@39
    .line 704
    const-string v4, "DataConnectionManager"

    #@3b
    const-string v5, "InetAddress is null"

    #@3d
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@40
    goto :goto_f

    #@41
    .line 697
    :catch_41
    move-exception v2

    #@42
    .line 698
    .local v2, e:Ljava/net/UnknownHostException;
    const-string v5, "DataConnectionManager"

    #@44
    new-instance v6, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    const-string v7, "Unknown Host :: "

    #@4b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v6

    #@4f
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v6

    #@53
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v6

    #@57
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@5a
    .line 699
    invoke-virtual {v2}, Ljava/net/UnknownHostException;->printStackTrace()V

    #@5d
    .line 700
    const/4 v0, 0x0

    #@5e
    goto :goto_37

    #@5f
    .line 708
    .end local v2           #e:Ljava/net/UnknownHostException;
    :cond_5f
    invoke-virtual {p0}, Lcom/lge/ims/DataConnectionManager;->isMultipleApnSupported()Z

    #@62
    move-result v5

    #@63
    if-eqz v5, :cond_81

    #@65
    .line 709
    if-ne p1, v4, :cond_6e

    #@67
    .line 710
    const/16 v3, 0xb

    #@69
    invoke-virtual {v1, v3, v0}, Landroid/net/ConnectivityManager;->requestRouteToHostAddress(ILjava/net/InetAddress;)Z

    #@6c
    move-result v3

    #@6d
    goto :goto_f

    #@6e
    .line 711
    :cond_6e
    const/4 v4, 0x2

    #@6f
    if-ne p1, v4, :cond_76

    #@71
    .line 712
    invoke-virtual {v1, v3, v0}, Landroid/net/ConnectivityManager;->requestRouteToHostAddress(ILjava/net/InetAddress;)Z

    #@74
    move-result v3

    #@75
    goto :goto_f

    #@76
    .line 713
    :cond_76
    const/16 v4, 0x9

    #@78
    if-ne p1, v4, :cond_f

    #@7a
    .line 714
    const/16 v3, 0x17

    #@7c
    invoke-virtual {v1, v3, v0}, Landroid/net/ConnectivityManager;->requestRouteToHostAddress(ILjava/net/InetAddress;)Z

    #@7f
    move-result v3

    #@80
    goto :goto_f

    #@81
    :cond_81
    move v3, v4

    #@82
    .line 718
    goto :goto_f
.end method

.method public setContext(Landroid/content/Context;)V
    .registers 7
    .parameter "context"

    #@0
    .prologue
    .line 734
    iget-object v2, p0, Lcom/lge/ims/DataConnectionManager;->mContext:Landroid/content/Context;

    #@2
    if-eq v2, p1, :cond_15

    #@4
    .line 735
    iget-object v2, p0, Lcom/lge/ims/DataConnectionManager;->mContext:Landroid/content/Context;

    #@6
    if-eqz v2, :cond_f

    #@8
    .line 736
    iget-object v2, p0, Lcom/lge/ims/DataConnectionManager;->mContext:Landroid/content/Context;

    #@a
    iget-object v3, p0, Lcom/lge/ims/DataConnectionManager;->mDataConnectionReceiver:Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;

    #@c
    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@f
    .line 739
    :cond_f
    iput-object p1, p0, Lcom/lge/ims/DataConnectionManager;->mContext:Landroid/content/Context;

    #@11
    .line 741
    iget-object v2, p0, Lcom/lge/ims/DataConnectionManager;->mContext:Landroid/content/Context;

    #@13
    if-nez v2, :cond_16

    #@15
    .line 762
    :cond_15
    :goto_15
    return-void

    #@16
    .line 746
    :cond_16
    invoke-virtual {p0}, Lcom/lge/ims/DataConnectionManager;->isSingleAndImsApnSupported()Z

    #@19
    move-result v2

    #@1a
    if-eqz v2, :cond_2f

    #@1c
    .line 747
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getConnectivityManager()Landroid/net/ConnectivityManager;

    #@1f
    move-result-object v0

    #@20
    .line 749
    .local v0, cm:Landroid/net/ConnectivityManager;
    if-eqz v0, :cond_2f

    #@22
    .line 750
    const/4 v2, 0x1

    #@23
    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@26
    move-result-object v1

    #@27
    .line 752
    .local v1, netInfo:Landroid/net/NetworkInfo;
    if-eqz v1, :cond_2f

    #@29
    .line 753
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    #@2c
    move-result v2

    #@2d
    iput-boolean v2, p0, Lcom/lge/ims/DataConnectionManager;->mWiFiConnected:Z

    #@2f
    .line 758
    .end local v0           #cm:Landroid/net/ConnectivityManager;
    .end local v1           #netInfo:Landroid/net/NetworkInfo;
    :cond_2f
    iget-object v2, p0, Lcom/lge/ims/DataConnectionManager;->mDataConnectionReceiver:Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;

    #@31
    if-eqz v2, :cond_15

    #@33
    .line 759
    iget-object v2, p0, Lcom/lge/ims/DataConnectionManager;->mContext:Landroid/content/Context;

    #@35
    iget-object v3, p0, Lcom/lge/ims/DataConnectionManager;->mDataConnectionReceiver:Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;

    #@37
    iget-object v4, p0, Lcom/lge/ims/DataConnectionManager;->mDataConnectionReceiver:Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;

    #@39
    invoke-virtual {v4}, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;->getFilter()Landroid/content/IntentFilter;

    #@3c
    move-result-object v4

    #@3d
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@40
    goto :goto_15
.end method

.method public unregisterForDataStateChanged(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 730
    iget-object v0, p0, Lcom/lge/ims/DataConnectionManager;->mDataStateChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 731
    return-void
.end method
