.class public Lcom/lge/ims/ImsWakeLock;
.super Ljava/lang/Object;
.source "ImsWakeLock.java"


# static fields
.field private static DEFAULT_TIMEOUT:I = 0x0

.field private static final TAG:Ljava/lang/String; = "ImsWakeLock"

.field private static mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 11
    const/16 v0, 0x1388

    #@2
    sput v0, Lcom/lge/ims/ImsWakeLock;->DEFAULT_TIMEOUT:I

    #@4
    .line 13
    const/4 v0, 0x0

    #@5
    sput-object v0, Lcom/lge/ims/ImsWakeLock;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@7
    return-void
.end method

.method private constructor <init>()V
    .registers 3

    #@0
    .prologue
    .line 17
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 18
    const-string v0, "ImsWakeLock"

    #@5
    const-string v1, "ImsWakeLock is created"

    #@7
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 19
    return-void
.end method

.method public static acquire(I)V
    .registers 6
    .parameter "timeout"

    #@0
    .prologue
    .line 23
    if-nez p0, :cond_4

    #@2
    .line 25
    sget p0, Lcom/lge/ims/ImsWakeLock;->DEFAULT_TIMEOUT:I

    #@4
    .line 28
    :cond_4
    invoke-static {}, Lcom/lge/ims/ImsWakeLock;->getWakeLock()Landroid/os/PowerManager$WakeLock;

    #@7
    move-result-object v1

    #@8
    .line 30
    .local v1, wakeLock:Landroid/os/PowerManager$WakeLock;
    if-nez v1, :cond_23

    #@a
    .line 31
    const-string v2, "ImsWakeLock"

    #@c
    new-instance v3, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v4, "WakeLock is null :: timeout="

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@22
    .line 56
    :goto_22
    return-void

    #@23
    .line 44
    :cond_23
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getPowerManager()Landroid/os/PowerManager;

    #@26
    move-result-object v0

    #@27
    .line 46
    .local v0, pm:Landroid/os/PowerManager;
    if-eqz v0, :cond_48

    #@29
    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    #@2c
    move-result v2

    #@2d
    if-eqz v2, :cond_48

    #@2f
    .line 47
    const-string v2, "ImsWakeLock"

    #@31
    new-instance v3, Ljava/lang/StringBuilder;

    #@33
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@36
    const-string v4, "WakeLock :: screen on; no action; timeout="

    #@38
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v3

    #@3c
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v3

    #@40
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v3

    #@44
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@47
    goto :goto_22

    #@48
    .line 53
    :cond_48
    const-string v2, "ImsWakeLock"

    #@4a
    new-instance v3, Ljava/lang/StringBuilder;

    #@4c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@4f
    const-string v4, "WakeLock :: timeout="

    #@51
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v3

    #@55
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@58
    move-result-object v3

    #@59
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v3

    #@5d
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@60
    .line 55
    int-to-long v2, p0

    #@61
    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    #@64
    goto :goto_22
.end method

.method public static acquire(IZ)V
    .registers 8
    .parameter "timeout"
    .parameter "screenOffOnly"

    #@0
    .prologue
    .line 60
    if-nez p0, :cond_4

    #@2
    .line 62
    sget p0, Lcom/lge/ims/ImsWakeLock;->DEFAULT_TIMEOUT:I

    #@4
    .line 65
    :cond_4
    invoke-static {}, Lcom/lge/ims/ImsWakeLock;->getWakeLock()Landroid/os/PowerManager$WakeLock;

    #@7
    move-result-object v2

    #@8
    .line 67
    .local v2, wakeLock:Landroid/os/PowerManager$WakeLock;
    if-nez v2, :cond_23

    #@a
    .line 68
    const-string v3, "ImsWakeLock"

    #@c
    new-instance v4, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v5, "WakeLock is null :: timeout="

    #@13
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v4

    #@1f
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@22
    .line 93
    :goto_22
    return-void

    #@23
    .line 72
    :cond_23
    invoke-static {}, Lcom/lge/ims/BatteryStateTracker;->getInstance()Lcom/lge/ims/BatteryStateTracker;

    #@26
    move-result-object v0

    #@27
    .line 74
    .local v0, bst:Lcom/lge/ims/BatteryStateTracker;
    if-eqz v0, :cond_49

    #@29
    const/4 v3, 0x0

    #@2a
    invoke-virtual {v0, v3}, Lcom/lge/ims/BatteryStateTracker;->isPowerPlugged(Z)Z

    #@2d
    move-result v3

    #@2e
    if-eqz v3, :cond_49

    #@30
    .line 75
    const-string v3, "ImsWakeLock"

    #@32
    new-instance v4, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    const-string v5, "WakeLock :: power plugged; no action; timeout="

    #@39
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v4

    #@3d
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v4

    #@41
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v4

    #@45
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@48
    goto :goto_22

    #@49
    .line 79
    :cond_49
    if-eqz p1, :cond_70

    #@4b
    .line 80
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getPowerManager()Landroid/os/PowerManager;

    #@4e
    move-result-object v1

    #@4f
    .line 82
    .local v1, pm:Landroid/os/PowerManager;
    if-eqz v1, :cond_70

    #@51
    invoke-virtual {v1}, Landroid/os/PowerManager;->isScreenOn()Z

    #@54
    move-result v3

    #@55
    if-eqz v3, :cond_70

    #@57
    .line 83
    const-string v3, "ImsWakeLock"

    #@59
    new-instance v4, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v5, "WakeLock :: screen on; no action; timeout="

    #@60
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v4

    #@64
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@67
    move-result-object v4

    #@68
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v4

    #@6c
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@6f
    goto :goto_22

    #@70
    .line 90
    .end local v1           #pm:Landroid/os/PowerManager;
    :cond_70
    const-string v3, "ImsWakeLock"

    #@72
    new-instance v4, Ljava/lang/StringBuilder;

    #@74
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@77
    const-string v5, "WakeLock :: timeout="

    #@79
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v4

    #@7d
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@80
    move-result-object v4

    #@81
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@84
    move-result-object v4

    #@85
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@88
    .line 92
    int-to-long v3, p0

    #@89
    invoke-virtual {v2, v3, v4}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    #@8c
    goto :goto_22
.end method

.method private static getWakeLock()Landroid/os/PowerManager$WakeLock;
    .registers 3

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 96
    sget-object v1, Lcom/lge/ims/ImsWakeLock;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@3
    if-nez v1, :cond_21

    #@5
    .line 97
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getPowerManager()Landroid/os/PowerManager;

    #@8
    move-result-object v0

    #@9
    .line 99
    .local v0, pm:Landroid/os/PowerManager;
    if-nez v0, :cond_14

    #@b
    .line 100
    const-string v1, "ImsWakeLock"

    #@d
    const-string v2, "PowerManager is null"

    #@f
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@12
    .line 101
    const/4 v1, 0x0

    #@13
    .line 108
    :goto_13
    return-object v1

    #@14
    .line 104
    :cond_14
    const-string v1, "IMS"

    #@16
    invoke-virtual {v0, v2, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@19
    move-result-object v1

    #@1a
    sput-object v1, Lcom/lge/ims/ImsWakeLock;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@1c
    .line 105
    sget-object v1, Lcom/lge/ims/ImsWakeLock;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@1e
    invoke-virtual {v1, v2}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    #@21
    .line 108
    :cond_21
    sget-object v1, Lcom/lge/ims/ImsWakeLock;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@23
    goto :goto_13
.end method
