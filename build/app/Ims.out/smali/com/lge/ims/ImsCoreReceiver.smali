.class public Lcom/lge/ims/ImsCoreReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ImsCoreReceiver.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ImsCoreReceiver"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 12
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 5
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 17
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    #@6
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_1e

    #@c
    .line 18
    const-string v0, "ImsCoreReceiver"

    #@e
    const-string v1, "ImsCoreReceiver received ACTION_BOOT_COMPLETED"

    #@10
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@13
    .line 19
    new-instance v0, Landroid/content/Intent;

    #@15
    const-class v1, Lcom/lge/ims/ImsService;

    #@17
    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    #@1a
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    #@1d
    .line 24
    :cond_1d
    :goto_1d
    return-void

    #@1e
    .line 20
    :cond_1e
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@21
    move-result-object v0

    #@22
    const-string v1, "com.lge.ims.action.START_SERVICE"

    #@24
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@27
    move-result v0

    #@28
    if-eqz v0, :cond_1d

    #@2a
    .line 21
    const-string v0, "ImsCoreReceiver"

    #@2c
    const-string v1, "ImsCoreReceiver received START_SERVICE"

    #@2e
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@31
    .line 22
    new-instance v0, Landroid/content/Intent;

    #@33
    const-class v1, Lcom/lge/ims/ImsService;

    #@35
    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    #@38
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    #@3b
    goto :goto_1d
.end method
