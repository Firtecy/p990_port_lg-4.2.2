.class public Lcom/lge/ims/PhoneSettingsTracker;
.super Landroid/database/ContentObserver;
.source "PhoneSettingsTracker.java"


# static fields
.field private static final AUTHORITY:Ljava/lang/String; = "com.android.phone.CallSettingsProvider"

.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final KEY_NAME:Ljava/lang/String; = "name"

.field public static final KEY_VALUE_INT:Ljava/lang/String; = "value_int"

.field private static final KT_4G_NETWORK_SETTING:Ljava/lang/String; = "KT_4G_network_setting"

.field private static final KT_HD_VOICE_SETTING:Ljava/lang/String; = "KT_hd_voice_setting"

.field private static final TAG:Ljava/lang/String; = "PhoneSettingsTracker"

.field private static mContentObserver:Landroid/database/ContentObserver;

.field private static mPhonesettingsTracker:Lcom/lge/ims/PhoneSettingsTracker;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mVoLTEsetRegistrants:Landroid/os/RegistrantList;

.field public nHDsetValue:I

.field public nNetworkSetValue:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 18
    const-string v0, "content://com.android.phone.CallSettingsProvider/callsettings"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/lge/ims/PhoneSettingsTracker;->CONTENT_URI:Landroid/net/Uri;

    #@8
    .line 26
    new-instance v0, Lcom/lge/ims/PhoneSettingsTracker;

    #@a
    invoke-direct {v0}, Lcom/lge/ims/PhoneSettingsTracker;-><init>()V

    #@d
    sput-object v0, Lcom/lge/ims/PhoneSettingsTracker;->mPhonesettingsTracker:Lcom/lge/ims/PhoneSettingsTracker;

    #@f
    return-void
.end method

.method private constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 35
    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    #@5
    .line 28
    iput-object v0, p0, Lcom/lge/ims/PhoneSettingsTracker;->mContext:Landroid/content/Context;

    #@7
    .line 29
    new-instance v0, Landroid/os/RegistrantList;

    #@9
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@c
    iput-object v0, p0, Lcom/lge/ims/PhoneSettingsTracker;->mVoLTEsetRegistrants:Landroid/os/RegistrantList;

    #@e
    .line 31
    iput v1, p0, Lcom/lge/ims/PhoneSettingsTracker;->nHDsetValue:I

    #@10
    .line 32
    iput v1, p0, Lcom/lge/ims/PhoneSettingsTracker;->nNetworkSetValue:I

    #@12
    .line 36
    const-string v0, "PhoneSettingsTracker"

    #@14
    const-string v1, "PhoneSettingsTracker is created"

    #@16
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@19
    .line 37
    return-void
.end method

.method static synthetic access$000(Lcom/lge/ims/PhoneSettingsTracker;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 14
    iget-object v0, p0, Lcom/lge/ims/PhoneSettingsTracker;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/lge/ims/PhoneSettingsTracker;)Landroid/os/RegistrantList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 14
    iget-object v0, p0, Lcom/lge/ims/PhoneSettingsTracker;->mVoLTEsetRegistrants:Landroid/os/RegistrantList;

    #@2
    return-object v0
.end method

.method public static getInstance()Lcom/lge/ims/PhoneSettingsTracker;
    .registers 1

    #@0
    .prologue
    .line 40
    sget-object v0, Lcom/lge/ims/PhoneSettingsTracker;->mPhonesettingsTracker:Lcom/lge/ims/PhoneSettingsTracker;

    #@2
    return-object v0
.end method

.method public static queryCallSettingValueByKey(Landroid/content/Context;Ljava/lang/String;)I
    .registers 11
    .parameter "context"
    .parameter "key"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 108
    new-instance v0, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v1, "name=\'"

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v0

    #@10
    const-string v1, "\'"

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v0

    #@16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v3

    #@1a
    .line 109
    .local v3, selection:Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1d
    move-result-object v0

    #@1e
    sget-object v1, Lcom/lge/ims/PhoneSettingsTracker;->CONTENT_URI:Landroid/net/Uri;

    #@20
    move-object v4, v2

    #@21
    move-object v5, v2

    #@22
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@25
    move-result-object v6

    #@26
    .line 110
    .local v6, c:Landroid/database/Cursor;
    const/4 v8, -0x1

    #@27
    .line 112
    .local v8, value:I
    if-eqz v6, :cond_44

    #@29
    .line 114
    :try_start_29
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@2c
    move-result v0

    #@2d
    if-eqz v0, :cond_40

    #@2f
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    #@32
    move-result v0

    #@33
    const/4 v1, 0x1

    #@34
    if-ne v0, v1, :cond_40

    #@36
    .line 115
    const-string v0, "value_int"

    #@38
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@3b
    move-result v0

    #@3c
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_3f
    .catchall {:try_start_29 .. :try_end_3f} :catchall_6c
    .catch Ljava/lang/Exception; {:try_start_29 .. :try_end_3f} :catch_5d

    #@3f
    move-result v8

    #@40
    .line 120
    :cond_40
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@43
    .line 121
    const/4 v6, 0x0

    #@44
    .line 124
    :cond_44
    :goto_44
    const-string v0, "PhoneSettingsTracker"

    #@46
    new-instance v1, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v2, "queryCallSettingValueByKey() : return value is "

    #@4d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v1

    #@51
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@54
    move-result-object v1

    #@55
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v1

    #@59
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@5c
    .line 125
    return v8

    #@5d
    .line 117
    :catch_5d
    move-exception v7

    #@5e
    .line 118
    .local v7, e:Ljava/lang/Exception;
    :try_start_5e
    const-string v0, "PhoneSettingsTracker"

    #@60
    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@63
    move-result-object v1

    #@64
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_67
    .catchall {:try_start_5e .. :try_end_67} :catchall_6c

    #@67
    .line 120
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@6a
    .line 121
    const/4 v6, 0x0

    #@6b
    .line 122
    goto :goto_44

    #@6c
    .line 120
    .end local v7           #e:Ljava/lang/Exception;
    :catchall_6c
    move-exception v0

    #@6d
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@70
    .line 121
    const/4 v6, 0x0

    #@71
    throw v0
.end method


# virtual methods
.method public destroy()V
    .registers 2

    #@0
    .prologue
    .line 44
    iget-object v0, p0, Lcom/lge/ims/PhoneSettingsTracker;->mContext:Landroid/content/Context;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 45
    invoke-virtual {p0}, Lcom/lge/ims/PhoneSettingsTracker;->unregisterObserver()V

    #@7
    .line 47
    :cond_7
    return-void
.end method

.method public isVoLTEUsedForHDVoice()Z
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 58
    const/4 v0, 0x0

    #@2
    .line 60
    .local v0, nVoLTEsetValue:Z
    iget-object v2, p0, Lcom/lge/ims/PhoneSettingsTracker;->mContext:Landroid/content/Context;

    #@4
    if-nez v2, :cond_f

    #@6
    .line 61
    const-string v2, "PhoneSettingsTracker"

    #@8
    const-string v3, "Context is null"

    #@a
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    move v1, v0

    #@e
    .line 73
    .end local v0           #nVoLTEsetValue:Z
    .local v1, nVoLTEsetValue:I
    :goto_e
    return v1

    #@f
    .line 65
    .end local v1           #nVoLTEsetValue:I
    .restart local v0       #nVoLTEsetValue:Z
    :cond_f
    iget-object v2, p0, Lcom/lge/ims/PhoneSettingsTracker;->mContext:Landroid/content/Context;

    #@11
    const-string v3, "KT_hd_voice_setting"

    #@13
    invoke-static {v2, v3}, Lcom/lge/ims/PhoneSettingsTracker;->queryCallSettingValueByKey(Landroid/content/Context;Ljava/lang/String;)I

    #@16
    move-result v2

    #@17
    iput v2, p0, Lcom/lge/ims/PhoneSettingsTracker;->nHDsetValue:I

    #@19
    .line 66
    iget-object v2, p0, Lcom/lge/ims/PhoneSettingsTracker;->mContext:Landroid/content/Context;

    #@1b
    const-string v3, "KT_4G_network_setting"

    #@1d
    invoke-static {v2, v3}, Lcom/lge/ims/PhoneSettingsTracker;->queryCallSettingValueByKey(Landroid/content/Context;Ljava/lang/String;)I

    #@20
    move-result v2

    #@21
    iput v2, p0, Lcom/lge/ims/PhoneSettingsTracker;->nNetworkSetValue:I

    #@23
    .line 67
    const-string v2, "PhoneSettingsTracker"

    #@25
    new-instance v3, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v4, "HD Setting Value : "

    #@2c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v3

    #@30
    iget v4, p0, Lcom/lge/ims/PhoneSettingsTracker;->nHDsetValue:I

    #@32
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35
    move-result-object v3

    #@36
    const-string v4, " Network Setting Value : "

    #@38
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v3

    #@3c
    iget v4, p0, Lcom/lge/ims/PhoneSettingsTracker;->nNetworkSetValue:I

    #@3e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@41
    move-result-object v3

    #@42
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v3

    #@46
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@49
    .line 69
    iget v2, p0, Lcom/lge/ims/PhoneSettingsTracker;->nHDsetValue:I

    #@4b
    if-ne v2, v5, :cond_52

    #@4d
    iget v2, p0, Lcom/lge/ims/PhoneSettingsTracker;->nNetworkSetValue:I

    #@4f
    if-ne v2, v5, :cond_52

    #@51
    .line 70
    const/4 v0, 0x1

    #@52
    :cond_52
    move v1, v0

    #@53
    .line 73
    .restart local v1       #nVoLTEsetValue:I
    goto :goto_e
.end method

.method public registerCallSettingObserver()V
    .registers 5

    #@0
    .prologue
    .line 83
    const-string v1, "PhoneSettingsTracker"

    #@2
    const-string v2, "registerCallSettingObserve"

    #@4
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 84
    sget-object v1, Lcom/lge/ims/PhoneSettingsTracker;->mContentObserver:Landroid/database/ContentObserver;

    #@9
    if-eqz v1, :cond_15

    #@b
    .line 85
    const-string v1, "PhoneSettingsTracker"

    #@d
    const-string v2, "already registed Observer"

    #@f
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@12
    .line 86
    invoke-virtual {p0}, Lcom/lge/ims/PhoneSettingsTracker;->unregisterObserver()V

    #@15
    .line 89
    :cond_15
    iget-object v1, p0, Lcom/lge/ims/PhoneSettingsTracker;->mContext:Landroid/content/Context;

    #@17
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1a
    move-result-object v0

    #@1b
    .line 90
    .local v0, cr:Landroid/content/ContentResolver;
    new-instance v1, Lcom/lge/ims/PhoneSettingsTracker$1;

    #@1d
    new-instance v2, Landroid/os/Handler;

    #@1f
    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    #@22
    invoke-direct {v1, p0, v2}, Lcom/lge/ims/PhoneSettingsTracker$1;-><init>(Lcom/lge/ims/PhoneSettingsTracker;Landroid/os/Handler;)V

    #@25
    sput-object v1, Lcom/lge/ims/PhoneSettingsTracker;->mContentObserver:Landroid/database/ContentObserver;

    #@27
    .line 104
    sget-object v1, Lcom/lge/ims/PhoneSettingsTracker;->CONTENT_URI:Landroid/net/Uri;

    #@29
    const/4 v2, 0x1

    #@2a
    sget-object v3, Lcom/lge/ims/PhoneSettingsTracker;->mContentObserver:Landroid/database/ContentObserver;

    #@2c
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@2f
    .line 105
    return-void
.end method

.method public registerForVoLTESettingsChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 76
    iget-object v0, p0, Lcom/lge/ims/PhoneSettingsTracker;->mVoLTEsetRegistrants:Landroid/os/RegistrantList;

    #@2
    new-instance v1, Landroid/os/Registrant;

    #@4
    invoke-direct {v1, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@7
    invoke-virtual {v0, v1}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 77
    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 49
    iput-object p1, p0, Lcom/lge/ims/PhoneSettingsTracker;->mContext:Landroid/content/Context;

    #@2
    .line 51
    iget-object v0, p0, Lcom/lge/ims/PhoneSettingsTracker;->mContext:Landroid/content/Context;

    #@4
    if-nez v0, :cond_7

    #@6
    .line 55
    :goto_6
    return-void

    #@7
    .line 54
    :cond_7
    invoke-virtual {p0}, Lcom/lge/ims/PhoneSettingsTracker;->registerCallSettingObserver()V

    #@a
    goto :goto_6
.end method

.method public unregisterObserver()V
    .registers 4

    #@0
    .prologue
    .line 129
    sget-object v1, Lcom/lge/ims/PhoneSettingsTracker;->mContentObserver:Landroid/database/ContentObserver;

    #@2
    if-nez v1, :cond_c

    #@4
    .line 130
    const-string v1, "PhoneSettingsTracker"

    #@6
    const-string v2, "already unregisted Observer"

    #@8
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@b
    .line 135
    :goto_b
    return-void

    #@c
    .line 133
    :cond_c
    iget-object v1, p0, Lcom/lge/ims/PhoneSettingsTracker;->mContext:Landroid/content/Context;

    #@e
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@11
    move-result-object v0

    #@12
    .line 134
    .local v0, cr:Landroid/content/ContentResolver;
    sget-object v1, Lcom/lge/ims/PhoneSettingsTracker;->mContentObserver:Landroid/database/ContentObserver;

    #@14
    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    #@17
    goto :goto_b
.end method

.method public unregisterVoLTESettingsChanged(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 79
    iget-object v0, p0, Lcom/lge/ims/PhoneSettingsTracker;->mVoLTEsetRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 80
    return-void
.end method
