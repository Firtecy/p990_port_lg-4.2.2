.class Lcom/lge/ims/BootupController$1;
.super Landroid/os/Handler;
.source "BootupController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/BootupController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/BootupController;


# direct methods
.method constructor <init>(Lcom/lge/ims/BootupController;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 107
    iput-object p1, p0, Lcom/lge/ims/BootupController$1;->this$0:Lcom/lge/ims/BootupController;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    .line 109
    const-string v0, "BootupController"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "msg : "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget v2, p1, Landroid/os/Message;->what:I

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@1a
    .line 111
    iget v0, p1, Landroid/os/Message;->what:I

    #@1c
    packed-switch v0, :pswitch_data_6a

    #@1f
    .line 154
    const-string v0, "BootupController"

    #@21
    new-instance v1, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v2, "invalid msg : "

    #@28
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v1

    #@2c
    iget v2, p1, Landroid/os/Message;->what:I

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v1

    #@36
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@39
    .line 158
    :goto_39
    return-void

    #@3a
    .line 114
    :pswitch_3a
    iget-object v0, p0, Lcom/lge/ims/BootupController$1;->this$0:Lcom/lge/ims/BootupController;

    #@3c
    invoke-static {v0}, Lcom/lge/ims/BootupController;->access$000(Lcom/lge/ims/BootupController;)V

    #@3f
    goto :goto_39

    #@40
    .line 119
    :pswitch_40
    iget-object v0, p0, Lcom/lge/ims/BootupController$1;->this$0:Lcom/lge/ims/BootupController;

    #@42
    invoke-static {v0}, Lcom/lge/ims/BootupController;->access$100(Lcom/lge/ims/BootupController;)V

    #@45
    goto :goto_39

    #@46
    .line 124
    :pswitch_46
    iget-object v0, p0, Lcom/lge/ims/BootupController$1;->this$0:Lcom/lge/ims/BootupController;

    #@48
    invoke-static {v0}, Lcom/lge/ims/BootupController;->access$200(Lcom/lge/ims/BootupController;)V

    #@4b
    goto :goto_39

    #@4c
    .line 129
    :pswitch_4c
    iget-object v0, p0, Lcom/lge/ims/BootupController$1;->this$0:Lcom/lge/ims/BootupController;

    #@4e
    invoke-static {v0}, Lcom/lge/ims/BootupController;->access$300(Lcom/lge/ims/BootupController;)V

    #@51
    goto :goto_39

    #@52
    .line 134
    :pswitch_52
    iget-object v0, p0, Lcom/lge/ims/BootupController$1;->this$0:Lcom/lge/ims/BootupController;

    #@54
    invoke-static {v0}, Lcom/lge/ims/BootupController;->access$400(Lcom/lge/ims/BootupController;)V

    #@57
    goto :goto_39

    #@58
    .line 139
    :pswitch_58
    iget-object v0, p0, Lcom/lge/ims/BootupController$1;->this$0:Lcom/lge/ims/BootupController;

    #@5a
    invoke-static {v0}, Lcom/lge/ims/BootupController;->access$500(Lcom/lge/ims/BootupController;)V

    #@5d
    goto :goto_39

    #@5e
    .line 144
    :pswitch_5e
    iget-object v0, p0, Lcom/lge/ims/BootupController$1;->this$0:Lcom/lge/ims/BootupController;

    #@60
    invoke-static {v0}, Lcom/lge/ims/BootupController;->access$600(Lcom/lge/ims/BootupController;)V

    #@63
    goto :goto_39

    #@64
    .line 149
    :pswitch_64
    iget-object v0, p0, Lcom/lge/ims/BootupController$1;->this$0:Lcom/lge/ims/BootupController;

    #@66
    invoke-static {v0}, Lcom/lge/ims/BootupController;->access$700(Lcom/lge/ims/BootupController;)V

    #@69
    goto :goto_39

    #@6a
    .line 111
    :pswitch_data_6a
    .packed-switch 0x65
        :pswitch_3a
        :pswitch_40
        :pswitch_46
        :pswitch_4c
        :pswitch_52
        :pswitch_58
        :pswitch_5e
        :pswitch_64
    .end packed-switch
.end method
