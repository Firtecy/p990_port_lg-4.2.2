.class final Lcom/lge/ims/SysMonitor$SysMonitorHandler;
.super Landroid/os/Handler;
.source "SysMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/SysMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SysMonitorHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/SysMonitor;


# direct methods
.method private constructor <init>(Lcom/lge/ims/SysMonitor;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 489
    iput-object p1, p0, Lcom/lge/ims/SysMonitor$SysMonitorHandler;->this$0:Lcom/lge/ims/SysMonitor;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/lge/ims/SysMonitor;Lcom/lge/ims/SysMonitor$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 489
    invoke-direct {p0, p1}, Lcom/lge/ims/SysMonitor$SysMonitorHandler;-><init>(Lcom/lge/ims/SysMonitor;)V

    #@3
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 23
    .parameter "msg"

    #@0
    .prologue
    .line 492
    const-string v17, "SysMonitor"

    #@2
    new-instance v18, Ljava/lang/StringBuilder;

    #@4
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v19, "SysMonitorHandler - what="

    #@9
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v18

    #@d
    move-object/from16 v0, p1

    #@f
    iget v0, v0, Landroid/os/Message;->what:I

    #@11
    move/from16 v19, v0

    #@13
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v18

    #@17
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v18

    #@1b
    invoke-static/range {v17 .. v18}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@1e
    .line 494
    move-object/from16 v0, p1

    #@20
    iget v0, v0, Landroid/os/Message;->what:I

    #@22
    move/from16 v17, v0

    #@24
    sparse-switch v17, :sswitch_data_250

    #@27
    .line 753
    :cond_27
    :goto_27
    :sswitch_27
    return-void

    #@28
    .line 496
    :sswitch_28
    const/high16 v17, 0x1

    #@2a
    const/16 v18, 0x0

    #@2c
    const/16 v19, 0x0

    #@2e
    invoke-static/range {v17 .. v19}, Lcom/lge/ims/SysMonitor;->nativeOnEventReceived(III)I

    #@31
    goto :goto_27

    #@32
    .line 503
    :sswitch_32
    move-object/from16 v0, p0

    #@34
    iget-object v0, v0, Lcom/lge/ims/SysMonitor$SysMonitorHandler;->this$0:Lcom/lge/ims/SysMonitor;

    #@36
    move-object/from16 v17, v0

    #@38
    invoke-static/range {v17 .. v17}, Lcom/lge/ims/SysMonitor;->access$100(Lcom/lge/ims/SysMonitor;)Z

    #@3b
    move-result v17

    #@3c
    if-eqz v17, :cond_27

    #@3e
    .line 504
    move-object/from16 v0, p0

    #@40
    iget-object v0, v0, Lcom/lge/ims/SysMonitor$SysMonitorHandler;->this$0:Lcom/lge/ims/SysMonitor;

    #@42
    move-object/from16 v17, v0

    #@44
    const/16 v18, 0x0

    #@46
    invoke-static/range {v17 .. v18}, Lcom/lge/ims/SysMonitor;->access$102(Lcom/lge/ims/SysMonitor;Z)Z

    #@49
    .line 505
    const/16 v17, 0x8

    #@4b
    const/16 v18, 0x1

    #@4d
    const/16 v19, 0x0

    #@4f
    invoke-static/range {v17 .. v19}, Lcom/lge/ims/SysMonitor;->nativeOnEventReceived(III)I

    #@52
    goto :goto_27

    #@53
    .line 510
    :sswitch_53
    move-object/from16 v0, p0

    #@55
    iget-object v0, v0, Lcom/lge/ims/SysMonitor$SysMonitorHandler;->this$0:Lcom/lge/ims/SysMonitor;

    #@57
    move-object/from16 v17, v0

    #@59
    invoke-static/range {v17 .. v17}, Lcom/lge/ims/SysMonitor;->access$100(Lcom/lge/ims/SysMonitor;)Z

    #@5c
    move-result v17

    #@5d
    if-nez v17, :cond_27

    #@5f
    .line 511
    move-object/from16 v0, p0

    #@61
    iget-object v0, v0, Lcom/lge/ims/SysMonitor$SysMonitorHandler;->this$0:Lcom/lge/ims/SysMonitor;

    #@63
    move-object/from16 v17, v0

    #@65
    const/16 v18, 0x1

    #@67
    invoke-static/range {v17 .. v18}, Lcom/lge/ims/SysMonitor;->access$102(Lcom/lge/ims/SysMonitor;Z)Z

    #@6a
    .line 512
    const/16 v17, 0x8

    #@6c
    const/16 v18, 0x0

    #@6e
    const/16 v19, 0x0

    #@70
    invoke-static/range {v17 .. v19}, Lcom/lge/ims/SysMonitor;->nativeOnEventReceived(III)I

    #@73
    goto :goto_27

    #@74
    .line 538
    :sswitch_74
    move-object/from16 v0, p1

    #@76
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@78
    check-cast v3, Landroid/os/AsyncResult;

    #@7a
    .line 540
    .local v3, ar:Landroid/os/AsyncResult;
    if-eqz v3, :cond_27

    #@7c
    .line 544
    iget-object v4, v3, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@7e
    check-cast v4, Ljava/lang/Integer;

    #@80
    .line 546
    .local v4, callState:Ljava/lang/Integer;
    if-eqz v4, :cond_27

    #@82
    .line 550
    const/16 v17, 0x20

    #@84
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    #@87
    move-result v18

    #@88
    const/16 v19, 0x0

    #@8a
    invoke-static/range {v17 .. v19}, Lcom/lge/ims/SysMonitor;->nativeOnEventReceived(III)I

    #@8d
    goto :goto_27

    #@8e
    .line 559
    .end local v3           #ar:Landroid/os/AsyncResult;
    .end local v4           #callState:Ljava/lang/Integer;
    :sswitch_8e
    move-object/from16 v0, p1

    #@90
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@92
    check-cast v3, Landroid/os/AsyncResult;

    #@94
    .line 561
    .restart local v3       #ar:Landroid/os/AsyncResult;
    if-eqz v3, :cond_27

    #@96
    .line 565
    iget-object v13, v3, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@98
    check-cast v13, Ljava/lang/Integer;

    #@9a
    .line 567
    .local v13, serviceState:Ljava/lang/Integer;
    if-eqz v13, :cond_27

    #@9c
    .line 571
    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    #@9f
    move-result v17

    #@a0
    const/16 v18, 0x0

    #@a2
    const/16 v19, 0x0

    #@a4
    invoke-static/range {v17 .. v19}, Lcom/lge/ims/SysMonitor;->nativeOnServiceStateChanged(IIZ)V

    #@a7
    goto :goto_27

    #@a8
    .line 577
    .end local v3           #ar:Landroid/os/AsyncResult;
    .end local v13           #serviceState:Ljava/lang/Integer;
    :sswitch_a8
    move-object/from16 v0, p1

    #@aa
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@ac
    check-cast v3, Landroid/os/AsyncResult;

    #@ae
    .line 579
    .restart local v3       #ar:Landroid/os/AsyncResult;
    if-eqz v3, :cond_27

    #@b0
    .line 583
    iget-object v11, v3, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@b2
    check-cast v11, Ljava/lang/Integer;

    #@b4
    .line 585
    .local v11, rat:Ljava/lang/Integer;
    if-eqz v11, :cond_27

    #@b6
    .line 589
    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    #@b9
    move-result v17

    #@ba
    invoke-static/range {v17 .. v17}, Lcom/lge/ims/SysMonitor;->nativeOnRadioTechStateChanged(I)V

    #@bd
    goto/16 :goto_27

    #@bf
    .line 595
    .end local v3           #ar:Landroid/os/AsyncResult;
    .end local v11           #rat:Ljava/lang/Integer;
    :sswitch_bf
    move-object/from16 v0, p1

    #@c1
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@c3
    check-cast v3, Landroid/os/AsyncResult;

    #@c5
    .line 597
    .restart local v3       #ar:Landroid/os/AsyncResult;
    if-eqz v3, :cond_27

    #@c7
    .line 601
    iget-object v12, v3, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@c9
    check-cast v12, Ljava/lang/Boolean;

    #@cb
    .line 603
    .local v12, roaming:Ljava/lang/Boolean;
    if-eqz v12, :cond_27

    #@cd
    .line 607
    invoke-virtual {v12}, Ljava/lang/Boolean;->booleanValue()Z

    #@d0
    move-result v17

    #@d1
    if-eqz v17, :cond_fa

    #@d3
    .line 608
    const/16 v17, 0x200

    #@d5
    const/16 v18, 0x1

    #@d7
    const/16 v19, 0x0

    #@d9
    invoke-static/range {v17 .. v19}, Lcom/lge/ims/SysMonitor;->nativeOnEventReceived(III)I

    #@dc
    .line 611
    invoke-static {}, Lcom/lge/ims/DataConnectionManager;->getInstance()Lcom/lge/ims/DataConnectionManager;

    #@df
    move-result-object v6

    #@e0
    .line 613
    .local v6, dcm:Lcom/lge/ims/DataConnectionManager;
    if-eqz v6, :cond_27

    #@e2
    invoke-virtual {v6}, Lcom/lge/ims/DataConnectionManager;->isMultiplePdnSupported()Z

    #@e5
    move-result v17

    #@e6
    if-nez v17, :cond_27

    #@e8
    .line 614
    const-string v17, "SysMonitor"

    #@ea
    const-string v18, "Network is roaming; Ims will transit to OUT_OF_SERVICE state"

    #@ec
    invoke-static/range {v17 .. v18}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@ef
    .line 615
    const/16 v17, 0x1

    #@f1
    const/16 v18, 0x0

    #@f3
    const/16 v19, 0x1

    #@f5
    invoke-static/range {v17 .. v19}, Lcom/lge/ims/SysMonitor;->nativeOnServiceStateChanged(IIZ)V

    #@f8
    goto/16 :goto_27

    #@fa
    .line 618
    .end local v6           #dcm:Lcom/lge/ims/DataConnectionManager;
    :cond_fa
    const/16 v17, 0x200

    #@fc
    const/16 v18, 0x0

    #@fe
    const/16 v19, 0x0

    #@100
    invoke-static/range {v17 .. v19}, Lcom/lge/ims/SysMonitor;->nativeOnEventReceived(III)I

    #@103
    goto/16 :goto_27

    #@105
    .line 631
    .end local v3           #ar:Landroid/os/AsyncResult;
    .end local v12           #roaming:Ljava/lang/Boolean;
    :sswitch_105
    move-object/from16 v0, p1

    #@107
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@109
    check-cast v3, Landroid/os/AsyncResult;

    #@10b
    .line 633
    .restart local v3       #ar:Landroid/os/AsyncResult;
    if-eqz v3, :cond_27

    #@10d
    .line 637
    iget-object v8, v3, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@10f
    check-cast v8, Ljava/lang/Boolean;

    #@111
    .line 639
    .local v8, mode:Ljava/lang/Boolean;
    if-eqz v8, :cond_27

    #@113
    .line 643
    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    #@116
    move-result v17

    #@117
    if-eqz v17, :cond_124

    #@119
    .line 644
    const/16 v17, 0x1

    #@11b
    const/16 v18, 0x1

    #@11d
    const/16 v19, 0x0

    #@11f
    invoke-static/range {v17 .. v19}, Lcom/lge/ims/SysMonitor;->nativeOnEventReceived(III)I

    #@122
    goto/16 :goto_27

    #@124
    .line 646
    :cond_124
    const/16 v17, 0x1

    #@126
    const/16 v18, 0x0

    #@128
    const/16 v19, 0x0

    #@12a
    invoke-static/range {v17 .. v19}, Lcom/lge/ims/SysMonitor;->nativeOnEventReceived(III)I

    #@12d
    goto/16 :goto_27

    #@12f
    .line 653
    .end local v3           #ar:Landroid/os/AsyncResult;
    .end local v8           #mode:Ljava/lang/Boolean;
    :sswitch_12f
    move-object/from16 v0, p1

    #@131
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@133
    check-cast v3, Landroid/os/AsyncResult;

    #@135
    .line 655
    .restart local v3       #ar:Landroid/os/AsyncResult;
    if-eqz v3, :cond_27

    #@137
    .line 659
    iget-object v7, v3, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@139
    check-cast v7, Ljava/lang/String;

    #@13b
    .line 661
    .local v7, iccState:Ljava/lang/String;
    if-eqz v7, :cond_27

    #@13d
    const-string v17, "LOADED"

    #@13f
    move-object/from16 v0, v17

    #@141
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@144
    move-result v17

    #@145
    if-eqz v17, :cond_27

    #@147
    .line 662
    const/16 v17, 0x10

    #@149
    const/16 v18, 0x0

    #@14b
    const/16 v19, 0x0

    #@14d
    invoke-static/range {v17 .. v19}, Lcom/lge/ims/SysMonitor;->nativeOnEventReceived(III)I

    #@150
    goto/16 :goto_27

    #@152
    .line 669
    .end local v3           #ar:Landroid/os/AsyncResult;
    .end local v7           #iccState:Ljava/lang/String;
    :sswitch_152
    move-object/from16 v0, p1

    #@154
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@156
    check-cast v3, Landroid/os/AsyncResult;

    #@158
    .line 671
    .restart local v3       #ar:Landroid/os/AsyncResult;
    if-eqz v3, :cond_27

    #@15a
    .line 675
    iget-object v5, v3, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@15c
    check-cast v5, Ljava/lang/Integer;

    #@15e
    .line 677
    .local v5, dataState:Ljava/lang/Integer;
    if-eqz v5, :cond_27

    #@160
    .line 681
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    #@163
    move-result v17

    #@164
    invoke-static/range {v17 .. v17}, Lcom/lge/ims/DataConnectionManager;->getApnTypeFromDataStateChanged(I)I

    #@167
    move-result v2

    #@168
    .line 682
    .local v2, apnType:I
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    #@16b
    move-result v17

    #@16c
    invoke-static/range {v17 .. v17}, Lcom/lge/ims/DataConnectionManager;->getDataStateFromDataStateChanged(I)I

    #@16f
    move-result v14

    #@170
    .line 684
    .local v14, state:I
    const-string v17, "SysMonitor"

    #@172
    new-instance v18, Ljava/lang/StringBuilder;

    #@174
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@177
    const-string v19, "DataStateChanged :: apnType="

    #@179
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17c
    move-result-object v18

    #@17d
    move-object/from16 v0, v18

    #@17f
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@182
    move-result-object v18

    #@183
    const-string v19, ", state="

    #@185
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@188
    move-result-object v18

    #@189
    move-object/from16 v0, v18

    #@18b
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18e
    move-result-object v18

    #@18f
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@192
    move-result-object v18

    #@193
    invoke-static/range {v17 .. v18}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@196
    .line 686
    const/16 v17, 0x2

    #@198
    move/from16 v0, v17

    #@19a
    if-ne v14, v0, :cond_1a1

    #@19c
    .line 687
    invoke-static {v2}, Lcom/lge/ims/SysMonitor;->nativeDataConnectionFailed(I)V

    #@19f
    goto/16 :goto_27

    #@1a1
    .line 691
    :cond_1a1
    const/16 v17, 0x2

    #@1a3
    move/from16 v0, v17

    #@1a5
    if-ne v2, v0, :cond_1c5

    #@1a7
    .line 692
    invoke-static {}, Lcom/lge/ims/DataConnectionManager;->getInstance()Lcom/lge/ims/DataConnectionManager;

    #@1aa
    move-result-object v17

    #@1ab
    invoke-virtual/range {v17 .. v17}, Lcom/lge/ims/DataConnectionManager;->isMultipleApnSupported()Z

    #@1ae
    move-result v17

    #@1af
    if-nez v17, :cond_1b6

    #@1b1
    .line 693
    invoke-static {v2, v14}, Lcom/lge/ims/SysMonitor;->nativeOnDataConnectionStateChanged(II)V

    #@1b4
    goto/16 :goto_27

    #@1b6
    .line 695
    :cond_1b6
    invoke-static {v2}, Lcom/lge/ims/DataConnectionManager;->getApnName(I)Ljava/lang/String;

    #@1b9
    move-result-object v17

    #@1ba
    invoke-static/range {v17 .. v17}, Lcom/lge/ims/Configuration;->isApnSupported(Ljava/lang/String;)Z

    #@1bd
    move-result v17

    #@1be
    if-eqz v17, :cond_27

    #@1c0
    .line 696
    invoke-static {v2, v14}, Lcom/lge/ims/SysMonitor;->nativeOnDataConnectionStateChanged(II)V

    #@1c3
    goto/16 :goto_27

    #@1c5
    .line 699
    :cond_1c5
    const/16 v17, 0x1

    #@1c7
    move/from16 v0, v17

    #@1c9
    if-ne v2, v0, :cond_1d0

    #@1cb
    .line 700
    invoke-static {v2, v14}, Lcom/lge/ims/SysMonitor;->nativeOnDataConnectionStateChanged(II)V

    #@1ce
    goto/16 :goto_27

    #@1d0
    .line 701
    :cond_1d0
    const/16 v17, 0x9

    #@1d2
    move/from16 v0, v17

    #@1d4
    if-ne v2, v0, :cond_27

    #@1d6
    .line 702
    invoke-static {v2, v14}, Lcom/lge/ims/SysMonitor;->nativeOnDataConnectionStateChanged(II)V

    #@1d9
    goto/16 :goto_27

    #@1db
    .line 709
    .end local v2           #apnType:I
    .end local v3           #ar:Landroid/os/AsyncResult;
    .end local v5           #dataState:Ljava/lang/Integer;
    .end local v14           #state:I
    :sswitch_1db
    invoke-static {}, Lcom/lge/ims/PhoneSettingsTracker;->getInstance()Lcom/lge/ims/PhoneSettingsTracker;

    #@1de
    move-result-object v10

    #@1df
    .line 710
    .local v10, psTracker:Lcom/lge/ims/PhoneSettingsTracker;
    invoke-virtual {v10}, Lcom/lge/ims/PhoneSettingsTracker;->isVoLTEUsedForHDVoice()Z

    #@1e2
    move-result v17

    #@1e3
    const/16 v18, 0x1

    #@1e5
    move/from16 v0, v17

    #@1e7
    move/from16 v1, v18

    #@1e9
    if-ne v0, v1, :cond_1f6

    #@1eb
    .line 711
    const/high16 v17, 0x40

    #@1ed
    const/16 v18, 0x1

    #@1ef
    const/16 v19, 0x0

    #@1f1
    invoke-static/range {v17 .. v19}, Lcom/lge/ims/SysMonitor;->sendEventToNative(III)I

    #@1f4
    goto/16 :goto_27

    #@1f6
    .line 714
    :cond_1f6
    const/high16 v17, 0x40

    #@1f8
    const/16 v18, 0x0

    #@1fa
    const/16 v19, 0x0

    #@1fc
    invoke-static/range {v17 .. v19}, Lcom/lge/ims/SysMonitor;->sendEventToNative(III)I

    #@1ff
    goto/16 :goto_27

    #@201
    .line 721
    .end local v10           #psTracker:Lcom/lge/ims/PhoneSettingsTracker;
    :sswitch_201
    move-object/from16 v0, p1

    #@203
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@205
    check-cast v3, Landroid/os/AsyncResult;

    #@207
    .line 723
    .restart local v3       #ar:Landroid/os/AsyncResult;
    if-eqz v3, :cond_27

    #@209
    .line 727
    iget-object v15, v3, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@20b
    check-cast v15, Lcom/lge/ims/service/ac/ACUpdateState;

    #@20d
    .line 729
    .local v15, updateState:Lcom/lge/ims/service/ac/ACUpdateState;
    if-eqz v15, :cond_27

    #@20f
    .line 733
    invoke-virtual {v15}, Lcom/lge/ims/service/ac/ACUpdateState;->getProvisionedServices()I

    #@212
    move-result v9

    #@213
    .line 734
    .local v9, provisionedServices:I
    invoke-virtual {v15}, Lcom/lge/ims/service/ac/ACUpdateState;->getUpdatedImsTables()I

    #@216
    move-result v16

    #@217
    .line 736
    .local v16, updatedTables:I
    const/16 v17, -0x1

    #@219
    move/from16 v0, v17

    #@21b
    if-eq v9, v0, :cond_229

    #@21d
    .line 737
    const v17, 0x8000

    #@220
    const/16 v18, 0x2

    #@222
    move/from16 v0, v17

    #@224
    move/from16 v1, v18

    #@226
    invoke-static {v0, v1, v9}, Lcom/lge/ims/SysMonitor;->nativeOnEventReceived(III)I

    #@229
    .line 741
    :cond_229
    const/16 v17, -0x1

    #@22b
    move/from16 v0, v16

    #@22d
    move/from16 v1, v17

    #@22f
    if-eq v0, v1, :cond_23c

    #@231
    .line 742
    const/16 v17, 0x40

    #@233
    const/high16 v18, 0x66

    #@235
    or-int v18, v18, v16

    #@237
    const/16 v19, 0x0

    #@239
    invoke-static/range {v17 .. v19}, Lcom/lge/ims/SysMonitor;->nativeOnEventReceived(III)I

    #@23c
    .line 746
    :cond_23c
    move-object/from16 v0, p0

    #@23e
    iget-object v0, v0, Lcom/lge/ims/SysMonitor$SysMonitorHandler;->this$0:Lcom/lge/ims/SysMonitor;

    #@240
    move-object/from16 v17, v0

    #@242
    invoke-static/range {v17 .. v17}, Lcom/lge/ims/SysMonitor;->access$300(Lcom/lge/ims/SysMonitor;)Lcom/lge/ims/SysMonitor$SysMonitorHandler;

    #@245
    move-result-object v17

    #@246
    const/16 v18, 0x65

    #@248
    const-wide/16 v19, 0x64

    #@24a
    invoke-virtual/range {v17 .. v20}, Lcom/lge/ims/SysMonitor$SysMonitorHandler;->sendEmptyMessageDelayed(IJ)Z

    #@24d
    goto/16 :goto_27

    #@24f
    .line 494
    nop

    #@250
    :sswitch_data_250
    .sparse-switch
        0x65 -> :sswitch_28
        0x3e9 -> :sswitch_27
        0x3ea -> :sswitch_53
        0x3eb -> :sswitch_32
        0x44d -> :sswitch_74
        0x44f -> :sswitch_8e
        0x450 -> :sswitch_a8
        0x451 -> :sswitch_bf
        0x454 -> :sswitch_105
        0x455 -> :sswitch_12f
        0x4b1 -> :sswitch_152
        0x515 -> :sswitch_1db
        0x835 -> :sswitch_201
    .end sparse-switch
.end method
