.class Lcom/lge/ims/SysMonitor$WiFiReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SysMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/SysMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WiFiReceiver"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mIsFilterRegistered:Z

.field final synthetic this$0:Lcom/lge/ims/SysMonitor;


# direct methods
.method public constructor <init>(Lcom/lge/ims/SysMonitor;Landroid/content/Context;)V
    .registers 4
    .parameter
    .parameter "context"

    #@0
    .prologue
    .line 312
    iput-object p1, p0, Lcom/lge/ims/SysMonitor$WiFiReceiver;->this$0:Lcom/lge/ims/SysMonitor;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    .line 309
    const/4 v0, 0x0

    #@6
    iput-object v0, p0, Lcom/lge/ims/SysMonitor$WiFiReceiver;->mContext:Landroid/content/Context;

    #@8
    .line 310
    const/4 v0, 0x0

    #@9
    iput-boolean v0, p0, Lcom/lge/ims/SysMonitor$WiFiReceiver;->mIsFilterRegistered:Z

    #@b
    .line 313
    iput-object p2, p0, Lcom/lge/ims/SysMonitor$WiFiReceiver;->mContext:Landroid/content/Context;

    #@d
    .line 314
    return-void
.end method


# virtual methods
.method public declared-synchronized onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 11
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 350
    monitor-enter p0

    #@1
    :try_start_1
    const-string v5, "SysMonitor"

    #@3
    new-instance v6, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v7, "onReceive()"

    #@a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v6

    #@e
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@11
    move-result-object v7

    #@12
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v6

    #@16
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v6

    #@1a
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@1d
    .line 354
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@20
    move-result-object v5

    #@21
    const-string v6, "android.net.wifi.STATE_CHANGE"

    #@23
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@26
    move-result v5

    #@27
    if-eqz v5, :cond_81

    #@29
    .line 355
    sget-object v1, Landroid/net/NetworkInfo$State;->UNKNOWN:Landroid/net/NetworkInfo$State;

    #@2b
    .line 356
    .local v1, eState:Landroid/net/NetworkInfo$State;
    sget-object v0, Landroid/net/NetworkInfo$DetailedState;->IDLE:Landroid/net/NetworkInfo$DetailedState;

    #@2d
    .line 358
    .local v0, eDetailedState:Landroid/net/NetworkInfo$DetailedState;
    const-string v5, "networkInfo"

    #@2f
    invoke-virtual {p2, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@32
    move-result-object v3

    #@33
    check-cast v3, Landroid/net/NetworkInfo;

    #@35
    .line 360
    .local v3, networkInfo:Landroid/net/NetworkInfo;
    if-nez v3, :cond_40

    #@37
    .line 361
    const-string v5, "SysMonitor"

    #@39
    const-string v6, "WifiManager.NETWORK_STATE_CHANGED_ACTION : networkinfo is NULL"

    #@3b
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3e
    .catchall {:try_start_1 .. :try_end_3e} :catchall_7e

    #@3e
    .line 409
    .end local v0           #eDetailedState:Landroid/net/NetworkInfo$DetailedState;
    .end local v1           #eState:Landroid/net/NetworkInfo$State;
    .end local v3           #networkInfo:Landroid/net/NetworkInfo;
    :cond_3e
    :goto_3e
    monitor-exit p0

    #@3f
    return-void

    #@40
    .line 363
    .restart local v0       #eDetailedState:Landroid/net/NetworkInfo$DetailedState;
    .restart local v1       #eState:Landroid/net/NetworkInfo$State;
    .restart local v3       #networkInfo:Landroid/net/NetworkInfo;
    :cond_40
    :try_start_40
    const-string v5, "SysMonitor"

    #@42
    new-instance v6, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v7, "WifiManager.NETWORK_STATE_CHANGED_ACTION : networkinfo ("

    #@49
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v6

    #@4d
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->toString()Ljava/lang/String;

    #@50
    move-result-object v7

    #@51
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v6

    #@55
    const-string v7, ")"

    #@57
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v6

    #@5b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v6

    #@5f
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@62
    .line 368
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    #@65
    move-result-object v1

    #@66
    .line 375
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    #@69
    move-result-object v0

    #@6a
    .line 376
    iget-object v5, p0, Lcom/lge/ims/SysMonitor$WiFiReceiver;->this$0:Lcom/lge/ims/SysMonitor;

    #@6c
    invoke-static {v0}, Lcom/lge/ims/SysMonitor;->getNetworkInfoDetailedState(Landroid/net/NetworkInfo$DetailedState;)I

    #@6f
    move-result v6

    #@70
    iput v6, v5, Lcom/lge/ims/SysMonitor;->mWifiDetailedStatus:I

    #@72
    .line 378
    invoke-static {v1}, Lcom/lge/ims/SysMonitor;->getNetworkInfoState(Landroid/net/NetworkInfo$State;)I

    #@75
    move-result v5

    #@76
    iget-object v6, p0, Lcom/lge/ims/SysMonitor$WiFiReceiver;->this$0:Lcom/lge/ims/SysMonitor;

    #@78
    iget v6, v6, Lcom/lge/ims/SysMonitor;->mWifiDetailedStatus:I

    #@7a
    invoke-static {v5, v6}, Lcom/lge/ims/SysMonitor;->nativeWifiNetworkStateChanged(II)V
    :try_end_7d
    .catchall {:try_start_40 .. :try_end_7d} :catchall_7e

    #@7d
    goto :goto_3e

    #@7e
    .line 350
    .end local v0           #eDetailedState:Landroid/net/NetworkInfo$DetailedState;
    .end local v1           #eState:Landroid/net/NetworkInfo$State;
    .end local v3           #networkInfo:Landroid/net/NetworkInfo;
    :catchall_7e
    move-exception v5

    #@7f
    monitor-exit p0

    #@80
    throw v5

    #@81
    .line 387
    :cond_81
    :try_start_81
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@84
    move-result-object v5

    #@85
    const-string v6, "android.net.wifi.WIFI_STATE_CHANGED"

    #@87
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8a
    move-result v5

    #@8b
    if-eqz v5, :cond_c3

    #@8d
    .line 389
    iget-object v5, p0, Lcom/lge/ims/SysMonitor$WiFiReceiver;->this$0:Lcom/lge/ims/SysMonitor;

    #@8f
    const-string v6, "wifi_state"

    #@91
    const/4 v7, 0x4

    #@92
    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@95
    move-result v6

    #@96
    iput v6, v5, Lcom/lge/ims/SysMonitor;->mWifiState:I

    #@98
    .line 391
    const-string v5, "SysMonitor"

    #@9a
    new-instance v6, Ljava/lang/StringBuilder;

    #@9c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@9f
    const-string v7, "WifiManager.WIFI_STATE_CHANGED_ACTION : nStatus ("

    #@a1
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v6

    #@a5
    iget-object v7, p0, Lcom/lge/ims/SysMonitor$WiFiReceiver;->this$0:Lcom/lge/ims/SysMonitor;

    #@a7
    iget v7, v7, Lcom/lge/ims/SysMonitor;->mWifiState:I

    #@a9
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v6

    #@ad
    const-string v7, ")"

    #@af
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b2
    move-result-object v6

    #@b3
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b6
    move-result-object v6

    #@b7
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@ba
    .line 393
    iget-object v5, p0, Lcom/lge/ims/SysMonitor$WiFiReceiver;->this$0:Lcom/lge/ims/SysMonitor;

    #@bc
    iget v5, v5, Lcom/lge/ims/SysMonitor;->mWifiState:I

    #@be
    invoke-static {v5}, Lcom/lge/ims/SysMonitor;->nativeWifiStateChanged(I)V

    #@c1
    goto/16 :goto_3e

    #@c3
    .line 394
    :cond_c3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@c6
    move-result-object v5

    #@c7
    const-string v6, "android.net.wifi.RSSI_CHANGED"

    #@c9
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@cc
    move-result v5

    #@cd
    if-eqz v5, :cond_3e

    #@cf
    .line 395
    const-string v5, "newRssi"

    #@d1
    const/16 v6, -0xc8

    #@d3
    invoke-virtual {p2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@d6
    move-result v4

    #@d7
    .line 396
    .local v4, newRssi:I
    mul-int/lit8 v4, v4, -0x1

    #@d9
    .line 398
    const/16 v5, 0x800

    #@db
    const/4 v6, 0x0

    #@dc
    invoke-static {v5, v4, v6}, Lcom/lge/ims/SysMonitor;->nativeOnEventReceived(III)I

    #@df
    move-result v2

    #@e0
    .line 400
    .local v2, nResult:I
    const-string v5, "SysMonitor"

    #@e2
    new-instance v6, Ljava/lang/StringBuilder;

    #@e4
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@e7
    const-string v7, "WifiManager.RSSI_CHANGED_ACTION : ["

    #@e9
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ec
    move-result-object v6

    #@ed
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f0
    move-result-object v6

    #@f1
    const-string v7, "], and Event Received Result is ["

    #@f3
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f6
    move-result-object v6

    #@f7
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@fa
    move-result-object v6

    #@fb
    const-string v7, "]"

    #@fd
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@100
    move-result-object v6

    #@101
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@104
    move-result-object v6

    #@105
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_108
    .catchall {:try_start_81 .. :try_end_108} :catchall_7e

    #@108
    goto/16 :goto_3e
.end method

.method public registerFilters()V
    .registers 3

    #@0
    .prologue
    .line 317
    iget-object v1, p0, Lcom/lge/ims/SysMonitor$WiFiReceiver;->mContext:Landroid/content/Context;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 336
    :cond_4
    :goto_4
    return-void

    #@5
    .line 321
    :cond_5
    iget-boolean v1, p0, Lcom/lge/ims/SysMonitor$WiFiReceiver;->mIsFilterRegistered:Z

    #@7
    if-nez v1, :cond_4

    #@9
    .line 325
    new-instance v0, Landroid/content/IntentFilter;

    #@b
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@e
    .line 327
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "android.net.wifi.STATE_CHANGE"

    #@10
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@13
    .line 328
    const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"

    #@15
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@18
    .line 329
    const-string v1, "android.net.wifi.RSSI_CHANGED"

    #@1a
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1d
    .line 333
    iget-object v1, p0, Lcom/lge/ims/SysMonitor$WiFiReceiver;->mContext:Landroid/content/Context;

    #@1f
    invoke-virtual {v1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@22
    .line 335
    const/4 v1, 0x1

    #@23
    iput-boolean v1, p0, Lcom/lge/ims/SysMonitor$WiFiReceiver;->mIsFilterRegistered:Z

    #@25
    goto :goto_4
.end method

.method public unregisterFilters()V
    .registers 2

    #@0
    .prologue
    .line 339
    iget-boolean v0, p0, Lcom/lge/ims/SysMonitor$WiFiReceiver;->mIsFilterRegistered:Z

    #@2
    if-eqz v0, :cond_10

    #@4
    .line 340
    iget-object v0, p0, Lcom/lge/ims/SysMonitor$WiFiReceiver;->mContext:Landroid/content/Context;

    #@6
    if-eqz v0, :cond_d

    #@8
    .line 341
    iget-object v0, p0, Lcom/lge/ims/SysMonitor$WiFiReceiver;->mContext:Landroid/content/Context;

    #@a
    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@d
    .line 344
    :cond_d
    const/4 v0, 0x0

    #@e
    iput-boolean v0, p0, Lcom/lge/ims/SysMonitor$WiFiReceiver;->mIsFilterRegistered:Z

    #@10
    .line 346
    :cond_10
    return-void
.end method
