.class public final Lcom/lge/ims/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final app_name:I = 0x7f060000

.field public static final ims_disabled:I = 0x7f06038e

.field public static final ims_enabled:I = 0x7f06038f

.field public static final ims_key_ac:I = 0x7f06012f

.field public static final ims_key_ac_availability:I = 0x7f060020

.field public static final ims_key_ac_commercial_network:I = 0x7f060028

.field public static final ims_key_ac_connection_server:I = 0x7f060026

.field public static final ims_key_ac_imsi:I = 0x7f060025

.field public static final ims_key_ac_phone_number:I = 0x7f060024

.field public static final ims_key_ac_server_selection:I = 0x7f060027

.field public static final ims_key_ac_subscriber:I = 0x7f060022

.field public static final ims_key_ac_testbed:I = 0x7f060029

.field public static final ims_key_ac_update_exception_list:I = 0x7f060021

.field public static final ims_key_ac_usim:I = 0x7f060023

.field public static final ims_key_acsettings:I = 0x7f06001f

.field public static final ims_key_admin_ac:I = 0x7f060116

.field public static final ims_key_admin_debug:I = 0x7f060115

.field public static final ims_key_admin_ims:I = 0x7f060112

.field public static final ims_key_admin_isim:I = 0x7f060113

.field public static final ims_key_admin_pcscf:I = 0x7f060120

.field public static final ims_key_admin_services:I = 0x7f060117

.field public static final ims_key_admin_testmode:I = 0x7f060118

.field public static final ims_key_admin_usim:I = 0x7f060114

.field public static final ims_key_aos:I = 0x7f060302

.field public static final ims_key_aos_connection:I = 0x7f060303

.field public static final ims_key_aos_connection_0:I = 0x7f060304

.field public static final ims_key_aos_connection_0_access_policy:I = 0x7f06030d

.field public static final ims_key_aos_connection_0_ip_version:I = 0x7f06030a

.field public static final ims_key_aos_connection_0_profile_name:I = 0x7f060307

.field public static final ims_key_aos_connection_0_service_in_time:I = 0x7f060310

.field public static final ims_key_aos_connection_0_service_out_time:I = 0x7f060313

.field public static final ims_key_aos_connection_1:I = 0x7f060305

.field public static final ims_key_aos_connection_1_access_policy:I = 0x7f06030e

.field public static final ims_key_aos_connection_1_ip_version:I = 0x7f06030b

.field public static final ims_key_aos_connection_1_profile_name:I = 0x7f060308

.field public static final ims_key_aos_connection_1_service_in_time:I = 0x7f060311

.field public static final ims_key_aos_connection_1_service_out_time:I = 0x7f060314

.field public static final ims_key_aos_connection_2:I = 0x7f060306

.field public static final ims_key_aos_connection_2_access_policy:I = 0x7f06030f

.field public static final ims_key_aos_connection_2_ip_version:I = 0x7f06030c

.field public static final ims_key_aos_connection_2_profile_name:I = 0x7f060309

.field public static final ims_key_aos_connection_2_service_in_time:I = 0x7f060312

.field public static final ims_key_aos_connection_2_service_out_time:I = 0x7f060315

.field public static final ims_key_aos_reg:I = 0x7f060316

.field public static final ims_key_aos_reg_0:I = 0x7f060317

.field public static final ims_key_aos_reg_0_flow_id:I = 0x7f06031b

.field public static final ims_key_aos_reg_0_ipsec:I = 0x7f06031e

.field public static final ims_key_aos_reg_0_retry_interval:I = 0x7f06031c

.field public static final ims_key_aos_reg_0_retry_repeat_interval:I = 0x7f06031d

.field public static final ims_key_aos_reg_0_type:I = 0x7f06031a

.field public static final ims_key_aos_reg_1:I = 0x7f060318

.field public static final ims_key_aos_reg_1_flow_id:I = 0x7f060320

.field public static final ims_key_aos_reg_1_ipsec:I = 0x7f060323

.field public static final ims_key_aos_reg_1_retry_interval:I = 0x7f060321

.field public static final ims_key_aos_reg_1_retry_repeat_interval:I = 0x7f060322

.field public static final ims_key_aos_reg_1_type:I = 0x7f06031f

.field public static final ims_key_aos_reg_2:I = 0x7f060319

.field public static final ims_key_aos_reg_2_flow_id:I = 0x7f060325

.field public static final ims_key_aos_reg_2_ipsec:I = 0x7f060328

.field public static final ims_key_aos_reg_2_retry_interval:I = 0x7f060326

.field public static final ims_key_aos_reg_2_retry_repeat_interval:I = 0x7f060327

.field public static final ims_key_aos_reg_2_type:I = 0x7f060324

.field public static final ims_key_application:I = 0x7f0602ea

.field public static final ims_key_application_client_object_data_limit:I = 0x7f0602f3

.field public static final ims_key_application_conf_factory_uri:I = 0x7f0602ec

.field public static final ims_key_application_conf_uri_template:I = 0x7f0602fe

.field public static final ims_key_application_content_server_uri:I = 0x7f0602f4

.field public static final ims_key_application_conv_history_func_uri:I = 0x7f0602ee

.field public static final ims_key_application_deferred_msg_func_uri:I = 0x7f0602ef

.field public static final ims_key_application_exploder_uri:I = 0x7f0602ed

.field public static final ims_key_application_max_adhoc_group:I = 0x7f0602eb

.field public static final ims_key_application_max_backend_subscription:I = 0x7f0602f6

.field public static final ims_key_application_rls_list:I = 0x7f0602f8

.field public static final ims_key_application_service_uri_template:I = 0x7f0602f7

.field public static final ims_key_application_source_throttle_publish:I = 0x7f0602f5

.field public static final ims_key_auth_algorithm:I = 0x7f06012c

.field public static final ims_key_auth_password:I = 0x7f06012b

.field public static final ims_key_auth_realm:I = 0x7f06012a

.field public static final ims_key_auth_realm_leniency:I = 0x7f06012d

.field public static final ims_key_auth_username:I = 0x7f060129

.field public static final ims_key_common:I = 0x7f06010e

.field public static final ims_key_debug_screen:I = 0x7f06000f

.field public static final ims_key_default_settings:I = 0x7f06000d

.field public static final ims_key_engineering_service_code:I = 0x7f06000e

.field public static final ims_key_home_domain_name:I = 0x7f060121

.field public static final ims_key_http:I = 0x7f0602ff

.field public static final ims_key_http_server_info_proxy_address:I = 0x7f060300

.field public static final ims_key_http_server_info_proxy_port:I = 0x7f060301

.field public static final ims_key_im:I = 0x7f0602e9

.field public static final ims_key_im_application:I = 0x7f06035a

.field public static final ims_key_im_application_conf_factory_uri:I = 0x7f06035c

.field public static final ims_key_im_application_conv_history_func_uri:I = 0x7f06035e

.field public static final ims_key_im_application_deferred_msg_func_uri:I = 0x7f06035f

.field public static final ims_key_im_application_exploder_uri:I = 0x7f06035d

.field public static final ims_key_im_application_max_adhoc_group:I = 0x7f06035b

.field public static final ims_key_im_http:I = 0x7f060360

.field public static final ims_key_im_http_server_info_proxy_address:I = 0x7f060361

.field public static final ims_key_im_http_server_info_proxy_port:I = 0x7f060362

.field public static final ims_key_impi:I = 0x7f060122

.field public static final ims_key_impu:I = 0x7f060123

.field public static final ims_key_impu_0:I = 0x7f060125

.field public static final ims_key_impu_1:I = 0x7f060126

.field public static final ims_key_impu_2:I = 0x7f060127

.field public static final ims_key_impu_primary_ref_index:I = 0x7f060124

.field public static final ims_key_ims:I = 0x7f060111

.field public static final ims_key_ims_debug:I = 0x7f06010f

.field public static final ims_key_imsi:I = 0x7f060013

.field public static final ims_key_ip:I = 0x7f0602f0

.field public static final ims_key_ip_address:I = 0x7f060011

.field public static final ims_key_ip_address_wifi:I = 0x7f060012

.field public static final ims_key_ip_application:I = 0x7f060363

.field public static final ims_key_ip_application_client_object_data_limit:I = 0x7f060364

.field public static final ims_key_ip_application_content_server_uri:I = 0x7f060365

.field public static final ims_key_ip_application_max_backend_subscription:I = 0x7f060367

.field public static final ims_key_ip_application_rls_list:I = 0x7f060369

.field public static final ims_key_ip_application_service_uri_template:I = 0x7f060368

.field public static final ims_key_ip_application_source_throttle_publish:I = 0x7f060366

.field public static final ims_key_ippresence:I = 0x7f0602f1

.field public static final ims_key_ipxdm:I = 0x7f0602f2

.field public static final ims_key_ipxdm_application:I = 0x7f06036f

.field public static final ims_key_ipxdm_application_conf_uri_template:I = 0x7f060370

.field public static final ims_key_ipxdm_http:I = 0x7f060371

.field public static final ims_key_ipxdm_http_server_info_proxy_address:I = 0x7f060372

.field public static final ims_key_ipxdm_http_server_info_proxy_port:I = 0x7f060373

.field public static final ims_key_ipxdm_resource:I = 0x7f06036a

.field public static final ims_key_ipxdm_resource_auth_name:I = 0x7f06036c

.field public static final ims_key_ipxdm_resource_auth_secret:I = 0x7f06036d

.field public static final ims_key_ipxdm_resource_auth_type:I = 0x7f06036e

.field public static final ims_key_ipxdm_resource_root_uri:I = 0x7f06036b

.field public static final ims_key_media:I = 0x7f060162

.field public static final ims_key_media_audio_codec_volte:I = 0x7f060167

.field public static final ims_key_media_audio_codec_vt:I = 0x7f06016a

.field public static final ims_key_media_audio_common_volte:I = 0x7f060165

.field public static final ims_key_media_audio_common_vt:I = 0x7f060168

.field public static final ims_key_media_audio_loopback:I = 0x7f06016e

.field public static final ims_key_media_audio_sdp_volte:I = 0x7f060166

.field public static final ims_key_media_audio_sdp_vt:I = 0x7f060169

.field public static final ims_key_media_audio_volte_bw_as:I = 0x7f06017d

.field public static final ims_key_media_audio_volte_bw_mode:I = 0x7f06017c

.field public static final ims_key_media_audio_volte_bw_rr:I = 0x7f06017e

.field public static final ims_key_media_audio_volte_bw_rs:I = 0x7f06017f

.field public static final ims_key_media_audio_volte_candidate_priority:I = 0x7f060176

.field public static final ims_key_media_audio_volte_codec0:I = 0x7f0601a9

.field public static final ims_key_media_audio_volte_codec0_channel:I = 0x7f0601b7

.field public static final ims_key_media_audio_volte_codec0_codec_type:I = 0x7f0601af

.field public static final ims_key_media_audio_volte_codec0_max_ptime:I = 0x7f0601c0

.field public static final ims_key_media_audio_volte_codec0_max_red:I = 0x7f0601bd

.field public static final ims_key_media_audio_volte_codec0_mode_change_capability:I = 0x7f0601ba

.field public static final ims_key_media_audio_volte_codec0_mode_change_neighbor:I = 0x7f0601bc

.field public static final ims_key_media_audio_volte_codec0_mode_change_period:I = 0x7f0601bb

.field public static final ims_key_media_audio_volte_codec0_mode_set:I = 0x7f0601b9

.field public static final ims_key_media_audio_volte_codec0_network_type:I = 0x7f0601b6

.field public static final ims_key_media_audio_volte_codec0_octet_align:I = 0x7f0601b8

.field public static final ims_key_media_audio_volte_codec0_payload_type:I = 0x7f0601b5

.field public static final ims_key_media_audio_volte_codec0_ptime:I = 0x7f0601bf

.field public static final ims_key_media_audio_volte_codec0_robust_sorting:I = 0x7f0601be

.field public static final ims_key_media_audio_volte_codec1:I = 0x7f0601aa

.field public static final ims_key_media_audio_volte_codec1_channel:I = 0x7f0601c3

.field public static final ims_key_media_audio_volte_codec1_codec_type:I = 0x7f0601b0

.field public static final ims_key_media_audio_volte_codec1_max_ptime:I = 0x7f0601cc

.field public static final ims_key_media_audio_volte_codec1_max_red:I = 0x7f0601c9

.field public static final ims_key_media_audio_volte_codec1_mode_change_capability:I = 0x7f0601c6

.field public static final ims_key_media_audio_volte_codec1_mode_change_neighbor:I = 0x7f0601c8

.field public static final ims_key_media_audio_volte_codec1_mode_change_period:I = 0x7f0601c7

.field public static final ims_key_media_audio_volte_codec1_mode_set:I = 0x7f0601c5

.field public static final ims_key_media_audio_volte_codec1_network_type:I = 0x7f0601c2

.field public static final ims_key_media_audio_volte_codec1_octet_align:I = 0x7f0601c4

.field public static final ims_key_media_audio_volte_codec1_payload_type:I = 0x7f0601c1

.field public static final ims_key_media_audio_volte_codec1_ptime:I = 0x7f0601cb

.field public static final ims_key_media_audio_volte_codec1_robust_sorting:I = 0x7f0601ca

.field public static final ims_key_media_audio_volte_codec2:I = 0x7f0601ab

.field public static final ims_key_media_audio_volte_codec2_channel:I = 0x7f0601cf

.field public static final ims_key_media_audio_volte_codec2_codec_type:I = 0x7f0601b1

.field public static final ims_key_media_audio_volte_codec2_max_ptime:I = 0x7f0601d8

.field public static final ims_key_media_audio_volte_codec2_max_red:I = 0x7f0601d5

.field public static final ims_key_media_audio_volte_codec2_mode_change_capability:I = 0x7f0601d2

.field public static final ims_key_media_audio_volte_codec2_mode_change_neighbor:I = 0x7f0601d4

.field public static final ims_key_media_audio_volte_codec2_mode_change_period:I = 0x7f0601d3

.field public static final ims_key_media_audio_volte_codec2_mode_set:I = 0x7f0601d1

.field public static final ims_key_media_audio_volte_codec2_network_type:I = 0x7f0601ce

.field public static final ims_key_media_audio_volte_codec2_octet_align:I = 0x7f0601d0

.field public static final ims_key_media_audio_volte_codec2_payload_type:I = 0x7f0601cd

.field public static final ims_key_media_audio_volte_codec2_ptime:I = 0x7f0601d7

.field public static final ims_key_media_audio_volte_codec2_robust_sorting:I = 0x7f0601d6

.field public static final ims_key_media_audio_volte_codec3:I = 0x7f0601ac

.field public static final ims_key_media_audio_volte_codec3_channel:I = 0x7f0601db

.field public static final ims_key_media_audio_volte_codec3_codec_type:I = 0x7f0601b2

.field public static final ims_key_media_audio_volte_codec3_max_ptime:I = 0x7f0601e4

.field public static final ims_key_media_audio_volte_codec3_max_red:I = 0x7f0601e1

.field public static final ims_key_media_audio_volte_codec3_mode_change_capability:I = 0x7f0601de

.field public static final ims_key_media_audio_volte_codec3_mode_change_neighbor:I = 0x7f0601e0

.field public static final ims_key_media_audio_volte_codec3_mode_change_period:I = 0x7f0601df

.field public static final ims_key_media_audio_volte_codec3_mode_set:I = 0x7f0601dd

.field public static final ims_key_media_audio_volte_codec3_network_type:I = 0x7f0601da

.field public static final ims_key_media_audio_volte_codec3_octet_align:I = 0x7f0601dc

.field public static final ims_key_media_audio_volte_codec3_payload_type:I = 0x7f0601d9

.field public static final ims_key_media_audio_volte_codec3_ptime:I = 0x7f0601e3

.field public static final ims_key_media_audio_volte_codec3_robust_sorting:I = 0x7f0601e2

.field public static final ims_key_media_audio_volte_codec4:I = 0x7f0601ad

.field public static final ims_key_media_audio_volte_codec4_channel:I = 0x7f0601e7

.field public static final ims_key_media_audio_volte_codec4_codec_type:I = 0x7f0601b3

.field public static final ims_key_media_audio_volte_codec4_max_ptime:I = 0x7f0601f0

.field public static final ims_key_media_audio_volte_codec4_max_red:I = 0x7f0601ed

.field public static final ims_key_media_audio_volte_codec4_mode_change_capability:I = 0x7f0601ea

.field public static final ims_key_media_audio_volte_codec4_mode_change_neighbor:I = 0x7f0601ec

.field public static final ims_key_media_audio_volte_codec4_mode_change_period:I = 0x7f0601eb

.field public static final ims_key_media_audio_volte_codec4_mode_set:I = 0x7f0601e9

.field public static final ims_key_media_audio_volte_codec4_network_type:I = 0x7f0601e6

.field public static final ims_key_media_audio_volte_codec4_octet_align:I = 0x7f0601e8

.field public static final ims_key_media_audio_volte_codec4_payload_type:I = 0x7f0601e5

.field public static final ims_key_media_audio_volte_codec4_ptime:I = 0x7f0601ef

.field public static final ims_key_media_audio_volte_codec4_robust_sorting:I = 0x7f0601ee

.field public static final ims_key_media_audio_volte_codec5:I = 0x7f0601ae

.field public static final ims_key_media_audio_volte_codec5_channel:I = 0x7f0601f3

.field public static final ims_key_media_audio_volte_codec5_codec_type:I = 0x7f0601b4

.field public static final ims_key_media_audio_volte_codec5_max_ptime:I = 0x7f0601fc

.field public static final ims_key_media_audio_volte_codec5_max_red:I = 0x7f0601f9

.field public static final ims_key_media_audio_volte_codec5_mode_change_capability:I = 0x7f0601f6

.field public static final ims_key_media_audio_volte_codec5_mode_change_neighbor:I = 0x7f0601f8

.field public static final ims_key_media_audio_volte_codec5_mode_change_period:I = 0x7f0601f7

.field public static final ims_key_media_audio_volte_codec5_mode_set:I = 0x7f0601f5

.field public static final ims_key_media_audio_volte_codec5_network_type:I = 0x7f0601f2

.field public static final ims_key_media_audio_volte_codec5_octet_align:I = 0x7f0601f4

.field public static final ims_key_media_audio_volte_codec5_payload_type:I = 0x7f0601f1

.field public static final ims_key_media_audio_volte_codec5_ptime:I = 0x7f0601fb

.field public static final ims_key_media_audio_volte_codec5_robust_sorting:I = 0x7f0601fa

.field public static final ims_key_media_audio_volte_codecs:I = 0x7f060182

.field public static final ims_key_media_audio_volte_codecs_profile_num:I = 0x7f060183

.field public static final ims_key_media_audio_volte_jitter_buffer_size:I = 0x7f060177

.field public static final ims_key_media_audio_volte_max_ptime:I = 0x7f060181

.field public static final ims_key_media_audio_volte_pdp_profile_num:I = 0x7f06017b

.field public static final ims_key_media_audio_volte_port_rtcp:I = 0x7f06017a

.field public static final ims_key_media_audio_volte_port_rtp:I = 0x7f060179

.field public static final ims_key_media_audio_volte_ptime:I = 0x7f060180

.field public static final ims_key_media_audio_volte_rtcp_enable:I = 0x7f060170

.field public static final ims_key_media_audio_volte_scr_enable:I = 0x7f060175

.field public static final ims_key_media_audio_volte_sdp_answer_fullcapa:I = 0x7f060172

.field public static final ims_key_media_audio_volte_sdp_keep_m_line:I = 0x7f060174

.field public static final ims_key_media_audio_volte_sdp_reoffer_fullcapa:I = 0x7f060173

.field public static final ims_key_media_audio_volte_send_rtcp_bye:I = 0x7f060171

.field public static final ims_key_media_audio_volte_telephone_event_duration:I = 0x7f060178

.field public static final ims_key_media_audio_volte_tv_rtp_inactivity:I = 0x7f06016f

.field public static final ims_key_media_audio_vt_bw_as:I = 0x7f060192

.field public static final ims_key_media_audio_vt_bw_mode:I = 0x7f060191

.field public static final ims_key_media_audio_vt_bw_rr:I = 0x7f060193

.field public static final ims_key_media_audio_vt_bw_rs:I = 0x7f060194

.field public static final ims_key_media_audio_vt_candidate_priority:I = 0x7f06018b

.field public static final ims_key_media_audio_vt_codec0:I = 0x7f0601fd

.field public static final ims_key_media_audio_vt_codec0_channel:I = 0x7f06020b

.field public static final ims_key_media_audio_vt_codec0_codec_type:I = 0x7f060203

.field public static final ims_key_media_audio_vt_codec0_max_ptime:I = 0x7f060214

.field public static final ims_key_media_audio_vt_codec0_max_red:I = 0x7f060211

.field public static final ims_key_media_audio_vt_codec0_mode_change_capability:I = 0x7f06020e

.field public static final ims_key_media_audio_vt_codec0_mode_change_neighbor:I = 0x7f060210

.field public static final ims_key_media_audio_vt_codec0_mode_change_period:I = 0x7f06020f

.field public static final ims_key_media_audio_vt_codec0_mode_set:I = 0x7f06020d

.field public static final ims_key_media_audio_vt_codec0_network_type:I = 0x7f06020a

.field public static final ims_key_media_audio_vt_codec0_octet_align:I = 0x7f06020c

.field public static final ims_key_media_audio_vt_codec0_payload_type:I = 0x7f060209

.field public static final ims_key_media_audio_vt_codec0_ptime:I = 0x7f060213

.field public static final ims_key_media_audio_vt_codec0_robust_sorting:I = 0x7f060212

.field public static final ims_key_media_audio_vt_codec1:I = 0x7f0601fe

.field public static final ims_key_media_audio_vt_codec1_channel:I = 0x7f060217

.field public static final ims_key_media_audio_vt_codec1_codec_type:I = 0x7f060204

.field public static final ims_key_media_audio_vt_codec1_max_ptime:I = 0x7f060220

.field public static final ims_key_media_audio_vt_codec1_max_red:I = 0x7f06021d

.field public static final ims_key_media_audio_vt_codec1_mode_change_capability:I = 0x7f06021a

.field public static final ims_key_media_audio_vt_codec1_mode_change_neighbor:I = 0x7f06021c

.field public static final ims_key_media_audio_vt_codec1_mode_change_period:I = 0x7f06021b

.field public static final ims_key_media_audio_vt_codec1_mode_set:I = 0x7f060219

.field public static final ims_key_media_audio_vt_codec1_network_type:I = 0x7f060216

.field public static final ims_key_media_audio_vt_codec1_octet_align:I = 0x7f060218

.field public static final ims_key_media_audio_vt_codec1_payload_type:I = 0x7f060215

.field public static final ims_key_media_audio_vt_codec1_ptime:I = 0x7f06021f

.field public static final ims_key_media_audio_vt_codec1_robust_sorting:I = 0x7f06021e

.field public static final ims_key_media_audio_vt_codec2:I = 0x7f0601ff

.field public static final ims_key_media_audio_vt_codec2_channel:I = 0x7f060223

.field public static final ims_key_media_audio_vt_codec2_codec_type:I = 0x7f060205

.field public static final ims_key_media_audio_vt_codec2_max_ptime:I = 0x7f06022c

.field public static final ims_key_media_audio_vt_codec2_max_red:I = 0x7f060229

.field public static final ims_key_media_audio_vt_codec2_mode_change_capability:I = 0x7f060226

.field public static final ims_key_media_audio_vt_codec2_mode_change_neighbor:I = 0x7f060228

.field public static final ims_key_media_audio_vt_codec2_mode_change_period:I = 0x7f060227

.field public static final ims_key_media_audio_vt_codec2_mode_set:I = 0x7f060225

.field public static final ims_key_media_audio_vt_codec2_network_type:I = 0x7f060222

.field public static final ims_key_media_audio_vt_codec2_octet_align:I = 0x7f060224

.field public static final ims_key_media_audio_vt_codec2_payload_type:I = 0x7f060221

.field public static final ims_key_media_audio_vt_codec2_ptime:I = 0x7f06022b

.field public static final ims_key_media_audio_vt_codec2_robust_sorting:I = 0x7f06022a

.field public static final ims_key_media_audio_vt_codec3:I = 0x7f060200

.field public static final ims_key_media_audio_vt_codec3_channel:I = 0x7f06022f

.field public static final ims_key_media_audio_vt_codec3_codec_type:I = 0x7f060206

.field public static final ims_key_media_audio_vt_codec3_max_ptime:I = 0x7f060238

.field public static final ims_key_media_audio_vt_codec3_max_red:I = 0x7f060235

.field public static final ims_key_media_audio_vt_codec3_mode_change_capability:I = 0x7f060232

.field public static final ims_key_media_audio_vt_codec3_mode_change_neighbor:I = 0x7f060234

.field public static final ims_key_media_audio_vt_codec3_mode_change_period:I = 0x7f060233

.field public static final ims_key_media_audio_vt_codec3_mode_set:I = 0x7f060231

.field public static final ims_key_media_audio_vt_codec3_network_type:I = 0x7f06022e

.field public static final ims_key_media_audio_vt_codec3_octet_align:I = 0x7f060230

.field public static final ims_key_media_audio_vt_codec3_payload_type:I = 0x7f06022d

.field public static final ims_key_media_audio_vt_codec3_ptime:I = 0x7f060237

.field public static final ims_key_media_audio_vt_codec3_robust_sorting:I = 0x7f060236

.field public static final ims_key_media_audio_vt_codec4:I = 0x7f060201

.field public static final ims_key_media_audio_vt_codec4_channel:I = 0x7f06023b

.field public static final ims_key_media_audio_vt_codec4_codec_type:I = 0x7f060207

.field public static final ims_key_media_audio_vt_codec4_max_ptime:I = 0x7f060244

.field public static final ims_key_media_audio_vt_codec4_max_red:I = 0x7f060241

.field public static final ims_key_media_audio_vt_codec4_mode_change_capability:I = 0x7f06023e

.field public static final ims_key_media_audio_vt_codec4_mode_change_neighbor:I = 0x7f060240

.field public static final ims_key_media_audio_vt_codec4_mode_change_period:I = 0x7f06023f

.field public static final ims_key_media_audio_vt_codec4_mode_set:I = 0x7f06023d

.field public static final ims_key_media_audio_vt_codec4_network_type:I = 0x7f06023a

.field public static final ims_key_media_audio_vt_codec4_octet_align:I = 0x7f06023c

.field public static final ims_key_media_audio_vt_codec4_payload_type:I = 0x7f060239

.field public static final ims_key_media_audio_vt_codec4_ptime:I = 0x7f060243

.field public static final ims_key_media_audio_vt_codec4_robust_sorting:I = 0x7f060242

.field public static final ims_key_media_audio_vt_codec5:I = 0x7f060202

.field public static final ims_key_media_audio_vt_codec5_channel:I = 0x7f060247

.field public static final ims_key_media_audio_vt_codec5_codec_type:I = 0x7f060208

.field public static final ims_key_media_audio_vt_codec5_max_ptime:I = 0x7f060250

.field public static final ims_key_media_audio_vt_codec5_max_red:I = 0x7f06024d

.field public static final ims_key_media_audio_vt_codec5_mode_change_capability:I = 0x7f06024a

.field public static final ims_key_media_audio_vt_codec5_mode_change_neighbor:I = 0x7f06024c

.field public static final ims_key_media_audio_vt_codec5_mode_change_period:I = 0x7f06024b

.field public static final ims_key_media_audio_vt_codec5_mode_set:I = 0x7f060249

.field public static final ims_key_media_audio_vt_codec5_network_type:I = 0x7f060246

.field public static final ims_key_media_audio_vt_codec5_octet_align:I = 0x7f060248

.field public static final ims_key_media_audio_vt_codec5_payload_type:I = 0x7f060245

.field public static final ims_key_media_audio_vt_codec5_ptime:I = 0x7f06024f

.field public static final ims_key_media_audio_vt_codec5_robust_sorting:I = 0x7f06024e

.field public static final ims_key_media_audio_vt_codecs:I = 0x7f060197

.field public static final ims_key_media_audio_vt_codecs_profile_num:I = 0x7f060198

.field public static final ims_key_media_audio_vt_jitter_buffer_size:I = 0x7f06018c

.field public static final ims_key_media_audio_vt_max_ptime:I = 0x7f060196

.field public static final ims_key_media_audio_vt_pdp_profile_num:I = 0x7f060190

.field public static final ims_key_media_audio_vt_port_rtcp:I = 0x7f06018f

.field public static final ims_key_media_audio_vt_port_rtp:I = 0x7f06018e

.field public static final ims_key_media_audio_vt_ptime:I = 0x7f060195

.field public static final ims_key_media_audio_vt_rtcp_enable:I = 0x7f060185

.field public static final ims_key_media_audio_vt_scr_enable:I = 0x7f06018a

.field public static final ims_key_media_audio_vt_sdp_answer_fullcapa:I = 0x7f060187

.field public static final ims_key_media_audio_vt_sdp_keep_m_line:I = 0x7f060189

.field public static final ims_key_media_audio_vt_sdp_reoffer_fullcapa:I = 0x7f060188

.field public static final ims_key_media_audio_vt_send_rtcp_bye:I = 0x7f060186

.field public static final ims_key_media_audio_vt_telephone_event_duration:I = 0x7f06018d

.field public static final ims_key_media_audio_vt_tv_rtp_inactivity:I = 0x7f060184

.field public static final ims_key_media_carrier_audio_amr_be:I = 0x7f0602d0

.field public static final ims_key_media_carrier_audio_amr_be_modeset:I = 0x7f0602d5

.field public static final ims_key_media_carrier_audio_amr_be_payload_num:I = 0x7f0602d4

.field public static final ims_key_media_carrier_audio_amr_oa:I = 0x7f0602d1

.field public static final ims_key_media_carrier_audio_amr_oa_modeset:I = 0x7f0602d7

.field public static final ims_key_media_carrier_audio_amr_oa_payload_num:I = 0x7f0602d6

.field public static final ims_key_media_carrier_audio_amr_wb_be:I = 0x7f0602d2

.field public static final ims_key_media_carrier_audio_amr_wb_be_modeset:I = 0x7f0602d9

.field public static final ims_key_media_carrier_audio_amr_wb_be_payload_num:I = 0x7f0602d8

.field public static final ims_key_media_carrier_audio_amr_wb_oa:I = 0x7f0602d3

.field public static final ims_key_media_carrier_audio_amr_wb_oa_modeset:I = 0x7f0602db

.field public static final ims_key_media_carrier_audio_amr_wb_oa_payload_num:I = 0x7f0602da

.field public static final ims_key_media_carrier_audio_codecs:I = 0x7f0602ce

.field public static final ims_key_media_carrier_audio_pref_codec:I = 0x7f0602cc

.field public static final ims_key_media_carrier_audio_pref_codec_payload:I = 0x7f0602cd

.field public static final ims_key_media_carrier_psvt:I = 0x7f0602cb

.field public static final ims_key_media_carrier_video_codecs:I = 0x7f0602cf

.field public static final ims_key_media_carrier_video_h263_qcif:I = 0x7f0602df

.field public static final ims_key_media_carrier_video_h263_qcif_bandwidth:I = 0x7f0602e7

.field public static final ims_key_media_carrier_video_h263_qcif_bitrate:I = 0x7f0602e3

.field public static final ims_key_media_carrier_video_h264_qcif:I = 0x7f0602de

.field public static final ims_key_media_carrier_video_h264_qcif_bandwidth:I = 0x7f0602e6

.field public static final ims_key_media_carrier_video_h264_qcif_bitrate:I = 0x7f0602e2

.field public static final ims_key_media_carrier_video_h264_qvga:I = 0x7f0602dd

.field public static final ims_key_media_carrier_video_h264_qvga_bandwidth:I = 0x7f0602e5

.field public static final ims_key_media_carrier_video_h264_qvga_bitrate:I = 0x7f0602e1

.field public static final ims_key_media_carrier_video_h264_vga:I = 0x7f0602dc

.field public static final ims_key_media_carrier_video_h264_vga_bandwidth:I = 0x7f0602e4

.field public static final ims_key_media_carrier_video_h264_vga_bitrate:I = 0x7f0602e0

.field public static final ims_key_media_carrier_volte:I = 0x7f0602ca

.field public static final ims_key_media_operator:I = 0x7f0602c9

.field public static final ims_key_media_video_codec_vt:I = 0x7f06016d

.field public static final ims_key_media_video_common_vt:I = 0x7f06016b

.field public static final ims_key_media_video_enable_onscreen_debug_info_video:I = 0x7f0601a5

.field public static final ims_key_media_video_framerate:I = 0x7f0601a6

.field public static final ims_key_media_video_framerate_mode:I = 0x7f0601a4

.field public static final ims_key_media_video_sdp_vt:I = 0x7f06016c

.field public static final ims_key_media_video_send_periodic_sps_pps:I = 0x7f0601a7

.field public static final ims_key_media_video_vt_bw_as:I = 0x7f0601a1

.field public static final ims_key_media_video_vt_bw_mode:I = 0x7f0601a0

.field public static final ims_key_media_video_vt_bw_rr:I = 0x7f0601a2

.field public static final ims_key_media_video_vt_bw_rs:I = 0x7f0601a3

.field public static final ims_key_media_video_vt_candidate_priority:I = 0x7f0601a8

.field public static final ims_key_media_video_vt_codec0:I = 0x7f060251

.field public static final ims_key_media_video_vt_codec0_QCIF:I = 0x7f060262

.field public static final ims_key_media_video_vt_codec0_asvalue:I = 0x7f06025d

.field public static final ims_key_media_video_vt_codec0_bitrate:I = 0x7f06025c

.field public static final ims_key_media_video_vt_codec0_codec_type:I = 0x7f060257

.field public static final ims_key_media_video_vt_codec0_framerate:I = 0x7f06025b

.field public static final ims_key_media_video_vt_codec0_framesize:I = 0x7f06025e

.field public static final ims_key_media_video_vt_codec0_level:I = 0x7f060261

.field public static final ims_key_media_video_vt_codec0_max_br:I = 0x7f060269

.field public static final ims_key_media_video_vt_codec0_max_cpb:I = 0x7f060267

.field public static final ims_key_media_video_vt_codec0_max_dpb:I = 0x7f060268

.field public static final ims_key_media_video_vt_codec0_max_fs:I = 0x7f060266

.field public static final ims_key_media_video_vt_codec0_max_mbps:I = 0x7f060265

.field public static final ims_key_media_video_vt_codec0_network_type:I = 0x7f060259

.field public static final ims_key_media_video_vt_codec0_packetization_mode:I = 0x7f060263

.field public static final ims_key_media_video_vt_codec0_payload_type:I = 0x7f060258

.field public static final ims_key_media_video_vt_codec0_profile:I = 0x7f060260

.field public static final ims_key_media_video_vt_codec0_profile_level_id:I = 0x7f06025f

.field public static final ims_key_media_video_vt_codec0_resolution:I = 0x7f06025a

.field public static final ims_key_media_video_vt_codec0_sprop_parameter_sets:I = 0x7f060264

.field public static final ims_key_media_video_vt_codec1:I = 0x7f060252

.field public static final ims_key_media_video_vt_codec1_QCIF:I = 0x7f060275

.field public static final ims_key_media_video_vt_codec1_asvalue:I = 0x7f060270

.field public static final ims_key_media_video_vt_codec1_bitrate:I = 0x7f06026f

.field public static final ims_key_media_video_vt_codec1_codec_type:I = 0x7f06026a

.field public static final ims_key_media_video_vt_codec1_framerate:I = 0x7f06026e

.field public static final ims_key_media_video_vt_codec1_framesize:I = 0x7f060271

.field public static final ims_key_media_video_vt_codec1_level:I = 0x7f060274

.field public static final ims_key_media_video_vt_codec1_max_br:I = 0x7f06027c

.field public static final ims_key_media_video_vt_codec1_max_cpb:I = 0x7f06027a

.field public static final ims_key_media_video_vt_codec1_max_dpb:I = 0x7f06027b

.field public static final ims_key_media_video_vt_codec1_max_fs:I = 0x7f060279

.field public static final ims_key_media_video_vt_codec1_max_mbps:I = 0x7f060278

.field public static final ims_key_media_video_vt_codec1_network_type:I = 0x7f06026c

.field public static final ims_key_media_video_vt_codec1_packetization_mode:I = 0x7f060276

.field public static final ims_key_media_video_vt_codec1_payload_type:I = 0x7f06026b

.field public static final ims_key_media_video_vt_codec1_profile:I = 0x7f060273

.field public static final ims_key_media_video_vt_codec1_profile_level_id:I = 0x7f060272

.field public static final ims_key_media_video_vt_codec1_resolution:I = 0x7f06026d

.field public static final ims_key_media_video_vt_codec1_sprop_parameter_sets:I = 0x7f060277

.field public static final ims_key_media_video_vt_codec2:I = 0x7f060253

.field public static final ims_key_media_video_vt_codec2_QCIF:I = 0x7f060288

.field public static final ims_key_media_video_vt_codec2_asvalue:I = 0x7f060283

.field public static final ims_key_media_video_vt_codec2_bitrate:I = 0x7f060282

.field public static final ims_key_media_video_vt_codec2_codec_type:I = 0x7f06027d

.field public static final ims_key_media_video_vt_codec2_framerate:I = 0x7f060281

.field public static final ims_key_media_video_vt_codec2_framesize:I = 0x7f060284

.field public static final ims_key_media_video_vt_codec2_level:I = 0x7f060287

.field public static final ims_key_media_video_vt_codec2_max_br:I = 0x7f06028f

.field public static final ims_key_media_video_vt_codec2_max_cpb:I = 0x7f06028d

.field public static final ims_key_media_video_vt_codec2_max_dpb:I = 0x7f06028e

.field public static final ims_key_media_video_vt_codec2_max_fs:I = 0x7f06028c

.field public static final ims_key_media_video_vt_codec2_max_mbps:I = 0x7f06028b

.field public static final ims_key_media_video_vt_codec2_network_type:I = 0x7f06027f

.field public static final ims_key_media_video_vt_codec2_packetization_mode:I = 0x7f060289

.field public static final ims_key_media_video_vt_codec2_payload_type:I = 0x7f06027e

.field public static final ims_key_media_video_vt_codec2_profile:I = 0x7f060286

.field public static final ims_key_media_video_vt_codec2_profile_level_id:I = 0x7f060285

.field public static final ims_key_media_video_vt_codec2_resolution:I = 0x7f060280

.field public static final ims_key_media_video_vt_codec2_sprop_parameter_sets:I = 0x7f06028a

.field public static final ims_key_media_video_vt_codec3:I = 0x7f060254

.field public static final ims_key_media_video_vt_codec3_QCIF:I = 0x7f06029b

.field public static final ims_key_media_video_vt_codec3_asvalue:I = 0x7f060296

.field public static final ims_key_media_video_vt_codec3_bitrate:I = 0x7f060295

.field public static final ims_key_media_video_vt_codec3_codec_type:I = 0x7f060290

.field public static final ims_key_media_video_vt_codec3_framerate:I = 0x7f060294

.field public static final ims_key_media_video_vt_codec3_framesize:I = 0x7f060297

.field public static final ims_key_media_video_vt_codec3_level:I = 0x7f06029a

.field public static final ims_key_media_video_vt_codec3_max_br:I = 0x7f0602a2

.field public static final ims_key_media_video_vt_codec3_max_cpb:I = 0x7f0602a0

.field public static final ims_key_media_video_vt_codec3_max_dpb:I = 0x7f0602a1

.field public static final ims_key_media_video_vt_codec3_max_fs:I = 0x7f06029f

.field public static final ims_key_media_video_vt_codec3_max_mbps:I = 0x7f06029e

.field public static final ims_key_media_video_vt_codec3_network_type:I = 0x7f060292

.field public static final ims_key_media_video_vt_codec3_packetization_mode:I = 0x7f06029c

.field public static final ims_key_media_video_vt_codec3_payload_type:I = 0x7f060291

.field public static final ims_key_media_video_vt_codec3_profile:I = 0x7f060299

.field public static final ims_key_media_video_vt_codec3_profile_level_id:I = 0x7f060298

.field public static final ims_key_media_video_vt_codec3_resolution:I = 0x7f060293

.field public static final ims_key_media_video_vt_codec3_sprop_parameter_sets:I = 0x7f06029d

.field public static final ims_key_media_video_vt_codec4:I = 0x7f060255

.field public static final ims_key_media_video_vt_codec4_QCIF:I = 0x7f0602ae

.field public static final ims_key_media_video_vt_codec4_asvalue:I = 0x7f0602a9

.field public static final ims_key_media_video_vt_codec4_bitrate:I = 0x7f0602a8

.field public static final ims_key_media_video_vt_codec4_codec_type:I = 0x7f0602a3

.field public static final ims_key_media_video_vt_codec4_framerate:I = 0x7f0602a7

.field public static final ims_key_media_video_vt_codec4_framesize:I = 0x7f0602aa

.field public static final ims_key_media_video_vt_codec4_level:I = 0x7f0602ad

.field public static final ims_key_media_video_vt_codec4_max_br:I = 0x7f0602b5

.field public static final ims_key_media_video_vt_codec4_max_cpb:I = 0x7f0602b3

.field public static final ims_key_media_video_vt_codec4_max_dpb:I = 0x7f0602b4

.field public static final ims_key_media_video_vt_codec4_max_fs:I = 0x7f0602b2

.field public static final ims_key_media_video_vt_codec4_max_mbps:I = 0x7f0602b1

.field public static final ims_key_media_video_vt_codec4_network_type:I = 0x7f0602a5

.field public static final ims_key_media_video_vt_codec4_packetization_mode:I = 0x7f0602af

.field public static final ims_key_media_video_vt_codec4_payload_type:I = 0x7f0602a4

.field public static final ims_key_media_video_vt_codec4_profile:I = 0x7f0602ac

.field public static final ims_key_media_video_vt_codec4_profile_level_id:I = 0x7f0602ab

.field public static final ims_key_media_video_vt_codec4_resolution:I = 0x7f0602a6

.field public static final ims_key_media_video_vt_codec4_sprop_parameter_sets:I = 0x7f0602b0

.field public static final ims_key_media_video_vt_codec5:I = 0x7f060256

.field public static final ims_key_media_video_vt_codec5_QCIF:I = 0x7f0602c1

.field public static final ims_key_media_video_vt_codec5_asvalue:I = 0x7f0602bc

.field public static final ims_key_media_video_vt_codec5_bitrate:I = 0x7f0602bb

.field public static final ims_key_media_video_vt_codec5_codec_type:I = 0x7f0602b6

.field public static final ims_key_media_video_vt_codec5_framerate:I = 0x7f0602ba

.field public static final ims_key_media_video_vt_codec5_framesize:I = 0x7f0602bd

.field public static final ims_key_media_video_vt_codec5_level:I = 0x7f0602c0

.field public static final ims_key_media_video_vt_codec5_max_br:I = 0x7f0602c8

.field public static final ims_key_media_video_vt_codec5_max_cpb:I = 0x7f0602c6

.field public static final ims_key_media_video_vt_codec5_max_dpb:I = 0x7f0602c7

.field public static final ims_key_media_video_vt_codec5_max_fs:I = 0x7f0602c5

.field public static final ims_key_media_video_vt_codec5_max_mbps:I = 0x7f0602c4

.field public static final ims_key_media_video_vt_codec5_network_type:I = 0x7f0602b8

.field public static final ims_key_media_video_vt_codec5_packetization_mode:I = 0x7f0602c2

.field public static final ims_key_media_video_vt_codec5_payload_type:I = 0x7f0602b7

.field public static final ims_key_media_video_vt_codec5_profile:I = 0x7f0602bf

.field public static final ims_key_media_video_vt_codec5_profile_level_id:I = 0x7f0602be

.field public static final ims_key_media_video_vt_codec5_resolution:I = 0x7f0602b9

.field public static final ims_key_media_video_vt_codec5_sprop_parameter_sets:I = 0x7f0602c3

.field public static final ims_key_media_video_vt_port_rtp:I = 0x7f06019f

.field public static final ims_key_media_video_vt_rtcp_enable:I = 0x7f06019a

.field public static final ims_key_media_video_vt_sdp_answer_fullcapa:I = 0x7f06019c

.field public static final ims_key_media_video_vt_sdp_keep_m_line:I = 0x7f06019e

.field public static final ims_key_media_video_vt_sdp_reoffer_fullcapa:I = 0x7f06019d

.field public static final ims_key_media_video_vt_send_rtcp_bye:I = 0x7f060199

.field public static final ims_key_media_video_vt_tv_rtp_inactivity:I = 0x7f06019b

.field public static final ims_key_media_volte:I = 0x7f060163

.field public static final ims_key_media_vt:I = 0x7f060164

.field public static final ims_key_msisdn:I = 0x7f060014

.field public static final ims_key_pcscf:I = 0x7f060119

.field public static final ims_key_pcscf_address_0:I = 0x7f06011a

.field public static final ims_key_pcscf_address_1:I = 0x7f06011b

.field public static final ims_key_pcscf_address_2:I = 0x7f06011c

.field public static final ims_key_pcscf_port_0:I = 0x7f06011d

.field public static final ims_key_pcscf_port_1:I = 0x7f06011e

.field public static final ims_key_pcscf_port_2:I = 0x7f06011f

.field public static final ims_key_phone_context:I = 0x7f060128

.field public static final ims_key_preferred_id:I = 0x7f060150

.field public static final ims_key_provisioning:I = 0x7f06002b

.field public static final ims_key_rcse:I = 0x7f0602e8

.field public static final ims_key_reg_sub_expiration:I = 0x7f06014f

.field public static final ims_key_registration_duration:I = 0x7f06014d

.field public static final ims_key_registration_status:I = 0x7f060010

.field public static final ims_key_registration_subscription:I = 0x7f06014e

.field public static final ims_key_resource:I = 0x7f0602f9

.field public static final ims_key_resource_auth_name:I = 0x7f0602fb

.field public static final ims_key_resource_auth_secret:I = 0x7f0602fc

.field public static final ims_key_resource_auth_type:I = 0x7f0602fd

.field public static final ims_key_resource_root_uri:I = 0x7f0602fa

.field public static final ims_key_scscf:I = 0x7f06012e

.field public static final ims_key_service_onoff:I = 0x7f06015b

.field public static final ims_key_service_version:I = 0x7f06014c

.field public static final ims_key_session_conference_factory_uri:I = 0x7f060160

.field public static final ims_key_session_max_count:I = 0x7f06015c

.field public static final ims_key_session_rpr_supported:I = 0x7f060161

.field public static final ims_key_session_st_headers:I = 0x7f060158

.field public static final ims_key_session_st_method:I = 0x7f060157

.field public static final ims_key_session_st_minse:I = 0x7f060154

.field public static final ims_key_session_st_no_refresh_by_reinvite:I = 0x7f060159

.field public static final ims_key_session_st_refresher:I = 0x7f060156

.field public static final ims_key_session_st_session_expires:I = 0x7f060155

.field public static final ims_key_session_timer:I = 0x7f060153

.field public static final ims_key_session_tv_mo_1xx_wait:I = 0x7f06015e

.field public static final ims_key_session_tv_mo_no_answer:I = 0x7f06015d

.field public static final ims_key_session_tv_mt_alerting:I = 0x7f06015f

.field public static final ims_key_settings:I = 0x7f06000c

.field public static final ims_key_sip:I = 0x7f060130

.field public static final ims_key_sip_common:I = 0x7f060131

.field public static final ims_key_sip_compact_form:I = 0x7f060132

.field public static final ims_key_sip_device_id:I = 0x7f060135

.field public static final ims_key_sip_features:I = 0x7f060133

.field public static final ims_key_sip_listen_channel:I = 0x7f06013c

.field public static final ims_key_sip_listen_channel_port:I = 0x7f06013e

.field public static final ims_key_sip_listen_channel_scheme:I = 0x7f06013d

.field public static final ims_key_sip_listen_channel_transport:I = 0x7f06013f

.field public static final ims_key_sip_registration:I = 0x7f060137

.field public static final ims_key_sip_tag_prefix:I = 0x7f060134

.field public static final ims_key_sip_tcp_connection_timer:I = 0x7f060139

.field public static final ims_key_sip_tcp_criterion_len:I = 0x7f060136

.field public static final ims_key_sip_tcp_keepalive_timer:I = 0x7f06013a

.field public static final ims_key_sip_tcp_transport_timer:I = 0x7f060138

.field public static final ims_key_sip_tcp_wouldblock_timer:I = 0x7f06013b

.field public static final ims_key_sip_timer:I = 0x7f060140

.field public static final ims_key_sip_timer_t1:I = 0x7f060141

.field public static final ims_key_sip_timer_t2:I = 0x7f060142

.field public static final ims_key_sip_timer_tb:I = 0x7f060143

.field public static final ims_key_sip_timer_td:I = 0x7f060144

.field public static final ims_key_sip_timer_tf:I = 0x7f060145

.field public static final ims_key_sip_timer_th:I = 0x7f060146

.field public static final ims_key_sip_timer_ti:I = 0x7f060147

.field public static final ims_key_sip_timer_tj:I = 0x7f060148

.field public static final ims_key_sip_timer_tk:I = 0x7f060149

.field public static final ims_key_subscriber:I = 0x7f060110

.field public static final ims_key_sw_version:I = 0x7f06014b

.field public static final ims_key_target_number_format:I = 0x7f060152

.field public static final ims_key_target_scheme:I = 0x7f060151

.field public static final ims_key_test:I = 0x7f06015a

.field public static final ims_key_user_agent:I = 0x7f06014a

.field public static final ims_key_voip:I = 0x7f060346

.field public static final ims_key_voip_session:I = 0x7f060353

.field public static final ims_key_voip_session_conference_factory_uri:I = 0x7f060358

.field public static final ims_key_voip_session_expires:I = 0x7f060352

.field public static final ims_key_voip_session_max_count:I = 0x7f060354

.field public static final ims_key_voip_session_minse:I = 0x7f060351

.field public static final ims_key_voip_session_rpr_supported:I = 0x7f060359

.field public static final ims_key_voip_session_tv_mo_1xx_wait:I = 0x7f060356

.field public static final ims_key_voip_session_tv_mo_no_answer:I = 0x7f060355

.field public static final ims_key_voip_session_tv_mt_alerting:I = 0x7f060357

.field public static final ims_key_voip_sip:I = 0x7f060347

.field public static final ims_key_voip_sip_timer:I = 0x7f060348

.field public static final ims_key_voip_sip_timer_t1:I = 0x7f060349

.field public static final ims_key_voip_sip_timer_t2:I = 0x7f06034a

.field public static final ims_key_voip_sip_timer_tb:I = 0x7f06034b

.field public static final ims_key_voip_sip_timer_tf:I = 0x7f06034c

.field public static final ims_key_voip_sip_timer_th:I = 0x7f06034d

.field public static final ims_key_voip_target_number_format:I = 0x7f060350

.field public static final ims_key_voip_target_scheme:I = 0x7f06034f

.field public static final ims_key_voip_user_agent:I = 0x7f06034e

.field public static final ims_key_volte_debug_screen:I = 0x7f060015

.field public static final ims_key_vs:I = 0x7f060374

.field public static final ims_key_vs_media_codec:I = 0x7f060387

.field public static final ims_key_vs_media_codec_h263:I = 0x7f06038b

.field public static final ims_key_vs_media_codec_h264:I = 0x7f060388

.field public static final ims_key_vs_media_common:I = 0x7f06037c

.field public static final ims_key_vs_media_loopback:I = 0x7f06037d

.field public static final ims_key_vs_media_tv_heartbeat:I = 0x7f06037e

.field public static final ims_key_vs_media_video:I = 0x7f06037f

.field public static final ims_key_vs_media_video_bitrate:I = 0x7f060384

.field public static final ims_key_vs_media_video_codecs:I = 0x7f060386

.field public static final ims_key_vs_media_video_framerate:I = 0x7f060385

.field public static final ims_key_vs_media_video_h263_framesize:I = 0x7f06038c

.field public static final ims_key_vs_media_video_h263_level:I = 0x7f06038d

.field public static final ims_key_vs_media_video_h264_framesize:I = 0x7f060389

.field public static final ims_key_vs_media_video_h264_profile_level_id:I = 0x7f06038a

.field public static final ims_key_vs_media_video_jitter_buffer_size:I = 0x7f060383

.field public static final ims_key_vs_media_video_port_rtcp:I = 0x7f060381

.field public static final ims_key_vs_media_video_port_rtp:I = 0x7f060380

.field public static final ims_key_vs_media_video_tv_rtp_inactivity:I = 0x7f060382

.field public static final ims_key_vs_session:I = 0x7f060375

.field public static final ims_key_vs_session_conference_factory_uri:I = 0x7f06037a

.field public static final ims_key_vs_session_max_count:I = 0x7f060376

.field public static final ims_key_vs_session_rpr_supported:I = 0x7f06037b

.field public static final ims_key_vs_session_tv_mo_1xx_wait:I = 0x7f060378

.field public static final ims_key_vs_session_tv_mo_no_answer:I = 0x7f060377

.field public static final ims_key_vs_session_tv_mt_alerting:I = 0x7f060379

.field public static final ims_key_vt:I = 0x7f060329

.field public static final ims_key_vt_preferred_id:I = 0x7f060336

.field public static final ims_key_vt_session:I = 0x7f06033f

.field public static final ims_key_vt_session_conference_factory_uri:I = 0x7f060344

.field public static final ims_key_vt_session_max_count:I = 0x7f060340

.field public static final ims_key_vt_session_rpr_supported:I = 0x7f060345

.field public static final ims_key_vt_session_st_headers:I = 0x7f06033d

.field public static final ims_key_vt_session_st_method:I = 0x7f06033c

.field public static final ims_key_vt_session_st_minse:I = 0x7f060339

.field public static final ims_key_vt_session_st_no_refresh_by_reinvite:I = 0x7f06033e

.field public static final ims_key_vt_session_st_refresher:I = 0x7f06033b

.field public static final ims_key_vt_session_st_session_expires:I = 0x7f06033a

.field public static final ims_key_vt_session_tv_mo_1xx_wait:I = 0x7f060342

.field public static final ims_key_vt_session_tv_mo_no_answer:I = 0x7f060341

.field public static final ims_key_vt_session_tv_mt_alerting:I = 0x7f060343

.field public static final ims_key_vt_sip:I = 0x7f06032a

.field public static final ims_key_vt_sip_timer:I = 0x7f06032b

.field public static final ims_key_vt_sip_timer_t1:I = 0x7f06032c

.field public static final ims_key_vt_sip_timer_t2:I = 0x7f06032d

.field public static final ims_key_vt_sip_timer_tb:I = 0x7f06032e

.field public static final ims_key_vt_sip_timer_td:I = 0x7f06032f

.field public static final ims_key_vt_sip_timer_tf:I = 0x7f060330

.field public static final ims_key_vt_sip_timer_th:I = 0x7f060331

.field public static final ims_key_vt_sip_timer_ti:I = 0x7f060332

.field public static final ims_key_vt_sip_timer_tj:I = 0x7f060333

.field public static final ims_key_vt_sip_timer_tk:I = 0x7f060334

.field public static final ims_key_vt_target_number_format:I = 0x7f060338

.field public static final ims_key_vt_target_scheme:I = 0x7f060337

.field public static final ims_key_vt_user_agent:I = 0x7f060335

.field public static final ims_label_16K:I = 0x7f0600af

.field public static final ims_label_ac:I = 0x7f06004b

.field public static final ims_label_ac_availability:I = 0x7f060017

.field public static final ims_label_ac_update_exception_list:I = 0x7f060018

.field public static final ims_label_acsettings:I = 0x7f060016

.field public static final ims_label_address:I = 0x7f060039

.field public static final ims_label_admin_pcscf:I = 0x7f06003b

.field public static final ims_label_algorithm:I = 0x7f060048

.field public static final ims_label_amr:I = 0x7f0600ad

.field public static final ims_label_amr_wb:I = 0x7f0600ac

.field public static final ims_label_aos:I = 0x7f0600de

.field public static final ims_label_aos_connection:I = 0x7f0600df

.field public static final ims_label_aos_connection_0:I = 0x7f0600e0

.field public static final ims_label_aos_connection_1:I = 0x7f0600e1

.field public static final ims_label_aos_connection_2:I = 0x7f0600e2

.field public static final ims_label_aos_connection_access_policy:I = 0x7f0600e5

.field public static final ims_label_aos_connection_ip_version:I = 0x7f0600e4

.field public static final ims_label_aos_connection_profile_name:I = 0x7f0600e3

.field public static final ims_label_aos_connection_service_in_time:I = 0x7f0600e6

.field public static final ims_label_aos_connection_service_out_time:I = 0x7f0600e7

.field public static final ims_label_aos_reg:I = 0x7f0600e8

.field public static final ims_label_aos_reg_0:I = 0x7f0600e9

.field public static final ims_label_aos_reg_1:I = 0x7f0600ea

.field public static final ims_label_aos_reg_2:I = 0x7f0600eb

.field public static final ims_label_aos_reg_flow_id:I = 0x7f0600ed

.field public static final ims_label_aos_reg_ipsec:I = 0x7f0600f0

.field public static final ims_label_aos_reg_retry_interval:I = 0x7f0600ee

.field public static final ims_label_aos_reg_retry_repeat_interval:I = 0x7f0600ef

.field public static final ims_label_aos_reg_type:I = 0x7f0600ec

.field public static final ims_label_application:I = 0x7f0600f5

.field public static final ims_label_application_client_object_data_limit:I = 0x7f0600fd

.field public static final ims_label_application_conf_factory_uri:I = 0x7f0600f7

.field public static final ims_label_application_conf_uri_template:I = 0x7f060109

.field public static final ims_label_application_content_server_uri:I = 0x7f0600fe

.field public static final ims_label_application_conv_history_func_uri:I = 0x7f0600f9

.field public static final ims_label_application_deferred_msg_func_uri:I = 0x7f0600fa

.field public static final ims_label_application_exploder_uri:I = 0x7f0600f8

.field public static final ims_label_application_max_adhoc_group:I = 0x7f0600f6

.field public static final ims_label_application_max_backend_subscription:I = 0x7f060100

.field public static final ims_label_application_rls_list:I = 0x7f060102

.field public static final ims_label_application_service_uri_template:I = 0x7f060101

.field public static final ims_label_application_source_throttle_publish:I = 0x7f0600ff

.field public static final ims_label_commercial_network:I = 0x7f06001d

.field public static final ims_label_common:I = 0x7f06002d

.field public static final ims_label_connection_server:I = 0x7f06001b

.field public static final ims_label_credential:I = 0x7f060044

.field public static final ims_label_debug:I = 0x7f060032

.field public static final ims_label_debug_screen:I = 0x7f060006

.field public static final ims_label_default_settings:I = 0x7f060004

.field public static final ims_label_enabler_sip:I = 0x7f06006c

.field public static final ims_label_engineering_service_code:I = 0x7f060005

.field public static final ims_label_home_domain_name:I = 0x7f06003c

.field public static final ims_label_http:I = 0x7f06010a

.field public static final ims_label_http_server_info_proxy_address:I = 0x7f06010b

.field public static final ims_label_http_server_info_proxy_port:I = 0x7f06010c

.field public static final ims_label_im:I = 0x7f0600f4

.field public static final ims_label_impi:I = 0x7f06003d

.field public static final ims_label_impu:I = 0x7f06003e

.field public static final ims_label_impu_0:I = 0x7f060040

.field public static final ims_label_impu_1:I = 0x7f060041

.field public static final ims_label_impu_2:I = 0x7f060042

.field public static final ims_label_impu_primary_ref_index:I = 0x7f06003f

.field public static final ims_label_ims:I = 0x7f06002f

.field public static final ims_label_ims_debug:I = 0x7f06002c

.field public static final ims_label_imsi:I = 0x7f06001a

.field public static final ims_label_ip:I = 0x7f0600fb

.field public static final ims_label_ip_address:I = 0x7f060008

.field public static final ims_label_ip_address_wifi:I = 0x7f060009

.field public static final ims_label_isim:I = 0x7f060030

.field public static final ims_label_media:I = 0x7f060080

.field public static final ims_label_media_DTMF_Duration:I = 0x7f060093

.field public static final ims_label_media_audio_codec:I = 0x7f060087

.field public static final ims_label_media_audio_common:I = 0x7f060083

.field public static final ims_label_media_audio_pdp_profile:I = 0x7f0600a0

.field public static final ims_label_media_audio_sdp:I = 0x7f060085

.field public static final ims_label_media_bandwidth:I = 0x7f06009d

.field public static final ims_label_media_bandwidth_menual_as:I = 0x7f06009f

.field public static final ims_label_media_bandwidth_mode:I = 0x7f06009e

.field public static final ims_label_media_bw_as:I = 0x7f0600a3

.field public static final ims_label_media_bw_rr:I = 0x7f0600a5

.field public static final ims_label_media_bw_rs:I = 0x7f0600a4

.field public static final ims_label_media_candidate_priority:I = 0x7f06008e

.field public static final ims_label_media_carrier_audio_amr_be:I = 0x7f0600d3

.field public static final ims_label_media_carrier_audio_amr_oa:I = 0x7f0600d4

.field public static final ims_label_media_carrier_audio_amr_wb_be:I = 0x7f0600d5

.field public static final ims_label_media_carrier_audio_amr_wb_oa:I = 0x7f0600d6

.field public static final ims_label_media_carrier_audio_codecs:I = 0x7f0600d2

.field public static final ims_label_media_carrier_audio_pref_codec:I = 0x7f0600d0

.field public static final ims_label_media_carrier_audio_pref_codec_payload:I = 0x7f0600d1

.field public static final ims_label_media_carrier_psvt:I = 0x7f0600cf

.field public static final ims_label_media_carrier_video_bandwidth:I = 0x7f0600d9

.field public static final ims_label_media_carrier_video_bitrate:I = 0x7f0600d8

.field public static final ims_label_media_carrier_video_enable_onscreen:I = 0x7f0600d7

.field public static final ims_label_media_carrier_video_h263_qcif:I = 0x7f0600dd

.field public static final ims_label_media_carrier_video_h264_qcif:I = 0x7f0600dc

.field public static final ims_label_media_carrier_video_h264_qvga:I = 0x7f0600db

.field public static final ims_label_media_carrier_video_h264_vga:I = 0x7f0600da

.field public static final ims_label_media_carrier_volte:I = 0x7f0600ce

.field public static final ims_label_media_codec0:I = 0x7f0600a6

.field public static final ims_label_media_codec1:I = 0x7f0600a7

.field public static final ims_label_media_codec2:I = 0x7f0600a8

.field public static final ims_label_media_codec3:I = 0x7f0600a9

.field public static final ims_label_media_codec4:I = 0x7f0600aa

.field public static final ims_label_media_codec5:I = 0x7f0600ab

.field public static final ims_label_media_codec_channel:I = 0x7f0600b3

.field public static final ims_label_media_codec_max_ptime:I = 0x7f0600bc

.field public static final ims_label_media_codec_max_red:I = 0x7f0600b9

.field public static final ims_label_media_codec_mode_change_capability:I = 0x7f0600b6

.field public static final ims_label_media_codec_mode_change_neighbor:I = 0x7f0600b8

.field public static final ims_label_media_codec_mode_change_period:I = 0x7f0600b7

.field public static final ims_label_media_codec_mode_set:I = 0x7f0600b5

.field public static final ims_label_media_codec_network_type:I = 0x7f0600b2

.field public static final ims_label_media_codec_octet_align:I = 0x7f0600b4

.field public static final ims_label_media_codec_payload_type:I = 0x7f0600b1

.field public static final ims_label_media_codec_ptime:I = 0x7f0600bb

.field public static final ims_label_media_codec_robust_sorting:I = 0x7f0600ba

.field public static final ims_label_media_codec_sdp_name:I = 0x7f0600b0

.field public static final ims_label_media_heartbeat_time:I = 0x7f06008a

.field public static final ims_label_media_jitter_buffer_size:I = 0x7f06008b

.field public static final ims_label_media_loopback_mode:I = 0x7f060089

.field public static final ims_label_media_max_ptime:I = 0x7f0600a2

.field public static final ims_label_media_operator:I = 0x7f0600cd

.field public static final ims_label_media_port_rtcp:I = 0x7f06009a

.field public static final ims_label_media_port_rtp:I = 0x7f060099

.field public static final ims_label_media_port_rtp_random:I = 0x7f06009b

.field public static final ims_label_media_ptime:I = 0x7f0600a1

.field public static final ims_label_media_rtcp_enable:I = 0x7f06008c

.field public static final ims_label_media_scr_enable:I = 0x7f060092

.field public static final ims_label_media_sdp_answer_fullcapa:I = 0x7f06008f

.field public static final ims_label_media_sdp_keep_m_line:I = 0x7f060091

.field public static final ims_label_media_sdp_reoffer_fullcapa:I = 0x7f060090

.field public static final ims_label_media_send_rtcp_bye:I = 0x7f06008d

.field public static final ims_label_media_tv_rtp_inactivity:I = 0x7f06009c

.field public static final ims_label_media_video_codec:I = 0x7f060088

.field public static final ims_label_media_video_codec_QCIF:I = 0x7f0600c5

.field public static final ims_label_media_video_codec_asvalue:I = 0x7f0600c0

.field public static final ims_label_media_video_codec_bitrate:I = 0x7f0600bf

.field public static final ims_label_media_video_codec_framerate:I = 0x7f0600be

.field public static final ims_label_media_video_codec_framesize:I = 0x7f0600c1

.field public static final ims_label_media_video_codec_level:I = 0x7f0600c4

.field public static final ims_label_media_video_codec_max_br:I = 0x7f0600cc

.field public static final ims_label_media_video_codec_max_cpb:I = 0x7f0600ca

.field public static final ims_label_media_video_codec_max_dpb:I = 0x7f0600cb

.field public static final ims_label_media_video_codec_max_fs:I = 0x7f0600c9

.field public static final ims_label_media_video_codec_max_mbps:I = 0x7f0600c8

.field public static final ims_label_media_video_codec_packetization_mode:I = 0x7f0600c6

.field public static final ims_label_media_video_codec_profile:I = 0x7f0600c3

.field public static final ims_label_media_video_codec_profile_level_id:I = 0x7f0600c2

.field public static final ims_label_media_video_codec_resolution:I = 0x7f0600bd

.field public static final ims_label_media_video_codec_sprop_parameter_sets:I = 0x7f0600c7

.field public static final ims_label_media_video_common:I = 0x7f060084

.field public static final ims_label_media_video_enable_onscreen_debug_info_video:I = 0x7f060097

.field public static final ims_label_media_video_framerate:I = 0x7f060094

.field public static final ims_label_media_video_framerate_mode:I = 0x7f060095

.field public static final ims_label_media_video_framerate_value:I = 0x7f060096

.field public static final ims_label_media_video_sdp:I = 0x7f060086

.field public static final ims_label_media_video_send_periodic_sps_pps:I = 0x7f060098

.field public static final ims_label_media_volte:I = 0x7f060081

.field public static final ims_label_media_vt:I = 0x7f060082

.field public static final ims_label_mobile_pcscf_0:I = 0x7f060036

.field public static final ims_label_msisdn:I = 0x7f06000a

.field public static final ims_label_password:I = 0x7f060047

.field public static final ims_label_pcscf:I = 0x7f060035

.field public static final ims_label_pcscf_2:I = 0x7f060038

.field public static final ims_label_phone_number:I = 0x7f060019

.field public static final ims_label_port:I = 0x7f06003a

.field public static final ims_label_preferred_id:I = 0x7f06006d

.field public static final ims_label_presence:I = 0x7f0600fc

.field public static final ims_label_rcse:I = 0x7f0600f3

.field public static final ims_label_realm:I = 0x7f060046

.field public static final ims_label_realm_leniency:I = 0x7f060049

.field public static final ims_label_reg_sub_expiration:I = 0x7f06006b

.field public static final ims_label_registration_duration:I = 0x7f060069

.field public static final ims_label_registration_expires:I = 0x7f060002

.field public static final ims_label_registration_status:I = 0x7f060007

.field public static final ims_label_registration_subscription:I = 0x7f060003

.field public static final ims_label_resource:I = 0x7f060104

.field public static final ims_label_resource_auth_name:I = 0x7f060106

.field public static final ims_label_resource_auth_secret:I = 0x7f060107

.field public static final ims_label_resource_auth_type:I = 0x7f060108

.field public static final ims_label_resource_root_uri:I = 0x7f060105

.field public static final ims_label_scscf:I = 0x7f06004a

.field public static final ims_label_server_selection:I = 0x7f06001c

.field public static final ims_label_service_onoff:I = 0x7f060078

.field public static final ims_label_service_version:I = 0x7f060068

.field public static final ims_label_services:I = 0x7f060033

.field public static final ims_label_session:I = 0x7f060079

.field public static final ims_label_session_conference_factory_uri:I = 0x7f06007e

.field public static final ims_label_session_max_count:I = 0x7f06007a

.field public static final ims_label_session_rpr_supported:I = 0x7f06007f

.field public static final ims_label_session_st_headers:I = 0x7f060075

.field public static final ims_label_session_st_method:I = 0x7f060074

.field public static final ims_label_session_st_minse:I = 0x7f060071

.field public static final ims_label_session_st_no_refresh_by_reinvite:I = 0x7f060076

.field public static final ims_label_session_st_refresher:I = 0x7f060073

.field public static final ims_label_session_st_session_expires:I = 0x7f060072

.field public static final ims_label_session_timer:I = 0x7f060070

.field public static final ims_label_session_tv_mo_1xx_wait:I = 0x7f06007c

.field public static final ims_label_session_tv_mo_no_answer:I = 0x7f06007b

.field public static final ims_label_session_tv_mt_alerting:I = 0x7f06007d

.field public static final ims_label_settings:I = 0x7f060001

.field public static final ims_label_sip:I = 0x7f06004c

.field public static final ims_label_sip_common:I = 0x7f06004d

.field public static final ims_label_sip_compact_form:I = 0x7f06004e

.field public static final ims_label_sip_device_id:I = 0x7f060051

.field public static final ims_label_sip_features:I = 0x7f06004f

.field public static final ims_label_sip_listen_channel:I = 0x7f060058

.field public static final ims_label_sip_listen_channel_port:I = 0x7f06005a

.field public static final ims_label_sip_listen_channel_scheme:I = 0x7f060059

.field public static final ims_label_sip_listen_channel_transport:I = 0x7f06005b

.field public static final ims_label_sip_phone_context:I = 0x7f060043

.field public static final ims_label_sip_registration:I = 0x7f060053

.field public static final ims_label_sip_tag_prefix:I = 0x7f060050

.field public static final ims_label_sip_tcp_connection_timer:I = 0x7f060055

.field public static final ims_label_sip_tcp_criterion_len:I = 0x7f060052

.field public static final ims_label_sip_tcp_keepalive_timer:I = 0x7f060056

.field public static final ims_label_sip_tcp_transport_timer:I = 0x7f060054

.field public static final ims_label_sip_tcp_wouldblock_timer:I = 0x7f060057

.field public static final ims_label_sip_timer:I = 0x7f06005c

.field public static final ims_label_sip_timer1:I = 0x7f06005d

.field public static final ims_label_sip_timer2:I = 0x7f06005e

.field public static final ims_label_sip_timerb:I = 0x7f06005f

.field public static final ims_label_sip_timerd:I = 0x7f060060

.field public static final ims_label_sip_timerf:I = 0x7f060061

.field public static final ims_label_sip_timerh:I = 0x7f060062

.field public static final ims_label_sip_timeri:I = 0x7f060063

.field public static final ims_label_sip_timerj:I = 0x7f060064

.field public static final ims_label_sip_timerk:I = 0x7f060065

.field public static final ims_label_subscriber:I = 0x7f06002e

.field public static final ims_label_subscription:I = 0x7f06006a

.field public static final ims_label_sw_version:I = 0x7f060067

.field public static final ims_label_target_number_format:I = 0x7f06006f

.field public static final ims_label_target_scheme:I = 0x7f06006e

.field public static final ims_label_telephone_event_8K:I = 0x7f0600ae

.field public static final ims_label_test:I = 0x7f060077

.field public static final ims_label_testbed:I = 0x7f06001e

.field public static final ims_label_testmode:I = 0x7f060034

.field public static final ims_label_user_agent:I = 0x7f060066

.field public static final ims_label_username:I = 0x7f060045

.field public static final ims_label_usim:I = 0x7f060031

.field public static final ims_label_voip:I = 0x7f0600f2

.field public static final ims_label_volte_debug_screen:I = 0x7f06000b

.field public static final ims_label_vs:I = 0x7f06010d

.field public static final ims_label_vt:I = 0x7f0600f1

.field public static final ims_label_wifi_pcscf_1:I = 0x7f060037

.field public static final ims_label_xdm:I = 0x7f060103

.field public static final ims_olleh_inquiry:I = 0x7f0603a8

.field public static final ims_port_range:I = 0x7f060390

.field public static final ims_provisioning:I = 0x7f06002a

.field public static final ims_rcs_invitation:I = 0x7f0603aa

.field public static final ims_registration_failed:I = 0x7f0603a6

.field public static final ims_service_not_provisioned:I = 0x7f0603a7

.field public static final ims_summary_ac_update_exception_list:I = 0x7f0603a2

.field public static final ims_summary_acs_url_scheme:I = 0x7f0603a1

.field public static final ims_summary_admin_pcscf:I = 0x7f060392

.field public static final ims_summary_amr_mode_set:I = 0x7f06039f

.field public static final ims_summary_amr_wb_mode_set:I = 0x7f0603a0

.field public static final ims_summary_audio_bandwidth_as:I = 0x7f06039b

.field public static final ims_summary_dtmf_duration:I = 0x7f060398

.field public static final ims_summary_ims_not_registered:I = 0x7f060394

.field public static final ims_summary_ims_registered:I = 0x7f060393

.field public static final ims_summary_jitter_buffer_size:I = 0x7f060397

.field public static final ims_summary_payload_type:I = 0x7f060391

.field public static final ims_summary_pdp_profile:I = 0x7f06039a

.field public static final ims_summary_port_rtp_range:I = 0x7f060399

.field public static final ims_summary_session_st_headers:I = 0x7f060396

.field public static final ims_summary_sip_features:I = 0x7f060395

.field public static final ims_summary_tv_rtp_inactivity:I = 0x7f06039e

.field public static final ims_summary_video_bandwidth_as:I = 0x7f06039c

.field public static final ims_summary_video_framerate:I = 0x7f06039d

.field public static final ims_volte_call:I = 0x7f0603a4

.field public static final ims_volte_join_required:I = 0x7f0603a9

.field public static final ims_vt_call:I = 0x7f0603a5

.field public static final rejected_call:I = 0x7f0603a3


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 88
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
