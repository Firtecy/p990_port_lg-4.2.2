.class final Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiverUninstaller;
.super Ljava/lang/Object;
.source "BatteryStateTracker.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/BatteryStateTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "BatteryChangedReceiverUninstaller"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/BatteryStateTracker;


# direct methods
.method public constructor <init>(Lcom/lge/ims/BatteryStateTracker;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 431
    iput-object p1, p0, Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiverUninstaller;->this$0:Lcom/lge/ims/BatteryStateTracker;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 432
    return-void
.end method


# virtual methods
.method public run()V
    .registers 3

    #@0
    .prologue
    .line 436
    const-string v0, "BatteryStateTracker"

    #@2
    const-string v1, "BatteryChangedReceiverUninstaller is run..."

    #@4
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 438
    iget-object v0, p0, Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiverUninstaller;->this$0:Lcom/lge/ims/BatteryStateTracker;

    #@9
    invoke-static {v0}, Lcom/lge/ims/BatteryStateTracker;->access$1000(Lcom/lge/ims/BatteryStateTracker;)Landroid/content/Context;

    #@c
    move-result-object v0

    #@d
    if-nez v0, :cond_17

    #@f
    .line 439
    const-string v0, "BatteryStateTracker"

    #@11
    const-string v1, "Context is null"

    #@13
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@16
    .line 444
    :goto_16
    return-void

    #@17
    .line 443
    :cond_17
    iget-object v0, p0, Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiverUninstaller;->this$0:Lcom/lge/ims/BatteryStateTracker;

    #@19
    invoke-static {v0}, Lcom/lge/ims/BatteryStateTracker;->access$1000(Lcom/lge/ims/BatteryStateTracker;)Landroid/content/Context;

    #@1c
    move-result-object v0

    #@1d
    iget-object v1, p0, Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiverUninstaller;->this$0:Lcom/lge/ims/BatteryStateTracker;

    #@1f
    invoke-static {v1}, Lcom/lge/ims/BatteryStateTracker;->access$1100(Lcom/lge/ims/BatteryStateTracker;)Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiver;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@26
    goto :goto_16
.end method
