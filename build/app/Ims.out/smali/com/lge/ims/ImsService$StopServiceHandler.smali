.class public Lcom/lge/ims/ImsService$StopServiceHandler;
.super Ljava/lang/Object;
.source "ImsService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/ImsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "StopServiceHandler"
.end annotation


# instance fields
.field id:I

.field final synthetic this$0:Lcom/lge/ims/ImsService;


# direct methods
.method public constructor <init>(Lcom/lge/ims/ImsService;I)V
    .registers 3
    .parameter
    .parameter "_id"

    #@0
    .prologue
    .line 302
    iput-object p1, p0, Lcom/lge/ims/ImsService$StopServiceHandler;->this$0:Lcom/lge/ims/ImsService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 303
    iput p2, p0, Lcom/lge/ims/ImsService$StopServiceHandler;->id:I

    #@7
    .line 304
    return-void
.end method


# virtual methods
.method public run()V
    .registers 3

    #@0
    .prologue
    .line 307
    iget v0, p0, Lcom/lge/ims/ImsService$StopServiceHandler;->id:I

    #@2
    const/4 v1, 0x1

    #@3
    if-ne v0, v1, :cond_13

    #@5
    .line 308
    const-string v0, "ImsService"

    #@7
    const-string v1, "Kill IMS Process!"

    #@9
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@c
    .line 309
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@f
    move-result v0

    #@10
    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    #@13
    .line 311
    :cond_13
    return-void
.end method
