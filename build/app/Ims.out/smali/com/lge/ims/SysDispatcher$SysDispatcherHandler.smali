.class final Lcom/lge/ims/SysDispatcher$SysDispatcherHandler;
.super Landroid/os/Handler;
.source "SysDispatcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/SysDispatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SysDispatcherHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/SysDispatcher;


# direct methods
.method private constructor <init>(Lcom/lge/ims/SysDispatcher;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1059
    iput-object p1, p0, Lcom/lge/ims/SysDispatcher$SysDispatcherHandler;->this$0:Lcom/lge/ims/SysDispatcher;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/lge/ims/SysDispatcher;Lcom/lge/ims/SysDispatcher$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1059
    invoke-direct {p0, p1}, Lcom/lge/ims/SysDispatcher$SysDispatcherHandler;-><init>(Lcom/lge/ims/SysDispatcher;)V

    #@3
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 7
    .parameter "msg"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1062
    const-string v1, "SysDispatcher"

    #@3
    new-instance v2, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v3, "SysDispatcherHandler :: "

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {p1}, Landroid/os/Message;->toString()Ljava/lang/String;

    #@11
    move-result-object v3

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v2

    #@1a
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@1d
    .line 1064
    iget v1, p1, Landroid/os/Message;->what:I

    #@1f
    packed-switch v1, :pswitch_data_60

    #@22
    .line 1086
    :goto_22
    return-void

    #@23
    .line 1066
    :pswitch_23
    invoke-static {}, Lcom/lge/ims/SysDispatcher;->access$100()Landroid/content/Context;

    #@26
    move-result-object v1

    #@27
    const v2, 0x7f0603a3

    #@2a
    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    #@2d
    move-result-object v1

    #@2e
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    #@31
    goto :goto_22

    #@32
    .line 1071
    :pswitch_32
    invoke-static {}, Lcom/lge/ims/SysDispatcher;->access$100()Landroid/content/Context;

    #@35
    move-result-object v1

    #@36
    const v2, 0x7f0603a6

    #@39
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@3c
    move-result-object v0

    #@3d
    .line 1072
    .local v0, text:Ljava/lang/String;
    invoke-static {}, Lcom/lge/ims/SysDispatcher;->access$100()Landroid/content/Context;

    #@40
    move-result-object v1

    #@41
    invoke-static {v1, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@44
    move-result-object v1

    #@45
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    #@48
    goto :goto_22

    #@49
    .line 1078
    .end local v0           #text:Ljava/lang/String;
    :pswitch_49
    invoke-static {}, Lcom/lge/ims/SysDispatcher;->access$100()Landroid/content/Context;

    #@4c
    move-result-object v1

    #@4d
    const v2, 0x7f0603a7

    #@50
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@53
    move-result-object v0

    #@54
    .line 1079
    .restart local v0       #text:Ljava/lang/String;
    invoke-static {}, Lcom/lge/ims/SysDispatcher;->access$100()Landroid/content/Context;

    #@57
    move-result-object v1

    #@58
    invoke-static {v1, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@5b
    move-result-object v1

    #@5c
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    #@5f
    goto :goto_22

    #@60
    .line 1064
    :pswitch_data_60
    .packed-switch 0x1
        :pswitch_23
        :pswitch_32
        :pswitch_49
    .end packed-switch
.end method
