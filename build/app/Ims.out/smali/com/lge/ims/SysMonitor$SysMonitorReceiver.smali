.class public Lcom/lge/ims/SysMonitor$SysMonitorReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SysMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/SysMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SysMonitorReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/SysMonitor;


# direct methods
.method public constructor <init>(Lcom/lge/ims/SysMonitor;Landroid/content/Context;)V
    .registers 5
    .parameter
    .parameter "context"

    #@0
    .prologue
    .line 194
    iput-object p1, p0, Lcom/lge/ims/SysMonitor$SysMonitorReceiver;->this$0:Lcom/lge/ims/SysMonitor;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    .line 195
    new-instance v0, Landroid/content/IntentFilter;

    #@7
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@a
    .line 197
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.REBOOT"

    #@c
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@f
    .line 198
    const-string v1, "android.intent.action.ACTION_SHUTDOWN"

    #@11
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@14
    .line 199
    const-string v1, "lge.intent.action.DATA_IP_CHANGED"

    #@16
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@19
    .line 200
    const-string v1, "android.intent.action.DATA_DISABLE"

    #@1b
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1e
    .line 201
    const-string v1, "com.lge.ims.action.IMS_BOOT_COMPLETED"

    #@20
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@23
    .line 202
    const-string v1, "com.lge.ims.action.CONFIG_UPDATE"

    #@25
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@28
    .line 210
    const-string v1, "com.lge.ims.action.LOG_SERVICE"

    #@2a
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@2d
    .line 212
    invoke-virtual {p2, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@30
    .line 213
    return-void
.end method


# virtual methods
.method public declared-synchronized onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 13
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    .line 217
    monitor-enter p0

    #@2
    :try_start_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 219
    .local v0, action:Ljava/lang/String;
    const-string v6, "SysMonitor"

    #@8
    new-instance v7, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v8, "SysMonitor received an intent: "

    #@f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v7

    #@13
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v7

    #@17
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v7

    #@1b
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@1e
    .line 221
    const-string v6, "android.intent.action.REBOOT"

    #@20
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v6

    #@24
    if-eqz v6, :cond_2e

    #@26
    .line 222
    const/4 v6, 0x4

    #@27
    const/4 v7, 0x0

    #@28
    const/4 v8, 0x0

    #@29
    invoke-static {v6, v7, v8}, Lcom/lge/ims/SysMonitor;->nativeOnEventReceived(III)I
    :try_end_2c
    .catchall {:try_start_2 .. :try_end_2c} :catchall_3d

    #@2c
    .line 305
    :cond_2c
    :goto_2c
    monitor-exit p0

    #@2d
    return-void

    #@2e
    .line 223
    :cond_2e
    :try_start_2e
    const-string v6, "android.intent.action.ACTION_SHUTDOWN"

    #@30
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@33
    move-result v6

    #@34
    if-eqz v6, :cond_40

    #@36
    .line 224
    const/4 v6, 0x4

    #@37
    const/4 v7, 0x0

    #@38
    const/4 v8, 0x0

    #@39
    invoke-static {v6, v7, v8}, Lcom/lge/ims/SysMonitor;->nativeOnEventReceived(III)I
    :try_end_3c
    .catchall {:try_start_2e .. :try_end_3c} :catchall_3d

    #@3c
    goto :goto_2c

    #@3d
    .line 217
    .end local v0           #action:Ljava/lang/String;
    :catchall_3d
    move-exception v6

    #@3e
    monitor-exit p0

    #@3f
    throw v6

    #@40
    .line 225
    .restart local v0       #action:Ljava/lang/String;
    :cond_40
    :try_start_40
    const-string v6, "lge.intent.action.DATA_IP_CHANGED"

    #@42
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@45
    move-result v6

    #@46
    if-eqz v6, :cond_6c

    #@48
    .line 226
    const-string v6, "ipAddress"

    #@4a
    invoke-virtual {p2, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@4d
    move-result-object v1

    #@4e
    .line 228
    .local v1, address:Ljava/lang/String;
    const-string v6, "SysMonitor"

    #@50
    new-instance v7, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v8, "ipAddress="

    #@57
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v7

    #@5b
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v7

    #@5f
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v7

    #@63
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@66
    .line 230
    const/4 v6, 0x1

    #@67
    const/4 v7, 0x3

    #@68
    invoke-static {v6, v7}, Lcom/lge/ims/SysMonitor;->nativeOnDataConnectionStateChanged(II)V

    #@6b
    goto :goto_2c

    #@6c
    .line 231
    .end local v1           #address:Ljava/lang/String;
    :cond_6c
    const-string v6, "android.intent.action.DATA_DISABLE"

    #@6e
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@71
    move-result v6

    #@72
    if-eqz v6, :cond_8d

    #@74
    .line 232
    invoke-static {}, Lcom/lge/ims/DataConnectionManager;->getInstance()Lcom/lge/ims/DataConnectionManager;

    #@77
    move-result-object v6

    #@78
    invoke-virtual {v6}, Lcom/lge/ims/DataConnectionManager;->isMultiplePdnSupported()Z

    #@7b
    move-result v6

    #@7c
    if-eqz v6, :cond_86

    #@7e
    .line 234
    const-string v6, "SysMonitor"

    #@80
    const-string v7, "MPDN is supported; ignored"

    #@82
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@85
    goto :goto_2c

    #@86
    .line 236
    :cond_86
    const/4 v6, 0x2

    #@87
    const/4 v7, 0x0

    #@88
    const/4 v8, 0x0

    #@89
    invoke-static {v6, v7, v8}, Lcom/lge/ims/SysMonitor;->nativeOnEventReceived(III)I

    #@8c
    goto :goto_2c

    #@8d
    .line 238
    :cond_8d
    const-string v6, "com.lge.ims.action.IMS_BOOT_COMPLETED"

    #@8f
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@92
    move-result v6

    #@93
    if-eqz v6, :cond_110

    #@95
    .line 240
    iget-object v6, p0, Lcom/lge/ims/SysMonitor$SysMonitorReceiver;->this$0:Lcom/lge/ims/SysMonitor;

    #@97
    invoke-static {v6}, Lcom/lge/ims/SysMonitor;->access$100(Lcom/lge/ims/SysMonitor;)Z

    #@9a
    move-result v6

    #@9b
    if-eqz v6, :cond_a4

    #@9d
    .line 241
    const/16 v6, 0x8

    #@9f
    const/4 v7, 0x0

    #@a0
    const/4 v8, 0x0

    #@a1
    invoke-static {v6, v7, v8}, Lcom/lge/ims/SysMonitor;->nativeOnEventReceived(III)I

    #@a4
    .line 245
    :cond_a4
    invoke-static {}, Lcom/lge/ims/service/uc/UCServiceManager;->getInstance()Lcom/lge/ims/service/uc/UCServiceManager;

    #@a7
    move-result-object v6

    #@a8
    invoke-static {}, Lcom/lge/ims/SysMonitor;->access$200()Landroid/content/Context;

    #@ab
    move-result-object v7

    #@ac
    invoke-virtual {v6, v7}, Lcom/lge/ims/service/uc/UCServiceManager;->startService(Landroid/content/Context;)V

    #@af
    .line 248
    invoke-static {}, Lcom/lge/ims/PhoneSettingsTracker;->getInstance()Lcom/lge/ims/PhoneSettingsTracker;

    #@b2
    move-result-object v5

    #@b3
    .line 249
    .local v5, psTracker:Lcom/lge/ims/PhoneSettingsTracker;
    invoke-virtual {v5}, Lcom/lge/ims/PhoneSettingsTracker;->isVoLTEUsedForHDVoice()Z

    #@b6
    move-result v6

    #@b7
    if-ne v6, v9, :cond_f1

    #@b9
    .line 250
    const/high16 v6, 0x40

    #@bb
    const/4 v7, 0x1

    #@bc
    const/4 v8, 0x0

    #@bd
    invoke-static {v6, v7, v8}, Lcom/lge/ims/SysMonitor;->sendEventToNative(III)I

    #@c0
    .line 256
    :goto_c0
    invoke-static {}, Lcom/lge/ims/Configuration;->isRCSeOn()Z

    #@c3
    move-result v6

    #@c4
    if-eqz v6, :cond_d1

    #@c6
    .line 258
    invoke-static {}, Lcom/lge/ims/service/im/IMServiceManager;->getInstance()Lcom/lge/ims/service/im/IMServiceManager;

    #@c9
    move-result-object v6

    #@ca
    invoke-static {}, Lcom/lge/ims/SysMonitor;->access$200()Landroid/content/Context;

    #@cd
    move-result-object v7

    #@ce
    invoke-virtual {v6, v7}, Lcom/lge/ims/service/im/IMServiceManager;->startService(Landroid/content/Context;)V

    #@d1
    .line 261
    :cond_d1
    invoke-static {}, Lcom/lge/ims/Configuration;->isACOn()Z

    #@d4
    move-result v6

    #@d5
    if-eqz v6, :cond_f9

    #@d7
    invoke-static {}, Lcom/lge/ims/Configuration;->isRCSeOn()Z

    #@da
    move-result v6

    #@db
    if-eqz v6, :cond_f9

    #@dd
    .line 262
    new-instance v6, Lcom/lge/ims/BootupController;

    #@df
    invoke-static {}, Lcom/lge/ims/SysMonitor;->access$200()Landroid/content/Context;

    #@e2
    move-result-object v7

    #@e3
    invoke-direct {v6, v7}, Lcom/lge/ims/BootupController;-><init>(Landroid/content/Context;)V

    #@e6
    .line 272
    :cond_e6
    :goto_e6
    invoke-static {}, Lcom/lge/ims/DataConnectionManager;->getInstance()Lcom/lge/ims/DataConnectionManager;

    #@e9
    move-result-object v2

    #@ea
    .line 274
    .local v2, dcm:Lcom/lge/ims/DataConnectionManager;
    if-eqz v2, :cond_2c

    #@ec
    .line 275
    invoke-virtual {v2}, Lcom/lge/ims/DataConnectionManager;->notifyImsServiceStarted()V

    #@ef
    goto/16 :goto_2c

    #@f1
    .line 253
    .end local v2           #dcm:Lcom/lge/ims/DataConnectionManager;
    :cond_f1
    const/high16 v6, 0x40

    #@f3
    const/4 v7, 0x0

    #@f4
    const/4 v8, 0x0

    #@f5
    invoke-static {v6, v7, v8}, Lcom/lge/ims/SysMonitor;->sendEventToNative(III)I

    #@f8
    goto :goto_c0

    #@f9
    .line 264
    :cond_f9
    invoke-static {}, Lcom/lge/ims/Configuration;->isACOn()Z

    #@fc
    move-result v6

    #@fd
    if-nez v6, :cond_e6

    #@ff
    .line 265
    const v6, 0x8000

    #@102
    const/4 v7, 0x2

    #@103
    const/16 v8, 0x101

    #@105
    invoke-static {v6, v7, v8}, Lcom/lge/ims/SysMonitor;->nativeOnEventReceived(III)I

    #@108
    .line 267
    const/high16 v6, 0x1

    #@10a
    const/4 v7, 0x0

    #@10b
    const/4 v8, 0x0

    #@10c
    invoke-static {v6, v7, v8}, Lcom/lge/ims/SysMonitor;->nativeOnEventReceived(III)I

    #@10f
    goto :goto_e6

    #@110
    .line 277
    .end local v5           #psTracker:Lcom/lge/ims/PhoneSettingsTracker;
    :cond_110
    const-string v6, "com.lge.ims.action.CONFIG_UPDATE"

    #@112
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@115
    move-result v6

    #@116
    if-eqz v6, :cond_132

    #@118
    .line 278
    const-string v6, "SysMonitor"

    #@11a
    const-string v7, "Configuration is changed by hidden menu"

    #@11c
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@11f
    .line 279
    const-string v6, "flags"

    #@121
    const v7, 0xffff

    #@124
    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@127
    move-result v3

    #@128
    .line 280
    .local v3, flags:I
    const/16 v6, 0x40

    #@12a
    or-int/lit8 v7, v3, 0x0

    #@12c
    const/4 v8, 0x0

    #@12d
    invoke-static {v6, v7, v8}, Lcom/lge/ims/SysMonitor;->nativeOnEventReceived(III)I

    #@130
    goto/16 :goto_2c

    #@132
    .line 299
    .end local v3           #flags:I
    :cond_132
    const-string v6, "com.lge.ims.action.LOG_SERVICE"

    #@134
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@137
    move-result v6

    #@138
    if-eqz v6, :cond_2c

    #@13a
    .line 300
    invoke-static {}, Lcom/lge/ims/SysMonitor;->access$200()Landroid/content/Context;

    #@13d
    move-result-object v7

    #@13e
    const-string v6, "option"

    #@140
    invoke-virtual {p2, v6}, Landroid/content/Intent;->getExtra(Ljava/lang/String;)Ljava/lang/Object;

    #@143
    move-result-object v6

    #@144
    check-cast v6, Ljava/lang/String;

    #@146
    invoke-static {v7, v6}, Lcom/lge/ims/ImsLog;->updateConfig(Landroid/content/Context;Ljava/lang/String;)V

    #@149
    .line 302
    const/high16 v4, 0x2712

    #@14b
    .line 303
    .local v4, param:I
    const/16 v6, 0x40

    #@14d
    const/4 v7, 0x0

    #@14e
    invoke-static {v6, v4, v7}, Lcom/lge/ims/SysMonitor;->nativeOnEventReceived(III)I
    :try_end_151
    .catchall {:try_start_40 .. :try_end_151} :catchall_3d

    #@151
    goto/16 :goto_2c
.end method
