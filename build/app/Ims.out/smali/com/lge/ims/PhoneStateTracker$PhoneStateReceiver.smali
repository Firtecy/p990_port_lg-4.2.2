.class final Lcom/lge/ims/PhoneStateTracker$PhoneStateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PhoneStateTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/PhoneStateTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "PhoneStateReceiver"
.end annotation


# instance fields
.field private mIntentFilter:Landroid/content/IntentFilter;

.field final synthetic this$0:Lcom/lge/ims/PhoneStateTracker;


# direct methods
.method public constructor <init>(Lcom/lge/ims/PhoneStateTracker;)V
    .registers 4
    .parameter

    #@0
    .prologue
    .line 553
    iput-object p1, p0, Lcom/lge/ims/PhoneStateTracker$PhoneStateReceiver;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    .line 551
    new-instance v0, Landroid/content/IntentFilter;

    #@7
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@a
    iput-object v0, p0, Lcom/lge/ims/PhoneStateTracker$PhoneStateReceiver;->mIntentFilter:Landroid/content/IntentFilter;

    #@c
    .line 554
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker$PhoneStateReceiver;->mIntentFilter:Landroid/content/IntentFilter;

    #@e
    if-eqz v0, :cond_2b

    #@10
    .line 555
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker$PhoneStateReceiver;->mIntentFilter:Landroid/content/IntentFilter;

    #@12
    const-string v1, "android.intent.action.AIRPLANE_MODE"

    #@14
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@17
    .line 556
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker$PhoneStateReceiver;->mIntentFilter:Landroid/content/IntentFilter;

    #@19
    const-string v1, "android.intent.action.SIM_STATE_CHANGED"

    #@1b
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1e
    .line 558
    invoke-static {}, Lcom/lge/ims/Configuration;->isEmergencySupported()Z

    #@21
    move-result v0

    #@22
    if-eqz v0, :cond_2b

    #@24
    .line 560
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker$PhoneStateReceiver;->mIntentFilter:Landroid/content/IntentFilter;

    #@26
    const-string v1, "lge.intent.action.LTE_STATE_INFO"

    #@28
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@2b
    .line 563
    :cond_2b
    return-void
.end method


# virtual methods
.method public getFilter()Landroid/content/IntentFilter;
    .registers 2

    #@0
    .prologue
    .line 566
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker$PhoneStateReceiver;->mIntentFilter:Landroid/content/IntentFilter;

    #@2
    return-object v0
.end method

.method public declared-synchronized onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 12
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 571
    monitor-enter p0

    #@1
    :try_start_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    .line 573
    .local v0, action:Ljava/lang/String;
    const-string v6, "PhoneStateTracker"

    #@7
    new-instance v7, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v8, "PhoneStateReceiver - "

    #@e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v7

    #@12
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v7

    #@16
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v7

    #@1a
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@1d
    .line 575
    const-string v6, "android.intent.action.AIRPLANE_MODE"

    #@1f
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22
    move-result v6

    #@23
    if-eqz v6, :cond_c4

    #@25
    .line 576
    const-string v6, "state"

    #@27
    const/4 v7, 0x0

    #@28
    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@2b
    move-result v3

    #@2c
    .line 578
    .local v3, state:Z
    iget-object v6, p0, Lcom/lge/ims/PhoneStateTracker$PhoneStateReceiver;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@2e
    invoke-static {v6}, Lcom/lge/ims/PhoneStateTracker;->access$1000(Lcom/lge/ims/PhoneStateTracker;)Z

    #@31
    move-result v6

    #@32
    if-eq v6, v3, :cond_9e

    #@34
    .line 579
    const/4 v4, 0x0

    #@35
    .line 581
    .local v4, stateFromIntent:I
    if-eqz v3, :cond_a0

    #@37
    .line 582
    const/4 v4, 0x1

    #@38
    .line 583
    const-string v6, "PhoneStateTracker"

    #@3a
    const-string v7, "AirplaneMode :: ON"

    #@3c
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@3f
    .line 588
    :goto_3f
    iget-object v6, p0, Lcom/lge/ims/PhoneStateTracker$PhoneStateReceiver;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@41
    invoke-static {v6}, Lcom/lge/ims/PhoneStateTracker;->access$1100(Lcom/lge/ims/PhoneStateTracker;)Landroid/content/Context;

    #@44
    move-result-object v6

    #@45
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@48
    move-result-object v6

    #@49
    const-string v7, "airplane_mode_on"

    #@4b
    const/4 v8, -0x1

    #@4c
    invoke-static {v6, v7, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@4f
    move-result v5

    #@50
    .line 590
    .local v5, stateFromSettings:I
    if-eqz v5, :cond_74

    #@52
    const/4 v6, 0x1

    #@53
    if-eq v5, v6, :cond_74

    #@55
    .line 591
    const-string v6, "PhoneStateTracker"

    #@57
    new-instance v7, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    const-string v8, "AirplaneMode :: settings("

    #@5e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v7

    #@62
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@65
    move-result-object v7

    #@66
    const-string v8, ") is not valid; fallback to the default value(0)"

    #@68
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v7

    #@6c
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6f
    move-result-object v7

    #@70
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@73
    .line 592
    const/4 v5, 0x0

    #@74
    .line 595
    :cond_74
    if-eq v4, v5, :cond_ab

    #@76
    .line 596
    const-string v6, "PhoneStateTracker"

    #@78
    new-instance v7, Ljava/lang/StringBuilder;

    #@7a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@7d
    const-string v8, "AirplaneMode :: state (intent="

    #@7f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v7

    #@83
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@86
    move-result-object v7

    #@87
    const-string v8, ", settings="

    #@89
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v7

    #@8d
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@90
    move-result-object v7

    #@91
    const-string v8, ") is not matched; ignored..."

    #@93
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v7

    #@97
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9a
    move-result-object v7

    #@9b
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9e
    .catchall {:try_start_1 .. :try_end_9e} :catchall_a8

    #@9e
    .line 628
    .end local v3           #state:Z
    .end local v4           #stateFromIntent:I
    .end local v5           #stateFromSettings:I
    :cond_9e
    :goto_9e
    monitor-exit p0

    #@9f
    return-void

    #@a0
    .line 585
    .restart local v3       #state:Z
    .restart local v4       #stateFromIntent:I
    :cond_a0
    :try_start_a0
    const-string v6, "PhoneStateTracker"

    #@a2
    const-string v7, "AirplaneMode :: OFF"

    #@a4
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a7
    .catchall {:try_start_a0 .. :try_end_a7} :catchall_a8

    #@a7
    goto :goto_3f

    #@a8
    .line 571
    .end local v0           #action:Ljava/lang/String;
    .end local v3           #state:Z
    .end local v4           #stateFromIntent:I
    :catchall_a8
    move-exception v6

    #@a9
    monitor-exit p0

    #@aa
    throw v6

    #@ab
    .line 600
    .restart local v0       #action:Ljava/lang/String;
    .restart local v3       #state:Z
    .restart local v4       #stateFromIntent:I
    .restart local v5       #stateFromSettings:I
    :cond_ab
    :try_start_ab
    iget-object v6, p0, Lcom/lge/ims/PhoneStateTracker$PhoneStateReceiver;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@ad
    invoke-static {v6, v3}, Lcom/lge/ims/PhoneStateTracker;->access$1002(Lcom/lge/ims/PhoneStateTracker;Z)Z

    #@b0
    .line 603
    iget-object v6, p0, Lcom/lge/ims/PhoneStateTracker$PhoneStateReceiver;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@b2
    invoke-static {v6}, Lcom/lge/ims/PhoneStateTracker;->access$1200(Lcom/lge/ims/PhoneStateTracker;)Landroid/os/RegistrantList;

    #@b5
    move-result-object v6

    #@b6
    iget-object v7, p0, Lcom/lge/ims/PhoneStateTracker$PhoneStateReceiver;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@b8
    invoke-static {v7}, Lcom/lge/ims/PhoneStateTracker;->access$1000(Lcom/lge/ims/PhoneStateTracker;)Z

    #@bb
    move-result v7

    #@bc
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@bf
    move-result-object v7

    #@c0
    invoke-virtual {v6, v7}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@c3
    goto :goto_9e

    #@c4
    .line 605
    .end local v3           #state:Z
    .end local v4           #stateFromIntent:I
    .end local v5           #stateFromSettings:I
    :cond_c4
    const-string v6, "android.intent.action.SIM_STATE_CHANGED"

    #@c6
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c9
    move-result v6

    #@ca
    if-eqz v6, :cond_126

    #@cc
    .line 606
    const-string v6, "ss"

    #@ce
    invoke-virtual {p2, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@d1
    move-result-object v2

    #@d2
    .line 608
    .local v2, iccState:Ljava/lang/String;
    if-nez v2, :cond_dc

    #@d4
    .line 609
    const-string v6, "PhoneStateTracker"

    #@d6
    const-string v7, "iccState is null"

    #@d8
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@db
    goto :goto_9e

    #@dc
    .line 613
    :cond_dc
    iget-object v6, p0, Lcom/lge/ims/PhoneStateTracker$PhoneStateReceiver;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@de
    invoke-static {v6}, Lcom/lge/ims/PhoneStateTracker;->access$1300(Lcom/lge/ims/PhoneStateTracker;)Ljava/lang/String;

    #@e1
    move-result-object v6

    #@e2
    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e5
    move-result v6

    #@e6
    if-nez v6, :cond_9e

    #@e8
    .line 614
    const-string v6, "PhoneStateTracker"

    #@ea
    new-instance v7, Ljava/lang/StringBuilder;

    #@ec
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@ef
    const-string v8, "IccState :: "

    #@f1
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v7

    #@f5
    iget-object v8, p0, Lcom/lge/ims/PhoneStateTracker$PhoneStateReceiver;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@f7
    invoke-static {v8}, Lcom/lge/ims/PhoneStateTracker;->access$1300(Lcom/lge/ims/PhoneStateTracker;)Ljava/lang/String;

    #@fa
    move-result-object v8

    #@fb
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fe
    move-result-object v7

    #@ff
    const-string v8, " >> "

    #@101
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@104
    move-result-object v7

    #@105
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@108
    move-result-object v7

    #@109
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10c
    move-result-object v7

    #@10d
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@110
    .line 616
    iget-object v6, p0, Lcom/lge/ims/PhoneStateTracker$PhoneStateReceiver;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@112
    invoke-static {v6, v2}, Lcom/lge/ims/PhoneStateTracker;->access$1302(Lcom/lge/ims/PhoneStateTracker;Ljava/lang/String;)Ljava/lang/String;

    #@115
    .line 619
    iget-object v6, p0, Lcom/lge/ims/PhoneStateTracker$PhoneStateReceiver;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@117
    invoke-static {v6}, Lcom/lge/ims/PhoneStateTracker;->access$1400(Lcom/lge/ims/PhoneStateTracker;)Landroid/os/RegistrantList;

    #@11a
    move-result-object v6

    #@11b
    iget-object v7, p0, Lcom/lge/ims/PhoneStateTracker$PhoneStateReceiver;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@11d
    invoke-static {v7}, Lcom/lge/ims/PhoneStateTracker;->access$1300(Lcom/lge/ims/PhoneStateTracker;)Ljava/lang/String;

    #@120
    move-result-object v7

    #@121
    invoke-virtual {v6, v7}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@124
    goto/16 :goto_9e

    #@126
    .line 621
    .end local v2           #iccState:Ljava/lang/String;
    :cond_126
    const-string v6, "lge.intent.action.LTE_STATE_INFO"

    #@128
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12b
    move-result v6

    #@12c
    if-eqz v6, :cond_9e

    #@12e
    .line 622
    const-string v6, "LteStateInfo"

    #@130
    const/4 v7, 0x0

    #@131
    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@134
    move-result v1

    #@135
    .line 624
    .local v1, code:I
    const-string v6, "PhoneStateTracker"

    #@137
    new-instance v7, Ljava/lang/StringBuilder;

    #@139
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@13c
    const-string v8, "LteStateInfo :: "

    #@13e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@141
    move-result-object v7

    #@142
    iget-object v8, p0, Lcom/lge/ims/PhoneStateTracker$PhoneStateReceiver;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@144
    invoke-static {v8}, Lcom/lge/ims/PhoneStateTracker;->access$1500(Lcom/lge/ims/PhoneStateTracker;)Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@147
    move-result-object v8

    #@148
    invoke-virtual {v8}, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;->getCode()I

    #@14b
    move-result v8

    #@14c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14f
    move-result-object v7

    #@150
    const-string v8, " >> "

    #@152
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@155
    move-result-object v7

    #@156
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@159
    move-result-object v7

    #@15a
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15d
    move-result-object v7

    #@15e
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@161
    .line 626
    iget-object v6, p0, Lcom/lge/ims/PhoneStateTracker$PhoneStateReceiver;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@163
    invoke-static {v1}, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;->fromInt(I)Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@166
    move-result-object v7

    #@167
    invoke-static {v6, v7}, Lcom/lge/ims/PhoneStateTracker;->access$1502(Lcom/lge/ims/PhoneStateTracker;Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;)Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;
    :try_end_16a
    .catchall {:try_start_ab .. :try_end_16a} :catchall_a8

    #@16a
    goto/16 :goto_9e
.end method
