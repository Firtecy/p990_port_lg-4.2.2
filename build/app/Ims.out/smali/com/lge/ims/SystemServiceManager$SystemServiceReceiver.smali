.class final Lcom/lge/ims/SystemServiceManager$SystemServiceReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SystemServiceManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/SystemServiceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SystemServiceReceiver"
.end annotation


# instance fields
.field private mHandler:Landroid/os/Handler;

.field final synthetic this$0:Lcom/lge/ims/SystemServiceManager;


# direct methods
.method public constructor <init>(Lcom/lge/ims/SystemServiceManager;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 363
    iput-object p1, p0, Lcom/lge/ims/SystemServiceManager$SystemServiceReceiver;->this$0:Lcom/lge/ims/SystemServiceManager;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    .line 361
    new-instance v0, Landroid/os/Handler;

    #@7
    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    #@a
    iput-object v0, p0, Lcom/lge/ims/SystemServiceManager$SystemServiceReceiver;->mHandler:Landroid/os/Handler;

    #@c
    .line 364
    return-void
.end method


# virtual methods
.method public declared-synchronized onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 8
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 368
    monitor-enter p0

    #@1
    :try_start_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    .line 370
    .local v0, action:Ljava/lang/String;
    const-string v1, "SystemServiceManager"

    #@7
    new-instance v2, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v3, "SystemServiceReceiver - action="

    #@e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v2

    #@1a
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@1d
    .line 372
    const-string v1, "com.lge.ims.action.IMS_PHONE_STARTED"

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22
    move-result v1

    #@23
    if-eqz v1, :cond_37

    #@25
    .line 373
    iget-object v1, p0, Lcom/lge/ims/SystemServiceManager$SystemServiceReceiver;->mHandler:Landroid/os/Handler;

    #@27
    if-eqz v1, :cond_37

    #@29
    .line 374
    iget-object v1, p0, Lcom/lge/ims/SystemServiceManager$SystemServiceReceiver;->mHandler:Landroid/os/Handler;

    #@2b
    new-instance v2, Lcom/lge/ims/SystemServiceManager$SystemServiceConnector;

    #@2d
    iget-object v3, p0, Lcom/lge/ims/SystemServiceManager$SystemServiceReceiver;->this$0:Lcom/lge/ims/SystemServiceManager;

    #@2f
    invoke-direct {v2, v3}, Lcom/lge/ims/SystemServiceManager$SystemServiceConnector;-><init>(Lcom/lge/ims/SystemServiceManager;)V

    #@32
    const-wide/16 v3, 0x3e8

    #@34
    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_37
    .catchall {:try_start_1 .. :try_end_37} :catchall_39

    #@37
    .line 377
    :cond_37
    monitor-exit p0

    #@38
    return-void

    #@39
    .line 368
    .end local v0           #action:Ljava/lang/String;
    :catchall_39
    move-exception v1

    #@3a
    monitor-exit p0

    #@3b
    throw v1
.end method
