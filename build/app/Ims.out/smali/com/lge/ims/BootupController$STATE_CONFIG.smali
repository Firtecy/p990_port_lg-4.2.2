.class final enum Lcom/lge/ims/BootupController$STATE_CONFIG;
.super Ljava/lang/Enum;
.source "BootupController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/BootupController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "STATE_CONFIG"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/lge/ims/BootupController$STATE_CONFIG;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/lge/ims/BootupController$STATE_CONFIG;

.field public static final enum DIRTY:Lcom/lge/ims/BootupController$STATE_CONFIG;

.field public static final enum INVALID:Lcom/lge/ims/BootupController$STATE_CONFIG;

.field public static final enum VALID:Lcom/lge/ims/BootupController$STATE_CONFIG;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 83
    new-instance v0, Lcom/lge/ims/BootupController$STATE_CONFIG;

    #@5
    const-string v1, "INVALID"

    #@7
    invoke-direct {v0, v1, v2}, Lcom/lge/ims/BootupController$STATE_CONFIG;-><init>(Ljava/lang/String;I)V

    #@a
    sput-object v0, Lcom/lge/ims/BootupController$STATE_CONFIG;->INVALID:Lcom/lge/ims/BootupController$STATE_CONFIG;

    #@c
    .line 84
    new-instance v0, Lcom/lge/ims/BootupController$STATE_CONFIG;

    #@e
    const-string v1, "DIRTY"

    #@10
    invoke-direct {v0, v1, v3}, Lcom/lge/ims/BootupController$STATE_CONFIG;-><init>(Ljava/lang/String;I)V

    #@13
    sput-object v0, Lcom/lge/ims/BootupController$STATE_CONFIG;->DIRTY:Lcom/lge/ims/BootupController$STATE_CONFIG;

    #@15
    .line 85
    new-instance v0, Lcom/lge/ims/BootupController$STATE_CONFIG;

    #@17
    const-string v1, "VALID"

    #@19
    invoke-direct {v0, v1, v4}, Lcom/lge/ims/BootupController$STATE_CONFIG;-><init>(Ljava/lang/String;I)V

    #@1c
    sput-object v0, Lcom/lge/ims/BootupController$STATE_CONFIG;->VALID:Lcom/lge/ims/BootupController$STATE_CONFIG;

    #@1e
    .line 82
    const/4 v0, 0x3

    #@1f
    new-array v0, v0, [Lcom/lge/ims/BootupController$STATE_CONFIG;

    #@21
    sget-object v1, Lcom/lge/ims/BootupController$STATE_CONFIG;->INVALID:Lcom/lge/ims/BootupController$STATE_CONFIG;

    #@23
    aput-object v1, v0, v2

    #@25
    sget-object v1, Lcom/lge/ims/BootupController$STATE_CONFIG;->DIRTY:Lcom/lge/ims/BootupController$STATE_CONFIG;

    #@27
    aput-object v1, v0, v3

    #@29
    sget-object v1, Lcom/lge/ims/BootupController$STATE_CONFIG;->VALID:Lcom/lge/ims/BootupController$STATE_CONFIG;

    #@2b
    aput-object v1, v0, v4

    #@2d
    sput-object v0, Lcom/lge/ims/BootupController$STATE_CONFIG;->$VALUES:[Lcom/lge/ims/BootupController$STATE_CONFIG;

    #@2f
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 82
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/lge/ims/BootupController$STATE_CONFIG;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 82
    const-class v0, Lcom/lge/ims/BootupController$STATE_CONFIG;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/lge/ims/BootupController$STATE_CONFIG;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/lge/ims/BootupController$STATE_CONFIG;
    .registers 1

    #@0
    .prologue
    .line 82
    sget-object v0, Lcom/lge/ims/BootupController$STATE_CONFIG;->$VALUES:[Lcom/lge/ims/BootupController$STATE_CONFIG;

    #@2
    invoke-virtual {v0}, [Lcom/lge/ims/BootupController$STATE_CONFIG;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/lge/ims/BootupController$STATE_CONFIG;

    #@8
    return-object v0
.end method
