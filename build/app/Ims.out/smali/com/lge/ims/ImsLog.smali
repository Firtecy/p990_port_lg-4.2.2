.class public final Lcom/lge/ims/ImsLog;
.super Ljava/lang/Object;
.source "ImsLog.java"


# static fields
.field private static final DEBUG_MODE:Z = false

.field private static IMS_LOG:Z = false

.field private static IMS_LOG_D:Z = false

.field private static IMS_LOG_E:Z = false

.field private static IMS_LOG_I:Z = false

.field private static final OPT_CAT_D:I = 0x1

.field private static final OPT_CAT_E:I = 0x2

.field private static final OPT_CAT_I:I = 0x4

.field private static final OPT_MEDIUM_SERIAL:I = 0x40000

.field private static final TAG:Ljava/lang/String; = "LGIMS"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 27
    sput-boolean v0, Lcom/lge/ims/ImsLog;->IMS_LOG:Z

    #@3
    .line 28
    sput-boolean v0, Lcom/lge/ims/ImsLog;->IMS_LOG_D:Z

    #@5
    .line 29
    sput-boolean v0, Lcom/lge/ims/ImsLog;->IMS_LOG_E:Z

    #@7
    .line 30
    sput-boolean v0, Lcom/lge/ims/ImsLog;->IMS_LOG_I:Z

    #@9
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 18
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static final d(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "tag"
    .parameter "log"

    #@0
    .prologue
    .line 45
    sget-boolean v0, Lcom/lge/ims/ImsLog;->IMS_LOG:Z

    #@2
    if-eqz v0, :cond_2a

    #@4
    sget-boolean v0, Lcom/lge/ims/ImsLog;->IMS_LOG_D:Z

    #@6
    if-eqz v0, :cond_2a

    #@8
    .line 51
    const-string v0, "LGIMS"

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "["

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    const-string v2, "] "

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 54
    :cond_2a
    return-void
.end method

.method public static final e(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "tag"
    .parameter "log"

    #@0
    .prologue
    .line 81
    sget-boolean v0, Lcom/lge/ims/ImsLog;->IMS_LOG:Z

    #@2
    if-eqz v0, :cond_2a

    #@4
    sget-boolean v0, Lcom/lge/ims/ImsLog;->IMS_LOG_E:Z

    #@6
    if-eqz v0, :cond_2a

    #@8
    .line 87
    const-string v0, "LGIMS"

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "["

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    const-string v2, "] "

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 90
    :cond_2a
    return-void
.end method

.method public static final e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .registers 6
    .parameter "tag"
    .parameter "log"
    .parameter "ex"

    #@0
    .prologue
    .line 93
    sget-boolean v0, Lcom/lge/ims/ImsLog;->IMS_LOG:Z

    #@2
    if-eqz v0, :cond_2a

    #@4
    sget-boolean v0, Lcom/lge/ims/ImsLog;->IMS_LOG_E:Z

    #@6
    if-eqz v0, :cond_2a

    #@8
    .line 99
    const-string v0, "LGIMS"

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "["

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    const-string v2, "] "

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-static {v0, v1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2a
    .line 102
    :cond_2a
    return-void
.end method

.method public static final i(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "tag"
    .parameter "log"

    #@0
    .prologue
    .line 57
    sget-boolean v0, Lcom/lge/ims/ImsLog;->IMS_LOG:Z

    #@2
    if-eqz v0, :cond_2a

    #@4
    sget-boolean v0, Lcom/lge/ims/ImsLog;->IMS_LOG_I:Z

    #@6
    if-eqz v0, :cond_2a

    #@8
    .line 63
    const-string v0, "LGIMS"

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "["

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    const-string v2, "] "

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 66
    :cond_2a
    return-void
.end method

.method public static final initConfig(Landroid/content/Context;)V
    .registers 12
    .parameter "context"

    #@0
    .prologue
    const/high16 v10, 0x4

    #@2
    const/4 v9, 0x1

    #@3
    .line 106
    if-nez p0, :cond_6

    #@5
    .line 161
    :cond_5
    :goto_5
    return-void

    #@6
    .line 110
    :cond_6
    const/4 v6, 0x0

    #@7
    .line 113
    .local v6, c:Landroid/database/Cursor;
    :try_start_7
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@a
    move-result-object v0

    #@b
    sget-object v1, Lcom/lge/ims/provider/IMS$Engine;->CONTENT_URI:Landroid/net/Uri;

    #@d
    const/4 v2, 0x0

    #@e
    const/4 v3, 0x0

    #@f
    const/4 v4, 0x0

    #@10
    const/4 v5, 0x0

    #@11
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@14
    move-result-object v6

    #@15
    .line 115
    if-eqz v6, :cond_9d

    #@17
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@1a
    move-result v0

    #@1b
    if-eqz v0, :cond_9d

    #@1d
    .line 116
    const-string v0, "trace_option"

    #@1f
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@22
    move-result v0

    #@23
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    #@26
    move-result v8

    #@27
    .line 118
    .local v8, option:I
    const-string v0, "LGIMS"

    #@29
    new-instance v1, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v2, "LOG :: option=0x"

    #@30
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v1

    #@34
    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@37
    move-result-object v2

    #@38
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v1

    #@3c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v1

    #@40
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@43
    .line 121
    and-int v0, v8, v10

    #@45
    if-ne v0, v10, :cond_a4

    #@47
    .line 122
    const/4 v0, 0x1

    #@48
    sput-boolean v0, Lcom/lge/ims/ImsLog;->IMS_LOG:Z

    #@4a
    .line 128
    :goto_4a
    and-int/lit8 v0, v8, 0x1

    #@4c
    if-ne v0, v9, :cond_b9

    #@4e
    .line 129
    const/4 v0, 0x1

    #@4f
    sput-boolean v0, Lcom/lge/ims/ImsLog;->IMS_LOG_D:Z

    #@51
    .line 134
    :goto_51
    and-int/lit8 v0, v8, 0x2

    #@53
    const/4 v1, 0x2

    #@54
    if-ne v0, v1, :cond_ce

    #@56
    .line 135
    const/4 v0, 0x1

    #@57
    sput-boolean v0, Lcom/lge/ims/ImsLog;->IMS_LOG_E:Z

    #@59
    .line 140
    :goto_59
    and-int/lit8 v0, v8, 0x4

    #@5b
    const/4 v1, 0x4

    #@5c
    if-ne v0, v1, :cond_d9

    #@5e
    .line 141
    const/4 v0, 0x1

    #@5f
    sput-boolean v0, Lcom/lge/ims/ImsLog;->IMS_LOG_I:Z

    #@61
    .line 146
    :goto_61
    sget-boolean v0, Lcom/lge/ims/ImsLog;->IMS_LOG:Z

    #@63
    if-eqz v0, :cond_dd

    #@65
    .line 147
    const-string v0, "LGIMS"

    #@67
    new-instance v1, Ljava/lang/StringBuilder;

    #@69
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6c
    const-string v2, "LOG is ON :: D("

    #@6e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v1

    #@72
    sget-boolean v2, Lcom/lge/ims/ImsLog;->IMS_LOG_D:Z

    #@74
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@77
    move-result-object v1

    #@78
    const-string v2, "), E("

    #@7a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v1

    #@7e
    sget-boolean v2, Lcom/lge/ims/ImsLog;->IMS_LOG_E:Z

    #@80
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@83
    move-result-object v1

    #@84
    const-string v2, "), I("

    #@86
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v1

    #@8a
    sget-boolean v2, Lcom/lge/ims/ImsLog;->IMS_LOG_I:Z

    #@8c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v1

    #@90
    const-string v2, ")"

    #@92
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v1

    #@96
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@99
    move-result-object v1

    #@9a
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9d
    .catchall {:try_start_7 .. :try_end_9d} :catchall_d2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_9d} :catch_a8
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_9d} :catch_bd

    #@9d
    .line 157
    .end local v8           #option:I
    :cond_9d
    :goto_9d
    if-eqz v6, :cond_5

    #@9f
    .line 158
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@a2
    goto/16 :goto_5

    #@a4
    .line 124
    .restart local v8       #option:I
    :cond_a4
    const/4 v0, 0x0

    #@a5
    :try_start_a5
    sput-boolean v0, Lcom/lge/ims/ImsLog;->IMS_LOG:Z
    :try_end_a7
    .catchall {:try_start_a5 .. :try_end_a7} :catchall_d2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_a5 .. :try_end_a7} :catch_a8
    .catch Ljava/lang/Exception; {:try_start_a5 .. :try_end_a7} :catch_bd

    #@a7
    goto :goto_4a

    #@a8
    .line 152
    .end local v8           #option:I
    :catch_a8
    move-exception v7

    #@a9
    .line 153
    .local v7, e:Landroid/database/sqlite/SQLiteException;
    :try_start_a9
    const-string v0, "LGIMS"

    #@ab
    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    #@ae
    move-result-object v1

    #@af
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b2
    .catchall {:try_start_a9 .. :try_end_b2} :catchall_d2

    #@b2
    .line 157
    if-eqz v6, :cond_5

    #@b4
    .line 158
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@b7
    goto/16 :goto_5

    #@b9
    .line 131
    .end local v7           #e:Landroid/database/sqlite/SQLiteException;
    .restart local v8       #option:I
    :cond_b9
    const/4 v0, 0x0

    #@ba
    :try_start_ba
    sput-boolean v0, Lcom/lge/ims/ImsLog;->IMS_LOG_D:Z
    :try_end_bc
    .catchall {:try_start_ba .. :try_end_bc} :catchall_d2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_ba .. :try_end_bc} :catch_a8
    .catch Ljava/lang/Exception; {:try_start_ba .. :try_end_bc} :catch_bd

    #@bc
    goto :goto_51

    #@bd
    .line 154
    .end local v8           #option:I
    :catch_bd
    move-exception v7

    #@be
    .line 155
    .local v7, e:Ljava/lang/Exception;
    :try_start_be
    const-string v0, "LGIMS"

    #@c0
    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@c3
    move-result-object v1

    #@c4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c7
    .catchall {:try_start_be .. :try_end_c7} :catchall_d2

    #@c7
    .line 157
    if-eqz v6, :cond_5

    #@c9
    .line 158
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@cc
    goto/16 :goto_5

    #@ce
    .line 137
    .end local v7           #e:Ljava/lang/Exception;
    .restart local v8       #option:I
    :cond_ce
    const/4 v0, 0x0

    #@cf
    :try_start_cf
    sput-boolean v0, Lcom/lge/ims/ImsLog;->IMS_LOG_E:Z
    :try_end_d1
    .catchall {:try_start_cf .. :try_end_d1} :catchall_d2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_cf .. :try_end_d1} :catch_a8
    .catch Ljava/lang/Exception; {:try_start_cf .. :try_end_d1} :catch_bd

    #@d1
    goto :goto_59

    #@d2
    .line 157
    .end local v8           #option:I
    :catchall_d2
    move-exception v0

    #@d3
    if-eqz v6, :cond_d8

    #@d5
    .line 158
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@d8
    :cond_d8
    throw v0

    #@d9
    .line 143
    .restart local v8       #option:I
    :cond_d9
    const/4 v0, 0x0

    #@da
    :try_start_da
    sput-boolean v0, Lcom/lge/ims/ImsLog;->IMS_LOG_I:Z

    #@dc
    goto :goto_61

    #@dd
    .line 149
    :cond_dd
    const-string v0, "LGIMS"

    #@df
    new-instance v1, Ljava/lang/StringBuilder;

    #@e1
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e4
    const-string v2, "LOG is OFF :: D("

    #@e6
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e9
    move-result-object v1

    #@ea
    sget-boolean v2, Lcom/lge/ims/ImsLog;->IMS_LOG_D:Z

    #@ec
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@ef
    move-result-object v1

    #@f0
    const-string v2, "), E("

    #@f2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f5
    move-result-object v1

    #@f6
    sget-boolean v2, Lcom/lge/ims/ImsLog;->IMS_LOG_E:Z

    #@f8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@fb
    move-result-object v1

    #@fc
    const-string v2, "), I("

    #@fe
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@101
    move-result-object v1

    #@102
    sget-boolean v2, Lcom/lge/ims/ImsLog;->IMS_LOG_I:Z

    #@104
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@107
    move-result-object v1

    #@108
    const-string v2, ")"

    #@10a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10d
    move-result-object v1

    #@10e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@111
    move-result-object v1

    #@112
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_115
    .catchall {:try_start_da .. :try_end_115} :catchall_d2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_da .. :try_end_115} :catch_a8
    .catch Ljava/lang/Exception; {:try_start_da .. :try_end_115} :catch_bd

    #@115
    goto :goto_9d
.end method

.method public static final updateConfig(Landroid/content/Context;Ljava/lang/String;)V
    .registers 12
    .parameter "context"
    .parameter "option"

    #@0
    .prologue
    .line 165
    if-nez p0, :cond_3

    #@2
    .line 227
    :cond_2
    :goto_2
    return-void

    #@3
    .line 169
    :cond_3
    if-eqz p1, :cond_2

    #@5
    .line 173
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@8
    move-result v6

    #@9
    const/16 v7, 0xa

    #@b
    if-eq v6, v7, :cond_26

    #@d
    .line 174
    const-string v6, "LGIMS"

    #@f
    new-instance v7, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v8, "LOG :: option is not valid - "

    #@16
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v7

    #@1a
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v7

    #@1e
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v7

    #@22
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    goto :goto_2

    #@26
    .line 178
    :cond_26
    const-string v6, "content://com.lge.ims.provider.lgims/lgims_engine"

    #@28
    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@2b
    move-result-object v0

    #@2c
    .line 179
    .local v0, CONTENT_URI:Landroid/net/Uri;
    new-instance v1, Landroid/content/ContentValues;

    #@2e
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    #@31
    .line 181
    .local v1, cvs:Landroid/content/ContentValues;
    const-string v6, "trace_option"

    #@33
    invoke-virtual {v1, v6, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@36
    .line 184
    :try_start_36
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@39
    move-result-object v6

    #@3a
    const/4 v7, 0x0

    #@3b
    const/4 v8, 0x0

    #@3c
    invoke-virtual {v6, v0, v1, v7, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@3f
    move-result v6

    #@40
    if-lez v6, :cond_60

    #@42
    .line 185
    const-string v6, "LGIMS"

    #@44
    new-instance v7, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    const-string v8, "LOG :: option ("

    #@4b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v7

    #@4f
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v7

    #@53
    const-string v8, ") is updated"

    #@55
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v7

    #@59
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v7

    #@5d
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_60
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_36 .. :try_end_60} :catch_d8
    .catch Ljava/lang/Exception; {:try_start_36 .. :try_end_60} :catch_e4

    #@60
    .line 193
    :cond_60
    :goto_60
    invoke-static {p1}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    #@63
    move-result-object v5

    #@64
    .line 194
    .local v5, temp:Ljava/lang/Integer;
    invoke-virtual {v5}, Ljava/lang/Integer;->longValue()J

    #@67
    move-result-wide v3

    #@68
    .line 197
    .local v3, intOption:J
    const-wide/32 v6, 0x40000

    #@6b
    and-long/2addr v6, v3

    #@6c
    const-wide/32 v8, 0x40000

    #@6f
    cmp-long v6, v6, v8

    #@71
    if-nez v6, :cond_f0

    #@73
    .line 198
    const/4 v6, 0x1

    #@74
    sput-boolean v6, Lcom/lge/ims/ImsLog;->IMS_LOG:Z

    #@76
    .line 204
    :goto_76
    const-wide/16 v6, 0x1

    #@78
    and-long/2addr v6, v3

    #@79
    const-wide/16 v8, 0x1

    #@7b
    cmp-long v6, v6, v8

    #@7d
    if-nez v6, :cond_f4

    #@7f
    .line 205
    const/4 v6, 0x1

    #@80
    sput-boolean v6, Lcom/lge/ims/ImsLog;->IMS_LOG_D:Z

    #@82
    .line 210
    :goto_82
    const-wide/16 v6, 0x2

    #@84
    and-long/2addr v6, v3

    #@85
    const-wide/16 v8, 0x2

    #@87
    cmp-long v6, v6, v8

    #@89
    if-nez v6, :cond_f8

    #@8b
    .line 211
    const/4 v6, 0x1

    #@8c
    sput-boolean v6, Lcom/lge/ims/ImsLog;->IMS_LOG_E:Z

    #@8e
    .line 216
    :goto_8e
    const-wide/16 v6, 0x4

    #@90
    and-long/2addr v6, v3

    #@91
    const-wide/16 v8, 0x4

    #@93
    cmp-long v6, v6, v8

    #@95
    if-nez v6, :cond_fc

    #@97
    .line 217
    const/4 v6, 0x1

    #@98
    sput-boolean v6, Lcom/lge/ims/ImsLog;->IMS_LOG_I:Z

    #@9a
    .line 222
    :goto_9a
    sget-boolean v6, Lcom/lge/ims/ImsLog;->IMS_LOG:Z

    #@9c
    if-eqz v6, :cond_100

    #@9e
    .line 223
    const-string v6, "LGIMS"

    #@a0
    new-instance v7, Ljava/lang/StringBuilder;

    #@a2
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@a5
    const-string v8, "LOG is ON :: D("

    #@a7
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v7

    #@ab
    sget-boolean v8, Lcom/lge/ims/ImsLog;->IMS_LOG_D:Z

    #@ad
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v7

    #@b1
    const-string v8, "), E("

    #@b3
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v7

    #@b7
    sget-boolean v8, Lcom/lge/ims/ImsLog;->IMS_LOG_E:Z

    #@b9
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v7

    #@bd
    const-string v8, "), I("

    #@bf
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v7

    #@c3
    sget-boolean v8, Lcom/lge/ims/ImsLog;->IMS_LOG_I:Z

    #@c5
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v7

    #@c9
    const-string v8, ")"

    #@cb
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v7

    #@cf
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d2
    move-result-object v7

    #@d3
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@d6
    goto/16 :goto_2

    #@d8
    .line 187
    .end local v3           #intOption:J
    .end local v5           #temp:Ljava/lang/Integer;
    :catch_d8
    move-exception v2

    #@d9
    .line 188
    .local v2, e:Landroid/database/sqlite/SQLiteException;
    const-string v6, "LGIMS"

    #@db
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    #@de
    move-result-object v7

    #@df
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e2
    goto/16 :goto_60

    #@e4
    .line 189
    .end local v2           #e:Landroid/database/sqlite/SQLiteException;
    :catch_e4
    move-exception v2

    #@e5
    .line 190
    .local v2, e:Ljava/lang/Exception;
    const-string v6, "LGIMS"

    #@e7
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@ea
    move-result-object v7

    #@eb
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ee
    goto/16 :goto_60

    #@f0
    .line 200
    .end local v2           #e:Ljava/lang/Exception;
    .restart local v3       #intOption:J
    .restart local v5       #temp:Ljava/lang/Integer;
    :cond_f0
    const/4 v6, 0x0

    #@f1
    sput-boolean v6, Lcom/lge/ims/ImsLog;->IMS_LOG:Z

    #@f3
    goto :goto_76

    #@f4
    .line 207
    :cond_f4
    const/4 v6, 0x0

    #@f5
    sput-boolean v6, Lcom/lge/ims/ImsLog;->IMS_LOG_D:Z

    #@f7
    goto :goto_82

    #@f8
    .line 213
    :cond_f8
    const/4 v6, 0x0

    #@f9
    sput-boolean v6, Lcom/lge/ims/ImsLog;->IMS_LOG_E:Z

    #@fb
    goto :goto_8e

    #@fc
    .line 219
    :cond_fc
    const/4 v6, 0x0

    #@fd
    sput-boolean v6, Lcom/lge/ims/ImsLog;->IMS_LOG_I:Z

    #@ff
    goto :goto_9a

    #@100
    .line 225
    :cond_100
    const-string v6, "LGIMS"

    #@102
    new-instance v7, Ljava/lang/StringBuilder;

    #@104
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@107
    const-string v8, "LOG is OFF :: D("

    #@109
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10c
    move-result-object v7

    #@10d
    sget-boolean v8, Lcom/lge/ims/ImsLog;->IMS_LOG_D:Z

    #@10f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@112
    move-result-object v7

    #@113
    const-string v8, "), E("

    #@115
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@118
    move-result-object v7

    #@119
    sget-boolean v8, Lcom/lge/ims/ImsLog;->IMS_LOG_E:Z

    #@11b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@11e
    move-result-object v7

    #@11f
    const-string v8, "), I("

    #@121
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@124
    move-result-object v7

    #@125
    sget-boolean v8, Lcom/lge/ims/ImsLog;->IMS_LOG_I:Z

    #@127
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@12a
    move-result-object v7

    #@12b
    const-string v8, ")"

    #@12d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@130
    move-result-object v7

    #@131
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@134
    move-result-object v7

    #@135
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@138
    goto/16 :goto_2
.end method

.method public static final v(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "tag"
    .parameter "log"

    #@0
    .prologue
    .line 33
    sget-boolean v0, Lcom/lge/ims/ImsLog;->IMS_LOG:Z

    #@2
    if-eqz v0, :cond_2a

    #@4
    sget-boolean v0, Lcom/lge/ims/ImsLog;->IMS_LOG_D:Z

    #@6
    if-eqz v0, :cond_2a

    #@8
    .line 39
    const-string v0, "LGIMS"

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "["

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    const-string v2, "] "

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 42
    :cond_2a
    return-void
.end method

.method public static final w(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "tag"
    .parameter "log"

    #@0
    .prologue
    .line 69
    sget-boolean v0, Lcom/lge/ims/ImsLog;->IMS_LOG:Z

    #@2
    if-eqz v0, :cond_2a

    #@4
    sget-boolean v0, Lcom/lge/ims/ImsLog;->IMS_LOG_E:Z

    #@6
    if-eqz v0, :cond_2a

    #@8
    .line 75
    const-string v0, "LGIMS"

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "["

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    const-string v2, "] "

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 78
    :cond_2a
    return-void
.end method
