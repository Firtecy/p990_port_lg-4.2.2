.class final Lcom/lge/ims/AlarmTimerManager$AlarmTimerReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AlarmTimerManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/AlarmTimerManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "AlarmTimerReceiver"
.end annotation


# instance fields
.field private mIntentFilter:Landroid/content/IntentFilter;

.field final synthetic this$0:Lcom/lge/ims/AlarmTimerManager;


# direct methods
.method public constructor <init>(Lcom/lge/ims/AlarmTimerManager;)V
    .registers 4
    .parameter

    #@0
    .prologue
    .line 237
    iput-object p1, p0, Lcom/lge/ims/AlarmTimerManager$AlarmTimerReceiver;->this$0:Lcom/lge/ims/AlarmTimerManager;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    .line 235
    new-instance v0, Landroid/content/IntentFilter;

    #@7
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@a
    iput-object v0, p0, Lcom/lge/ims/AlarmTimerManager$AlarmTimerReceiver;->mIntentFilter:Landroid/content/IntentFilter;

    #@c
    .line 238
    iget-object v0, p0, Lcom/lge/ims/AlarmTimerManager$AlarmTimerReceiver;->mIntentFilter:Landroid/content/IntentFilter;

    #@e
    if-eqz v0, :cond_1e

    #@10
    .line 239
    iget-object v0, p0, Lcom/lge/ims/AlarmTimerManager$AlarmTimerReceiver;->mIntentFilter:Landroid/content/IntentFilter;

    #@12
    const-string v1, "com.lge.ims.action.ALARM_NATIVE_TIMER"

    #@14
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@17
    .line 240
    iget-object v0, p0, Lcom/lge/ims/AlarmTimerManager$AlarmTimerReceiver;->mIntentFilter:Landroid/content/IntentFilter;

    #@19
    const-string v1, "com.lge.ims.action.ALARM_TIMER"

    #@1b
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1e
    .line 242
    :cond_1e
    return-void
.end method


# virtual methods
.method public getFilter()Landroid/content/IntentFilter;
    .registers 2

    #@0
    .prologue
    .line 245
    iget-object v0, p0, Lcom/lge/ims/AlarmTimerManager$AlarmTimerReceiver;->mIntentFilter:Landroid/content/IntentFilter;

    #@2
    return-object v0
.end method

.method public declared-synchronized onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 10
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 250
    monitor-enter p0

    #@1
    :try_start_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@4
    move-result-object v1

    #@5
    .line 251
    .local v1, action:Ljava/lang/String;
    const-string v4, "tid"

    #@7
    const/4 v5, 0x0

    #@8
    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@b
    move-result v3

    #@c
    .line 253
    .local v3, tid:I
    const-string v4, "AlarmTimerManager"

    #@e
    new-instance v5, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v6, "AlarmTimerReceiver - "

    #@15
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v5

    #@19
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v5

    #@1d
    const-string v6, ", tid="

    #@1f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v5

    #@23
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v5

    #@27
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v5

    #@2b
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@2e
    .line 255
    const-string v4, "com.lge.ims.action.ALARM_NATIVE_TIMER"

    #@30
    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@33
    move-result v4

    #@34
    if-eqz v4, :cond_67

    #@36
    .line 256
    iget-object v4, p0, Lcom/lge/ims/AlarmTimerManager$AlarmTimerReceiver;->this$0:Lcom/lge/ims/AlarmTimerManager;

    #@38
    invoke-static {v4, v3}, Lcom/lge/ims/AlarmTimerManager;->access$000(Lcom/lge/ims/AlarmTimerManager;I)Z

    #@3b
    move-result v4

    #@3c
    if-eqz v4, :cond_5a

    #@3e
    .line 258
    const/16 v4, 0x5dc

    #@40
    invoke-static {v4}, Lcom/lge/ims/ImsWakeLock;->acquire(I)V

    #@43
    .line 264
    :goto_43
    iget-object v4, p0, Lcom/lge/ims/AlarmTimerManager$AlarmTimerReceiver;->this$0:Lcom/lge/ims/AlarmTimerManager;

    #@45
    invoke-static {v4}, Lcom/lge/ims/AlarmTimerManager;->access$100(Lcom/lge/ims/AlarmTimerManager;)Landroid/os/Registrant;

    #@48
    move-result-object v4

    #@49
    if-eqz v4, :cond_63

    #@4b
    .line 265
    iget-object v4, p0, Lcom/lge/ims/AlarmTimerManager$AlarmTimerReceiver;->this$0:Lcom/lge/ims/AlarmTimerManager;

    #@4d
    invoke-static {v4}, Lcom/lge/ims/AlarmTimerManager;->access$100(Lcom/lge/ims/AlarmTimerManager;)Landroid/os/Registrant;

    #@50
    move-result-object v4

    #@51
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@54
    move-result-object v5

    #@55
    invoke-virtual {v4, v5}, Landroid/os/Registrant;->notifyResult(Ljava/lang/Object;)V
    :try_end_58
    .catchall {:try_start_1 .. :try_end_58} :catchall_60

    #@58
    .line 285
    :cond_58
    :goto_58
    monitor-exit p0

    #@59
    return-void

    #@5a
    .line 261
    :cond_5a
    const/16 v4, 0x1f4

    #@5c
    :try_start_5c
    invoke-static {v4}, Lcom/lge/ims/ImsWakeLock;->acquire(I)V
    :try_end_5f
    .catchall {:try_start_5c .. :try_end_5f} :catchall_60

    #@5f
    goto :goto_43

    #@60
    .line 250
    .end local v1           #action:Ljava/lang/String;
    .end local v3           #tid:I
    :catchall_60
    move-exception v4

    #@61
    monitor-exit p0

    #@62
    throw v4

    #@63
    .line 267
    .restart local v1       #action:Ljava/lang/String;
    .restart local v3       #tid:I
    :cond_63
    :try_start_63
    invoke-static {v3}, Lcom/lge/ims/SysMonitor;->nativeProcessAlarmTimer(I)V

    #@66
    goto :goto_58

    #@67
    .line 269
    :cond_67
    const-string v4, "com.lge.ims.action.ALARM_TIMER"

    #@69
    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6c
    move-result v4

    #@6d
    if-eqz v4, :cond_58

    #@6f
    .line 270
    const/4 v2, 0x0

    #@70
    .line 272
    .local v2, r:Landroid/os/Registrant;
    iget-object v4, p0, Lcom/lge/ims/AlarmTimerManager$AlarmTimerReceiver;->this$0:Lcom/lge/ims/AlarmTimerManager;

    #@72
    invoke-static {v4}, Lcom/lge/ims/AlarmTimerManager;->access$200(Lcom/lge/ims/AlarmTimerManager;)Ljava/util/Hashtable;

    #@75
    move-result-object v5

    #@76
    monitor-enter v5
    :try_end_77
    .catchall {:try_start_63 .. :try_end_77} :catchall_60

    #@77
    .line 273
    :try_start_77
    iget-object v4, p0, Lcom/lge/ims/AlarmTimerManager$AlarmTimerReceiver;->this$0:Lcom/lge/ims/AlarmTimerManager;

    #@79
    invoke-static {v4}, Lcom/lge/ims/AlarmTimerManager;->access$200(Lcom/lge/ims/AlarmTimerManager;)Ljava/util/Hashtable;

    #@7c
    move-result-object v4

    #@7d
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@80
    move-result-object v6

    #@81
    invoke-virtual {v4, v6}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@84
    move-result-object v4

    #@85
    move-object v0, v4

    #@86
    check-cast v0, Landroid/os/Registrant;

    #@88
    move-object v2, v0

    #@89
    .line 274
    monitor-exit v5
    :try_end_8a
    .catchall {:try_start_77 .. :try_end_8a} :catchall_95

    #@8a
    .line 276
    if-eqz v2, :cond_98

    #@8c
    .line 278
    const/16 v4, 0x1f4

    #@8e
    :try_start_8e
    invoke-static {v4}, Lcom/lge/ims/ImsWakeLock;->acquire(I)V

    #@91
    .line 280
    invoke-virtual {v2}, Landroid/os/Registrant;->notifyRegistrant()V
    :try_end_94
    .catchall {:try_start_8e .. :try_end_94} :catchall_60

    #@94
    goto :goto_58

    #@95
    .line 274
    :catchall_95
    move-exception v4

    #@96
    :try_start_96
    monitor-exit v5
    :try_end_97
    .catchall {:try_start_96 .. :try_end_97} :catchall_95

    #@97
    :try_start_97
    throw v4

    #@98
    .line 282
    :cond_98
    const-string v4, "AlarmTimerManager"

    #@9a
    new-instance v5, Ljava/lang/StringBuilder;

    #@9c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@9f
    const-string v6, "No listener for tid="

    #@a1
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v5

    #@a5
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v5

    #@a9
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ac
    move-result-object v5

    #@ad
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_b0
    .catchall {:try_start_97 .. :try_end_b0} :catchall_60

    #@b0
    goto :goto_58
.end method
