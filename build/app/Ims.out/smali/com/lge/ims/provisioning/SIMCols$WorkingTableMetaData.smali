.class public final Lcom/lge/ims/provisioning/SIMCols$WorkingTableMetaData;
.super Ljava/lang/Object;
.source "SIMCols.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/provisioning/SIMCols;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "WorkingTableMetaData"
.end annotation


# static fields
.field public static final CONTENT_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/vnd.lge.rcse.working"

.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final ID:Ljava/lang/String; = "_id"

.field public static final KEY_RCS_E_IS_WORKING:I = 0x3

.field public static final KEY_RCS_E_WORKING:I = 0x2

.field public static final KEY_RCS_STATUS:I = 0x4

.field public static final KEY_WORKING_ID:I = 0x0

.field public static final KEY_WORKING_KEY:I = 0x1

.field public static final RCS_E_IS_WORKING:Ljava/lang/String; = "rcs_e_is_working"

.field public static final RCS_E_WORKING:Ljava/lang/String; = "rcs_e_working"

.field public static final RCS_STATUS:Ljava/lang/String; = "rcs_status"

.field public static final WORKING_KEY:Ljava/lang/String; = "working_key"

.field public static final WORKING_TABLE:Ljava/lang/String; = "working_table"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 233
    const-string v0, "content://com.lge.ims.provisioning/workings"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/lge/ims/provisioning/SIMCols$WorkingTableMetaData;->CONTENT_URI:Landroid/net/Uri;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 229
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
