.class public Lcom/lge/ims/provisioning/SIMCols;
.super Ljava/lang/Object;
.source "SIMCols.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/provisioning/SIMCols$WorkingTableMetaData;,
        Lcom/lge/ims/provisioning/SIMCols$SettingTableMetaData;,
        Lcom/lge/ims/provisioning/SIMCols$ProvisioningTableMetaData;
    }
.end annotation


# static fields
.field public static final AUTHORITY:Ljava/lang/String; = "com.lge.ims.provisioning"

.field public static final DB_NAME:Ljava/lang/String; = "rcse_provisioning.db"

.field public static final DB_VERSION:I = 0x5

.field public static final DEFAULT_SORT_ORDER:Ljava/lang/String; = "_id DESC"

.field public static bUseEncrypt:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 250
    const/4 v0, 0x0

    #@1
    sput-boolean v0, Lcom/lge/ims/provisioning/SIMCols;->bUseEncrypt:Z

    #@3
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 12
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 229
    return-void
.end method
