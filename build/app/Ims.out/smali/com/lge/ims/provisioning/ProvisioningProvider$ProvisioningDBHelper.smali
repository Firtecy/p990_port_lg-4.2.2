.class Lcom/lge/ims/provisioning/ProvisioningProvider$ProvisioningDBHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "ProvisioningProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/provisioning/ProvisioningProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ProvisioningDBHelper"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 51
    const-string v0, "rcse_provisioning.db"

    #@2
    const/4 v1, 0x0

    #@3
    const/4 v2, 0x5

    #@4
    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    #@7
    .line 52
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 4
    .parameter "db"

    #@0
    .prologue
    .line 56
    const-string v0, "RCS_ProvisioningProvider"

    #@2
    const-string v1, "ProvisioningDBHelper::onCreate"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 59
    const-string v0, "CREATE TABLE IF NOT EXISTS provisioning_table(_id INTEGER PRIMARY KEY AUTOINCREMENT,imsi TEXT NOT NULL UNIQUE,sip_proxy_max INTEGER ,sip_proxy_addr STRING ,sip_proxy_port INTEGER ,sip_proxy_addr1 STRING ,sip_proxy_port1 INTEGER ,sip_proxy_addr2 TEXT ,sip_proxy_port2 INTEGER ,sip_proxy_tcp_max INTEGER,sip_proxy_tcp_addr TEXT,sip_proxy_tcp_port INTEGER ,sip_proxy_tcp_addr1 TEXT,sip_proxy_tcp_port1 INTEGER ,sip_proxy_tcp_addr2 TEXT,sip_proxy_tcp_port2 INTEGER,tel_uri TEXT,sip_uri TEXT,sip_user TEXT,sip_password TEXT,im_conference_factory_uri TEXT,im_cap_always_on INTEGER,im_warn_sf INTEGER,polling_period INTEGER,capability_info_expiry INTEGER,ListcapinfoExpiry INTEGER,use_presence INTEGER,presence_discovery INTEGER,presence_profile INTEGER,enable_rcs_e_switch INTEGER,rcs_e_only_apn TEXT,ft_warn_size INTEGER,ft_max_size INTEGER,end_user_conf_req_id TEXT,validity_period INTEGER,ac_version INTEGER,title TEXT,message TEXT,accept_btn INTEGER,reject_btn INTEGER,ConRef TEXT,pdp_context_oper_pref INTEGER,p_cscf_address TEXT,timer_t1 INTEGER,timer_t2 INTEGER,timer_t4 INTEGER,private_user_identity TEXT,home_network_domain_name TEXT,nat_url_fmt TEXT,int_url_fmt TEXT,q_value TEXT,voice_call TEXT,chat TEXT,send_sms TEXT,file_tranfer TEXT,Video_Share TEXT,image_share TEXT,max_size_image_share INTEGER,max_time_video_share INTEGER,ICSI TEXT,icsi_resource_allocation_mode INTEGER,lbo_p_cscf_address TEXT,lbo_p_cscf_address_type TEXT,voice_domain_preference_e_utran INTEGER,sms_over_ip_networks_indication INTEGER,Keep_Alive_Enabled INTEGER,Voice_Domain_Preference_UTRAN INTEGER,Mobility_Management_IMS_Voice_Terminaton INTEGER,reg_retry_basetime INTEGER,reg_retry_maxtime INTEGER,phone_context TEXT,auth_type TEXT,realm TEXT,to_app_ref TEXT,availability_auth TEXT,aut_ma TEXT,op_fav_url TEXT,icon_maxsize INTEGER,note_maxsize INTEGER,fetch_auth TEXT,contact_cap_pres_aut TEXT,watcher_fetch_auth TEXT,client_obj_datalimit TEXT,presence_content_server_uri TEXT,source_throttlepublish INTEGER,max_number_of_subscriptions_inpresence_ist TEXT,service_uritemplate TEXT,rls_subs_period INTEGER,one_contact_interval INTEGER,revoke_timer INTEGER,xcap_root_uri TEXT,xcap_authentication_user_name TEXT,xcap_authentication_secret TEXT,xcap_authentication_type TEXT,chat_auth TEXT,sms_fall_back_auth TEXT,aut_accept TEXT,max_size_1to1 INTEGER,max_size_1tom INTEGER,timer_idle INTEGER,pres_srv_cap TEXT,deferred_msg_func_uri TEXT,max_adhoc_group_size TEXT,exploder_uri TEXT,imSessionStart INTEGER,im_content_server_uri TEXT,psSignalling TEXT,psMedia TEXT,psRTMedia TEXT,wifiSignalling TEXT,wifiMedia TEXT,wifiRTMedia TEXT,status_code INTEGER,status_message TEXT,timer_a INTEGER,timer_b INTEGER,timer_c INTEGER,timer_d INTEGER,timer_e INTEGER,timer_f INTEGER,timer_g INTEGER,timer_h INTEGER,timer_i INTEGER,timer_j INTEGER,timer_k INTEGER,rtpport_port1 INTEGER,rtpport_port2 INTEGER,rtpport_port3 INTEGER,rtpport_port4 INTEGER,regretry_interval1 INTEGER,regretry_interval2 INTEGER,regretry_interval3 INTEGER,regretry_initialRetry INTEGER,regretry_secondRetry INTEGER,regretry_thirdRetry INTEGER,regretry_repeatRetry INTEGER,abcinfo_address TEXT,abcinfo_addressType INTEGER,sbcinfo_address TEXT,sbcinfo_addressType INTEGER,sbc_tls_address TEXT,sbc_tls_addressType INTEGER,presence_publishtimer INTEGER,im_conv_hist_func_uri TEXT,im_delete_uri TEXT,software_version TEXT,software_url TEXT,other_timestamp TEXT,other_termsversion TEXT,other_deviceid TEXT);"

    #@9
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@c
    .line 236
    const-string v0, "CREATE TABLE IF NOT EXISTS setting_table(_id INTEGER PRIMARY KEY AUTOINCREMENT,imsi TEXT NOT NULL UNIQUE,rcs_e_service INTEGER,rcs_e_roaming INTEGER, latest_polling INTEGER, is_accept INTEGER, again_notification INTEGER, same_xml_version INTEGER);"

    #@e
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@11
    .line 246
    const-string v0, "CREATE TABLE IF NOT EXISTS working_table(_id INTEGER PRIMARY KEY AUTOINCREMENT,working_key INTEGER NOT NULL UNIQUE,rcs_e_working INTEGER,rcs_e_is_working INTEGER,rcs_status INTEGER);"

    #@13
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@16
    .line 254
    const-string v0, "INSERT INTO working_table VALUES (null, 1, 1, 0, 1);"

    #@18
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@1b
    .line 257
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .registers 6
    .parameter "db"
    .parameter "oldVersion"
    .parameter "newVersion"

    #@0
    .prologue
    .line 261
    const-string v0, "RCS_ProvisioningProvider"

    #@2
    const-string v1, "ProvisioningDBHelper::onUpgrade"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 263
    const-string v0, "DROP TABLE IF EXISTS provisioning_table"

    #@9
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@c
    .line 264
    const-string v0, "DROP TABLE IF EXISTS setting_table"

    #@e
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@11
    .line 265
    const-string v0, "DROP TABLE IF EXISTS working_table"

    #@13
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@16
    .line 266
    invoke-virtual {p0, p1}, Lcom/lge/ims/provisioning/ProvisioningProvider$ProvisioningDBHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    #@19
    .line 267
    return-void
.end method
