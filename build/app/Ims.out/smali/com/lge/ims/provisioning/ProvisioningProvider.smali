.class public Lcom/lge/ims/provisioning/ProvisioningProvider;
.super Landroid/content/ContentProvider;
.source "ProvisioningProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/provisioning/ProvisioningProvider$ProvisioningDBHelper;
    }
.end annotation


# static fields
.field static final ALLSIM:I = 0x1

.field static final ONESIM:I = 0x2

.field static final SETTINGS:I = 0x3

.field static final SETTINGSBYSIM:I = 0x4

.field public static final TAG:Ljava/lang/String; = "RCS_ProvisioningProvider"

.field private static final URI_MATCHER:Landroid/content/UriMatcher; = null

.field static final WORKINGS:I = 0x5

.field private static objProvisioningMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static objSettingMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static objWorkingMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected mDB:Landroid/database/sqlite/SQLiteDatabase;

.field private objProvisioningDBHelper:Lcom/lge/ims/provisioning/ProvisioningProvider$ProvisioningDBHelper;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    .line 742
    new-instance v0, Landroid/content/UriMatcher;

    #@2
    const/4 v1, -0x1

    #@3
    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    #@6
    sput-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@8
    .line 743
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@a
    const-string v1, "com.lge.ims.provisioning"

    #@c
    const-string v2, "sims"

    #@e
    const/4 v3, 0x1

    #@f
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@12
    .line 744
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@14
    const-string v1, "com.lge.ims.provisioning"

    #@16
    const-string v2, "sims/*"

    #@18
    const/4 v3, 0x2

    #@19
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@1c
    .line 745
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@1e
    const-string v1, "com.lge.ims.provisioning"

    #@20
    const-string v2, "settings"

    #@22
    const/4 v3, 0x3

    #@23
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@26
    .line 746
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@28
    const-string v1, "com.lge.ims.provisioning"

    #@2a
    const-string v2, "settings/*"

    #@2c
    const/4 v3, 0x4

    #@2d
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@30
    .line 747
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@32
    const-string v1, "com.lge.ims.provisioning"

    #@34
    const-string v2, "workings"

    #@36
    const/4 v3, 0x5

    #@37
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@3a
    .line 749
    new-instance v0, Ljava/util/HashMap;

    #@3c
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@3f
    sput-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@41
    .line 750
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@43
    const-string v1, "_id"

    #@45
    const-string v2, "_id"

    #@47
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4a
    .line 751
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@4c
    const-string v1, "imsi"

    #@4e
    const-string v2, "imsi"

    #@50
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@53
    .line 752
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@55
    const-string v1, "sip_proxy_max"

    #@57
    const-string v2, "sip_proxy_max"

    #@59
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5c
    .line 753
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@5e
    const-string v1, "sip_proxy_addr"

    #@60
    const-string v2, "sip_proxy_addr"

    #@62
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@65
    .line 754
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@67
    const-string v1, "sip_proxy_port"

    #@69
    const-string v2, "sip_proxy_port"

    #@6b
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@6e
    .line 755
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@70
    const-string v1, "sip_proxy_addr1"

    #@72
    const-string v2, "sip_proxy_addr1"

    #@74
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@77
    .line 756
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@79
    const-string v1, "sip_proxy_port1"

    #@7b
    const-string v2, "sip_proxy_port1"

    #@7d
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@80
    .line 757
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@82
    const-string v1, "sip_proxy_addr2"

    #@84
    const-string v2, "sip_proxy_addr2"

    #@86
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@89
    .line 758
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@8b
    const-string v1, "sip_proxy_port2"

    #@8d
    const-string v2, "sip_proxy_port2"

    #@8f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@92
    .line 759
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@94
    const-string v1, "sip_proxy_tcp_max"

    #@96
    const-string v2, "sip_proxy_tcp_max"

    #@98
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@9b
    .line 760
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@9d
    const-string v1, "sip_proxy_tcp_addr"

    #@9f
    const-string v2, "sip_proxy_tcp_addr"

    #@a1
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@a4
    .line 761
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@a6
    const-string v1, "sip_proxy_tcp_port"

    #@a8
    const-string v2, "sip_proxy_tcp_port"

    #@aa
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@ad
    .line 762
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@af
    const-string v1, "sip_proxy_tcp_addr1"

    #@b1
    const-string v2, "sip_proxy_tcp_addr1"

    #@b3
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@b6
    .line 763
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@b8
    const-string v1, "sip_proxy_tcp_port1"

    #@ba
    const-string v2, "sip_proxy_tcp_port1"

    #@bc
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@bf
    .line 764
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@c1
    const-string v1, "sip_proxy_tcp_addr2"

    #@c3
    const-string v2, "sip_proxy_tcp_addr2"

    #@c5
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@c8
    .line 765
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@ca
    const-string v1, "sip_proxy_tcp_port2"

    #@cc
    const-string v2, "sip_proxy_tcp_port2"

    #@ce
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@d1
    .line 766
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@d3
    const-string v1, "tel_uri"

    #@d5
    const-string v2, "tel_uri"

    #@d7
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@da
    .line 767
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@dc
    const-string v1, "sip_uri"

    #@de
    const-string v2, "sip_uri"

    #@e0
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@e3
    .line 768
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@e5
    const-string v1, "sip_user"

    #@e7
    const-string v2, "sip_user"

    #@e9
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@ec
    .line 769
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@ee
    const-string v1, "sip_password"

    #@f0
    const-string v2, "sip_password"

    #@f2
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@f5
    .line 770
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@f7
    const-string v1, "im_conference_factory_uri"

    #@f9
    const-string v2, "im_conference_factory_uri"

    #@fb
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@fe
    .line 771
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@100
    const-string v1, "im_cap_always_on"

    #@102
    const-string v2, "im_cap_always_on"

    #@104
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@107
    .line 772
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@109
    const-string v1, "im_warn_sf"

    #@10b
    const-string v2, "im_warn_sf"

    #@10d
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@110
    .line 773
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@112
    const-string v1, "polling_period"

    #@114
    const-string v2, "polling_period"

    #@116
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@119
    .line 774
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@11b
    const-string v1, "capability_info_expiry"

    #@11d
    const-string v2, "capability_info_expiry"

    #@11f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@122
    .line 775
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@124
    const-string v1, "ListcapinfoExpiry"

    #@126
    const-string v2, "ListcapinfoExpiry"

    #@128
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@12b
    .line 776
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@12d
    const-string v1, "use_presence"

    #@12f
    const-string v2, "use_presence"

    #@131
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@134
    .line 777
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@136
    const-string v1, "presence_discovery"

    #@138
    const-string v2, "presence_discovery"

    #@13a
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@13d
    .line 778
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@13f
    const-string v1, "presence_profile"

    #@141
    const-string v2, "presence_profile"

    #@143
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@146
    .line 779
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@148
    const-string v1, "enable_rcs_e_switch"

    #@14a
    const-string v2, "enable_rcs_e_switch"

    #@14c
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@14f
    .line 780
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@151
    const-string v1, "rcs_e_only_apn"

    #@153
    const-string v2, "rcs_e_only_apn"

    #@155
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@158
    .line 781
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@15a
    const-string v1, "ft_warn_size"

    #@15c
    const-string v2, "ft_warn_size"

    #@15e
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@161
    .line 782
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@163
    const-string v1, "ft_max_size"

    #@165
    const-string v2, "ft_max_size"

    #@167
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@16a
    .line 783
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@16c
    const-string v1, "end_user_conf_req_id"

    #@16e
    const-string v2, "end_user_conf_req_id"

    #@170
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@173
    .line 784
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@175
    const-string v1, "validity_period"

    #@177
    const-string v2, "validity_period"

    #@179
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@17c
    .line 785
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@17e
    const-string v1, "ac_version"

    #@180
    const-string v2, "ac_version"

    #@182
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@185
    .line 786
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@187
    const-string v1, "title"

    #@189
    const-string v2, "title"

    #@18b
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@18e
    .line 787
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@190
    const-string v1, "message"

    #@192
    const-string v2, "message"

    #@194
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@197
    .line 788
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@199
    const-string v1, "accept_btn"

    #@19b
    const-string v2, "accept_btn"

    #@19d
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1a0
    .line 789
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@1a2
    const-string v1, "reject_btn"

    #@1a4
    const-string v2, "reject_btn"

    #@1a6
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1a9
    .line 791
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@1ab
    const-string v1, "ConRef"

    #@1ad
    const-string v2, "ConRef"

    #@1af
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1b2
    .line 792
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@1b4
    const-string v1, "pdp_context_oper_pref"

    #@1b6
    const-string v2, "pdp_context_oper_pref"

    #@1b8
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1bb
    .line 793
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@1bd
    const-string v1, "p_cscf_address"

    #@1bf
    const-string v2, "p_cscf_address"

    #@1c1
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1c4
    .line 794
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@1c6
    const-string v1, "timer_t1"

    #@1c8
    const-string v2, "timer_t1"

    #@1ca
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1cd
    .line 795
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@1cf
    const-string v1, "timer_t2"

    #@1d1
    const-string v2, "timer_t2"

    #@1d3
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1d6
    .line 796
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@1d8
    const-string v1, "timer_t4"

    #@1da
    const-string v2, "timer_t4"

    #@1dc
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1df
    .line 797
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@1e1
    const-string v1, "private_user_identity"

    #@1e3
    const-string v2, "private_user_identity"

    #@1e5
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1e8
    .line 798
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@1ea
    const-string v1, "home_network_domain_name"

    #@1ec
    const-string v2, "home_network_domain_name"

    #@1ee
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1f1
    .line 799
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@1f3
    const-string v1, "nat_url_fmt"

    #@1f5
    const-string v2, "nat_url_fmt"

    #@1f7
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1fa
    .line 800
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@1fc
    const-string v1, "int_url_fmt"

    #@1fe
    const-string v2, "int_url_fmt"

    #@200
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@203
    .line 801
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@205
    const-string v1, "q_value"

    #@207
    const-string v2, "q_value"

    #@209
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@20c
    .line 803
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@20e
    const-string v1, "voice_call"

    #@210
    const-string v2, "voice_call"

    #@212
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@215
    .line 804
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@217
    const-string v1, "chat"

    #@219
    const-string v2, "chat"

    #@21b
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@21e
    .line 805
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@220
    const-string v1, "send_sms"

    #@222
    const-string v2, "send_sms"

    #@224
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@227
    .line 806
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@229
    const-string v1, "file_tranfer"

    #@22b
    const-string v2, "file_tranfer"

    #@22d
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@230
    .line 807
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@232
    const-string v1, "Video_Share"

    #@234
    const-string v2, "Video_Share"

    #@236
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@239
    .line 808
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@23b
    const-string v1, "image_share"

    #@23d
    const-string v2, "image_share"

    #@23f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@242
    .line 809
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@244
    const-string v1, "max_size_image_share"

    #@246
    const-string v2, "max_size_image_share"

    #@248
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@24b
    .line 810
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@24d
    const-string v1, "max_time_video_share"

    #@24f
    const-string v2, "max_time_video_share"

    #@251
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@254
    .line 812
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@256
    const-string v1, "ICSI"

    #@258
    const-string v2, "ICSI"

    #@25a
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@25d
    .line 813
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@25f
    const-string v1, "icsi_resource_allocation_mode"

    #@261
    const-string v2, "icsi_resource_allocation_mode"

    #@263
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@266
    .line 815
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@268
    const-string v1, "lbo_p_cscf_address"

    #@26a
    const-string v2, "lbo_p_cscf_address"

    #@26c
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@26f
    .line 816
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@271
    const-string v1, "lbo_p_cscf_address_type"

    #@273
    const-string v2, "lbo_p_cscf_address_type"

    #@275
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@278
    .line 817
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@27a
    const-string v1, "voice_domain_preference_e_utran"

    #@27c
    const-string v2, "voice_domain_preference_e_utran"

    #@27e
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@281
    .line 818
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@283
    const-string v1, "sms_over_ip_networks_indication"

    #@285
    const-string v2, "sms_over_ip_networks_indication"

    #@287
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@28a
    .line 819
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@28c
    const-string v1, "Keep_Alive_Enabled"

    #@28e
    const-string v2, "Keep_Alive_Enabled"

    #@290
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@293
    .line 820
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@295
    const-string v1, "Voice_Domain_Preference_UTRAN"

    #@297
    const-string v2, "Voice_Domain_Preference_UTRAN"

    #@299
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@29c
    .line 821
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@29e
    const-string v1, "Mobility_Management_IMS_Voice_Terminaton"

    #@2a0
    const-string v2, "Mobility_Management_IMS_Voice_Terminaton"

    #@2a2
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2a5
    .line 822
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@2a7
    const-string v1, "reg_retry_basetime"

    #@2a9
    const-string v2, "reg_retry_basetime"

    #@2ab
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2ae
    .line 823
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@2b0
    const-string v1, "reg_retry_maxtime"

    #@2b2
    const-string v2, "reg_retry_maxtime"

    #@2b4
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2b7
    .line 825
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@2b9
    const-string v1, "phone_context"

    #@2bb
    const-string v2, "phone_context"

    #@2bd
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2c0
    .line 827
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@2c2
    const-string v1, "auth_type"

    #@2c4
    const-string v2, "auth_type"

    #@2c6
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2c9
    .line 828
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@2cb
    const-string v1, "realm"

    #@2cd
    const-string v2, "realm"

    #@2cf
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2d2
    .line 830
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@2d4
    const-string v1, "to_app_ref"

    #@2d6
    const-string v2, "to_app_ref"

    #@2d8
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2db
    .line 832
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@2dd
    const-string v1, "availability_auth"

    #@2df
    const-string v2, "availability_auth"

    #@2e1
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2e4
    .line 834
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@2e6
    const-string v1, "aut_ma"

    #@2e8
    const-string v2, "aut_ma"

    #@2ea
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2ed
    .line 835
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@2ef
    const-string v1, "op_fav_url"

    #@2f1
    const-string v2, "op_fav_url"

    #@2f3
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2f6
    .line 836
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@2f8
    const-string v1, "icon_maxsize"

    #@2fa
    const-string v2, "icon_maxsize"

    #@2fc
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2ff
    .line 837
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@301
    const-string v1, "note_maxsize"

    #@303
    const-string v2, "note_maxsize"

    #@305
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@308
    .line 839
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@30a
    const-string v1, "fetch_auth"

    #@30c
    const-string v2, "fetch_auth"

    #@30e
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@311
    .line 840
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@313
    const-string v1, "contact_cap_pres_aut"

    #@315
    const-string v2, "contact_cap_pres_aut"

    #@317
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@31a
    .line 842
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@31c
    const-string v1, "watcher_fetch_auth"

    #@31e
    const-string v2, "watcher_fetch_auth"

    #@320
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@323
    .line 843
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@325
    const-string v1, "client_obj_datalimit"

    #@327
    const-string v2, "client_obj_datalimit"

    #@329
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@32c
    .line 844
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@32e
    const-string v1, "presence_content_server_uri"

    #@330
    const-string v2, "presence_content_server_uri"

    #@332
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@335
    .line 845
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@337
    const-string v1, "source_throttlepublish"

    #@339
    const-string v2, "source_throttlepublish"

    #@33b
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@33e
    .line 846
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@340
    const-string v1, "max_number_of_subscriptions_inpresence_ist"

    #@342
    const-string v2, "max_number_of_subscriptions_inpresence_ist"

    #@344
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@347
    .line 847
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@349
    const-string v1, "service_uritemplate"

    #@34b
    const-string v2, "service_uritemplate"

    #@34d
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@350
    .line 848
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@352
    const-string v1, "rls_subs_period"

    #@354
    const-string v2, "rls_subs_period"

    #@356
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@359
    .line 849
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@35b
    const-string v1, "one_contact_interval"

    #@35d
    const-string v2, "one_contact_interval"

    #@35f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@362
    .line 851
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@364
    const-string v1, "revoke_timer"

    #@366
    const-string v2, "revoke_timer"

    #@368
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@36b
    .line 852
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@36d
    const-string v1, "xcap_root_uri"

    #@36f
    const-string v2, "xcap_root_uri"

    #@371
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@374
    .line 853
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@376
    const-string v1, "xcap_authentication_user_name"

    #@378
    const-string v2, "xcap_authentication_user_name"

    #@37a
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@37d
    .line 854
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@37f
    const-string v1, "xcap_authentication_secret"

    #@381
    const-string v2, "xcap_authentication_secret"

    #@383
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@386
    .line 855
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@388
    const-string v1, "xcap_authentication_type"

    #@38a
    const-string v2, "xcap_authentication_type"

    #@38c
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@38f
    .line 857
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@391
    const-string v1, "chat_auth"

    #@393
    const-string v2, "chat_auth"

    #@395
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@398
    .line 858
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@39a
    const-string v1, "sms_fall_back_auth"

    #@39c
    const-string v2, "sms_fall_back_auth"

    #@39e
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3a1
    .line 859
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@3a3
    const-string v1, "aut_accept"

    #@3a5
    const-string v2, "aut_accept"

    #@3a7
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3aa
    .line 860
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@3ac
    const-string v1, "max_size_1to1"

    #@3ae
    const-string v2, "max_size_1to1"

    #@3b0
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3b3
    .line 861
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@3b5
    const-string v1, "max_size_1tom"

    #@3b7
    const-string v2, "max_size_1tom"

    #@3b9
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3bc
    .line 862
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@3be
    const-string v1, "timer_idle"

    #@3c0
    const-string v2, "timer_idle"

    #@3c2
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3c5
    .line 863
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@3c7
    const-string v1, "pres_srv_cap"

    #@3c9
    const-string v2, "pres_srv_cap"

    #@3cb
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3ce
    .line 864
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@3d0
    const-string v1, "deferred_msg_func_uri"

    #@3d2
    const-string v2, "deferred_msg_func_uri"

    #@3d4
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3d7
    .line 865
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@3d9
    const-string v1, "max_adhoc_group_size"

    #@3db
    const-string v2, "max_adhoc_group_size"

    #@3dd
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3e0
    .line 866
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@3e2
    const-string v1, "exploder_uri"

    #@3e4
    const-string v2, "exploder_uri"

    #@3e6
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3e9
    .line 867
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@3eb
    const-string v1, "imSessionStart"

    #@3ed
    const-string v2, "imSessionStart"

    #@3ef
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3f2
    .line 868
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@3f4
    const-string v1, "im_content_server_uri"

    #@3f6
    const-string v2, "im_content_server_uri"

    #@3f8
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3fb
    .line 870
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@3fd
    const-string v1, "psSignalling"

    #@3ff
    const-string v2, "psSignalling"

    #@401
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@404
    .line 871
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@406
    const-string v1, "psMedia"

    #@408
    const-string v2, "psMedia"

    #@40a
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@40d
    .line 872
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@40f
    const-string v1, "psRTMedia"

    #@411
    const-string v2, "psRTMedia"

    #@413
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@416
    .line 873
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@418
    const-string v1, "wifiSignalling"

    #@41a
    const-string v2, "wifiSignalling"

    #@41c
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@41f
    .line 874
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@421
    const-string v1, "wifiMedia"

    #@423
    const-string v2, "wifiMedia"

    #@425
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@428
    .line 875
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@42a
    const-string v1, "wifiRTMedia"

    #@42c
    const-string v2, "wifiRTMedia"

    #@42e
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@431
    .line 877
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@433
    const-string v1, "status_code"

    #@435
    const-string v2, "status_code"

    #@437
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@43a
    .line 878
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@43c
    const-string v1, "status_message"

    #@43e
    const-string v2, "status_message"

    #@440
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@443
    .line 880
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@445
    const-string v1, "timer_a"

    #@447
    const-string v2, "timer_a"

    #@449
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@44c
    .line 881
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@44e
    const-string v1, "timer_b"

    #@450
    const-string v2, "timer_b"

    #@452
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@455
    .line 882
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@457
    const-string v1, "timer_c"

    #@459
    const-string v2, "timer_c"

    #@45b
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@45e
    .line 883
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@460
    const-string v1, "timer_d"

    #@462
    const-string v2, "timer_d"

    #@464
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@467
    .line 884
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@469
    const-string v1, "timer_e"

    #@46b
    const-string v2, "timer_e"

    #@46d
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@470
    .line 885
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@472
    const-string v1, "timer_f"

    #@474
    const-string v2, "timer_f"

    #@476
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@479
    .line 886
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@47b
    const-string v1, "timer_g"

    #@47d
    const-string v2, "timer_g"

    #@47f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@482
    .line 887
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@484
    const-string v1, "timer_h"

    #@486
    const-string v2, "timer_h"

    #@488
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@48b
    .line 888
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@48d
    const-string v1, "timer_i"

    #@48f
    const-string v2, "timer_i"

    #@491
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@494
    .line 889
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@496
    const-string v1, "timer_j"

    #@498
    const-string v2, "timer_j"

    #@49a
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@49d
    .line 890
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@49f
    const-string v1, "timer_k"

    #@4a1
    const-string v2, "timer_k"

    #@4a3
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4a6
    .line 892
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@4a8
    const-string v1, "rtpport_port1"

    #@4aa
    const-string v2, "rtpport_port1"

    #@4ac
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4af
    .line 893
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@4b1
    const-string v1, "rtpport_port2"

    #@4b3
    const-string v2, "rtpport_port2"

    #@4b5
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4b8
    .line 894
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@4ba
    const-string v1, "rtpport_port3"

    #@4bc
    const-string v2, "rtpport_port3"

    #@4be
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4c1
    .line 895
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@4c3
    const-string v1, "rtpport_port4"

    #@4c5
    const-string v2, "rtpport_port4"

    #@4c7
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4ca
    .line 897
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@4cc
    const-string v1, "regretry_interval1"

    #@4ce
    const-string v2, "regretry_interval1"

    #@4d0
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4d3
    .line 898
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@4d5
    const-string v1, "regretry_interval2"

    #@4d7
    const-string v2, "regretry_interval2"

    #@4d9
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4dc
    .line 899
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@4de
    const-string v1, "regretry_interval3"

    #@4e0
    const-string v2, "regretry_interval3"

    #@4e2
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4e5
    .line 900
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@4e7
    const-string v1, "regretry_initialRetry"

    #@4e9
    const-string v2, "regretry_initialRetry"

    #@4eb
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4ee
    .line 901
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@4f0
    const-string v1, "regretry_secondRetry"

    #@4f2
    const-string v2, "regretry_secondRetry"

    #@4f4
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4f7
    .line 902
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@4f9
    const-string v1, "regretry_thirdRetry"

    #@4fb
    const-string v2, "regretry_thirdRetry"

    #@4fd
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@500
    .line 903
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@502
    const-string v1, "regretry_repeatRetry"

    #@504
    const-string v2, "regretry_repeatRetry"

    #@506
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@509
    .line 905
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@50b
    const-string v1, "abcinfo_address"

    #@50d
    const-string v2, "abcinfo_address"

    #@50f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@512
    .line 906
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@514
    const-string v1, "abcinfo_addressType"

    #@516
    const-string v2, "abcinfo_addressType"

    #@518
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@51b
    .line 908
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@51d
    const-string v1, "sbcinfo_address"

    #@51f
    const-string v2, "sbcinfo_address"

    #@521
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@524
    .line 909
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@526
    const-string v1, "sbcinfo_addressType"

    #@528
    const-string v2, "sbcinfo_addressType"

    #@52a
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@52d
    .line 910
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@52f
    const-string v1, "sbc_tls_address"

    #@531
    const-string v2, "sbc_tls_address"

    #@533
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@536
    .line 911
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@538
    const-string v1, "sbc_tls_addressType"

    #@53a
    const-string v2, "sbc_tls_addressType"

    #@53c
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@53f
    .line 913
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@541
    const-string v1, "presence_publishtimer"

    #@543
    const-string v2, "presence_publishtimer"

    #@545
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@548
    .line 915
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@54a
    const-string v1, "im_conv_hist_func_uri"

    #@54c
    const-string v2, "im_conv_hist_func_uri"

    #@54e
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@551
    .line 916
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@553
    const-string v1, "im_delete_uri"

    #@555
    const-string v2, "im_delete_uri"

    #@557
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@55a
    .line 918
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@55c
    const-string v1, "software_version"

    #@55e
    const-string v2, "software_version"

    #@560
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@563
    .line 919
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@565
    const-string v1, "software_url"

    #@567
    const-string v2, "software_url"

    #@569
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@56c
    .line 921
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@56e
    const-string v1, "other_timestamp"

    #@570
    const-string v2, "other_timestamp"

    #@572
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@575
    .line 922
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@577
    const-string v1, "other_termsversion"

    #@579
    const-string v2, "other_termsversion"

    #@57b
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@57e
    .line 923
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@580
    const-string v1, "other_deviceid"

    #@582
    const-string v2, "other_deviceid"

    #@584
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@587
    .line 928
    new-instance v0, Ljava/util/HashMap;

    #@589
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@58c
    sput-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objSettingMap:Ljava/util/HashMap;

    #@58e
    .line 929
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objSettingMap:Ljava/util/HashMap;

    #@590
    const-string v1, "_id"

    #@592
    const-string v2, "_id"

    #@594
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@597
    .line 930
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objSettingMap:Ljava/util/HashMap;

    #@599
    const-string v1, "imsi"

    #@59b
    const-string v2, "imsi"

    #@59d
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5a0
    .line 931
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objSettingMap:Ljava/util/HashMap;

    #@5a2
    const-string v1, "rcs_e_service"

    #@5a4
    const-string v2, "rcs_e_service"

    #@5a6
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5a9
    .line 932
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objSettingMap:Ljava/util/HashMap;

    #@5ab
    const-string v1, "rcs_e_roaming"

    #@5ad
    const-string v2, "rcs_e_roaming"

    #@5af
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5b2
    .line 933
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objSettingMap:Ljava/util/HashMap;

    #@5b4
    const-string v1, "latest_polling"

    #@5b6
    const-string v2, "latest_polling"

    #@5b8
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5bb
    .line 934
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objSettingMap:Ljava/util/HashMap;

    #@5bd
    const-string v1, "is_accept"

    #@5bf
    const-string v2, "is_accept"

    #@5c1
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5c4
    .line 935
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objSettingMap:Ljava/util/HashMap;

    #@5c6
    const-string v1, "again_notification"

    #@5c8
    const-string v2, "again_notification"

    #@5ca
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5cd
    .line 936
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objSettingMap:Ljava/util/HashMap;

    #@5cf
    const-string v1, "same_xml_version"

    #@5d1
    const-string v2, "same_xml_version"

    #@5d3
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5d6
    .line 941
    new-instance v0, Ljava/util/HashMap;

    #@5d8
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@5db
    sput-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objWorkingMap:Ljava/util/HashMap;

    #@5dd
    .line 942
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objWorkingMap:Ljava/util/HashMap;

    #@5df
    const-string v1, "_id"

    #@5e1
    const-string v2, "_id"

    #@5e3
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5e6
    .line 943
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objWorkingMap:Ljava/util/HashMap;

    #@5e8
    const-string v1, "working_key"

    #@5ea
    const-string v2, "working_key"

    #@5ec
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5ef
    .line 944
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objWorkingMap:Ljava/util/HashMap;

    #@5f1
    const-string v1, "rcs_e_working"

    #@5f3
    const-string v2, "rcs_e_working"

    #@5f5
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5f8
    .line 945
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objWorkingMap:Ljava/util/HashMap;

    #@5fa
    const-string v1, "rcs_e_is_working"

    #@5fc
    const-string v2, "rcs_e_is_working"

    #@5fe
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@601
    .line 946
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objWorkingMap:Ljava/util/HashMap;

    #@603
    const-string v1, "rcs_status"

    #@605
    const-string v2, "rcs_status"

    #@607
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@60a
    .line 947
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 26
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    #@3
    .line 47
    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 12
    .parameter "uri"
    .parameter "where"
    .parameter "whereArgs"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 683
    const-string v4, "RCS_ProvisioningProvider"

    #@3
    new-instance v5, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v6, "ProvisioningProvider::delete - Uri : "

    #@a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v5

    #@e
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@11
    move-result-object v6

    #@12
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v5

    #@16
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v5

    #@1a
    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 685
    const/4 v0, 0x0

    #@1e
    .line 686
    .local v0, nCount:I
    iget-object v4, p0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningDBHelper:Lcom/lge/ims/provisioning/ProvisioningProvider$ProvisioningDBHelper;

    #@20
    invoke-virtual {v4}, Lcom/lge/ims/provisioning/ProvisioningProvider$ProvisioningDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@23
    move-result-object v1

    #@24
    .line 688
    .local v1, objSQLiteDatabase:Landroid/database/sqlite/SQLiteDatabase;
    sget-object v4, Lcom/lge/ims/provisioning/ProvisioningProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@26
    invoke-virtual {v4, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@29
    move-result v4

    #@2a
    packed-switch v4, :pswitch_data_10c

    #@2d
    .line 733
    :goto_2d
    if-lez v0, :cond_3b

    #@2f
    .line 734
    invoke-virtual {p0}, Lcom/lge/ims/provisioning/ProvisioningProvider;->getContext()Landroid/content/Context;

    #@32
    move-result-object v4

    #@33
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@36
    move-result-object v4

    #@37
    const/4 v5, 0x0

    #@38
    invoke-virtual {v4, p1, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    #@3b
    .line 737
    :cond_3b
    return v0

    #@3c
    .line 691
    :pswitch_3c
    if-nez p2, :cond_40

    #@3e
    if-eqz p3, :cond_48

    #@40
    .line 692
    :cond_40
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@42
    const-string v5, "Uri is invaild"

    #@44
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@47
    throw v4

    #@48
    .line 695
    :cond_48
    const-string v4, "provisioning_table"

    #@4a
    invoke-virtual {v1, v4, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    #@4d
    move-result v0

    #@4e
    .line 696
    goto :goto_2d

    #@4f
    .line 700
    :pswitch_4f
    if-nez p2, :cond_53

    #@51
    if-eqz p3, :cond_5b

    #@53
    .line 701
    :cond_53
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@55
    const-string v5, "Uri is invaild"

    #@57
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@5a
    throw v4

    #@5b
    .line 704
    :cond_5b
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@5e
    move-result-object v4

    #@5f
    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@62
    move-result-object v3

    #@63
    check-cast v3, Ljava/lang/String;

    #@65
    .line 705
    .local v3, strIMSI:Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    const-string v5, "imsi=\'"

    #@6c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v4

    #@70
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v4

    #@74
    const-string v5, "\'"

    #@76
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v5

    #@7a
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@7d
    move-result v4

    #@7e
    if-nez v4, :cond_a8

    #@80
    new-instance v4, Ljava/lang/StringBuilder;

    #@82
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@85
    const-string v6, " AND ("

    #@87
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v4

    #@8b
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v4

    #@8f
    const-string v6, ")"

    #@91
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v4

    #@95
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@98
    move-result-object v4

    #@99
    :goto_99
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v4

    #@9d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a0
    move-result-object v2

    #@a1
    .line 706
    .local v2, strDeletedWhere:Ljava/lang/String;
    const-string v4, "provisioning_table"

    #@a3
    invoke-virtual {v1, v4, v2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    #@a6
    move-result v0

    #@a7
    .line 707
    goto :goto_2d

    #@a8
    .line 705
    .end local v2           #strDeletedWhere:Ljava/lang/String;
    :cond_a8
    const-string v4, ""

    #@aa
    goto :goto_99

    #@ab
    .line 711
    .end local v3           #strIMSI:Ljava/lang/String;
    :pswitch_ab
    const-string v4, "setting_table"

    #@ad
    invoke-virtual {v1, v4, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    #@b0
    move-result v0

    #@b1
    .line 712
    goto/16 :goto_2d

    #@b3
    .line 716
    :pswitch_b3
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@b6
    move-result-object v4

    #@b7
    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@ba
    move-result-object v3

    #@bb
    check-cast v3, Ljava/lang/String;

    #@bd
    .line 717
    .restart local v3       #strIMSI:Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    #@bf
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@c2
    const-string v5, "imsi=\'"

    #@c4
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v4

    #@c8
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cb
    move-result-object v4

    #@cc
    const-string v5, "\'"

    #@ce
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v5

    #@d2
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@d5
    move-result v4

    #@d6
    if-nez v4, :cond_101

    #@d8
    new-instance v4, Ljava/lang/StringBuilder;

    #@da
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@dd
    const-string v6, " AND ("

    #@df
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v4

    #@e3
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e6
    move-result-object v4

    #@e7
    const-string v6, ")"

    #@e9
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ec
    move-result-object v4

    #@ed
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f0
    move-result-object v4

    #@f1
    :goto_f1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v4

    #@f5
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f8
    move-result-object v2

    #@f9
    .line 718
    .restart local v2       #strDeletedWhere:Ljava/lang/String;
    const-string v4, "setting_table"

    #@fb
    invoke-virtual {v1, v4, v2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    #@fe
    move-result v0

    #@ff
    .line 719
    goto/16 :goto_2d

    #@101
    .line 717
    .end local v2           #strDeletedWhere:Ljava/lang/String;
    :cond_101
    const-string v4, ""

    #@103
    goto :goto_f1

    #@104
    .line 723
    .end local v3           #strIMSI:Ljava/lang/String;
    :pswitch_104
    const-string v4, "working_table"

    #@106
    invoke-virtual {v1, v4, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    #@109
    move-result v0

    #@10a
    .line 724
    goto/16 :goto_2d

    #@10c
    .line 688
    :pswitch_data_10c
    .packed-switch 0x1
        :pswitch_3c
        :pswitch_4f
        :pswitch_ab
        :pswitch_b3
        :pswitch_104
    .end packed-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .registers 5
    .parameter "uri"

    #@0
    .prologue
    .line 284
    const-string v0, "RCS_ProvisioningProvider"

    #@2
    const-string v1, "ProvisioningProvider::getType"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 286
    sget-object v0, Lcom/lge/ims/provisioning/ProvisioningProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@9
    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@c
    move-result v0

    #@d
    packed-switch v0, :pswitch_data_36

    #@10
    .line 305
    :pswitch_10
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@12
    new-instance v1, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v2, "Unknown URI "

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@28
    throw v0

    #@29
    .line 289
    :pswitch_29
    const-string v0, "vnd.android.cursor.dir/vnd.lge.rcse.provisioning"

    #@2b
    .line 301
    :goto_2b
    return-object v0

    #@2c
    .line 293
    :pswitch_2c
    const-string v0, "vnd.android.cursor.id/vnd.lge.rcse.provisioning"

    #@2e
    goto :goto_2b

    #@2f
    .line 297
    :pswitch_2f
    const-string v0, "vnd.android.cursor.dir/vnd.lge.rcse.setting"

    #@31
    goto :goto_2b

    #@32
    .line 301
    :pswitch_32
    const-string v0, "vnd.android.cursor.dir/vnd.lge.rcse.working"

    #@34
    goto :goto_2b

    #@35
    .line 286
    nop

    #@36
    :pswitch_data_36
    .packed-switch 0x1
        :pswitch_29
        :pswitch_2c
        :pswitch_2f
        :pswitch_10
        :pswitch_32
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .registers 15
    .parameter "uri"
    .parameter "values"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v11, 0x1

    #@2
    const-wide/16 v9, 0x0

    #@4
    .line 470
    const-string v5, "RCS_ProvisioningProvider"

    #@6
    new-instance v7, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v8, "ProvisioningProvider::insert - Uri : "

    #@d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v7

    #@11
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@14
    move-result-object v8

    #@15
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v7

    #@19
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v7

    #@1d
    invoke-static {v5, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 472
    const/4 v3, 0x0

    #@21
    .line 473
    .local v3, objInsertedRowUri:Landroid/net/Uri;
    const-wide/16 v0, -0x1

    #@23
    .line 475
    .local v0, nRowId:J
    iget-object v5, p0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningDBHelper:Lcom/lge/ims/provisioning/ProvisioningProvider$ProvisioningDBHelper;

    #@25
    invoke-virtual {v5}, Lcom/lge/ims/provisioning/ProvisioningProvider$ProvisioningDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@28
    move-result-object v4

    #@29
    .line 476
    .local v4, objSQLiteDatabase:Landroid/database/sqlite/SQLiteDatabase;
    sget-object v5, Lcom/lge/ims/provisioning/ProvisioningProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@2b
    invoke-virtual {v5, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@2e
    move-result v2

    #@2f
    .line 478
    .local v2, nUriMatch:I
    packed-switch v2, :pswitch_data_d2

    #@32
    .line 579
    :cond_32
    :goto_32
    if-eqz v3, :cond_b8

    #@34
    .line 580
    invoke-virtual {p0}, Lcom/lge/ims/provisioning/ProvisioningProvider;->getContext()Landroid/content/Context;

    #@37
    move-result-object v5

    #@38
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3b
    move-result-object v5

    #@3c
    invoke-virtual {v5, v3, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    #@3f
    move-object v5, v3

    #@40
    .line 581
    :goto_40
    return-object v5

    #@41
    .line 481
    :pswitch_41
    const-string v5, "ALLSIM"

    #@43
    invoke-static {v5}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@46
    .line 498
    const-string v5, "provisioning_table"

    #@48
    const-string v7, "imsi"

    #@4a
    invoke-virtual {v4, v5, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    #@4d
    move-result-wide v0

    #@4e
    .line 501
    cmp-long v5, v0, v9

    #@50
    if-lez v5, :cond_32

    #@52
    .line 502
    sget-object v5, Lcom/lge/ims/provisioning/SIMCols$ProvisioningTableMetaData;->CONTENT_URI:Landroid/net/Uri;

    #@54
    invoke-static {v5, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    #@57
    move-result-object v3

    #@58
    goto :goto_32

    #@59
    .line 509
    :pswitch_59
    const-string v5, "ONESIM"

    #@5b
    invoke-static {v5}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5e
    .line 510
    const-string v7, "imsi"

    #@60
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@63
    move-result-object v5

    #@64
    invoke-interface {v5, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@67
    move-result-object v5

    #@68
    check-cast v5, Ljava/lang/String;

    #@6a
    invoke-virtual {p2, v7, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@6d
    .line 527
    const-string v5, "provisioning_table"

    #@6f
    const-string v7, "imsi"

    #@71
    invoke-virtual {v4, v5, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    #@74
    move-result-wide v0

    #@75
    .line 530
    cmp-long v5, v0, v9

    #@77
    if-lez v5, :cond_32

    #@79
    .line 531
    sget-object v5, Lcom/lge/ims/provisioning/SIMCols$ProvisioningTableMetaData;->CONTENT_URI:Landroid/net/Uri;

    #@7b
    invoke-static {v5, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    #@7e
    move-result-object v3

    #@7f
    goto :goto_32

    #@80
    .line 538
    :pswitch_80
    const-string v5, "setting_table"

    #@82
    const-string v7, "imsi"

    #@84
    invoke-virtual {v4, v5, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    #@87
    move-result-wide v0

    #@88
    .line 540
    cmp-long v5, v0, v9

    #@8a
    if-lez v5, :cond_32

    #@8c
    .line 541
    sget-object v5, Lcom/lge/ims/provisioning/SIMCols$SettingTableMetaData;->CONTENT_URI:Landroid/net/Uri;

    #@8e
    invoke-static {v5, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    #@91
    move-result-object v3

    #@92
    goto :goto_32

    #@93
    .line 549
    :pswitch_93
    const-string v7, "imsi"

    #@95
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@98
    move-result-object v5

    #@99
    invoke-interface {v5, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@9c
    move-result-object v5

    #@9d
    check-cast v5, Ljava/lang/String;

    #@9f
    invoke-virtual {p2, v7, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@a2
    .line 550
    const-string v5, "setting_table"

    #@a4
    const-string v7, "imsi"

    #@a6
    invoke-virtual {v4, v5, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    #@a9
    move-result-wide v0

    #@aa
    .line 552
    cmp-long v5, v0, v9

    #@ac
    if-lez v5, :cond_32

    #@ae
    .line 553
    sget-object v5, Lcom/lge/ims/provisioning/SIMCols$SettingTableMetaData;->CONTENT_URI:Landroid/net/Uri;

    #@b0
    invoke-static {v5, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    #@b3
    move-result-object v3

    #@b4
    goto/16 :goto_32

    #@b6
    :pswitch_b6
    move-object v5, v6

    #@b7
    .line 569
    goto :goto_40

    #@b8
    .line 584
    :cond_b8
    new-instance v5, Landroid/database/SQLException;

    #@ba
    new-instance v6, Ljava/lang/StringBuilder;

    #@bc
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@bf
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v6

    #@c3
    const-string v7, "can\'t insert"

    #@c5
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v6

    #@c9
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cc
    move-result-object v6

    #@cd
    invoke-direct {v5, v6}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    #@d0
    throw v5

    #@d1
    .line 478
    nop

    #@d2
    :pswitch_data_d2
    .packed-switch 0x1
        :pswitch_41
        :pswitch_59
        :pswitch_80
        :pswitch_93
        :pswitch_b6
    .end packed-switch
.end method

.method public onCreate()Z
    .registers 3

    #@0
    .prologue
    .line 272
    new-instance v0, Lcom/lge/ims/provisioning/ProvisioningProvider$ProvisioningDBHelper;

    #@2
    invoke-virtual {p0}, Lcom/lge/ims/provisioning/ProvisioningProvider;->getContext()Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1}, Lcom/lge/ims/provisioning/ProvisioningProvider$ProvisioningDBHelper;-><init>(Landroid/content/Context;)V

    #@9
    iput-object v0, p0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningDBHelper:Lcom/lge/ims/provisioning/ProvisioningProvider$ProvisioningDBHelper;

    #@b
    .line 273
    iget-object v0, p0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningDBHelper:Lcom/lge/ims/provisioning/ProvisioningProvider$ProvisioningDBHelper;

    #@d
    invoke-virtual {v0}, Lcom/lge/ims/provisioning/ProvisioningProvider$ProvisioningDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@10
    move-result-object v0

    #@11
    iput-object v0, p0, Lcom/lge/ims/provisioning/ProvisioningProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    #@13
    .line 275
    iget-object v0, p0, Lcom/lge/ims/provisioning/ProvisioningProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    #@15
    if-nez v0, :cond_19

    #@17
    .line 276
    const/4 v0, 0x0

    #@18
    .line 279
    :goto_18
    return v0

    #@19
    :cond_19
    const/4 v0, 0x1

    #@1a
    goto :goto_18
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 18
    .parameter "uri"
    .parameter "projection"
    .parameter "selection"
    .parameter "selectionArgs"
    .parameter "sortOrder"

    #@0
    .prologue
    .line 312
    const-string v2, "RCS_ProvisioningProvider"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "ProvisioningProvider::query - Uri : "

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@10
    move-result-object v4

    #@11
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v3

    #@15
    const-string v4, " Selection : "

    #@17
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v3

    #@23
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 314
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    #@28
    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    #@2b
    .line 316
    .local v0, objSQLiteQueryBuilder:Landroid/database/sqlite/SQLiteQueryBuilder;
    sget-object v2, Lcom/lge/ims/provisioning/ProvisioningProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@2d
    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@30
    move-result v2

    #@31
    packed-switch v2, :pswitch_data_1d8

    #@34
    .line 367
    :goto_34
    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@37
    move-result v2

    #@38
    if-eqz v2, :cond_f1

    #@3a
    .line 368
    const-string v7, "_id DESC"

    #@3c
    .line 373
    .local v7, strOrderBy:Ljava/lang/String;
    :goto_3c
    iget-object v2, p0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningDBHelper:Lcom/lge/ims/provisioning/ProvisioningProvider$ProvisioningDBHelper;

    #@3e
    invoke-virtual {v2}, Lcom/lge/ims/provisioning/ProvisioningProvider$ProvisioningDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@41
    move-result-object v1

    #@42
    .line 375
    .local v1, objSQLiteDatabase:Landroid/database/sqlite/SQLiteDatabase;
    sget-object v2, Lcom/lge/ims/provisioning/ProvisioningProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@44
    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@47
    move-result v2

    #@48
    packed-switch v2, :pswitch_data_1e6

    #@4b
    .line 463
    const/4 v10, 0x0

    #@4c
    :cond_4c
    :goto_4c
    return-object v10

    #@4d
    .line 319
    .end local v1           #objSQLiteDatabase:Landroid/database/sqlite/SQLiteDatabase;
    .end local v7           #strOrderBy:Ljava/lang/String;
    :pswitch_4d
    if-nez p3, :cond_51

    #@4f
    if-eqz p4, :cond_59

    #@51
    .line 320
    :cond_51
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@53
    const-string v3, "Uri is invaild"

    #@55
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@58
    throw v2

    #@59
    .line 323
    :cond_59
    const-string v2, "provisioning_table"

    #@5b
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    #@5e
    .line 324
    sget-object v2, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@60
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    #@63
    goto :goto_34

    #@64
    .line 329
    :pswitch_64
    if-nez p3, :cond_68

    #@66
    if-eqz p4, :cond_70

    #@68
    .line 330
    :cond_68
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@6a
    const-string v3, "Uri is invaild"

    #@6c
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@6f
    throw v2

    #@70
    .line 333
    :cond_70
    const-string v2, "provisioning_table"

    #@72
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    #@75
    .line 334
    sget-object v2, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@77
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    #@7a
    .line 335
    new-instance v2, Ljava/lang/StringBuilder;

    #@7c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7f
    const-string v3, "imsi=\'"

    #@81
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v3

    #@85
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@88
    move-result-object v2

    #@89
    const/4 v4, 0x1

    #@8a
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@8d
    move-result-object v2

    #@8e
    check-cast v2, Ljava/lang/String;

    #@90
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v2

    #@94
    const-string v3, "\'"

    #@96
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v2

    #@9a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9d
    move-result-object v2

    #@9e
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    #@a1
    goto :goto_34

    #@a2
    .line 340
    :pswitch_a2
    const-string v2, "setting_table"

    #@a4
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    #@a7
    .line 341
    sget-object v2, Lcom/lge/ims/provisioning/ProvisioningProvider;->objSettingMap:Ljava/util/HashMap;

    #@a9
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    #@ac
    goto :goto_34

    #@ad
    .line 346
    :pswitch_ad
    const-string v2, "setting_table"

    #@af
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    #@b2
    .line 347
    sget-object v2, Lcom/lge/ims/provisioning/ProvisioningProvider;->objSettingMap:Ljava/util/HashMap;

    #@b4
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    #@b7
    .line 348
    new-instance v2, Ljava/lang/StringBuilder;

    #@b9
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@bc
    const-string v3, "imsi=\'"

    #@be
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v3

    #@c2
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@c5
    move-result-object v2

    #@c6
    const/4 v4, 0x1

    #@c7
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@ca
    move-result-object v2

    #@cb
    check-cast v2, Ljava/lang/String;

    #@cd
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d0
    move-result-object v2

    #@d1
    const-string v3, "\'"

    #@d3
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d6
    move-result-object v2

    #@d7
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@da
    move-result-object v2

    #@db
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    #@de
    goto/16 :goto_34

    #@e0
    .line 353
    :pswitch_e0
    const-string v2, "working_table"

    #@e2
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    #@e5
    .line 354
    sget-object v2, Lcom/lge/ims/provisioning/ProvisioningProvider;->objWorkingMap:Ljava/util/HashMap;

    #@e7
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    #@ea
    .line 355
    const-string v2, "working_key=1"

    #@ec
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    #@ef
    goto/16 :goto_34

    #@f1
    .line 370
    :cond_f1
    move-object/from16 v7, p5

    #@f3
    .restart local v7       #strOrderBy:Ljava/lang/String;
    goto/16 :goto_3c

    #@f5
    .line 379
    .restart local v1       #objSQLiteDatabase:Landroid/database/sqlite/SQLiteDatabase;
    :pswitch_f5
    sget-boolean v2, Lcom/lge/ims/provisioning/SIMCols;->bUseEncrypt:Z

    #@f7
    if-eqz v2, :cond_18c

    #@f9
    .line 382
    const/4 v5, 0x0

    #@fa
    const/4 v6, 0x0

    #@fb
    move-object v2, p2

    #@fc
    move-object v3, p3

    #@fd
    move-object/from16 v4, p4

    #@ff
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@102
    move-result-object v10

    #@103
    .line 384
    .local v10, objCursor:Landroid/database/Cursor;
    if-nez v10, :cond_10d

    #@105
    .line 385
    const-string v2, "--- Cursor is null"

    #@107
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@10a
    .line 386
    const/4 v10, 0x0

    #@10b
    goto/16 :goto_4c

    #@10d
    .line 389
    :cond_10d
    new-instance v11, Landroid/database/MatrixCursor;

    #@10f
    invoke-interface {v10}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    #@112
    move-result-object v2

    #@113
    invoke-direct {v11, v2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    #@116
    .line 391
    .local v11, objMatrixCursor:Landroid/database/MatrixCursor;
    :goto_116
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    #@119
    move-result v2

    #@11a
    if-eqz v2, :cond_186

    #@11c
    .line 392
    invoke-interface {v10}, Landroid/database/Cursor;->getColumnCount()I

    #@11f
    move-result v2

    #@120
    new-array v8, v2, [Ljava/lang/Object;

    #@122
    .line 394
    .local v8, aobjObject:[Ljava/lang/Object;
    const/4 v9, 0x0

    #@123
    .local v9, i:I
    :goto_123
    invoke-interface {v10}, Landroid/database/Cursor;->getColumnCount()I

    #@126
    move-result v2

    #@127
    if-ge v9, v2, :cond_182

    #@129
    .line 395
    invoke-interface {v10, v9}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    #@12c
    move-result-object v2

    #@12d
    const-string v3, "imsi"

    #@12f
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@132
    move-result v2

    #@133
    if-eqz v2, :cond_13e

    #@135
    .line 396
    invoke-interface {v10, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@138
    move-result-object v2

    #@139
    aput-object v2, v8, v9

    #@13b
    .line 394
    :goto_13b
    add-int/lit8 v9, v9, 0x1

    #@13d
    goto :goto_123

    #@13e
    .line 399
    :cond_13e
    invoke-interface {v10, v9}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    #@141
    move-result-object v2

    #@142
    const-string v3, "message"

    #@144
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@147
    move-result v2

    #@148
    if-eqz v2, :cond_151

    #@14a
    .line 401
    invoke-interface {v10, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@14d
    move-result-object v2

    #@14e
    aput-object v2, v8, v9

    #@150
    goto :goto_13b

    #@151
    .line 403
    :cond_151
    invoke-interface {v10, v9}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    #@154
    move-result-object v2

    #@155
    const-string v3, "title"

    #@157
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15a
    move-result v2

    #@15b
    if-eqz v2, :cond_164

    #@15d
    .line 405
    invoke-interface {v10, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@160
    move-result-object v2

    #@161
    aput-object v2, v8, v9

    #@163
    goto :goto_13b

    #@164
    .line 408
    :cond_164
    invoke-interface {v10, v9}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    #@167
    move-result-object v2

    #@168
    const-string v3, "_id"

    #@16a
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16d
    move-result v2

    #@16e
    if-eqz v2, :cond_17b

    #@170
    .line 409
    invoke-interface {v10, v9}, Landroid/database/Cursor;->getInt(I)I

    #@173
    move-result v2

    #@174
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@177
    move-result-object v2

    #@178
    aput-object v2, v8, v9

    #@17a
    goto :goto_13b

    #@17b
    .line 412
    :cond_17b
    invoke-interface {v10, v9}, Landroid/database/Cursor;->getBlob(I)[B

    #@17e
    move-result-object v2

    #@17f
    aput-object v2, v8, v9

    #@181
    goto :goto_13b

    #@182
    .line 423
    :cond_182
    invoke-virtual {v11, v8}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    #@185
    goto :goto_116

    #@186
    .line 426
    .end local v8           #aobjObject:[Ljava/lang/Object;
    .end local v9           #i:I
    :cond_186
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@189
    move-object v10, v11

    #@18a
    .line 427
    goto/16 :goto_4c

    #@18c
    .line 430
    .end local v10           #objCursor:Landroid/database/Cursor;
    .end local v11           #objMatrixCursor:Landroid/database/MatrixCursor;
    :cond_18c
    const/4 v5, 0x0

    #@18d
    const/4 v6, 0x0

    #@18e
    move-object v2, p2

    #@18f
    move-object v3, p3

    #@190
    move-object/from16 v4, p4

    #@192
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@195
    move-result-object v10

    #@196
    .line 432
    .restart local v10       #objCursor:Landroid/database/Cursor;
    if-eqz v10, :cond_4c

    #@198
    .line 433
    invoke-virtual {p0}, Lcom/lge/ims/provisioning/ProvisioningProvider;->getContext()Landroid/content/Context;

    #@19b
    move-result-object v2

    #@19c
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@19f
    move-result-object v2

    #@1a0
    invoke-interface {v10, v2, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    #@1a3
    goto/16 :goto_4c

    #@1a5
    .line 442
    .end local v10           #objCursor:Landroid/database/Cursor;
    :pswitch_1a5
    const/4 v5, 0x0

    #@1a6
    const/4 v6, 0x0

    #@1a7
    move-object v2, p2

    #@1a8
    move-object v3, p3

    #@1a9
    move-object/from16 v4, p4

    #@1ab
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@1ae
    move-result-object v10

    #@1af
    .line 444
    .restart local v10       #objCursor:Landroid/database/Cursor;
    if-eqz v10, :cond_4c

    #@1b1
    .line 445
    invoke-virtual {p0}, Lcom/lge/ims/provisioning/ProvisioningProvider;->getContext()Landroid/content/Context;

    #@1b4
    move-result-object v2

    #@1b5
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1b8
    move-result-object v2

    #@1b9
    invoke-interface {v10, v2, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    #@1bc
    goto/16 :goto_4c

    #@1be
    .line 452
    .end local v10           #objCursor:Landroid/database/Cursor;
    :pswitch_1be
    const/4 v5, 0x0

    #@1bf
    const/4 v6, 0x0

    #@1c0
    move-object v2, p2

    #@1c1
    move-object v3, p3

    #@1c2
    move-object/from16 v4, p4

    #@1c4
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@1c7
    move-result-object v10

    #@1c8
    .line 454
    .restart local v10       #objCursor:Landroid/database/Cursor;
    if-eqz v10, :cond_4c

    #@1ca
    .line 455
    invoke-virtual {p0}, Lcom/lge/ims/provisioning/ProvisioningProvider;->getContext()Landroid/content/Context;

    #@1cd
    move-result-object v2

    #@1ce
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1d1
    move-result-object v2

    #@1d2
    invoke-interface {v10, v2, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    #@1d5
    goto/16 :goto_4c

    #@1d7
    .line 316
    nop

    #@1d8
    :pswitch_data_1d8
    .packed-switch 0x1
        :pswitch_4d
        :pswitch_64
        :pswitch_a2
        :pswitch_ad
        :pswitch_e0
    .end packed-switch

    #@1e6
    .line 375
    :pswitch_data_1e6
    .packed-switch 0x1
        :pswitch_f5
        :pswitch_f5
        :pswitch_1a5
        :pswitch_1a5
        :pswitch_1be
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 13
    .parameter "uri"
    .parameter "values"
    .parameter "selection"
    .parameter "selectionArgs"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 589
    const-string v4, "RCS_ProvisioningProvider"

    #@3
    new-instance v5, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v6, "ProvisioningProvider::update - Uri : "

    #@a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v5

    #@e
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@11
    move-result-object v6

    #@12
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v5

    #@16
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v5

    #@1a
    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 591
    iget-object v4, p0, Lcom/lge/ims/provisioning/ProvisioningProvider;->objProvisioningDBHelper:Lcom/lge/ims/provisioning/ProvisioningProvider$ProvisioningDBHelper;

    #@1f
    invoke-virtual {v4}, Lcom/lge/ims/provisioning/ProvisioningProvider$ProvisioningDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@22
    move-result-object v1

    #@23
    .line 592
    .local v1, objSQLiteDatabase:Landroid/database/sqlite/SQLiteDatabase;
    const/4 v0, 0x0

    #@24
    .line 594
    .local v0, nCount:I
    sget-object v4, Lcom/lge/ims/provisioning/ProvisioningProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@26
    invoke-virtual {v4, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@29
    move-result v4

    #@2a
    packed-switch v4, :pswitch_data_134

    #@2d
    .line 673
    :goto_2d
    if-lez v0, :cond_3b

    #@2f
    .line 674
    invoke-virtual {p0}, Lcom/lge/ims/provisioning/ProvisioningProvider;->getContext()Landroid/content/Context;

    #@32
    move-result-object v4

    #@33
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@36
    move-result-object v4

    #@37
    const/4 v5, 0x0

    #@38
    invoke-virtual {v4, p1, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    #@3b
    .line 677
    :cond_3b
    return v0

    #@3c
    .line 616
    :pswitch_3c
    const-string v4, "provisioning_table"

    #@3e
    invoke-virtual {v1, v4, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@41
    move-result v0

    #@42
    .line 619
    goto :goto_2d

    #@43
    .line 623
    :pswitch_43
    if-nez p3, :cond_47

    #@45
    if-eqz p4, :cond_4f

    #@47
    .line 624
    :cond_47
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@49
    const-string v5, "Uri is invaild"

    #@4b
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@4e
    throw v4

    #@4f
    .line 627
    :cond_4f
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@52
    move-result-object v4

    #@53
    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@56
    move-result-object v2

    #@57
    check-cast v2, Ljava/lang/String;

    #@59
    .line 628
    .local v2, strIMSI:Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v5, "imsi=\'"

    #@60
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v4

    #@64
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v4

    #@68
    const-string v5, "\'"

    #@6a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v5

    #@6e
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@71
    move-result v4

    #@72
    if-nez v4, :cond_9c

    #@74
    new-instance v4, Ljava/lang/StringBuilder;

    #@76
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@79
    const-string v6, " AND ("

    #@7b
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v4

    #@7f
    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v4

    #@83
    const-string v6, ")"

    #@85
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v4

    #@89
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8c
    move-result-object v4

    #@8d
    :goto_8d
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v4

    #@91
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v3

    #@95
    .line 644
    .local v3, strUpdatedWhere:Ljava/lang/String;
    const-string v4, "provisioning_table"

    #@97
    invoke-virtual {v1, v4, p2, v3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@9a
    move-result v0

    #@9b
    .line 646
    goto :goto_2d

    #@9c
    .line 628
    .end local v3           #strUpdatedWhere:Ljava/lang/String;
    :cond_9c
    const-string v4, ""

    #@9e
    goto :goto_8d

    #@9f
    .line 650
    .end local v2           #strIMSI:Ljava/lang/String;
    :pswitch_9f
    const-string v4, "setting_table"

    #@a1
    invoke-virtual {v1, v4, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@a4
    move-result v0

    #@a5
    .line 651
    goto :goto_2d

    #@a6
    .line 655
    :pswitch_a6
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@a9
    move-result-object v4

    #@aa
    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@ad
    move-result-object v2

    #@ae
    check-cast v2, Ljava/lang/String;

    #@b0
    .line 656
    .restart local v2       #strIMSI:Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    #@b2
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@b5
    const-string v5, "imsi=\'"

    #@b7
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v4

    #@bb
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v4

    #@bf
    const-string v5, "\'"

    #@c1
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v5

    #@c5
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@c8
    move-result v4

    #@c9
    if-nez v4, :cond_f4

    #@cb
    new-instance v4, Ljava/lang/StringBuilder;

    #@cd
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@d0
    const-string v6, " AND ("

    #@d2
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v4

    #@d6
    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v4

    #@da
    const-string v6, ")"

    #@dc
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@df
    move-result-object v4

    #@e0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e3
    move-result-object v4

    #@e4
    :goto_e4
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v4

    #@e8
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@eb
    move-result-object v3

    #@ec
    .line 657
    .restart local v3       #strUpdatedWhere:Ljava/lang/String;
    const-string v4, "setting_table"

    #@ee
    invoke-virtual {v1, v4, p2, v3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@f1
    move-result v0

    #@f2
    .line 658
    goto/16 :goto_2d

    #@f4
    .line 656
    .end local v3           #strUpdatedWhere:Ljava/lang/String;
    :cond_f4
    const-string v4, ""

    #@f6
    goto :goto_e4

    #@f7
    .line 662
    .end local v2           #strIMSI:Ljava/lang/String;
    :pswitch_f7
    new-instance v4, Ljava/lang/StringBuilder;

    #@f9
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@fc
    const-string v5, "working_key=1"

    #@fe
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@101
    move-result-object v5

    #@102
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@105
    move-result v4

    #@106
    if-nez v4, :cond_131

    #@108
    new-instance v4, Ljava/lang/StringBuilder;

    #@10a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@10d
    const-string v6, " AND ("

    #@10f
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@112
    move-result-object v4

    #@113
    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@116
    move-result-object v4

    #@117
    const-string v6, ")"

    #@119
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11c
    move-result-object v4

    #@11d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@120
    move-result-object v4

    #@121
    :goto_121
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@124
    move-result-object v4

    #@125
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@128
    move-result-object v3

    #@129
    .line 663
    .restart local v3       #strUpdatedWhere:Ljava/lang/String;
    const-string v4, "working_table"

    #@12b
    invoke-virtual {v1, v4, p2, v3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@12e
    move-result v0

    #@12f
    .line 664
    goto/16 :goto_2d

    #@131
    .line 662
    .end local v3           #strUpdatedWhere:Ljava/lang/String;
    :cond_131
    const-string v4, ""

    #@133
    goto :goto_121

    #@134
    .line 594
    :pswitch_data_134
    .packed-switch 0x1
        :pswitch_3c
        :pswitch_43
        :pswitch_9f
        :pswitch_a6
        :pswitch_f7
    .end packed-switch
.end method
