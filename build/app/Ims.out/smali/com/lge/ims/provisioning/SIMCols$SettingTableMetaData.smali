.class public final Lcom/lge/ims/provisioning/SIMCols$SettingTableMetaData;
.super Ljava/lang/Object;
.source "SIMCols.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/provisioning/SIMCols;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SettingTableMetaData"
.end annotation


# static fields
.field public static final AGAIN_NOTIFICATION:Ljava/lang/String; = "again_notification"

.field public static final CONTENT_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/vnd.lge.rcse.setting"

.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final ID:Ljava/lang/String; = "_id"

.field public static final IMSI:Ljava/lang/String; = "imsi"

.field public static final IS_ACCEPT:Ljava/lang/String; = "is_accept"

.field public static final LATEST_POLLING:Ljava/lang/String; = "latest_polling"

.field public static final RCS_E_ROAMING:Ljava/lang/String; = "rcs_e_roaming"

.field public static final RCS_E_SERVICE:Ljava/lang/String; = "rcs_e_service"

.field public static final SAME_XML_VERSION:Ljava/lang/String; = "same_xml_version"

.field public static final SETTING_TABLE:Ljava/lang/String; = "setting_table"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 215
    const-string v0, "content://com.lge.ims.provisioning/settings"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/lge/ims/provisioning/SIMCols$SettingTableMetaData;->CONTENT_URI:Landroid/net/Uri;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 212
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
