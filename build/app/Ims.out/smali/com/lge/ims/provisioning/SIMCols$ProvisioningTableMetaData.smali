.class public final Lcom/lge/ims/provisioning/SIMCols$ProvisioningTableMetaData;
.super Ljava/lang/Object;
.source "SIMCols.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/provisioning/SIMCols;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ProvisioningTableMetaData"
.end annotation


# static fields
.field public static final ABCINFO_ADDRESS:Ljava/lang/String; = "abcinfo_address"

.field public static final ABCINFO_ADDRESSTYPE:Ljava/lang/String; = "abcinfo_addressType"

.field public static final ACCEPT_BTN:Ljava/lang/String; = "accept_btn"

.field public static final AC_VERSION:Ljava/lang/String; = "ac_version"

.field public static final AUTH_TYPE:Ljava/lang/String; = "auth_type"

.field public static final AUT_ACCEPT:Ljava/lang/String; = "aut_accept"

.field public static final AUT_MA:Ljava/lang/String; = "aut_ma"

.field public static final AVAILABILITY_AUTH:Ljava/lang/String; = "availability_auth"

.field public static final CAPABILITY_INFO_EXPIRY:Ljava/lang/String; = "capability_info_expiry"

.field public static final CHAT:Ljava/lang/String; = "chat"

.field public static final CHAT_AUTH:Ljava/lang/String; = "chat_auth"

.field public static final CLIENT_OBJ_DATALIMIT:Ljava/lang/String; = "client_obj_datalimit"

.field public static final CONREF:Ljava/lang/String; = "ConRef"

.field public static final CONTACT_CAP_PRES_AUT:Ljava/lang/String; = "contact_cap_pres_aut"

.field public static final CONTENT_ID_TYPE:Ljava/lang/String; = "vnd.android.cursor.id/vnd.lge.rcse.provisioning"

.field public static final CONTENT_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/vnd.lge.rcse.provisioning"

.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final DEFERRED_MSG_FUNC_URI:Ljava/lang/String; = "deferred_msg_func_uri"

.field public static final ENABLE_RCS_E_SWITCH:Ljava/lang/String; = "enable_rcs_e_switch"

.field public static final END_USER_CONF_REQ_ID:Ljava/lang/String; = "end_user_conf_req_id"

.field public static final EXPLODER_URI:Ljava/lang/String; = "exploder_uri"

.field public static final FETCH_AUTH:Ljava/lang/String; = "fetch_auth"

.field public static final FILE_TRANFER:Ljava/lang/String; = "file_tranfer"

.field public static final FT_MAX_SIZE:Ljava/lang/String; = "ft_max_size"

.field public static final FT_WARN_SIZE:Ljava/lang/String; = "ft_warn_size"

.field public static final HOME_NETWORK_DOMAIN_NAME:Ljava/lang/String; = "home_network_domain_name"

.field public static final ICON_MAXSIZE:Ljava/lang/String; = "icon_maxsize"

.field public static final ICSI:Ljava/lang/String; = "ICSI"

.field public static final ICSI_RESOURCE_ALLOCATION_MODE:Ljava/lang/String; = "icsi_resource_allocation_mode"

.field public static final ID:Ljava/lang/String; = "_id"

.field public static final IMAGE_SHARE:Ljava/lang/String; = "image_share"

.field public static final IMSI:Ljava/lang/String; = "imsi"

.field public static final IM_CAP_ALWAYS_ON:Ljava/lang/String; = "im_cap_always_on"

.field public static final IM_CONFERENCE_FACTORY_URI:Ljava/lang/String; = "im_conference_factory_uri"

.field public static final IM_CONTENT_SERVER_URI:Ljava/lang/String; = "im_content_server_uri"

.field public static final IM_CONV_HIST_FUNC_URI:Ljava/lang/String; = "im_conv_hist_func_uri"

.field public static final IM_DELETE_URI:Ljava/lang/String; = "im_delete_uri"

.field public static final IM_SESSION_START:Ljava/lang/String; = "imSessionStart"

.field public static final IM_WARN_SF:Ljava/lang/String; = "im_warn_sf"

.field public static final INT_URL_FMT:Ljava/lang/String; = "int_url_fmt"

.field public static final KEEP_ALIVE_ENABLED:Ljava/lang/String; = "Keep_Alive_Enabled"

.field public static final LBO_P_CSCF_ADDRESS:Ljava/lang/String; = "lbo_p_cscf_address"

.field public static final LBO_P_CSCF_ADDRESS_TYPE:Ljava/lang/String; = "lbo_p_cscf_address_type"

.field public static final LISTCAP_INFO_EXPIRY:Ljava/lang/String; = "ListcapinfoExpiry"

.field public static final MAX_ADHOC_GROUP_SIZE:Ljava/lang/String; = "max_adhoc_group_size"

.field public static final MAX_NUMBER_OF_SUBSCRIPTIONS_INPRESENCE_IST:Ljava/lang/String; = "max_number_of_subscriptions_inpresence_ist"

.field public static final MAX_SIZE_1TO1:Ljava/lang/String; = "max_size_1to1"

.field public static final MAX_SIZE_1TOM:Ljava/lang/String; = "max_size_1tom"

.field public static final MAX_SIZE_IMAGE_SHARE:Ljava/lang/String; = "max_size_image_share"

.field public static final MAX_TIME_VIDEO_SHARE:Ljava/lang/String; = "max_time_video_share"

.field public static final MESSAGE:Ljava/lang/String; = "message"

.field public static final MOBILITY_MANAGEMENT_IMS_VOICE_TERMINATON:Ljava/lang/String; = "Mobility_Management_IMS_Voice_Terminaton"

.field public static final NAT_URL_FMT:Ljava/lang/String; = "nat_url_fmt"

.field public static final NOTE_MAXSIZE:Ljava/lang/String; = "note_maxsize"

.field public static final ONECONTACT_INTERVAL:Ljava/lang/String; = "one_contact_interval"

.field public static final OP_FAV_URL:Ljava/lang/String; = "op_fav_url"

.field public static final OTHER_DEVICEID:Ljava/lang/String; = "other_deviceid"

.field public static final OTHER_TERMSVERSION:Ljava/lang/String; = "other_termsversion"

.field public static final OTHER_TIMESTAMP:Ljava/lang/String; = "other_timestamp"

.field public static final PCSCF_ADDRESS:Ljava/lang/String; = "p_cscf_address"

.field public static final PDP_CONTEXT_OPER_PREF:Ljava/lang/String; = "pdp_context_oper_pref"

.field public static final PHONE_CONTEXT:Ljava/lang/String; = "phone_context"

.field public static final POLLING_PERIOD:Ljava/lang/String; = "polling_period"

.field public static final PRESENCE_CONTENT_SERVER_URI:Ljava/lang/String; = "presence_content_server_uri"

.field public static final PRESENCE_DISCOVERY:Ljava/lang/String; = "presence_discovery"

.field public static final PRESENCE_PROFILE:Ljava/lang/String; = "presence_profile"

.field public static final PRESENCE_PUBLISHTIMER:Ljava/lang/String; = "presence_publishtimer"

.field public static final PRES_SRV_CAP:Ljava/lang/String; = "pres_srv_cap"

.field public static final PRIVATE_USER_IDENTITY:Ljava/lang/String; = "private_user_identity"

.field public static final PROVISIONING_TABLE:Ljava/lang/String; = "provisioning_table"

.field public static final PS_MEDIA:Ljava/lang/String; = "psMedia"

.field public static final PS_RTMEDIA:Ljava/lang/String; = "psRTMedia"

.field public static final PS_SIGNALING:Ljava/lang/String; = "psSignalling"

.field public static final Q_VALUE:Ljava/lang/String; = "q_value"

.field public static final RCS_E_ONLY_APN:Ljava/lang/String; = "rcs_e_only_apn"

.field public static final REALM:Ljava/lang/String; = "realm"

.field public static final REGRETRY_INITIALRETRY:Ljava/lang/String; = "regretry_initialRetry"

.field public static final REGRETRY_INTERVAL1:Ljava/lang/String; = "regretry_interval1"

.field public static final REGRETRY_INTERVAL2:Ljava/lang/String; = "regretry_interval2"

.field public static final REGRETRY_INTERVAL3:Ljava/lang/String; = "regretry_interval3"

.field public static final REGRETRY_REPEATRETRY:Ljava/lang/String; = "regretry_repeatRetry"

.field public static final REGRETRY_SECONDRETRY:Ljava/lang/String; = "regretry_secondRetry"

.field public static final REGRETRY_THIRDRETRY:Ljava/lang/String; = "regretry_thirdRetry"

.field public static final REG_RETRY_BASETIME:Ljava/lang/String; = "reg_retry_basetime"

.field public static final REG_RETRY_MAXTIME:Ljava/lang/String; = "reg_retry_maxtime"

.field public static final REJECT_BTN:Ljava/lang/String; = "reject_btn"

.field public static final REVOKE_TIMER:Ljava/lang/String; = "revoke_timer"

.field public static final RLS_SUBS_PERIOD:Ljava/lang/String; = "rls_subs_period"

.field public static final RTPPORT_PORT1:Ljava/lang/String; = "rtpport_port1"

.field public static final RTPPORT_PORT2:Ljava/lang/String; = "rtpport_port2"

.field public static final RTPPORT_PORT3:Ljava/lang/String; = "rtpport_port3"

.field public static final RTPPORT_PORT4:Ljava/lang/String; = "rtpport_port4"

.field public static final SBCINFO_ADDRESS:Ljava/lang/String; = "sbcinfo_address"

.field public static final SBCINFO_ADDRESSTYPE:Ljava/lang/String; = "sbcinfo_addressType"

.field public static final SBCTLS_ADDRESS:Ljava/lang/String; = "sbc_tls_address"

.field public static final SBCTLS_ADDRESSTYPE:Ljava/lang/String; = "sbc_tls_addressType"

.field public static final SEND_SMS:Ljava/lang/String; = "send_sms"

.field public static final SERVICE_URI_TEMPLATE:Ljava/lang/String; = "service_uritemplate"

.field public static final SIP_PASSWORD:Ljava/lang/String; = "sip_password"

.field public static final SIP_PROXY_ADDR:Ljava/lang/String; = "sip_proxy_addr"

.field public static final SIP_PROXY_ADDR1:Ljava/lang/String; = "sip_proxy_addr1"

.field public static final SIP_PROXY_ADDR2:Ljava/lang/String; = "sip_proxy_addr2"

.field public static final SIP_PROXY_MAX:Ljava/lang/String; = "sip_proxy_max"

.field public static final SIP_PROXY_PORT:Ljava/lang/String; = "sip_proxy_port"

.field public static final SIP_PROXY_PORT1:Ljava/lang/String; = "sip_proxy_port1"

.field public static final SIP_PROXY_PORT2:Ljava/lang/String; = "sip_proxy_port2"

.field public static final SIP_PROXY_TCP_ADDR:Ljava/lang/String; = "sip_proxy_tcp_addr"

.field public static final SIP_PROXY_TCP_ADDR1:Ljava/lang/String; = "sip_proxy_tcp_addr1"

.field public static final SIP_PROXY_TCP_ADDR2:Ljava/lang/String; = "sip_proxy_tcp_addr2"

.field public static final SIP_PROXY_TCP_MAX:Ljava/lang/String; = "sip_proxy_tcp_max"

.field public static final SIP_PROXY_TCP_PORT:Ljava/lang/String; = "sip_proxy_tcp_port"

.field public static final SIP_PROXY_TCP_PORT1:Ljava/lang/String; = "sip_proxy_tcp_port1"

.field public static final SIP_PROXY_TCP_PORT2:Ljava/lang/String; = "sip_proxy_tcp_port2"

.field public static final SIP_URI:Ljava/lang/String; = "sip_uri"

.field public static final SIP_USER:Ljava/lang/String; = "sip_user"

.field public static final SMS_FALL_BACK_AUTH:Ljava/lang/String; = "sms_fall_back_auth"

.field public static final SMS_OVER_IP_NETWORKS_INDICATION:Ljava/lang/String; = "sms_over_ip_networks_indication"

.field public static final SOFTWARE_URL:Ljava/lang/String; = "software_url"

.field public static final SOFTWARE_VERSION:Ljava/lang/String; = "software_version"

.field public static final SOURCE_THROTTLEPUBLISH:Ljava/lang/String; = "source_throttlepublish"

.field public static final STATUS_CODE:Ljava/lang/String; = "status_code"

.field public static final STATUS_MESSAGE:Ljava/lang/String; = "status_message"

.field public static final TEL_URI:Ljava/lang/String; = "tel_uri"

.field public static final TIMER_A:Ljava/lang/String; = "timer_a"

.field public static final TIMER_B:Ljava/lang/String; = "timer_b"

.field public static final TIMER_C:Ljava/lang/String; = "timer_c"

.field public static final TIMER_D:Ljava/lang/String; = "timer_d"

.field public static final TIMER_E:Ljava/lang/String; = "timer_e"

.field public static final TIMER_F:Ljava/lang/String; = "timer_f"

.field public static final TIMER_G:Ljava/lang/String; = "timer_g"

.field public static final TIMER_H:Ljava/lang/String; = "timer_h"

.field public static final TIMER_I:Ljava/lang/String; = "timer_i"

.field public static final TIMER_IDLE:Ljava/lang/String; = "timer_idle"

.field public static final TIMER_J:Ljava/lang/String; = "timer_j"

.field public static final TIMER_K:Ljava/lang/String; = "timer_k"

.field public static final TIMER_T1:Ljava/lang/String; = "timer_t1"

.field public static final TIMER_T2:Ljava/lang/String; = "timer_t2"

.field public static final TIMER_T4:Ljava/lang/String; = "timer_t4"

.field public static final TITLE:Ljava/lang/String; = "title"

.field public static final TO_APP_REF:Ljava/lang/String; = "to_app_ref"

.field public static final USE_PRESENCE:Ljava/lang/String; = "use_presence"

.field public static final VALIDITY_PERIOD:Ljava/lang/String; = "validity_period"

.field public static final VIDEO_SHARE:Ljava/lang/String; = "Video_Share"

.field public static final VOICE_CALL:Ljava/lang/String; = "voice_call"

.field public static final VOICE_DOMAIN_PREFERENCE_E_UTRAN:Ljava/lang/String; = "voice_domain_preference_e_utran"

.field public static final VOICE_DOMAIN_PREFERENCE_UTRAN:Ljava/lang/String; = "Voice_Domain_Preference_UTRAN"

.field public static final WATCHER_FETCH_AUTH:Ljava/lang/String; = "watcher_fetch_auth"

.field public static final WIFI_MEDIA:Ljava/lang/String; = "wifiMedia"

.field public static final WIFI_RTMEDIA:Ljava/lang/String; = "wifiRTMedia"

.field public static final WIFI_SIGNALING:Ljava/lang/String; = "wifiSignalling"

.field public static final XCAP_AUTHENTICATION_SECRET:Ljava/lang/String; = "xcap_authentication_secret"

.field public static final XCAP_AUTHENTICATION_TYPE:Ljava/lang/String; = "xcap_authentication_type"

.field public static final XCAP_AUTHENTICATION_USER_NAME:Ljava/lang/String; = "xcap_authentication_user_name"

.field public static final XCAP_ROOT_URI:Ljava/lang/String; = "xcap_root_uri"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 25
    const-string v0, "content://com.lge.ims.provisioning/sims"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/lge/ims/provisioning/SIMCols$ProvisioningTableMetaData;->CONTENT_URI:Landroid/net/Uri;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 21
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
