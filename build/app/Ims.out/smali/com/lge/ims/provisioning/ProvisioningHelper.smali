.class public Lcom/lge/ims/provisioning/ProvisioningHelper;
.super Ljava/lang/Object;
.source "ProvisioningHelper.java"


# static fields
.field private static final KT_DOMAIN:Ljava/lang/String; = "ims.kt.com"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 17
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static InsertDefaultProvisioningByIMSI(Landroid/content/Context;Ljava/lang/String;)Z
    .registers 14
    .parameter "objContext"
    .parameter "strIMSI"

    #@0
    .prologue
    const/16 v11, 0xc8

    #@2
    const/16 v10, 0x7d0

    #@4
    const v9, 0x1f400

    #@7
    const/4 v5, 0x1

    #@8
    const/4 v4, 0x0

    #@9
    .line 22
    const-string v3, "ims.kt.com"

    #@b
    .line 24
    .local v3, strDomain:Ljava/lang/String;
    if-eqz p0, :cond_13

    #@d
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@10
    move-result v6

    #@11
    if-eqz v6, :cond_14

    #@13
    .line 223
    :cond_13
    :goto_13
    return v4

    #@14
    .line 28
    :cond_14
    new-instance v1, Landroid/content/ContentValues;

    #@16
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    #@19
    .line 29
    .local v1, objContentValues:Landroid/content/ContentValues;
    const-string v6, "imsi"

    #@1b
    invoke-virtual {v1, v6, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1e
    .line 31
    const-string v6, "sip_proxy_max"

    #@20
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@23
    move-result-object v7

    #@24
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@27
    .line 32
    const-string v6, "sip_proxy_addr"

    #@29
    const-string v7, "62.87.89.70"

    #@2b
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2e
    .line 33
    const-string v6, "sip_proxy_port"

    #@30
    const/16 v7, 0x13e7

    #@32
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@35
    move-result-object v7

    #@36
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@39
    .line 34
    const-string v6, "sip_proxy_addr1"

    #@3b
    const-string v7, "0.0.0.0"

    #@3d
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@40
    .line 35
    const-string v6, "sip_proxy_port1"

    #@42
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@45
    move-result-object v7

    #@46
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@49
    .line 36
    const-string v6, "sip_proxy_addr2"

    #@4b
    const-string v7, "0.0.0.0"

    #@4d
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@50
    .line 37
    const-string v6, "sip_proxy_port2"

    #@52
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@55
    move-result-object v7

    #@56
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@59
    .line 39
    const-string v6, "sip_proxy_tcp_max"

    #@5b
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5e
    move-result-object v7

    #@5f
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@62
    .line 40
    const-string v6, "sip_proxy_tcp_addr"

    #@64
    const-string v7, "0.0.0.0"

    #@66
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@69
    .line 41
    const-string v6, "sip_proxy_tcp_port"

    #@6b
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6e
    move-result-object v7

    #@6f
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@72
    .line 42
    const-string v6, "sip_proxy_tcp_addr1"

    #@74
    const-string v7, "0.0.0.0"

    #@76
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@79
    .line 43
    const-string v6, "sip_proxy_tcp_port1"

    #@7b
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@7e
    move-result-object v7

    #@7f
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@82
    .line 44
    const-string v6, "sip_proxy_tcp_addr2"

    #@84
    const-string v7, "0.0.0.0"

    #@86
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@89
    .line 45
    const-string v6, "sip_proxy_tcp_port2"

    #@8b
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8e
    move-result-object v7

    #@8f
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@92
    .line 47
    const-string v6, "tel_uri"

    #@94
    const-string v7, "tel:01050301234"

    #@96
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@99
    .line 48
    const-string v6, "sip_uri"

    #@9b
    new-instance v7, Ljava/lang/StringBuilder;

    #@9d
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@a0
    const-string v8, "sip:01032164410@"

    #@a2
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v7

    #@a6
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v7

    #@aa
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ad
    move-result-object v7

    #@ae
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@b1
    .line 49
    const-string v6, "sip_user"

    #@b3
    new-instance v7, Ljava/lang/StringBuilder;

    #@b5
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@b8
    const-string v8, "450086000001597@"

    #@ba
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v7

    #@be
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v7

    #@c2
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c5
    move-result-object v7

    #@c6
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@c9
    .line 50
    const-string v6, "sip_password"

    #@cb
    const-string v7, "01032164410"

    #@cd
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@d0
    .line 51
    const-string v6, "im_conference_factory_uri"

    #@d2
    new-instance v7, Ljava/lang/StringBuilder;

    #@d4
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@d7
    const-string v8, "sip:conference-factory@"

    #@d9
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v7

    #@dd
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e0
    move-result-object v7

    #@e1
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e4
    move-result-object v7

    #@e5
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@e8
    .line 52
    const-string v6, "im_cap_always_on"

    #@ea
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ed
    move-result-object v7

    #@ee
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@f1
    .line 53
    const-string v6, "im_warn_sf"

    #@f3
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@f6
    move-result-object v7

    #@f7
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@fa
    .line 54
    const-string v6, "polling_period"

    #@fc
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ff
    move-result-object v7

    #@100
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@103
    .line 55
    const-string v6, "capability_info_expiry"

    #@105
    const v7, 0x2b750

    #@108
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10b
    move-result-object v7

    #@10c
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@10f
    .line 56
    const-string v6, "ListcapinfoExpiry"

    #@111
    const/16 v7, 0x708

    #@113
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@116
    move-result-object v7

    #@117
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@11a
    .line 57
    const-string v6, "use_presence"

    #@11c
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11f
    move-result-object v7

    #@120
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@123
    .line 58
    const-string v6, "presence_discovery"

    #@125
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@128
    move-result-object v7

    #@129
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@12c
    .line 59
    const-string v6, "presence_profile"

    #@12e
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@131
    move-result-object v7

    #@132
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@135
    .line 60
    const-string v6, "enable_rcs_e_switch"

    #@137
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@13a
    move-result-object v7

    #@13b
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@13e
    .line 61
    const-string v6, "rcs_e_only_apn"

    #@140
    const-string v7, "apn.ktf.com"

    #@142
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@145
    .line 62
    const-string v6, "ft_warn_size"

    #@147
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@14a
    move-result-object v7

    #@14b
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@14e
    .line 63
    const-string v6, "ft_max_size"

    #@150
    const/16 v7, 0x2710

    #@152
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@155
    move-result-object v7

    #@156
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@159
    .line 64
    const-string v6, "end_user_conf_req_id"

    #@15b
    const-string v7, "sip:01032164410@ims.kt.com"

    #@15d
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@160
    .line 65
    const-string v6, "validity_period"

    #@162
    const v7, 0x2b750

    #@165
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@168
    move-result-object v7

    #@169
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@16c
    .line 66
    const-string v6, "ac_version"

    #@16e
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@171
    move-result-object v7

    #@172
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@175
    .line 67
    const-string v6, "title"

    #@177
    const-string v7, "Example"

    #@179
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@17c
    .line 68
    const-string v6, "message"

    #@17e
    const-string v7, "Hello world"

    #@180
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@183
    .line 69
    const-string v6, "accept_btn"

    #@185
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@188
    move-result-object v7

    #@189
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@18c
    .line 70
    const-string v6, "reject_btn"

    #@18e
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@191
    move-result-object v7

    #@192
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@195
    .line 72
    const-string v6, "ConRef"

    #@197
    const-string v7, "kt_con_ref"

    #@199
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@19c
    .line 73
    const-string v6, "pdp_context_oper_pref"

    #@19e
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a1
    move-result-object v7

    #@1a2
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@1a5
    .line 74
    const-string v6, "p_cscf_address"

    #@1a7
    const-string v7, "10.10.10.1:5060"

    #@1a9
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1ac
    .line 75
    const-string v6, "timer_t1"

    #@1ae
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1b1
    move-result-object v7

    #@1b2
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@1b5
    .line 76
    const-string v6, "timer_t2"

    #@1b7
    const/16 v7, 0x3e80

    #@1b9
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1bc
    move-result-object v7

    #@1bd
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@1c0
    .line 77
    const-string v6, "timer_t4"

    #@1c2
    const/16 v7, 0x4268

    #@1c4
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1c7
    move-result-object v7

    #@1c8
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@1cb
    .line 78
    const-string v6, "private_user_identity"

    #@1cd
    new-instance v7, Ljava/lang/StringBuilder;

    #@1cf
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1d2
    const-string v8, "450086000001597@"

    #@1d4
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d7
    move-result-object v7

    #@1d8
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1db
    move-result-object v7

    #@1dc
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1df
    move-result-object v7

    #@1e0
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1e3
    .line 79
    const-string v6, "home_network_domain_name"

    #@1e5
    invoke-virtual {v1, v6, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1e8
    .line 80
    const-string v6, "nat_url_fmt"

    #@1ea
    const-string v7, "0"

    #@1ec
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1ef
    .line 81
    const-string v6, "int_url_fmt"

    #@1f1
    const-string v7, "0"

    #@1f3
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1f6
    .line 82
    const-string v6, "q_value"

    #@1f8
    const-string v7, "0.1"

    #@1fa
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1fd
    .line 84
    const-string v6, "voice_call"

    #@1ff
    const-string v7, "0"

    #@201
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@204
    .line 85
    const-string v6, "chat"

    #@206
    const-string v7, "1"

    #@208
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@20b
    .line 86
    const-string v6, "send_sms"

    #@20d
    const-string v7, "1"

    #@20f
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@212
    .line 87
    const-string v6, "file_tranfer"

    #@214
    const-string v7, "1"

    #@216
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@219
    .line 88
    const-string v6, "Video_Share"

    #@21b
    const-string v7, "1"

    #@21d
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@220
    .line 89
    const-string v6, "image_share"

    #@222
    const-string v7, "1"

    #@224
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@227
    .line 90
    const-string v6, "max_size_image_share"

    #@229
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@22c
    move-result-object v7

    #@22d
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@230
    .line 91
    const-string v6, "max_time_video_share"

    #@232
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@235
    move-result-object v7

    #@236
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@239
    .line 93
    const-string v6, "ICSI"

    #@23b
    const-string v7, "kt.icsi.test"

    #@23d
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@240
    .line 94
    const-string v6, "icsi_resource_allocation_mode"

    #@242
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@245
    move-result-object v7

    #@246
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@249
    .line 96
    const-string v6, "lbo_p_cscf_address"

    #@24b
    const-string v7, "125.152.96.245:5060"

    #@24d
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@250
    .line 97
    const-string v6, "lbo_p_cscf_address_type"

    #@252
    const-string v7, "IPv4"

    #@254
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@257
    .line 98
    const-string v6, "voice_domain_preference_e_utran"

    #@259
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@25c
    move-result-object v7

    #@25d
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@260
    .line 99
    const-string v6, "sms_over_ip_networks_indication"

    #@262
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@265
    move-result-object v7

    #@266
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@269
    .line 100
    const-string v6, "Keep_Alive_Enabled"

    #@26b
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@26e
    move-result-object v7

    #@26f
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@272
    .line 101
    const-string v6, "Voice_Domain_Preference_UTRAN"

    #@274
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@277
    move-result-object v7

    #@278
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@27b
    .line 102
    const-string v6, "Mobility_Management_IMS_Voice_Terminaton"

    #@27d
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@280
    move-result-object v7

    #@281
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@284
    .line 103
    const-string v6, "reg_retry_basetime"

    #@286
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@289
    move-result-object v7

    #@28a
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@28d
    .line 104
    const-string v6, "reg_retry_maxtime"

    #@28f
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@292
    move-result-object v7

    #@293
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@296
    .line 106
    const-string v6, "phone_context"

    #@298
    invoke-virtual {v1, v6, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@29b
    .line 108
    const-string v6, "auth_type"

    #@29d
    const-string v7, "Digest"

    #@29f
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2a2
    .line 109
    const-string v6, "realm"

    #@2a4
    invoke-virtual {v1, v6, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2a7
    .line 111
    const-string v6, "to_app_ref"

    #@2a9
    const-string v7, "IMS-Settings"

    #@2ab
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2ae
    .line 113
    const-string v6, "availability_auth"

    #@2b0
    const-string v7, "1"

    #@2b2
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2b5
    .line 115
    const-string v6, "aut_ma"

    #@2b7
    const-string v7, "0"

    #@2b9
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2bc
    .line 116
    const-string v6, "op_fav_url"

    #@2be
    const-string v7, "X"

    #@2c0
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2c3
    .line 117
    const-string v6, "icon_maxsize"

    #@2c5
    const v7, 0x76980

    #@2c8
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2cb
    move-result-object v7

    #@2cc
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@2cf
    .line 118
    const-string v6, "note_maxsize"

    #@2d1
    const/16 v7, 0x3e8

    #@2d3
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2d6
    move-result-object v7

    #@2d7
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@2da
    .line 120
    const-string v6, "fetch_auth"

    #@2dc
    const-string v7, "1"

    #@2de
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2e1
    .line 121
    const-string v6, "contact_cap_pres_aut"

    #@2e3
    const-string v7, "1"

    #@2e5
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2e8
    .line 123
    const-string v6, "watcher_fetch_auth"

    #@2ea
    const-string v7, "1"

    #@2ec
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2ef
    .line 124
    const-string v6, "client_obj_datalimit"

    #@2f1
    const-string v7, "10000"

    #@2f3
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2f6
    .line 125
    const-string v6, "presence_content_server_uri"

    #@2f8
    const-string v7, "221.148.247.125:443"

    #@2fa
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2fd
    .line 126
    const-string v6, "source_throttlepublish"

    #@2ff
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@302
    move-result-object v7

    #@303
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@306
    .line 127
    const-string v6, "max_number_of_subscriptions_inpresence_ist"

    #@308
    const-string v7, "200"

    #@30a
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@30d
    .line 128
    const-string v6, "service_uritemplate"

    #@30f
    const-string v7, "x"

    #@311
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@314
    .line 129
    const-string v6, "rls_subs_period"

    #@316
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@319
    move-result-object v7

    #@31a
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@31d
    .line 130
    const-string v6, "one_contact_interval"

    #@31f
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@322
    move-result-object v7

    #@323
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@326
    .line 132
    const-string v6, "revoke_timer"

    #@328
    const/16 v7, 0xe10

    #@32a
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@32d
    move-result-object v7

    #@32e
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@331
    .line 133
    const-string v6, "xcap_root_uri"

    #@333
    const-string v7, "125.152.96.245:9110"

    #@335
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@338
    .line 134
    const-string v6, "xcap_authentication_user_name"

    #@33a
    const-string v7, "0"

    #@33c
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@33f
    .line 135
    const-string v6, "xcap_authentication_secret"

    #@341
    const-string v7, "0"

    #@343
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@346
    .line 136
    const-string v6, "xcap_authentication_type"

    #@348
    const-string v7, "0"

    #@34a
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@34d
    .line 138
    const-string v6, "chat_auth"

    #@34f
    const-string v7, "1"

    #@351
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@354
    .line 139
    const-string v6, "sms_fall_back_auth"

    #@356
    const-string v7, "1"

    #@358
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@35b
    .line 140
    const-string v6, "aut_accept"

    #@35d
    const-string v7, "1"

    #@35f
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@362
    .line 141
    const-string v6, "max_size_1to1"

    #@364
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@367
    move-result-object v7

    #@368
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@36b
    .line 142
    const-string v6, "max_size_1tom"

    #@36d
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@370
    move-result-object v7

    #@371
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@374
    .line 143
    const-string v6, "timer_idle"

    #@376
    const/16 v7, 0x12c

    #@378
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@37b
    move-result-object v7

    #@37c
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@37f
    .line 144
    const-string v6, "pres_srv_cap"

    #@381
    const-string v7, "1"

    #@383
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@386
    .line 145
    const-string v6, "deferred_msg_func_uri"

    #@388
    const-string v7, "x"

    #@38a
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@38d
    .line 146
    const-string v6, "max_adhoc_group_size"

    #@38f
    const-string v7, "100"

    #@391
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@394
    .line 147
    const-string v6, "exploder_uri"

    #@396
    const-string v7, "x"

    #@398
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@39b
    .line 148
    const-string v6, "imSessionStart"

    #@39d
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3a0
    move-result-object v7

    #@3a1
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@3a4
    .line 149
    const-string v6, "im_content_server_uri"

    #@3a6
    const-string v7, "221.148.247.125:443"

    #@3a8
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@3ab
    .line 151
    const-string v6, "psSignalling"

    #@3ad
    const-string v7, "x"

    #@3af
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@3b2
    .line 152
    const-string v6, "psMedia"

    #@3b4
    const-string v7, "x"

    #@3b6
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@3b9
    .line 153
    const-string v6, "psRTMedia"

    #@3bb
    const-string v7, "x"

    #@3bd
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@3c0
    .line 154
    const-string v6, "wifiSignalling"

    #@3c2
    const-string v7, "x"

    #@3c4
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@3c7
    .line 155
    const-string v6, "wifiMedia"

    #@3c9
    const-string v7, "x"

    #@3cb
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@3ce
    .line 156
    const-string v6, "wifiRTMedia"

    #@3d0
    const-string v7, "x"

    #@3d2
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@3d5
    .line 158
    const-string v6, "status_code"

    #@3d7
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3da
    move-result-object v7

    #@3db
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@3de
    .line 159
    const-string v6, "status_message"

    #@3e0
    const-string v7, "ok"

    #@3e2
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@3e5
    .line 161
    const-string v6, "timer_a"

    #@3e7
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3ea
    move-result-object v7

    #@3eb
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@3ee
    .line 162
    const-string v6, "timer_b"

    #@3f0
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3f3
    move-result-object v7

    #@3f4
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@3f7
    .line 163
    const-string v6, "timer_c"

    #@3f9
    const v7, 0x2bf20

    #@3fc
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3ff
    move-result-object v7

    #@400
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@403
    .line 164
    const-string v6, "timer_d"

    #@405
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@408
    move-result-object v7

    #@409
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@40c
    .line 165
    const-string v6, "timer_e"

    #@40e
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@411
    move-result-object v7

    #@412
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@415
    .line 166
    const-string v6, "timer_f"

    #@417
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@41a
    move-result-object v7

    #@41b
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@41e
    .line 167
    const-string v6, "timer_g"

    #@420
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@423
    move-result-object v7

    #@424
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@427
    .line 168
    const-string v6, "timer_h"

    #@429
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@42c
    move-result-object v7

    #@42d
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@430
    .line 169
    const-string v6, "timer_i"

    #@432
    const/16 v7, 0x4268

    #@434
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@437
    move-result-object v7

    #@438
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@43b
    .line 170
    const-string v6, "timer_j"

    #@43d
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@440
    move-result-object v7

    #@441
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@444
    .line 171
    const-string v6, "timer_k"

    #@446
    const/16 v7, 0x4268

    #@448
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@44b
    move-result-object v7

    #@44c
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@44f
    .line 173
    const-string v6, "rtpport_port1"

    #@451
    const/16 v7, 0x1b62

    #@453
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@456
    move-result-object v7

    #@457
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@45a
    .line 174
    const-string v6, "rtpport_port2"

    #@45c
    const/16 v7, 0x1b64

    #@45e
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@461
    move-result-object v7

    #@462
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@465
    .line 175
    const-string v6, "rtpport_port3"

    #@467
    const/16 v7, 0x1b66

    #@469
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@46c
    move-result-object v7

    #@46d
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@470
    .line 176
    const-string v6, "rtpport_port4"

    #@472
    const/16 v7, 0x1b68

    #@474
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@477
    move-result-object v7

    #@478
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@47b
    .line 178
    const-string v6, "regretry_interval1"

    #@47d
    const/16 v7, 0xb4

    #@47f
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@482
    move-result-object v7

    #@483
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@486
    .line 179
    const-string v6, "regretry_interval2"

    #@488
    const/16 v7, 0x12c

    #@48a
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@48d
    move-result-object v7

    #@48e
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@491
    .line 180
    const-string v6, "regretry_interval3"

    #@493
    const/16 v7, 0x258

    #@495
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@498
    move-result-object v7

    #@499
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@49c
    .line 181
    const-string v6, "regretry_initialRetry"

    #@49e
    const/16 v7, 0xb4

    #@4a0
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4a3
    move-result-object v7

    #@4a4
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@4a7
    .line 182
    const-string v6, "regretry_secondRetry"

    #@4a9
    const/16 v7, 0x12c

    #@4ab
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4ae
    move-result-object v7

    #@4af
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@4b2
    .line 183
    const-string v6, "regretry_thirdRetry"

    #@4b4
    const/16 v7, 0x258

    #@4b6
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4b9
    move-result-object v7

    #@4ba
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@4bd
    .line 184
    const-string v6, "regretry_repeatRetry"

    #@4bf
    const/16 v7, 0x258

    #@4c1
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4c4
    move-result-object v7

    #@4c5
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@4c8
    .line 186
    const-string v6, "abcinfo_address"

    #@4ca
    const-string v7, "x"

    #@4cc
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@4cf
    .line 187
    const-string v6, "abcinfo_addressType"

    #@4d1
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4d4
    move-result-object v7

    #@4d5
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@4d8
    .line 189
    const-string v6, "sbcinfo_address"

    #@4da
    const-string v7, "125.152.96.245:5060"

    #@4dc
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@4df
    .line 190
    const-string v6, "sbcinfo_addressType"

    #@4e1
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4e4
    move-result-object v7

    #@4e5
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@4e8
    .line 191
    const-string v6, "sbc_tls_address"

    #@4ea
    const-string v7, "x"

    #@4ec
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@4ef
    .line 192
    const-string v6, "sbc_tls_addressType"

    #@4f1
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4f4
    move-result-object v7

    #@4f5
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@4f8
    .line 194
    const-string v6, "presence_publishtimer"

    #@4fa
    const/16 v7, 0x3e8

    #@4fc
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4ff
    move-result-object v7

    #@500
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@503
    .line 196
    const-string v6, "im_conv_hist_func_uri"

    #@505
    const-string v7, "x"

    #@507
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@50a
    .line 197
    const-string v6, "im_delete_uri"

    #@50c
    const-string v7, "x"

    #@50e
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@511
    .line 199
    const-string v6, "software_version"

    #@513
    const-string v7, "x"

    #@515
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@518
    .line 200
    const-string v6, "software_url"

    #@51a
    const-string v7, "x"

    #@51c
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@51f
    .line 202
    const-string v6, "other_timestamp"

    #@521
    const-string v7, "0"

    #@523
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@526
    .line 203
    const-string v6, "other_termsversion"

    #@528
    const-string v7, "0"

    #@52a
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@52d
    .line 204
    const-string v6, "other_deviceid"

    #@52f
    const-string v7, "1"

    #@531
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@534
    .line 206
    const/4 v2, 0x0

    #@535
    .line 209
    .local v2, objUri:Landroid/net/Uri;
    :try_start_535
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@538
    move-result-object v6

    #@539
    sget-object v7, Lcom/lge/ims/provisioning/SIMCols$ProvisioningTableMetaData;->CONTENT_URI:Landroid/net/Uri;

    #@53b
    invoke-virtual {v6, v7, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_53e
    .catch Landroid/database/SQLException; {:try_start_535 .. :try_end_53e} :catch_561

    #@53e
    move-result-object v2

    #@53f
    .line 217
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    #@542
    .line 218
    if-eqz v2, :cond_13

    #@544
    .line 222
    new-instance v4, Ljava/lang/StringBuilder;

    #@546
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@549
    const-string v6, "- "

    #@54b
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54e
    move-result-object v4

    #@54f
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@552
    move-result-object v6

    #@553
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@556
    move-result-object v4

    #@557
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55a
    move-result-object v4

    #@55b
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@55e
    move v4, v5

    #@55f
    .line 223
    goto/16 :goto_13

    #@561
    .line 210
    :catch_561
    move-exception v0

    #@562
    .line 211
    .local v0, e:Landroid/database/SQLException;
    invoke-virtual {v0}, Landroid/database/SQLException;->printStackTrace()V

    #@565
    .line 212
    const-string v5, "Can\'t Insert Default Provisioning - settings"

    #@567
    invoke-static {v5}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@56a
    .line 213
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    #@56d
    goto/16 :goto_13
.end method

.method public static InsertDefaultSettingByIMSI(Landroid/content/Context;Ljava/lang/String;)Z
    .registers 9
    .parameter "objContext"
    .parameter "strIMSI"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 227
    if-eqz p0, :cond_a

    #@4
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@7
    move-result v5

    #@8
    if-eqz v5, :cond_b

    #@a
    .line 259
    :cond_a
    :goto_a
    return v3

    #@b
    .line 231
    :cond_b
    new-instance v1, Landroid/content/ContentValues;

    #@d
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    #@10
    .line 233
    .local v1, objContentValues:Landroid/content/ContentValues;
    const-string v5, "imsi"

    #@12
    invoke-virtual {v1, v5, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@15
    .line 234
    const-string v5, "rcs_e_service"

    #@17
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a
    move-result-object v6

    #@1b
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@1e
    .line 235
    const-string v5, "rcs_e_roaming"

    #@20
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@23
    move-result-object v6

    #@24
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@27
    .line 236
    const-string v5, "latest_polling"

    #@29
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2c
    move-result-object v6

    #@2d
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@30
    .line 237
    const-string v5, "is_accept"

    #@32
    const/4 v6, 0x2

    #@33
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@36
    move-result-object v6

    #@37
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@3a
    .line 238
    const-string v5, "again_notification"

    #@3c
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3f
    move-result-object v6

    #@40
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@43
    .line 239
    const-string v5, "same_xml_version"

    #@45
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@48
    move-result-object v6

    #@49
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@4c
    .line 241
    const/4 v2, 0x0

    #@4d
    .line 244
    .local v2, objUri:Landroid/net/Uri;
    :try_start_4d
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@50
    move-result-object v5

    #@51
    sget-object v6, Lcom/lge/ims/provisioning/SIMCols$SettingTableMetaData;->CONTENT_URI:Landroid/net/Uri;

    #@53
    invoke-virtual {v5, v6, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_56
    .catch Landroid/database/SQLException; {:try_start_4d .. :try_end_56} :catch_78

    #@56
    move-result-object v2

    #@57
    .line 252
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    #@5a
    .line 254
    if-eqz v2, :cond_a

    #@5c
    .line 258
    new-instance v3, Ljava/lang/StringBuilder;

    #@5e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@61
    const-string v5, "- "

    #@63
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v3

    #@67
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@6a
    move-result-object v5

    #@6b
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v3

    #@6f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@72
    move-result-object v3

    #@73
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@76
    move v3, v4

    #@77
    .line 259
    goto :goto_a

    #@78
    .line 245
    :catch_78
    move-exception v0

    #@79
    .line 246
    .local v0, e:Landroid/database/SQLException;
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    #@7c
    .line 247
    const-string v4, "Can\'t Insert Default DB - settings"

    #@7e
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@81
    .line 248
    invoke-virtual {v0}, Landroid/database/SQLException;->printStackTrace()V

    #@84
    goto :goto_a
.end method

.method public static InsertDefaultWorkingByIMSI(Landroid/content/Context;Ljava/lang/String;)Z
    .registers 8
    .parameter "objContext"
    .parameter "strIMSI"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 263
    if-eqz p0, :cond_9

    #@3
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@6
    move-result v4

    #@7
    if-eqz v4, :cond_a

    #@9
    .line 293
    :cond_9
    :goto_9
    return v3

    #@a
    .line 267
    :cond_a
    new-instance v1, Landroid/content/ContentValues;

    #@c
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    #@f
    .line 269
    .local v1, objContentValues:Landroid/content/ContentValues;
    const-string v4, "working_key"

    #@11
    invoke-virtual {v1, v4, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@14
    .line 270
    const-string v4, "rcs_e_working"

    #@16
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@19
    move-result-object v5

    #@1a
    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@1d
    .line 271
    const-string v4, "rcs_e_is_working"

    #@1f
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@22
    move-result-object v5

    #@23
    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@26
    .line 272
    const-string v4, "rcs_status"

    #@28
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2b
    move-result-object v5

    #@2c
    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@2f
    .line 274
    const/4 v2, 0x0

    #@30
    .line 278
    .local v2, objUri:Landroid/net/Uri;
    :try_start_30
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@33
    move-result-object v4

    #@34
    sget-object v5, Lcom/lge/ims/provisioning/SIMCols$SettingTableMetaData;->CONTENT_URI:Landroid/net/Uri;

    #@36
    invoke-virtual {v4, v5, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_39
    .catch Landroid/database/SQLException; {:try_start_30 .. :try_end_39} :catch_5b

    #@39
    move-result-object v2

    #@3a
    .line 286
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    #@3d
    .line 288
    if-eqz v2, :cond_9

    #@3f
    .line 292
    new-instance v3, Ljava/lang/StringBuilder;

    #@41
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@44
    const-string v4, "- "

    #@46
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v3

    #@4a
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@4d
    move-result-object v4

    #@4e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v3

    #@52
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v3

    #@56
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@59
    .line 293
    const/4 v3, 0x1

    #@5a
    goto :goto_9

    #@5b
    .line 279
    :catch_5b
    move-exception v0

    #@5c
    .line 280
    .local v0, e:Landroid/database/SQLException;
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    #@5f
    .line 281
    const-string v4, "Can\'t Insert Default DB - settings"

    #@61
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@64
    .line 282
    invoke-virtual {v0}, Landroid/database/SQLException;->printStackTrace()V

    #@67
    goto :goto_9
.end method

.method public static setTelUri(Landroid/content/Context;Ljava/lang/String;)Z
    .registers 10
    .parameter "objContext"
    .parameter "strTelUri"

    #@0
    .prologue
    .line 297
    const/4 v2, 0x0

    #@1
    .line 298
    .local v2, rowsUpdated:I
    new-instance v1, Landroid/content/ContentValues;

    #@3
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    #@6
    .line 299
    .local v1, objContentValues:Landroid/content/ContentValues;
    invoke-static {}, Lcom/lge/ims/SysDispatcher;->getSubscriberID()Ljava/lang/String;

    #@9
    move-result-object v3

    #@a
    .line 301
    .local v3, strIMSI:Ljava/lang/String;
    const-string v4, "tel_uri"

    #@c
    invoke-virtual {v1, v4, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@f
    .line 304
    :try_start_f
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@12
    move-result-object v4

    #@13
    sget-object v5, Lcom/lge/ims/provisioning/SIMCols$ProvisioningTableMetaData;->CONTENT_URI:Landroid/net/Uri;

    #@15
    new-instance v6, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v7, "imsi = \'"

    #@1c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v6

    #@20
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v6

    #@24
    const-string v7, "\'"

    #@26
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v6

    #@2a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v6

    #@2e
    const/4 v7, 0x0

    #@2f
    invoke-virtual {v4, v5, v1, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_32
    .catch Landroid/database/SQLException; {:try_start_f .. :try_end_32} :catch_4e

    #@32
    move-result v2

    #@33
    .line 312
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    #@36
    .line 314
    new-instance v4, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v5, "effected row counts: "

    #@3d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v4

    #@41
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@44
    move-result-object v4

    #@45
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v4

    #@49
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@4c
    .line 315
    const/4 v4, 0x1

    #@4d
    :goto_4d
    return v4

    #@4e
    .line 305
    :catch_4e
    move-exception v0

    #@4f
    .line 306
    .local v0, e:Landroid/database/SQLException;
    invoke-virtual {v0}, Landroid/database/SQLException;->printStackTrace()V

    #@52
    .line 307
    const-string v4, "Can\'t Insert Tel URI - settings"

    #@54
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@57
    .line 308
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    #@5a
    .line 309
    const/4 v4, 0x0

    #@5b
    goto :goto_4d
.end method
