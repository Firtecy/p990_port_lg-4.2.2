.class public final Lcom/lge/ims/R$array;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "array"
.end annotation


# static fields
.field public static final app_setting:I = 0x7f050036

.field public static final audio_amr_octet_align:I = 0x7f05000b

.field public static final audio_amr_octet_align_name:I = 0x7f05000a

.field public static final audio_codec_item:I = 0x7f050003

.field public static final carrier_audio_pref_codec:I = 0x7f050004

.field public static final carrier_audio_pref_codec_payload:I = 0x7f050005

.field public static final codec_channel_name:I = 0x7f050016

.field public static final codec_channel_val:I = 0x7f050015

.field public static final codec_max_red_name:I = 0x7f050020

.field public static final codec_max_red_val:I = 0x7f05001f

.field public static final codec_mode_change_capability_name:I = 0x7f050018

.field public static final codec_mode_change_capability_val:I = 0x7f050017

.field public static final codec_mode_change_neighbor_name:I = 0x7f05001c

.field public static final codec_mode_change_neighbor_val:I = 0x7f05001b

.field public static final codec_mode_change_period_name:I = 0x7f05001a

.field public static final codec_mode_change_period_val:I = 0x7f050019

.field public static final codec_robust_sorting_name:I = 0x7f05001e

.field public static final codec_robust_sorting_val:I = 0x7f05001d

.field public static final device_id_entry:I = 0x7f05002e

.field public static final device_id_entry_value:I = 0x7f05002d

.field public static final five_item_value:I = 0x7f05002c

.field public static final four_item_value:I = 0x7f05002b

.field public static final ft_mp:I = 0x7f050035

.field public static final ip_version_entry:I = 0x7f050031

.field public static final ip_version_value:I = 0x7f050030

.field public static final max_ptime_list:I = 0x7f050013

.field public static final max_ptime_list_name:I = 0x7f050014

.field public static final media_audio_bandwidth_mode_name:I = 0x7f05000e

.field public static final media_audio_bandwidth_mode_value:I = 0x7f05000c

.field public static final media_sdp_answer_fullcapa:I = 0x7f050007

.field public static final media_sdp_keep_m_line:I = 0x7f050009

.field public static final media_sdp_reoffer_fullcapa:I = 0x7f050008

.field public static final media_video_bandwidth_mode_name:I = 0x7f05000f

.field public static final media_video_bandwidth_mode_value:I = 0x7f05000d

.field public static final media_video_framerate_mode_name:I = 0x7f050010

.field public static final mode_change_item:I = 0x7f050027

.field public static final network_types:I = 0x7f050006

.field public static final one_item_value:I = 0x7f050028

.field public static final preferred_id_entry:I = 0x7f050032

.field public static final ptime_list:I = 0x7f050011

.field public static final ptime_list_name:I = 0x7f050012

.field public static final rtcp_enable_name:I = 0x7f050021

.field public static final server_selection_entry:I = 0x7f05002f

.field public static final session_refresh_method_entry:I = 0x7f050034

.field public static final session_refresher_entry:I = 0x7f050033

.field public static final target_number_format_entry:I = 0x7f050002

.field public static final target_number_format_value:I = 0x7f050001

.field public static final target_scheme_value:I = 0x7f050000

.field public static final three_item_value:I = 0x7f05002a

.field public static final two_item_value:I = 0x7f050029

.field public static final vt_video_codec_h263_resolution:I = 0x7f050024

.field public static final vt_video_codec_item:I = 0x7f050022

.field public static final vt_video_codec_resolution:I = 0x7f050023

.field public static final vt_video_frame_rate_item:I = 0x7f050025

.field public static final vt_video_packetization_mode:I = 0x7f050026


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 11
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
