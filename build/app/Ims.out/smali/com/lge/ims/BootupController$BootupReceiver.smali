.class public Lcom/lge/ims/BootupController$BootupReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BootupController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/BootupController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "BootupReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/BootupController;


# direct methods
.method public constructor <init>(Lcom/lge/ims/BootupController;Landroid/content/Context;)V
    .registers 5
    .parameter
    .parameter "context"

    #@0
    .prologue
    .line 212
    iput-object p1, p0, Lcom/lge/ims/BootupController$BootupReceiver;->this$0:Lcom/lge/ims/BootupController;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    .line 213
    new-instance v0, Landroid/content/IntentFilter;

    #@7
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@a
    .line 215
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    #@c
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@f
    .line 216
    const-string v1, "android.net.wifi.STATE_CHANGE"

    #@11
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@14
    .line 217
    const-string v1, "com.lge.ims.ac.action.AC_STOPED"

    #@16
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@19
    .line 218
    const-string v1, "android.intent.action.SIM_STATE_CHANGED"

    #@1b
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1e
    .line 219
    const-string v1, "com.lge.ims.action.SETTINGS_CHANGED"

    #@20
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@23
    .line 220
    invoke-virtual {p2, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@26
    .line 221
    return-void
.end method


# virtual methods
.method public declared-synchronized onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 13
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 225
    monitor-enter p0

    #@1
    :try_start_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    .line 226
    .local v0, action:Ljava/lang/String;
    const-string v6, "BootupController"

    #@7
    new-instance v7, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v8, "BootupReceiver received an intent: "

    #@e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v7

    #@12
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v7

    #@16
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v7

    #@1a
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@1d
    .line 228
    const-string v6, "android.net.conn.CONNECTIVITY_CHANGE"

    #@1f
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22
    move-result v6

    #@23
    if-eqz v6, :cond_91

    #@25
    .line 229
    const/4 v2, 0x1

    #@26
    .line 230
    .local v2, imsState:I
    const-string v6, "networkInfo"

    #@28
    invoke-virtual {p2, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@2b
    move-result-object v3

    #@2c
    check-cast v3, Landroid/net/NetworkInfo;

    #@2e
    .line 232
    .local v3, netInfo:Landroid/net/NetworkInfo;
    if-nez v3, :cond_39

    #@30
    .line 233
    const-string v6, "BootupController"

    #@32
    const-string v7, "CONNECTIVITY_ACTION : network info is null"

    #@34
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_37
    .catchall {:try_start_1 .. :try_end_37} :catchall_53

    #@37
    .line 300
    .end local v2           #imsState:I
    .end local v3           #netInfo:Landroid/net/NetworkInfo;
    :cond_37
    :goto_37
    monitor-exit p0

    #@38
    return-void

    #@39
    .line 238
    .restart local v2       #imsState:I
    .restart local v3       #netInfo:Landroid/net/NetworkInfo;
    :cond_39
    :try_start_39
    invoke-static {}, Lcom/lge/ims/DataConnectionManager;->getInstance()Lcom/lge/ims/DataConnectionManager;

    #@3c
    move-result-object v6

    #@3d
    invoke-virtual {v6}, Lcom/lge/ims/DataConnectionManager;->isMultiplePdnSupported()Z

    #@40
    move-result v6

    #@41
    if-eqz v6, :cond_56

    #@43
    .line 239
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getType()I

    #@46
    move-result v6

    #@47
    const/16 v7, 0xb

    #@49
    if-eq v6, v7, :cond_64

    #@4b
    .line 240
    const-string v6, "BootupController"

    #@4d
    const-string v7, "CONNECTIVITY_ACTION (MPDN) : not MOBILE_IMS"

    #@4f
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_52
    .catchall {:try_start_39 .. :try_end_52} :catchall_53

    #@52
    goto :goto_37

    #@53
    .line 225
    .end local v0           #action:Ljava/lang/String;
    .end local v2           #imsState:I
    .end local v3           #netInfo:Landroid/net/NetworkInfo;
    :catchall_53
    move-exception v6

    #@54
    monitor-exit p0

    #@55
    throw v6

    #@56
    .line 244
    .restart local v0       #action:Ljava/lang/String;
    .restart local v2       #imsState:I
    .restart local v3       #netInfo:Landroid/net/NetworkInfo;
    :cond_56
    :try_start_56
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getType()I

    #@59
    move-result v6

    #@5a
    if-eqz v6, :cond_64

    #@5c
    .line 245
    const-string v6, "BootupController"

    #@5e
    const-string v7, "CONNECTIVITY_ACTION (SPDN) : not MOBILE"

    #@60
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@63
    goto :goto_37

    #@64
    .line 250
    :cond_64
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z

    #@67
    move-result v6

    #@68
    const/4 v7, 0x1

    #@69
    if-ne v6, v7, :cond_88

    #@6b
    .line 251
    const-string v6, "BootupController"

    #@6d
    const-string v7, "IMS DATA is CONNECTED"

    #@6f
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@72
    .line 252
    const/4 v2, 0x0

    #@73
    .line 258
    :goto_73
    iget-object v6, p0, Lcom/lge/ims/BootupController$BootupReceiver;->this$0:Lcom/lge/ims/BootupController;

    #@75
    invoke-static {v6}, Lcom/lge/ims/BootupController;->access$800(Lcom/lge/ims/BootupController;)I

    #@78
    move-result v6

    #@79
    if-eq v6, v2, :cond_37

    #@7b
    .line 259
    iget-object v6, p0, Lcom/lge/ims/BootupController$BootupReceiver;->this$0:Lcom/lge/ims/BootupController;

    #@7d
    invoke-static {v6, v2}, Lcom/lge/ims/BootupController;->access$802(Lcom/lge/ims/BootupController;I)I

    #@80
    .line 260
    iget-object v6, p0, Lcom/lge/ims/BootupController$BootupReceiver;->this$0:Lcom/lge/ims/BootupController;

    #@82
    const/16 v7, 0x66

    #@84
    invoke-static {v6, v7}, Lcom/lge/ims/BootupController;->access$900(Lcom/lge/ims/BootupController;I)V

    #@87
    goto :goto_37

    #@88
    .line 254
    :cond_88
    const-string v6, "BootupController"

    #@8a
    const-string v7, "IMS DATA is DISCONNECTED"

    #@8c
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@8f
    .line 255
    const/4 v2, 0x1

    #@90
    goto :goto_73

    #@91
    .line 262
    .end local v2           #imsState:I
    .end local v3           #netInfo:Landroid/net/NetworkInfo;
    :cond_91
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@94
    move-result-object v6

    #@95
    const-string v7, "android.net.wifi.STATE_CHANGE"

    #@97
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9a
    move-result v6

    #@9b
    if-eqz v6, :cond_ed

    #@9d
    .line 263
    sget-object v1, Landroid/net/NetworkInfo$State;->UNKNOWN:Landroid/net/NetworkInfo$State;

    #@9f
    .line 264
    .local v1, eState:Landroid/net/NetworkInfo$State;
    const-string v6, "networkInfo"

    #@a1
    invoke-virtual {p2, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@a4
    move-result-object v4

    #@a5
    check-cast v4, Landroid/net/NetworkInfo;

    #@a7
    .line 266
    .local v4, networkInfo:Landroid/net/NetworkInfo;
    if-nez v4, :cond_b1

    #@a9
    .line 267
    const-string v6, "BootupController"

    #@ab
    const-string v7, "WifiManager.NETWORK_STATE_CHANGED_ACTION : networkinfo is NULL"

    #@ad
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@b0
    goto :goto_37

    #@b1
    .line 269
    :cond_b1
    const-string v6, "BootupController"

    #@b3
    new-instance v7, Ljava/lang/StringBuilder;

    #@b5
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@b8
    const-string v8, "WifiManager.NETWORK_STATE_CHANGED_ACTION : networkinfo ("

    #@ba
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v7

    #@be
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->toString()Ljava/lang/String;

    #@c1
    move-result-object v8

    #@c2
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v7

    #@c6
    const-string v8, ")"

    #@c8
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cb
    move-result-object v7

    #@cc
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cf
    move-result-object v7

    #@d0
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@d3
    .line 272
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    #@d6
    move-result-object v1

    #@d7
    .line 274
    iget-object v6, p0, Lcom/lge/ims/BootupController$BootupReceiver;->this$0:Lcom/lge/ims/BootupController;

    #@d9
    invoke-static {v6}, Lcom/lge/ims/BootupController;->access$1000(Lcom/lge/ims/BootupController;)Landroid/net/NetworkInfo$State;

    #@dc
    move-result-object v6

    #@dd
    if-eq v6, v1, :cond_37

    #@df
    .line 275
    iget-object v6, p0, Lcom/lge/ims/BootupController$BootupReceiver;->this$0:Lcom/lge/ims/BootupController;

    #@e1
    invoke-static {v6, v1}, Lcom/lge/ims/BootupController;->access$1002(Lcom/lge/ims/BootupController;Landroid/net/NetworkInfo$State;)Landroid/net/NetworkInfo$State;

    #@e4
    .line 276
    iget-object v6, p0, Lcom/lge/ims/BootupController$BootupReceiver;->this$0:Lcom/lge/ims/BootupController;

    #@e6
    const/16 v7, 0x66

    #@e8
    invoke-static {v6, v7}, Lcom/lge/ims/BootupController;->access$900(Lcom/lge/ims/BootupController;I)V

    #@eb
    goto/16 :goto_37

    #@ed
    .line 279
    .end local v1           #eState:Landroid/net/NetworkInfo$State;
    .end local v4           #networkInfo:Landroid/net/NetworkInfo;
    :cond_ed
    const-string v6, "com.lge.ims.ac.action.AC_STOPED"

    #@ef
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f2
    move-result v6

    #@f3
    if-eqz v6, :cond_128

    #@f5
    .line 280
    iget-object v6, p0, Lcom/lge/ims/BootupController$BootupReceiver;->this$0:Lcom/lge/ims/BootupController;

    #@f7
    const-string v7, "ac_result"

    #@f9
    const/4 v8, 0x0

    #@fa
    invoke-virtual {p2, v7, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@fd
    move-result v7

    #@fe
    invoke-static {v6, v7}, Lcom/lge/ims/BootupController;->access$1102(Lcom/lge/ims/BootupController;Z)Z

    #@101
    .line 281
    const-string v6, "BootupController"

    #@103
    new-instance v7, Ljava/lang/StringBuilder;

    #@105
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@108
    const-string v8, "AC is done!!!! Result: "

    #@10a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10d
    move-result-object v7

    #@10e
    iget-object v8, p0, Lcom/lge/ims/BootupController$BootupReceiver;->this$0:Lcom/lge/ims/BootupController;

    #@110
    invoke-static {v8}, Lcom/lge/ims/BootupController;->access$1100(Lcom/lge/ims/BootupController;)Z

    #@113
    move-result v8

    #@114
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@117
    move-result-object v7

    #@118
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11b
    move-result-object v7

    #@11c
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@11f
    .line 283
    iget-object v6, p0, Lcom/lge/ims/BootupController$BootupReceiver;->this$0:Lcom/lge/ims/BootupController;

    #@121
    const/16 v7, 0x67

    #@123
    invoke-static {v6, v7}, Lcom/lge/ims/BootupController;->access$900(Lcom/lge/ims/BootupController;I)V

    #@126
    goto/16 :goto_37

    #@128
    .line 284
    :cond_128
    const-string v6, "android.intent.action.SIM_STATE_CHANGED"

    #@12a
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12d
    move-result v6

    #@12e
    if-eqz v6, :cond_161

    #@130
    .line 285
    const-string v6, "ss"

    #@132
    invoke-virtual {p2, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@135
    move-result-object v5

    #@136
    .line 286
    .local v5, stateExtra:Ljava/lang/String;
    const-string v6, "BootupController"

    #@138
    new-instance v7, Ljava/lang/StringBuilder;

    #@13a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@13d
    const-string v8, "SIM stateExtra: "

    #@13f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@142
    move-result-object v7

    #@143
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@146
    move-result-object v7

    #@147
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14a
    move-result-object v7

    #@14b
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@14e
    .line 288
    const-string v6, "LOADED"

    #@150
    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@153
    move-result v6

    #@154
    if-eqz v6, :cond_37

    #@156
    .line 289
    iget-object v6, p0, Lcom/lge/ims/BootupController$BootupReceiver;->this$0:Lcom/lge/ims/BootupController;

    #@158
    const/16 v7, 0x65

    #@15a
    const-wide/16 v8, 0xbb8

    #@15c
    invoke-static {v6, v7, v8, v9}, Lcom/lge/ims/BootupController;->access$1200(Lcom/lge/ims/BootupController;IJ)V

    #@15f
    goto/16 :goto_37

    #@161
    .line 291
    .end local v5           #stateExtra:Ljava/lang/String;
    :cond_161
    const-string v6, "com.lge.ims.action.SETTINGS_CHANGED"

    #@163
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@166
    move-result v6

    #@167
    if-eqz v6, :cond_172

    #@169
    .line 292
    iget-object v6, p0, Lcom/lge/ims/BootupController$BootupReceiver;->this$0:Lcom/lge/ims/BootupController;

    #@16b
    const/16 v7, 0x69

    #@16d
    invoke-static {v6, v7}, Lcom/lge/ims/BootupController;->access$900(Lcom/lge/ims/BootupController;I)V

    #@170
    goto/16 :goto_37

    #@172
    .line 293
    :cond_172
    const-string v6, "com.lge.ims.action.STOP_RCSE"

    #@174
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@177
    move-result v6

    #@178
    if-eqz v6, :cond_18a

    #@17a
    .line 294
    const-string v6, "BootupController"

    #@17c
    const-string v7, "AC Reset!!"

    #@17e
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@181
    .line 296
    iget-object v6, p0, Lcom/lge/ims/BootupController$BootupReceiver;->this$0:Lcom/lge/ims/BootupController;

    #@183
    const/16 v7, 0x6b

    #@185
    invoke-static {v6, v7}, Lcom/lge/ims/BootupController;->access$900(Lcom/lge/ims/BootupController;I)V

    #@188
    goto/16 :goto_37

    #@18a
    .line 298
    :cond_18a
    const-string v6, "BootupController"

    #@18c
    const-string v7, "invalid intent"

    #@18e
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_191
    .catchall {:try_start_56 .. :try_end_191} :catchall_53

    #@191
    goto/16 :goto_37
.end method
