.class Lcom/lge/ims/PhoneSettingsTracker$1;
.super Landroid/database/ContentObserver;
.source "PhoneSettingsTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lge/ims/PhoneSettingsTracker;->registerCallSettingObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/PhoneSettingsTracker;


# direct methods
.method constructor <init>(Lcom/lge/ims/PhoneSettingsTracker;Landroid/os/Handler;)V
    .registers 3
    .parameter
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iput-object p1, p0, Lcom/lge/ims/PhoneSettingsTracker$1;->this$0:Lcom/lge/ims/PhoneSettingsTracker;

    #@2
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    #@5
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .registers 7
    .parameter "selfChange"

    #@0
    .prologue
    .line 92
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    #@3
    .line 94
    iget-object v2, p0, Lcom/lge/ims/PhoneSettingsTracker$1;->this$0:Lcom/lge/ims/PhoneSettingsTracker;

    #@5
    invoke-static {v2}, Lcom/lge/ims/PhoneSettingsTracker;->access$000(Lcom/lge/ims/PhoneSettingsTracker;)Landroid/content/Context;

    #@8
    move-result-object v2

    #@9
    const-string v3, "KT_hd_voice_setting"

    #@b
    invoke-static {v2, v3}, Lcom/lge/ims/PhoneSettingsTracker;->queryCallSettingValueByKey(Landroid/content/Context;Ljava/lang/String;)I

    #@e
    move-result v0

    #@f
    .line 95
    .local v0, nCurHDSetValue:I
    iget-object v2, p0, Lcom/lge/ims/PhoneSettingsTracker$1;->this$0:Lcom/lge/ims/PhoneSettingsTracker;

    #@11
    invoke-static {v2}, Lcom/lge/ims/PhoneSettingsTracker;->access$000(Lcom/lge/ims/PhoneSettingsTracker;)Landroid/content/Context;

    #@14
    move-result-object v2

    #@15
    const-string v3, "KT_4G_network_setting"

    #@17
    invoke-static {v2, v3}, Lcom/lge/ims/PhoneSettingsTracker;->queryCallSettingValueByKey(Landroid/content/Context;Ljava/lang/String;)I

    #@1a
    move-result v1

    #@1b
    .line 97
    .local v1, nCurNetworkSetValue:I
    iget-object v2, p0, Lcom/lge/ims/PhoneSettingsTracker$1;->this$0:Lcom/lge/ims/PhoneSettingsTracker;

    #@1d
    iget v2, v2, Lcom/lge/ims/PhoneSettingsTracker;->nHDsetValue:I

    #@1f
    if-ne v2, v0, :cond_27

    #@21
    iget-object v2, p0, Lcom/lge/ims/PhoneSettingsTracker$1;->this$0:Lcom/lge/ims/PhoneSettingsTracker;

    #@23
    iget v2, v2, Lcom/lge/ims/PhoneSettingsTracker;->nNetworkSetValue:I

    #@25
    if-eq v2, v1, :cond_7c

    #@27
    .line 98
    :cond_27
    const-string v2, "PhoneSettingsTracker"

    #@29
    new-instance v3, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v4, "HD Setting "

    #@30
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    iget-object v4, p0, Lcom/lge/ims/PhoneSettingsTracker$1;->this$0:Lcom/lge/ims/PhoneSettingsTracker;

    #@36
    iget v4, v4, Lcom/lge/ims/PhoneSettingsTracker;->nHDsetValue:I

    #@38
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v3

    #@3c
    const-string v4, "->"

    #@3e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v3

    #@42
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@45
    move-result-object v3

    #@46
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v3

    #@4a
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@4d
    .line 99
    const-string v2, "PhoneSettingsTracker"

    #@4f
    new-instance v3, Ljava/lang/StringBuilder;

    #@51
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@54
    const-string v4, "4GNetwork Setting "

    #@56
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v3

    #@5a
    iget-object v4, p0, Lcom/lge/ims/PhoneSettingsTracker$1;->this$0:Lcom/lge/ims/PhoneSettingsTracker;

    #@5c
    iget v4, v4, Lcom/lge/ims/PhoneSettingsTracker;->nNetworkSetValue:I

    #@5e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@61
    move-result-object v3

    #@62
    const-string v4, "->"

    #@64
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v3

    #@68
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v3

    #@6c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6f
    move-result-object v3

    #@70
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@73
    .line 100
    iget-object v2, p0, Lcom/lge/ims/PhoneSettingsTracker$1;->this$0:Lcom/lge/ims/PhoneSettingsTracker;

    #@75
    invoke-static {v2}, Lcom/lge/ims/PhoneSettingsTracker;->access$100(Lcom/lge/ims/PhoneSettingsTracker;)Landroid/os/RegistrantList;

    #@78
    move-result-object v2

    #@79
    invoke-virtual {v2}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@7c
    .line 102
    :cond_7c
    return-void
.end method
