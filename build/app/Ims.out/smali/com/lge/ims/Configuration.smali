.class public final Lcom/lge/ims/Configuration;
.super Ljava/lang/Object;
.source "Configuration.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/Configuration$DataConnection;
    }
.end annotation


# static fields
.field public static final BUILD_RCSE:Z = false

.field public static final BUILD_TYPE:Ljava/lang/String; = null

.field public static final IPV4:I = 0x4

.field public static final IPV4V6:I = 0x2e

.field public static final IPV6:I = 0x6

.field public static final IPV6V4:I = 0x40

.field public static final MODEL:Ljava/lang/String; = null

.field public static final OPERATOR:Ljava/lang/String; = null

.field public static final RAT_EHRPD:I = 0x10000000

.field public static final RAT_GSM:I = 0x80000

.field public static final RAT_LTE:I = 0x20000000

.field public static final RAT_WCDMA:I = 0x200000

.field private static final TAG:Ljava/lang/String; = "Configuration"

.field private static mConfiguration:Lcom/lge/ims/Configuration;


# instance fields
.field private mACOn:Z

.field private mDataConnections:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/lge/ims/Configuration$DataConnection;",
            ">;"
        }
    .end annotation
.end field

.field private mDebugOn:Z

.field private mDefaultIPVersion:I

.field private mEmergencySupported:Z

.field private mImsOn:Z

.field private mIsimOn:Z

.field private mRCSeOn:Z

.field private mTestMode:Z

.field private mUsimOn:Z


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 29
    const-string v0, "ro.build.type"

    #@2
    const-string v1, "userdebug"

    #@4
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    sput-object v0, Lcom/lge/ims/Configuration;->BUILD_TYPE:Ljava/lang/String;

    #@a
    .line 30
    const-string v0, "ro.product.model"

    #@c
    const-string v1, "LG-Unknown"

    #@e
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@11
    move-result-object v0

    #@12
    sput-object v0, Lcom/lge/ims/Configuration;->MODEL:Ljava/lang/String;

    #@14
    .line 31
    const-string v0, "ro.build.target_operator"

    #@16
    const-string v1, ""

    #@18
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    sput-object v0, Lcom/lge/ims/Configuration;->OPERATOR:Ljava/lang/String;

    #@1e
    .line 36
    new-instance v0, Lcom/lge/ims/Configuration;

    #@20
    invoke-direct {v0}, Lcom/lge/ims/Configuration;-><init>()V

    #@23
    sput-object v0, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@25
    return-void
.end method

.method private constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v0, 0x0

    #@2
    .line 75
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 38
    iput-boolean v0, p0, Lcom/lge/ims/Configuration;->mACOn:Z

    #@7
    .line 39
    iput-boolean v0, p0, Lcom/lge/ims/Configuration;->mDebugOn:Z

    #@9
    .line 40
    iput-boolean v1, p0, Lcom/lge/ims/Configuration;->mImsOn:Z

    #@b
    .line 41
    iput-boolean v1, p0, Lcom/lge/ims/Configuration;->mIsimOn:Z

    #@d
    .line 42
    iput-boolean v1, p0, Lcom/lge/ims/Configuration;->mUsimOn:Z

    #@f
    .line 43
    iput-boolean v0, p0, Lcom/lge/ims/Configuration;->mRCSeOn:Z

    #@11
    .line 44
    iput-boolean v0, p0, Lcom/lge/ims/Configuration;->mTestMode:Z

    #@13
    .line 45
    iput-boolean v0, p0, Lcom/lge/ims/Configuration;->mEmergencySupported:Z

    #@15
    .line 46
    const/16 v0, 0x2e

    #@17
    iput v0, p0, Lcom/lge/ims/Configuration;->mDefaultIPVersion:I

    #@19
    .line 47
    new-instance v0, Ljava/util/HashMap;

    #@1b
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@1e
    iput-object v0, p0, Lcom/lge/ims/Configuration;->mDataConnections:Ljava/util/HashMap;

    #@20
    .line 76
    const-string v0, "Configuration"

    #@22
    const-string v1, "Configuration is created"

    #@24
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@27
    .line 79
    return-void
.end method

.method public static getAccessPolicy(Ljava/lang/String;)I
    .registers 4
    .parameter "apn"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 115
    if-eqz p0, :cond_7

    #@3
    sget-object v2, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@5
    if-nez v2, :cond_8

    #@7
    .line 125
    :cond_7
    :goto_7
    return v1

    #@8
    .line 119
    :cond_8
    sget-object v2, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@a
    iget-object v2, v2, Lcom/lge/ims/Configuration;->mDataConnections:Ljava/util/HashMap;

    #@c
    invoke-virtual {v2, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Lcom/lge/ims/Configuration$DataConnection;

    #@12
    .line 121
    .local v0, dc:Lcom/lge/ims/Configuration$DataConnection;
    if-eqz v0, :cond_7

    #@14
    .line 125
    invoke-virtual {v0}, Lcom/lge/ims/Configuration$DataConnection;->getAccessPolicy()I

    #@17
    move-result v1

    #@18
    goto :goto_7
.end method

.method public static getIPVersion(Ljava/lang/String;)I
    .registers 4
    .parameter "apn"

    #@0
    .prologue
    const/16 v1, 0x40

    #@2
    .line 129
    if-eqz p0, :cond_8

    #@4
    sget-object v2, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@6
    if-nez v2, :cond_9

    #@8
    .line 139
    :cond_8
    :goto_8
    return v1

    #@9
    .line 133
    :cond_9
    sget-object v2, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@b
    iget-object v2, v2, Lcom/lge/ims/Configuration;->mDataConnections:Ljava/util/HashMap;

    #@d
    invoke-virtual {v2, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@10
    move-result-object v0

    #@11
    check-cast v0, Lcom/lge/ims/Configuration$DataConnection;

    #@13
    .line 135
    .local v0, dc:Lcom/lge/ims/Configuration$DataConnection;
    if-eqz v0, :cond_8

    #@15
    .line 139
    invoke-virtual {v0}, Lcom/lge/ims/Configuration$DataConnection;->getIPVersion()I

    #@18
    move-result v1

    #@19
    goto :goto_8
.end method

.method public static init(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 82
    if-nez p0, :cond_5

    #@4
    .line 112
    :cond_4
    :goto_4
    return-void

    #@5
    .line 86
    :cond_5
    sget-object v0, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@7
    if-eqz v0, :cond_4

    #@9
    .line 90
    sget-object v0, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@b
    iput-boolean v1, v0, Lcom/lge/ims/Configuration;->mACOn:Z

    #@d
    .line 91
    sget-object v0, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@f
    iput-boolean v1, v0, Lcom/lge/ims/Configuration;->mDebugOn:Z

    #@11
    .line 92
    sget-object v0, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@13
    iput-boolean v2, v0, Lcom/lge/ims/Configuration;->mImsOn:Z

    #@15
    .line 93
    sget-object v0, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@17
    iput-boolean v2, v0, Lcom/lge/ims/Configuration;->mIsimOn:Z

    #@19
    .line 94
    sget-object v0, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@1b
    iput-boolean v2, v0, Lcom/lge/ims/Configuration;->mUsimOn:Z

    #@1d
    .line 95
    sget-object v0, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@1f
    iput-boolean v1, v0, Lcom/lge/ims/Configuration;->mRCSeOn:Z

    #@21
    .line 96
    sget-object v0, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@23
    iput-boolean v1, v0, Lcom/lge/ims/Configuration;->mTestMode:Z

    #@25
    .line 97
    sget-object v0, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@27
    iput-boolean v1, v0, Lcom/lge/ims/Configuration;->mEmergencySupported:Z

    #@29
    .line 98
    sget-object v0, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@2b
    iget-object v0, v0, Lcom/lge/ims/Configuration;->mDataConnections:Ljava/util/HashMap;

    #@2d
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    #@30
    .line 100
    sget-object v0, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@32
    invoke-direct {v0, p0}, Lcom/lge/ims/Configuration;->initForSubscriber(Landroid/content/Context;)V

    #@35
    .line 101
    sget-object v0, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@37
    invoke-direct {v0, p0}, Lcom/lge/ims/Configuration;->initForAoSConnection(Landroid/content/Context;)V

    #@3a
    .line 103
    const-string v0, "Configuration"

    #@3c
    new-instance v1, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v2, "AC="

    #@43
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v1

    #@47
    sget-object v2, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@49
    iget-boolean v2, v2, Lcom/lge/ims/Configuration;->mACOn:Z

    #@4b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v1

    #@4f
    const-string v2, ", Debug="

    #@51
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v1

    #@55
    sget-object v2, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@57
    iget-boolean v2, v2, Lcom/lge/ims/Configuration;->mDebugOn:Z

    #@59
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v1

    #@5d
    const-string v2, ", IMS="

    #@5f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v1

    #@63
    sget-object v2, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@65
    iget-boolean v2, v2, Lcom/lge/ims/Configuration;->mImsOn:Z

    #@67
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v1

    #@6b
    const-string v2, ", ISIM="

    #@6d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v1

    #@71
    sget-object v2, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@73
    iget-boolean v2, v2, Lcom/lge/ims/Configuration;->mIsimOn:Z

    #@75
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@78
    move-result-object v1

    #@79
    const-string v2, ", USIM="

    #@7b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v1

    #@7f
    sget-object v2, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@81
    iget-boolean v2, v2, Lcom/lge/ims/Configuration;->mUsimOn:Z

    #@83
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@86
    move-result-object v1

    #@87
    const-string v2, ", RCS-e="

    #@89
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v1

    #@8d
    sget-object v2, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@8f
    iget-boolean v2, v2, Lcom/lge/ims/Configuration;->mRCSeOn:Z

    #@91
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@94
    move-result-object v1

    #@95
    const-string v2, ", TestMode="

    #@97
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v1

    #@9b
    sget-object v2, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@9d
    iget-boolean v2, v2, Lcom/lge/ims/Configuration;->mTestMode:Z

    #@9f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v1

    #@a3
    const-string v2, ", EmergencySupported="

    #@a5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v1

    #@a9
    sget-object v2, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@ab
    iget-boolean v2, v2, Lcom/lge/ims/Configuration;->mEmergencySupported:Z

    #@ad
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v1

    #@b1
    const-string v2, ", "

    #@b3
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v1

    #@b7
    sget-object v2, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@b9
    iget-object v2, v2, Lcom/lge/ims/Configuration;->mDataConnections:Ljava/util/HashMap;

    #@bb
    invoke-virtual {v2}, Ljava/util/HashMap;->toString()Ljava/lang/String;

    #@be
    move-result-object v2

    #@bf
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v1

    #@c3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c6
    move-result-object v1

    #@c7
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@ca
    goto/16 :goto_4
.end method

.method private initForAoSConnection(Landroid/content/Context;)V
    .registers 16
    .parameter "context"

    #@0
    .prologue
    .line 217
    const/4 v10, 0x0

    #@1
    .line 220
    .local v10, cursor:Landroid/database/Cursor;
    :try_start_1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4
    move-result-object v0

    #@5
    sget-object v1, Lcom/lge/ims/provider/IMS$AoSConnection;->CONTENT_URI:Landroid/net/Uri;

    #@7
    const/4 v2, 0x0

    #@8
    const/4 v3, 0x0

    #@9
    const/4 v4, 0x0

    #@a
    const/4 v5, 0x0

    #@b
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@e
    move-result-object v10

    #@f
    .line 222
    if-eqz v10, :cond_b3

    #@11
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    #@14
    move-result v0

    #@15
    if-eqz v0, :cond_b3

    #@17
    .line 223
    const/4 v13, 0x0

    #@18
    .line 224
    .local v13, profileName:Ljava/lang/String;
    const/4 v6, 0x0

    #@19
    .line 225
    .local v6, accessPolicy:I
    const/4 v12, 0x0

    #@1a
    .line 226
    .local v12, ipVersion:I
    const-string v0, "aos_connection_0_profile_name"

    #@1c
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@1f
    move-result v9

    #@20
    .line 227
    .local v9, columnForProfile:I
    const-string v0, "aos_connection_0_access_policy"

    #@22
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@25
    move-result v7

    #@26
    .line 228
    .local v7, columnForAccessPolicy:I
    const-string v0, "aos_connection_0_ip_version"

    #@28
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@2b
    move-result v8

    #@2c
    .line 230
    .local v8, columnForIPV:I
    if-ltz v9, :cond_32

    #@2e
    .line 231
    invoke-interface {v10, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@31
    move-result-object v13

    #@32
    .line 234
    :cond_32
    if-ltz v7, :cond_38

    #@34
    .line 235
    invoke-interface {v10, v7}, Landroid/database/Cursor;->getInt(I)I

    #@37
    move-result v6

    #@38
    .line 238
    :cond_38
    if-ltz v8, :cond_4e

    #@3a
    .line 239
    invoke-interface {v10, v8}, Landroid/database/Cursor;->getInt(I)I

    #@3d
    move-result v12

    #@3e
    .line 241
    const/4 v0, 0x4

    #@3f
    if-eq v12, v0, :cond_4e

    #@41
    const/4 v0, 0x6

    #@42
    if-eq v12, v0, :cond_4e

    #@44
    const/16 v0, 0x2e

    #@46
    if-eq v12, v0, :cond_4e

    #@48
    const/16 v0, 0x40

    #@4a
    if-eq v12, v0, :cond_4e

    #@4c
    .line 243
    iget v12, p0, Lcom/lge/ims/Configuration;->mDefaultIPVersion:I

    #@4e
    .line 247
    :cond_4e
    if-eqz v13, :cond_65

    #@50
    .line 248
    iget-object v0, p0, Lcom/lge/ims/Configuration;->mDataConnections:Ljava/util/HashMap;

    #@52
    new-instance v1, Lcom/lge/ims/Configuration$DataConnection;

    #@54
    invoke-direct {v1, p0, v6, v12}, Lcom/lge/ims/Configuration$DataConnection;-><init>(Lcom/lge/ims/Configuration;II)V

    #@57
    invoke-virtual {v0, v13, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5a
    .line 250
    const-string v0, "emergency"

    #@5c
    invoke-virtual {v13, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@5f
    move-result v0

    #@60
    if-eqz v0, :cond_65

    #@62
    .line 251
    const/4 v0, 0x1

    #@63
    iput-boolean v0, p0, Lcom/lge/ims/Configuration;->mEmergencySupported:Z

    #@65
    .line 255
    :cond_65
    const/4 v13, 0x0

    #@66
    .line 256
    const/4 v6, 0x0

    #@67
    .line 257
    const/4 v12, 0x0

    #@68
    .line 258
    const-string v0, "aos_connection_1_profile_name"

    #@6a
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@6d
    move-result v9

    #@6e
    .line 259
    const-string v0, "aos_connection_1_access_policy"

    #@70
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@73
    move-result v7

    #@74
    .line 260
    const-string v0, "aos_connection_1_ip_version"

    #@76
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@79
    move-result v8

    #@7a
    .line 262
    if-ltz v9, :cond_80

    #@7c
    .line 263
    invoke-interface {v10, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@7f
    move-result-object v13

    #@80
    .line 266
    :cond_80
    if-ltz v7, :cond_86

    #@82
    .line 267
    invoke-interface {v10, v7}, Landroid/database/Cursor;->getInt(I)I

    #@85
    move-result v6

    #@86
    .line 270
    :cond_86
    if-ltz v8, :cond_9c

    #@88
    .line 271
    invoke-interface {v10, v8}, Landroid/database/Cursor;->getInt(I)I

    #@8b
    move-result v12

    #@8c
    .line 273
    const/4 v0, 0x4

    #@8d
    if-eq v12, v0, :cond_9c

    #@8f
    const/4 v0, 0x6

    #@90
    if-eq v12, v0, :cond_9c

    #@92
    const/16 v0, 0x2e

    #@94
    if-eq v12, v0, :cond_9c

    #@96
    const/16 v0, 0x40

    #@98
    if-eq v12, v0, :cond_9c

    #@9a
    .line 275
    iget v12, p0, Lcom/lge/ims/Configuration;->mDefaultIPVersion:I

    #@9c
    .line 279
    :cond_9c
    if-eqz v13, :cond_b3

    #@9e
    .line 280
    iget-object v0, p0, Lcom/lge/ims/Configuration;->mDataConnections:Ljava/util/HashMap;

    #@a0
    new-instance v1, Lcom/lge/ims/Configuration$DataConnection;

    #@a2
    invoke-direct {v1, p0, v6, v12}, Lcom/lge/ims/Configuration$DataConnection;-><init>(Lcom/lge/ims/Configuration;II)V

    #@a5
    invoke-virtual {v0, v13, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@a8
    .line 282
    const-string v0, "emergency"

    #@aa
    invoke-virtual {v13, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@ad
    move-result v0

    #@ae
    if-eqz v0, :cond_b3

    #@b0
    .line 283
    const/4 v0, 0x1

    #@b1
    iput-boolean v0, p0, Lcom/lge/ims/Configuration;->mEmergencySupported:Z
    :try_end_b3
    .catchall {:try_start_1 .. :try_end_b3} :catchall_d9
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_b3} :catch_b9
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_b3} :catch_c9

    #@b3
    .line 326
    .end local v6           #accessPolicy:I
    .end local v7           #columnForAccessPolicy:I
    .end local v8           #columnForIPV:I
    .end local v9           #columnForProfile:I
    .end local v12           #ipVersion:I
    .end local v13           #profileName:Ljava/lang/String;
    :cond_b3
    if-eqz v10, :cond_b8

    #@b5
    .line 327
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@b8
    .line 330
    :cond_b8
    :goto_b8
    return-void

    #@b9
    .line 321
    :catch_b9
    move-exception v11

    #@ba
    .line 322
    .local v11, e:Landroid/database/sqlite/SQLiteException;
    :try_start_ba
    const-string v0, "Configuration"

    #@bc
    invoke-virtual {v11}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    #@bf
    move-result-object v1

    #@c0
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_c3
    .catchall {:try_start_ba .. :try_end_c3} :catchall_d9

    #@c3
    .line 326
    if-eqz v10, :cond_b8

    #@c5
    .line 327
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@c8
    goto :goto_b8

    #@c9
    .line 323
    .end local v11           #e:Landroid/database/sqlite/SQLiteException;
    :catch_c9
    move-exception v11

    #@ca
    .line 324
    .local v11, e:Ljava/lang/Exception;
    :try_start_ca
    const-string v0, "Configuration"

    #@cc
    invoke-virtual {v11}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@cf
    move-result-object v1

    #@d0
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_d3
    .catchall {:try_start_ca .. :try_end_d3} :catchall_d9

    #@d3
    .line 326
    if-eqz v10, :cond_b8

    #@d5
    .line 327
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@d8
    goto :goto_b8

    #@d9
    .line 326
    .end local v11           #e:Ljava/lang/Exception;
    :catchall_d9
    move-exception v0

    #@da
    if-eqz v10, :cond_df

    #@dc
    .line 327
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@df
    :cond_df
    throw v0
.end method

.method private initForSubscriber(Landroid/content/Context;)V
    .registers 19
    .parameter "context"

    #@0
    .prologue
    .line 333
    const/4 v9, 0x0

    #@1
    .line 336
    .local v9, cursor:Landroid/database/Cursor;
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4
    move-result-object v1

    #@5
    sget-object v2, Lcom/lge/ims/provider/IMS$Subscriber;->CONTENT_URI:Landroid/net/Uri;

    #@7
    const/4 v3, 0x0

    #@8
    const/4 v4, 0x0

    #@9
    const/4 v5, 0x0

    #@a
    const/4 v6, 0x0

    #@b
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@e
    move-result-object v9

    #@f
    .line 338
    if-eqz v9, :cond_cc

    #@11
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    #@14
    move-result v1

    #@15
    if-eqz v1, :cond_cc

    #@17
    .line 339
    const-string v1, "admin_ims"

    #@19
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@1c
    move-result v8

    #@1d
    .line 341
    .local v8, column:I
    if-ltz v8, :cond_37

    #@1f
    .line 342
    invoke-interface {v9, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@22
    move-result-object v12

    #@23
    .line 344
    .local v12, ims:Ljava/lang/String;
    const-string v1, "true"

    #@25
    invoke-virtual {v1, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@28
    move-result v1

    #@29
    if-nez v1, :cond_37

    #@2b
    .line 345
    const/4 v1, 0x0

    #@2c
    move-object/from16 v0, p0

    #@2e
    iput-boolean v1, v0, Lcom/lge/ims/Configuration;->mImsOn:Z

    #@30
    .line 346
    const-string v1, "Configuration"

    #@32
    const-string v2, "IMS is disabled"

    #@34
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@37
    .line 351
    .end local v12           #ims:Ljava/lang/String;
    :cond_37
    const-string v1, "admin_isim"

    #@39
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@3c
    move-result v8

    #@3d
    .line 353
    if-ltz v8, :cond_50

    #@3f
    .line 354
    invoke-interface {v9, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@42
    move-result-object v13

    #@43
    .line 356
    .local v13, isim:Ljava/lang/String;
    const-string v1, "true"

    #@45
    invoke-virtual {v1, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@48
    move-result v1

    #@49
    if-nez v1, :cond_50

    #@4b
    .line 357
    const/4 v1, 0x0

    #@4c
    move-object/from16 v0, p0

    #@4e
    iput-boolean v1, v0, Lcom/lge/ims/Configuration;->mIsimOn:Z

    #@50
    .line 362
    .end local v13           #isim:Ljava/lang/String;
    :cond_50
    const-string v1, "admin_usim"

    #@52
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@55
    move-result v8

    #@56
    .line 364
    if-ltz v8, :cond_6b

    #@58
    .line 365
    invoke-interface {v9, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@5b
    move-result-object v16

    #@5c
    .line 367
    .local v16, usim:Ljava/lang/String;
    const-string v1, "true"

    #@5e
    move-object/from16 v0, v16

    #@60
    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@63
    move-result v1

    #@64
    if-nez v1, :cond_6b

    #@66
    .line 368
    const/4 v1, 0x0

    #@67
    move-object/from16 v0, p0

    #@69
    iput-boolean v1, v0, Lcom/lge/ims/Configuration;->mUsimOn:Z

    #@6b
    .line 373
    .end local v16           #usim:Ljava/lang/String;
    :cond_6b
    const-string v1, "admin_ac"

    #@6d
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@70
    move-result v8

    #@71
    .line 375
    if-ltz v8, :cond_84

    #@73
    .line 376
    invoke-interface {v9, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@76
    move-result-object v7

    #@77
    .line 378
    .local v7, ac:Ljava/lang/String;
    const-string v1, "true"

    #@79
    invoke-virtual {v1, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@7c
    move-result v1

    #@7d
    if-eqz v1, :cond_84

    #@7f
    .line 379
    const/4 v1, 0x1

    #@80
    move-object/from16 v0, p0

    #@82
    iput-boolean v1, v0, Lcom/lge/ims/Configuration;->mACOn:Z

    #@84
    .line 384
    .end local v7           #ac:Ljava/lang/String;
    :cond_84
    const-string v1, "admin_debug"

    #@86
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@89
    move-result v8

    #@8a
    .line 386
    if-ltz v8, :cond_9d

    #@8c
    .line 387
    invoke-interface {v9, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@8f
    move-result-object v10

    #@90
    .line 389
    .local v10, debug:Ljava/lang/String;
    const-string v1, "true"

    #@92
    invoke-virtual {v1, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@95
    move-result v1

    #@96
    if-eqz v1, :cond_9d

    #@98
    .line 390
    const/4 v1, 0x1

    #@99
    move-object/from16 v0, p0

    #@9b
    iput-boolean v1, v0, Lcom/lge/ims/Configuration;->mDebugOn:Z

    #@9d
    .line 395
    .end local v10           #debug:Ljava/lang/String;
    :cond_9d
    const-string v1, "admin_services"

    #@9f
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@a2
    move-result v8

    #@a3
    .line 397
    if-ltz v8, :cond_b3

    #@a5
    .line 398
    invoke-interface {v9, v8}, Landroid/database/Cursor;->getInt(I)I

    #@a8
    move-result v14

    #@a9
    .line 400
    .local v14, services:I
    const/high16 v1, -0x8000

    #@ab
    and-int/2addr v1, v14

    #@ac
    if-eqz v1, :cond_b3

    #@ae
    .line 401
    const/4 v1, 0x1

    #@af
    move-object/from16 v0, p0

    #@b1
    iput-boolean v1, v0, Lcom/lge/ims/Configuration;->mRCSeOn:Z

    #@b3
    .line 406
    .end local v14           #services:I
    :cond_b3
    const-string v1, "admin_testmode"

    #@b5
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@b8
    move-result v8

    #@b9
    .line 408
    if-ltz v8, :cond_cc

    #@bb
    .line 409
    invoke-interface {v9, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@be
    move-result-object v15

    #@bf
    .line 411
    .local v15, testmode:Ljava/lang/String;
    const-string v1, "true"

    #@c1
    invoke-virtual {v1, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@c4
    move-result v1

    #@c5
    if-eqz v1, :cond_cc

    #@c7
    .line 412
    const/4 v1, 0x1

    #@c8
    move-object/from16 v0, p0

    #@ca
    iput-boolean v1, v0, Lcom/lge/ims/Configuration;->mTestMode:Z
    :try_end_cc
    .catchall {:try_start_1 .. :try_end_cc} :catchall_f2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_cc} :catch_d2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_cc} :catch_e2

    #@cc
    .line 421
    .end local v8           #column:I
    .end local v15           #testmode:Ljava/lang/String;
    :cond_cc
    if-eqz v9, :cond_d1

    #@ce
    .line 422
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@d1
    .line 425
    :cond_d1
    :goto_d1
    return-void

    #@d2
    .line 416
    :catch_d2
    move-exception v11

    #@d3
    .line 417
    .local v11, e:Landroid/database/sqlite/SQLiteException;
    :try_start_d3
    const-string v1, "Configuration"

    #@d5
    invoke-virtual {v11}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    #@d8
    move-result-object v2

    #@d9
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_dc
    .catchall {:try_start_d3 .. :try_end_dc} :catchall_f2

    #@dc
    .line 421
    if-eqz v9, :cond_d1

    #@de
    .line 422
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@e1
    goto :goto_d1

    #@e2
    .line 418
    .end local v11           #e:Landroid/database/sqlite/SQLiteException;
    :catch_e2
    move-exception v11

    #@e3
    .line 419
    .local v11, e:Ljava/lang/Exception;
    :try_start_e3
    const-string v1, "Configuration"

    #@e5
    invoke-virtual {v11}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@e8
    move-result-object v2

    #@e9
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_ec
    .catchall {:try_start_e3 .. :try_end_ec} :catchall_f2

    #@ec
    .line 421
    if-eqz v9, :cond_d1

    #@ee
    .line 422
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@f1
    goto :goto_d1

    #@f2
    .line 421
    .end local v11           #e:Ljava/lang/Exception;
    :catchall_f2
    move-exception v1

    #@f3
    if-eqz v9, :cond_f8

    #@f5
    .line 422
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@f8
    :cond_f8
    throw v1
.end method

.method public static isACOn()Z
    .registers 1

    #@0
    .prologue
    .line 143
    sget-object v0, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 144
    const/4 v0, 0x0

    #@5
    .line 147
    :goto_5
    return v0

    #@6
    :cond_6
    sget-object v0, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@8
    iget-boolean v0, v0, Lcom/lge/ims/Configuration;->mACOn:Z

    #@a
    goto :goto_5
.end method

.method public static isApnSupported(Ljava/lang/String;)Z
    .registers 4
    .parameter "apn"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 151
    if-eqz p0, :cond_7

    #@3
    sget-object v2, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@5
    if-nez v2, :cond_8

    #@7
    .line 157
    :cond_7
    :goto_7
    return v1

    #@8
    .line 155
    :cond_8
    sget-object v2, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@a
    iget-object v2, v2, Lcom/lge/ims/Configuration;->mDataConnections:Ljava/util/HashMap;

    #@c
    invoke-virtual {v2, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Lcom/lge/ims/Configuration$DataConnection;

    #@12
    .line 157
    .local v0, dc:Lcom/lge/ims/Configuration$DataConnection;
    if-eqz v0, :cond_7

    #@14
    const/4 v1, 0x1

    #@15
    goto :goto_7
.end method

.method public static isDebugOn()Z
    .registers 1

    #@0
    .prologue
    .line 161
    sget-object v0, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 162
    const/4 v0, 0x0

    #@5
    .line 165
    :goto_5
    return v0

    #@6
    :cond_6
    sget-object v0, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@8
    iget-boolean v0, v0, Lcom/lge/ims/Configuration;->mDebugOn:Z

    #@a
    goto :goto_5
.end method

.method public static isEmergencySupported()Z
    .registers 1

    #@0
    .prologue
    .line 169
    sget-object v0, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 170
    const/4 v0, 0x0

    #@5
    .line 173
    :goto_5
    return v0

    #@6
    :cond_6
    sget-object v0, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@8
    iget-boolean v0, v0, Lcom/lge/ims/Configuration;->mEmergencySupported:Z

    #@a
    goto :goto_5
.end method

.method public static isImsOn()Z
    .registers 1

    #@0
    .prologue
    .line 177
    sget-object v0, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 178
    const/4 v0, 0x1

    #@5
    .line 181
    :goto_5
    return v0

    #@6
    :cond_6
    sget-object v0, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@8
    iget-boolean v0, v0, Lcom/lge/ims/Configuration;->mImsOn:Z

    #@a
    goto :goto_5
.end method

.method public static isIsimOn()Z
    .registers 1

    #@0
    .prologue
    .line 185
    sget-object v0, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 186
    const/4 v0, 0x1

    #@5
    .line 189
    :goto_5
    return v0

    #@6
    :cond_6
    sget-object v0, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@8
    iget-boolean v0, v0, Lcom/lge/ims/Configuration;->mIsimOn:Z

    #@a
    goto :goto_5
.end method

.method public static isRCSeOn()Z
    .registers 1

    #@0
    .prologue
    .line 201
    sget-object v0, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 202
    const/4 v0, 0x0

    #@5
    .line 205
    :goto_5
    return v0

    #@6
    :cond_6
    sget-object v0, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@8
    iget-boolean v0, v0, Lcom/lge/ims/Configuration;->mRCSeOn:Z

    #@a
    goto :goto_5
.end method

.method public static isTestMode()Z
    .registers 1

    #@0
    .prologue
    .line 209
    sget-object v0, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 210
    const/4 v0, 0x0

    #@5
    .line 213
    :goto_5
    return v0

    #@6
    :cond_6
    sget-object v0, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@8
    iget-boolean v0, v0, Lcom/lge/ims/Configuration;->mTestMode:Z

    #@a
    goto :goto_5
.end method

.method public static isUsimOn()Z
    .registers 1

    #@0
    .prologue
    .line 193
    sget-object v0, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 194
    const/4 v0, 0x1

    #@5
    .line 197
    :goto_5
    return v0

    #@6
    :cond_6
    sget-object v0, Lcom/lge/ims/Configuration;->mConfiguration:Lcom/lge/ims/Configuration;

    #@8
    iget-boolean v0, v0, Lcom/lge/ims/Configuration;->mUsimOn:Z

    #@a
    goto :goto_5
.end method
