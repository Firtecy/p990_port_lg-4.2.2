.class final enum Lcom/lge/ims/BootupController$STATE;
.super Ljava/lang/Enum;
.source "BootupController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/BootupController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "STATE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/lge/ims/BootupController$STATE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/lge/ims/BootupController$STATE;

.field public static final enum NONE:Lcom/lge/ims/BootupController$STATE;

.field public static final enum UPDATING_AUTOCONFIG:Lcom/lge/ims/BootupController$STATE;

.field public static final enum WAITING_BOOT_COMPLETE:Lcom/lge/ims/BootupController$STATE;

.field public static final enum WAITING_CLEANUP:Lcom/lge/ims/BootupController$STATE;

.field public static final enum WAITING_NETWORK_CONNECTED:Lcom/lge/ims/BootupController$STATE;

.field public static final enum WAITING_USER_ACTION:Lcom/lge/ims/BootupController$STATE;

.field public static final enum WORKING_RCSE:Lcom/lge/ims/BootupController$STATE;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 73
    new-instance v0, Lcom/lge/ims/BootupController$STATE;

    #@7
    const-string v1, "NONE"

    #@9
    invoke-direct {v0, v1, v3}, Lcom/lge/ims/BootupController$STATE;-><init>(Ljava/lang/String;I)V

    #@c
    sput-object v0, Lcom/lge/ims/BootupController$STATE;->NONE:Lcom/lge/ims/BootupController$STATE;

    #@e
    .line 74
    new-instance v0, Lcom/lge/ims/BootupController$STATE;

    #@10
    const-string v1, "WAITING_BOOT_COMPLETE"

    #@12
    invoke-direct {v0, v1, v4}, Lcom/lge/ims/BootupController$STATE;-><init>(Ljava/lang/String;I)V

    #@15
    sput-object v0, Lcom/lge/ims/BootupController$STATE;->WAITING_BOOT_COMPLETE:Lcom/lge/ims/BootupController$STATE;

    #@17
    .line 75
    new-instance v0, Lcom/lge/ims/BootupController$STATE;

    #@19
    const-string v1, "WAITING_NETWORK_CONNECTED"

    #@1b
    invoke-direct {v0, v1, v5}, Lcom/lge/ims/BootupController$STATE;-><init>(Ljava/lang/String;I)V

    #@1e
    sput-object v0, Lcom/lge/ims/BootupController$STATE;->WAITING_NETWORK_CONNECTED:Lcom/lge/ims/BootupController$STATE;

    #@20
    .line 76
    new-instance v0, Lcom/lge/ims/BootupController$STATE;

    #@22
    const-string v1, "UPDATING_AUTOCONFIG"

    #@24
    invoke-direct {v0, v1, v6}, Lcom/lge/ims/BootupController$STATE;-><init>(Ljava/lang/String;I)V

    #@27
    sput-object v0, Lcom/lge/ims/BootupController$STATE;->UPDATING_AUTOCONFIG:Lcom/lge/ims/BootupController$STATE;

    #@29
    .line 77
    new-instance v0, Lcom/lge/ims/BootupController$STATE;

    #@2b
    const-string v1, "WORKING_RCSE"

    #@2d
    invoke-direct {v0, v1, v7}, Lcom/lge/ims/BootupController$STATE;-><init>(Ljava/lang/String;I)V

    #@30
    sput-object v0, Lcom/lge/ims/BootupController$STATE;->WORKING_RCSE:Lcom/lge/ims/BootupController$STATE;

    #@32
    .line 78
    new-instance v0, Lcom/lge/ims/BootupController$STATE;

    #@34
    const-string v1, "WAITING_USER_ACTION"

    #@36
    const/4 v2, 0x5

    #@37
    invoke-direct {v0, v1, v2}, Lcom/lge/ims/BootupController$STATE;-><init>(Ljava/lang/String;I)V

    #@3a
    sput-object v0, Lcom/lge/ims/BootupController$STATE;->WAITING_USER_ACTION:Lcom/lge/ims/BootupController$STATE;

    #@3c
    .line 79
    new-instance v0, Lcom/lge/ims/BootupController$STATE;

    #@3e
    const-string v1, "WAITING_CLEANUP"

    #@40
    const/4 v2, 0x6

    #@41
    invoke-direct {v0, v1, v2}, Lcom/lge/ims/BootupController$STATE;-><init>(Ljava/lang/String;I)V

    #@44
    sput-object v0, Lcom/lge/ims/BootupController$STATE;->WAITING_CLEANUP:Lcom/lge/ims/BootupController$STATE;

    #@46
    .line 72
    const/4 v0, 0x7

    #@47
    new-array v0, v0, [Lcom/lge/ims/BootupController$STATE;

    #@49
    sget-object v1, Lcom/lge/ims/BootupController$STATE;->NONE:Lcom/lge/ims/BootupController$STATE;

    #@4b
    aput-object v1, v0, v3

    #@4d
    sget-object v1, Lcom/lge/ims/BootupController$STATE;->WAITING_BOOT_COMPLETE:Lcom/lge/ims/BootupController$STATE;

    #@4f
    aput-object v1, v0, v4

    #@51
    sget-object v1, Lcom/lge/ims/BootupController$STATE;->WAITING_NETWORK_CONNECTED:Lcom/lge/ims/BootupController$STATE;

    #@53
    aput-object v1, v0, v5

    #@55
    sget-object v1, Lcom/lge/ims/BootupController$STATE;->UPDATING_AUTOCONFIG:Lcom/lge/ims/BootupController$STATE;

    #@57
    aput-object v1, v0, v6

    #@59
    sget-object v1, Lcom/lge/ims/BootupController$STATE;->WORKING_RCSE:Lcom/lge/ims/BootupController$STATE;

    #@5b
    aput-object v1, v0, v7

    #@5d
    const/4 v1, 0x5

    #@5e
    sget-object v2, Lcom/lge/ims/BootupController$STATE;->WAITING_USER_ACTION:Lcom/lge/ims/BootupController$STATE;

    #@60
    aput-object v2, v0, v1

    #@62
    const/4 v1, 0x6

    #@63
    sget-object v2, Lcom/lge/ims/BootupController$STATE;->WAITING_CLEANUP:Lcom/lge/ims/BootupController$STATE;

    #@65
    aput-object v2, v0, v1

    #@67
    sput-object v0, Lcom/lge/ims/BootupController$STATE;->$VALUES:[Lcom/lge/ims/BootupController$STATE;

    #@69
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 72
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/lge/ims/BootupController$STATE;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 72
    const-class v0, Lcom/lge/ims/BootupController$STATE;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/lge/ims/BootupController$STATE;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/lge/ims/BootupController$STATE;
    .registers 1

    #@0
    .prologue
    .line 72
    sget-object v0, Lcom/lge/ims/BootupController$STATE;->$VALUES:[Lcom/lge/ims/BootupController$STATE;

    #@2
    invoke-virtual {v0}, [Lcom/lge/ims/BootupController$STATE;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/lge/ims/BootupController$STATE;

    #@8
    return-object v0
.end method
