.class public final Lcom/lge/ims/PhoneInfo;
.super Ljava/lang/Object;
.source "PhoneInfo.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 8
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static getCallState()I
    .registers 2

    #@0
    .prologue
    .line 12
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getTelephonyManager()Landroid/telephony/TelephonyManager;

    #@3
    move-result-object v0

    #@4
    .line 14
    .local v0, tm:Landroid/telephony/TelephonyManager;
    if-nez v0, :cond_8

    #@6
    .line 15
    const/4 v1, 0x0

    #@7
    .line 18
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public static getDataActivity()I
    .registers 2

    #@0
    .prologue
    .line 23
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getTelephonyManager()Landroid/telephony/TelephonyManager;

    #@3
    move-result-object v0

    #@4
    .line 25
    .local v0, tm:Landroid/telephony/TelephonyManager;
    if-nez v0, :cond_8

    #@6
    .line 26
    const/4 v1, 0x0

    #@7
    .line 29
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDataActivity()I

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public static getDeviceId()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 34
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getTelephonyManager()Landroid/telephony/TelephonyManager;

    #@3
    move-result-object v0

    #@4
    .line 36
    .local v0, tm:Landroid/telephony/TelephonyManager;
    if-nez v0, :cond_8

    #@6
    .line 37
    const/4 v1, 0x0

    #@7
    .line 40
    :goto_7
    return-object v1

    #@8
    :cond_8
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    goto :goto_7
.end method

.method public static getDeviceSoftwareVersion()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 45
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getTelephonyManager()Landroid/telephony/TelephonyManager;

    #@3
    move-result-object v0

    #@4
    .line 47
    .local v0, tm:Landroid/telephony/TelephonyManager;
    if-nez v0, :cond_8

    #@6
    .line 48
    const/4 v1, 0x0

    #@7
    .line 51
    :goto_7
    return-object v1

    #@8
    :cond_8
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceSoftwareVersion()Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    goto :goto_7
.end method

.method public static getMcc(Z)Ljava/lang/String;
    .registers 6
    .parameter "fromSim"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 56
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getTelephonyManager()Landroid/telephony/TelephonyManager;

    #@4
    move-result-object v1

    #@5
    .line 58
    .local v1, tm:Landroid/telephony/TelephonyManager;
    if-nez v1, :cond_8

    #@7
    .line 78
    :cond_7
    :goto_7
    return-object v2

    #@8
    .line 62
    :cond_8
    const/4 v0, 0x0

    #@9
    .line 64
    .local v0, operator:Ljava/lang/String;
    if-eqz p0, :cond_1f

    #@b
    .line 65
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    .line 70
    :goto_f
    if-eqz v0, :cond_7

    #@11
    .line 74
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@14
    move-result v3

    #@15
    const/4 v4, 0x5

    #@16
    if-lt v3, v4, :cond_7

    #@18
    .line 78
    const/4 v2, 0x0

    #@19
    const/4 v3, 0x3

    #@1a
    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    goto :goto_7

    #@1f
    .line 67
    :cond_1f
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    goto :goto_f
.end method

.method public static getMnc(Z)Ljava/lang/String;
    .registers 6
    .parameter "fromSim"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 83
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getTelephonyManager()Landroid/telephony/TelephonyManager;

    #@4
    move-result-object v1

    #@5
    .line 85
    .local v1, tm:Landroid/telephony/TelephonyManager;
    if-nez v1, :cond_8

    #@7
    .line 105
    :cond_7
    :goto_7
    return-object v2

    #@8
    .line 89
    :cond_8
    const/4 v0, 0x0

    #@9
    .line 91
    .local v0, operator:Ljava/lang/String;
    if-eqz p0, :cond_1e

    #@b
    .line 92
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    .line 97
    :goto_f
    if-eqz v0, :cond_7

    #@11
    .line 101
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@14
    move-result v3

    #@15
    const/4 v4, 0x5

    #@16
    if-lt v3, v4, :cond_7

    #@18
    .line 105
    const/4 v2, 0x3

    #@19
    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    goto :goto_7

    #@1e
    .line 94
    :cond_1e
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    #@21
    move-result-object v0

    #@22
    goto :goto_f
.end method

.method public static getNetworkType()I
    .registers 2

    #@0
    .prologue
    .line 152
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getTelephonyManager()Landroid/telephony/TelephonyManager;

    #@3
    move-result-object v0

    #@4
    .line 154
    .local v0, tm:Landroid/telephony/TelephonyManager;
    if-nez v0, :cond_8

    #@6
    .line 155
    const/4 v1, 0x0

    #@7
    .line 158
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public static getPhoneNumber()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 110
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getTelephonyManager()Landroid/telephony/TelephonyManager;

    #@3
    move-result-object v0

    #@4
    .line 112
    .local v0, tm:Landroid/telephony/TelephonyManager;
    if-nez v0, :cond_8

    #@6
    .line 113
    const/4 v1, 0x0

    #@7
    .line 116
    :goto_7
    return-object v1

    #@8
    :cond_8
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    goto :goto_7
.end method

.method public static getPhoneNumberExcludingNationalPrefix()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x3

    #@1
    .line 121
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getTelephonyManager()Landroid/telephony/TelephonyManager;

    #@4
    move-result-object v1

    #@5
    .line 123
    .local v1, tm:Landroid/telephony/TelephonyManager;
    if-nez v1, :cond_9

    #@7
    .line 124
    const/4 v0, 0x0

    #@8
    .line 136
    :cond_8
    :goto_8
    return-object v0

    #@9
    .line 127
    :cond_9
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    .line 130
    .local v0, phoneNumber:Ljava/lang/String;
    if-eqz v0, :cond_8

    #@f
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@12
    move-result v2

    #@13
    if-lt v2, v4, :cond_8

    #@15
    .line 131
    const-string v2, "+82"

    #@17
    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@1a
    move-result v2

    #@1b
    if-eqz v2, :cond_8

    #@1d
    .line 132
    new-instance v2, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v3, "0"

    #@24
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@2b
    move-result-object v3

    #@2c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v0

    #@34
    goto :goto_8
.end method

.method public static getPhoneType()I
    .registers 2

    #@0
    .prologue
    .line 141
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getTelephonyManager()Landroid/telephony/TelephonyManager;

    #@3
    move-result-object v0

    #@4
    .line 143
    .local v0, tm:Landroid/telephony/TelephonyManager;
    if-nez v0, :cond_8

    #@6
    .line 144
    const/4 v1, 0x0

    #@7
    .line 147
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public static getSubscriberId()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 163
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getTelephonyManager()Landroid/telephony/TelephonyManager;

    #@3
    move-result-object v0

    #@4
    .line 165
    .local v0, tm:Landroid/telephony/TelephonyManager;
    if-nez v0, :cond_8

    #@6
    .line 166
    const/4 v1, 0x0

    #@7
    .line 169
    :goto_7
    return-object v1

    #@8
    :cond_8
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    goto :goto_7
.end method

.method public static getSubscriberIdWithDummy()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 174
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getTelephonyManager()Landroid/telephony/TelephonyManager;

    #@3
    move-result-object v1

    #@4
    .line 176
    .local v1, tm:Landroid/telephony/TelephonyManager;
    if-nez v1, :cond_8

    #@6
    .line 177
    const/4 v0, 0x0

    #@7
    .line 190
    :goto_7
    return-object v0

    #@8
    .line 180
    :cond_8
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    .line 182
    .local v0, imsi:Ljava/lang/String;
    if-nez v0, :cond_11

    #@e
    .line 183
    const-string v0, "450000000000000"

    #@10
    goto :goto_7

    #@11
    .line 184
    :cond_11
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    #@14
    move-result v2

    #@15
    if-eqz v2, :cond_1a

    #@17
    .line 185
    const-string v0, "450000000000000"

    #@19
    goto :goto_7

    #@1a
    .line 187
    :cond_1a
    const-string v2, ":"

    #@1c
    const-string v3, "0"

    #@1e
    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@21
    move-result-object v0

    #@22
    goto :goto_7
.end method

.method public static isImsEmergencyCallSupported()Z
    .registers 6

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 195
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getInstance()Lcom/lge/ims/SystemServiceManager;

    #@5
    move-result-object v1

    #@6
    .line 197
    .local v1, ssm:Lcom/lge/ims/SystemServiceManager;
    if-nez v1, :cond_9

    #@8
    .line 208
    :cond_8
    :goto_8
    return v2

    #@9
    .line 201
    :cond_9
    const/16 v3, 0x8

    #@b
    const-string v4, ""

    #@d
    invoke-virtual {v1, v3, v5, v4}, Lcom/lge/ims/SystemServiceManager;->getSysInfo(IILjava/lang/String;)[Ljava/lang/String;

    #@10
    move-result-object v0

    #@11
    .line 204
    .local v0, result:[Ljava/lang/String;
    if-eqz v0, :cond_8

    #@13
    .line 208
    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@16
    move-result-object v3

    #@17
    aget-object v2, v0, v2

    #@19
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c
    move-result v2

    #@1d
    goto :goto_8
.end method

.method public static isImsVoiceCallSupported()Z
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 213
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getInstance()Lcom/lge/ims/SystemServiceManager;

    #@4
    move-result-object v1

    #@5
    .line 215
    .local v1, ssm:Lcom/lge/ims/SystemServiceManager;
    if-nez v1, :cond_8

    #@7
    .line 226
    :cond_7
    :goto_7
    return v2

    #@8
    .line 219
    :cond_8
    const/16 v3, 0x8

    #@a
    const/4 v4, 0x2

    #@b
    const-string v5, ""

    #@d
    invoke-virtual {v1, v3, v4, v5}, Lcom/lge/ims/SystemServiceManager;->getSysInfo(IILjava/lang/String;)[Ljava/lang/String;

    #@10
    move-result-object v0

    #@11
    .line 222
    .local v0, result:[Ljava/lang/String;
    if-eqz v0, :cond_7

    #@13
    .line 226
    const/4 v3, 0x1

    #@14
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@17
    move-result-object v3

    #@18
    aget-object v2, v0, v2

    #@1a
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v2

    #@1e
    goto :goto_7
.end method

.method public static isNetworkRoaming()Z
    .registers 2

    #@0
    .prologue
    .line 231
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getTelephonyManager()Landroid/telephony/TelephonyManager;

    #@3
    move-result-object v0

    #@4
    .line 233
    .local v0, tm:Landroid/telephony/TelephonyManager;
    if-nez v0, :cond_8

    #@6
    .line 234
    const/4 v1, 0x0

    #@7
    .line 237
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method
