.class public Lcom/lge/ims/SysDispatcher;
.super Ljava/lang/Object;
.source "SysDispatcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/SysDispatcher$1;,
        Lcom/lge/ims/SysDispatcher$SysDispatcherHandler;
    }
.end annotation


# static fields
.field private static final EVENT_TOAST_CALL_REJECTED:I = 0x1

.field private static final EVENT_TOAST_REGISTRATION_FAILED:I = 0x2

.field private static final EVENT_TOAST_SERVICE_NOT_PROVISIONED:I = 0x3

.field public static final NAME_INDEX:I = 0x0

.field public static final NUMBER_INDEX:I = 0x1

.field public static final PHONELOOKUP_PROJECTION:[Ljava/lang/String; = null

.field public static final PHONE_NUMBER_SEPARATORS:Ljava/lang/String; = " ()-.+"

.field private static final TAG:Ljava/lang/String; = "SysDispatcher"

.field private static mContext:Landroid/content/Context;

.field private static mNativeCompleted:Z

.field private static mSysDispatcherHandler:Lcom/lge/ims/SysDispatcher$SysDispatcherHandler;

.field private static mWifiManager:Landroid/net/wifi/WifiManager;

.field private static wifiLock:Landroid/net/wifi/WifiManager$WifiLock;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v0, 0x0

    #@2
    .line 48
    sput-object v0, Lcom/lge/ims/SysDispatcher;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    #@4
    .line 55
    sput-object v0, Lcom/lge/ims/SysDispatcher;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@6
    .line 56
    sput-object v0, Lcom/lge/ims/SysDispatcher;->mContext:Landroid/content/Context;

    #@8
    .line 57
    sput-object v0, Lcom/lge/ims/SysDispatcher;->mSysDispatcherHandler:Lcom/lge/ims/SysDispatcher$SysDispatcherHandler;

    #@a
    .line 59
    sput-boolean v2, Lcom/lge/ims/SysDispatcher;->mNativeCompleted:Z

    #@c
    .line 950
    const/4 v0, 0x2

    #@d
    new-array v0, v0, [Ljava/lang/String;

    #@f
    const-string v1, "display_name"

    #@11
    aput-object v1, v0, v2

    #@13
    const/4 v1, 0x1

    #@14
    const-string v2, "number"

    #@16
    aput-object v2, v0, v1

    #@18
    sput-object v0, Lcom/lge/ims/SysDispatcher;->PHONELOOKUP_PROJECTION:[Ljava/lang/String;

    #@1a
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 62
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 63
    sput-object p1, Lcom/lge/ims/SysDispatcher;->mContext:Landroid/content/Context;

    #@5
    .line 65
    new-instance v1, Lcom/lge/ims/SysDispatcher$SysDispatcherHandler;

    #@7
    const/4 v2, 0x0

    #@8
    invoke-direct {v1, p0, v2}, Lcom/lge/ims/SysDispatcher$SysDispatcherHandler;-><init>(Lcom/lge/ims/SysDispatcher;Lcom/lge/ims/SysDispatcher$1;)V

    #@b
    sput-object v1, Lcom/lge/ims/SysDispatcher;->mSysDispatcherHandler:Lcom/lge/ims/SysDispatcher$SysDispatcherHandler;

    #@d
    .line 67
    invoke-direct {p0}, Lcom/lge/ims/SysDispatcher;->initializeSystemProperties()V

    #@10
    .line 70
    invoke-static {}, Lcom/lge/ims/service/uc/UCCallManager;->getInstance()Lcom/lge/ims/service/uc/UCCallManager;

    #@13
    move-result-object v0

    #@14
    .line 72
    .local v0, ucm:Lcom/lge/ims/service/uc/UCCallManager;
    if-eqz v0, :cond_1e

    #@16
    .line 73
    sget-object v1, Lcom/lge/ims/SysDispatcher;->mContext:Landroid/content/Context;

    #@18
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/uc/UCCallManager;->setContext(Landroid/content/Context;)V

    #@1b
    .line 74
    invoke-virtual {v0}, Lcom/lge/ims/service/uc/UCCallManager;->intializeUcState()V

    #@1e
    .line 77
    :cond_1e
    const-string v1, "SysDispatcher"

    #@20
    const-string v2, "SysDispatcher created"

    #@22
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@25
    .line 78
    return-void
.end method

.method static synthetic access$100()Landroid/content/Context;
    .registers 1

    #@0
    .prologue
    .line 45
    sget-object v0, Lcom/lge/ims/SysDispatcher;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method public static disableDataConnectivity(I)Z
    .registers 3
    .parameter "apnType"

    #@0
    .prologue
    .line 912
    invoke-static {}, Lcom/lge/ims/DataConnectionManager;->getInstance()Lcom/lge/ims/DataConnectionManager;

    #@3
    move-result-object v0

    #@4
    .line 914
    .local v0, dcm:Lcom/lge/ims/DataConnectionManager;
    if-eqz v0, :cond_a

    #@6
    .line 915
    const/4 v1, 0x0

    #@7
    invoke-virtual {v0, p0, v1}, Lcom/lge/ims/DataConnectionManager;->disconnect(II)V

    #@a
    .line 918
    :cond_a
    const/4 v1, 0x1

    #@b
    return v1
.end method

.method public static disableWifiLock()V
    .registers 1

    #@0
    .prologue
    .line 704
    sget-object v0, Lcom/lge/ims/SysDispatcher;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 705
    sget-object v0, Lcom/lge/ims/SysDispatcher;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    #@6
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    #@9
    .line 706
    const/4 v0, 0x0

    #@a
    sput-object v0, Lcom/lge/ims/SysDispatcher;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    #@c
    .line 708
    :cond_c
    return-void
.end method

.method public static enableDataConnectivity(I)Z
    .registers 3
    .parameter "apnType"

    #@0
    .prologue
    .line 902
    invoke-static {}, Lcom/lge/ims/DataConnectionManager;->getInstance()Lcom/lge/ims/DataConnectionManager;

    #@3
    move-result-object v0

    #@4
    .line 904
    .local v0, dcm:Lcom/lge/ims/DataConnectionManager;
    if-eqz v0, :cond_b

    #@6
    .line 905
    invoke-virtual {v0, p0}, Lcom/lge/ims/DataConnectionManager;->connect(I)Z

    #@9
    move-result v1

    #@a
    .line 908
    :goto_a
    return v1

    #@b
    :cond_b
    const/4 v1, 0x0

    #@c
    goto :goto_a
.end method

.method public static enableWifiLock()V
    .registers 2

    #@0
    .prologue
    .line 711
    invoke-static {}, Lcom/lge/ims/SysDispatcher;->getWifiManager()Landroid/net/wifi/WifiManager;

    #@3
    .line 713
    sget-object v0, Lcom/lge/ims/SysDispatcher;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    #@5
    if-nez v0, :cond_1c

    #@7
    .line 714
    sget-object v0, Lcom/lge/ims/SysDispatcher;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@9
    const-string v1, "VT/wifilock"

    #@b
    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager;->createWifiLock(Ljava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    #@e
    move-result-object v0

    #@f
    sput-object v0, Lcom/lge/ims/SysDispatcher;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    #@11
    .line 715
    sget-object v0, Lcom/lge/ims/SysDispatcher;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    #@13
    const/4 v1, 0x1

    #@14
    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager$WifiLock;->setReferenceCounted(Z)V

    #@17
    .line 716
    sget-object v0, Lcom/lge/ims/SysDispatcher;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    #@19
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    #@1c
    .line 718
    :cond_1c
    return-void
.end method

.method public static getAccessNetworkInfo()[Ljava/lang/String;
    .registers 14

    #@0
    .prologue
    const/4 v13, 0x0

    #@1
    const/4 v12, -0x1

    #@2
    const/4 v11, 0x3

    #@3
    const/4 v7, 0x0

    #@4
    .line 588
    const/4 v3, 0x0

    #@5
    .line 589
    .local v3, panInfo:[Ljava/lang/String;
    invoke-static {}, Lcom/lge/ims/PhoneInfo;->getNetworkType()I

    #@8
    move-result v4

    #@9
    .line 591
    .local v4, rat:I
    const-string v8, "SysDispatcher"

    #@b
    new-instance v9, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v10, "RAT="

    #@12
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v9

    #@16
    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v9

    #@1a
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v9

    #@1e
    invoke-static {v8, v9}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@21
    .line 593
    const/16 v8, 0xd

    #@23
    if-ne v4, v8, :cond_67

    #@25
    .line 594
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getInstance()Lcom/lge/ims/SystemServiceManager;

    #@28
    move-result-object v5

    #@29
    .line 596
    .local v5, ssm:Lcom/lge/ims/SystemServiceManager;
    if-nez v5, :cond_34

    #@2b
    .line 597
    const-string v8, "SysDispatcher"

    #@2d
    const-string v9, "SystemServiceManager is null"

    #@2f
    invoke-static {v8, v9}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@32
    move-object v3, v7

    #@33
    .line 663
    .end local v3           #panInfo:[Ljava/lang/String;
    .end local v5           #ssm:Lcom/lge/ims/SystemServiceManager;
    :cond_33
    :goto_33
    return-object v3

    #@34
    .line 601
    .restart local v3       #panInfo:[Ljava/lang/String;
    .restart local v5       #ssm:Lcom/lge/ims/SystemServiceManager;
    :cond_34
    const-string v7, ""

    #@36
    invoke-virtual {v5, v11, v4, v7}, Lcom/lge/ims/SystemServiceManager;->getSysInfo(IILjava/lang/String;)[Ljava/lang/String;

    #@39
    move-result-object v3

    #@3a
    .line 657
    .end local v5           #ssm:Lcom/lge/ims/SystemServiceManager;
    :goto_3a
    if-eqz v3, :cond_33

    #@3c
    .line 658
    const/4 v1, 0x0

    #@3d
    .local v1, i:I
    :goto_3d
    array-length v7, v3

    #@3e
    if-ge v1, v7, :cond_33

    #@40
    .line 659
    const-string v7, "SysDispatcher"

    #@42
    new-instance v8, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v9, "PAN Info ("

    #@49
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v8

    #@4d
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@50
    move-result-object v8

    #@51
    const-string v9, ") :: "

    #@53
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v8

    #@57
    aget-object v9, v3, v1

    #@59
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v8

    #@5d
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v8

    #@61
    invoke-static {v7, v8}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@64
    .line 658
    add-int/lit8 v1, v1, 0x1

    #@66
    goto :goto_3d

    #@67
    .line 602
    .end local v1           #i:I
    :cond_67
    if-eq v4, v11, :cond_79

    #@69
    const/16 v8, 0x8

    #@6b
    if-eq v4, v8, :cond_79

    #@6d
    const/16 v8, 0x9

    #@6f
    if-eq v4, v8, :cond_79

    #@71
    const/16 v8, 0xa

    #@73
    if-eq v4, v8, :cond_79

    #@75
    const/16 v8, 0xf

    #@77
    if-ne v4, v8, :cond_154

    #@79
    .line 608
    :cond_79
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getTelephonyManager()Landroid/telephony/TelephonyManager;

    #@7c
    move-result-object v6

    #@7d
    .line 610
    .local v6, tm:Landroid/telephony/TelephonyManager;
    if-nez v6, :cond_88

    #@7f
    .line 611
    const-string v8, "SysDispatcher"

    #@81
    const-string v9, "TelephonyManager is null"

    #@83
    invoke-static {v8, v9}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@86
    move-object v3, v7

    #@87
    .line 612
    goto :goto_33

    #@88
    .line 616
    :cond_88
    invoke-virtual {v6}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    #@8b
    move-result-object v2

    #@8c
    .line 618
    .local v2, operator:Ljava/lang/String;
    if-nez v2, :cond_97

    #@8e
    .line 619
    const-string v8, "SysDispatcher"

    #@90
    const-string v9, "Network operator is null"

    #@92
    invoke-static {v8, v9}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@95
    move-object v3, v7

    #@96
    .line 620
    goto :goto_33

    #@97
    .line 623
    :cond_97
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@9a
    move-result v8

    #@9b
    const/4 v9, 0x5

    #@9c
    if-ge v8, v9, :cond_bf

    #@9e
    .line 624
    const-string v8, "SysDispatcher"

    #@a0
    new-instance v9, Ljava/lang/StringBuilder;

    #@a2
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@a5
    const-string v10, "Network operator ("

    #@a7
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v9

    #@ab
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v9

    #@af
    const-string v10, ") is not valid"

    #@b1
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v9

    #@b5
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b8
    move-result-object v9

    #@b9
    invoke-static {v8, v9}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@bc
    move-object v3, v7

    #@bd
    .line 625
    goto/16 :goto_33

    #@bf
    .line 629
    :cond_bf
    invoke-virtual {v6}, Landroid/telephony/TelephonyManager;->getCellLocation()Landroid/telephony/CellLocation;

    #@c2
    move-result-object v0

    #@c3
    check-cast v0, Landroid/telephony/gsm/GsmCellLocation;

    #@c5
    .line 631
    .local v0, cl:Landroid/telephony/gsm/GsmCellLocation;
    if-nez v0, :cond_d1

    #@c7
    .line 632
    const-string v8, "SysDispatcher"

    #@c9
    const-string v9, "CellLocation is null"

    #@cb
    invoke-static {v8, v9}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@ce
    move-object v3, v7

    #@cf
    .line 633
    goto/16 :goto_33

    #@d1
    .line 636
    :cond_d1
    invoke-virtual {v0}, Landroid/telephony/gsm/GsmCellLocation;->getLac()I

    #@d4
    move-result v8

    #@d5
    if-eq v8, v12, :cond_e0

    #@d7
    invoke-virtual {v0}, Landroid/telephony/gsm/GsmCellLocation;->getLac()I

    #@da
    move-result v8

    #@db
    const v9, 0xffff

    #@de
    if-le v8, v9, :cond_ff

    #@e0
    .line 637
    :cond_e0
    const-string v8, "SysDispatcher"

    #@e2
    new-instance v9, Ljava/lang/StringBuilder;

    #@e4
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@e7
    const-string v10, "LAC is not valid; LAC="

    #@e9
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ec
    move-result-object v9

    #@ed
    invoke-virtual {v0}, Landroid/telephony/gsm/GsmCellLocation;->getLac()I

    #@f0
    move-result v10

    #@f1
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v9

    #@f5
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f8
    move-result-object v9

    #@f9
    invoke-static {v8, v9}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@fc
    move-object v3, v7

    #@fd
    .line 638
    goto/16 :goto_33

    #@ff
    .line 641
    :cond_ff
    invoke-virtual {v0}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    #@102
    move-result v8

    #@103
    if-eq v8, v12, :cond_10e

    #@105
    invoke-virtual {v0}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    #@108
    move-result v8

    #@109
    const v9, 0xfffffff

    #@10c
    if-le v8, v9, :cond_12d

    #@10e
    .line 642
    :cond_10e
    const-string v8, "SysDispatcher"

    #@110
    new-instance v9, Ljava/lang/StringBuilder;

    #@112
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@115
    const-string v10, "Cell identity is not valid; CID="

    #@117
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11a
    move-result-object v9

    #@11b
    invoke-virtual {v0}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    #@11e
    move-result v10

    #@11f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@122
    move-result-object v9

    #@123
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@126
    move-result-object v9

    #@127
    invoke-static {v8, v9}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@12a
    move-object v3, v7

    #@12b
    .line 643
    goto/16 :goto_33

    #@12d
    .line 646
    :cond_12d
    const/4 v7, 0x4

    #@12e
    new-array v3, v7, [Ljava/lang/String;

    #@130
    .line 649
    invoke-virtual {v2, v13, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@133
    move-result-object v7

    #@134
    aput-object v7, v3, v13

    #@136
    .line 650
    const/4 v7, 0x1

    #@137
    invoke-virtual {v2, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@13a
    move-result-object v8

    #@13b
    aput-object v8, v3, v7

    #@13d
    .line 651
    const/4 v7, 0x2

    #@13e
    invoke-virtual {v0}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    #@141
    move-result v8

    #@142
    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@145
    move-result-object v8

    #@146
    aput-object v8, v3, v7

    #@148
    .line 652
    invoke-virtual {v0}, Landroid/telephony/gsm/GsmCellLocation;->getLac()I

    #@14b
    move-result v7

    #@14c
    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@14f
    move-result-object v7

    #@150
    aput-object v7, v3, v11

    #@152
    goto/16 :goto_3a

    #@154
    .line 654
    .end local v0           #cl:Landroid/telephony/gsm/GsmCellLocation;
    .end local v2           #operator:Ljava/lang/String;
    .end local v6           #tm:Landroid/telephony/TelephonyManager;
    :cond_154
    const-string v7, "SysDispatcher"

    #@156
    const-string v8, "RAT is not valid"

    #@158
    invoke-static {v7, v8}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@15b
    goto/16 :goto_3a
.end method

.method public static getApn(I)Ljava/lang/String;
    .registers 3
    .parameter "apnType"

    #@0
    .prologue
    .line 558
    invoke-static {}, Lcom/lge/ims/DataConnectionManager;->getInstance()Lcom/lge/ims/DataConnectionManager;

    #@3
    move-result-object v0

    #@4
    .line 560
    .local v0, dcm:Lcom/lge/ims/DataConnectionManager;
    if-eqz v0, :cond_b

    #@6
    .line 561
    invoke-virtual {v0, p0}, Lcom/lge/ims/DataConnectionManager;->getApn(I)Ljava/lang/String;

    #@9
    move-result-object v1

    #@a
    .line 563
    :goto_a
    return-object v1

    #@b
    :cond_b
    const/4 v1, 0x0

    #@c
    goto :goto_a
.end method

.method public static getCallState()I
    .registers 1

    #@0
    .prologue
    .line 229
    invoke-static {}, Lcom/lge/ims/PhoneInfo;->getCallState()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static getDataActivity()I
    .registers 1

    #@0
    .prologue
    .line 266
    invoke-static {}, Lcom/lge/ims/PhoneInfo;->getDataActivity()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static getDataState(I)I
    .registers 3
    .parameter "apnType"

    #@0
    .prologue
    .line 278
    invoke-static {}, Lcom/lge/ims/DataConnectionManager;->getInstance()Lcom/lge/ims/DataConnectionManager;

    #@3
    move-result-object v0

    #@4
    .line 280
    .local v0, dcm:Lcom/lge/ims/DataConnectionManager;
    if-eqz v0, :cond_f

    #@6
    .line 281
    invoke-virtual {v0, p0}, Lcom/lge/ims/DataConnectionManager;->getDataState(I)I

    #@9
    move-result v1

    #@a
    invoke-static {v1}, Lcom/lge/ims/DataConnectionManager;->convertImsDataState(I)I

    #@d
    move-result v1

    #@e
    .line 283
    :goto_e
    return v1

    #@f
    :cond_f
    const/4 v1, 0x0

    #@10
    invoke-static {v1}, Lcom/lge/ims/DataConnectionManager;->convertImsDataState(I)I

    #@13
    move-result v1

    #@14
    goto :goto_e
.end method

.method public static getDeviceId()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 324
    invoke-static {}, Lcom/lge/ims/PhoneInfo;->getDeviceId()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public static getDeviceSoftwareVersion()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 328
    invoke-static {}, Lcom/lge/ims/PhoneInfo;->getDeviceSoftwareVersion()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public static getLocalAddress(II)Ljava/lang/String;
    .registers 4
    .parameter "apnType"
    .parameter "ipVersion"

    #@0
    .prologue
    .line 568
    invoke-static {}, Lcom/lge/ims/DataConnectionManager;->getInstance()Lcom/lge/ims/DataConnectionManager;

    #@3
    move-result-object v0

    #@4
    .line 570
    .local v0, dcm:Lcom/lge/ims/DataConnectionManager;
    if-eqz v0, :cond_b

    #@6
    .line 571
    invoke-virtual {v0, p0, p1}, Lcom/lge/ims/DataConnectionManager;->getLocalAddress(II)Ljava/lang/String;

    #@9
    move-result-object v1

    #@a
    .line 573
    :goto_a
    return-object v1

    #@b
    :cond_b
    const/4 v1, 0x0

    #@c
    goto :goto_a
.end method

.method public static getNetworkType()I
    .registers 1

    #@0
    .prologue
    .line 316
    invoke-static {}, Lcom/lge/ims/PhoneInfo;->getNetworkType()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static getPcscfAddress(II)[Ljava/lang/String;
    .registers 4
    .parameter "apnType"
    .parameter "ipVersion"

    #@0
    .prologue
    .line 578
    invoke-static {}, Lcom/lge/ims/DataConnectionManager;->getInstance()Lcom/lge/ims/DataConnectionManager;

    #@3
    move-result-object v0

    #@4
    .line 580
    .local v0, dcm:Lcom/lge/ims/DataConnectionManager;
    if-eqz v0, :cond_b

    #@6
    .line 581
    invoke-virtual {v0, p0, p1}, Lcom/lge/ims/DataConnectionManager;->getPcscfAddress(II)[Ljava/lang/String;

    #@9
    move-result-object v1

    #@a
    .line 583
    :goto_a
    return-object v1

    #@b
    :cond_b
    const/4 v1, 0x0

    #@c
    goto :goto_a
.end method

.method public static getPhoneNumber()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 320
    invoke-static {}, Lcom/lge/ims/PhoneInfo;->getPhoneNumberExcludingNationalPrefix()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public static getPhoneType()I
    .registers 1

    #@0
    .prologue
    .line 293
    invoke-static {}, Lcom/lge/ims/PhoneInfo;->getPhoneType()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static getServiceState()I
    .registers 5

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 241
    invoke-static {}, Lcom/lge/ims/PhoneStateTracker;->getInstance()Lcom/lge/ims/PhoneStateTracker;

    #@4
    move-result-object v1

    #@5
    .line 243
    .local v1, pst:Lcom/lge/ims/PhoneStateTracker;
    if-eqz v1, :cond_20

    #@7
    .line 244
    invoke-static {}, Lcom/lge/ims/DataConnectionManager;->getInstance()Lcom/lge/ims/DataConnectionManager;

    #@a
    move-result-object v0

    #@b
    .line 246
    .local v0, dcm:Lcom/lge/ims/DataConnectionManager;
    if-eqz v0, :cond_21

    #@d
    invoke-virtual {v0}, Lcom/lge/ims/DataConnectionManager;->isMultiplePdnSupported()Z

    #@10
    move-result v3

    #@11
    if-nez v3, :cond_21

    #@13
    invoke-virtual {v1}, Lcom/lge/ims/PhoneStateTracker;->isRoaming()Z

    #@16
    move-result v3

    #@17
    if-eqz v3, :cond_21

    #@19
    .line 247
    const-string v3, "SysDispatcher"

    #@1b
    const-string v4, "getServiceState :: network is roaming; Ims will transit to OUT_OF_SERVICE state"

    #@1d
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@20
    .line 253
    .end local v0           #dcm:Lcom/lge/ims/DataConnectionManager;
    :cond_20
    :goto_20
    return v2

    #@21
    .line 251
    .restart local v0       #dcm:Lcom/lge/ims/DataConnectionManager;
    :cond_21
    invoke-virtual {v1}, Lcom/lge/ims/PhoneStateTracker;->getDataServiceState()I

    #@24
    move-result v2

    #@25
    goto :goto_20
.end method

.method public static getSubscriberID()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 544
    invoke-static {}, Lcom/lge/ims/PhoneInfo;->getSubscriberId()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public static getWifiBSSID()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 683
    invoke-static {}, Lcom/lge/ims/SysDispatcher;->getWifiManager()Landroid/net/wifi/WifiManager;

    #@4
    .line 685
    sget-object v2, Lcom/lge/ims/SysDispatcher;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@6
    if-eqz v2, :cond_2e

    #@8
    .line 686
    sget-object v2, Lcom/lge/ims/SysDispatcher;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@a
    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    #@d
    move-result-object v1

    #@e
    .line 688
    .local v1, wifiInfo:Landroid/net/wifi/WifiInfo;
    if-nez v1, :cond_11

    #@10
    .line 699
    .end local v1           #wifiInfo:Landroid/net/wifi/WifiInfo;
    :goto_10
    return-object v0

    #@11
    .line 692
    .restart local v1       #wifiInfo:Landroid/net/wifi/WifiInfo;
    :cond_11
    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    .line 694
    .local v0, bssID:Ljava/lang/String;
    const-string v2, "SysDispatcher"

    #@17
    new-instance v3, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v4, "getWifiBSSID() : BSS ID is "

    #@1e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@2d
    goto :goto_10

    #@2e
    .line 698
    .end local v0           #bssID:Ljava/lang/String;
    .end local v1           #wifiInfo:Landroid/net/wifi/WifiInfo;
    :cond_2e
    const-string v2, "SysDispatcher"

    #@30
    const-string v3, "mWifiManager is null"

    #@32
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@35
    goto :goto_10
.end method

.method public static getWifiDetailedState()I
    .registers 1

    #@0
    .prologue
    .line 675
    sget-object v0, Lcom/lge/ims/ImsService;->sysMonitor:Lcom/lge/ims/SysMonitor;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 676
    sget-object v0, Lcom/lge/ims/ImsService;->sysMonitor:Lcom/lge/ims/SysMonitor;

    #@6
    iget v0, v0, Lcom/lge/ims/SysMonitor;->mWifiDetailedStatus:I

    #@8
    .line 678
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x4

    #@a
    goto :goto_8
.end method

.method private static getWifiManager()Landroid/net/wifi/WifiManager;
    .registers 2

    #@0
    .prologue
    .line 81
    sget-object v0, Lcom/lge/ims/SysDispatcher;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@2
    if-nez v0, :cond_10

    #@4
    .line 82
    sget-object v0, Lcom/lge/ims/SysDispatcher;->mContext:Landroid/content/Context;

    #@6
    const-string v1, "wifi"

    #@8
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@b
    move-result-object v0

    #@c
    check-cast v0, Landroid/net/wifi/WifiManager;

    #@e
    sput-object v0, Lcom/lge/ims/SysDispatcher;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@10
    .line 85
    :cond_10
    sget-object v0, Lcom/lge/ims/SysDispatcher;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@12
    return-object v0
.end method

.method public static getWifiState()I
    .registers 1

    #@0
    .prologue
    .line 667
    sget-object v0, Lcom/lge/ims/ImsService;->sysMonitor:Lcom/lge/ims/SysMonitor;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 668
    sget-object v0, Lcom/lge/ims/ImsService;->sysMonitor:Lcom/lge/ims/SysMonitor;

    #@6
    iget v0, v0, Lcom/lge/ims/SysMonitor;->mWifiState:I

    #@8
    .line 670
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x4

    #@a
    goto :goto_8
.end method

.method private initializeSystemProperties()V
    .registers 6

    #@0
    .prologue
    .line 1047
    const-string v0, "net.ims.reg"

    #@2
    const-string v1, "0"

    #@4
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1056
    const-string v0, "com.lge.ims.action.SVC_STATUS_IND"

    #@9
    const-string v1, "state"

    #@b
    const/4 v2, 0x2

    #@c
    const-string v3, "svcType"

    #@e
    const-string v4, "VoLTE"

    #@10
    invoke-static {v0, v1, v2, v3, v4}, Lcom/lge/ims/SysDispatcher;->sendIntent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    #@13
    .line 1057
    return-void
.end method

.method public static isBlockedNumber(Ljava/lang/String;)I
    .registers 24
    .parameter "number"

    #@0
    .prologue
    .line 332
    const-string v2, "SysDispatcher"

    #@2
    new-instance v6, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v8, " isBlockedNumber = "

    #@9
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v6

    #@d
    move-object/from16 v0, p0

    #@f
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v6

    #@13
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v6

    #@17
    invoke-static {v2, v6}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@1a
    .line 334
    const-string v2, "content://com.android.phone.CallSettingsProvider/callsettings"

    #@1c
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@1f
    move-result-object v3

    #@20
    .line 336
    .local v3, REJECTCONTENT_URI:Landroid/net/Uri;
    const-string v13, "_id"

    #@22
    .line 337
    .local v13, KEY_ID:Ljava/lang/String;
    const-string v14, "name"

    #@24
    .line 338
    .local v14, KEY_NAME:Ljava/lang/String;
    const-string v15, "value_str"

    #@26
    .line 339
    .local v15, KEY_NUMBER:Ljava/lang/String;
    const-string v16, "value_int"

    #@28
    .line 341
    .local v16, KEY_VALUE:Ljava/lang/String;
    const/4 v2, 0x4

    #@29
    new-array v4, v2, [Ljava/lang/String;

    #@2b
    const/4 v2, 0x0

    #@2c
    aput-object v13, v4, v2

    #@2e
    const/4 v2, 0x1

    #@2f
    aput-object v14, v4, v2

    #@31
    const/4 v2, 0x2

    #@32
    aput-object v15, v4, v2

    #@34
    const/4 v2, 0x3

    #@35
    aput-object v16, v4, v2

    #@37
    .line 348
    .local v4, CALLSETTINGS_PROJECTION:[Ljava/lang/String;
    const-string v12, "callRejectState"

    #@39
    .line 349
    .local v12, CALL_REJECT_OPTION_KEY:Ljava/lang/String;
    const/16 v20, 0x0

    #@3b
    .line 350
    .local v20, rejectCursor:Landroid/database/Cursor;
    const/16 v22, -0x1

    #@3d
    .line 352
    .local v22, value:I
    :try_start_3d
    new-instance v2, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v2

    #@46
    const-string v6, "="

    #@48
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v2

    #@4c
    const-string v6, "\'"

    #@4e
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v2

    #@52
    const-string v6, "callRejectState"

    #@54
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v2

    #@58
    const-string v6, "\'"

    #@5a
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v2

    #@5e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v5

    #@62
    .line 353
    .local v5, selection:Ljava/lang/String;
    sget-object v2, Lcom/lge/ims/SysDispatcher;->mContext:Landroid/content/Context;

    #@64
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@67
    move-result-object v2

    #@68
    const/4 v6, 0x0

    #@69
    const/4 v7, 0x0

    #@6a
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@6d
    move-result-object v20

    #@6e
    .line 354
    if-eqz v20, :cond_84

    #@70
    .line 355
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->moveToFirst()Z

    #@73
    move-result v2

    #@74
    if-eqz v2, :cond_84

    #@76
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->getCount()I

    #@79
    move-result v2

    #@7a
    const/4 v6, 0x1

    #@7b
    if-ne v2, v6, :cond_84

    #@7d
    .line 356
    const/4 v2, 0x3

    #@7e
    move-object/from16 v0, v20

    #@80
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I
    :try_end_83
    .catchall {:try_start_3d .. :try_end_83} :catchall_a9

    #@83
    move-result v22

    #@84
    .line 360
    :cond_84
    if-eqz v20, :cond_8b

    #@86
    .line 361
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    #@89
    .line 362
    const/16 v20, 0x0

    #@8b
    .line 366
    :cond_8b
    const-string v2, "SysDispatcher"

    #@8d
    new-instance v6, Ljava/lang/StringBuilder;

    #@8f
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@92
    const-string v8, "reject options = "

    #@94
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v6

    #@98
    move/from16 v0, v22

    #@9a
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v6

    #@9e
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a1
    move-result-object v6

    #@a2
    invoke-static {v2, v6}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@a5
    .line 371
    if-nez v22, :cond_b2

    #@a7
    .line 372
    const/4 v2, 0x0

    #@a8
    .line 411
    :cond_a8
    :goto_a8
    return v2

    #@a9
    .line 360
    .end local v5           #selection:Ljava/lang/String;
    :catchall_a9
    move-exception v2

    #@aa
    if-eqz v20, :cond_b1

    #@ac
    .line 361
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    #@af
    .line 362
    const/16 v20, 0x0

    #@b1
    :cond_b1
    throw v2

    #@b2
    .line 373
    .restart local v5       #selection:Ljava/lang/String;
    :cond_b2
    const/4 v2, 0x2

    #@b3
    move/from16 v0, v22

    #@b5
    if-ne v0, v2, :cond_b9

    #@b7
    .line 374
    const/4 v2, 0x1

    #@b8
    goto :goto_a8

    #@b9
    .line 377
    :cond_b9
    const-string v2, "content://com.android.phone.CallRejectProvider/callreject"

    #@bb
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@be
    move-result-object v7

    #@bf
    .line 378
    .local v7, CONTENT_URI:Landroid/net/Uri;
    sget-object v2, Lcom/lge/ims/SysDispatcher;->mContext:Landroid/content/Context;

    #@c1
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@c4
    move-result-object v6

    #@c5
    const/4 v8, 0x0

    #@c6
    const/4 v9, 0x0

    #@c7
    const/4 v10, 0x0

    #@c8
    const/4 v11, 0x0

    #@c9
    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@cc
    move-result-object v17

    #@cd
    .line 381
    .local v17, cursor:Landroid/database/Cursor;
    if-nez v17, :cond_df

    #@cf
    .line 382
    :try_start_cf
    const-string v2, "SysDispatcher"

    #@d1
    const-string v6, "cursor is null"

    #@d3
    invoke-static {v2, v6}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_d6
    .catchall {:try_start_cf .. :try_end_d6} :catchall_160
    .catch Ljava/lang/Exception; {:try_start_cf .. :try_end_d6} :catch_14e

    #@d6
    .line 383
    const/4 v2, 0x0

    #@d7
    .line 405
    if-eqz v17, :cond_a8

    #@d9
    .line 406
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    #@dc
    .line 407
    const/16 v17, 0x0

    #@de
    goto :goto_a8

    #@df
    .line 386
    :cond_df
    :try_start_df
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->getCount()I

    #@e2
    move-result v2

    #@e3
    if-nez v2, :cond_f5

    #@e5
    .line 387
    const-string v2, "SysDispatcher"

    #@e7
    const-string v6, "cursor count is 0"

    #@e9
    invoke-static {v2, v6}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_ec
    .catchall {:try_start_df .. :try_end_ec} :catchall_160
    .catch Ljava/lang/Exception; {:try_start_df .. :try_end_ec} :catch_14e

    #@ec
    .line 388
    const/4 v2, 0x0

    #@ed
    .line 405
    if-eqz v17, :cond_a8

    #@ef
    .line 406
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    #@f2
    .line 407
    const/16 v17, 0x0

    #@f4
    goto :goto_a8

    #@f5
    .line 391
    :cond_f5
    :try_start_f5
    const-string v2, "number"

    #@f7
    move-object/from16 v0, v17

    #@f9
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@fc
    move-result v19

    #@fd
    .line 393
    .local v19, phoneNumId:I
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToFirst()Z

    #@100
    .line 396
    :cond_100
    move-object/from16 v0, v17

    #@102
    move/from16 v1, v19

    #@104
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@107
    move-result-object v2

    #@108
    const-string v6, "-"

    #@10a
    const-string v8, ""

    #@10c
    invoke-virtual {v2, v6, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@10f
    move-result-object v21

    #@110
    .line 397
    .local v21, temp:Ljava/lang/String;
    move-object/from16 v0, p0

    #@112
    move-object/from16 v1, v21

    #@114
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@117
    move-result v2

    #@118
    if-eqz v2, :cond_13e

    #@11a
    .line 398
    const-string v2, "SysDispatcher"

    #@11c
    new-instance v6, Ljava/lang/StringBuilder;

    #@11e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@121
    move-object/from16 v0, p0

    #@123
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@126
    move-result-object v6

    #@127
    const-string v8, " is Blocked Number"

    #@129
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12c
    move-result-object v6

    #@12d
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@130
    move-result-object v6

    #@131
    invoke-static {v2, v6}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_134
    .catchall {:try_start_f5 .. :try_end_134} :catchall_160
    .catch Ljava/lang/Exception; {:try_start_f5 .. :try_end_134} :catch_14e

    #@134
    .line 399
    const/4 v2, 0x1

    #@135
    .line 405
    if-eqz v17, :cond_a8

    #@137
    .line 406
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    #@13a
    .line 407
    const/16 v17, 0x0

    #@13c
    goto/16 :goto_a8

    #@13e
    .line 401
    :cond_13e
    :try_start_13e
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_141
    .catchall {:try_start_13e .. :try_end_141} :catchall_160
    .catch Ljava/lang/Exception; {:try_start_13e .. :try_end_141} :catch_14e

    #@141
    move-result v2

    #@142
    if-nez v2, :cond_100

    #@144
    .line 405
    if-eqz v17, :cond_14b

    #@146
    .line 406
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    #@149
    .line 407
    const/16 v17, 0x0

    #@14b
    .line 411
    .end local v19           #phoneNumId:I
    .end local v21           #temp:Ljava/lang/String;
    :cond_14b
    :goto_14b
    const/4 v2, 0x0

    #@14c
    goto/16 :goto_a8

    #@14e
    .line 402
    :catch_14e
    move-exception v18

    #@14f
    .line 403
    .local v18, e:Ljava/lang/Exception;
    :try_start_14f
    const-string v2, "SysDispatcher"

    #@151
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@154
    move-result-object v6

    #@155
    invoke-static {v2, v6}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_158
    .catchall {:try_start_14f .. :try_end_158} :catchall_160

    #@158
    .line 405
    if-eqz v17, :cond_14b

    #@15a
    .line 406
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    #@15d
    .line 407
    const/16 v17, 0x0

    #@15f
    goto :goto_14b

    #@160
    .line 405
    .end local v18           #e:Ljava/lang/Exception;
    :catchall_160
    move-exception v2

    #@161
    if-eqz v17, :cond_168

    #@163
    .line 406
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    #@166
    .line 407
    const/16 v17, 0x0

    #@168
    :cond_168
    throw v2
.end method

.method public static qureyContactsWidthCondition(Landroid/content/Context;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 11
    .parameter "context"
    .parameter "condition"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 1003
    const/4 v6, 0x0

    #@2
    .line 1006
    .local v6, cursor:Landroid/database/Cursor;
    :try_start_2
    sget-object v2, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    #@4
    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v3

    #@8
    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@b
    move-result-object v1

    #@c
    .line 1007
    .local v1, uri:Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@f
    move-result-object v0

    #@10
    .line 1008
    .local v0, cr:Landroid/content/ContentResolver;
    sget-object v2, Lcom/lge/ims/SysDispatcher;->PHONELOOKUP_PROJECTION:[Ljava/lang/String;

    #@12
    const/4 v3, 0x0

    #@13
    const/4 v4, 0x0

    #@14
    const/4 v5, 0x0

    #@15
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_18} :catch_1b

    #@18
    move-result-object v6

    #@19
    move-object v2, v6

    #@1a
    .line 1015
    .end local v0           #cr:Landroid/content/ContentResolver;
    .end local v1           #uri:Landroid/net/Uri;
    :goto_1a
    return-object v2

    #@1b
    .line 1014
    :catch_1b
    move-exception v7

    #@1c
    .local v7, e:Ljava/lang/Exception;
    move-object v2, v8

    #@1d
    .line 1015
    goto :goto_1a
.end method

.method public static removeHyphen(Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "phoneNumber"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1025
    if-nez p0, :cond_4

    #@3
    .line 1042
    :cond_3
    :goto_3
    return-object v4

    #@4
    .line 1029
    :cond_4
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@7
    move-result v3

    #@8
    .line 1030
    .local v3, length:I
    new-instance v0, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    #@d
    .line 1032
    .local v0, builder:Ljava/lang/StringBuilder;
    if-eqz v0, :cond_3

    #@f
    .line 1036
    const/4 v2, 0x0

    #@10
    .local v2, i:I
    :goto_10
    if-ge v2, v3, :cond_25

    #@12
    .line 1037
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    #@15
    move-result v1

    #@16
    .line 1038
    .local v1, character:C
    const-string v4, " ()-.+"

    #@18
    invoke-virtual {v4, v1}, Ljava/lang/String;->indexOf(I)I

    #@1b
    move-result v4

    #@1c
    const/4 v5, -0x1

    #@1d
    if-ne v4, v5, :cond_22

    #@1f
    .line 1039
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@22
    .line 1036
    :cond_22
    add-int/lit8 v2, v2, 0x1

    #@24
    goto :goto_10

    #@25
    .line 1042
    .end local v1           #character:C
    :cond_25
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v4

    #@29
    goto :goto_3
.end method

.method public static requestRouteToHostAddress(ILjava/lang/String;)I
    .registers 5
    .parameter "apnType"
    .parameter "hostAddress"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 548
    invoke-static {}, Lcom/lge/ims/DataConnectionManager;->getInstance()Lcom/lge/ims/DataConnectionManager;

    #@4
    move-result-object v0

    #@5
    .line 550
    .local v0, dcm:Lcom/lge/ims/DataConnectionManager;
    if-eqz v0, :cond_e

    #@7
    .line 551
    invoke-virtual {v0, p0, p1}, Lcom/lge/ims/DataConnectionManager;->requestRouteToHostAddress(ILjava/lang/String;)Z

    #@a
    move-result v2

    #@b
    if-eqz v2, :cond_e

    #@d
    const/4 v1, 0x1

    #@e
    .line 553
    :cond_e
    return v1
.end method

.method public static saveMissedCall(ILjava/lang/String;)I
    .registers 16
    .parameter "type"
    .parameter "number"

    #@0
    .prologue
    .line 415
    const/4 v8, 0x0

    #@1
    .line 416
    .local v8, name:Ljava/lang/String;
    const/4 v3, 0x2

    #@2
    new-array v2, v3, [Ljava/lang/String;

    #@4
    const/4 v3, 0x0

    #@5
    const-string v4, "display_name"

    #@7
    aput-object v4, v2, v3

    #@9
    const/4 v3, 0x1

    #@a
    const-string v4, "number"

    #@c
    aput-object v4, v2, v3

    #@e
    .line 421
    .local v2, PHONELOOKUP_PROJECTION:[Ljava/lang/String;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@11
    move-result v3

    #@12
    if-nez v3, :cond_be

    #@14
    .line 422
    invoke-static {p1}, Lcom/lge/ims/SysDispatcher;->removeHyphen(Ljava/lang/String;)Ljava/lang/String;

    #@17
    move-result-object p1

    #@18
    .line 424
    if-nez p1, :cond_1c

    #@1a
    .line 425
    const/4 v3, 0x0

    #@1b
    .line 477
    :goto_1b
    return v3

    #@1c
    .line 427
    :cond_1c
    const/4 v7, 0x0

    #@1d
    .line 430
    .local v7, cursor:Landroid/database/Cursor;
    :try_start_1d
    sget-object v3, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    #@1f
    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    #@22
    move-result-object v4

    #@23
    invoke-static {v3, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@26
    move-result-object v1

    #@27
    .line 431
    .local v1, uri:Landroid/net/Uri;
    sget-object v3, Lcom/lge/ims/SysDispatcher;->mContext:Landroid/content/Context;

    #@29
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2c
    move-result-object v0

    #@2d
    .line 432
    .local v0, cr:Landroid/content/ContentResolver;
    const/4 v3, 0x0

    #@2e
    const/4 v4, 0x0

    #@2f
    const/4 v5, 0x0

    #@30
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_33
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_33} :catch_c1

    #@33
    move-result-object v7

    #@34
    .line 436
    .end local v0           #cr:Landroid/content/ContentResolver;
    .end local v1           #uri:Landroid/net/Uri;
    :goto_34
    if-eqz v7, :cond_be

    #@36
    .line 438
    :try_start_36
    const-string v3, "anonymous"

    #@38
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3b
    move-result v3

    #@3c
    if-eqz v3, :cond_42

    #@3e
    .line 439
    const-string p1, "-2"

    #@40
    .line 440
    const-string v8, ""

    #@42
    .line 442
    :cond_42
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    #@45
    move-result v3

    #@46
    if-eqz v3, :cond_54

    #@48
    .line 443
    const/4 v3, 0x1

    #@49
    invoke-interface {v7, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@4c
    move-result-object v9

    #@4d
    .line 444
    .local v9, phoneNumber:Ljava/lang/String;
    invoke-static {v9}, Lcom/lge/ims/SysDispatcher;->removeHyphen(Ljava/lang/String;)Ljava/lang/String;
    :try_end_50
    .catchall {:try_start_36 .. :try_end_50} :catchall_b9
    .catch Ljava/lang/Exception; {:try_start_36 .. :try_end_50} :catch_b4

    #@50
    move-result-object v10

    #@51
    .line 446
    .local v10, removeHyphenPhoneNumer:Ljava/lang/String;
    if-nez v10, :cond_a6

    #@53
    .line 447
    const/4 v8, 0x0

    #@54
    .line 459
    .end local v9           #phoneNumber:Ljava/lang/String;
    .end local v10           #removeHyphenPhoneNumer:Ljava/lang/String;
    :cond_54
    :goto_54
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@57
    .line 462
    :goto_57
    new-instance v13, Landroid/content/ContentValues;

    #@59
    const/4 v3, 0x5

    #@5a
    invoke-direct {v13, v3}, Landroid/content/ContentValues;-><init>(I)V

    #@5d
    .line 463
    .local v13, values:Landroid/content/ContentValues;
    const-string v3, "number"

    #@5f
    invoke-virtual {v13, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@62
    .line 464
    const-string v3, "type"

    #@64
    const/4 v4, 0x7

    #@65
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@68
    move-result-object v4

    #@69
    invoke-virtual {v13, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@6c
    .line 465
    const-string v3, "date"

    #@6e
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@71
    move-result-wide v4

    #@72
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@75
    move-result-object v4

    #@76
    invoke-virtual {v13, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@79
    .line 466
    const-string v3, "duration"

    #@7b
    const-wide/16 v4, 0x0

    #@7d
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@80
    move-result-object v4

    #@81
    invoke-virtual {v13, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@84
    .line 467
    const-string v3, "new"

    #@86
    const/4 v4, 0x1

    #@87
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8a
    move-result-object v4

    #@8b
    invoke-virtual {v13, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@8e
    .line 468
    const-string v3, "name"

    #@90
    invoke-virtual {v13, v3, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@93
    .line 469
    const-string v3, "content://call_log"

    #@95
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@98
    move-result-object v6

    #@99
    .line 470
    .local v6, CONTENT_URI:Landroid/net/Uri;
    sget-object v3, Lcom/lge/ims/SysDispatcher;->mContext:Landroid/content/Context;

    #@9b
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@9e
    move-result-object v11

    #@9f
    .line 471
    .local v11, resolver:Landroid/content/ContentResolver;
    invoke-virtual {v11, v6, v13}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@a2
    move-result-object v12

    #@a3
    .line 472
    .local v12, result:Landroid/net/Uri;
    const/4 v3, 0x1

    #@a4
    goto/16 :goto_1b

    #@a6
    .line 449
    .end local v6           #CONTENT_URI:Landroid/net/Uri;
    .end local v11           #resolver:Landroid/content/ContentResolver;
    .end local v12           #result:Landroid/net/Uri;
    .end local v13           #values:Landroid/content/ContentValues;
    .restart local v9       #phoneNumber:Ljava/lang/String;
    .restart local v10       #removeHyphenPhoneNumer:Ljava/lang/String;
    :cond_a6
    :try_start_a6
    invoke-virtual {v10, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a9
    move-result v3

    #@aa
    if-eqz v3, :cond_b2

    #@ac
    .line 450
    const/4 v3, 0x0

    #@ad
    invoke-interface {v7, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_b0
    .catchall {:try_start_a6 .. :try_end_b0} :catchall_b9
    .catch Ljava/lang/Exception; {:try_start_a6 .. :try_end_b0} :catch_b4

    #@b0
    move-result-object v8

    #@b1
    goto :goto_54

    #@b2
    .line 452
    :cond_b2
    const/4 v8, 0x0

    #@b3
    goto :goto_54

    #@b4
    .line 456
    .end local v9           #phoneNumber:Ljava/lang/String;
    .end local v10           #removeHyphenPhoneNumer:Ljava/lang/String;
    :catch_b4
    move-exception v3

    #@b5
    .line 459
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@b8
    goto :goto_57

    #@b9
    :catchall_b9
    move-exception v3

    #@ba
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@bd
    throw v3

    #@be
    .line 477
    .end local v7           #cursor:Landroid/database/Cursor;
    :cond_be
    const/4 v3, 0x0

    #@bf
    goto/16 :goto_1b

    #@c1
    .line 433
    .restart local v7       #cursor:Landroid/database/Cursor;
    :catch_c1
    move-exception v3

    #@c2
    goto/16 :goto_34
.end method

.method public static saveRejectedCall(ILjava/lang/String;)V
    .registers 16
    .parameter "type"
    .parameter "number"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v13, 0x1

    #@2
    .line 481
    const/4 v8, 0x0

    #@3
    .line 482
    .local v8, name:Ljava/lang/String;
    const/4 v3, 0x2

    #@4
    new-array v2, v3, [Ljava/lang/String;

    #@6
    const-string v3, "display_name"

    #@8
    aput-object v3, v2, v4

    #@a
    const-string v3, "number"

    #@c
    aput-object v3, v2, v13

    #@e
    .line 487
    .local v2, PHONELOOKUP_PROJECTION:[Ljava/lang/String;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@11
    move-result v3

    #@12
    if-nez v3, :cond_1a

    #@14
    .line 488
    invoke-static {p1}, Lcom/lge/ims/SysDispatcher;->removeHyphen(Ljava/lang/String;)Ljava/lang/String;

    #@17
    move-result-object p1

    #@18
    .line 490
    if-nez p1, :cond_1b

    #@1a
    .line 541
    :cond_1a
    :goto_1a
    return-void

    #@1b
    .line 493
    :cond_1b
    const/4 v7, 0x0

    #@1c
    .line 496
    .local v7, cursor:Landroid/database/Cursor;
    :try_start_1c
    sget-object v3, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    #@1e
    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    #@21
    move-result-object v4

    #@22
    invoke-static {v3, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@25
    move-result-object v1

    #@26
    .line 497
    .local v1, uri:Landroid/net/Uri;
    sget-object v3, Lcom/lge/ims/SysDispatcher;->mContext:Landroid/content/Context;

    #@28
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2b
    move-result-object v0

    #@2c
    .line 498
    .local v0, cr:Landroid/content/ContentResolver;
    const/4 v3, 0x0

    #@2d
    const/4 v4, 0x0

    #@2e
    const/4 v5, 0x0

    #@2f
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_32
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_32} :catch_bb

    #@32
    move-result-object v7

    #@33
    .line 502
    .end local v0           #cr:Landroid/content/ContentResolver;
    .end local v1           #uri:Landroid/net/Uri;
    :goto_33
    if-eqz v7, :cond_1a

    #@35
    .line 504
    :try_start_35
    const-string v3, "anonymous"

    #@37
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3a
    move-result v3

    #@3b
    if-eqz v3, :cond_41

    #@3d
    .line 505
    const-string p1, "-2"

    #@3f
    .line 506
    const-string v8, ""

    #@41
    .line 508
    :cond_41
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    #@44
    move-result v3

    #@45
    if-eqz v3, :cond_53

    #@47
    .line 509
    const/4 v3, 0x1

    #@48
    invoke-interface {v7, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@4b
    move-result-object v9

    #@4c
    .line 510
    .local v9, phoneNumber:Ljava/lang/String;
    invoke-static {v9}, Lcom/lge/ims/SysDispatcher;->removeHyphen(Ljava/lang/String;)Ljava/lang/String;
    :try_end_4f
    .catchall {:try_start_35 .. :try_end_4f} :catchall_b6
    .catch Ljava/lang/Exception; {:try_start_35 .. :try_end_4f} :catch_b1

    #@4f
    move-result-object v10

    #@50
    .line 512
    .local v10, removeHyphenPhoneNumer:Ljava/lang/String;
    if-nez v10, :cond_a3

    #@52
    .line 513
    const/4 v8, 0x0

    #@53
    .line 525
    .end local v9           #phoneNumber:Ljava/lang/String;
    .end local v10           #removeHyphenPhoneNumer:Ljava/lang/String;
    :cond_53
    :goto_53
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@56
    .line 528
    :goto_56
    new-instance v12, Landroid/content/ContentValues;

    #@58
    const/4 v3, 0x5

    #@59
    invoke-direct {v12, v3}, Landroid/content/ContentValues;-><init>(I)V

    #@5c
    .line 529
    .local v12, values:Landroid/content/ContentValues;
    const-string v3, "number"

    #@5e
    invoke-virtual {v12, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@61
    .line 530
    const-string v3, "type"

    #@63
    const/16 v4, 0x8

    #@65
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@68
    move-result-object v4

    #@69
    invoke-virtual {v12, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@6c
    .line 531
    const-string v3, "date"

    #@6e
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@71
    move-result-wide v4

    #@72
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@75
    move-result-object v4

    #@76
    invoke-virtual {v12, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@79
    .line 532
    const-string v3, "duration"

    #@7b
    const-wide/16 v4, 0x0

    #@7d
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@80
    move-result-object v4

    #@81
    invoke-virtual {v12, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@84
    .line 533
    const-string v3, "new"

    #@86
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@89
    move-result-object v4

    #@8a
    invoke-virtual {v12, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@8d
    .line 534
    const-string v3, "name"

    #@8f
    invoke-virtual {v12, v3, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@92
    .line 535
    const-string v3, "content://call_log/calls"

    #@94
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@97
    move-result-object v6

    #@98
    .line 536
    .local v6, CONTENT_URI:Landroid/net/Uri;
    sget-object v3, Lcom/lge/ims/SysDispatcher;->mContext:Landroid/content/Context;

    #@9a
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@9d
    move-result-object v11

    #@9e
    .line 537
    .local v11, resolver:Landroid/content/ContentResolver;
    invoke-virtual {v11, v6, v12}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@a1
    goto/16 :goto_1a

    #@a3
    .line 515
    .end local v6           #CONTENT_URI:Landroid/net/Uri;
    .end local v11           #resolver:Landroid/content/ContentResolver;
    .end local v12           #values:Landroid/content/ContentValues;
    .restart local v9       #phoneNumber:Ljava/lang/String;
    .restart local v10       #removeHyphenPhoneNumer:Ljava/lang/String;
    :cond_a3
    :try_start_a3
    invoke-virtual {v10, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a6
    move-result v3

    #@a7
    if-eqz v3, :cond_af

    #@a9
    .line 516
    const/4 v3, 0x0

    #@aa
    invoke-interface {v7, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_ad
    .catchall {:try_start_a3 .. :try_end_ad} :catchall_b6
    .catch Ljava/lang/Exception; {:try_start_a3 .. :try_end_ad} :catch_b1

    #@ad
    move-result-object v8

    #@ae
    goto :goto_53

    #@af
    .line 518
    :cond_af
    const/4 v8, 0x0

    #@b0
    goto :goto_53

    #@b1
    .line 522
    .end local v9           #phoneNumber:Ljava/lang/String;
    .end local v10           #removeHyphenPhoneNumer:Ljava/lang/String;
    :catch_b1
    move-exception v3

    #@b2
    .line 525
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@b5
    goto :goto_56

    #@b6
    :catchall_b6
    move-exception v3

    #@b7
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@ba
    throw v3

    #@bb
    .line 499
    :catch_bb
    move-exception v3

    #@bc
    goto/16 :goto_33
.end method

.method public static searchContactsByNumber(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .registers 8
    .parameter "context"
    .parameter "inputPhoneNumber"

    #@0
    .prologue
    .line 962
    const/4 v1, 0x0

    #@1
    .line 964
    .local v1, name:Ljava/lang/String;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@4
    move-result v3

    #@5
    if-nez v3, :cond_34

    #@7
    .line 965
    invoke-static {p1}, Lcom/lge/ims/SysDispatcher;->removeHyphen(Ljava/lang/String;)Ljava/lang/String;

    #@a
    move-result-object p1

    #@b
    .line 967
    if-nez p1, :cond_f

    #@d
    .line 968
    const/4 v3, 0x0

    #@e
    .line 994
    :goto_e
    return-object v3

    #@f
    .line 971
    :cond_f
    invoke-static {p0, p1}, Lcom/lge/ims/SysDispatcher;->qureyContactsWidthCondition(Landroid/content/Context;Ljava/lang/String;)Landroid/database/Cursor;

    #@12
    move-result-object v0

    #@13
    .line 972
    .local v0, cursor:Landroid/database/Cursor;
    if-eqz v0, :cond_34

    #@15
    .line 974
    :try_start_15
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    #@18
    move-result v3

    #@19
    if-eqz v3, :cond_31

    #@1b
    .line 975
    const/4 v3, 0x1

    #@1c
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    .line 976
    .local v2, phoneNumber:Ljava/lang/String;
    invoke-static {v2}, Lcom/lge/ims/SysDispatcher;->removeHyphen(Ljava/lang/String;)Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    .line 977
    if-eqz v2, :cond_4e

    #@26
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@29
    move-result v3

    #@2a
    if-eqz v3, :cond_4e

    #@2c
    .line 978
    const/4 v3, 0x0

    #@2d
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_30
    .catchall {:try_start_15 .. :try_end_30} :catchall_55
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_30} :catch_50

    #@30
    move-result-object v1

    #@31
    .line 987
    .end local v2           #phoneNumber:Ljava/lang/String;
    :cond_31
    :goto_31
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    #@34
    .line 992
    .end local v0           #cursor:Landroid/database/Cursor;
    :cond_34
    :goto_34
    const-string v3, "SysDispatcher"

    #@36
    new-instance v4, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v5, "searchContactsByNumber :"

    #@3d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v4

    #@41
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v4

    #@45
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v4

    #@49
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@4c
    move-object v3, v1

    #@4d
    .line 994
    goto :goto_e

    #@4e
    .line 980
    .restart local v0       #cursor:Landroid/database/Cursor;
    .restart local v2       #phoneNumber:Ljava/lang/String;
    :cond_4e
    const/4 v1, 0x0

    #@4f
    goto :goto_31

    #@50
    .line 984
    .end local v2           #phoneNumber:Ljava/lang/String;
    :catch_50
    move-exception v3

    #@51
    .line 987
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    #@54
    goto :goto_34

    #@55
    :catchall_55
    move-exception v3

    #@56
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    #@59
    throw v3
.end method

.method public static sendEvent(III)V
    .registers 16
    .parameter "nEvent"
    .parameter "nWParam"
    .parameter "nLParam"

    #@0
    .prologue
    .line 721
    const-string v8, "SysDispatcher"

    #@2
    new-instance v9, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v10, "sendEvent() - Event ("

    #@9
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v9

    #@d
    invoke-virtual {v9, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v9

    #@11
    const-string v10, "), WP ("

    #@13
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v9

    #@17
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v9

    #@1b
    const-string v10, "), LP ("

    #@1d
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v9

    #@21
    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v9

    #@25
    const-string v10, ")"

    #@27
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v9

    #@2b
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v9

    #@2f
    invoke-static {v8, v9}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@32
    .line 723
    sparse-switch p0, :sswitch_data_1e2

    #@35
    .line 896
    const-string v8, "SysDispatcher"

    #@37
    const-string v9, "sendEvent() - Not Supported ..."

    #@39
    invoke-static {v8, v9}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@3c
    .line 899
    :cond_3c
    :goto_3c
    return-void

    #@3d
    .line 725
    :sswitch_3d
    sget-boolean v8, Lcom/lge/ims/SysDispatcher;->mNativeCompleted:Z

    #@3f
    if-nez v8, :cond_3c

    #@41
    .line 726
    const-string v8, "com.lge.ims.action.IMS_BOOT_COMPLETED"

    #@43
    invoke-static {v8}, Lcom/lge/ims/SysDispatcher;->sendIntent(Ljava/lang/String;)V

    #@46
    .line 727
    const/4 v8, 0x1

    #@47
    sput-boolean v8, Lcom/lge/ims/SysDispatcher;->mNativeCompleted:Z

    #@49
    goto :goto_3c

    #@4a
    .line 732
    :sswitch_4a
    const/4 v8, 0x1

    #@4b
    if-ne p2, v8, :cond_51

    #@4d
    .line 733
    invoke-static {}, Lcom/lge/ims/SysDispatcher;->enableWifiLock()V

    #@50
    goto :goto_3c

    #@51
    .line 735
    :cond_51
    invoke-static {}, Lcom/lge/ims/SysDispatcher;->disableWifiLock()V

    #@54
    goto :goto_3c

    #@55
    .line 740
    :sswitch_55
    const v8, 0xffff

    #@58
    and-int v1, p1, v8

    #@5a
    .line 742
    .local v1, appType:I
    const/16 v8, 0xa

    #@5c
    if-ne v1, v8, :cond_99

    #@5e
    .line 743
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getInstance()Lcom/lge/ims/SystemServiceManager;

    #@61
    move-result-object v6

    #@62
    .line 745
    .local v6, ssm:Lcom/lge/ims/SystemServiceManager;
    if-nez v6, :cond_6c

    #@64
    .line 746
    const-string v8, "SysDispatcher"

    #@66
    const-string v9, "SystemServiceManager is null"

    #@68
    invoke-static {v8, v9}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@6b
    goto :goto_3c

    #@6c
    .line 750
    :cond_6c
    const-string v8, "SysDispatcher"

    #@6e
    new-instance v9, Ljava/lang/StringBuilder;

    #@70
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@73
    const-string v10, "ImsRegState="

    #@75
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v9

    #@79
    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v9

    #@7d
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@80
    move-result-object v9

    #@81
    invoke-static {v8, v9}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@84
    .line 752
    const/4 v8, 0x1

    #@85
    if-ne p2, v8, :cond_90

    #@87
    .line 753
    const/4 v8, 0x4

    #@88
    const/4 v9, 0x1

    #@89
    const/4 v10, -0x1

    #@8a
    const-string v11, ""

    #@8c
    invoke-virtual {v6, v8, v9, v10, v11}, Lcom/lge/ims/SystemServiceManager;->setSysInfo(IIILjava/lang/String;)V

    #@8f
    goto :goto_3c

    #@90
    .line 755
    :cond_90
    const/4 v8, 0x4

    #@91
    const/4 v9, 0x0

    #@92
    const/4 v10, -0x1

    #@93
    const-string v11, ""

    #@95
    invoke-virtual {v6, v8, v9, v10, v11}, Lcom/lge/ims/SystemServiceManager;->setSysInfo(IIILjava/lang/String;)V

    #@98
    goto :goto_3c

    #@99
    .line 757
    .end local v6           #ssm:Lcom/lge/ims/SystemServiceManager;
    :cond_99
    const/16 v8, 0x8

    #@9b
    if-eq v1, v8, :cond_3c

    #@9d
    .line 759
    if-nez v1, :cond_d5

    #@9f
    .line 760
    const-string v8, "SysDispatcher"

    #@a1
    new-instance v9, Ljava/lang/StringBuilder;

    #@a3
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@a6
    const-string v10, "VoLTE :: reg_state="

    #@a8
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v9

    #@ac
    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@af
    move-result-object v9

    #@b0
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b3
    move-result-object v9

    #@b4
    invoke-static {v8, v9}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@b7
    .line 762
    invoke-static {}, Lcom/lge/ims/service/uc/UCCallManager;->getInstance()Lcom/lge/ims/service/uc/UCCallManager;

    #@ba
    move-result-object v7

    #@bb
    .line 764
    .local v7, ucm:Lcom/lge/ims/service/uc/UCCallManager;
    if-eqz v7, :cond_c0

    #@bd
    .line 765
    invoke-virtual {v7, p2}, Lcom/lge/ims/service/uc/UCCallManager;->updateVoLTEService(I)V

    #@c0
    .line 768
    :cond_c0
    invoke-static {}, Lcom/lge/ims/debug/DebugService;->getInstance()Lcom/lge/ims/debug/DebugService;

    #@c3
    move-result-object v2

    #@c4
    .line 770
    .local v2, ds:Lcom/lge/ims/debug/DebugService;
    if-eqz v2, :cond_3c

    #@c6
    .line 771
    const/4 v8, 0x1

    #@c7
    if-ne p2, v8, :cond_cf

    #@c9
    .line 772
    const/4 v8, 0x1

    #@ca
    invoke-virtual {v2, v8}, Lcom/lge/ims/debug/DebugService;->setVoLTERegState(Z)V

    #@cd
    goto/16 :goto_3c

    #@cf
    .line 774
    :cond_cf
    const/4 v8, 0x0

    #@d0
    invoke-virtual {v2, v8}, Lcom/lge/ims/debug/DebugService;->setVoLTERegState(Z)V

    #@d3
    goto/16 :goto_3c

    #@d5
    .line 778
    .end local v2           #ds:Lcom/lge/ims/debug/DebugService;
    .end local v7           #ucm:Lcom/lge/ims/service/uc/UCCallManager;
    :cond_d5
    invoke-static {}, Lcom/lge/ims/service/ServiceMngr;->getInstance()Lcom/lge/ims/service/ServiceMngr;

    #@d8
    move-result-object v5

    #@d9
    .line 780
    .local v5, serviceMngr:Lcom/lge/ims/service/ServiceMngr;
    if-eqz v5, :cond_3c

    #@db
    .line 781
    const/4 v4, 0x0

    #@dc
    .line 783
    .local v4, isOldRCSState:Z
    const/4 v8, 0x2

    #@dd
    invoke-virtual {v5, v8}, Lcom/lge/ims/service/ServiceMngr;->isLTEConnected(I)Z

    #@e0
    move-result v8

    #@e1
    if-nez v8, :cond_ea

    #@e3
    const/4 v8, 0x2

    #@e4
    invoke-virtual {v5, v8}, Lcom/lge/ims/service/ServiceMngr;->isWifiConnected(I)Z

    #@e7
    move-result v8

    #@e8
    if-eqz v8, :cond_eb

    #@ea
    .line 785
    :cond_ea
    const/4 v4, 0x1

    #@eb
    .line 788
    :cond_eb
    invoke-virtual {v5, p1, p2}, Lcom/lge/ims/service/ServiceMngr;->updateServiceState(II)V

    #@ee
    .line 790
    const/4 v3, 0x0

    #@ef
    .line 792
    .local v3, isNewRCSState:Z
    const/4 v8, 0x2

    #@f0
    invoke-virtual {v5, v8}, Lcom/lge/ims/service/ServiceMngr;->isLTEConnected(I)Z

    #@f3
    move-result v8

    #@f4
    if-nez v8, :cond_fd

    #@f6
    const/4 v8, 0x2

    #@f7
    invoke-virtual {v5, v8}, Lcom/lge/ims/service/ServiceMngr;->isWifiConnected(I)Z

    #@fa
    move-result v8

    #@fb
    if-eqz v8, :cond_fe

    #@fd
    .line 794
    :cond_fd
    const/4 v3, 0x1

    #@fe
    .line 797
    :cond_fe
    if-eq v4, v3, :cond_3c

    #@100
    .line 798
    if-eqz v3, :cond_110

    #@102
    .line 799
    const-string v8, "com.lge.ims.action.SVC_STATUS_IND"

    #@104
    const-string v9, "state"

    #@106
    const/4 v10, 0x1

    #@107
    const-string v11, "svcType"

    #@109
    const-string v12, "RCS"

    #@10b
    invoke-static {v8, v9, v10, v11, v12}, Lcom/lge/ims/SysDispatcher;->sendIntent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    #@10e
    goto/16 :goto_3c

    #@110
    .line 801
    :cond_110
    const-string v8, "com.lge.ims.action.SVC_STATUS_IND"

    #@112
    const-string v9, "state"

    #@114
    const/4 v10, 0x0

    #@115
    const-string v11, "svcType"

    #@117
    const-string v12, "RCS"

    #@119
    invoke-static {v8, v9, v10, v11, v12}, Lcom/lge/ims/SysDispatcher;->sendIntent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    #@11c
    goto/16 :goto_3c

    #@11e
    .line 809
    .end local v1           #appType:I
    .end local v3           #isNewRCSState:Z
    .end local v4           #isOldRCSState:Z
    .end local v5           #serviceMngr:Lcom/lge/ims/service/ServiceMngr;
    :sswitch_11e
    if-nez p1, :cond_150

    #@120
    .line 810
    const-string v8, "SysDispatcher"

    #@122
    new-instance v9, Ljava/lang/StringBuilder;

    #@124
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@127
    const-string v10, "VT_CALL_STARTED :: lParam="

    #@129
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12c
    move-result-object v9

    #@12d
    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@130
    move-result-object v9

    #@131
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@134
    move-result-object v9

    #@135
    invoke-static {v8, v9}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@138
    .line 813
    const-string v8, "com.lge.ims.action.VT_STARTED"

    #@13a
    invoke-static {v8}, Lcom/lge/ims/SysDispatcher;->sendIntent(Ljava/lang/String;)V

    #@13d
    .line 816
    const/4 v8, 0x1

    #@13e
    if-ne p2, v8, :cond_148

    #@140
    .line 817
    const/16 v8, 0xb

    #@142
    const/4 v9, 0x1

    #@143
    invoke-static {v8, v9}, Lcom/lge/ims/SysDispatcher;->setModemInfo(II)V

    #@146
    goto/16 :goto_3c

    #@148
    .line 819
    :cond_148
    const/16 v8, 0xb

    #@14a
    const/4 v9, 0x0

    #@14b
    invoke-static {v8, v9}, Lcom/lge/ims/SysDispatcher;->setModemInfo(II)V

    #@14e
    goto/16 :goto_3c

    #@150
    .line 821
    :cond_150
    const/4 v8, 0x1

    #@151
    if-ne p1, v8, :cond_3c

    #@153
    .line 823
    const-string v8, "com.lge.ims.action.VT_ENDED"

    #@155
    invoke-static {v8}, Lcom/lge/ims/SysDispatcher;->sendIntent(Ljava/lang/String;)V

    #@158
    .line 826
    const/16 v8, 0xb

    #@15a
    const/4 v9, 0x0

    #@15b
    invoke-static {v8, v9}, Lcom/lge/ims/SysDispatcher;->setModemInfo(II)V

    #@15e
    goto/16 :goto_3c

    #@160
    .line 831
    :sswitch_160
    const/4 v8, 0x1

    #@161
    if-ne p1, v8, :cond_171

    #@163
    .line 832
    const-string v8, "com.lge.ims.action.SVC_STATUS_IND"

    #@165
    const-string v9, "state"

    #@167
    const/4 v10, 0x1

    #@168
    const-string v11, "svcType"

    #@16a
    const-string v12, "VT"

    #@16c
    invoke-static {v8, v9, v10, v11, v12}, Lcom/lge/ims/SysDispatcher;->sendIntent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    #@16f
    goto/16 :goto_3c

    #@171
    .line 834
    :cond_171
    const-string v8, "com.lge.ims.action.SVC_STATUS_IND"

    #@173
    const-string v9, "state"

    #@175
    const/4 v10, 0x0

    #@176
    const-string v11, "svcType"

    #@178
    const-string v12, "VT"

    #@17a
    invoke-static {v8, v9, v10, v11, v12}, Lcom/lge/ims/SysDispatcher;->sendIntent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    #@17d
    goto/16 :goto_3c

    #@17f
    .line 839
    :sswitch_17f
    const-string v8, "com.lge.ims.action.SVC_STATUS_IND"

    #@181
    const-string v9, "state"

    #@183
    const-string v10, "svcType"

    #@185
    const-string v11, "VoLTE"

    #@187
    invoke-static {v8, v9, p1, v10, v11}, Lcom/lge/ims/SysDispatcher;->sendIntent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    #@18a
    goto/16 :goto_3c

    #@18c
    .line 850
    :sswitch_18c
    const/16 v8, 0xb

    #@18e
    invoke-static {v8, p1}, Lcom/lge/ims/SysDispatcher;->setModemInfo(II)V

    #@191
    goto/16 :goto_3c

    #@193
    .line 854
    :sswitch_193
    invoke-static {p2}, Lcom/lge/ims/ImsWakeLock;->acquire(I)V

    #@196
    goto/16 :goto_3c

    #@198
    .line 858
    :sswitch_198
    invoke-static {}, Lcom/lge/ims/Configuration;->isACOn()Z

    #@19b
    move-result v8

    #@19c
    if-eqz v8, :cond_3c

    #@19e
    .line 859
    invoke-static {}, Lcom/lge/ims/service/ac/ACService;->getInstance()Lcom/lge/ims/service/ac/ACService;

    #@1a1
    move-result-object v0

    #@1a2
    .line 861
    .local v0, acs:Lcom/lge/ims/service/ac/ACService;
    if-eqz v0, :cond_3c

    #@1a4
    .line 862
    invoke-virtual {v0}, Lcom/lge/ims/service/ac/ACService;->requestConfigurationRetrieval()V

    #@1a7
    goto/16 :goto_3c

    #@1a9
    .line 868
    .end local v0           #acs:Lcom/lge/ims/service/ac/ACService;
    :sswitch_1a9
    const-string v8, "SysDispatcher"

    #@1ab
    const-string v9, "CS_CALL_PROTECTION is ignored"

    #@1ad
    invoke-static {v8, v9}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@1b0
    goto/16 :goto_3c

    #@1b2
    .line 873
    :sswitch_1b2
    invoke-static {}, Lcom/lge/ims/debug/DebugService;->getInstance()Lcom/lge/ims/debug/DebugService;

    #@1b5
    move-result-object v2

    #@1b6
    .line 875
    .restart local v2       #ds:Lcom/lge/ims/debug/DebugService;
    if-eqz v2, :cond_3c

    #@1b8
    .line 876
    invoke-virtual {v2, p1, p2}, Lcom/lge/ims/debug/DebugService;->setDebugInfo(II)V

    #@1bb
    goto/16 :goto_3c

    #@1bd
    .line 882
    .end local v2           #ds:Lcom/lge/ims/debug/DebugService;
    :sswitch_1bd
    const/4 v8, 0x1

    #@1be
    if-ne p2, v8, :cond_1ca

    #@1c0
    .line 883
    sget-object v8, Lcom/lge/ims/SysDispatcher;->mSysDispatcherHandler:Lcom/lge/ims/SysDispatcher$SysDispatcherHandler;

    #@1c2
    const/4 v9, 0x2

    #@1c3
    const-wide/16 v10, 0xa

    #@1c5
    invoke-virtual {v8, v9, v10, v11}, Lcom/lge/ims/SysDispatcher$SysDispatcherHandler;->sendEmptyMessageDelayed(IJ)Z

    #@1c8
    goto/16 :goto_3c

    #@1ca
    .line 884
    :cond_1ca
    const/4 v8, 0x2

    #@1cb
    if-ne p2, v8, :cond_3c

    #@1cd
    .line 885
    sget-object v8, Lcom/lge/ims/SysDispatcher;->mSysDispatcherHandler:Lcom/lge/ims/SysDispatcher$SysDispatcherHandler;

    #@1cf
    const/4 v9, 0x3

    #@1d0
    const-wide/16 v10, 0xa

    #@1d2
    invoke-virtual {v8, v9, v10, v11}, Lcom/lge/ims/SysDispatcher$SysDispatcherHandler;->sendEmptyMessageDelayed(IJ)Z

    #@1d5
    .line 887
    invoke-static {}, Lcom/lge/ims/service/uc/UCCallManager;->getInstance()Lcom/lge/ims/service/uc/UCCallManager;

    #@1d8
    move-result-object v7

    #@1d9
    .line 889
    .restart local v7       #ucm:Lcom/lge/ims/service/uc/UCCallManager;
    if-eqz v7, :cond_3c

    #@1db
    .line 890
    const/4 v8, 0x2

    #@1dc
    invoke-virtual {v7, v8}, Lcom/lge/ims/service/uc/UCCallManager;->updateVTService(I)V

    #@1df
    goto/16 :goto_3c

    #@1e1
    .line 723
    nop

    #@1e2
    :sswitch_data_1e2
    .sparse-switch
        0x1 -> :sswitch_4a
        0x2 -> :sswitch_55
        0x4 -> :sswitch_11e
        0x5 -> :sswitch_160
        0x6 -> :sswitch_17f
        0x8 -> :sswitch_18c
        0x12 -> :sswitch_3d
        0x13 -> :sswitch_193
        0x70 -> :sswitch_198
        0x80 -> :sswitch_1bd
        0x100 -> :sswitch_1a9
        0x800 -> :sswitch_1b2
    .end sparse-switch
.end method

.method private static sendIntent(Ljava/lang/String;)V
    .registers 5
    .parameter "action"

    #@0
    .prologue
    .line 89
    const-string v1, "SysDispatcher"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "sendIntent ("

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    const-string v3, ")"

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v2

    #@1b
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@1e
    .line 91
    new-instance v0, Landroid/content/Intent;

    #@20
    invoke-direct {v0, p0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@23
    .line 93
    .local v0, intent:Landroid/content/Intent;
    if-nez v0, :cond_2d

    #@25
    .line 94
    const-string v1, "SysDispatcher"

    #@27
    const-string v2, "Intent is null"

    #@29
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@2c
    .line 101
    :goto_2c
    return-void

    #@2d
    .line 99
    :cond_2d
    const/16 v1, 0x20

    #@2f
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@32
    .line 100
    sget-object v1, Lcom/lge/ims/SysDispatcher;->mContext:Landroid/content/Context;

    #@34
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@37
    goto :goto_2c
.end method

.method private static sendIntent(Ljava/lang/String;Ljava/lang/String;I)V
    .registers 7
    .parameter "action"
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 104
    const-string v1, "SysDispatcher"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "sendIntent ("

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    const-string v3, "), name="

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    const-string v3, ", value="

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@2c
    .line 106
    new-instance v0, Landroid/content/Intent;

    #@2e
    invoke-direct {v0, p0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@31
    .line 108
    .local v0, intent:Landroid/content/Intent;
    if-nez v0, :cond_3b

    #@33
    .line 109
    const-string v1, "SysDispatcher"

    #@35
    const-string v2, "Intent is null"

    #@37
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@3a
    .line 118
    :goto_3a
    return-void

    #@3b
    .line 114
    :cond_3b
    const/16 v1, 0x20

    #@3d
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@40
    .line 115
    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@43
    .line 117
    sget-object v1, Lcom/lge/ims/SysDispatcher;->mContext:Landroid/content/Context;

    #@45
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@48
    goto :goto_3a
.end method

.method private static sendIntent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "action"
    .parameter "name1"
    .parameter "value1"
    .parameter "name2"
    .parameter "value2"

    #@0
    .prologue
    .line 121
    const-string v1, "SysDispatcher"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "sendIntent ("

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    const-string v3, "), name1="

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    const-string v3, ", value1="

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    const-string v3, ", name2="

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    const-string v3, ", value2="

    #@31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v2

    #@3d
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@40
    .line 124
    new-instance v0, Landroid/content/Intent;

    #@42
    invoke-direct {v0, p0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@45
    .line 126
    .local v0, intent:Landroid/content/Intent;
    if-nez v0, :cond_4f

    #@47
    .line 127
    const-string v1, "SysDispatcher"

    #@49
    const-string v2, "Intent is null"

    #@4b
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@4e
    .line 137
    :goto_4e
    return-void

    #@4f
    .line 132
    :cond_4f
    const/16 v1, 0x20

    #@51
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@54
    .line 133
    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@57
    .line 134
    invoke-virtual {v0, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@5a
    .line 136
    sget-object v1, Lcom/lge/ims/SysDispatcher;->mContext:Landroid/content/Context;

    #@5c
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@5f
    goto :goto_4e
.end method

.method public static setModemInfo(II)V
    .registers 6
    .parameter "item"
    .parameter "data"

    #@0
    .prologue
    .line 922
    packed-switch p0, :pswitch_data_32

    #@3
    .line 926
    const-string v1, "SysDispatcher"

    #@5
    new-instance v2, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v3, "Unknown modem info :: item="

    #@c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v2

    #@18
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@1b
    .line 938
    :goto_1b
    return-void

    #@1c
    .line 930
    :pswitch_1c
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getInstance()Lcom/lge/ims/SystemServiceManager;

    #@1f
    move-result-object v0

    #@20
    .line 932
    .local v0, ssm:Lcom/lge/ims/SystemServiceManager;
    if-nez v0, :cond_2a

    #@22
    .line 933
    const-string v1, "SysDispatcher"

    #@24
    const-string v2, "SystemServiceManager is null"

    #@26
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@29
    goto :goto_1b

    #@2a
    .line 937
    :cond_2a
    const/4 v1, 0x1

    #@2b
    const-string v2, ""

    #@2d
    invoke-virtual {v0, v1, p0, p1, v2}, Lcom/lge/ims/SystemServiceManager;->setSysInfo(IIILjava/lang/String;)V

    #@30
    goto :goto_1b

    #@31
    .line 922
    nop

    #@32
    :pswitch_data_32
    .packed-switch 0xb
        :pswitch_1c
    .end packed-switch
.end method

.method public static startAlarmTimer(II)V
    .registers 5
    .parameter "timerId"
    .parameter "duration"

    #@0
    .prologue
    .line 197
    invoke-static {}, Lcom/lge/ims/AlarmTimerManager;->getInstance()Lcom/lge/ims/AlarmTimerManager;

    #@3
    move-result-object v0

    #@4
    .line 199
    .local v0, atm:Lcom/lge/ims/AlarmTimerManager;
    if-nez v0, :cond_e

    #@6
    .line 200
    const-string v1, "SysDispatcher"

    #@8
    const-string v2, "AlarmTimerManager is null"

    #@a
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    .line 205
    :goto_d
    return-void

    #@e
    .line 204
    :cond_e
    invoke-virtual {v0, p0, p1}, Lcom/lge/ims/AlarmTimerManager;->startNativeTimer(II)V

    #@11
    goto :goto_d
.end method

.method public static stopAlarmTimer(I)V
    .registers 4
    .parameter "timerId"

    #@0
    .prologue
    .line 208
    invoke-static {}, Lcom/lge/ims/AlarmTimerManager;->getInstance()Lcom/lge/ims/AlarmTimerManager;

    #@3
    move-result-object v0

    #@4
    .line 210
    .local v0, atm:Lcom/lge/ims/AlarmTimerManager;
    if-nez v0, :cond_e

    #@6
    .line 211
    const-string v1, "SysDispatcher"

    #@8
    const-string v2, "AlarmTimerManager is null"

    #@a
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    .line 216
    :goto_d
    return-void

    #@e
    .line 215
    :cond_e
    invoke-virtual {v0, p0}, Lcom/lge/ims/AlarmTimerManager;->stopNativeTimer(I)V

    #@11
    goto :goto_d
.end method


# virtual methods
.method public setMissedCall(ILjava/lang/String;Ljava/lang/String;)V
    .registers 14
    .parameter "callType"
    .parameter "displayName"
    .parameter "Originator"

    #@0
    .prologue
    .line 151
    const-string v7, "SysDispatcher"

    #@2
    new-instance v8, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v9, "setMissedCall. callType:"

    #@9
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v8

    #@d
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v8

    #@11
    const-string v9, " DisplayName:"

    #@13
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v8

    #@17
    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v8

    #@1b
    const-string v9, " Originator:"

    #@1d
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v8

    #@21
    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v8

    #@25
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v8

    #@29
    invoke-static {v7, v8}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@2c
    .line 153
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@2f
    move-result-wide v1

    #@30
    .line 154
    .local v1, currentTime:J
    sget-object v7, Lcom/lge/ims/SysDispatcher;->mContext:Landroid/content/Context;

    #@32
    invoke-static {v7, p3}, Lcom/lge/ims/SysDispatcher;->searchContactsByNumber(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    #@35
    move-result-object v3

    #@36
    .line 156
    .local v3, name:Ljava/lang/String;
    new-instance v6, Landroid/content/ContentValues;

    #@38
    const/4 v7, 0x5

    #@39
    invoke-direct {v6, v7}, Landroid/content/ContentValues;-><init>(I)V

    #@3c
    .line 158
    .local v6, values:Landroid/content/ContentValues;
    const-string v7, "number"

    #@3e
    invoke-virtual {v6, v7, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@41
    .line 159
    const-string v7, "type"

    #@43
    const/4 v8, 0x7

    #@44
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@47
    move-result-object v8

    #@48
    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@4b
    .line 160
    const-string v7, "date"

    #@4d
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@50
    move-result-object v8

    #@51
    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@54
    .line 161
    const-string v7, "duration"

    #@56
    const-wide/16 v8, 0x0

    #@58
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@5b
    move-result-object v8

    #@5c
    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@5f
    .line 162
    const-string v7, "new"

    #@61
    const/4 v8, 0x1

    #@62
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@65
    move-result-object v8

    #@66
    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@69
    .line 163
    const-string v7, "name"

    #@6b
    invoke-virtual {v6, v7, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@6e
    .line 165
    sget-object v7, Lcom/lge/ims/SysDispatcher;->mContext:Landroid/content/Context;

    #@70
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@73
    move-result-object v4

    #@74
    .line 167
    .local v4, resolver:Landroid/content/ContentResolver;
    const-string v7, "content://call_log"

    #@76
    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@79
    move-result-object v0

    #@7a
    .line 168
    .local v0, CONTENT_URI:Landroid/net/Uri;
    invoke-virtual {v4, v0, v6}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@7d
    move-result-object v5

    #@7e
    .line 169
    .local v5, result:Landroid/net/Uri;
    return-void
.end method

.method public setRejectedCall(ILjava/lang/String;Ljava/lang/String;)V
    .registers 15
    .parameter "callType"
    .parameter "displayName"
    .parameter "Originator"

    #@0
    .prologue
    const/4 v10, 0x1

    #@1
    .line 172
    const-string v7, "SysDispatcher"

    #@3
    new-instance v8, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v9, "setRejectedCall. callType:"

    #@a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v8

    #@e
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v8

    #@12
    const-string v9, " DisplayName:"

    #@14
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v8

    #@18
    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v8

    #@1c
    const-string v9, " Originator:"

    #@1e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v8

    #@22
    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v8

    #@26
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v8

    #@2a
    invoke-static {v7, v8}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@2d
    .line 174
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@30
    move-result-wide v1

    #@31
    .line 175
    .local v1, currentTime:J
    sget-object v7, Lcom/lge/ims/SysDispatcher;->mContext:Landroid/content/Context;

    #@33
    invoke-static {v7, p3}, Lcom/lge/ims/SysDispatcher;->searchContactsByNumber(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    #@36
    move-result-object v3

    #@37
    .line 177
    .local v3, name:Ljava/lang/String;
    new-instance v6, Landroid/content/ContentValues;

    #@39
    const/4 v7, 0x5

    #@3a
    invoke-direct {v6, v7}, Landroid/content/ContentValues;-><init>(I)V

    #@3d
    .line 179
    .local v6, values:Landroid/content/ContentValues;
    const-string v7, "number"

    #@3f
    invoke-virtual {v6, v7, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@42
    .line 180
    const-string v7, "type"

    #@44
    const/16 v8, 0x8

    #@46
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@49
    move-result-object v8

    #@4a
    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@4d
    .line 181
    const-string v7, "date"

    #@4f
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@52
    move-result-object v8

    #@53
    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@56
    .line 182
    const-string v7, "duration"

    #@58
    const-wide/16 v8, 0x0

    #@5a
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@5d
    move-result-object v8

    #@5e
    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@61
    .line 183
    const-string v7, "new"

    #@63
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@66
    move-result-object v8

    #@67
    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@6a
    .line 184
    const-string v7, "name"

    #@6c
    invoke-virtual {v6, v7, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@6f
    .line 186
    sget-object v7, Lcom/lge/ims/SysDispatcher;->mContext:Landroid/content/Context;

    #@71
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@74
    move-result-object v4

    #@75
    .line 188
    .local v4, resolver:Landroid/content/ContentResolver;
    const-string v7, "content://call_log"

    #@77
    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@7a
    move-result-object v0

    #@7b
    .line 189
    .local v0, CONTENT_URI:Landroid/net/Uri;
    invoke-virtual {v4, v0, v6}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@7e
    move-result-object v5

    #@7f
    .line 191
    .local v5, result:Landroid/net/Uri;
    const-string v7, "SysDispatcher"

    #@81
    const-string v8, "setRejectedCall. string"

    #@83
    invoke-static {v7, v8}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@86
    .line 193
    sget-object v7, Lcom/lge/ims/SysDispatcher;->mSysDispatcherHandler:Lcom/lge/ims/SysDispatcher$SysDispatcherHandler;

    #@88
    const-wide/16 v8, 0x5

    #@8a
    invoke-virtual {v7, v10, v8, v9}, Lcom/lge/ims/SysDispatcher$SysDispatcherHandler;->sendEmptyMessageDelayed(IJ)Z

    #@8d
    .line 194
    return-void
.end method
