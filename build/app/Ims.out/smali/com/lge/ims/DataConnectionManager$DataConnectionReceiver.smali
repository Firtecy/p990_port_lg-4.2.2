.class final Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DataConnectionManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/DataConnectionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "DataConnectionReceiver"
.end annotation


# instance fields
.field private mIntentFilter:Landroid/content/IntentFilter;

.field final synthetic this$0:Lcom/lge/ims/DataConnectionManager;


# direct methods
.method public constructor <init>(Lcom/lge/ims/DataConnectionManager;)V
    .registers 4
    .parameter

    #@0
    .prologue
    .line 1357
    iput-object p1, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    .line 1355
    new-instance v0, Landroid/content/IntentFilter;

    #@7
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@a
    iput-object v0, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;->mIntentFilter:Landroid/content/IntentFilter;

    #@c
    .line 1358
    iget-object v0, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;->mIntentFilter:Landroid/content/IntentFilter;

    #@e
    if-eqz v0, :cond_24

    #@10
    .line 1359
    iget-object v0, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;->mIntentFilter:Landroid/content/IntentFilter;

    #@12
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    #@14
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@17
    .line 1361
    invoke-static {}, Lcom/lge/ims/Configuration;->isEmergencySupported()Z

    #@1a
    move-result v0

    #@1b
    if-eqz v0, :cond_24

    #@1d
    .line 1363
    iget-object v0, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;->mIntentFilter:Landroid/content/IntentFilter;

    #@1f
    const-string v1, "lge.intent.action.DATA_EMERGENCY_FAILED"

    #@21
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@24
    .line 1366
    :cond_24
    return-void
.end method


# virtual methods
.method public getFilter()Landroid/content/IntentFilter;
    .registers 2

    #@0
    .prologue
    .line 1369
    iget-object v0, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;->mIntentFilter:Landroid/content/IntentFilter;

    #@2
    return-object v0
.end method

.method public declared-synchronized onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 16
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/16 v12, 0x17

    #@2
    const/16 v11, 0xb

    #@4
    const/4 v10, 0x1

    #@5
    const/4 v9, 0x2

    #@6
    .line 1374
    monitor-enter p0

    #@7
    :try_start_7
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    .line 1376
    .local v0, action:Ljava/lang/String;
    const-string v6, "DataConnectionManager"

    #@d
    new-instance v7, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v8, "DataConnectionReceiver - "

    #@14
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v7

    #@18
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v7

    #@1c
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v7

    #@20
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@23
    .line 1378
    const-string v6, "android.net.conn.CONNECTIVITY_CHANGE"

    #@25
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28
    move-result v6

    #@29
    if-eqz v6, :cond_237

    #@2b
    .line 1379
    const-string v6, "networkInfo"

    #@2d
    invoke-virtual {p2, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@30
    move-result-object v3

    #@31
    check-cast v3, Landroid/net/NetworkInfo;

    #@33
    .line 1381
    .local v3, netInfo:Landroid/net/NetworkInfo;
    if-nez v3, :cond_3e

    #@35
    .line 1382
    const-string v6, "DataConnectionManager"

    #@37
    const-string v7, "NetworkInfo is null"

    #@39
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3c
    .catchall {:try_start_7 .. :try_end_3c} :catchall_d1

    #@3c
    .line 1493
    .end local v3           #netInfo:Landroid/net/NetworkInfo;
    :cond_3c
    :goto_3c
    monitor-exit p0

    #@3d
    return-void

    #@3e
    .line 1386
    .restart local v3       #netInfo:Landroid/net/NetworkInfo;
    :cond_3e
    :try_start_3e
    const-string v6, "DataConnectionManager"

    #@40
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->toString()Ljava/lang/String;

    #@43
    move-result-object v7

    #@44
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@47
    .line 1388
    iget-object v6, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@49
    invoke-virtual {v6}, Lcom/lge/ims/DataConnectionManager;->isMultiplePdnSupported()Z

    #@4c
    move-result v6

    #@4d
    if-eqz v6, :cond_108

    #@4f
    .line 1389
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getType()I

    #@52
    move-result v6

    #@53
    if-ne v6, v12, :cond_d4

    #@55
    const/16 v6, 0x9

    #@57
    invoke-static {v6}, Lcom/lge/ims/DataConnectionManager;->getApnName(I)Ljava/lang/String;

    #@5a
    move-result-object v6

    #@5b
    invoke-static {v6}, Lcom/lge/ims/Configuration;->isApnSupported(Ljava/lang/String;)Z

    #@5e
    move-result v6

    #@5f
    if-nez v6, :cond_d4

    #@61
    .line 1391
    const-string v6, "DataConnectionManager"

    #@63
    const-string v7, "MPDN :: mobile_emergency is not supported; ignored"

    #@65
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@68
    .line 1418
    :cond_68
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    #@6b
    move-result-object v4

    #@6c
    .line 1419
    .local v4, niState:Landroid/net/NetworkInfo$State;
    const/4 v2, 0x0

    #@6d
    .line 1421
    .local v2, dataState:I
    sget-object v6, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    #@6f
    if-eq v4, v6, :cond_75

    #@71
    sget-object v6, Landroid/net/NetworkInfo$State;->SUSPENDED:Landroid/net/NetworkInfo$State;

    #@73
    if-ne v4, v6, :cond_76

    #@75
    .line 1422
    :cond_75
    const/4 v2, 0x2

    #@76
    .line 1425
    :cond_76
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getType()I

    #@79
    move-result v6

    #@7a
    if-nez v6, :cond_141

    #@7c
    .line 1426
    iget-object v6, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@7e
    invoke-static {v6}, Lcom/lge/ims/DataConnectionManager;->access$200(Lcom/lge/ims/DataConnectionManager;)I

    #@81
    move-result v6

    #@82
    if-eq v6, v2, :cond_3c

    #@84
    .line 1427
    const-string v6, "DataConnectionManager"

    #@86
    new-instance v7, Ljava/lang/StringBuilder;

    #@88
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@8b
    const-string v8, "Data state :: "

    #@8d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v7

    #@91
    iget-object v8, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@93
    invoke-static {v8}, Lcom/lge/ims/DataConnectionManager;->access$200(Lcom/lge/ims/DataConnectionManager;)I

    #@96
    move-result v8

    #@97
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v7

    #@9b
    const-string v8, " >> "

    #@9d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v7

    #@a1
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v7

    #@a5
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a8
    move-result-object v7

    #@a9
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@ac
    .line 1429
    iget-object v6, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@ae
    invoke-static {v6, v2}, Lcom/lge/ims/DataConnectionManager;->access$202(Lcom/lge/ims/DataConnectionManager;I)I

    #@b1
    .line 1430
    iget-object v6, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@b3
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getExtraInfo()Ljava/lang/String;

    #@b6
    move-result-object v7

    #@b7
    invoke-static {v6, v7}, Lcom/lge/ims/DataConnectionManager;->access$302(Lcom/lge/ims/DataConnectionManager;Ljava/lang/String;)Ljava/lang/String;

    #@ba
    .line 1433
    const/high16 v6, 0x2

    #@bc
    invoke-static {v2}, Lcom/lge/ims/DataConnectionManager;->convertImsDataState(I)I

    #@bf
    move-result v7

    #@c0
    or-int v5, v6, v7

    #@c2
    .line 1434
    .local v5, result:I
    iget-object v6, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@c4
    invoke-static {v6}, Lcom/lge/ims/DataConnectionManager;->access$400(Lcom/lge/ims/DataConnectionManager;)Landroid/os/RegistrantList;

    #@c7
    move-result-object v6

    #@c8
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@cb
    move-result-object v7

    #@cc
    invoke-virtual {v6, v7}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V
    :try_end_cf
    .catchall {:try_start_3e .. :try_end_cf} :catchall_d1

    #@cf
    goto/16 :goto_3c

    #@d1
    .line 1374
    .end local v0           #action:Ljava/lang/String;
    .end local v2           #dataState:I
    .end local v3           #netInfo:Landroid/net/NetworkInfo;
    .end local v4           #niState:Landroid/net/NetworkInfo$State;
    .end local v5           #result:I
    :catchall_d1
    move-exception v6

    #@d2
    monitor-exit p0

    #@d3
    throw v6

    #@d4
    .line 1392
    .restart local v0       #action:Ljava/lang/String;
    .restart local v3       #netInfo:Landroid/net/NetworkInfo;
    :cond_d4
    :try_start_d4
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getType()I

    #@d7
    move-result v6

    #@d8
    if-ne v6, v11, :cond_ee

    #@da
    const/4 v6, 0x1

    #@db
    invoke-static {v6}, Lcom/lge/ims/DataConnectionManager;->getApnName(I)Ljava/lang/String;

    #@de
    move-result-object v6

    #@df
    invoke-static {v6}, Lcom/lge/ims/Configuration;->isApnSupported(Ljava/lang/String;)Z

    #@e2
    move-result v6

    #@e3
    if-nez v6, :cond_ee

    #@e5
    .line 1394
    const-string v6, "DataConnectionManager"

    #@e7
    const-string v7, "MPDN :: mobile_ims is not supported; ignored"

    #@e9
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@ec
    goto/16 :goto_3c

    #@ee
    .line 1396
    :cond_ee
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getType()I

    #@f1
    move-result v6

    #@f2
    if-nez v6, :cond_68

    #@f4
    const/4 v6, 0x2

    #@f5
    invoke-static {v6}, Lcom/lge/ims/DataConnectionManager;->getApnName(I)Ljava/lang/String;

    #@f8
    move-result-object v6

    #@f9
    invoke-static {v6}, Lcom/lge/ims/Configuration;->isApnSupported(Ljava/lang/String;)Z

    #@fc
    move-result v6

    #@fd
    if-nez v6, :cond_68

    #@ff
    .line 1398
    const-string v6, "DataConnectionManager"

    #@101
    const-string v7, "MPDN :: mobile_internet is not supported; ignored"

    #@103
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@106
    goto/16 :goto_3c

    #@108
    .line 1402
    :cond_108
    iget-object v6, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@10a
    invoke-virtual {v6}, Lcom/lge/ims/DataConnectionManager;->isMultipleApnSupported()Z

    #@10d
    move-result v6

    #@10e
    if-eqz v6, :cond_132

    #@110
    .line 1403
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getType()I

    #@113
    move-result v6

    #@114
    if-ne v6, v10, :cond_68

    #@116
    .line 1404
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z

    #@119
    move-result v6

    #@11a
    if-eqz v6, :cond_68

    #@11c
    iget-object v6, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@11e
    invoke-virtual {v6}, Lcom/lge/ims/DataConnectionManager;->isWiFiConnected()Z

    #@121
    move-result v6

    #@122
    if-nez v6, :cond_68

    #@124
    .line 1405
    iget-object v6, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@126
    const/4 v7, 0x1

    #@127
    invoke-static {v6, v7}, Lcom/lge/ims/DataConnectionManager;->access$102(Lcom/lge/ims/DataConnectionManager;Z)Z

    #@12a
    .line 1406
    iget-object v6, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@12c
    const/4 v7, 0x1

    #@12d
    invoke-virtual {v6, v7}, Lcom/lge/ims/DataConnectionManager;->connect(I)Z

    #@130
    goto/16 :goto_3c

    #@132
    .line 1411
    :cond_132
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getType()I

    #@135
    move-result v6

    #@136
    if-eqz v6, :cond_68

    #@138
    .line 1412
    const-string v6, "DataConnectionManager"

    #@13a
    const-string v7, "SPDN :: no mobile type; ignored"

    #@13c
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@13f
    goto/16 :goto_3c

    #@141
    .line 1436
    .restart local v2       #dataState:I
    .restart local v4       #niState:Landroid/net/NetworkInfo$State;
    :cond_141
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getType()I

    #@144
    move-result v6

    #@145
    if-ne v6, v11, :cond_1b3

    #@147
    .line 1437
    iget-object v6, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@149
    invoke-static {v6}, Lcom/lge/ims/DataConnectionManager;->access$500(Lcom/lge/ims/DataConnectionManager;)I

    #@14c
    move-result v6

    #@14d
    if-eq v6, v9, :cond_15e

    #@14f
    .line 1438
    const-string v6, "DataConnectionManager"

    #@151
    const-string v7, "Ims apn is not requested, but ims apn state is changed"

    #@153
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@156
    .line 1440
    iget-object v6, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@158
    invoke-static {v6}, Lcom/lge/ims/DataConnectionManager;->access$600(Lcom/lge/ims/DataConnectionManager;)I

    #@15b
    move-result v6

    #@15c
    if-ne v6, v9, :cond_3c

    #@15e
    .line 1445
    :cond_15e
    iget-object v6, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@160
    invoke-static {v6}, Lcom/lge/ims/DataConnectionManager;->access$600(Lcom/lge/ims/DataConnectionManager;)I

    #@163
    move-result v6

    #@164
    if-eq v6, v2, :cond_3c

    #@166
    .line 1446
    const-string v6, "DataConnectionManager"

    #@168
    new-instance v7, Ljava/lang/StringBuilder;

    #@16a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@16d
    const-string v8, "Data state :: "

    #@16f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@172
    move-result-object v7

    #@173
    iget-object v8, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@175
    invoke-static {v8}, Lcom/lge/ims/DataConnectionManager;->access$600(Lcom/lge/ims/DataConnectionManager;)I

    #@178
    move-result v8

    #@179
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17c
    move-result-object v7

    #@17d
    const-string v8, " >> "

    #@17f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@182
    move-result-object v7

    #@183
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@186
    move-result-object v7

    #@187
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18a
    move-result-object v7

    #@18b
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@18e
    .line 1448
    iget-object v6, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@190
    invoke-static {v6, v2}, Lcom/lge/ims/DataConnectionManager;->access$602(Lcom/lge/ims/DataConnectionManager;I)I

    #@193
    .line 1449
    iget-object v6, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@195
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getExtraInfo()Ljava/lang/String;

    #@198
    move-result-object v7

    #@199
    invoke-static {v6, v7}, Lcom/lge/ims/DataConnectionManager;->access$702(Lcom/lge/ims/DataConnectionManager;Ljava/lang/String;)Ljava/lang/String;

    #@19c
    .line 1452
    const/high16 v6, 0x1

    #@19e
    invoke-static {v2}, Lcom/lge/ims/DataConnectionManager;->convertImsDataState(I)I

    #@1a1
    move-result v7

    #@1a2
    or-int v5, v6, v7

    #@1a4
    .line 1453
    .restart local v5       #result:I
    iget-object v6, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@1a6
    invoke-static {v6}, Lcom/lge/ims/DataConnectionManager;->access$400(Lcom/lge/ims/DataConnectionManager;)Landroid/os/RegistrantList;

    #@1a9
    move-result-object v6

    #@1aa
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1ad
    move-result-object v7

    #@1ae
    invoke-virtual {v6, v7}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@1b1
    goto/16 :goto_3c

    #@1b3
    .line 1455
    .end local v5           #result:I
    :cond_1b3
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getType()I

    #@1b6
    move-result v6

    #@1b7
    if-ne v6, v12, :cond_3c

    #@1b9
    .line 1456
    iget-object v6, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@1bb
    invoke-static {v6}, Lcom/lge/ims/DataConnectionManager;->access$800(Lcom/lge/ims/DataConnectionManager;)I

    #@1be
    move-result v6

    #@1bf
    if-eq v6, v9, :cond_1d0

    #@1c1
    .line 1457
    const-string v6, "DataConnectionManager"

    #@1c3
    const-string v7, "Emergency apn is not requested, but emergency apn state is changed"

    #@1c5
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@1c8
    .line 1459
    iget-object v6, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@1ca
    invoke-static {v6}, Lcom/lge/ims/DataConnectionManager;->access$900(Lcom/lge/ims/DataConnectionManager;)I

    #@1cd
    move-result v6

    #@1ce
    if-ne v6, v9, :cond_3c

    #@1d0
    .line 1464
    :cond_1d0
    iget-object v6, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@1d2
    invoke-static {v6}, Lcom/lge/ims/DataConnectionManager;->access$900(Lcom/lge/ims/DataConnectionManager;)I

    #@1d5
    move-result v6

    #@1d6
    if-eq v6, v2, :cond_3c

    #@1d8
    .line 1465
    const-string v6, "DataConnectionManager"

    #@1da
    new-instance v7, Ljava/lang/StringBuilder;

    #@1dc
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1df
    const-string v8, "Data state :: "

    #@1e1
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e4
    move-result-object v7

    #@1e5
    iget-object v8, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@1e7
    invoke-static {v8}, Lcom/lge/ims/DataConnectionManager;->access$900(Lcom/lge/ims/DataConnectionManager;)I

    #@1ea
    move-result v8

    #@1eb
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1ee
    move-result-object v7

    #@1ef
    const-string v8, " >> "

    #@1f1
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f4
    move-result-object v7

    #@1f5
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f8
    move-result-object v7

    #@1f9
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1fc
    move-result-object v7

    #@1fd
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@200
    .line 1467
    iget-object v6, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@202
    invoke-static {v6, v2}, Lcom/lge/ims/DataConnectionManager;->access$902(Lcom/lge/ims/DataConnectionManager;I)I

    #@205
    .line 1468
    iget-object v6, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@207
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getExtraInfo()Ljava/lang/String;

    #@20a
    move-result-object v7

    #@20b
    invoke-static {v6, v7}, Lcom/lge/ims/DataConnectionManager;->access$1002(Lcom/lge/ims/DataConnectionManager;Ljava/lang/String;)Ljava/lang/String;

    #@20e
    .line 1470
    iget-object v6, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@210
    invoke-static {v6}, Lcom/lge/ims/DataConnectionManager;->access$900(Lcom/lge/ims/DataConnectionManager;)I

    #@213
    move-result v6

    #@214
    if-nez v6, :cond_220

    #@216
    .line 1471
    iget-object v6, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@218
    const/4 v7, 0x0

    #@219
    invoke-static {v7}, Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;->fromInt(I)Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

    #@21c
    move-result-object v7

    #@21d
    invoke-static {v6, v7}, Lcom/lge/ims/DataConnectionManager;->access$1102(Lcom/lge/ims/DataConnectionManager;Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;)Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

    #@220
    .line 1475
    :cond_220
    const/high16 v6, 0x9

    #@222
    invoke-static {v2}, Lcom/lge/ims/DataConnectionManager;->convertImsDataState(I)I

    #@225
    move-result v7

    #@226
    or-int v5, v6, v7

    #@228
    .line 1476
    .restart local v5       #result:I
    iget-object v6, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@22a
    invoke-static {v6}, Lcom/lge/ims/DataConnectionManager;->access$400(Lcom/lge/ims/DataConnectionManager;)Landroid/os/RegistrantList;

    #@22d
    move-result-object v6

    #@22e
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@231
    move-result-object v7

    #@232
    invoke-virtual {v6, v7}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@235
    goto/16 :goto_3c

    #@237
    .line 1479
    .end local v2           #dataState:I
    .end local v3           #netInfo:Landroid/net/NetworkInfo;
    .end local v4           #niState:Landroid/net/NetworkInfo$State;
    .end local v5           #result:I
    :cond_237
    const-string v6, "lge.intent.action.DATA_EMERGENCY_FAILED"

    #@239
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23c
    move-result v6

    #@23d
    if-eqz v6, :cond_3c

    #@23f
    .line 1480
    iget-object v6, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@241
    invoke-static {v6}, Lcom/lge/ims/DataConnectionManager;->access$800(Lcom/lge/ims/DataConnectionManager;)I

    #@244
    move-result v6

    #@245
    if-eq v6, v9, :cond_250

    #@247
    .line 1481
    const-string v6, "DataConnectionManager"

    #@249
    const-string v7, "Emergency apn is not requested, but emergency apn state is changed"

    #@24b
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@24e
    goto/16 :goto_3c

    #@250
    .line 1485
    :cond_250
    const-string v6, "EMC_FailCause"

    #@252
    const/4 v7, 0x0

    #@253
    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@256
    move-result v1

    #@257
    .line 1487
    .local v1, code:I
    iget-object v6, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@259
    invoke-static {v1}, Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;->fromInt(I)Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

    #@25c
    move-result-object v7

    #@25d
    invoke-static {v6, v7}, Lcom/lge/ims/DataConnectionManager;->access$1102(Lcom/lge/ims/DataConnectionManager;Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;)Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

    #@260
    .line 1490
    const v5, 0x90002

    #@263
    .line 1491
    .restart local v5       #result:I
    iget-object v6, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionReceiver;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@265
    invoke-static {v6}, Lcom/lge/ims/DataConnectionManager;->access$400(Lcom/lge/ims/DataConnectionManager;)Landroid/os/RegistrantList;

    #@268
    move-result-object v6

    #@269
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@26c
    move-result-object v7

    #@26d
    invoke-virtual {v6, v7}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V
    :try_end_270
    .catchall {:try_start_d4 .. :try_end_270} :catchall_d1

    #@270
    goto/16 :goto_3c
.end method
