.class public final Lcom/lge/ims/debug/SipInfo;
.super Ljava/lang/Object;
.source "SipInfo.java"


# static fields
.field public static final DIR_IN:I = 0x1

.field public static final DIR_OUT:I = 0x0

.field public static final MSG_REQ:I = 0x0

.field public static final MSG_RSP:I = 0x1

.field public static final M_BYE:I = 0x1

.field public static final M_INVITE:I = 0x3

.field public static final M_MAX:I = 0xf

.field public static final M_REGISTER:I = 0x5


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 6
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static final getMethod(I)Ljava/lang/String;
    .registers 2
    .parameter "method"

    #@0
    .prologue
    .line 35
    packed-switch p0, :pswitch_data_10

    #@3
    .line 43
    :pswitch_3
    const-string v0, "Unknown"

    #@5
    :goto_5
    return-object v0

    #@6
    .line 37
    :pswitch_6
    const-string v0, "BYE"

    #@8
    goto :goto_5

    #@9
    .line 39
    :pswitch_9
    const-string v0, "INVITE"

    #@b
    goto :goto_5

    #@c
    .line 41
    :pswitch_c
    const-string v0, "REGISTER"

    #@e
    goto :goto_5

    #@f
    .line 35
    nop

    #@10
    :pswitch_data_10
    .packed-switch 0x1
        :pswitch_6
        :pswitch_3
        :pswitch_9
        :pswitch_3
        :pswitch_c
    .end packed-switch
.end method
