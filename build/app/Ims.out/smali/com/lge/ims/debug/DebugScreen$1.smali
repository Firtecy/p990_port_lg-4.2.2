.class Lcom/lge/ims/debug/DebugScreen$1;
.super Landroid/os/Handler;
.source "DebugScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/debug/DebugScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/debug/DebugScreen;


# direct methods
.method constructor <init>(Lcom/lge/ims/debug/DebugScreen;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 539
    iput-object p1, p0, Lcom/lge/ims/debug/DebugScreen$1;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    .line 542
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2
    check-cast v0, Landroid/os/Bundle;

    #@4
    .line 543
    .local v0, b:Landroid/os/Bundle;
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen$1;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@6
    invoke-static {v1}, Lcom/lge/ims/debug/DebugScreen;->access$1600(Lcom/lge/ims/debug/DebugScreen;)Landroid/widget/TextView;

    #@9
    move-result-object v1

    #@a
    const-string v2, "networkmode"

    #@c
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@13
    .line 544
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen$1;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@15
    invoke-static {v1}, Lcom/lge/ims/debug/DebugScreen;->access$1700(Lcom/lge/ims/debug/DebugScreen;)Landroid/widget/TextView;

    #@18
    move-result-object v1

    #@19
    const-string v2, "voltestatus"

    #@1b
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@22
    .line 545
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen$1;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@24
    invoke-static {v1}, Lcom/lge/ims/debug/DebugScreen;->access$1800(Lcom/lge/ims/debug/DebugScreen;)Landroid/widget/TextView;

    #@27
    move-result-object v1

    #@28
    const-string v2, "apcsvolte"

    #@2a
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@31
    .line 546
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen$1;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@33
    invoke-static {v1}, Lcom/lge/ims/debug/DebugScreen;->access$1900(Lcom/lge/ims/debug/DebugScreen;)Landroid/widget/TextView;

    #@36
    move-result-object v1

    #@37
    const-string v2, "apcsconfig"

    #@39
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@3c
    move-result-object v2

    #@3d
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@40
    .line 547
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen$1;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@42
    invoke-static {v1}, Lcom/lge/ims/debug/DebugScreen;->access$2000(Lcom/lge/ims/debug/DebugScreen;)Landroid/widget/TextView;

    #@45
    move-result-object v1

    #@46
    const-string v2, "sipregi"

    #@48
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@4b
    move-result-object v2

    #@4c
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@4f
    .line 548
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen$1;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@51
    invoke-static {v1}, Lcom/lge/ims/debug/DebugScreen;->access$2100(Lcom/lge/ims/debug/DebugScreen;)Landroid/widget/TextView;

    #@54
    move-result-object v1

    #@55
    const-string v2, "lastcallinfoup"

    #@57
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@5a
    move-result-object v2

    #@5b
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@5e
    .line 549
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen$1;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@60
    invoke-static {v1}, Lcom/lge/ims/debug/DebugScreen;->access$2200(Lcom/lge/ims/debug/DebugScreen;)Landroid/widget/TextView;

    #@63
    move-result-object v1

    #@64
    const-string v2, "lastcallinfodown"

    #@66
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@69
    move-result-object v2

    #@6a
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@6d
    .line 550
    return-void
.end method
