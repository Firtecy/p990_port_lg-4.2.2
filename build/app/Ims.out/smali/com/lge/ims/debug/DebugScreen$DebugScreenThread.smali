.class final Lcom/lge/ims/debug/DebugScreen$DebugScreenThread;
.super Ljava/lang/Thread;
.source "DebugScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/debug/DebugScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "DebugScreenThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/debug/DebugScreen;


# direct methods
.method public constructor <init>(Lcom/lge/ims/debug/DebugScreen;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 555
    iput-object p1, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenThread;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@2
    .line 556
    const-string v0, "DebugScreen"

    #@4
    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    #@7
    .line 557
    return-void
.end method


# virtual methods
.method public run()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 560
    invoke-static {}, Landroid/os/Looper;->prepare()V

    #@4
    .line 562
    const-string v0, "DebugScreen"

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "DebugScreenThread is running ... ("

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-static {}, Landroid/os/Process;->myTid()I

    #@14
    move-result v2

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    const-string v2, ")"

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@26
    .line 564
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenThread;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@28
    new-instance v1, Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;

    #@2a
    iget-object v2, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenThread;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@2c
    invoke-direct {v1, v2}, Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;-><init>(Lcom/lge/ims/debug/DebugScreen;)V

    #@2f
    invoke-static {v0, v1}, Lcom/lge/ims/debug/DebugScreen;->access$2302(Lcom/lge/ims/debug/DebugScreen;Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;)Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;

    #@32
    .line 566
    const-string v0, "DebugScreen"

    #@34
    const-string v1, "Register the Events."

    #@36
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@39
    .line 567
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenThread;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@3b
    invoke-static {v0}, Lcom/lge/ims/debug/DebugScreen;->access$2400(Lcom/lge/ims/debug/DebugScreen;)Lcom/lge/ims/PhoneStateTracker;

    #@3e
    move-result-object v0

    #@3f
    if-eqz v0, :cond_e9

    #@41
    .line 568
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenThread;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@43
    invoke-static {v0}, Lcom/lge/ims/debug/DebugScreen;->access$2400(Lcom/lge/ims/debug/DebugScreen;)Lcom/lge/ims/PhoneStateTracker;

    #@46
    move-result-object v0

    #@47
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenThread;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@49
    invoke-static {v1}, Lcom/lge/ims/debug/DebugScreen;->access$2300(Lcom/lge/ims/debug/DebugScreen;)Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;

    #@4c
    move-result-object v1

    #@4d
    const/16 v2, 0x65

    #@4f
    invoke-virtual {v0, v1, v2, v3}, Lcom/lge/ims/PhoneStateTracker;->registerForRATChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@52
    .line 569
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenThread;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@54
    invoke-static {v0}, Lcom/lge/ims/debug/DebugScreen;->access$2400(Lcom/lge/ims/debug/DebugScreen;)Lcom/lge/ims/PhoneStateTracker;

    #@57
    move-result-object v0

    #@58
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenThread;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@5a
    invoke-static {v1}, Lcom/lge/ims/debug/DebugScreen;->access$2300(Lcom/lge/ims/debug/DebugScreen;)Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;

    #@5d
    move-result-object v1

    #@5e
    const/16 v2, 0x66

    #@60
    invoke-virtual {v0, v1, v2, v3}, Lcom/lge/ims/PhoneStateTracker;->registerForAirplaneModeChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@63
    .line 574
    :goto_63
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenThread;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@65
    invoke-static {v0}, Lcom/lge/ims/debug/DebugScreen;->access$400(Lcom/lge/ims/debug/DebugScreen;)Lcom/lge/ims/service/ac/ACService;

    #@68
    move-result-object v0

    #@69
    if-eqz v0, :cond_f2

    #@6b
    .line 575
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenThread;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@6d
    invoke-static {v0}, Lcom/lge/ims/debug/DebugScreen;->access$400(Lcom/lge/ims/debug/DebugScreen;)Lcom/lge/ims/service/ac/ACService;

    #@70
    move-result-object v0

    #@71
    invoke-virtual {v0}, Lcom/lge/ims/service/ac/ACService;->isConfigurationAccessAllowed()Z

    #@74
    move-result v0

    #@75
    if-nez v0, :cond_88

    #@77
    .line 576
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenThread;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@79
    invoke-static {v0}, Lcom/lge/ims/debug/DebugScreen;->access$400(Lcom/lge/ims/debug/DebugScreen;)Lcom/lge/ims/service/ac/ACService;

    #@7c
    move-result-object v0

    #@7d
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenThread;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@7f
    invoke-static {v1}, Lcom/lge/ims/debug/DebugScreen;->access$2300(Lcom/lge/ims/debug/DebugScreen;)Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;

    #@82
    move-result-object v1

    #@83
    const/16 v2, 0x6c

    #@85
    invoke-virtual {v0, v1, v2, v3}, Lcom/lge/ims/service/ac/ACService;->registerForConfigurationAccessAllowed(Landroid/os/Handler;ILjava/lang/Object;)V

    #@88
    .line 578
    :cond_88
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenThread;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@8a
    invoke-static {v0}, Lcom/lge/ims/debug/DebugScreen;->access$400(Lcom/lge/ims/debug/DebugScreen;)Lcom/lge/ims/service/ac/ACService;

    #@8d
    move-result-object v0

    #@8e
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenThread;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@90
    invoke-static {v1}, Lcom/lge/ims/debug/DebugScreen;->access$2300(Lcom/lge/ims/debug/DebugScreen;)Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;

    #@93
    move-result-object v1

    #@94
    const/16 v2, 0x68

    #@96
    invoke-virtual {v0, v1, v2, v3}, Lcom/lge/ims/service/ac/ACService;->registerForConfigurationUpdateCompleted(Landroid/os/Handler;ILjava/lang/Object;)V

    #@99
    .line 579
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenThread;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@9b
    invoke-static {v0}, Lcom/lge/ims/debug/DebugScreen;->access$400(Lcom/lge/ims/debug/DebugScreen;)Lcom/lge/ims/service/ac/ACService;

    #@9e
    move-result-object v0

    #@9f
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenThread;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@a1
    invoke-static {v1}, Lcom/lge/ims/debug/DebugScreen;->access$2300(Lcom/lge/ims/debug/DebugScreen;)Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;

    #@a4
    move-result-object v1

    #@a5
    const/16 v2, 0x6b

    #@a7
    invoke-virtual {v0, v1, v2, v3}, Lcom/lge/ims/service/ac/ACService;->registerForServerInterworkingResult(Landroid/os/Handler;ILjava/lang/Object;)V

    #@aa
    .line 584
    :goto_aa
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenThread;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@ac
    invoke-static {v0}, Lcom/lge/ims/debug/DebugScreen;->access$100(Lcom/lge/ims/debug/DebugScreen;)Lcom/lge/ims/debug/DebugService;

    #@af
    move-result-object v0

    #@b0
    if-eqz v0, :cond_fa

    #@b2
    .line 585
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenThread;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@b4
    invoke-static {v0}, Lcom/lge/ims/debug/DebugScreen;->access$100(Lcom/lge/ims/debug/DebugScreen;)Lcom/lge/ims/debug/DebugService;

    #@b7
    move-result-object v0

    #@b8
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenThread;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@ba
    invoke-static {v1}, Lcom/lge/ims/debug/DebugScreen;->access$2300(Lcom/lge/ims/debug/DebugScreen;)Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;

    #@bd
    move-result-object v1

    #@be
    const/16 v2, 0x67

    #@c0
    invoke-virtual {v0, v1, v2, v3}, Lcom/lge/ims/debug/DebugService;->registerForVoLTERegStateUpdated(Landroid/os/Handler;ILjava/lang/Object;)V

    #@c3
    .line 586
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenThread;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@c5
    invoke-static {v0}, Lcom/lge/ims/debug/DebugScreen;->access$100(Lcom/lge/ims/debug/DebugScreen;)Lcom/lge/ims/debug/DebugService;

    #@c8
    move-result-object v0

    #@c9
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenThread;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@cb
    invoke-static {v1}, Lcom/lge/ims/debug/DebugScreen;->access$2300(Lcom/lge/ims/debug/DebugScreen;)Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;

    #@ce
    move-result-object v1

    #@cf
    const/16 v2, 0x69

    #@d1
    invoke-virtual {v0, v1, v2, v3}, Lcom/lge/ims/debug/DebugService;->registerForRegistrationUpdated(Landroid/os/Handler;ILjava/lang/Object;)V

    #@d4
    .line 587
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenThread;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@d6
    invoke-static {v0}, Lcom/lge/ims/debug/DebugScreen;->access$100(Lcom/lge/ims/debug/DebugScreen;)Lcom/lge/ims/debug/DebugService;

    #@d9
    move-result-object v0

    #@da
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenThread;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@dc
    invoke-static {v1}, Lcom/lge/ims/debug/DebugScreen;->access$2300(Lcom/lge/ims/debug/DebugScreen;)Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;

    #@df
    move-result-object v1

    #@e0
    const/16 v2, 0x6a

    #@e2
    invoke-virtual {v0, v1, v2, v3}, Lcom/lge/ims/debug/DebugService;->registerForCallMessageUpdated(Landroid/os/Handler;ILjava/lang/Object;)V

    #@e5
    .line 592
    :goto_e5
    invoke-static {}, Landroid/os/Looper;->loop()V

    #@e8
    .line 593
    return-void

    #@e9
    .line 571
    :cond_e9
    const-string v0, "DebugScreen"

    #@eb
    const-string v1, "PhoneStateTracker for register Event is null"

    #@ed
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@f0
    goto/16 :goto_63

    #@f2
    .line 581
    :cond_f2
    const-string v0, "DebugScreen"

    #@f4
    const-string v1, "ACS for register Event is null"

    #@f6
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@f9
    goto :goto_aa

    #@fa
    .line 589
    :cond_fa
    const-string v0, "DebugScreen"

    #@fc
    const-string v1, "DebugService for register Event is null"

    #@fe
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@101
    goto :goto_e5
.end method
