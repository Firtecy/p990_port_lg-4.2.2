.class final Lcom/lge/ims/debug/DebugService$DebugServiceHandler;
.super Landroid/os/Handler;
.source "DebugService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/debug/DebugService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "DebugServiceHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/debug/DebugService;


# direct methods
.method private constructor <init>(Lcom/lge/ims/debug/DebugService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 227
    iput-object p1, p0, Lcom/lge/ims/debug/DebugService$DebugServiceHandler;->this$0:Lcom/lge/ims/debug/DebugService;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/lge/ims/debug/DebugService;Lcom/lge/ims/debug/DebugService$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 227
    invoke-direct {p0, p1}, Lcom/lge/ims/debug/DebugService$DebugServiceHandler;-><init>(Lcom/lge/ims/debug/DebugService;)V

    #@3
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    .line 232
    if-nez p1, :cond_3

    #@2
    .line 266
    :goto_2
    return-void

    #@3
    .line 235
    :cond_3
    const-string v0, "DebugService"

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "DebugServiceHandler :: handleMessage - "

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    iget v2, p1, Landroid/os/Message;->what:I

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@1d
    .line 237
    iget v0, p1, Landroid/os/Message;->what:I

    #@1f
    packed-switch v0, :pswitch_data_70

    #@22
    .line 263
    const-string v0, "DebugService"

    #@24
    new-instance v1, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v2, "Unhandled Message :: "

    #@2b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    iget v2, p1, Landroid/os/Message;->what:I

    #@31
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v1

    #@35
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v1

    #@39
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@3c
    goto :goto_2

    #@3d
    .line 240
    :pswitch_3d
    iget-object v0, p0, Lcom/lge/ims/debug/DebugService$DebugServiceHandler;->this$0:Lcom/lge/ims/debug/DebugService;

    #@3f
    invoke-static {v0}, Lcom/lge/ims/debug/DebugService;->access$000(Lcom/lge/ims/debug/DebugService;)Landroid/os/RegistrantList;

    #@42
    move-result-object v0

    #@43
    invoke-virtual {v0}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@46
    goto :goto_2

    #@47
    .line 246
    :pswitch_47
    iget-object v0, p0, Lcom/lge/ims/debug/DebugService$DebugServiceHandler;->this$0:Lcom/lge/ims/debug/DebugService;

    #@49
    invoke-static {v0}, Lcom/lge/ims/debug/DebugService;->access$100(Lcom/lge/ims/debug/DebugService;)Landroid/os/RegistrantList;

    #@4c
    move-result-object v0

    #@4d
    invoke-virtual {v0}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@50
    goto :goto_2

    #@51
    .line 252
    :pswitch_51
    iget-object v0, p0, Lcom/lge/ims/debug/DebugService$DebugServiceHandler;->this$0:Lcom/lge/ims/debug/DebugService;

    #@53
    invoke-static {v0}, Lcom/lge/ims/debug/DebugService;->access$200(Lcom/lge/ims/debug/DebugService;)Landroid/os/RegistrantList;

    #@56
    move-result-object v0

    #@57
    const/4 v1, 0x1

    #@58
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5b
    move-result-object v1

    #@5c
    invoke-virtual {v0, v1}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@5f
    goto :goto_2

    #@60
    .line 258
    :pswitch_60
    iget-object v0, p0, Lcom/lge/ims/debug/DebugService$DebugServiceHandler;->this$0:Lcom/lge/ims/debug/DebugService;

    #@62
    invoke-static {v0}, Lcom/lge/ims/debug/DebugService;->access$200(Lcom/lge/ims/debug/DebugService;)Landroid/os/RegistrantList;

    #@65
    move-result-object v0

    #@66
    const/4 v1, 0x0

    #@67
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6a
    move-result-object v1

    #@6b
    invoke-virtual {v0, v1}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@6e
    goto :goto_2

    #@6f
    .line 237
    nop

    #@70
    :pswitch_data_70
    .packed-switch 0x65
        :pswitch_3d
        :pswitch_47
        :pswitch_51
        :pswitch_60
    .end packed-switch
.end method
