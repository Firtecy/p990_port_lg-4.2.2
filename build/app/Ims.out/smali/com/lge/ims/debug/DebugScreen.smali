.class public Lcom/lge/ims/debug/DebugScreen;
.super Landroid/app/Activity;
.source "DebugScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/debug/DebugScreen$DebugScreenThread;,
        Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;
    }
.end annotation


# static fields
.field private static final EVENT_AC_CONFIGURATION_ACCESS_ALLOWED:I = 0x6c

.field private static final EVENT_AC_CONFIGURATION_UPDATE_COMPLETED:I = 0x68

.field private static final EVENT_AIRPLANE_MODE_CHANGED:I = 0x66

.field private static final EVENT_LAST_MESSAGE_FOR_CALL_UPDATED:I = 0x6a

.field private static final EVENT_RAT_CHANGED:I = 0x65

.field private static final EVENT_REGISTRATION_RESULT_UPDATED:I = 0x69

.field private static final EVENT_SERVER_INTERWORKING_UPDATED:I = 0x6b

.field private static final EVENT_VOLTE_REG_STATE_UPDATED:I = 0x67

.field public static final KEY_APCSCONFIG:Ljava/lang/String; = "apcsconfig"

.field public static final KEY_APCSVOLTE:Ljava/lang/String; = "apcsvolte"

.field public static final KEY_LASTCALLINFODOWN:Ljava/lang/String; = "lastcallinfodown"

.field public static final KEY_LASTCALLINFOUP:Ljava/lang/String; = "lastcallinfoup"

.field public static final KEY_NETWORKMODE:Ljava/lang/String; = "networkmode"

.field public static final KEY_SIPREGI:Ljava/lang/String; = "sipregi"

.field public static final KEY_VOLTESTATUS:Ljava/lang/String; = "voltestatus"

.field private static final TAG:Ljava/lang/String; = "DebugScreen"


# instance fields
.field private mACService:Lcom/lge/ims/service/ac/ACService;

.field private mApcsConfigView:Landroid/widget/TextView;

.field private mApcsVolteView:Landroid/widget/TextView;

.field private mDebugScreenHandler:Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;

.field private mDebugScreenThread:Lcom/lge/ims/debug/DebugScreen$DebugScreenThread;

.field private mDebugService:Lcom/lge/ims/debug/DebugService;

.field private mHandler_UI:Landroid/os/Handler;

.field private mLastCallViewDown:Landroid/widget/TextView;

.field private mLastCallViewUp:Landroid/widget/TextView;

.field private mNetworkModeView:Landroid/widget/TextView;

.field private mPhoneStateTracker:Lcom/lge/ims/PhoneStateTracker;

.field private mSipRegiView:Landroid/widget/TextView;

.field private mVolteStatusView:Landroid/widget/TextView;

.field private sbApcsConfig:Ljava/lang/StringBuffer;

.field private sbApcsVolte:Ljava/lang/StringBuffer;

.field private sbLastCallInfoDown:Ljava/lang/StringBuffer;

.field private sbLastCallInfoUp:Ljava/lang/StringBuffer;

.field private sbNetworkMode:Ljava/lang/StringBuffer;

.field private sbSipRegi:Ljava/lang/StringBuffer;

.field private sbVolteStatus:Ljava/lang/StringBuffer;


# direct methods
.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 68
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    #@4
    .line 44
    new-instance v0, Ljava/lang/StringBuffer;

    #@6
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    #@9
    iput-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->sbNetworkMode:Ljava/lang/StringBuffer;

    #@b
    .line 45
    new-instance v0, Ljava/lang/StringBuffer;

    #@d
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    #@10
    iput-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->sbVolteStatus:Ljava/lang/StringBuffer;

    #@12
    .line 46
    new-instance v0, Ljava/lang/StringBuffer;

    #@14
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    #@17
    iput-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->sbApcsVolte:Ljava/lang/StringBuffer;

    #@19
    .line 47
    new-instance v0, Ljava/lang/StringBuffer;

    #@1b
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    #@1e
    iput-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->sbApcsConfig:Ljava/lang/StringBuffer;

    #@20
    .line 48
    new-instance v0, Ljava/lang/StringBuffer;

    #@22
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    #@25
    iput-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->sbSipRegi:Ljava/lang/StringBuffer;

    #@27
    .line 49
    new-instance v0, Ljava/lang/StringBuffer;

    #@29
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    #@2c
    iput-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->sbLastCallInfoUp:Ljava/lang/StringBuffer;

    #@2e
    .line 50
    new-instance v0, Ljava/lang/StringBuffer;

    #@30
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    #@33
    iput-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->sbLastCallInfoDown:Ljava/lang/StringBuffer;

    #@35
    .line 52
    iput-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugScreenHandler:Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;

    #@37
    .line 53
    iput-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugScreenThread:Lcom/lge/ims/debug/DebugScreen$DebugScreenThread;

    #@39
    .line 55
    invoke-static {}, Lcom/lge/ims/PhoneStateTracker;->getInstance()Lcom/lge/ims/PhoneStateTracker;

    #@3c
    move-result-object v0

    #@3d
    iput-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->mPhoneStateTracker:Lcom/lge/ims/PhoneStateTracker;

    #@3f
    .line 56
    invoke-static {}, Lcom/lge/ims/service/ac/ACService;->getInstance()Lcom/lge/ims/service/ac/ACService;

    #@42
    move-result-object v0

    #@43
    iput-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->mACService:Lcom/lge/ims/service/ac/ACService;

    #@45
    .line 57
    invoke-static {}, Lcom/lge/ims/debug/DebugService;->getInstance()Lcom/lge/ims/debug/DebugService;

    #@48
    move-result-object v0

    #@49
    iput-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugService:Lcom/lge/ims/debug/DebugService;

    #@4b
    .line 539
    new-instance v0, Lcom/lge/ims/debug/DebugScreen$1;

    #@4d
    invoke-direct {v0, p0}, Lcom/lge/ims/debug/DebugScreen$1;-><init>(Lcom/lge/ims/debug/DebugScreen;)V

    #@50
    iput-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->mHandler_UI:Landroid/os/Handler;

    #@52
    .line 69
    const-string v0, "DebugScreen"

    #@54
    const-string v1, "Constructor of Debug Screen."

    #@56
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@59
    .line 70
    new-instance v0, Lcom/lge/ims/debug/DebugScreen$DebugScreenThread;

    #@5b
    invoke-direct {v0, p0}, Lcom/lge/ims/debug/DebugScreen$DebugScreenThread;-><init>(Lcom/lge/ims/debug/DebugScreen;)V

    #@5e
    iput-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugScreenThread:Lcom/lge/ims/debug/DebugScreen$DebugScreenThread;

    #@60
    .line 71
    return-void
.end method

.method static synthetic access$000(Lcom/lge/ims/debug/DebugScreen;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/lge/ims/debug/DebugScreen;->updateNetworkMode(I)V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/lge/ims/debug/DebugScreen;)Lcom/lge/ims/debug/DebugService;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 32
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugService:Lcom/lge/ims/debug/DebugService;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Lcom/lge/ims/debug/DebugScreen;)Ljava/lang/StringBuffer;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 32
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->sbApcsVolte:Ljava/lang/StringBuffer;

    #@2
    return-object v0
.end method

.method static synthetic access$1100(Lcom/lge/ims/debug/DebugScreen;)Ljava/lang/StringBuffer;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 32
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->sbApcsConfig:Ljava/lang/StringBuffer;

    #@2
    return-object v0
.end method

.method static synthetic access$1200(Lcom/lge/ims/debug/DebugScreen;)Ljava/lang/StringBuffer;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 32
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->sbSipRegi:Ljava/lang/StringBuffer;

    #@2
    return-object v0
.end method

.method static synthetic access$1300(Lcom/lge/ims/debug/DebugScreen;)Ljava/lang/StringBuffer;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 32
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->sbLastCallInfoUp:Ljava/lang/StringBuffer;

    #@2
    return-object v0
.end method

.method static synthetic access$1400(Lcom/lge/ims/debug/DebugScreen;)Ljava/lang/StringBuffer;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 32
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->sbLastCallInfoDown:Ljava/lang/StringBuffer;

    #@2
    return-object v0
.end method

.method static synthetic access$1500(Lcom/lge/ims/debug/DebugScreen;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 32
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->mHandler_UI:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$1600(Lcom/lge/ims/debug/DebugScreen;)Landroid/widget/TextView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 32
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->mNetworkModeView:Landroid/widget/TextView;

    #@2
    return-object v0
.end method

.method static synthetic access$1700(Lcom/lge/ims/debug/DebugScreen;)Landroid/widget/TextView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 32
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->mVolteStatusView:Landroid/widget/TextView;

    #@2
    return-object v0
.end method

.method static synthetic access$1800(Lcom/lge/ims/debug/DebugScreen;)Landroid/widget/TextView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 32
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->mApcsVolteView:Landroid/widget/TextView;

    #@2
    return-object v0
.end method

.method static synthetic access$1900(Lcom/lge/ims/debug/DebugScreen;)Landroid/widget/TextView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 32
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->mApcsConfigView:Landroid/widget/TextView;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/lge/ims/debug/DebugScreen;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/lge/ims/debug/DebugScreen;->updateVolteStatus(Z)V

    #@3
    return-void
.end method

.method static synthetic access$2000(Lcom/lge/ims/debug/DebugScreen;)Landroid/widget/TextView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 32
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->mSipRegiView:Landroid/widget/TextView;

    #@2
    return-object v0
.end method

.method static synthetic access$2100(Lcom/lge/ims/debug/DebugScreen;)Landroid/widget/TextView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 32
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->mLastCallViewUp:Landroid/widget/TextView;

    #@2
    return-object v0
.end method

.method static synthetic access$2200(Lcom/lge/ims/debug/DebugScreen;)Landroid/widget/TextView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 32
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->mLastCallViewDown:Landroid/widget/TextView;

    #@2
    return-object v0
.end method

.method static synthetic access$2300(Lcom/lge/ims/debug/DebugScreen;)Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 32
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugScreenHandler:Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;

    #@2
    return-object v0
.end method

.method static synthetic access$2302(Lcom/lge/ims/debug/DebugScreen;Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;)Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 32
    iput-object p1, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugScreenHandler:Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;

    #@2
    return-object p1
.end method

.method static synthetic access$2400(Lcom/lge/ims/debug/DebugScreen;)Lcom/lge/ims/PhoneStateTracker;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 32
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->mPhoneStateTracker:Lcom/lge/ims/PhoneStateTracker;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/lge/ims/debug/DebugScreen;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/lge/ims/debug/DebugScreen;->updateApcsVolte(I)V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/lge/ims/debug/DebugScreen;)Lcom/lge/ims/service/ac/ACService;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 32
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->mACService:Lcom/lge/ims/service/ac/ACService;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Lcom/lge/ims/debug/DebugScreen;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/lge/ims/debug/DebugScreen;->updateApcsConfig(I)V

    #@3
    return-void
.end method

.method static synthetic access$600(Lcom/lge/ims/debug/DebugScreen;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/lge/ims/debug/DebugScreen;->updateSipRegi(I)V

    #@3
    return-void
.end method

.method static synthetic access$700(Lcom/lge/ims/debug/DebugScreen;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/lge/ims/debug/DebugScreen;->updateLastCallInfo(I)V

    #@3
    return-void
.end method

.method static synthetic access$800(Lcom/lge/ims/debug/DebugScreen;)Ljava/lang/StringBuffer;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 32
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->sbNetworkMode:Ljava/lang/StringBuffer;

    #@2
    return-object v0
.end method

.method static synthetic access$900(Lcom/lge/ims/debug/DebugScreen;)Ljava/lang/StringBuffer;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 32
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->sbVolteStatus:Ljava/lang/StringBuffer;

    #@2
    return-object v0
.end method

.method private firstDebugScreenInfo()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 157
    const-string v2, "DebugScreen"

    #@3
    const-string v3, "Setting the debug screen...."

    #@5
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 159
    iget-object v2, p0, Lcom/lge/ims/debug/DebugScreen;->sbNetworkMode:Ljava/lang/StringBuffer;

    #@a
    const-string v3, "Network Mode   : -"

    #@c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@f
    .line 160
    iget-object v2, p0, Lcom/lge/ims/debug/DebugScreen;->sbVolteStatus:Ljava/lang/StringBuffer;

    #@11
    const-string v3, "VoLTE Status   : -"

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@16
    .line 161
    iget-object v2, p0, Lcom/lge/ims/debug/DebugScreen;->sbApcsVolte:Ljava/lang/StringBuffer;

    #@18
    const-string v3, "ACS VoLTE      : -"

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@1d
    .line 162
    iget-object v2, p0, Lcom/lge/ims/debug/DebugScreen;->sbApcsConfig:Ljava/lang/StringBuffer;

    #@1f
    const-string v3, "ACS Config.    : -"

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@24
    .line 163
    iget-object v2, p0, Lcom/lge/ims/debug/DebugScreen;->sbSipRegi:Ljava/lang/StringBuffer;

    #@26
    const-string v3, "SIP Reg.       : -"

    #@28
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@2b
    .line 164
    iget-object v2, p0, Lcom/lge/ims/debug/DebugScreen;->sbLastCallInfoUp:Ljava/lang/StringBuffer;

    #@2d
    const-string v3, "Last Call(UP)  : -"

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@32
    .line 165
    iget-object v2, p0, Lcom/lge/ims/debug/DebugScreen;->sbLastCallInfoDown:Ljava/lang/StringBuffer;

    #@34
    const-string v3, "Last Call(DOWN): -"

    #@36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@39
    .line 167
    const/4 v1, 0x0

    #@3a
    .line 168
    .local v1, rat:I
    iget-object v2, p0, Lcom/lge/ims/debug/DebugScreen;->mPhoneStateTracker:Lcom/lge/ims/PhoneStateTracker;

    #@3c
    if-eqz v2, :cond_e9

    #@3e
    .line 169
    iget-object v2, p0, Lcom/lge/ims/debug/DebugScreen;->mPhoneStateTracker:Lcom/lge/ims/PhoneStateTracker;

    #@40
    invoke-virtual {v2}, Lcom/lge/ims/PhoneStateTracker;->is3G()Z

    #@43
    move-result v2

    #@44
    if-eqz v2, :cond_db

    #@46
    .line 170
    const/4 v1, 0x3

    #@47
    .line 176
    :goto_47
    invoke-direct {p0, v1}, Lcom/lge/ims/debug/DebugScreen;->updateNetworkMode(I)V

    #@4a
    .line 181
    :goto_4a
    iget-object v2, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugService:Lcom/lge/ims/debug/DebugService;

    #@4c
    if-eqz v2, :cond_f2

    #@4e
    .line 182
    iget-object v2, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugService:Lcom/lge/ims/debug/DebugService;

    #@50
    invoke-virtual {v2}, Lcom/lge/ims/debug/DebugService;->isVoLTERegistered()Z

    #@53
    move-result v2

    #@54
    invoke-direct {p0, v2}, Lcom/lge/ims/debug/DebugScreen;->updateVolteStatus(Z)V

    #@57
    .line 183
    iget-object v2, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugService:Lcom/lge/ims/debug/DebugService;

    #@59
    invoke-virtual {v2}, Lcom/lge/ims/debug/DebugService;->getRegistrationResult()I

    #@5c
    move-result v2

    #@5d
    invoke-direct {p0, v2}, Lcom/lge/ims/debug/DebugScreen;->updateSipRegi(I)V

    #@60
    .line 184
    invoke-direct {p0, v4}, Lcom/lge/ims/debug/DebugScreen;->updateLastCallInfo(I)V

    #@63
    .line 185
    const/4 v2, 0x1

    #@64
    invoke-direct {p0, v2}, Lcom/lge/ims/debug/DebugScreen;->updateLastCallInfo(I)V

    #@67
    .line 190
    :goto_67
    iget-object v2, p0, Lcom/lge/ims/debug/DebugScreen;->mACService:Lcom/lge/ims/service/ac/ACService;

    #@69
    if-eqz v2, :cond_fb

    #@6b
    .line 191
    iget-object v2, p0, Lcom/lge/ims/debug/DebugScreen;->mACService:Lcom/lge/ims/service/ac/ACService;

    #@6d
    invoke-virtual {v2}, Lcom/lge/ims/service/ac/ACService;->getProvisionedServices()I

    #@70
    move-result v2

    #@71
    invoke-direct {p0, v2}, Lcom/lge/ims/debug/DebugScreen;->updateApcsVolte(I)V

    #@74
    .line 192
    iget-object v2, p0, Lcom/lge/ims/debug/DebugScreen;->mACService:Lcom/lge/ims/service/ac/ACService;

    #@76
    invoke-virtual {v2}, Lcom/lge/ims/service/ac/ACService;->getServerInterworkingResult()I

    #@79
    move-result v2

    #@7a
    invoke-direct {p0, v2}, Lcom/lge/ims/debug/DebugScreen;->updateApcsConfig(I)V

    #@7d
    .line 197
    :goto_7d
    new-instance v0, Landroid/os/Bundle;

    #@7f
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@82
    .line 198
    .local v0, b:Landroid/os/Bundle;
    const-string v2, "networkmode"

    #@84
    iget-object v3, p0, Lcom/lge/ims/debug/DebugScreen;->sbNetworkMode:Ljava/lang/StringBuffer;

    #@86
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@89
    move-result-object v3

    #@8a
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@8d
    .line 199
    const-string v2, "voltestatus"

    #@8f
    iget-object v3, p0, Lcom/lge/ims/debug/DebugScreen;->sbVolteStatus:Ljava/lang/StringBuffer;

    #@91
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@94
    move-result-object v3

    #@95
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@98
    .line 200
    const-string v2, "apcsvolte"

    #@9a
    iget-object v3, p0, Lcom/lge/ims/debug/DebugScreen;->sbApcsVolte:Ljava/lang/StringBuffer;

    #@9c
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@9f
    move-result-object v3

    #@a0
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@a3
    .line 201
    const-string v2, "apcsconfig"

    #@a5
    iget-object v3, p0, Lcom/lge/ims/debug/DebugScreen;->sbApcsConfig:Ljava/lang/StringBuffer;

    #@a7
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@aa
    move-result-object v3

    #@ab
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@ae
    .line 202
    const-string v2, "sipregi"

    #@b0
    iget-object v3, p0, Lcom/lge/ims/debug/DebugScreen;->sbSipRegi:Ljava/lang/StringBuffer;

    #@b2
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@b5
    move-result-object v3

    #@b6
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@b9
    .line 203
    const-string v2, "lastcallinfoup"

    #@bb
    iget-object v3, p0, Lcom/lge/ims/debug/DebugScreen;->sbLastCallInfoUp:Ljava/lang/StringBuffer;

    #@bd
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@c0
    move-result-object v3

    #@c1
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@c4
    .line 204
    const-string v2, "lastcallinfodown"

    #@c6
    iget-object v3, p0, Lcom/lge/ims/debug/DebugScreen;->sbLastCallInfoDown:Ljava/lang/StringBuffer;

    #@c8
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@cb
    move-result-object v3

    #@cc
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@cf
    .line 206
    iget-object v2, p0, Lcom/lge/ims/debug/DebugScreen;->mHandler_UI:Landroid/os/Handler;

    #@d1
    iget-object v3, p0, Lcom/lge/ims/debug/DebugScreen;->mHandler_UI:Landroid/os/Handler;

    #@d3
    invoke-static {v3, v4, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    #@d6
    move-result-object v3

    #@d7
    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@da
    .line 207
    return-void

    #@db
    .line 171
    .end local v0           #b:Landroid/os/Bundle;
    :cond_db
    iget-object v2, p0, Lcom/lge/ims/debug/DebugScreen;->mPhoneStateTracker:Lcom/lge/ims/PhoneStateTracker;

    #@dd
    invoke-virtual {v2}, Lcom/lge/ims/PhoneStateTracker;->is4G()Z

    #@e0
    move-result v2

    #@e1
    if-eqz v2, :cond_e6

    #@e3
    .line 172
    const/4 v1, 0x2

    #@e4
    goto/16 :goto_47

    #@e6
    .line 174
    :cond_e6
    const/4 v1, 0x0

    #@e7
    goto/16 :goto_47

    #@e9
    .line 178
    :cond_e9
    const-string v2, "DebugScreen"

    #@eb
    const-string v3, "PhoneStateTracker is null."

    #@ed
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@f0
    goto/16 :goto_4a

    #@f2
    .line 187
    :cond_f2
    const-string v2, "DebugScreen"

    #@f4
    const-string v3, "DebugService is null."

    #@f6
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@f9
    goto/16 :goto_67

    #@fb
    .line 194
    :cond_fb
    const-string v2, "DebugScreen"

    #@fd
    const-string v3, "ACService is null."

    #@ff
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@102
    goto/16 :goto_7d
.end method

.method private updateApcsConfig(I)V
    .registers 6
    .parameter "res"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 272
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mACService:Lcom/lge/ims/service/ac/ACService;

    #@3
    if-nez v1, :cond_6

    #@5
    .line 310
    :cond_5
    :goto_5
    return-void

    #@6
    .line 276
    :cond_6
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugService:Lcom/lge/ims/debug/DebugService;

    #@8
    if-eqz v1, :cond_5

    #@a
    .line 280
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mACService:Lcom/lge/ims/service/ac/ACService;

    #@c
    invoke-virtual {v1}, Lcom/lge/ims/service/ac/ACService;->getTimestampForServerInterworkingResult()Ljava/lang/String;

    #@f
    move-result-object v0

    #@10
    .line 282
    .local v0, date:Ljava/lang/String;
    sparse-switch p1, :sswitch_data_aa

    #@13
    .line 296
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbApcsConfig:Ljava/lang/StringBuffer;

    #@15
    iget-object v2, p0, Lcom/lge/ims/debug/DebugScreen;->sbApcsConfig:Ljava/lang/StringBuffer;

    #@17
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->capacity()I

    #@1a
    move-result v2

    #@1b
    invoke-virtual {v1, v3, v2}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    #@1e
    .line 297
    const/4 v1, -0x1

    #@1f
    if-ne p1, v1, :cond_6b

    #@21
    .line 298
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbApcsConfig:Ljava/lang/StringBuffer;

    #@23
    const-string v2, "ACS Config.    : -"

    #@25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@28
    .line 305
    :goto_28
    if-nez v0, :cond_84

    #@2a
    .line 306
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbApcsConfig:Ljava/lang/StringBuffer;

    #@2c
    const-string v2, "(-)"

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@31
    goto :goto_5

    #@32
    .line 284
    :sswitch_32
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbApcsConfig:Ljava/lang/StringBuffer;

    #@34
    iget-object v2, p0, Lcom/lge/ims/debug/DebugScreen;->sbApcsConfig:Ljava/lang/StringBuffer;

    #@36
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->capacity()I

    #@39
    move-result v2

    #@3a
    invoke-virtual {v1, v3, v2}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    #@3d
    .line 285
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbApcsConfig:Ljava/lang/StringBuffer;

    #@3f
    const-string v2, "ACS Config.    : OK"

    #@41
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@44
    goto :goto_28

    #@45
    .line 288
    :sswitch_45
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbApcsConfig:Ljava/lang/StringBuffer;

    #@47
    iget-object v2, p0, Lcom/lge/ims/debug/DebugScreen;->sbApcsConfig:Ljava/lang/StringBuffer;

    #@49
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->capacity()I

    #@4c
    move-result v2

    #@4d
    invoke-virtual {v1, v3, v2}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    #@50
    .line 289
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbApcsConfig:Ljava/lang/StringBuffer;

    #@52
    const-string v2, "ACS Config.    : NO RESPONSE"

    #@54
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@57
    goto :goto_28

    #@58
    .line 292
    :sswitch_58
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbApcsConfig:Ljava/lang/StringBuffer;

    #@5a
    iget-object v2, p0, Lcom/lge/ims/debug/DebugScreen;->sbApcsConfig:Ljava/lang/StringBuffer;

    #@5c
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->capacity()I

    #@5f
    move-result v2

    #@60
    invoke-virtual {v1, v3, v2}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    #@63
    .line 293
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbApcsConfig:Ljava/lang/StringBuffer;

    #@65
    const-string v2, "ACS Config.    : ABNORMAL"

    #@67
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@6a
    goto :goto_28

    #@6b
    .line 300
    :cond_6b
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbApcsConfig:Ljava/lang/StringBuffer;

    #@6d
    new-instance v2, Ljava/lang/StringBuilder;

    #@6f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@72
    const-string v3, "ACS Config.    : "

    #@74
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v2

    #@78
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v2

    #@7c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7f
    move-result-object v2

    #@80
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@83
    goto :goto_28

    #@84
    .line 308
    :cond_84
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbApcsConfig:Ljava/lang/StringBuffer;

    #@86
    new-instance v2, Ljava/lang/StringBuilder;

    #@88
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8b
    const-string v3, " ("

    #@8d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v2

    #@91
    iget-object v3, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugService:Lcom/lge/ims/debug/DebugService;

    #@93
    invoke-static {v0}, Lcom/lge/ims/debug/DebugService;->getShortFormFromDate(Ljava/lang/String;)Ljava/lang/String;

    #@96
    move-result-object v3

    #@97
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v2

    #@9b
    const-string v3, ")"

    #@9d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v2

    #@a1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a4
    move-result-object v2

    #@a5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@a8
    goto/16 :goto_5

    #@aa
    .line 282
    :sswitch_data_aa
    .sparse-switch
        0x0 -> :sswitch_45
        0x1 -> :sswitch_58
        0xc8 -> :sswitch_32
    .end sparse-switch
.end method

.method private updateApcsVolte(I)V
    .registers 11
    .parameter "mProvisionedService"

    #@0
    .prologue
    const-wide/16 v7, 0x3e8

    #@2
    const/4 v6, 0x0

    #@3
    .line 251
    new-instance v2, Ljava/text/SimpleDateFormat;

    #@5
    const-string v3, "yyyy-MM-dd HH:mm:ss"

    #@7
    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    #@a
    .line 252
    .local v2, sdf:Ljava/text/SimpleDateFormat;
    new-instance v3, Ljava/util/Date;

    #@c
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@f
    move-result-wide v4

    #@10
    div-long/2addr v4, v7

    #@11
    mul-long/2addr v4, v7

    #@12
    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    #@15
    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    .line 254
    .local v0, currentDate:Ljava/lang/String;
    iget-object v3, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugService:Lcom/lge/ims/debug/DebugService;

    #@1b
    if-nez v3, :cond_1e

    #@1d
    .line 269
    :goto_1d
    return-void

    #@1e
    .line 258
    :cond_1e
    iget-object v3, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugService:Lcom/lge/ims/debug/DebugService;

    #@20
    invoke-static {v0}, Lcom/lge/ims/debug/DebugService;->getShortFormFromDate(Ljava/lang/String;)Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    .line 260
    .local v1, date:Ljava/lang/String;
    and-int/lit8 v3, p1, 0x1

    #@26
    if-eqz v3, :cond_52

    #@28
    .line 262
    iget-object v3, p0, Lcom/lge/ims/debug/DebugScreen;->sbApcsVolte:Ljava/lang/StringBuffer;

    #@2a
    iget-object v4, p0, Lcom/lge/ims/debug/DebugScreen;->sbApcsVolte:Ljava/lang/StringBuffer;

    #@2c
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->capacity()I

    #@2f
    move-result v4

    #@30
    invoke-virtual {v3, v6, v4}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    #@33
    .line 263
    iget-object v3, p0, Lcom/lge/ims/debug/DebugScreen;->sbApcsVolte:Ljava/lang/StringBuffer;

    #@35
    new-instance v4, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v5, "ACS VoLTE      : OK ("

    #@3c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v4

    #@40
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v4

    #@44
    const-string v5, ")"

    #@46
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v4

    #@4a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v4

    #@4e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@51
    goto :goto_1d

    #@52
    .line 266
    :cond_52
    iget-object v3, p0, Lcom/lge/ims/debug/DebugScreen;->sbApcsVolte:Ljava/lang/StringBuffer;

    #@54
    iget-object v4, p0, Lcom/lge/ims/debug/DebugScreen;->sbApcsVolte:Ljava/lang/StringBuffer;

    #@56
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->capacity()I

    #@59
    move-result v4

    #@5a
    invoke-virtual {v3, v6, v4}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    #@5d
    .line 267
    iget-object v3, p0, Lcom/lge/ims/debug/DebugScreen;->sbApcsVolte:Ljava/lang/StringBuffer;

    #@5f
    new-instance v4, Ljava/lang/StringBuilder;

    #@61
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    const-string v5, "ACS VoLTE      : NOK ("

    #@66
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v4

    #@6a
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v4

    #@6e
    const-string v5, ")"

    #@70
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v4

    #@74
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@77
    move-result-object v4

    #@78
    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@7b
    goto :goto_1d
.end method

.method private updateLastCallInfo(I)V
    .registers 6
    .parameter "res"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 335
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugService:Lcom/lge/ims/debug/DebugService;

    #@3
    if-nez v1, :cond_6

    #@5
    .line 375
    :goto_5
    return-void

    #@6
    .line 338
    :cond_6
    if-nez p1, :cond_81

    #@8
    .line 340
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugService:Lcom/lge/ims/debug/DebugService;

    #@a
    invoke-virtual {v1}, Lcom/lge/ims/debug/DebugService;->getTimestampForLastOutgoingMessageForCall()Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    .line 342
    .local v0, date:Ljava/lang/String;
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugService:Lcom/lge/ims/debug/DebugService;

    #@10
    invoke-virtual {v1}, Lcom/lge/ims/debug/DebugService;->getLastOutgoingMessageForCall()Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    if-eqz v1, :cond_49

    #@16
    .line 343
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbLastCallInfoUp:Ljava/lang/StringBuffer;

    #@18
    iget-object v2, p0, Lcom/lge/ims/debug/DebugScreen;->sbLastCallInfoUp:Ljava/lang/StringBuffer;

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->capacity()I

    #@1d
    move-result v2

    #@1e
    invoke-virtual {v1, v3, v2}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    #@21
    .line 344
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbLastCallInfoUp:Ljava/lang/StringBuffer;

    #@23
    new-instance v2, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v3, "Last Call(UP)  : "

    #@2a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v2

    #@2e
    iget-object v3, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugService:Lcom/lge/ims/debug/DebugService;

    #@30
    invoke-virtual {v3}, Lcom/lge/ims/debug/DebugService;->getLastOutgoingMessageForCall()Ljava/lang/String;

    #@33
    move-result-object v3

    #@34
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v2

    #@38
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v2

    #@3c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@3f
    .line 350
    :goto_3f
    if-nez v0, :cond_5c

    #@41
    .line 351
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbLastCallInfoUp:Ljava/lang/StringBuffer;

    #@43
    const-string v2, " (-)"

    #@45
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@48
    goto :goto_5

    #@49
    .line 346
    :cond_49
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbLastCallInfoUp:Ljava/lang/StringBuffer;

    #@4b
    iget-object v2, p0, Lcom/lge/ims/debug/DebugScreen;->sbLastCallInfoUp:Ljava/lang/StringBuffer;

    #@4d
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->capacity()I

    #@50
    move-result v2

    #@51
    invoke-virtual {v1, v3, v2}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    #@54
    .line 347
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbLastCallInfoUp:Ljava/lang/StringBuffer;

    #@56
    const-string v2, "Last Call(UP)  : -"

    #@58
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@5b
    goto :goto_3f

    #@5c
    .line 353
    :cond_5c
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbLastCallInfoUp:Ljava/lang/StringBuffer;

    #@5e
    new-instance v2, Ljava/lang/StringBuilder;

    #@60
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@63
    const-string v3, " ("

    #@65
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v2

    #@69
    iget-object v3, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugService:Lcom/lge/ims/debug/DebugService;

    #@6b
    invoke-static {v0}, Lcom/lge/ims/debug/DebugService;->getShortFormFromDate(Ljava/lang/String;)Ljava/lang/String;

    #@6e
    move-result-object v3

    #@6f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v2

    #@73
    const-string v3, ")"

    #@75
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v2

    #@79
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v2

    #@7d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@80
    goto :goto_5

    #@81
    .line 355
    .end local v0           #date:Ljava/lang/String;
    :cond_81
    const/4 v1, 0x1

    #@82
    if-ne p1, v1, :cond_ff

    #@84
    .line 357
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugService:Lcom/lge/ims/debug/DebugService;

    #@86
    invoke-virtual {v1}, Lcom/lge/ims/debug/DebugService;->getTimestampForLastIncomingMessageForCall()Ljava/lang/String;

    #@89
    move-result-object v0

    #@8a
    .line 359
    .restart local v0       #date:Ljava/lang/String;
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugService:Lcom/lge/ims/debug/DebugService;

    #@8c
    invoke-virtual {v1}, Lcom/lge/ims/debug/DebugService;->getLastIncomingMessageForCall()Ljava/lang/String;

    #@8f
    move-result-object v1

    #@90
    if-eqz v1, :cond_c6

    #@92
    .line 360
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbLastCallInfoDown:Ljava/lang/StringBuffer;

    #@94
    iget-object v2, p0, Lcom/lge/ims/debug/DebugScreen;->sbLastCallInfoDown:Ljava/lang/StringBuffer;

    #@96
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->capacity()I

    #@99
    move-result v2

    #@9a
    invoke-virtual {v1, v3, v2}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    #@9d
    .line 361
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbLastCallInfoDown:Ljava/lang/StringBuffer;

    #@9f
    new-instance v2, Ljava/lang/StringBuilder;

    #@a1
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a4
    const-string v3, "Last Call(DOWN): "

    #@a6
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v2

    #@aa
    iget-object v3, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugService:Lcom/lge/ims/debug/DebugService;

    #@ac
    invoke-virtual {v3}, Lcom/lge/ims/debug/DebugService;->getLastIncomingMessageForCall()Ljava/lang/String;

    #@af
    move-result-object v3

    #@b0
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v2

    #@b4
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b7
    move-result-object v2

    #@b8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@bb
    .line 367
    :goto_bb
    if-nez v0, :cond_d9

    #@bd
    .line 368
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbLastCallInfoDown:Ljava/lang/StringBuffer;

    #@bf
    const-string v2, " (-)"

    #@c1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@c4
    goto/16 :goto_5

    #@c6
    .line 363
    :cond_c6
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbLastCallInfoDown:Ljava/lang/StringBuffer;

    #@c8
    iget-object v2, p0, Lcom/lge/ims/debug/DebugScreen;->sbLastCallInfoDown:Ljava/lang/StringBuffer;

    #@ca
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->capacity()I

    #@cd
    move-result v2

    #@ce
    invoke-virtual {v1, v3, v2}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    #@d1
    .line 364
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbLastCallInfoDown:Ljava/lang/StringBuffer;

    #@d3
    const-string v2, "Last Call(DOWN): -"

    #@d5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@d8
    goto :goto_bb

    #@d9
    .line 370
    :cond_d9
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbLastCallInfoDown:Ljava/lang/StringBuffer;

    #@db
    new-instance v2, Ljava/lang/StringBuilder;

    #@dd
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e0
    const-string v3, " ("

    #@e2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e5
    move-result-object v2

    #@e6
    iget-object v3, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugService:Lcom/lge/ims/debug/DebugService;

    #@e8
    invoke-static {v0}, Lcom/lge/ims/debug/DebugService;->getShortFormFromDate(Ljava/lang/String;)Ljava/lang/String;

    #@eb
    move-result-object v3

    #@ec
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ef
    move-result-object v2

    #@f0
    const-string v3, ")"

    #@f2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f5
    move-result-object v2

    #@f6
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f9
    move-result-object v2

    #@fa
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@fd
    goto/16 :goto_5

    #@ff
    .line 373
    .end local v0           #date:Ljava/lang/String;
    :cond_ff
    const-string v1, "DebugScreen"

    #@101
    const-string v2, "There\'s no handling this result."

    #@103
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@106
    goto/16 :goto_5
.end method

.method private updateNetworkMode(I)V
    .registers 5
    .parameter "rat"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 210
    packed-switch p1, :pswitch_data_3e

    #@4
    .line 222
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->sbNetworkMode:Ljava/lang/StringBuffer;

    #@6
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbNetworkMode:Ljava/lang/StringBuffer;

    #@8
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->capacity()I

    #@b
    move-result v1

    #@c
    invoke-virtual {v0, v2, v1}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    #@f
    .line 223
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->sbNetworkMode:Ljava/lang/StringBuffer;

    #@11
    const-string v1, "Network Mode   : Unknown"

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@16
    .line 226
    :goto_16
    return-void

    #@17
    .line 212
    :pswitch_17
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->sbNetworkMode:Ljava/lang/StringBuffer;

    #@19
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbNetworkMode:Ljava/lang/StringBuffer;

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->capacity()I

    #@1e
    move-result v1

    #@1f
    invoke-virtual {v0, v2, v1}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    #@22
    .line 213
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->sbNetworkMode:Ljava/lang/StringBuffer;

    #@24
    const-string v1, "Network Mode   : LTE"

    #@26
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@29
    goto :goto_16

    #@2a
    .line 217
    :pswitch_2a
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->sbNetworkMode:Ljava/lang/StringBuffer;

    #@2c
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbNetworkMode:Ljava/lang/StringBuffer;

    #@2e
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->capacity()I

    #@31
    move-result v1

    #@32
    invoke-virtual {v0, v2, v1}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    #@35
    .line 218
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->sbNetworkMode:Ljava/lang/StringBuffer;

    #@37
    const-string v1, "Network Mode   : 3G"

    #@39
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@3c
    goto :goto_16

    #@3d
    .line 210
    nop

    #@3e
    :pswitch_data_3e
    .packed-switch 0x2
        :pswitch_17
        :pswitch_2a
    .end packed-switch
.end method

.method private updateSipRegi(I)V
    .registers 6
    .parameter "regiResult"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 313
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugService:Lcom/lge/ims/debug/DebugService;

    #@3
    if-nez v1, :cond_6

    #@5
    .line 332
    :goto_5
    return-void

    #@6
    .line 317
    :cond_6
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugService:Lcom/lge/ims/debug/DebugService;

    #@8
    invoke-virtual {v1}, Lcom/lge/ims/debug/DebugService;->getTimestampForRegistrationResult()Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    .line 319
    .local v0, date:Ljava/lang/String;
    if-nez p1, :cond_2a

    #@e
    .line 320
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbSipRegi:Ljava/lang/StringBuffer;

    #@10
    iget-object v2, p0, Lcom/lge/ims/debug/DebugScreen;->sbSipRegi:Ljava/lang/StringBuffer;

    #@12
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->capacity()I

    #@15
    move-result v2

    #@16
    invoke-virtual {v1, v3, v2}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    #@19
    .line 321
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbSipRegi:Ljava/lang/StringBuffer;

    #@1b
    const-string v2, "SIP Reg.       : NO RESPONSE"

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@20
    .line 327
    :goto_20
    if-nez v0, :cond_4e

    #@22
    .line 328
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbSipRegi:Ljava/lang/StringBuffer;

    #@24
    const-string v2, " (-)"

    #@26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@29
    goto :goto_5

    #@2a
    .line 323
    :cond_2a
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbSipRegi:Ljava/lang/StringBuffer;

    #@2c
    iget-object v2, p0, Lcom/lge/ims/debug/DebugScreen;->sbSipRegi:Ljava/lang/StringBuffer;

    #@2e
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->capacity()I

    #@31
    move-result v2

    #@32
    invoke-virtual {v1, v3, v2}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    #@35
    .line 324
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbSipRegi:Ljava/lang/StringBuffer;

    #@37
    new-instance v2, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v3, "SIP Reg.       : "

    #@3e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v2

    #@42
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@45
    move-result-object v2

    #@46
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v2

    #@4a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@4d
    goto :goto_20

    #@4e
    .line 330
    :cond_4e
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbSipRegi:Ljava/lang/StringBuffer;

    #@50
    new-instance v2, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v3, " ("

    #@57
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v2

    #@5b
    iget-object v3, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugService:Lcom/lge/ims/debug/DebugService;

    #@5d
    invoke-static {v0}, Lcom/lge/ims/debug/DebugService;->getShortFormFromDate(Ljava/lang/String;)Ljava/lang/String;

    #@60
    move-result-object v3

    #@61
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v2

    #@65
    const-string v3, ")"

    #@67
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v2

    #@6b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6e
    move-result-object v2

    #@6f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@72
    goto :goto_5
.end method

.method private updateVolteStatus(Z)V
    .registers 6
    .parameter "isRegistered"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 229
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugService:Lcom/lge/ims/debug/DebugService;

    #@3
    if-nez v1, :cond_6

    #@5
    .line 248
    :goto_5
    return-void

    #@6
    .line 233
    :cond_6
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugService:Lcom/lge/ims/debug/DebugService;

    #@8
    invoke-virtual {v1}, Lcom/lge/ims/debug/DebugService;->getTimestampForVoLTEReg()Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    .line 235
    .local v0, date:Ljava/lang/String;
    if-eqz p1, :cond_2a

    #@e
    .line 236
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbVolteStatus:Ljava/lang/StringBuffer;

    #@10
    iget-object v2, p0, Lcom/lge/ims/debug/DebugScreen;->sbVolteStatus:Ljava/lang/StringBuffer;

    #@12
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->capacity()I

    #@15
    move-result v2

    #@16
    invoke-virtual {v1, v3, v2}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    #@19
    .line 237
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbVolteStatus:Ljava/lang/StringBuffer;

    #@1b
    const-string v2, "VoLTE Status   : Enable"

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@20
    .line 243
    :goto_20
    if-nez v0, :cond_3d

    #@22
    .line 244
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbVolteStatus:Ljava/lang/StringBuffer;

    #@24
    const-string v2, "(-)"

    #@26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@29
    goto :goto_5

    #@2a
    .line 239
    :cond_2a
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbVolteStatus:Ljava/lang/StringBuffer;

    #@2c
    iget-object v2, p0, Lcom/lge/ims/debug/DebugScreen;->sbVolteStatus:Ljava/lang/StringBuffer;

    #@2e
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->capacity()I

    #@31
    move-result v2

    #@32
    invoke-virtual {v1, v3, v2}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    #@35
    .line 240
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbVolteStatus:Ljava/lang/StringBuffer;

    #@37
    const-string v2, "VoLTE Status   : Disable"

    #@39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@3c
    goto :goto_20

    #@3d
    .line 246
    :cond_3d
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbVolteStatus:Ljava/lang/StringBuffer;

    #@3f
    new-instance v2, Ljava/lang/StringBuilder;

    #@41
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@44
    const-string v3, " ("

    #@46
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v2

    #@4a
    iget-object v3, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugService:Lcom/lge/ims/debug/DebugService;

    #@4c
    invoke-static {v0}, Lcom/lge/ims/debug/DebugService;->getShortFormFromDate(Ljava/lang/String;)Ljava/lang/String;

    #@4f
    move-result-object v3

    #@50
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v2

    #@54
    const-string v3, ")"

    #@56
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v2

    #@5a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object v2

    #@5e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@61
    goto :goto_5
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .registers 5
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 75
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    #@3
    .line 76
    const-string v1, "DebugScreen"

    #@5
    const-string v2, "onCreate()"

    #@7
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 77
    const v1, 0x7f040001

    #@d
    invoke-virtual {p0, v1}, Lcom/lge/ims/debug/DebugScreen;->setContentView(I)V

    #@10
    .line 78
    const v1, 0x7f06000b

    #@13
    invoke-virtual {p0, v1}, Lcom/lge/ims/debug/DebugScreen;->setTitle(I)V

    #@16
    .line 80
    const/high16 v1, 0x7f07

    #@18
    invoke-virtual {p0, v1}, Lcom/lge/ims/debug/DebugScreen;->findViewById(I)Landroid/view/View;

    #@1b
    move-result-object v1

    #@1c
    check-cast v1, Landroid/widget/TextView;

    #@1e
    iput-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mNetworkModeView:Landroid/widget/TextView;

    #@20
    .line 81
    const v1, 0x7f070001

    #@23
    invoke-virtual {p0, v1}, Lcom/lge/ims/debug/DebugScreen;->findViewById(I)Landroid/view/View;

    #@26
    move-result-object v1

    #@27
    check-cast v1, Landroid/widget/TextView;

    #@29
    iput-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mVolteStatusView:Landroid/widget/TextView;

    #@2b
    .line 82
    const v1, 0x7f070002

    #@2e
    invoke-virtual {p0, v1}, Lcom/lge/ims/debug/DebugScreen;->findViewById(I)Landroid/view/View;

    #@31
    move-result-object v1

    #@32
    check-cast v1, Landroid/widget/TextView;

    #@34
    iput-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mApcsVolteView:Landroid/widget/TextView;

    #@36
    .line 83
    const v1, 0x7f070003

    #@39
    invoke-virtual {p0, v1}, Lcom/lge/ims/debug/DebugScreen;->findViewById(I)Landroid/view/View;

    #@3c
    move-result-object v1

    #@3d
    check-cast v1, Landroid/widget/TextView;

    #@3f
    iput-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mApcsConfigView:Landroid/widget/TextView;

    #@41
    .line 84
    const v1, 0x7f070004

    #@44
    invoke-virtual {p0, v1}, Lcom/lge/ims/debug/DebugScreen;->findViewById(I)Landroid/view/View;

    #@47
    move-result-object v1

    #@48
    check-cast v1, Landroid/widget/TextView;

    #@4a
    iput-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mSipRegiView:Landroid/widget/TextView;

    #@4c
    .line 85
    const v1, 0x7f070005

    #@4f
    invoke-virtual {p0, v1}, Lcom/lge/ims/debug/DebugScreen;->findViewById(I)Landroid/view/View;

    #@52
    move-result-object v1

    #@53
    check-cast v1, Landroid/widget/TextView;

    #@55
    iput-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mLastCallViewUp:Landroid/widget/TextView;

    #@57
    .line 86
    const v1, 0x7f070006

    #@5a
    invoke-virtual {p0, v1}, Lcom/lge/ims/debug/DebugScreen;->findViewById(I)Landroid/view/View;

    #@5d
    move-result-object v1

    #@5e
    check-cast v1, Landroid/widget/TextView;

    #@60
    iput-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mLastCallViewDown:Landroid/widget/TextView;

    #@62
    .line 88
    sget-object v1, Landroid/graphics/Typeface;->MONOSPACE:Landroid/graphics/Typeface;

    #@64
    const/4 v2, 0x0

    #@65
    invoke-static {v1, v2}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    #@68
    move-result-object v0

    #@69
    .line 89
    .local v0, tf:Landroid/graphics/Typeface;
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mNetworkModeView:Landroid/widget/TextView;

    #@6b
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    #@6e
    .line 90
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mVolteStatusView:Landroid/widget/TextView;

    #@70
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    #@73
    .line 91
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mApcsVolteView:Landroid/widget/TextView;

    #@75
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    #@78
    .line 92
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mApcsConfigView:Landroid/widget/TextView;

    #@7a
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    #@7d
    .line 93
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mSipRegiView:Landroid/widget/TextView;

    #@7f
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    #@82
    .line 94
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mLastCallViewUp:Landroid/widget/TextView;

    #@84
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    #@87
    .line 95
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mLastCallViewDown:Landroid/widget/TextView;

    #@89
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    #@8c
    .line 97
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugScreenThread:Lcom/lge/ims/debug/DebugScreen$DebugScreenThread;

    #@8e
    if-eqz v1, :cond_95

    #@90
    .line 98
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugScreenThread:Lcom/lge/ims/debug/DebugScreen$DebugScreenThread;

    #@92
    invoke-virtual {v1}, Lcom/lge/ims/debug/DebugScreen$DebugScreenThread;->start()V

    #@95
    .line 100
    :cond_95
    return-void
.end method

.method public onDestroy()V
    .registers 3

    #@0
    .prologue
    .line 125
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    #@3
    .line 126
    const-string v0, "DebugScreen"

    #@5
    const-string v1, "onDestroy()"

    #@7
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 128
    const-string v0, "DebugScreen"

    #@c
    const-string v1, "Unregister the Events."

    #@e
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 130
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->mPhoneStateTracker:Lcom/lge/ims/PhoneStateTracker;

    #@13
    if-eqz v0, :cond_5e

    #@15
    .line 131
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->mPhoneStateTracker:Lcom/lge/ims/PhoneStateTracker;

    #@17
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugScreenHandler:Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;

    #@19
    invoke-virtual {v0, v1}, Lcom/lge/ims/PhoneStateTracker;->unregisterForRATChanged(Landroid/os/Handler;)V

    #@1c
    .line 132
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->mPhoneStateTracker:Lcom/lge/ims/PhoneStateTracker;

    #@1e
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugScreenHandler:Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;

    #@20
    invoke-virtual {v0, v1}, Lcom/lge/ims/PhoneStateTracker;->unregisterForAirplaneModeChanged(Landroid/os/Handler;)V

    #@23
    .line 137
    :goto_23
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->mACService:Lcom/lge/ims/service/ac/ACService;

    #@25
    if-eqz v0, :cond_66

    #@27
    .line 138
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->mACService:Lcom/lge/ims/service/ac/ACService;

    #@29
    invoke-virtual {v0}, Lcom/lge/ims/service/ac/ACService;->isConfigurationAccessAllowed()Z

    #@2c
    move-result v0

    #@2d
    if-nez v0, :cond_36

    #@2f
    .line 139
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->mACService:Lcom/lge/ims/service/ac/ACService;

    #@31
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugScreenHandler:Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;

    #@33
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ac/ACService;->unregisterForConfigurationAccessAllowed(Landroid/os/Handler;)V

    #@36
    .line 141
    :cond_36
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->mACService:Lcom/lge/ims/service/ac/ACService;

    #@38
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugScreenHandler:Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;

    #@3a
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ac/ACService;->unregisterForConfigurationUpdateCompleted(Landroid/os/Handler;)V

    #@3d
    .line 142
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->mACService:Lcom/lge/ims/service/ac/ACService;

    #@3f
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugScreenHandler:Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;

    #@41
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ac/ACService;->unregisterForServerInterworkingResult(Landroid/os/Handler;)V

    #@44
    .line 147
    :goto_44
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugService:Lcom/lge/ims/debug/DebugService;

    #@46
    if-eqz v0, :cond_6e

    #@48
    .line 148
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugService:Lcom/lge/ims/debug/DebugService;

    #@4a
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugScreenHandler:Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;

    #@4c
    invoke-virtual {v0, v1}, Lcom/lge/ims/debug/DebugService;->unregisterForVoLTERegStateUpdated(Landroid/os/Handler;)V

    #@4f
    .line 149
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugService:Lcom/lge/ims/debug/DebugService;

    #@51
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugScreenHandler:Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;

    #@53
    invoke-virtual {v0, v1}, Lcom/lge/ims/debug/DebugService;->unregisterForRegistrationUpdated(Landroid/os/Handler;)V

    #@56
    .line 150
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugService:Lcom/lge/ims/debug/DebugService;

    #@58
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->mDebugScreenHandler:Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;

    #@5a
    invoke-virtual {v0, v1}, Lcom/lge/ims/debug/DebugService;->unregisterForCallMessageUpdated(Landroid/os/Handler;)V

    #@5d
    .line 154
    :goto_5d
    return-void

    #@5e
    .line 134
    :cond_5e
    const-string v0, "DebugScreen"

    #@60
    const-string v1, "PhoneStateTracker is null"

    #@62
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@65
    goto :goto_23

    #@66
    .line 144
    :cond_66
    const-string v0, "DebugScreen"

    #@68
    const-string v1, "ACS is null"

    #@6a
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@6d
    goto :goto_44

    #@6e
    .line 152
    :cond_6e
    const-string v0, "DebugScreen"

    #@70
    const-string v1, "DebugService is null"

    #@72
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@75
    goto :goto_5d
.end method

.method public onPause()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 104
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    #@4
    .line 105
    const-string v0, "DebugScreen"

    #@6
    const-string v1, "onPause()"

    #@8
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@b
    .line 107
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->sbNetworkMode:Ljava/lang/StringBuffer;

    #@d
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbNetworkMode:Ljava/lang/StringBuffer;

    #@f
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->capacity()I

    #@12
    move-result v1

    #@13
    invoke-virtual {v0, v2, v1}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    #@16
    .line 108
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->sbVolteStatus:Ljava/lang/StringBuffer;

    #@18
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbVolteStatus:Ljava/lang/StringBuffer;

    #@1a
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->capacity()I

    #@1d
    move-result v1

    #@1e
    invoke-virtual {v0, v2, v1}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    #@21
    .line 109
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->sbApcsVolte:Ljava/lang/StringBuffer;

    #@23
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbApcsVolte:Ljava/lang/StringBuffer;

    #@25
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->capacity()I

    #@28
    move-result v1

    #@29
    invoke-virtual {v0, v2, v1}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    #@2c
    .line 110
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->sbApcsConfig:Ljava/lang/StringBuffer;

    #@2e
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbApcsConfig:Ljava/lang/StringBuffer;

    #@30
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->capacity()I

    #@33
    move-result v1

    #@34
    invoke-virtual {v0, v2, v1}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    #@37
    .line 111
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->sbSipRegi:Ljava/lang/StringBuffer;

    #@39
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbSipRegi:Ljava/lang/StringBuffer;

    #@3b
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->capacity()I

    #@3e
    move-result v1

    #@3f
    invoke-virtual {v0, v2, v1}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    #@42
    .line 112
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->sbLastCallInfoUp:Ljava/lang/StringBuffer;

    #@44
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbLastCallInfoUp:Ljava/lang/StringBuffer;

    #@46
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->capacity()I

    #@49
    move-result v1

    #@4a
    invoke-virtual {v0, v2, v1}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    #@4d
    .line 113
    iget-object v0, p0, Lcom/lge/ims/debug/DebugScreen;->sbLastCallInfoDown:Ljava/lang/StringBuffer;

    #@4f
    iget-object v1, p0, Lcom/lge/ims/debug/DebugScreen;->sbLastCallInfoDown:Ljava/lang/StringBuffer;

    #@51
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->capacity()I

    #@54
    move-result v1

    #@55
    invoke-virtual {v0, v2, v1}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    #@58
    .line 114
    return-void
.end method

.method public onResume()V
    .registers 3

    #@0
    .prologue
    .line 118
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    #@3
    .line 119
    const-string v0, "DebugScreen"

    #@5
    const-string v1, "onResume()"

    #@7
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 120
    invoke-direct {p0}, Lcom/lge/ims/debug/DebugScreen;->firstDebugScreenInfo()V

    #@d
    .line 121
    return-void
.end method
