.class public Lcom/lge/ims/debug/DebugService;
.super Ljava/lang/Object;
.source "DebugService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/debug/DebugService$1;,
        Lcom/lge/ims/debug/DebugService$DebugServiceThread;,
        Lcom/lge/ims/debug/DebugService$DebugServiceHandler;
    }
.end annotation


# static fields
.field private static final EVENT_LAST_INCOMING_MESSAGE_FOR_CALL_UPDATED:I = 0x67

.field private static final EVENT_LAST_OUTGOING_MESSAGE_FOR_CALL_UPDATED:I = 0x68

.field private static final EVENT_REGISTRATION_RESULT_UPDATED:I = 0x66

.field private static final EVENT_VOLTE_REG_STATE_UPDATED:I = 0x65

.field public static final RESULT_ABNORMAL_CONFIG:I = 0x1

.field public static final RESULT_NONE:I = -0x1

.field public static final RESULT_NO_RESPONSE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "DebugService"

.field private static mDebugService:Lcom/lge/ims/debug/DebugService;


# instance fields
.field private mCallMessageUpdatedRegistrants:Landroid/os/RegistrantList;

.field private mDebugServiceHandler:Lcom/lge/ims/debug/DebugService$DebugServiceHandler;

.field private mDebugServiceThread:Lcom/lge/ims/debug/DebugService$DebugServiceThread;

.field private mLastIncomingMessageForCall:Ljava/lang/String;

.field private mLastOutgoingMessageForCall:Ljava/lang/String;

.field private mRegistrationResult:I

.field private mRegistrationUpdatedRegistrants:Landroid/os/RegistrantList;

.field private mTimestampForLastIncomingMessageForCall:Ljava/lang/String;

.field private mTimestampForLastOutgoingMessageForCall:Ljava/lang/String;

.field private mTimestampForRegistrationResult:Ljava/lang/String;

.field private mTimestampForVoLTEReg:Ljava/lang/String;

.field private mVoLTERegStateUpdatedRegistrants:Landroid/os/RegistrantList;

.field private mVoLTERegistered:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 36
    const/4 v0, 0x0

    #@1
    sput-object v0, Lcom/lge/ims/debug/DebugService;->mDebugService:Lcom/lge/ims/debug/DebugService;

    #@3
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 54
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 37
    new-instance v0, Landroid/os/RegistrantList;

    #@6
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@9
    iput-object v0, p0, Lcom/lge/ims/debug/DebugService;->mVoLTERegStateUpdatedRegistrants:Landroid/os/RegistrantList;

    #@b
    .line 38
    new-instance v0, Landroid/os/RegistrantList;

    #@d
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@10
    iput-object v0, p0, Lcom/lge/ims/debug/DebugService;->mRegistrationUpdatedRegistrants:Landroid/os/RegistrantList;

    #@12
    .line 39
    new-instance v0, Landroid/os/RegistrantList;

    #@14
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@17
    iput-object v0, p0, Lcom/lge/ims/debug/DebugService;->mCallMessageUpdatedRegistrants:Landroid/os/RegistrantList;

    #@19
    .line 40
    iput-object v1, p0, Lcom/lge/ims/debug/DebugService;->mDebugServiceHandler:Lcom/lge/ims/debug/DebugService$DebugServiceHandler;

    #@1b
    .line 41
    iput-object v1, p0, Lcom/lge/ims/debug/DebugService;->mDebugServiceThread:Lcom/lge/ims/debug/DebugService$DebugServiceThread;

    #@1d
    .line 43
    const/4 v0, 0x0

    #@1e
    iput-boolean v0, p0, Lcom/lge/ims/debug/DebugService;->mVoLTERegistered:Z

    #@20
    .line 44
    const/4 v0, -0x1

    #@21
    iput v0, p0, Lcom/lge/ims/debug/DebugService;->mRegistrationResult:I

    #@23
    .line 45
    iput-object v1, p0, Lcom/lge/ims/debug/DebugService;->mLastIncomingMessageForCall:Ljava/lang/String;

    #@25
    .line 46
    iput-object v1, p0, Lcom/lge/ims/debug/DebugService;->mLastOutgoingMessageForCall:Ljava/lang/String;

    #@27
    .line 47
    iput-object v1, p0, Lcom/lge/ims/debug/DebugService;->mTimestampForVoLTEReg:Ljava/lang/String;

    #@29
    .line 48
    iput-object v1, p0, Lcom/lge/ims/debug/DebugService;->mTimestampForRegistrationResult:Ljava/lang/String;

    #@2b
    .line 49
    iput-object v1, p0, Lcom/lge/ims/debug/DebugService;->mTimestampForLastIncomingMessageForCall:Ljava/lang/String;

    #@2d
    .line 50
    iput-object v1, p0, Lcom/lge/ims/debug/DebugService;->mTimestampForLastOutgoingMessageForCall:Ljava/lang/String;

    #@2f
    .line 55
    new-instance v0, Lcom/lge/ims/debug/DebugService$DebugServiceThread;

    #@31
    invoke-direct {v0, p0}, Lcom/lge/ims/debug/DebugService$DebugServiceThread;-><init>(Lcom/lge/ims/debug/DebugService;)V

    #@34
    iput-object v0, p0, Lcom/lge/ims/debug/DebugService;->mDebugServiceThread:Lcom/lge/ims/debug/DebugService$DebugServiceThread;

    #@36
    .line 56
    return-void
.end method

.method static synthetic access$000(Lcom/lge/ims/debug/DebugService;)Landroid/os/RegistrantList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 22
    iget-object v0, p0, Lcom/lge/ims/debug/DebugService;->mVoLTERegStateUpdatedRegistrants:Landroid/os/RegistrantList;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/lge/ims/debug/DebugService;)Landroid/os/RegistrantList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 22
    iget-object v0, p0, Lcom/lge/ims/debug/DebugService;->mRegistrationUpdatedRegistrants:Landroid/os/RegistrantList;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/lge/ims/debug/DebugService;)Landroid/os/RegistrantList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 22
    iget-object v0, p0, Lcom/lge/ims/debug/DebugService;->mCallMessageUpdatedRegistrants:Landroid/os/RegistrantList;

    #@2
    return-object v0
.end method

.method static synthetic access$302(Lcom/lge/ims/debug/DebugService;Lcom/lge/ims/debug/DebugService$DebugServiceHandler;)Lcom/lge/ims/debug/DebugService$DebugServiceHandler;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 22
    iput-object p1, p0, Lcom/lge/ims/debug/DebugService;->mDebugServiceHandler:Lcom/lge/ims/debug/DebugService$DebugServiceHandler;

    #@2
    return-object p1
.end method

.method private static getDateFormat(J)Ljava/lang/String;
    .registers 6
    .parameter "currentTimeSeconds"

    #@0
    .prologue
    .line 178
    new-instance v0, Ljava/text/SimpleDateFormat;

    #@2
    const-string v1, "yyyy-MM-dd HH:mm:ss"

    #@4
    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    #@7
    .line 179
    .local v0, sdf:Ljava/text/SimpleDateFormat;
    new-instance v1, Ljava/util/Date;

    #@9
    const-wide/16 v2, 0x3e8

    #@b
    mul-long/2addr v2, p0

    #@c
    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    #@f
    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    return-object v1
.end method

.method public static getInstance()Lcom/lge/ims/debug/DebugService;
    .registers 2

    #@0
    .prologue
    .line 59
    sget-object v0, Lcom/lge/ims/debug/DebugService;->mDebugService:Lcom/lge/ims/debug/DebugService;

    #@2
    if-nez v0, :cond_18

    #@4
    .line 60
    new-instance v0, Lcom/lge/ims/debug/DebugService;

    #@6
    invoke-direct {v0}, Lcom/lge/ims/debug/DebugService;-><init>()V

    #@9
    sput-object v0, Lcom/lge/ims/debug/DebugService;->mDebugService:Lcom/lge/ims/debug/DebugService;

    #@b
    .line 62
    sget-object v0, Lcom/lge/ims/debug/DebugService;->mDebugService:Lcom/lge/ims/debug/DebugService;

    #@d
    if-nez v0, :cond_18

    #@f
    .line 63
    const-string v0, "DebugService"

    #@11
    const-string v1, "Instantiating a Debug Service failed"

    #@13
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@16
    .line 64
    const/4 v0, 0x0

    #@17
    .line 68
    :goto_17
    return-object v0

    #@18
    :cond_18
    sget-object v0, Lcom/lge/ims/debug/DebugService;->mDebugService:Lcom/lge/ims/debug/DebugService;

    #@1a
    goto :goto_17
.end method

.method public static getShortFormFromDate(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "date"

    #@0
    .prologue
    const/4 v1, 0x5

    #@1
    .line 165
    if-nez p0, :cond_5

    #@3
    .line 166
    const/4 p0, 0x0

    #@4
    .line 173
    .end local p0
    :cond_4
    :goto_4
    return-object p0

    #@5
    .line 169
    .restart local p0
    :cond_5
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@8
    move-result v0

    #@9
    if-le v0, v1, :cond_4

    #@b
    .line 171
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@e
    move-result-object p0

    #@f
    goto :goto_4
.end method

.method private setSipInfo(II)V
    .registers 15
    .parameter "wParam"
    .parameter "lParam"

    #@0
    .prologue
    const-wide/16 v10, 0x3e8

    #@2
    const-wide/16 v8, 0x64

    #@4
    const/4 v7, 0x1

    #@5
    .line 183
    const v4, 0xff00

    #@8
    and-int/2addr v4, p1

    #@9
    shr-int/lit8 v2, v4, 0x8

    #@b
    .line 184
    .local v2, msgType:I
    and-int/lit16 v0, p1, 0xff

    #@d
    .line 185
    .local v0, direction:I
    const/high16 v4, -0x1

    #@f
    and-int/2addr v4, p2

    #@10
    shr-int/lit8 v1, v4, 0x10

    #@12
    .line 186
    .local v1, method:I
    const v4, 0xffff

    #@15
    and-int v3, p2, v4

    #@17
    .line 188
    .local v3, statusCode:I
    const-string v4, "DebugService"

    #@19
    new-instance v5, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v6, "SipInfo :: msgType="

    #@20
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v5

    #@24
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@27
    move-result-object v5

    #@28
    const-string v6, ", direction="

    #@2a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v5

    #@2e
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@31
    move-result-object v5

    #@32
    const-string v6, ", method="

    #@34
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v5

    #@38
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v5

    #@3c
    const-string v6, ", statusCode="

    #@3e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v5

    #@42
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@45
    move-result-object v5

    #@46
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v5

    #@4a
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@4d
    .line 193
    const/4 v4, 0x5

    #@4e
    if-ne v1, v4, :cond_eb

    #@50
    .line 194
    if-ne v2, v7, :cond_6a

    #@52
    .line 195
    iput v3, p0, Lcom/lge/ims/debug/DebugService;->mRegistrationResult:I

    #@54
    .line 196
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@57
    move-result-wide v4

    #@58
    div-long/2addr v4, v10

    #@59
    invoke-static {v4, v5}, Lcom/lge/ims/debug/DebugService;->getDateFormat(J)Ljava/lang/String;

    #@5c
    move-result-object v4

    #@5d
    iput-object v4, p0, Lcom/lge/ims/debug/DebugService;->mTimestampForRegistrationResult:Ljava/lang/String;

    #@5f
    .line 198
    iget-object v4, p0, Lcom/lge/ims/debug/DebugService;->mDebugServiceHandler:Lcom/lge/ims/debug/DebugService$DebugServiceHandler;

    #@61
    if-eqz v4, :cond_6a

    #@63
    .line 199
    iget-object v4, p0, Lcom/lge/ims/debug/DebugService;->mDebugServiceHandler:Lcom/lge/ims/debug/DebugService$DebugServiceHandler;

    #@65
    const/16 v5, 0x66

    #@67
    invoke-virtual {v4, v5, v8, v9}, Lcom/lge/ims/debug/DebugService$DebugServiceHandler;->sendEmptyMessageDelayed(IJ)Z

    #@6a
    .line 220
    :cond_6a
    :goto_6a
    const-string v4, "user"

    #@6c
    sget-object v5, Lcom/lge/ims/Configuration;->BUILD_TYPE:Ljava/lang/String;

    #@6e
    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@71
    move-result v4

    #@72
    if-nez v4, :cond_ea

    #@74
    .line 221
    const-string v4, "DebugService"

    #@76
    new-instance v5, Ljava/lang/StringBuilder;

    #@78
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@7b
    const-string v6, "SipInfo :: REG="

    #@7d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v5

    #@81
    iget v6, p0, Lcom/lge/ims/debug/DebugService;->mRegistrationResult:I

    #@83
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@86
    move-result-object v5

    #@87
    const-string v6, ", "

    #@89
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v5

    #@8d
    iget-object v6, p0, Lcom/lge/ims/debug/DebugService;->mTimestampForRegistrationResult:Ljava/lang/String;

    #@8f
    invoke-static {v6}, Lcom/lge/ims/debug/DebugService;->getShortFormFromDate(Ljava/lang/String;)Ljava/lang/String;

    #@92
    move-result-object v6

    #@93
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v5

    #@97
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9a
    move-result-object v5

    #@9b
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@9e
    .line 222
    const-string v4, "DebugService"

    #@a0
    new-instance v5, Ljava/lang/StringBuilder;

    #@a2
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@a5
    const-string v6, "SipInfo :: LAST_IN_MSG="

    #@a7
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v5

    #@ab
    iget-object v6, p0, Lcom/lge/ims/debug/DebugService;->mLastIncomingMessageForCall:Ljava/lang/String;

    #@ad
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v5

    #@b1
    const-string v6, ", "

    #@b3
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v5

    #@b7
    iget-object v6, p0, Lcom/lge/ims/debug/DebugService;->mTimestampForLastIncomingMessageForCall:Ljava/lang/String;

    #@b9
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v5

    #@bd
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c0
    move-result-object v5

    #@c1
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@c4
    .line 223
    const-string v4, "DebugService"

    #@c6
    new-instance v5, Ljava/lang/StringBuilder;

    #@c8
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@cb
    const-string v6, "SipInfo :: LAST_OUT_MSG="

    #@cd
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d0
    move-result-object v5

    #@d1
    iget-object v6, p0, Lcom/lge/ims/debug/DebugService;->mLastOutgoingMessageForCall:Ljava/lang/String;

    #@d3
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d6
    move-result-object v5

    #@d7
    const-string v6, ", "

    #@d9
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v5

    #@dd
    iget-object v6, p0, Lcom/lge/ims/debug/DebugService;->mTimestampForLastOutgoingMessageForCall:Ljava/lang/String;

    #@df
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v5

    #@e3
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e6
    move-result-object v5

    #@e7
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@ea
    .line 225
    :cond_ea
    return-void

    #@eb
    .line 202
    :cond_eb
    if-eq v1, v7, :cond_f0

    #@ed
    const/4 v4, 0x3

    #@ee
    if-ne v1, v4, :cond_6a

    #@f0
    .line 203
    :cond_f0
    if-ne v0, v7, :cond_110

    #@f2
    .line 204
    invoke-static {v1}, Lcom/lge/ims/debug/SipInfo;->getMethod(I)Ljava/lang/String;

    #@f5
    move-result-object v4

    #@f6
    iput-object v4, p0, Lcom/lge/ims/debug/DebugService;->mLastIncomingMessageForCall:Ljava/lang/String;

    #@f8
    .line 205
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@fb
    move-result-wide v4

    #@fc
    div-long/2addr v4, v10

    #@fd
    invoke-static {v4, v5}, Lcom/lge/ims/debug/DebugService;->getDateFormat(J)Ljava/lang/String;

    #@100
    move-result-object v4

    #@101
    iput-object v4, p0, Lcom/lge/ims/debug/DebugService;->mTimestampForLastIncomingMessageForCall:Ljava/lang/String;

    #@103
    .line 207
    iget-object v4, p0, Lcom/lge/ims/debug/DebugService;->mDebugServiceHandler:Lcom/lge/ims/debug/DebugService$DebugServiceHandler;

    #@105
    if-eqz v4, :cond_6a

    #@107
    .line 208
    iget-object v4, p0, Lcom/lge/ims/debug/DebugService;->mDebugServiceHandler:Lcom/lge/ims/debug/DebugService$DebugServiceHandler;

    #@109
    const/16 v5, 0x67

    #@10b
    invoke-virtual {v4, v5, v8, v9}, Lcom/lge/ims/debug/DebugService$DebugServiceHandler;->sendEmptyMessageDelayed(IJ)Z

    #@10e
    goto/16 :goto_6a

    #@110
    .line 210
    :cond_110
    if-nez v0, :cond_6a

    #@112
    .line 211
    invoke-static {v1}, Lcom/lge/ims/debug/SipInfo;->getMethod(I)Ljava/lang/String;

    #@115
    move-result-object v4

    #@116
    iput-object v4, p0, Lcom/lge/ims/debug/DebugService;->mLastOutgoingMessageForCall:Ljava/lang/String;

    #@118
    .line 212
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@11b
    move-result-wide v4

    #@11c
    div-long/2addr v4, v10

    #@11d
    invoke-static {v4, v5}, Lcom/lge/ims/debug/DebugService;->getDateFormat(J)Ljava/lang/String;

    #@120
    move-result-object v4

    #@121
    iput-object v4, p0, Lcom/lge/ims/debug/DebugService;->mTimestampForLastOutgoingMessageForCall:Ljava/lang/String;

    #@123
    .line 214
    iget-object v4, p0, Lcom/lge/ims/debug/DebugService;->mDebugServiceHandler:Lcom/lge/ims/debug/DebugService$DebugServiceHandler;

    #@125
    if-eqz v4, :cond_6a

    #@127
    .line 215
    iget-object v4, p0, Lcom/lge/ims/debug/DebugService;->mDebugServiceHandler:Lcom/lge/ims/debug/DebugService$DebugServiceHandler;

    #@129
    const/16 v5, 0x68

    #@12b
    invoke-virtual {v4, v5, v8, v9}, Lcom/lge/ims/debug/DebugService$DebugServiceHandler;->sendEmptyMessageDelayed(IJ)Z

    #@12e
    goto/16 :goto_6a
.end method


# virtual methods
.method public create(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 72
    iget-object v0, p0, Lcom/lge/ims/debug/DebugService;->mDebugServiceThread:Lcom/lge/ims/debug/DebugService$DebugServiceThread;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 73
    iget-object v0, p0, Lcom/lge/ims/debug/DebugService;->mDebugServiceThread:Lcom/lge/ims/debug/DebugService$DebugServiceThread;

    #@6
    invoke-virtual {v0}, Lcom/lge/ims/debug/DebugService$DebugServiceThread;->start()V

    #@9
    .line 75
    :cond_9
    return-void
.end method

.method public destroy()V
    .registers 1

    #@0
    .prologue
    .line 78
    return-void
.end method

.method public getLastIncomingMessageForCall()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 81
    iget-object v0, p0, Lcom/lge/ims/debug/DebugService;->mLastIncomingMessageForCall:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getLastOutgoingMessageForCall()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 85
    iget-object v0, p0, Lcom/lge/ims/debug/DebugService;->mLastOutgoingMessageForCall:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getRegistrationResult()I
    .registers 2

    #@0
    .prologue
    .line 90
    iget v0, p0, Lcom/lge/ims/debug/DebugService;->mRegistrationResult:I

    #@2
    return v0
.end method

.method public getTimestampForLastIncomingMessageForCall()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 98
    iget-object v0, p0, Lcom/lge/ims/debug/DebugService;->mTimestampForLastIncomingMessageForCall:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getTimestampForLastOutgoingMessageForCall()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 102
    iget-object v0, p0, Lcom/lge/ims/debug/DebugService;->mTimestampForLastOutgoingMessageForCall:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getTimestampForRegistrationResult()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 94
    iget-object v0, p0, Lcom/lge/ims/debug/DebugService;->mTimestampForRegistrationResult:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getTimestampForVoLTEReg()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 106
    iget-object v0, p0, Lcom/lge/ims/debug/DebugService;->mTimestampForVoLTEReg:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public isVoLTERegistered()Z
    .registers 2

    #@0
    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/lge/ims/debug/DebugService;->mVoLTERegistered:Z

    #@2
    return v0
.end method

.method public registerForCallMessageUpdated(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 157
    iget-object v0, p0, Lcom/lge/ims/debug/DebugService;->mCallMessageUpdatedRegistrants:Landroid/os/RegistrantList;

    #@2
    new-instance v1, Landroid/os/Registrant;

    #@4
    invoke-direct {v1, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@7
    invoke-virtual {v0, v1}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 158
    return-void
.end method

.method public registerForRegistrationUpdated(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 148
    iget-object v0, p0, Lcom/lge/ims/debug/DebugService;->mRegistrationUpdatedRegistrants:Landroid/os/RegistrantList;

    #@2
    new-instance v1, Landroid/os/Registrant;

    #@4
    invoke-direct {v1, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@7
    invoke-virtual {v0, v1}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 149
    return-void
.end method

.method public registerForVoLTERegStateUpdated(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 139
    iget-object v0, p0, Lcom/lge/ims/debug/DebugService;->mVoLTERegStateUpdatedRegistrants:Landroid/os/RegistrantList;

    #@2
    new-instance v1, Landroid/os/Registrant;

    #@4
    invoke-direct {v1, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@7
    invoke-virtual {v0, v1}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 140
    return-void
.end method

.method public setDebugInfo(II)V
    .registers 5
    .parameter "wParam"
    .parameter "lParam"

    #@0
    .prologue
    .line 114
    const/high16 v1, -0x1

    #@2
    and-int/2addr v1, p1

    #@3
    shr-int/lit8 v0, v1, 0x10

    #@5
    .line 117
    .local v0, category:I
    if-nez v0, :cond_a

    #@7
    .line 118
    invoke-direct {p0, p1, p2}, Lcom/lge/ims/debug/DebugService;->setSipInfo(II)V

    #@a
    .line 120
    :cond_a
    return-void
.end method

.method public setVoLTERegState(Z)V
    .registers 6
    .parameter "isRegistered"

    #@0
    .prologue
    .line 123
    iget-boolean v0, p0, Lcom/lge/ims/debug/DebugService;->mVoLTERegistered:Z

    #@2
    if-ne v0, p1, :cond_5

    #@4
    .line 135
    :cond_4
    :goto_4
    return-void

    #@5
    .line 127
    :cond_5
    iput-boolean p1, p0, Lcom/lge/ims/debug/DebugService;->mVoLTERegistered:Z

    #@7
    .line 128
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@a
    move-result-wide v0

    #@b
    const-wide/16 v2, 0x3e8

    #@d
    div-long/2addr v0, v2

    #@e
    invoke-static {v0, v1}, Lcom/lge/ims/debug/DebugService;->getDateFormat(J)Ljava/lang/String;

    #@11
    move-result-object v0

    #@12
    iput-object v0, p0, Lcom/lge/ims/debug/DebugService;->mTimestampForVoLTEReg:Ljava/lang/String;

    #@14
    .line 130
    const-string v0, "DebugService"

    #@16
    new-instance v1, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v2, "VOLTE_REG="

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    iget-boolean v2, p0, Lcom/lge/ims/debug/DebugService;->mVoLTERegistered:Z

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    const-string v2, ", "

    #@29
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v1

    #@2d
    iget-object v2, p0, Lcom/lge/ims/debug/DebugService;->mTimestampForVoLTEReg:Ljava/lang/String;

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v1

    #@37
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@3a
    .line 132
    iget-object v0, p0, Lcom/lge/ims/debug/DebugService;->mDebugServiceHandler:Lcom/lge/ims/debug/DebugService$DebugServiceHandler;

    #@3c
    if-eqz v0, :cond_4

    #@3e
    .line 133
    iget-object v0, p0, Lcom/lge/ims/debug/DebugService;->mDebugServiceHandler:Lcom/lge/ims/debug/DebugService$DebugServiceHandler;

    #@40
    const/16 v1, 0x65

    #@42
    const-wide/16 v2, 0x64

    #@44
    invoke-virtual {v0, v1, v2, v3}, Lcom/lge/ims/debug/DebugService$DebugServiceHandler;->sendEmptyMessageDelayed(IJ)Z

    #@47
    goto :goto_4
.end method

.method public unregisterForCallMessageUpdated(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 161
    iget-object v0, p0, Lcom/lge/ims/debug/DebugService;->mCallMessageUpdatedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 162
    return-void
.end method

.method public unregisterForRegistrationUpdated(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 152
    iget-object v0, p0, Lcom/lge/ims/debug/DebugService;->mRegistrationUpdatedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 153
    return-void
.end method

.method public unregisterForVoLTERegStateUpdated(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 143
    iget-object v0, p0, Lcom/lge/ims/debug/DebugService;->mVoLTERegStateUpdatedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 144
    return-void
.end method
