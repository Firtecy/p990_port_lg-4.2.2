.class final Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;
.super Landroid/os/Handler;
.source "DebugScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/debug/DebugScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "DebugScreenHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/debug/DebugScreen;


# direct methods
.method public constructor <init>(Lcom/lge/ims/debug/DebugScreen;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 379
    iput-object p1, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    .line 380
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 16
    .parameter "msg"

    #@0
    .prologue
    const/4 v13, 0x0

    #@1
    .line 384
    if-nez p1, :cond_4

    #@3
    .line 528
    :goto_3
    return-void

    #@4
    .line 388
    :cond_4
    const-string v10, "DebugScreen"

    #@6
    new-instance v11, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v12, "handleMessage - "

    #@d
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v11

    #@11
    iget v12, p1, Landroid/os/Message;->what:I

    #@13
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v11

    #@17
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v11

    #@1b
    invoke-static {v10, v11}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@1e
    .line 390
    iget v10, p1, Landroid/os/Message;->what:I

    #@20
    packed-switch v10, :pswitch_data_1e2

    #@23
    .line 518
    :cond_23
    :goto_23
    new-instance v2, Landroid/os/Bundle;

    #@25
    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    #@28
    .line 519
    .local v2, b:Landroid/os/Bundle;
    const-string v10, "networkmode"

    #@2a
    iget-object v11, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@2c
    invoke-static {v11}, Lcom/lge/ims/debug/DebugScreen;->access$800(Lcom/lge/ims/debug/DebugScreen;)Ljava/lang/StringBuffer;

    #@2f
    move-result-object v11

    #@30
    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@33
    move-result-object v11

    #@34
    invoke-virtual {v2, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@37
    .line 520
    const-string v10, "voltestatus"

    #@39
    iget-object v11, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@3b
    invoke-static {v11}, Lcom/lge/ims/debug/DebugScreen;->access$900(Lcom/lge/ims/debug/DebugScreen;)Ljava/lang/StringBuffer;

    #@3e
    move-result-object v11

    #@3f
    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@42
    move-result-object v11

    #@43
    invoke-virtual {v2, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@46
    .line 521
    const-string v10, "apcsvolte"

    #@48
    iget-object v11, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@4a
    invoke-static {v11}, Lcom/lge/ims/debug/DebugScreen;->access$1000(Lcom/lge/ims/debug/DebugScreen;)Ljava/lang/StringBuffer;

    #@4d
    move-result-object v11

    #@4e
    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@51
    move-result-object v11

    #@52
    invoke-virtual {v2, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@55
    .line 522
    const-string v10, "apcsconfig"

    #@57
    iget-object v11, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@59
    invoke-static {v11}, Lcom/lge/ims/debug/DebugScreen;->access$1100(Lcom/lge/ims/debug/DebugScreen;)Ljava/lang/StringBuffer;

    #@5c
    move-result-object v11

    #@5d
    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@60
    move-result-object v11

    #@61
    invoke-virtual {v2, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@64
    .line 523
    const-string v10, "sipregi"

    #@66
    iget-object v11, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@68
    invoke-static {v11}, Lcom/lge/ims/debug/DebugScreen;->access$1200(Lcom/lge/ims/debug/DebugScreen;)Ljava/lang/StringBuffer;

    #@6b
    move-result-object v11

    #@6c
    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@6f
    move-result-object v11

    #@70
    invoke-virtual {v2, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@73
    .line 524
    const-string v10, "lastcallinfoup"

    #@75
    iget-object v11, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@77
    invoke-static {v11}, Lcom/lge/ims/debug/DebugScreen;->access$1300(Lcom/lge/ims/debug/DebugScreen;)Ljava/lang/StringBuffer;

    #@7a
    move-result-object v11

    #@7b
    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@7e
    move-result-object v11

    #@7f
    invoke-virtual {v2, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@82
    .line 525
    const-string v10, "lastcallinfodown"

    #@84
    iget-object v11, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@86
    invoke-static {v11}, Lcom/lge/ims/debug/DebugScreen;->access$1400(Lcom/lge/ims/debug/DebugScreen;)Ljava/lang/StringBuffer;

    #@89
    move-result-object v11

    #@8a
    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@8d
    move-result-object v11

    #@8e
    invoke-virtual {v2, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@91
    .line 527
    iget-object v10, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@93
    invoke-static {v10}, Lcom/lge/ims/debug/DebugScreen;->access$1500(Lcom/lge/ims/debug/DebugScreen;)Landroid/os/Handler;

    #@96
    move-result-object v10

    #@97
    iget-object v11, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@99
    invoke-static {v11}, Lcom/lge/ims/debug/DebugScreen;->access$1500(Lcom/lge/ims/debug/DebugScreen;)Landroid/os/Handler;

    #@9c
    move-result-object v11

    #@9d
    invoke-static {v11, v13, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    #@a0
    move-result-object v11

    #@a1
    invoke-virtual {v10, v11}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@a4
    goto/16 :goto_3

    #@a6
    .line 393
    .end local v2           #b:Landroid/os/Bundle;
    :pswitch_a6
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@a8
    check-cast v1, Landroid/os/AsyncResult;

    #@aa
    .line 395
    .local v1, ar:Landroid/os/AsyncResult;
    if-nez v1, :cond_b5

    #@ac
    .line 396
    const-string v10, "DebugScreen"

    #@ae
    const-string v11, "There\'s no Result from AsyncResult."

    #@b0
    invoke-static {v10, v11}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@b3
    goto/16 :goto_23

    #@b5
    .line 400
    :cond_b5
    iget-object v7, v1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@b7
    check-cast v7, Ljava/lang/Integer;

    #@b9
    .line 402
    .local v7, res:Ljava/lang/Integer;
    if-eqz v7, :cond_23

    #@bb
    .line 406
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    #@be
    move-result v5

    #@bf
    .line 407
    .local v5, rat:I
    const-string v10, "DebugScreen"

    #@c1
    new-instance v11, Ljava/lang/StringBuilder;

    #@c3
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@c6
    const-string v12, "RAT is changed. RAT : "

    #@c8
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cb
    move-result-object v11

    #@cc
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v11

    #@d0
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d3
    move-result-object v11

    #@d4
    invoke-static {v10, v11}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@d7
    .line 408
    iget-object v10, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@d9
    invoke-static {v10, v5}, Lcom/lge/ims/debug/DebugScreen;->access$000(Lcom/lge/ims/debug/DebugScreen;I)V

    #@dc
    goto/16 :goto_23

    #@de
    .line 413
    .end local v1           #ar:Landroid/os/AsyncResult;
    .end local v5           #rat:I
    .end local v7           #res:Ljava/lang/Integer;
    :pswitch_de
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@e0
    check-cast v1, Landroid/os/AsyncResult;

    #@e2
    .line 415
    .restart local v1       #ar:Landroid/os/AsyncResult;
    if-nez v1, :cond_ed

    #@e4
    .line 416
    const-string v10, "DebugScreen"

    #@e6
    const-string v11, "There\'s no Result from AsyncResult."

    #@e8
    invoke-static {v10, v11}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@eb
    goto/16 :goto_23

    #@ed
    .line 420
    :cond_ed
    iget-object v0, v1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@ef
    check-cast v0, Ljava/lang/Boolean;

    #@f1
    .line 422
    .local v0, airplane:Ljava/lang/Boolean;
    if-eqz v0, :cond_110

    #@f3
    .line 423
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    #@f6
    move-result v10

    #@f7
    if-eqz v10, :cond_107

    #@f9
    .line 424
    const-string v10, "DebugScreen"

    #@fb
    const-string v11, "Airplane Mode is changed. Airplane Mode ON"

    #@fd
    invoke-static {v10, v11}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@100
    .line 425
    iget-object v10, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@102
    invoke-static {v10, v13}, Lcom/lge/ims/debug/DebugScreen;->access$000(Lcom/lge/ims/debug/DebugScreen;I)V

    #@105
    goto/16 :goto_23

    #@107
    .line 427
    :cond_107
    const-string v10, "DebugScreen"

    #@109
    const-string v11, "Airplane Mode is changed. Airplane Mode OFF"

    #@10b
    invoke-static {v10, v11}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@10e
    goto/16 :goto_23

    #@110
    .line 430
    :cond_110
    const-string v10, "DebugScreen"

    #@112
    const-string v11, "Airplane mode is not changed.."

    #@114
    invoke-static {v10, v11}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@117
    goto/16 :goto_23

    #@119
    .line 437
    .end local v0           #airplane:Ljava/lang/Boolean;
    .end local v1           #ar:Landroid/os/AsyncResult;
    :pswitch_119
    iget-object v10, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@11b
    invoke-static {v10}, Lcom/lge/ims/debug/DebugScreen;->access$100(Lcom/lge/ims/debug/DebugScreen;)Lcom/lge/ims/debug/DebugService;

    #@11e
    move-result-object v10

    #@11f
    if-eqz v10, :cond_23

    #@121
    .line 441
    iget-object v10, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@123
    invoke-static {v10}, Lcom/lge/ims/debug/DebugScreen;->access$100(Lcom/lge/ims/debug/DebugScreen;)Lcom/lge/ims/debug/DebugService;

    #@126
    move-result-object v10

    #@127
    invoke-virtual {v10}, Lcom/lge/ims/debug/DebugService;->isVoLTERegistered()Z

    #@12a
    move-result v3

    #@12b
    .line 442
    .local v3, isRegistered:Z
    const-string v10, "DebugScreen"

    #@12d
    const-string v11, "VoLTE Status is changed."

    #@12f
    invoke-static {v10, v11}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@132
    .line 443
    iget-object v10, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@134
    invoke-static {v10}, Lcom/lge/ims/debug/DebugScreen;->access$100(Lcom/lge/ims/debug/DebugScreen;)Lcom/lge/ims/debug/DebugService;

    #@137
    move-result-object v10

    #@138
    invoke-virtual {v10, v3}, Lcom/lge/ims/debug/DebugService;->setVoLTERegState(Z)V

    #@13b
    .line 444
    iget-object v10, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@13d
    invoke-static {v10, v3}, Lcom/lge/ims/debug/DebugScreen;->access$200(Lcom/lge/ims/debug/DebugScreen;Z)V

    #@140
    goto/16 :goto_23

    #@142
    .line 450
    .end local v3           #isRegistered:Z
    :pswitch_142
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@144
    check-cast v1, Landroid/os/AsyncResult;

    #@146
    .line 452
    .restart local v1       #ar:Landroid/os/AsyncResult;
    if-nez v1, :cond_151

    #@148
    .line 453
    const-string v10, "DebugScreen"

    #@14a
    const-string v11, "There\'s no Result from AsyncResult."

    #@14c
    invoke-static {v10, v11}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@14f
    goto/16 :goto_23

    #@151
    .line 457
    :cond_151
    iget-object v9, v1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@153
    check-cast v9, Lcom/lge/ims/service/ac/ACUpdateState;

    #@155
    .line 459
    .local v9, updateState:Lcom/lge/ims/service/ac/ACUpdateState;
    if-eqz v9, :cond_23

    #@157
    .line 463
    const-string v10, "DebugScreen"

    #@159
    const-string v11, "ACS VoLTE is changed."

    #@15b
    invoke-static {v10, v11}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@15e
    .line 465
    invoke-virtual {v9}, Lcom/lge/ims/service/ac/ACUpdateState;->getProvisionedServices()I

    #@161
    move-result v4

    #@162
    .line 466
    .local v4, mProvisionedService:I
    iget-object v10, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@164
    invoke-static {v10, v4}, Lcom/lge/ims/debug/DebugScreen;->access$300(Lcom/lge/ims/debug/DebugScreen;I)V

    #@167
    goto/16 :goto_23

    #@169
    .line 471
    .end local v1           #ar:Landroid/os/AsyncResult;
    .end local v4           #mProvisionedService:I
    .end local v9           #updateState:Lcom/lge/ims/service/ac/ACUpdateState;
    :pswitch_169
    iget-object v10, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@16b
    invoke-static {v10}, Lcom/lge/ims/debug/DebugScreen;->access$400(Lcom/lge/ims/debug/DebugScreen;)Lcom/lge/ims/service/ac/ACService;

    #@16e
    move-result-object v10

    #@16f
    if-eqz v10, :cond_23

    #@171
    .line 475
    const-string v10, "DebugScreen"

    #@173
    const-string v11, "ACS Config. is changed."

    #@175
    invoke-static {v10, v11}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@178
    .line 477
    iget-object v10, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@17a
    invoke-static {v10}, Lcom/lge/ims/debug/DebugScreen;->access$400(Lcom/lge/ims/debug/DebugScreen;)Lcom/lge/ims/service/ac/ACService;

    #@17d
    move-result-object v10

    #@17e
    invoke-virtual {v10}, Lcom/lge/ims/service/ac/ACService;->getServerInterworkingResult()I

    #@181
    move-result v8

    #@182
    .line 478
    .local v8, result:I
    iget-object v10, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@184
    invoke-static {v10, v8}, Lcom/lge/ims/debug/DebugScreen;->access$500(Lcom/lge/ims/debug/DebugScreen;I)V

    #@187
    goto/16 :goto_23

    #@189
    .line 483
    .end local v8           #result:I
    :pswitch_189
    iget-object v10, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@18b
    invoke-static {v10}, Lcom/lge/ims/debug/DebugScreen;->access$100(Lcom/lge/ims/debug/DebugScreen;)Lcom/lge/ims/debug/DebugService;

    #@18e
    move-result-object v10

    #@18f
    if-eqz v10, :cond_23

    #@191
    .line 487
    const-string v10, "DebugScreen"

    #@193
    const-string v11, "SIP Regi. is changed."

    #@195
    invoke-static {v10, v11}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@198
    .line 488
    iget-object v10, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@19a
    invoke-static {v10}, Lcom/lge/ims/debug/DebugScreen;->access$100(Lcom/lge/ims/debug/DebugScreen;)Lcom/lge/ims/debug/DebugService;

    #@19d
    move-result-object v10

    #@19e
    invoke-virtual {v10}, Lcom/lge/ims/debug/DebugService;->getRegistrationResult()I

    #@1a1
    move-result v6

    #@1a2
    .line 489
    .local v6, regiResult:I
    iget-object v10, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@1a4
    invoke-static {v10, v6}, Lcom/lge/ims/debug/DebugScreen;->access$600(Lcom/lge/ims/debug/DebugScreen;I)V

    #@1a7
    goto/16 :goto_23

    #@1a9
    .line 494
    .end local v6           #regiResult:I
    :pswitch_1a9
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1ab
    check-cast v1, Landroid/os/AsyncResult;

    #@1ad
    .line 496
    .restart local v1       #ar:Landroid/os/AsyncResult;
    if-nez v1, :cond_1b8

    #@1af
    .line 497
    const-string v10, "DebugScreen"

    #@1b1
    const-string v11, "There\'s no Result from AsyncResult."

    #@1b3
    invoke-static {v10, v11}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@1b6
    goto/16 :goto_23

    #@1b8
    .line 501
    :cond_1b8
    iget-object v8, v1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@1ba
    check-cast v8, Ljava/lang/Integer;

    #@1bc
    .line 502
    .local v8, result:Ljava/lang/Integer;
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    #@1bf
    move-result v7

    #@1c0
    .line 504
    .local v7, res:I
    if-nez v7, :cond_1d0

    #@1c2
    .line 505
    const-string v10, "DebugScreen"

    #@1c4
    const-string v11, "Last Call Info.(UP) is changed."

    #@1c6
    invoke-static {v10, v11}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@1c9
    .line 506
    iget-object v10, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@1cb
    invoke-static {v10, v7}, Lcom/lge/ims/debug/DebugScreen;->access$700(Lcom/lge/ims/debug/DebugScreen;I)V

    #@1ce
    goto/16 :goto_23

    #@1d0
    .line 507
    :cond_1d0
    const/4 v10, 0x1

    #@1d1
    if-ne v7, v10, :cond_23

    #@1d3
    .line 508
    const-string v10, "DebugScreen"

    #@1d5
    const-string v11, "Last Call Info.(DOWN) is changed."

    #@1d7
    invoke-static {v10, v11}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@1da
    .line 509
    iget-object v10, p0, Lcom/lge/ims/debug/DebugScreen$DebugScreenHandler;->this$0:Lcom/lge/ims/debug/DebugScreen;

    #@1dc
    invoke-static {v10, v7}, Lcom/lge/ims/debug/DebugScreen;->access$700(Lcom/lge/ims/debug/DebugScreen;I)V

    #@1df
    goto/16 :goto_23

    #@1e1
    .line 390
    nop

    #@1e2
    :pswitch_data_1e2
    .packed-switch 0x65
        :pswitch_a6
        :pswitch_de
        :pswitch_119
        :pswitch_142
        :pswitch_189
        :pswitch_1a9
        :pswitch_169
        :pswitch_142
    .end packed-switch
.end method
