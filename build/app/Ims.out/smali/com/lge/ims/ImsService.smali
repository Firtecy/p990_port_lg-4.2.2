.class public Lcom/lge/ims/ImsService;
.super Landroid/app/Service;
.source "ImsService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/ImsService$StopServiceHandler;
    }
.end annotation


# static fields
.field private static final START_FOREGROUND_SIGNATURE:[Ljava/lang/Class; = null

.field private static final STOP_FOREGROUND_SIGNATURE:[Ljava/lang/Class; = null

.field private static final TAG:Ljava/lang/String; = "ImsService"

.field public static bStarted:Z

.field public static mContext:Landroid/content/Context;

.field public static sysDispatcher:Lcom/lge/ims/SysDispatcher;

.field public static sysMonitor:Lcom/lge/ims/SysMonitor;


# instance fields
.field public mHandler:Landroid/os/Handler;

.field private mNM:Landroid/app/NotificationManager;

.field private mStartForeground:Ljava/lang/reflect/Method;

.field private mStartForegroundArgs:[Ljava/lang/Object;

.field private mStopForeground:Ljava/lang/reflect/Method;

.field private mStopForegroundArgs:[Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v0, 0x0

    #@2
    const/4 v2, 0x0

    #@3
    .line 30
    sput-object v0, Lcom/lge/ims/ImsService;->sysMonitor:Lcom/lge/ims/SysMonitor;

    #@5
    .line 31
    sput-object v0, Lcom/lge/ims/ImsService;->sysDispatcher:Lcom/lge/ims/SysDispatcher;

    #@7
    .line 33
    sput-boolean v2, Lcom/lge/ims/ImsService;->bStarted:Z

    #@9
    .line 34
    sput-object v0, Lcom/lge/ims/ImsService;->mContext:Landroid/content/Context;

    #@b
    .line 43
    const/4 v0, 0x2

    #@c
    new-array v0, v0, [Ljava/lang/Class;

    #@e
    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@10
    aput-object v1, v0, v2

    #@12
    const-class v1, Landroid/app/Notification;

    #@14
    aput-object v1, v0, v3

    #@16
    sput-object v0, Lcom/lge/ims/ImsService;->START_FOREGROUND_SIGNATURE:[Ljava/lang/Class;

    #@18
    .line 44
    new-array v0, v3, [Ljava/lang/Class;

    #@1a
    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    #@1c
    aput-object v1, v0, v2

    #@1e
    sput-object v0, Lcom/lge/ims/ImsService;->STOP_FOREGROUND_SIGNATURE:[Ljava/lang/Class;

    #@20
    .line 47
    const-string v0, "ImsService"

    #@22
    const-string v1, "System.loadLibrary: libims.so"

    #@24
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@27
    .line 48
    const-string v0, "ims"

    #@29
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@2c
    .line 49
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 51
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    #@3
    .line 36
    new-instance v0, Landroid/os/Handler;

    #@5
    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    #@8
    iput-object v0, p0, Lcom/lge/ims/ImsService;->mHandler:Landroid/os/Handler;

    #@a
    .line 41
    const/4 v0, 0x2

    #@b
    new-array v0, v0, [Ljava/lang/Object;

    #@d
    iput-object v0, p0, Lcom/lge/ims/ImsService;->mStartForegroundArgs:[Ljava/lang/Object;

    #@f
    .line 42
    const/4 v0, 0x1

    #@10
    new-array v0, v0, [Ljava/lang/Object;

    #@12
    iput-object v0, p0, Lcom/lge/ims/ImsService;->mStopForegroundArgs:[Ljava/lang/Object;

    #@14
    .line 52
    return-void
.end method

.method private static setInstance(Lcom/lge/ims/SysMonitor;Lcom/lge/ims/SysDispatcher;)V
    .registers 2
    .parameter "_sysMonitor"
    .parameter "_sysDispatcher"

    #@0
    .prologue
    .line 182
    sput-object p1, Lcom/lge/ims/ImsService;->sysDispatcher:Lcom/lge/ims/SysDispatcher;

    #@2
    .line 183
    sput-object p0, Lcom/lge/ims/ImsService;->sysMonitor:Lcom/lge/ims/SysMonitor;

    #@4
    .line 184
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 4
    .parameter "intent"

    #@0
    .prologue
    .line 288
    const-string v0, "ImsService"

    #@2
    const-string v1, "onBind!!"

    #@4
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 290
    const/4 v0, 0x0

    #@8
    return-object v0
.end method

.method public onCreate()V
    .registers 21

    #@0
    .prologue
    .line 56
    invoke-super/range {p0 .. p0}, Landroid/app/Service;->onCreate()V

    #@3
    .line 58
    sput-object p0, Lcom/lge/ims/ImsService;->mContext:Landroid/content/Context;

    #@5
    .line 61
    sget-object v17, Lcom/lge/ims/ImsService;->mContext:Landroid/content/Context;

    #@7
    invoke-static/range {v17 .. v17}, Lcom/lge/ims/ImsLog;->initConfig(Landroid/content/Context;)V

    #@a
    .line 62
    sget-object v17, Lcom/lge/ims/ImsService;->mContext:Landroid/content/Context;

    #@c
    invoke-static/range {v17 .. v17}, Lcom/lge/ims/Configuration;->init(Landroid/content/Context;)V

    #@f
    .line 64
    new-instance v17, Landroid/content/Intent;

    #@11
    const-string v18, "com.lge.ims.action.IMS_STARTED"

    #@13
    invoke-direct/range {v17 .. v18}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@16
    move-object/from16 v0, p0

    #@18
    move-object/from16 v1, v17

    #@1a
    invoke-virtual {v0, v1}, Lcom/lge/ims/ImsService;->sendBroadcast(Landroid/content/Intent;)V

    #@1d
    .line 66
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getInstance()Lcom/lge/ims/SystemServiceManager;

    #@20
    move-result-object v14

    #@21
    .line 68
    .local v14, ssm:Lcom/lge/ims/SystemServiceManager;
    if-eqz v14, :cond_2a

    #@23
    .line 69
    sget-object v17, Lcom/lge/ims/ImsService;->mContext:Landroid/content/Context;

    #@25
    move-object/from16 v0, v17

    #@27
    invoke-virtual {v14, v0}, Lcom/lge/ims/SystemServiceManager;->setContext(Landroid/content/Context;)V

    #@2a
    .line 72
    :cond_2a
    invoke-static {}, Lcom/lge/ims/BatteryStateTracker;->getInstance()Lcom/lge/ims/BatteryStateTracker;

    #@2d
    move-result-object v4

    #@2e
    .line 74
    .local v4, bsTracker:Lcom/lge/ims/BatteryStateTracker;
    if-eqz v4, :cond_37

    #@30
    .line 75
    sget-object v17, Lcom/lge/ims/ImsService;->mContext:Landroid/content/Context;

    #@32
    move-object/from16 v0, v17

    #@34
    invoke-virtual {v4, v0}, Lcom/lge/ims/BatteryStateTracker;->setContext(Landroid/content/Context;)V

    #@37
    .line 78
    :cond_37
    invoke-static {}, Lcom/lge/ims/AlarmTimerManager;->getInstance()Lcom/lge/ims/AlarmTimerManager;

    #@3a
    move-result-object v3

    #@3b
    .line 80
    .local v3, atm:Lcom/lge/ims/AlarmTimerManager;
    if-eqz v3, :cond_44

    #@3d
    .line 81
    sget-object v17, Lcom/lge/ims/ImsService;->mContext:Landroid/content/Context;

    #@3f
    move-object/from16 v0, v17

    #@41
    invoke-virtual {v3, v0}, Lcom/lge/ims/AlarmTimerManager;->setContext(Landroid/content/Context;)V

    #@44
    .line 84
    :cond_44
    invoke-static {}, Lcom/lge/ims/PhoneStateTracker;->getInstance()Lcom/lge/ims/PhoneStateTracker;

    #@47
    move-result-object v13

    #@48
    .line 86
    .local v13, pst:Lcom/lge/ims/PhoneStateTracker;
    if-eqz v13, :cond_54

    #@4a
    .line 87
    invoke-virtual {v13}, Lcom/lge/ims/PhoneStateTracker;->create()V

    #@4d
    .line 88
    sget-object v17, Lcom/lge/ims/ImsService;->mContext:Landroid/content/Context;

    #@4f
    move-object/from16 v0, v17

    #@51
    invoke-virtual {v13, v0}, Lcom/lge/ims/PhoneStateTracker;->setContext(Landroid/content/Context;)V

    #@54
    .line 91
    :cond_54
    invoke-static {}, Lcom/lge/ims/DataConnectionManager;->getInstance()Lcom/lge/ims/DataConnectionManager;

    #@57
    move-result-object v6

    #@58
    .line 93
    .local v6, dcm:Lcom/lge/ims/DataConnectionManager;
    if-eqz v6, :cond_68

    #@5a
    .line 94
    sget-object v17, Lcom/lge/ims/ImsService;->mContext:Landroid/content/Context;

    #@5c
    move-object/from16 v0, v17

    #@5e
    invoke-virtual {v6, v0}, Lcom/lge/ims/DataConnectionManager;->create(Landroid/content/Context;)V

    #@61
    .line 95
    sget-object v17, Lcom/lge/ims/ImsService;->mContext:Landroid/content/Context;

    #@63
    move-object/from16 v0, v17

    #@65
    invoke-virtual {v6, v0}, Lcom/lge/ims/DataConnectionManager;->setContext(Landroid/content/Context;)V

    #@68
    .line 98
    :cond_68
    invoke-static {}, Lcom/lge/ims/debug/DebugService;->getInstance()Lcom/lge/ims/debug/DebugService;

    #@6b
    move-result-object v7

    #@6c
    .line 100
    .local v7, ds:Lcom/lge/ims/debug/DebugService;
    if-eqz v7, :cond_75

    #@6e
    .line 101
    sget-object v17, Lcom/lge/ims/ImsService;->mContext:Landroid/content/Context;

    #@70
    move-object/from16 v0, v17

    #@72
    invoke-virtual {v7, v0}, Lcom/lge/ims/debug/DebugService;->create(Landroid/content/Context;)V

    #@75
    .line 104
    :cond_75
    invoke-static {}, Lcom/lge/ims/PhoneSettingsTracker;->getInstance()Lcom/lge/ims/PhoneSettingsTracker;

    #@78
    move-result-object v12

    #@79
    .line 105
    .local v12, psettingst:Lcom/lge/ims/PhoneSettingsTracker;
    if-eqz v12, :cond_82

    #@7b
    .line 106
    sget-object v17, Lcom/lge/ims/ImsService;->mContext:Landroid/content/Context;

    #@7d
    move-object/from16 v0, v17

    #@7f
    invoke-virtual {v12, v0}, Lcom/lge/ims/PhoneSettingsTracker;->setContext(Landroid/content/Context;)V

    #@82
    .line 109
    :cond_82
    new-instance v16, Lcom/lge/ims/SysMonitor;

    #@84
    sget-object v17, Lcom/lge/ims/ImsService;->mContext:Landroid/content/Context;

    #@86
    invoke-direct/range {v16 .. v17}, Lcom/lge/ims/SysMonitor;-><init>(Landroid/content/Context;)V

    #@89
    .line 110
    .local v16, sysMonitor:Lcom/lge/ims/SysMonitor;
    new-instance v15, Lcom/lge/ims/SysDispatcher;

    #@8b
    sget-object v17, Lcom/lge/ims/ImsService;->mContext:Landroid/content/Context;

    #@8d
    move-object/from16 v0, v17

    #@8f
    invoke-direct {v15, v0}, Lcom/lge/ims/SysDispatcher;-><init>(Landroid/content/Context;)V

    #@92
    .line 112
    .local v15, sysDispatcher:Lcom/lge/ims/SysDispatcher;
    move-object/from16 v0, v16

    #@94
    invoke-static {v0, v15}, Lcom/lge/ims/ImsService;->setInstance(Lcom/lge/ims/SysMonitor;Lcom/lge/ims/SysDispatcher;)V

    #@97
    .line 115
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->rebindIMSPhone()V

    #@9a
    .line 117
    sget-boolean v17, Lcom/lge/ims/ImsService;->bStarted:Z

    #@9c
    if-nez v17, :cond_176

    #@9e
    .line 118
    const-string v17, "ImsService"

    #@a0
    const-string v18, "ImsService start"

    #@a2
    invoke-static/range {v17 .. v18}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@a5
    .line 121
    const-string v17, "notification"

    #@a7
    move-object/from16 v0, p0

    #@a9
    move-object/from16 v1, v17

    #@ab
    invoke-virtual {v0, v1}, Lcom/lge/ims/ImsService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@ae
    move-result-object v17

    #@af
    check-cast v17, Landroid/app/NotificationManager;

    #@b1
    move-object/from16 v0, v17

    #@b3
    move-object/from16 v1, p0

    #@b5
    iput-object v0, v1, Lcom/lge/ims/ImsService;->mNM:Landroid/app/NotificationManager;

    #@b7
    .line 124
    :try_start_b7
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@ba
    move-result-object v17

    #@bb
    const-string v18, "startForeground"

    #@bd
    sget-object v19, Lcom/lge/ims/ImsService;->START_FOREGROUND_SIGNATURE:[Ljava/lang/Class;

    #@bf
    invoke-virtual/range {v17 .. v19}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@c2
    move-result-object v17

    #@c3
    move-object/from16 v0, v17

    #@c5
    move-object/from16 v1, p0

    #@c7
    iput-object v0, v1, Lcom/lge/ims/ImsService;->mStartForeground:Ljava/lang/reflect/Method;

    #@c9
    .line 125
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@cc
    move-result-object v17

    #@cd
    const-string v18, "stopForeground"

    #@cf
    sget-object v19, Lcom/lge/ims/ImsService;->STOP_FOREGROUND_SIGNATURE:[Ljava/lang/Class;

    #@d1
    invoke-virtual/range {v17 .. v19}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@d4
    move-result-object v17

    #@d5
    move-object/from16 v0, v17

    #@d7
    move-object/from16 v1, p0

    #@d9
    iput-object v0, v1, Lcom/lge/ims/ImsService;->mStopForeground:Ljava/lang/reflect/Method;

    #@db
    .line 126
    const-string v17, "ImsService"

    #@dd
    const-string v18, "onCreate : getMethod"

    #@df
    invoke-static/range {v17 .. v18}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_e2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_b7 .. :try_end_e2} :catch_15a

    #@e2
    .line 133
    :goto_e2
    new-instance v9, Landroid/app/Notification;

    #@e4
    invoke-direct {v9}, Landroid/app/Notification;-><init>()V

    #@e7
    .line 135
    .local v9, notification:Landroid/app/Notification;
    const/16 v17, 0x3

    #@e9
    move-object/from16 v0, p0

    #@eb
    move/from16 v1, v17

    #@ed
    invoke-virtual {v0, v1, v9}, Lcom/lge/ims/ImsService;->startForegroundCompat(ILandroid/app/Notification;)V

    #@f0
    .line 138
    invoke-static {}, Lcom/lge/ims/JNICore;->construct()V

    #@f3
    .line 139
    const/16 v17, 0x1

    #@f5
    sput-boolean v17, Lcom/lge/ims/ImsService;->bStarted:Z

    #@f7
    .line 141
    invoke-static {}, Lcom/lge/ims/Configuration;->isACOn()Z

    #@fa
    move-result v17

    #@fb
    if-eqz v17, :cond_10a

    #@fd
    .line 142
    invoke-static {}, Lcom/lge/ims/service/ac/ACService;->getInstance()Lcom/lge/ims/service/ac/ACService;

    #@100
    move-result-object v2

    #@101
    .line 144
    .local v2, acs:Lcom/lge/ims/service/ac/ACService;
    if-eqz v2, :cond_10a

    #@103
    .line 145
    sget-object v17, Lcom/lge/ims/ImsService;->mContext:Landroid/content/Context;

    #@105
    move-object/from16 v0, v17

    #@107
    invoke-virtual {v2, v0}, Lcom/lge/ims/service/ac/ACService;->start(Landroid/content/Context;)V

    #@10a
    .line 149
    .end local v2           #acs:Lcom/lge/ims/service/ac/ACService;
    :cond_10a
    invoke-static {}, Lcom/lge/ims/Configuration;->isRCSeOn()Z

    #@10d
    move-result v17

    #@10e
    if-eqz v17, :cond_159

    #@110
    .line 150
    invoke-static {}, Lcom/lge/ims/service/cd/CapabilityService;->getInstance()Lcom/lge/ims/service/cd/CapabilityService;

    #@113
    move-result-object v5

    #@114
    .line 152
    .local v5, cs:Lcom/lge/ims/service/cd/CapabilityService;
    if-eqz v5, :cond_11d

    #@116
    .line 153
    sget-object v17, Lcom/lge/ims/ImsService;->mContext:Landroid/content/Context;

    #@118
    move-object/from16 v0, v17

    #@11a
    invoke-virtual {v5, v0}, Lcom/lge/ims/service/cd/CapabilityService;->start(Landroid/content/Context;)V

    #@11d
    .line 156
    :cond_11d
    invoke-static {}, Lcom/lge/ims/service/cd/CapabilityServiceThread;->isTestModeEnabled()Z

    #@120
    move-result v17

    #@121
    if-eqz v17, :cond_126

    #@123
    .line 157
    invoke-static {}, Lcom/lge/ims/service/cd/CapabilityServiceThread;->getInstance()Lcom/lge/ims/service/cd/CapabilityServiceThread;

    #@126
    .line 160
    :cond_126
    sget-object v17, Lcom/lge/ims/ImsService;->mContext:Landroid/content/Context;

    #@128
    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    #@12b
    move-result-object v17

    #@12c
    invoke-static/range {v17 .. v17}, Lcom/lge/ims/service/ip/IPListenerThread;->CreateIPListenerThread(Landroid/content/Context;)V

    #@12f
    .line 161
    invoke-static {}, Lcom/lge/ims/service/ip/IPListenerThread;->GetIPListenerThread()Lcom/lge/ims/service/ip/IPListenerThread;

    #@132
    move-result-object v11

    #@133
    .line 162
    .local v11, objIPListenerThread:Lcom/lge/ims/service/ip/IPListenerThread;
    if-eqz v11, :cond_13e

    #@135
    .line 163
    invoke-virtual {v11}, Lcom/lge/ims/service/ip/IPListenerThread;->isAlive()Z

    #@138
    move-result v17

    #@139
    if-nez v17, :cond_13e

    #@13b
    .line 164
    invoke-virtual {v11}, Lcom/lge/ims/service/ip/IPListenerThread;->start()V

    #@13e
    .line 167
    :cond_13e
    sget-object v17, Lcom/lge/ims/ImsService;->mContext:Landroid/content/Context;

    #@140
    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    #@143
    move-result-object v17

    #@144
    invoke-static/range {v17 .. v17}, Lcom/lge/ims/service/ip/IPAgent;->CreateIPAgent(Landroid/content/Context;)V

    #@147
    .line 168
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@14a
    move-result-object v10

    #@14b
    .line 169
    .local v10, objIPAgent:Lcom/lge/ims/service/ip/IPAgent;
    if-eqz v10, :cond_159

    #@14d
    .line 170
    invoke-virtual {v10}, Lcom/lge/ims/service/ip/IPAgent;->isAlive()Z

    #@150
    move-result v17

    #@151
    if-nez v17, :cond_156

    #@153
    .line 171
    invoke-virtual {v10}, Lcom/lge/ims/service/ip/IPAgent;->start()V

    #@156
    .line 173
    :cond_156
    invoke-virtual {v10}, Lcom/lge/ims/service/ip/IPAgent;->ConnectNativeBind()V

    #@159
    .line 179
    .end local v5           #cs:Lcom/lge/ims/service/cd/CapabilityService;
    .end local v9           #notification:Landroid/app/Notification;
    .end local v10           #objIPAgent:Lcom/lge/ims/service/ip/IPAgent;
    .end local v11           #objIPListenerThread:Lcom/lge/ims/service/ip/IPListenerThread;
    :cond_159
    :goto_159
    return-void

    #@15a
    .line 127
    :catch_15a
    move-exception v8

    #@15b
    .line 129
    .local v8, e:Ljava/lang/NoSuchMethodException;
    const/16 v17, 0x0

    #@15d
    move-object/from16 v0, v17

    #@15f
    move-object/from16 v1, p0

    #@161
    iput-object v0, v1, Lcom/lge/ims/ImsService;->mStopForeground:Ljava/lang/reflect/Method;

    #@163
    move-object/from16 v0, v17

    #@165
    move-object/from16 v1, p0

    #@167
    iput-object v0, v1, Lcom/lge/ims/ImsService;->mStartForeground:Ljava/lang/reflect/Method;

    #@169
    .line 130
    const-string v17, "ImsService"

    #@16b
    const-string v18, "onCreate : NoSuchMethodException"

    #@16d
    move-object/from16 v0, v17

    #@16f
    move-object/from16 v1, v18

    #@171
    invoke-static {v0, v1, v8}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    #@174
    goto/16 :goto_e2

    #@176
    .line 177
    .end local v8           #e:Ljava/lang/NoSuchMethodException;
    :cond_176
    const-string v17, "ImsService"

    #@178
    const-string v18, "ImsService is already started"

    #@17a
    invoke-static/range {v17 .. v18}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@17d
    goto :goto_159
.end method

.method public onDestroy()V
    .registers 12

    #@0
    .prologue
    .line 188
    const-string v7, "ImsService"

    #@2
    const-string v8, "ImsService destroy"

    #@4
    invoke-static {v7, v8}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 190
    const/4 v7, 0x0

    #@8
    sput-boolean v7, Lcom/lge/ims/ImsService;->bStarted:Z

    #@a
    .line 192
    invoke-static {}, Lcom/lge/ims/Configuration;->isACOn()Z

    #@d
    move-result v7

    #@e
    if-eqz v7, :cond_19

    #@10
    .line 193
    invoke-static {}, Lcom/lge/ims/service/ac/ACService;->getInstance()Lcom/lge/ims/service/ac/ACService;

    #@13
    move-result-object v0

    #@14
    .line 195
    .local v0, acs:Lcom/lge/ims/service/ac/ACService;
    if-eqz v0, :cond_19

    #@16
    .line 196
    invoke-virtual {v0}, Lcom/lge/ims/service/ac/ACService;->destroy()V

    #@19
    .line 200
    .end local v0           #acs:Lcom/lge/ims/service/ac/ACService;
    :cond_19
    invoke-static {}, Lcom/lge/ims/debug/DebugService;->getInstance()Lcom/lge/ims/debug/DebugService;

    #@1c
    move-result-object v4

    #@1d
    .line 202
    .local v4, ds:Lcom/lge/ims/debug/DebugService;
    if-eqz v4, :cond_22

    #@1f
    .line 203
    invoke-virtual {v4}, Lcom/lge/ims/debug/DebugService;->destroy()V

    #@22
    .line 206
    :cond_22
    invoke-static {}, Lcom/lge/ims/DataConnectionManager;->getInstance()Lcom/lge/ims/DataConnectionManager;

    #@25
    move-result-object v3

    #@26
    .line 208
    .local v3, dcm:Lcom/lge/ims/DataConnectionManager;
    if-eqz v3, :cond_2b

    #@28
    .line 209
    invoke-virtual {v3}, Lcom/lge/ims/DataConnectionManager;->destroy()V

    #@2b
    .line 212
    :cond_2b
    invoke-static {}, Lcom/lge/ims/PhoneStateTracker;->getInstance()Lcom/lge/ims/PhoneStateTracker;

    #@2e
    move-result-object v5

    #@2f
    .line 214
    .local v5, pst:Lcom/lge/ims/PhoneStateTracker;
    if-eqz v5, :cond_34

    #@31
    .line 215
    invoke-virtual {v5}, Lcom/lge/ims/PhoneStateTracker;->destroy()V

    #@34
    .line 218
    :cond_34
    invoke-static {}, Lcom/lge/ims/AlarmTimerManager;->getInstance()Lcom/lge/ims/AlarmTimerManager;

    #@37
    move-result-object v1

    #@38
    .line 220
    .local v1, atm:Lcom/lge/ims/AlarmTimerManager;
    if-eqz v1, :cond_3d

    #@3a
    .line 221
    invoke-virtual {v1}, Lcom/lge/ims/AlarmTimerManager;->destroy()V

    #@3d
    .line 224
    :cond_3d
    invoke-static {}, Lcom/lge/ims/BatteryStateTracker;->getInstance()Lcom/lge/ims/BatteryStateTracker;

    #@40
    move-result-object v2

    #@41
    .line 226
    .local v2, bsTracker:Lcom/lge/ims/BatteryStateTracker;
    if-eqz v2, :cond_46

    #@43
    .line 227
    invoke-virtual {v2}, Lcom/lge/ims/BatteryStateTracker;->destroy()V

    #@46
    .line 230
    :cond_46
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getInstance()Lcom/lge/ims/SystemServiceManager;

    #@49
    move-result-object v6

    #@4a
    .line 232
    .local v6, ssm:Lcom/lge/ims/SystemServiceManager;
    if-eqz v6, :cond_4f

    #@4c
    .line 233
    invoke-virtual {v6}, Lcom/lge/ims/SystemServiceManager;->destroy()V

    #@4f
    .line 236
    :cond_4f
    sget-object v7, Lcom/lge/ims/ImsService;->sysMonitor:Lcom/lge/ims/SysMonitor;

    #@51
    invoke-virtual {v7}, Lcom/lge/ims/SysMonitor;->destroy()V

    #@54
    .line 237
    const/4 v7, 0x3

    #@55
    invoke-virtual {p0, v7}, Lcom/lge/ims/ImsService;->stopForegroundCompat(I)V

    #@58
    .line 238
    iget-object v7, p0, Lcom/lge/ims/ImsService;->mHandler:Landroid/os/Handler;

    #@5a
    new-instance v8, Lcom/lge/ims/ImsService$StopServiceHandler;

    #@5c
    const/4 v9, 0x1

    #@5d
    invoke-direct {v8, p0, v9}, Lcom/lge/ims/ImsService$StopServiceHandler;-><init>(Lcom/lge/ims/ImsService;I)V

    #@60
    const-wide/16 v9, 0x3e8

    #@62
    invoke-virtual {v7, v8, v9, v10}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@65
    .line 239
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .registers 4
    .parameter "intent"

    #@0
    .prologue
    .line 294
    const-string v0, "ImsService"

    #@2
    const-string v1, "onUnbind!!"

    #@4
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 296
    const/4 v0, 0x1

    #@8
    return v0
.end method

.method startForegroundCompat(ILandroid/app/Notification;)V
    .registers 8
    .parameter "id"
    .parameter "notification"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 243
    iget-object v1, p0, Lcom/lge/ims/ImsService;->mStartForeground:Ljava/lang/reflect/Method;

    #@3
    if-eqz v1, :cond_3a

    #@5
    .line 244
    const-string v1, "ImsService"

    #@7
    const-string v2, "startForegroundCompat : mStartForeground is not null"

    #@9
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@c
    .line 245
    iget-object v1, p0, Lcom/lge/ims/ImsService;->mStartForegroundArgs:[Ljava/lang/Object;

    #@e
    const/4 v2, 0x0

    #@f
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@12
    move-result-object v3

    #@13
    aput-object v3, v1, v2

    #@15
    .line 246
    iget-object v1, p0, Lcom/lge/ims/ImsService;->mStartForegroundArgs:[Ljava/lang/Object;

    #@17
    aput-object p2, v1, v4

    #@19
    .line 248
    :try_start_19
    iget-object v1, p0, Lcom/lge/ims/ImsService;->mStartForeground:Ljava/lang/reflect/Method;

    #@1b
    iget-object v2, p0, Lcom/lge/ims/ImsService;->mStartForegroundArgs:[Ljava/lang/Object;

    #@1d
    invoke-virtual {v1, p0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    #@20
    .line 249
    const-string v1, "ImsService"

    #@22
    const-string v2, "startForegroundCompat : invoke"

    #@24
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_27
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_19 .. :try_end_27} :catch_28
    .catch Ljava/lang/IllegalAccessException; {:try_start_19 .. :try_end_27} :catch_31

    #@27
    .line 263
    :goto_27
    return-void

    #@28
    .line 250
    :catch_28
    move-exception v0

    #@29
    .line 252
    .local v0, e:Ljava/lang/reflect/InvocationTargetException;
    const-string v1, "ImsService"

    #@2b
    const-string v2, "startForegroundCompat : Unable to invoke startForeground!!"

    #@2d
    invoke-static {v1, v2, v0}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    #@30
    goto :goto_27

    #@31
    .line 253
    .end local v0           #e:Ljava/lang/reflect/InvocationTargetException;
    :catch_31
    move-exception v0

    #@32
    .line 255
    .local v0, e:Ljava/lang/IllegalAccessException;
    const-string v1, "ImsService"

    #@34
    const-string v2, "startForegroundCompat : Unable to invoke startForeground!!"

    #@36
    invoke-static {v1, v2, v0}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    #@39
    goto :goto_27

    #@3a
    .line 261
    .end local v0           #e:Ljava/lang/IllegalAccessException;
    :cond_3a
    invoke-virtual {p0, v4}, Lcom/lge/ims/ImsService;->setForeground(Z)V

    #@3d
    .line 262
    iget-object v1, p0, Lcom/lge/ims/ImsService;->mNM:Landroid/app/NotificationManager;

    #@3f
    invoke-virtual {v1, p1, p2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    #@42
    goto :goto_27
.end method

.method stopForegroundCompat(I)V
    .registers 6
    .parameter "id"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 267
    iget-object v1, p0, Lcom/lge/ims/ImsService;->mStopForeground:Ljava/lang/reflect/Method;

    #@3
    if-eqz v1, :cond_25

    #@5
    .line 268
    iget-object v1, p0, Lcom/lge/ims/ImsService;->mStopForegroundArgs:[Ljava/lang/Object;

    #@7
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    #@9
    aput-object v2, v1, v3

    #@b
    .line 270
    :try_start_b
    iget-object v1, p0, Lcom/lge/ims/ImsService;->mStopForeground:Ljava/lang/reflect/Method;

    #@d
    iget-object v2, p0, Lcom/lge/ims/ImsService;->mStopForegroundArgs:[Ljava/lang/Object;

    #@f
    invoke-virtual {v1, p0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_12
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_b .. :try_end_12} :catch_13
    .catch Ljava/lang/IllegalAccessException; {:try_start_b .. :try_end_12} :catch_1c

    #@12
    .line 285
    :goto_12
    return-void

    #@13
    .line 271
    :catch_13
    move-exception v0

    #@14
    .line 272
    .local v0, e:Ljava/lang/reflect/InvocationTargetException;
    const-string v1, "ImsService"

    #@16
    const-string v2, "Unable to invoke stopForeground!!"

    #@18
    invoke-static {v1, v2, v0}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    #@1b
    goto :goto_12

    #@1c
    .line 274
    .end local v0           #e:Ljava/lang/reflect/InvocationTargetException;
    :catch_1c
    move-exception v0

    #@1d
    .line 276
    .local v0, e:Ljava/lang/IllegalAccessException;
    const-string v1, "ImsService"

    #@1f
    const-string v2, "Unable to invoke stopForeground!!"

    #@21
    invoke-static {v1, v2, v0}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    #@24
    goto :goto_12

    #@25
    .line 283
    .end local v0           #e:Ljava/lang/IllegalAccessException;
    :cond_25
    iget-object v1, p0, Lcom/lge/ims/ImsService;->mNM:Landroid/app/NotificationManager;

    #@27
    invoke-virtual {v1, p1}, Landroid/app/NotificationManager;->cancel(I)V

    #@2a
    .line 284
    invoke-virtual {p0, v3}, Lcom/lge/ims/ImsService;->setForeground(Z)V

    #@2d
    goto :goto_12
.end method
