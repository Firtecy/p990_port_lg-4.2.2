.class Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;
.super Ljava/lang/Object;
.source "MediaSettingsCarrier.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/setting/MediaSettingsCarrier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CarrierVoLTEChangeListener"
.end annotation


# instance fields
.field m_audioCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;

.field final synthetic this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;


# direct methods
.method public constructor <init>(Lcom/lge/ims/setting/MediaSettingsCarrier;Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;)V
    .registers 5
    .parameter
    .parameter "command"

    #@0
    .prologue
    .line 844
    iput-object p1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 845
    iput-object p2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->m_audioCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;

    #@7
    .line 848
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->m_audioCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;

    #@9
    const-string v1, "lgims_com_media_audio_codec_volte"

    #@b
    invoke-virtual {v0, v1}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->loadDB(Ljava/lang/String;)V

    #@e
    .line 849
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->m_audioCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;

    #@10
    invoke-virtual {v0, p0}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->setAudioListener(Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;)V

    #@13
    .line 850
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->m_audioCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;

    #@15
    const-string v1, "lgims_com_media_audio_codec_volte"

    #@17
    invoke-virtual {v0, v1}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->updatePreference(Ljava/lang/String;)V

    #@1a
    .line 851
    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .registers 12
    .parameter "preference"
    .parameter "newValue"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/16 v8, 0x200

    #@3
    .line 855
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    #@6
    move-result-object v2

    #@7
    .line 856
    .local v2, key:Ljava/lang/String;
    const/4 v1, 0x0

    #@8
    .line 857
    .local v1, isUpdate:Z
    const-string v4, "IMSMediaSettingsCarrier"

    #@a
    new-instance v5, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v6, "onPreferenceChange - key : "

    #@11
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v5

    #@15
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v5

    #@19
    const-string v6, ", value : "

    #@1b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v5

    #@1f
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@22
    move-result-object v6

    #@23
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v5

    #@27
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v5

    #@2b
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@2e
    .line 859
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@30
    const v5, 0x7f0602cc

    #@33
    invoke-virtual {v4, v5}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@36
    move-result-object v4

    #@37
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3a
    move-result v4

    #@3b
    if-eqz v4, :cond_88

    #@3d
    .line 860
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->m_audioCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;

    #@3f
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@42
    move-result-object v4

    #@43
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@46
    move-result v4

    #@47
    invoke-virtual {v3, v4}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->addCodecList(I)V

    #@4a
    .line 917
    :cond_4a
    :goto_4a
    if-nez v1, :cond_7a

    #@4c
    .line 919
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->m_audioCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;

    #@4e
    const-string v4, "lgims_com_media_audio_codec_volte"

    #@50
    invoke-virtual {v3, v4}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->clearCodecDB(Ljava/lang/String;)V

    #@53
    .line 920
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->m_audioCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;

    #@55
    const-string v4, "lgims_com_media_audio_codec_volte"

    #@57
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@5a
    move-result-object v5

    #@5b
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@5e
    move-result v5

    #@5f
    invoke-virtual {v3, v4, v5}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->updateCodecListDB(Ljava/lang/String;I)Z

    #@62
    move-result v1

    #@63
    .line 921
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->m_audioCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;

    #@65
    const-string v4, "lgims_com_media_audio_codec_vt"

    #@67
    invoke-virtual {v3, v4}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->clearCodecDB(Ljava/lang/String;)V

    #@6a
    .line 922
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->m_audioCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;

    #@6c
    const-string v4, "lgims_com_media_audio_codec_vt"

    #@6e
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@71
    move-result-object v5

    #@72
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@75
    move-result v5

    #@76
    invoke-virtual {v3, v4, v5}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->updateCodecListDB(Ljava/lang/String;I)Z

    #@79
    move-result v1

    #@7a
    .line 925
    :cond_7a
    if-eqz v1, :cond_86

    #@7c
    .line 926
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@7e
    invoke-virtual {v3, p1, p2}, Lcom/lge/ims/setting/MediaSettingsCarrier;->summaryChange(Landroid/preference/Preference;Ljava/lang/Object;)V

    #@81
    .line 927
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@83
    invoke-static {v3, v8}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$676(Lcom/lge/ims/setting/MediaSettingsCarrier;I)I

    #@86
    :cond_86
    move v3, v1

    #@87
    .line 930
    :goto_87
    return v3

    #@88
    .line 862
    :cond_88
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@8a
    const v5, 0x7f0602cd

    #@8d
    invoke-virtual {v4, v5}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@90
    move-result-object v4

    #@91
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@94
    move-result v4

    #@95
    if-eqz v4, :cond_a5

    #@97
    .line 863
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->m_audioCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;

    #@99
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@9c
    move-result-object v4

    #@9d
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@a0
    move-result v4

    #@a1
    invoke-virtual {v3, v4}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->addPayloadFormatList(I)V

    #@a4
    goto :goto_4a

    #@a5
    .line 865
    :cond_a5
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@a7
    const v5, 0x7f060170

    #@aa
    invoke-virtual {v4, v5}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@ad
    move-result-object v4

    #@ae
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b1
    move-result v4

    #@b2
    if-eqz v4, :cond_cd

    #@b4
    .line 867
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@b6
    const-string v5, "lgims_com_media_audio"

    #@b8
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@bb
    move-result-object v6

    #@bc
    invoke-virtual {v4, v5, v2, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier;->updateDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@bf
    move-result v4

    #@c0
    if-eqz v4, :cond_c7

    #@c2
    .line 868
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@c4
    invoke-virtual {v4, p1, p2}, Lcom/lge/ims/setting/MediaSettingsCarrier;->summaryChange(Landroid/preference/Preference;Ljava/lang/Object;)V

    #@c7
    .line 870
    :cond_c7
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@c9
    invoke-static {v4, v8}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$676(Lcom/lge/ims/setting/MediaSettingsCarrier;I)I

    #@cc
    goto :goto_87

    #@cd
    .line 873
    :cond_cd
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@cf
    const v5, 0x7f060185

    #@d2
    invoke-virtual {v4, v5}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@d5
    move-result-object v4

    #@d6
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d9
    move-result v4

    #@da
    if-eqz v4, :cond_f5

    #@dc
    .line 874
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@de
    const-string v5, "lgims_com_media_audio"

    #@e0
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@e3
    move-result-object v6

    #@e4
    invoke-virtual {v4, v5, v2, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier;->updateDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@e7
    move-result v4

    #@e8
    if-eqz v4, :cond_ef

    #@ea
    .line 875
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@ec
    invoke-virtual {v4, p1, p2}, Lcom/lge/ims/setting/MediaSettingsCarrier;->summaryChange(Landroid/preference/Preference;Ljava/lang/Object;)V

    #@ef
    .line 877
    :cond_ef
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@f1
    invoke-static {v4, v8}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$676(Lcom/lge/ims/setting/MediaSettingsCarrier;I)I

    #@f4
    goto :goto_87

    #@f5
    .line 881
    :cond_f5
    const/4 v0, -0x1

    #@f6
    .line 883
    .local v0, codecIndex:I
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@f8
    const v4, 0x7f0602d4

    #@fb
    invoke-virtual {v3, v4}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@fe
    move-result-object v3

    #@ff
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@102
    move-result v3

    #@103
    if-eqz v3, :cond_127

    #@105
    .line 884
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->m_audioCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;

    #@107
    const-string v4, "lgims_com_media_audio_codec_volte"

    #@109
    const-string v5, "AMR"

    #@10b
    const-string v6, "BE"

    #@10d
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@110
    move-result-object v7

    #@111
    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->updateCodecPayloadNumDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@114
    move-result v1

    #@115
    .line 885
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->m_audioCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;

    #@117
    const-string v4, "lgims_com_media_audio_codec_vt"

    #@119
    const-string v5, "AMR"

    #@11b
    const-string v6, "BE"

    #@11d
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@120
    move-result-object v7

    #@121
    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->updateCodecPayloadNumDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@124
    move-result v1

    #@125
    goto/16 :goto_4a

    #@127
    .line 887
    :cond_127
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@129
    const v4, 0x7f0602d5

    #@12c
    invoke-virtual {v3, v4}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@12f
    move-result-object v3

    #@130
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@133
    move-result v3

    #@134
    if-eqz v3, :cond_158

    #@136
    .line 888
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->m_audioCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;

    #@138
    const-string v4, "lgims_com_media_audio_codec_volte"

    #@13a
    const-string v5, "AMR"

    #@13c
    const-string v6, "BE"

    #@13e
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@141
    move-result-object v7

    #@142
    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->updateCodecModeSetDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@145
    move-result v1

    #@146
    .line 889
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->m_audioCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;

    #@148
    const-string v4, "lgims_com_media_audio_codec_vt"

    #@14a
    const-string v5, "AMR"

    #@14c
    const-string v6, "BE"

    #@14e
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@151
    move-result-object v7

    #@152
    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->updateCodecModeSetDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@155
    move-result v1

    #@156
    goto/16 :goto_4a

    #@158
    .line 891
    :cond_158
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@15a
    const v4, 0x7f0602d6

    #@15d
    invoke-virtual {v3, v4}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@160
    move-result-object v3

    #@161
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@164
    move-result v3

    #@165
    if-eqz v3, :cond_189

    #@167
    .line 892
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->m_audioCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;

    #@169
    const-string v4, "lgims_com_media_audio_codec_volte"

    #@16b
    const-string v5, "AMR"

    #@16d
    const-string v6, "OA"

    #@16f
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@172
    move-result-object v7

    #@173
    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->updateCodecPayloadNumDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@176
    move-result v1

    #@177
    .line 893
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->m_audioCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;

    #@179
    const-string v4, "lgims_com_media_audio_codec_vt"

    #@17b
    const-string v5, "AMR"

    #@17d
    const-string v6, "OA"

    #@17f
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@182
    move-result-object v7

    #@183
    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->updateCodecPayloadNumDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@186
    move-result v1

    #@187
    goto/16 :goto_4a

    #@189
    .line 895
    :cond_189
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@18b
    const v4, 0x7f0602d7

    #@18e
    invoke-virtual {v3, v4}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@191
    move-result-object v3

    #@192
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@195
    move-result v3

    #@196
    if-eqz v3, :cond_1ba

    #@198
    .line 896
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->m_audioCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;

    #@19a
    const-string v4, "lgims_com_media_audio_codec_volte"

    #@19c
    const-string v5, "AMR"

    #@19e
    const-string v6, "OA"

    #@1a0
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1a3
    move-result-object v7

    #@1a4
    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->updateCodecModeSetDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@1a7
    move-result v1

    #@1a8
    .line 897
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->m_audioCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;

    #@1aa
    const-string v4, "lgims_com_media_audio_codec_vt"

    #@1ac
    const-string v5, "AMR"

    #@1ae
    const-string v6, "OA"

    #@1b0
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1b3
    move-result-object v7

    #@1b4
    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->updateCodecModeSetDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@1b7
    move-result v1

    #@1b8
    goto/16 :goto_4a

    #@1ba
    .line 899
    :cond_1ba
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@1bc
    const v4, 0x7f0602d8

    #@1bf
    invoke-virtual {v3, v4}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@1c2
    move-result-object v3

    #@1c3
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c6
    move-result v3

    #@1c7
    if-eqz v3, :cond_1eb

    #@1c9
    .line 900
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->m_audioCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;

    #@1cb
    const-string v4, "lgims_com_media_audio_codec_volte"

    #@1cd
    const-string v5, "AMR-WB"

    #@1cf
    const-string v6, "BE"

    #@1d1
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1d4
    move-result-object v7

    #@1d5
    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->updateCodecPayloadNumDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@1d8
    move-result v1

    #@1d9
    .line 901
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->m_audioCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;

    #@1db
    const-string v4, "lgims_com_media_audio_codec_vt"

    #@1dd
    const-string v5, "AMR-WB"

    #@1df
    const-string v6, "BE"

    #@1e1
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1e4
    move-result-object v7

    #@1e5
    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->updateCodecPayloadNumDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@1e8
    move-result v1

    #@1e9
    goto/16 :goto_4a

    #@1eb
    .line 903
    :cond_1eb
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@1ed
    const v4, 0x7f0602d9

    #@1f0
    invoke-virtual {v3, v4}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@1f3
    move-result-object v3

    #@1f4
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f7
    move-result v3

    #@1f8
    if-eqz v3, :cond_21c

    #@1fa
    .line 904
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->m_audioCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;

    #@1fc
    const-string v4, "lgims_com_media_audio_codec_volte"

    #@1fe
    const-string v5, "AMR-WB"

    #@200
    const-string v6, "BE"

    #@202
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@205
    move-result-object v7

    #@206
    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->updateCodecModeSetDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@209
    move-result v1

    #@20a
    .line 905
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->m_audioCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;

    #@20c
    const-string v4, "lgims_com_media_audio_codec_vt"

    #@20e
    const-string v5, "AMR-WB"

    #@210
    const-string v6, "BE"

    #@212
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@215
    move-result-object v7

    #@216
    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->updateCodecModeSetDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@219
    move-result v1

    #@21a
    goto/16 :goto_4a

    #@21c
    .line 907
    :cond_21c
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@21e
    const v4, 0x7f0602da

    #@221
    invoke-virtual {v3, v4}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@224
    move-result-object v3

    #@225
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@228
    move-result v3

    #@229
    if-eqz v3, :cond_24d

    #@22b
    .line 908
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->m_audioCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;

    #@22d
    const-string v4, "lgims_com_media_audio_codec_volte"

    #@22f
    const-string v5, "AMR-WB"

    #@231
    const-string v6, "OA"

    #@233
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@236
    move-result-object v7

    #@237
    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->updateCodecPayloadNumDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@23a
    move-result v1

    #@23b
    .line 909
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->m_audioCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;

    #@23d
    const-string v4, "lgims_com_media_audio_codec_vt"

    #@23f
    const-string v5, "AMR-WB"

    #@241
    const-string v6, "OA"

    #@243
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@246
    move-result-object v7

    #@247
    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->updateCodecPayloadNumDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@24a
    move-result v1

    #@24b
    goto/16 :goto_4a

    #@24d
    .line 911
    :cond_24d
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@24f
    const v4, 0x7f0602db

    #@252
    invoke-virtual {v3, v4}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@255
    move-result-object v3

    #@256
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@259
    move-result v3

    #@25a
    if-eqz v3, :cond_4a

    #@25c
    .line 912
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->m_audioCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;

    #@25e
    const-string v4, "lgims_com_media_audio_codec_volte"

    #@260
    const-string v5, "AMR-WB"

    #@262
    const-string v6, "OA"

    #@264
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@267
    move-result-object v7

    #@268
    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->updateCodecModeSetDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@26b
    move-result v1

    #@26c
    .line 913
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;->m_audioCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;

    #@26e
    const-string v4, "lgims_com_media_audio_codec_vt"

    #@270
    const-string v5, "AMR-WB"

    #@272
    const-string v6, "OA"

    #@274
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@277
    move-result-object v7

    #@278
    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->updateCodecModeSetDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@27b
    move-result v1

    #@27c
    goto/16 :goto_4a
.end method
