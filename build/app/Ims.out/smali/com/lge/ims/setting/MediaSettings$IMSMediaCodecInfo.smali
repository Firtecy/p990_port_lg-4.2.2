.class public Lcom/lge/ims/setting/MediaSettings$IMSMediaCodecInfo;
.super Ljava/lang/Object;
.source "MediaSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/setting/MediaSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "IMSMediaCodecInfo"
.end annotation


# static fields
.field public static final DEFAULT_PAYLOAD_NUM:I = 0x64


# instance fields
.field public codecAudioAMRAttribute:[Ljava/lang/String;

.field public codecAudioCommonAttribute:[Ljava/lang/String;

.field public codecVideoCommonAttribute:[Ljava/lang/String;

.field public codecVideoH263Attribute:[Ljava/lang/String;

.field public codecVideoH264Attribute:[Ljava/lang/String;

.field final synthetic this$0:Lcom/lge/ims/setting/MediaSettings;


# direct methods
.method public constructor <init>(Lcom/lge/ims/setting/MediaSettings;)V
    .registers 10
    .parameter

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 473
    iput-object p1, p0, Lcom/lge/ims/setting/MediaSettings$IMSMediaCodecInfo;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@7
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@a
    .line 475
    new-array v0, v6, [Ljava/lang/String;

    #@c
    const-string v1, "payload_type"

    #@e
    aput-object v1, v0, v3

    #@10
    const-string v1, "network_type"

    #@12
    aput-object v1, v0, v4

    #@14
    const-string v1, "sampling_rate"

    #@16
    aput-object v1, v0, v5

    #@18
    iput-object v0, p0, Lcom/lge/ims/setting/MediaSettings$IMSMediaCodecInfo;->codecAudioCommonAttribute:[Ljava/lang/String;

    #@1a
    .line 476
    const/16 v0, 0xa

    #@1c
    new-array v0, v0, [Ljava/lang/String;

    #@1e
    const-string v1, "channel"

    #@20
    aput-object v1, v0, v3

    #@22
    const-string v1, "octet_align"

    #@24
    aput-object v1, v0, v4

    #@26
    const-string v1, "mode_set"

    #@28
    aput-object v1, v0, v5

    #@2a
    const-string v1, "mode_change_capability"

    #@2c
    aput-object v1, v0, v6

    #@2e
    const-string v1, "mode_change_period"

    #@30
    aput-object v1, v0, v7

    #@32
    const/4 v1, 0x5

    #@33
    const-string v2, "mode_change_neighbor"

    #@35
    aput-object v2, v0, v1

    #@37
    const/4 v1, 0x6

    #@38
    const-string v2, "max_red"

    #@3a
    aput-object v2, v0, v1

    #@3c
    const/4 v1, 0x7

    #@3d
    const-string v2, "robust_sorting"

    #@3f
    aput-object v2, v0, v1

    #@41
    const/16 v1, 0x8

    #@43
    const-string v2, "ptime"

    #@45
    aput-object v2, v0, v1

    #@47
    const/16 v1, 0x9

    #@49
    const-string v2, "max_ptime"

    #@4b
    aput-object v2, v0, v1

    #@4d
    iput-object v0, p0, Lcom/lge/ims/setting/MediaSettings$IMSMediaCodecInfo;->codecAudioAMRAttribute:[Ljava/lang/String;

    #@4f
    .line 479
    const/16 v0, 0x8

    #@51
    new-array v0, v0, [Ljava/lang/String;

    #@53
    const-string v1, "network_type"

    #@55
    aput-object v1, v0, v3

    #@57
    const-string v1, "payload_type"

    #@59
    aput-object v1, v0, v4

    #@5b
    const-string v1, "resolution"

    #@5d
    aput-object v1, v0, v5

    #@5f
    const-string v1, "framerate"

    #@61
    aput-object v1, v0, v6

    #@63
    const-string v1, "bitrate"

    #@65
    aput-object v1, v0, v7

    #@67
    const/4 v1, 0x5

    #@68
    const-string v2, "AS"

    #@6a
    aput-object v2, v0, v1

    #@6c
    const/4 v1, 0x6

    #@6d
    const-string v2, "profile_level_id"

    #@6f
    aput-object v2, v0, v1

    #@71
    const/4 v1, 0x7

    #@72
    const-string v2, "framesize"

    #@74
    aput-object v2, v0, v1

    #@76
    iput-object v0, p0, Lcom/lge/ims/setting/MediaSettings$IMSMediaCodecInfo;->codecVideoCommonAttribute:[Ljava/lang/String;

    #@78
    .line 480
    new-array v0, v6, [Ljava/lang/String;

    #@7a
    const-string v1, "profile"

    #@7c
    aput-object v1, v0, v3

    #@7e
    const-string v1, "level"

    #@80
    aput-object v1, v0, v4

    #@82
    const-string v1, "QCIF"

    #@84
    aput-object v1, v0, v5

    #@86
    iput-object v0, p0, Lcom/lge/ims/setting/MediaSettings$IMSMediaCodecInfo;->codecVideoH263Attribute:[Ljava/lang/String;

    #@88
    .line 481
    const/16 v0, 0x8

    #@8a
    new-array v0, v0, [Ljava/lang/String;

    #@8c
    const-string v1, "packetization_mode"

    #@8e
    aput-object v1, v0, v3

    #@90
    const-string v1, "sprop_parameter_sets"

    #@92
    aput-object v1, v0, v4

    #@94
    const-string v1, "max_mbps"

    #@96
    aput-object v1, v0, v5

    #@98
    const-string v1, "max_fs"

    #@9a
    aput-object v1, v0, v6

    #@9c
    const-string v1, "max_cpb"

    #@9e
    aput-object v1, v0, v7

    #@a0
    const/4 v1, 0x5

    #@a1
    const-string v2, "max_dpb"

    #@a3
    aput-object v2, v0, v1

    #@a5
    const/4 v1, 0x6

    #@a6
    const-string v2, "max_br"

    #@a8
    aput-object v2, v0, v1

    #@aa
    const/4 v1, 0x7

    #@ab
    const-string v2, "redundant_pic_ca"

    #@ad
    aput-object v2, v0, v1

    #@af
    iput-object v0, p0, Lcom/lge/ims/setting/MediaSettings$IMSMediaCodecInfo;->codecVideoH264Attribute:[Ljava/lang/String;

    #@b1
    .line 483
    return-void
.end method
