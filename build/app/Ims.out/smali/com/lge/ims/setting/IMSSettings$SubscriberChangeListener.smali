.class Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;
.super Ljava/lang/Object;
.source "IMSSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/setting/IMSSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SubscriberChangeListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/setting/IMSSettings;


# direct methods
.method private constructor <init>(Lcom/lge/ims/setting/IMSSettings;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1233
    iput-object p1, p0, Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/lge/ims/setting/IMSSettings;Lcom/lge/ims/setting/IMSSettings$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1233
    invoke-direct {p0, p1}, Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;-><init>(Lcom/lge/ims/setting/IMSSettings;)V

    #@3
    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .registers 14
    .parameter "preference"
    .parameter "newValue"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    .line 1235
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    #@5
    move-result-object v3

    #@6
    .line 1237
    .local v3, key:Ljava/lang/String;
    const-string v7, "LGIMSSettings"

    #@8
    new-instance v8, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v9, "lgims_subscriber :: "

    #@f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v8

    #@13
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v8

    #@17
    const-string v9, "="

    #@19
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v8

    #@1d
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@20
    move-result-object v9

    #@21
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v8

    #@25
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v8

    #@29
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 1239
    iget-object v7, p0, Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@2e
    invoke-static {v7}, Lcom/lge/ims/setting/IMSSettings;->access$1000(Lcom/lge/ims/setting/IMSSettings;)Landroid/database/sqlite/SQLiteDatabase;

    #@31
    move-result-object v7

    #@32
    if-nez v7, :cond_3c

    #@34
    .line 1240
    const-string v6, "LGIMSSettings"

    #@36
    const-string v7, "DB is null"

    #@38
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 1323
    :goto_3b
    return v5

    #@3c
    .line 1244
    :cond_3c
    const/4 v0, 0x0

    #@3d
    .line 1245
    .local v0, affectedRows:I
    new-instance v4, Landroid/content/ContentValues;

    #@3f
    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    #@42
    .line 1247
    .local v4, values:Landroid/content/ContentValues;
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@45
    move-result-object v7

    #@46
    invoke-virtual {v4, v3, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@49
    .line 1249
    const-string v7, "subscriber_0_home_domain_name"

    #@4b
    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4e
    move-result v7

    #@4f
    if-eqz v7, :cond_8e

    #@51
    .line 1250
    const-string v7, "subscriber_0_server_scscf"

    #@53
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@56
    move-result-object v8

    #@57
    invoke-virtual {v4, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@5a
    .line 1274
    :cond_5a
    :goto_5a
    iget-object v7, p0, Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@5c
    invoke-static {v7}, Lcom/lge/ims/setting/IMSSettings;->access$1000(Lcom/lge/ims/setting/IMSSettings;)Landroid/database/sqlite/SQLiteDatabase;

    #@5f
    move-result-object v7

    #@60
    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    #@63
    .line 1277
    :try_start_63
    iget-object v7, p0, Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@65
    invoke-static {v7}, Lcom/lge/ims/setting/IMSSettings;->access$1000(Lcom/lge/ims/setting/IMSSettings;)Landroid/database/sqlite/SQLiteDatabase;

    #@68
    move-result-object v7

    #@69
    const-string v8, "lgims_subscriber"

    #@6b
    const-string v9, "id = \'1\'"

    #@6d
    const/4 v10, 0x0

    #@6e
    invoke-virtual {v7, v8, v4, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@71
    move-result v0

    #@72
    .line 1278
    iget-object v7, p0, Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@74
    invoke-static {v7}, Lcom/lge/ims/setting/IMSSettings;->access$1000(Lcom/lge/ims/setting/IMSSettings;)Landroid/database/sqlite/SQLiteDatabase;

    #@77
    move-result-object v7

    #@78
    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_7b
    .catchall {:try_start_63 .. :try_end_7b} :catchall_140
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_63 .. :try_end_7b} :catch_131

    #@7b
    .line 1282
    iget-object v7, p0, Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@7d
    invoke-static {v7}, Lcom/lge/ims/setting/IMSSettings;->access$1000(Lcom/lge/ims/setting/IMSSettings;)Landroid/database/sqlite/SQLiteDatabase;

    #@80
    move-result-object v7

    #@81
    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@84
    .line 1285
    :goto_84
    if-eq v0, v6, :cond_14b

    #@86
    .line 1286
    const-string v6, "LGIMSSettings"

    #@88
    const-string v7, "Update fails"

    #@8a
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8d
    goto :goto_3b

    #@8e
    .line 1251
    :cond_8e
    const-string v7, "subscriber_0_impi"

    #@90
    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@93
    move-result v7

    #@94
    if-eqz v7, :cond_b1

    #@96
    .line 1252
    const-string v7, "subscriber_0_auth_username"

    #@98
    const-string v8, ""

    #@9a
    invoke-virtual {v4, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@9d
    .line 1254
    iget-object v7, p0, Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@9f
    invoke-static {v7}, Lcom/lge/ims/setting/IMSSettings;->access$1100(Lcom/lge/ims/setting/IMSSettings;)Landroid/preference/EditTextPreference;

    #@a2
    move-result-object v7

    #@a3
    if-eqz v7, :cond_5a

    #@a5
    .line 1255
    iget-object v7, p0, Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@a7
    invoke-static {v7}, Lcom/lge/ims/setting/IMSSettings;->access$1100(Lcom/lge/ims/setting/IMSSettings;)Landroid/preference/EditTextPreference;

    #@aa
    move-result-object v7

    #@ab
    const-string v8, ""

    #@ad
    invoke-virtual {v7, v8}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    #@b0
    goto :goto_5a

    #@b1
    .line 1257
    :cond_b1
    const-string v7, "subscriber_0_auth_password"

    #@b3
    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b6
    move-result v7

    #@b7
    if-eqz v7, :cond_5a

    #@b9
    .line 1258
    iget-object v7, p0, Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@bb
    const-string v8, "MD5"

    #@bd
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@c0
    move-result-object v9

    #@c1
    invoke-static {v8, v9}, Lcom/lge/ims/service/ac/ACAuthHelper;->calculateMessageDigest(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@c4
    move-result-object v8

    #@c5
    invoke-static {v7, v8}, Lcom/lge/ims/setting/IMSSettings;->access$1202(Lcom/lge/ims/setting/IMSSettings;Ljava/lang/String;)Ljava/lang/String;

    #@c8
    .line 1260
    iget-object v7, p0, Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@ca
    invoke-static {v7}, Lcom/lge/ims/setting/IMSSettings;->access$1200(Lcom/lge/ims/setting/IMSSettings;)Ljava/lang/String;

    #@cd
    move-result-object v7

    #@ce
    if-eqz v7, :cond_110

    #@d0
    .line 1261
    iget-object v7, p0, Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@d2
    iget-object v8, p0, Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@d4
    invoke-static {v8}, Lcom/lge/ims/setting/IMSSettings;->access$1200(Lcom/lge/ims/setting/IMSSettings;)Ljava/lang/String;

    #@d7
    move-result-object v8

    #@d8
    invoke-virtual {v8}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@db
    move-result-object v8

    #@dc
    invoke-static {v7, v8}, Lcom/lge/ims/setting/IMSSettings;->access$1202(Lcom/lge/ims/setting/IMSSettings;Ljava/lang/String;)Ljava/lang/String;

    #@df
    .line 1262
    iget-object v7, p0, Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@e1
    invoke-static {v7}, Lcom/lge/ims/setting/IMSSettings;->access$1200(Lcom/lge/ims/setting/IMSSettings;)Ljava/lang/String;

    #@e4
    move-result-object v7

    #@e5
    invoke-virtual {v4, v3, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@e8
    .line 1264
    const-string v7, "user"

    #@ea
    sget-object v8, Lcom/lge/ims/Configuration;->BUILD_TYPE:Ljava/lang/String;

    #@ec
    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@ef
    move-result v7

    #@f0
    if-nez v7, :cond_110

    #@f2
    .line 1265
    const-string v7, "LGIMSSettings"

    #@f4
    new-instance v8, Ljava/lang/StringBuilder;

    #@f6
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@f9
    const-string v9, "Password="

    #@fb
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fe
    move-result-object v8

    #@ff
    iget-object v9, p0, Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@101
    invoke-static {v9}, Lcom/lge/ims/setting/IMSSettings;->access$1200(Lcom/lge/ims/setting/IMSSettings;)Ljava/lang/String;

    #@104
    move-result-object v9

    #@105
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@108
    move-result-object v8

    #@109
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10c
    move-result-object v8

    #@10d
    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@110
    .line 1269
    :cond_110
    iget-object v7, p0, Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@112
    invoke-static {v7}, Lcom/lge/ims/setting/IMSSettings;->access$1300(Lcom/lge/ims/setting/IMSSettings;)Landroid/preference/EditTextPreference;

    #@115
    move-result-object v7

    #@116
    if-eqz v7, :cond_5a

    #@118
    iget-object v7, p0, Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@11a
    invoke-static {v7}, Lcom/lge/ims/setting/IMSSettings;->access$1200(Lcom/lge/ims/setting/IMSSettings;)Ljava/lang/String;

    #@11d
    move-result-object v7

    #@11e
    if-eqz v7, :cond_5a

    #@120
    .line 1270
    iget-object v7, p0, Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@122
    invoke-static {v7}, Lcom/lge/ims/setting/IMSSettings;->access$1300(Lcom/lge/ims/setting/IMSSettings;)Landroid/preference/EditTextPreference;

    #@125
    move-result-object v7

    #@126
    iget-object v8, p0, Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@128
    invoke-static {v8}, Lcom/lge/ims/setting/IMSSettings;->access$1200(Lcom/lge/ims/setting/IMSSettings;)Ljava/lang/String;

    #@12b
    move-result-object v8

    #@12c
    invoke-virtual {v7, v8}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    #@12f
    goto/16 :goto_5a

    #@131
    .line 1279
    :catch_131
    move-exception v2

    #@132
    .line 1280
    .local v2, e:Landroid/database/sqlite/SQLiteException;
    :try_start_132
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_135
    .catchall {:try_start_132 .. :try_end_135} :catchall_140

    #@135
    .line 1282
    iget-object v7, p0, Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@137
    invoke-static {v7}, Lcom/lge/ims/setting/IMSSettings;->access$1000(Lcom/lge/ims/setting/IMSSettings;)Landroid/database/sqlite/SQLiteDatabase;

    #@13a
    move-result-object v7

    #@13b
    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@13e
    goto/16 :goto_84

    #@140
    .end local v2           #e:Landroid/database/sqlite/SQLiteException;
    :catchall_140
    move-exception v5

    #@141
    iget-object v6, p0, Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@143
    invoke-static {v6}, Lcom/lge/ims/setting/IMSSettings;->access$1000(Lcom/lge/ims/setting/IMSSettings;)Landroid/database/sqlite/SQLiteDatabase;

    #@146
    move-result-object v6

    #@147
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@14a
    throw v5

    #@14b
    .line 1290
    :cond_14b
    const-string v7, "admin_ims"

    #@14d
    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@150
    move-result v7

    #@151
    if-eqz v7, :cond_17f

    #@153
    .line 1291
    const-string v7, "true"

    #@155
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@158
    move-result-object v8

    #@159
    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@15c
    move-result v7

    #@15d
    if-eqz v7, :cond_179

    #@15f
    .line 1292
    iget-object v5, p0, Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@161
    invoke-static {v5, v6}, Lcom/lge/ims/setting/IMSSettings;->access$1400(Lcom/lge/ims/setting/IMSSettings;Z)V

    #@164
    .line 1317
    :cond_164
    :goto_164
    const-string v5, "admin_ac"

    #@166
    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@169
    move-result v5

    #@16a
    if-nez v5, :cond_171

    #@16c
    .line 1318
    iget-object v5, p0, Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@16e
    invoke-static {v5, v6}, Lcom/lge/ims/setting/IMSSettings;->access$1676(Lcom/lge/ims/setting/IMSSettings;I)I

    #@171
    .line 1321
    :cond_171
    iget-object v5, p0, Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@173
    invoke-static {v5, p1, p2}, Lcom/lge/ims/setting/IMSSettings;->access$1700(Lcom/lge/ims/setting/IMSSettings;Landroid/preference/Preference;Ljava/lang/Object;)V

    #@176
    move v5, v6

    #@177
    .line 1323
    goto/16 :goto_3b

    #@179
    .line 1294
    :cond_179
    iget-object v7, p0, Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@17b
    invoke-static {v7, v5}, Lcom/lge/ims/setting/IMSSettings;->access$1400(Lcom/lge/ims/setting/IMSSettings;Z)V

    #@17e
    goto :goto_164

    #@17f
    .line 1296
    :cond_17f
    const-string v7, "admin_usim"

    #@181
    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@184
    move-result v7

    #@185
    if-eqz v7, :cond_19f

    #@187
    .line 1297
    const-string v7, "true"

    #@189
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@18c
    move-result-object v8

    #@18d
    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@190
    move-result v7

    #@191
    if-eqz v7, :cond_199

    #@193
    .line 1298
    iget-object v5, p0, Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@195
    invoke-static {v5, v6}, Lcom/lge/ims/setting/IMSSettings;->access$1500(Lcom/lge/ims/setting/IMSSettings;Z)V

    #@198
    goto :goto_164

    #@199
    .line 1300
    :cond_199
    iget-object v7, p0, Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@19b
    invoke-static {v7, v5}, Lcom/lge/ims/setting/IMSSettings;->access$1500(Lcom/lge/ims/setting/IMSSettings;Z)V

    #@19e
    goto :goto_164

    #@19f
    .line 1302
    :cond_19f
    const-string v5, "admin_ac"

    #@1a1
    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1a4
    move-result v5

    #@1a5
    if-eqz v5, :cond_164

    #@1a7
    .line 1304
    new-instance v1, Landroid/content/ContentValues;

    #@1a9
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    #@1ac
    .line 1306
    .local v1, cvs:Landroid/content/ContentValues;
    const-string v5, "availability"

    #@1ae
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1b1
    move-result-object v7

    #@1b2
    invoke-virtual {v1, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1b5
    .line 1309
    :try_start_1b5
    iget-object v5, p0, Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@1b7
    invoke-virtual {v5}, Lcom/lge/ims/setting/IMSSettings;->getContentResolver()Landroid/content/ContentResolver;

    #@1ba
    move-result-object v5

    #@1bb
    sget-object v7, Lcom/lge/ims/provider/ac/AC$Settings$Common;->CONTENT_URI:Landroid/net/Uri;

    #@1bd
    const/4 v8, 0x0

    #@1be
    const/4 v9, 0x0

    #@1bf
    invoke-virtual {v5, v7, v1, v8, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@1c2
    move-result v5

    #@1c3
    if-gtz v5, :cond_164

    #@1c5
    .line 1310
    const-string v5, "LGIMSSettings"

    #@1c7
    const-string v7, "Updating AC on/off failed"

    #@1c9
    invoke-static {v5, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1cc
    .catch Ljava/lang/Exception; {:try_start_1b5 .. :try_end_1cc} :catch_1cd

    #@1cc
    goto :goto_164

    #@1cd
    .line 1312
    :catch_1cd
    move-exception v2

    #@1ce
    .line 1313
    .local v2, e:Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    #@1d1
    goto :goto_164
.end method
