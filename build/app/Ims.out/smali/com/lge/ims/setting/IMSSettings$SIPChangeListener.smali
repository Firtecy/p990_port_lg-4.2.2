.class Lcom/lge/ims/setting/IMSSettings$SIPChangeListener;
.super Ljava/lang/Object;
.source "IMSSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/setting/IMSSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SIPChangeListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/setting/IMSSettings;


# direct methods
.method private constructor <init>(Lcom/lge/ims/setting/IMSSettings;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1380
    iput-object p1, p0, Lcom/lge/ims/setting/IMSSettings$SIPChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/lge/ims/setting/IMSSettings;Lcom/lge/ims/setting/IMSSettings$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1380
    invoke-direct {p0, p1}, Lcom/lge/ims/setting/IMSSettings$SIPChangeListener;-><init>(Lcom/lge/ims/setting/IMSSettings;)V

    #@3
    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .registers 13
    .parameter "preference"
    .parameter "newValue"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 1382
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    #@5
    move-result-object v2

    #@6
    .line 1384
    .local v2, key:Ljava/lang/String;
    const-string v6, "LGIMSSettings"

    #@8
    new-instance v7, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v8, "lgims_sip :: "

    #@f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v7

    #@13
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v7

    #@17
    const-string v8, "="

    #@19
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v7

    #@1d
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@20
    move-result-object v8

    #@21
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v7

    #@25
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v7

    #@29
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 1386
    iget-object v6, p0, Lcom/lge/ims/setting/IMSSettings$SIPChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@2e
    invoke-static {v6}, Lcom/lge/ims/setting/IMSSettings;->access$1000(Lcom/lge/ims/setting/IMSSettings;)Landroid/database/sqlite/SQLiteDatabase;

    #@31
    move-result-object v6

    #@32
    if-nez v6, :cond_3c

    #@34
    .line 1387
    const-string v5, "LGIMSSettings"

    #@36
    const-string v6, "DB is null"

    #@38
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 1416
    :goto_3b
    return v4

    #@3c
    .line 1391
    :cond_3c
    const/4 v0, 0x0

    #@3d
    .line 1392
    .local v0, affectedRows:I
    new-instance v3, Landroid/content/ContentValues;

    #@3f
    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    #@42
    .line 1394
    .local v3, values:Landroid/content/ContentValues;
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@45
    move-result-object v6

    #@46
    invoke-virtual {v3, v2, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@49
    .line 1396
    iget-object v6, p0, Lcom/lge/ims/setting/IMSSettings$SIPChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@4b
    invoke-static {v6}, Lcom/lge/ims/setting/IMSSettings;->access$1000(Lcom/lge/ims/setting/IMSSettings;)Landroid/database/sqlite/SQLiteDatabase;

    #@4e
    move-result-object v6

    #@4f
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    #@52
    .line 1399
    :try_start_52
    iget-object v6, p0, Lcom/lge/ims/setting/IMSSettings$SIPChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@54
    invoke-static {v6}, Lcom/lge/ims/setting/IMSSettings;->access$1000(Lcom/lge/ims/setting/IMSSettings;)Landroid/database/sqlite/SQLiteDatabase;

    #@57
    move-result-object v6

    #@58
    const-string v7, "lgims_sip"

    #@5a
    const-string v8, "id = \'1\'"

    #@5c
    const/4 v9, 0x0

    #@5d
    invoke-virtual {v6, v7, v3, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@60
    move-result v0

    #@61
    .line 1400
    iget-object v6, p0, Lcom/lge/ims/setting/IMSSettings$SIPChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@63
    invoke-static {v6}, Lcom/lge/ims/setting/IMSSettings;->access$1000(Lcom/lge/ims/setting/IMSSettings;)Landroid/database/sqlite/SQLiteDatabase;

    #@66
    move-result-object v6

    #@67
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_6a
    .catchall {:try_start_52 .. :try_end_6a} :catchall_8b
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_52 .. :try_end_6a} :catch_7d

    #@6a
    .line 1404
    iget-object v6, p0, Lcom/lge/ims/setting/IMSSettings$SIPChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@6c
    invoke-static {v6}, Lcom/lge/ims/setting/IMSSettings;->access$1000(Lcom/lge/ims/setting/IMSSettings;)Landroid/database/sqlite/SQLiteDatabase;

    #@6f
    move-result-object v6

    #@70
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@73
    .line 1407
    :goto_73
    if-eq v0, v5, :cond_96

    #@75
    .line 1408
    const-string v5, "LGIMSSettings"

    #@77
    const-string v6, "Update fails"

    #@79
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7c
    goto :goto_3b

    #@7d
    .line 1401
    :catch_7d
    move-exception v1

    #@7e
    .line 1402
    .local v1, e:Landroid/database/sqlite/SQLiteException;
    :try_start_7e
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_81
    .catchall {:try_start_7e .. :try_end_81} :catchall_8b

    #@81
    .line 1404
    iget-object v6, p0, Lcom/lge/ims/setting/IMSSettings$SIPChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@83
    invoke-static {v6}, Lcom/lge/ims/setting/IMSSettings;->access$1000(Lcom/lge/ims/setting/IMSSettings;)Landroid/database/sqlite/SQLiteDatabase;

    #@86
    move-result-object v6

    #@87
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@8a
    goto :goto_73

    #@8b
    .end local v1           #e:Landroid/database/sqlite/SQLiteException;
    :catchall_8b
    move-exception v4

    #@8c
    iget-object v5, p0, Lcom/lge/ims/setting/IMSSettings$SIPChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@8e
    invoke-static {v5}, Lcom/lge/ims/setting/IMSSettings;->access$1000(Lcom/lge/ims/setting/IMSSettings;)Landroid/database/sqlite/SQLiteDatabase;

    #@91
    move-result-object v5

    #@92
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@95
    throw v4

    #@96
    .line 1412
    :cond_96
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings$SIPChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@98
    const/4 v6, 0x4

    #@99
    invoke-static {v4, v6}, Lcom/lge/ims/setting/IMSSettings;->access$1676(Lcom/lge/ims/setting/IMSSettings;I)I

    #@9c
    .line 1414
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings$SIPChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@9e
    invoke-static {v4, p1, p2}, Lcom/lge/ims/setting/IMSSettings;->access$1700(Lcom/lge/ims/setting/IMSSettings;Landroid/preference/Preference;Ljava/lang/Object;)V

    #@a1
    move v4, v5

    #@a2
    .line 1416
    goto :goto_3b
.end method
