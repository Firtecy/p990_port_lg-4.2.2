.class Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;
.super Ljava/lang/Object;
.source "MediaSettingsCarrier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/setting/MediaSettingsCarrier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CarrierVideoCommand"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;


# direct methods
.method public constructor <init>(Lcom/lge/ims/setting/MediaSettingsCarrier;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 666
    iput-object p1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 667
    return-void
.end method


# virtual methods
.method public loadDBToPreference(Ljava/lang/String;Landroid/preference/Preference;Ljava/lang/String;)Z
    .registers 9
    .parameter "tableName"
    .parameter "pref"
    .parameter "key"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 794
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@3
    invoke-virtual {v2, p1, p3}, Lcom/lge/ims/setting/MediaSettingsCarrier;->load(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 796
    .local v0, value:Ljava/lang/String;
    const-string v2, "IMSMediaSettingsCarrier"

    #@9
    new-instance v3, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v4, "loadDBToPreference : key - "

    #@10
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v3

    #@1c
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@1f
    .line 798
    if-nez v0, :cond_22

    #@21
    .line 810
    :cond_21
    :goto_21
    return v1

    #@22
    .line 802
    :cond_22
    if-eqz p2, :cond_21

    #@24
    .line 806
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@26
    invoke-virtual {v2, p2, v0}, Lcom/lge/ims/setting/MediaSettingsCarrier;->setValue(Landroid/preference/Preference;Ljava/lang/String;)Z

    #@29
    move-result v2

    #@2a
    if-eqz v2, :cond_21

    #@2c
    .line 807
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@2e
    invoke-virtual {v1, p2, v0}, Lcom/lge/ims/setting/MediaSettingsCarrier;->summaryChange(Landroid/preference/Preference;Ljava/lang/Object;)V

    #@31
    .line 808
    const/4 v1, 0x1

    #@32
    goto :goto_21
.end method

.method public searchCodecIndex(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .registers 14
    .parameter "tableName"
    .parameter "codecType"
    .parameter "resolution"

    #@0
    .prologue
    const/4 v6, -0x1

    #@1
    .line 759
    const/4 v3, 0x0

    #@2
    .line 760
    .local v3, target:Ljava/lang/String;
    const-string v7, "VGA"

    #@4
    invoke-virtual {p3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v7

    #@8
    if-eqz v7, :cond_54

    #@a
    .line 761
    const-string v3, "4"

    #@c
    .line 770
    :goto_c
    const/4 v0, 0x0

    #@d
    .local v0, i:I
    :goto_d
    const/4 v7, 0x6

    #@e
    if-ge v0, v7, :cond_71

    #@10
    .line 772
    new-instance v7, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v8, "videocodec_"

    #@17
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v7

    #@1b
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v7

    #@1f
    const-string v8, "_codec_type"

    #@21
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v7

    #@25
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v1

    #@29
    .line 773
    .local v1, key1:Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v8, "videocodec_"

    #@30
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v7

    #@34
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@37
    move-result-object v7

    #@38
    const-string v8, "_resolution"

    #@3a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v7

    #@3e
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v2

    #@42
    .line 774
    .local v2, key2:Ljava/lang/String;
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@44
    invoke-virtual {v7, p1, v1}, Lcom/lge/ims/setting/MediaSettingsCarrier;->load(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@47
    move-result-object v4

    #@48
    .line 775
    .local v4, value1:Ljava/lang/String;
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@4a
    invoke-virtual {v7, p1, v2}, Lcom/lge/ims/setting/MediaSettingsCarrier;->load(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@4d
    move-result-object v5

    #@4e
    .line 777
    .local v5, value2:Ljava/lang/String;
    if-eqz v4, :cond_52

    #@50
    if-nez v5, :cond_62

    #@52
    :cond_52
    move v0, v6

    #@53
    .line 789
    .end local v0           #i:I
    .end local v1           #key1:Ljava/lang/String;
    .end local v2           #key2:Ljava/lang/String;
    .end local v4           #value1:Ljava/lang/String;
    .end local v5           #value2:Ljava/lang/String;
    :cond_53
    :goto_53
    return v0

    #@54
    .line 763
    :cond_54
    const-string v7, "QVGA"

    #@56
    invoke-virtual {p3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@59
    move-result v7

    #@5a
    if-eqz v7, :cond_5f

    #@5c
    .line 764
    const-string v3, "1"

    #@5e
    goto :goto_c

    #@5f
    .line 767
    :cond_5f
    const-string v3, "0"

    #@61
    goto :goto_c

    #@62
    .line 781
    .restart local v0       #i:I
    .restart local v1       #key1:Ljava/lang/String;
    .restart local v2       #key2:Ljava/lang/String;
    .restart local v4       #value1:Ljava/lang/String;
    .restart local v5       #value2:Ljava/lang/String;
    :cond_62
    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@65
    move-result v7

    #@66
    if-eqz v7, :cond_6e

    #@68
    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6b
    move-result v7

    #@6c
    if-nez v7, :cond_53

    #@6e
    .line 770
    :cond_6e
    add-int/lit8 v0, v0, 0x1

    #@70
    goto :goto_d

    #@71
    .line 788
    .end local v1           #key1:Ljava/lang/String;
    .end local v2           #key2:Ljava/lang/String;
    .end local v4           #value1:Ljava/lang/String;
    .end local v5           #value2:Ljava/lang/String;
    :cond_71
    const-string v7, "IMSMediaSettingsCarrier"

    #@73
    new-instance v8, Ljava/lang/StringBuilder;

    #@75
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@78
    const-string v9, "searchCodecIndex : "

    #@7a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v8

    #@7e
    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v8

    #@82
    const-string v9, ","

    #@84
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v8

    #@88
    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v8

    #@8c
    const-string v9, " type Codec is not existed in DB"

    #@8e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v8

    #@92
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@95
    move-result-object v8

    #@96
    invoke-static {v7, v8}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@99
    move v0, v6

    #@9a
    .line 789
    goto :goto_53
.end method

.method public setVideoListener(Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierPSVTChangeListener;)V
    .registers 6
    .parameter "listener"

    #@0
    .prologue
    .line 672
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@2
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@4
    const v3, 0x7f0601a5

    #@7
    invoke-virtual {v2, v3}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@a
    move-result-object v2

    #@b
    invoke-static {v1, v2}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@e
    move-result-object v0

    #@f
    .line 673
    .local v0, pref:Landroid/preference/Preference;
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@11
    invoke-static {v1, v0, p1}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$100(Lcom/lge/ims/setting/MediaSettingsCarrier;Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@14
    .line 675
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@16
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@18
    const v3, 0x7f0602e0

    #@1b
    invoke-virtual {v2, v3}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-static {v1, v2}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@22
    move-result-object v0

    #@23
    .line 676
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@25
    invoke-static {v1, v0, p1}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$100(Lcom/lge/ims/setting/MediaSettingsCarrier;Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@28
    .line 678
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@2a
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@2c
    const v3, 0x7f0602e4

    #@2f
    invoke-virtual {v2, v3}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@32
    move-result-object v2

    #@33
    invoke-static {v1, v2}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@36
    move-result-object v0

    #@37
    .line 679
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@39
    invoke-static {v1, v0, p1}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$100(Lcom/lge/ims/setting/MediaSettingsCarrier;Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@3c
    .line 681
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@3e
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@40
    const v3, 0x7f0602e1

    #@43
    invoke-virtual {v2, v3}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@46
    move-result-object v2

    #@47
    invoke-static {v1, v2}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@4a
    move-result-object v0

    #@4b
    .line 682
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@4d
    invoke-static {v1, v0, p1}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$100(Lcom/lge/ims/setting/MediaSettingsCarrier;Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@50
    .line 684
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@52
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@54
    const v3, 0x7f0602e5

    #@57
    invoke-virtual {v2, v3}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@5a
    move-result-object v2

    #@5b
    invoke-static {v1, v2}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@5e
    move-result-object v0

    #@5f
    .line 685
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@61
    invoke-static {v1, v0, p1}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$100(Lcom/lge/ims/setting/MediaSettingsCarrier;Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@64
    .line 687
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@66
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@68
    const v3, 0x7f0602e2

    #@6b
    invoke-virtual {v2, v3}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@6e
    move-result-object v2

    #@6f
    invoke-static {v1, v2}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@72
    move-result-object v0

    #@73
    .line 688
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@75
    invoke-static {v1, v0, p1}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$100(Lcom/lge/ims/setting/MediaSettingsCarrier;Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@78
    .line 690
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@7a
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@7c
    const v3, 0x7f0602e6

    #@7f
    invoke-virtual {v2, v3}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@82
    move-result-object v2

    #@83
    invoke-static {v1, v2}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@86
    move-result-object v0

    #@87
    .line 691
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@89
    invoke-static {v1, v0, p1}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$100(Lcom/lge/ims/setting/MediaSettingsCarrier;Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@8c
    .line 693
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@8e
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@90
    const v3, 0x7f0602e3

    #@93
    invoke-virtual {v2, v3}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@96
    move-result-object v2

    #@97
    invoke-static {v1, v2}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@9a
    move-result-object v0

    #@9b
    .line 694
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@9d
    invoke-static {v1, v0, p1}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$100(Lcom/lge/ims/setting/MediaSettingsCarrier;Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@a0
    .line 696
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@a2
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@a4
    const v3, 0x7f0602e7

    #@a7
    invoke-virtual {v2, v3}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@aa
    move-result-object v2

    #@ab
    invoke-static {v1, v2}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@ae
    move-result-object v0

    #@af
    .line 697
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@b1
    invoke-static {v1, v0, p1}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$100(Lcom/lge/ims/setting/MediaSettingsCarrier;Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@b4
    .line 698
    return-void
.end method

.method public updateCodecBandwidthToDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 10
    .parameter "tableName"
    .parameter "codecType"
    .parameter "resolution"
    .parameter "value"

    #@0
    .prologue
    .line 827
    const/4 v1, 0x0

    #@1
    .line 828
    .local v1, isUpdated:Z
    invoke-virtual {p0, p1, p2, p3}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->searchCodecIndex(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@4
    move-result v0

    #@5
    .line 830
    .local v0, codecIndex:I
    const/4 v2, -0x1

    #@6
    if-ne v0, v2, :cond_a

    #@8
    .line 831
    const/4 v2, 0x0

    #@9
    .line 835
    :goto_9
    return v2

    #@a
    .line 834
    :cond_a
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@c
    new-instance v3, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v4, "videocodec_"

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    const-string v4, "_AS"

    #@1d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    invoke-virtual {v2, p1, v3, p4}, Lcom/lge/ims/setting/MediaSettingsCarrier;->updateDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@28
    move-result v1

    #@29
    move v2, v1

    #@2a
    .line 835
    goto :goto_9
.end method

.method public updateCodecBitrateToDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 10
    .parameter "tableName"
    .parameter "codecType"
    .parameter "resolution"
    .parameter "value"

    #@0
    .prologue
    .line 815
    const/4 v1, 0x0

    #@1
    .line 816
    .local v1, isUpdated:Z
    invoke-virtual {p0, p1, p2, p3}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->searchCodecIndex(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@4
    move-result v0

    #@5
    .line 817
    .local v0, codecIndex:I
    const/4 v2, -0x1

    #@6
    if-ne v0, v2, :cond_a

    #@8
    .line 818
    const/4 v2, 0x0

    #@9
    .line 822
    :goto_9
    return v2

    #@a
    .line 821
    :cond_a
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@c
    new-instance v3, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v4, "videocodec_"

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    const-string v4, "_bitrate"

    #@1d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    invoke-virtual {v2, p1, v3, p4}, Lcom/lge/ims/setting/MediaSettingsCarrier;->updateDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@28
    move-result v1

    #@29
    move v2, v1

    #@2a
    .line 822
    goto :goto_9
.end method

.method public updatePreference(Ljava/lang/String;)V
    .registers 11
    .parameter "tableName"

    #@0
    .prologue
    const v8, 0x7f0601a5

    #@3
    .line 702
    iget-object v6, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@5
    invoke-static {v6, p1}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$200(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/database/Cursor;

    #@8
    move-result-object v2

    #@9
    .line 704
    .local v2, cursor:Landroid/database/Cursor;
    if-nez v2, :cond_2a

    #@b
    .line 705
    const-string v6, "IMSMediaSettingsCarrier"

    #@d
    new-instance v7, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v8, "Cursor :: "

    #@14
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v7

    #@18
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v7

    #@1c
    const-string v8, " is null"

    #@1e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v7

    #@22
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v7

    #@26
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@29
    .line 752
    :goto_29
    return-void

    #@2a
    .line 709
    :cond_2a
    invoke-interface {v2}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    #@2d
    move-result-object v1

    #@2e
    .line 710
    .local v1, columns:[Ljava/lang/String;
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    #@31
    .line 714
    iget-object v6, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@33
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@35
    invoke-virtual {v7, v8}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@38
    move-result-object v7

    #@39
    invoke-static {v6, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$300(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/ListPreference;

    #@3c
    move-result-object v4

    #@3d
    .line 715
    .local v4, list:Landroid/preference/ListPreference;
    iget-object v6, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@3f
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@41
    invoke-virtual {v7, v8}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@44
    move-result-object v7

    #@45
    invoke-static {v6, v1, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$400(Lcom/lge/ims/setting/MediaSettingsCarrier;[Ljava/lang/String;Ljava/lang/String;)I

    #@48
    move-result v3

    #@49
    .line 716
    .local v3, index:I
    iget-object v6, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@4b
    invoke-static {v6, v2, v3, v4}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$500(Lcom/lge/ims/setting/MediaSettingsCarrier;Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@4e
    .line 717
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    #@51
    .line 719
    const/4 v0, -0x1

    #@52
    .line 722
    .local v0, codecIndex:I
    iget-object v6, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@54
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@56
    const v8, 0x7f0602e0

    #@59
    invoke-virtual {v7, v8}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@5c
    move-result-object v7

    #@5d
    invoke-static {v6, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@60
    move-result-object v5

    #@61
    .line 723
    .local v5, pref:Landroid/preference/Preference;
    const-string v6, "H264"

    #@63
    const-string v7, "VGA"

    #@65
    invoke-virtual {p0, p1, v6, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->searchCodecIndex(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@68
    move-result v0

    #@69
    .line 724
    new-instance v6, Ljava/lang/StringBuilder;

    #@6b
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@6e
    const-string v7, "videocodec_"

    #@70
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v6

    #@74
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@77
    move-result-object v6

    #@78
    const-string v7, "_bitrate"

    #@7a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v6

    #@7e
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@81
    move-result-object v6

    #@82
    invoke-virtual {p0, p1, v5, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->loadDBToPreference(Ljava/lang/String;Landroid/preference/Preference;Ljava/lang/String;)Z

    #@85
    .line 726
    iget-object v6, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@87
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@89
    const v8, 0x7f0602e4

    #@8c
    invoke-virtual {v7, v8}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@8f
    move-result-object v7

    #@90
    invoke-static {v6, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@93
    move-result-object v5

    #@94
    .line 727
    new-instance v6, Ljava/lang/StringBuilder;

    #@96
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@99
    const-string v7, "videocodec_"

    #@9b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v6

    #@9f
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v6

    #@a3
    const-string v7, "_AS"

    #@a5
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v6

    #@a9
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ac
    move-result-object v6

    #@ad
    invoke-virtual {p0, p1, v5, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->loadDBToPreference(Ljava/lang/String;Landroid/preference/Preference;Ljava/lang/String;)Z

    #@b0
    .line 730
    iget-object v6, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@b2
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@b4
    const v8, 0x7f0602e1

    #@b7
    invoke-virtual {v7, v8}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@ba
    move-result-object v7

    #@bb
    invoke-static {v6, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@be
    move-result-object v5

    #@bf
    .line 731
    const-string v6, "H264"

    #@c1
    const-string v7, "QVGA"

    #@c3
    invoke-virtual {p0, p1, v6, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->searchCodecIndex(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@c6
    move-result v0

    #@c7
    .line 732
    new-instance v6, Ljava/lang/StringBuilder;

    #@c9
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@cc
    const-string v7, "videocodec_"

    #@ce
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v6

    #@d2
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v6

    #@d6
    const-string v7, "_bitrate"

    #@d8
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@db
    move-result-object v6

    #@dc
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@df
    move-result-object v6

    #@e0
    invoke-virtual {p0, p1, v5, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->loadDBToPreference(Ljava/lang/String;Landroid/preference/Preference;Ljava/lang/String;)Z

    #@e3
    .line 734
    iget-object v6, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@e5
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@e7
    const v8, 0x7f0602e5

    #@ea
    invoke-virtual {v7, v8}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@ed
    move-result-object v7

    #@ee
    invoke-static {v6, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@f1
    move-result-object v5

    #@f2
    .line 735
    new-instance v6, Ljava/lang/StringBuilder;

    #@f4
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@f7
    const-string v7, "videocodec_"

    #@f9
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fc
    move-result-object v6

    #@fd
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@100
    move-result-object v6

    #@101
    const-string v7, "_AS"

    #@103
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@106
    move-result-object v6

    #@107
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10a
    move-result-object v6

    #@10b
    invoke-virtual {p0, p1, v5, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->loadDBToPreference(Ljava/lang/String;Landroid/preference/Preference;Ljava/lang/String;)Z

    #@10e
    .line 738
    iget-object v6, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@110
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@112
    const v8, 0x7f0602e2

    #@115
    invoke-virtual {v7, v8}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@118
    move-result-object v7

    #@119
    invoke-static {v6, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@11c
    move-result-object v5

    #@11d
    .line 739
    const-string v6, "H264"

    #@11f
    const-string v7, "QCIF"

    #@121
    invoke-virtual {p0, p1, v6, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->searchCodecIndex(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@124
    move-result v0

    #@125
    .line 740
    new-instance v6, Ljava/lang/StringBuilder;

    #@127
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@12a
    const-string v7, "videocodec_"

    #@12c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12f
    move-result-object v6

    #@130
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@133
    move-result-object v6

    #@134
    const-string v7, "_bitrate"

    #@136
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@139
    move-result-object v6

    #@13a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13d
    move-result-object v6

    #@13e
    invoke-virtual {p0, p1, v5, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->loadDBToPreference(Ljava/lang/String;Landroid/preference/Preference;Ljava/lang/String;)Z

    #@141
    .line 742
    iget-object v6, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@143
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@145
    const v8, 0x7f0602e6

    #@148
    invoke-virtual {v7, v8}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@14b
    move-result-object v7

    #@14c
    invoke-static {v6, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@14f
    move-result-object v5

    #@150
    .line 743
    new-instance v6, Ljava/lang/StringBuilder;

    #@152
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@155
    const-string v7, "videocodec_"

    #@157
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15a
    move-result-object v6

    #@15b
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15e
    move-result-object v6

    #@15f
    const-string v7, "_AS"

    #@161
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@164
    move-result-object v6

    #@165
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@168
    move-result-object v6

    #@169
    invoke-virtual {p0, p1, v5, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->loadDBToPreference(Ljava/lang/String;Landroid/preference/Preference;Ljava/lang/String;)Z

    #@16c
    .line 746
    iget-object v6, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@16e
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@170
    const v8, 0x7f0602e3

    #@173
    invoke-virtual {v7, v8}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@176
    move-result-object v7

    #@177
    invoke-static {v6, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@17a
    move-result-object v5

    #@17b
    .line 747
    const-string v6, "H263"

    #@17d
    const-string v7, "QCIF"

    #@17f
    invoke-virtual {p0, p1, v6, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->searchCodecIndex(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@182
    move-result v0

    #@183
    .line 748
    new-instance v6, Ljava/lang/StringBuilder;

    #@185
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@188
    const-string v7, "videocodec_"

    #@18a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18d
    move-result-object v6

    #@18e
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@191
    move-result-object v6

    #@192
    const-string v7, "_bitrate"

    #@194
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@197
    move-result-object v6

    #@198
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19b
    move-result-object v6

    #@19c
    invoke-virtual {p0, p1, v5, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->loadDBToPreference(Ljava/lang/String;Landroid/preference/Preference;Ljava/lang/String;)Z

    #@19f
    .line 750
    iget-object v6, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@1a1
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@1a3
    const v8, 0x7f0602e7

    #@1a6
    invoke-virtual {v7, v8}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@1a9
    move-result-object v7

    #@1aa
    invoke-static {v6, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@1ad
    move-result-object v5

    #@1ae
    .line 751
    new-instance v6, Ljava/lang/StringBuilder;

    #@1b0
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@1b3
    const-string v7, "videocodec_"

    #@1b5
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b8
    move-result-object v6

    #@1b9
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1bc
    move-result-object v6

    #@1bd
    const-string v7, "_AS"

    #@1bf
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c2
    move-result-object v6

    #@1c3
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c6
    move-result-object v6

    #@1c7
    invoke-virtual {p0, p1, v5, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->loadDBToPreference(Ljava/lang/String;Landroid/preference/Preference;Ljava/lang/String;)Z

    #@1ca
    goto/16 :goto_29
.end method
