.class Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierPSVTChangeListener;
.super Ljava/lang/Object;
.source "MediaSettingsCarrier.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/setting/MediaSettingsCarrier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CarrierPSVTChangeListener"
.end annotation


# instance fields
.field m_videoCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;

.field final synthetic this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;


# direct methods
.method public constructor <init>(Lcom/lge/ims/setting/MediaSettingsCarrier;Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;)V
    .registers 5
    .parameter
    .parameter "command"

    #@0
    .prologue
    .line 938
    iput-object p1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierPSVTChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 939
    iput-object p2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierPSVTChangeListener;->m_videoCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;

    #@7
    .line 942
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierPSVTChangeListener;->m_videoCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;

    #@9
    invoke-virtual {v0, p0}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->setVideoListener(Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierPSVTChangeListener;)V

    #@c
    .line 943
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierPSVTChangeListener;->m_videoCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;

    #@e
    const-string v1, "lgims_com_media_video_codec_vt"

    #@10
    invoke-virtual {v0, v1}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->updatePreference(Ljava/lang/String;)V

    #@13
    .line 944
    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .registers 11
    .parameter "preference"
    .parameter "newValue"

    #@0
    .prologue
    const/16 v7, 0x200

    #@2
    .line 948
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    .line 949
    .local v1, key:Ljava/lang/String;
    const/4 v0, 0x0

    #@7
    .line 950
    .local v0, isUpdate:Z
    const-string v2, "IMSMediaSettingsCarrier"

    #@9
    new-instance v3, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v4, "onPreferenceChange - key : "

    #@10
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    const-string v4, ", value : "

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@21
    move-result-object v4

    #@22
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@2d
    .line 952
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierPSVTChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@2f
    const v3, 0x7f0601a5

    #@32
    invoke-virtual {v2, v3}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@35
    move-result-object v2

    #@36
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@39
    move-result v2

    #@3a
    if-eqz v2, :cond_56

    #@3c
    .line 953
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierPSVTChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@3e
    const-string v3, "lgims_com_media_video"

    #@40
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@43
    move-result-object v4

    #@44
    invoke-virtual {v2, v3, v1, v4}, Lcom/lge/ims/setting/MediaSettingsCarrier;->updateDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@47
    move-result v2

    #@48
    if-eqz v2, :cond_4f

    #@4a
    .line 954
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierPSVTChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@4c
    invoke-virtual {v2, p1, p2}, Lcom/lge/ims/setting/MediaSettingsCarrier;->summaryChange(Landroid/preference/Preference;Ljava/lang/Object;)V

    #@4f
    .line 956
    :cond_4f
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierPSVTChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@51
    invoke-static {v2, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$676(Lcom/lge/ims/setting/MediaSettingsCarrier;I)I

    #@54
    .line 957
    const/4 v2, 0x1

    #@55
    .line 990
    :goto_55
    return v2

    #@56
    .line 959
    :cond_56
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierPSVTChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@58
    const v3, 0x7f0602e0

    #@5b
    invoke-virtual {v2, v3}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@5e
    move-result-object v2

    #@5f
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@62
    move-result v2

    #@63
    if-eqz v2, :cond_83

    #@65
    .line 960
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierPSVTChangeListener;->m_videoCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;

    #@67
    const-string v3, "lgims_com_media_video_codec_vt"

    #@69
    const-string v4, "H264"

    #@6b
    const-string v5, "VGA"

    #@6d
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@70
    move-result-object v6

    #@71
    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->updateCodecBitrateToDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@74
    move-result v0

    #@75
    .line 985
    :cond_75
    :goto_75
    if-eqz v0, :cond_81

    #@77
    .line 986
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierPSVTChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@79
    invoke-virtual {v2, p1, p2}, Lcom/lge/ims/setting/MediaSettingsCarrier;->summaryChange(Landroid/preference/Preference;Ljava/lang/Object;)V

    #@7c
    .line 987
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierPSVTChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@7e
    invoke-static {v2, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$676(Lcom/lge/ims/setting/MediaSettingsCarrier;I)I

    #@81
    :cond_81
    move v2, v0

    #@82
    .line 990
    goto :goto_55

    #@83
    .line 963
    :cond_83
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierPSVTChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@85
    const v3, 0x7f0602e4

    #@88
    invoke-virtual {v2, v3}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@8b
    move-result-object v2

    #@8c
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8f
    move-result v2

    #@90
    if-eqz v2, :cond_a3

    #@92
    .line 964
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierPSVTChangeListener;->m_videoCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;

    #@94
    const-string v3, "lgims_com_media_video_codec_vt"

    #@96
    const-string v4, "H264"

    #@98
    const-string v5, "VGA"

    #@9a
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@9d
    move-result-object v6

    #@9e
    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->updateCodecBandwidthToDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@a1
    move-result v0

    #@a2
    goto :goto_75

    #@a3
    .line 966
    :cond_a3
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierPSVTChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@a5
    const v3, 0x7f0602e1

    #@a8
    invoke-virtual {v2, v3}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@ab
    move-result-object v2

    #@ac
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@af
    move-result v2

    #@b0
    if-eqz v2, :cond_c3

    #@b2
    .line 967
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierPSVTChangeListener;->m_videoCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;

    #@b4
    const-string v3, "lgims_com_media_video_codec_vt"

    #@b6
    const-string v4, "H264"

    #@b8
    const-string v5, "QVGA"

    #@ba
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@bd
    move-result-object v6

    #@be
    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->updateCodecBitrateToDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@c1
    move-result v0

    #@c2
    goto :goto_75

    #@c3
    .line 969
    :cond_c3
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierPSVTChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@c5
    const v3, 0x7f0602e5

    #@c8
    invoke-virtual {v2, v3}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@cb
    move-result-object v2

    #@cc
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@cf
    move-result v2

    #@d0
    if-eqz v2, :cond_e3

    #@d2
    .line 970
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierPSVTChangeListener;->m_videoCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;

    #@d4
    const-string v3, "lgims_com_media_video_codec_vt"

    #@d6
    const-string v4, "H264"

    #@d8
    const-string v5, "QVGA"

    #@da
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@dd
    move-result-object v6

    #@de
    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->updateCodecBandwidthToDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@e1
    move-result v0

    #@e2
    goto :goto_75

    #@e3
    .line 972
    :cond_e3
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierPSVTChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@e5
    const v3, 0x7f0602e2

    #@e8
    invoke-virtual {v2, v3}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@eb
    move-result-object v2

    #@ec
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ef
    move-result v2

    #@f0
    if-eqz v2, :cond_104

    #@f2
    .line 973
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierPSVTChangeListener;->m_videoCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;

    #@f4
    const-string v3, "lgims_com_media_video_codec_vt"

    #@f6
    const-string v4, "H264"

    #@f8
    const-string v5, "QCIF"

    #@fa
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@fd
    move-result-object v6

    #@fe
    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->updateCodecBitrateToDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@101
    move-result v0

    #@102
    goto/16 :goto_75

    #@104
    .line 975
    :cond_104
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierPSVTChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@106
    const v3, 0x7f0602e6

    #@109
    invoke-virtual {v2, v3}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@10c
    move-result-object v2

    #@10d
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@110
    move-result v2

    #@111
    if-eqz v2, :cond_125

    #@113
    .line 976
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierPSVTChangeListener;->m_videoCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;

    #@115
    const-string v3, "lgims_com_media_video_codec_vt"

    #@117
    const-string v4, "H264"

    #@119
    const-string v5, "QCIF"

    #@11b
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@11e
    move-result-object v6

    #@11f
    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->updateCodecBandwidthToDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@122
    move-result v0

    #@123
    goto/16 :goto_75

    #@125
    .line 978
    :cond_125
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierPSVTChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@127
    const v3, 0x7f0602e3

    #@12a
    invoke-virtual {v2, v3}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@12d
    move-result-object v2

    #@12e
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@131
    move-result v2

    #@132
    if-eqz v2, :cond_146

    #@134
    .line 979
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierPSVTChangeListener;->m_videoCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;

    #@136
    const-string v3, "lgims_com_media_video_codec_vt"

    #@138
    const-string v4, "H263"

    #@13a
    const-string v5, "QCIF"

    #@13c
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@13f
    move-result-object v6

    #@140
    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->updateCodecBitrateToDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@143
    move-result v0

    #@144
    goto/16 :goto_75

    #@146
    .line 981
    :cond_146
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierPSVTChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@148
    const v3, 0x7f0602e7

    #@14b
    invoke-virtual {v2, v3}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@14e
    move-result-object v2

    #@14f
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@152
    move-result v2

    #@153
    if-eqz v2, :cond_75

    #@155
    .line 982
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierPSVTChangeListener;->m_videoCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;

    #@157
    const-string v3, "lgims_com_media_video_codec_vt"

    #@159
    const-string v4, "H263"

    #@15b
    const-string v5, "QCIF"

    #@15d
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@160
    move-result-object v6

    #@161
    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;->updateCodecBandwidthToDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@164
    move-result v0

    #@165
    goto/16 :goto_75
.end method
