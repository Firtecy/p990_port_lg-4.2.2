.class public Lcom/lge/ims/setting/IMSSettings;
.super Landroid/preference/PreferenceActivity;
.source "IMSSettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/setting/IMSSettings$TestChangeListener;,
        Lcom/lge/ims/setting/IMSSettings$OperatorSpecificChangeListener;,
        Lcom/lge/ims/setting/IMSSettings$ComSIPChangeListener;,
        Lcom/lge/ims/setting/IMSSettings$SIPChangeListener;,
        Lcom/lge/ims/setting/IMSSettings$ACSettingsChangeListener;,
        Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;,
        Lcom/lge/ims/setting/IMSSettings$DebugScreenHandler;,
        Lcom/lge/ims/setting/IMSSettings$RebootThread;
    }
.end annotation


# static fields
.field private static final EVENT_DATA_CONNECTION_STATE_CHANGED:I = 0x66

.field private static final EVENT_UPDATE_DEBUG_SCREEN:I = 0x65

.field private static final LGIMS_COM_SIP:Ljava/lang/String; = "lgims_com_kt_sip"

.field private static final LGIMS_COM_SIP_VT:Ljava/lang/String; = "lgims_com_kt_sip_vt"

.field private static final LGIMS_UC:Ljava/lang/String; = "lgims_com_kt_service_uc"

.field private static final LGIMS_VT:Ljava/lang/String; = "lgims_com_kt_service_vt"

.field private static final TAG:Ljava/lang/String; = "LGIMSSettings"


# instance fields
.field private mAC:Landroid/preference/CheckBoxPreference;

.field private mACCommonChangeListener:Lcom/lge/ims/setting/IMSSettings$ACSettingsChangeListener;

.field private mACConfigurationChanged:Z

.field private mACServerSelection:Ljava/lang/String;

.field private mAuthAlgorithm:Landroid/preference/EditTextPreference;

.field private mAuthPassword:Landroid/preference/EditTextPreference;

.field private mAuthRealm:Landroid/preference/EditTextPreference;

.field private mAuthUsername:Landroid/preference/EditTextPreference;

.field private mComSIPChangeListener:Lcom/lge/ims/setting/IMSSettings$ComSIPChangeListener;

.field private mConnectionServerChangeListener:Lcom/lge/ims/setting/IMSSettings$ACSettingsChangeListener;

.field private mDebugScreenHandler:Lcom/lge/ims/setting/IMSSettings$DebugScreenHandler;

.field private mDebugScreenUpdateRequired:Z

.field private mDefaultSettingsPreference:Landroid/preference/Preference;

.field private mIMPI:Landroid/preference/EditTextPreference;

.field private mIMPU:Landroid/preference/EditTextPreference;

.field private mIMS:Landroid/preference/CheckBoxPreference;

.field private mIMSI:Landroid/preference/Preference;

.field private mIPAddress:Landroid/preference/Preference;

.field private mIPAddressForWiFi:Landroid/preference/Preference;

.field private mISIM:Landroid/preference/CheckBoxPreference;

.field private mImsDB:Landroid/database/sqlite/SQLiteDatabase;

.field private mImsDebugPreference:Landroid/preference/Preference;

.field private mImsRegistrationStatus:Landroid/preference/Preference;

.field private mMSISDN:Landroid/preference/Preference;

.field private mRealPasswordForMD5:Ljava/lang/String;

.field private mSIPChangeListener:Lcom/lge/ims/setting/IMSSettings$SIPChangeListener;

.field private mSIPPreference:Landroid/preference/Preference;

.field private mServiceCode:Landroid/preference/EditTextPreference;

.field private mSettingsUpdatedFlags:I

.field private mSubscriberChangeListener:Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;

.field private mSubscriberPreference:Landroid/preference/Preference;

.field private mTestChangeListener:Lcom/lge/ims/setting/IMSSettings$TestChangeListener;

.field private mTestMode:Landroid/preference/CheckBoxPreference;

.field private mTestPreference:Landroid/preference/Preference;

.field private mUSIM:Landroid/preference/CheckBoxPreference;


# direct methods
.method public constructor <init>()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 50
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    #@5
    .line 61
    iput-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mImsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@7
    .line 62
    iput-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mSubscriberPreference:Landroid/preference/Preference;

    #@9
    .line 63
    iput-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mSIPPreference:Landroid/preference/Preference;

    #@b
    .line 64
    iput-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mDefaultSettingsPreference:Landroid/preference/Preference;

    #@d
    .line 65
    iput-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mTestPreference:Landroid/preference/Preference;

    #@f
    .line 66
    iput-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mImsDebugPreference:Landroid/preference/Preference;

    #@11
    .line 67
    iput-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mImsRegistrationStatus:Landroid/preference/Preference;

    #@13
    .line 68
    iput-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mIMSI:Landroid/preference/Preference;

    #@15
    .line 69
    iput-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mMSISDN:Landroid/preference/Preference;

    #@17
    .line 70
    iput-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mIPAddress:Landroid/preference/Preference;

    #@19
    .line 71
    iput-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mIPAddressForWiFi:Landroid/preference/Preference;

    #@1b
    .line 72
    iput-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mAC:Landroid/preference/CheckBoxPreference;

    #@1d
    .line 73
    iput-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mIMS:Landroid/preference/CheckBoxPreference;

    #@1f
    .line 74
    iput-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mISIM:Landroid/preference/CheckBoxPreference;

    #@21
    .line 75
    iput-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mUSIM:Landroid/preference/CheckBoxPreference;

    #@23
    .line 76
    iput-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mTestMode:Landroid/preference/CheckBoxPreference;

    #@25
    .line 77
    iput-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mIMPI:Landroid/preference/EditTextPreference;

    #@27
    .line 78
    iput-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mIMPU:Landroid/preference/EditTextPreference;

    #@29
    .line 79
    iput-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mServiceCode:Landroid/preference/EditTextPreference;

    #@2b
    .line 80
    iput-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mAuthUsername:Landroid/preference/EditTextPreference;

    #@2d
    .line 81
    iput-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mAuthRealm:Landroid/preference/EditTextPreference;

    #@2f
    .line 82
    iput-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mAuthPassword:Landroid/preference/EditTextPreference;

    #@31
    .line 83
    iput-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mAuthAlgorithm:Landroid/preference/EditTextPreference;

    #@33
    .line 85
    new-instance v0, Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;

    #@35
    invoke-direct {v0, p0, v2}, Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;-><init>(Lcom/lge/ims/setting/IMSSettings;Lcom/lge/ims/setting/IMSSettings$1;)V

    #@38
    iput-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mSubscriberChangeListener:Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;

    #@3a
    .line 86
    new-instance v0, Lcom/lge/ims/setting/IMSSettings$ACSettingsChangeListener;

    #@3c
    sget-object v1, Lcom/lge/ims/provider/ac/AC$Settings$Common;->CONTENT_URI:Landroid/net/Uri;

    #@3e
    invoke-direct {v0, p0, v1}, Lcom/lge/ims/setting/IMSSettings$ACSettingsChangeListener;-><init>(Lcom/lge/ims/setting/IMSSettings;Landroid/net/Uri;)V

    #@41
    iput-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mACCommonChangeListener:Lcom/lge/ims/setting/IMSSettings$ACSettingsChangeListener;

    #@43
    .line 87
    new-instance v0, Lcom/lge/ims/setting/IMSSettings$ACSettingsChangeListener;

    #@45
    sget-object v1, Lcom/lge/ims/provider/ac/AC$Settings$ConnectionServer;->CONTENT_URI:Landroid/net/Uri;

    #@47
    invoke-direct {v0, p0, v1}, Lcom/lge/ims/setting/IMSSettings$ACSettingsChangeListener;-><init>(Lcom/lge/ims/setting/IMSSettings;Landroid/net/Uri;)V

    #@4a
    iput-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mConnectionServerChangeListener:Lcom/lge/ims/setting/IMSSettings$ACSettingsChangeListener;

    #@4c
    .line 88
    new-instance v0, Lcom/lge/ims/setting/IMSSettings$SIPChangeListener;

    #@4e
    invoke-direct {v0, p0, v2}, Lcom/lge/ims/setting/IMSSettings$SIPChangeListener;-><init>(Lcom/lge/ims/setting/IMSSettings;Lcom/lge/ims/setting/IMSSettings$1;)V

    #@51
    iput-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mSIPChangeListener:Lcom/lge/ims/setting/IMSSettings$SIPChangeListener;

    #@53
    .line 89
    new-instance v0, Lcom/lge/ims/setting/IMSSettings$ComSIPChangeListener;

    #@55
    invoke-direct {v0, p0, v2}, Lcom/lge/ims/setting/IMSSettings$ComSIPChangeListener;-><init>(Lcom/lge/ims/setting/IMSSettings;Lcom/lge/ims/setting/IMSSettings$1;)V

    #@58
    iput-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mComSIPChangeListener:Lcom/lge/ims/setting/IMSSettings$ComSIPChangeListener;

    #@5a
    .line 91
    new-instance v0, Lcom/lge/ims/setting/IMSSettings$TestChangeListener;

    #@5c
    invoke-direct {v0, p0, v2}, Lcom/lge/ims/setting/IMSSettings$TestChangeListener;-><init>(Lcom/lge/ims/setting/IMSSettings;Lcom/lge/ims/setting/IMSSettings$1;)V

    #@5f
    iput-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mTestChangeListener:Lcom/lge/ims/setting/IMSSettings$TestChangeListener;

    #@61
    .line 92
    new-instance v0, Lcom/lge/ims/setting/IMSSettings$DebugScreenHandler;

    #@63
    invoke-direct {v0, p0}, Lcom/lge/ims/setting/IMSSettings$DebugScreenHandler;-><init>(Lcom/lge/ims/setting/IMSSettings;)V

    #@66
    iput-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mDebugScreenHandler:Lcom/lge/ims/setting/IMSSettings$DebugScreenHandler;

    #@68
    .line 94
    iput v3, p0, Lcom/lge/ims/setting/IMSSettings;->mSettingsUpdatedFlags:I

    #@6a
    .line 96
    const-string v0, "0"

    #@6c
    iput-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mACServerSelection:Ljava/lang/String;

    #@6e
    .line 98
    iput-boolean v3, p0, Lcom/lge/ims/setting/IMSSettings;->mACConfigurationChanged:Z

    #@70
    .line 100
    iput-boolean v3, p0, Lcom/lge/ims/setting/IMSSettings;->mDebugScreenUpdateRequired:Z

    #@72
    .line 101
    iput-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mRealPasswordForMD5:Ljava/lang/String;

    #@74
    .line 1486
    return-void
.end method

.method static synthetic access$1000(Lcom/lge/ims/setting/IMSSettings;)Landroid/database/sqlite/SQLiteDatabase;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mImsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@2
    return-object v0
.end method

.method static synthetic access$1100(Lcom/lge/ims/setting/IMSSettings;)Landroid/preference/EditTextPreference;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mAuthUsername:Landroid/preference/EditTextPreference;

    #@2
    return-object v0
.end method

.method static synthetic access$1200(Lcom/lge/ims/setting/IMSSettings;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mRealPasswordForMD5:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$1202(Lcom/lge/ims/setting/IMSSettings;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 50
    iput-object p1, p0, Lcom/lge/ims/setting/IMSSettings;->mRealPasswordForMD5:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$1300(Lcom/lge/ims/setting/IMSSettings;)Landroid/preference/EditTextPreference;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mAuthPassword:Landroid/preference/EditTextPreference;

    #@2
    return-object v0
.end method

.method static synthetic access$1400(Lcom/lge/ims/setting/IMSSettings;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/lge/ims/setting/IMSSettings;->checkAndSetEnabledOnIMSChanged(Z)V

    #@3
    return-void
.end method

.method static synthetic access$1500(Lcom/lge/ims/setting/IMSSettings;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/lge/ims/setting/IMSSettings;->checkAndSetEnabledOnUSIMChanged(Z)V

    #@3
    return-void
.end method

.method static synthetic access$1676(Lcom/lge/ims/setting/IMSSettings;I)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 50
    iget v0, p0, Lcom/lge/ims/setting/IMSSettings;->mSettingsUpdatedFlags:I

    #@2
    or-int/2addr v0, p1

    #@3
    iput v0, p0, Lcom/lge/ims/setting/IMSSettings;->mSettingsUpdatedFlags:I

    #@5
    return v0
.end method

.method static synthetic access$1700(Lcom/lge/ims/setting/IMSSettings;Landroid/preference/Preference;Ljava/lang/Object;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/lge/ims/setting/IMSSettings;->setSummary(Landroid/preference/Preference;Ljava/lang/Object;)V

    #@3
    return-void
.end method

.method static synthetic access$1800(Lcom/lge/ims/setting/IMSSettings;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mACServerSelection:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$1802(Lcom/lge/ims/setting/IMSSettings;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 50
    iput-object p1, p0, Lcom/lge/ims/setting/IMSSettings;->mACServerSelection:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$1902(Lcom/lge/ims/setting/IMSSettings;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/lge/ims/setting/IMSSettings;->mACConfigurationChanged:Z

    #@2
    return p1
.end method

.method static synthetic access$2000(Lcom/lge/ims/setting/IMSSettings;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/lge/ims/setting/IMSSettings;->checkAndSetEnabledOnServiceCodeChanged(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/lge/ims/setting/IMSSettings;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 50
    invoke-direct {p0}, Lcom/lge/ims/setting/IMSSettings;->restoreDefaultSettings()V

    #@3
    return-void
.end method

.method static synthetic access$500(Lcom/lge/ims/setting/IMSSettings;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 50
    invoke-direct {p0}, Lcom/lge/ims/setting/IMSSettings;->updateRegistrationStatus()V

    #@3
    return-void
.end method

.method static synthetic access$600(Lcom/lge/ims/setting/IMSSettings;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/lge/ims/setting/IMSSettings;->mDebugScreenUpdateRequired:Z

    #@2
    return v0
.end method

.method static synthetic access$700(Lcom/lge/ims/setting/IMSSettings;)Lcom/lge/ims/setting/IMSSettings$DebugScreenHandler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mDebugScreenHandler:Lcom/lge/ims/setting/IMSSettings$DebugScreenHandler;

    #@2
    return-object v0
.end method

.method static synthetic access$800(Lcom/lge/ims/setting/IMSSettings;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 50
    invoke-direct {p0}, Lcom/lge/ims/setting/IMSSettings;->updateIPAddress()V

    #@3
    return-void
.end method

.method static synthetic access$900(Lcom/lge/ims/setting/IMSSettings;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 50
    invoke-direct {p0}, Lcom/lge/ims/setting/IMSSettings;->updateIPAddressForWiFi()V

    #@3
    return-void
.end method

.method private checkAndSetEnabledOnACChanged(Z)V
    .registers 6
    .parameter "enabled"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 651
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mTestMode:Landroid/preference/CheckBoxPreference;

    #@4
    if-eqz v0, :cond_11

    #@6
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mTestMode:Landroid/preference/CheckBoxPreference;

    #@8
    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_11

    #@e
    .line 653
    if-eqz p1, :cond_11

    #@10
    .line 654
    const/4 p1, 0x0

    #@11
    .line 658
    :cond_11
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mAuthUsername:Landroid/preference/EditTextPreference;

    #@13
    if-eqz v0, :cond_1d

    #@15
    .line 659
    iget-object v3, p0, Lcom/lge/ims/setting/IMSSettings;->mAuthUsername:Landroid/preference/EditTextPreference;

    #@17
    if-nez p1, :cond_41

    #@19
    move v0, v1

    #@1a
    :goto_1a
    invoke-virtual {v3, v0}, Landroid/preference/EditTextPreference;->setEnabled(Z)V

    #@1d
    .line 662
    :cond_1d
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mAuthRealm:Landroid/preference/EditTextPreference;

    #@1f
    if-eqz v0, :cond_29

    #@21
    .line 663
    iget-object v3, p0, Lcom/lge/ims/setting/IMSSettings;->mAuthRealm:Landroid/preference/EditTextPreference;

    #@23
    if-nez p1, :cond_43

    #@25
    move v0, v1

    #@26
    :goto_26
    invoke-virtual {v3, v0}, Landroid/preference/EditTextPreference;->setEnabled(Z)V

    #@29
    .line 666
    :cond_29
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mAuthPassword:Landroid/preference/EditTextPreference;

    #@2b
    if-eqz v0, :cond_35

    #@2d
    .line 667
    iget-object v3, p0, Lcom/lge/ims/setting/IMSSettings;->mAuthPassword:Landroid/preference/EditTextPreference;

    #@2f
    if-nez p1, :cond_45

    #@31
    move v0, v1

    #@32
    :goto_32
    invoke-virtual {v3, v0}, Landroid/preference/EditTextPreference;->setEnabled(Z)V

    #@35
    .line 670
    :cond_35
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mAuthAlgorithm:Landroid/preference/EditTextPreference;

    #@37
    if-eqz v0, :cond_40

    #@39
    .line 671
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mAuthAlgorithm:Landroid/preference/EditTextPreference;

    #@3b
    if-nez p1, :cond_47

    #@3d
    :goto_3d
    invoke-virtual {v0, v1}, Landroid/preference/EditTextPreference;->setEnabled(Z)V

    #@40
    .line 673
    :cond_40
    return-void

    #@41
    :cond_41
    move v0, v2

    #@42
    .line 659
    goto :goto_1a

    #@43
    :cond_43
    move v0, v2

    #@44
    .line 663
    goto :goto_26

    #@45
    :cond_45
    move v0, v2

    #@46
    .line 667
    goto :goto_32

    #@47
    :cond_47
    move v1, v2

    #@48
    .line 671
    goto :goto_3d
.end method

.method private checkAndSetEnabledOnIMSChanged(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 676
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mSubscriberPreference:Landroid/preference/Preference;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 677
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mSubscriberPreference:Landroid/preference/Preference;

    #@6
    invoke-virtual {v0, p1}, Landroid/preference/Preference;->setEnabled(Z)V

    #@9
    .line 680
    :cond_9
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mSIPPreference:Landroid/preference/Preference;

    #@b
    if-eqz v0, :cond_12

    #@d
    .line 681
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mSIPPreference:Landroid/preference/Preference;

    #@f
    invoke-virtual {v0, p1}, Landroid/preference/Preference;->setEnabled(Z)V

    #@12
    .line 684
    :cond_12
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mDefaultSettingsPreference:Landroid/preference/Preference;

    #@14
    if-eqz v0, :cond_1b

    #@16
    .line 685
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mDefaultSettingsPreference:Landroid/preference/Preference;

    #@18
    invoke-virtual {v0, p1}, Landroid/preference/Preference;->setEnabled(Z)V

    #@1b
    .line 688
    :cond_1b
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mTestPreference:Landroid/preference/Preference;

    #@1d
    if-eqz v0, :cond_24

    #@1f
    .line 689
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mTestPreference:Landroid/preference/Preference;

    #@21
    invoke-virtual {v0, p1}, Landroid/preference/Preference;->setEnabled(Z)V

    #@24
    .line 691
    :cond_24
    return-void
.end method

.method private checkAndSetEnabledOnServiceCodeChanged(Ljava/lang/String;)V
    .registers 5
    .parameter "code"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 704
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mImsDebugPreference:Landroid/preference/Preference;

    #@4
    if-nez v0, :cond_7

    #@6
    .line 734
    :cond_6
    :goto_6
    return-void

    #@7
    .line 708
    :cond_7
    if-nez p1, :cond_18

    #@9
    .line 709
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mImsDebugPreference:Landroid/preference/Preference;

    #@b
    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    #@e
    .line 711
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mISIM:Landroid/preference/CheckBoxPreference;

    #@10
    if-eqz v0, :cond_6

    #@12
    .line 712
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mISIM:Landroid/preference/CheckBoxPreference;

    #@14
    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    #@17
    goto :goto_6

    #@18
    .line 717
    :cond_18
    const-string v0, "ims"

    #@1a
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v0

    #@1e
    if-eqz v0, :cond_2f

    #@20
    .line 718
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mImsDebugPreference:Landroid/preference/Preference;

    #@22
    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setEnabled(Z)V

    #@25
    .line 720
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mISIM:Landroid/preference/CheckBoxPreference;

    #@27
    if-eqz v0, :cond_6

    #@29
    .line 721
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mISIM:Landroid/preference/CheckBoxPreference;

    #@2b
    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    #@2e
    goto :goto_6

    #@2f
    .line 724
    :cond_2f
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mImsDebugPreference:Landroid/preference/Preference;

    #@31
    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    #@34
    .line 726
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mISIM:Landroid/preference/CheckBoxPreference;

    #@36
    if-eqz v0, :cond_6

    #@38
    .line 727
    const-string v0, "isim"

    #@3a
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3d
    move-result v0

    #@3e
    if-eqz v0, :cond_46

    #@40
    .line 728
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mISIM:Landroid/preference/CheckBoxPreference;

    #@42
    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    #@45
    goto :goto_6

    #@46
    .line 730
    :cond_46
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mISIM:Landroid/preference/CheckBoxPreference;

    #@48
    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    #@4b
    goto :goto_6
.end method

.method private checkAndSetEnabledOnUSIMChanged(Z)V
    .registers 6
    .parameter "enabled"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 694
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mIMPI:Landroid/preference/EditTextPreference;

    #@4
    if-eqz v0, :cond_e

    #@6
    .line 695
    iget-object v3, p0, Lcom/lge/ims/setting/IMSSettings;->mIMPI:Landroid/preference/EditTextPreference;

    #@8
    if-nez p1, :cond_1a

    #@a
    move v0, v1

    #@b
    :goto_b
    invoke-virtual {v3, v0}, Landroid/preference/EditTextPreference;->setEnabled(Z)V

    #@e
    .line 698
    :cond_e
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mIMPU:Landroid/preference/EditTextPreference;

    #@10
    if-eqz v0, :cond_19

    #@12
    .line 699
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mIMPU:Landroid/preference/EditTextPreference;

    #@14
    if-nez p1, :cond_1c

    #@16
    :goto_16
    invoke-virtual {v0, v1}, Landroid/preference/EditTextPreference;->setEnabled(Z)V

    #@19
    .line 701
    :cond_19
    return-void

    #@1a
    :cond_1a
    move v0, v2

    #@1b
    .line 695
    goto :goto_b

    #@1c
    :cond_1c
    move v1, v2

    #@1d
    .line 699
    goto :goto_16
.end method

.method private displayColumns([Ljava/lang/String;)V
    .registers 6
    .parameter "columns"

    #@0
    .prologue
    .line 737
    invoke-static {}, Lcom/lge/ims/Configuration;->isDebugOn()Z

    #@3
    move-result v1

    #@4
    if-nez v1, :cond_7

    #@6
    .line 753
    :cond_6
    return-void

    #@7
    .line 741
    :cond_7
    if-eqz p1, :cond_6

    #@9
    .line 745
    const/4 v0, 0x0

    #@a
    .local v0, i:I
    :goto_a
    array-length v1, p1

    #@b
    if-ge v0, v1, :cond_6

    #@d
    .line 746
    aget-object v1, p1, v0

    #@f
    if-nez v1, :cond_32

    #@11
    .line 747
    const-string v1, "LGIMSSettings"

    #@13
    new-instance v2, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v3, "column at index ("

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    const-string v3, ") is null"

    #@24
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v2

    #@2c
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 745
    :goto_2f
    add-int/lit8 v0, v0, 0x1

    #@31
    goto :goto_a

    #@32
    .line 751
    :cond_32
    const-string v1, "LGIMSSettings"

    #@34
    new-instance v2, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v3, "Column at index ("

    #@3b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@42
    move-result-object v2

    #@43
    const-string v3, ")"

    #@45
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v2

    #@49
    aget-object v3, p1, v0

    #@4b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v2

    #@4f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v2

    #@53
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    goto :goto_2f
.end method

.method private getCheckBoxPreference(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 792
    invoke-virtual {p0}, Lcom/lge/ims/setting/IMSSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    #@3
    move-result-object v0

    #@4
    .line 794
    .local v0, root:Landroid/preference/PreferenceScreen;
    if-eqz v0, :cond_d

    #@6
    .line 795
    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    #@9
    move-result-object v1

    #@a
    check-cast v1, Landroid/preference/CheckBoxPreference;

    #@c
    .line 798
    :goto_c
    return-object v1

    #@d
    :cond_d
    const/4 v1, 0x0

    #@e
    goto :goto_c
.end method

.method private getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I
    .registers 5
    .parameter "columns"
    .parameter "column"

    #@0
    .prologue
    .line 782
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    array-length v1, p1

    #@2
    if-ge v0, v1, :cond_10

    #@4
    .line 783
    aget-object v1, p1, v0

    #@6
    invoke-virtual {p2, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@9
    move-result v1

    #@a
    if-nez v1, :cond_d

    #@c
    .line 788
    .end local v0           #i:I
    :goto_c
    return v0

    #@d
    .line 782
    .restart local v0       #i:I
    :cond_d
    add-int/lit8 v0, v0, 0x1

    #@f
    goto :goto_1

    #@10
    .line 788
    :cond_10
    const/4 v0, -0x1

    #@11
    goto :goto_c
.end method

.method private getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 812
    invoke-virtual {p0}, Lcom/lge/ims/setting/IMSSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    #@3
    move-result-object v0

    #@4
    .line 814
    .local v0, root:Landroid/preference/PreferenceScreen;
    if-eqz v0, :cond_d

    #@6
    .line 815
    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    #@9
    move-result-object v1

    #@a
    check-cast v1, Landroid/preference/EditTextPreference;

    #@c
    .line 818
    :goto_c
    return-object v1

    #@d
    :cond_d
    const/4 v1, 0x0

    #@e
    goto :goto_c
.end method

.method private getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 802
    invoke-virtual {p0}, Lcom/lge/ims/setting/IMSSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    #@3
    move-result-object v0

    #@4
    .line 804
    .local v0, root:Landroid/preference/PreferenceScreen;
    if-eqz v0, :cond_d

    #@6
    .line 805
    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    #@9
    move-result-object v1

    #@a
    check-cast v1, Landroid/preference/ListPreference;

    #@c
    .line 808
    :goto_c
    return-object v1

    #@d
    :cond_d
    const/4 v1, 0x0

    #@e
    goto :goto_c
.end method

.method private getPreference(Ljava/lang/String;)Landroid/preference/Preference;
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 822
    invoke-virtual {p0}, Lcom/lge/ims/setting/IMSSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    #@3
    move-result-object v0

    #@4
    .line 824
    .local v0, root:Landroid/preference/PreferenceScreen;
    if-eqz v0, :cond_b

    #@6
    .line 825
    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    #@9
    move-result-object v1

    #@a
    .line 828
    :goto_a
    return-object v1

    #@b
    :cond_b
    const/4 v1, 0x0

    #@c
    goto :goto_a
.end method

.method private getSettingsTable(Ljava/lang/String;)Landroid/database/Cursor;
    .registers 7
    .parameter "table"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 756
    iget-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mImsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 778
    :cond_5
    :goto_5
    return-object v0

    #@6
    .line 760
    :cond_6
    if-eqz p1, :cond_5

    #@8
    .line 764
    const/4 v0, 0x0

    #@9
    .line 767
    .local v0, cursor:Landroid/database/Cursor;
    :try_start_9
    iget-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mImsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@b
    new-instance v3, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v4, "select * from "

    #@12
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v3

    #@1e
    const/4 v4, 0x0

    #@1f
    invoke-virtual {v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_22
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_9 .. :try_end_22} :catch_24

    #@22
    move-result-object v0

    #@23
    goto :goto_5

    #@24
    .line 768
    :catch_24
    move-exception v1

    #@25
    .line 769
    .local v1, e:Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    #@28
    .line 771
    if-eqz v0, :cond_2d

    #@2a
    .line 772
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    #@2d
    .line 775
    :cond_2d
    const/4 v0, 0x0

    #@2e
    goto :goto_5
.end method

.method private registerACCommonChangeListener()V
    .registers 11

    #@0
    .prologue
    .line 430
    const/4 v7, 0x0

    #@1
    .line 431
    .local v7, cursor:Landroid/database/Cursor;
    invoke-virtual {p0}, Lcom/lge/ims/setting/IMSSettings;->getContentResolver()Landroid/content/ContentResolver;

    #@4
    move-result-object v0

    #@5
    .line 434
    .local v0, cr:Landroid/content/ContentResolver;
    :try_start_5
    sget-object v1, Lcom/lge/ims/provider/ac/AC$Settings$Common;->CONTENT_URI:Landroid/net/Uri;

    #@7
    const/4 v2, 0x0

    #@8
    const/4 v3, 0x0

    #@9
    const/4 v4, 0x0

    #@a
    const/4 v5, 0x0

    #@b
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@e
    move-result-object v7

    #@f
    .line 436
    if-eqz v7, :cond_30

    #@11
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    #@14
    move-result v1

    #@15
    if-eqz v1, :cond_30

    #@17
    .line 438
    const-string v1, "update_exception_list"

    #@19
    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@1c
    move-result v6

    #@1d
    .line 439
    .local v6, column:I
    const v1, 0x7f060021

    #@20
    invoke-virtual {p0, v1}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    invoke-direct {p0, v1}, Lcom/lge/ims/setting/IMSSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@27
    move-result-object v9

    #@28
    .line 440
    .local v9, edit:Landroid/preference/EditTextPreference;
    iget-object v1, p0, Lcom/lge/ims/setting/IMSSettings;->mACCommonChangeListener:Lcom/lge/ims/setting/IMSSettings$ACSettingsChangeListener;

    #@2a
    invoke-direct {p0, v9, v1}, Lcom/lge/ims/setting/IMSSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@2d
    .line 441
    invoke-direct {p0, v7, v6, v9}, Lcom/lge/ims/setting/IMSSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V
    :try_end_30
    .catchall {:try_start_5 .. :try_end_30} :catchall_5c
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_30} :catch_36
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_30} :catch_49

    #@30
    .line 450
    .end local v6           #column:I
    .end local v9           #edit:Landroid/preference/EditTextPreference;
    :cond_30
    if-eqz v7, :cond_35

    #@32
    .line 451
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@35
    .line 454
    :cond_35
    :goto_35
    return-void

    #@36
    .line 443
    :catch_36
    move-exception v8

    #@37
    .line 444
    .local v8, e:Landroid/database/sqlite/SQLiteException;
    :try_start_37
    const-string v1, "LGIMSSettings"

    #@39
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    #@3c
    move-result-object v2

    #@3d
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 445
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_43
    .catchall {:try_start_37 .. :try_end_43} :catchall_5c

    #@43
    .line 450
    if-eqz v7, :cond_35

    #@45
    .line 451
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@48
    goto :goto_35

    #@49
    .line 446
    .end local v8           #e:Landroid/database/sqlite/SQLiteException;
    :catch_49
    move-exception v8

    #@4a
    .line 447
    .local v8, e:Ljava/lang/Exception;
    :try_start_4a
    const-string v1, "LGIMSSettings"

    #@4c
    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@4f
    move-result-object v2

    #@50
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    .line 448
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_56
    .catchall {:try_start_4a .. :try_end_56} :catchall_5c

    #@56
    .line 450
    if-eqz v7, :cond_35

    #@58
    .line 451
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@5b
    goto :goto_35

    #@5c
    .line 450
    .end local v8           #e:Ljava/lang/Exception;
    :catchall_5c
    move-exception v1

    #@5d
    if-eqz v7, :cond_62

    #@5f
    .line 451
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@62
    :cond_62
    throw v1
.end method

.method private registerACConnectionServerChangeListener()V
    .registers 12

    #@0
    .prologue
    .line 459
    const/4 v7, 0x0

    #@1
    .line 460
    .local v7, cursor:Landroid/database/Cursor;
    invoke-virtual {p0}, Lcom/lge/ims/setting/IMSSettings;->getContentResolver()Landroid/content/ContentResolver;

    #@4
    move-result-object v0

    #@5
    .line 463
    .local v0, cr:Landroid/content/ContentResolver;
    :try_start_5
    sget-object v1, Lcom/lge/ims/provider/ac/AC$Settings$ConnectionServer;->CONTENT_URI:Landroid/net/Uri;

    #@7
    const/4 v2, 0x0

    #@8
    const/4 v3, 0x0

    #@9
    const/4 v4, 0x0

    #@a
    const/4 v5, 0x0

    #@b
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@e
    move-result-object v7

    #@f
    .line 465
    if-eqz v7, :cond_84

    #@11
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    #@14
    move-result v1

    #@15
    if-eqz v1, :cond_84

    #@17
    .line 467
    const-string v1, "server_selection"

    #@19
    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@1c
    move-result v6

    #@1d
    .line 468
    .local v6, column:I
    const v1, 0x7f060027

    #@20
    invoke-virtual {p0, v1}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    invoke-direct {p0, v1}, Lcom/lge/ims/setting/IMSSettings;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@27
    move-result-object v10

    #@28
    .line 469
    .local v10, list:Landroid/preference/ListPreference;
    iget-object v1, p0, Lcom/lge/ims/setting/IMSSettings;->mConnectionServerChangeListener:Lcom/lge/ims/setting/IMSSettings$ACSettingsChangeListener;

    #@2a
    invoke-direct {p0, v10, v1}, Lcom/lge/ims/setting/IMSSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@2d
    .line 470
    invoke-direct {p0, v7, v6, v10}, Lcom/lge/ims/setting/IMSSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@30
    .line 472
    if-eqz v10, :cond_52

    #@32
    .line 473
    invoke-virtual {v10}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    #@35
    move-result-object v1

    #@36
    iput-object v1, p0, Lcom/lge/ims/setting/IMSSettings;->mACServerSelection:Ljava/lang/String;

    #@38
    .line 474
    const-string v1, "LGIMSSettings"

    #@3a
    new-instance v2, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const-string v3, "AC server selection="

    #@41
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v2

    #@45
    iget-object v3, p0, Lcom/lge/ims/setting/IMSSettings;->mACServerSelection:Ljava/lang/String;

    #@47
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v2

    #@4f
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 478
    :cond_52
    const-string v1, "commercial_network"

    #@54
    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@57
    move-result v6

    #@58
    .line 479
    const v1, 0x7f060028

    #@5b
    invoke-virtual {p0, v1}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@5e
    move-result-object v1

    #@5f
    invoke-direct {p0, v1}, Lcom/lge/ims/setting/IMSSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@62
    move-result-object v9

    #@63
    .line 480
    .local v9, edit:Landroid/preference/EditTextPreference;
    iget-object v1, p0, Lcom/lge/ims/setting/IMSSettings;->mConnectionServerChangeListener:Lcom/lge/ims/setting/IMSSettings$ACSettingsChangeListener;

    #@65
    invoke-direct {p0, v9, v1}, Lcom/lge/ims/setting/IMSSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@68
    .line 481
    invoke-direct {p0, v7, v6, v9}, Lcom/lge/ims/setting/IMSSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@6b
    .line 484
    const-string v1, "testbed"

    #@6d
    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@70
    move-result v6

    #@71
    .line 485
    const v1, 0x7f060029

    #@74
    invoke-virtual {p0, v1}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@77
    move-result-object v1

    #@78
    invoke-direct {p0, v1}, Lcom/lge/ims/setting/IMSSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@7b
    move-result-object v9

    #@7c
    .line 486
    iget-object v1, p0, Lcom/lge/ims/setting/IMSSettings;->mConnectionServerChangeListener:Lcom/lge/ims/setting/IMSSettings$ACSettingsChangeListener;

    #@7e
    invoke-direct {p0, v9, v1}, Lcom/lge/ims/setting/IMSSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@81
    .line 487
    invoke-direct {p0, v7, v6, v9}, Lcom/lge/ims/setting/IMSSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V
    :try_end_84
    .catchall {:try_start_5 .. :try_end_84} :catchall_b0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_84} :catch_8a
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_84} :catch_9d

    #@84
    .line 496
    .end local v6           #column:I
    .end local v9           #edit:Landroid/preference/EditTextPreference;
    .end local v10           #list:Landroid/preference/ListPreference;
    :cond_84
    if-eqz v7, :cond_89

    #@86
    .line 497
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@89
    .line 500
    :cond_89
    :goto_89
    return-void

    #@8a
    .line 489
    :catch_8a
    move-exception v8

    #@8b
    .line 490
    .local v8, e:Landroid/database/sqlite/SQLiteException;
    :try_start_8b
    const-string v1, "LGIMSSettings"

    #@8d
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    #@90
    move-result-object v2

    #@91
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@94
    .line 491
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_97
    .catchall {:try_start_8b .. :try_end_97} :catchall_b0

    #@97
    .line 496
    if-eqz v7, :cond_89

    #@99
    .line 497
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@9c
    goto :goto_89

    #@9d
    .line 492
    .end local v8           #e:Landroid/database/sqlite/SQLiteException;
    :catch_9d
    move-exception v8

    #@9e
    .line 493
    .local v8, e:Ljava/lang/Exception;
    :try_start_9e
    const-string v1, "LGIMSSettings"

    #@a0
    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@a3
    move-result-object v2

    #@a4
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a7
    .line 494
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_aa
    .catchall {:try_start_9e .. :try_end_aa} :catchall_b0

    #@aa
    .line 496
    if-eqz v7, :cond_89

    #@ac
    .line 497
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@af
    goto :goto_89

    #@b0
    .line 496
    .end local v8           #e:Ljava/lang/Exception;
    :catchall_b0
    move-exception v1

    #@b1
    if-eqz v7, :cond_b6

    #@b3
    .line 497
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@b6
    :cond_b6
    throw v1
.end method

.method private registerComSIPChangeListener()V
    .registers 11

    #@0
    .prologue
    const v9, 0x7f060145

    #@3
    const v8, 0x7f060144

    #@6
    const v7, 0x7f060143

    #@9
    const v6, 0x7f060142

    #@c
    const v5, 0x7f060141

    #@f
    .line 546
    const-string v4, "lgims_com_kt_sip"

    #@11
    invoke-direct {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getSettingsTable(Ljava/lang/String;)Landroid/database/Cursor;

    #@14
    move-result-object v1

    #@15
    .line 548
    .local v1, cursor:Landroid/database/Cursor;
    if-nez v1, :cond_1f

    #@17
    .line 549
    const-string v4, "LGIMSSettings"

    #@19
    const-string v5, "Cursor :: (lgims_com_kt_sip) is null"

    #@1b
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 627
    :goto_1e
    return-void

    #@1f
    .line 553
    :cond_1f
    invoke-interface {v1}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    .line 555
    .local v0, columns:[Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/lge/ims/setting/IMSSettings;->displayColumns([Ljava/lang/String;)V

    #@26
    .line 558
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    #@29
    .line 560
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@2c
    move-result-object v4

    #@2d
    invoke-direct {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@30
    move-result-object v2

    #@31
    .line 561
    .local v2, edit:Landroid/preference/EditTextPreference;
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mComSIPChangeListener:Lcom/lge/ims/setting/IMSSettings$ComSIPChangeListener;

    #@33
    invoke-direct {p0, v2, v4}, Lcom/lge/ims/setting/IMSSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@36
    .line 562
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@39
    move-result-object v4

    #@3a
    invoke-direct {p0, v0, v4}, Lcom/lge/ims/setting/IMSSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    move-result v3

    #@3e
    .line 563
    .local v3, index:I
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@41
    .line 564
    invoke-direct {p0, v2}, Lcom/lge/ims/setting/IMSSettings;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@44
    .line 566
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@47
    move-result-object v4

    #@48
    invoke-direct {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@4b
    move-result-object v2

    #@4c
    .line 567
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mComSIPChangeListener:Lcom/lge/ims/setting/IMSSettings$ComSIPChangeListener;

    #@4e
    invoke-direct {p0, v2, v4}, Lcom/lge/ims/setting/IMSSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@51
    .line 568
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@54
    move-result-object v4

    #@55
    invoke-direct {p0, v0, v4}, Lcom/lge/ims/setting/IMSSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@58
    move-result v3

    #@59
    .line 569
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@5c
    .line 570
    invoke-direct {p0, v2}, Lcom/lge/ims/setting/IMSSettings;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@5f
    .line 572
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@62
    move-result-object v4

    #@63
    invoke-direct {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@66
    move-result-object v2

    #@67
    .line 573
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mComSIPChangeListener:Lcom/lge/ims/setting/IMSSettings$ComSIPChangeListener;

    #@69
    invoke-direct {p0, v2, v4}, Lcom/lge/ims/setting/IMSSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@6c
    .line 574
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@6f
    move-result-object v4

    #@70
    invoke-direct {p0, v0, v4}, Lcom/lge/ims/setting/IMSSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@73
    move-result v3

    #@74
    .line 575
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@77
    .line 576
    invoke-direct {p0, v2}, Lcom/lge/ims/setting/IMSSettings;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@7a
    .line 578
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@7d
    move-result-object v4

    #@7e
    invoke-direct {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@81
    move-result-object v2

    #@82
    .line 579
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mComSIPChangeListener:Lcom/lge/ims/setting/IMSSettings$ComSIPChangeListener;

    #@84
    invoke-direct {p0, v2, v4}, Lcom/lge/ims/setting/IMSSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@87
    .line 580
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@8a
    move-result-object v4

    #@8b
    invoke-direct {p0, v0, v4}, Lcom/lge/ims/setting/IMSSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@8e
    move-result v3

    #@8f
    .line 581
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@92
    .line 582
    invoke-direct {p0, v2}, Lcom/lge/ims/setting/IMSSettings;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@95
    .line 584
    invoke-virtual {p0, v9}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@98
    move-result-object v4

    #@99
    invoke-direct {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@9c
    move-result-object v2

    #@9d
    .line 585
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mComSIPChangeListener:Lcom/lge/ims/setting/IMSSettings$ComSIPChangeListener;

    #@9f
    invoke-direct {p0, v2, v4}, Lcom/lge/ims/setting/IMSSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@a2
    .line 586
    invoke-virtual {p0, v9}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@a5
    move-result-object v4

    #@a6
    invoke-direct {p0, v0, v4}, Lcom/lge/ims/setting/IMSSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@a9
    move-result v3

    #@aa
    .line 587
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@ad
    .line 588
    invoke-direct {p0, v2}, Lcom/lge/ims/setting/IMSSettings;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@b0
    .line 590
    const v4, 0x7f060146

    #@b3
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@b6
    move-result-object v4

    #@b7
    invoke-direct {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@ba
    move-result-object v2

    #@bb
    .line 591
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mComSIPChangeListener:Lcom/lge/ims/setting/IMSSettings$ComSIPChangeListener;

    #@bd
    invoke-direct {p0, v2, v4}, Lcom/lge/ims/setting/IMSSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@c0
    .line 592
    const v4, 0x7f060146

    #@c3
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@c6
    move-result-object v4

    #@c7
    invoke-direct {p0, v0, v4}, Lcom/lge/ims/setting/IMSSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@ca
    move-result v3

    #@cb
    .line 593
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@ce
    .line 594
    invoke-direct {p0, v2}, Lcom/lge/ims/setting/IMSSettings;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@d1
    .line 596
    const v4, 0x7f060147

    #@d4
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@d7
    move-result-object v4

    #@d8
    invoke-direct {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@db
    move-result-object v2

    #@dc
    .line 597
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mComSIPChangeListener:Lcom/lge/ims/setting/IMSSettings$ComSIPChangeListener;

    #@de
    invoke-direct {p0, v2, v4}, Lcom/lge/ims/setting/IMSSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@e1
    .line 598
    const v4, 0x7f060147

    #@e4
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@e7
    move-result-object v4

    #@e8
    invoke-direct {p0, v0, v4}, Lcom/lge/ims/setting/IMSSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@eb
    move-result v3

    #@ec
    .line 599
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@ef
    .line 600
    invoke-direct {p0, v2}, Lcom/lge/ims/setting/IMSSettings;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@f2
    .line 602
    const v4, 0x7f060148

    #@f5
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@f8
    move-result-object v4

    #@f9
    invoke-direct {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@fc
    move-result-object v2

    #@fd
    .line 603
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mComSIPChangeListener:Lcom/lge/ims/setting/IMSSettings$ComSIPChangeListener;

    #@ff
    invoke-direct {p0, v2, v4}, Lcom/lge/ims/setting/IMSSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@102
    .line 604
    const v4, 0x7f060148

    #@105
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@108
    move-result-object v4

    #@109
    invoke-direct {p0, v0, v4}, Lcom/lge/ims/setting/IMSSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@10c
    move-result v3

    #@10d
    .line 605
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@110
    .line 606
    invoke-direct {p0, v2}, Lcom/lge/ims/setting/IMSSettings;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@113
    .line 608
    const v4, 0x7f060149

    #@116
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@119
    move-result-object v4

    #@11a
    invoke-direct {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@11d
    move-result-object v2

    #@11e
    .line 609
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mComSIPChangeListener:Lcom/lge/ims/setting/IMSSettings$ComSIPChangeListener;

    #@120
    invoke-direct {p0, v2, v4}, Lcom/lge/ims/setting/IMSSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@123
    .line 610
    const v4, 0x7f060149

    #@126
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@129
    move-result-object v4

    #@12a
    invoke-direct {p0, v0, v4}, Lcom/lge/ims/setting/IMSSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@12d
    move-result v3

    #@12e
    .line 611
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@131
    .line 612
    invoke-direct {p0, v2}, Lcom/lge/ims/setting/IMSSettings;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@134
    .line 614
    const v4, 0x7f06014c

    #@137
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@13a
    move-result-object v4

    #@13b
    invoke-direct {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@13e
    move-result-object v2

    #@13f
    .line 615
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mComSIPChangeListener:Lcom/lge/ims/setting/IMSSettings$ComSIPChangeListener;

    #@141
    invoke-direct {p0, v2, v4}, Lcom/lge/ims/setting/IMSSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@144
    .line 616
    const v4, 0x7f06014c

    #@147
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@14a
    move-result-object v4

    #@14b
    invoke-direct {p0, v0, v4}, Lcom/lge/ims/setting/IMSSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@14e
    move-result v3

    #@14f
    .line 617
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@152
    .line 619
    const v4, 0x7f060155

    #@155
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@158
    move-result-object v4

    #@159
    invoke-direct {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@15c
    move-result-object v2

    #@15d
    .line 620
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mComSIPChangeListener:Lcom/lge/ims/setting/IMSSettings$ComSIPChangeListener;

    #@15f
    invoke-direct {p0, v2, v4}, Lcom/lge/ims/setting/IMSSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@162
    .line 621
    const v4, 0x7f060155

    #@165
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@168
    move-result-object v4

    #@169
    invoke-direct {p0, v0, v4}, Lcom/lge/ims/setting/IMSSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@16c
    move-result v3

    #@16d
    .line 622
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@170
    .line 623
    invoke-direct {p0, v2}, Lcom/lge/ims/setting/IMSSettings;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@173
    .line 626
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    #@176
    goto/16 :goto_1e
.end method

.method private registerOperatorSpecificChangeListener()V
    .registers 1

    #@0
    .prologue
    .line 631
    return-void
.end method

.method private registerSIPChangeListener()V
    .registers 10

    #@0
    .prologue
    const v8, 0x7f06014f

    #@3
    const v7, 0x7f06014e

    #@6
    const v6, 0x7f06014d

    #@9
    .line 507
    const-string v5, "lgims_sip"

    #@b
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSSettings;->getSettingsTable(Ljava/lang/String;)Landroid/database/Cursor;

    #@e
    move-result-object v2

    #@f
    .line 509
    .local v2, cursor:Landroid/database/Cursor;
    if-nez v2, :cond_19

    #@11
    .line 510
    const-string v5, "LGIMSSettings"

    #@13
    const-string v6, "Cursor :: (lgims_sip) is null"

    #@15
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 540
    :goto_18
    return-void

    #@19
    .line 514
    :cond_19
    invoke-interface {v2}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    .line 516
    .local v1, columns:[Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/lge/ims/setting/IMSSettings;->displayColumns([Ljava/lang/String;)V

    #@20
    .line 519
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    #@23
    .line 521
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@26
    move-result-object v5

    #@27
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@2a
    move-result-object v3

    #@2b
    .line 522
    .local v3, edit:Landroid/preference/EditTextPreference;
    iget-object v5, p0, Lcom/lge/ims/setting/IMSSettings;->mSIPChangeListener:Lcom/lge/ims/setting/IMSSettings$SIPChangeListener;

    #@2d
    invoke-direct {p0, v3, v5}, Lcom/lge/ims/setting/IMSSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@30
    .line 523
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@33
    move-result-object v5

    #@34
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@37
    move-result v4

    #@38
    .line 524
    .local v4, index:I
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@3b
    .line 525
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSSettings;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@3e
    .line 527
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@41
    move-result-object v5

    #@42
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSSettings;->getCheckBoxPreference(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    #@45
    move-result-object v0

    #@46
    .line 528
    .local v0, checkbox:Landroid/preference/CheckBoxPreference;
    iget-object v5, p0, Lcom/lge/ims/setting/IMSSettings;->mSIPChangeListener:Lcom/lge/ims/setting/IMSSettings$SIPChangeListener;

    #@48
    invoke-direct {p0, v0, v5}, Lcom/lge/ims/setting/IMSSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@4b
    .line 529
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@4e
    move-result-object v5

    #@4f
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@52
    move-result v4

    #@53
    .line 530
    invoke-direct {p0, v2, v4, v0}, Lcom/lge/ims/setting/IMSSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/CheckBoxPreference;)V

    #@56
    .line 532
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@59
    move-result-object v5

    #@5a
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@5d
    move-result-object v3

    #@5e
    .line 533
    iget-object v5, p0, Lcom/lge/ims/setting/IMSSettings;->mSIPChangeListener:Lcom/lge/ims/setting/IMSSettings$SIPChangeListener;

    #@60
    invoke-direct {p0, v3, v5}, Lcom/lge/ims/setting/IMSSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@63
    .line 534
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@66
    move-result-object v5

    #@67
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@6a
    move-result v4

    #@6b
    .line 535
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@6e
    .line 536
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSSettings;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@71
    .line 539
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    #@74
    goto :goto_18
.end method

.method private registerSubscriberChangeListener()V
    .registers 12

    #@0
    .prologue
    const v10, 0x7f060118

    #@3
    const v9, 0x7f060116

    #@6
    const v8, 0x7f060114

    #@9
    const v7, 0x7f060113

    #@c
    const v6, 0x7f060112

    #@f
    .line 323
    const-string v4, "lgims_subscriber"

    #@11
    invoke-direct {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getSettingsTable(Ljava/lang/String;)Landroid/database/Cursor;

    #@14
    move-result-object v1

    #@15
    .line 325
    .local v1, cursor:Landroid/database/Cursor;
    if-nez v1, :cond_1f

    #@17
    .line 326
    const-string v4, "LGIMSSettings"

    #@19
    const-string v5, "Cursor :: (lgims_subscriber) is null"

    #@1b
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 426
    :goto_1e
    return-void

    #@1f
    .line 330
    :cond_1f
    invoke-interface {v1}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    .line 332
    .local v0, columns:[Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/lge/ims/setting/IMSSettings;->displayColumns([Ljava/lang/String;)V

    #@26
    .line 335
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    #@29
    .line 337
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@2c
    move-result-object v4

    #@2d
    invoke-direct {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getCheckBoxPreference(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    #@30
    move-result-object v4

    #@31
    iput-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mIMS:Landroid/preference/CheckBoxPreference;

    #@33
    .line 338
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mIMS:Landroid/preference/CheckBoxPreference;

    #@35
    iget-object v5, p0, Lcom/lge/ims/setting/IMSSettings;->mSubscriberChangeListener:Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;

    #@37
    invoke-direct {p0, v4, v5}, Lcom/lge/ims/setting/IMSSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@3a
    .line 339
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@3d
    move-result-object v4

    #@3e
    invoke-direct {p0, v0, v4}, Lcom/lge/ims/setting/IMSSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@41
    move-result v3

    #@42
    .line 340
    .local v3, index:I
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mIMS:Landroid/preference/CheckBoxPreference;

    #@44
    invoke-direct {p0, v1, v3, v4}, Lcom/lge/ims/setting/IMSSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/CheckBoxPreference;)V

    #@47
    .line 342
    invoke-virtual {p0, v9}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@4a
    move-result-object v4

    #@4b
    invoke-direct {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getCheckBoxPreference(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    #@4e
    move-result-object v4

    #@4f
    iput-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mAC:Landroid/preference/CheckBoxPreference;

    #@51
    .line 343
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mAC:Landroid/preference/CheckBoxPreference;

    #@53
    iget-object v5, p0, Lcom/lge/ims/setting/IMSSettings;->mSubscriberChangeListener:Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;

    #@55
    invoke-direct {p0, v4, v5}, Lcom/lge/ims/setting/IMSSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@58
    .line 344
    invoke-virtual {p0, v9}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@5b
    move-result-object v4

    #@5c
    invoke-direct {p0, v0, v4}, Lcom/lge/ims/setting/IMSSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@5f
    move-result v3

    #@60
    .line 345
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mAC:Landroid/preference/CheckBoxPreference;

    #@62
    invoke-direct {p0, v1, v3, v4}, Lcom/lge/ims/setting/IMSSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/CheckBoxPreference;)V

    #@65
    .line 347
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@68
    move-result-object v4

    #@69
    invoke-direct {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getCheckBoxPreference(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    #@6c
    move-result-object v4

    #@6d
    iput-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mUSIM:Landroid/preference/CheckBoxPreference;

    #@6f
    .line 348
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mUSIM:Landroid/preference/CheckBoxPreference;

    #@71
    iget-object v5, p0, Lcom/lge/ims/setting/IMSSettings;->mSubscriberChangeListener:Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;

    #@73
    invoke-direct {p0, v4, v5}, Lcom/lge/ims/setting/IMSSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@76
    .line 349
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@79
    move-result-object v4

    #@7a
    invoke-direct {p0, v0, v4}, Lcom/lge/ims/setting/IMSSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@7d
    move-result v3

    #@7e
    .line 350
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mUSIM:Landroid/preference/CheckBoxPreference;

    #@80
    invoke-direct {p0, v1, v3, v4}, Lcom/lge/ims/setting/IMSSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/CheckBoxPreference;)V

    #@83
    .line 352
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@86
    move-result-object v4

    #@87
    invoke-direct {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getCheckBoxPreference(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    #@8a
    move-result-object v4

    #@8b
    iput-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mISIM:Landroid/preference/CheckBoxPreference;

    #@8d
    .line 353
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mISIM:Landroid/preference/CheckBoxPreference;

    #@8f
    iget-object v5, p0, Lcom/lge/ims/setting/IMSSettings;->mSubscriberChangeListener:Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;

    #@91
    invoke-direct {p0, v4, v5}, Lcom/lge/ims/setting/IMSSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@94
    .line 354
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@97
    move-result-object v4

    #@98
    invoke-direct {p0, v0, v4}, Lcom/lge/ims/setting/IMSSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@9b
    move-result v3

    #@9c
    .line 355
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mISIM:Landroid/preference/CheckBoxPreference;

    #@9e
    invoke-direct {p0, v1, v3, v4}, Lcom/lge/ims/setting/IMSSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/CheckBoxPreference;)V

    #@a1
    .line 357
    invoke-virtual {p0, v10}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@a4
    move-result-object v4

    #@a5
    invoke-direct {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getCheckBoxPreference(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    #@a8
    move-result-object v4

    #@a9
    iput-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mTestMode:Landroid/preference/CheckBoxPreference;

    #@ab
    .line 358
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mTestMode:Landroid/preference/CheckBoxPreference;

    #@ad
    iget-object v5, p0, Lcom/lge/ims/setting/IMSSettings;->mSubscriberChangeListener:Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;

    #@af
    invoke-direct {p0, v4, v5}, Lcom/lge/ims/setting/IMSSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@b2
    .line 359
    invoke-virtual {p0, v10}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@b5
    move-result-object v4

    #@b6
    invoke-direct {p0, v0, v4}, Lcom/lge/ims/setting/IMSSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@b9
    move-result v3

    #@ba
    .line 360
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mTestMode:Landroid/preference/CheckBoxPreference;

    #@bc
    invoke-direct {p0, v1, v3, v4}, Lcom/lge/ims/setting/IMSSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/CheckBoxPreference;)V

    #@bf
    .line 362
    const v4, 0x7f06011a

    #@c2
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@c5
    move-result-object v4

    #@c6
    invoke-direct {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@c9
    move-result-object v2

    #@ca
    .line 363
    .local v2, edit:Landroid/preference/EditTextPreference;
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mSubscriberChangeListener:Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;

    #@cc
    invoke-direct {p0, v2, v4}, Lcom/lge/ims/setting/IMSSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@cf
    .line 364
    const v4, 0x7f06011a

    #@d2
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@d5
    move-result-object v4

    #@d6
    invoke-direct {p0, v0, v4}, Lcom/lge/ims/setting/IMSSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@d9
    move-result v3

    #@da
    .line 365
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@dd
    .line 367
    const v4, 0x7f06011d

    #@e0
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@e3
    move-result-object v4

    #@e4
    invoke-direct {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@e7
    move-result-object v2

    #@e8
    .line 368
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mSubscriberChangeListener:Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;

    #@ea
    invoke-direct {p0, v2, v4}, Lcom/lge/ims/setting/IMSSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@ed
    .line 369
    const v4, 0x7f06011d

    #@f0
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@f3
    move-result-object v4

    #@f4
    invoke-direct {p0, v0, v4}, Lcom/lge/ims/setting/IMSSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@f7
    move-result v3

    #@f8
    .line 370
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@fb
    .line 371
    invoke-direct {p0, v2}, Lcom/lge/ims/setting/IMSSettings;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@fe
    .line 373
    const v4, 0x7f06011b

    #@101
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@104
    move-result-object v4

    #@105
    invoke-direct {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@108
    move-result-object v2

    #@109
    .line 374
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mSubscriberChangeListener:Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;

    #@10b
    invoke-direct {p0, v2, v4}, Lcom/lge/ims/setting/IMSSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@10e
    .line 375
    const v4, 0x7f06011b

    #@111
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@114
    move-result-object v4

    #@115
    invoke-direct {p0, v0, v4}, Lcom/lge/ims/setting/IMSSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@118
    move-result v3

    #@119
    .line 376
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@11c
    .line 378
    const v4, 0x7f06011e

    #@11f
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@122
    move-result-object v4

    #@123
    invoke-direct {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@126
    move-result-object v2

    #@127
    .line 379
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mSubscriberChangeListener:Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;

    #@129
    invoke-direct {p0, v2, v4}, Lcom/lge/ims/setting/IMSSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@12c
    .line 380
    const v4, 0x7f06011e

    #@12f
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@132
    move-result-object v4

    #@133
    invoke-direct {p0, v0, v4}, Lcom/lge/ims/setting/IMSSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@136
    move-result v3

    #@137
    .line 381
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@13a
    .line 382
    invoke-direct {p0, v2}, Lcom/lge/ims/setting/IMSSettings;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@13d
    .line 384
    const v4, 0x7f060121

    #@140
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@143
    move-result-object v4

    #@144
    invoke-direct {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@147
    move-result-object v2

    #@148
    .line 385
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mSubscriberChangeListener:Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;

    #@14a
    invoke-direct {p0, v2, v4}, Lcom/lge/ims/setting/IMSSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@14d
    .line 386
    const v4, 0x7f060121

    #@150
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@153
    move-result-object v4

    #@154
    invoke-direct {p0, v0, v4}, Lcom/lge/ims/setting/IMSSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@157
    move-result v3

    #@158
    .line 387
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@15b
    .line 389
    const v4, 0x7f060122

    #@15e
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@161
    move-result-object v4

    #@162
    invoke-direct {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@165
    move-result-object v4

    #@166
    iput-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mIMPI:Landroid/preference/EditTextPreference;

    #@168
    .line 390
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mIMPI:Landroid/preference/EditTextPreference;

    #@16a
    iget-object v5, p0, Lcom/lge/ims/setting/IMSSettings;->mSubscriberChangeListener:Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;

    #@16c
    invoke-direct {p0, v4, v5}, Lcom/lge/ims/setting/IMSSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@16f
    .line 391
    const v4, 0x7f060122

    #@172
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@175
    move-result-object v4

    #@176
    invoke-direct {p0, v0, v4}, Lcom/lge/ims/setting/IMSSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@179
    move-result v3

    #@17a
    .line 392
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mIMPI:Landroid/preference/EditTextPreference;

    #@17c
    invoke-direct {p0, v1, v3, v4}, Lcom/lge/ims/setting/IMSSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@17f
    .line 394
    const v4, 0x7f060125

    #@182
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@185
    move-result-object v4

    #@186
    invoke-direct {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@189
    move-result-object v4

    #@18a
    iput-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mIMPU:Landroid/preference/EditTextPreference;

    #@18c
    .line 395
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mIMPU:Landroid/preference/EditTextPreference;

    #@18e
    iget-object v5, p0, Lcom/lge/ims/setting/IMSSettings;->mSubscriberChangeListener:Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;

    #@190
    invoke-direct {p0, v4, v5}, Lcom/lge/ims/setting/IMSSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@193
    .line 396
    const v4, 0x7f060125

    #@196
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@199
    move-result-object v4

    #@19a
    invoke-direct {p0, v0, v4}, Lcom/lge/ims/setting/IMSSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@19d
    move-result v3

    #@19e
    .line 397
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mIMPU:Landroid/preference/EditTextPreference;

    #@1a0
    invoke-direct {p0, v1, v3, v4}, Lcom/lge/ims/setting/IMSSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@1a3
    .line 399
    const v4, 0x7f060129

    #@1a6
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@1a9
    move-result-object v4

    #@1aa
    invoke-direct {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@1ad
    move-result-object v4

    #@1ae
    iput-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mAuthUsername:Landroid/preference/EditTextPreference;

    #@1b0
    .line 400
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mAuthUsername:Landroid/preference/EditTextPreference;

    #@1b2
    iget-object v5, p0, Lcom/lge/ims/setting/IMSSettings;->mSubscriberChangeListener:Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;

    #@1b4
    invoke-direct {p0, v4, v5}, Lcom/lge/ims/setting/IMSSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@1b7
    .line 401
    const v4, 0x7f060129

    #@1ba
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@1bd
    move-result-object v4

    #@1be
    invoke-direct {p0, v0, v4}, Lcom/lge/ims/setting/IMSSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@1c1
    move-result v3

    #@1c2
    .line 402
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mAuthUsername:Landroid/preference/EditTextPreference;

    #@1c4
    invoke-direct {p0, v1, v3, v4}, Lcom/lge/ims/setting/IMSSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@1c7
    .line 404
    const v4, 0x7f06012a

    #@1ca
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@1cd
    move-result-object v4

    #@1ce
    invoke-direct {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@1d1
    move-result-object v4

    #@1d2
    iput-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mAuthRealm:Landroid/preference/EditTextPreference;

    #@1d4
    .line 405
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mAuthRealm:Landroid/preference/EditTextPreference;

    #@1d6
    iget-object v5, p0, Lcom/lge/ims/setting/IMSSettings;->mSubscriberChangeListener:Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;

    #@1d8
    invoke-direct {p0, v4, v5}, Lcom/lge/ims/setting/IMSSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@1db
    .line 406
    const v4, 0x7f06012a

    #@1de
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@1e1
    move-result-object v4

    #@1e2
    invoke-direct {p0, v0, v4}, Lcom/lge/ims/setting/IMSSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@1e5
    move-result v3

    #@1e6
    .line 407
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mAuthRealm:Landroid/preference/EditTextPreference;

    #@1e8
    invoke-direct {p0, v1, v3, v4}, Lcom/lge/ims/setting/IMSSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@1eb
    .line 409
    const v4, 0x7f06012b

    #@1ee
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@1f1
    move-result-object v4

    #@1f2
    invoke-direct {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@1f5
    move-result-object v4

    #@1f6
    iput-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mAuthPassword:Landroid/preference/EditTextPreference;

    #@1f8
    .line 410
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mAuthPassword:Landroid/preference/EditTextPreference;

    #@1fa
    iget-object v5, p0, Lcom/lge/ims/setting/IMSSettings;->mSubscriberChangeListener:Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;

    #@1fc
    invoke-direct {p0, v4, v5}, Lcom/lge/ims/setting/IMSSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@1ff
    .line 412
    const v4, 0x7f06012b

    #@202
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@205
    move-result-object v4

    #@206
    invoke-direct {p0, v0, v4}, Lcom/lge/ims/setting/IMSSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@209
    move-result v3

    #@20a
    .line 415
    if-eqz v1, :cond_214

    #@20c
    if-ltz v3, :cond_214

    #@20e
    .line 416
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@211
    move-result-object v4

    #@212
    iput-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mRealPasswordForMD5:Ljava/lang/String;

    #@214
    .line 419
    :cond_214
    const v4, 0x7f06012c

    #@217
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@21a
    move-result-object v4

    #@21b
    invoke-direct {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@21e
    move-result-object v4

    #@21f
    iput-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mAuthAlgorithm:Landroid/preference/EditTextPreference;

    #@221
    .line 420
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mAuthAlgorithm:Landroid/preference/EditTextPreference;

    #@223
    iget-object v5, p0, Lcom/lge/ims/setting/IMSSettings;->mSubscriberChangeListener:Lcom/lge/ims/setting/IMSSettings$SubscriberChangeListener;

    #@225
    invoke-direct {p0, v4, v5}, Lcom/lge/ims/setting/IMSSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@228
    .line 421
    const v4, 0x7f06012c

    #@22b
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@22e
    move-result-object v4

    #@22f
    invoke-direct {p0, v0, v4}, Lcom/lge/ims/setting/IMSSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@232
    move-result v3

    #@233
    .line 422
    iget-object v4, p0, Lcom/lge/ims/setting/IMSSettings;->mAuthAlgorithm:Landroid/preference/EditTextPreference;

    #@235
    invoke-direct {p0, v1, v3, v4}, Lcom/lge/ims/setting/IMSSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@238
    .line 425
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    #@23b
    goto/16 :goto_1e
.end method

.method private registerTestChangeListener()V
    .registers 3

    #@0
    .prologue
    .line 634
    const v0, 0x7f06015a

    #@3
    invoke-virtual {p0, v0}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    invoke-direct {p0, v0}, Lcom/lge/ims/setting/IMSSettings;->getPreference(Ljava/lang/String;)Landroid/preference/Preference;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mTestPreference:Landroid/preference/Preference;

    #@d
    .line 636
    const v0, 0x7f060010

    #@10
    invoke-virtual {p0, v0}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@13
    move-result-object v0

    #@14
    invoke-direct {p0, v0}, Lcom/lge/ims/setting/IMSSettings;->getPreference(Ljava/lang/String;)Landroid/preference/Preference;

    #@17
    move-result-object v0

    #@18
    iput-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mImsRegistrationStatus:Landroid/preference/Preference;

    #@1a
    .line 638
    const v0, 0x7f060013

    #@1d
    invoke-virtual {p0, v0}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@20
    move-result-object v0

    #@21
    invoke-direct {p0, v0}, Lcom/lge/ims/setting/IMSSettings;->getPreference(Ljava/lang/String;)Landroid/preference/Preference;

    #@24
    move-result-object v0

    #@25
    iput-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mIMSI:Landroid/preference/Preference;

    #@27
    .line 639
    const v0, 0x7f060014

    #@2a
    invoke-virtual {p0, v0}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@2d
    move-result-object v0

    #@2e
    invoke-direct {p0, v0}, Lcom/lge/ims/setting/IMSSettings;->getPreference(Ljava/lang/String;)Landroid/preference/Preference;

    #@31
    move-result-object v0

    #@32
    iput-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mMSISDN:Landroid/preference/Preference;

    #@34
    .line 640
    const v0, 0x7f060011

    #@37
    invoke-virtual {p0, v0}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@3a
    move-result-object v0

    #@3b
    invoke-direct {p0, v0}, Lcom/lge/ims/setting/IMSSettings;->getPreference(Ljava/lang/String;)Landroid/preference/Preference;

    #@3e
    move-result-object v0

    #@3f
    iput-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mIPAddress:Landroid/preference/Preference;

    #@41
    .line 641
    const v0, 0x7f060012

    #@44
    invoke-virtual {p0, v0}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@47
    move-result-object v0

    #@48
    invoke-direct {p0, v0}, Lcom/lge/ims/setting/IMSSettings;->getPreference(Ljava/lang/String;)Landroid/preference/Preference;

    #@4b
    move-result-object v0

    #@4c
    iput-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mIPAddressForWiFi:Landroid/preference/Preference;

    #@4e
    .line 643
    const v0, 0x7f06000e

    #@51
    invoke-virtual {p0, v0}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@54
    move-result-object v0

    #@55
    invoke-direct {p0, v0}, Lcom/lge/ims/setting/IMSSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@58
    move-result-object v0

    #@59
    iput-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mServiceCode:Landroid/preference/EditTextPreference;

    #@5b
    .line 644
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mServiceCode:Landroid/preference/EditTextPreference;

    #@5d
    iget-object v1, p0, Lcom/lge/ims/setting/IMSSettings;->mTestChangeListener:Lcom/lge/ims/setting/IMSSettings$TestChangeListener;

    #@5f
    invoke-direct {p0, v0, v1}, Lcom/lge/ims/setting/IMSSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@62
    .line 646
    const v0, 0x7f06010f

    #@65
    invoke-virtual {p0, v0}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@68
    move-result-object v0

    #@69
    invoke-direct {p0, v0}, Lcom/lge/ims/setting/IMSSettings;->getPreference(Ljava/lang/String;)Landroid/preference/Preference;

    #@6c
    move-result-object v0

    #@6d
    iput-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mImsDebugPreference:Landroid/preference/Preference;

    #@6f
    .line 647
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mImsDebugPreference:Landroid/preference/Preference;

    #@71
    iget-object v1, p0, Lcom/lge/ims/setting/IMSSettings;->mTestChangeListener:Lcom/lge/ims/setting/IMSSettings$TestChangeListener;

    #@73
    invoke-direct {p0, v0, v1}, Lcom/lge/ims/setting/IMSSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@76
    .line 648
    return-void
.end method

.method private restoreDefaultSettings()V
    .registers 13

    #@0
    .prologue
    .line 832
    invoke-static {}, Lcom/lge/ims/provider/ConfigurationManager;->getInstance()Lcom/lge/ims/provider/ConfigurationManager;

    #@3
    move-result-object v0

    #@4
    .line 834
    .local v0, cm:Lcom/lge/ims/provider/ConfigurationManager;
    if-nez v0, :cond_7

    #@6
    .line 924
    :cond_6
    :goto_6
    return-void

    #@7
    .line 838
    :cond_7
    invoke-virtual {v0}, Lcom/lge/ims/provider/ConfigurationManager;->getConfigurations()Ljava/util/ArrayList;

    #@a
    move-result-object v2

    #@b
    .line 840
    .local v2, configs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/lge/ims/provider/IConfiguration;>;"
    const/4 v8, 0x0

    #@c
    .local v8, i:I
    :goto_c
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@f
    move-result v9

    #@10
    if-ge v8, v9, :cond_6e

    #@12
    .line 841
    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@15
    move-result-object v1

    #@16
    check-cast v1, Lcom/lge/ims/provider/IConfiguration;

    #@18
    .line 843
    .local v1, config:Lcom/lge/ims/provider/IConfiguration;
    if-nez v1, :cond_1d

    #@1a
    .line 840
    :cond_1a
    :goto_1a
    add-int/lit8 v8, v8, 0x1

    #@1c
    goto :goto_c

    #@1d
    .line 847
    :cond_1d
    const-string v9, "lgims_subscriber"

    #@1f
    invoke-interface {v1, v9}, Lcom/lge/ims/provider/IConfiguration;->getTableContent(Ljava/lang/String;)[[Ljava/lang/String;

    #@22
    move-result-object v3

    #@23
    .line 849
    .local v3, content:[[Ljava/lang/String;
    if-eqz v3, :cond_38

    #@25
    .line 850
    const-string v9, "lgims_subscriber"

    #@27
    invoke-direct {p0, v9, v3}, Lcom/lge/ims/setting/IMSSettings;->setDefaultValues(Ljava/lang/String;[[Ljava/lang/String;)V

    #@2a
    .line 852
    const-string v9, "lgims_sip"

    #@2c
    invoke-interface {v1, v9}, Lcom/lge/ims/provider/IConfiguration;->getTableContent(Ljava/lang/String;)[[Ljava/lang/String;

    #@2f
    move-result-object v3

    #@30
    .line 854
    if-eqz v3, :cond_1a

    #@32
    .line 855
    const-string v9, "lgims_sip"

    #@34
    invoke-direct {p0, v9, v3}, Lcom/lge/ims/setting/IMSSettings;->setDefaultValues(Ljava/lang/String;[[Ljava/lang/String;)V

    #@37
    goto :goto_1a

    #@38
    .line 861
    :cond_38
    const-string v9, "lgims_com_kt_sip"

    #@3a
    invoke-interface {v1, v9}, Lcom/lge/ims/provider/IConfiguration;->getTableContent(Ljava/lang/String;)[[Ljava/lang/String;

    #@3d
    move-result-object v3

    #@3e
    .line 863
    if-eqz v3, :cond_46

    #@40
    .line 864
    const-string v9, "lgims_com_kt_sip"

    #@42
    invoke-direct {p0, v9, v3}, Lcom/lge/ims/setting/IMSSettings;->setDefaultValues(Ljava/lang/String;[[Ljava/lang/String;)V

    #@45
    goto :goto_1a

    #@46
    .line 868
    :cond_46
    const-string v9, "lgims_com_kt_service_uc"

    #@48
    invoke-interface {v1, v9}, Lcom/lge/ims/provider/IConfiguration;->getTableContent(Ljava/lang/String;)[[Ljava/lang/String;

    #@4b
    move-result-object v3

    #@4c
    .line 870
    if-eqz v3, :cond_1a

    #@4e
    .line 871
    const-string v9, "lgims_com_kt_service_uc"

    #@50
    invoke-direct {p0, v9, v3}, Lcom/lge/ims/setting/IMSSettings;->setDefaultValues(Ljava/lang/String;[[Ljava/lang/String;)V

    #@53
    .line 873
    const-string v9, "lgims_com_kt_service_vt"

    #@55
    invoke-interface {v1, v9}, Lcom/lge/ims/provider/IConfiguration;->getTableContent(Ljava/lang/String;)[[Ljava/lang/String;

    #@58
    move-result-object v3

    #@59
    .line 875
    if-eqz v3, :cond_60

    #@5b
    .line 876
    const-string v9, "lgims_com_kt_service_vt"

    #@5d
    invoke-direct {p0, v9, v3}, Lcom/lge/ims/setting/IMSSettings;->setDefaultValues(Ljava/lang/String;[[Ljava/lang/String;)V

    #@60
    .line 879
    :cond_60
    const-string v9, "lgims_com_kt_sip_vt"

    #@62
    invoke-interface {v1, v9}, Lcom/lge/ims/provider/IConfiguration;->getTableContent(Ljava/lang/String;)[[Ljava/lang/String;

    #@65
    move-result-object v3

    #@66
    .line 881
    if-eqz v3, :cond_1a

    #@68
    .line 882
    const-string v9, "lgims_com_kt_sip_vt"

    #@6a
    invoke-direct {p0, v9, v3}, Lcom/lge/ims/setting/IMSSettings;->setDefaultValues(Ljava/lang/String;[[Ljava/lang/String;)V

    #@6d
    goto :goto_1a

    #@6e
    .line 890
    .end local v1           #config:Lcom/lge/ims/provider/IConfiguration;
    .end local v3           #content:[[Ljava/lang/String;
    :cond_6e
    invoke-virtual {p0}, Lcom/lge/ims/setting/IMSSettings;->getContentResolver()Landroid/content/ContentResolver;

    #@71
    move-result-object v5

    #@72
    .line 891
    .local v5, cr:Landroid/content/ContentResolver;
    new-instance v6, Landroid/content/ContentValues;

    #@74
    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    #@77
    .line 893
    .local v6, cv:Landroid/content/ContentValues;
    const-string v9, "availability"

    #@79
    const-string v10, "true"

    #@7b
    invoke-virtual {v6, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@7e
    .line 894
    const-string v9, "validity"

    #@80
    const-string v10, "0"

    #@82
    invoke-virtual {v6, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@85
    .line 895
    const-string v9, "update_exception_list"

    #@87
    const-string v10, "0x00000000"

    #@89
    invoke-virtual {v6, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@8c
    .line 898
    :try_start_8c
    sget-object v9, Lcom/lge/ims/provider/ac/AC$Settings$Common;->CONTENT_URI:Landroid/net/Uri;

    #@8e
    const/4 v10, 0x0

    #@8f
    const/4 v11, 0x0

    #@90
    invoke-virtual {v5, v9, v6, v10, v11}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@93
    move-result v4

    #@94
    .line 899
    .local v4, count:I
    const-string v9, "LGIMSSettings"

    #@96
    new-instance v10, Ljava/lang/StringBuilder;

    #@98
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@9b
    const-string v11, "AC :: Common is updated; count="

    #@9d
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v10

    #@a1
    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v10

    #@a5
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a8
    move-result-object v10

    #@a9
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_ac
    .catch Ljava/lang/Exception; {:try_start_8c .. :try_end_ac} :catch_f1

    #@ac
    .line 906
    .end local v4           #count:I
    :goto_ac
    invoke-virtual {v6}, Landroid/content/ContentValues;->clear()V

    #@af
    .line 908
    const-string v9, "server_selection"

    #@b1
    const-string v10, "0"

    #@b3
    invoke-virtual {v6, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@b6
    .line 909
    const-string v9, "commercial_network"

    #@b8
    const-string v10, "https://config.rcs.mnc008.mcc450.pub.3gppnetwork.org:9443"

    #@ba
    invoke-virtual {v6, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@bd
    .line 910
    const-string v9, "testbed"

    #@bf
    const-string v10, "https://221.148.247.125:9443"

    #@c1
    invoke-virtual {v6, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@c4
    .line 913
    :try_start_c4
    sget-object v9, Lcom/lge/ims/provider/ac/AC$Settings$ConnectionServer;->CONTENT_URI:Landroid/net/Uri;

    #@c6
    const/4 v10, 0x0

    #@c7
    const/4 v11, 0x0

    #@c8
    invoke-virtual {v5, v9, v6, v10, v11}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@cb
    move-result v4

    #@cc
    .line 914
    .restart local v4       #count:I
    const-string v9, "LGIMSSettings"

    #@ce
    new-instance v10, Ljava/lang/StringBuilder;

    #@d0
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@d3
    const-string v11, "AC :: Connection server is updated; count="

    #@d5
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v10

    #@d9
    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v10

    #@dd
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e0
    move-result-object v10

    #@e1
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_e4
    .catch Ljava/lang/Exception; {:try_start_c4 .. :try_end_e4} :catch_112

    #@e4
    .line 921
    .end local v4           #count:I
    :goto_e4
    iget-object v9, p0, Lcom/lge/ims/setting/IMSSettings;->mServiceCode:Landroid/preference/EditTextPreference;

    #@e6
    if-eqz v9, :cond_6

    #@e8
    .line 922
    iget-object v9, p0, Lcom/lge/ims/setting/IMSSettings;->mServiceCode:Landroid/preference/EditTextPreference;

    #@ea
    const-string v10, ""

    #@ec
    invoke-virtual {v9, v10}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    #@ef
    goto/16 :goto_6

    #@f1
    .line 900
    :catch_f1
    move-exception v7

    #@f2
    .line 901
    .local v7, e:Ljava/lang/Exception;
    const-string v9, "LGIMSSettings"

    #@f4
    new-instance v10, Ljava/lang/StringBuilder;

    #@f6
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@f9
    const-string v11, "Exception :: "

    #@fb
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fe
    move-result-object v10

    #@ff
    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@102
    move-result-object v11

    #@103
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@106
    move-result-object v10

    #@107
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10a
    move-result-object v10

    #@10b
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@10e
    .line 902
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    #@111
    goto :goto_ac

    #@112
    .line 915
    .end local v7           #e:Ljava/lang/Exception;
    :catch_112
    move-exception v7

    #@113
    .line 916
    .restart local v7       #e:Ljava/lang/Exception;
    const-string v9, "LGIMSSettings"

    #@115
    new-instance v10, Ljava/lang/StringBuilder;

    #@117
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@11a
    const-string v11, "Exception :: "

    #@11c
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11f
    move-result-object v10

    #@120
    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@123
    move-result-object v11

    #@124
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@127
    move-result-object v10

    #@128
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12b
    move-result-object v10

    #@12c
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@12f
    .line 917
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    #@132
    goto :goto_e4
.end method

.method private setDefaultValues(Ljava/lang/String;[[Ljava/lang/String;)V
    .registers 9
    .parameter "table"
    .parameter "content"

    #@0
    .prologue
    .line 927
    new-instance v2, Landroid/content/ContentValues;

    #@2
    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    #@5
    .line 929
    .local v2, values:Landroid/content/ContentValues;
    const/4 v1, 0x0

    #@6
    .local v1, i:I
    :goto_6
    array-length v3, p2

    #@7
    if-ge v1, v3, :cond_19

    #@9
    .line 931
    aget-object v3, p2, v1

    #@b
    const/4 v4, 0x1

    #@c
    aget-object v3, v3, v4

    #@e
    aget-object v4, p2, v1

    #@10
    const/4 v5, 0x2

    #@11
    aget-object v4, v4, v5

    #@13
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@16
    .line 929
    add-int/lit8 v1, v1, 0x1

    #@18
    goto :goto_6

    #@19
    .line 935
    :cond_19
    :try_start_19
    iget-object v3, p0, Lcom/lge/ims/setting/IMSSettings;->mImsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@1b
    const-string v4, "id = \'1\'"

    #@1d
    const/4 v5, 0x0

    #@1e
    invoke-virtual {v3, p1, v2, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_21
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_19 .. :try_end_21} :catch_22
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_21} :catch_43

    #@21
    .line 943
    :goto_21
    return-void

    #@22
    .line 936
    :catch_22
    move-exception v0

    #@23
    .line 937
    .local v0, e:Landroid/database/sqlite/SQLiteException;
    const-string v3, "LGIMSSettings"

    #@25
    new-instance v4, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v5, "SQLiteException :: "

    #@2c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v4

    #@30
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    #@33
    move-result-object v5

    #@34
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v4

    #@38
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v4

    #@3c
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    .line 938
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    #@42
    goto :goto_21

    #@43
    .line 939
    .end local v0           #e:Landroid/database/sqlite/SQLiteException;
    :catch_43
    move-exception v0

    #@44
    .line 940
    .local v0, e:Ljava/lang/Exception;
    const-string v3, "LGIMSSettings"

    #@46
    new-instance v4, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v5, "Exception :: "

    #@4d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v4

    #@51
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@54
    move-result-object v5

    #@55
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v4

    #@59
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v4

    #@5d
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@60
    .line 941
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@63
    goto :goto_21
.end method

.method private setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V
    .registers 4
    .parameter "edit"

    #@0
    .prologue
    .line 946
    if-nez p1, :cond_3

    #@2
    .line 951
    :goto_2
    return-void

    #@3
    .line 950
    :cond_3
    invoke-virtual {p1}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    #@6
    move-result-object v0

    #@7
    const/4 v1, 0x2

    #@8
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    #@b
    goto :goto_2
.end method

.method private setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V
    .registers 3
    .parameter "preference"
    .parameter "listener"

    #@0
    .prologue
    .line 954
    if-eqz p1, :cond_5

    #@2
    .line 955
    invoke-virtual {p1, p2}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@5
    .line 957
    :cond_5
    return-void
.end method

.method private setSummary(Landroid/preference/Preference;Ljava/lang/Object;)V
    .registers 7
    .parameter "preference"
    .parameter "newValue"

    #@0
    .prologue
    .line 1043
    if-eqz p1, :cond_4

    #@2
    if-nez p2, :cond_5

    #@4
    .line 1060
    :cond_4
    :goto_4
    return-void

    #@5
    .line 1047
    :cond_5
    instance-of v3, p1, Landroid/preference/CheckBoxPreference;

    #@7
    if-nez v3, :cond_4

    #@9
    .line 1049
    instance-of v3, p1, Landroid/preference/EditTextPreference;

    #@b
    if-eqz v3, :cond_15

    #@d
    .line 1050
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {p1, v3}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    #@14
    goto :goto_4

    #@15
    .line 1051
    :cond_15
    instance-of v3, p1, Landroid/preference/ListPreference;

    #@17
    if-eqz v3, :cond_4

    #@19
    move-object v1, p1

    #@1a
    .line 1052
    check-cast v1, Landroid/preference/ListPreference;

    #@1c
    .line 1053
    .local v1, list:Landroid/preference/ListPreference;
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v1, v3}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    #@23
    move-result v2

    #@24
    .line 1055
    .local v2, valueIndex:I
    if-ltz v2, :cond_4

    #@26
    .line 1056
    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    #@29
    move-result-object v0

    #@2a
    .line 1057
    .local v0, entries:[Ljava/lang/CharSequence;
    aget-object v3, v0, v2

    #@2c
    invoke-virtual {v1, v3}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    #@2f
    goto :goto_4
.end method

.method private setValue(Landroid/database/Cursor;ILandroid/preference/CheckBoxPreference;)V
    .registers 7
    .parameter "cursor"
    .parameter "index"
    .parameter "checkbox"

    #@0
    .prologue
    .line 960
    const/4 v1, -0x1

    #@1
    if-ne p2, v1, :cond_b

    #@3
    .line 961
    const-string v1, "LGIMSSettings"

    #@5
    const-string v2, "index is (-1)"

    #@7
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 987
    :cond_a
    :goto_a
    return-void

    #@b
    .line 965
    :cond_b
    if-nez p3, :cond_15

    #@d
    .line 966
    const-string v1, "LGIMSSettings"

    #@f
    const-string v2, "CheckBoxPreference is null"

    #@11
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    goto :goto_a

    #@15
    .line 970
    :cond_15
    if-nez p1, :cond_1f

    #@17
    .line 971
    const-string v1, "LGIMSSettings"

    #@19
    const-string v2, "Cursor is null"

    #@1b
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    goto :goto_a

    #@1f
    .line 975
    :cond_1f
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    .line 977
    .local v0, value:Ljava/lang/String;
    if-eqz v0, :cond_a

    #@25
    .line 981
    const-string v1, "true"

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@2a
    move-result v1

    #@2b
    if-eqz v1, :cond_32

    #@2d
    .line 982
    const/4 v1, 0x1

    #@2e
    invoke-virtual {p3, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    #@31
    goto :goto_a

    #@32
    .line 985
    :cond_32
    const/4 v1, 0x0

    #@33
    invoke-virtual {p3, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    #@36
    goto :goto_a
.end method

.method private setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V
    .registers 6
    .parameter "cursor"
    .parameter "index"
    .parameter "edit"

    #@0
    .prologue
    .line 990
    const/4 v1, -0x1

    #@1
    if-ne p2, v1, :cond_4

    #@3
    .line 1010
    :cond_3
    :goto_3
    return-void

    #@4
    .line 994
    :cond_4
    if-eqz p3, :cond_3

    #@6
    .line 998
    if-eqz p1, :cond_3

    #@8
    .line 1002
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    .line 1004
    .local v0, value:Ljava/lang/String;
    if-eqz v0, :cond_3

    #@e
    .line 1008
    invoke-virtual {p3, v0}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    #@11
    .line 1009
    invoke-virtual {p3, v0}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    #@14
    goto :goto_3
.end method

.method private setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V
    .registers 9
    .parameter "cursor"
    .parameter "index"
    .parameter "list"

    #@0
    .prologue
    .line 1013
    const/4 v2, -0x1

    #@1
    if-ne p2, v2, :cond_4

    #@3
    .line 1040
    :cond_3
    :goto_3
    return-void

    #@4
    .line 1017
    :cond_4
    if-eqz p3, :cond_3

    #@6
    .line 1021
    if-eqz p1, :cond_3

    #@8
    .line 1025
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    .line 1027
    .local v0, value:Ljava/lang/String;
    if-eqz v0, :cond_3

    #@e
    .line 1031
    invoke-virtual {p3, v0}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    #@11
    move-result v1

    #@12
    .line 1033
    .local v1, valueIndex:I
    if-gez v1, :cond_3b

    #@14
    .line 1034
    const-string v2, "LGIMSSettings"

    #@16
    new-instance v3, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v4, "setValue :: list - "

    #@1d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {p3}, Landroid/preference/ListPreference;->getKey()Ljava/lang/String;

    #@24
    move-result-object v4

    #@25
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    const-string v4, "="

    #@2b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v3

    #@37
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    goto :goto_3

    #@3b
    .line 1038
    :cond_3b
    invoke-virtual {p3, v1}, Landroid/preference/ListPreference;->setValueIndex(I)V

    #@3e
    .line 1039
    invoke-virtual {p3}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    #@41
    move-result-object v2

    #@42
    invoke-virtual {p3, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    #@45
    goto :goto_3
.end method

.method private updateIMSI()V
    .registers 4

    #@0
    .prologue
    .line 1063
    iget-object v1, p0, Lcom/lge/ims/setting/IMSSettings;->mIMSI:Landroid/preference/Preference;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 1074
    :goto_4
    return-void

    #@5
    .line 1067
    :cond_5
    invoke-static {}, Lcom/lge/ims/PhoneInfo;->getSubscriberId()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    .line 1069
    .local v0, imsi:Ljava/lang/String;
    if-eqz v0, :cond_11

    #@b
    .line 1070
    iget-object v1, p0, Lcom/lge/ims/setting/IMSSettings;->mIMSI:Landroid/preference/Preference;

    #@d
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    #@10
    goto :goto_4

    #@11
    .line 1072
    :cond_11
    iget-object v1, p0, Lcom/lge/ims/setting/IMSSettings;->mIMSI:Landroid/preference/Preference;

    #@13
    const-string v2, ""

    #@15
    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    #@18
    goto :goto_4
.end method

.method private updateIPAddress()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 1077
    iget-object v3, p0, Lcom/lge/ims/setting/IMSSettings;->mIPAddress:Landroid/preference/Preference;

    #@3
    if-nez v3, :cond_6

    #@5
    .line 1108
    :cond_5
    :goto_5
    return-void

    #@6
    .line 1081
    :cond_6
    invoke-static {}, Lcom/lge/ims/DataConnectionManager;->getInstance()Lcom/lge/ims/DataConnectionManager;

    #@9
    move-result-object v0

    #@a
    .line 1083
    .local v0, dcm:Lcom/lge/ims/DataConnectionManager;
    if-eqz v0, :cond_5

    #@c
    .line 1084
    const-string v2, "0.0.0.0"

    #@e
    .line 1086
    .local v2, summary:Ljava/lang/String;
    invoke-virtual {v0, v4}, Lcom/lge/ims/DataConnectionManager;->isConnected(I)Z

    #@11
    move-result v3

    #@12
    if-eqz v3, :cond_1d

    #@14
    .line 1087
    const/4 v3, 0x0

    #@15
    invoke-virtual {v0, v4, v3}, Lcom/lge/ims/DataConnectionManager;->getLocalAddress(II)Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    .line 1089
    if-nez v2, :cond_1d

    #@1b
    .line 1090
    const-string v2, "0.0.0.0"

    #@1d
    .line 1094
    :cond_1d
    invoke-static {}, Lcom/lge/ims/PhoneStateTracker;->getInstance()Lcom/lge/ims/PhoneStateTracker;

    #@20
    move-result-object v1

    #@21
    .line 1096
    .local v1, pst:Lcom/lge/ims/PhoneStateTracker;
    if-eqz v1, :cond_2f

    #@23
    .line 1097
    invoke-virtual {v1}, Lcom/lge/ims/PhoneStateTracker;->is3G()Z

    #@26
    move-result v3

    #@27
    if-eqz v3, :cond_35

    #@29
    .line 1098
    const-string v3, " (3G)"

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@2e
    move-result-object v2

    #@2f
    .line 1106
    :cond_2f
    :goto_2f
    iget-object v3, p0, Lcom/lge/ims/setting/IMSSettings;->mIPAddress:Landroid/preference/Preference;

    #@31
    invoke-virtual {v3, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    #@34
    goto :goto_5

    #@35
    .line 1099
    :cond_35
    invoke-virtual {v1}, Lcom/lge/ims/PhoneStateTracker;->is4G()Z

    #@38
    move-result v3

    #@39
    if-eqz v3, :cond_42

    #@3b
    .line 1100
    const-string v3, " (LTE)"

    #@3d
    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@40
    move-result-object v2

    #@41
    goto :goto_2f

    #@42
    .line 1102
    :cond_42
    const-string v3, " (Unknown)"

    #@44
    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@47
    move-result-object v2

    #@48
    goto :goto_2f
.end method

.method private updateIPAddressForWiFi()V
    .registers 8

    #@0
    .prologue
    .line 1111
    iget-object v5, p0, Lcom/lge/ims/setting/IMSSettings;->mIPAddressForWiFi:Landroid/preference/Preference;

    #@2
    if-nez v5, :cond_5

    #@4
    .line 1141
    :goto_4
    return-void

    #@5
    .line 1115
    :cond_5
    const-string v2, ""

    #@7
    .line 1116
    .local v2, summary:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/lge/ims/setting/IMSSettings;->getBaseContext()Landroid/content/Context;

    #@a
    move-result-object v0

    #@b
    .line 1118
    .local v0, context:Landroid/content/Context;
    if-eqz v0, :cond_b3

    #@d
    .line 1119
    const-string v5, "wifi"

    #@f
    invoke-virtual {v0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@12
    move-result-object v4

    #@13
    check-cast v4, Landroid/net/wifi/WifiManager;

    #@15
    .line 1121
    .local v4, wm:Landroid/net/wifi/WifiManager;
    if-eqz v4, :cond_b3

    #@17
    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    #@1a
    move-result v5

    #@1b
    if-eqz v5, :cond_b3

    #@1d
    .line 1122
    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    #@20
    move-result-object v3

    #@21
    .line 1124
    .local v3, wifiInfo:Landroid/net/wifi/WifiInfo;
    if-eqz v3, :cond_b3

    #@23
    .line 1125
    invoke-virtual {v3}, Landroid/net/wifi/WifiInfo;->getIpAddress()I

    #@26
    move-result v1

    #@27
    .line 1127
    .local v1, ipAddress:I
    if-eqz v1, :cond_b3

    #@29
    .line 1128
    and-int/lit16 v5, v1, 0xff

    #@2b
    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@2e
    move-result-object v2

    #@2f
    .line 1129
    new-instance v5, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v5

    #@38
    const-string v6, "."

    #@3a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v5

    #@3e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v2

    #@42
    .line 1130
    new-instance v5, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v5

    #@4b
    shr-int/lit8 v6, v1, 0x8

    #@4d
    and-int/lit16 v6, v6, 0xff

    #@4f
    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@52
    move-result-object v6

    #@53
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v5

    #@57
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v2

    #@5b
    .line 1131
    new-instance v5, Ljava/lang/StringBuilder;

    #@5d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@60
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v5

    #@64
    const-string v6, "."

    #@66
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v5

    #@6a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d
    move-result-object v2

    #@6e
    .line 1132
    new-instance v5, Ljava/lang/StringBuilder;

    #@70
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@73
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v5

    #@77
    shr-int/lit8 v6, v1, 0x10

    #@79
    and-int/lit16 v6, v6, 0xff

    #@7b
    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@7e
    move-result-object v6

    #@7f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v5

    #@83
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@86
    move-result-object v2

    #@87
    .line 1133
    new-instance v5, Ljava/lang/StringBuilder;

    #@89
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@8c
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v5

    #@90
    const-string v6, "."

    #@92
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v5

    #@96
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@99
    move-result-object v2

    #@9a
    .line 1134
    new-instance v5, Ljava/lang/StringBuilder;

    #@9c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@9f
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v5

    #@a3
    shr-int/lit8 v6, v1, 0x18

    #@a5
    and-int/lit16 v6, v6, 0xff

    #@a7
    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@aa
    move-result-object v6

    #@ab
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v5

    #@af
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b2
    move-result-object v2

    #@b3
    .line 1140
    .end local v1           #ipAddress:I
    .end local v3           #wifiInfo:Landroid/net/wifi/WifiInfo;
    .end local v4           #wm:Landroid/net/wifi/WifiManager;
    :cond_b3
    iget-object v5, p0, Lcom/lge/ims/setting/IMSSettings;->mIPAddressForWiFi:Landroid/preference/Preference;

    #@b5
    invoke-virtual {v5, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    #@b8
    goto/16 :goto_4
.end method

.method private updateMSISDN()V
    .registers 4

    #@0
    .prologue
    .line 1144
    iget-object v1, p0, Lcom/lge/ims/setting/IMSSettings;->mMSISDN:Landroid/preference/Preference;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 1155
    :goto_4
    return-void

    #@5
    .line 1148
    :cond_5
    invoke-static {}, Lcom/lge/ims/PhoneInfo;->getPhoneNumber()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    .line 1150
    .local v0, msisdn:Ljava/lang/String;
    if-eqz v0, :cond_11

    #@b
    .line 1151
    iget-object v1, p0, Lcom/lge/ims/setting/IMSSettings;->mMSISDN:Landroid/preference/Preference;

    #@d
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    #@10
    goto :goto_4

    #@11
    .line 1153
    :cond_11
    iget-object v1, p0, Lcom/lge/ims/setting/IMSSettings;->mMSISDN:Landroid/preference/Preference;

    #@13
    const-string v2, ""

    #@15
    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    #@18
    goto :goto_4
.end method

.method private updateRegistrationStatus()V
    .registers 4

    #@0
    .prologue
    .line 1158
    iget-object v1, p0, Lcom/lge/ims/setting/IMSSettings;->mImsRegistrationStatus:Landroid/preference/Preference;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 1169
    :goto_4
    return-void

    #@5
    .line 1162
    :cond_5
    const-string v1, "net.ims.reg"

    #@7
    const-string v2, "0"

    #@9
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    .line 1164
    .local v0, reg:Ljava/lang/String;
    const-string v1, "1"

    #@f
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v1

    #@13
    if-eqz v1, :cond_1e

    #@15
    .line 1165
    iget-object v1, p0, Lcom/lge/ims/setting/IMSSettings;->mImsRegistrationStatus:Landroid/preference/Preference;

    #@17
    const v2, 0x7f060393

    #@1a
    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(I)V

    #@1d
    goto :goto_4

    #@1e
    .line 1167
    :cond_1e
    iget-object v1, p0, Lcom/lge/ims/setting/IMSSettings;->mImsRegistrationStatus:Landroid/preference/Preference;

    #@20
    const v2, 0x7f060394

    #@23
    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(I)V

    #@26
    goto :goto_4
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .registers 8
    .parameter "savedInstanceState"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 107
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    #@4
    .line 109
    const v2, 0x7f040005

    #@7
    invoke-virtual {p0, v2}, Lcom/lge/ims/setting/IMSSettings;->addPreferencesFromResource(I)V

    #@a
    .line 112
    iget-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mImsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@c
    if-nez v2, :cond_18

    #@e
    .line 114
    :try_start_e
    const-string v2, "/data/data/com.lge.ims/databases/lgims.db"

    #@10
    const/4 v3, 0x0

    #@11
    const/4 v4, 0x0

    #@12
    invoke-static {v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->openDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)Landroid/database/sqlite/SQLiteDatabase;

    #@15
    move-result-object v2

    #@16
    iput-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mImsDB:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_18
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_e .. :try_end_18} :catch_ca

    #@18
    .line 121
    :cond_18
    const v2, 0x7f060110

    #@1b
    invoke-virtual {p0, v2}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-direct {p0, v2}, Lcom/lge/ims/setting/IMSSettings;->getPreference(Ljava/lang/String;)Landroid/preference/Preference;

    #@22
    move-result-object v2

    #@23
    iput-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mSubscriberPreference:Landroid/preference/Preference;

    #@25
    .line 122
    const v2, 0x7f060130

    #@28
    invoke-virtual {p0, v2}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@2b
    move-result-object v2

    #@2c
    invoke-direct {p0, v2}, Lcom/lge/ims/setting/IMSSettings;->getPreference(Ljava/lang/String;)Landroid/preference/Preference;

    #@2f
    move-result-object v2

    #@30
    iput-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mSIPPreference:Landroid/preference/Preference;

    #@32
    .line 123
    const v2, 0x7f06000d

    #@35
    invoke-virtual {p0, v2}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@38
    move-result-object v2

    #@39
    invoke-direct {p0, v2}, Lcom/lge/ims/setting/IMSSettings;->getPreference(Ljava/lang/String;)Landroid/preference/Preference;

    #@3c
    move-result-object v2

    #@3d
    iput-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mDefaultSettingsPreference:Landroid/preference/Preference;

    #@3f
    .line 125
    invoke-direct {p0}, Lcom/lge/ims/setting/IMSSettings;->registerSubscriberChangeListener()V

    #@42
    .line 126
    invoke-direct {p0}, Lcom/lge/ims/setting/IMSSettings;->registerACCommonChangeListener()V

    #@45
    .line 127
    invoke-direct {p0}, Lcom/lge/ims/setting/IMSSettings;->registerACConnectionServerChangeListener()V

    #@48
    .line 128
    invoke-direct {p0}, Lcom/lge/ims/setting/IMSSettings;->registerSIPChangeListener()V

    #@4b
    .line 129
    invoke-direct {p0}, Lcom/lge/ims/setting/IMSSettings;->registerComSIPChangeListener()V

    #@4e
    .line 130
    invoke-direct {p0}, Lcom/lge/ims/setting/IMSSettings;->registerOperatorSpecificChangeListener()V

    #@51
    .line 131
    invoke-direct {p0}, Lcom/lge/ims/setting/IMSSettings;->registerTestChangeListener()V

    #@54
    .line 134
    iget-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mAuthPassword:Landroid/preference/EditTextPreference;

    #@56
    if-eqz v2, :cond_a0

    #@58
    .line 135
    invoke-static {}, Lcom/lge/ims/Configuration;->isACOn()Z

    #@5b
    move-result v2

    #@5c
    if-eqz v2, :cond_a0

    #@5e
    .line 136
    invoke-virtual {p0}, Lcom/lge/ims/setting/IMSSettings;->getBaseContext()Landroid/content/Context;

    #@61
    move-result-object v2

    #@62
    invoke-static {v2}, Lcom/lge/ims/service/ac/ACContentHelper;->getPasswordFromSubscriber(Landroid/content/Context;)Ljava/lang/String;

    #@65
    move-result-object v1

    #@66
    .line 138
    .local v1, password:Ljava/lang/String;
    if-eqz v1, :cond_a0

    #@68
    .line 139
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@6b
    move-result v2

    #@6c
    if-lez v2, :cond_a0

    #@6e
    .line 140
    const-string v2, "user"

    #@70
    sget-object v3, Lcom/lge/ims/Configuration;->BUILD_TYPE:Ljava/lang/String;

    #@72
    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@75
    move-result v2

    #@76
    if-nez v2, :cond_90

    #@78
    .line 141
    const-string v2, "LGIMSSettings"

    #@7a
    new-instance v3, Ljava/lang/StringBuilder;

    #@7c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7f
    const-string v4, "Password will be set from AC; password="

    #@81
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v3

    #@85
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v3

    #@89
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8c
    move-result-object v3

    #@8d
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@90
    .line 144
    :cond_90
    iget-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mAuthPassword:Landroid/preference/EditTextPreference;

    #@92
    invoke-virtual {v2, v1}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    #@95
    .line 146
    iget-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mRealPasswordForMD5:Ljava/lang/String;

    #@97
    if-eqz v2, :cond_a0

    #@99
    .line 147
    iget-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mAuthPassword:Landroid/preference/EditTextPreference;

    #@9b
    iget-object v3, p0, Lcom/lge/ims/setting/IMSSettings;->mRealPasswordForMD5:Ljava/lang/String;

    #@9d
    invoke-virtual {v2, v3}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    #@a0
    .line 156
    .end local v1           #password:Ljava/lang/String;
    :cond_a0
    iget-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mIMS:Landroid/preference/CheckBoxPreference;

    #@a2
    if-eqz v2, :cond_ad

    #@a4
    .line 157
    iget-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mIMS:Landroid/preference/CheckBoxPreference;

    #@a6
    invoke-virtual {v2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    #@a9
    move-result v2

    #@aa
    invoke-direct {p0, v2}, Lcom/lge/ims/setting/IMSSettings;->checkAndSetEnabledOnIMSChanged(Z)V

    #@ad
    .line 160
    :cond_ad
    iget-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mUSIM:Landroid/preference/CheckBoxPreference;

    #@af
    if-eqz v2, :cond_ba

    #@b1
    .line 161
    iget-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mUSIM:Landroid/preference/CheckBoxPreference;

    #@b3
    invoke-virtual {v2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    #@b6
    move-result v2

    #@b7
    invoke-direct {p0, v2}, Lcom/lge/ims/setting/IMSSettings;->checkAndSetEnabledOnUSIMChanged(Z)V

    #@ba
    .line 164
    :cond_ba
    iget-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mServiceCode:Landroid/preference/EditTextPreference;

    #@bc
    if-eqz v2, :cond_c7

    #@be
    .line 165
    iget-object v2, p0, Lcom/lge/ims/setting/IMSSettings;->mServiceCode:Landroid/preference/EditTextPreference;

    #@c0
    invoke-virtual {v2}, Landroid/preference/EditTextPreference;->getText()Ljava/lang/String;

    #@c3
    move-result-object v2

    #@c4
    invoke-direct {p0, v2}, Lcom/lge/ims/setting/IMSSettings;->checkAndSetEnabledOnServiceCodeChanged(Ljava/lang/String;)V

    #@c7
    .line 168
    :cond_c7
    iput v5, p0, Lcom/lge/ims/setting/IMSSettings;->mSettingsUpdatedFlags:I

    #@c9
    .line 169
    :goto_c9
    return-void

    #@ca
    .line 115
    :catch_ca
    move-exception v0

    #@cb
    .line 116
    .local v0, e:Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    #@ce
    goto :goto_c9
.end method

.method public onDestroy()V
    .registers 2

    #@0
    .prologue
    .line 238
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    #@3
    .line 240
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mImsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@5
    if-eqz v0, :cond_f

    #@7
    .line 241
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mImsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@9
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    #@c
    .line 242
    const/4 v0, 0x0

    #@d
    iput-object v0, p0, Lcom/lge/ims/setting/IMSSettings;->mImsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@f
    .line 244
    :cond_f
    return-void
.end method

.method public onPause()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 173
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPause()V

    #@4
    .line 174
    const-string v3, "LGIMSSettings"

    #@6
    const-string v4, "onPause()"

    #@8
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 176
    iget-boolean v3, p0, Lcom/lge/ims/setting/IMSSettings;->mDebugScreenUpdateRequired:Z

    #@d
    if-eqz v3, :cond_11

    #@f
    .line 177
    iput-boolean v5, p0, Lcom/lge/ims/setting/IMSSettings;->mDebugScreenUpdateRequired:Z

    #@11
    .line 180
    :cond_11
    const/4 v0, 0x1

    #@12
    .line 182
    .local v0, checkForACOnOff:Z
    iget v3, p0, Lcom/lge/ims/setting/IMSSettings;->mSettingsUpdatedFlags:I

    #@14
    if-eqz v3, :cond_47

    #@16
    .line 183
    const-string v3, "LGIMSSettings"

    #@18
    const-string v4, "Send an intent to update the configuration"

    #@1a
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 185
    new-instance v2, Landroid/content/Intent;

    #@1f
    const-string v3, "com.lge.ims.action.CONFIG_UPDATE"

    #@21
    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@24
    .line 186
    .local v2, intent:Landroid/content/Intent;
    const/16 v3, 0x20

    #@26
    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@29
    .line 187
    const-string v3, "flags"

    #@2b
    iget v4, p0, Lcom/lge/ims/setting/IMSSettings;->mSettingsUpdatedFlags:I

    #@2d
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@30
    .line 188
    invoke-virtual {p0}, Lcom/lge/ims/setting/IMSSettings;->getBaseContext()Landroid/content/Context;

    #@33
    move-result-object v3

    #@34
    invoke-virtual {v3, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@37
    .line 190
    iget v3, p0, Lcom/lge/ims/setting/IMSSettings;->mSettingsUpdatedFlags:I

    #@39
    and-int/lit8 v3, v3, 0x1

    #@3b
    if-eqz v3, :cond_45

    #@3d
    .line 191
    invoke-virtual {p0}, Lcom/lge/ims/setting/IMSSettings;->getBaseContext()Landroid/content/Context;

    #@40
    move-result-object v3

    #@41
    invoke-static {v3}, Lcom/lge/ims/Configuration;->init(Landroid/content/Context;)V

    #@44
    .line 192
    const/4 v0, 0x0

    #@45
    .line 195
    :cond_45
    iput v5, p0, Lcom/lge/ims/setting/IMSSettings;->mSettingsUpdatedFlags:I

    #@47
    .line 199
    .end local v2           #intent:Landroid/content/Intent;
    :cond_47
    if-eqz v0, :cond_5c

    #@49
    .line 200
    iget-object v3, p0, Lcom/lge/ims/setting/IMSSettings;->mAC:Landroid/preference/CheckBoxPreference;

    #@4b
    invoke-virtual {v3}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    #@4e
    move-result v3

    #@4f
    invoke-static {}, Lcom/lge/ims/Configuration;->isACOn()Z

    #@52
    move-result v4

    #@53
    if-eq v3, v4, :cond_5c

    #@55
    .line 201
    invoke-virtual {p0}, Lcom/lge/ims/setting/IMSSettings;->getBaseContext()Landroid/content/Context;

    #@58
    move-result-object v3

    #@59
    invoke-static {v3}, Lcom/lge/ims/Configuration;->init(Landroid/content/Context;)V

    #@5c
    .line 206
    :cond_5c
    iget-boolean v3, p0, Lcom/lge/ims/setting/IMSSettings;->mACConfigurationChanged:Z

    #@5e
    if-eqz v3, :cond_69

    #@60
    .line 207
    invoke-virtual {p0}, Lcom/lge/ims/setting/IMSSettings;->getBaseContext()Landroid/content/Context;

    #@63
    move-result-object v3

    #@64
    invoke-static {v3}, Lcom/lge/ims/service/ac/ACConfiguration;->init(Landroid/content/Context;)V

    #@67
    .line 208
    iput-boolean v5, p0, Lcom/lge/ims/setting/IMSSettings;->mACConfigurationChanged:Z

    #@69
    .line 211
    :cond_69
    invoke-static {}, Lcom/lge/ims/DataConnectionManager;->getInstance()Lcom/lge/ims/DataConnectionManager;

    #@6c
    move-result-object v1

    #@6d
    .line 213
    .local v1, dcm:Lcom/lge/ims/DataConnectionManager;
    if-eqz v1, :cond_74

    #@6f
    .line 214
    iget-object v3, p0, Lcom/lge/ims/setting/IMSSettings;->mDebugScreenHandler:Lcom/lge/ims/setting/IMSSettings$DebugScreenHandler;

    #@71
    invoke-virtual {v1, v3}, Lcom/lge/ims/DataConnectionManager;->unregisterForDataStateChanged(Landroid/os/Handler;)V

    #@74
    .line 216
    :cond_74
    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .registers 10
    .parameter "preferenceScreen"
    .parameter "preference"

    #@0
    .prologue
    .line 249
    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    #@3
    move-result-object v2

    #@4
    .line 251
    .local v2, key:Ljava/lang/String;
    if-nez v2, :cond_b

    #@6
    .line 252
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    #@9
    move-result v3

    #@a
    .line 315
    :goto_a
    return v3

    #@b
    .line 255
    :cond_b
    const v3, 0x7f06000d

    #@e
    invoke-virtual {p0, v3}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@11
    move-result-object v3

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v3

    #@16
    if-eqz v3, :cond_4d

    #@18
    .line 256
    new-instance v3, Landroid/app/AlertDialog$Builder;

    #@1a
    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@1d
    const v4, 0x7f060004

    #@20
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@23
    move-result-object v4

    #@24
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@27
    move-result-object v3

    #@28
    const-string v4, "Do you really want to set the default settings?"

    #@2a
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@2d
    move-result-object v3

    #@2e
    const v4, 0x104000a

    #@31
    new-instance v5, Lcom/lge/ims/setting/IMSSettings$2;

    #@33
    invoke-direct {v5, p0}, Lcom/lge/ims/setting/IMSSettings$2;-><init>(Lcom/lge/ims/setting/IMSSettings;)V

    #@36
    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@39
    move-result-object v3

    #@3a
    const/high16 v4, 0x104

    #@3c
    new-instance v5, Lcom/lge/ims/setting/IMSSettings$1;

    #@3e
    invoke-direct {v5, p0}, Lcom/lge/ims/setting/IMSSettings$1;-><init>(Lcom/lge/ims/setting/IMSSettings;)V

    #@41
    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@44
    move-result-object v3

    #@45
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    #@48
    .line 315
    :cond_48
    :goto_48
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    #@4b
    move-result v3

    #@4c
    goto :goto_a

    #@4d
    .line 272
    :cond_4d
    const v3, 0x7f06010f

    #@50
    invoke-virtual {p0, v3}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@53
    move-result-object v3

    #@54
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@57
    move-result v3

    #@58
    if-eqz v3, :cond_6a

    #@5a
    .line 273
    new-instance v1, Landroid/content/Intent;

    #@5c
    const-string v3, "com.lge.ims.action.IMS_PROVISIONING"

    #@5e
    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@61
    .line 276
    .local v1, i:Landroid/content/Intent;
    :try_start_61
    invoke-virtual {p0, v1}, Lcom/lge/ims/setting/IMSSettings;->startActivity(Landroid/content/Intent;)V
    :try_end_64
    .catch Landroid/content/ActivityNotFoundException; {:try_start_61 .. :try_end_64} :catch_65

    #@64
    goto :goto_48

    #@65
    .line 277
    :catch_65
    move-exception v0

    #@66
    .line 278
    .local v0, e:Landroid/content/ActivityNotFoundException;
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    #@69
    goto :goto_48

    #@6a
    .line 280
    .end local v0           #e:Landroid/content/ActivityNotFoundException;
    .end local v1           #i:Landroid/content/Intent;
    :cond_6a
    const v3, 0x7f06000f

    #@6d
    invoke-virtual {p0, v3}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@70
    move-result-object v3

    #@71
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@74
    move-result v3

    #@75
    if-eqz v3, :cond_97

    #@77
    .line 281
    invoke-direct {p0}, Lcom/lge/ims/setting/IMSSettings;->updateIMSI()V

    #@7a
    .line 282
    invoke-direct {p0}, Lcom/lge/ims/setting/IMSSettings;->updateMSISDN()V

    #@7d
    .line 283
    invoke-direct {p0}, Lcom/lge/ims/setting/IMSSettings;->updateIPAddress()V

    #@80
    .line 284
    invoke-direct {p0}, Lcom/lge/ims/setting/IMSSettings;->updateIPAddressForWiFi()V

    #@83
    .line 285
    invoke-direct {p0}, Lcom/lge/ims/setting/IMSSettings;->updateRegistrationStatus()V

    #@86
    .line 287
    iget-boolean v3, p0, Lcom/lge/ims/setting/IMSSettings;->mDebugScreenUpdateRequired:Z

    #@88
    if-nez v3, :cond_48

    #@8a
    .line 288
    const/4 v3, 0x1

    #@8b
    iput-boolean v3, p0, Lcom/lge/ims/setting/IMSSettings;->mDebugScreenUpdateRequired:Z

    #@8d
    .line 289
    iget-object v3, p0, Lcom/lge/ims/setting/IMSSettings;->mDebugScreenHandler:Lcom/lge/ims/setting/IMSSettings$DebugScreenHandler;

    #@8f
    const/16 v4, 0x65

    #@91
    const-wide/16 v5, 0x7d0

    #@93
    invoke-virtual {v3, v4, v5, v6}, Lcom/lge/ims/setting/IMSSettings$DebugScreenHandler;->sendEmptyMessageDelayed(IJ)Z

    #@96
    goto :goto_48

    #@97
    .line 291
    :cond_97
    const v3, 0x7f060010

    #@9a
    invoke-virtual {p0, v3}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@9d
    move-result-object v3

    #@9e
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a1
    move-result v3

    #@a2
    if-eqz v3, :cond_a8

    #@a4
    .line 292
    invoke-direct {p0}, Lcom/lge/ims/setting/IMSSettings;->updateRegistrationStatus()V

    #@a7
    goto :goto_48

    #@a8
    .line 293
    :cond_a8
    const v3, 0x7f060011

    #@ab
    invoke-virtual {p0, v3}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@ae
    move-result-object v3

    #@af
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b2
    move-result v3

    #@b3
    if-eqz v3, :cond_b9

    #@b5
    .line 294
    invoke-direct {p0}, Lcom/lge/ims/setting/IMSSettings;->updateIPAddress()V

    #@b8
    goto :goto_48

    #@b9
    .line 295
    :cond_b9
    const v3, 0x7f060012

    #@bc
    invoke-virtual {p0, v3}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@bf
    move-result-object v3

    #@c0
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c3
    move-result v3

    #@c4
    if-eqz v3, :cond_cb

    #@c6
    .line 296
    invoke-direct {p0}, Lcom/lge/ims/setting/IMSSettings;->updateIPAddressForWiFi()V

    #@c9
    goto/16 :goto_48

    #@cb
    .line 297
    :cond_cb
    const v3, 0x7f060110

    #@ce
    invoke-virtual {p0, v3}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@d1
    move-result-object v3

    #@d2
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d5
    move-result v3

    #@d6
    if-eqz v3, :cond_f4

    #@d8
    .line 298
    iget-object v3, p0, Lcom/lge/ims/setting/IMSSettings;->mAC:Landroid/preference/CheckBoxPreference;

    #@da
    if-eqz v3, :cond_e5

    #@dc
    .line 299
    iget-object v3, p0, Lcom/lge/ims/setting/IMSSettings;->mAC:Landroid/preference/CheckBoxPreference;

    #@de
    invoke-virtual {v3}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    #@e1
    move-result v3

    #@e2
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSSettings;->checkAndSetEnabledOnACChanged(Z)V

    #@e5
    .line 302
    :cond_e5
    iget-object v3, p0, Lcom/lge/ims/setting/IMSSettings;->mServiceCode:Landroid/preference/EditTextPreference;

    #@e7
    if-eqz v3, :cond_48

    #@e9
    .line 303
    iget-object v3, p0, Lcom/lge/ims/setting/IMSSettings;->mServiceCode:Landroid/preference/EditTextPreference;

    #@eb
    invoke-virtual {v3}, Landroid/preference/EditTextPreference;->getText()Ljava/lang/String;

    #@ee
    move-result-object v3

    #@ef
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSSettings;->checkAndSetEnabledOnServiceCodeChanged(Ljava/lang/String;)V

    #@f2
    goto/16 :goto_48

    #@f4
    .line 305
    :cond_f4
    const v3, 0x7f060015

    #@f7
    invoke-virtual {p0, v3}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@fa
    move-result-object v3

    #@fb
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@fe
    move-result v3

    #@ff
    if-eqz v3, :cond_48

    #@101
    .line 306
    new-instance v1, Landroid/content/Intent;

    #@103
    const-string v3, "com.lge.ims.action.DEBUG_SCREEN"

    #@105
    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@108
    .line 309
    .restart local v1       #i:Landroid/content/Intent;
    :try_start_108
    invoke-virtual {p0, v1}, Lcom/lge/ims/setting/IMSSettings;->startActivity(Landroid/content/Intent;)V
    :try_end_10b
    .catch Landroid/content/ActivityNotFoundException; {:try_start_108 .. :try_end_10b} :catch_10d

    #@10b
    goto/16 :goto_48

    #@10d
    .line 310
    :catch_10d
    move-exception v0

    #@10e
    .line 311
    .restart local v0       #e:Landroid/content/ActivityNotFoundException;
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    #@111
    goto/16 :goto_48
.end method

.method public onResume()V
    .registers 5

    #@0
    .prologue
    .line 220
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    #@3
    .line 221
    const-string v1, "LGIMSSettings"

    #@5
    const-string v2, "onResume()"

    #@7
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 223
    invoke-direct {p0}, Lcom/lge/ims/setting/IMSSettings;->updateIMSI()V

    #@d
    .line 224
    invoke-direct {p0}, Lcom/lge/ims/setting/IMSSettings;->updateMSISDN()V

    #@10
    .line 225
    invoke-direct {p0}, Lcom/lge/ims/setting/IMSSettings;->updateIPAddress()V

    #@13
    .line 226
    invoke-direct {p0}, Lcom/lge/ims/setting/IMSSettings;->updateIPAddressForWiFi()V

    #@16
    .line 227
    invoke-direct {p0}, Lcom/lge/ims/setting/IMSSettings;->updateRegistrationStatus()V

    #@19
    .line 229
    invoke-static {}, Lcom/lge/ims/DataConnectionManager;->getInstance()Lcom/lge/ims/DataConnectionManager;

    #@1c
    move-result-object v0

    #@1d
    .line 231
    .local v0, dcm:Lcom/lge/ims/DataConnectionManager;
    if-eqz v0, :cond_27

    #@1f
    .line 232
    iget-object v1, p0, Lcom/lge/ims/setting/IMSSettings;->mDebugScreenHandler:Lcom/lge/ims/setting/IMSSettings$DebugScreenHandler;

    #@21
    const/16 v2, 0x66

    #@23
    const/4 v3, 0x0

    #@24
    invoke-virtual {v0, v1, v2, v3}, Lcom/lge/ims/DataConnectionManager;->registerForDataStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@27
    .line 234
    :cond_27
    return-void
.end method
