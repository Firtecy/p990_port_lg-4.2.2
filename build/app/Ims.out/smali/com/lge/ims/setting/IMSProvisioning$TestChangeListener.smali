.class Lcom/lge/ims/setting/IMSProvisioning$TestChangeListener;
.super Ljava/lang/Object;
.source "IMSProvisioning.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/setting/IMSProvisioning;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TestChangeListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/setting/IMSProvisioning;


# direct methods
.method public constructor <init>(Lcom/lge/ims/setting/IMSProvisioning;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1518
    iput-object p1, p0, Lcom/lge/ims/setting/IMSProvisioning$TestChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 1519
    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .registers 16
    .parameter "preference"
    .parameter "newValue"

    #@0
    .prologue
    const v12, 0x7f06011e

    #@3
    const v11, 0x7f06011d

    #@6
    const v10, 0x7f06011b

    #@9
    const v9, 0x7f06011a

    #@c
    const/4 v5, 0x1

    #@d
    .line 1522
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    #@10
    move-result-object v2

    #@11
    .line 1524
    .local v2, key:Ljava/lang/String;
    const-string v6, "LGIMSProvisioning"

    #@13
    new-instance v7, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v8, "Test :: key: "

    #@1a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v7

    #@1e
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v7

    #@22
    const-string v8, " -> value:"

    #@24
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v7

    #@28
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@2b
    move-result-object v8

    #@2c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v7

    #@30
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v7

    #@34
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 1526
    iget-object v6, p0, Lcom/lge/ims/setting/IMSProvisioning$TestChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@39
    invoke-static {v6}, Lcom/lge/ims/setting/IMSProvisioning;->access$000(Lcom/lge/ims/setting/IMSProvisioning;)Landroid/database/sqlite/SQLiteDatabase;

    #@3c
    move-result-object v6

    #@3d
    if-nez v6, :cond_48

    #@3f
    .line 1527
    const-string v5, "LGIMSProvisioning"

    #@41
    const-string v6, "DB table (Test) is null"

    #@43
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    .line 1528
    const/4 v5, 0x0

    #@47
    .line 1603
    :goto_47
    return v5

    #@48
    .line 1531
    :cond_48
    const-string v6, "my_number"

    #@4a
    invoke-virtual {v2, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@4d
    move-result v6

    #@4e
    if-eqz v6, :cond_5f

    #@50
    .line 1532
    iget-object v6, p0, Lcom/lge/ims/setting/IMSProvisioning$TestChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@52
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@55
    move-result-object v7

    #@56
    invoke-static {v6, v7}, Lcom/lge/ims/setting/IMSProvisioning;->access$300(Lcom/lge/ims/setting/IMSProvisioning;Ljava/lang/String;)V

    #@59
    .line 1533
    iget-object v6, p0, Lcom/lge/ims/setting/IMSProvisioning$TestChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@5b
    invoke-static {v6, p1, p2}, Lcom/lge/ims/setting/IMSProvisioning;->access$200(Lcom/lge/ims/setting/IMSProvisioning;Landroid/preference/Preference;Ljava/lang/Object;)V

    #@5e
    goto :goto_47

    #@5f
    .line 1535
    :cond_5f
    const-string v6, "ft_media_protocol"

    #@61
    invoke-virtual {v2, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@64
    move-result v6

    #@65
    if-eqz v6, :cond_b1

    #@67
    .line 1536
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@6a
    move-result-object v6

    #@6b
    const-string v7, "HTTP"

    #@6d
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@70
    move-result v6

    #@71
    if-eqz v6, :cond_87

    #@73
    .line 1537
    const-string v6, "LGIMSProvisioning"

    #@75
    const-string v7, "KEY_FILE_MEDIA_PROTOCOL --- HTTP"

    #@77
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7a
    .line 1538
    const-string v6, "net.ims.media.protocol"

    #@7c
    const-string v7, "HTTP"

    #@7e
    invoke-static {v6, v7}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@81
    .line 1547
    :goto_81
    iget-object v6, p0, Lcom/lge/ims/setting/IMSProvisioning$TestChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@83
    invoke-static {v6, p1, p2}, Lcom/lge/ims/setting/IMSProvisioning;->access$200(Lcom/lge/ims/setting/IMSProvisioning;Landroid/preference/Preference;Ljava/lang/Object;)V

    #@86
    goto :goto_47

    #@87
    .line 1539
    :cond_87
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@8a
    move-result-object v6

    #@8b
    const-string v7, "MSRP"

    #@8d
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@90
    move-result v6

    #@91
    if-eqz v6, :cond_a2

    #@93
    .line 1540
    const-string v6, "LGIMSProvisioning"

    #@95
    const-string v7, "KEY_FILE_MEDIA_PROTOCOL --- MSRP"

    #@97
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@9a
    .line 1541
    const-string v6, "net.ims.media.protocol"

    #@9c
    const-string v7, "MSRP"

    #@9e
    invoke-static {v6, v7}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@a1
    goto :goto_81

    #@a2
    .line 1543
    :cond_a2
    const-string v6, "LGIMSProvisioning"

    #@a4
    const-string v7, "KEY_FILE_MEDIA_PROTOCOL --- NONE"

    #@a6
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@a9
    .line 1544
    const-string v6, "net.ims.media.protocol"

    #@ab
    const-string v7, "NONE"

    #@ad
    invoke-static {v6, v7}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@b0
    goto :goto_81

    #@b1
    .line 1549
    :cond_b1
    const-string v6, "app_server_setting"

    #@b3
    invoke-virtual {v2, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@b6
    move-result v6

    #@b7
    if-eqz v6, :cond_22d

    #@b9
    .line 1550
    const/4 v0, 0x0

    #@ba
    .line 1551
    .local v0, affectedRows:I
    new-instance v4, Landroid/content/ContentValues;

    #@bc
    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    #@bf
    .line 1552
    .local v4, valuesForSubscriber:Landroid/content/ContentValues;
    new-instance v3, Landroid/content/ContentValues;

    #@c1
    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    #@c4
    .line 1554
    .local v3, valuesForSIP:Landroid/content/ContentValues;
    const-string v6, "LGIMSProvisioning"

    #@c6
    new-instance v7, Ljava/lang/StringBuilder;

    #@c8
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@cb
    const-string v8, "KEY_APP_SERVER_SETTING :: Value: "

    #@cd
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d0
    move-result-object v7

    #@d1
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@d4
    move-result-object v8

    #@d5
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v7

    #@d9
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@dc
    move-result-object v7

    #@dd
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e0
    .line 1556
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@e3
    move-result-object v6

    #@e4
    const-string v7, "VT_Commercial"

    #@e6
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e9
    move-result v6

    #@ea
    if-eqz v6, :cond_1b1

    #@ec
    .line 1557
    iget-object v6, p0, Lcom/lge/ims/setting/IMSProvisioning$TestChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@ee
    invoke-virtual {v6, v9}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@f1
    move-result-object v6

    #@f2
    const-string v7, "abc.ims.kt.com"

    #@f4
    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@f7
    .line 1558
    iget-object v6, p0, Lcom/lge/ims/setting/IMSProvisioning$TestChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@f9
    invoke-virtual {v6, v11}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@fc
    move-result-object v6

    #@fd
    const-string v7, "5075"

    #@ff
    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@102
    .line 1559
    iget-object v6, p0, Lcom/lge/ims/setting/IMSProvisioning$TestChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@104
    invoke-virtual {v6, v10}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@107
    move-result-object v6

    #@108
    const-string v7, "mss.ims.kt.com"

    #@10a
    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@10d
    .line 1560
    iget-object v6, p0, Lcom/lge/ims/setting/IMSProvisioning$TestChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@10f
    invoke-virtual {v6, v12}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@112
    move-result-object v6

    #@113
    const-string v7, "5060"

    #@115
    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@118
    .line 1561
    iget-object v6, p0, Lcom/lge/ims/setting/IMSProvisioning$TestChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@11a
    const v7, 0x7f06014d

    #@11d
    invoke-virtual {v6, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@120
    move-result-object v6

    #@121
    const-string v7, "600000"

    #@123
    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@126
    .line 1570
    :cond_126
    :goto_126
    iget-object v6, p0, Lcom/lge/ims/setting/IMSProvisioning$TestChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@128
    invoke-static {v6}, Lcom/lge/ims/setting/IMSProvisioning;->access$000(Lcom/lge/ims/setting/IMSProvisioning;)Landroid/database/sqlite/SQLiteDatabase;

    #@12b
    move-result-object v6

    #@12c
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    #@12f
    .line 1573
    :try_start_12f
    iget-object v6, p0, Lcom/lge/ims/setting/IMSProvisioning$TestChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@131
    invoke-static {v6}, Lcom/lge/ims/setting/IMSProvisioning;->access$000(Lcom/lge/ims/setting/IMSProvisioning;)Landroid/database/sqlite/SQLiteDatabase;

    #@134
    move-result-object v6

    #@135
    const-string v7, "lgims_subscriber"

    #@137
    const-string v8, "id = \'1\'"

    #@139
    const/4 v9, 0x0

    #@13a
    invoke-virtual {v6, v7, v4, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@13d
    move-result v0

    #@13e
    .line 1574
    iget-object v6, p0, Lcom/lge/ims/setting/IMSProvisioning$TestChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@140
    invoke-static {v6}, Lcom/lge/ims/setting/IMSProvisioning;->access$000(Lcom/lge/ims/setting/IMSProvisioning;)Landroid/database/sqlite/SQLiteDatabase;

    #@143
    move-result-object v6

    #@144
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_147
    .catchall {:try_start_12f .. :try_end_147} :catchall_208
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_12f .. :try_end_147} :catch_1f9

    #@147
    .line 1578
    iget-object v6, p0, Lcom/lge/ims/setting/IMSProvisioning$TestChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@149
    invoke-static {v6}, Lcom/lge/ims/setting/IMSProvisioning;->access$000(Lcom/lge/ims/setting/IMSProvisioning;)Landroid/database/sqlite/SQLiteDatabase;

    #@14c
    move-result-object v6

    #@14d
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@150
    .line 1581
    :goto_150
    const-string v6, "LGIMSProvisioning"

    #@152
    new-instance v7, Ljava/lang/StringBuilder;

    #@154
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@157
    const-string v8, "Subscriber :: updated row count: "

    #@159
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15c
    move-result-object v7

    #@15d
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@160
    move-result-object v7

    #@161
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@164
    move-result-object v7

    #@165
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@168
    .line 1583
    iget-object v6, p0, Lcom/lge/ims/setting/IMSProvisioning$TestChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@16a
    invoke-static {v6}, Lcom/lge/ims/setting/IMSProvisioning;->access$000(Lcom/lge/ims/setting/IMSProvisioning;)Landroid/database/sqlite/SQLiteDatabase;

    #@16d
    move-result-object v6

    #@16e
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    #@171
    .line 1586
    :try_start_171
    iget-object v6, p0, Lcom/lge/ims/setting/IMSProvisioning$TestChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@173
    invoke-static {v6}, Lcom/lge/ims/setting/IMSProvisioning;->access$000(Lcom/lge/ims/setting/IMSProvisioning;)Landroid/database/sqlite/SQLiteDatabase;

    #@176
    move-result-object v6

    #@177
    const-string v7, "lgims_sip"

    #@179
    const-string v8, "id = \'1\'"

    #@17b
    const/4 v9, 0x0

    #@17c
    invoke-virtual {v6, v7, v3, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@17f
    move-result v0

    #@180
    .line 1587
    iget-object v6, p0, Lcom/lge/ims/setting/IMSProvisioning$TestChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@182
    invoke-static {v6}, Lcom/lge/ims/setting/IMSProvisioning;->access$000(Lcom/lge/ims/setting/IMSProvisioning;)Landroid/database/sqlite/SQLiteDatabase;

    #@185
    move-result-object v6

    #@186
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_189
    .catchall {:try_start_171 .. :try_end_189} :catchall_222
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_171 .. :try_end_189} :catch_213

    #@189
    .line 1591
    iget-object v6, p0, Lcom/lge/ims/setting/IMSProvisioning$TestChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@18b
    invoke-static {v6}, Lcom/lge/ims/setting/IMSProvisioning;->access$000(Lcom/lge/ims/setting/IMSProvisioning;)Landroid/database/sqlite/SQLiteDatabase;

    #@18e
    move-result-object v6

    #@18f
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@192
    .line 1594
    :goto_192
    const-string v6, "LGIMSProvisioning"

    #@194
    new-instance v7, Ljava/lang/StringBuilder;

    #@196
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@199
    const-string v8, "SIP :: updated row count: "

    #@19b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19e
    move-result-object v7

    #@19f
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a2
    move-result-object v7

    #@1a3
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a6
    move-result-object v7

    #@1a7
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1aa
    .line 1596
    iget-object v6, p0, Lcom/lge/ims/setting/IMSProvisioning$TestChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@1ac
    invoke-static {v6, p1, p2}, Lcom/lge/ims/setting/IMSProvisioning;->access$200(Lcom/lge/ims/setting/IMSProvisioning;Landroid/preference/Preference;Ljava/lang/Object;)V

    #@1af
    goto/16 :goto_47

    #@1b1
    .line 1562
    :cond_1b1
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1b4
    move-result-object v6

    #@1b5
    const-string v7, "RCS_Testbed"

    #@1b7
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1ba
    move-result v6

    #@1bb
    if-eqz v6, :cond_126

    #@1bd
    .line 1563
    iget-object v6, p0, Lcom/lge/ims/setting/IMSProvisioning$TestChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@1bf
    invoke-virtual {v6, v9}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@1c2
    move-result-object v6

    #@1c3
    const-string v7, "125.152.96.242"

    #@1c5
    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1c8
    .line 1564
    iget-object v6, p0, Lcom/lge/ims/setting/IMSProvisioning$TestChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@1ca
    invoke-virtual {v6, v11}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@1cd
    move-result-object v6

    #@1ce
    const-string v7, "5080"

    #@1d0
    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1d3
    .line 1565
    iget-object v6, p0, Lcom/lge/ims/setting/IMSProvisioning$TestChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@1d5
    invoke-virtual {v6, v10}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@1d8
    move-result-object v6

    #@1d9
    const-string v7, "125.152.1.153"

    #@1db
    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1de
    .line 1566
    iget-object v6, p0, Lcom/lge/ims/setting/IMSProvisioning$TestChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@1e0
    invoke-virtual {v6, v12}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@1e3
    move-result-object v6

    #@1e4
    const-string v7, "5080"

    #@1e6
    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1e9
    .line 1567
    iget-object v6, p0, Lcom/lge/ims/setting/IMSProvisioning$TestChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@1eb
    const v7, 0x7f06014d

    #@1ee
    invoke-virtual {v6, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@1f1
    move-result-object v6

    #@1f2
    const-string v7, "360"

    #@1f4
    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1f7
    goto/16 :goto_126

    #@1f9
    .line 1575
    :catch_1f9
    move-exception v1

    #@1fa
    .line 1576
    .local v1, e:Landroid/database/sqlite/SQLiteException;
    :try_start_1fa
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_1fd
    .catchall {:try_start_1fa .. :try_end_1fd} :catchall_208

    #@1fd
    .line 1578
    iget-object v6, p0, Lcom/lge/ims/setting/IMSProvisioning$TestChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@1ff
    invoke-static {v6}, Lcom/lge/ims/setting/IMSProvisioning;->access$000(Lcom/lge/ims/setting/IMSProvisioning;)Landroid/database/sqlite/SQLiteDatabase;

    #@202
    move-result-object v6

    #@203
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@206
    goto/16 :goto_150

    #@208
    .end local v1           #e:Landroid/database/sqlite/SQLiteException;
    :catchall_208
    move-exception v5

    #@209
    iget-object v6, p0, Lcom/lge/ims/setting/IMSProvisioning$TestChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@20b
    invoke-static {v6}, Lcom/lge/ims/setting/IMSProvisioning;->access$000(Lcom/lge/ims/setting/IMSProvisioning;)Landroid/database/sqlite/SQLiteDatabase;

    #@20e
    move-result-object v6

    #@20f
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@212
    throw v5

    #@213
    .line 1588
    :catch_213
    move-exception v1

    #@214
    .line 1589
    .restart local v1       #e:Landroid/database/sqlite/SQLiteException;
    :try_start_214
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_217
    .catchall {:try_start_214 .. :try_end_217} :catchall_222

    #@217
    .line 1591
    iget-object v6, p0, Lcom/lge/ims/setting/IMSProvisioning$TestChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@219
    invoke-static {v6}, Lcom/lge/ims/setting/IMSProvisioning;->access$000(Lcom/lge/ims/setting/IMSProvisioning;)Landroid/database/sqlite/SQLiteDatabase;

    #@21c
    move-result-object v6

    #@21d
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@220
    goto/16 :goto_192

    #@222
    .end local v1           #e:Landroid/database/sqlite/SQLiteException;
    :catchall_222
    move-exception v5

    #@223
    iget-object v6, p0, Lcom/lge/ims/setting/IMSProvisioning$TestChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@225
    invoke-static {v6}, Lcom/lge/ims/setting/IMSProvisioning;->access$000(Lcom/lge/ims/setting/IMSProvisioning;)Landroid/database/sqlite/SQLiteDatabase;

    #@228
    move-result-object v6

    #@229
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@22c
    throw v5

    #@22d
    .line 1601
    .end local v0           #affectedRows:I
    .end local v3           #valuesForSIP:Landroid/content/ContentValues;
    .end local v4           #valuesForSubscriber:Landroid/content/ContentValues;
    :cond_22d
    iget-object v6, p0, Lcom/lge/ims/setting/IMSProvisioning$TestChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@22f
    invoke-static {v6, p1, p2}, Lcom/lge/ims/setting/IMSProvisioning;->access$200(Lcom/lge/ims/setting/IMSProvisioning;Landroid/preference/Preference;Ljava/lang/Object;)V

    #@232
    goto/16 :goto_47
.end method
