.class public Lcom/lge/ims/setting/IMSProvisioning;
.super Landroid/preference/PreferenceActivity;
.source "IMSProvisioning.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/setting/IMSProvisioning$TestChangeListener;,
        Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;,
        Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;
    }
.end annotation


# static fields
.field private static final KEY_APP_SERVER_SETTING:Ljava/lang/String; = "app_server_setting"

.field private static final KEY_FILE_MEDIA_PROTOCOL:Ljava/lang/String; = "ft_media_protocol"

.field private static final KEY_IM_FT_HTTP_CONFIG:Ljava/lang/String; = "ft_http_config"

.field private static final KEY_IM_PREFIX:Ljava/lang/String; = "im_"

.field private static final KEY_IPXDM_PREFIX:Ljava/lang/String; = "ipxdm_"

.field private static final KEY_IP_PREFIX:Ljava/lang/String; = "ip_"

.field private static final KEY_MY_NUMBER:Ljava/lang/String; = "my_number"

.field private static final KEY_TARGET_NUMBER:Ljava/lang/String; = "subscriber_0_impu_2"

.field private static final KEY_VOIP_PREFIX:Ljava/lang/String; = "voip_"

.field private static final KEY_VS_PREFIX:Ljava/lang/String; = "vs_"

.field private static final KEY_VT_PREFIX:Ljava/lang/String; = "vt_"

.field private static final KEY_XDM_RESOURCE_HTTP_CONFIG:Ljava/lang/String; = "resource_http_config"

.field private static final LGIMS_AOS:Ljava/lang/String; = "lgims_aos"

.field private static final LGIMS_COM:Ljava/lang/String; = "lgims_com_kt"

.field private static final LGIMS_COM_SIP:Ljava/lang/String; = "lgims_com_kt_sip"

.field private static final LGIMS_IM:Ljava/lang/String; = "lgims_com_kt_service_rcse_im"

.field private static final LGIMS_IP:Ljava/lang/String; = "lgims_com_kt_service_rcse_ip"

.field private static final LGIMS_IPXDM:Ljava/lang/String; = "lgims_com_kt_service_rcse_ipxdm"

.field private static final LGIMS_KT_TEST:Ljava/lang/String; = "lgims_com_kt_test"

.field private static final LGIMS_VOIP:Ljava/lang/String; = "lgims_com_kt_service_uc"

.field private static final LGIMS_VS:Ljava/lang/String; = "lgims_com_kt_service_rcse_vs"

.field private static final LGIMS_VT:Ljava/lang/String; = "lgims_com_kt_service_vt"

.field private static final LGIMS_VT_SIP:Ljava/lang/String; = "lgims_com_kt_sip_vt"

.field private static final TAG:Ljava/lang/String; = "LGIMSProvisioning"


# instance fields
.field private imHTTPTable:Ljava/lang/String;

.field private imsDB:Landroid/database/sqlite/SQLiteDatabase;

.field private mAdministrativeConfigChanged:Z

.field private mIPAddress:Landroid/preference/Preference;

.field private mImsRegistrationStatus:Landroid/preference/Preference;

.field private onAoSConnectionListener:Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;

.field private onAoSRegListener:Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;

.field private onComSIPListener:Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;

.field private onIMApplicationListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

.field private onIMHTTPListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

.field private onIPApplicationListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

.field private onSIPListener:Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;

.field private onSubscriberListener:Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;

.field private onVSSessionListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

.field private onVTSIPListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

.field private onVTSessionListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

.field private onVoIPSessionListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

.field private onXDMApplicationListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

.field private onXDMHTTPListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

.field private onXDMResourceListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

.field private voipVisible:Z

.field private xdmHTTPTable:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 30
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    #@4
    .line 75
    iput-object v0, p0, Lcom/lge/ims/setting/IMSProvisioning;->onSubscriberListener:Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;

    #@6
    .line 76
    iput-object v0, p0, Lcom/lge/ims/setting/IMSProvisioning;->onAoSConnectionListener:Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;

    #@8
    .line 77
    iput-object v0, p0, Lcom/lge/ims/setting/IMSProvisioning;->onAoSRegListener:Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;

    #@a
    .line 78
    iput-object v0, p0, Lcom/lge/ims/setting/IMSProvisioning;->onSIPListener:Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;

    #@c
    .line 79
    iput-object v0, p0, Lcom/lge/ims/setting/IMSProvisioning;->onComSIPListener:Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;

    #@e
    .line 81
    iput-object v0, p0, Lcom/lge/ims/setting/IMSProvisioning;->onVTSessionListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@10
    .line 82
    iput-object v0, p0, Lcom/lge/ims/setting/IMSProvisioning;->onVTSIPListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@12
    .line 84
    iput-object v0, p0, Lcom/lge/ims/setting/IMSProvisioning;->onVoIPSessionListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@14
    .line 87
    iput-object v0, p0, Lcom/lge/ims/setting/IMSProvisioning;->onIMApplicationListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@16
    .line 88
    iput-object v0, p0, Lcom/lge/ims/setting/IMSProvisioning;->onIMHTTPListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@18
    .line 89
    iput-object v0, p0, Lcom/lge/ims/setting/IMSProvisioning;->imHTTPTable:Ljava/lang/String;

    #@1a
    .line 91
    iput-object v0, p0, Lcom/lge/ims/setting/IMSProvisioning;->onIPApplicationListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@1c
    .line 92
    iput-object v0, p0, Lcom/lge/ims/setting/IMSProvisioning;->onXDMApplicationListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@1e
    .line 93
    iput-object v0, p0, Lcom/lge/ims/setting/IMSProvisioning;->onXDMResourceListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@20
    .line 94
    iput-object v0, p0, Lcom/lge/ims/setting/IMSProvisioning;->onXDMHTTPListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@22
    .line 95
    iput-object v0, p0, Lcom/lge/ims/setting/IMSProvisioning;->xdmHTTPTable:Ljava/lang/String;

    #@24
    .line 97
    iput-object v0, p0, Lcom/lge/ims/setting/IMSProvisioning;->onVSSessionListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@26
    .line 99
    iput-object v0, p0, Lcom/lge/ims/setting/IMSProvisioning;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@28
    .line 100
    iput-object v0, p0, Lcom/lge/ims/setting/IMSProvisioning;->mIPAddress:Landroid/preference/Preference;

    #@2a
    .line 101
    iput-object v0, p0, Lcom/lge/ims/setting/IMSProvisioning;->mImsRegistrationStatus:Landroid/preference/Preference;

    #@2c
    .line 102
    const/4 v0, 0x1

    #@2d
    iput-boolean v0, p0, Lcom/lge/ims/setting/IMSProvisioning;->voipVisible:Z

    #@2f
    .line 103
    const/4 v0, 0x0

    #@30
    iput-boolean v0, p0, Lcom/lge/ims/setting/IMSProvisioning;->mAdministrativeConfigChanged:Z

    #@32
    .line 1516
    return-void
.end method

.method static synthetic access$000(Lcom/lge/ims/setting/IMSProvisioning;)Landroid/database/sqlite/SQLiteDatabase;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 30
    iget-object v0, p0, Lcom/lge/ims/setting/IMSProvisioning;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@2
    return-object v0
.end method

.method static synthetic access$102(Lcom/lge/ims/setting/IMSProvisioning;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 30
    iput-boolean p1, p0, Lcom/lge/ims/setting/IMSProvisioning;->mAdministrativeConfigChanged:Z

    #@2
    return p1
.end method

.method static synthetic access$200(Lcom/lge/ims/setting/IMSProvisioning;Landroid/preference/Preference;Ljava/lang/Object;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lcom/lge/ims/setting/IMSProvisioning;->setSummary(Landroid/preference/Preference;Ljava/lang/Object;)V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/lge/ims/setting/IMSProvisioning;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/lge/ims/setting/IMSProvisioning;->updateAllDBItems(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method private displayColumns([Ljava/lang/String;)V
    .registers 6
    .parameter "columns"

    #@0
    .prologue
    .line 1608
    invoke-static {}, Lcom/lge/ims/Configuration;->isDebugOn()Z

    #@3
    move-result v1

    #@4
    if-nez v1, :cond_7

    #@6
    .line 1624
    :cond_6
    return-void

    #@7
    .line 1612
    :cond_7
    if-eqz p1, :cond_6

    #@9
    .line 1616
    const/4 v0, 0x0

    #@a
    .local v0, i:I
    :goto_a
    array-length v1, p1

    #@b
    if-ge v0, v1, :cond_6

    #@d
    .line 1617
    aget-object v1, p1, v0

    #@f
    if-nez v1, :cond_32

    #@11
    .line 1618
    const-string v1, "LGIMSProvisioning"

    #@13
    new-instance v2, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v3, "column at index ("

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    const-string v3, ") is null"

    #@24
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v2

    #@2c
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 1616
    :goto_2f
    add-int/lit8 v0, v0, 0x1

    #@31
    goto :goto_a

    #@32
    .line 1622
    :cond_32
    const-string v1, "LGIMSProvisioning"

    #@34
    new-instance v2, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v3, "Column at index ("

    #@3b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@42
    move-result-object v2

    #@43
    const-string v3, ")"

    #@45
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v2

    #@49
    aget-object v3, p1, v0

    #@4b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v2

    #@4f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v2

    #@53
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    goto :goto_2f
.end method

.method private getCheckBoxPreference(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 1663
    invoke-virtual {p0}, Lcom/lge/ims/setting/IMSProvisioning;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    #@3
    move-result-object v0

    #@4
    .line 1665
    .local v0, root:Landroid/preference/PreferenceScreen;
    if-eqz v0, :cond_d

    #@6
    .line 1666
    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    #@9
    move-result-object v1

    #@a
    check-cast v1, Landroid/preference/CheckBoxPreference;

    #@c
    .line 1669
    :goto_c
    return-object v1

    #@d
    :cond_d
    const/4 v1, 0x0

    #@e
    goto :goto_c
.end method

.method private getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I
    .registers 5
    .parameter "columns"
    .parameter "column"

    #@0
    .prologue
    .line 1653
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    array-length v1, p1

    #@2
    if-ge v0, v1, :cond_10

    #@4
    .line 1654
    aget-object v1, p1, v0

    #@6
    invoke-virtual {p2, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@9
    move-result v1

    #@a
    if-nez v1, :cond_d

    #@c
    .line 1659
    .end local v0           #i:I
    :goto_c
    return v0

    #@d
    .line 1653
    .restart local v0       #i:I
    :cond_d
    add-int/lit8 v0, v0, 0x1

    #@f
    goto :goto_1

    #@10
    .line 1659
    :cond_10
    const/4 v0, -0x1

    #@11
    goto :goto_c
.end method

.method private getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 1683
    invoke-virtual {p0}, Lcom/lge/ims/setting/IMSProvisioning;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    #@3
    move-result-object v0

    #@4
    .line 1685
    .local v0, root:Landroid/preference/PreferenceScreen;
    if-eqz v0, :cond_d

    #@6
    .line 1686
    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    #@9
    move-result-object v1

    #@a
    check-cast v1, Landroid/preference/EditTextPreference;

    #@c
    .line 1689
    :goto_c
    return-object v1

    #@d
    :cond_d
    const/4 v1, 0x0

    #@e
    goto :goto_c
.end method

.method private getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 1673
    invoke-virtual {p0}, Lcom/lge/ims/setting/IMSProvisioning;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    #@3
    move-result-object v0

    #@4
    .line 1675
    .local v0, root:Landroid/preference/PreferenceScreen;
    if-eqz v0, :cond_d

    #@6
    .line 1676
    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    #@9
    move-result-object v1

    #@a
    check-cast v1, Landroid/preference/ListPreference;

    #@c
    .line 1679
    :goto_c
    return-object v1

    #@d
    :cond_d
    const/4 v1, 0x0

    #@e
    goto :goto_c
.end method

.method private getPreference(Ljava/lang/String;)Landroid/preference/Preference;
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 1693
    invoke-virtual {p0}, Lcom/lge/ims/setting/IMSProvisioning;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    #@3
    move-result-object v0

    #@4
    .line 1695
    .local v0, root:Landroid/preference/PreferenceScreen;
    if-eqz v0, :cond_b

    #@6
    .line 1696
    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    #@9
    move-result-object v1

    #@a
    .line 1699
    :goto_a
    return-object v1

    #@b
    :cond_b
    const/4 v1, 0x0

    #@c
    goto :goto_a
.end method

.method private getSettingsTable(Ljava/lang/String;)Landroid/database/Cursor;
    .registers 7
    .parameter "table"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1627
    iget-object v2, p0, Lcom/lge/ims/setting/IMSProvisioning;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 1649
    :cond_5
    :goto_5
    return-object v0

    #@6
    .line 1631
    :cond_6
    if-eqz p1, :cond_5

    #@8
    .line 1635
    const/4 v0, 0x0

    #@9
    .line 1638
    .local v0, cursor:Landroid/database/Cursor;
    :try_start_9
    iget-object v2, p0, Lcom/lge/ims/setting/IMSProvisioning;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@b
    new-instance v3, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v4, "select * from "

    #@12
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v3

    #@1e
    const/4 v4, 0x0

    #@1f
    invoke-virtual {v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_22
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_9 .. :try_end_22} :catch_24

    #@22
    move-result-object v0

    #@23
    goto :goto_5

    #@24
    .line 1639
    :catch_24
    move-exception v1

    #@25
    .line 1640
    .local v1, e:Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    #@28
    .line 1642
    if-eqz v0, :cond_2d

    #@2a
    .line 1643
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    #@2d
    .line 1646
    :cond_2d
    const/4 v0, 0x0

    #@2e
    goto :goto_5
.end method

.method private registerAoSConnectionChangeListener(Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;)V
    .registers 13
    .parameter "listener"

    #@0
    .prologue
    const v10, 0x7f06030d

    #@3
    const v9, 0x7f06030b

    #@6
    const v8, 0x7f06030a

    #@9
    const v7, 0x7f060308

    #@c
    const v6, 0x7f060307

    #@f
    .line 425
    invoke-virtual {p1}, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;->getTableName()Ljava/lang/String;

    #@12
    move-result-object v5

    #@13
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getSettingsTable(Ljava/lang/String;)Landroid/database/Cursor;

    #@16
    move-result-object v1

    #@17
    .line 427
    .local v1, cursor:Landroid/database/Cursor;
    if-nez v1, :cond_3c

    #@19
    .line 428
    const-string v5, "LGIMSProvisioning"

    #@1b
    new-instance v6, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v7, "Cursor :: ("

    #@22
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v6

    #@26
    invoke-virtual {p1}, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;->getTableName()Ljava/lang/String;

    #@29
    move-result-object v7

    #@2a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v6

    #@2e
    const-string v7, ") is null"

    #@30
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v6

    #@34
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v6

    #@38
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 542
    :goto_3b
    return-void

    #@3c
    .line 432
    :cond_3c
    invoke-interface {v1}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    #@3f
    move-result-object v0

    #@40
    .line 434
    .local v0, columns:[Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/lge/ims/setting/IMSProvisioning;->displayColumns([Ljava/lang/String;)V

    #@43
    .line 437
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    #@46
    .line 441
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@49
    move-result-object v5

    #@4a
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@4d
    move-result-object v2

    #@4e
    .line 442
    .local v2, edit:Landroid/preference/EditTextPreference;
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@51
    .line 443
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@54
    move-result-object v5

    #@55
    invoke-direct {p0, v0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@58
    move-result v3

    #@59
    .line 444
    .local v3, index:I
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@5c
    .line 447
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@5f
    move-result-object v5

    #@60
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@63
    move-result-object v4

    #@64
    .line 448
    .local v4, list:Landroid/preference/ListPreference;
    invoke-direct {p0, v4, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@67
    .line 449
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@6a
    move-result-object v5

    #@6b
    invoke-direct {p0, v0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@6e
    move-result v3

    #@6f
    .line 450
    invoke-direct {p0, v1, v3, v4}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@72
    .line 453
    invoke-virtual {p0, v10}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@75
    move-result-object v5

    #@76
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@79
    move-result-object v2

    #@7a
    .line 454
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@7d
    .line 455
    invoke-virtual {p0, v10}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@80
    move-result-object v5

    #@81
    invoke-direct {p0, v0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@84
    move-result v3

    #@85
    .line 456
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@88
    .line 459
    const v5, 0x7f060310

    #@8b
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@8e
    move-result-object v5

    #@8f
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@92
    move-result-object v2

    #@93
    .line 460
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@96
    .line 461
    const v5, 0x7f060310

    #@99
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@9c
    move-result-object v5

    #@9d
    invoke-direct {p0, v0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@a0
    move-result v3

    #@a1
    .line 462
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@a4
    .line 463
    invoke-direct {p0, v2}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@a7
    .line 466
    const v5, 0x7f060313

    #@aa
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@ad
    move-result-object v5

    #@ae
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@b1
    move-result-object v2

    #@b2
    .line 467
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@b5
    .line 468
    const v5, 0x7f060313

    #@b8
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@bb
    move-result-object v5

    #@bc
    invoke-direct {p0, v0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@bf
    move-result v3

    #@c0
    .line 469
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@c3
    .line 470
    invoke-direct {p0, v2}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@c6
    .line 474
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@c9
    move-result-object v5

    #@ca
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@cd
    move-result-object v2

    #@ce
    .line 475
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@d1
    .line 476
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@d4
    move-result-object v5

    #@d5
    invoke-direct {p0, v0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@d8
    move-result v3

    #@d9
    .line 477
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@dc
    .line 480
    invoke-virtual {p0, v9}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@df
    move-result-object v5

    #@e0
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@e3
    move-result-object v4

    #@e4
    .line 481
    invoke-direct {p0, v4, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@e7
    .line 482
    invoke-virtual {p0, v9}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@ea
    move-result-object v5

    #@eb
    invoke-direct {p0, v0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@ee
    move-result v3

    #@ef
    .line 483
    invoke-direct {p0, v1, v3, v4}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@f2
    .line 486
    const v5, 0x7f06030e

    #@f5
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@f8
    move-result-object v5

    #@f9
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@fc
    move-result-object v2

    #@fd
    .line 487
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@100
    .line 488
    const v5, 0x7f06030e

    #@103
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@106
    move-result-object v5

    #@107
    invoke-direct {p0, v0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@10a
    move-result v3

    #@10b
    .line 489
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@10e
    .line 492
    const v5, 0x7f060311

    #@111
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@114
    move-result-object v5

    #@115
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@118
    move-result-object v2

    #@119
    .line 493
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@11c
    .line 494
    const v5, 0x7f060311

    #@11f
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@122
    move-result-object v5

    #@123
    invoke-direct {p0, v0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@126
    move-result v3

    #@127
    .line 495
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@12a
    .line 496
    invoke-direct {p0, v2}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@12d
    .line 499
    const v5, 0x7f060314

    #@130
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@133
    move-result-object v5

    #@134
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@137
    move-result-object v2

    #@138
    .line 500
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@13b
    .line 501
    const v5, 0x7f060314

    #@13e
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@141
    move-result-object v5

    #@142
    invoke-direct {p0, v0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@145
    move-result v3

    #@146
    .line 502
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@149
    .line 503
    invoke-direct {p0, v2}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@14c
    .line 541
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    #@14f
    goto/16 :goto_3b
.end method

.method private registerAoSRegChangeListener(Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;)V
    .registers 13
    .parameter "listener"

    #@0
    .prologue
    const v10, 0x7f06031e

    #@3
    const v9, 0x7f06031d

    #@6
    const v8, 0x7f06031c

    #@9
    const v7, 0x7f06031b

    #@c
    const v6, 0x7f06031a

    #@f
    .line 549
    invoke-virtual {p1}, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;->getTableName()Ljava/lang/String;

    #@12
    move-result-object v5

    #@13
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getSettingsTable(Ljava/lang/String;)Landroid/database/Cursor;

    #@16
    move-result-object v2

    #@17
    .line 551
    .local v2, cursor:Landroid/database/Cursor;
    if-nez v2, :cond_3c

    #@19
    .line 552
    const-string v5, "LGIMSProvisioning"

    #@1b
    new-instance v6, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v7, "Cursor :: ("

    #@22
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v6

    #@26
    invoke-virtual {p1}, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;->getTableName()Ljava/lang/String;

    #@29
    move-result-object v7

    #@2a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v6

    #@2e
    const-string v7, ") is null"

    #@30
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v6

    #@34
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v6

    #@38
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 645
    :goto_3b
    return-void

    #@3c
    .line 556
    :cond_3c
    invoke-interface {v2}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    #@3f
    move-result-object v1

    #@40
    .line 558
    .local v1, columns:[Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/lge/ims/setting/IMSProvisioning;->displayColumns([Ljava/lang/String;)V

    #@43
    .line 561
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    #@46
    .line 564
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@49
    move-result-object v5

    #@4a
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@4d
    move-result-object v3

    #@4e
    .line 565
    .local v3, edit:Landroid/preference/EditTextPreference;
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@51
    .line 566
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@54
    move-result-object v5

    #@55
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@58
    move-result v4

    #@59
    .line 567
    .local v4, index:I
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@5c
    .line 569
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@5f
    move-result-object v5

    #@60
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@63
    move-result-object v3

    #@64
    .line 570
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@67
    .line 571
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@6a
    move-result-object v5

    #@6b
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@6e
    move-result v4

    #@6f
    .line 572
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@72
    .line 574
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@75
    move-result-object v5

    #@76
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@79
    move-result-object v3

    #@7a
    .line 575
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@7d
    .line 576
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@80
    move-result-object v5

    #@81
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@84
    move-result v4

    #@85
    .line 577
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@88
    .line 579
    invoke-virtual {p0, v9}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@8b
    move-result-object v5

    #@8c
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@8f
    move-result-object v3

    #@90
    .line 580
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@93
    .line 581
    invoke-virtual {p0, v9}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@96
    move-result-object v5

    #@97
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@9a
    move-result v4

    #@9b
    .line 582
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@9e
    .line 584
    invoke-virtual {p0, v10}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@a1
    move-result-object v5

    #@a2
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getCheckBoxPreference(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    #@a5
    move-result-object v0

    #@a6
    .line 585
    .local v0, checkbox:Landroid/preference/CheckBoxPreference;
    invoke-direct {p0, v0, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@a9
    .line 586
    invoke-virtual {p0, v10}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@ac
    move-result-object v5

    #@ad
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@b0
    move-result v4

    #@b1
    .line 587
    invoke-direct {p0, v2, v4, v0}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/CheckBoxPreference;)V

    #@b4
    .line 590
    const v5, 0x7f06031f

    #@b7
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@ba
    move-result-object v5

    #@bb
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@be
    move-result-object v3

    #@bf
    .line 591
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@c2
    .line 592
    const v5, 0x7f06031f

    #@c5
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@c8
    move-result-object v5

    #@c9
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@cc
    move-result v4

    #@cd
    .line 593
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@d0
    .line 595
    const v5, 0x7f060320

    #@d3
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@d6
    move-result-object v5

    #@d7
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@da
    move-result-object v3

    #@db
    .line 596
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@de
    .line 597
    const v5, 0x7f060320

    #@e1
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@e4
    move-result-object v5

    #@e5
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@e8
    move-result v4

    #@e9
    .line 598
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@ec
    .line 600
    const v5, 0x7f060321

    #@ef
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@f2
    move-result-object v5

    #@f3
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@f6
    move-result-object v3

    #@f7
    .line 601
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@fa
    .line 602
    const v5, 0x7f060321

    #@fd
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@100
    move-result-object v5

    #@101
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@104
    move-result v4

    #@105
    .line 603
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@108
    .line 605
    const v5, 0x7f060322

    #@10b
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@10e
    move-result-object v5

    #@10f
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@112
    move-result-object v3

    #@113
    .line 606
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@116
    .line 607
    const v5, 0x7f060322

    #@119
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@11c
    move-result-object v5

    #@11d
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@120
    move-result v4

    #@121
    .line 608
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@124
    .line 610
    const v5, 0x7f060323

    #@127
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@12a
    move-result-object v5

    #@12b
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getCheckBoxPreference(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    #@12e
    move-result-object v0

    #@12f
    .line 611
    invoke-direct {p0, v0, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@132
    .line 612
    const v5, 0x7f060323

    #@135
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@138
    move-result-object v5

    #@139
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@13c
    move-result v4

    #@13d
    .line 613
    invoke-direct {p0, v2, v4, v0}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/CheckBoxPreference;)V

    #@140
    .line 644
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    #@143
    goto/16 :goto_3b
.end method

.method private registerComSIPChangeListener(Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;)V
    .registers 14
    .parameter "listener"

    #@0
    .prologue
    const v11, 0x7f060145

    #@3
    const v10, 0x7f060144

    #@6
    const v9, 0x7f060143

    #@9
    const v8, 0x7f060142

    #@c
    const v7, 0x7f060141

    #@f
    .line 752
    invoke-virtual {p1}, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;->getTableName()Ljava/lang/String;

    #@12
    move-result-object v6

    #@13
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getSettingsTable(Ljava/lang/String;)Landroid/database/Cursor;

    #@16
    move-result-object v2

    #@17
    .line 754
    .local v2, cursor:Landroid/database/Cursor;
    if-nez v2, :cond_3c

    #@19
    .line 755
    const-string v6, "LGIMSProvisioning"

    #@1b
    new-instance v7, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v8, "Cursor :: ("

    #@22
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v7

    #@26
    invoke-virtual {p1}, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;->getTableName()Ljava/lang/String;

    #@29
    move-result-object v8

    #@2a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v7

    #@2e
    const-string v8, ") is null"

    #@30
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v7

    #@34
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v7

    #@38
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 895
    :goto_3b
    return-void

    #@3c
    .line 759
    :cond_3c
    invoke-interface {v2}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    #@3f
    move-result-object v1

    #@40
    .line 761
    .local v1, columns:[Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/lge/ims/setting/IMSProvisioning;->displayColumns([Ljava/lang/String;)V

    #@43
    .line 764
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    #@46
    .line 766
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@49
    move-result-object v6

    #@4a
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@4d
    move-result-object v3

    #@4e
    .line 767
    .local v3, edit:Landroid/preference/EditTextPreference;
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@51
    .line 768
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@54
    move-result-object v6

    #@55
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@58
    move-result v4

    #@59
    .line 769
    .local v4, index:I
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@5c
    .line 770
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@5f
    .line 772
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@62
    move-result-object v6

    #@63
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@66
    move-result-object v3

    #@67
    .line 773
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@6a
    .line 774
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@6d
    move-result-object v6

    #@6e
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@71
    move-result v4

    #@72
    .line 775
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@75
    .line 776
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@78
    .line 778
    invoke-virtual {p0, v9}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@7b
    move-result-object v6

    #@7c
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@7f
    move-result-object v3

    #@80
    .line 780
    if-eqz v3, :cond_93

    #@82
    .line 781
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@85
    .line 782
    invoke-virtual {p0, v9}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@88
    move-result-object v6

    #@89
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@8c
    move-result v4

    #@8d
    .line 783
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@90
    .line 784
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@93
    .line 787
    :cond_93
    invoke-virtual {p0, v10}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@96
    move-result-object v6

    #@97
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@9a
    move-result-object v3

    #@9b
    .line 789
    if-eqz v3, :cond_ae

    #@9d
    .line 790
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@a0
    .line 791
    invoke-virtual {p0, v10}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@a3
    move-result-object v6

    #@a4
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@a7
    move-result v4

    #@a8
    .line 792
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@ab
    .line 793
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@ae
    .line 796
    :cond_ae
    invoke-virtual {p0, v11}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@b1
    move-result-object v6

    #@b2
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@b5
    move-result-object v3

    #@b6
    .line 798
    if-eqz v3, :cond_c9

    #@b8
    .line 799
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@bb
    .line 800
    invoke-virtual {p0, v11}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@be
    move-result-object v6

    #@bf
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@c2
    move-result v4

    #@c3
    .line 801
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@c6
    .line 802
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@c9
    .line 805
    :cond_c9
    const v6, 0x7f060146

    #@cc
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@cf
    move-result-object v6

    #@d0
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@d3
    move-result-object v3

    #@d4
    .line 807
    if-eqz v3, :cond_ea

    #@d6
    .line 808
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@d9
    .line 809
    const v6, 0x7f060146

    #@dc
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@df
    move-result-object v6

    #@e0
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@e3
    move-result v4

    #@e4
    .line 810
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@e7
    .line 811
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@ea
    .line 814
    :cond_ea
    const v6, 0x7f060147

    #@ed
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@f0
    move-result-object v6

    #@f1
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@f4
    move-result-object v3

    #@f5
    .line 816
    if-eqz v3, :cond_10b

    #@f7
    .line 817
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@fa
    .line 818
    const v6, 0x7f060147

    #@fd
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@100
    move-result-object v6

    #@101
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@104
    move-result v4

    #@105
    .line 819
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@108
    .line 820
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@10b
    .line 823
    :cond_10b
    const v6, 0x7f060148

    #@10e
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@111
    move-result-object v6

    #@112
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@115
    move-result-object v3

    #@116
    .line 825
    if-eqz v3, :cond_12c

    #@118
    .line 826
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@11b
    .line 827
    const v6, 0x7f060148

    #@11e
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@121
    move-result-object v6

    #@122
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@125
    move-result v4

    #@126
    .line 828
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@129
    .line 829
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@12c
    .line 832
    :cond_12c
    const v6, 0x7f060149

    #@12f
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@132
    move-result-object v6

    #@133
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@136
    move-result-object v3

    #@137
    .line 834
    if-eqz v3, :cond_14d

    #@139
    .line 835
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@13c
    .line 836
    const v6, 0x7f060149

    #@13f
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@142
    move-result-object v6

    #@143
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@146
    move-result v4

    #@147
    .line 837
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@14a
    .line 838
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@14d
    .line 841
    :cond_14d
    const v6, 0x7f06014c

    #@150
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@153
    move-result-object v6

    #@154
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@157
    move-result-object v3

    #@158
    .line 842
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@15b
    .line 843
    const v6, 0x7f06014c

    #@15e
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@161
    move-result-object v6

    #@162
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@165
    move-result v4

    #@166
    .line 844
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@169
    .line 846
    const v6, 0x7f060150

    #@16c
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@16f
    move-result-object v6

    #@170
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@173
    move-result-object v5

    #@174
    .line 847
    .local v5, list:Landroid/preference/ListPreference;
    invoke-direct {p0, v5, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@177
    .line 848
    const v6, 0x7f060150

    #@17a
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@17d
    move-result-object v6

    #@17e
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@181
    move-result v4

    #@182
    .line 849
    invoke-direct {p0, v2, v4, v5}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@185
    .line 851
    const v6, 0x7f060152

    #@188
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@18b
    move-result-object v6

    #@18c
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@18f
    move-result-object v5

    #@190
    .line 852
    invoke-direct {p0, v5, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@193
    .line 853
    const v6, 0x7f060152

    #@196
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@199
    move-result-object v6

    #@19a
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@19d
    move-result v4

    #@19e
    .line 854
    invoke-direct {p0, v2, v4, v5}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@1a1
    .line 856
    const v6, 0x7f060151

    #@1a4
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@1a7
    move-result-object v6

    #@1a8
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@1ab
    move-result-object v5

    #@1ac
    .line 857
    invoke-direct {p0, v5, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@1af
    .line 858
    const v6, 0x7f060151

    #@1b2
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@1b5
    move-result-object v6

    #@1b6
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@1b9
    move-result v4

    #@1ba
    .line 859
    invoke-direct {p0, v2, v4, v5}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@1bd
    .line 861
    const v6, 0x7f060154

    #@1c0
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@1c3
    move-result-object v6

    #@1c4
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@1c7
    move-result-object v3

    #@1c8
    .line 862
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@1cb
    .line 863
    const v6, 0x7f060154

    #@1ce
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@1d1
    move-result-object v6

    #@1d2
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@1d5
    move-result v4

    #@1d6
    .line 864
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@1d9
    .line 865
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@1dc
    .line 867
    const v6, 0x7f060155

    #@1df
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@1e2
    move-result-object v6

    #@1e3
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@1e6
    move-result-object v3

    #@1e7
    .line 868
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@1ea
    .line 869
    const v6, 0x7f060155

    #@1ed
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@1f0
    move-result-object v6

    #@1f1
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@1f4
    move-result v4

    #@1f5
    .line 870
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@1f8
    .line 871
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@1fb
    .line 873
    const v6, 0x7f060156

    #@1fe
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@201
    move-result-object v6

    #@202
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@205
    move-result-object v5

    #@206
    .line 874
    invoke-direct {p0, v5, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@209
    .line 875
    const v6, 0x7f060156

    #@20c
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@20f
    move-result-object v6

    #@210
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@213
    move-result v4

    #@214
    .line 876
    invoke-direct {p0, v2, v4, v5}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@217
    .line 878
    const v6, 0x7f060157

    #@21a
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@21d
    move-result-object v6

    #@21e
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@221
    move-result-object v5

    #@222
    .line 879
    invoke-direct {p0, v5, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@225
    .line 880
    const v6, 0x7f060157

    #@228
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@22b
    move-result-object v6

    #@22c
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@22f
    move-result v4

    #@230
    .line 881
    invoke-direct {p0, v2, v4, v5}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@233
    .line 883
    const v6, 0x7f060158

    #@236
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@239
    move-result-object v6

    #@23a
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@23d
    move-result-object v3

    #@23e
    .line 884
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@241
    .line 885
    const v6, 0x7f060158

    #@244
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@247
    move-result-object v6

    #@248
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@24b
    move-result v4

    #@24c
    .line 886
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@24f
    .line 888
    const v6, 0x7f060159

    #@252
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@255
    move-result-object v6

    #@256
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getCheckBoxPreference(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    #@259
    move-result-object v0

    #@25a
    .line 889
    .local v0, checkbox:Landroid/preference/CheckBoxPreference;
    invoke-direct {p0, v0, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@25d
    .line 890
    const v6, 0x7f060159

    #@260
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@263
    move-result-object v6

    #@264
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@267
    move-result v4

    #@268
    .line 891
    invoke-direct {p0, v2, v4, v0}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/CheckBoxPreference;)V

    #@26b
    .line 894
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    #@26e
    goto/16 :goto_3b
.end method

.method private registerEnablerSIPChangeListener(Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;)V
    .registers 16
    .parameter "listener"

    #@0
    .prologue
    const v13, 0x7f060145

    #@3
    const v12, 0x7f060144

    #@6
    const v11, 0x7f060143

    #@9
    const v10, 0x7f060142

    #@c
    const v9, 0x7f060141

    #@f
    .line 960
    invoke-virtual {p1}, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;->getTableName()Ljava/lang/String;

    #@12
    move-result-object v7

    #@13
    invoke-direct {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getSettingsTable(Ljava/lang/String;)Landroid/database/Cursor;

    #@16
    move-result-object v2

    #@17
    .line 962
    .local v2, cursor:Landroid/database/Cursor;
    if-nez v2, :cond_3c

    #@19
    .line 963
    const-string v7, "LGIMSProvisioning"

    #@1b
    new-instance v8, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v9, "Cursor :: "

    #@22
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v8

    #@26
    invoke-virtual {p1}, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;->getTableName()Ljava/lang/String;

    #@29
    move-result-object v9

    #@2a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v8

    #@2e
    const-string v9, " is null"

    #@30
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v8

    #@34
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v8

    #@38
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 1105
    :goto_3b
    return-void

    #@3c
    .line 967
    :cond_3c
    invoke-interface {v2}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    #@3f
    move-result-object v1

    #@40
    .line 969
    .local v1, columns:[Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/lge/ims/setting/IMSProvisioning;->displayColumns([Ljava/lang/String;)V

    #@43
    .line 972
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    #@46
    .line 974
    invoke-virtual {p1}, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;->getPrefix()Ljava/lang/String;

    #@49
    move-result-object v6

    #@4a
    .line 976
    .local v6, prefix:Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    #@4c
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@4f
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v7

    #@53
    invoke-virtual {p0, v9}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@56
    move-result-object v8

    #@57
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v7

    #@5b
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v7

    #@5f
    invoke-direct {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@62
    move-result-object v3

    #@63
    .line 977
    .local v3, edit:Landroid/preference/EditTextPreference;
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@66
    .line 978
    invoke-virtual {p0, v9}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@69
    move-result-object v7

    #@6a
    invoke-direct {p0, v1, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@6d
    move-result v4

    #@6e
    .line 979
    .local v4, index:I
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@71
    .line 980
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@74
    .line 982
    new-instance v7, Ljava/lang/StringBuilder;

    #@76
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@79
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v7

    #@7d
    invoke-virtual {p0, v10}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@80
    move-result-object v8

    #@81
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v7

    #@85
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@88
    move-result-object v7

    #@89
    invoke-direct {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@8c
    move-result-object v3

    #@8d
    .line 983
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@90
    .line 984
    invoke-virtual {p0, v10}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@93
    move-result-object v7

    #@94
    invoke-direct {p0, v1, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@97
    move-result v4

    #@98
    .line 985
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@9b
    .line 986
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@9e
    .line 988
    new-instance v7, Ljava/lang/StringBuilder;

    #@a0
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@a3
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v7

    #@a7
    invoke-virtual {p0, v11}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@aa
    move-result-object v8

    #@ab
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v7

    #@af
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b2
    move-result-object v7

    #@b3
    invoke-direct {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@b6
    move-result-object v3

    #@b7
    .line 990
    if-eqz v3, :cond_ca

    #@b9
    .line 991
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@bc
    .line 992
    invoke-virtual {p0, v11}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@bf
    move-result-object v7

    #@c0
    invoke-direct {p0, v1, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@c3
    move-result v4

    #@c4
    .line 993
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@c7
    .line 994
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@ca
    .line 997
    :cond_ca
    new-instance v7, Ljava/lang/StringBuilder;

    #@cc
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@cf
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v7

    #@d3
    invoke-virtual {p0, v12}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@d6
    move-result-object v8

    #@d7
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@da
    move-result-object v7

    #@db
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@de
    move-result-object v7

    #@df
    invoke-direct {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@e2
    move-result-object v3

    #@e3
    .line 999
    if-eqz v3, :cond_f6

    #@e5
    .line 1000
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@e8
    .line 1001
    invoke-virtual {p0, v12}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@eb
    move-result-object v7

    #@ec
    invoke-direct {p0, v1, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@ef
    move-result v4

    #@f0
    .line 1002
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@f3
    .line 1003
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@f6
    .line 1006
    :cond_f6
    new-instance v7, Ljava/lang/StringBuilder;

    #@f8
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@fb
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fe
    move-result-object v7

    #@ff
    invoke-virtual {p0, v13}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@102
    move-result-object v8

    #@103
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@106
    move-result-object v7

    #@107
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10a
    move-result-object v7

    #@10b
    invoke-direct {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@10e
    move-result-object v3

    #@10f
    .line 1008
    if-eqz v3, :cond_122

    #@111
    .line 1009
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@114
    .line 1010
    invoke-virtual {p0, v13}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@117
    move-result-object v7

    #@118
    invoke-direct {p0, v1, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@11b
    move-result v4

    #@11c
    .line 1011
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@11f
    .line 1012
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@122
    .line 1015
    :cond_122
    new-instance v7, Ljava/lang/StringBuilder;

    #@124
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@127
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12a
    move-result-object v7

    #@12b
    const v8, 0x7f060146

    #@12e
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@131
    move-result-object v8

    #@132
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@135
    move-result-object v7

    #@136
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@139
    move-result-object v7

    #@13a
    invoke-direct {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@13d
    move-result-object v3

    #@13e
    .line 1017
    if-eqz v3, :cond_154

    #@140
    .line 1018
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@143
    .line 1019
    const v7, 0x7f060146

    #@146
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@149
    move-result-object v7

    #@14a
    invoke-direct {p0, v1, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@14d
    move-result v4

    #@14e
    .line 1020
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@151
    .line 1021
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@154
    .line 1024
    :cond_154
    new-instance v7, Ljava/lang/StringBuilder;

    #@156
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@159
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15c
    move-result-object v7

    #@15d
    const v8, 0x7f060147

    #@160
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@163
    move-result-object v8

    #@164
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@167
    move-result-object v7

    #@168
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16b
    move-result-object v7

    #@16c
    invoke-direct {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@16f
    move-result-object v3

    #@170
    .line 1026
    if-eqz v3, :cond_186

    #@172
    .line 1027
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@175
    .line 1028
    const v7, 0x7f060147

    #@178
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@17b
    move-result-object v7

    #@17c
    invoke-direct {p0, v1, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@17f
    move-result v4

    #@180
    .line 1029
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@183
    .line 1030
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@186
    .line 1033
    :cond_186
    new-instance v7, Ljava/lang/StringBuilder;

    #@188
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@18b
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18e
    move-result-object v7

    #@18f
    const v8, 0x7f060148

    #@192
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@195
    move-result-object v8

    #@196
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@199
    move-result-object v7

    #@19a
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19d
    move-result-object v7

    #@19e
    invoke-direct {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@1a1
    move-result-object v3

    #@1a2
    .line 1035
    if-eqz v3, :cond_1b8

    #@1a4
    .line 1036
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@1a7
    .line 1037
    const v7, 0x7f060148

    #@1aa
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@1ad
    move-result-object v7

    #@1ae
    invoke-direct {p0, v1, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@1b1
    move-result v4

    #@1b2
    .line 1038
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@1b5
    .line 1039
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@1b8
    .line 1042
    :cond_1b8
    new-instance v7, Ljava/lang/StringBuilder;

    #@1ba
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1bd
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c0
    move-result-object v7

    #@1c1
    const v8, 0x7f060149

    #@1c4
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@1c7
    move-result-object v8

    #@1c8
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cb
    move-result-object v7

    #@1cc
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1cf
    move-result-object v7

    #@1d0
    invoke-direct {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@1d3
    move-result-object v3

    #@1d4
    .line 1044
    if-eqz v3, :cond_1ea

    #@1d6
    .line 1045
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@1d9
    .line 1046
    const v7, 0x7f060149

    #@1dc
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@1df
    move-result-object v7

    #@1e0
    invoke-direct {p0, v1, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@1e3
    move-result v4

    #@1e4
    .line 1047
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@1e7
    .line 1048
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@1ea
    .line 1051
    :cond_1ea
    new-instance v7, Ljava/lang/StringBuilder;

    #@1ec
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1ef
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f2
    move-result-object v7

    #@1f3
    const v8, 0x7f06014c

    #@1f6
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@1f9
    move-result-object v8

    #@1fa
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1fd
    move-result-object v7

    #@1fe
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@201
    move-result-object v7

    #@202
    invoke-direct {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@205
    move-result-object v3

    #@206
    .line 1052
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@209
    .line 1053
    const v7, 0x7f06014c

    #@20c
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@20f
    move-result-object v7

    #@210
    invoke-direct {p0, v1, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@213
    move-result v4

    #@214
    .line 1054
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@217
    .line 1056
    new-instance v7, Ljava/lang/StringBuilder;

    #@219
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@21c
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21f
    move-result-object v7

    #@220
    const v8, 0x7f060150

    #@223
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@226
    move-result-object v8

    #@227
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22a
    move-result-object v7

    #@22b
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22e
    move-result-object v7

    #@22f
    invoke-direct {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@232
    move-result-object v5

    #@233
    .line 1057
    .local v5, list:Landroid/preference/ListPreference;
    invoke-direct {p0, v5, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@236
    .line 1058
    const v7, 0x7f060150

    #@239
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@23c
    move-result-object v7

    #@23d
    invoke-direct {p0, v1, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@240
    move-result v4

    #@241
    .line 1059
    invoke-direct {p0, v2, v4, v5}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@244
    .line 1061
    new-instance v7, Ljava/lang/StringBuilder;

    #@246
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@249
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24c
    move-result-object v7

    #@24d
    const v8, 0x7f060152

    #@250
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@253
    move-result-object v8

    #@254
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@257
    move-result-object v7

    #@258
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25b
    move-result-object v7

    #@25c
    invoke-direct {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@25f
    move-result-object v5

    #@260
    .line 1062
    invoke-direct {p0, v5, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@263
    .line 1063
    const v7, 0x7f060152

    #@266
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@269
    move-result-object v7

    #@26a
    invoke-direct {p0, v1, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@26d
    move-result v4

    #@26e
    .line 1064
    invoke-direct {p0, v2, v4, v5}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@271
    .line 1066
    new-instance v7, Ljava/lang/StringBuilder;

    #@273
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@276
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@279
    move-result-object v7

    #@27a
    const v8, 0x7f060151

    #@27d
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@280
    move-result-object v8

    #@281
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@284
    move-result-object v7

    #@285
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@288
    move-result-object v7

    #@289
    invoke-direct {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@28c
    move-result-object v5

    #@28d
    .line 1067
    invoke-direct {p0, v5, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@290
    .line 1068
    const v7, 0x7f060151

    #@293
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@296
    move-result-object v7

    #@297
    invoke-direct {p0, v1, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@29a
    move-result v4

    #@29b
    .line 1069
    invoke-direct {p0, v2, v4, v5}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@29e
    .line 1071
    new-instance v7, Ljava/lang/StringBuilder;

    #@2a0
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@2a3
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a6
    move-result-object v7

    #@2a7
    const v8, 0x7f060154

    #@2aa
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@2ad
    move-result-object v8

    #@2ae
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b1
    move-result-object v7

    #@2b2
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b5
    move-result-object v7

    #@2b6
    invoke-direct {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@2b9
    move-result-object v3

    #@2ba
    .line 1072
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@2bd
    .line 1073
    const v7, 0x7f060154

    #@2c0
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@2c3
    move-result-object v7

    #@2c4
    invoke-direct {p0, v1, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@2c7
    move-result v4

    #@2c8
    .line 1074
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@2cb
    .line 1075
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@2ce
    .line 1077
    new-instance v7, Ljava/lang/StringBuilder;

    #@2d0
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@2d3
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d6
    move-result-object v7

    #@2d7
    const v8, 0x7f060155

    #@2da
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@2dd
    move-result-object v8

    #@2de
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e1
    move-result-object v7

    #@2e2
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e5
    move-result-object v7

    #@2e6
    invoke-direct {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@2e9
    move-result-object v3

    #@2ea
    .line 1078
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@2ed
    .line 1079
    const v7, 0x7f060155

    #@2f0
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@2f3
    move-result-object v7

    #@2f4
    invoke-direct {p0, v1, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@2f7
    move-result v4

    #@2f8
    .line 1080
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@2fb
    .line 1081
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@2fe
    .line 1083
    new-instance v7, Ljava/lang/StringBuilder;

    #@300
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@303
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@306
    move-result-object v7

    #@307
    const v8, 0x7f060156

    #@30a
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@30d
    move-result-object v8

    #@30e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@311
    move-result-object v7

    #@312
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@315
    move-result-object v7

    #@316
    invoke-direct {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@319
    move-result-object v5

    #@31a
    .line 1084
    invoke-direct {p0, v5, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@31d
    .line 1085
    const v7, 0x7f060156

    #@320
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@323
    move-result-object v7

    #@324
    invoke-direct {p0, v1, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@327
    move-result v4

    #@328
    .line 1086
    invoke-direct {p0, v2, v4, v5}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@32b
    .line 1088
    new-instance v7, Ljava/lang/StringBuilder;

    #@32d
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@330
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@333
    move-result-object v7

    #@334
    const v8, 0x7f060157

    #@337
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@33a
    move-result-object v8

    #@33b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33e
    move-result-object v7

    #@33f
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@342
    move-result-object v7

    #@343
    invoke-direct {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@346
    move-result-object v5

    #@347
    .line 1089
    invoke-direct {p0, v5, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@34a
    .line 1090
    const v7, 0x7f060157

    #@34d
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@350
    move-result-object v7

    #@351
    invoke-direct {p0, v1, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@354
    move-result v4

    #@355
    .line 1091
    invoke-direct {p0, v2, v4, v5}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@358
    .line 1093
    new-instance v7, Ljava/lang/StringBuilder;

    #@35a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@35d
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@360
    move-result-object v7

    #@361
    const v8, 0x7f060158

    #@364
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@367
    move-result-object v8

    #@368
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36b
    move-result-object v7

    #@36c
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36f
    move-result-object v7

    #@370
    invoke-direct {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@373
    move-result-object v3

    #@374
    .line 1094
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@377
    .line 1095
    const v7, 0x7f060158

    #@37a
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@37d
    move-result-object v7

    #@37e
    invoke-direct {p0, v1, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@381
    move-result v4

    #@382
    .line 1096
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@385
    .line 1098
    new-instance v7, Ljava/lang/StringBuilder;

    #@387
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@38a
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38d
    move-result-object v7

    #@38e
    const v8, 0x7f060159

    #@391
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@394
    move-result-object v8

    #@395
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@398
    move-result-object v7

    #@399
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39c
    move-result-object v7

    #@39d
    invoke-direct {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getCheckBoxPreference(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    #@3a0
    move-result-object v0

    #@3a1
    .line 1099
    .local v0, checkbox:Landroid/preference/CheckBoxPreference;
    invoke-direct {p0, v0, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@3a4
    .line 1100
    const v7, 0x7f060159

    #@3a7
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@3aa
    move-result-object v7

    #@3ab
    invoke-direct {p0, v1, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@3ae
    move-result v4

    #@3af
    .line 1101
    invoke-direct {p0, v2, v4, v0}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/CheckBoxPreference;)V

    #@3b2
    .line 1104
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    #@3b5
    goto/16 :goto_3b
.end method

.method private registerHTTPChangeListener(Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;)V
    .registers 11
    .parameter "listener"

    #@0
    .prologue
    const v8, 0x7f060301

    #@3
    const v7, 0x7f060300

    #@6
    .line 1312
    invoke-virtual {p1}, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;->getTableName()Ljava/lang/String;

    #@9
    move-result-object v5

    #@a
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getSettingsTable(Ljava/lang/String;)Landroid/database/Cursor;

    #@d
    move-result-object v1

    #@e
    .line 1314
    .local v1, cursor:Landroid/database/Cursor;
    if-nez v1, :cond_33

    #@10
    .line 1315
    const-string v5, "LGIMSProvisioning"

    #@12
    new-instance v6, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v7, "Cursor :: "

    #@19
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v6

    #@1d
    invoke-virtual {p1}, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;->getTableName()Ljava/lang/String;

    #@20
    move-result-object v7

    #@21
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v6

    #@25
    const-string v7, " is null"

    #@27
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v6

    #@2b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v6

    #@2f
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 1341
    :goto_32
    return-void

    #@33
    .line 1319
    :cond_33
    invoke-interface {v1}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    #@36
    move-result-object v0

    #@37
    .line 1321
    .local v0, columns:[Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/lge/ims/setting/IMSProvisioning;->displayColumns([Ljava/lang/String;)V

    #@3a
    .line 1324
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    #@3d
    .line 1326
    invoke-virtual {p1}, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;->getPrefix()Ljava/lang/String;

    #@40
    move-result-object v4

    #@41
    .line 1328
    .local v4, prefix:Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v5

    #@4a
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@4d
    move-result-object v6

    #@4e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v5

    #@52
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v5

    #@56
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@59
    move-result-object v2

    #@5a
    .line 1329
    .local v2, edit:Landroid/preference/EditTextPreference;
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@5d
    .line 1330
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@60
    move-result-object v5

    #@61
    invoke-direct {p0, v0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@64
    move-result v3

    #@65
    .line 1331
    .local v3, index:I
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@68
    .line 1333
    new-instance v5, Ljava/lang/StringBuilder;

    #@6a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@6d
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v5

    #@71
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@74
    move-result-object v6

    #@75
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v5

    #@79
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v5

    #@7d
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@80
    move-result-object v2

    #@81
    .line 1334
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@84
    .line 1335
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@87
    move-result-object v5

    #@88
    invoke-direct {p0, v0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@8b
    move-result v3

    #@8c
    .line 1336
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@8f
    .line 1337
    invoke-direct {p0, v2}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@92
    .line 1340
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    #@95
    goto :goto_32
.end method

.method private registerIMApplicationChangeListener(Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;)V
    .registers 14
    .parameter "listener"

    #@0
    .prologue
    const v11, 0x7f0602ef

    #@3
    const v10, 0x7f0602ee

    #@6
    const v9, 0x7f0602ed

    #@9
    const v8, 0x7f0602ec

    #@c
    const v7, 0x7f0602eb

    #@f
    .line 1110
    invoke-virtual {p1}, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;->getTableName()Ljava/lang/String;

    #@12
    move-result-object v5

    #@13
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getSettingsTable(Ljava/lang/String;)Landroid/database/Cursor;

    #@16
    move-result-object v1

    #@17
    .line 1112
    .local v1, cursor:Landroid/database/Cursor;
    if-nez v1, :cond_3c

    #@19
    .line 1113
    const-string v5, "LGIMSProvisioning"

    #@1b
    new-instance v6, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v7, "Cursor :: "

    #@22
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v6

    #@26
    invoke-virtual {p1}, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;->getTableName()Ljava/lang/String;

    #@29
    move-result-object v7

    #@2a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v6

    #@2e
    const-string v7, " is null"

    #@30
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v6

    #@34
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v6

    #@38
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 1167
    :goto_3b
    return-void

    #@3c
    .line 1117
    :cond_3c
    invoke-interface {v1}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    #@3f
    move-result-object v0

    #@40
    .line 1119
    .local v0, columns:[Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/lge/ims/setting/IMSProvisioning;->displayColumns([Ljava/lang/String;)V

    #@43
    .line 1122
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    #@46
    .line 1124
    invoke-virtual {p1}, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;->getPrefix()Ljava/lang/String;

    #@49
    move-result-object v4

    #@4a
    .line 1126
    .local v4, prefix:Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    #@4c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@4f
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v5

    #@53
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@56
    move-result-object v6

    #@57
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v5

    #@5b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v5

    #@5f
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@62
    move-result-object v2

    #@63
    .line 1127
    .local v2, edit:Landroid/preference/EditTextPreference;
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@66
    .line 1128
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@69
    move-result-object v5

    #@6a
    invoke-direct {p0, v0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@6d
    move-result v3

    #@6e
    .line 1129
    .local v3, index:I
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@71
    .line 1130
    invoke-direct {p0, v2}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@74
    .line 1132
    new-instance v5, Ljava/lang/StringBuilder;

    #@76
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@79
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v5

    #@7d
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@80
    move-result-object v6

    #@81
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v5

    #@85
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@88
    move-result-object v5

    #@89
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@8c
    move-result-object v2

    #@8d
    .line 1133
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@90
    .line 1134
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@93
    move-result-object v5

    #@94
    invoke-direct {p0, v0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@97
    move-result v3

    #@98
    .line 1135
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@9b
    .line 1137
    new-instance v5, Ljava/lang/StringBuilder;

    #@9d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@a0
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v5

    #@a4
    invoke-virtual {p0, v9}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@a7
    move-result-object v6

    #@a8
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v5

    #@ac
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@af
    move-result-object v5

    #@b0
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@b3
    move-result-object v2

    #@b4
    .line 1138
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@b7
    .line 1139
    invoke-virtual {p0, v9}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@ba
    move-result-object v5

    #@bb
    invoke-direct {p0, v0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@be
    move-result v3

    #@bf
    .line 1140
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@c2
    .line 1142
    new-instance v5, Ljava/lang/StringBuilder;

    #@c4
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@c7
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v5

    #@cb
    invoke-virtual {p0, v10}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@ce
    move-result-object v6

    #@cf
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v5

    #@d3
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d6
    move-result-object v5

    #@d7
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@da
    move-result-object v2

    #@db
    .line 1143
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@de
    .line 1144
    invoke-virtual {p0, v10}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@e1
    move-result-object v5

    #@e2
    invoke-direct {p0, v0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@e5
    move-result v3

    #@e6
    .line 1145
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@e9
    .line 1147
    new-instance v5, Ljava/lang/StringBuilder;

    #@eb
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@ee
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f1
    move-result-object v5

    #@f2
    invoke-virtual {p0, v11}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@f5
    move-result-object v6

    #@f6
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f9
    move-result-object v5

    #@fa
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fd
    move-result-object v5

    #@fe
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@101
    move-result-object v2

    #@102
    .line 1148
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@105
    .line 1149
    invoke-virtual {p0, v11}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@108
    move-result-object v5

    #@109
    invoke-direct {p0, v0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@10c
    move-result v3

    #@10d
    .line 1150
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@110
    .line 1153
    const-string v5, "ft_http_config"

    #@112
    invoke-direct {p0, v0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@115
    move-result v3

    #@116
    .line 1155
    if-ltz v3, :cond_148

    #@118
    .line 1156
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@11b
    move-result-object v5

    #@11c
    iput-object v5, p0, Lcom/lge/ims/setting/IMSProvisioning;->imHTTPTable:Ljava/lang/String;

    #@11e
    .line 1158
    iget-object v5, p0, Lcom/lge/ims/setting/IMSProvisioning;->imHTTPTable:Ljava/lang/String;

    #@120
    if-eqz v5, :cond_12e

    #@122
    .line 1159
    iget-object v5, p0, Lcom/lge/ims/setting/IMSProvisioning;->imHTTPTable:Ljava/lang/String;

    #@124
    const/16 v6, 0x2e

    #@126
    const/16 v7, 0x5f

    #@128
    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    #@12b
    move-result-object v5

    #@12c
    iput-object v5, p0, Lcom/lge/ims/setting/IMSProvisioning;->imHTTPTable:Ljava/lang/String;

    #@12e
    .line 1162
    :cond_12e
    const-string v5, "LGIMSProvisioning"

    #@130
    new-instance v6, Ljava/lang/StringBuilder;

    #@132
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@135
    const-string v7, "IM HTTP config. table :: "

    #@137
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13a
    move-result-object v6

    #@13b
    iget-object v7, p0, Lcom/lge/ims/setting/IMSProvisioning;->imHTTPTable:Ljava/lang/String;

    #@13d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@140
    move-result-object v6

    #@141
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@144
    move-result-object v6

    #@145
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@148
    .line 1166
    :cond_148
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    #@14b
    goto/16 :goto_3b
.end method

.method private registerIPApplicationChangeListener(Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;)V
    .registers 14
    .parameter "listener"

    #@0
    .prologue
    const v11, 0x7f0602f7

    #@3
    const v10, 0x7f0602f6

    #@6
    const v9, 0x7f0602f5

    #@9
    const v8, 0x7f0602f4

    #@c
    const v7, 0x7f0602f3

    #@f
    .line 1172
    invoke-virtual {p1}, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;->getTableName()Ljava/lang/String;

    #@12
    move-result-object v5

    #@13
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getSettingsTable(Ljava/lang/String;)Landroid/database/Cursor;

    #@16
    move-result-object v1

    #@17
    .line 1174
    .local v1, cursor:Landroid/database/Cursor;
    if-nez v1, :cond_3c

    #@19
    .line 1175
    const-string v5, "LGIMSProvisioning"

    #@1b
    new-instance v6, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v7, "Cursor :: "

    #@22
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v6

    #@26
    invoke-virtual {p1}, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;->getTableName()Ljava/lang/String;

    #@29
    move-result-object v7

    #@2a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v6

    #@2e
    const-string v7, " is null"

    #@30
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v6

    #@34
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v6

    #@38
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 1223
    :goto_3b
    return-void

    #@3c
    .line 1179
    :cond_3c
    invoke-interface {v1}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    #@3f
    move-result-object v0

    #@40
    .line 1181
    .local v0, columns:[Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/lge/ims/setting/IMSProvisioning;->displayColumns([Ljava/lang/String;)V

    #@43
    .line 1184
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    #@46
    .line 1186
    invoke-virtual {p1}, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;->getPrefix()Ljava/lang/String;

    #@49
    move-result-object v4

    #@4a
    .line 1188
    .local v4, prefix:Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    #@4c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@4f
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v5

    #@53
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@56
    move-result-object v6

    #@57
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v5

    #@5b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v5

    #@5f
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@62
    move-result-object v2

    #@63
    .line 1189
    .local v2, edit:Landroid/preference/EditTextPreference;
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@66
    .line 1190
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@69
    move-result-object v5

    #@6a
    invoke-direct {p0, v0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@6d
    move-result v3

    #@6e
    .line 1191
    .local v3, index:I
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@71
    .line 1192
    invoke-direct {p0, v2}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@74
    .line 1194
    new-instance v5, Ljava/lang/StringBuilder;

    #@76
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@79
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v5

    #@7d
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@80
    move-result-object v6

    #@81
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v5

    #@85
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@88
    move-result-object v5

    #@89
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@8c
    move-result-object v2

    #@8d
    .line 1195
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@90
    .line 1196
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@93
    move-result-object v5

    #@94
    invoke-direct {p0, v0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@97
    move-result v3

    #@98
    .line 1197
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@9b
    .line 1199
    new-instance v5, Ljava/lang/StringBuilder;

    #@9d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@a0
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v5

    #@a4
    invoke-virtual {p0, v9}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@a7
    move-result-object v6

    #@a8
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v5

    #@ac
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@af
    move-result-object v5

    #@b0
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@b3
    move-result-object v2

    #@b4
    .line 1200
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@b7
    .line 1201
    invoke-virtual {p0, v9}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@ba
    move-result-object v5

    #@bb
    invoke-direct {p0, v0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@be
    move-result v3

    #@bf
    .line 1202
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@c2
    .line 1203
    invoke-direct {p0, v2}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@c5
    .line 1205
    new-instance v5, Ljava/lang/StringBuilder;

    #@c7
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@ca
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v5

    #@ce
    invoke-virtual {p0, v10}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@d1
    move-result-object v6

    #@d2
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v5

    #@d6
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d9
    move-result-object v5

    #@da
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@dd
    move-result-object v2

    #@de
    .line 1206
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@e1
    .line 1207
    invoke-virtual {p0, v10}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@e4
    move-result-object v5

    #@e5
    invoke-direct {p0, v0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@e8
    move-result v3

    #@e9
    .line 1208
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@ec
    .line 1209
    invoke-direct {p0, v2}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@ef
    .line 1211
    new-instance v5, Ljava/lang/StringBuilder;

    #@f1
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@f4
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f7
    move-result-object v5

    #@f8
    invoke-virtual {p0, v11}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@fb
    move-result-object v6

    #@fc
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v5

    #@100
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@103
    move-result-object v5

    #@104
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@107
    move-result-object v2

    #@108
    .line 1212
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@10b
    .line 1213
    invoke-virtual {p0, v11}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@10e
    move-result-object v5

    #@10f
    invoke-direct {p0, v0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@112
    move-result v3

    #@113
    .line 1214
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@116
    .line 1216
    new-instance v5, Ljava/lang/StringBuilder;

    #@118
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@11b
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11e
    move-result-object v5

    #@11f
    const v6, 0x7f0602f8

    #@122
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@125
    move-result-object v6

    #@126
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@129
    move-result-object v5

    #@12a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12d
    move-result-object v5

    #@12e
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@131
    move-result-object v2

    #@132
    .line 1217
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@135
    .line 1218
    const v5, 0x7f0602f8

    #@138
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@13b
    move-result-object v5

    #@13c
    invoke-direct {p0, v0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@13f
    move-result v3

    #@140
    .line 1219
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@143
    .line 1222
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    #@146
    goto/16 :goto_3b
.end method

.method private registerSIPChangeListener(Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;)V
    .registers 14
    .parameter "listener"

    #@0
    .prologue
    const v11, 0x7f060136

    #@3
    const v10, 0x7f060135

    #@6
    const v9, 0x7f060134

    #@9
    const v8, 0x7f060133

    #@c
    const v7, 0x7f060132

    #@f
    .line 652
    invoke-virtual {p1}, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;->getTableName()Ljava/lang/String;

    #@12
    move-result-object v6

    #@13
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getSettingsTable(Ljava/lang/String;)Landroid/database/Cursor;

    #@16
    move-result-object v2

    #@17
    .line 654
    .local v2, cursor:Landroid/database/Cursor;
    if-nez v2, :cond_3c

    #@19
    .line 655
    const-string v6, "LGIMSProvisioning"

    #@1b
    new-instance v7, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v8, "Cursor :: ("

    #@22
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v7

    #@26
    invoke-virtual {p1}, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;->getTableName()Ljava/lang/String;

    #@29
    move-result-object v8

    #@2a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v7

    #@2e
    const-string v8, ") is null"

    #@30
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v7

    #@34
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v7

    #@38
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 745
    :goto_3b
    return-void

    #@3c
    .line 659
    :cond_3c
    invoke-interface {v2}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    #@3f
    move-result-object v1

    #@40
    .line 661
    .local v1, columns:[Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/lge/ims/setting/IMSProvisioning;->displayColumns([Ljava/lang/String;)V

    #@43
    .line 664
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    #@46
    .line 666
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@49
    move-result-object v6

    #@4a
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getCheckBoxPreference(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    #@4d
    move-result-object v0

    #@4e
    .line 667
    .local v0, checkbox:Landroid/preference/CheckBoxPreference;
    invoke-direct {p0, v0, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@51
    .line 668
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@54
    move-result-object v6

    #@55
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@58
    move-result v4

    #@59
    .line 669
    .local v4, index:I
    invoke-direct {p0, v2, v4, v0}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/CheckBoxPreference;)V

    #@5c
    .line 671
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@5f
    move-result-object v6

    #@60
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@63
    move-result-object v3

    #@64
    .line 672
    .local v3, edit:Landroid/preference/EditTextPreference;
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@67
    .line 673
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@6a
    move-result-object v6

    #@6b
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@6e
    move-result v4

    #@6f
    .line 674
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@72
    .line 676
    invoke-virtual {p0, v9}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@75
    move-result-object v6

    #@76
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@79
    move-result-object v3

    #@7a
    .line 677
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@7d
    .line 678
    invoke-virtual {p0, v9}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@80
    move-result-object v6

    #@81
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@84
    move-result v4

    #@85
    .line 679
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@88
    .line 681
    invoke-virtual {p0, v10}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@8b
    move-result-object v6

    #@8c
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@8f
    move-result-object v5

    #@90
    .line 682
    .local v5, list:Landroid/preference/ListPreference;
    invoke-direct {p0, v5, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@93
    .line 683
    invoke-virtual {p0, v10}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@96
    move-result-object v6

    #@97
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@9a
    move-result v4

    #@9b
    .line 684
    invoke-direct {p0, v2, v4, v5}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@9e
    .line 686
    invoke-virtual {p0, v11}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@a1
    move-result-object v6

    #@a2
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@a5
    move-result-object v3

    #@a6
    .line 687
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@a9
    .line 688
    invoke-virtual {p0, v11}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@ac
    move-result-object v6

    #@ad
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@b0
    move-result v4

    #@b1
    .line 689
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@b4
    .line 691
    const v6, 0x7f060139

    #@b7
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@ba
    move-result-object v6

    #@bb
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@be
    move-result-object v3

    #@bf
    .line 692
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@c2
    .line 693
    const v6, 0x7f060139

    #@c5
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@c8
    move-result-object v6

    #@c9
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@cc
    move-result v4

    #@cd
    .line 694
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@d0
    .line 696
    const v6, 0x7f06013a

    #@d3
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@d6
    move-result-object v6

    #@d7
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@da
    move-result-object v3

    #@db
    .line 697
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@de
    .line 698
    const v6, 0x7f06013a

    #@e1
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@e4
    move-result-object v6

    #@e5
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@e8
    move-result v4

    #@e9
    .line 699
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@ec
    .line 701
    const v6, 0x7f06013b

    #@ef
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@f2
    move-result-object v6

    #@f3
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@f6
    move-result-object v3

    #@f7
    .line 702
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@fa
    .line 703
    const v6, 0x7f06013b

    #@fd
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@100
    move-result-object v6

    #@101
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@104
    move-result v4

    #@105
    .line 704
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@108
    .line 706
    const v6, 0x7f06013d

    #@10b
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@10e
    move-result-object v6

    #@10f
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@112
    move-result-object v3

    #@113
    .line 707
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@116
    .line 708
    const v6, 0x7f06013d

    #@119
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@11c
    move-result-object v6

    #@11d
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@120
    move-result v4

    #@121
    .line 709
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@124
    .line 711
    const v6, 0x7f06013e

    #@127
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@12a
    move-result-object v6

    #@12b
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@12e
    move-result-object v3

    #@12f
    .line 712
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@132
    .line 713
    const v6, 0x7f06013e

    #@135
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@138
    move-result-object v6

    #@139
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@13c
    move-result v4

    #@13d
    .line 714
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@140
    .line 716
    const v6, 0x7f06013f

    #@143
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@146
    move-result-object v6

    #@147
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@14a
    move-result-object v3

    #@14b
    .line 717
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@14e
    .line 718
    const v6, 0x7f06013f

    #@151
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@154
    move-result-object v6

    #@155
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@158
    move-result v4

    #@159
    .line 719
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@15c
    .line 721
    const v6, 0x7f06014d

    #@15f
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@162
    move-result-object v6

    #@163
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@166
    move-result-object v3

    #@167
    .line 722
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@16a
    .line 723
    const v6, 0x7f06014d

    #@16d
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@170
    move-result-object v6

    #@171
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@174
    move-result v4

    #@175
    .line 724
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@178
    .line 725
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@17b
    .line 727
    const v6, 0x7f06014e

    #@17e
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@181
    move-result-object v6

    #@182
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getCheckBoxPreference(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    #@185
    move-result-object v0

    #@186
    .line 728
    invoke-direct {p0, v0, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@189
    .line 729
    const v6, 0x7f06014e

    #@18c
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@18f
    move-result-object v6

    #@190
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@193
    move-result v4

    #@194
    .line 730
    invoke-direct {p0, v2, v4, v0}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/CheckBoxPreference;)V

    #@197
    .line 732
    const v6, 0x7f06014f

    #@19a
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@19d
    move-result-object v6

    #@19e
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@1a1
    move-result-object v3

    #@1a2
    .line 733
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@1a5
    .line 734
    const v6, 0x7f06014f

    #@1a8
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@1ab
    move-result-object v6

    #@1ac
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@1af
    move-result v4

    #@1b0
    .line 735
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@1b3
    .line 736
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@1b6
    .line 738
    const v6, 0x7f06014b

    #@1b9
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@1bc
    move-result-object v6

    #@1bd
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@1c0
    move-result-object v3

    #@1c1
    .line 739
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@1c4
    .line 740
    const v6, 0x7f06014b

    #@1c7
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@1ca
    move-result-object v6

    #@1cb
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@1ce
    move-result v4

    #@1cf
    .line 741
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@1d2
    .line 744
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    #@1d5
    goto/16 :goto_3b
.end method

.method private registerSessionChangeListener(Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;)V
    .registers 15
    .parameter "listener"

    #@0
    .prologue
    const v12, 0x7f060160

    #@3
    const v11, 0x7f06015f

    #@6
    const v10, 0x7f06015e

    #@9
    const v9, 0x7f06015d

    #@c
    const v8, 0x7f06015c

    #@f
    .line 901
    invoke-virtual {p1}, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;->getTableName()Ljava/lang/String;

    #@12
    move-result-object v6

    #@13
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getSettingsTable(Ljava/lang/String;)Landroid/database/Cursor;

    #@16
    move-result-object v2

    #@17
    .line 903
    .local v2, cursor:Landroid/database/Cursor;
    if-nez v2, :cond_3c

    #@19
    .line 904
    const-string v6, "LGIMSProvisioning"

    #@1b
    new-instance v7, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v8, "Cursor :: "

    #@22
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v7

    #@26
    invoke-virtual {p1}, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;->getTableName()Ljava/lang/String;

    #@29
    move-result-object v8

    #@2a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v7

    #@2e
    const-string v8, " is null"

    #@30
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v7

    #@34
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v7

    #@38
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 953
    :goto_3b
    return-void

    #@3c
    .line 908
    :cond_3c
    invoke-interface {v2}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    #@3f
    move-result-object v1

    #@40
    .line 910
    .local v1, columns:[Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/lge/ims/setting/IMSProvisioning;->displayColumns([Ljava/lang/String;)V

    #@43
    .line 913
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    #@46
    .line 915
    invoke-virtual {p1}, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;->getPrefix()Ljava/lang/String;

    #@49
    move-result-object v5

    #@4a
    .line 917
    .local v5, prefix:Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    #@4c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@4f
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v6

    #@53
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@56
    move-result-object v7

    #@57
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v6

    #@5b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v6

    #@5f
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@62
    move-result-object v3

    #@63
    .line 918
    .local v3, edit:Landroid/preference/EditTextPreference;
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@66
    .line 919
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@69
    move-result-object v6

    #@6a
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@6d
    move-result v4

    #@6e
    .line 920
    .local v4, index:I
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@71
    .line 921
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@74
    .line 923
    new-instance v6, Ljava/lang/StringBuilder;

    #@76
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@79
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v6

    #@7d
    invoke-virtual {p0, v9}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@80
    move-result-object v7

    #@81
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v6

    #@85
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@88
    move-result-object v6

    #@89
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@8c
    move-result-object v3

    #@8d
    .line 924
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@90
    .line 925
    invoke-virtual {p0, v9}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@93
    move-result-object v6

    #@94
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@97
    move-result v4

    #@98
    .line 926
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@9b
    .line 927
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@9e
    .line 929
    new-instance v6, Ljava/lang/StringBuilder;

    #@a0
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@a3
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v6

    #@a7
    invoke-virtual {p0, v10}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@aa
    move-result-object v7

    #@ab
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v6

    #@af
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b2
    move-result-object v6

    #@b3
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@b6
    move-result-object v3

    #@b7
    .line 930
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@ba
    .line 931
    invoke-virtual {p0, v10}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@bd
    move-result-object v6

    #@be
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@c1
    move-result v4

    #@c2
    .line 932
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@c5
    .line 933
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@c8
    .line 935
    new-instance v6, Ljava/lang/StringBuilder;

    #@ca
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@cd
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d0
    move-result-object v6

    #@d1
    invoke-virtual {p0, v11}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@d4
    move-result-object v7

    #@d5
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v6

    #@d9
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@dc
    move-result-object v6

    #@dd
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@e0
    move-result-object v3

    #@e1
    .line 936
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@e4
    .line 937
    invoke-virtual {p0, v11}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@e7
    move-result-object v6

    #@e8
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@eb
    move-result v4

    #@ec
    .line 938
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@ef
    .line 939
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@f2
    .line 941
    new-instance v6, Ljava/lang/StringBuilder;

    #@f4
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@f7
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fa
    move-result-object v6

    #@fb
    invoke-virtual {p0, v12}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@fe
    move-result-object v7

    #@ff
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@102
    move-result-object v6

    #@103
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@106
    move-result-object v6

    #@107
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@10a
    move-result-object v3

    #@10b
    .line 942
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@10e
    .line 943
    invoke-virtual {p0, v12}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@111
    move-result-object v6

    #@112
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@115
    move-result v4

    #@116
    .line 944
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@119
    .line 946
    new-instance v6, Ljava/lang/StringBuilder;

    #@11b
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@11e
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@121
    move-result-object v6

    #@122
    const v7, 0x7f060161

    #@125
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@128
    move-result-object v7

    #@129
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12c
    move-result-object v6

    #@12d
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@130
    move-result-object v6

    #@131
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getCheckBoxPreference(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    #@134
    move-result-object v0

    #@135
    .line 947
    .local v0, checkbox:Landroid/preference/CheckBoxPreference;
    invoke-direct {p0, v0, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@138
    .line 948
    const v6, 0x7f060161

    #@13b
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@13e
    move-result-object v6

    #@13f
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@142
    move-result v4

    #@143
    .line 949
    invoke-direct {p0, v2, v4, v0}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/CheckBoxPreference;)V

    #@146
    .line 952
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    #@149
    goto/16 :goto_3b
.end method

.method private registerSubscriberChangeListener(Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;)V
    .registers 13
    .parameter "listener"

    #@0
    .prologue
    const v10, 0x7f060116

    #@3
    const v9, 0x7f060115

    #@6
    const v8, 0x7f060114

    #@9
    const v7, 0x7f060113

    #@c
    const v6, 0x7f060112

    #@f
    .line 263
    invoke-virtual {p1}, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;->getTableName()Ljava/lang/String;

    #@12
    move-result-object v5

    #@13
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getSettingsTable(Ljava/lang/String;)Landroid/database/Cursor;

    #@16
    move-result-object v2

    #@17
    .line 265
    .local v2, cursor:Landroid/database/Cursor;
    if-nez v2, :cond_3c

    #@19
    .line 266
    const-string v5, "LGIMSProvisioning"

    #@1b
    new-instance v6, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v7, "Cursor :: ("

    #@22
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v6

    #@26
    invoke-virtual {p1}, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;->getTableName()Ljava/lang/String;

    #@29
    move-result-object v7

    #@2a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v6

    #@2e
    const-string v7, ") is null"

    #@30
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v6

    #@34
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v6

    #@38
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 418
    :goto_3b
    return-void

    #@3c
    .line 270
    :cond_3c
    invoke-interface {v2}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    #@3f
    move-result-object v1

    #@40
    .line 272
    .local v1, columns:[Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/lge/ims/setting/IMSProvisioning;->displayColumns([Ljava/lang/String;)V

    #@43
    .line 275
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    #@46
    .line 277
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@49
    move-result-object v5

    #@4a
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getCheckBoxPreference(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    #@4d
    move-result-object v0

    #@4e
    .line 278
    .local v0, checkbox:Landroid/preference/CheckBoxPreference;
    invoke-direct {p0, v0, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@51
    .line 279
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@54
    move-result-object v5

    #@55
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@58
    move-result v4

    #@59
    .line 280
    .local v4, index:I
    invoke-direct {p0, v2, v4, v0}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/CheckBoxPreference;)V

    #@5c
    .line 282
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@5f
    move-result-object v5

    #@60
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getCheckBoxPreference(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    #@63
    move-result-object v0

    #@64
    .line 283
    invoke-direct {p0, v0, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@67
    .line 284
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@6a
    move-result-object v5

    #@6b
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@6e
    move-result v4

    #@6f
    .line 285
    invoke-direct {p0, v2, v4, v0}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/CheckBoxPreference;)V

    #@72
    .line 287
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@75
    move-result-object v5

    #@76
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getCheckBoxPreference(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    #@79
    move-result-object v0

    #@7a
    .line 288
    invoke-direct {p0, v0, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@7d
    .line 289
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@80
    move-result-object v5

    #@81
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@84
    move-result v4

    #@85
    .line 290
    invoke-direct {p0, v2, v4, v0}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/CheckBoxPreference;)V

    #@88
    .line 292
    invoke-virtual {p0, v10}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@8b
    move-result-object v5

    #@8c
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getCheckBoxPreference(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    #@8f
    move-result-object v0

    #@90
    .line 293
    invoke-direct {p0, v0, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@93
    .line 294
    invoke-virtual {p0, v10}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@96
    move-result-object v5

    #@97
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@9a
    move-result v4

    #@9b
    .line 295
    invoke-direct {p0, v2, v4, v0}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/CheckBoxPreference;)V

    #@9e
    .line 297
    invoke-virtual {p0, v9}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@a1
    move-result-object v5

    #@a2
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getCheckBoxPreference(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    #@a5
    move-result-object v0

    #@a6
    .line 298
    invoke-direct {p0, v0, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@a9
    .line 299
    invoke-virtual {p0, v9}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@ac
    move-result-object v5

    #@ad
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@b0
    move-result v4

    #@b1
    .line 300
    invoke-direct {p0, v2, v4, v0}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/CheckBoxPreference;)V

    #@b4
    .line 302
    const v5, 0x7f060118

    #@b7
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@ba
    move-result-object v5

    #@bb
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getCheckBoxPreference(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    #@be
    move-result-object v0

    #@bf
    .line 303
    invoke-direct {p0, v0, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@c2
    .line 304
    const v5, 0x7f060118

    #@c5
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@c8
    move-result-object v5

    #@c9
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@cc
    move-result v4

    #@cd
    .line 305
    invoke-direct {p0, v2, v4, v0}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/CheckBoxPreference;)V

    #@d0
    .line 307
    const v5, 0x7f06011a

    #@d3
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@d6
    move-result-object v5

    #@d7
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@da
    move-result-object v3

    #@db
    .line 308
    .local v3, edit:Landroid/preference/EditTextPreference;
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@de
    .line 309
    const v5, 0x7f06011a

    #@e1
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@e4
    move-result-object v5

    #@e5
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@e8
    move-result v4

    #@e9
    .line 310
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@ec
    .line 312
    const v5, 0x7f06011d

    #@ef
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@f2
    move-result-object v5

    #@f3
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@f6
    move-result-object v3

    #@f7
    .line 313
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@fa
    .line 314
    const v5, 0x7f06011d

    #@fd
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@100
    move-result-object v5

    #@101
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@104
    move-result v4

    #@105
    .line 315
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@108
    .line 316
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@10b
    .line 318
    const v5, 0x7f06011b

    #@10e
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@111
    move-result-object v5

    #@112
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@115
    move-result-object v3

    #@116
    .line 319
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@119
    .line 320
    const v5, 0x7f06011b

    #@11c
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@11f
    move-result-object v5

    #@120
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@123
    move-result v4

    #@124
    .line 321
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@127
    .line 323
    const v5, 0x7f06011e

    #@12a
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@12d
    move-result-object v5

    #@12e
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@131
    move-result-object v3

    #@132
    .line 324
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@135
    .line 325
    const v5, 0x7f06011e

    #@138
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@13b
    move-result-object v5

    #@13c
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@13f
    move-result v4

    #@140
    .line 326
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@143
    .line 327
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@146
    .line 329
    const v5, 0x7f06011c

    #@149
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@14c
    move-result-object v5

    #@14d
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@150
    move-result-object v3

    #@151
    .line 330
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@154
    .line 331
    const v5, 0x7f06011c

    #@157
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@15a
    move-result-object v5

    #@15b
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@15e
    move-result v4

    #@15f
    .line 332
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@162
    .line 334
    const v5, 0x7f06011f

    #@165
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@168
    move-result-object v5

    #@169
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@16c
    move-result-object v3

    #@16d
    .line 335
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@170
    .line 336
    const v5, 0x7f06011f

    #@173
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@176
    move-result-object v5

    #@177
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@17a
    move-result v4

    #@17b
    .line 337
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@17e
    .line 338
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@181
    .line 340
    const v5, 0x7f060120

    #@184
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@187
    move-result-object v5

    #@188
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@18b
    move-result-object v3

    #@18c
    .line 341
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@18f
    .line 342
    const v5, 0x7f060120

    #@192
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@195
    move-result-object v5

    #@196
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@199
    move-result v4

    #@19a
    .line 343
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@19d
    .line 345
    const v5, 0x7f060121

    #@1a0
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@1a3
    move-result-object v5

    #@1a4
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@1a7
    move-result-object v3

    #@1a8
    .line 346
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@1ab
    .line 347
    const v5, 0x7f060121

    #@1ae
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@1b1
    move-result-object v5

    #@1b2
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@1b5
    move-result v4

    #@1b6
    .line 348
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@1b9
    .line 350
    const v5, 0x7f060122

    #@1bc
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@1bf
    move-result-object v5

    #@1c0
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@1c3
    move-result-object v3

    #@1c4
    .line 351
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@1c7
    .line 352
    const v5, 0x7f060122

    #@1ca
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@1cd
    move-result-object v5

    #@1ce
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@1d1
    move-result v4

    #@1d2
    .line 353
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@1d5
    .line 355
    const v5, 0x7f060124

    #@1d8
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@1db
    move-result-object v5

    #@1dc
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@1df
    move-result-object v3

    #@1e0
    .line 356
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@1e3
    .line 357
    const v5, 0x7f060124

    #@1e6
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@1e9
    move-result-object v5

    #@1ea
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@1ed
    move-result v4

    #@1ee
    .line 358
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@1f1
    .line 359
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@1f4
    .line 361
    const v5, 0x7f060125

    #@1f7
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@1fa
    move-result-object v5

    #@1fb
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@1fe
    move-result-object v3

    #@1ff
    .line 362
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@202
    .line 363
    const v5, 0x7f060125

    #@205
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@208
    move-result-object v5

    #@209
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@20c
    move-result v4

    #@20d
    .line 364
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@210
    .line 366
    const v5, 0x7f060126

    #@213
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@216
    move-result-object v5

    #@217
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@21a
    move-result-object v3

    #@21b
    .line 367
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@21e
    .line 368
    const v5, 0x7f060126

    #@221
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@224
    move-result-object v5

    #@225
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@228
    move-result v4

    #@229
    .line 369
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@22c
    .line 371
    const v5, 0x7f060127

    #@22f
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@232
    move-result-object v5

    #@233
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@236
    move-result-object v3

    #@237
    .line 372
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@23a
    .line 373
    const v5, 0x7f060127

    #@23d
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@240
    move-result-object v5

    #@241
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@244
    move-result v4

    #@245
    .line 374
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@248
    .line 376
    const v5, 0x7f060128

    #@24b
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@24e
    move-result-object v5

    #@24f
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@252
    move-result-object v3

    #@253
    .line 377
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@256
    .line 378
    const v5, 0x7f060128

    #@259
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@25c
    move-result-object v5

    #@25d
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@260
    move-result v4

    #@261
    .line 379
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@264
    .line 381
    const v5, 0x7f060129

    #@267
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@26a
    move-result-object v5

    #@26b
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@26e
    move-result-object v3

    #@26f
    .line 382
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@272
    .line 383
    const v5, 0x7f060129

    #@275
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@278
    move-result-object v5

    #@279
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@27c
    move-result v4

    #@27d
    .line 384
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@280
    .line 386
    const v5, 0x7f06012a

    #@283
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@286
    move-result-object v5

    #@287
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@28a
    move-result-object v3

    #@28b
    .line 387
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@28e
    .line 388
    const v5, 0x7f06012a

    #@291
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@294
    move-result-object v5

    #@295
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@298
    move-result v4

    #@299
    .line 389
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@29c
    .line 391
    const v5, 0x7f06012b

    #@29f
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@2a2
    move-result-object v5

    #@2a3
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@2a6
    move-result-object v3

    #@2a7
    .line 392
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@2aa
    .line 393
    const v5, 0x7f06012b

    #@2ad
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@2b0
    move-result-object v5

    #@2b1
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@2b4
    move-result v4

    #@2b5
    .line 394
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@2b8
    .line 396
    const v5, 0x7f06012c

    #@2bb
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@2be
    move-result-object v5

    #@2bf
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@2c2
    move-result-object v3

    #@2c3
    .line 397
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@2c6
    .line 398
    const v5, 0x7f06012c

    #@2c9
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@2cc
    move-result-object v5

    #@2cd
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@2d0
    move-result v4

    #@2d1
    .line 399
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@2d4
    .line 401
    const v5, 0x7f06012d

    #@2d7
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@2da
    move-result-object v5

    #@2db
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getCheckBoxPreference(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    #@2de
    move-result-object v0

    #@2df
    .line 402
    invoke-direct {p0, v0, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@2e2
    .line 403
    const v5, 0x7f06012d

    #@2e5
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@2e8
    move-result-object v5

    #@2e9
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@2ec
    move-result v4

    #@2ed
    .line 404
    invoke-direct {p0, v2, v4, v0}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/CheckBoxPreference;)V

    #@2f0
    .line 406
    const v5, 0x7f06012e

    #@2f3
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@2f6
    move-result-object v5

    #@2f7
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@2fa
    move-result-object v3

    #@2fb
    .line 407
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@2fe
    .line 408
    const v5, 0x7f06012e

    #@301
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@304
    move-result-object v5

    #@305
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@308
    move-result v4

    #@309
    .line 409
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@30c
    .line 411
    const v5, 0x7f060117

    #@30f
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@312
    move-result-object v5

    #@313
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@316
    move-result-object v3

    #@317
    .line 412
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@31a
    .line 413
    const v5, 0x7f060117

    #@31d
    invoke-virtual {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@320
    move-result-object v5

    #@321
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@324
    move-result v4

    #@325
    .line 414
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@328
    .line 417
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    #@32b
    goto/16 :goto_3b
.end method

.method private registerTestChangeListener(Lcom/lge/ims/setting/IMSProvisioning$TestChangeListener;)V
    .registers 10
    .parameter "listener"

    #@0
    .prologue
    .line 1348
    const-string v6, "lgims_subscriber"

    #@2
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getSettingsTable(Ljava/lang/String;)Landroid/database/Cursor;

    #@5
    move-result-object v2

    #@6
    .line 1350
    .local v2, cursor:Landroid/database/Cursor;
    if-nez v2, :cond_10

    #@8
    .line 1351
    const-string v6, "LGIMSProvisioning"

    #@a
    const-string v7, "Cursor :: (lgims_subscriber) is null"

    #@c
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 1379
    :goto_f
    return-void

    #@10
    .line 1355
    :cond_10
    invoke-interface {v2}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    .line 1357
    .local v1, columns:[Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/lge/ims/setting/IMSProvisioning;->displayColumns([Ljava/lang/String;)V

    #@17
    .line 1360
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    #@1a
    .line 1362
    const-string v6, "subscriber_0_impu_2"

    #@1c
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@1f
    move-result-object v3

    #@20
    .line 1363
    .local v3, edit:Landroid/preference/EditTextPreference;
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@23
    .line 1364
    const-string v6, "subscriber_0_impu_2"

    #@25
    invoke-direct {p0, v1, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@28
    move-result v4

    #@29
    .line 1365
    .local v4, index:I
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@2c
    .line 1368
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    #@2f
    .line 1370
    const-string v6, "my_number"

    #@31
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@34
    move-result-object v3

    #@35
    .line 1371
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@38
    .line 1372
    const-string v6, "ft_media_protocol"

    #@3a
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@3d
    move-result-object v5

    #@3e
    .line 1373
    .local v5, list:Landroid/preference/ListPreference;
    invoke-direct {p0, v5, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@41
    .line 1375
    new-instance v6, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v7, "voip_"

    #@48
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v6

    #@4c
    const v7, 0x7f06015b

    #@4f
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@52
    move-result-object v7

    #@53
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v6

    #@57
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v6

    #@5b
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getCheckBoxPreference(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    #@5e
    move-result-object v0

    #@5f
    .line 1376
    .local v0, checkbox:Landroid/preference/CheckBoxPreference;
    invoke-direct {p0, v0, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@62
    .line 1377
    const-string v6, "app_server_setting"

    #@64
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/IMSProvisioning;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@67
    move-result-object v5

    #@68
    .line 1378
    invoke-direct {p0, v5, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@6b
    goto :goto_f
.end method

.method private registerXDMApplicationChangeListener(Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;)V
    .registers 10
    .parameter "listener"

    #@0
    .prologue
    const v7, 0x7f0602fe

    #@3
    .line 1228
    invoke-virtual {p1}, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;->getTableName()Ljava/lang/String;

    #@6
    move-result-object v5

    #@7
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getSettingsTable(Ljava/lang/String;)Landroid/database/Cursor;

    #@a
    move-result-object v1

    #@b
    .line 1230
    .local v1, cursor:Landroid/database/Cursor;
    if-nez v1, :cond_30

    #@d
    .line 1231
    const-string v5, "LGIMSProvisioning"

    #@f
    new-instance v6, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v7, "Cursor :: "

    #@16
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v6

    #@1a
    invoke-virtual {p1}, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;->getTableName()Ljava/lang/String;

    #@1d
    move-result-object v7

    #@1e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v6

    #@22
    const-string v7, " is null"

    #@24
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v6

    #@28
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v6

    #@2c
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 1251
    :goto_2f
    return-void

    #@30
    .line 1235
    :cond_30
    invoke-interface {v1}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    #@33
    move-result-object v0

    #@34
    .line 1237
    .local v0, columns:[Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/lge/ims/setting/IMSProvisioning;->displayColumns([Ljava/lang/String;)V

    #@37
    .line 1240
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    #@3a
    .line 1242
    invoke-virtual {p1}, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;->getPrefix()Ljava/lang/String;

    #@3d
    move-result-object v4

    #@3e
    .line 1244
    .local v4, prefix:Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v5

    #@47
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@4a
    move-result-object v6

    #@4b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v5

    #@4f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v5

    #@53
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@56
    move-result-object v2

    #@57
    .line 1245
    .local v2, edit:Landroid/preference/EditTextPreference;
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@5a
    .line 1246
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@5d
    move-result-object v5

    #@5e
    invoke-direct {p0, v0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@61
    move-result v3

    #@62
    .line 1247
    .local v3, index:I
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@65
    .line 1250
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    #@68
    goto :goto_2f
.end method

.method private registerXDMResourceChangeListener(Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;)V
    .registers 13
    .parameter "listener"

    #@0
    .prologue
    const v10, 0x7f0602fd

    #@3
    const v9, 0x7f0602fc

    #@6
    const v8, 0x7f0602fb

    #@9
    const v7, 0x7f0602fa

    #@c
    .line 1256
    invoke-virtual {p1}, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;->getTableName()Ljava/lang/String;

    #@f
    move-result-object v5

    #@10
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getSettingsTable(Ljava/lang/String;)Landroid/database/Cursor;

    #@13
    move-result-object v1

    #@14
    .line 1258
    .local v1, cursor:Landroid/database/Cursor;
    if-nez v1, :cond_39

    #@16
    .line 1259
    const-string v5, "LGIMSProvisioning"

    #@18
    new-instance v6, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v7, "Cursor :: "

    #@1f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v6

    #@23
    invoke-virtual {p1}, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;->getTableName()Ljava/lang/String;

    #@26
    move-result-object v7

    #@27
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v6

    #@2b
    const-string v7, " is null"

    #@2d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v6

    #@31
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v6

    #@35
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 1307
    :goto_38
    return-void

    #@39
    .line 1263
    :cond_39
    invoke-interface {v1}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    #@3c
    move-result-object v0

    #@3d
    .line 1265
    .local v0, columns:[Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/lge/ims/setting/IMSProvisioning;->displayColumns([Ljava/lang/String;)V

    #@40
    .line 1268
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    #@43
    .line 1270
    invoke-virtual {p1}, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;->getPrefix()Ljava/lang/String;

    #@46
    move-result-object v4

    #@47
    .line 1272
    .local v4, prefix:Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    #@49
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v5

    #@50
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@53
    move-result-object v6

    #@54
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v5

    #@58
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v5

    #@5c
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@5f
    move-result-object v2

    #@60
    .line 1273
    .local v2, edit:Landroid/preference/EditTextPreference;
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@63
    .line 1274
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@66
    move-result-object v5

    #@67
    invoke-direct {p0, v0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@6a
    move-result v3

    #@6b
    .line 1275
    .local v3, index:I
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@6e
    .line 1277
    new-instance v5, Ljava/lang/StringBuilder;

    #@70
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@73
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v5

    #@77
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@7a
    move-result-object v6

    #@7b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v5

    #@7f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@82
    move-result-object v5

    #@83
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@86
    move-result-object v2

    #@87
    .line 1278
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@8a
    .line 1279
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@8d
    move-result-object v5

    #@8e
    invoke-direct {p0, v0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@91
    move-result v3

    #@92
    .line 1280
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@95
    .line 1282
    new-instance v5, Ljava/lang/StringBuilder;

    #@97
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@9a
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v5

    #@9e
    invoke-virtual {p0, v9}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@a1
    move-result-object v6

    #@a2
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v5

    #@a6
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a9
    move-result-object v5

    #@aa
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@ad
    move-result-object v2

    #@ae
    .line 1283
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@b1
    .line 1284
    invoke-virtual {p0, v9}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@b4
    move-result-object v5

    #@b5
    invoke-direct {p0, v0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@b8
    move-result v3

    #@b9
    .line 1285
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@bc
    .line 1287
    new-instance v5, Ljava/lang/StringBuilder;

    #@be
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@c1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v5

    #@c5
    invoke-virtual {p0, v10}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@c8
    move-result-object v6

    #@c9
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cc
    move-result-object v5

    #@cd
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d0
    move-result-object v5

    #@d1
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@d4
    move-result-object v2

    #@d5
    .line 1288
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/IMSProvisioning;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@d8
    .line 1289
    invoke-virtual {p0, v10}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@db
    move-result-object v5

    #@dc
    invoke-direct {p0, v0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@df
    move-result v3

    #@e0
    .line 1290
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/IMSProvisioning;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@e3
    .line 1293
    const-string v5, "resource_http_config"

    #@e5
    invoke-direct {p0, v0, v5}, Lcom/lge/ims/setting/IMSProvisioning;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@e8
    move-result v3

    #@e9
    .line 1295
    if-ltz v3, :cond_11b

    #@eb
    .line 1296
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@ee
    move-result-object v5

    #@ef
    iput-object v5, p0, Lcom/lge/ims/setting/IMSProvisioning;->xdmHTTPTable:Ljava/lang/String;

    #@f1
    .line 1298
    iget-object v5, p0, Lcom/lge/ims/setting/IMSProvisioning;->xdmHTTPTable:Ljava/lang/String;

    #@f3
    if-eqz v5, :cond_101

    #@f5
    .line 1299
    iget-object v5, p0, Lcom/lge/ims/setting/IMSProvisioning;->xdmHTTPTable:Ljava/lang/String;

    #@f7
    const/16 v6, 0x2e

    #@f9
    const/16 v7, 0x5f

    #@fb
    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    #@fe
    move-result-object v5

    #@ff
    iput-object v5, p0, Lcom/lge/ims/setting/IMSProvisioning;->xdmHTTPTable:Ljava/lang/String;

    #@101
    .line 1302
    :cond_101
    const-string v5, "LGIMSProvisioning"

    #@103
    new-instance v6, Ljava/lang/StringBuilder;

    #@105
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@108
    const-string v7, "XDM HTTP config. table :: "

    #@10a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10d
    move-result-object v6

    #@10e
    iget-object v7, p0, Lcom/lge/ims/setting/IMSProvisioning;->xdmHTTPTable:Ljava/lang/String;

    #@110
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@113
    move-result-object v6

    #@114
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@117
    move-result-object v6

    #@118
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@11b
    .line 1306
    :cond_11b
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    #@11e
    goto/16 :goto_38
.end method

.method private removePreference(Ljava/lang/String;)V
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 1703
    invoke-virtual {p0}, Lcom/lge/ims/setting/IMSProvisioning;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    #@3
    move-result-object v1

    #@4
    .line 1705
    .local v1, root:Landroid/preference/PreferenceScreen;
    if-eqz v1, :cond_f

    #@6
    .line 1706
    invoke-virtual {v1, p1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    #@9
    move-result-object v0

    #@a
    .line 1708
    .local v0, preference:Landroid/preference/Preference;
    if-eqz v0, :cond_f

    #@c
    .line 1709
    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    #@f
    .line 1712
    .end local v0           #preference:Landroid/preference/Preference;
    :cond_f
    return-void
.end method

.method private setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V
    .registers 4
    .parameter "edit"

    #@0
    .prologue
    .line 1715
    if-nez p1, :cond_3

    #@2
    .line 1720
    :goto_2
    return-void

    #@3
    .line 1719
    :cond_3
    invoke-virtual {p1}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    #@6
    move-result-object v0

    #@7
    const/4 v1, 0x2

    #@8
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    #@b
    goto :goto_2
.end method

.method private setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V
    .registers 3
    .parameter "preference"
    .parameter "listener"

    #@0
    .prologue
    .line 1723
    if-eqz p1, :cond_5

    #@2
    .line 1724
    invoke-virtual {p1, p2}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@5
    .line 1726
    :cond_5
    return-void
.end method

.method private setSummary(Landroid/preference/Preference;Ljava/lang/Object;)V
    .registers 7
    .parameter "preference"
    .parameter "newValue"

    #@0
    .prologue
    .line 1816
    if-eqz p1, :cond_4

    #@2
    if-nez p2, :cond_5

    #@4
    .line 1833
    :cond_4
    :goto_4
    return-void

    #@5
    .line 1820
    :cond_5
    instance-of v3, p1, Landroid/preference/CheckBoxPreference;

    #@7
    if-nez v3, :cond_4

    #@9
    .line 1822
    instance-of v3, p1, Landroid/preference/EditTextPreference;

    #@b
    if-eqz v3, :cond_15

    #@d
    .line 1823
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {p1, v3}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    #@14
    goto :goto_4

    #@15
    .line 1824
    :cond_15
    instance-of v3, p1, Landroid/preference/ListPreference;

    #@17
    if-eqz v3, :cond_4

    #@19
    move-object v1, p1

    #@1a
    .line 1825
    check-cast v1, Landroid/preference/ListPreference;

    #@1c
    .line 1826
    .local v1, list:Landroid/preference/ListPreference;
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v1, v3}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    #@23
    move-result v2

    #@24
    .line 1828
    .local v2, valueIndex:I
    if-ltz v2, :cond_4

    #@26
    .line 1829
    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    #@29
    move-result-object v0

    #@2a
    .line 1830
    .local v0, entries:[Ljava/lang/CharSequence;
    aget-object v3, v0, v2

    #@2c
    invoke-virtual {v1, v3}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    #@2f
    goto :goto_4
.end method

.method private setValue(Landroid/database/Cursor;ILandroid/preference/CheckBoxPreference;)V
    .registers 7
    .parameter "cursor"
    .parameter "index"
    .parameter "checkbox"

    #@0
    .prologue
    .line 1729
    const/4 v1, -0x1

    #@1
    if-ne p2, v1, :cond_b

    #@3
    .line 1730
    const-string v1, "LGIMSProvisioning"

    #@5
    const-string v2, "index is (-1)"

    #@7
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 1756
    :cond_a
    :goto_a
    return-void

    #@b
    .line 1734
    :cond_b
    if-nez p1, :cond_15

    #@d
    .line 1735
    const-string v1, "LGIMSProvisioning"

    #@f
    const-string v2, "Cursor is null"

    #@11
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    goto :goto_a

    #@15
    .line 1739
    :cond_15
    if-nez p3, :cond_1f

    #@17
    .line 1740
    const-string v1, "LGIMSProvisioning"

    #@19
    const-string v2, "CheckBoxPreference is null"

    #@1b
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    goto :goto_a

    #@1f
    .line 1744
    :cond_1f
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    .line 1746
    .local v0, value:Ljava/lang/String;
    if-eqz v0, :cond_a

    #@25
    .line 1750
    const-string v1, "true"

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@2a
    move-result v1

    #@2b
    if-eqz v1, :cond_32

    #@2d
    .line 1751
    const/4 v1, 0x1

    #@2e
    invoke-virtual {p3, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    #@31
    goto :goto_a

    #@32
    .line 1754
    :cond_32
    const/4 v1, 0x0

    #@33
    invoke-virtual {p3, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    #@36
    goto :goto_a
.end method

.method private setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V
    .registers 7
    .parameter "cursor"
    .parameter "index"
    .parameter "edit"

    #@0
    .prologue
    .line 1759
    const/4 v1, -0x1

    #@1
    if-ne p2, v1, :cond_4

    #@3
    .line 1781
    :cond_3
    :goto_3
    return-void

    #@4
    .line 1763
    :cond_4
    if-nez p1, :cond_e

    #@6
    .line 1764
    const-string v1, "LGIMSProvisioning"

    #@8
    const-string v2, "Cursor is null"

    #@a
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    goto :goto_3

    #@e
    .line 1768
    :cond_e
    if-nez p3, :cond_18

    #@10
    .line 1769
    const-string v1, "LGIMSProvisioning"

    #@12
    const-string v2, "EditTextPreference is null"

    #@14
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    goto :goto_3

    #@18
    .line 1773
    :cond_18
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    .line 1775
    .local v0, value:Ljava/lang/String;
    if-eqz v0, :cond_3

    #@1e
    .line 1779
    invoke-virtual {p3, v0}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    #@21
    .line 1780
    invoke-virtual {p3, v0}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    #@24
    goto :goto_3
.end method

.method private setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V
    .registers 9
    .parameter "cursor"
    .parameter "index"
    .parameter "list"

    #@0
    .prologue
    .line 1784
    const/4 v2, -0x1

    #@1
    if-ne p2, v2, :cond_4

    #@3
    .line 1813
    :cond_3
    :goto_3
    return-void

    #@4
    .line 1788
    :cond_4
    if-nez p1, :cond_e

    #@6
    .line 1789
    const-string v2, "LGIMSProvisioning"

    #@8
    const-string v3, "Cursor is null"

    #@a
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    goto :goto_3

    #@e
    .line 1793
    :cond_e
    if-nez p3, :cond_18

    #@10
    .line 1794
    const-string v2, "LGIMSProvisioning"

    #@12
    const-string v3, "ListPreference is null"

    #@14
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    goto :goto_3

    #@18
    .line 1798
    :cond_18
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    .line 1800
    .local v0, value:Ljava/lang/String;
    if-eqz v0, :cond_3

    #@1e
    .line 1804
    invoke-virtual {p3, v0}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    #@21
    move-result v1

    #@22
    .line 1806
    .local v1, valueIndex:I
    if-gez v1, :cond_4b

    #@24
    .line 1807
    const-string v2, "LGIMSProvisioning"

    #@26
    new-instance v3, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v4, "setValue :: list - key="

    #@2d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    invoke-virtual {p3}, Landroid/preference/ListPreference;->getKey()Ljava/lang/String;

    #@34
    move-result-object v4

    #@35
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v3

    #@39
    const-string v4, ", value="

    #@3b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v3

    #@3f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v3

    #@43
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v3

    #@47
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    goto :goto_3

    #@4b
    .line 1811
    :cond_4b
    invoke-virtual {p3, v1}, Landroid/preference/ListPreference;->setValueIndex(I)V

    #@4e
    .line 1812
    invoke-virtual {p3}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    #@51
    move-result-object v2

    #@52
    invoke-virtual {p3, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    #@55
    goto :goto_3
.end method

.method private updateAllDBItems(Ljava/lang/String;)V
    .registers 5
    .parameter "value"

    #@0
    .prologue
    .line 1986
    const-string v0, "LGIMSProvisioning"

    #@2
    const-string v1, "updateAllDBItems for TEST ONLY"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1988
    const-string v0, "kt:"

    #@9
    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_18

    #@f
    .line 1989
    const/4 v0, 0x3

    #@10
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@13
    move-result-object v0

    #@14
    invoke-direct {p0, v0}, Lcom/lge/ims/setting/IMSProvisioning;->updateAllDBItemsForKT(Ljava/lang/String;)V

    #@17
    .line 1995
    :goto_17
    return-void

    #@18
    .line 1990
    :cond_18
    const-string v0, "lgu:"

    #@1a
    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@1d
    move-result v0

    #@1e
    if-eqz v0, :cond_29

    #@20
    .line 1991
    const/4 v0, 0x4

    #@21
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@24
    move-result-object v0

    #@25
    invoke-direct {p0, v0}, Lcom/lge/ims/setting/IMSProvisioning;->updateAllDBItemsForLGU(Ljava/lang/String;)V

    #@28
    goto :goto_17

    #@29
    .line 1993
    :cond_29
    const-string v0, "LGIMSProvisioning"

    #@2b
    new-instance v1, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v2, "Invalid scheme :: "

    #@32
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v1

    #@36
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v1

    #@3a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v1

    #@3e
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    goto :goto_17
.end method

.method private updateAllDBItemsForKT(Ljava/lang/String;)V
    .registers 10
    .parameter "value"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 1936
    const/4 v0, 0x0

    #@2
    .line 1937
    .local v0, affectedRows:I
    new-instance v3, Landroid/content/ContentValues;

    #@4
    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    #@7
    .line 1939
    .local v3, valuesForSubscriber:Landroid/content/ContentValues;
    const v4, 0x7f06011a

    #@a
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@d
    move-result-object v4

    #@e
    const-string v5, "211.115.7.244"

    #@10
    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@13
    .line 1940
    const v4, 0x7f06011d

    #@16
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@19
    move-result-object v4

    #@1a
    const-string v5, "5080"

    #@1c
    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1f
    .line 1941
    const v4, 0x7f06011b

    #@22
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@25
    move-result-object v4

    #@26
    const-string v5, "211.115.15.199"

    #@28
    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2b
    .line 1942
    const v4, 0x7f06011e

    #@2e
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@31
    move-result-object v4

    #@32
    const-string v5, "5060"

    #@34
    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@37
    .line 1943
    const v4, 0x7f060121

    #@3a
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@3d
    move-result-object v4

    #@3e
    const-string v5, "ims.kt.com"

    #@40
    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@43
    .line 1944
    const v4, 0x7f060122

    #@46
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@49
    move-result-object v4

    #@4a
    new-instance v5, Ljava/lang/StringBuilder;

    #@4c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@4f
    invoke-virtual {p1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@52
    move-result-object v6

    #@53
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v5

    #@57
    const-string v6, "@ims.kt.com"

    #@59
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v5

    #@5d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v5

    #@61
    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@64
    .line 1945
    const v4, 0x7f060123

    #@67
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@6a
    move-result-object v4

    #@6b
    new-instance v5, Ljava/lang/StringBuilder;

    #@6d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@70
    const-string v6, "sip:"

    #@72
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v5

    #@76
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v5

    #@7a
    const-string v6, "@ims.kt.com"

    #@7c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v5

    #@80
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@83
    move-result-object v5

    #@84
    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@87
    .line 1946
    const-string v4, "subscriber_0_impu_0"

    #@89
    new-instance v5, Ljava/lang/StringBuilder;

    #@8b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@8e
    const-string v6, "sip:"

    #@90
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v5

    #@94
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v5

    #@98
    const-string v6, "@ims.kt.com"

    #@9a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v5

    #@9e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a1
    move-result-object v5

    #@a2
    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@a5
    .line 1947
    const v4, 0x7f060129

    #@a8
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@ab
    move-result-object v4

    #@ac
    new-instance v5, Ljava/lang/StringBuilder;

    #@ae
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@b1
    invoke-virtual {p1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@b4
    move-result-object v6

    #@b5
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v5

    #@b9
    const-string v6, "@ims.kt.com"

    #@bb
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v5

    #@bf
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c2
    move-result-object v5

    #@c3
    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@c6
    .line 1948
    const v4, 0x7f060128

    #@c9
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@cc
    move-result-object v4

    #@cd
    const-string v5, "ims.kt.com"

    #@cf
    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@d2
    .line 1949
    const v4, 0x7f06012a

    #@d5
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@d8
    move-result-object v4

    #@d9
    const-string v5, "ims.kt.com"

    #@db
    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@de
    .line 1951
    const v4, 0x7f06012e

    #@e1
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@e4
    move-result-object v4

    #@e5
    const-string v5, "ims.kt.com"

    #@e7
    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@ea
    .line 1953
    iget-object v4, p0, Lcom/lge/ims/setting/IMSProvisioning;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@ec
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    #@ef
    .line 1956
    :try_start_ef
    iget-object v4, p0, Lcom/lge/ims/setting/IMSProvisioning;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@f1
    const-string v5, "lgims_subscriber"

    #@f3
    const-string v6, "id = \'1\'"

    #@f5
    const/4 v7, 0x0

    #@f6
    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@f9
    move-result v0

    #@fa
    .line 1957
    iget-object v4, p0, Lcom/lge/ims/setting/IMSProvisioning;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@fc
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_ff
    .catchall {:try_start_ef .. :try_end_ff} :catchall_16a
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_ef .. :try_end_ff} :catch_160

    #@ff
    .line 1961
    iget-object v4, p0, Lcom/lge/ims/setting/IMSProvisioning;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@101
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@104
    .line 1964
    :goto_104
    const-string v4, "LGIMSProvisioning"

    #@106
    new-instance v5, Ljava/lang/StringBuilder;

    #@108
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@10b
    const-string v6, "Subscriber :: updated row count: "

    #@10d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@110
    move-result-object v5

    #@111
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@114
    move-result-object v5

    #@115
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@118
    move-result-object v5

    #@119
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@11c
    .line 1967
    new-instance v2, Landroid/content/ContentValues;

    #@11e
    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    #@121
    .line 1969
    .local v2, valuesForSIP:Landroid/content/ContentValues;
    const v4, 0x7f06014c

    #@124
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@127
    move-result-object v4

    #@128
    const-string v5, "KT-client/RCS-e1.2"

    #@12a
    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@12d
    .line 1971
    iget-object v4, p0, Lcom/lge/ims/setting/IMSProvisioning;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@12f
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    #@132
    .line 1974
    :try_start_132
    iget-object v4, p0, Lcom/lge/ims/setting/IMSProvisioning;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@134
    const-string v5, "lgims_com_kt_sip"

    #@136
    const-string v6, "id = \'1\'"

    #@138
    const/4 v7, 0x0

    #@139
    invoke-virtual {v4, v5, v2, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@13c
    move-result v0

    #@13d
    .line 1975
    iget-object v4, p0, Lcom/lge/ims/setting/IMSProvisioning;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@13f
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_142
    .catchall {:try_start_132 .. :try_end_142} :catchall_17b
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_132 .. :try_end_142} :catch_171

    #@142
    .line 1979
    iget-object v4, p0, Lcom/lge/ims/setting/IMSProvisioning;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@144
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@147
    .line 1982
    :goto_147
    const-string v4, "LGIMSProvisioning"

    #@149
    new-instance v5, Ljava/lang/StringBuilder;

    #@14b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@14e
    const-string v6, "SIP :: updated row count: "

    #@150
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@153
    move-result-object v5

    #@154
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@157
    move-result-object v5

    #@158
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15b
    move-result-object v5

    #@15c
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@15f
    .line 1983
    return-void

    #@160
    .line 1958
    .end local v2           #valuesForSIP:Landroid/content/ContentValues;
    :catch_160
    move-exception v1

    #@161
    .line 1959
    .local v1, e:Landroid/database/sqlite/SQLiteException;
    :try_start_161
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_164
    .catchall {:try_start_161 .. :try_end_164} :catchall_16a

    #@164
    .line 1961
    iget-object v4, p0, Lcom/lge/ims/setting/IMSProvisioning;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@166
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@169
    goto :goto_104

    #@16a
    .end local v1           #e:Landroid/database/sqlite/SQLiteException;
    :catchall_16a
    move-exception v4

    #@16b
    iget-object v5, p0, Lcom/lge/ims/setting/IMSProvisioning;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@16d
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@170
    throw v4

    #@171
    .line 1976
    .restart local v2       #valuesForSIP:Landroid/content/ContentValues;
    :catch_171
    move-exception v1

    #@172
    .line 1977
    .restart local v1       #e:Landroid/database/sqlite/SQLiteException;
    :try_start_172
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_175
    .catchall {:try_start_172 .. :try_end_175} :catchall_17b

    #@175
    .line 1979
    iget-object v4, p0, Lcom/lge/ims/setting/IMSProvisioning;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@177
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@17a
    goto :goto_147

    #@17b
    .end local v1           #e:Landroid/database/sqlite/SQLiteException;
    :catchall_17b
    move-exception v4

    #@17c
    iget-object v5, p0, Lcom/lge/ims/setting/IMSProvisioning;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@17e
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@181
    throw v4
.end method

.method private updateAllDBItemsForLGU(Ljava/lang/String;)V
    .registers 10
    .parameter "value"

    #@0
    .prologue
    .line 1886
    const/4 v0, 0x0

    #@1
    .line 1887
    .local v0, affectedRows:I
    new-instance v3, Landroid/content/ContentValues;

    #@3
    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    #@6
    .line 1889
    .local v3, valuesForSubscriber:Landroid/content/ContentValues;
    const v4, 0x7f06011a

    #@9
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@c
    move-result-object v4

    #@d
    const-string v5, "180.210.196.248"

    #@f
    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@12
    .line 1890
    const v4, 0x7f06011d

    #@15
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@18
    move-result-object v4

    #@19
    const-string v5, "5060"

    #@1b
    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1e
    .line 1891
    const v4, 0x7f06011b

    #@21
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@24
    move-result-object v4

    #@25
    const-string v5, "10.160.254.6"

    #@27
    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2a
    .line 1892
    const v4, 0x7f06011e

    #@2d
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@30
    move-result-object v4

    #@31
    const-string v5, "5060"

    #@33
    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@36
    .line 1893
    const v4, 0x7f060121

    #@39
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@3c
    move-result-object v4

    #@3d
    const-string v5, "lte-lguplus.co.kr"

    #@3f
    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@42
    .line 1894
    const v4, 0x7f060122

    #@45
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@48
    move-result-object v4

    #@49
    new-instance v5, Ljava/lang/StringBuilder;

    #@4b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@4e
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v5

    #@52
    const-string v6, "@lte-lguplus.co.kr"

    #@54
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v5

    #@58
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v5

    #@5c
    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@5f
    .line 1895
    const v4, 0x7f060123

    #@62
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@65
    move-result-object v4

    #@66
    new-instance v5, Ljava/lang/StringBuilder;

    #@68
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@6b
    const-string v6, "sip:"

    #@6d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v5

    #@71
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v5

    #@75
    const-string v6, "@lte-lguplus.co.kr"

    #@77
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v5

    #@7b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e
    move-result-object v5

    #@7f
    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@82
    .line 1896
    const v4, 0x7f060128

    #@85
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@88
    move-result-object v4

    #@89
    const-string v5, "lte-lguplus.co.kr"

    #@8b
    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@8e
    .line 1897
    const v4, 0x7f060129

    #@91
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@94
    move-result-object v4

    #@95
    new-instance v5, Ljava/lang/StringBuilder;

    #@97
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@9a
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v5

    #@9e
    const-string v6, "@lte-lguplus.co.kr"

    #@a0
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v5

    #@a4
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a7
    move-result-object v5

    #@a8
    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@ab
    .line 1898
    const v4, 0x7f06012a

    #@ae
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@b1
    move-result-object v4

    #@b2
    const-string v5, "lte-lguplus.co.kr"

    #@b4
    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@b7
    .line 1899
    const v4, 0x7f06012b

    #@ba
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@bd
    move-result-object v4

    #@be
    invoke-virtual {v3, v4, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@c1
    .line 1900
    const v4, 0x7f06012e

    #@c4
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@c7
    move-result-object v4

    #@c8
    const-string v5, "lte-lguplus.co.kr"

    #@ca
    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@cd
    .line 1902
    iget-object v4, p0, Lcom/lge/ims/setting/IMSProvisioning;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@cf
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    #@d2
    .line 1905
    :try_start_d2
    iget-object v4, p0, Lcom/lge/ims/setting/IMSProvisioning;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@d4
    const-string v5, "lgims_subscriber"

    #@d6
    const-string v6, "id = \'1\'"

    #@d8
    const/4 v7, 0x0

    #@d9
    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@dc
    move-result v0

    #@dd
    .line 1906
    iget-object v4, p0, Lcom/lge/ims/setting/IMSProvisioning;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@df
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_e2
    .catchall {:try_start_d2 .. :try_end_e2} :catchall_14d
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_d2 .. :try_end_e2} :catch_143

    #@e2
    .line 1910
    iget-object v4, p0, Lcom/lge/ims/setting/IMSProvisioning;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@e4
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@e7
    .line 1913
    :goto_e7
    const-string v4, "LGIMSProvisioning"

    #@e9
    new-instance v5, Ljava/lang/StringBuilder;

    #@eb
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@ee
    const-string v6, "Subscriber :: updated row count: "

    #@f0
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v5

    #@f4
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f7
    move-result-object v5

    #@f8
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fb
    move-result-object v5

    #@fc
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@ff
    .line 1916
    new-instance v2, Landroid/content/ContentValues;

    #@101
    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    #@104
    .line 1918
    .local v2, valuesForSIP:Landroid/content/ContentValues;
    const v4, 0x7f06014c

    #@107
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@10a
    move-result-object v4

    #@10b
    const-string v5, "LGUPlus-client/RCSe;Device_Type=Android_Phone"

    #@10d
    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@110
    .line 1920
    iget-object v4, p0, Lcom/lge/ims/setting/IMSProvisioning;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@112
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    #@115
    .line 1923
    :try_start_115
    iget-object v4, p0, Lcom/lge/ims/setting/IMSProvisioning;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@117
    const-string v5, "lgims_com_kt_sip"

    #@119
    const-string v6, "id = \'1\'"

    #@11b
    const/4 v7, 0x0

    #@11c
    invoke-virtual {v4, v5, v2, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@11f
    move-result v0

    #@120
    .line 1924
    iget-object v4, p0, Lcom/lge/ims/setting/IMSProvisioning;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@122
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_125
    .catchall {:try_start_115 .. :try_end_125} :catchall_15e
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_115 .. :try_end_125} :catch_154

    #@125
    .line 1928
    iget-object v4, p0, Lcom/lge/ims/setting/IMSProvisioning;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@127
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@12a
    .line 1931
    :goto_12a
    const-string v4, "LGIMSProvisioning"

    #@12c
    new-instance v5, Ljava/lang/StringBuilder;

    #@12e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@131
    const-string v6, "SIP :: updated row count: "

    #@133
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@136
    move-result-object v5

    #@137
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13a
    move-result-object v5

    #@13b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13e
    move-result-object v5

    #@13f
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@142
    .line 1932
    return-void

    #@143
    .line 1907
    .end local v2           #valuesForSIP:Landroid/content/ContentValues;
    :catch_143
    move-exception v1

    #@144
    .line 1908
    .local v1, e:Landroid/database/sqlite/SQLiteException;
    :try_start_144
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_147
    .catchall {:try_start_144 .. :try_end_147} :catchall_14d

    #@147
    .line 1910
    iget-object v4, p0, Lcom/lge/ims/setting/IMSProvisioning;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@149
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@14c
    goto :goto_e7

    #@14d
    .end local v1           #e:Landroid/database/sqlite/SQLiteException;
    :catchall_14d
    move-exception v4

    #@14e
    iget-object v5, p0, Lcom/lge/ims/setting/IMSProvisioning;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@150
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@153
    throw v4

    #@154
    .line 1925
    .restart local v2       #valuesForSIP:Landroid/content/ContentValues;
    :catch_154
    move-exception v1

    #@155
    .line 1926
    .restart local v1       #e:Landroid/database/sqlite/SQLiteException;
    :try_start_155
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_158
    .catchall {:try_start_155 .. :try_end_158} :catchall_15e

    #@158
    .line 1928
    iget-object v4, p0, Lcom/lge/ims/setting/IMSProvisioning;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@15a
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@15d
    goto :goto_12a

    #@15e
    .end local v1           #e:Landroid/database/sqlite/SQLiteException;
    :catchall_15e
    move-exception v4

    #@15f
    iget-object v5, p0, Lcom/lge/ims/setting/IMSProvisioning;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@161
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@164
    throw v4
.end method

.method private updateIPAddress()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 1836
    iget-object v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->mIPAddress:Landroid/preference/Preference;

    #@3
    if-nez v3, :cond_6

    #@5
    .line 1867
    :cond_5
    :goto_5
    return-void

    #@6
    .line 1840
    :cond_6
    invoke-static {}, Lcom/lge/ims/DataConnectionManager;->getInstance()Lcom/lge/ims/DataConnectionManager;

    #@9
    move-result-object v0

    #@a
    .line 1842
    .local v0, dcm:Lcom/lge/ims/DataConnectionManager;
    if-eqz v0, :cond_5

    #@c
    .line 1843
    const-string v2, "0.0.0.0"

    #@e
    .line 1845
    .local v2, summary:Ljava/lang/String;
    invoke-virtual {v0, v4}, Lcom/lge/ims/DataConnectionManager;->isConnected(I)Z

    #@11
    move-result v3

    #@12
    if-eqz v3, :cond_1d

    #@14
    .line 1846
    const/4 v3, 0x0

    #@15
    invoke-virtual {v0, v4, v3}, Lcom/lge/ims/DataConnectionManager;->getLocalAddress(II)Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    .line 1848
    if-nez v2, :cond_1d

    #@1b
    .line 1849
    const-string v2, "0.0.0.0"

    #@1d
    .line 1853
    :cond_1d
    invoke-static {}, Lcom/lge/ims/PhoneStateTracker;->getInstance()Lcom/lge/ims/PhoneStateTracker;

    #@20
    move-result-object v1

    #@21
    .line 1855
    .local v1, pst:Lcom/lge/ims/PhoneStateTracker;
    if-eqz v1, :cond_2f

    #@23
    .line 1856
    invoke-virtual {v1}, Lcom/lge/ims/PhoneStateTracker;->is3G()Z

    #@26
    move-result v3

    #@27
    if-eqz v3, :cond_35

    #@29
    .line 1857
    const-string v3, " (3G)"

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@2e
    move-result-object v2

    #@2f
    .line 1865
    :cond_2f
    :goto_2f
    iget-object v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->mIPAddress:Landroid/preference/Preference;

    #@31
    invoke-virtual {v3, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    #@34
    goto :goto_5

    #@35
    .line 1858
    :cond_35
    invoke-virtual {v1}, Lcom/lge/ims/PhoneStateTracker;->is4G()Z

    #@38
    move-result v3

    #@39
    if-eqz v3, :cond_42

    #@3b
    .line 1859
    const-string v3, " (LTE)"

    #@3d
    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@40
    move-result-object v2

    #@41
    goto :goto_2f

    #@42
    .line 1861
    :cond_42
    const-string v3, " (Unknown)"

    #@44
    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@47
    move-result-object v2

    #@48
    goto :goto_2f
.end method

.method private updateRegistrationStatus()V
    .registers 4

    #@0
    .prologue
    .line 1870
    iget-object v1, p0, Lcom/lge/ims/setting/IMSProvisioning;->mImsRegistrationStatus:Landroid/preference/Preference;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 1881
    :goto_4
    return-void

    #@5
    .line 1874
    :cond_5
    const-string v1, "net.ims.reg"

    #@7
    const-string v2, "0"

    #@9
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    .line 1876
    .local v0, reg:Ljava/lang/String;
    const-string v1, "1"

    #@f
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v1

    #@13
    if-eqz v1, :cond_1e

    #@15
    .line 1877
    iget-object v1, p0, Lcom/lge/ims/setting/IMSProvisioning;->mImsRegistrationStatus:Landroid/preference/Preference;

    #@17
    const v2, 0x7f060393

    #@1a
    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(I)V

    #@1d
    goto :goto_4

    #@1e
    .line 1879
    :cond_1e
    iget-object v1, p0, Lcom/lge/ims/setting/IMSProvisioning;->mImsRegistrationStatus:Landroid/preference/Preference;

    #@20
    const v2, 0x7f060394

    #@23
    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(I)V

    #@26
    goto :goto_4
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .registers 9
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 109
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    #@3
    .line 111
    const v3, 0x7f040004

    #@6
    invoke-virtual {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->addPreferencesFromResource(I)V

    #@9
    .line 113
    invoke-virtual {p0}, Lcom/lge/ims/setting/IMSProvisioning;->getWindow()Landroid/view/Window;

    #@c
    move-result-object v2

    #@d
    .line 114
    .local v2, wd:Landroid/view/Window;
    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@10
    move-result-object v1

    #@11
    .line 115
    .local v1, layoutParams:Landroid/view/WindowManager$LayoutParams;
    iget v3, v1, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@13
    or-int/lit16 v3, v3, 0x100

    #@15
    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@17
    .line 116
    invoke-virtual {v2, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    #@1a
    .line 118
    iget-boolean v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->voipVisible:Z

    #@1c
    if-nez v3, :cond_28

    #@1e
    .line 119
    const v3, 0x7f060346

    #@21
    invoke-virtual {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->removePreference(Ljava/lang/String;)V

    #@28
    .line 123
    :cond_28
    iget-object v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@2a
    if-nez v3, :cond_36

    #@2c
    .line 125
    :try_start_2c
    const-string v3, "/data/data/com.lge.ims/databases/lgims.db"

    #@2e
    const/4 v4, 0x0

    #@2f
    const/4 v5, 0x0

    #@30
    invoke-static {v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->openDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)Landroid/database/sqlite/SQLiteDatabase;

    #@33
    move-result-object v3

    #@34
    iput-object v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->imsDB:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_36
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2c .. :try_end_36} :catch_fb

    #@36
    .line 132
    :cond_36
    new-instance v3, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;

    #@38
    const-string v4, "lgims_subscriber"

    #@3a
    invoke-direct {v3, p0, v4}, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;-><init>(Lcom/lge/ims/setting/IMSProvisioning;Ljava/lang/String;)V

    #@3d
    iput-object v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->onSubscriberListener:Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;

    #@3f
    .line 133
    new-instance v3, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;

    #@41
    const-string v4, "lgims_aosconnection"

    #@43
    invoke-direct {v3, p0, v4}, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;-><init>(Lcom/lge/ims/setting/IMSProvisioning;Ljava/lang/String;)V

    #@46
    iput-object v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->onAoSConnectionListener:Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;

    #@48
    .line 134
    new-instance v3, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;

    #@4a
    const-string v4, "lgims_aosreg"

    #@4c
    invoke-direct {v3, p0, v4}, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;-><init>(Lcom/lge/ims/setting/IMSProvisioning;Ljava/lang/String;)V

    #@4f
    iput-object v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->onAoSRegListener:Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;

    #@51
    .line 135
    new-instance v3, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;

    #@53
    const-string v4, "lgims_sip"

    #@55
    invoke-direct {v3, p0, v4}, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;-><init>(Lcom/lge/ims/setting/IMSProvisioning;Ljava/lang/String;)V

    #@58
    iput-object v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->onSIPListener:Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;

    #@5a
    .line 136
    new-instance v3, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;

    #@5c
    const-string v4, "lgims_com_kt_sip"

    #@5e
    invoke-direct {v3, p0, v4}, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;-><init>(Lcom/lge/ims/setting/IMSProvisioning;Ljava/lang/String;)V

    #@61
    iput-object v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->onComSIPListener:Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;

    #@63
    .line 138
    new-instance v3, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@65
    const-string v4, "vt_"

    #@67
    const-string v5, "lgims_com_kt_service_vt"

    #@69
    const-string v6, "VT::Session"

    #@6b
    invoke-direct {v3, p0, v4, v5, v6}, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;-><init>(Lcom/lge/ims/setting/IMSProvisioning;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@6e
    iput-object v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->onVTSessionListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@70
    .line 139
    new-instance v3, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@72
    const-string v4, "vt_"

    #@74
    const-string v5, "lgims_com_kt_sip_vt"

    #@76
    const-string v6, "VT::SIP"

    #@78
    invoke-direct {v3, p0, v4, v5, v6}, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;-><init>(Lcom/lge/ims/setting/IMSProvisioning;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@7b
    iput-object v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->onVTSIPListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@7d
    .line 144
    iget-object v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->onSubscriberListener:Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;

    #@7f
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->registerSubscriberChangeListener(Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;)V

    #@82
    .line 149
    iget-object v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->onAoSConnectionListener:Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;

    #@84
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->registerAoSConnectionChangeListener(Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;)V

    #@87
    .line 150
    iget-object v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->onAoSRegListener:Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;

    #@89
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->registerAoSRegChangeListener(Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;)V

    #@8c
    .line 155
    iget-object v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->onSIPListener:Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;

    #@8e
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->registerSIPChangeListener(Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;)V

    #@91
    .line 156
    iget-object v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->onComSIPListener:Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;

    #@93
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->registerComSIPChangeListener(Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;)V

    #@96
    .line 161
    iget-object v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->onVTSessionListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@98
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->registerSessionChangeListener(Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;)V

    #@9b
    .line 162
    iget-object v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->onVTSIPListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@9d
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->registerEnablerSIPChangeListener(Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;)V

    #@a0
    .line 164
    iget-boolean v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->voipVisible:Z

    #@a2
    if-eqz v3, :cond_b6

    #@a4
    .line 166
    new-instance v3, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@a6
    const-string v4, "voip_"

    #@a8
    const-string v5, "lgims_com_kt_service_uc"

    #@aa
    const-string v6, "VoIP::Session"

    #@ac
    invoke-direct {v3, p0, v4, v5, v6}, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;-><init>(Lcom/lge/ims/setting/IMSProvisioning;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@af
    iput-object v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->onVoIPSessionListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@b1
    .line 169
    iget-object v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->onVoIPSessionListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@b3
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->registerSessionChangeListener(Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;)V

    #@b6
    .line 173
    :cond_b6
    invoke-static {}, Lcom/lge/ims/Configuration;->isACOn()Z

    #@b9
    move-result v3

    #@ba
    if-nez v3, :cond_c6

    #@bc
    .line 174
    const v3, 0x7f06012f

    #@bf
    invoke-virtual {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@c2
    move-result-object v3

    #@c3
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->removePreference(Ljava/lang/String;)V

    #@c6
    .line 177
    :cond_c6
    invoke-static {}, Lcom/lge/ims/Configuration;->isRCSeOn()Z

    #@c9
    move-result v3

    #@ca
    if-nez v3, :cond_100

    #@cc
    .line 178
    const v3, 0x7f0602e8

    #@cf
    invoke-virtual {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@d2
    move-result-object v3

    #@d3
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->removePreference(Ljava/lang/String;)V

    #@d6
    .line 179
    const v3, 0x7f06015a

    #@d9
    invoke-virtual {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@dc
    move-result-object v3

    #@dd
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->removePreference(Ljava/lang/String;)V

    #@e0
    .line 213
    :goto_e0
    const v3, 0x7f060011

    #@e3
    invoke-virtual {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@e6
    move-result-object v3

    #@e7
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->getPreference(Ljava/lang/String;)Landroid/preference/Preference;

    #@ea
    move-result-object v3

    #@eb
    iput-object v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->mIPAddress:Landroid/preference/Preference;

    #@ed
    .line 214
    const v3, 0x7f060010

    #@f0
    invoke-virtual {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@f3
    move-result-object v3

    #@f4
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->getPreference(Ljava/lang/String;)Landroid/preference/Preference;

    #@f7
    move-result-object v3

    #@f8
    iput-object v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->mImsRegistrationStatus:Landroid/preference/Preference;

    #@fa
    .line 215
    :goto_fa
    return-void

    #@fb
    .line 126
    :catch_fb
    move-exception v0

    #@fc
    .line 127
    .local v0, e:Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    #@ff
    goto :goto_fa

    #@100
    .line 182
    .end local v0           #e:Landroid/database/sqlite/SQLiteException;
    :cond_100
    new-instance v3, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@102
    const-string v4, "im_"

    #@104
    const-string v5, "lgims_com_kt_service_rcse_im"

    #@106
    const-string v6, "IM::Application"

    #@108
    invoke-direct {v3, p0, v4, v5, v6}, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;-><init>(Lcom/lge/ims/setting/IMSProvisioning;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@10b
    iput-object v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->onIMApplicationListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@10d
    .line 183
    iget-object v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->onIMApplicationListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@10f
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->registerIMApplicationChangeListener(Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;)V

    #@112
    .line 185
    new-instance v3, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@114
    const-string v4, "im_"

    #@116
    iget-object v5, p0, Lcom/lge/ims/setting/IMSProvisioning;->imHTTPTable:Ljava/lang/String;

    #@118
    const-string v6, "IM::HTTP"

    #@11a
    invoke-direct {v3, p0, v4, v5, v6}, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;-><init>(Lcom/lge/ims/setting/IMSProvisioning;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@11d
    iput-object v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->onIMHTTPListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@11f
    .line 186
    iget-object v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->onIMHTTPListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@121
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->registerHTTPChangeListener(Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;)V

    #@124
    .line 189
    new-instance v3, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@126
    const-string v4, "ip_"

    #@128
    const-string v5, "lgims_com_kt_service_rcse_ip"

    #@12a
    const-string v6, "IP::Application"

    #@12c
    invoke-direct {v3, p0, v4, v5, v6}, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;-><init>(Lcom/lge/ims/setting/IMSProvisioning;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@12f
    iput-object v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->onIPApplicationListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@131
    .line 190
    new-instance v3, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@133
    const-string v4, "ipxdm_"

    #@135
    const-string v5, "lgims_com_kt_service_rcse_ipxdm"

    #@137
    const-string v6, "IPXDM::Application"

    #@139
    invoke-direct {v3, p0, v4, v5, v6}, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;-><init>(Lcom/lge/ims/setting/IMSProvisioning;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@13c
    iput-object v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->onXDMApplicationListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@13e
    .line 191
    new-instance v3, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@140
    const-string v4, "ipxdm_"

    #@142
    const-string v5, "lgims_com_kt_service_rcse_ipxdm"

    #@144
    const-string v6, "IPXDM::Resource"

    #@146
    invoke-direct {v3, p0, v4, v5, v6}, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;-><init>(Lcom/lge/ims/setting/IMSProvisioning;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@149
    iput-object v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->onXDMResourceListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@14b
    .line 193
    iget-object v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->onIPApplicationListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@14d
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->registerIPApplicationChangeListener(Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;)V

    #@150
    .line 194
    iget-object v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->onXDMApplicationListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@152
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->registerXDMApplicationChangeListener(Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;)V

    #@155
    .line 195
    iget-object v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->onXDMResourceListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@157
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->registerXDMResourceChangeListener(Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;)V

    #@15a
    .line 197
    new-instance v3, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@15c
    const-string v4, "ipxdm_"

    #@15e
    iget-object v5, p0, Lcom/lge/ims/setting/IMSProvisioning;->xdmHTTPTable:Ljava/lang/String;

    #@160
    const-string v6, "IPXDM::HTTP"

    #@162
    invoke-direct {v3, p0, v4, v5, v6}, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;-><init>(Lcom/lge/ims/setting/IMSProvisioning;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@165
    iput-object v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->onXDMHTTPListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@167
    .line 198
    iget-object v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->onXDMHTTPListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@169
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->registerHTTPChangeListener(Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;)V

    #@16c
    .line 203
    new-instance v3, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@16e
    const-string v4, "vs_"

    #@170
    const-string v5, "lgims_com_kt_service_rcse_vs"

    #@172
    const-string v6, "VS::Session"

    #@174
    invoke-direct {v3, p0, v4, v5, v6}, Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;-><init>(Lcom/lge/ims/setting/IMSProvisioning;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@177
    iput-object v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->onVSSessionListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@179
    .line 205
    iget-object v3, p0, Lcom/lge/ims/setting/IMSProvisioning;->onVSSessionListener:Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;

    #@17b
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->registerSessionChangeListener(Lcom/lge/ims/setting/IMSProvisioning$EnablerSpecificChangeListener;)V

    #@17e
    .line 210
    new-instance v3, Lcom/lge/ims/setting/IMSProvisioning$TestChangeListener;

    #@180
    invoke-direct {v3, p0}, Lcom/lge/ims/setting/IMSProvisioning$TestChangeListener;-><init>(Lcom/lge/ims/setting/IMSProvisioning;)V

    #@183
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/IMSProvisioning;->registerTestChangeListener(Lcom/lge/ims/setting/IMSProvisioning$TestChangeListener;)V

    #@186
    goto/16 :goto_e0
.end method

.method public onDestroy()V
    .registers 2

    #@0
    .prologue
    .line 219
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    #@3
    .line 221
    iget-object v0, p0, Lcom/lge/ims/setting/IMSProvisioning;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@5
    if-eqz v0, :cond_f

    #@7
    .line 222
    iget-object v0, p0, Lcom/lge/ims/setting/IMSProvisioning;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@9
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    #@c
    .line 223
    const/4 v0, 0x0

    #@d
    iput-object v0, p0, Lcom/lge/ims/setting/IMSProvisioning;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@f
    .line 225
    :cond_f
    return-void
.end method

.method public onPause()V
    .registers 2

    #@0
    .prologue
    .line 229
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPause()V

    #@3
    .line 231
    iget-boolean v0, p0, Lcom/lge/ims/setting/IMSProvisioning;->mAdministrativeConfigChanged:Z

    #@5
    if-eqz v0, :cond_11

    #@7
    .line 232
    invoke-virtual {p0}, Lcom/lge/ims/setting/IMSProvisioning;->getBaseContext()Landroid/content/Context;

    #@a
    move-result-object v0

    #@b
    invoke-static {v0}, Lcom/lge/ims/Configuration;->init(Landroid/content/Context;)V

    #@e
    .line 233
    const/4 v0, 0x0

    #@f
    iput-boolean v0, p0, Lcom/lge/ims/setting/IMSProvisioning;->mAdministrativeConfigChanged:Z

    #@11
    .line 235
    :cond_11
    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .registers 5
    .parameter "preferenceScreen"
    .parameter "preference"

    #@0
    .prologue
    .line 240
    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 242
    .local v0, key:Ljava/lang/String;
    if-nez v0, :cond_b

    #@6
    .line 243
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    #@9
    move-result v1

    #@a
    .line 255
    :goto_a
    return v1

    #@b
    .line 246
    :cond_b
    const v1, 0x7f06000f

    #@e
    invoke-virtual {p0, v1}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v1

    #@16
    if-eqz v1, :cond_23

    #@18
    .line 247
    invoke-direct {p0}, Lcom/lge/ims/setting/IMSProvisioning;->updateIPAddress()V

    #@1b
    .line 248
    invoke-direct {p0}, Lcom/lge/ims/setting/IMSProvisioning;->updateRegistrationStatus()V

    #@1e
    .line 255
    :cond_1e
    :goto_1e
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    #@21
    move-result v1

    #@22
    goto :goto_a

    #@23
    .line 249
    :cond_23
    const v1, 0x7f060011

    #@26
    invoke-virtual {p0, v1}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2d
    move-result v1

    #@2e
    if-eqz v1, :cond_34

    #@30
    .line 250
    invoke-direct {p0}, Lcom/lge/ims/setting/IMSProvisioning;->updateIPAddress()V

    #@33
    goto :goto_1e

    #@34
    .line 251
    :cond_34
    const v1, 0x7f060010

    #@37
    invoke-virtual {p0, v1}, Lcom/lge/ims/setting/IMSProvisioning;->getString(I)Ljava/lang/String;

    #@3a
    move-result-object v1

    #@3b
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3e
    move-result v1

    #@3f
    if-eqz v1, :cond_1e

    #@41
    .line 252
    invoke-direct {p0}, Lcom/lge/ims/setting/IMSProvisioning;->updateRegistrationStatus()V

    #@44
    goto :goto_1e
.end method
