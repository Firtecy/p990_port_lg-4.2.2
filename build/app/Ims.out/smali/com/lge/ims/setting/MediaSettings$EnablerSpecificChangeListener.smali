.class Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;
.super Ljava/lang/Object;
.source "MediaSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/setting/MediaSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EnablerSpecificChangeListener"
.end annotation


# instance fields
.field private TAG:Ljava/lang/String;

.field private keyPrefix:Ljava/lang/String;

.field private tableName:Ljava/lang/String;

.field final synthetic this$0:Lcom/lge/ims/setting/MediaSettings;


# direct methods
.method public constructor <init>(Lcom/lge/ims/setting/MediaSettings;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter "keyPrefix_"
    .parameter "tableName_"
    .parameter "TAG_"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 894
    iput-object p1, p0, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@3
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 890
    iput-object v0, p0, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->keyPrefix:Ljava/lang/String;

    #@8
    .line 891
    iput-object v0, p0, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->tableName:Ljava/lang/String;

    #@a
    .line 892
    iput-object v0, p0, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->TAG:Ljava/lang/String;

    #@c
    .line 895
    iput-object p2, p0, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->keyPrefix:Ljava/lang/String;

    #@e
    .line 896
    iput-object p3, p0, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->tableName:Ljava/lang/String;

    #@10
    .line 897
    iput-object p4, p0, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->TAG:Ljava/lang/String;

    #@12
    .line 898
    return-void
.end method


# virtual methods
.method public getPrefix()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 901
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->keyPrefix:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getTableName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 905
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->tableName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .registers 13
    .parameter "preference"
    .parameter "newValue"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 909
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    #@5
    move-result-object v2

    #@6
    .line 911
    .local v2, key:Ljava/lang/String;
    iget-object v6, p0, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->TAG:Ljava/lang/String;

    #@8
    new-instance v7, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    iget-object v8, p0, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->TAG:Ljava/lang/String;

    #@f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v7

    #@13
    const-string v8, " :: key: "

    #@15
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v7

    #@19
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v7

    #@1d
    const-string v8, " -> value:"

    #@1f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v7

    #@23
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@26
    move-result-object v8

    #@27
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v7

    #@2b
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v7

    #@2f
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@32
    .line 913
    iget-object v6, p0, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@34
    invoke-static {v6}, Lcom/lge/ims/setting/MediaSettings;->access$200(Lcom/lge/ims/setting/MediaSettings;)Landroid/database/sqlite/SQLiteDatabase;

    #@37
    move-result-object v6

    #@38
    if-nez v6, :cond_5b

    #@3a
    .line 914
    iget-object v5, p0, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->TAG:Ljava/lang/String;

    #@3c
    new-instance v6, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v7, "DB table ("

    #@43
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v6

    #@47
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->tableName:Ljava/lang/String;

    #@49
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v6

    #@4d
    const-string v7, ") is null"

    #@4f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v6

    #@53
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v6

    #@57
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@5a
    .line 945
    :goto_5a
    return v4

    #@5b
    .line 918
    :cond_5b
    const/4 v0, 0x0

    #@5c
    .line 919
    .local v0, affectedRows:I
    new-instance v3, Landroid/content/ContentValues;

    #@5e
    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    #@61
    .line 922
    .local v3, values:Landroid/content/ContentValues;
    iget-object v6, p0, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->keyPrefix:Ljava/lang/String;

    #@63
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@66
    move-result v6

    #@67
    invoke-virtual {v2, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@6a
    move-result-object v6

    #@6b
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@6e
    move-result-object v7

    #@6f
    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@72
    .line 924
    iget-object v6, p0, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@74
    invoke-static {v6}, Lcom/lge/ims/setting/MediaSettings;->access$200(Lcom/lge/ims/setting/MediaSettings;)Landroid/database/sqlite/SQLiteDatabase;

    #@77
    move-result-object v6

    #@78
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    #@7b
    .line 927
    :try_start_7b
    iget-object v6, p0, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@7d
    invoke-static {v6}, Lcom/lge/ims/setting/MediaSettings;->access$200(Lcom/lge/ims/setting/MediaSettings;)Landroid/database/sqlite/SQLiteDatabase;

    #@80
    move-result-object v6

    #@81
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->tableName:Ljava/lang/String;

    #@83
    const-string v8, "id = \'1\'"

    #@85
    const/4 v9, 0x0

    #@86
    invoke-virtual {v6, v7, v3, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@89
    move-result v0

    #@8a
    .line 928
    iget-object v6, p0, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@8c
    invoke-static {v6}, Lcom/lge/ims/setting/MediaSettings;->access$200(Lcom/lge/ims/setting/MediaSettings;)Landroid/database/sqlite/SQLiteDatabase;

    #@8f
    move-result-object v6

    #@90
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_93
    .catchall {:try_start_7b .. :try_end_93} :catchall_d2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7b .. :try_end_93} :catch_c4

    #@93
    .line 932
    iget-object v6, p0, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@95
    invoke-static {v6}, Lcom/lge/ims/setting/MediaSettings;->access$200(Lcom/lge/ims/setting/MediaSettings;)Landroid/database/sqlite/SQLiteDatabase;

    #@98
    move-result-object v6

    #@99
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@9c
    .line 935
    :goto_9c
    iget-object v6, p0, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->TAG:Ljava/lang/String;

    #@9e
    new-instance v7, Ljava/lang/StringBuilder;

    #@a0
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@a3
    iget-object v8, p0, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->TAG:Ljava/lang/String;

    #@a5
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v7

    #@a9
    const-string v8, " :: updated row count: "

    #@ab
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v7

    #@af
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b2
    move-result-object v7

    #@b3
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b6
    move-result-object v7

    #@b7
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@ba
    .line 937
    if-eq v0, v5, :cond_dd

    #@bc
    .line 938
    iget-object v5, p0, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->TAG:Ljava/lang/String;

    #@be
    const-string v6, "Update fails"

    #@c0
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@c3
    goto :goto_5a

    #@c4
    .line 929
    :catch_c4
    move-exception v1

    #@c5
    .line 930
    .local v1, e:Landroid/database/sqlite/SQLiteException;
    :try_start_c5
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_c8
    .catchall {:try_start_c5 .. :try_end_c8} :catchall_d2

    #@c8
    .line 932
    iget-object v6, p0, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@ca
    invoke-static {v6}, Lcom/lge/ims/setting/MediaSettings;->access$200(Lcom/lge/ims/setting/MediaSettings;)Landroid/database/sqlite/SQLiteDatabase;

    #@cd
    move-result-object v6

    #@ce
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@d1
    goto :goto_9c

    #@d2
    .end local v1           #e:Landroid/database/sqlite/SQLiteException;
    :catchall_d2
    move-exception v4

    #@d3
    iget-object v5, p0, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@d5
    invoke-static {v5}, Lcom/lge/ims/setting/MediaSettings;->access$200(Lcom/lge/ims/setting/MediaSettings;)Landroid/database/sqlite/SQLiteDatabase;

    #@d8
    move-result-object v5

    #@d9
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@dc
    throw v4

    #@dd
    .line 943
    :cond_dd
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@df
    invoke-virtual {v4, p1, p2}, Lcom/lge/ims/setting/MediaSettings;->summaryChange(Landroid/preference/Preference;Ljava/lang/Object;)V

    #@e2
    move v4, v5

    #@e3
    .line 945
    goto/16 :goto_5a
.end method
