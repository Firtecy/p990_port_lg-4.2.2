.class public Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;
.super Ljava/lang/Object;
.source "MediaSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/setting/MediaSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MediaSettingCommand"
.end annotation


# instance fields
.field protected mCodecKeys:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mCodecTypeName:Ljava/lang/String;

.field protected mListCodecAttributes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mMaxCodecNum:I

.field protected mMediaCodecInfo:Lcom/lge/ims/setting/MediaSettings$IMSMediaCodecInfo;

.field protected mSessionType:I

.field protected mTableName:Ljava/lang/String;

.field protected mTypePrefix:Ljava/lang/String;

.field final synthetic this$0:Lcom/lge/ims/setting/MediaSettings;


# direct methods
.method public constructor <init>(Lcom/lge/ims/setting/MediaSettings;Ljava/lang/String;Ljava/lang/String;I)V
    .registers 6
    .parameter
    .parameter "tableName"
    .parameter "typePrefix"
    .parameter "sessionType"

    #@0
    .prologue
    .line 498
    iput-object p1, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 500
    iput-object p2, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mTableName:Ljava/lang/String;

    #@7
    .line 501
    iput-object p3, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mTypePrefix:Ljava/lang/String;

    #@9
    .line 502
    iput p4, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mSessionType:I

    #@b
    .line 503
    const/4 v0, 0x6

    #@c
    iput v0, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mMaxCodecNum:I

    #@e
    .line 504
    const-string v0, "codec_type"

    #@10
    iput-object v0, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mCodecTypeName:Ljava/lang/String;

    #@12
    .line 506
    new-instance v0, Lcom/lge/ims/setting/MediaSettings$IMSMediaCodecInfo;

    #@14
    invoke-direct {v0, p1}, Lcom/lge/ims/setting/MediaSettings$IMSMediaCodecInfo;-><init>(Lcom/lge/ims/setting/MediaSettings;)V

    #@17
    iput-object v0, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mMediaCodecInfo:Lcom/lge/ims/setting/MediaSettings$IMSMediaCodecInfo;

    #@19
    .line 507
    new-instance v0, Ljava/util/ArrayList;

    #@1b
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@1e
    iput-object v0, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mCodecKeys:Ljava/util/ArrayList;

    #@20
    .line 508
    new-instance v0, Ljava/util/HashMap;

    #@22
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@25
    iput-object v0, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mListCodecAttributes:Ljava/util/HashMap;

    #@27
    .line 509
    return-void
.end method


# virtual methods
.method public loadCodecAttribute(Ljava/lang/String;I[Ljava/lang/String;Z)V
    .registers 13
    .parameter "prefix"
    .parameter "index"
    .parameter "attributeArray"
    .parameter "isEnabled"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 606
    const/4 v1, 0x0

    #@2
    .local v1, j:I
    :goto_2
    array-length v4, p3

    #@3
    if-ge v1, v4, :cond_81

    #@5
    .line 608
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@7
    new-instance v5, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    iget v6, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mSessionType:I

    #@e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v5

    #@12
    const-string v6, "_"

    #@14
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v5

    #@18
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v5

    #@1c
    const-string v6, "_"

    #@1e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v5

    #@22
    aget-object v6, p3, v1

    #@24
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v5

    #@28
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v5

    #@2c
    invoke-virtual {v4, v5}, Lcom/lge/ims/setting/MediaSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    #@2f
    move-result-object v2

    #@30
    .line 610
    .local v2, pref:Landroid/preference/Preference;
    if-eqz v2, :cond_6b

    #@32
    .line 612
    invoke-virtual {v2, v7}, Landroid/preference/Preference;->setShouldDisableView(Z)V

    #@35
    .line 614
    if-eqz p4, :cond_7c

    #@37
    .line 617
    new-instance v4, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v4

    #@40
    const-string v5, "_"

    #@42
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v4

    #@46
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@49
    move-result-object v4

    #@4a
    const-string v5, "_"

    #@4c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v4

    #@50
    aget-object v5, p3, v1

    #@52
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v4

    #@56
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v3

    #@5a
    .line 621
    .local v3, query:Ljava/lang/String;
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@5c
    iget-object v5, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mTableName:Ljava/lang/String;

    #@5e
    invoke-virtual {v4, v5, v3}, Lcom/lge/ims/setting/MediaSettings;->load(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@61
    move-result-object v0

    #@62
    .line 625
    .local v0, attributeValue:Ljava/lang/String;
    if-nez v0, :cond_6e

    #@64
    .line 626
    const-string v4, "IMSMediaSetting"

    #@66
    const-string v5, "loadCodecAttribute : Wrong Key"

    #@68
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@6b
    .line 606
    .end local v0           #attributeValue:Ljava/lang/String;
    .end local v3           #query:Ljava/lang/String;
    :cond_6b
    :goto_6b
    add-int/lit8 v1, v1, 0x1

    #@6d
    goto :goto_2

    #@6e
    .line 629
    .restart local v0       #attributeValue:Ljava/lang/String;
    .restart local v3       #query:Ljava/lang/String;
    :cond_6e
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@70
    invoke-virtual {v4, v2, v0}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/preference/Preference;Ljava/lang/String;)Z

    #@73
    .line 630
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@75
    invoke-virtual {v4, v2, v0}, Lcom/lge/ims/setting/MediaSettings;->summaryChange(Landroid/preference/Preference;Ljava/lang/Object;)V

    #@78
    .line 632
    invoke-virtual {v2, v7}, Landroid/preference/Preference;->setEnabled(Z)V

    #@7b
    goto :goto_6b

    #@7c
    .line 635
    .end local v0           #attributeValue:Ljava/lang/String;
    .end local v3           #query:Ljava/lang/String;
    :cond_7c
    const/4 v4, 0x0

    #@7d
    invoke-virtual {v2, v4}, Landroid/preference/Preference;->setEnabled(Z)V

    #@80
    goto :goto_6b

    #@81
    .line 639
    .end local v2           #pref:Landroid/preference/Preference;
    :cond_81
    return-void
.end method

.method public loadCodecList(I)Z
    .registers 13
    .parameter "index"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 558
    iget-object v5, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@4
    iget-object v8, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mTableName:Ljava/lang/String;

    #@6
    new-instance v9, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    iget-object v10, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mTypePrefix:Ljava/lang/String;

    #@d
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v9

    #@11
    const-string v10, "_"

    #@13
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v9

    #@17
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v9

    #@1b
    const-string v10, "_"

    #@1d
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v9

    #@21
    iget-object v10, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mCodecTypeName:Ljava/lang/String;

    #@23
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v9

    #@27
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v9

    #@2b
    invoke-virtual {v5, v8, v9}, Lcom/lge/ims/setting/MediaSettings;->load(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2e
    move-result-object v4

    #@2f
    .line 560
    .local v4, value:Ljava/lang/String;
    if-eqz v4, :cond_39

    #@31
    const-string v5, ""

    #@33
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@36
    move-result v5

    #@37
    if-eqz v5, :cond_6b

    #@39
    .line 561
    :cond_39
    const-string v5, "IMSMediaSetting"

    #@3b
    new-instance v6, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v8, "loadCodecList - wrong key : "

    #@42
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v6

    #@46
    iget-object v8, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mTypePrefix:Ljava/lang/String;

    #@48
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v6

    #@4c
    const-string v8, "_"

    #@4e
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v6

    #@52
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@55
    move-result-object v6

    #@56
    const-string v8, "_"

    #@58
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v6

    #@5c
    iget-object v8, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mCodecTypeName:Ljava/lang/String;

    #@5e
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v6

    #@62
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65
    move-result-object v6

    #@66
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@69
    move v5, v7

    #@6a
    .line 601
    :goto_6a
    return v5

    #@6b
    .line 565
    :cond_6b
    new-instance v5, Ljava/lang/StringBuilder;

    #@6d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@70
    iget v8, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mSessionType:I

    #@72
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@75
    move-result-object v5

    #@76
    const-string v8, "_"

    #@78
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v5

    #@7c
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v5

    #@80
    const-string v8, "_"

    #@82
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v5

    #@86
    iget-object v8, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mCodecTypeName:Ljava/lang/String;

    #@88
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v5

    #@8c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8f
    move-result-object v2

    #@90
    .line 566
    .local v2, key:Ljava/lang/String;
    iget-object v5, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@92
    invoke-virtual {v5, v2}, Lcom/lge/ims/setting/MediaSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    #@95
    move-result-object v3

    #@96
    .line 568
    .local v3, pref:Landroid/preference/Preference;
    if-eqz v3, :cond_d1

    #@98
    .line 570
    iget-object v5, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@9a
    invoke-virtual {v5, v3, v4}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/preference/Preference;Ljava/lang/String;)Z

    #@9d
    .line 571
    iget-object v5, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@9f
    invoke-virtual {v5, v3, v4}, Lcom/lge/ims/setting/MediaSettings;->summaryChange(Landroid/preference/Preference;Ljava/lang/Object;)V

    #@a2
    .line 578
    :goto_a2
    const/4 v1, 0x0

    #@a3
    .local v1, i:I
    :goto_a3
    iget-object v5, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mCodecKeys:Ljava/util/ArrayList;

    #@a5
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@a8
    move-result v5

    #@a9
    if-ge v1, v5, :cond_124

    #@ab
    .line 580
    iget-object v5, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mListCodecAttributes:Ljava/util/HashMap;

    #@ad
    iget-object v8, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mCodecKeys:Ljava/util/ArrayList;

    #@af
    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@b2
    move-result-object v8

    #@b3
    invoke-virtual {v5, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@b6
    move-result-object v0

    #@b7
    check-cast v0, [Ljava/lang/String;

    #@b9
    .line 582
    .local v0, attributes:[Ljava/lang/String;
    if-eqz v0, :cond_ce

    #@bb
    .line 584
    const-string v5, "None"

    #@bd
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c0
    move-result v5

    #@c1
    if-eqz v5, :cond_ea

    #@c3
    .line 585
    iget-object v5, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mCodecKeys:Ljava/util/ArrayList;

    #@c5
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@c8
    move-result-object v5

    #@c9
    check-cast v5, Ljava/lang/String;

    #@cb
    invoke-virtual {p0, v5, p1, v0, v7}, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->loadCodecAttribute(Ljava/lang/String;I[Ljava/lang/String;Z)V

    #@ce
    .line 578
    :cond_ce
    :goto_ce
    add-int/lit8 v1, v1, 0x1

    #@d0
    goto :goto_a3

    #@d1
    .line 574
    .end local v0           #attributes:[Ljava/lang/String;
    .end local v1           #i:I
    :cond_d1
    const-string v5, "IMSMediaSetting"

    #@d3
    new-instance v8, Ljava/lang/StringBuilder;

    #@d5
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@d8
    const-string v9, "loadCodecList - wrong key : "

    #@da
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v8

    #@de
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e1
    move-result-object v8

    #@e2
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e5
    move-result-object v8

    #@e6
    invoke-static {v5, v8}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@e9
    goto :goto_a2

    #@ea
    .line 589
    .restart local v0       #attributes:[Ljava/lang/String;
    .restart local v1       #i:I
    :cond_ea
    iget-object v5, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mCodecKeys:Ljava/util/ArrayList;

    #@ec
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@ef
    move-result-object v5

    #@f0
    check-cast v5, Ljava/lang/String;

    #@f2
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f5
    move-result v5

    #@f6
    if-eqz v5, :cond_fc

    #@f8
    .line 590
    invoke-virtual {p0, v4, p1, v0, v6}, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->loadCodecAttribute(Ljava/lang/String;I[Ljava/lang/String;Z)V

    #@fb
    goto :goto_ce

    #@fc
    .line 592
    :cond_fc
    iget-object v5, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mCodecKeys:Ljava/util/ArrayList;

    #@fe
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@101
    move-result-object v5

    #@102
    check-cast v5, Ljava/lang/String;

    #@104
    iget-object v8, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mTypePrefix:Ljava/lang/String;

    #@106
    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@109
    move-result v5

    #@10a
    if-eqz v5, :cond_118

    #@10c
    .line 593
    iget-object v5, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mCodecKeys:Ljava/util/ArrayList;

    #@10e
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@111
    move-result-object v5

    #@112
    check-cast v5, Ljava/lang/String;

    #@114
    invoke-virtual {p0, v5, p1, v0, v6}, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->loadCodecAttribute(Ljava/lang/String;I[Ljava/lang/String;Z)V

    #@117
    goto :goto_ce

    #@118
    .line 596
    :cond_118
    iget-object v5, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mCodecKeys:Ljava/util/ArrayList;

    #@11a
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@11d
    move-result-object v5

    #@11e
    check-cast v5, Ljava/lang/String;

    #@120
    invoke-virtual {p0, v5, p1, v0, v7}, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->loadCodecAttribute(Ljava/lang/String;I[Ljava/lang/String;Z)V

    #@123
    goto :goto_ce

    #@124
    .end local v0           #attributes:[Ljava/lang/String;
    :cond_124
    move v5, v6

    #@125
    .line 601
    goto/16 :goto_6a
.end method

.method public loadDB()Z
    .registers 3

    #@0
    .prologue
    .line 549
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget v1, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mMaxCodecNum:I

    #@3
    if-ge v0, v1, :cond_b

    #@5
    .line 550
    invoke-virtual {p0, v0}, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->loadCodecList(I)Z

    #@8
    .line 549
    add-int/lit8 v0, v0, 0x1

    #@a
    goto :goto_1

    #@b
    .line 552
    :cond_b
    const/4 v1, 0x1

    #@c
    return v1
.end method

.method public preferenceChange(ILjava/lang/String;Ljava/lang/String;)Z
    .registers 10
    .parameter "index"
    .parameter "attribute"
    .parameter "inputValue"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 678
    const/4 v0, 0x0

    #@3
    .line 680
    .local v0, prefix:Ljava/lang/String;
    const-string v4, "network_type"

    #@5
    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8
    move-result v4

    #@9
    if-eqz v4, :cond_47

    #@b
    .line 683
    invoke-virtual {p3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@e
    move-result-object p3

    #@f
    .line 684
    const-string v4, " "

    #@11
    const-string v5, ""

    #@13
    invoke-virtual {p3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@16
    move-result-object p3

    #@17
    .line 685
    const-string v4, "["

    #@19
    const-string v5, ""

    #@1b
    invoke-virtual {p3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@1e
    move-result-object p3

    #@1f
    .line 686
    const-string v4, "]"

    #@21
    const-string v5, ""

    #@23
    invoke-virtual {p3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@26
    move-result-object p3

    #@27
    .line 688
    const-string v4, ""

    #@29
    invoke-virtual {p3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2c
    move-result v4

    #@2d
    if-eqz v4, :cond_31

    #@2f
    .line 689
    const-string p3, "lte"

    #@31
    .line 692
    :cond_31
    invoke-virtual {p3, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@34
    move-result-object v4

    #@35
    if-eqz v4, :cond_47

    #@37
    invoke-virtual {p3, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@3a
    move-result-object v4

    #@3b
    const-string v5, ","

    #@3d
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@40
    move-result v4

    #@41
    if-eqz v4, :cond_47

    #@43
    .line 693
    invoke-virtual {p3, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@46
    move-result-object p3

    #@47
    .line 697
    :cond_47
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mCodecTypeName:Ljava/lang/String;

    #@49
    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4c
    move-result v4

    #@4d
    if-eqz v4, :cond_6c

    #@4f
    .line 698
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mTypePrefix:Ljava/lang/String;

    #@51
    .line 704
    :goto_51
    if-nez v0, :cond_71

    #@53
    .line 705
    const-string v3, "IMSMediaSetting"

    #@55
    new-instance v4, Ljava/lang/StringBuilder;

    #@57
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5a
    const-string v5, "cannot find prefix - : "

    #@5c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v4

    #@60
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v4

    #@64
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@67
    move-result-object v4

    #@68
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@6b
    .line 717
    :goto_6b
    return v2

    #@6c
    .line 701
    :cond_6c
    invoke-virtual {p0, p2}, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->searchCodecPrefix(Ljava/lang/String;)Ljava/lang/String;

    #@6f
    move-result-object v0

    #@70
    goto :goto_51

    #@71
    .line 709
    :cond_71
    new-instance v2, Ljava/lang/StringBuilder;

    #@73
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v2

    #@7a
    const-string v4, "_"

    #@7c
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v2

    #@80
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@83
    move-result-object v2

    #@84
    const-string v4, "_"

    #@86
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v2

    #@8a
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v2

    #@8e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@91
    move-result-object v1

    #@92
    .line 711
    .local v1, query:Ljava/lang/String;
    const-string v2, "IMSMediaSetting"

    #@94
    new-instance v4, Ljava/lang/StringBuilder;

    #@96
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@99
    const-string v5, "updateCodec - query : "

    #@9b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v4

    #@9f
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v4

    #@a3
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a6
    move-result-object v4

    #@a7
    invoke-static {v2, v4}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@aa
    .line 713
    invoke-virtual {p0, v1, p3}, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->updateCodec(Ljava/lang/String;Ljava/lang/String;)Z

    #@ad
    move-result v2

    #@ae
    if-eqz v2, :cond_b3

    #@b0
    .line 715
    invoke-virtual {p0, p1}, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->loadCodecList(I)Z

    #@b3
    :cond_b3
    move v2, v3

    #@b4
    .line 717
    goto :goto_6b
.end method

.method public searchCodecPrefix(Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "attribute"

    #@0
    .prologue
    .line 643
    const/4 v1, 0x0

    #@1
    .local v1, i:I
    :goto_1
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mCodecKeys:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v3

    #@7
    if-ge v1, v3, :cond_34

    #@9
    .line 645
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mListCodecAttributes:Ljava/util/HashMap;

    #@b
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mCodecKeys:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@10
    move-result-object v4

    #@11
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, [Ljava/lang/String;

    #@17
    .line 647
    .local v0, attributes:[Ljava/lang/String;
    if-nez v0, :cond_1c

    #@19
    .line 643
    :cond_19
    add-int/lit8 v1, v1, 0x1

    #@1b
    goto :goto_1

    #@1c
    .line 651
    :cond_1c
    const/4 v2, 0x0

    #@1d
    .local v2, j:I
    :goto_1d
    array-length v3, v0

    #@1e
    if-ge v2, v3, :cond_19

    #@20
    .line 653
    aget-object v3, v0, v2

    #@22
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25
    move-result v3

    #@26
    if-eqz v3, :cond_31

    #@28
    .line 655
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mCodecKeys:Ljava/util/ArrayList;

    #@2a
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2d
    move-result-object v3

    #@2e
    check-cast v3, Ljava/lang/String;

    #@30
    .line 659
    .end local v0           #attributes:[Ljava/lang/String;
    .end local v2           #j:I
    :goto_30
    return-object v3

    #@31
    .line 651
    .restart local v0       #attributes:[Ljava/lang/String;
    .restart local v2       #j:I
    :cond_31
    add-int/lit8 v2, v2, 0x1

    #@33
    goto :goto_1d

    #@34
    .line 659
    .end local v0           #attributes:[Ljava/lang/String;
    .end local v2           #j:I
    :cond_34
    const/4 v3, 0x0

    #@35
    goto :goto_30
.end method

.method public setCodecAttributes(Ljava/lang/String;[Ljava/lang/String;)V
    .registers 4
    .parameter "key"
    .parameter "codecAttributes"

    #@0
    .prologue
    .line 513
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mCodecKeys:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5
    .line 515
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mListCodecAttributes:Ljava/util/HashMap;

    #@7
    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    .line 516
    return-void
.end method

.method public setListener(Lcom/lge/ims/setting/MediaSettings$MediaCodecChangeListener;)V
    .registers 10
    .parameter "listener"

    #@0
    .prologue
    .line 520
    const/4 v1, 0x0

    #@1
    .local v1, i:I
    :goto_1
    iget v5, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mMaxCodecNum:I

    #@3
    if-ge v1, v5, :cond_8c

    #@5
    .line 522
    iget-object v5, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@7
    new-instance v6, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    iget v7, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mSessionType:I

    #@e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v6

    #@12
    const-string v7, "_"

    #@14
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v6

    #@18
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v6

    #@1c
    const-string v7, "_"

    #@1e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v6

    #@22
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mCodecTypeName:Ljava/lang/String;

    #@24
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v6

    #@28
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v6

    #@2c
    invoke-virtual {v5, v6}, Lcom/lge/ims/setting/MediaSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    #@2f
    move-result-object v4

    #@30
    .line 523
    .local v4, pref:Landroid/preference/Preference;
    if-eqz v4, :cond_35

    #@32
    .line 525
    invoke-virtual {v4, p1}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@35
    .line 528
    :cond_35
    const/4 v2, 0x0

    #@36
    .local v2, j:I
    :goto_36
    iget-object v5, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mCodecKeys:Ljava/util/ArrayList;

    #@38
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@3b
    move-result v5

    #@3c
    if-ge v2, v5, :cond_88

    #@3e
    .line 530
    iget-object v5, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mListCodecAttributes:Ljava/util/HashMap;

    #@40
    iget-object v6, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mCodecKeys:Ljava/util/ArrayList;

    #@42
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@45
    move-result-object v6

    #@46
    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@49
    move-result-object v0

    #@4a
    check-cast v0, [Ljava/lang/String;

    #@4c
    .line 532
    .local v0, attributes:[Ljava/lang/String;
    if-nez v0, :cond_51

    #@4e
    .line 528
    :cond_4e
    add-int/lit8 v2, v2, 0x1

    #@50
    goto :goto_36

    #@51
    .line 536
    :cond_51
    const/4 v3, 0x0

    #@52
    .local v3, k:I
    :goto_52
    array-length v5, v0

    #@53
    if-ge v3, v5, :cond_4e

    #@55
    .line 537
    iget-object v5, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@57
    new-instance v6, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    iget v7, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mSessionType:I

    #@5e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@61
    move-result-object v6

    #@62
    const-string v7, "_"

    #@64
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v6

    #@68
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v6

    #@6c
    const-string v7, "_"

    #@6e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v6

    #@72
    aget-object v7, v0, v3

    #@74
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v6

    #@78
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v6

    #@7c
    invoke-virtual {v5, v6}, Lcom/lge/ims/setting/MediaSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    #@7f
    move-result-object v4

    #@80
    .line 539
    if-eqz v4, :cond_85

    #@82
    .line 540
    invoke-virtual {v4, p1}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@85
    .line 536
    :cond_85
    add-int/lit8 v3, v3, 0x1

    #@87
    goto :goto_52

    #@88
    .line 520
    .end local v0           #attributes:[Ljava/lang/String;
    .end local v3           #k:I
    :cond_88
    add-int/lit8 v1, v1, 0x1

    #@8a
    goto/16 :goto_1

    #@8c
    .line 545
    .end local v2           #j:I
    .end local v4           #pref:Landroid/preference/Preference;
    :cond_8c
    return-void
.end method

.method public updateCodec(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 6
    .parameter "query"
    .parameter "inputValue"

    #@0
    .prologue
    .line 664
    new-instance v0, Landroid/content/ContentValues;

    #@2
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    #@5
    .line 666
    .local v0, values:Landroid/content/ContentValues;
    invoke-virtual {v0, p1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 668
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@a
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mTableName:Ljava/lang/String;

    #@c
    invoke-virtual {v1, v2, v0}, Lcom/lge/ims/setting/MediaSettings;->updateValue(Ljava/lang/String;Landroid/content/ContentValues;)Z

    #@f
    move-result v1

    #@10
    if-nez v1, :cond_1b

    #@12
    .line 670
    const-string v1, "IMSMediaSetting"

    #@14
    const-string v2, "updateCodec - fail"

    #@16
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@19
    .line 671
    const/4 v1, 0x0

    #@1a
    .line 673
    :goto_1a
    return v1

    #@1b
    :cond_1b
    const/4 v1, 0x1

    #@1c
    goto :goto_1a
.end method
