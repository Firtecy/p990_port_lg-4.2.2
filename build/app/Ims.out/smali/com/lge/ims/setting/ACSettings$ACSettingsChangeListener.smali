.class Lcom/lge/ims/setting/ACSettings$ACSettingsChangeListener;
.super Ljava/lang/Object;
.source "ACSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/setting/ACSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ACSettingsChangeListener"
.end annotation


# instance fields
.field private mContentURI:Landroid/net/Uri;

.field final synthetic this$0:Lcom/lge/ims/setting/ACSettings;


# direct methods
.method public constructor <init>(Lcom/lge/ims/setting/ACSettings;Landroid/net/Uri;)V
    .registers 4
    .parameter
    .parameter "uri"

    #@0
    .prologue
    .line 203
    iput-object p1, p0, Lcom/lge/ims/setting/ACSettings$ACSettingsChangeListener;->this$0:Lcom/lge/ims/setting/ACSettings;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 201
    const/4 v0, 0x0

    #@6
    iput-object v0, p0, Lcom/lge/ims/setting/ACSettings$ACSettingsChangeListener;->mContentURI:Landroid/net/Uri;

    #@8
    .line 204
    iput-object p2, p0, Lcom/lge/ims/setting/ACSettings$ACSettingsChangeListener;->mContentURI:Landroid/net/Uri;

    #@a
    .line 205
    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .registers 13
    .parameter "preference"
    .parameter "newValue"

    #@0
    .prologue
    const/4 v7, 0x3

    #@1
    const/4 v5, 0x1

    #@2
    .line 208
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    #@5
    move-result-object v3

    #@6
    .line 211
    .local v3, key:Ljava/lang/String;
    if-eqz v3, :cond_12

    #@8
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@b
    move-result v6

    #@c
    if-le v6, v7, :cond_12

    #@e
    .line 212
    invoke-virtual {v3, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@11
    move-result-object v3

    #@12
    .line 215
    :cond_12
    const-string v6, "ACSettings"

    #@14
    new-instance v7, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    iget-object v8, p0, Lcom/lge/ims/setting/ACSettings$ACSettingsChangeListener;->mContentURI:Landroid/net/Uri;

    #@1b
    invoke-virtual {v8}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    #@1e
    move-result-object v8

    #@1f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v7

    #@23
    const-string v8, " :: "

    #@25
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v7

    #@29
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v7

    #@2d
    const-string v8, "="

    #@2f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v7

    #@33
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@36
    move-result-object v8

    #@37
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v7

    #@3b
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v7

    #@3f
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@42
    .line 217
    const/4 v0, 0x0

    #@43
    .line 218
    .local v0, affectedRows:I
    new-instance v4, Landroid/content/ContentValues;

    #@45
    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    #@48
    .line 220
    .local v4, values:Landroid/content/ContentValues;
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@4b
    move-result-object v6

    #@4c
    invoke-virtual {v4, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@4f
    .line 223
    :try_start_4f
    iget-object v6, p0, Lcom/lge/ims/setting/ACSettings$ACSettingsChangeListener;->this$0:Lcom/lge/ims/setting/ACSettings;

    #@51
    invoke-virtual {v6}, Lcom/lge/ims/setting/ACSettings;->getContentResolver()Landroid/content/ContentResolver;

    #@54
    move-result-object v6

    #@55
    iget-object v7, p0, Lcom/lge/ims/setting/ACSettings$ACSettingsChangeListener;->mContentURI:Landroid/net/Uri;

    #@57
    const/4 v8, 0x0

    #@58
    const/4 v9, 0x0

    #@59
    invoke-virtual {v6, v7, v4, v8, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_5c
    .catch Ljava/lang/Exception; {:try_start_4f .. :try_end_5c} :catch_80

    #@5c
    move-result v0

    #@5d
    .line 228
    :goto_5d
    const-string v6, "ACSettings"

    #@5f
    new-instance v7, Ljava/lang/StringBuilder;

    #@61
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    const-string v8, "updated row count: "

    #@66
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v7

    #@6a
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v7

    #@6e
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v7

    #@72
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@75
    .line 230
    if-eq v0, v5, :cond_85

    #@77
    .line 231
    const-string v5, "ACSettings"

    #@79
    const-string v6, "Update fails"

    #@7b
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@7e
    .line 232
    const/4 v5, 0x0

    #@7f
    .line 252
    :goto_7f
    return v5

    #@80
    .line 224
    :catch_80
    move-exception v2

    #@81
    .line 225
    .local v2, e:Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    #@84
    goto :goto_5d

    #@85
    .line 236
    .end local v2           #e:Ljava/lang/Exception;
    :cond_85
    const-string v6, "availability"

    #@87
    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8a
    move-result v6

    #@8b
    if-eqz v6, :cond_b2

    #@8d
    .line 237
    new-instance v1, Landroid/content/ContentValues;

    #@8f
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    #@92
    .line 239
    .local v1, cvs:Landroid/content/ContentValues;
    const-string v6, "admin_ac"

    #@94
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@97
    move-result-object v7

    #@98
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@9b
    .line 242
    :try_start_9b
    iget-object v6, p0, Lcom/lge/ims/setting/ACSettings$ACSettingsChangeListener;->this$0:Lcom/lge/ims/setting/ACSettings;

    #@9d
    invoke-virtual {v6}, Lcom/lge/ims/setting/ACSettings;->getContentResolver()Landroid/content/ContentResolver;

    #@a0
    move-result-object v6

    #@a1
    sget-object v7, Lcom/lge/ims/provider/IMS$Subscriber;->CONTENT_URI:Landroid/net/Uri;

    #@a3
    const/4 v8, 0x0

    #@a4
    const/4 v9, 0x0

    #@a5
    invoke-virtual {v6, v7, v1, v8, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@a8
    move-result v6

    #@a9
    if-gtz v6, :cond_b2

    #@ab
    .line 243
    const-string v6, "ACSettings"

    #@ad
    const-string v7, "Updating AC on/off failed"

    #@af
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_b2
    .catch Ljava/lang/Exception; {:try_start_9b .. :try_end_b2} :catch_b8

    #@b2
    .line 250
    .end local v1           #cvs:Landroid/content/ContentValues;
    :cond_b2
    :goto_b2
    iget-object v6, p0, Lcom/lge/ims/setting/ACSettings$ACSettingsChangeListener;->this$0:Lcom/lge/ims/setting/ACSettings;

    #@b4
    invoke-static {v6, p1, p2}, Lcom/lge/ims/setting/ACSettings;->access$000(Lcom/lge/ims/setting/ACSettings;Landroid/preference/Preference;Ljava/lang/Object;)V

    #@b7
    goto :goto_7f

    #@b8
    .line 245
    .restart local v1       #cvs:Landroid/content/ContentValues;
    :catch_b8
    move-exception v2

    #@b9
    .line 246
    .restart local v2       #e:Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    #@bc
    goto :goto_b2
.end method
