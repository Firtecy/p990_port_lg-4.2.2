.class Lcom/lge/ims/setting/MediaSettings$MediaCodecChangeListener;
.super Ljava/lang/Object;
.source "MediaSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/setting/MediaSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MediaCodecChangeListener"
.end annotation


# instance fields
.field private mCommand:Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;

.field final synthetic this$0:Lcom/lge/ims/setting/MediaSettings;


# direct methods
.method public constructor <init>(Lcom/lge/ims/setting/MediaSettings;Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;)V
    .registers 4
    .parameter
    .parameter "command"

    #@0
    .prologue
    .line 954
    iput-object p1, p0, Lcom/lge/ims/setting/MediaSettings$MediaCodecChangeListener;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 955
    iput-object p2, p0, Lcom/lge/ims/setting/MediaSettings$MediaCodecChangeListener;->mCommand:Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;

    #@7
    .line 956
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettings$MediaCodecChangeListener;->mCommand:Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;

    #@9
    invoke-virtual {v0}, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->loadDB()Z

    #@c
    .line 957
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettings$MediaCodecChangeListener;->mCommand:Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;

    #@e
    invoke-virtual {v0, p0}, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->setListener(Lcom/lge/ims/setting/MediaSettings$MediaCodecChangeListener;)V

    #@11
    .line 958
    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .registers 12
    .parameter "preference"
    .parameter "newValue"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 962
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettings$MediaCodecChangeListener;->mCommand:Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;

    #@4
    if-nez v7, :cond_7

    #@6
    .line 979
    :goto_6
    return v5

    #@7
    .line 966
    :cond_7
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    #@a
    move-result-object v3

    #@b
    .line 968
    .local v3, key:Ljava/lang/String;
    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@e
    move-result-object v4

    #@f
    .line 969
    .local v4, prefix:Ljava/lang/String;
    const/4 v5, 0x2

    #@10
    const/4 v7, 0x3

    #@11
    invoke-virtual {v3, v5, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@14
    move-result-object v5

    #@15
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@18
    move-result v1

    #@19
    .line 970
    .local v1, index:I
    const/4 v5, 0x4

    #@1a
    invoke-virtual {v3, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@1d
    move-result-object v0

    #@1e
    .line 972
    .local v0, attribute:Ljava/lang/String;
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    .line 974
    .local v2, inputValue:Ljava/lang/String;
    const-string v5, "IMSMediaSetting"

    #@24
    new-instance v7, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v8, "onPreferenceChange - key : "

    #@2b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v7

    #@2f
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v7

    #@33
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v7

    #@37
    invoke-static {v5, v7}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@3a
    .line 975
    const-string v5, "IMSMediaSetting"

    #@3c
    new-instance v7, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v8, "onPreferenceChange - value : "

    #@43
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v7

    #@47
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v7

    #@4b
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v7

    #@4f
    invoke-static {v5, v7}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@52
    .line 977
    iget-object v5, p0, Lcom/lge/ims/setting/MediaSettings$MediaCodecChangeListener;->mCommand:Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;

    #@54
    invoke-virtual {v5, v1, v0, v2}, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->preferenceChange(ILjava/lang/String;Ljava/lang/String;)Z

    #@57
    move v5, v6

    #@58
    .line 979
    goto :goto_6
.end method
