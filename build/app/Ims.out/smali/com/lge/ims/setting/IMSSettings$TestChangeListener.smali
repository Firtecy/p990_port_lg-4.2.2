.class Lcom/lge/ims/setting/IMSSettings$TestChangeListener;
.super Ljava/lang/Object;
.source "IMSSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/setting/IMSSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TestChangeListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/setting/IMSSettings;


# direct methods
.method private constructor <init>(Lcom/lge/ims/setting/IMSSettings;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1486
    iput-object p1, p0, Lcom/lge/ims/setting/IMSSettings$TestChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/lge/ims/setting/IMSSettings;Lcom/lge/ims/setting/IMSSettings$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1486
    invoke-direct {p0, p1}, Lcom/lge/ims/setting/IMSSettings$TestChangeListener;-><init>(Lcom/lge/ims/setting/IMSSettings;)V

    #@3
    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .registers 7
    .parameter "preference"
    .parameter "newValue"

    #@0
    .prologue
    .line 1488
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 1490
    .local v0, key:Ljava/lang/String;
    const-string v1, "LGIMSSettings"

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "Test :: "

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    const-string v3, "="

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 1492
    iget-object v1, p0, Lcom/lge/ims/setting/IMSSettings$TestChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@2c
    const v2, 0x7f06000e

    #@2f
    invoke-virtual {v1, v2}, Lcom/lge/ims/setting/IMSSettings;->getString(I)Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@36
    move-result v1

    #@37
    if-eqz v1, :cond_42

    #@39
    .line 1493
    iget-object v1, p0, Lcom/lge/ims/setting/IMSSettings$TestChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@3b
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@3e
    move-result-object v2

    #@3f
    invoke-static {v1, v2}, Lcom/lge/ims/setting/IMSSettings;->access$2000(Lcom/lge/ims/setting/IMSSettings;Ljava/lang/String;)V

    #@42
    .line 1496
    :cond_42
    iget-object v1, p0, Lcom/lge/ims/setting/IMSSettings$TestChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@44
    invoke-static {v1, p1, p2}, Lcom/lge/ims/setting/IMSSettings;->access$1700(Lcom/lge/ims/setting/IMSSettings;Landroid/preference/Preference;Ljava/lang/Object;)V

    #@47
    .line 1498
    const/4 v1, 0x1

    #@48
    return v1
.end method
