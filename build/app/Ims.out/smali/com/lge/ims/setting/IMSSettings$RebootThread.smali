.class Lcom/lge/ims/setting/IMSSettings$RebootThread;
.super Ljava/lang/Thread;
.source "IMSSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/setting/IMSSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RebootThread"
.end annotation


# instance fields
.field mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/lge/ims/setting/IMSSettings;


# direct methods
.method public constructor <init>(Lcom/lge/ims/setting/IMSSettings;Landroid/content/Context;)V
    .registers 4
    .parameter
    .parameter "context"

    #@0
    .prologue
    .line 1174
    iput-object p1, p0, Lcom/lge/ims/setting/IMSSettings$RebootThread;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@2
    .line 1175
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@5
    .line 1172
    const/4 v0, 0x0

    #@6
    iput-object v0, p0, Lcom/lge/ims/setting/IMSSettings$RebootThread;->mContext:Landroid/content/Context;

    #@8
    .line 1177
    iput-object p2, p0, Lcom/lge/ims/setting/IMSSettings$RebootThread;->mContext:Landroid/content/Context;

    #@a
    .line 1178
    return-void
.end method


# virtual methods
.method public run()V
    .registers 6

    #@0
    .prologue
    .line 1181
    const-wide/16 v2, 0xc8

    #@2
    invoke-static {v2, v3}, Landroid/os/SystemClock;->sleep(J)V

    #@5
    .line 1183
    iget-object v2, p0, Lcom/lge/ims/setting/IMSSettings$RebootThread;->mContext:Landroid/content/Context;

    #@7
    if-nez v2, :cond_a

    #@9
    .line 1197
    :cond_9
    :goto_9
    return-void

    #@a
    .line 1187
    :cond_a
    iget-object v2, p0, Lcom/lge/ims/setting/IMSSettings$RebootThread;->mContext:Landroid/content/Context;

    #@c
    const-string v3, "power"

    #@e
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@11
    move-result-object v1

    #@12
    check-cast v1, Landroid/os/PowerManager;

    #@14
    .line 1190
    .local v1, pm:Landroid/os/PowerManager;
    if-eqz v1, :cond_9

    #@16
    .line 1191
    :try_start_16
    const-string v2, "IMS settings is changed"

    #@18
    invoke-virtual {v1, v2}, Landroid/os/PowerManager;->reboot(Ljava/lang/String;)V
    :try_end_1b
    .catch Ljava/lang/SecurityException; {:try_start_16 .. :try_end_1b} :catch_1c

    #@1b
    goto :goto_9

    #@1c
    .line 1193
    :catch_1c
    move-exception v0

    #@1d
    .line 1194
    .local v0, e:Ljava/lang/SecurityException;
    const-string v2, "LGIMSSettings"

    #@1f
    new-instance v3, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v4, "SecurityException :: "

    #@26
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v3

    #@2a
    invoke-virtual {v0}, Ljava/lang/SecurityException;->toString()Ljava/lang/String;

    #@2d
    move-result-object v4

    #@2e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v3

    #@32
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v3

    #@36
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    .line 1195
    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    #@3c
    goto :goto_9
.end method
