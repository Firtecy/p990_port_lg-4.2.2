.class Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;
.super Ljava/lang/Object;
.source "MediaSettingsCarrier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/setting/MediaSettingsCarrier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CarrierAudioCommand"
.end annotation


# instance fields
.field private m_audioCodecList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private m_audioPayloadFormatList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;


# direct methods
.method public constructor <init>(Lcom/lge/ims/setting/MediaSettingsCarrier;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 125
    iput-object p1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 127
    new-instance v0, Ljava/util/ArrayList;

    #@7
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@a
    iput-object v0, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioCodecList:Ljava/util/ArrayList;

    #@c
    .line 128
    new-instance v0, Ljava/util/ArrayList;

    #@e
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@11
    iput-object v0, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioPayloadFormatList:Ljava/util/ArrayList;

    #@13
    .line 129
    return-void
.end method


# virtual methods
.method public addCodecList(I)V
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 405
    invoke-virtual {p0}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->clearCodecList()V

    #@3
    .line 406
    if-nez p1, :cond_22

    #@5
    .line 407
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioCodecList:Ljava/util/ArrayList;

    #@7
    const-string v1, "AMR-WB"

    #@9
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@c
    .line 408
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioCodecList:Ljava/util/ArrayList;

    #@e
    const-string v1, "DTMF-WB"

    #@10
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@13
    .line 409
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioCodecList:Ljava/util/ArrayList;

    #@15
    const-string v1, "AMR"

    #@17
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1a
    .line 410
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioCodecList:Ljava/util/ArrayList;

    #@1c
    const-string v1, "DTMF"

    #@1e
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@21
    .line 426
    :goto_21
    return-void

    #@22
    .line 412
    :cond_22
    const/4 v0, 0x1

    #@23
    if-ne p1, v0, :cond_34

    #@25
    .line 413
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioCodecList:Ljava/util/ArrayList;

    #@27
    const-string v1, "AMR-WB"

    #@29
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2c
    .line 414
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioCodecList:Ljava/util/ArrayList;

    #@2e
    const-string v1, "DTMF-WB"

    #@30
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@33
    goto :goto_21

    #@34
    .line 416
    :cond_34
    const/4 v0, 0x2

    #@35
    if-ne p1, v0, :cond_54

    #@37
    .line 417
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioCodecList:Ljava/util/ArrayList;

    #@39
    const-string v1, "AMR"

    #@3b
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3e
    .line 418
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioCodecList:Ljava/util/ArrayList;

    #@40
    const-string v1, "DTMF"

    #@42
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@45
    .line 419
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioCodecList:Ljava/util/ArrayList;

    #@47
    const-string v1, "AMR-WB"

    #@49
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@4c
    .line 420
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioCodecList:Ljava/util/ArrayList;

    #@4e
    const-string v1, "DTMF-WB"

    #@50
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@53
    goto :goto_21

    #@54
    .line 423
    :cond_54
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioCodecList:Ljava/util/ArrayList;

    #@56
    const-string v1, "AMR"

    #@58
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5b
    .line 424
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioCodecList:Ljava/util/ArrayList;

    #@5d
    const-string v1, "DTMF"

    #@5f
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@62
    goto :goto_21
.end method

.method public addPayloadFormatList(I)V
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 492
    invoke-virtual {p0}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->clearPayloadList()V

    #@3
    .line 493
    if-nez p1, :cond_d

    #@5
    .line 494
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioPayloadFormatList:Ljava/util/ArrayList;

    #@7
    const-string v1, "BE"

    #@9
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@c
    .line 507
    :goto_c
    return-void

    #@d
    .line 496
    :cond_d
    const/4 v0, 0x1

    #@e
    if-ne p1, v0, :cond_1f

    #@10
    .line 497
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioPayloadFormatList:Ljava/util/ArrayList;

    #@12
    const-string v1, "BE"

    #@14
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@17
    .line 498
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioPayloadFormatList:Ljava/util/ArrayList;

    #@19
    const-string v1, "OA"

    #@1b
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1e
    goto :goto_c

    #@1f
    .line 500
    :cond_1f
    const/4 v0, 0x2

    #@20
    if-ne p1, v0, :cond_2a

    #@22
    .line 501
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioPayloadFormatList:Ljava/util/ArrayList;

    #@24
    const-string v1, "OA"

    #@26
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@29
    goto :goto_c

    #@2a
    .line 504
    :cond_2a
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioPayloadFormatList:Ljava/util/ArrayList;

    #@2c
    const-string v1, "OA"

    #@2e
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@31
    .line 505
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioPayloadFormatList:Ljava/util/ArrayList;

    #@33
    const-string v1, "BE"

    #@35
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@38
    goto :goto_c
.end method

.method public clearCodecDB(Ljava/lang/String;)V
    .registers 6
    .parameter "tableName"

    #@0
    .prologue
    .line 476
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    const/4 v2, 0x6

    #@2
    if-ge v0, v2, :cond_27

    #@4
    .line 477
    new-instance v2, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v3, "audiocodec_"

    #@b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    const-string v3, "_codec_type"

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    .line 478
    .local v1, key:Ljava/lang/String;
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@1f
    const-string v3, "None"

    #@21
    invoke-virtual {v2, p1, v1, v3}, Lcom/lge/ims/setting/MediaSettingsCarrier;->updateDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@24
    .line 476
    add-int/lit8 v0, v0, 0x1

    #@26
    goto :goto_1

    #@27
    .line 480
    .end local v1           #key:Ljava/lang/String;
    :cond_27
    return-void
.end method

.method public clearCodecList()V
    .registers 2

    #@0
    .prologue
    .line 483
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioCodecList:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@5
    .line 484
    return-void
.end method

.method public clearPayloadList()V
    .registers 2

    #@0
    .prologue
    .line 487
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioPayloadFormatList:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@5
    .line 488
    return-void
.end method

.method public loadDB(Ljava/lang/String;)V
    .registers 11
    .parameter "tableName"

    #@0
    .prologue
    .line 133
    const/4 v0, 0x0

    #@1
    .line 134
    .local v0, codecValue:Ljava/lang/String;
    const/4 v4, 0x0

    #@2
    .line 135
    .local v4, payloadValue:Ljava/lang/String;
    const/4 v2, 0x0

    #@3
    .line 138
    .local v2, key:Ljava/lang/String;
    iget-object v6, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioCodecList:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    #@8
    .line 139
    iget-object v6, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioPayloadFormatList:Ljava/util/ArrayList;

    #@a
    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    #@d
    .line 142
    const/4 v1, 0x0

    #@e
    .local v1, i:I
    :goto_e
    const/4 v6, 0x6

    #@f
    if-ge v1, v6, :cond_18e

    #@11
    .line 144
    new-instance v6, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v7, "audiocodec_"

    #@18
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v6

    #@1c
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v6

    #@20
    const-string v7, "_codec_type"

    #@22
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v6

    #@26
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    .line 145
    iget-object v6, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@2c
    invoke-virtual {v6, p1, v2}, Lcom/lge/ims/setting/MediaSettingsCarrier;->load(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2f
    move-result-object v0

    #@30
    .line 146
    const-string v6, "IMSMediaSettingsCarrier"

    #@32
    new-instance v7, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    const-string v8, "loadDB : "

    #@39
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v7

    #@3d
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v7

    #@41
    const-string v8, ", value : "

    #@43
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v7

    #@47
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v7

    #@4b
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v7

    #@4f
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@52
    .line 148
    if-nez v0, :cond_5e

    #@54
    .line 149
    const-string v6, "IMSMediaSettingsCarrier"

    #@56
    const-string v7, "loadDB : no such codec"

    #@58
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@5b
    .line 142
    :cond_5b
    :goto_5b
    add-int/lit8 v1, v1, 0x1

    #@5d
    goto :goto_e

    #@5e
    .line 153
    :cond_5e
    iget-object v6, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@60
    new-instance v7, Ljava/lang/StringBuilder;

    #@62
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@65
    const-string v8, "audiocodec_"

    #@67
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v7

    #@6b
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v7

    #@6f
    const-string v8, "_network_type"

    #@71
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v7

    #@75
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@78
    move-result-object v7

    #@79
    invoke-virtual {v6, p1, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier;->load(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7c
    move-result-object v3

    #@7d
    .line 155
    .local v3, networkValue:Ljava/lang/String;
    if-eqz v3, :cond_87

    #@7f
    const-string v6, ".*lte.*"

    #@81
    invoke-virtual {v3, v6}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    #@84
    move-result v6

    #@85
    if-nez v6, :cond_a6

    #@87
    .line 156
    :cond_87
    const-string v6, "IMSMediaSettingsCarrier"

    #@89
    new-instance v7, Ljava/lang/StringBuilder;

    #@8b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@8e
    const-string v8, "loadDB : "

    #@90
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v7

    #@94
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v7

    #@98
    const-string v8, ", value is not LTE"

    #@9a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v7

    #@9e
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a1
    move-result-object v7

    #@a2
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@a5
    goto :goto_5b

    #@a6
    .line 160
    :cond_a6
    const-string v6, "AMR"

    #@a8
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ab
    move-result v6

    #@ac
    if-eqz v6, :cond_142

    #@ae
    .line 161
    const/4 v5, 0x0

    #@af
    .line 162
    .local v5, value:Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    #@b1
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@b4
    const-string v7, "audiocodec_"

    #@b6
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v6

    #@ba
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v6

    #@be
    const-string v7, "_sampling_rate"

    #@c0
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v6

    #@c4
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c7
    move-result-object v2

    #@c8
    .line 163
    iget-object v6, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@ca
    invoke-virtual {v6, p1, v2}, Lcom/lge/ims/setting/MediaSettingsCarrier;->load(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@cd
    move-result-object v5

    #@ce
    .line 165
    if-eqz v5, :cond_5b

    #@d0
    .line 169
    const-string v6, "16000"

    #@d2
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d5
    move-result v6

    #@d6
    if-eqz v6, :cond_121

    #@d8
    .line 170
    iget-object v6, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioCodecList:Ljava/util/ArrayList;

    #@da
    const-string v7, "AMR-WB"

    #@dc
    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@df
    .line 176
    :cond_df
    :goto_df
    new-instance v6, Ljava/lang/StringBuilder;

    #@e1
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@e4
    const-string v7, "AMR_"

    #@e6
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e9
    move-result-object v6

    #@ea
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v6

    #@ee
    const-string v7, "_octet_align"

    #@f0
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v6

    #@f4
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f7
    move-result-object v2

    #@f8
    .line 177
    iget-object v6, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@fa
    invoke-virtual {v6, p1, v2}, Lcom/lge/ims/setting/MediaSettingsCarrier;->load(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@fd
    move-result-object v4

    #@fe
    .line 179
    if-eqz v4, :cond_5b

    #@100
    .line 183
    const-string v6, "0"

    #@102
    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@105
    move-result v6

    #@106
    if-nez v6, :cond_118

    #@108
    const-string v6, ""

    #@10a
    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10d
    move-result v6

    #@10e
    if-nez v6, :cond_118

    #@110
    const-string v6, "-1"

    #@112
    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@115
    move-result v6

    #@116
    if-eqz v6, :cond_131

    #@118
    .line 185
    :cond_118
    iget-object v6, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioPayloadFormatList:Ljava/util/ArrayList;

    #@11a
    const-string v7, "BE"

    #@11c
    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@11f
    goto/16 :goto_5b

    #@121
    .line 172
    :cond_121
    const-string v6, "8000"

    #@123
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@126
    move-result v6

    #@127
    if-eqz v6, :cond_df

    #@129
    .line 173
    iget-object v6, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioCodecList:Ljava/util/ArrayList;

    #@12b
    const-string v7, "AMR"

    #@12d
    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@130
    goto :goto_df

    #@131
    .line 187
    :cond_131
    const-string v6, "1"

    #@133
    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@136
    move-result v6

    #@137
    if-eqz v6, :cond_5b

    #@139
    .line 188
    iget-object v6, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioPayloadFormatList:Ljava/util/ArrayList;

    #@13b
    const-string v7, "OA"

    #@13d
    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@140
    goto/16 :goto_5b

    #@142
    .line 191
    .end local v5           #value:Ljava/lang/String;
    :cond_142
    const-string v6, "telephone-event"

    #@144
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@147
    move-result v6

    #@148
    if-eqz v6, :cond_5b

    #@14a
    .line 193
    new-instance v6, Ljava/lang/StringBuilder;

    #@14c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@14f
    const-string v7, "audiocodec_"

    #@151
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@154
    move-result-object v6

    #@155
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@158
    move-result-object v6

    #@159
    const-string v7, "_sampling_rate"

    #@15b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15e
    move-result-object v6

    #@15f
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@162
    move-result-object v2

    #@163
    .line 195
    const/4 v5, 0x0

    #@164
    .line 196
    .restart local v5       #value:Ljava/lang/String;
    iget-object v6, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@166
    invoke-virtual {v6, p1, v2}, Lcom/lge/ims/setting/MediaSettingsCarrier;->load(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@169
    move-result-object v5

    #@16a
    .line 197
    if-eqz v5, :cond_5b

    #@16c
    .line 201
    const-string v6, "16000"

    #@16e
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@171
    move-result v6

    #@172
    if-eqz v6, :cond_17d

    #@174
    .line 202
    iget-object v6, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioCodecList:Ljava/util/ArrayList;

    #@176
    const-string v7, "DTMF-WB"

    #@178
    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@17b
    goto/16 :goto_5b

    #@17d
    .line 204
    :cond_17d
    const-string v6, "8000"

    #@17f
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@182
    move-result v6

    #@183
    if-eqz v6, :cond_5b

    #@185
    .line 205
    iget-object v6, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioCodecList:Ljava/util/ArrayList;

    #@187
    const-string v7, "DTMF"

    #@189
    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@18c
    goto/16 :goto_5b

    #@18e
    .line 209
    .end local v3           #networkValue:Ljava/lang/String;
    .end local v5           #value:Ljava/lang/String;
    :cond_18e
    return-void
.end method

.method public loadDBToPreference(Ljava/lang/String;Landroid/preference/Preference;Ljava/lang/String;)Z
    .registers 9
    .parameter "tableName"
    .parameter "pref"
    .parameter "key"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 384
    const-string v2, "IMSMediaSettingsCarrier"

    #@3
    new-instance v3, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v4, "loadDBToPreference : key - "

    #@a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v3

    #@e
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v3

    #@12
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v3

    #@16
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@19
    .line 386
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@1b
    invoke-virtual {v2, p1, p3}, Lcom/lge/ims/setting/MediaSettingsCarrier;->load(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    .line 388
    .local v0, value:Ljava/lang/String;
    if-nez v0, :cond_22

    #@21
    .line 400
    :cond_21
    :goto_21
    return v1

    #@22
    .line 392
    :cond_22
    if-eqz p2, :cond_21

    #@24
    .line 396
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@26
    invoke-virtual {v2, p2, v0}, Lcom/lge/ims/setting/MediaSettingsCarrier;->setValue(Landroid/preference/Preference;Ljava/lang/String;)Z

    #@29
    move-result v2

    #@2a
    if-eqz v2, :cond_21

    #@2c
    .line 397
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@2e
    invoke-virtual {v1, p2, v0}, Lcom/lge/ims/setting/MediaSettingsCarrier;->summaryChange(Landroid/preference/Preference;Ljava/lang/Object;)V

    #@31
    .line 398
    const/4 v1, 0x1

    #@32
    goto :goto_21
.end method

.method public searchCodecIndex(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .registers 15
    .parameter "tableName"
    .parameter "codecType"
    .parameter "payloadType"

    #@0
    .prologue
    const/4 v7, -0x1

    #@1
    .line 433
    const-string v8, "AMR-WB"

    #@3
    invoke-virtual {p2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6
    move-result v8

    #@7
    if-eqz v8, :cond_55

    #@9
    .line 434
    const-string v3, "AMR"

    #@b
    .line 435
    .local v3, target1:Ljava/lang/String;
    const-string v4, "16000"

    #@d
    .line 442
    .local v4, target2:Ljava/lang/String;
    :goto_d
    const/4 v0, 0x0

    #@e
    .local v0, i:I
    :goto_e
    const/4 v8, 0x6

    #@f
    if-ge v0, v8, :cond_bd

    #@11
    .line 444
    new-instance v8, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v9, "audiocodec_"

    #@18
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v8

    #@1c
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v8

    #@20
    const-string v9, "_codec_type"

    #@22
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v8

    #@26
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v1

    #@2a
    .line 445
    .local v1, key1:Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v9, "audiocodec_"

    #@31
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v8

    #@35
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38
    move-result-object v8

    #@39
    const-string v9, "_sampling_rate"

    #@3b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v8

    #@3f
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v2

    #@43
    .line 446
    .local v2, key2:Ljava/lang/String;
    iget-object v8, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@45
    invoke-virtual {v8, p1, v1}, Lcom/lge/ims/setting/MediaSettingsCarrier;->load(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@48
    move-result-object v5

    #@49
    .line 447
    .local v5, value1:Ljava/lang/String;
    iget-object v8, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@4b
    invoke-virtual {v8, p1, v2}, Lcom/lge/ims/setting/MediaSettingsCarrier;->load(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@4e
    move-result-object v6

    #@4f
    .line 449
    .local v6, value2:Ljava/lang/String;
    if-eqz v5, :cond_53

    #@51
    if-nez v6, :cond_5a

    #@53
    :cond_53
    move v0, v7

    #@54
    .line 472
    .end local v0           #i:I
    .end local v1           #key1:Ljava/lang/String;
    .end local v2           #key2:Ljava/lang/String;
    .end local v5           #value1:Ljava/lang/String;
    .end local v6           #value2:Ljava/lang/String;
    :cond_54
    :goto_54
    return v0

    #@55
    .line 438
    .end local v3           #target1:Ljava/lang/String;
    .end local v4           #target2:Ljava/lang/String;
    :cond_55
    const-string v3, "AMR"

    #@57
    .line 439
    .restart local v3       #target1:Ljava/lang/String;
    const-string v4, "8000"

    #@59
    .restart local v4       #target2:Ljava/lang/String;
    goto :goto_d

    #@5a
    .line 453
    .restart local v0       #i:I
    .restart local v1       #key1:Ljava/lang/String;
    .restart local v2       #key2:Ljava/lang/String;
    .restart local v5       #value1:Ljava/lang/String;
    .restart local v6       #value2:Ljava/lang/String;
    :cond_5a
    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5d
    move-result v8

    #@5e
    if-eqz v8, :cond_b9

    #@60
    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@63
    move-result v8

    #@64
    if-eqz v8, :cond_b9

    #@66
    .line 455
    new-instance v8, Ljava/lang/StringBuilder;

    #@68
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@6b
    const-string v9, "AMR_"

    #@6d
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v8

    #@71
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@74
    move-result-object v8

    #@75
    const-string v9, "_octet_align"

    #@77
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v8

    #@7b
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e
    move-result-object v1

    #@7f
    .line 456
    iget-object v8, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@81
    invoke-virtual {v8, p1, v1}, Lcom/lge/ims/setting/MediaSettingsCarrier;->load(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@84
    move-result-object v5

    #@85
    .line 458
    if-nez v5, :cond_89

    #@87
    move v0, v7

    #@88
    .line 459
    goto :goto_54

    #@89
    .line 462
    :cond_89
    const-string v8, "1"

    #@8b
    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8e
    move-result v8

    #@8f
    if-eqz v8, :cond_99

    #@91
    const-string v8, "OA"

    #@93
    invoke-virtual {p3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@96
    move-result v8

    #@97
    if-nez v8, :cond_54

    #@99
    .line 465
    :cond_99
    const-string v8, "0"

    #@9b
    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9e
    move-result v8

    #@9f
    if-nez v8, :cond_b1

    #@a1
    const-string v8, ""

    #@a3
    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a6
    move-result v8

    #@a7
    if-nez v8, :cond_b1

    #@a9
    const-string v8, "-1"

    #@ab
    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ae
    move-result v8

    #@af
    if-eqz v8, :cond_b9

    #@b1
    :cond_b1
    const-string v8, "BE"

    #@b3
    invoke-virtual {p3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b6
    move-result v8

    #@b7
    if-nez v8, :cond_54

    #@b9
    .line 442
    :cond_b9
    add-int/lit8 v0, v0, 0x1

    #@bb
    goto/16 :goto_e

    #@bd
    .line 471
    .end local v1           #key1:Ljava/lang/String;
    .end local v2           #key2:Ljava/lang/String;
    .end local v5           #value1:Ljava/lang/String;
    .end local v6           #value2:Ljava/lang/String;
    :cond_bd
    const-string v8, "IMSMediaSettingsCarrier"

    #@bf
    new-instance v9, Ljava/lang/StringBuilder;

    #@c1
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@c4
    const-string v10, "searchCodecIndex : "

    #@c6
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v9

    #@ca
    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v9

    #@ce
    const-string v10, ","

    #@d0
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d3
    move-result-object v9

    #@d4
    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v9

    #@d8
    const-string v10, " type Codec is not existed in DB"

    #@da
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v9

    #@de
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e1
    move-result-object v9

    #@e2
    invoke-static {v8, v9}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@e5
    move v0, v7

    #@e6
    .line 472
    goto/16 :goto_54
.end method

.method public setAudioListener(Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;)V
    .registers 6
    .parameter "listener"

    #@0
    .prologue
    .line 213
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@2
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@4
    const v3, 0x7f0602cc

    #@7
    invoke-virtual {v2, v3}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@a
    move-result-object v2

    #@b
    invoke-static {v1, v2}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@e
    move-result-object v0

    #@f
    .line 214
    .local v0, pref:Landroid/preference/Preference;
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@11
    invoke-static {v1, v0, p1}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$100(Lcom/lge/ims/setting/MediaSettingsCarrier;Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@14
    .line 216
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@16
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@18
    const v3, 0x7f0602cd

    #@1b
    invoke-virtual {v2, v3}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-static {v1, v2}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@22
    move-result-object v0

    #@23
    .line 217
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@25
    invoke-static {v1, v0, p1}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$100(Lcom/lge/ims/setting/MediaSettingsCarrier;Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@28
    .line 219
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@2a
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@2c
    const v3, 0x7f060170

    #@2f
    invoke-virtual {v2, v3}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@32
    move-result-object v2

    #@33
    invoke-static {v1, v2}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@36
    move-result-object v0

    #@37
    .line 220
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@39
    invoke-static {v1, v0, p1}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$100(Lcom/lge/ims/setting/MediaSettingsCarrier;Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@3c
    .line 222
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@3e
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@40
    const v3, 0x7f0602d4

    #@43
    invoke-virtual {v2, v3}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@46
    move-result-object v2

    #@47
    invoke-static {v1, v2}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@4a
    move-result-object v0

    #@4b
    .line 223
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@4d
    invoke-static {v1, v0, p1}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$100(Lcom/lge/ims/setting/MediaSettingsCarrier;Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@50
    .line 225
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@52
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@54
    const v3, 0x7f0602d5

    #@57
    invoke-virtual {v2, v3}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@5a
    move-result-object v2

    #@5b
    invoke-static {v1, v2}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@5e
    move-result-object v0

    #@5f
    .line 226
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@61
    invoke-static {v1, v0, p1}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$100(Lcom/lge/ims/setting/MediaSettingsCarrier;Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@64
    .line 228
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@66
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@68
    const v3, 0x7f0602d6

    #@6b
    invoke-virtual {v2, v3}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@6e
    move-result-object v2

    #@6f
    invoke-static {v1, v2}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@72
    move-result-object v0

    #@73
    .line 229
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@75
    invoke-static {v1, v0, p1}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$100(Lcom/lge/ims/setting/MediaSettingsCarrier;Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@78
    .line 231
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@7a
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@7c
    const v3, 0x7f0602d7

    #@7f
    invoke-virtual {v2, v3}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@82
    move-result-object v2

    #@83
    invoke-static {v1, v2}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@86
    move-result-object v0

    #@87
    .line 232
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@89
    invoke-static {v1, v0, p1}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$100(Lcom/lge/ims/setting/MediaSettingsCarrier;Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@8c
    .line 234
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@8e
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@90
    const v3, 0x7f0602d8

    #@93
    invoke-virtual {v2, v3}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@96
    move-result-object v2

    #@97
    invoke-static {v1, v2}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@9a
    move-result-object v0

    #@9b
    .line 235
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@9d
    invoke-static {v1, v0, p1}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$100(Lcom/lge/ims/setting/MediaSettingsCarrier;Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@a0
    .line 237
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@a2
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@a4
    const v3, 0x7f0602d9

    #@a7
    invoke-virtual {v2, v3}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@aa
    move-result-object v2

    #@ab
    invoke-static {v1, v2}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@ae
    move-result-object v0

    #@af
    .line 238
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@b1
    invoke-static {v1, v0, p1}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$100(Lcom/lge/ims/setting/MediaSettingsCarrier;Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@b4
    .line 240
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@b6
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@b8
    const v3, 0x7f0602da

    #@bb
    invoke-virtual {v2, v3}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@be
    move-result-object v2

    #@bf
    invoke-static {v1, v2}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@c2
    move-result-object v0

    #@c3
    .line 241
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@c5
    invoke-static {v1, v0, p1}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$100(Lcom/lge/ims/setting/MediaSettingsCarrier;Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@c8
    .line 243
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@ca
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@cc
    const v3, 0x7f0602db

    #@cf
    invoke-virtual {v2, v3}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@d2
    move-result-object v2

    #@d3
    invoke-static {v1, v2}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@d6
    move-result-object v0

    #@d7
    .line 244
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@d9
    invoke-static {v1, v0, p1}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$100(Lcom/lge/ims/setting/MediaSettingsCarrier;Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@dc
    .line 245
    return-void
.end method

.method public updateCodecListDB(Ljava/lang/String;I)Z
    .registers 12
    .parameter "tableName"
    .parameter "listIndexValue"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v8, 0x6

    #@2
    .line 511
    const/4 v3, 0x0

    #@3
    .line 514
    .local v3, key:Ljava/lang/String;
    const/4 v0, 0x0

    #@4
    .line 515
    .local v0, codecIndex:I
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioCodecList:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v4

    #@b
    if-ge v1, v4, :cond_2d

    #@d
    .line 517
    if-lt v0, v8, :cond_7a

    #@f
    .line 518
    const-string v4, "IMSMediaSettingsCarrier"

    #@11
    new-instance v6, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v7, "updateCodecListDB : "

    #@18
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v6

    #@1c
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v6

    #@20
    const-string v7, " is out of MAX CODEC NUMBER"

    #@22
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v6

    #@26
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v6

    #@2a
    invoke-static {v4, v6}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@2d
    .line 590
    :cond_2d
    if-ne p2, v5, :cond_47c

    #@2f
    .line 592
    const/4 v2, 0x0

    #@30
    .local v2, j:I
    :goto_30
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioPayloadFormatList:Ljava/util/ArrayList;

    #@32
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@35
    move-result v4

    #@36
    if-ge v2, v4, :cond_58

    #@38
    .line 594
    if-lt v0, v8, :cond_343

    #@3a
    .line 595
    const-string v4, "IMSMediaSettingsCarrier"

    #@3c
    new-instance v6, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v7, "updateCodecListDB : "

    #@43
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v6

    #@47
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v6

    #@4b
    const-string v7, " is out of MAX CODEC NUMBER"

    #@4d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v6

    #@51
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v6

    #@55
    invoke-static {v4, v6}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@58
    .line 619
    :cond_58
    if-lt v0, v8, :cond_41a

    #@5a
    .line 620
    const-string v4, "IMSMediaSettingsCarrier"

    #@5c
    new-instance v5, Ljava/lang/StringBuilder;

    #@5e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@61
    const-string v6, "updateCodecListDB : "

    #@63
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v5

    #@67
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v5

    #@6b
    const-string v6, " is out of MAX CODEC NUMBER"

    #@6d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v5

    #@71
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v5

    #@75
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@78
    .line 621
    const/4 v4, 0x0

    #@79
    .line 632
    .end local v2           #j:I
    :goto_79
    return v4

    #@7a
    .line 522
    :cond_7a
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioCodecList:Ljava/util/ArrayList;

    #@7c
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@7f
    move-result-object v4

    #@80
    check-cast v4, Ljava/lang/String;

    #@82
    const-string v6, "AMR-WB"

    #@84
    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@87
    move-result v4

    #@88
    if-eqz v4, :cond_16a

    #@8a
    .line 524
    const/4 v2, 0x0

    #@8b
    .restart local v2       #j:I
    :goto_8b
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioPayloadFormatList:Ljava/util/ArrayList;

    #@8d
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@90
    move-result v4

    #@91
    if-ge v2, v4, :cond_2cc

    #@93
    .line 526
    new-instance v4, Ljava/lang/StringBuilder;

    #@95
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@98
    const-string v6, "audiocodec_"

    #@9a
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v4

    #@9e
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v4

    #@a2
    const-string v6, "_codec_type"

    #@a4
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v4

    #@a8
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ab
    move-result-object v3

    #@ac
    .line 527
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@ae
    const-string v6, "AMR"

    #@b0
    invoke-virtual {v4, p1, v3, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier;->updateDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@b3
    .line 528
    new-instance v4, Ljava/lang/StringBuilder;

    #@b5
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@b8
    const-string v6, "audiocodec_"

    #@ba
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v4

    #@be
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v4

    #@c2
    const-string v6, "_sampling_rate"

    #@c4
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v4

    #@c8
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cb
    move-result-object v3

    #@cc
    .line 529
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@ce
    const-string v6, "16000"

    #@d0
    invoke-virtual {v4, p1, v3, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier;->updateDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@d3
    .line 530
    new-instance v4, Ljava/lang/StringBuilder;

    #@d5
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@d8
    const-string v6, "audiocodec_"

    #@da
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v4

    #@de
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e1
    move-result-object v4

    #@e2
    const-string v6, "_network_type"

    #@e4
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v4

    #@e8
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@eb
    move-result-object v3

    #@ec
    .line 531
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@ee
    const-string v6, "lte"

    #@f0
    invoke-virtual {v4, p1, v3, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier;->updateDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@f3
    .line 532
    new-instance v4, Ljava/lang/StringBuilder;

    #@f5
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@f8
    const-string v6, "AMR_"

    #@fa
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fd
    move-result-object v4

    #@fe
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@101
    move-result-object v4

    #@102
    const-string v6, "_channel"

    #@104
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@107
    move-result-object v4

    #@108
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10b
    move-result-object v3

    #@10c
    .line 533
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@10e
    const-string v6, "1"

    #@110
    invoke-virtual {v4, p1, v3, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier;->updateDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@113
    .line 535
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioPayloadFormatList:Ljava/util/ArrayList;

    #@115
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@118
    move-result-object v4

    #@119
    check-cast v4, Ljava/lang/String;

    #@11b
    const-string v6, "BE"

    #@11d
    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@120
    move-result v4

    #@121
    if-eqz v4, :cond_149

    #@123
    .line 536
    new-instance v4, Ljava/lang/StringBuilder;

    #@125
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@128
    const-string v6, "AMR_"

    #@12a
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12d
    move-result-object v4

    #@12e
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@131
    move-result-object v4

    #@132
    const-string v6, "_octet_align"

    #@134
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@137
    move-result-object v4

    #@138
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13b
    move-result-object v3

    #@13c
    .line 537
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@13e
    const-string v6, "-1"

    #@140
    invoke-virtual {v4, p1, v3, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier;->updateDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@143
    .line 543
    :goto_143
    add-int/lit8 v0, v0, 0x1

    #@145
    .line 524
    add-int/lit8 v2, v2, 0x1

    #@147
    goto/16 :goto_8b

    #@149
    .line 540
    :cond_149
    new-instance v4, Ljava/lang/StringBuilder;

    #@14b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@14e
    const-string v6, "AMR_"

    #@150
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@153
    move-result-object v4

    #@154
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@157
    move-result-object v4

    #@158
    const-string v6, "_octet_align"

    #@15a
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15d
    move-result-object v4

    #@15e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@161
    move-result-object v3

    #@162
    .line 541
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@164
    const-string v6, "1"

    #@166
    invoke-virtual {v4, p1, v3, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier;->updateDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@169
    goto :goto_143

    #@16a
    .line 546
    .end local v2           #j:I
    :cond_16a
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioCodecList:Ljava/util/ArrayList;

    #@16c
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@16f
    move-result-object v4

    #@170
    check-cast v4, Ljava/lang/String;

    #@172
    const-string v6, "AMR"

    #@174
    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@177
    move-result v4

    #@178
    if-eqz v4, :cond_25a

    #@17a
    .line 548
    const/4 v2, 0x0

    #@17b
    .restart local v2       #j:I
    :goto_17b
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioPayloadFormatList:Ljava/util/ArrayList;

    #@17d
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@180
    move-result v4

    #@181
    if-ge v2, v4, :cond_2cc

    #@183
    .line 549
    new-instance v4, Ljava/lang/StringBuilder;

    #@185
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@188
    const-string v6, "audiocodec_"

    #@18a
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18d
    move-result-object v4

    #@18e
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@191
    move-result-object v4

    #@192
    const-string v6, "_codec_type"

    #@194
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@197
    move-result-object v4

    #@198
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19b
    move-result-object v3

    #@19c
    .line 550
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@19e
    const-string v6, "AMR"

    #@1a0
    invoke-virtual {v4, p1, v3, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier;->updateDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@1a3
    .line 551
    new-instance v4, Ljava/lang/StringBuilder;

    #@1a5
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1a8
    const-string v6, "audiocodec_"

    #@1aa
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ad
    move-result-object v4

    #@1ae
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b1
    move-result-object v4

    #@1b2
    const-string v6, "_sampling_rate"

    #@1b4
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b7
    move-result-object v4

    #@1b8
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1bb
    move-result-object v3

    #@1bc
    .line 552
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@1be
    const-string v6, "8000"

    #@1c0
    invoke-virtual {v4, p1, v3, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier;->updateDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@1c3
    .line 553
    new-instance v4, Ljava/lang/StringBuilder;

    #@1c5
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1c8
    const-string v6, "audiocodec_"

    #@1ca
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cd
    move-result-object v4

    #@1ce
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d1
    move-result-object v4

    #@1d2
    const-string v6, "_network_type"

    #@1d4
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d7
    move-result-object v4

    #@1d8
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1db
    move-result-object v3

    #@1dc
    .line 554
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@1de
    const-string v6, "lte,hspa,3g"

    #@1e0
    invoke-virtual {v4, p1, v3, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier;->updateDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@1e3
    .line 555
    new-instance v4, Ljava/lang/StringBuilder;

    #@1e5
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1e8
    const-string v6, "AMR_"

    #@1ea
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ed
    move-result-object v4

    #@1ee
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f1
    move-result-object v4

    #@1f2
    const-string v6, "_channel"

    #@1f4
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f7
    move-result-object v4

    #@1f8
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1fb
    move-result-object v3

    #@1fc
    .line 556
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@1fe
    const-string v6, "1"

    #@200
    invoke-virtual {v4, p1, v3, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier;->updateDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@203
    .line 558
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioPayloadFormatList:Ljava/util/ArrayList;

    #@205
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@208
    move-result-object v4

    #@209
    check-cast v4, Ljava/lang/String;

    #@20b
    const-string v6, "BE"

    #@20d
    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@210
    move-result v4

    #@211
    if-eqz v4, :cond_239

    #@213
    .line 559
    new-instance v4, Ljava/lang/StringBuilder;

    #@215
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@218
    const-string v6, "AMR_"

    #@21a
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21d
    move-result-object v4

    #@21e
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@221
    move-result-object v4

    #@222
    const-string v6, "_octet_align"

    #@224
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@227
    move-result-object v4

    #@228
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22b
    move-result-object v3

    #@22c
    .line 560
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@22e
    const-string v6, "-1"

    #@230
    invoke-virtual {v4, p1, v3, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier;->updateDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@233
    .line 566
    :goto_233
    add-int/lit8 v0, v0, 0x1

    #@235
    .line 548
    add-int/lit8 v2, v2, 0x1

    #@237
    goto/16 :goto_17b

    #@239
    .line 563
    :cond_239
    new-instance v4, Ljava/lang/StringBuilder;

    #@23b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@23e
    const-string v6, "AMR_"

    #@240
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@243
    move-result-object v4

    #@244
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@247
    move-result-object v4

    #@248
    const-string v6, "_octet_align"

    #@24a
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24d
    move-result-object v4

    #@24e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@251
    move-result-object v3

    #@252
    .line 564
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@254
    const-string v6, "1"

    #@256
    invoke-virtual {v4, p1, v3, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier;->updateDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@259
    goto :goto_233

    #@25a
    .line 569
    .end local v2           #j:I
    :cond_25a
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioCodecList:Ljava/util/ArrayList;

    #@25c
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@25f
    move-result-object v4

    #@260
    check-cast v4, Ljava/lang/String;

    #@262
    const-string v6, "DTMF-WB"

    #@264
    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@267
    move-result v4

    #@268
    if-eqz v4, :cond_2d0

    #@26a
    .line 570
    new-instance v4, Ljava/lang/StringBuilder;

    #@26c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@26f
    const-string v6, "audiocodec_"

    #@271
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@274
    move-result-object v4

    #@275
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@278
    move-result-object v4

    #@279
    const-string v6, "_codec_type"

    #@27b
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27e
    move-result-object v4

    #@27f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@282
    move-result-object v3

    #@283
    .line 571
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@285
    const-string v6, "telephone-event"

    #@287
    invoke-virtual {v4, p1, v3, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier;->updateDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@28a
    .line 572
    new-instance v4, Ljava/lang/StringBuilder;

    #@28c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@28f
    const-string v6, "audiocodec_"

    #@291
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@294
    move-result-object v4

    #@295
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@298
    move-result-object v4

    #@299
    const-string v6, "_sampling_rate"

    #@29b
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29e
    move-result-object v4

    #@29f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a2
    move-result-object v3

    #@2a3
    .line 573
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@2a5
    const-string v6, "16000"

    #@2a7
    invoke-virtual {v4, p1, v3, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier;->updateDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@2aa
    .line 574
    new-instance v4, Ljava/lang/StringBuilder;

    #@2ac
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2af
    const-string v6, "audiocodec_"

    #@2b1
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b4
    move-result-object v4

    #@2b5
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2b8
    move-result-object v4

    #@2b9
    const-string v6, "_network_type"

    #@2bb
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2be
    move-result-object v4

    #@2bf
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c2
    move-result-object v3

    #@2c3
    .line 575
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@2c5
    const-string v6, "lte"

    #@2c7
    invoke-virtual {v4, p1, v3, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier;->updateDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@2ca
    .line 576
    add-int/lit8 v0, v0, 0x1

    #@2cc
    .line 515
    :cond_2cc
    :goto_2cc
    add-int/lit8 v1, v1, 0x1

    #@2ce
    goto/16 :goto_5

    #@2d0
    .line 578
    :cond_2d0
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioCodecList:Ljava/util/ArrayList;

    #@2d2
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2d5
    move-result-object v4

    #@2d6
    check-cast v4, Ljava/lang/String;

    #@2d8
    const-string v6, "DTMF"

    #@2da
    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2dd
    move-result v4

    #@2de
    if-eqz v4, :cond_2cc

    #@2e0
    .line 579
    new-instance v4, Ljava/lang/StringBuilder;

    #@2e2
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2e5
    const-string v6, "audiocodec_"

    #@2e7
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ea
    move-result-object v4

    #@2eb
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2ee
    move-result-object v4

    #@2ef
    const-string v6, "_codec_type"

    #@2f1
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f4
    move-result-object v4

    #@2f5
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f8
    move-result-object v3

    #@2f9
    .line 580
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@2fb
    const-string v6, "telephone-event"

    #@2fd
    invoke-virtual {v4, p1, v3, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier;->updateDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@300
    .line 581
    new-instance v4, Ljava/lang/StringBuilder;

    #@302
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@305
    const-string v6, "audiocodec_"

    #@307
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30a
    move-result-object v4

    #@30b
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30e
    move-result-object v4

    #@30f
    const-string v6, "_sampling_rate"

    #@311
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@314
    move-result-object v4

    #@315
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@318
    move-result-object v3

    #@319
    .line 582
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@31b
    const-string v6, "8000"

    #@31d
    invoke-virtual {v4, p1, v3, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier;->updateDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@320
    .line 583
    new-instance v4, Ljava/lang/StringBuilder;

    #@322
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@325
    const-string v6, "audiocodec_"

    #@327
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32a
    move-result-object v4

    #@32b
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32e
    move-result-object v4

    #@32f
    const-string v6, "_network_type"

    #@331
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@334
    move-result-object v4

    #@335
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@338
    move-result-object v3

    #@339
    .line 584
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@33b
    const-string v6, "lte,hspa,3g"

    #@33d
    invoke-virtual {v4, p1, v3, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier;->updateDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@340
    .line 585
    add-int/lit8 v0, v0, 0x1

    #@342
    goto :goto_2cc

    #@343
    .line 599
    .restart local v2       #j:I
    :cond_343
    new-instance v4, Ljava/lang/StringBuilder;

    #@345
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@348
    const-string v6, "audiocodec_"

    #@34a
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34d
    move-result-object v4

    #@34e
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@351
    move-result-object v4

    #@352
    const-string v6, "_codec_type"

    #@354
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@357
    move-result-object v4

    #@358
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35b
    move-result-object v3

    #@35c
    .line 600
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@35e
    const-string v6, "AMR"

    #@360
    invoke-virtual {v4, p1, v3, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier;->updateDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@363
    .line 601
    new-instance v4, Ljava/lang/StringBuilder;

    #@365
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@368
    const-string v6, "audiocodec_"

    #@36a
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36d
    move-result-object v4

    #@36e
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@371
    move-result-object v4

    #@372
    const-string v6, "_sampling_rate"

    #@374
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@377
    move-result-object v4

    #@378
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37b
    move-result-object v3

    #@37c
    .line 602
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@37e
    const-string v6, "8000"

    #@380
    invoke-virtual {v4, p1, v3, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier;->updateDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@383
    .line 603
    new-instance v4, Ljava/lang/StringBuilder;

    #@385
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@388
    const-string v6, "audiocodec_"

    #@38a
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38d
    move-result-object v4

    #@38e
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@391
    move-result-object v4

    #@392
    const-string v6, "_network_type"

    #@394
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@397
    move-result-object v4

    #@398
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39b
    move-result-object v3

    #@39c
    .line 604
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@39e
    const-string v6, "hspa,3g"

    #@3a0
    invoke-virtual {v4, p1, v3, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier;->updateDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@3a3
    .line 605
    new-instance v4, Ljava/lang/StringBuilder;

    #@3a5
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3a8
    const-string v6, "AMR_"

    #@3aa
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3ad
    move-result-object v4

    #@3ae
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3b1
    move-result-object v4

    #@3b2
    const-string v6, "_channel"

    #@3b4
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b7
    move-result-object v4

    #@3b8
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3bb
    move-result-object v3

    #@3bc
    .line 606
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@3be
    const-string v6, "1"

    #@3c0
    invoke-virtual {v4, p1, v3, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier;->updateDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@3c3
    .line 608
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioPayloadFormatList:Ljava/util/ArrayList;

    #@3c5
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@3c8
    move-result-object v4

    #@3c9
    check-cast v4, Ljava/lang/String;

    #@3cb
    const-string v6, "BE"

    #@3cd
    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3d0
    move-result v4

    #@3d1
    if-eqz v4, :cond_3f9

    #@3d3
    .line 609
    new-instance v4, Ljava/lang/StringBuilder;

    #@3d5
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3d8
    const-string v6, "AMR_"

    #@3da
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3dd
    move-result-object v4

    #@3de
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e1
    move-result-object v4

    #@3e2
    const-string v6, "_octet_align"

    #@3e4
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e7
    move-result-object v4

    #@3e8
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3eb
    move-result-object v3

    #@3ec
    .line 610
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@3ee
    const-string v6, "-1"

    #@3f0
    invoke-virtual {v4, p1, v3, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier;->updateDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@3f3
    .line 616
    :goto_3f3
    add-int/lit8 v0, v0, 0x1

    #@3f5
    .line 592
    add-int/lit8 v2, v2, 0x1

    #@3f7
    goto/16 :goto_30

    #@3f9
    .line 613
    :cond_3f9
    new-instance v4, Ljava/lang/StringBuilder;

    #@3fb
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3fe
    const-string v6, "AMR_"

    #@400
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@403
    move-result-object v4

    #@404
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@407
    move-result-object v4

    #@408
    const-string v6, "_octet_align"

    #@40a
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40d
    move-result-object v4

    #@40e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@411
    move-result-object v3

    #@412
    .line 614
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@414
    const-string v6, "1"

    #@416
    invoke-virtual {v4, p1, v3, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier;->updateDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@419
    goto :goto_3f3

    #@41a
    .line 624
    :cond_41a
    new-instance v4, Ljava/lang/StringBuilder;

    #@41c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@41f
    const-string v6, "audiocodec_"

    #@421
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@424
    move-result-object v4

    #@425
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@428
    move-result-object v4

    #@429
    const-string v6, "_codec_type"

    #@42b
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42e
    move-result-object v4

    #@42f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@432
    move-result-object v3

    #@433
    .line 625
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@435
    const-string v6, "telephone-event"

    #@437
    invoke-virtual {v4, p1, v3, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier;->updateDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@43a
    .line 626
    new-instance v4, Ljava/lang/StringBuilder;

    #@43c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@43f
    const-string v6, "audiocodec_"

    #@441
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@444
    move-result-object v4

    #@445
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@448
    move-result-object v4

    #@449
    const-string v6, "_sampling_rate"

    #@44b
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44e
    move-result-object v4

    #@44f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@452
    move-result-object v3

    #@453
    .line 627
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@455
    const-string v6, "8000"

    #@457
    invoke-virtual {v4, p1, v3, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier;->updateDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@45a
    .line 628
    new-instance v4, Ljava/lang/StringBuilder;

    #@45c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@45f
    const-string v6, "audiocodec_"

    #@461
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@464
    move-result-object v4

    #@465
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@468
    move-result-object v4

    #@469
    const-string v6, "_network_type"

    #@46b
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46e
    move-result-object v4

    #@46f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@472
    move-result-object v3

    #@473
    .line 629
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@475
    const-string v6, "hspa,3g"

    #@477
    invoke-virtual {v4, p1, v3, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier;->updateDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@47a
    .line 630
    add-int/lit8 v0, v0, 0x1

    #@47c
    .end local v2           #j:I
    :cond_47c
    move v4, v5

    #@47d
    .line 632
    goto/16 :goto_79
.end method

.method public updateCodecModeSetDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 11
    .parameter "tableName"
    .parameter "codecType"
    .parameter "payload"
    .parameter "value"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 647
    const/4 v1, 0x0

    #@2
    .line 648
    .local v1, isUpdated:Z
    invoke-virtual {p0, p1, p2, p3}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->searchCodecIndex(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    .line 650
    .local v0, codecIndex:I
    const/4 v3, -0x1

    #@7
    if-ne v0, v3, :cond_a

    #@9
    .line 660
    :cond_9
    :goto_9
    return v2

    #@a
    .line 654
    :cond_a
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@c
    new-instance v4, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v5, "AMR_"

    #@13
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    const-string v5, "_mode_set"

    #@1d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v4

    #@21
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v4

    #@25
    invoke-virtual {v3, p1, v4, p4}, Lcom/lge/ims/setting/MediaSettingsCarrier;->updateDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@28
    move-result v1

    #@29
    .line 656
    if-eqz v1, :cond_9

    #@2b
    .line 657
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@2d
    new-instance v3, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v4, "AMR_"

    #@34
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v3

    #@38
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v3

    #@3c
    const-string v4, "_mode_change_capability"

    #@3e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v3

    #@42
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v3

    #@46
    const-string v4, "2"

    #@48
    invoke-virtual {v2, p1, v3, v4}, Lcom/lge/ims/setting/MediaSettingsCarrier;->updateDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@4b
    move-result v1

    #@4c
    move v2, v1

    #@4d
    .line 658
    goto :goto_9
.end method

.method public updateCodecPayloadNumDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 9
    .parameter "tableName"
    .parameter "codecType"
    .parameter "payload"
    .parameter "value"

    #@0
    .prologue
    .line 636
    invoke-virtual {p0, p1, p2, p3}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->searchCodecIndex(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@3
    move-result v0

    #@4
    .line 638
    .local v0, codecIndex:I
    const/4 v1, -0x1

    #@5
    if-ne v0, v1, :cond_9

    #@7
    .line 639
    const/4 v1, 0x0

    #@8
    .line 642
    :goto_8
    return v1

    #@9
    :cond_9
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "audiocodec_"

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    const-string v3, "_payload_type"

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v1, p1, v2, p4}, Lcom/lge/ims/setting/MediaSettingsCarrier;->updateDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@27
    move-result v1

    #@28
    goto :goto_8
.end method

.method public updatePreference(Ljava/lang/String;)V
    .registers 15
    .parameter "tableName"

    #@0
    .prologue
    const v12, 0x7f060170

    #@3
    const/4 v11, 0x1

    #@4
    const/4 v10, 0x0

    #@5
    .line 250
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@7
    iget-object v8, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@9
    const v9, 0x7f0602cc

    #@c
    invoke-virtual {v8, v9}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@f
    move-result-object v8

    #@10
    invoke-static {v7, v8}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@13
    move-result-object v5

    #@14
    .line 252
    .local v5, pref:Landroid/preference/Preference;
    if-nez v5, :cond_17

    #@16
    .line 380
    :cond_16
    :goto_16
    return-void

    #@17
    .line 256
    :cond_17
    const/4 v6, -0x1

    #@18
    .line 257
    .local v6, value:I
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioCodecList:Ljava/util/ArrayList;

    #@1a
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@1d
    move-result v7

    #@1e
    const/4 v8, 0x3

    #@1f
    if-le v7, v8, :cond_b8

    #@21
    .line 258
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioCodecList:Ljava/util/ArrayList;

    #@23
    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@26
    move-result-object v7

    #@27
    check-cast v7, Ljava/lang/String;

    #@29
    const-string v8, "AMR-WB"

    #@2b
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2e
    move-result v7

    #@2f
    if-eqz v7, :cond_b5

    #@31
    .line 259
    const/4 v6, 0x0

    #@32
    .line 275
    :goto_32
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@34
    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@37
    move-result-object v8

    #@38
    invoke-virtual {v7, v5, v8}, Lcom/lge/ims/setting/MediaSettingsCarrier;->setValue(Landroid/preference/Preference;Ljava/lang/String;)Z

    #@3b
    move-result v7

    #@3c
    if-eqz v7, :cond_4a

    #@3e
    .line 276
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@40
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@43
    move-result-object v8

    #@44
    invoke-virtual {v7, v5, v8}, Lcom/lge/ims/setting/MediaSettingsCarrier;->summaryChange(Landroid/preference/Preference;Ljava/lang/Object;)V

    #@47
    .line 277
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->addCodecList(I)V

    #@4a
    .line 281
    :cond_4a
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@4c
    iget-object v8, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@4e
    const v9, 0x7f0602cd

    #@51
    invoke-virtual {v8, v9}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@54
    move-result-object v8

    #@55
    invoke-static {v7, v8}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@58
    move-result-object v5

    #@59
    .line 283
    if-eqz v5, :cond_16

    #@5b
    .line 287
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioPayloadFormatList:Ljava/util/ArrayList;

    #@5d
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@60
    move-result v7

    #@61
    const/4 v8, 0x2

    #@62
    if-le v7, v8, :cond_d0

    #@64
    .line 289
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioPayloadFormatList:Ljava/util/ArrayList;

    #@66
    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@69
    move-result-object v7

    #@6a
    check-cast v7, Ljava/lang/String;

    #@6c
    const-string v8, "BE"

    #@6e
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@71
    move-result v7

    #@72
    if-eqz v7, :cond_ce

    #@74
    .line 290
    const/4 v6, 0x1

    #@75
    .line 329
    :goto_75
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@77
    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@7a
    move-result-object v8

    #@7b
    invoke-virtual {v7, v5, v8}, Lcom/lge/ims/setting/MediaSettingsCarrier;->setValue(Landroid/preference/Preference;Ljava/lang/String;)Z

    #@7e
    move-result v7

    #@7f
    if-eqz v7, :cond_8d

    #@81
    .line 330
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@83
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@86
    move-result-object v8

    #@87
    invoke-virtual {v7, v5, v8}, Lcom/lge/ims/setting/MediaSettingsCarrier;->summaryChange(Landroid/preference/Preference;Ljava/lang/Object;)V

    #@8a
    .line 331
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->addPayloadFormatList(I)V

    #@8d
    .line 335
    :cond_8d
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@8f
    invoke-static {v7, p1}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$200(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/database/Cursor;

    #@92
    move-result-object v2

    #@93
    .line 337
    .local v2, cursor:Landroid/database/Cursor;
    if-nez v2, :cond_12a

    #@95
    .line 338
    const-string v7, "IMSMediaSettingsCarrier"

    #@97
    new-instance v8, Ljava/lang/StringBuilder;

    #@99
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@9c
    const-string v9, "Cursor :: "

    #@9e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v8

    #@a2
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v8

    #@a6
    const-string v9, " is null"

    #@a8
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v8

    #@ac
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@af
    move-result-object v8

    #@b0
    invoke-static {v7, v8}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@b3
    goto/16 :goto_16

    #@b5
    .line 262
    .end local v2           #cursor:Landroid/database/Cursor;
    :cond_b5
    const/4 v6, 0x2

    #@b6
    goto/16 :goto_32

    #@b8
    .line 266
    :cond_b8
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioCodecList:Ljava/util/ArrayList;

    #@ba
    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@bd
    move-result-object v7

    #@be
    check-cast v7, Ljava/lang/String;

    #@c0
    const-string v8, "AMR-WB"

    #@c2
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c5
    move-result v7

    #@c6
    if-eqz v7, :cond_cb

    #@c8
    .line 267
    const/4 v6, 0x1

    #@c9
    goto/16 :goto_32

    #@cb
    .line 270
    :cond_cb
    const/4 v6, 0x3

    #@cc
    goto/16 :goto_32

    #@ce
    .line 293
    :cond_ce
    const/4 v6, 0x3

    #@cf
    goto :goto_75

    #@d0
    .line 298
    :cond_d0
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioPayloadFormatList:Ljava/util/ArrayList;

    #@d2
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@d5
    move-result v7

    #@d6
    if-le v7, v11, :cond_114

    #@d8
    .line 300
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioPayloadFormatList:Ljava/util/ArrayList;

    #@da
    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@dd
    move-result-object v7

    #@de
    check-cast v7, Ljava/lang/String;

    #@e0
    const-string v8, "BE"

    #@e2
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e5
    move-result v7

    #@e6
    if-eqz v7, :cond_fe

    #@e8
    .line 302
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioPayloadFormatList:Ljava/util/ArrayList;

    #@ea
    invoke-virtual {v7, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@ed
    move-result-object v7

    #@ee
    check-cast v7, Ljava/lang/String;

    #@f0
    const-string v8, "BE"

    #@f2
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f5
    move-result v7

    #@f6
    if-eqz v7, :cond_fb

    #@f8
    .line 303
    const/4 v6, 0x0

    #@f9
    goto/16 :goto_75

    #@fb
    .line 306
    :cond_fb
    const/4 v6, 0x1

    #@fc
    goto/16 :goto_75

    #@fe
    .line 310
    :cond_fe
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioPayloadFormatList:Ljava/util/ArrayList;

    #@100
    invoke-virtual {v7, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@103
    move-result-object v7

    #@104
    check-cast v7, Ljava/lang/String;

    #@106
    const-string v8, "BE"

    #@108
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10b
    move-result v7

    #@10c
    if-eqz v7, :cond_111

    #@10e
    .line 311
    const/4 v6, 0x3

    #@10f
    goto/16 :goto_75

    #@111
    .line 314
    :cond_111
    const/4 v6, 0x2

    #@112
    goto/16 :goto_75

    #@114
    .line 319
    :cond_114
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->m_audioPayloadFormatList:Ljava/util/ArrayList;

    #@116
    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@119
    move-result-object v7

    #@11a
    check-cast v7, Ljava/lang/String;

    #@11c
    const-string v8, "BE"

    #@11e
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@121
    move-result v7

    #@122
    if-eqz v7, :cond_127

    #@124
    .line 320
    const/4 v6, 0x0

    #@125
    goto/16 :goto_75

    #@127
    .line 323
    :cond_127
    const/4 v6, 0x2

    #@128
    goto/16 :goto_75

    #@12a
    .line 342
    .restart local v2       #cursor:Landroid/database/Cursor;
    :cond_12a
    invoke-interface {v2}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    #@12d
    move-result-object v1

    #@12e
    .line 343
    .local v1, columns:[Ljava/lang/String;
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    #@131
    .line 345
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@133
    iget-object v8, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@135
    invoke-virtual {v8, v12}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@138
    move-result-object v8

    #@139
    invoke-static {v7, v8}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$300(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/ListPreference;

    #@13c
    move-result-object v4

    #@13d
    .line 346
    .local v4, list:Landroid/preference/ListPreference;
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@13f
    iget-object v8, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@141
    invoke-virtual {v8, v12}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@144
    move-result-object v8

    #@145
    invoke-static {v7, v1, v8}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$400(Lcom/lge/ims/setting/MediaSettingsCarrier;[Ljava/lang/String;Ljava/lang/String;)I

    #@148
    move-result v3

    #@149
    .line 347
    .local v3, index:I
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@14b
    invoke-static {v7, v2, v3, v4}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$500(Lcom/lge/ims/setting/MediaSettingsCarrier;Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@14e
    .line 349
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    #@151
    .line 351
    const/4 v0, -0x1

    #@152
    .line 353
    .local v0, codecIndex:I
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@154
    iget-object v8, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@156
    const v9, 0x7f0602d4

    #@159
    invoke-virtual {v8, v9}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@15c
    move-result-object v8

    #@15d
    invoke-static {v7, v8}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@160
    move-result-object v5

    #@161
    .line 354
    const-string v7, "AMR"

    #@163
    const-string v8, "BE"

    #@165
    invoke-virtual {p0, p1, v7, v8}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->searchCodecIndex(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@168
    move-result v0

    #@169
    .line 355
    new-instance v7, Ljava/lang/StringBuilder;

    #@16b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@16e
    const-string v8, "audiocodec_"

    #@170
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@173
    move-result-object v7

    #@174
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@177
    move-result-object v7

    #@178
    const-string v8, "_payload_type"

    #@17a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17d
    move-result-object v7

    #@17e
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@181
    move-result-object v7

    #@182
    invoke-virtual {p0, p1, v5, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->loadDBToPreference(Ljava/lang/String;Landroid/preference/Preference;Ljava/lang/String;)Z

    #@185
    .line 357
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@187
    iget-object v8, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@189
    const v9, 0x7f0602d5

    #@18c
    invoke-virtual {v8, v9}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@18f
    move-result-object v8

    #@190
    invoke-static {v7, v8}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@193
    move-result-object v5

    #@194
    .line 358
    new-instance v7, Ljava/lang/StringBuilder;

    #@196
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@199
    const-string v8, "AMR_"

    #@19b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19e
    move-result-object v7

    #@19f
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a2
    move-result-object v7

    #@1a3
    const-string v8, "_mode_set"

    #@1a5
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a8
    move-result-object v7

    #@1a9
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ac
    move-result-object v7

    #@1ad
    invoke-virtual {p0, p1, v5, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->loadDBToPreference(Ljava/lang/String;Landroid/preference/Preference;Ljava/lang/String;)Z

    #@1b0
    .line 360
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@1b2
    iget-object v8, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@1b4
    const v9, 0x7f0602d6

    #@1b7
    invoke-virtual {v8, v9}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@1ba
    move-result-object v8

    #@1bb
    invoke-static {v7, v8}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@1be
    move-result-object v5

    #@1bf
    .line 361
    const-string v7, "AMR"

    #@1c1
    const-string v8, "OA"

    #@1c3
    invoke-virtual {p0, p1, v7, v8}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->searchCodecIndex(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@1c6
    move-result v0

    #@1c7
    .line 362
    new-instance v7, Ljava/lang/StringBuilder;

    #@1c9
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1cc
    const-string v8, "audiocodec_"

    #@1ce
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d1
    move-result-object v7

    #@1d2
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d5
    move-result-object v7

    #@1d6
    const-string v8, "_payload_type"

    #@1d8
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1db
    move-result-object v7

    #@1dc
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1df
    move-result-object v7

    #@1e0
    invoke-virtual {p0, p1, v5, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->loadDBToPreference(Ljava/lang/String;Landroid/preference/Preference;Ljava/lang/String;)Z

    #@1e3
    .line 364
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@1e5
    iget-object v8, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@1e7
    const v9, 0x7f0602d7

    #@1ea
    invoke-virtual {v8, v9}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@1ed
    move-result-object v8

    #@1ee
    invoke-static {v7, v8}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@1f1
    move-result-object v5

    #@1f2
    .line 365
    new-instance v7, Ljava/lang/StringBuilder;

    #@1f4
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1f7
    const-string v8, "AMR_"

    #@1f9
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1fc
    move-result-object v7

    #@1fd
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@200
    move-result-object v7

    #@201
    const-string v8, "_mode_set"

    #@203
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@206
    move-result-object v7

    #@207
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20a
    move-result-object v7

    #@20b
    invoke-virtual {p0, p1, v5, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->loadDBToPreference(Ljava/lang/String;Landroid/preference/Preference;Ljava/lang/String;)Z

    #@20e
    .line 367
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@210
    iget-object v8, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@212
    const v9, 0x7f0602d8

    #@215
    invoke-virtual {v8, v9}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@218
    move-result-object v8

    #@219
    invoke-static {v7, v8}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@21c
    move-result-object v5

    #@21d
    .line 368
    const-string v7, "AMR-WB"

    #@21f
    const-string v8, "BE"

    #@221
    invoke-virtual {p0, p1, v7, v8}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->searchCodecIndex(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@224
    move-result v0

    #@225
    .line 369
    new-instance v7, Ljava/lang/StringBuilder;

    #@227
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@22a
    const-string v8, "audiocodec_"

    #@22c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22f
    move-result-object v7

    #@230
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@233
    move-result-object v7

    #@234
    const-string v8, "_payload_type"

    #@236
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@239
    move-result-object v7

    #@23a
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23d
    move-result-object v7

    #@23e
    invoke-virtual {p0, p1, v5, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->loadDBToPreference(Ljava/lang/String;Landroid/preference/Preference;Ljava/lang/String;)Z

    #@241
    .line 371
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@243
    iget-object v8, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@245
    const v9, 0x7f0602d9

    #@248
    invoke-virtual {v8, v9}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@24b
    move-result-object v8

    #@24c
    invoke-static {v7, v8}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@24f
    move-result-object v5

    #@250
    .line 372
    new-instance v7, Ljava/lang/StringBuilder;

    #@252
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@255
    const-string v8, "AMR_"

    #@257
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25a
    move-result-object v7

    #@25b
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25e
    move-result-object v7

    #@25f
    const-string v8, "_mode_set"

    #@261
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@264
    move-result-object v7

    #@265
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@268
    move-result-object v7

    #@269
    invoke-virtual {p0, p1, v5, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->loadDBToPreference(Ljava/lang/String;Landroid/preference/Preference;Ljava/lang/String;)Z

    #@26c
    .line 374
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@26e
    iget-object v8, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@270
    const v9, 0x7f0602da

    #@273
    invoke-virtual {v8, v9}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@276
    move-result-object v8

    #@277
    invoke-static {v7, v8}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@27a
    move-result-object v5

    #@27b
    .line 375
    const-string v7, "AMR-WB"

    #@27d
    const-string v8, "OA"

    #@27f
    invoke-virtual {p0, p1, v7, v8}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->searchCodecIndex(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@282
    move-result v0

    #@283
    .line 376
    new-instance v7, Ljava/lang/StringBuilder;

    #@285
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@288
    const-string v8, "audiocodec_"

    #@28a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28d
    move-result-object v7

    #@28e
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@291
    move-result-object v7

    #@292
    const-string v8, "_payload_type"

    #@294
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@297
    move-result-object v7

    #@298
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29b
    move-result-object v7

    #@29c
    invoke-virtual {p0, p1, v5, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->loadDBToPreference(Ljava/lang/String;Landroid/preference/Preference;Ljava/lang/String;)Z

    #@29f
    .line 378
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@2a1
    iget-object v8, p0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->this$0:Lcom/lge/ims/setting/MediaSettingsCarrier;

    #@2a3
    const v9, 0x7f0602db

    #@2a6
    invoke-virtual {v8, v9}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getString(I)Ljava/lang/String;

    #@2a9
    move-result-object v8

    #@2aa
    invoke-static {v7, v8}, Lcom/lge/ims/setting/MediaSettingsCarrier;->access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;

    #@2ad
    move-result-object v5

    #@2ae
    .line 379
    new-instance v7, Ljava/lang/StringBuilder;

    #@2b0
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@2b3
    const-string v8, "AMR_"

    #@2b5
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b8
    move-result-object v7

    #@2b9
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2bc
    move-result-object v7

    #@2bd
    const-string v8, "_mode_set"

    #@2bf
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c2
    move-result-object v7

    #@2c3
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c6
    move-result-object v7

    #@2c7
    invoke-virtual {p0, p1, v5, v7}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;->loadDBToPreference(Ljava/lang/String;Landroid/preference/Preference;Ljava/lang/String;)Z

    #@2ca
    goto/16 :goto_16
.end method
