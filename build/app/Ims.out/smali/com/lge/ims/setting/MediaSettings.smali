.class public Lcom/lge/ims/setting/MediaSettings;
.super Landroid/preference/PreferenceActivity;
.source "MediaSettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/setting/MediaSettings$MediaCodecChangeListener;,
        Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;,
        Lcom/lge/ims/setting/MediaSettings$AudioSettingCommand;,
        Lcom/lge/ims/setting/MediaSettings$VideoSettingCommand;,
        Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;,
        Lcom/lge/ims/setting/MediaSettings$IMSMediaCodecInfo;
    }
.end annotation


# static fields
.field private static final KEY_MEDIA_PREFIX:Ljava/lang/String; = ""

.field private static final LGIMS_COM_MEDIA:Ljava/lang/String; = "lgims_com_media"

.field private static final LGIMS_MEDIA_AUDIO:Ljava/lang/String; = "lgims_com_media_audio"

.field private static final LGIMS_MEDIA_CODEC_AUDIO_VOLTE:Ljava/lang/String; = "lgims_com_media_audio_codec_volte"

.field private static final LGIMS_MEDIA_CODEC_AUDIO_VT:Ljava/lang/String; = "lgims_com_media_audio_codec_vt"

.field private static final LGIMS_MEDIA_CODEC_VIDEO_VT:Ljava/lang/String; = "lgims_com_media_video_codec_vt"

.field private static final LGIMS_MEDIA_VIDEO:Ljava/lang/String; = "lgims_com_media_video"

.field private static final SESSION_TYPE_AUDIO_VT:I = 0x1

.field private static final SESSION_TYPE_VIDEO_VT:I = 0x2

.field private static final SESSION_TYPE_VOLTE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "IMSMediaSetting"


# instance fields
.field private imsDB:Landroid/database/sqlite/SQLiteDatabase;

.field private mAdministrativeConfigChanged:Z

.field private onCodecAudioVTListener:Lcom/lge/ims/setting/MediaSettings$MediaCodecChangeListener;

.field private onCodecAudioVoLTEListener:Lcom/lge/ims/setting/MediaSettings$MediaCodecChangeListener;

.field private onCodecVideoVTListener:Lcom/lge/ims/setting/MediaSettings$MediaCodecChangeListener;

.field private onVoIPAudioListener:Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;

.field private onVoIPMediaListener:Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;

.field private onVoIPVideoListener:Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;

.field private rootScreenKey:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 32
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    #@4
    .line 50
    iput-object v1, p0, Lcom/lge/ims/setting/MediaSettings;->onVoIPMediaListener:Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;

    #@6
    .line 51
    iput-object v1, p0, Lcom/lge/ims/setting/MediaSettings;->onVoIPAudioListener:Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;

    #@8
    .line 52
    iput-object v1, p0, Lcom/lge/ims/setting/MediaSettings;->onVoIPVideoListener:Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;

    #@a
    .line 55
    iput-object v1, p0, Lcom/lge/ims/setting/MediaSettings;->onCodecAudioVoLTEListener:Lcom/lge/ims/setting/MediaSettings$MediaCodecChangeListener;

    #@c
    .line 56
    iput-object v1, p0, Lcom/lge/ims/setting/MediaSettings;->onCodecAudioVTListener:Lcom/lge/ims/setting/MediaSettings$MediaCodecChangeListener;

    #@e
    .line 57
    iput-object v1, p0, Lcom/lge/ims/setting/MediaSettings;->onCodecVideoVTListener:Lcom/lge/ims/setting/MediaSettings$MediaCodecChangeListener;

    #@10
    .line 59
    iput-object v1, p0, Lcom/lge/ims/setting/MediaSettings;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@12
    .line 60
    const/4 v0, 0x0

    #@13
    iput-boolean v0, p0, Lcom/lge/ims/setting/MediaSettings;->mAdministrativeConfigChanged:Z

    #@15
    .line 61
    iput-object v1, p0, Lcom/lge/ims/setting/MediaSettings;->rootScreenKey:Ljava/lang/String;

    #@17
    .line 949
    return-void
.end method

.method static synthetic access$000(Lcom/lge/ims/setting/MediaSettings;Ljava/lang/String;)Landroid/preference/ListPreference;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/lge/ims/setting/MediaSettings;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$100(Lcom/lge/ims/setting/MediaSettings;Ljava/lang/String;)Landroid/preference/EditTextPreference;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/lge/ims/setting/MediaSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$200(Lcom/lge/ims/setting/MediaSettings;)Landroid/database/sqlite/SQLiteDatabase;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 32
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettings;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@2
    return-object v0
.end method

.method private displayColumns([Ljava/lang/String;)V
    .registers 6
    .parameter "columns"

    #@0
    .prologue
    .line 1126
    invoke-static {}, Lcom/lge/ims/Configuration;->isDebugOn()Z

    #@3
    move-result v1

    #@4
    if-nez v1, :cond_7

    #@6
    .line 1142
    :cond_6
    return-void

    #@7
    .line 1130
    :cond_7
    if-eqz p1, :cond_6

    #@9
    .line 1134
    const/4 v0, 0x0

    #@a
    .local v0, i:I
    :goto_a
    array-length v1, p1

    #@b
    if-ge v0, v1, :cond_6

    #@d
    .line 1135
    aget-object v1, p1, v0

    #@f
    if-nez v1, :cond_32

    #@11
    .line 1136
    const-string v1, "IMSMediaSetting"

    #@13
    new-instance v2, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v3, "column at index ("

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    const-string v3, ") is null"

    #@24
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v2

    #@2c
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@2f
    .line 1134
    :goto_2f
    add-int/lit8 v0, v0, 0x1

    #@31
    goto :goto_a

    #@32
    .line 1140
    :cond_32
    const-string v1, "IMSMediaSetting"

    #@34
    new-instance v2, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v3, "Column at index ("

    #@3b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@42
    move-result-object v2

    #@43
    const-string v3, ")"

    #@45
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v2

    #@49
    aget-object v3, p1, v0

    #@4b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v2

    #@4f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v2

    #@53
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@56
    goto :goto_2f
.end method

.method private getCheckBoxPreference(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 1181
    invoke-virtual {p0}, Lcom/lge/ims/setting/MediaSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    #@3
    move-result-object v0

    #@4
    .line 1183
    .local v0, root:Landroid/preference/PreferenceScreen;
    if-eqz v0, :cond_d

    #@6
    .line 1184
    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    #@9
    move-result-object v1

    #@a
    check-cast v1, Landroid/preference/CheckBoxPreference;

    #@c
    .line 1187
    :goto_c
    return-object v1

    #@d
    :cond_d
    const/4 v1, 0x0

    #@e
    goto :goto_c
.end method

.method private getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I
    .registers 5
    .parameter "columns"
    .parameter "column"

    #@0
    .prologue
    .line 1171
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    array-length v1, p1

    #@2
    if-ge v0, v1, :cond_10

    #@4
    .line 1172
    aget-object v1, p1, v0

    #@6
    invoke-virtual {p2, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@9
    move-result v1

    #@a
    if-nez v1, :cond_d

    #@c
    .line 1177
    .end local v0           #i:I
    :goto_c
    return v0

    #@d
    .line 1171
    .restart local v0       #i:I
    :cond_d
    add-int/lit8 v0, v0, 0x1

    #@f
    goto :goto_1

    #@10
    .line 1177
    :cond_10
    const/4 v0, -0x1

    #@11
    goto :goto_c
.end method

.method private getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 1201
    invoke-virtual {p0}, Lcom/lge/ims/setting/MediaSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    #@3
    move-result-object v0

    #@4
    .line 1203
    .local v0, root:Landroid/preference/PreferenceScreen;
    if-eqz v0, :cond_d

    #@6
    .line 1204
    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    #@9
    move-result-object v1

    #@a
    check-cast v1, Landroid/preference/EditTextPreference;

    #@c
    .line 1207
    :goto_c
    return-object v1

    #@d
    :cond_d
    const/4 v1, 0x0

    #@e
    goto :goto_c
.end method

.method private getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 1191
    invoke-virtual {p0}, Lcom/lge/ims/setting/MediaSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    #@3
    move-result-object v0

    #@4
    .line 1193
    .local v0, root:Landroid/preference/PreferenceScreen;
    if-eqz v0, :cond_d

    #@6
    .line 1194
    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    #@9
    move-result-object v1

    #@a
    check-cast v1, Landroid/preference/ListPreference;

    #@c
    .line 1197
    :goto_c
    return-object v1

    #@d
    :cond_d
    const/4 v1, 0x0

    #@e
    goto :goto_c
.end method

.method private getPreference(Ljava/lang/String;)Landroid/preference/Preference;
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 1211
    invoke-virtual {p0}, Lcom/lge/ims/setting/MediaSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    #@3
    move-result-object v0

    #@4
    .line 1213
    .local v0, root:Landroid/preference/PreferenceScreen;
    if-eqz v0, :cond_b

    #@6
    .line 1214
    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    #@9
    move-result-object v1

    #@a
    .line 1217
    :goto_a
    return-object v1

    #@b
    :cond_b
    const/4 v1, 0x0

    #@c
    goto :goto_a
.end method

.method private getSettingsTable(Ljava/lang/String;)Landroid/database/Cursor;
    .registers 7
    .parameter "table"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1145
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettings;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 1167
    :cond_5
    :goto_5
    return-object v0

    #@6
    .line 1149
    :cond_6
    if-eqz p1, :cond_5

    #@8
    .line 1153
    const/4 v0, 0x0

    #@9
    .line 1156
    .local v0, cursor:Landroid/database/Cursor;
    :try_start_9
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettings;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@b
    new-instance v3, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v4, "select * from "

    #@12
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v3

    #@1e
    const/4 v4, 0x0

    #@1f
    invoke-virtual {v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_22
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_9 .. :try_end_22} :catch_24

    #@22
    move-result-object v0

    #@23
    goto :goto_5

    #@24
    .line 1157
    :catch_24
    move-exception v1

    #@25
    .line 1158
    .local v1, e:Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    #@28
    .line 1160
    if-eqz v0, :cond_2d

    #@2a
    .line 1161
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    #@2d
    .line 1164
    :cond_2d
    const/4 v0, 0x0

    #@2e
    goto :goto_5
.end method

.method private registerMediaAudioVTChangeListener(Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;)V
    .registers 16
    .parameter "listener"

    #@0
    .prologue
    const v13, 0x7f060189

    #@3
    const v12, 0x7f060186

    #@6
    const v11, 0x7f060185

    #@9
    const v10, 0x7f060184

    #@c
    const v9, 0x7f060187

    #@f
    .line 270
    invoke-virtual {p1}, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->getTableName()Ljava/lang/String;

    #@12
    move-result-object v7

    #@13
    invoke-direct {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getSettingsTable(Ljava/lang/String;)Landroid/database/Cursor;

    #@16
    move-result-object v2

    #@17
    .line 272
    .local v2, cursor:Landroid/database/Cursor;
    if-nez v2, :cond_3c

    #@19
    .line 273
    const-string v7, "IMSMediaSetting"

    #@1b
    new-instance v8, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v9, "Cursor :: "

    #@22
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v8

    #@26
    invoke-virtual {p1}, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->getTableName()Ljava/lang/String;

    #@29
    move-result-object v9

    #@2a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v8

    #@2e
    const-string v9, " is null"

    #@30
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v8

    #@34
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v8

    #@38
    invoke-static {v7, v8}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@3b
    .line 375
    :goto_3b
    return-void

    #@3c
    .line 277
    :cond_3c
    invoke-interface {v2}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    #@3f
    move-result-object v1

    #@40
    .line 279
    .local v1, columns:[Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/lge/ims/setting/MediaSettings;->displayColumns([Ljava/lang/String;)V

    #@43
    .line 282
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    #@46
    .line 284
    invoke-virtual {p1}, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->getPrefix()Ljava/lang/String;

    #@49
    move-result-object v6

    #@4a
    .line 286
    .local v6, prefix:Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    #@4c
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@4f
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v7

    #@53
    invoke-virtual {p0, v10}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@56
    move-result-object v8

    #@57
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v7

    #@5b
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v7

    #@5f
    invoke-direct {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@62
    move-result-object v3

    #@63
    .line 287
    .local v3, edit:Landroid/preference/EditTextPreference;
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@66
    .line 288
    invoke-virtual {p0, v10}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@69
    move-result-object v7

    #@6a
    invoke-direct {p0, v1, v7}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@6d
    move-result v4

    #@6e
    .line 289
    .local v4, index:I
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@71
    .line 291
    new-instance v7, Ljava/lang/StringBuilder;

    #@73
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v7

    #@7a
    invoke-virtual {p0, v11}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@7d
    move-result-object v8

    #@7e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v7

    #@82
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v7

    #@86
    invoke-direct {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@89
    move-result-object v5

    #@8a
    .line 292
    .local v5, list:Landroid/preference/ListPreference;
    invoke-direct {p0, v5, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@8d
    .line 293
    invoke-virtual {p0, v11}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@90
    move-result-object v7

    #@91
    invoke-direct {p0, v1, v7}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@94
    move-result v4

    #@95
    .line 294
    invoke-direct {p0, v2, v4, v5}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@98
    .line 296
    new-instance v7, Ljava/lang/StringBuilder;

    #@9a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@9d
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v7

    #@a1
    invoke-virtual {p0, v12}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@a4
    move-result-object v8

    #@a5
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v7

    #@a9
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ac
    move-result-object v7

    #@ad
    invoke-direct {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@b0
    move-result-object v5

    #@b1
    .line 297
    invoke-direct {p0, v5, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@b4
    .line 298
    invoke-virtual {p0, v12}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@b7
    move-result-object v7

    #@b8
    invoke-direct {p0, v1, v7}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@bb
    move-result v4

    #@bc
    .line 299
    invoke-direct {p0, v2, v4, v5}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@bf
    .line 301
    new-instance v7, Ljava/lang/StringBuilder;

    #@c1
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@c4
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v7

    #@c8
    const v8, 0x7f06018c

    #@cb
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@ce
    move-result-object v8

    #@cf
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v7

    #@d3
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d6
    move-result-object v7

    #@d7
    invoke-direct {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@da
    move-result-object v3

    #@db
    .line 302
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@de
    .line 303
    const v7, 0x7f06018c

    #@e1
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@e4
    move-result-object v7

    #@e5
    invoke-direct {p0, v1, v7}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@e8
    move-result v4

    #@e9
    .line 304
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@ec
    .line 306
    new-instance v7, Ljava/lang/StringBuilder;

    #@ee
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@f1
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v7

    #@f5
    const v8, 0x7f06018d

    #@f8
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@fb
    move-result-object v8

    #@fc
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v7

    #@100
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@103
    move-result-object v7

    #@104
    invoke-direct {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@107
    move-result-object v3

    #@108
    .line 307
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@10b
    .line 308
    const v7, 0x7f06018d

    #@10e
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@111
    move-result-object v7

    #@112
    invoke-direct {p0, v1, v7}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@115
    move-result v4

    #@116
    .line 309
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@119
    .line 311
    new-instance v7, Ljava/lang/StringBuilder;

    #@11b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@11e
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@121
    move-result-object v7

    #@122
    const v8, 0x7f06018e

    #@125
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@128
    move-result-object v8

    #@129
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12c
    move-result-object v7

    #@12d
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@130
    move-result-object v7

    #@131
    invoke-direct {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@134
    move-result-object v3

    #@135
    .line 312
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@138
    .line 313
    const v7, 0x7f06018e

    #@13b
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@13e
    move-result-object v7

    #@13f
    invoke-direct {p0, v1, v7}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@142
    move-result v4

    #@143
    .line 314
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@146
    .line 316
    new-instance v7, Ljava/lang/StringBuilder;

    #@148
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@14b
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14e
    move-result-object v7

    #@14f
    invoke-virtual {p0, v9}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@152
    move-result-object v8

    #@153
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@156
    move-result-object v7

    #@157
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15a
    move-result-object v7

    #@15b
    invoke-direct {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@15e
    move-result-object v5

    #@15f
    .line 317
    invoke-direct {p0, v5, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@162
    .line 318
    invoke-virtual {p0, v9}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@165
    move-result-object v7

    #@166
    invoke-direct {p0, v1, v7}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@169
    move-result v4

    #@16a
    .line 319
    invoke-direct {p0, v2, v4, v5}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@16d
    .line 321
    new-instance v7, Ljava/lang/StringBuilder;

    #@16f
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@172
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@175
    move-result-object v7

    #@176
    const v8, 0x7f060188

    #@179
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@17c
    move-result-object v8

    #@17d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@180
    move-result-object v7

    #@181
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@184
    move-result-object v7

    #@185
    invoke-direct {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@188
    move-result-object v5

    #@189
    .line 322
    invoke-direct {p0, v5, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@18c
    .line 323
    invoke-virtual {p0, v9}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@18f
    move-result-object v7

    #@190
    invoke-direct {p0, v1, v7}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@193
    move-result v4

    #@194
    .line 324
    invoke-direct {p0, v2, v4, v5}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@197
    .line 326
    new-instance v7, Ljava/lang/StringBuilder;

    #@199
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@19c
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19f
    move-result-object v7

    #@1a0
    invoke-virtual {p0, v13}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@1a3
    move-result-object v8

    #@1a4
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a7
    move-result-object v7

    #@1a8
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ab
    move-result-object v7

    #@1ac
    invoke-direct {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@1af
    move-result-object v5

    #@1b0
    .line 327
    invoke-direct {p0, v5, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@1b3
    .line 328
    invoke-virtual {p0, v13}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@1b6
    move-result-object v7

    #@1b7
    invoke-direct {p0, v1, v7}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@1ba
    move-result v4

    #@1bb
    .line 329
    invoke-direct {p0, v2, v4, v5}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@1be
    .line 331
    new-instance v7, Ljava/lang/StringBuilder;

    #@1c0
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1c3
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c6
    move-result-object v7

    #@1c7
    const v8, 0x7f06018a

    #@1ca
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@1cd
    move-result-object v8

    #@1ce
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d1
    move-result-object v7

    #@1d2
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d5
    move-result-object v7

    #@1d6
    invoke-direct {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@1d9
    move-result-object v5

    #@1da
    .line 332
    invoke-direct {p0, v5, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@1dd
    .line 333
    const v7, 0x7f06018a

    #@1e0
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@1e3
    move-result-object v7

    #@1e4
    invoke-direct {p0, v1, v7}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@1e7
    move-result v4

    #@1e8
    .line 334
    invoke-direct {p0, v2, v4, v5}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@1eb
    .line 337
    const/4 v0, 0x0

    #@1ec
    .line 339
    .local v0, bwAS:Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    #@1ee
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1f1
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f4
    move-result-object v7

    #@1f5
    const v8, 0x7f060191

    #@1f8
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@1fb
    move-result-object v8

    #@1fc
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ff
    move-result-object v7

    #@200
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@203
    move-result-object v7

    #@204
    invoke-direct {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@207
    move-result-object v5

    #@208
    .line 340
    invoke-direct {p0, v5, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@20b
    .line 341
    const v7, 0x7f060191

    #@20e
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@211
    move-result-object v7

    #@212
    invoke-direct {p0, v1, v7}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@215
    move-result v4

    #@216
    .line 342
    invoke-direct {p0, v2, v4, v5}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@219
    .line 344
    new-instance v7, Ljava/lang/StringBuilder;

    #@21b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@21e
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@221
    move-result-object v7

    #@222
    const v8, 0x7f060192

    #@225
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@228
    move-result-object v8

    #@229
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22c
    move-result-object v7

    #@22d
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@230
    move-result-object v7

    #@231
    invoke-direct {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@234
    move-result-object v3

    #@235
    .line 345
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@238
    .line 346
    const v7, 0x7f060192

    #@23b
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@23e
    move-result-object v7

    #@23f
    invoke-direct {p0, v1, v7}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@242
    move-result v4

    #@243
    .line 347
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@246
    .line 348
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/MediaSettings;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@249
    .line 352
    new-instance v7, Ljava/lang/StringBuilder;

    #@24b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@24e
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@251
    move-result-object v7

    #@252
    const v8, 0x7f060195

    #@255
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@258
    move-result-object v8

    #@259
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25c
    move-result-object v7

    #@25d
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@260
    move-result-object v7

    #@261
    invoke-direct {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@264
    move-result-object v5

    #@265
    .line 353
    invoke-direct {p0, v5, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@268
    .line 354
    const v7, 0x7f060195

    #@26b
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@26e
    move-result-object v7

    #@26f
    invoke-direct {p0, v1, v7}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@272
    move-result v4

    #@273
    .line 355
    invoke-direct {p0, v2, v4, v5}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@276
    .line 357
    new-instance v7, Ljava/lang/StringBuilder;

    #@278
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@27b
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27e
    move-result-object v7

    #@27f
    const v8, 0x7f06018b

    #@282
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@285
    move-result-object v8

    #@286
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@289
    move-result-object v7

    #@28a
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28d
    move-result-object v7

    #@28e
    invoke-direct {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@291
    move-result-object v3

    #@292
    .line 358
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@295
    .line 359
    const v7, 0x7f06018b

    #@298
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@29b
    move-result-object v7

    #@29c
    invoke-direct {p0, v1, v7}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@29f
    move-result v4

    #@2a0
    .line 360
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@2a3
    .line 362
    new-instance v7, Ljava/lang/StringBuilder;

    #@2a5
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@2a8
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ab
    move-result-object v7

    #@2ac
    const v8, 0x7f060196

    #@2af
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@2b2
    move-result-object v8

    #@2b3
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b6
    move-result-object v7

    #@2b7
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2ba
    move-result-object v7

    #@2bb
    invoke-direct {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@2be
    move-result-object v5

    #@2bf
    .line 363
    invoke-direct {p0, v5, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@2c2
    .line 364
    const v7, 0x7f060196

    #@2c5
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@2c8
    move-result-object v7

    #@2c9
    invoke-direct {p0, v1, v7}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@2cc
    move-result v4

    #@2cd
    .line 365
    invoke-direct {p0, v2, v4, v5}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@2d0
    .line 367
    new-instance v7, Ljava/lang/StringBuilder;

    #@2d2
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@2d5
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d8
    move-result-object v7

    #@2d9
    const v8, 0x7f060190

    #@2dc
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@2df
    move-result-object v8

    #@2e0
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e3
    move-result-object v7

    #@2e4
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e7
    move-result-object v7

    #@2e8
    invoke-direct {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@2eb
    move-result-object v3

    #@2ec
    .line 368
    invoke-direct {p0, v3, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@2ef
    .line 369
    const v7, 0x7f060190

    #@2f2
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@2f5
    move-result-object v7

    #@2f6
    invoke-direct {p0, v1, v7}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@2f9
    move-result v4

    #@2fa
    .line 370
    invoke-direct {p0, v2, v4, v3}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@2fd
    .line 371
    invoke-direct {p0, v3}, Lcom/lge/ims/setting/MediaSettings;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@300
    .line 374
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    #@303
    goto/16 :goto_3b
.end method

.method private registerMediaAudioVoLTEChangeListener(Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;)V
    .registers 15
    .parameter "listener"

    #@0
    .prologue
    const v12, 0x7f060173

    #@3
    const v11, 0x7f060172

    #@6
    const v10, 0x7f060171

    #@9
    const v9, 0x7f060170

    #@c
    const v8, 0x7f06016f

    #@f
    .line 163
    invoke-virtual {p1}, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->getTableName()Ljava/lang/String;

    #@12
    move-result-object v6

    #@13
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getSettingsTable(Ljava/lang/String;)Landroid/database/Cursor;

    #@16
    move-result-object v1

    #@17
    .line 165
    .local v1, cursor:Landroid/database/Cursor;
    if-nez v1, :cond_3c

    #@19
    .line 166
    const-string v6, "IMSMediaSetting"

    #@1b
    new-instance v7, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v8, "Cursor :: "

    #@22
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v7

    #@26
    invoke-virtual {p1}, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->getTableName()Ljava/lang/String;

    #@29
    move-result-object v8

    #@2a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v7

    #@2e
    const-string v8, " is null"

    #@30
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v7

    #@34
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v7

    #@38
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@3b
    .line 263
    :goto_3b
    return-void

    #@3c
    .line 170
    :cond_3c
    invoke-interface {v1}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    #@3f
    move-result-object v0

    #@40
    .line 172
    .local v0, columns:[Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/lge/ims/setting/MediaSettings;->displayColumns([Ljava/lang/String;)V

    #@43
    .line 175
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    #@46
    .line 177
    invoke-virtual {p1}, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->getPrefix()Ljava/lang/String;

    #@49
    move-result-object v5

    #@4a
    .line 179
    .local v5, prefix:Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    #@4c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@4f
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v6

    #@53
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@56
    move-result-object v7

    #@57
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v6

    #@5b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v6

    #@5f
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@62
    move-result-object v2

    #@63
    .line 180
    .local v2, edit:Landroid/preference/EditTextPreference;
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@66
    .line 181
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@69
    move-result-object v6

    #@6a
    invoke-direct {p0, v0, v6}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@6d
    move-result v3

    #@6e
    .line 182
    .local v3, index:I
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@71
    .line 184
    new-instance v6, Ljava/lang/StringBuilder;

    #@73
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v6

    #@7a
    invoke-virtual {p0, v9}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@7d
    move-result-object v7

    #@7e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v6

    #@82
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v6

    #@86
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@89
    move-result-object v4

    #@8a
    .line 185
    .local v4, list:Landroid/preference/ListPreference;
    invoke-direct {p0, v4, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@8d
    .line 186
    invoke-virtual {p0, v9}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@90
    move-result-object v6

    #@91
    invoke-direct {p0, v0, v6}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@94
    move-result v3

    #@95
    .line 187
    invoke-direct {p0, v1, v3, v4}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@98
    .line 189
    new-instance v6, Ljava/lang/StringBuilder;

    #@9a
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@9d
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v6

    #@a1
    invoke-virtual {p0, v10}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@a4
    move-result-object v7

    #@a5
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v6

    #@a9
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ac
    move-result-object v6

    #@ad
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@b0
    move-result-object v4

    #@b1
    .line 190
    invoke-direct {p0, v4, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@b4
    .line 191
    invoke-virtual {p0, v10}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@b7
    move-result-object v6

    #@b8
    invoke-direct {p0, v0, v6}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@bb
    move-result v3

    #@bc
    .line 192
    invoke-direct {p0, v1, v3, v4}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@bf
    .line 194
    new-instance v6, Ljava/lang/StringBuilder;

    #@c1
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@c4
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v6

    #@c8
    const v7, 0x7f060177

    #@cb
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@ce
    move-result-object v7

    #@cf
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v6

    #@d3
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d6
    move-result-object v6

    #@d7
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@da
    move-result-object v2

    #@db
    .line 195
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@de
    .line 196
    const v6, 0x7f060177

    #@e1
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@e4
    move-result-object v6

    #@e5
    invoke-direct {p0, v0, v6}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@e8
    move-result v3

    #@e9
    .line 197
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@ec
    .line 199
    new-instance v6, Ljava/lang/StringBuilder;

    #@ee
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@f1
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v6

    #@f5
    const v7, 0x7f060178

    #@f8
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@fb
    move-result-object v7

    #@fc
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v6

    #@100
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@103
    move-result-object v6

    #@104
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@107
    move-result-object v2

    #@108
    .line 200
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@10b
    .line 201
    const v6, 0x7f060178

    #@10e
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@111
    move-result-object v6

    #@112
    invoke-direct {p0, v0, v6}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@115
    move-result v3

    #@116
    .line 202
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@119
    .line 204
    new-instance v6, Ljava/lang/StringBuilder;

    #@11b
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@11e
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@121
    move-result-object v6

    #@122
    const v7, 0x7f060179

    #@125
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@128
    move-result-object v7

    #@129
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12c
    move-result-object v6

    #@12d
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@130
    move-result-object v6

    #@131
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@134
    move-result-object v2

    #@135
    .line 205
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@138
    .line 206
    const v6, 0x7f060179

    #@13b
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@13e
    move-result-object v6

    #@13f
    invoke-direct {p0, v0, v6}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@142
    move-result v3

    #@143
    .line 207
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@146
    .line 209
    new-instance v6, Ljava/lang/StringBuilder;

    #@148
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@14b
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14e
    move-result-object v6

    #@14f
    invoke-virtual {p0, v11}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@152
    move-result-object v7

    #@153
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@156
    move-result-object v6

    #@157
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15a
    move-result-object v6

    #@15b
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@15e
    move-result-object v4

    #@15f
    .line 210
    invoke-direct {p0, v4, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@162
    .line 211
    invoke-virtual {p0, v11}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@165
    move-result-object v6

    #@166
    invoke-direct {p0, v0, v6}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@169
    move-result v3

    #@16a
    .line 212
    invoke-direct {p0, v1, v3, v4}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@16d
    .line 214
    new-instance v6, Ljava/lang/StringBuilder;

    #@16f
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@172
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@175
    move-result-object v6

    #@176
    invoke-virtual {p0, v12}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@179
    move-result-object v7

    #@17a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17d
    move-result-object v6

    #@17e
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@181
    move-result-object v6

    #@182
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@185
    move-result-object v4

    #@186
    .line 215
    invoke-direct {p0, v4, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@189
    .line 216
    invoke-virtual {p0, v12}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@18c
    move-result-object v6

    #@18d
    invoke-direct {p0, v0, v6}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@190
    move-result v3

    #@191
    .line 217
    invoke-direct {p0, v1, v3, v4}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@194
    .line 219
    new-instance v6, Ljava/lang/StringBuilder;

    #@196
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@199
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19c
    move-result-object v6

    #@19d
    const v7, 0x7f060174

    #@1a0
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@1a3
    move-result-object v7

    #@1a4
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a7
    move-result-object v6

    #@1a8
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ab
    move-result-object v6

    #@1ac
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@1af
    move-result-object v4

    #@1b0
    .line 220
    invoke-direct {p0, v4, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@1b3
    .line 221
    const v6, 0x7f060174

    #@1b6
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@1b9
    move-result-object v6

    #@1ba
    invoke-direct {p0, v0, v6}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@1bd
    move-result v3

    #@1be
    .line 222
    invoke-direct {p0, v1, v3, v4}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@1c1
    .line 224
    new-instance v6, Ljava/lang/StringBuilder;

    #@1c3
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@1c6
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c9
    move-result-object v6

    #@1ca
    const v7, 0x7f060175

    #@1cd
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@1d0
    move-result-object v7

    #@1d1
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d4
    move-result-object v6

    #@1d5
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d8
    move-result-object v6

    #@1d9
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@1dc
    move-result-object v4

    #@1dd
    .line 225
    invoke-direct {p0, v4, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@1e0
    .line 226
    const v6, 0x7f060175

    #@1e3
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@1e6
    move-result-object v6

    #@1e7
    invoke-direct {p0, v0, v6}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@1ea
    move-result v3

    #@1eb
    .line 227
    invoke-direct {p0, v1, v3, v4}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@1ee
    .line 229
    new-instance v6, Ljava/lang/StringBuilder;

    #@1f0
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@1f3
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f6
    move-result-object v6

    #@1f7
    const v7, 0x7f06017c

    #@1fa
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@1fd
    move-result-object v7

    #@1fe
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@201
    move-result-object v6

    #@202
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@205
    move-result-object v6

    #@206
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@209
    move-result-object v4

    #@20a
    .line 230
    invoke-direct {p0, v4, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@20d
    .line 231
    const v6, 0x7f06017c

    #@210
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@213
    move-result-object v6

    #@214
    invoke-direct {p0, v0, v6}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@217
    move-result v3

    #@218
    .line 232
    invoke-direct {p0, v1, v3, v4}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@21b
    .line 234
    new-instance v6, Ljava/lang/StringBuilder;

    #@21d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@220
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@223
    move-result-object v6

    #@224
    const v7, 0x7f06017d

    #@227
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@22a
    move-result-object v7

    #@22b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22e
    move-result-object v6

    #@22f
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@232
    move-result-object v6

    #@233
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@236
    move-result-object v2

    #@237
    .line 235
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@23a
    .line 236
    const v6, 0x7f06017d

    #@23d
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@240
    move-result-object v6

    #@241
    invoke-direct {p0, v0, v6}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@244
    move-result v3

    #@245
    .line 237
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@248
    .line 238
    invoke-direct {p0, v2}, Lcom/lge/ims/setting/MediaSettings;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@24b
    .line 240
    new-instance v6, Ljava/lang/StringBuilder;

    #@24d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@250
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@253
    move-result-object v6

    #@254
    const v7, 0x7f060180

    #@257
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@25a
    move-result-object v7

    #@25b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25e
    move-result-object v6

    #@25f
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@262
    move-result-object v6

    #@263
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@266
    move-result-object v4

    #@267
    .line 241
    invoke-direct {p0, v4, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@26a
    .line 242
    const v6, 0x7f060180

    #@26d
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@270
    move-result-object v6

    #@271
    invoke-direct {p0, v0, v6}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@274
    move-result v3

    #@275
    .line 243
    invoke-direct {p0, v1, v3, v4}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@278
    .line 245
    new-instance v6, Ljava/lang/StringBuilder;

    #@27a
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@27d
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@280
    move-result-object v6

    #@281
    const v7, 0x7f060176

    #@284
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@287
    move-result-object v7

    #@288
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28b
    move-result-object v6

    #@28c
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28f
    move-result-object v6

    #@290
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@293
    move-result-object v2

    #@294
    .line 246
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@297
    .line 247
    const v6, 0x7f060176

    #@29a
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@29d
    move-result-object v6

    #@29e
    invoke-direct {p0, v0, v6}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@2a1
    move-result v3

    #@2a2
    .line 248
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@2a5
    .line 250
    new-instance v6, Ljava/lang/StringBuilder;

    #@2a7
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@2aa
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ad
    move-result-object v6

    #@2ae
    const v7, 0x7f060181

    #@2b1
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@2b4
    move-result-object v7

    #@2b5
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b8
    move-result-object v6

    #@2b9
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2bc
    move-result-object v6

    #@2bd
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@2c0
    move-result-object v4

    #@2c1
    .line 251
    invoke-direct {p0, v4, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@2c4
    .line 252
    const v6, 0x7f060181

    #@2c7
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@2ca
    move-result-object v6

    #@2cb
    invoke-direct {p0, v0, v6}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@2ce
    move-result v3

    #@2cf
    .line 253
    invoke-direct {p0, v1, v3, v4}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@2d2
    .line 255
    new-instance v6, Ljava/lang/StringBuilder;

    #@2d4
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@2d7
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2da
    move-result-object v6

    #@2db
    const v7, 0x7f06017b

    #@2de
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@2e1
    move-result-object v7

    #@2e2
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e5
    move-result-object v6

    #@2e6
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e9
    move-result-object v6

    #@2ea
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@2ed
    move-result-object v2

    #@2ee
    .line 256
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@2f1
    .line 257
    const v6, 0x7f06017b

    #@2f4
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@2f7
    move-result-object v6

    #@2f8
    invoke-direct {p0, v0, v6}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@2fb
    move-result v3

    #@2fc
    .line 258
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@2ff
    .line 259
    invoke-direct {p0, v2}, Lcom/lge/ims/setting/MediaSettings;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@302
    .line 262
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    #@305
    goto/16 :goto_3b
.end method

.method private registerMediaChangeListener(Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;)V
    .registers 10
    .parameter "listener"

    #@0
    .prologue
    const v7, 0x7f06016e

    #@3
    .line 133
    invoke-virtual {p1}, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->getTableName()Ljava/lang/String;

    #@6
    move-result-object v5

    #@7
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/MediaSettings;->getSettingsTable(Ljava/lang/String;)Landroid/database/Cursor;

    #@a
    move-result-object v2

    #@b
    .line 135
    .local v2, cursor:Landroid/database/Cursor;
    if-nez v2, :cond_30

    #@d
    .line 136
    const-string v5, "IMSMediaSetting"

    #@f
    new-instance v6, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v7, "Cursor :: "

    #@16
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v6

    #@1a
    invoke-virtual {p1}, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->getTableName()Ljava/lang/String;

    #@1d
    move-result-object v7

    #@1e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v6

    #@22
    const-string v7, " is null"

    #@24
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v6

    #@28
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v6

    #@2c
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@2f
    .line 156
    :goto_2f
    return-void

    #@30
    .line 140
    :cond_30
    invoke-interface {v2}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    #@33
    move-result-object v1

    #@34
    .line 142
    .local v1, columns:[Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/lge/ims/setting/MediaSettings;->displayColumns([Ljava/lang/String;)V

    #@37
    .line 145
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    #@3a
    .line 147
    invoke-virtual {p1}, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->getPrefix()Ljava/lang/String;

    #@3d
    move-result-object v4

    #@3e
    .line 149
    .local v4, prefix:Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v5

    #@47
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@4a
    move-result-object v6

    #@4b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v5

    #@4f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v5

    #@53
    invoke-direct {p0, v5}, Lcom/lge/ims/setting/MediaSettings;->getCheckBoxPreference(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    #@56
    move-result-object v0

    #@57
    .line 150
    .local v0, checkbox:Landroid/preference/CheckBoxPreference;
    invoke-direct {p0, v0, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@5a
    .line 151
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@5d
    move-result-object v5

    #@5e
    invoke-direct {p0, v1, v5}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@61
    move-result v3

    #@62
    .line 152
    .local v3, index:I
    invoke-direct {p0, v2, v3, v0}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/CheckBoxPreference;)V

    #@65
    .line 155
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    #@68
    goto :goto_2f
.end method

.method private registerMediaVideoChangeListener(Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;)V
    .registers 15
    .parameter "listener"

    #@0
    .prologue
    const v12, 0x7f06019d

    #@3
    const v11, 0x7f06019c

    #@6
    const v10, 0x7f06019b

    #@9
    const v9, 0x7f06019a

    #@c
    const v8, 0x7f060199

    #@f
    .line 382
    invoke-virtual {p1}, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->getTableName()Ljava/lang/String;

    #@12
    move-result-object v6

    #@13
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getSettingsTable(Ljava/lang/String;)Landroid/database/Cursor;

    #@16
    move-result-object v1

    #@17
    .line 384
    .local v1, cursor:Landroid/database/Cursor;
    if-nez v1, :cond_3c

    #@19
    .line 385
    const-string v6, "IMSMediaSetting"

    #@1b
    new-instance v7, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v8, "Cursor :: "

    #@22
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v7

    #@26
    invoke-virtual {p1}, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->getTableName()Ljava/lang/String;

    #@29
    move-result-object v8

    #@2a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v7

    #@2e
    const-string v8, " is null"

    #@30
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v7

    #@34
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v7

    #@38
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@3b
    .line 460
    :goto_3b
    return-void

    #@3c
    .line 389
    :cond_3c
    invoke-interface {v1}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    #@3f
    move-result-object v0

    #@40
    .line 391
    .local v0, columns:[Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/lge/ims/setting/MediaSettings;->displayColumns([Ljava/lang/String;)V

    #@43
    .line 394
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    #@46
    .line 396
    invoke-virtual {p1}, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;->getPrefix()Ljava/lang/String;

    #@49
    move-result-object v5

    #@4a
    .line 398
    .local v5, prefix:Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    #@4c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@4f
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v6

    #@53
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@56
    move-result-object v7

    #@57
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v6

    #@5b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v6

    #@5f
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@62
    move-result-object v4

    #@63
    .line 399
    .local v4, list:Landroid/preference/ListPreference;
    invoke-direct {p0, v4, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@66
    .line 400
    invoke-virtual {p0, v8}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@69
    move-result-object v6

    #@6a
    invoke-direct {p0, v0, v6}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@6d
    move-result v3

    #@6e
    .line 401
    .local v3, index:I
    invoke-direct {p0, v1, v3, v4}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@71
    .line 402
    new-instance v6, Ljava/lang/StringBuilder;

    #@73
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v6

    #@7a
    invoke-virtual {p0, v9}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@7d
    move-result-object v7

    #@7e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v6

    #@82
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v6

    #@86
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@89
    move-result-object v4

    #@8a
    .line 403
    invoke-direct {p0, v4, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@8d
    .line 404
    invoke-virtual {p0, v9}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@90
    move-result-object v6

    #@91
    invoke-direct {p0, v0, v6}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@94
    move-result v3

    #@95
    .line 405
    invoke-direct {p0, v1, v3, v4}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@98
    .line 406
    new-instance v6, Ljava/lang/StringBuilder;

    #@9a
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@9d
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v6

    #@a1
    invoke-virtual {p0, v10}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@a4
    move-result-object v7

    #@a5
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v6

    #@a9
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ac
    move-result-object v6

    #@ad
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@b0
    move-result-object v2

    #@b1
    .line 407
    .local v2, edit:Landroid/preference/EditTextPreference;
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@b4
    .line 408
    invoke-virtual {p0, v10}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@b7
    move-result-object v6

    #@b8
    invoke-direct {p0, v0, v6}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@bb
    move-result v3

    #@bc
    .line 409
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@bf
    .line 412
    new-instance v6, Ljava/lang/StringBuilder;

    #@c1
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@c4
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v6

    #@c8
    const v7, 0x7f0601a5

    #@cb
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@ce
    move-result-object v7

    #@cf
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v6

    #@d3
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d6
    move-result-object v6

    #@d7
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@da
    move-result-object v4

    #@db
    .line 413
    invoke-direct {p0, v4, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@de
    .line 414
    const v6, 0x7f0601a5

    #@e1
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@e4
    move-result-object v6

    #@e5
    invoke-direct {p0, v0, v6}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@e8
    move-result v3

    #@e9
    .line 415
    invoke-direct {p0, v1, v3, v4}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@ec
    .line 417
    new-instance v6, Ljava/lang/StringBuilder;

    #@ee
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@f1
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v6

    #@f5
    invoke-virtual {p0, v11}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@f8
    move-result-object v7

    #@f9
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fc
    move-result-object v6

    #@fd
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@100
    move-result-object v6

    #@101
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@104
    move-result-object v4

    #@105
    .line 418
    invoke-direct {p0, v4, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@108
    .line 419
    invoke-virtual {p0, v11}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@10b
    move-result-object v6

    #@10c
    invoke-direct {p0, v0, v6}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@10f
    move-result v3

    #@110
    .line 420
    invoke-direct {p0, v1, v3, v4}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@113
    .line 421
    new-instance v6, Ljava/lang/StringBuilder;

    #@115
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@118
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11b
    move-result-object v6

    #@11c
    invoke-virtual {p0, v12}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@11f
    move-result-object v7

    #@120
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@123
    move-result-object v6

    #@124
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@127
    move-result-object v6

    #@128
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@12b
    move-result-object v4

    #@12c
    .line 422
    invoke-direct {p0, v4, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@12f
    .line 423
    invoke-virtual {p0, v12}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@132
    move-result-object v6

    #@133
    invoke-direct {p0, v0, v6}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@136
    move-result v3

    #@137
    .line 424
    invoke-direct {p0, v1, v3, v4}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@13a
    .line 425
    new-instance v6, Ljava/lang/StringBuilder;

    #@13c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@13f
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@142
    move-result-object v6

    #@143
    const v7, 0x7f06019e

    #@146
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@149
    move-result-object v7

    #@14a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14d
    move-result-object v6

    #@14e
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@151
    move-result-object v6

    #@152
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@155
    move-result-object v4

    #@156
    .line 426
    invoke-direct {p0, v4, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@159
    .line 427
    const v6, 0x7f06019e

    #@15c
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@15f
    move-result-object v6

    #@160
    invoke-direct {p0, v0, v6}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@163
    move-result v3

    #@164
    .line 428
    invoke-direct {p0, v1, v3, v4}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@167
    .line 429
    new-instance v6, Ljava/lang/StringBuilder;

    #@169
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@16c
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16f
    move-result-object v6

    #@170
    const v7, 0x7f06019f

    #@173
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@176
    move-result-object v7

    #@177
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17a
    move-result-object v6

    #@17b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17e
    move-result-object v6

    #@17f
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@182
    move-result-object v2

    #@183
    .line 430
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@186
    .line 431
    const v6, 0x7f06019f

    #@189
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@18c
    move-result-object v6

    #@18d
    invoke-direct {p0, v0, v6}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@190
    move-result v3

    #@191
    .line 432
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@194
    .line 433
    new-instance v6, Ljava/lang/StringBuilder;

    #@196
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@199
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19c
    move-result-object v6

    #@19d
    const v7, 0x7f0601a0

    #@1a0
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@1a3
    move-result-object v7

    #@1a4
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a7
    move-result-object v6

    #@1a8
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ab
    move-result-object v6

    #@1ac
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@1af
    move-result-object v4

    #@1b0
    .line 434
    invoke-direct {p0, v4, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@1b3
    .line 435
    const v6, 0x7f0601a0

    #@1b6
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@1b9
    move-result-object v6

    #@1ba
    invoke-direct {p0, v0, v6}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@1bd
    move-result v3

    #@1be
    .line 436
    invoke-direct {p0, v1, v3, v4}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@1c1
    .line 437
    new-instance v6, Ljava/lang/StringBuilder;

    #@1c3
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@1c6
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c9
    move-result-object v6

    #@1ca
    const v7, 0x7f0601a1

    #@1cd
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@1d0
    move-result-object v7

    #@1d1
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d4
    move-result-object v6

    #@1d5
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d8
    move-result-object v6

    #@1d9
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@1dc
    move-result-object v2

    #@1dd
    .line 438
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@1e0
    .line 439
    const v6, 0x7f0601a1

    #@1e3
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@1e6
    move-result-object v6

    #@1e7
    invoke-direct {p0, v0, v6}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@1ea
    move-result v3

    #@1eb
    .line 440
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@1ee
    .line 441
    new-instance v6, Ljava/lang/StringBuilder;

    #@1f0
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@1f3
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f6
    move-result-object v6

    #@1f7
    const v7, 0x7f0601a4

    #@1fa
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@1fd
    move-result-object v7

    #@1fe
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@201
    move-result-object v6

    #@202
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@205
    move-result-object v6

    #@206
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@209
    move-result-object v4

    #@20a
    .line 442
    invoke-direct {p0, v4, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@20d
    .line 443
    const v6, 0x7f0601a4

    #@210
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@213
    move-result-object v6

    #@214
    invoke-direct {p0, v0, v6}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@217
    move-result v3

    #@218
    .line 444
    invoke-direct {p0, v1, v3, v4}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@21b
    .line 445
    new-instance v6, Ljava/lang/StringBuilder;

    #@21d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@220
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@223
    move-result-object v6

    #@224
    const v7, 0x7f0601a6

    #@227
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@22a
    move-result-object v7

    #@22b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22e
    move-result-object v6

    #@22f
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@232
    move-result-object v6

    #@233
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@236
    move-result-object v2

    #@237
    .line 446
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@23a
    .line 447
    const v6, 0x7f0601a6

    #@23d
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@240
    move-result-object v6

    #@241
    invoke-direct {p0, v0, v6}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@244
    move-result v3

    #@245
    .line 448
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@248
    .line 449
    new-instance v6, Ljava/lang/StringBuilder;

    #@24a
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@24d
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@250
    move-result-object v6

    #@251
    const v7, 0x7f0601a7

    #@254
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@257
    move-result-object v7

    #@258
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25b
    move-result-object v6

    #@25c
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25f
    move-result-object v6

    #@260
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@263
    move-result-object v4

    #@264
    .line 450
    invoke-direct {p0, v4, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@267
    .line 451
    const v6, 0x7f0601a7

    #@26a
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@26d
    move-result-object v6

    #@26e
    invoke-direct {p0, v0, v6}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@271
    move-result v3

    #@272
    .line 452
    invoke-direct {p0, v1, v3, v4}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@275
    .line 453
    new-instance v6, Ljava/lang/StringBuilder;

    #@277
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@27a
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27d
    move-result-object v6

    #@27e
    const v7, 0x7f0601a8

    #@281
    invoke-virtual {p0, v7}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@284
    move-result-object v7

    #@285
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@288
    move-result-object v6

    #@289
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28c
    move-result-object v6

    #@28d
    invoke-direct {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@290
    move-result-object v2

    #@291
    .line 454
    invoke-direct {p0, v2, p1}, Lcom/lge/ims/setting/MediaSettings;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@294
    .line 455
    const v6, 0x7f0601a8

    #@297
    invoke-virtual {p0, v6}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@29a
    move-result-object v6

    #@29b
    invoke-direct {p0, v0, v6}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@29e
    move-result v3

    #@29f
    .line 456
    invoke-direct {p0, v1, v3, v2}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@2a2
    .line 459
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    #@2a5
    goto/16 :goto_3b
.end method

.method private removePreference(Ljava/lang/String;)V
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 1221
    invoke-virtual {p0}, Lcom/lge/ims/setting/MediaSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    #@3
    move-result-object v1

    #@4
    .line 1223
    .local v1, root:Landroid/preference/PreferenceScreen;
    if-eqz v1, :cond_f

    #@6
    .line 1224
    invoke-virtual {v1, p1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    #@9
    move-result-object v0

    #@a
    .line 1226
    .local v0, preference:Landroid/preference/Preference;
    if-eqz v0, :cond_f

    #@c
    .line 1227
    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    #@f
    .line 1230
    .end local v0           #preference:Landroid/preference/Preference;
    :cond_f
    return-void
.end method

.method private setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V
    .registers 4
    .parameter "edit"

    #@0
    .prologue
    .line 1233
    if-nez p1, :cond_3

    #@2
    .line 1238
    :goto_2
    return-void

    #@3
    .line 1237
    :cond_3
    invoke-virtual {p1}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    #@6
    move-result-object v0

    #@7
    const/4 v1, 0x2

    #@8
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    #@b
    goto :goto_2
.end method

.method private setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V
    .registers 3
    .parameter "preference"
    .parameter "listener"

    #@0
    .prologue
    .line 1241
    if-eqz p1, :cond_5

    #@2
    .line 1242
    invoke-virtual {p1, p2}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@5
    .line 1244
    :cond_5
    return-void
.end method

.method private setValue(Landroid/database/Cursor;ILandroid/preference/CheckBoxPreference;)V
    .registers 7
    .parameter "cursor"
    .parameter "index"
    .parameter "checkbox"

    #@0
    .prologue
    .line 1247
    const/4 v1, -0x1

    #@1
    if-ne p2, v1, :cond_b

    #@3
    .line 1248
    const-string v1, "IMSMediaSetting"

    #@5
    const-string v2, "index is (-1)"

    #@7
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 1275
    :cond_a
    :goto_a
    return-void

    #@b
    .line 1252
    :cond_b
    if-nez p1, :cond_15

    #@d
    .line 1253
    const-string v1, "IMSMediaSetting"

    #@f
    const-string v2, "Cursor is null"

    #@11
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@14
    goto :goto_a

    #@15
    .line 1257
    :cond_15
    if-nez p3, :cond_1f

    #@17
    .line 1258
    const-string v1, "IMSMediaSetting"

    #@19
    const-string v2, "CheckBoxPreference is null"

    #@1b
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@1e
    goto :goto_a

    #@1f
    .line 1262
    :cond_1f
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    .line 1264
    .local v0, value:Ljava/lang/String;
    if-eqz v0, :cond_a

    #@25
    .line 1268
    const-string v1, "true"

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@2a
    move-result v1

    #@2b
    if-nez v1, :cond_37

    #@2d
    .line 1269
    const/4 v1, 0x1

    #@2e
    invoke-virtual {p3, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    #@31
    .line 1270
    const-string v1, "true"

    #@33
    invoke-virtual {p3, v1}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    #@36
    goto :goto_a

    #@37
    .line 1272
    :cond_37
    const/4 v1, 0x0

    #@38
    invoke-virtual {p3, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    #@3b
    .line 1273
    const-string v1, "false"

    #@3d
    invoke-virtual {p3, v1}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    #@40
    goto :goto_a
.end method

.method private setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V
    .registers 7
    .parameter "cursor"
    .parameter "index"
    .parameter "edit"

    #@0
    .prologue
    .line 1278
    const/4 v1, -0x1

    #@1
    if-ne p2, v1, :cond_4

    #@3
    .line 1300
    :cond_3
    :goto_3
    return-void

    #@4
    .line 1282
    :cond_4
    if-nez p1, :cond_e

    #@6
    .line 1283
    const-string v1, "IMSMediaSetting"

    #@8
    const-string v2, "Cursor is null"

    #@a
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    goto :goto_3

    #@e
    .line 1287
    :cond_e
    if-nez p3, :cond_18

    #@10
    .line 1288
    const-string v1, "IMSMediaSetting"

    #@12
    const-string v2, "EditTextPreference is null"

    #@14
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@17
    goto :goto_3

    #@18
    .line 1292
    :cond_18
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    .line 1294
    .local v0, value:Ljava/lang/String;
    if-eqz v0, :cond_3

    #@1e
    .line 1298
    invoke-virtual {p3, v0}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    #@21
    .line 1299
    invoke-virtual {p3, v0}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    #@24
    goto :goto_3
.end method

.method private setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V
    .registers 10
    .parameter "cursor"
    .parameter "index"
    .parameter "list"

    #@0
    .prologue
    .line 1303
    const/4 v3, -0x1

    #@1
    if-ne p2, v3, :cond_4

    #@3
    .line 1334
    :cond_3
    :goto_3
    return-void

    #@4
    .line 1307
    :cond_4
    if-nez p1, :cond_e

    #@6
    .line 1308
    const-string v3, "IMSMediaSetting"

    #@8
    const-string v4, "Cursor is null"

    #@a
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    goto :goto_3

    #@e
    .line 1312
    :cond_e
    if-nez p3, :cond_18

    #@10
    .line 1313
    const-string v3, "IMSMediaSetting"

    #@12
    const-string v4, "ListPreference is null"

    #@14
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@17
    goto :goto_3

    #@18
    .line 1317
    :cond_18
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    .line 1319
    .local v1, value:Ljava/lang/String;
    if-eqz v1, :cond_3

    #@1e
    .line 1323
    invoke-virtual {p3, v1}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    #@21
    move-result v2

    #@22
    .line 1325
    .local v2, valueIndex:I
    if-gez v2, :cond_4b

    #@24
    .line 1326
    const-string v3, "IMSMediaSetting"

    #@26
    new-instance v4, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v5, "setValue :: list - key="

    #@2d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v4

    #@31
    invoke-virtual {p3}, Landroid/preference/ListPreference;->getKey()Ljava/lang/String;

    #@34
    move-result-object v5

    #@35
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v4

    #@39
    const-string v5, ", value="

    #@3b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v4

    #@3f
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v4

    #@43
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v4

    #@47
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@4a
    goto :goto_3

    #@4b
    .line 1330
    :cond_4b
    invoke-virtual {p3, v2}, Landroid/preference/ListPreference;->setValueIndex(I)V

    #@4e
    .line 1332
    invoke-virtual {p3}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    #@51
    move-result-object v0

    #@52
    .line 1333
    .local v0, entrys:[Ljava/lang/CharSequence;
    aget-object v3, v0, v2

    #@54
    invoke-virtual {p3, v3}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    #@57
    goto :goto_3
.end method


# virtual methods
.method public getNameOfListPreference(Landroid/preference/ListPreference;Ljava/lang/Object;)Ljava/lang/String;
    .registers 6
    .parameter "listPref"
    .parameter "newValue"

    #@0
    .prologue
    .line 1089
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@3
    move-result-object v2

    #@4
    invoke-virtual {p1, v2}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    #@7
    move-result v1

    #@8
    .line 1091
    .local v1, index:I
    const/4 v2, -0x1

    #@9
    if-ne v1, v2, :cond_c

    #@b
    .line 1092
    const/4 v1, 0x0

    #@c
    .line 1095
    :cond_c
    invoke-virtual {p1}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    #@f
    move-result-object v0

    #@10
    .line 1097
    .local v0, entrys:[Ljava/lang/CharSequence;
    aget-object v2, v0, v1

    #@12
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    return-object v2
.end method

.method public load(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 10
    .parameter "tableName"
    .parameter "key"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1030
    invoke-direct {p0, p1}, Lcom/lge/ims/setting/MediaSettings;->getSettingsTable(Ljava/lang/String;)Landroid/database/Cursor;

    #@4
    move-result-object v1

    #@5
    .line 1032
    .local v1, cursor:Landroid/database/Cursor;
    if-nez v1, :cond_26

    #@7
    .line 1033
    const-string v4, "IMSMediaSetting"

    #@9
    new-instance v5, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v6, "Cursor :: "

    #@10
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v5

    #@14
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v5

    #@18
    const-string v6, " is null"

    #@1a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v5

    #@1e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v5

    #@22
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@25
    .line 1054
    :goto_25
    return-object v3

    #@26
    .line 1037
    :cond_26
    invoke-interface {v1}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    #@29
    move-result-object v0

    #@2a
    .line 1040
    .local v0, columns:[Ljava/lang/String;
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    #@2d
    .line 1043
    invoke-direct {p0, v0, p2}, Lcom/lge/ims/setting/MediaSettings;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@30
    move-result v2

    #@31
    .line 1045
    .local v2, index:I
    const/4 v4, -0x1

    #@32
    if-ne v2, v4, :cond_38

    #@34
    .line 1046
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    #@37
    goto :goto_25

    #@38
    .line 1050
    :cond_38
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@3b
    move-result-object v3

    #@3c
    .line 1053
    .local v3, value:Ljava/lang/String;
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    #@3f
    goto :goto_25
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 11
    .parameter "savedInstanceState"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 65
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    #@4
    .line 67
    const v4, 0x7f040002

    #@7
    invoke-virtual {p0, v4}, Lcom/lge/ims/setting/MediaSettings;->addPreferencesFromResource(I)V

    #@a
    .line 70
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettings;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@c
    if-nez v4, :cond_18

    #@e
    .line 72
    :try_start_e
    const-string v4, "/data/data/com.lge.ims/databases/lgims.db"

    #@10
    const/4 v5, 0x0

    #@11
    const/4 v6, 0x0

    #@12
    invoke-static {v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->openDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)Landroid/database/sqlite/SQLiteDatabase;

    #@15
    move-result-object v4

    #@16
    iput-object v4, p0, Lcom/lge/ims/setting/MediaSettings;->imsDB:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_18
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_e .. :try_end_18} :catch_86

    #@18
    .line 79
    :cond_18
    new-instance v4, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;

    #@1a
    const-string v5, ""

    #@1c
    const-string v6, "lgims_com_media"

    #@1e
    const-string v7, "IMSMediaSetting_Media"

    #@20
    invoke-direct {v4, p0, v5, v6, v7}, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;-><init>(Lcom/lge/ims/setting/MediaSettings;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@23
    iput-object v4, p0, Lcom/lge/ims/setting/MediaSettings;->onVoIPMediaListener:Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;

    #@25
    .line 80
    new-instance v4, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;

    #@27
    const-string v5, ""

    #@29
    const-string v6, "lgims_com_media_audio"

    #@2b
    const-string v7, "IMSMediaSetting_MediaAudio"

    #@2d
    invoke-direct {v4, p0, v5, v6, v7}, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;-><init>(Lcom/lge/ims/setting/MediaSettings;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@30
    iput-object v4, p0, Lcom/lge/ims/setting/MediaSettings;->onVoIPAudioListener:Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;

    #@32
    .line 81
    new-instance v4, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;

    #@34
    const-string v5, ""

    #@36
    const-string v6, "lgims_com_media_video"

    #@38
    const-string v7, "IMSMediaSetting_MediaVideo"

    #@3a
    invoke-direct {v4, p0, v5, v6, v7}, Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;-><init>(Lcom/lge/ims/setting/MediaSettings;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@3d
    iput-object v4, p0, Lcom/lge/ims/setting/MediaSettings;->onVoIPVideoListener:Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;

    #@3f
    .line 83
    new-instance v2, Lcom/lge/ims/setting/MediaSettings$AudioSettingCommand;

    #@41
    const-string v4, "lgims_com_media_audio_codec_volte"

    #@43
    const-string v5, "audiocodec"

    #@45
    invoke-direct {v2, p0, v4, v5, v8}, Lcom/lge/ims/setting/MediaSettings$AudioSettingCommand;-><init>(Lcom/lge/ims/setting/MediaSettings;Ljava/lang/String;Ljava/lang/String;I)V

    #@48
    .line 85
    .local v2, mediaAudioSettingCommandVoLTE:Lcom/lge/ims/setting/MediaSettings$AudioSettingCommand;
    new-instance v1, Lcom/lge/ims/setting/MediaSettings$AudioSettingCommand;

    #@4a
    const-string v4, "lgims_com_media_audio_codec_vt"

    #@4c
    const-string v5, "audiocodec"

    #@4e
    const/4 v6, 0x1

    #@4f
    invoke-direct {v1, p0, v4, v5, v6}, Lcom/lge/ims/setting/MediaSettings$AudioSettingCommand;-><init>(Lcom/lge/ims/setting/MediaSettings;Ljava/lang/String;Ljava/lang/String;I)V

    #@52
    .line 87
    .local v1, mediaAudioSettingCommandVT:Lcom/lge/ims/setting/MediaSettings$AudioSettingCommand;
    new-instance v3, Lcom/lge/ims/setting/MediaSettings$VideoSettingCommand;

    #@54
    const-string v4, "lgims_com_media_video_codec_vt"

    #@56
    const-string v5, "videocodec"

    #@58
    const/4 v6, 0x2

    #@59
    invoke-direct {v3, p0, v4, v5, v6}, Lcom/lge/ims/setting/MediaSettings$VideoSettingCommand;-><init>(Lcom/lge/ims/setting/MediaSettings;Ljava/lang/String;Ljava/lang/String;I)V

    #@5c
    .line 91
    .local v3, mediaVideoSettingCommandVT:Lcom/lge/ims/setting/MediaSettings$VideoSettingCommand;
    new-instance v4, Lcom/lge/ims/setting/MediaSettings$MediaCodecChangeListener;

    #@5e
    invoke-direct {v4, p0, v2}, Lcom/lge/ims/setting/MediaSettings$MediaCodecChangeListener;-><init>(Lcom/lge/ims/setting/MediaSettings;Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;)V

    #@61
    iput-object v4, p0, Lcom/lge/ims/setting/MediaSettings;->onCodecAudioVoLTEListener:Lcom/lge/ims/setting/MediaSettings$MediaCodecChangeListener;

    #@63
    .line 92
    new-instance v4, Lcom/lge/ims/setting/MediaSettings$MediaCodecChangeListener;

    #@65
    invoke-direct {v4, p0, v1}, Lcom/lge/ims/setting/MediaSettings$MediaCodecChangeListener;-><init>(Lcom/lge/ims/setting/MediaSettings;Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;)V

    #@68
    iput-object v4, p0, Lcom/lge/ims/setting/MediaSettings;->onCodecAudioVTListener:Lcom/lge/ims/setting/MediaSettings$MediaCodecChangeListener;

    #@6a
    .line 93
    new-instance v4, Lcom/lge/ims/setting/MediaSettings$MediaCodecChangeListener;

    #@6c
    invoke-direct {v4, p0, v3}, Lcom/lge/ims/setting/MediaSettings$MediaCodecChangeListener;-><init>(Lcom/lge/ims/setting/MediaSettings;Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;)V

    #@6f
    iput-object v4, p0, Lcom/lge/ims/setting/MediaSettings;->onCodecVideoVTListener:Lcom/lge/ims/setting/MediaSettings$MediaCodecChangeListener;

    #@71
    .line 95
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettings;->onVoIPMediaListener:Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;

    #@73
    invoke-direct {p0, v4}, Lcom/lge/ims/setting/MediaSettings;->registerMediaChangeListener(Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;)V

    #@76
    .line 96
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettings;->onVoIPAudioListener:Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;

    #@78
    invoke-direct {p0, v4}, Lcom/lge/ims/setting/MediaSettings;->registerMediaAudioVoLTEChangeListener(Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;)V

    #@7b
    .line 97
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettings;->onVoIPAudioListener:Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;

    #@7d
    invoke-direct {p0, v4}, Lcom/lge/ims/setting/MediaSettings;->registerMediaAudioVTChangeListener(Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;)V

    #@80
    .line 98
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettings;->onVoIPVideoListener:Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;

    #@82
    invoke-direct {p0, v4}, Lcom/lge/ims/setting/MediaSettings;->registerMediaVideoChangeListener(Lcom/lge/ims/setting/MediaSettings$EnablerSpecificChangeListener;)V

    #@85
    .line 99
    .end local v1           #mediaAudioSettingCommandVT:Lcom/lge/ims/setting/MediaSettings$AudioSettingCommand;
    .end local v2           #mediaAudioSettingCommandVoLTE:Lcom/lge/ims/setting/MediaSettings$AudioSettingCommand;
    .end local v3           #mediaVideoSettingCommandVT:Lcom/lge/ims/setting/MediaSettings$VideoSettingCommand;
    :goto_85
    return-void

    #@86
    .line 73
    :catch_86
    move-exception v0

    #@87
    .line 74
    .local v0, e:Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    #@8a
    goto :goto_85
.end method

.method public onDestroy()V
    .registers 2

    #@0
    .prologue
    .line 103
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    #@3
    .line 105
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettings;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@5
    if-eqz v0, :cond_f

    #@7
    .line 106
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettings;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@9
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    #@c
    .line 107
    const/4 v0, 0x0

    #@d
    iput-object v0, p0, Lcom/lge/ims/setting/MediaSettings;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@f
    .line 109
    :cond_f
    return-void
.end method

.method public onPause()V
    .registers 2

    #@0
    .prologue
    .line 113
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPause()V

    #@3
    .line 115
    iget-boolean v0, p0, Lcom/lge/ims/setting/MediaSettings;->mAdministrativeConfigChanged:Z

    #@5
    if-eqz v0, :cond_11

    #@7
    .line 116
    invoke-virtual {p0}, Lcom/lge/ims/setting/MediaSettings;->getBaseContext()Landroid/content/Context;

    #@a
    move-result-object v0

    #@b
    invoke-static {v0}, Lcom/lge/ims/Configuration;->init(Landroid/content/Context;)V

    #@e
    .line 117
    const/4 v0, 0x0

    #@f
    iput-boolean v0, p0, Lcom/lge/ims/setting/MediaSettings;->mAdministrativeConfigChanged:Z

    #@11
    .line 119
    :cond_11
    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .registers 4
    .parameter "preferenceScreen"
    .parameter "preference"

    #@0
    .prologue
    .line 125
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public setValue(Landroid/preference/Preference;Ljava/lang/String;)Z
    .registers 11
    .parameter "pref"
    .parameter "value"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 985
    instance-of v7, p1, Landroid/preference/EditTextPreference;

    #@4
    if-eqz v7, :cond_d

    #@6
    .line 986
    check-cast p1, Landroid/preference/EditTextPreference;

    #@8
    .end local p1
    invoke-virtual {p1, p2}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    #@b
    :cond_b
    :goto_b
    move v5, v6

    #@c
    .line 1024
    :cond_c
    return v5

    #@d
    .line 988
    .restart local p1
    :cond_d
    instance-of v7, p1, Landroid/preference/ListPreference;

    #@f
    if-eqz v7, :cond_28

    #@11
    move-object v5, p1

    #@12
    .line 990
    check-cast v5, Landroid/preference/ListPreference;

    #@14
    invoke-virtual {v5, p2}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    #@17
    move-result v1

    #@18
    .line 992
    .local v1, listIndex:I
    const/4 v5, -0x1

    #@19
    if-ne v1, v5, :cond_1c

    #@1b
    .line 993
    const/4 v1, 0x0

    #@1c
    :cond_1c
    move-object v5, p1

    #@1d
    .line 996
    check-cast v5, Landroid/preference/ListPreference;

    #@1f
    invoke-virtual {v5, v1}, Landroid/preference/ListPreference;->setValueIndex(I)V

    #@22
    .line 997
    check-cast p1, Landroid/preference/ListPreference;

    #@24
    .end local p1
    invoke-virtual {p1}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    #@27
    goto :goto_b

    #@28
    .line 999
    .end local v1           #listIndex:I
    .restart local p1
    :cond_28
    instance-of v7, p1, Landroid/preference/CheckBoxPreference;

    #@2a
    if-eqz v7, :cond_40

    #@2c
    .line 1001
    const-string v7, "true"

    #@2e
    invoke-virtual {p2, v7}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@31
    move-result v7

    #@32
    if-nez v7, :cond_3a

    #@34
    .line 1002
    check-cast p1, Landroid/preference/CheckBoxPreference;

    #@36
    .end local p1
    invoke-virtual {p1, v6}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    #@39
    goto :goto_b

    #@3a
    .line 1004
    .restart local p1
    :cond_3a
    check-cast p1, Landroid/preference/CheckBoxPreference;

    #@3c
    .end local p1
    invoke-virtual {p1, v5}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    #@3f
    goto :goto_b

    #@40
    .line 1007
    .restart local p1
    :cond_40
    instance-of v7, p1, Landroid/preference/MultiSelectListPreference;

    #@42
    if-eqz v7, :cond_b

    #@44
    .line 1009
    const-string v7, ","

    #@46
    invoke-virtual {p2, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@49
    move-result-object v3

    #@4a
    .line 1010
    .local v3, tokens:[Ljava/lang/String;
    new-instance v4, Ljava/util/HashSet;

    #@4c
    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    #@4f
    .line 1012
    .local v4, values:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    array-length v7, v3

    #@50
    if-eqz v7, :cond_c

    #@52
    .line 1016
    const/4 v0, 0x0

    #@53
    .local v0, i:I
    :goto_53
    array-length v5, v3

    #@54
    if-ge v0, v5, :cond_62

    #@56
    .line 1017
    aget-object v5, v3, v0

    #@58
    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@5b
    move-result-object v2

    #@5c
    .line 1018
    .local v2, newString:Ljava/lang/String;
    invoke-virtual {v4, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@5f
    .line 1016
    add-int/lit8 v0, v0, 0x1

    #@61
    goto :goto_53

    #@62
    .line 1021
    .end local v2           #newString:Ljava/lang/String;
    :cond_62
    check-cast p1, Landroid/preference/MultiSelectListPreference;

    #@64
    .end local p1
    invoke-virtual {p1, v4}, Landroid/preference/MultiSelectListPreference;->setValues(Ljava/util/Set;)V

    #@67
    goto :goto_b
.end method

.method public summaryChange(Landroid/preference/Preference;Ljava/lang/Object;)V
    .registers 6
    .parameter "preference"
    .parameter "newValue"

    #@0
    .prologue
    .line 1103
    instance-of v2, p1, Landroid/preference/ListPreference;

    #@2
    if-eqz v2, :cond_14

    #@4
    .line 1105
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    #@7
    move-result-object v2

    #@8
    invoke-direct {p0, v2}, Lcom/lge/ims/setting/MediaSettings;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@b
    move-result-object v0

    #@c
    .line 1106
    .local v0, listPref:Landroid/preference/ListPreference;
    invoke-virtual {p0, v0, p2}, Lcom/lge/ims/setting/MediaSettings;->getNameOfListPreference(Landroid/preference/ListPreference;Ljava/lang/Object;)Ljava/lang/String;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {p1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    #@13
    .line 1123
    .end local v0           #listPref:Landroid/preference/ListPreference;
    .end local p1
    :goto_13
    return-void

    #@14
    .line 1108
    .restart local p1
    :cond_14
    instance-of v2, p1, Landroid/preference/EditTextPreference;

    #@16
    if-eqz v2, :cond_20

    #@18
    .line 1110
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {p1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    #@1f
    goto :goto_13

    #@20
    .line 1112
    :cond_20
    instance-of v2, p1, Landroid/preference/CheckBoxPreference;

    #@22
    if-eqz v2, :cond_40

    #@24
    .line 1113
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    .line 1114
    .local v1, value:Ljava/lang/String;
    const-string v2, "true"

    #@2a
    invoke-virtual {v1, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@2d
    move-result v2

    #@2e
    if-nez v2, :cond_38

    #@30
    .line 1115
    check-cast p1, Landroid/preference/CheckBoxPreference;

    #@32
    .end local p1
    const-string v2, "true"

    #@34
    invoke-virtual {p1, v2}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    #@37
    goto :goto_13

    #@38
    .line 1117
    .restart local p1
    :cond_38
    check-cast p1, Landroid/preference/CheckBoxPreference;

    #@3a
    .end local p1
    const-string v2, "false"

    #@3c
    invoke-virtual {p1, v2}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    #@3f
    goto :goto_13

    #@40
    .line 1121
    .end local v1           #value:Ljava/lang/String;
    .restart local p1
    :cond_40
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@43
    move-result-object v2

    #@44
    invoke-virtual {p1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    #@47
    goto :goto_13
.end method

.method public updateValue(Ljava/lang/String;Landroid/content/ContentValues;)Z
    .registers 10
    .parameter "tableName"
    .parameter "values"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 1059
    const/4 v0, 0x0

    #@3
    .line 1061
    .local v0, affectedRows:I
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettings;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@5
    if-nez v4, :cond_26

    #@7
    .line 1062
    const-string v3, "IMSMediaSetting"

    #@9
    new-instance v4, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v5, "DB table ("

    #@10
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v4

    #@14
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v4

    #@18
    const-string v5, ") is null"

    #@1a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v4

    #@1e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v4

    #@22
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@25
    .line 1084
    :goto_25
    return v2

    #@26
    .line 1066
    :cond_26
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettings;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@28
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    #@2b
    .line 1069
    :try_start_2b
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettings;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@2d
    const-string v5, "id = \'1\'"

    #@2f
    const/4 v6, 0x0

    #@30
    invoke-virtual {v4, p1, p2, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@33
    move-result v0

    #@34
    .line 1070
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettings;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@36
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_39
    .catchall {:try_start_2b .. :try_end_39} :catchall_6a
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2b .. :try_end_39} :catch_60

    #@39
    .line 1074
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettings;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@3b
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@3e
    .line 1077
    :goto_3e
    const-string v4, "IMSMediaSetting"

    #@40
    new-instance v5, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v6, "IMSMediaSetting :: updated row count: "

    #@47
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v5

    #@4b
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v5

    #@4f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v5

    #@53
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@56
    .line 1079
    if-ge v0, v3, :cond_71

    #@58
    .line 1080
    const-string v3, "IMSMediaSetting"

    #@5a
    const-string v4, "Update fails"

    #@5c
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@5f
    goto :goto_25

    #@60
    .line 1071
    :catch_60
    move-exception v1

    #@61
    .line 1072
    .local v1, e:Landroid/database/sqlite/SQLiteException;
    :try_start_61
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_64
    .catchall {:try_start_61 .. :try_end_64} :catchall_6a

    #@64
    .line 1074
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettings;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@66
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@69
    goto :goto_3e

    #@6a
    .end local v1           #e:Landroid/database/sqlite/SQLiteException;
    :catchall_6a
    move-exception v2

    #@6b
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettings;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@6d
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@70
    throw v2

    #@71
    :cond_71
    move v2, v3

    #@72
    .line 1084
    goto :goto_25
.end method
