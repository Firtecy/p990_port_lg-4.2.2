.class public Lcom/lge/ims/setting/MediaSettings$VideoSettingCommand;
.super Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;
.source "MediaSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/setting/MediaSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "VideoSettingCommand"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/setting/MediaSettings;


# direct methods
.method public constructor <init>(Lcom/lge/ims/setting/MediaSettings;Ljava/lang/String;Ljava/lang/String;I)V
    .registers 7
    .parameter
    .parameter "tableName"
    .parameter "typePrefix"
    .parameter "sessionType"

    #@0
    .prologue
    .line 726
    iput-object p1, p0, Lcom/lge/ims/setting/MediaSettings$VideoSettingCommand;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@2
    .line 728
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;-><init>(Lcom/lge/ims/setting/MediaSettings;Ljava/lang/String;Ljava/lang/String;I)V

    #@5
    .line 730
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mMediaCodecInfo:Lcom/lge/ims/setting/MediaSettings$IMSMediaCodecInfo;

    #@7
    iget-object v0, v0, Lcom/lge/ims/setting/MediaSettings$IMSMediaCodecInfo;->codecVideoCommonAttribute:[Ljava/lang/String;

    #@9
    invoke-virtual {p0, p3, v0}, Lcom/lge/ims/setting/MediaSettings$VideoSettingCommand;->setCodecAttributes(Ljava/lang/String;[Ljava/lang/String;)V

    #@c
    .line 731
    const-string v0, "H264"

    #@e
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mMediaCodecInfo:Lcom/lge/ims/setting/MediaSettings$IMSMediaCodecInfo;

    #@10
    iget-object v1, v1, Lcom/lge/ims/setting/MediaSettings$IMSMediaCodecInfo;->codecVideoH264Attribute:[Ljava/lang/String;

    #@12
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/setting/MediaSettings$VideoSettingCommand;->setCodecAttributes(Ljava/lang/String;[Ljava/lang/String;)V

    #@15
    .line 732
    const-string v0, "H263"

    #@17
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mMediaCodecInfo:Lcom/lge/ims/setting/MediaSettings$IMSMediaCodecInfo;

    #@19
    iget-object v1, v1, Lcom/lge/ims/setting/MediaSettings$IMSMediaCodecInfo;->codecVideoH263Attribute:[Ljava/lang/String;

    #@1b
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/setting/MediaSettings$VideoSettingCommand;->setCodecAttributes(Ljava/lang/String;[Ljava/lang/String;)V

    #@1e
    .line 733
    return-void
.end method


# virtual methods
.method public loadCodecList(I)Z
    .registers 8
    .parameter "index"

    #@0
    .prologue
    .line 737
    invoke-super {p0, p1}, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->loadCodecList(I)Z

    #@3
    .line 740
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettings$VideoSettingCommand;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@5
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mTableName:Ljava/lang/String;

    #@7
    new-instance v4, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    iget-object v5, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mTypePrefix:Ljava/lang/String;

    #@e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v4

    #@12
    const-string v5, "_"

    #@14
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v4

    #@18
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v4

    #@1c
    const-string v5, "_"

    #@1e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v4

    #@22
    iget-object v5, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mCodecTypeName:Ljava/lang/String;

    #@24
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v4

    #@28
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v4

    #@2c
    invoke-virtual {v2, v3, v4}, Lcom/lge/ims/setting/MediaSettings;->load(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2f
    move-result-object v1

    #@30
    .line 742
    .local v1, value:Ljava/lang/String;
    if-eqz v1, :cond_3a

    #@32
    const-string v2, ""

    #@34
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@37
    move-result v2

    #@38
    if-eqz v2, :cond_3c

    #@3a
    .line 743
    :cond_3a
    const/4 v2, 0x0

    #@3b
    .line 757
    :goto_3b
    return v2

    #@3c
    .line 747
    :cond_3c
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettings$VideoSettingCommand;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@3e
    new-instance v3, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    iget v4, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mSessionType:I

    #@45
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@48
    move-result-object v3

    #@49
    const-string v4, "_"

    #@4b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v3

    #@4f
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@52
    move-result-object v3

    #@53
    const-string v4, "_resolution"

    #@55
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v3

    #@59
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v3

    #@5d
    invoke-static {v2, v3}, Lcom/lge/ims/setting/MediaSettings;->access$000(Lcom/lge/ims/setting/MediaSettings;Ljava/lang/String;)Landroid/preference/ListPreference;

    #@60
    move-result-object v0

    #@61
    .line 748
    .local v0, list:Landroid/preference/ListPreference;
    const-string v2, "H263"

    #@63
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@66
    move-result v2

    #@67
    if-eqz v2, :cond_8b

    #@69
    .line 749
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettings$VideoSettingCommand;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@6b
    invoke-virtual {v2}, Lcom/lge/ims/setting/MediaSettings;->getResources()Landroid/content/res/Resources;

    #@6e
    move-result-object v2

    #@6f
    const v3, 0x7f050024

    #@72
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    #@75
    move-result-object v2

    #@76
    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    #@79
    .line 750
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettings$VideoSettingCommand;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@7b
    invoke-virtual {v2}, Lcom/lge/ims/setting/MediaSettings;->getResources()Landroid/content/res/Resources;

    #@7e
    move-result-object v2

    #@7f
    const v3, 0x7f050029

    #@82
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    #@85
    move-result-object v2

    #@86
    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    #@89
    .line 757
    :goto_89
    const/4 v2, 0x1

    #@8a
    goto :goto_3b

    #@8b
    .line 753
    :cond_8b
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettings$VideoSettingCommand;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@8d
    invoke-virtual {v2}, Lcom/lge/ims/setting/MediaSettings;->getResources()Landroid/content/res/Resources;

    #@90
    move-result-object v2

    #@91
    const v3, 0x7f050023

    #@94
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    #@97
    move-result-object v2

    #@98
    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    #@9b
    .line 754
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettings$VideoSettingCommand;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@9d
    invoke-virtual {v2}, Lcom/lge/ims/setting/MediaSettings;->getResources()Landroid/content/res/Resources;

    #@a0
    move-result-object v2

    #@a1
    const v3, 0x7f05002c

    #@a4
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    #@a7
    move-result-object v2

    #@a8
    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    #@ab
    goto :goto_89
.end method

.method public preferenceChange(ILjava/lang/String;Ljava/lang/String;)Z
    .registers 9
    .parameter "index"
    .parameter "attribute"
    .parameter "inputValue"

    #@0
    .prologue
    .line 762
    invoke-super {p0, p1, p2, p3}, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->preferenceChange(ILjava/lang/String;Ljava/lang/String;)Z

    #@3
    .line 764
    const/4 v1, 0x0

    #@4
    .line 765
    .local v1, prefix:Ljava/lang/String;
    const-string v2, "codec_type"

    #@6
    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_59

    #@c
    .line 767
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettings$VideoSettingCommand;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@e
    new-instance v3, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    iget v4, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mSessionType:I

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    const-string v4, "_"

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    const-string v4, "_resolution"

    #@25
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v3

    #@2d
    invoke-static {v2, v3}, Lcom/lge/ims/setting/MediaSettings;->access$000(Lcom/lge/ims/setting/MediaSettings;Ljava/lang/String;)Landroid/preference/ListPreference;

    #@30
    move-result-object v0

    #@31
    .line 769
    .local v0, list:Landroid/preference/ListPreference;
    const-string v2, "H263"

    #@33
    invoke-virtual {p3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@36
    move-result v2

    #@37
    if-eqz v2, :cond_5b

    #@39
    .line 770
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettings$VideoSettingCommand;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@3b
    invoke-virtual {v2}, Lcom/lge/ims/setting/MediaSettings;->getResources()Landroid/content/res/Resources;

    #@3e
    move-result-object v2

    #@3f
    const v3, 0x7f050024

    #@42
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    #@45
    move-result-object v2

    #@46
    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    #@49
    .line 771
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettings$VideoSettingCommand;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@4b
    invoke-virtual {v2}, Lcom/lge/ims/setting/MediaSettings;->getResources()Landroid/content/res/Resources;

    #@4e
    move-result-object v2

    #@4f
    const v3, 0x7f050029

    #@52
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    #@55
    move-result-object v2

    #@56
    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    #@59
    .line 779
    .end local v0           #list:Landroid/preference/ListPreference;
    :cond_59
    :goto_59
    const/4 v2, 0x1

    #@5a
    return v2

    #@5b
    .line 774
    .restart local v0       #list:Landroid/preference/ListPreference;
    :cond_5b
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettings$VideoSettingCommand;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@5d
    invoke-virtual {v2}, Lcom/lge/ims/setting/MediaSettings;->getResources()Landroid/content/res/Resources;

    #@60
    move-result-object v2

    #@61
    const v3, 0x7f050023

    #@64
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    #@67
    move-result-object v2

    #@68
    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    #@6b
    .line 775
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettings$VideoSettingCommand;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@6d
    invoke-virtual {v2}, Lcom/lge/ims/setting/MediaSettings;->getResources()Landroid/content/res/Resources;

    #@70
    move-result-object v2

    #@71
    const v3, 0x7f05002c

    #@74
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    #@77
    move-result-object v2

    #@78
    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    #@7b
    goto :goto_59
.end method
