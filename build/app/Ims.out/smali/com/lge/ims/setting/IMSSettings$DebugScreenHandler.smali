.class final Lcom/lge/ims/setting/IMSSettings$DebugScreenHandler;
.super Landroid/os/Handler;
.source "IMSSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/setting/IMSSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "DebugScreenHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/setting/IMSSettings;


# direct methods
.method public constructor <init>(Lcom/lge/ims/setting/IMSSettings;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1202
    iput-object p1, p0, Lcom/lge/ims/setting/IMSSettings$DebugScreenHandler;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    .line 1203
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 6
    .parameter "msg"

    #@0
    .prologue
    .line 1207
    if-nez p1, :cond_3

    #@2
    .line 1230
    :cond_2
    :goto_2
    return-void

    #@3
    .line 1211
    :cond_3
    const-string v0, "LGIMSSettings"

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "DebugScreenHandler :: handleMessage - "

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    iget v2, p1, Landroid/os/Message;->what:I

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 1213
    iget v0, p1, Landroid/os/Message;->what:I

    #@1f
    packed-switch v0, :pswitch_data_4a

    #@22
    goto :goto_2

    #@23
    .line 1215
    :pswitch_23
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings$DebugScreenHandler;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@25
    invoke-static {v0}, Lcom/lge/ims/setting/IMSSettings;->access$500(Lcom/lge/ims/setting/IMSSettings;)V

    #@28
    .line 1217
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings$DebugScreenHandler;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@2a
    invoke-static {v0}, Lcom/lge/ims/setting/IMSSettings;->access$600(Lcom/lge/ims/setting/IMSSettings;)Z

    #@2d
    move-result v0

    #@2e
    if-eqz v0, :cond_2

    #@30
    .line 1218
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings$DebugScreenHandler;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@32
    invoke-static {v0}, Lcom/lge/ims/setting/IMSSettings;->access$700(Lcom/lge/ims/setting/IMSSettings;)Lcom/lge/ims/setting/IMSSettings$DebugScreenHandler;

    #@35
    move-result-object v0

    #@36
    const/16 v1, 0x65

    #@38
    const-wide/16 v2, 0x7d0

    #@3a
    invoke-virtual {v0, v1, v2, v3}, Lcom/lge/ims/setting/IMSSettings$DebugScreenHandler;->sendEmptyMessageDelayed(IJ)Z

    #@3d
    goto :goto_2

    #@3e
    .line 1223
    :pswitch_3e
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings$DebugScreenHandler;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@40
    invoke-static {v0}, Lcom/lge/ims/setting/IMSSettings;->access$800(Lcom/lge/ims/setting/IMSSettings;)V

    #@43
    .line 1224
    iget-object v0, p0, Lcom/lge/ims/setting/IMSSettings$DebugScreenHandler;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@45
    invoke-static {v0}, Lcom/lge/ims/setting/IMSSettings;->access$900(Lcom/lge/ims/setting/IMSSettings;)V

    #@48
    goto :goto_2

    #@49
    .line 1213
    nop

    #@4a
    :pswitch_data_4a
    .packed-switch 0x65
        :pswitch_23
        :pswitch_3e
    .end packed-switch
.end method
