.class public Lcom/lge/ims/setting/ACSettings;
.super Landroid/preference/PreferenceActivity;
.source "ACSettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/setting/ACSettings$ACSettingsChangeListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ACSettings"


# instance fields
.field private onCommonListener:Lcom/lge/ims/setting/ACSettings$ACSettingsChangeListener;

.field private onConnectionServerListener:Lcom/lge/ims/setting/ACSettings$ACSettingsChangeListener;

.field private onSubscriberListener:Lcom/lge/ims/setting/ACSettings$ACSettingsChangeListener;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 27
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    #@4
    .line 31
    iput-object v0, p0, Lcom/lge/ims/setting/ACSettings;->onCommonListener:Lcom/lge/ims/setting/ACSettings$ACSettingsChangeListener;

    #@6
    .line 32
    iput-object v0, p0, Lcom/lge/ims/setting/ACSettings;->onSubscriberListener:Lcom/lge/ims/setting/ACSettings$ACSettingsChangeListener;

    #@8
    .line 33
    iput-object v0, p0, Lcom/lge/ims/setting/ACSettings;->onConnectionServerListener:Lcom/lge/ims/setting/ACSettings$ACSettingsChangeListener;

    #@a
    .line 200
    return-void
.end method

.method static synthetic access$000(Lcom/lge/ims/setting/ACSettings;Landroid/preference/Preference;Ljava/lang/Object;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/lge/ims/setting/ACSettings;->setSummary(Landroid/preference/Preference;Ljava/lang/Object;)V

    #@3
    return-void
.end method

.method private getCheckBoxPreference(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 257
    invoke-virtual {p0}, Lcom/lge/ims/setting/ACSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    #@3
    move-result-object v0

    #@4
    .line 259
    .local v0, root:Landroid/preference/PreferenceScreen;
    if-eqz v0, :cond_d

    #@6
    .line 260
    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    #@9
    move-result-object v1

    #@a
    check-cast v1, Landroid/preference/CheckBoxPreference;

    #@c
    .line 263
    :goto_c
    return-object v1

    #@d
    :cond_d
    const/4 v1, 0x0

    #@e
    goto :goto_c
.end method

.method private getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 277
    invoke-virtual {p0}, Lcom/lge/ims/setting/ACSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    #@3
    move-result-object v0

    #@4
    .line 279
    .local v0, root:Landroid/preference/PreferenceScreen;
    if-eqz v0, :cond_d

    #@6
    .line 280
    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    #@9
    move-result-object v1

    #@a
    check-cast v1, Landroid/preference/EditTextPreference;

    #@c
    .line 283
    :goto_c
    return-object v1

    #@d
    :cond_d
    const/4 v1, 0x0

    #@e
    goto :goto_c
.end method

.method private getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 267
    invoke-virtual {p0}, Lcom/lge/ims/setting/ACSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    #@3
    move-result-object v0

    #@4
    .line 269
    .local v0, root:Landroid/preference/PreferenceScreen;
    if-eqz v0, :cond_d

    #@6
    .line 270
    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    #@9
    move-result-object v1

    #@a
    check-cast v1, Landroid/preference/ListPreference;

    #@c
    .line 273
    :goto_c
    return-object v1

    #@d
    :cond_d
    const/4 v1, 0x0

    #@e
    goto :goto_c
.end method

.method private registerCommonChangeListener(Lcom/lge/ims/setting/ACSettings$ACSettingsChangeListener;)V
    .registers 13
    .parameter "listener"

    #@0
    .prologue
    .line 87
    const/4 v8, 0x0

    #@1
    .line 90
    .local v8, cursor:Landroid/database/Cursor;
    :try_start_1
    invoke-virtual {p0}, Lcom/lge/ims/setting/ACSettings;->getContentResolver()Landroid/content/ContentResolver;

    #@4
    move-result-object v0

    #@5
    sget-object v1, Lcom/lge/ims/provider/ac/AC$Settings$Common;->CONTENT_URI:Landroid/net/Uri;

    #@7
    const/4 v2, 0x0

    #@8
    const/4 v3, 0x0

    #@9
    const/4 v4, 0x0

    #@a
    const/4 v5, 0x0

    #@b
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@e
    move-result-object v8

    #@f
    .line 92
    if-eqz v8, :cond_45

    #@11
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    #@14
    move-result v0

    #@15
    if-eqz v0, :cond_45

    #@17
    .line 94
    const-string v0, "availability"

    #@19
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@1c
    move-result v7

    #@1d
    .line 95
    .local v7, column:I
    const v0, 0x7f060020

    #@20
    invoke-virtual {p0, v0}, Lcom/lge/ims/setting/ACSettings;->getString(I)Ljava/lang/String;

    #@23
    move-result-object v0

    #@24
    invoke-direct {p0, v0}, Lcom/lge/ims/setting/ACSettings;->getCheckBoxPreference(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    #@27
    move-result-object v6

    #@28
    .line 96
    .local v6, checkbox:Landroid/preference/CheckBoxPreference;
    invoke-direct {p0, v6, p1}, Lcom/lge/ims/setting/ACSettings;->setListener(Landroid/preference/CheckBoxPreference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@2b
    .line 97
    invoke-direct {p0, v8, v7, v6}, Lcom/lge/ims/setting/ACSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/CheckBoxPreference;)V

    #@2e
    .line 100
    const-string v0, "update_exception_list"

    #@30
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@33
    move-result v7

    #@34
    .line 101
    const v0, 0x7f060021

    #@37
    invoke-virtual {p0, v0}, Lcom/lge/ims/setting/ACSettings;->getString(I)Ljava/lang/String;

    #@3a
    move-result-object v0

    #@3b
    invoke-direct {p0, v0}, Lcom/lge/ims/setting/ACSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@3e
    move-result-object v10

    #@3f
    .line 102
    .local v10, edit:Landroid/preference/EditTextPreference;
    invoke-direct {p0, v10, p1}, Lcom/lge/ims/setting/ACSettings;->setListener(Landroid/preference/EditTextPreference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@42
    .line 103
    invoke-direct {p0, v8, v7, v10}, Lcom/lge/ims/setting/ACSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V
    :try_end_45
    .catchall {:try_start_1 .. :try_end_45} :catchall_71
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_45} :catch_4b
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_45} :catch_5e

    #@45
    .line 112
    .end local v6           #checkbox:Landroid/preference/CheckBoxPreference;
    .end local v7           #column:I
    .end local v10           #edit:Landroid/preference/EditTextPreference;
    :cond_45
    if-eqz v8, :cond_4a

    #@47
    .line 113
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@4a
    .line 116
    :cond_4a
    :goto_4a
    return-void

    #@4b
    .line 105
    :catch_4b
    move-exception v9

    #@4c
    .line 106
    .local v9, e:Landroid/database/sqlite/SQLiteException;
    :try_start_4c
    const-string v0, "ACSettings"

    #@4e
    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    #@51
    move-result-object v1

    #@52
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@55
    .line 107
    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_58
    .catchall {:try_start_4c .. :try_end_58} :catchall_71

    #@58
    .line 112
    if-eqz v8, :cond_4a

    #@5a
    .line 113
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@5d
    goto :goto_4a

    #@5e
    .line 108
    .end local v9           #e:Landroid/database/sqlite/SQLiteException;
    :catch_5e
    move-exception v9

    #@5f
    .line 109
    .local v9, e:Ljava/lang/Exception;
    :try_start_5f
    const-string v0, "ACSettings"

    #@61
    invoke-virtual {v9}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@64
    move-result-object v1

    #@65
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@68
    .line 110
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_6b
    .catchall {:try_start_5f .. :try_end_6b} :catchall_71

    #@6b
    .line 112
    if-eqz v8, :cond_4a

    #@6d
    .line 113
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@70
    goto :goto_4a

    #@71
    .line 112
    .end local v9           #e:Ljava/lang/Exception;
    :catchall_71
    move-exception v0

    #@72
    if-eqz v8, :cond_77

    #@74
    .line 113
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@77
    :cond_77
    throw v0
.end method

.method private registerConnectionServerChangeListener(Lcom/lge/ims/setting/ACSettings$ACSettingsChangeListener;)V
    .registers 13
    .parameter "listener"

    #@0
    .prologue
    .line 163
    const/4 v7, 0x0

    #@1
    .line 166
    .local v7, cursor:Landroid/database/Cursor;
    :try_start_1
    invoke-virtual {p0}, Lcom/lge/ims/setting/ACSettings;->getContentResolver()Landroid/content/ContentResolver;

    #@4
    move-result-object v0

    #@5
    sget-object v1, Lcom/lge/ims/provider/ac/AC$Settings$ConnectionServer;->CONTENT_URI:Landroid/net/Uri;

    #@7
    const/4 v2, 0x0

    #@8
    const/4 v3, 0x0

    #@9
    const/4 v4, 0x0

    #@a
    const/4 v5, 0x0

    #@b
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@e
    move-result-object v7

    #@f
    .line 168
    if-eqz v7, :cond_5c

    #@11
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    #@14
    move-result v0

    #@15
    if-eqz v0, :cond_5c

    #@17
    .line 170
    const-string v0, "server_selection"

    #@19
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@1c
    move-result v6

    #@1d
    .line 171
    .local v6, column:I
    const v0, 0x7f060027

    #@20
    invoke-virtual {p0, v0}, Lcom/lge/ims/setting/ACSettings;->getString(I)Ljava/lang/String;

    #@23
    move-result-object v0

    #@24
    invoke-direct {p0, v0}, Lcom/lge/ims/setting/ACSettings;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@27
    move-result-object v10

    #@28
    .line 172
    .local v10, list:Landroid/preference/ListPreference;
    invoke-direct {p0, v10, p1}, Lcom/lge/ims/setting/ACSettings;->setListener(Landroid/preference/ListPreference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@2b
    .line 173
    invoke-direct {p0, v7, v6, v10}, Lcom/lge/ims/setting/ACSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@2e
    .line 176
    const-string v0, "commercial_network"

    #@30
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@33
    move-result v6

    #@34
    .line 177
    const v0, 0x7f060028

    #@37
    invoke-virtual {p0, v0}, Lcom/lge/ims/setting/ACSettings;->getString(I)Ljava/lang/String;

    #@3a
    move-result-object v0

    #@3b
    invoke-direct {p0, v0}, Lcom/lge/ims/setting/ACSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@3e
    move-result-object v9

    #@3f
    .line 178
    .local v9, edit:Landroid/preference/EditTextPreference;
    invoke-direct {p0, v9, p1}, Lcom/lge/ims/setting/ACSettings;->setListener(Landroid/preference/EditTextPreference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@42
    .line 179
    invoke-direct {p0, v7, v6, v9}, Lcom/lge/ims/setting/ACSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@45
    .line 182
    const-string v0, "testbed"

    #@47
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@4a
    move-result v6

    #@4b
    .line 183
    const v0, 0x7f060029

    #@4e
    invoke-virtual {p0, v0}, Lcom/lge/ims/setting/ACSettings;->getString(I)Ljava/lang/String;

    #@51
    move-result-object v0

    #@52
    invoke-direct {p0, v0}, Lcom/lge/ims/setting/ACSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@55
    move-result-object v9

    #@56
    .line 184
    invoke-direct {p0, v9, p1}, Lcom/lge/ims/setting/ACSettings;->setListener(Landroid/preference/EditTextPreference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@59
    .line 185
    invoke-direct {p0, v7, v6, v9}, Lcom/lge/ims/setting/ACSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V
    :try_end_5c
    .catchall {:try_start_1 .. :try_end_5c} :catchall_88
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_5c} :catch_62
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_5c} :catch_75

    #@5c
    .line 194
    .end local v6           #column:I
    .end local v9           #edit:Landroid/preference/EditTextPreference;
    .end local v10           #list:Landroid/preference/ListPreference;
    :cond_5c
    if-eqz v7, :cond_61

    #@5e
    .line 195
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@61
    .line 198
    :cond_61
    :goto_61
    return-void

    #@62
    .line 187
    :catch_62
    move-exception v8

    #@63
    .line 188
    .local v8, e:Landroid/database/sqlite/SQLiteException;
    :try_start_63
    const-string v0, "ACSettings"

    #@65
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    #@68
    move-result-object v1

    #@69
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@6c
    .line 189
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_6f
    .catchall {:try_start_63 .. :try_end_6f} :catchall_88

    #@6f
    .line 194
    if-eqz v7, :cond_61

    #@71
    .line 195
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@74
    goto :goto_61

    #@75
    .line 190
    .end local v8           #e:Landroid/database/sqlite/SQLiteException;
    :catch_75
    move-exception v8

    #@76
    .line 191
    .local v8, e:Ljava/lang/Exception;
    :try_start_76
    const-string v0, "ACSettings"

    #@78
    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@7b
    move-result-object v1

    #@7c
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@7f
    .line 192
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_82
    .catchall {:try_start_76 .. :try_end_82} :catchall_88

    #@82
    .line 194
    if-eqz v7, :cond_61

    #@84
    .line 195
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@87
    goto :goto_61

    #@88
    .line 194
    .end local v8           #e:Ljava/lang/Exception;
    :catchall_88
    move-exception v0

    #@89
    if-eqz v7, :cond_8e

    #@8b
    .line 195
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@8e
    :cond_8e
    throw v0
.end method

.method private registerSubscriberChangeListener(Lcom/lge/ims/setting/ACSettings$ACSettingsChangeListener;)V
    .registers 13
    .parameter "listener"

    #@0
    .prologue
    .line 121
    const/4 v8, 0x0

    #@1
    .line 124
    .local v8, cursor:Landroid/database/Cursor;
    :try_start_1
    invoke-virtual {p0}, Lcom/lge/ims/setting/ACSettings;->getContentResolver()Landroid/content/ContentResolver;

    #@4
    move-result-object v0

    #@5
    sget-object v1, Lcom/lge/ims/provider/ac/AC$Settings$Subscriber;->CONTENT_URI:Landroid/net/Uri;

    #@7
    const/4 v2, 0x0

    #@8
    const/4 v3, 0x0

    #@9
    const/4 v4, 0x0

    #@a
    const/4 v5, 0x0

    #@b
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@e
    move-result-object v8

    #@f
    .line 126
    if-eqz v8, :cond_62

    #@11
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    #@14
    move-result v0

    #@15
    if-eqz v0, :cond_62

    #@17
    .line 128
    const-string v0, "usim"

    #@19
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@1c
    move-result v7

    #@1d
    .line 129
    .local v7, column:I
    const v0, 0x7f060023

    #@20
    invoke-virtual {p0, v0}, Lcom/lge/ims/setting/ACSettings;->getString(I)Ljava/lang/String;

    #@23
    move-result-object v0

    #@24
    invoke-direct {p0, v0}, Lcom/lge/ims/setting/ACSettings;->getCheckBoxPreference(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;

    #@27
    move-result-object v6

    #@28
    .line 130
    .local v6, checkbox:Landroid/preference/CheckBoxPreference;
    invoke-direct {p0, v6, p1}, Lcom/lge/ims/setting/ACSettings;->setListener(Landroid/preference/CheckBoxPreference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@2b
    .line 131
    invoke-direct {p0, v8, v7, v6}, Lcom/lge/ims/setting/ACSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/CheckBoxPreference;)V

    #@2e
    .line 134
    const-string v0, "phone_number"

    #@30
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@33
    move-result v7

    #@34
    .line 135
    const v0, 0x7f060024

    #@37
    invoke-virtual {p0, v0}, Lcom/lge/ims/setting/ACSettings;->getString(I)Ljava/lang/String;

    #@3a
    move-result-object v0

    #@3b
    invoke-direct {p0, v0}, Lcom/lge/ims/setting/ACSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@3e
    move-result-object v10

    #@3f
    .line 136
    .local v10, edit:Landroid/preference/EditTextPreference;
    invoke-direct {p0, v10, p1}, Lcom/lge/ims/setting/ACSettings;->setListener(Landroid/preference/EditTextPreference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@42
    .line 137
    invoke-direct {p0, v8, v7, v10}, Lcom/lge/ims/setting/ACSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@45
    .line 138
    invoke-direct {p0, v10}, Lcom/lge/ims/setting/ACSettings;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V

    #@48
    .line 141
    const-string v0, "imsi"

    #@4a
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@4d
    move-result v7

    #@4e
    .line 142
    const v0, 0x7f060025

    #@51
    invoke-virtual {p0, v0}, Lcom/lge/ims/setting/ACSettings;->getString(I)Ljava/lang/String;

    #@54
    move-result-object v0

    #@55
    invoke-direct {p0, v0}, Lcom/lge/ims/setting/ACSettings;->getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@58
    move-result-object v10

    #@59
    .line 143
    invoke-direct {p0, v10, p1}, Lcom/lge/ims/setting/ACSettings;->setListener(Landroid/preference/EditTextPreference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@5c
    .line 144
    invoke-direct {p0, v8, v7, v10}, Lcom/lge/ims/setting/ACSettings;->setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V

    #@5f
    .line 145
    invoke-direct {p0, v10}, Lcom/lge/ims/setting/ACSettings;->setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V
    :try_end_62
    .catchall {:try_start_1 .. :try_end_62} :catchall_8e
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_62} :catch_68
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_62} :catch_7b

    #@62
    .line 154
    .end local v6           #checkbox:Landroid/preference/CheckBoxPreference;
    .end local v7           #column:I
    .end local v10           #edit:Landroid/preference/EditTextPreference;
    :cond_62
    if-eqz v8, :cond_67

    #@64
    .line 155
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@67
    .line 158
    :cond_67
    :goto_67
    return-void

    #@68
    .line 147
    :catch_68
    move-exception v9

    #@69
    .line 148
    .local v9, e:Landroid/database/sqlite/SQLiteException;
    :try_start_69
    const-string v0, "ACSettings"

    #@6b
    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    #@6e
    move-result-object v1

    #@6f
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@72
    .line 149
    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_75
    .catchall {:try_start_69 .. :try_end_75} :catchall_8e

    #@75
    .line 154
    if-eqz v8, :cond_67

    #@77
    .line 155
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@7a
    goto :goto_67

    #@7b
    .line 150
    .end local v9           #e:Landroid/database/sqlite/SQLiteException;
    :catch_7b
    move-exception v9

    #@7c
    .line 151
    .local v9, e:Ljava/lang/Exception;
    :try_start_7c
    const-string v0, "ACSettings"

    #@7e
    invoke-virtual {v9}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@81
    move-result-object v1

    #@82
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@85
    .line 152
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_88
    .catchall {:try_start_7c .. :try_end_88} :catchall_8e

    #@88
    .line 154
    if-eqz v8, :cond_67

    #@8a
    .line 155
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@8d
    goto :goto_67

    #@8e
    .line 154
    .end local v9           #e:Ljava/lang/Exception;
    :catchall_8e
    move-exception v0

    #@8f
    if-eqz v8, :cond_94

    #@91
    .line 155
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@94
    :cond_94
    throw v0
.end method

.method private setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V
    .registers 4
    .parameter "edit"

    #@0
    .prologue
    .line 287
    if-nez p1, :cond_3

    #@2
    .line 292
    :goto_2
    return-void

    #@3
    .line 291
    :cond_3
    invoke-virtual {p1}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    #@6
    move-result-object v0

    #@7
    const/4 v1, 0x2

    #@8
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    #@b
    goto :goto_2
.end method

.method private setListener(Landroid/preference/CheckBoxPreference;Landroid/preference/Preference$OnPreferenceChangeListener;)V
    .registers 3
    .parameter "checkbox"
    .parameter "listener"

    #@0
    .prologue
    .line 295
    if-eqz p1, :cond_5

    #@2
    .line 296
    invoke-virtual {p1, p2}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@5
    .line 298
    :cond_5
    return-void
.end method

.method private setListener(Landroid/preference/EditTextPreference;Landroid/preference/Preference$OnPreferenceChangeListener;)V
    .registers 3
    .parameter "edit"
    .parameter "listener"

    #@0
    .prologue
    .line 307
    if-eqz p1, :cond_5

    #@2
    .line 308
    invoke-virtual {p1, p2}, Landroid/preference/EditTextPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@5
    .line 310
    :cond_5
    return-void
.end method

.method private setListener(Landroid/preference/ListPreference;Landroid/preference/Preference$OnPreferenceChangeListener;)V
    .registers 3
    .parameter "list"
    .parameter "listener"

    #@0
    .prologue
    .line 301
    if-eqz p1, :cond_5

    #@2
    .line 302
    invoke-virtual {p1, p2}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@5
    .line 304
    :cond_5
    return-void
.end method

.method private setSummary(Landroid/preference/Preference;Ljava/lang/Object;)V
    .registers 7
    .parameter "preference"
    .parameter "newValue"

    #@0
    .prologue
    .line 400
    if-eqz p1, :cond_4

    #@2
    if-nez p2, :cond_5

    #@4
    .line 417
    :cond_4
    :goto_4
    return-void

    #@5
    .line 404
    :cond_5
    instance-of v3, p1, Landroid/preference/CheckBoxPreference;

    #@7
    if-nez v3, :cond_4

    #@9
    .line 406
    instance-of v3, p1, Landroid/preference/EditTextPreference;

    #@b
    if-eqz v3, :cond_15

    #@d
    .line 407
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {p1, v3}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    #@14
    goto :goto_4

    #@15
    .line 408
    :cond_15
    instance-of v3, p1, Landroid/preference/ListPreference;

    #@17
    if-eqz v3, :cond_4

    #@19
    move-object v1, p1

    #@1a
    .line 409
    check-cast v1, Landroid/preference/ListPreference;

    #@1c
    .line 410
    .local v1, list:Landroid/preference/ListPreference;
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v1, v3}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    #@23
    move-result v2

    #@24
    .line 412
    .local v2, valueIndex:I
    if-ltz v2, :cond_4

    #@26
    .line 413
    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    #@29
    move-result-object v0

    #@2a
    .line 414
    .local v0, entries:[Ljava/lang/CharSequence;
    aget-object v3, v0, v2

    #@2c
    invoke-virtual {v1, v3}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    #@2f
    goto :goto_4
.end method

.method private setValue(Landroid/database/Cursor;ILandroid/preference/CheckBoxPreference;)V
    .registers 7
    .parameter "cursor"
    .parameter "index"
    .parameter "checkbox"

    #@0
    .prologue
    .line 313
    const/4 v1, -0x1

    #@1
    if-ne p2, v1, :cond_b

    #@3
    .line 314
    const-string v1, "ACSettings"

    #@5
    const-string v2, "index is (-1)"

    #@7
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 340
    :cond_a
    :goto_a
    return-void

    #@b
    .line 318
    :cond_b
    if-nez p1, :cond_15

    #@d
    .line 319
    const-string v1, "ACSettings"

    #@f
    const-string v2, "Cursor is null"

    #@11
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@14
    goto :goto_a

    #@15
    .line 323
    :cond_15
    if-nez p3, :cond_1f

    #@17
    .line 324
    const-string v1, "ACSettings"

    #@19
    const-string v2, "CheckBoxPreference is null"

    #@1b
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@1e
    goto :goto_a

    #@1f
    .line 328
    :cond_1f
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    .line 330
    .local v0, value:Ljava/lang/String;
    if-eqz v0, :cond_a

    #@25
    .line 334
    const-string v1, "true"

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@2a
    move-result v1

    #@2b
    if-eqz v1, :cond_32

    #@2d
    .line 335
    const/4 v1, 0x1

    #@2e
    invoke-virtual {p3, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    #@31
    goto :goto_a

    #@32
    .line 338
    :cond_32
    const/4 v1, 0x0

    #@33
    invoke-virtual {p3, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    #@36
    goto :goto_a
.end method

.method private setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V
    .registers 7
    .parameter "cursor"
    .parameter "index"
    .parameter "edit"

    #@0
    .prologue
    .line 343
    const/4 v1, -0x1

    #@1
    if-ne p2, v1, :cond_4

    #@3
    .line 365
    :cond_3
    :goto_3
    return-void

    #@4
    .line 347
    :cond_4
    if-nez p1, :cond_e

    #@6
    .line 348
    const-string v1, "ACSettings"

    #@8
    const-string v2, "Cursor is null"

    #@a
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    goto :goto_3

    #@e
    .line 352
    :cond_e
    if-nez p3, :cond_18

    #@10
    .line 353
    const-string v1, "ACSettings"

    #@12
    const-string v2, "EditTextPreference is null"

    #@14
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@17
    goto :goto_3

    #@18
    .line 357
    :cond_18
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    .line 359
    .local v0, value:Ljava/lang/String;
    if-eqz v0, :cond_3

    #@1e
    .line 363
    invoke-virtual {p3, v0}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    #@21
    .line 364
    invoke-virtual {p3, v0}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    #@24
    goto :goto_3
.end method

.method private setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V
    .registers 9
    .parameter "cursor"
    .parameter "index"
    .parameter "list"

    #@0
    .prologue
    .line 368
    const/4 v2, -0x1

    #@1
    if-ne p2, v2, :cond_4

    #@3
    .line 397
    :cond_3
    :goto_3
    return-void

    #@4
    .line 372
    :cond_4
    if-nez p1, :cond_e

    #@6
    .line 373
    const-string v2, "ACSettings"

    #@8
    const-string v3, "Cursor is null"

    #@a
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    goto :goto_3

    #@e
    .line 377
    :cond_e
    if-nez p3, :cond_18

    #@10
    .line 378
    const-string v2, "ACSettings"

    #@12
    const-string v3, "ListPreference is null"

    #@14
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@17
    goto :goto_3

    #@18
    .line 382
    :cond_18
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    .line 384
    .local v0, value:Ljava/lang/String;
    if-eqz v0, :cond_3

    #@1e
    .line 388
    invoke-virtual {p3, v0}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    #@21
    move-result v1

    #@22
    .line 390
    .local v1, valueIndex:I
    if-gez v1, :cond_4b

    #@24
    .line 391
    const-string v2, "ACSettings"

    #@26
    new-instance v3, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v4, "setValue :: list - key="

    #@2d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    invoke-virtual {p3}, Landroid/preference/ListPreference;->getKey()Ljava/lang/String;

    #@34
    move-result-object v4

    #@35
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v3

    #@39
    const-string v4, ", value="

    #@3b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v3

    #@3f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v3

    #@43
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v3

    #@47
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@4a
    goto :goto_3

    #@4b
    .line 395
    :cond_4b
    invoke-virtual {p3, v1}, Landroid/preference/ListPreference;->setValueIndex(I)V

    #@4e
    .line 396
    invoke-virtual {p3}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    #@51
    move-result-object v2

    #@52
    invoke-virtual {p3, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    #@55
    goto :goto_3
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 39
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    #@3
    .line 41
    const/high16 v0, 0x7f04

    #@5
    invoke-virtual {p0, v0}, Lcom/lge/ims/setting/ACSettings;->addPreferencesFromResource(I)V

    #@8
    .line 50
    new-instance v0, Lcom/lge/ims/setting/ACSettings$ACSettingsChangeListener;

    #@a
    sget-object v1, Lcom/lge/ims/provider/ac/AC$Settings$Common;->CONTENT_URI:Landroid/net/Uri;

    #@c
    invoke-direct {v0, p0, v1}, Lcom/lge/ims/setting/ACSettings$ACSettingsChangeListener;-><init>(Lcom/lge/ims/setting/ACSettings;Landroid/net/Uri;)V

    #@f
    iput-object v0, p0, Lcom/lge/ims/setting/ACSettings;->onCommonListener:Lcom/lge/ims/setting/ACSettings$ACSettingsChangeListener;

    #@11
    .line 51
    new-instance v0, Lcom/lge/ims/setting/ACSettings$ACSettingsChangeListener;

    #@13
    sget-object v1, Lcom/lge/ims/provider/ac/AC$Settings$Subscriber;->CONTENT_URI:Landroid/net/Uri;

    #@15
    invoke-direct {v0, p0, v1}, Lcom/lge/ims/setting/ACSettings$ACSettingsChangeListener;-><init>(Lcom/lge/ims/setting/ACSettings;Landroid/net/Uri;)V

    #@18
    iput-object v0, p0, Lcom/lge/ims/setting/ACSettings;->onSubscriberListener:Lcom/lge/ims/setting/ACSettings$ACSettingsChangeListener;

    #@1a
    .line 52
    new-instance v0, Lcom/lge/ims/setting/ACSettings$ACSettingsChangeListener;

    #@1c
    sget-object v1, Lcom/lge/ims/provider/ac/AC$Settings$ConnectionServer;->CONTENT_URI:Landroid/net/Uri;

    #@1e
    invoke-direct {v0, p0, v1}, Lcom/lge/ims/setting/ACSettings$ACSettingsChangeListener;-><init>(Lcom/lge/ims/setting/ACSettings;Landroid/net/Uri;)V

    #@21
    iput-object v0, p0, Lcom/lge/ims/setting/ACSettings;->onConnectionServerListener:Lcom/lge/ims/setting/ACSettings$ACSettingsChangeListener;

    #@23
    .line 57
    iget-object v0, p0, Lcom/lge/ims/setting/ACSettings;->onCommonListener:Lcom/lge/ims/setting/ACSettings$ACSettingsChangeListener;

    #@25
    invoke-direct {p0, v0}, Lcom/lge/ims/setting/ACSettings;->registerCommonChangeListener(Lcom/lge/ims/setting/ACSettings$ACSettingsChangeListener;)V

    #@28
    .line 62
    iget-object v0, p0, Lcom/lge/ims/setting/ACSettings;->onSubscriberListener:Lcom/lge/ims/setting/ACSettings$ACSettingsChangeListener;

    #@2a
    invoke-direct {p0, v0}, Lcom/lge/ims/setting/ACSettings;->registerSubscriberChangeListener(Lcom/lge/ims/setting/ACSettings$ACSettingsChangeListener;)V

    #@2d
    .line 67
    iget-object v0, p0, Lcom/lge/ims/setting/ACSettings;->onConnectionServerListener:Lcom/lge/ims/setting/ACSettings$ACSettingsChangeListener;

    #@2f
    invoke-direct {p0, v0}, Lcom/lge/ims/setting/ACSettings;->registerConnectionServerChangeListener(Lcom/lge/ims/setting/ACSettings$ACSettingsChangeListener;)V

    #@32
    .line 68
    return-void
.end method

.method public onDestroy()V
    .registers 1

    #@0
    .prologue
    .line 72
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    #@3
    .line 73
    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .registers 4
    .parameter "preferenceScreen"
    .parameter "preference"

    #@0
    .prologue
    .line 79
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method
