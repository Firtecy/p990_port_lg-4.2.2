.class Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;
.super Ljava/lang/Object;
.source "IMSProvisioning.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/setting/IMSProvisioning;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "IMSProvisioningChangeListener"
.end annotation


# instance fields
.field private mTableName:Ljava/lang/String;

.field final synthetic this$0:Lcom/lge/ims/setting/IMSProvisioning;


# direct methods
.method public constructor <init>(Lcom/lge/ims/setting/IMSProvisioning;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter "dbTable"

    #@0
    .prologue
    .line 1384
    iput-object p1, p0, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 1382
    const/4 v0, 0x0

    #@6
    iput-object v0, p0, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;->mTableName:Ljava/lang/String;

    #@8
    .line 1385
    iput-object p2, p0, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;->mTableName:Ljava/lang/String;

    #@a
    .line 1386
    return-void
.end method


# virtual methods
.method public getTableName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1389
    iget-object v0, p0, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;->mTableName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .registers 14
    .parameter "preference"
    .parameter "newValue"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    .line 1393
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    #@5
    move-result-object v3

    #@6
    .line 1395
    .local v3, key:Ljava/lang/String;
    const-string v7, "LGIMSProvisioning"

    #@8
    new-instance v8, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    iget-object v9, p0, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;->mTableName:Ljava/lang/String;

    #@f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v8

    #@13
    const-string v9, " :: key: "

    #@15
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v8

    #@19
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v8

    #@1d
    const-string v9, " -> value:"

    #@1f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v8

    #@23
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@26
    move-result-object v9

    #@27
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v8

    #@2b
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v8

    #@2f
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 1397
    iget-object v7, p0, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@34
    invoke-static {v7}, Lcom/lge/ims/setting/IMSProvisioning;->access$000(Lcom/lge/ims/setting/IMSProvisioning;)Landroid/database/sqlite/SQLiteDatabase;

    #@37
    move-result-object v7

    #@38
    if-nez v7, :cond_5b

    #@3a
    .line 1398
    const-string v6, "LGIMSProvisioning"

    #@3c
    new-instance v7, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v8, "DB table ("

    #@43
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v7

    #@47
    iget-object v8, p0, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;->mTableName:Ljava/lang/String;

    #@49
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v7

    #@4d
    const-string v8, ") is null"

    #@4f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v7

    #@53
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v7

    #@57
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@5a
    .line 1453
    :goto_5a
    return v5

    #@5b
    .line 1402
    :cond_5b
    const/4 v0, 0x0

    #@5c
    .line 1403
    .local v0, affectedRows:I
    new-instance v4, Landroid/content/ContentValues;

    #@5e
    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    #@61
    .line 1405
    .local v4, values:Landroid/content/ContentValues;
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@64
    move-result-object v7

    #@65
    invoke-virtual {v4, v3, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@68
    .line 1407
    iget-object v7, p0, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@6a
    invoke-static {v7}, Lcom/lge/ims/setting/IMSProvisioning;->access$000(Lcom/lge/ims/setting/IMSProvisioning;)Landroid/database/sqlite/SQLiteDatabase;

    #@6d
    move-result-object v7

    #@6e
    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    #@71
    .line 1410
    :try_start_71
    iget-object v7, p0, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@73
    invoke-static {v7}, Lcom/lge/ims/setting/IMSProvisioning;->access$000(Lcom/lge/ims/setting/IMSProvisioning;)Landroid/database/sqlite/SQLiteDatabase;

    #@76
    move-result-object v7

    #@77
    iget-object v8, p0, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;->mTableName:Ljava/lang/String;

    #@79
    const-string v9, "id = \'1\'"

    #@7b
    const/4 v10, 0x0

    #@7c
    invoke-virtual {v7, v8, v4, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@7f
    move-result v0

    #@80
    .line 1411
    iget-object v7, p0, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@82
    invoke-static {v7}, Lcom/lge/ims/setting/IMSProvisioning;->access$000(Lcom/lge/ims/setting/IMSProvisioning;)Landroid/database/sqlite/SQLiteDatabase;

    #@85
    move-result-object v7

    #@86
    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_89
    .catchall {:try_start_71 .. :try_end_89} :catchall_c8
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_71 .. :try_end_89} :catch_ba

    #@89
    .line 1415
    iget-object v7, p0, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@8b
    invoke-static {v7}, Lcom/lge/ims/setting/IMSProvisioning;->access$000(Lcom/lge/ims/setting/IMSProvisioning;)Landroid/database/sqlite/SQLiteDatabase;

    #@8e
    move-result-object v7

    #@8f
    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@92
    .line 1418
    :goto_92
    const-string v7, "LGIMSProvisioning"

    #@94
    new-instance v8, Ljava/lang/StringBuilder;

    #@96
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@99
    iget-object v9, p0, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;->mTableName:Ljava/lang/String;

    #@9b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v8

    #@9f
    const-string v9, " :: updated row count: "

    #@a1
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v8

    #@a5
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v8

    #@a9
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ac
    move-result-object v8

    #@ad
    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b0
    .line 1420
    if-eq v0, v6, :cond_d3

    #@b2
    .line 1421
    const-string v6, "LGIMSProvisioning"

    #@b4
    const-string v7, "Update fails"

    #@b6
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b9
    goto :goto_5a

    #@ba
    .line 1412
    :catch_ba
    move-exception v2

    #@bb
    .line 1413
    .local v2, e:Landroid/database/sqlite/SQLiteException;
    :try_start_bb
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_be
    .catchall {:try_start_bb .. :try_end_be} :catchall_c8

    #@be
    .line 1415
    iget-object v7, p0, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@c0
    invoke-static {v7}, Lcom/lge/ims/setting/IMSProvisioning;->access$000(Lcom/lge/ims/setting/IMSProvisioning;)Landroid/database/sqlite/SQLiteDatabase;

    #@c3
    move-result-object v7

    #@c4
    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@c7
    goto :goto_92

    #@c8
    .end local v2           #e:Landroid/database/sqlite/SQLiteException;
    :catchall_c8
    move-exception v5

    #@c9
    iget-object v6, p0, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@cb
    invoke-static {v6}, Lcom/lge/ims/setting/IMSProvisioning;->access$000(Lcom/lge/ims/setting/IMSProvisioning;)Landroid/database/sqlite/SQLiteDatabase;

    #@ce
    move-result-object v6

    #@cf
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@d2
    throw v5

    #@d3
    .line 1425
    :cond_d3
    iget-object v5, p0, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;->mTableName:Ljava/lang/String;

    #@d5
    const-string v7, "lgims_subscriber"

    #@d7
    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@da
    move-result v5

    #@db
    if-eqz v5, :cond_13f

    #@dd
    .line 1426
    const-string v5, "admin_ac"

    #@df
    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e2
    move-result v5

    #@e3
    if-eqz v5, :cond_10a

    #@e5
    .line 1428
    new-instance v1, Landroid/content/ContentValues;

    #@e7
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    #@ea
    .line 1430
    .local v1, cvs:Landroid/content/ContentValues;
    const-string v5, "availability"

    #@ec
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@ef
    move-result-object v7

    #@f0
    invoke-virtual {v1, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@f3
    .line 1433
    :try_start_f3
    iget-object v5, p0, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@f5
    invoke-virtual {v5}, Lcom/lge/ims/setting/IMSProvisioning;->getContentResolver()Landroid/content/ContentResolver;

    #@f8
    move-result-object v5

    #@f9
    sget-object v7, Lcom/lge/ims/provider/ac/AC$Settings$Common;->CONTENT_URI:Landroid/net/Uri;

    #@fb
    const/4 v8, 0x0

    #@fc
    const/4 v9, 0x0

    #@fd
    invoke-virtual {v5, v7, v1, v8, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@100
    move-result v5

    #@101
    if-gtz v5, :cond_10a

    #@103
    .line 1434
    const-string v5, "LGIMSProvisioning"

    #@105
    const-string v7, "Updating AC on/off failed"

    #@107
    invoke-static {v5, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_10a
    .catch Ljava/lang/Exception; {:try_start_f3 .. :try_end_10a} :catch_147

    #@10a
    .line 1441
    .end local v1           #cvs:Landroid/content/ContentValues;
    :cond_10a
    :goto_10a
    const-string v5, "admin_ims"

    #@10c
    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10f
    move-result v5

    #@110
    if-nez v5, :cond_13a

    #@112
    const-string v5, "admin_isim"

    #@114
    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@117
    move-result v5

    #@118
    if-nez v5, :cond_13a

    #@11a
    const-string v5, "admin_usim"

    #@11c
    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11f
    move-result v5

    #@120
    if-nez v5, :cond_13a

    #@122
    const-string v5, "admin_ac"

    #@124
    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@127
    move-result v5

    #@128
    if-nez v5, :cond_13a

    #@12a
    const-string v5, "admin_debug"

    #@12c
    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12f
    move-result v5

    #@130
    if-nez v5, :cond_13a

    #@132
    const-string v5, "admin_services"

    #@134
    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@137
    move-result v5

    #@138
    if-eqz v5, :cond_13f

    #@13a
    .line 1447
    :cond_13a
    iget-object v5, p0, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@13c
    invoke-static {v5, v6}, Lcom/lge/ims/setting/IMSProvisioning;->access$102(Lcom/lge/ims/setting/IMSProvisioning;Z)Z

    #@13f
    .line 1451
    :cond_13f
    iget-object v5, p0, Lcom/lge/ims/setting/IMSProvisioning$IMSProvisioningChangeListener;->this$0:Lcom/lge/ims/setting/IMSProvisioning;

    #@141
    invoke-static {v5, p1, p2}, Lcom/lge/ims/setting/IMSProvisioning;->access$200(Lcom/lge/ims/setting/IMSProvisioning;Landroid/preference/Preference;Ljava/lang/Object;)V

    #@144
    move v5, v6

    #@145
    .line 1453
    goto/16 :goto_5a

    #@147
    .line 1436
    .restart local v1       #cvs:Landroid/content/ContentValues;
    :catch_147
    move-exception v2

    #@148
    .line 1437
    .local v2, e:Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    #@14b
    goto :goto_10a
.end method
