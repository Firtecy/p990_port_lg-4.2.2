.class Lcom/lge/ims/setting/IMSSettings$ACSettingsChangeListener;
.super Ljava/lang/Object;
.source "IMSSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/setting/IMSSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ACSettingsChangeListener"
.end annotation


# instance fields
.field private mContentURI:Landroid/net/Uri;

.field final synthetic this$0:Lcom/lge/ims/setting/IMSSettings;


# direct methods
.method public constructor <init>(Lcom/lge/ims/setting/IMSSettings;Landroid/net/Uri;)V
    .registers 4
    .parameter
    .parameter "uri"

    #@0
    .prologue
    .line 1330
    iput-object p1, p0, Lcom/lge/ims/setting/IMSSettings$ACSettingsChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 1328
    const/4 v0, 0x0

    #@6
    iput-object v0, p0, Lcom/lge/ims/setting/IMSSettings$ACSettingsChangeListener;->mContentURI:Landroid/net/Uri;

    #@8
    .line 1331
    iput-object p2, p0, Lcom/lge/ims/setting/IMSSettings$ACSettingsChangeListener;->mContentURI:Landroid/net/Uri;

    #@a
    .line 1332
    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .registers 12
    .parameter "preference"
    .parameter "newValue"

    #@0
    .prologue
    const/4 v7, 0x3

    #@1
    const/4 v5, 0x1

    #@2
    .line 1335
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    #@5
    move-result-object v3

    #@6
    .line 1338
    .local v3, key:Ljava/lang/String;
    if-eqz v3, :cond_12

    #@8
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@b
    move-result v6

    #@c
    if-le v6, v7, :cond_12

    #@e
    .line 1339
    invoke-virtual {v3, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@11
    move-result-object v3

    #@12
    .line 1342
    :cond_12
    const-string v6, "LGIMSSettings"

    #@14
    new-instance v7, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    iget-object v8, p0, Lcom/lge/ims/setting/IMSSettings$ACSettingsChangeListener;->mContentURI:Landroid/net/Uri;

    #@1b
    invoke-virtual {v8}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    #@1e
    move-result-object v8

    #@1f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v7

    #@23
    const-string v8, " :: "

    #@25
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v7

    #@29
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v7

    #@2d
    const-string v8, "="

    #@2f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v7

    #@33
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@36
    move-result-object v8

    #@37
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v7

    #@3b
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v7

    #@3f
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    .line 1344
    const/4 v0, 0x0

    #@43
    .line 1345
    .local v0, affectedRows:I
    iget-object v6, p0, Lcom/lge/ims/setting/IMSSettings$ACSettingsChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@45
    invoke-virtual {v6}, Lcom/lge/ims/setting/IMSSettings;->getContentResolver()Landroid/content/ContentResolver;

    #@48
    move-result-object v1

    #@49
    .line 1346
    .local v1, cr:Landroid/content/ContentResolver;
    new-instance v4, Landroid/content/ContentValues;

    #@4b
    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    #@4e
    .line 1348
    .local v4, values:Landroid/content/ContentValues;
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@51
    move-result-object v6

    #@52
    invoke-virtual {v4, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@55
    .line 1351
    :try_start_55
    iget-object v6, p0, Lcom/lge/ims/setting/IMSSettings$ACSettingsChangeListener;->mContentURI:Landroid/net/Uri;

    #@57
    const/4 v7, 0x0

    #@58
    const/4 v8, 0x0

    #@59
    invoke-virtual {v1, v6, v4, v7, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_5c
    .catch Ljava/lang/Exception; {:try_start_55 .. :try_end_5c} :catch_80

    #@5c
    move-result v0

    #@5d
    .line 1356
    :goto_5d
    const-string v6, "LGIMSSettings"

    #@5f
    new-instance v7, Ljava/lang/StringBuilder;

    #@61
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    const-string v8, "updated row count: "

    #@66
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v7

    #@6a
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v7

    #@6e
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v7

    #@72
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@75
    .line 1358
    if-eq v0, v5, :cond_85

    #@77
    .line 1359
    const-string v5, "LGIMSSettings"

    #@79
    const-string v6, "Update fails"

    #@7b
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7e
    .line 1360
    const/4 v5, 0x0

    #@7f
    .line 1376
    :goto_7f
    return v5

    #@80
    .line 1352
    :catch_80
    move-exception v2

    #@81
    .line 1353
    .local v2, e:Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    #@84
    goto :goto_5d

    #@85
    .line 1363
    .end local v2           #e:Ljava/lang/Exception;
    :cond_85
    const-string v6, "server_selection"

    #@87
    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8a
    move-result v6

    #@8b
    if-eqz v6, :cond_b7

    #@8d
    .line 1364
    iget-object v6, p0, Lcom/lge/ims/setting/IMSSettings$ACSettingsChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@8f
    invoke-static {v6}, Lcom/lge/ims/setting/IMSSettings;->access$1800(Lcom/lge/ims/setting/IMSSettings;)Ljava/lang/String;

    #@92
    move-result-object v6

    #@93
    if-eqz v6, :cond_b7

    #@95
    .line 1365
    iget-object v6, p0, Lcom/lge/ims/setting/IMSSettings$ACSettingsChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@97
    invoke-static {v6}, Lcom/lge/ims/setting/IMSSettings;->access$1800(Lcom/lge/ims/setting/IMSSettings;)Ljava/lang/String;

    #@9a
    move-result-object v6

    #@9b
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@9e
    move-result-object v7

    #@9f
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a2
    move-result v6

    #@a3
    if-nez v6, :cond_b7

    #@a5
    .line 1366
    iget-object v6, p0, Lcom/lge/ims/setting/IMSSettings$ACSettingsChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@a7
    invoke-virtual {v6}, Lcom/lge/ims/setting/IMSSettings;->getBaseContext()Landroid/content/Context;

    #@aa
    move-result-object v6

    #@ab
    invoke-static {v6}, Lcom/lge/ims/service/ac/ACContentHelper;->deleteContentForAC(Landroid/content/Context;)V

    #@ae
    .line 1367
    iget-object v6, p0, Lcom/lge/ims/setting/IMSSettings$ACSettingsChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@b0
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@b3
    move-result-object v7

    #@b4
    invoke-static {v6, v7}, Lcom/lge/ims/setting/IMSSettings;->access$1802(Lcom/lge/ims/setting/IMSSettings;Ljava/lang/String;)Ljava/lang/String;

    #@b7
    .line 1372
    :cond_b7
    iget-object v6, p0, Lcom/lge/ims/setting/IMSSettings$ACSettingsChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@b9
    invoke-static {v6, v5}, Lcom/lge/ims/setting/IMSSettings;->access$1902(Lcom/lge/ims/setting/IMSSettings;Z)Z

    #@bc
    .line 1374
    iget-object v6, p0, Lcom/lge/ims/setting/IMSSettings$ACSettingsChangeListener;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@be
    invoke-static {v6, p1, p2}, Lcom/lge/ims/setting/IMSSettings;->access$1700(Lcom/lge/ims/setting/IMSSettings;Landroid/preference/Preference;Ljava/lang/Object;)V

    #@c1
    goto :goto_7f
.end method
