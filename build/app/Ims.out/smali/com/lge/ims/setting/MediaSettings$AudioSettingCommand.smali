.class public Lcom/lge/ims/setting/MediaSettings$AudioSettingCommand;
.super Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;
.source "MediaSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/setting/MediaSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AudioSettingCommand"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/setting/MediaSettings;


# direct methods
.method public constructor <init>(Lcom/lge/ims/setting/MediaSettings;Ljava/lang/String;Ljava/lang/String;I)V
    .registers 7
    .parameter
    .parameter "tableName"
    .parameter "typePrefix"
    .parameter "sessionType"

    #@0
    .prologue
    .line 787
    iput-object p1, p0, Lcom/lge/ims/setting/MediaSettings$AudioSettingCommand;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@2
    .line 789
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;-><init>(Lcom/lge/ims/setting/MediaSettings;Ljava/lang/String;Ljava/lang/String;I)V

    #@5
    .line 791
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mMediaCodecInfo:Lcom/lge/ims/setting/MediaSettings$IMSMediaCodecInfo;

    #@7
    iget-object v0, v0, Lcom/lge/ims/setting/MediaSettings$IMSMediaCodecInfo;->codecAudioCommonAttribute:[Ljava/lang/String;

    #@9
    invoke-virtual {p0, p3, v0}, Lcom/lge/ims/setting/MediaSettings$AudioSettingCommand;->setCodecAttributes(Ljava/lang/String;[Ljava/lang/String;)V

    #@c
    .line 792
    const-string v0, "AMR"

    #@e
    iget-object v1, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mMediaCodecInfo:Lcom/lge/ims/setting/MediaSettings$IMSMediaCodecInfo;

    #@10
    iget-object v1, v1, Lcom/lge/ims/setting/MediaSettings$IMSMediaCodecInfo;->codecAudioAMRAttribute:[Ljava/lang/String;

    #@12
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/setting/MediaSettings$AudioSettingCommand;->setCodecAttributes(Ljava/lang/String;[Ljava/lang/String;)V

    #@15
    .line 793
    return-void
.end method


# virtual methods
.method public loadCodecList(I)Z
    .registers 11
    .parameter "index"

    #@0
    .prologue
    .line 797
    invoke-super {p0, p1}, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->loadCodecList(I)Z

    #@3
    .line 800
    iget-object v5, p0, Lcom/lge/ims/setting/MediaSettings$AudioSettingCommand;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@5
    iget-object v6, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mTableName:Ljava/lang/String;

    #@7
    new-instance v7, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    iget-object v8, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mTypePrefix:Ljava/lang/String;

    #@e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v7

    #@12
    const-string v8, "_"

    #@14
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v7

    #@18
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v7

    #@1c
    const-string v8, "_"

    #@1e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v7

    #@22
    iget-object v8, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mCodecTypeName:Ljava/lang/String;

    #@24
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v7

    #@28
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v7

    #@2c
    invoke-virtual {v5, v6, v7}, Lcom/lge/ims/setting/MediaSettings;->load(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2f
    move-result-object v4

    #@30
    .line 802
    .local v4, value:Ljava/lang/String;
    if-eqz v4, :cond_3a

    #@32
    const-string v5, ""

    #@34
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@37
    move-result v5

    #@38
    if-eqz v5, :cond_3c

    #@3a
    .line 803
    :cond_3a
    const/4 v5, 0x0

    #@3b
    .line 848
    :goto_3b
    return v5

    #@3c
    .line 806
    :cond_3c
    const-string v5, "None"

    #@3e
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@41
    move-result v5

    #@42
    if-nez v5, :cond_f7

    #@44
    .line 808
    iget-object v5, p0, Lcom/lge/ims/setting/MediaSettings$AudioSettingCommand;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@46
    new-instance v6, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    iget v7, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mSessionType:I

    #@4d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@50
    move-result-object v6

    #@51
    const-string v7, "_"

    #@53
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v6

    #@57
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v6

    #@5b
    const-string v7, "_"

    #@5d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v6

    #@61
    iget-object v7, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mCodecTypeName:Ljava/lang/String;

    #@63
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v6

    #@67
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6a
    move-result-object v6

    #@6b
    invoke-virtual {v5, v6}, Lcom/lge/ims/setting/MediaSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    #@6e
    move-result-object v2

    #@6f
    .line 810
    .local v2, pref:Landroid/preference/Preference;
    const-string v1, "AMR"

    #@71
    .line 811
    .local v1, inputValue:Ljava/lang/String;
    if-eqz v2, :cond_be

    #@73
    .line 813
    iget-object v5, p0, Lcom/lge/ims/setting/MediaSettings$AudioSettingCommand;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@75
    iget-object v6, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mTableName:Ljava/lang/String;

    #@77
    new-instance v7, Ljava/lang/StringBuilder;

    #@79
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@7c
    iget-object v8, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mTypePrefix:Ljava/lang/String;

    #@7e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v7

    #@82
    const-string v8, "_"

    #@84
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v7

    #@88
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v7

    #@8c
    const-string v8, "_"

    #@8e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v7

    #@92
    const-string v8, "sampling_rate"

    #@94
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v7

    #@98
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9b
    move-result-object v7

    #@9c
    invoke-virtual {v5, v6, v7}, Lcom/lge/ims/setting/MediaSettings;->load(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@9f
    move-result-object v3

    #@a0
    .line 816
    .local v3, samplingValue:Ljava/lang/String;
    if-eqz v3, :cond_be

    #@a2
    .line 818
    const-string v5, "AMR"

    #@a4
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a7
    move-result v5

    #@a8
    if-eqz v5, :cond_fa

    #@aa
    .line 819
    const-string v5, "16000"

    #@ac
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@af
    move-result v5

    #@b0
    if-eqz v5, :cond_b4

    #@b2
    .line 820
    const-string v1, "AMR-WB"

    #@b4
    .line 831
    :cond_b4
    :goto_b4
    iget-object v5, p0, Lcom/lge/ims/setting/MediaSettings$AudioSettingCommand;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@b6
    invoke-virtual {v5, v2, v1}, Lcom/lge/ims/setting/MediaSettings;->setValue(Landroid/preference/Preference;Ljava/lang/String;)Z

    #@b9
    .line 832
    iget-object v5, p0, Lcom/lge/ims/setting/MediaSettings$AudioSettingCommand;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@bb
    invoke-virtual {v5, v2, v1}, Lcom/lge/ims/setting/MediaSettings;->summaryChange(Landroid/preference/Preference;Ljava/lang/Object;)V

    #@be
    .line 837
    .end local v3           #samplingValue:Ljava/lang/String;
    :cond_be
    iget-object v5, p0, Lcom/lge/ims/setting/MediaSettings$AudioSettingCommand;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@c0
    new-instance v6, Ljava/lang/StringBuilder;

    #@c2
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@c5
    iget v7, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mSessionType:I

    #@c7
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v6

    #@cb
    const-string v7, "_"

    #@cd
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d0
    move-result-object v6

    #@d1
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d4
    move-result-object v6

    #@d5
    const-string v7, "_mode_set"

    #@d7
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@da
    move-result-object v6

    #@db
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@de
    move-result-object v6

    #@df
    invoke-static {v5, v6}, Lcom/lge/ims/setting/MediaSettings;->access$100(Lcom/lge/ims/setting/MediaSettings;Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@e2
    move-result-object v0

    #@e3
    .line 838
    .local v0, edit:Landroid/preference/EditTextPreference;
    const-string v5, "AMR-WB"

    #@e5
    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e8
    move-result v5

    #@e9
    if-eqz v5, :cond_110

    #@eb
    .line 840
    iget-object v5, p0, Lcom/lge/ims/setting/MediaSettings$AudioSettingCommand;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@ed
    const v6, 0x7f0603a0

    #@f0
    invoke-virtual {v5, v6}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@f3
    move-result-object v5

    #@f4
    invoke-virtual {v0, v5}, Landroid/preference/EditTextPreference;->setDialogMessage(Ljava/lang/CharSequence;)V

    #@f7
    .line 848
    .end local v0           #edit:Landroid/preference/EditTextPreference;
    .end local v1           #inputValue:Ljava/lang/String;
    .end local v2           #pref:Landroid/preference/Preference;
    :cond_f7
    :goto_f7
    const/4 v5, 0x1

    #@f8
    goto/16 :goto_3b

    #@fa
    .line 823
    .restart local v1       #inputValue:Ljava/lang/String;
    .restart local v2       #pref:Landroid/preference/Preference;
    .restart local v3       #samplingValue:Ljava/lang/String;
    :cond_fa
    const-string v5, "telephone-event"

    #@fc
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ff
    move-result v5

    #@100
    if-eqz v5, :cond_b4

    #@102
    .line 824
    const-string v5, "16000"

    #@104
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@107
    move-result v5

    #@108
    if-eqz v5, :cond_10d

    #@10a
    .line 825
    const-string v1, "telephone-event 16k"

    #@10c
    goto :goto_b4

    #@10d
    .line 828
    :cond_10d
    const-string v1, "telephone-event 8k"

    #@10f
    goto :goto_b4

    #@110
    .line 844
    .end local v3           #samplingValue:Ljava/lang/String;
    .restart local v0       #edit:Landroid/preference/EditTextPreference;
    :cond_110
    iget-object v5, p0, Lcom/lge/ims/setting/MediaSettings$AudioSettingCommand;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@112
    const v6, 0x7f06039f

    #@115
    invoke-virtual {v5, v6}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@118
    move-result-object v5

    #@119
    invoke-virtual {v0, v5}, Landroid/preference/EditTextPreference;->setDialogMessage(Ljava/lang/CharSequence;)V

    #@11c
    goto :goto_f7
.end method

.method public preferenceChange(ILjava/lang/String;Ljava/lang/String;)Z
    .registers 10
    .parameter "index"
    .parameter "attribute"
    .parameter "inputValue"

    #@0
    .prologue
    .line 853
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mCodecTypeName:Ljava/lang/String;

    #@2
    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5
    move-result v3

    #@6
    if-eqz v3, :cond_81

    #@8
    .line 855
    const-string v2, "8000"

    #@a
    .line 856
    .local v2, samplingRate:Ljava/lang/String;
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettings$AudioSettingCommand;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@c
    new-instance v4, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    iget v5, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mSessionType:I

    #@13
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    const-string v5, "_"

    #@19
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v4

    #@1d
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v4

    #@21
    const-string v5, "_mode_set"

    #@23
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v4

    #@27
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v4

    #@2b
    invoke-static {v3, v4}, Lcom/lge/ims/setting/MediaSettings;->access$100(Lcom/lge/ims/setting/MediaSettings;Ljava/lang/String;)Landroid/preference/EditTextPreference;

    #@2e
    move-result-object v0

    #@2f
    .line 858
    .local v0, edit:Landroid/preference/EditTextPreference;
    if-eqz v0, :cond_4f

    #@31
    .line 859
    const-string v3, "AMR-WB"

    #@33
    invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@36
    move-result v3

    #@37
    if-nez v3, :cond_41

    #@39
    const-string v3, "telephone-event 16k"

    #@3b
    invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3e
    move-result v3

    #@3f
    if-eqz v3, :cond_86

    #@41
    .line 861
    :cond_41
    const-string v2, "16000"

    #@43
    .line 862
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettings$AudioSettingCommand;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@45
    const v4, 0x7f0603a0

    #@48
    invoke-virtual {v3, v4}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@4b
    move-result-object v3

    #@4c
    invoke-virtual {v0, v3}, Landroid/preference/EditTextPreference;->setDialogMessage(Ljava/lang/CharSequence;)V

    #@4f
    .line 870
    :cond_4f
    :goto_4f
    const-string v3, "AMR-WB"

    #@51
    invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@54
    move-result v3

    #@55
    if-eqz v3, :cond_93

    #@57
    .line 871
    const-string p3, "AMR"

    #@59
    .line 877
    :cond_59
    :goto_59
    new-instance v3, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->mTypePrefix:Ljava/lang/String;

    #@60
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v3

    #@64
    const-string v4, "_"

    #@66
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v3

    #@6a
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v3

    #@6e
    const-string v4, "_"

    #@70
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v3

    #@74
    const-string v4, "sampling_rate"

    #@76
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v3

    #@7a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v1

    #@7e
    .line 878
    .local v1, query:Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/lge/ims/setting/MediaSettings$AudioSettingCommand;->updateCodec(Ljava/lang/String;Ljava/lang/String;)Z

    #@81
    .line 882
    .end local v0           #edit:Landroid/preference/EditTextPreference;
    .end local v1           #query:Ljava/lang/String;
    .end local v2           #samplingRate:Ljava/lang/String;
    :cond_81
    invoke-super {p0, p1, p2, p3}, Lcom/lge/ims/setting/MediaSettings$MediaSettingCommand;->preferenceChange(ILjava/lang/String;Ljava/lang/String;)Z

    #@84
    .line 884
    const/4 v3, 0x1

    #@85
    return v3

    #@86
    .line 866
    .restart local v0       #edit:Landroid/preference/EditTextPreference;
    .restart local v2       #samplingRate:Ljava/lang/String;
    :cond_86
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettings$AudioSettingCommand;->this$0:Lcom/lge/ims/setting/MediaSettings;

    #@88
    const v4, 0x7f06039f

    #@8b
    invoke-virtual {v3, v4}, Lcom/lge/ims/setting/MediaSettings;->getString(I)Ljava/lang/String;

    #@8e
    move-result-object v3

    #@8f
    invoke-virtual {v0, v3}, Landroid/preference/EditTextPreference;->setDialogMessage(Ljava/lang/CharSequence;)V

    #@92
    goto :goto_4f

    #@93
    .line 873
    :cond_93
    const-string v3, "telephone-event 16k"

    #@95
    invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@98
    move-result v3

    #@99
    if-nez v3, :cond_a3

    #@9b
    const-string v3, "telephone-event 8k"

    #@9d
    invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a0
    move-result v3

    #@a1
    if-eqz v3, :cond_59

    #@a3
    .line 874
    :cond_a3
    const-string p3, "telephone-event"

    #@a5
    goto :goto_59
.end method
