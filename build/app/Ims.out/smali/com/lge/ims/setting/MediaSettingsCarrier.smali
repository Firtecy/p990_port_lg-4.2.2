.class public Lcom/lge/ims/setting/MediaSettingsCarrier;
.super Landroid/preference/PreferenceActivity;
.source "MediaSettingsCarrier.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierPSVTChangeListener;,
        Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;,
        Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;,
        Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;
    }
.end annotation


# static fields
.field private static final LGIMS_COM_MEDIA:Ljava/lang/String; = "lgims_com_media"

.field private static final LGIMS_MEDIA_AUDIO:Ljava/lang/String; = "lgims_com_media_audio"

.field private static final LGIMS_MEDIA_CODEC_AUDIO_VOLTE:Ljava/lang/String; = "lgims_com_media_audio_codec_volte"

.field private static final LGIMS_MEDIA_CODEC_AUDIO_VT:Ljava/lang/String; = "lgims_com_media_audio_codec_vt"

.field private static final LGIMS_MEDIA_CODEC_VIDEO_VT:Ljava/lang/String; = "lgims_com_media_video_codec_vt"

.field private static final LGIMS_MEDIA_VIDEO:Ljava/lang/String; = "lgims_com_media_video"

.field public static final MAX_CODEC_NUM:I = 0x6

.field private static final TAG:Ljava/lang/String; = "IMSMediaSettingsCarrier"


# instance fields
.field private imsDB:Landroid/database/sqlite/SQLiteDatabase;

.field private mAdministrativeConfigChanged:Z

.field private mSettingsUpdatedFlags:I

.field private psvtChangeListstener:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierPSVTChangeListener;

.field private rootScreenKey:Ljava/lang/String;

.field private volteChangeListener:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;


# direct methods
.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x0

    #@2
    .line 35
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    #@5
    .line 47
    iput-object v0, p0, Lcom/lge/ims/setting/MediaSettingsCarrier;->volteChangeListener:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;

    #@7
    .line 48
    iput-object v0, p0, Lcom/lge/ims/setting/MediaSettingsCarrier;->psvtChangeListstener:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierPSVTChangeListener;

    #@9
    .line 50
    iput-object v0, p0, Lcom/lge/ims/setting/MediaSettingsCarrier;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@b
    .line 51
    iput-boolean v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier;->mAdministrativeConfigChanged:Z

    #@d
    .line 52
    iput-object v0, p0, Lcom/lge/ims/setting/MediaSettingsCarrier;->rootScreenKey:Ljava/lang/String;

    #@f
    .line 55
    iput v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier;->mSettingsUpdatedFlags:I

    #@11
    .line 934
    return-void
.end method

.method static synthetic access$000(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/Preference;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getPreference(Ljava/lang/String;)Landroid/preference/Preference;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$100(Lcom/lge/ims/setting/MediaSettingsCarrier;Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/lge/ims/setting/MediaSettingsCarrier;->setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getSettingsTable(Ljava/lang/String;)Landroid/database/Cursor;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$300(Lcom/lge/ims/setting/MediaSettingsCarrier;Ljava/lang/String;)Landroid/preference/ListPreference;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$400(Lcom/lge/ims/setting/MediaSettingsCarrier;[Ljava/lang/String;Ljava/lang/String;)I
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$500(Lcom/lge/ims/setting/MediaSettingsCarrier;Landroid/database/Cursor;ILandroid/preference/ListPreference;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Lcom/lge/ims/setting/MediaSettingsCarrier;->setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V

    #@3
    return-void
.end method

.method static synthetic access$676(Lcom/lge/ims/setting/MediaSettingsCarrier;I)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 35
    iget v0, p0, Lcom/lge/ims/setting/MediaSettingsCarrier;->mSettingsUpdatedFlags:I

    #@2
    or-int/2addr v0, p1

    #@3
    iput v0, p0, Lcom/lge/ims/setting/MediaSettingsCarrier;->mSettingsUpdatedFlags:I

    #@5
    return v0
.end method

.method private displayColumns([Ljava/lang/String;)V
    .registers 6
    .parameter "columns"

    #@0
    .prologue
    .line 1168
    invoke-static {}, Lcom/lge/ims/Configuration;->isDebugOn()Z

    #@3
    move-result v1

    #@4
    if-nez v1, :cond_7

    #@6
    .line 1184
    :cond_6
    return-void

    #@7
    .line 1172
    :cond_7
    if-eqz p1, :cond_6

    #@9
    .line 1176
    const/4 v0, 0x0

    #@a
    .local v0, i:I
    :goto_a
    array-length v1, p1

    #@b
    if-ge v0, v1, :cond_6

    #@d
    .line 1177
    aget-object v1, p1, v0

    #@f
    if-nez v1, :cond_32

    #@11
    .line 1178
    const-string v1, "IMSMediaSettingsCarrier"

    #@13
    new-instance v2, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v3, "column at index ("

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    const-string v3, ") is null"

    #@24
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v2

    #@2c
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@2f
    .line 1176
    :goto_2f
    add-int/lit8 v0, v0, 0x1

    #@31
    goto :goto_a

    #@32
    .line 1182
    :cond_32
    const-string v1, "IMSMediaSettingsCarrier"

    #@34
    new-instance v2, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v3, "Column at index ("

    #@3b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@42
    move-result-object v2

    #@43
    const-string v3, ")"

    #@45
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v2

    #@49
    aget-object v3, p1, v0

    #@4b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v2

    #@4f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v2

    #@53
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    #@56
    goto :goto_2f
.end method

.method private getCheckBoxPreference(Ljava/lang/String;)Landroid/preference/CheckBoxPreference;
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 1223
    invoke-virtual {p0}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    #@3
    move-result-object v0

    #@4
    .line 1225
    .local v0, root:Landroid/preference/PreferenceScreen;
    if-eqz v0, :cond_d

    #@6
    .line 1226
    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    #@9
    move-result-object v1

    #@a
    check-cast v1, Landroid/preference/CheckBoxPreference;

    #@c
    .line 1229
    :goto_c
    return-object v1

    #@d
    :cond_d
    const/4 v1, 0x0

    #@e
    goto :goto_c
.end method

.method private getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I
    .registers 5
    .parameter "columns"
    .parameter "column"

    #@0
    .prologue
    .line 1213
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    array-length v1, p1

    #@2
    if-ge v0, v1, :cond_10

    #@4
    .line 1214
    aget-object v1, p1, v0

    #@6
    invoke-virtual {p2, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@9
    move-result v1

    #@a
    if-nez v1, :cond_d

    #@c
    .line 1219
    .end local v0           #i:I
    :goto_c
    return v0

    #@d
    .line 1213
    .restart local v0       #i:I
    :cond_d
    add-int/lit8 v0, v0, 0x1

    #@f
    goto :goto_1

    #@10
    .line 1219
    :cond_10
    const/4 v0, -0x1

    #@11
    goto :goto_c
.end method

.method private getEditTextPreference(Ljava/lang/String;)Landroid/preference/EditTextPreference;
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 1243
    invoke-virtual {p0}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    #@3
    move-result-object v0

    #@4
    .line 1245
    .local v0, root:Landroid/preference/PreferenceScreen;
    if-eqz v0, :cond_d

    #@6
    .line 1246
    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    #@9
    move-result-object v1

    #@a
    check-cast v1, Landroid/preference/EditTextPreference;

    #@c
    .line 1249
    :goto_c
    return-object v1

    #@d
    :cond_d
    const/4 v1, 0x0

    #@e
    goto :goto_c
.end method

.method private getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 1233
    invoke-virtual {p0}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    #@3
    move-result-object v0

    #@4
    .line 1235
    .local v0, root:Landroid/preference/PreferenceScreen;
    if-eqz v0, :cond_d

    #@6
    .line 1236
    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    #@9
    move-result-object v1

    #@a
    check-cast v1, Landroid/preference/ListPreference;

    #@c
    .line 1239
    :goto_c
    return-object v1

    #@d
    :cond_d
    const/4 v1, 0x0

    #@e
    goto :goto_c
.end method

.method private getPreference(Ljava/lang/String;)Landroid/preference/Preference;
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 1253
    invoke-virtual {p0}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    #@3
    move-result-object v0

    #@4
    .line 1255
    .local v0, root:Landroid/preference/PreferenceScreen;
    if-eqz v0, :cond_b

    #@6
    .line 1256
    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    #@9
    move-result-object v1

    #@a
    .line 1259
    :goto_a
    return-object v1

    #@b
    :cond_b
    const/4 v1, 0x0

    #@c
    goto :goto_a
.end method

.method private getSettingsTable(Ljava/lang/String;)Landroid/database/Cursor;
    .registers 7
    .parameter "table"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1187
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 1209
    :cond_5
    :goto_5
    return-object v0

    #@6
    .line 1191
    :cond_6
    if-eqz p1, :cond_5

    #@8
    .line 1195
    const/4 v0, 0x0

    #@9
    .line 1198
    .local v0, cursor:Landroid/database/Cursor;
    :try_start_9
    iget-object v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@b
    new-instance v3, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v4, "select * from "

    #@12
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v3

    #@1e
    const/4 v4, 0x0

    #@1f
    invoke-virtual {v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_22
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_9 .. :try_end_22} :catch_24

    #@22
    move-result-object v0

    #@23
    goto :goto_5

    #@24
    .line 1199
    :catch_24
    move-exception v1

    #@25
    .line 1200
    .local v1, e:Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    #@28
    .line 1202
    if-eqz v0, :cond_2d

    #@2a
    .line 1203
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    #@2d
    .line 1206
    :cond_2d
    const/4 v0, 0x0

    #@2e
    goto :goto_5
.end method

.method private removePreference(Ljava/lang/String;)V
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 1263
    invoke-virtual {p0}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    #@3
    move-result-object v1

    #@4
    .line 1265
    .local v1, root:Landroid/preference/PreferenceScreen;
    if-eqz v1, :cond_f

    #@6
    .line 1266
    invoke-virtual {v1, p1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    #@9
    move-result-object v0

    #@a
    .line 1268
    .local v0, preference:Landroid/preference/Preference;
    if-eqz v0, :cond_f

    #@c
    .line 1269
    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    #@f
    .line 1272
    .end local v0           #preference:Landroid/preference/Preference;
    :cond_f
    return-void
.end method

.method private setEditTextAsNumberOnly(Landroid/preference/EditTextPreference;)V
    .registers 4
    .parameter "edit"

    #@0
    .prologue
    .line 1275
    if-nez p1, :cond_3

    #@2
    .line 1280
    :goto_2
    return-void

    #@3
    .line 1279
    :cond_3
    invoke-virtual {p1}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    #@6
    move-result-object v0

    #@7
    const/4 v1, 0x2

    #@8
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    #@b
    goto :goto_2
.end method

.method private setListener(Landroid/preference/Preference;Landroid/preference/Preference$OnPreferenceChangeListener;)V
    .registers 3
    .parameter "preference"
    .parameter "listener"

    #@0
    .prologue
    .line 1283
    if-eqz p1, :cond_5

    #@2
    .line 1284
    invoke-virtual {p1, p2}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@5
    .line 1286
    :cond_5
    return-void
.end method

.method private setValue(Landroid/database/Cursor;ILandroid/preference/CheckBoxPreference;)V
    .registers 7
    .parameter "cursor"
    .parameter "index"
    .parameter "checkbox"

    #@0
    .prologue
    .line 1289
    const/4 v1, -0x1

    #@1
    if-ne p2, v1, :cond_b

    #@3
    .line 1290
    const-string v1, "IMSMediaSettingsCarrier"

    #@5
    const-string v2, "index is (-1)"

    #@7
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 1317
    :cond_a
    :goto_a
    return-void

    #@b
    .line 1294
    :cond_b
    if-nez p1, :cond_15

    #@d
    .line 1295
    const-string v1, "IMSMediaSettingsCarrier"

    #@f
    const-string v2, "Cursor is null"

    #@11
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@14
    goto :goto_a

    #@15
    .line 1299
    :cond_15
    if-nez p3, :cond_1f

    #@17
    .line 1300
    const-string v1, "IMSMediaSettingsCarrier"

    #@19
    const-string v2, "CheckBoxPreference is null"

    #@1b
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@1e
    goto :goto_a

    #@1f
    .line 1304
    :cond_1f
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    .line 1306
    .local v0, value:Ljava/lang/String;
    if-eqz v0, :cond_a

    #@25
    .line 1310
    const-string v1, "true"

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@2a
    move-result v1

    #@2b
    if-nez v1, :cond_37

    #@2d
    .line 1311
    const/4 v1, 0x1

    #@2e
    invoke-virtual {p3, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    #@31
    .line 1312
    const-string v1, "true"

    #@33
    invoke-virtual {p3, v1}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    #@36
    goto :goto_a

    #@37
    .line 1314
    :cond_37
    const/4 v1, 0x0

    #@38
    invoke-virtual {p3, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    #@3b
    .line 1315
    const-string v1, "false"

    #@3d
    invoke-virtual {p3, v1}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    #@40
    goto :goto_a
.end method

.method private setValue(Landroid/database/Cursor;ILandroid/preference/EditTextPreference;)V
    .registers 7
    .parameter "cursor"
    .parameter "index"
    .parameter "edit"

    #@0
    .prologue
    .line 1320
    const/4 v1, -0x1

    #@1
    if-ne p2, v1, :cond_4

    #@3
    .line 1342
    :cond_3
    :goto_3
    return-void

    #@4
    .line 1324
    :cond_4
    if-nez p1, :cond_e

    #@6
    .line 1325
    const-string v1, "IMSMediaSettingsCarrier"

    #@8
    const-string v2, "Cursor is null"

    #@a
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    goto :goto_3

    #@e
    .line 1329
    :cond_e
    if-nez p3, :cond_18

    #@10
    .line 1330
    const-string v1, "IMSMediaSettingsCarrier"

    #@12
    const-string v2, "EditTextPreference is null"

    #@14
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@17
    goto :goto_3

    #@18
    .line 1334
    :cond_18
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    .line 1336
    .local v0, value:Ljava/lang/String;
    if-eqz v0, :cond_3

    #@1e
    .line 1340
    invoke-virtual {p3, v0}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    #@21
    .line 1341
    invoke-virtual {p3, v0}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    #@24
    goto :goto_3
.end method

.method private setValue(Landroid/database/Cursor;ILandroid/preference/ListPreference;)V
    .registers 10
    .parameter "cursor"
    .parameter "index"
    .parameter "list"

    #@0
    .prologue
    .line 1345
    const/4 v3, -0x1

    #@1
    if-ne p2, v3, :cond_4

    #@3
    .line 1376
    :cond_3
    :goto_3
    return-void

    #@4
    .line 1349
    :cond_4
    if-nez p1, :cond_e

    #@6
    .line 1350
    const-string v3, "IMSMediaSettingsCarrier"

    #@8
    const-string v4, "Cursor is null"

    #@a
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    goto :goto_3

    #@e
    .line 1354
    :cond_e
    if-nez p3, :cond_18

    #@10
    .line 1355
    const-string v3, "IMSMediaSettingsCarrier"

    #@12
    const-string v4, "ListPreference is null"

    #@14
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@17
    goto :goto_3

    #@18
    .line 1359
    :cond_18
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    .line 1361
    .local v1, value:Ljava/lang/String;
    if-eqz v1, :cond_3

    #@1e
    .line 1365
    invoke-virtual {p3, v1}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    #@21
    move-result v2

    #@22
    .line 1367
    .local v2, valueIndex:I
    if-gez v2, :cond_4b

    #@24
    .line 1368
    const-string v3, "IMSMediaSettingsCarrier"

    #@26
    new-instance v4, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v5, "setValue :: list - key="

    #@2d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v4

    #@31
    invoke-virtual {p3}, Landroid/preference/ListPreference;->getKey()Ljava/lang/String;

    #@34
    move-result-object v5

    #@35
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v4

    #@39
    const-string v5, ", value="

    #@3b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v4

    #@3f
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v4

    #@43
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v4

    #@47
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@4a
    goto :goto_3

    #@4b
    .line 1372
    :cond_4b
    invoke-virtual {p3, v2}, Landroid/preference/ListPreference;->setValueIndex(I)V

    #@4e
    .line 1374
    invoke-virtual {p3}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    #@51
    move-result-object v0

    #@52
    .line 1375
    .local v0, entrys:[Ljava/lang/CharSequence;
    aget-object v3, v0, v2

    #@54
    invoke-virtual {p3, v3}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    #@57
    goto :goto_3
.end method


# virtual methods
.method public getNameOfListPreference(Landroid/preference/ListPreference;Ljava/lang/Object;)Ljava/lang/String;
    .registers 6
    .parameter "listPref"
    .parameter "newValue"

    #@0
    .prologue
    .line 1131
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@3
    move-result-object v2

    #@4
    invoke-virtual {p1, v2}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    #@7
    move-result v1

    #@8
    .line 1133
    .local v1, index:I
    const/4 v2, -0x1

    #@9
    if-ne v1, v2, :cond_c

    #@b
    .line 1134
    const/4 v1, 0x0

    #@c
    .line 1137
    :cond_c
    invoke-virtual {p1}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    #@f
    move-result-object v0

    #@10
    .line 1139
    .local v0, entrys:[Ljava/lang/CharSequence;
    aget-object v2, v0, v1

    #@12
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    return-object v2
.end method

.method public load(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 10
    .parameter "tableName"
    .parameter "key"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1073
    invoke-direct {p0, p1}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getSettingsTable(Ljava/lang/String;)Landroid/database/Cursor;

    #@4
    move-result-object v1

    #@5
    .line 1075
    .local v1, cursor:Landroid/database/Cursor;
    if-nez v1, :cond_26

    #@7
    .line 1076
    const-string v4, "IMSMediaSettingsCarrier"

    #@9
    new-instance v5, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v6, "Cursor :: "

    #@10
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v5

    #@14
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v5

    #@18
    const-string v6, " is null"

    #@1a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v5

    #@1e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v5

    #@22
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@25
    .line 1096
    :goto_25
    return-object v3

    #@26
    .line 1080
    :cond_26
    invoke-interface {v1}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    #@29
    move-result-object v0

    #@2a
    .line 1083
    .local v0, columns:[Ljava/lang/String;
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    #@2d
    .line 1086
    invoke-direct {p0, v0, p2}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getColumnIndex([Ljava/lang/String;Ljava/lang/String;)I

    #@30
    move-result v2

    #@31
    .line 1088
    .local v2, index:I
    const/4 v4, -0x1

    #@32
    if-ne v2, v4, :cond_38

    #@34
    .line 1089
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    #@37
    goto :goto_25

    #@38
    .line 1093
    :cond_38
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@3b
    move-result-object v3

    #@3c
    .line 1095
    .local v3, value:Ljava/lang/String;
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    #@3f
    goto :goto_25
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 8
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 60
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    #@3
    .line 62
    const v3, 0x7f040003

    #@6
    invoke-virtual {p0, v3}, Lcom/lge/ims/setting/MediaSettingsCarrier;->addPreferencesFromResource(I)V

    #@9
    .line 65
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@b
    if-nez v3, :cond_17

    #@d
    .line 67
    :try_start_d
    const-string v3, "/data/data/com.lge.ims/databases/lgims.db"

    #@f
    const/4 v4, 0x0

    #@10
    const/4 v5, 0x0

    #@11
    invoke-static {v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->openDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)Landroid/database/sqlite/SQLiteDatabase;

    #@14
    move-result-object v3

    #@15
    iput-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier;->imsDB:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_17
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_d .. :try_end_17} :catch_30

    #@17
    .line 74
    :cond_17
    new-instance v0, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;

    #@19
    invoke-direct {v0, p0}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;-><init>(Lcom/lge/ims/setting/MediaSettingsCarrier;)V

    #@1c
    .line 75
    .local v0, audioVoLTECommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;
    new-instance v3, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;

    #@1e
    invoke-direct {v3, p0, v0}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;-><init>(Lcom/lge/ims/setting/MediaSettingsCarrier;Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;)V

    #@21
    iput-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier;->volteChangeListener:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVoLTEChangeListener;

    #@23
    .line 77
    new-instance v2, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;

    #@25
    invoke-direct {v2, p0}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;-><init>(Lcom/lge/ims/setting/MediaSettingsCarrier;)V

    #@28
    .line 78
    .local v2, videoPSVTCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;
    new-instance v3, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierPSVTChangeListener;

    #@2a
    invoke-direct {v3, p0, v2}, Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierPSVTChangeListener;-><init>(Lcom/lge/ims/setting/MediaSettingsCarrier;Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;)V

    #@2d
    iput-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier;->psvtChangeListstener:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierPSVTChangeListener;

    #@2f
    .line 80
    .end local v0           #audioVoLTECommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierAudioCommand;
    .end local v2           #videoPSVTCommand:Lcom/lge/ims/setting/MediaSettingsCarrier$CarrierVideoCommand;
    :goto_2f
    return-void

    #@30
    .line 68
    :catch_30
    move-exception v1

    #@31
    .line 69
    .local v1, e:Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    #@34
    goto :goto_2f
.end method

.method public onDestroy()V
    .registers 2

    #@0
    .prologue
    .line 84
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    #@3
    .line 86
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettingsCarrier;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@5
    if-eqz v0, :cond_f

    #@7
    .line 87
    iget-object v0, p0, Lcom/lge/ims/setting/MediaSettingsCarrier;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@9
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    #@c
    .line 88
    const/4 v0, 0x0

    #@d
    iput-object v0, p0, Lcom/lge/ims/setting/MediaSettingsCarrier;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@f
    .line 90
    :cond_f
    return-void
.end method

.method public onPause()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 94
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPause()V

    #@4
    .line 96
    iget-boolean v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier;->mAdministrativeConfigChanged:Z

    #@6
    if-eqz v1, :cond_11

    #@8
    .line 97
    invoke-virtual {p0}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getBaseContext()Landroid/content/Context;

    #@b
    move-result-object v1

    #@c
    invoke-static {v1}, Lcom/lge/ims/Configuration;->init(Landroid/content/Context;)V

    #@f
    .line 98
    iput-boolean v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier;->mAdministrativeConfigChanged:Z

    #@11
    .line 101
    :cond_11
    iget v1, p0, Lcom/lge/ims/setting/MediaSettingsCarrier;->mSettingsUpdatedFlags:I

    #@13
    if-eqz v1, :cond_38

    #@15
    .line 102
    const-string v1, "IMSMediaSettingsCarrier"

    #@17
    const-string v2, "Send an intent to update the configuration"

    #@19
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 104
    new-instance v0, Landroid/content/Intent;

    #@1e
    const-string v1, "com.lge.ims.action.CONFIG_UPDATE"

    #@20
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@23
    .line 105
    .local v0, intent:Landroid/content/Intent;
    const/16 v1, 0x20

    #@25
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@28
    .line 106
    const-string v1, "flags"

    #@2a
    iget v2, p0, Lcom/lge/ims/setting/MediaSettingsCarrier;->mSettingsUpdatedFlags:I

    #@2c
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@2f
    .line 107
    invoke-virtual {p0}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getBaseContext()Landroid/content/Context;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@36
    .line 109
    iput v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier;->mSettingsUpdatedFlags:I

    #@38
    .line 111
    .end local v0           #intent:Landroid/content/Intent;
    :cond_38
    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .registers 4
    .parameter "preferenceScreen"
    .parameter "preference"

    #@0
    .prologue
    .line 117
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public setValue(Landroid/preference/Preference;Ljava/lang/String;)Z
    .registers 11
    .parameter "pref"
    .parameter "value"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 1028
    instance-of v7, p1, Landroid/preference/EditTextPreference;

    #@4
    if-eqz v7, :cond_d

    #@6
    .line 1029
    check-cast p1, Landroid/preference/EditTextPreference;

    #@8
    .end local p1
    invoke-virtual {p1, p2}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    #@b
    :cond_b
    :goto_b
    move v5, v6

    #@c
    .line 1067
    :cond_c
    return v5

    #@d
    .line 1031
    .restart local p1
    :cond_d
    instance-of v7, p1, Landroid/preference/ListPreference;

    #@f
    if-eqz v7, :cond_28

    #@11
    move-object v5, p1

    #@12
    .line 1033
    check-cast v5, Landroid/preference/ListPreference;

    #@14
    invoke-virtual {v5, p2}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    #@17
    move-result v1

    #@18
    .line 1035
    .local v1, listIndex:I
    const/4 v5, -0x1

    #@19
    if-ne v1, v5, :cond_1c

    #@1b
    .line 1036
    const/4 v1, 0x0

    #@1c
    :cond_1c
    move-object v5, p1

    #@1d
    .line 1039
    check-cast v5, Landroid/preference/ListPreference;

    #@1f
    invoke-virtual {v5, v1}, Landroid/preference/ListPreference;->setValueIndex(I)V

    #@22
    .line 1040
    check-cast p1, Landroid/preference/ListPreference;

    #@24
    .end local p1
    invoke-virtual {p1}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    #@27
    goto :goto_b

    #@28
    .line 1042
    .end local v1           #listIndex:I
    .restart local p1
    :cond_28
    instance-of v7, p1, Landroid/preference/CheckBoxPreference;

    #@2a
    if-eqz v7, :cond_40

    #@2c
    .line 1044
    const-string v7, "true"

    #@2e
    invoke-virtual {p2, v7}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@31
    move-result v7

    #@32
    if-nez v7, :cond_3a

    #@34
    .line 1045
    check-cast p1, Landroid/preference/CheckBoxPreference;

    #@36
    .end local p1
    invoke-virtual {p1, v6}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    #@39
    goto :goto_b

    #@3a
    .line 1047
    .restart local p1
    :cond_3a
    check-cast p1, Landroid/preference/CheckBoxPreference;

    #@3c
    .end local p1
    invoke-virtual {p1, v5}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    #@3f
    goto :goto_b

    #@40
    .line 1050
    .restart local p1
    :cond_40
    instance-of v7, p1, Landroid/preference/MultiSelectListPreference;

    #@42
    if-eqz v7, :cond_b

    #@44
    .line 1052
    const-string v7, ","

    #@46
    invoke-virtual {p2, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@49
    move-result-object v3

    #@4a
    .line 1053
    .local v3, tokens:[Ljava/lang/String;
    new-instance v4, Ljava/util/HashSet;

    #@4c
    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    #@4f
    .line 1055
    .local v4, values:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    array-length v7, v3

    #@50
    if-eqz v7, :cond_c

    #@52
    .line 1059
    const/4 v0, 0x0

    #@53
    .local v0, i:I
    :goto_53
    array-length v5, v3

    #@54
    if-ge v0, v5, :cond_62

    #@56
    .line 1060
    aget-object v5, v3, v0

    #@58
    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@5b
    move-result-object v2

    #@5c
    .line 1061
    .local v2, newString:Ljava/lang/String;
    invoke-virtual {v4, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@5f
    .line 1059
    add-int/lit8 v0, v0, 0x1

    #@61
    goto :goto_53

    #@62
    .line 1064
    .end local v2           #newString:Ljava/lang/String;
    :cond_62
    check-cast p1, Landroid/preference/MultiSelectListPreference;

    #@64
    .end local p1
    invoke-virtual {p1, v4}, Landroid/preference/MultiSelectListPreference;->setValues(Ljava/util/Set;)V

    #@67
    goto :goto_b
.end method

.method public summaryChange(Landroid/preference/Preference;Ljava/lang/Object;)V
    .registers 6
    .parameter "preference"
    .parameter "newValue"

    #@0
    .prologue
    .line 1145
    instance-of v2, p1, Landroid/preference/ListPreference;

    #@2
    if-eqz v2, :cond_14

    #@4
    .line 1147
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    #@7
    move-result-object v2

    #@8
    invoke-direct {p0, v2}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getListPreference(Ljava/lang/String;)Landroid/preference/ListPreference;

    #@b
    move-result-object v0

    #@c
    .line 1148
    .local v0, listPref:Landroid/preference/ListPreference;
    invoke-virtual {p0, v0, p2}, Lcom/lge/ims/setting/MediaSettingsCarrier;->getNameOfListPreference(Landroid/preference/ListPreference;Ljava/lang/Object;)Ljava/lang/String;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {p1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    #@13
    .line 1165
    .end local v0           #listPref:Landroid/preference/ListPreference;
    .end local p1
    :goto_13
    return-void

    #@14
    .line 1150
    .restart local p1
    :cond_14
    instance-of v2, p1, Landroid/preference/EditTextPreference;

    #@16
    if-eqz v2, :cond_20

    #@18
    .line 1152
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {p1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    #@1f
    goto :goto_13

    #@20
    .line 1154
    :cond_20
    instance-of v2, p1, Landroid/preference/CheckBoxPreference;

    #@22
    if-eqz v2, :cond_40

    #@24
    .line 1155
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    .line 1156
    .local v1, value:Ljava/lang/String;
    const-string v2, "true"

    #@2a
    invoke-virtual {v1, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@2d
    move-result v2

    #@2e
    if-nez v2, :cond_38

    #@30
    .line 1157
    check-cast p1, Landroid/preference/CheckBoxPreference;

    #@32
    .end local p1
    const-string v2, "true"

    #@34
    invoke-virtual {p1, v2}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    #@37
    goto :goto_13

    #@38
    .line 1159
    .restart local p1
    :cond_38
    check-cast p1, Landroid/preference/CheckBoxPreference;

    #@3a
    .end local p1
    const-string v2, "false"

    #@3c
    invoke-virtual {p1, v2}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    #@3f
    goto :goto_13

    #@40
    .line 1163
    .end local v1           #value:Ljava/lang/String;
    .restart local p1
    :cond_40
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@43
    move-result-object v2

    #@44
    invoke-virtual {p1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    #@47
    goto :goto_13
.end method

.method public updateDB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 12
    .parameter "tableName"
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 996
    const-string v5, "IMSMediaSettingsCarrier"

    #@4
    new-instance v6, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v7, "updateDB :: key: "

    #@b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v6

    #@f
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v6

    #@13
    const-string v7, " -> value:"

    #@15
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v6

    #@19
    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v6

    #@1d
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v6

    #@21
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@24
    .line 998
    iget-object v5, p0, Lcom/lge/ims/setting/MediaSettingsCarrier;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@26
    if-nez v5, :cond_47

    #@28
    .line 999
    const-string v4, "IMSMediaSettingsCarrier"

    #@2a
    new-instance v5, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v6, "DB table ("

    #@31
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v5

    #@35
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v5

    #@39
    const-string v6, ") is null"

    #@3b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v5

    #@3f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v5

    #@43
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@46
    .line 1023
    :goto_46
    return v3

    #@47
    .line 1003
    :cond_47
    const/4 v0, 0x0

    #@48
    .line 1004
    .local v0, affectedRows:I
    new-instance v2, Landroid/content/ContentValues;

    #@4a
    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    #@4d
    .line 1005
    .local v2, values:Landroid/content/ContentValues;
    invoke-virtual {v2, p2, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@50
    .line 1006
    iget-object v5, p0, Lcom/lge/ims/setting/MediaSettingsCarrier;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@52
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    #@55
    .line 1009
    :try_start_55
    iget-object v5, p0, Lcom/lge/ims/setting/MediaSettingsCarrier;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@57
    const-string v6, "id = \'1\'"

    #@59
    const/4 v7, 0x0

    #@5a
    invoke-virtual {v5, p1, v2, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@5d
    move-result v0

    #@5e
    .line 1010
    iget-object v5, p0, Lcom/lge/ims/setting/MediaSettingsCarrier;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@60
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_63
    .catchall {:try_start_55 .. :try_end_63} :catchall_94
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_55 .. :try_end_63} :catch_8a

    #@63
    .line 1014
    iget-object v5, p0, Lcom/lge/ims/setting/MediaSettingsCarrier;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@65
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@68
    .line 1017
    :goto_68
    const-string v5, "IMSMediaSettingsCarrier"

    #@6a
    new-instance v6, Ljava/lang/StringBuilder;

    #@6c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@6f
    const-string v7, "IMSMediaSettingsCarrier :: updated row count: "

    #@71
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v6

    #@75
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@78
    move-result-object v6

    #@79
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v6

    #@7d
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@80
    .line 1019
    if-eq v0, v4, :cond_9b

    #@82
    .line 1020
    const-string v4, "IMSMediaSettingsCarrier"

    #@84
    const-string v5, "Update fails"

    #@86
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@89
    goto :goto_46

    #@8a
    .line 1011
    :catch_8a
    move-exception v1

    #@8b
    .line 1012
    .local v1, e:Landroid/database/sqlite/SQLiteException;
    :try_start_8b
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_8e
    .catchall {:try_start_8b .. :try_end_8e} :catchall_94

    #@8e
    .line 1014
    iget-object v5, p0, Lcom/lge/ims/setting/MediaSettingsCarrier;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@90
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@93
    goto :goto_68

    #@94
    .end local v1           #e:Landroid/database/sqlite/SQLiteException;
    :catchall_94
    move-exception v3

    #@95
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@97
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@9a
    throw v3

    #@9b
    :cond_9b
    move v3, v4

    #@9c
    .line 1023
    goto :goto_46
.end method

.method public updateValue(Ljava/lang/String;Landroid/content/ContentValues;)Z
    .registers 10
    .parameter "tableName"
    .parameter "values"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 1101
    const/4 v0, 0x0

    #@3
    .line 1103
    .local v0, affectedRows:I
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@5
    if-nez v4, :cond_26

    #@7
    .line 1104
    const-string v3, "IMSMediaSettingsCarrier"

    #@9
    new-instance v4, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v5, "DB table ("

    #@10
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v4

    #@14
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v4

    #@18
    const-string v5, ") is null"

    #@1a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v4

    #@1e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v4

    #@22
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@25
    .line 1126
    :goto_25
    return v2

    #@26
    .line 1108
    :cond_26
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@28
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    #@2b
    .line 1111
    :try_start_2b
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@2d
    const-string v5, "id = \'1\'"

    #@2f
    const/4 v6, 0x0

    #@30
    invoke-virtual {v4, p1, p2, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@33
    move-result v0

    #@34
    .line 1112
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@36
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_39
    .catchall {:try_start_2b .. :try_end_39} :catchall_6a
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2b .. :try_end_39} :catch_60

    #@39
    .line 1116
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@3b
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@3e
    .line 1119
    :goto_3e
    const-string v4, "IMSMediaSettingsCarrier"

    #@40
    new-instance v5, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v6, "IMSMediaSettingsCarrier :: updated row count: "

    #@47
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v5

    #@4b
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v5

    #@4f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v5

    #@53
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@56
    .line 1121
    if-ge v0, v3, :cond_71

    #@58
    .line 1122
    const-string v3, "IMSMediaSettingsCarrier"

    #@5a
    const-string v4, "Update fails"

    #@5c
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@5f
    goto :goto_25

    #@60
    .line 1113
    :catch_60
    move-exception v1

    #@61
    .line 1114
    .local v1, e:Landroid/database/sqlite/SQLiteException;
    :try_start_61
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_64
    .catchall {:try_start_61 .. :try_end_64} :catchall_6a

    #@64
    .line 1116
    iget-object v4, p0, Lcom/lge/ims/setting/MediaSettingsCarrier;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@66
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@69
    goto :goto_3e

    #@6a
    .end local v1           #e:Landroid/database/sqlite/SQLiteException;
    :catchall_6a
    move-exception v2

    #@6b
    iget-object v3, p0, Lcom/lge/ims/setting/MediaSettingsCarrier;->imsDB:Landroid/database/sqlite/SQLiteDatabase;

    #@6d
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@70
    throw v2

    #@71
    :cond_71
    move v2, v3

    #@72
    .line 1126
    goto :goto_25
.end method
