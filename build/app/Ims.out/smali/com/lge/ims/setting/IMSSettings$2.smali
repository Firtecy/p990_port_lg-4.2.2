.class Lcom/lge/ims/setting/IMSSettings$2;
.super Ljava/lang/Object;
.source "IMSSettings.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lge/ims/setting/IMSSettings;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/setting/IMSSettings;


# direct methods
.method constructor <init>(Lcom/lge/ims/setting/IMSSettings;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 259
    iput-object p1, p0, Lcom/lge/ims/setting/IMSSettings$2;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 6
    .parameter "dialog"
    .parameter "whichButton"

    #@0
    .prologue
    .line 261
    iget-object v1, p0, Lcom/lge/ims/setting/IMSSettings$2;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@2
    invoke-static {v1}, Lcom/lge/ims/setting/IMSSettings;->access$400(Lcom/lge/ims/setting/IMSSettings;)V

    #@5
    .line 262
    const-string v1, "LGIMSSettings"

    #@7
    const-string v2, "onPreferenceTreeClick :: RebootThread"

    #@9
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 263
    new-instance v0, Lcom/lge/ims/setting/IMSSettings$RebootThread;

    #@e
    iget-object v1, p0, Lcom/lge/ims/setting/IMSSettings$2;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@10
    iget-object v2, p0, Lcom/lge/ims/setting/IMSSettings$2;->this$0:Lcom/lge/ims/setting/IMSSettings;

    #@12
    invoke-virtual {v2}, Lcom/lge/ims/setting/IMSSettings;->getBaseContext()Landroid/content/Context;

    #@15
    move-result-object v2

    #@16
    invoke-direct {v0, v1, v2}, Lcom/lge/ims/setting/IMSSettings$RebootThread;-><init>(Lcom/lge/ims/setting/IMSSettings;Landroid/content/Context;)V

    #@19
    .line 264
    .local v0, rebootThread:Lcom/lge/ims/setting/IMSSettings$RebootThread;
    invoke-virtual {v0}, Lcom/lge/ims/setting/IMSSettings$RebootThread;->start()V

    #@1c
    .line 265
    return-void
.end method
