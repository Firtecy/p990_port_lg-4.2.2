.class Lcom/lge/ims/Configuration$DataConnection;
.super Ljava/lang/Object;
.source "Configuration.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/Configuration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DataConnection"
.end annotation


# instance fields
.field private mAccessPolicy:I

.field private mIPVersion:I

.field final synthetic this$0:Lcom/lge/ims/Configuration;


# direct methods
.method public constructor <init>(Lcom/lge/ims/Configuration;II)V
    .registers 5
    .parameter
    .parameter "accessPolicy"
    .parameter "ipVersion"

    #@0
    .prologue
    .line 55
    iput-object p1, p0, Lcom/lge/ims/Configuration$DataConnection;->this$0:Lcom/lge/ims/Configuration;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 52
    const/4 v0, 0x0

    #@6
    iput v0, p0, Lcom/lge/ims/Configuration$DataConnection;->mAccessPolicy:I

    #@8
    .line 53
    const/16 v0, 0x40

    #@a
    iput v0, p0, Lcom/lge/ims/Configuration$DataConnection;->mIPVersion:I

    #@c
    .line 56
    iput p2, p0, Lcom/lge/ims/Configuration$DataConnection;->mAccessPolicy:I

    #@e
    .line 57
    iput p3, p0, Lcom/lge/ims/Configuration$DataConnection;->mIPVersion:I

    #@10
    .line 58
    return-void
.end method


# virtual methods
.method public getAccessPolicy()I
    .registers 2

    #@0
    .prologue
    .line 61
    iget v0, p0, Lcom/lge/ims/Configuration$DataConnection;->mAccessPolicy:I

    #@2
    return v0
.end method

.method public getIPVersion()I
    .registers 2

    #@0
    .prologue
    .line 65
    iget v0, p0, Lcom/lge/ims/Configuration$DataConnection;->mIPVersion:I

    #@2
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "AccesssPolicy=0x"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Lcom/lge/ims/Configuration$DataConnection;->mAccessPolicy:I

    #@d
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    const-string v1, ", IPVersion="

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    iget v1, p0, Lcom/lge/ims/Configuration$DataConnection;->mIPVersion:I

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v0

    #@25
    return-object v0
.end method
