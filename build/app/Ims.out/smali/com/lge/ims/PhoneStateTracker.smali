.class public Lcom/lge/ims/PhoneStateTracker;
.super Ljava/lang/Object;
.source "PhoneStateTracker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/PhoneStateTracker$PhoneStateReceiver;,
        Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;
    }
.end annotation


# static fields
.field private static final POLICY_RAT_1xRTT:I = 0x1

.field private static final POLICY_RAT_2G:I = 0x10

.field private static final POLICY_RAT_3G:I = 0x4

.field private static final POLICY_RAT_4G:I = 0x8

.field private static final POLICY_RAT_EHRPD:I = 0x2

.field public static final RAT_1xRTT:I = 0x4

.field public static final RAT_2G:I = 0x5

.field public static final RAT_3G:I = 0x3

.field public static final RAT_4G:I = 0x2

.field public static final RAT_eHRPD:I = 0x1

.field private static final TAG:Ljava/lang/String; = "PhoneStateTracker"

.field private static mPhoneStateTracker:Lcom/lge/ims/PhoneStateTracker;


# instance fields
.field private mAirplaneMode:Z

.field private mAirplaneModeChangedRegistrants:Landroid/os/RegistrantList;

.field private mCallState:I

.field private mCallStateChangedRegistrants:Landroid/os/RegistrantList;

.field private mContext:Landroid/content/Context;

.field private mDataServiceState:I

.field private mDataServiceStateChangedRegistrants:Landroid/os/RegistrantList;

.field private mIccState:Ljava/lang/String;

.field private mLteStateInfo:Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

.field private mPhoneServiceState:I

.field private mPhoneStateListener:Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;

.field private mPhoneStateReceiver:Lcom/lge/ims/PhoneStateTracker$PhoneStateReceiver;

.field private mPolicy:I

.field private mRAT:I

.field private mRATChangedRegistrants:Landroid/os/RegistrantList;

.field private mRoaming:Z

.field private mRoamingStateChangedRegistrants:Landroid/os/RegistrantList;

.field private mServiceStateChangedRegistrants:Landroid/os/RegistrantList;

.field private mSimStateChangedRegistrants:Landroid/os/RegistrantList;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 42
    new-instance v0, Lcom/lge/ims/PhoneStateTracker;

    #@2
    invoke-direct {v0}, Lcom/lge/ims/PhoneStateTracker;-><init>()V

    #@5
    sput-object v0, Lcom/lge/ims/PhoneStateTracker;->mPhoneStateTracker:Lcom/lge/ims/PhoneStateTracker;

    #@7
    return-void
.end method

.method private constructor <init>()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 81
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 44
    new-instance v0, Landroid/os/RegistrantList;

    #@7
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@a
    iput-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mCallStateChangedRegistrants:Landroid/os/RegistrantList;

    #@c
    .line 47
    new-instance v0, Landroid/os/RegistrantList;

    #@e
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@11
    iput-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mDataServiceStateChangedRegistrants:Landroid/os/RegistrantList;

    #@13
    .line 48
    new-instance v0, Landroid/os/RegistrantList;

    #@15
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@18
    iput-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mRATChangedRegistrants:Landroid/os/RegistrantList;

    #@1a
    .line 49
    new-instance v0, Landroid/os/RegistrantList;

    #@1c
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@1f
    iput-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mRoamingStateChangedRegistrants:Landroid/os/RegistrantList;

    #@21
    .line 50
    new-instance v0, Landroid/os/RegistrantList;

    #@23
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@26
    iput-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mServiceStateChangedRegistrants:Landroid/os/RegistrantList;

    #@28
    .line 53
    new-instance v0, Landroid/os/RegistrantList;

    #@2a
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@2d
    iput-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mAirplaneModeChangedRegistrants:Landroid/os/RegistrantList;

    #@2f
    .line 54
    new-instance v0, Landroid/os/RegistrantList;

    #@31
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@34
    iput-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mSimStateChangedRegistrants:Landroid/os/RegistrantList;

    #@36
    .line 56
    const/4 v0, 0x0

    #@37
    iput-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mContext:Landroid/content/Context;

    #@39
    .line 57
    new-instance v0, Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;

    #@3b
    invoke-direct {v0, p0}, Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;-><init>(Lcom/lge/ims/PhoneStateTracker;)V

    #@3e
    iput-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mPhoneStateListener:Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;

    #@40
    .line 58
    new-instance v0, Lcom/lge/ims/PhoneStateTracker$PhoneStateReceiver;

    #@42
    invoke-direct {v0, p0}, Lcom/lge/ims/PhoneStateTracker$PhoneStateReceiver;-><init>(Lcom/lge/ims/PhoneStateTracker;)V

    #@45
    iput-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mPhoneStateReceiver:Lcom/lge/ims/PhoneStateTracker$PhoneStateReceiver;

    #@47
    .line 60
    iput v1, p0, Lcom/lge/ims/PhoneStateTracker;->mPolicy:I

    #@49
    .line 61
    iput v1, p0, Lcom/lge/ims/PhoneStateTracker;->mCallState:I

    #@4b
    .line 64
    iput v1, p0, Lcom/lge/ims/PhoneStateTracker;->mRAT:I

    #@4d
    .line 65
    iput v2, p0, Lcom/lge/ims/PhoneStateTracker;->mPhoneServiceState:I

    #@4f
    .line 66
    iput v2, p0, Lcom/lge/ims/PhoneStateTracker;->mDataServiceState:I

    #@51
    .line 67
    iput-boolean v1, p0, Lcom/lge/ims/PhoneStateTracker;->mRoaming:Z

    #@53
    .line 75
    iput-boolean v1, p0, Lcom/lge/ims/PhoneStateTracker;->mAirplaneMode:Z

    #@55
    .line 76
    const-string v0, "UNKNOWN"

    #@57
    iput-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mIccState:Ljava/lang/String;

    #@59
    .line 77
    const/4 v0, 0x3

    #@5a
    invoke-static {v0}, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;->fromInt(I)Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@5d
    move-result-object v0

    #@5e
    iput-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mLteStateInfo:Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@60
    .line 82
    const-string v0, "PhoneStateTracker"

    #@62
    const-string v1, "PhoneStateTracker is created"

    #@64
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@67
    .line 83
    return-void
.end method

.method static synthetic access$000(Lcom/lge/ims/PhoneStateTracker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 24
    iget v0, p0, Lcom/lge/ims/PhoneStateTracker;->mCallState:I

    #@2
    return v0
.end method

.method static synthetic access$002(Lcom/lge/ims/PhoneStateTracker;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 24
    iput p1, p0, Lcom/lge/ims/PhoneStateTracker;->mCallState:I

    #@2
    return p1
.end method

.method static synthetic access$100(Lcom/lge/ims/PhoneStateTracker;)Landroid/os/RegistrantList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 24
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mCallStateChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Lcom/lge/ims/PhoneStateTracker;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/lge/ims/PhoneStateTracker;->mAirplaneMode:Z

    #@2
    return v0
.end method

.method static synthetic access$1002(Lcom/lge/ims/PhoneStateTracker;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/lge/ims/PhoneStateTracker;->mAirplaneMode:Z

    #@2
    return p1
.end method

.method static synthetic access$1100(Lcom/lge/ims/PhoneStateTracker;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 24
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1200(Lcom/lge/ims/PhoneStateTracker;)Landroid/os/RegistrantList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 24
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mAirplaneModeChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    return-object v0
.end method

.method static synthetic access$1300(Lcom/lge/ims/PhoneStateTracker;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 24
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mIccState:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$1302(Lcom/lge/ims/PhoneStateTracker;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 24
    iput-object p1, p0, Lcom/lge/ims/PhoneStateTracker;->mIccState:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$1400(Lcom/lge/ims/PhoneStateTracker;)Landroid/os/RegistrantList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 24
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mSimStateChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    return-object v0
.end method

.method static synthetic access$1500(Lcom/lge/ims/PhoneStateTracker;)Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 24
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mLteStateInfo:Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@2
    return-object v0
.end method

.method static synthetic access$1502(Lcom/lge/ims/PhoneStateTracker;Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;)Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 24
    iput-object p1, p0, Lcom/lge/ims/PhoneStateTracker;->mLteStateInfo:Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@2
    return-object p1
.end method

.method static synthetic access$200(Lcom/lge/ims/PhoneStateTracker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 24
    iget v0, p0, Lcom/lge/ims/PhoneStateTracker;->mPhoneServiceState:I

    #@2
    return v0
.end method

.method static synthetic access$202(Lcom/lge/ims/PhoneStateTracker;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 24
    iput p1, p0, Lcom/lge/ims/PhoneStateTracker;->mPhoneServiceState:I

    #@2
    return p1
.end method

.method static synthetic access$300(Lcom/lge/ims/PhoneStateTracker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 24
    iget v0, p0, Lcom/lge/ims/PhoneStateTracker;->mDataServiceState:I

    #@2
    return v0
.end method

.method static synthetic access$302(Lcom/lge/ims/PhoneStateTracker;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 24
    iput p1, p0, Lcom/lge/ims/PhoneStateTracker;->mDataServiceState:I

    #@2
    return p1
.end method

.method static synthetic access$400(Lcom/lge/ims/PhoneStateTracker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 24
    iget v0, p0, Lcom/lge/ims/PhoneStateTracker;->mRAT:I

    #@2
    return v0
.end method

.method static synthetic access$402(Lcom/lge/ims/PhoneStateTracker;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 24
    iput p1, p0, Lcom/lge/ims/PhoneStateTracker;->mRAT:I

    #@2
    return p1
.end method

.method static synthetic access$500(Lcom/lge/ims/PhoneStateTracker;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/lge/ims/PhoneStateTracker;->mRoaming:Z

    #@2
    return v0
.end method

.method static synthetic access$502(Lcom/lge/ims/PhoneStateTracker;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/lge/ims/PhoneStateTracker;->mRoaming:Z

    #@2
    return p1
.end method

.method static synthetic access$600(Lcom/lge/ims/PhoneStateTracker;)Landroid/os/RegistrantList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 24
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mServiceStateChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Lcom/lge/ims/PhoneStateTracker;)Landroid/os/RegistrantList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 24
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mDataServiceStateChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    return-object v0
.end method

.method static synthetic access$800(Lcom/lge/ims/PhoneStateTracker;)Landroid/os/RegistrantList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 24
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mRATChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    return-object v0
.end method

.method static synthetic access$900(Lcom/lge/ims/PhoneStateTracker;)Landroid/os/RegistrantList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 24
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mRoamingStateChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    return-object v0
.end method

.method public static getInstance()Lcom/lge/ims/PhoneStateTracker;
    .registers 1

    #@0
    .prologue
    .line 86
    sget-object v0, Lcom/lge/ims/PhoneStateTracker;->mPhoneStateTracker:Lcom/lge/ims/PhoneStateTracker;

    #@2
    return-object v0
.end method

.method private setListener(Z)V
    .registers 7
    .parameter "isRegistering"

    #@0
    .prologue
    .line 391
    const-string v2, "PhoneStateTracker"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "setListener :: isRegistering="

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 393
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getTelephonyManager()Landroid/telephony/TelephonyManager;

    #@1b
    move-result-object v1

    #@1c
    .line 394
    .local v1, tm:Landroid/telephony/TelephonyManager;
    const/4 v0, 0x0

    #@1d
    .line 396
    .local v0, flags:I
    if-eqz v1, :cond_36

    #@1f
    .line 397
    if-eqz p1, :cond_31

    #@21
    .line 398
    or-int/lit8 v0, v0, 0x1

    #@23
    .line 399
    or-int/lit8 v0, v0, 0x20

    #@25
    .line 404
    sget-object v2, Lcom/lge/ims/Configuration;->OPERATOR:Ljava/lang/String;

    #@27
    const-string v3, "KDDI"

    #@29
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2c
    move-result v2

    #@2d
    if-eqz v2, :cond_31

    #@2f
    .line 405
    or-int/lit16 v0, v0, 0x80

    #@31
    .line 409
    :cond_31
    iget-object v2, p0, Lcom/lge/ims/PhoneStateTracker;->mPhoneStateListener:Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;

    #@33
    invoke-virtual {v1, v2, v0}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    #@36
    .line 411
    :cond_36
    return-void
.end method


# virtual methods
.method public create()V
    .registers 3

    #@0
    .prologue
    .line 90
    iget v0, p0, Lcom/lge/ims/PhoneStateTracker;->mPolicy:I

    #@2
    or-int/lit8 v0, v0, 0x2

    #@4
    iput v0, p0, Lcom/lge/ims/PhoneStateTracker;->mPolicy:I

    #@6
    .line 91
    iget v0, p0, Lcom/lge/ims/PhoneStateTracker;->mPolicy:I

    #@8
    or-int/lit8 v0, v0, 0x4

    #@a
    iput v0, p0, Lcom/lge/ims/PhoneStateTracker;->mPolicy:I

    #@c
    .line 92
    iget v0, p0, Lcom/lge/ims/PhoneStateTracker;->mPolicy:I

    #@e
    or-int/lit8 v0, v0, 0x8

    #@10
    iput v0, p0, Lcom/lge/ims/PhoneStateTracker;->mPolicy:I

    #@12
    .line 93
    iget v0, p0, Lcom/lge/ims/PhoneStateTracker;->mPolicy:I

    #@14
    or-int/lit8 v0, v0, 0x10

    #@16
    iput v0, p0, Lcom/lge/ims/PhoneStateTracker;->mPolicy:I

    #@18
    .line 95
    sget-object v0, Lcom/lge/ims/Configuration;->OPERATOR:Ljava/lang/String;

    #@1a
    const-string v1, "KDDI"

    #@1c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v0

    #@20
    if-eqz v0, :cond_28

    #@22
    .line 96
    iget v0, p0, Lcom/lge/ims/PhoneStateTracker;->mPolicy:I

    #@24
    or-int/lit8 v0, v0, 0x1

    #@26
    iput v0, p0, Lcom/lge/ims/PhoneStateTracker;->mPolicy:I

    #@28
    .line 99
    :cond_28
    invoke-static {}, Lcom/lge/ims/Configuration;->isEmergencySupported()Z

    #@2b
    move-result v0

    #@2c
    if-nez v0, :cond_35

    #@2e
    .line 100
    const/4 v0, 0x3

    #@2f
    invoke-static {v0}, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;->fromInt(I)Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@32
    move-result-object v0

    #@33
    iput-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mLteStateInfo:Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@35
    .line 103
    :cond_35
    const/4 v0, 0x1

    #@36
    invoke-direct {p0, v0}, Lcom/lge/ims/PhoneStateTracker;->setListener(Z)V

    #@39
    .line 104
    return-void
.end method

.method public destroy()V
    .registers 3

    #@0
    .prologue
    .line 107
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Lcom/lge/ims/PhoneStateTracker;->setListener(Z)V

    #@4
    .line 109
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mContext:Landroid/content/Context;

    #@6
    if-eqz v0, :cond_f

    #@8
    .line 110
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mContext:Landroid/content/Context;

    #@a
    iget-object v1, p0, Lcom/lge/ims/PhoneStateTracker;->mPhoneStateReceiver:Lcom/lge/ims/PhoneStateTracker$PhoneStateReceiver;

    #@c
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@f
    .line 112
    :cond_f
    return-void
.end method

.method public getCallState()I
    .registers 2

    #@0
    .prologue
    .line 116
    iget v0, p0, Lcom/lge/ims/PhoneStateTracker;->mCallState:I

    #@2
    return v0
.end method

.method public getDataServiceState()I
    .registers 2

    #@0
    .prologue
    .line 121
    iget v0, p0, Lcom/lge/ims/PhoneStateTracker;->mDataServiceState:I

    #@2
    return v0
.end method

.method public getIccState()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 126
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mIccState:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getNetworkType()I
    .registers 2

    #@0
    .prologue
    .line 131
    iget v0, p0, Lcom/lge/ims/PhoneStateTracker;->mRAT:I

    #@2
    packed-switch v0, :pswitch_data_2a

    #@5
    .line 162
    const/4 v0, 0x0

    #@6
    :goto_6
    return v0

    #@7
    .line 133
    :pswitch_7
    const/4 v0, 0x1

    #@8
    goto :goto_6

    #@9
    .line 135
    :pswitch_9
    const/4 v0, 0x2

    #@a
    goto :goto_6

    #@b
    .line 137
    :pswitch_b
    const/4 v0, 0x3

    #@c
    goto :goto_6

    #@d
    .line 140
    :pswitch_d
    const/4 v0, 0x4

    #@e
    goto :goto_6

    #@f
    .line 142
    :pswitch_f
    const/4 v0, 0x7

    #@10
    goto :goto_6

    #@11
    .line 144
    :pswitch_11
    const/4 v0, 0x5

    #@12
    goto :goto_6

    #@13
    .line 146
    :pswitch_13
    const/4 v0, 0x6

    #@14
    goto :goto_6

    #@15
    .line 148
    :pswitch_15
    const/16 v0, 0x8

    #@17
    goto :goto_6

    #@18
    .line 150
    :pswitch_18
    const/16 v0, 0x9

    #@1a
    goto :goto_6

    #@1b
    .line 152
    :pswitch_1b
    const/16 v0, 0xa

    #@1d
    goto :goto_6

    #@1e
    .line 154
    :pswitch_1e
    const/16 v0, 0xc

    #@20
    goto :goto_6

    #@21
    .line 156
    :pswitch_21
    const/16 v0, 0xe

    #@23
    goto :goto_6

    #@24
    .line 158
    :pswitch_24
    const/16 v0, 0xd

    #@26
    goto :goto_6

    #@27
    .line 160
    :pswitch_27
    const/16 v0, 0xf

    #@29
    goto :goto_6

    #@2a
    .line 131
    :pswitch_data_2a
    .packed-switch 0x1
        :pswitch_7
        :pswitch_9
        :pswitch_b
        :pswitch_d
        :pswitch_d
        :pswitch_f
        :pswitch_11
        :pswitch_13
        :pswitch_15
        :pswitch_18
        :pswitch_1b
        :pswitch_1e
        :pswitch_21
        :pswitch_24
        :pswitch_27
    .end packed-switch
.end method

.method public getPhoneServiceState()I
    .registers 2

    #@0
    .prologue
    .line 168
    iget v0, p0, Lcom/lge/ims/PhoneStateTracker;->mPhoneServiceState:I

    #@2
    return v0
.end method

.method public getRAT()I
    .registers 2

    #@0
    .prologue
    .line 173
    iget v0, p0, Lcom/lge/ims/PhoneStateTracker;->mRAT:I

    #@2
    return v0
.end method

.method public is1xRTT()Z
    .registers 2

    #@0
    .prologue
    .line 181
    iget v0, p0, Lcom/lge/ims/PhoneStateTracker;->mRAT:I

    #@2
    packed-switch v0, :pswitch_data_a

    #@5
    .line 187
    const/4 v0, 0x0

    #@6
    :goto_6
    return v0

    #@7
    .line 185
    :pswitch_7
    const/4 v0, 0x1

    #@8
    goto :goto_6

    #@9
    .line 181
    nop

    #@a
    :pswitch_data_a
    .packed-switch 0x4
        :pswitch_7
        :pswitch_7
        :pswitch_7
    .end packed-switch
.end method

.method protected is1xRTTRequired()Z
    .registers 2

    #@0
    .prologue
    .line 371
    iget v0, p0, Lcom/lge/ims/PhoneStateTracker;->mPolicy:I

    #@2
    and-int/lit8 v0, v0, 0x1

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public is2G()Z
    .registers 2

    #@0
    .prologue
    .line 192
    iget v0, p0, Lcom/lge/ims/PhoneStateTracker;->mRAT:I

    #@2
    sparse-switch v0, :sswitch_data_a

    #@5
    .line 197
    const/4 v0, 0x0

    #@6
    :goto_6
    return v0

    #@7
    .line 195
    :sswitch_7
    const/4 v0, 0x1

    #@8
    goto :goto_6

    #@9
    .line 192
    nop

    #@a
    :sswitch_data_a
    .sparse-switch
        0x2 -> :sswitch_7
        0x10 -> :sswitch_7
    .end sparse-switch
.end method

.method protected is2GRequired()Z
    .registers 2

    #@0
    .prologue
    .line 375
    iget v0, p0, Lcom/lge/ims/PhoneStateTracker;->mPolicy:I

    #@2
    and-int/lit8 v0, v0, 0x10

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public is3G()Z
    .registers 2

    #@0
    .prologue
    .line 202
    iget v0, p0, Lcom/lge/ims/PhoneStateTracker;->mRAT:I

    #@2
    sparse-switch v0, :sswitch_data_a

    #@5
    .line 210
    const/4 v0, 0x0

    #@6
    :goto_6
    return v0

    #@7
    .line 208
    :sswitch_7
    const/4 v0, 0x1

    #@8
    goto :goto_6

    #@9
    .line 202
    nop

    #@a
    :sswitch_data_a
    .sparse-switch
        0x3 -> :sswitch_7
        0x9 -> :sswitch_7
        0xa -> :sswitch_7
        0xb -> :sswitch_7
        0xf -> :sswitch_7
    .end sparse-switch
.end method

.method protected is3GRequired()Z
    .registers 2

    #@0
    .prologue
    .line 379
    iget v0, p0, Lcom/lge/ims/PhoneStateTracker;->mPolicy:I

    #@2
    and-int/lit8 v0, v0, 0x4

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public is4G()Z
    .registers 3

    #@0
    .prologue
    .line 215
    iget v0, p0, Lcom/lge/ims/PhoneStateTracker;->mRAT:I

    #@2
    const/16 v1, 0xe

    #@4
    if-ne v0, v1, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method protected is4GRequired()Z
    .registers 2

    #@0
    .prologue
    .line 383
    iget v0, p0, Lcom/lge/ims/PhoneStateTracker;->mPolicy:I

    #@2
    and-int/lit8 v0, v0, 0x8

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public isAirplaneMode()Z
    .registers 2

    #@0
    .prologue
    .line 177
    iget-boolean v0, p0, Lcom/lge/ims/PhoneStateTracker;->mAirplaneMode:Z

    #@2
    return v0
.end method

.method public isLteEmergencyOnly()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 223
    iget-object v1, p0, Lcom/lge/ims/PhoneStateTracker;->mLteStateInfo:Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@3
    if-nez v1, :cond_6

    #@5
    .line 227
    :cond_5
    :goto_5
    return v0

    #@6
    :cond_6
    iget-object v1, p0, Lcom/lge/ims/PhoneStateTracker;->mLteStateInfo:Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@8
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;->getCode()I

    #@b
    move-result v1

    #@c
    sget-object v2, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;->NORMAL_ATTACHED:Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@e
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;->getCode()I

    #@11
    move-result v2

    #@12
    if-ge v1, v2, :cond_5

    #@14
    const/4 v0, 0x1

    #@15
    goto :goto_5
.end method

.method public isRoaming()Z
    .registers 2

    #@0
    .prologue
    .line 231
    iget-boolean v0, p0, Lcom/lge/ims/PhoneStateTracker;->mRoaming:Z

    #@2
    return v0
.end method

.method public iseHRPD()Z
    .registers 3

    #@0
    .prologue
    .line 219
    iget v0, p0, Lcom/lge/ims/PhoneStateTracker;->mRAT:I

    #@2
    const/16 v1, 0xd

    #@4
    if-ne v0, v1, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method protected iseHRPDRequired()Z
    .registers 2

    #@0
    .prologue
    .line 387
    iget v0, p0, Lcom/lge/ims/PhoneStateTracker;->mPolicy:I

    #@2
    and-int/lit8 v0, v0, 0x2

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public registerForAirplaneModeChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 305
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mAirplaneModeChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    new-instance v1, Landroid/os/Registrant;

    #@4
    invoke-direct {v1, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@7
    invoke-virtual {v0, v1}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 306
    return-void
.end method

.method public registerForCallStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 236
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mCallStateChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    new-instance v1, Landroid/os/Registrant;

    #@4
    invoke-direct {v1, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@7
    invoke-virtual {v0, v1}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 237
    return-void
.end method

.method public registerForDataServiceStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 257
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mDataServiceStateChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    new-instance v1, Landroid/os/Registrant;

    #@4
    invoke-direct {v1, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@7
    invoke-virtual {v0, v1}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 258
    return-void
.end method

.method public registerForRATChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 266
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mRATChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    new-instance v1, Landroid/os/Registrant;

    #@4
    invoke-direct {v1, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@7
    invoke-virtual {v0, v1}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 267
    return-void
.end method

.method public registerForRoamingStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 275
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mRoamingStateChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    new-instance v1, Landroid/os/Registrant;

    #@4
    invoke-direct {v1, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@7
    invoke-virtual {v0, v1}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 276
    return-void
.end method

.method public registerForServiceStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 284
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mServiceStateChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    new-instance v1, Landroid/os/Registrant;

    #@4
    invoke-direct {v1, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@7
    invoke-virtual {v0, v1}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 285
    return-void
.end method

.method public registerForSimStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 314
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mSimStateChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    new-instance v1, Landroid/os/Registrant;

    #@4
    invoke-direct {v1, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@7
    invoke-virtual {v0, v1}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 315
    return-void
.end method

.method public setAccessPolicy(I)V
    .registers 4
    .parameter "accessPolicy"

    #@0
    .prologue
    .line 322
    invoke-virtual {p0}, Lcom/lge/ims/PhoneStateTracker;->is1xRTTRequired()Z

    #@3
    move-result v0

    #@4
    .line 324
    .local v0, is1xRTTPresent:Z
    iget v1, p0, Lcom/lge/ims/PhoneStateTracker;->mPolicy:I

    #@6
    and-int/lit16 v1, v1, -0x100

    #@8
    iput v1, p0, Lcom/lge/ims/PhoneStateTracker;->mPolicy:I

    #@a
    .line 326
    if-eqz v0, :cond_12

    #@c
    .line 327
    iget v1, p0, Lcom/lge/ims/PhoneStateTracker;->mPolicy:I

    #@e
    or-int/lit8 v1, v1, 0x1

    #@10
    iput v1, p0, Lcom/lge/ims/PhoneStateTracker;->mPolicy:I

    #@12
    .line 334
    :cond_12
    const/high16 v1, 0x2000

    #@14
    and-int/2addr v1, p1

    #@15
    if-eqz v1, :cond_1d

    #@17
    .line 335
    iget v1, p0, Lcom/lge/ims/PhoneStateTracker;->mPolicy:I

    #@19
    or-int/lit8 v1, v1, 0x8

    #@1b
    iput v1, p0, Lcom/lge/ims/PhoneStateTracker;->mPolicy:I

    #@1d
    .line 338
    :cond_1d
    const/high16 v1, 0x20

    #@1f
    and-int/2addr v1, p1

    #@20
    if-eqz v1, :cond_28

    #@22
    .line 339
    iget v1, p0, Lcom/lge/ims/PhoneStateTracker;->mPolicy:I

    #@24
    or-int/lit8 v1, v1, 0x4

    #@26
    iput v1, p0, Lcom/lge/ims/PhoneStateTracker;->mPolicy:I

    #@28
    .line 342
    :cond_28
    const/high16 v1, 0x1000

    #@2a
    and-int/2addr v1, p1

    #@2b
    if-eqz v1, :cond_33

    #@2d
    .line 343
    iget v1, p0, Lcom/lge/ims/PhoneStateTracker;->mPolicy:I

    #@2f
    or-int/lit8 v1, v1, 0x2

    #@31
    iput v1, p0, Lcom/lge/ims/PhoneStateTracker;->mPolicy:I

    #@33
    .line 346
    :cond_33
    const/high16 v1, 0x8

    #@35
    and-int/2addr v1, p1

    #@36
    if-eqz v1, :cond_3e

    #@38
    .line 347
    iget v1, p0, Lcom/lge/ims/PhoneStateTracker;->mPolicy:I

    #@3a
    or-int/lit8 v1, v1, 0x10

    #@3c
    iput v1, p0, Lcom/lge/ims/PhoneStateTracker;->mPolicy:I

    #@3e
    .line 349
    :cond_3e
    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 352
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mContext:Landroid/content/Context;

    #@2
    if-eq v0, p1, :cond_15

    #@4
    .line 354
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mContext:Landroid/content/Context;

    #@6
    if-eqz v0, :cond_f

    #@8
    .line 355
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mContext:Landroid/content/Context;

    #@a
    iget-object v1, p0, Lcom/lge/ims/PhoneStateTracker;->mPhoneStateReceiver:Lcom/lge/ims/PhoneStateTracker$PhoneStateReceiver;

    #@c
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@f
    .line 358
    :cond_f
    iput-object p1, p0, Lcom/lge/ims/PhoneStateTracker;->mContext:Landroid/content/Context;

    #@11
    .line 360
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mContext:Landroid/content/Context;

    #@13
    if-nez v0, :cond_16

    #@15
    .line 368
    :cond_15
    :goto_15
    return-void

    #@16
    .line 364
    :cond_16
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mPhoneStateReceiver:Lcom/lge/ims/PhoneStateTracker$PhoneStateReceiver;

    #@18
    if-eqz v0, :cond_15

    #@1a
    .line 365
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mContext:Landroid/content/Context;

    #@1c
    iget-object v1, p0, Lcom/lge/ims/PhoneStateTracker;->mPhoneStateReceiver:Lcom/lge/ims/PhoneStateTracker$PhoneStateReceiver;

    #@1e
    iget-object v2, p0, Lcom/lge/ims/PhoneStateTracker;->mPhoneStateReceiver:Lcom/lge/ims/PhoneStateTracker$PhoneStateReceiver;

    #@20
    invoke-virtual {v2}, Lcom/lge/ims/PhoneStateTracker$PhoneStateReceiver;->getFilter()Landroid/content/IntentFilter;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@27
    goto :goto_15
.end method

.method public unregisterForAirplaneModeChanged(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 309
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mAirplaneModeChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 310
    return-void
.end method

.method public unregisterForCallStateChanged(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 240
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mCallStateChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 241
    return-void
.end method

.method public unregisterForDataServiceStateChanged(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 261
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mDataServiceStateChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 262
    return-void
.end method

.method public unregisterForRATChanged(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 270
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mRATChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 271
    return-void
.end method

.method public unregisterForRoamingStateChanged(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 279
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mRoamingStateChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 280
    return-void
.end method

.method public unregisterForServiceStateChanged(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 288
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mServiceStateChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 289
    return-void
.end method

.method public unregisterForSimStateChanged(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 318
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker;->mSimStateChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 319
    return-void
.end method
