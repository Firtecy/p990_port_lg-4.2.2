.class final Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;
.super Landroid/telephony/PhoneStateListener;
.source "PhoneStateTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/PhoneStateTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ImsPhoneStateListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/PhoneStateTracker;


# direct methods
.method public constructor <init>(Lcom/lge/ims/PhoneStateTracker;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 415
    iput-object p1, p0, Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@2
    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    #@5
    .line 416
    return-void
.end method


# virtual methods
.method public onCallStateChanged(ILjava/lang/String;)V
    .registers 6
    .parameter "state"
    .parameter "incomingNumber"

    #@0
    .prologue
    .line 424
    const-string v0, "PhoneStateTracker"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "onCallStateChanged :: state="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, ", incomingNumber="

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@22
    .line 426
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@24
    invoke-static {v0}, Lcom/lge/ims/PhoneStateTracker;->access$000(Lcom/lge/ims/PhoneStateTracker;)I

    #@27
    move-result v0

    #@28
    if-eq v0, p1, :cond_42

    #@2a
    .line 427
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@2c
    invoke-static {v0, p1}, Lcom/lge/ims/PhoneStateTracker;->access$002(Lcom/lge/ims/PhoneStateTracker;I)I

    #@2f
    .line 428
    iget-object v0, p0, Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@31
    invoke-static {v0}, Lcom/lge/ims/PhoneStateTracker;->access$100(Lcom/lge/ims/PhoneStateTracker;)Landroid/os/RegistrantList;

    #@34
    move-result-object v0

    #@35
    iget-object v1, p0, Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@37
    invoke-static {v1}, Lcom/lge/ims/PhoneStateTracker;->access$000(Lcom/lge/ims/PhoneStateTracker;)I

    #@3a
    move-result v1

    #@3b
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3e
    move-result-object v1

    #@3f
    invoke-virtual {v0, v1}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@42
    .line 430
    :cond_42
    return-void
.end method

.method public onDataActivity(I)V
    .registers 5
    .parameter "direction"

    #@0
    .prologue
    .line 440
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getTelephonyManager()Landroid/telephony/TelephonyManager;

    #@3
    move-result-object v0

    #@4
    .line 442
    .local v0, tm:Landroid/telephony/TelephonyManager;
    if-eqz v0, :cond_14

    #@6
    .line 443
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDataState()I

    #@9
    move-result v1

    #@a
    const/4 v2, 0x2

    #@b
    if-eq v1, v2, :cond_14

    #@d
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDataState()I

    #@10
    move-result v1

    #@11
    const/4 v2, 0x3

    #@12
    if-ne v1, v2, :cond_14

    #@14
    .line 456
    :cond_14
    return-void
.end method

.method public onDataConnectionStateChanged(I)V
    .registers 5
    .parameter "state"

    #@0
    .prologue
    .line 465
    const-string v0, "PhoneStateTracker"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "onDataConnectionStateChanged :: state="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 466
    return-void
.end method

.method public onDataConnectionStateChanged(II)V
    .registers 6
    .parameter "state"
    .parameter "networkType"

    #@0
    .prologue
    .line 471
    const-string v0, "PhoneStateTracker"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "onDataConnectionStateChanged :: state="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, ", networkType="

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@22
    .line 472
    return-void
.end method

.method public onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .registers 8
    .parameter "serviceState"

    #@0
    .prologue
    .line 481
    const-string v3, "PhoneStateTracker"

    #@2
    new-instance v4, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v5, "onServiceStateChanged :: state="

    #@9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v4

    #@d
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->toString()Ljava/lang/String;

    #@10
    move-result-object v5

    #@11
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v4

    #@15
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v4

    #@19
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@1c
    .line 483
    const/4 v0, 0x0

    #@1d
    .line 484
    .local v0, dataServiceStateChanged:Z
    const/4 v1, 0x0

    #@1e
    .line 485
    .local v1, ratChanged:Z
    const/4 v2, 0x0

    #@1f
    .line 488
    .local v2, roamingChanged:Z
    iget-object v3, p0, Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@21
    invoke-static {v3}, Lcom/lge/ims/PhoneStateTracker;->access$200(Lcom/lge/ims/PhoneStateTracker;)I

    #@24
    move-result v3

    #@25
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getState()I

    #@28
    move-result v4

    #@29
    if-eq v3, v4, :cond_34

    #@2b
    .line 489
    iget-object v3, p0, Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@2d
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getState()I

    #@30
    move-result v4

    #@31
    invoke-static {v3, v4}, Lcom/lge/ims/PhoneStateTracker;->access$202(Lcom/lge/ims/PhoneStateTracker;I)I

    #@34
    .line 493
    :cond_34
    iget-object v3, p0, Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@36
    invoke-static {v3}, Lcom/lge/ims/PhoneStateTracker;->access$300(Lcom/lge/ims/PhoneStateTracker;)I

    #@39
    move-result v3

    #@3a
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getDataState()I

    #@3d
    move-result v4

    #@3e
    if-eq v3, v4, :cond_4a

    #@40
    .line 494
    iget-object v3, p0, Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@42
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getDataState()I

    #@45
    move-result v4

    #@46
    invoke-static {v3, v4}, Lcom/lge/ims/PhoneStateTracker;->access$302(Lcom/lge/ims/PhoneStateTracker;I)I

    #@49
    .line 495
    const/4 v0, 0x1

    #@4a
    .line 499
    :cond_4a
    iget-object v3, p0, Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@4c
    invoke-static {v3}, Lcom/lge/ims/PhoneStateTracker;->access$400(Lcom/lge/ims/PhoneStateTracker;)I

    #@4f
    move-result v3

    #@50
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getRadioTechnology()I

    #@53
    move-result v4

    #@54
    if-eq v3, v4, :cond_60

    #@56
    .line 500
    iget-object v3, p0, Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@58
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getRadioTechnology()I

    #@5b
    move-result v4

    #@5c
    invoke-static {v3, v4}, Lcom/lge/ims/PhoneStateTracker;->access$402(Lcom/lge/ims/PhoneStateTracker;I)I

    #@5f
    .line 501
    const/4 v1, 0x1

    #@60
    .line 505
    :cond_60
    iget-object v3, p0, Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@62
    invoke-static {v3}, Lcom/lge/ims/PhoneStateTracker;->access$500(Lcom/lge/ims/PhoneStateTracker;)Z

    #@65
    move-result v3

    #@66
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@69
    move-result v4

    #@6a
    if-eq v3, v4, :cond_76

    #@6c
    .line 506
    iget-object v3, p0, Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@6e
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@71
    move-result v4

    #@72
    invoke-static {v3, v4}, Lcom/lge/ims/PhoneStateTracker;->access$502(Lcom/lge/ims/PhoneStateTracker;Z)Z

    #@75
    .line 507
    const/4 v2, 0x1

    #@76
    .line 511
    :cond_76
    iget-object v3, p0, Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@78
    invoke-static {v3}, Lcom/lge/ims/PhoneStateTracker;->access$600(Lcom/lge/ims/PhoneStateTracker;)Landroid/os/RegistrantList;

    #@7b
    move-result-object v3

    #@7c
    invoke-virtual {v3, p1}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@7f
    .line 513
    if-eqz v0, :cond_94

    #@81
    .line 514
    iget-object v3, p0, Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@83
    invoke-static {v3}, Lcom/lge/ims/PhoneStateTracker;->access$700(Lcom/lge/ims/PhoneStateTracker;)Landroid/os/RegistrantList;

    #@86
    move-result-object v3

    #@87
    iget-object v4, p0, Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@89
    invoke-static {v4}, Lcom/lge/ims/PhoneStateTracker;->access$300(Lcom/lge/ims/PhoneStateTracker;)I

    #@8c
    move-result v4

    #@8d
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@90
    move-result-object v4

    #@91
    invoke-virtual {v3, v4}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@94
    .line 517
    :cond_94
    if-eqz v1, :cond_b4

    #@96
    .line 519
    iget-object v3, p0, Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@98
    invoke-virtual {v3}, Lcom/lge/ims/PhoneStateTracker;->is4GRequired()Z

    #@9b
    move-result v3

    #@9c
    if-eqz v3, :cond_ca

    #@9e
    iget-object v3, p0, Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@a0
    invoke-virtual {v3}, Lcom/lge/ims/PhoneStateTracker;->is4G()Z

    #@a3
    move-result v3

    #@a4
    if-eqz v3, :cond_ca

    #@a6
    .line 520
    iget-object v3, p0, Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@a8
    invoke-static {v3}, Lcom/lge/ims/PhoneStateTracker;->access$800(Lcom/lge/ims/PhoneStateTracker;)Landroid/os/RegistrantList;

    #@ab
    move-result-object v3

    #@ac
    const/4 v4, 0x2

    #@ad
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@b0
    move-result-object v4

    #@b1
    invoke-virtual {v3, v4}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@b4
    .line 532
    :cond_b4
    :goto_b4
    if-eqz v2, :cond_c9

    #@b6
    .line 533
    iget-object v3, p0, Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@b8
    invoke-static {v3}, Lcom/lge/ims/PhoneStateTracker;->access$900(Lcom/lge/ims/PhoneStateTracker;)Landroid/os/RegistrantList;

    #@bb
    move-result-object v3

    #@bc
    iget-object v4, p0, Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@be
    invoke-static {v4}, Lcom/lge/ims/PhoneStateTracker;->access$500(Lcom/lge/ims/PhoneStateTracker;)Z

    #@c1
    move-result v4

    #@c2
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@c5
    move-result-object v4

    #@c6
    invoke-virtual {v3, v4}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@c9
    .line 535
    :cond_c9
    return-void

    #@ca
    .line 521
    :cond_ca
    iget-object v3, p0, Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@cc
    invoke-virtual {v3}, Lcom/lge/ims/PhoneStateTracker;->is3GRequired()Z

    #@cf
    move-result v3

    #@d0
    if-eqz v3, :cond_e9

    #@d2
    iget-object v3, p0, Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@d4
    invoke-virtual {v3}, Lcom/lge/ims/PhoneStateTracker;->is3G()Z

    #@d7
    move-result v3

    #@d8
    if-eqz v3, :cond_e9

    #@da
    .line 522
    iget-object v3, p0, Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@dc
    invoke-static {v3}, Lcom/lge/ims/PhoneStateTracker;->access$800(Lcom/lge/ims/PhoneStateTracker;)Landroid/os/RegistrantList;

    #@df
    move-result-object v3

    #@e0
    const/4 v4, 0x3

    #@e1
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@e4
    move-result-object v4

    #@e5
    invoke-virtual {v3, v4}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@e8
    goto :goto_b4

    #@e9
    .line 523
    :cond_e9
    iget-object v3, p0, Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@eb
    invoke-virtual {v3}, Lcom/lge/ims/PhoneStateTracker;->iseHRPDRequired()Z

    #@ee
    move-result v3

    #@ef
    if-eqz v3, :cond_108

    #@f1
    iget-object v3, p0, Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@f3
    invoke-virtual {v3}, Lcom/lge/ims/PhoneStateTracker;->iseHRPD()Z

    #@f6
    move-result v3

    #@f7
    if-eqz v3, :cond_108

    #@f9
    .line 524
    iget-object v3, p0, Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@fb
    invoke-static {v3}, Lcom/lge/ims/PhoneStateTracker;->access$800(Lcom/lge/ims/PhoneStateTracker;)Landroid/os/RegistrantList;

    #@fe
    move-result-object v3

    #@ff
    const/4 v4, 0x1

    #@100
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@103
    move-result-object v4

    #@104
    invoke-virtual {v3, v4}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@107
    goto :goto_b4

    #@108
    .line 525
    :cond_108
    iget-object v3, p0, Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@10a
    invoke-virtual {v3}, Lcom/lge/ims/PhoneStateTracker;->is1xRTTRequired()Z

    #@10d
    move-result v3

    #@10e
    if-eqz v3, :cond_127

    #@110
    iget-object v3, p0, Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@112
    invoke-virtual {v3}, Lcom/lge/ims/PhoneStateTracker;->is1xRTT()Z

    #@115
    move-result v3

    #@116
    if-eqz v3, :cond_127

    #@118
    .line 526
    iget-object v3, p0, Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@11a
    invoke-static {v3}, Lcom/lge/ims/PhoneStateTracker;->access$800(Lcom/lge/ims/PhoneStateTracker;)Landroid/os/RegistrantList;

    #@11d
    move-result-object v3

    #@11e
    const/4 v4, 0x4

    #@11f
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@122
    move-result-object v4

    #@123
    invoke-virtual {v3, v4}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@126
    goto :goto_b4

    #@127
    .line 527
    :cond_127
    iget-object v3, p0, Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@129
    invoke-virtual {v3}, Lcom/lge/ims/PhoneStateTracker;->is2GRequired()Z

    #@12c
    move-result v3

    #@12d
    if-eqz v3, :cond_b4

    #@12f
    iget-object v3, p0, Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@131
    invoke-virtual {v3}, Lcom/lge/ims/PhoneStateTracker;->is2G()Z

    #@134
    move-result v3

    #@135
    if-eqz v3, :cond_b4

    #@137
    .line 528
    iget-object v3, p0, Lcom/lge/ims/PhoneStateTracker$ImsPhoneStateListener;->this$0:Lcom/lge/ims/PhoneStateTracker;

    #@139
    invoke-static {v3}, Lcom/lge/ims/PhoneStateTracker;->access$800(Lcom/lge/ims/PhoneStateTracker;)Landroid/os/RegistrantList;

    #@13c
    move-result-object v3

    #@13d
    const/4 v4, 0x5

    #@13e
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@141
    move-result-object v4

    #@142
    invoke-virtual {v3, v4}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@145
    goto/16 :goto_b4
.end method
