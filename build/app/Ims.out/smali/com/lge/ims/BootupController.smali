.class public Lcom/lge/ims/BootupController;
.super Ljava/lang/Object;
.source "BootupController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/BootupController$2;,
        Lcom/lge/ims/BootupController$BootupReceiver;,
        Lcom/lge/ims/BootupController$STATE_CONFIG;,
        Lcom/lge/ims/BootupController$STATE;,
        Lcom/lge/ims/BootupController$InternalMsg;
    }
.end annotation


# static fields
.field private static final ACTION_AC_START:Ljava/lang/String; = "com.lge.ims.ac.action.AC_START"

.field private static final ACTION_AC_STOPED:Ljava/lang/String; = "com.lge.ims.ac.action.AC_STOPED"

.field private static final ACTION_CONFIG_UPDATE_COMPLETED:Ljava/lang/String; = "com.lge.ims.action.IMS_EVENT_CONFIG_UPDATE_COMPLETE"

.field private static final ACTION_RCSE_STOPED:Ljava/lang/String; = "com.lge.ims.controller.action.RCSE_STOPED"

.field private static final ACTION_SETTINGS_CHANGED:Ljava/lang/String; = "com.lge.ims.action.SETTINGS_CHANGED"

.field private static final ACTION_STARTER:Ljava/lang/String; = "com.lge.ims.action.STARTER"

.field private static final ACTION_STOP_RCSE:Ljava/lang/String; = "com.lge.ims.action.STOP_RCSE"

.field private static final ACTION_VALIDITY_EXPIRED:Ljava/lang/String; = "com.lge.ims.ac.action.VALIDITY_CHANGED"

.field private static final CONFIG_EVENT_MASK:I = 0x640000

.field private static final CONTENT_URI_PROVISIONING:Ljava/lang/String; = "content://com.lge.ims.provisioning/sims"

.field private static final CONTENT_URI_SETTING:Ljava/lang/String; = "content://com.lge.ims.provisioning/settings"

.field private static final IMS_DATA_CONNECTED:I = 0x0

.field private static final IMS_DATA_DISCONNECTED:I = 0x1

.field private static final MSG_INTERNAL_BASE:I = 0x64

.field private static final STOP_SVC_CHECKABLE:Ljava/lang/String; = "0"

.field private static final STOP_SVC_FORCEFUL:Ljava/lang/String; = "-1"

.field private static final TAG:Ljava/lang/String; = "BootupController"


# instance fields
.field private bNetConnected:Z

.field private mAbcAddress:Ljava/lang/String;

.field private mAbcAddressPort:Ljava/lang/String;

.field private mAudioPort:I

.field private mBootupReceiver:Lcom/lge/ims/BootupController$BootupReceiver;

.field private mClientObjDataLimit:Ljava/lang/String;

.field private mConferenceFactoryUri:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mDataState:I

.field private mHandler:Landroid/os/Handler;

.field private mHomeDomainName:Ljava/lang/String;

.field private mImContentServerUri:Ljava/lang/String;

.field private mLockOrder:Z

.field private mMaxAdhocGroupSize:I

.field private mMaxNumberOfSubscriptionsInPresenceList:I

.field private mPcscfAddress:Ljava/lang/String;

.field private mPcscfAddressPort:Ljava/lang/String;

.field private mPresenceContentServerUri:Ljava/lang/String;

.field private mPrivateUri:Ljava/lang/String;

.field private mPublicUri:Ljava/lang/String;

.field private mRealm:Ljava/lang/String;

.field private mSourceThrottlePublish:Ljava/lang/String;

.field private mState:Lcom/lge/ims/BootupController$STATE;

.field private mTimerT1:I

.field private mTimerT2:I

.field private mTimerTb:I

.field private mTimerTd:I

.field private mTimerTf:I

.field private mTimerTh:I

.field private mTimerTi:I

.field private mTimerTj:I

.field private mTimerTk:I

.field private mUserName:Ljava/lang/String;

.field private mUserPwd:Ljava/lang/String;

.field private mVideoPort:I

.field private mWifiState:Landroid/net/NetworkInfo$State;

.field private mXCAPRootURI:Ljava/lang/String;

.field private m_bACResult:Z

.field private m_bAgainNotification:Z

.field private m_bRCSUserAccept:Z

.field private m_bSettingAllowRoaming:Z

.field private m_bSettingRCSeON:Z

.field private m_intSameXmlVersion:I

.field private m_strACVersion:Ljava/lang/String;

.field private m_strValidPeriod:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    const/4 v1, 0x0

    #@3
    .line 194
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 89
    sget-object v0, Lcom/lge/ims/BootupController$STATE;->NONE:Lcom/lge/ims/BootupController$STATE;

    #@8
    iput-object v0, p0, Lcom/lge/ims/BootupController;->mState:Lcom/lge/ims/BootupController$STATE;

    #@a
    .line 90
    iput v2, p0, Lcom/lge/ims/BootupController;->mDataState:I

    #@c
    .line 91
    sget-object v0, Landroid/net/NetworkInfo$State;->UNKNOWN:Landroid/net/NetworkInfo$State;

    #@e
    iput-object v0, p0, Lcom/lge/ims/BootupController;->mWifiState:Landroid/net/NetworkInfo$State;

    #@10
    .line 92
    iput-boolean v1, p0, Lcom/lge/ims/BootupController;->bNetConnected:Z

    #@12
    .line 94
    const-string v0, "0"

    #@14
    iput-object v0, p0, Lcom/lge/ims/BootupController;->m_strValidPeriod:Ljava/lang/String;

    #@16
    .line 95
    const-string v0, "0"

    #@18
    iput-object v0, p0, Lcom/lge/ims/BootupController;->m_strACVersion:Ljava/lang/String;

    #@1a
    .line 96
    iput v1, p0, Lcom/lge/ims/BootupController;->m_intSameXmlVersion:I

    #@1c
    .line 97
    iput-boolean v1, p0, Lcom/lge/ims/BootupController;->mLockOrder:Z

    #@1e
    .line 98
    iput-boolean v1, p0, Lcom/lge/ims/BootupController;->m_bACResult:Z

    #@20
    .line 99
    iput-boolean v1, p0, Lcom/lge/ims/BootupController;->m_bSettingAllowRoaming:Z

    #@22
    .line 100
    iput-boolean v1, p0, Lcom/lge/ims/BootupController;->m_bRCSUserAccept:Z

    #@24
    .line 101
    iput-boolean v2, p0, Lcom/lge/ims/BootupController;->m_bSettingRCSeON:Z

    #@26
    .line 102
    iput-boolean v2, p0, Lcom/lge/ims/BootupController;->m_bAgainNotification:Z

    #@28
    .line 104
    iput-object v3, p0, Lcom/lge/ims/BootupController;->mBootupReceiver:Lcom/lge/ims/BootupController$BootupReceiver;

    #@2a
    .line 105
    iput-object v3, p0, Lcom/lge/ims/BootupController;->mContext:Landroid/content/Context;

    #@2c
    .line 107
    new-instance v0, Lcom/lge/ims/BootupController$1;

    #@2e
    invoke-direct {v0, p0}, Lcom/lge/ims/BootupController$1;-><init>(Lcom/lge/ims/BootupController;)V

    #@31
    iput-object v0, p0, Lcom/lge/ims/BootupController;->mHandler:Landroid/os/Handler;

    #@33
    .line 195
    iput-object p1, p0, Lcom/lge/ims/BootupController;->mContext:Landroid/content/Context;

    #@35
    .line 197
    const-string v0, "BootupController"

    #@37
    new-instance v1, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v2, "BootupController / Context ["

    #@3e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v1

    #@42
    iget-object v2, p0, Lcom/lge/ims/BootupController;->mContext:Landroid/content/Context;

    #@44
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v1

    #@48
    const-string v2, "]"

    #@4a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v1

    #@4e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v1

    #@52
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@55
    .line 198
    const-string v0, "BootupController"

    #@57
    new-instance v1, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    const-string v2, "IMSI["

    #@5e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v1

    #@62
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->getSubscriberID()Ljava/lang/String;

    #@65
    move-result-object v2

    #@66
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v1

    #@6a
    const-string v2, "]"

    #@6c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v1

    #@70
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@73
    move-result-object v1

    #@74
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@77
    .line 200
    sget-object v0, Lcom/lge/ims/BootupController$STATE;->WAITING_BOOT_COMPLETE:Lcom/lge/ims/BootupController$STATE;

    #@79
    invoke-direct {p0, v0}, Lcom/lge/ims/BootupController;->setState(Lcom/lge/ims/BootupController$STATE;)V

    #@7c
    .line 201
    const-string v0, "BootupController"

    #@7e
    new-instance v1, Ljava/lang/StringBuilder;

    #@80
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@83
    const-string v2, "BootupController state["

    #@85
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v1

    #@89
    iget-object v2, p0, Lcom/lge/ims/BootupController;->mState:Lcom/lge/ims/BootupController$STATE;

    #@8b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v1

    #@8f
    const-string v2, "]"

    #@91
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v1

    #@95
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@98
    move-result-object v1

    #@99
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@9c
    .line 205
    new-instance v0, Lcom/lge/ims/BootupController$BootupReceiver;

    #@9e
    iget-object v1, p0, Lcom/lge/ims/BootupController;->mContext:Landroid/content/Context;

    #@a0
    invoke-direct {v0, p0, v1}, Lcom/lge/ims/BootupController$BootupReceiver;-><init>(Lcom/lge/ims/BootupController;Landroid/content/Context;)V

    #@a3
    iput-object v0, p0, Lcom/lge/ims/BootupController;->mBootupReceiver:Lcom/lge/ims/BootupController$BootupReceiver;

    #@a5
    .line 206
    return-void
.end method

.method private ForceToStartAC()V
    .registers 3

    #@0
    .prologue
    .line 1075
    const-string v0, "BootupController"

    #@2
    const-string v1, "ForceToStartAC()"

    #@4
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1077
    const/4 v0, 0x1

    #@8
    iget-boolean v1, p0, Lcom/lge/ims/BootupController;->bNetConnected:Z

    #@a
    if-ne v0, v1, :cond_15

    #@c
    .line 1078
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->startAC()V

    #@f
    .line 1079
    sget-object v0, Lcom/lge/ims/BootupController$STATE;->UPDATING_AUTOCONFIG:Lcom/lge/ims/BootupController$STATE;

    #@11
    invoke-direct {p0, v0}, Lcom/lge/ims/BootupController;->setState(Lcom/lge/ims/BootupController$STATE;)V

    #@14
    .line 1084
    :goto_14
    return-void

    #@15
    .line 1081
    :cond_15
    const-string v0, "BootupController"

    #@17
    const-string v1, "Network is not available"

    #@19
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@1c
    .line 1082
    sget-object v0, Lcom/lge/ims/BootupController$STATE;->WAITING_NETWORK_CONNECTED:Lcom/lge/ims/BootupController$STATE;

    #@1e
    invoke-direct {p0, v0}, Lcom/lge/ims/BootupController;->setState(Lcom/lge/ims/BootupController$STATE;)V

    #@21
    goto :goto_14
.end method

.method private StateNone_ACDone()V
    .registers 1

    #@0
    .prologue
    .line 638
    return-void
.end method

.method private StateNone_ACRenew()V
    .registers 3

    #@0
    .prologue
    .line 645
    const-string v0, "BootupController"

    #@2
    const-string v1, "StateNone_ACRenew()"

    #@4
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 647
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->moveAheadToRCSRun()V

    #@a
    .line 648
    return-void
.end method

.method private StateNone_BootupComplete()V
    .registers 1

    #@0
    .prologue
    .line 623
    return-void
.end method

.method private StateNone_RCSeStop()V
    .registers 1

    #@0
    .prologue
    .line 642
    return-void
.end method

.method private StateNone_SettingChanged()V
    .registers 4

    #@0
    .prologue
    .line 626
    const-string v0, "BootupController"

    #@2
    const-string v1, "StateNone_SettingChanged()"

    #@4
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 628
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->checkRCSSetting()Z

    #@a
    move-result v0

    #@b
    if-eqz v0, :cond_1c

    #@d
    .line 629
    sget-object v0, Lcom/lge/ims/BootupController$STATE;->WORKING_RCSE:Lcom/lge/ims/BootupController$STATE;

    #@f
    invoke-direct {p0, v0}, Lcom/lge/ims/BootupController;->setState(Lcom/lge/ims/BootupController$STATE;)V

    #@12
    .line 630
    const v0, 0x8000

    #@15
    const/4 v1, 0x1

    #@16
    const/high16 v2, 0xff

    #@18
    invoke-static {v0, v1, v2}, Lcom/lge/ims/SysMonitor;->sendEventToNative(III)I

    #@1b
    .line 634
    :goto_1b
    return-void

    #@1c
    .line 632
    :cond_1c
    const-string v0, "BootupController"

    #@1e
    const-string v1, "m_bSettingRCSeON is false !!!!"

    #@20
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@23
    goto :goto_1b
.end method

.method private StateUpdatingAC_ACDone()V
    .registers 9

    #@0
    .prologue
    const-wide/16 v6, 0x7d0

    #@2
    const/16 v5, 0x68

    #@4
    const/4 v4, 0x1

    #@5
    const/4 v3, 0x0

    #@6
    .line 692
    const-string v1, "BootupController"

    #@8
    const-string v2, "StateUpdatingAC_ACDone()"

    #@a
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    .line 694
    iget-boolean v1, p0, Lcom/lge/ims/BootupController;->m_bACResult:Z

    #@f
    if-nez v1, :cond_24

    #@11
    .line 695
    const-string v1, "BootupController"

    #@13
    const-string v2, "AC Result is FALSE !!!"

    #@15
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 697
    invoke-direct {p0, v3}, Lcom/lge/ims/BootupController;->sendEventToUpdateConfig(Z)V

    #@1b
    .line 698
    sget-object v1, Lcom/lge/ims/BootupController$STATE;->NONE:Lcom/lge/ims/BootupController$STATE;

    #@1d
    invoke-direct {p0, v1}, Lcom/lge/ims/BootupController;->setState(Lcom/lge/ims/BootupController$STATE;)V

    #@20
    .line 751
    :goto_20
    invoke-direct {p0, v5, v6, v7}, Lcom/lge/ims/BootupController;->sendInternalEvent(IJ)V

    #@23
    .line 752
    return-void

    #@24
    .line 701
    :cond_24
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->readACSettingFromDB()Z

    #@27
    move-result v1

    #@28
    if-nez v1, :cond_31

    #@2a
    .line 702
    const-string v1, "BootupController"

    #@2c
    const-string v2, "readACSettingFromDB FAIL !!!!"

    #@2e
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@31
    .line 706
    :cond_31
    iget-object v1, p0, Lcom/lge/ims/BootupController;->m_strACVersion:Ljava/lang/String;

    #@33
    const-string v2, "-1"

    #@35
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@38
    move-result v1

    #@39
    if-nez v1, :cond_59

    #@3b
    iget-object v1, p0, Lcom/lge/ims/BootupController;->m_strACVersion:Ljava/lang/String;

    #@3d
    const-string v2, "0"

    #@3f
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@42
    move-result v1

    #@43
    if-nez v1, :cond_59

    #@45
    iget-object v1, p0, Lcom/lge/ims/BootupController;->m_strValidPeriod:Ljava/lang/String;

    #@47
    const-string v2, "-1"

    #@49
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4c
    move-result v1

    #@4d
    if-nez v1, :cond_59

    #@4f
    iget-object v1, p0, Lcom/lge/ims/BootupController;->m_strValidPeriod:Ljava/lang/String;

    #@51
    const-string v2, "0"

    #@53
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@56
    move-result v1

    #@57
    if-eqz v1, :cond_6c

    #@59
    .line 710
    :cond_59
    const-string v1, "BootupController"

    #@5b
    const-string v2, "AC Version Or Period is Invalid !!!"

    #@5d
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@60
    .line 712
    sget-object v1, Lcom/lge/ims/BootupController$STATE;->NONE:Lcom/lge/ims/BootupController$STATE;

    #@62
    invoke-direct {p0, v1}, Lcom/lge/ims/BootupController;->setState(Lcom/lge/ims/BootupController$STATE;)V

    #@65
    .line 713
    invoke-direct {p0, v3}, Lcom/lge/ims/BootupController;->sendEventToUpdateConfig(Z)V

    #@68
    .line 714
    invoke-direct {p0, v5, v6, v7}, Lcom/lge/ims/BootupController;->sendInternalEvent(IJ)V

    #@6b
    goto :goto_20

    #@6c
    .line 716
    :cond_6c
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->readProvisioningDataFromDB()Z

    #@6f
    move-result v1

    #@70
    if-nez v1, :cond_79

    #@72
    .line 717
    const-string v1, "BootupController"

    #@74
    const-string v2, "readProvisioningDataFromDB FAIL !!!!"

    #@76
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@79
    .line 720
    :cond_79
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->writeProvisioningDataToImsDB()Z

    #@7c
    move-result v1

    #@7d
    if-nez v1, :cond_86

    #@7f
    .line 721
    const-string v1, "BootupController"

    #@81
    const-string v2, "writeProvisioningDataToImsDB FAIL !!!!"

    #@83
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@86
    .line 724
    :cond_86
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->updateTelURI()V

    #@89
    .line 736
    invoke-direct {p0, v4}, Lcom/lge/ims/BootupController;->sendEventToUpdateConfig(Z)V

    #@8c
    .line 738
    const/4 v0, 0x0

    #@8d
    .line 740
    .local v0, nAvailableSvc:I
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->checkRCSSetting()Z

    #@90
    move-result v1

    #@91
    if-eqz v1, :cond_a1

    #@93
    .line 741
    const/high16 v0, 0xff

    #@95
    .line 742
    sget-object v1, Lcom/lge/ims/BootupController$STATE;->WORKING_RCSE:Lcom/lge/ims/BootupController$STATE;

    #@97
    invoke-direct {p0, v1}, Lcom/lge/ims/BootupController;->setState(Lcom/lge/ims/BootupController$STATE;)V

    #@9a
    .line 747
    :goto_9a
    const v1, 0x8000

    #@9d
    invoke-static {v1, v4, v0}, Lcom/lge/ims/SysMonitor;->sendEventToNative(III)I

    #@a0
    goto :goto_20

    #@a1
    .line 744
    :cond_a1
    const/16 v0, 0x100

    #@a3
    goto :goto_9a
.end method

.method private StateUpdatingAC_ACRenew()V
    .registers 3

    #@0
    .prologue
    .line 762
    const-string v0, "BootupController"

    #@2
    const-string v1, "StateUpdatingAC_ACRenew()"

    #@4
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 765
    return-void
.end method

.method private StateUpdatingAC_BootupComplete()V
    .registers 1

    #@0
    .prologue
    .line 685
    return-void
.end method

.method private StateUpdatingAC_RCSeStop()V
    .registers 3

    #@0
    .prologue
    .line 755
    const-string v0, "BootupController"

    #@2
    const-string v1, "StateUpdatingAC_RCSeStop()"

    #@4
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 758
    sget-object v0, Lcom/lge/ims/BootupController$STATE;->NONE:Lcom/lge/ims/BootupController$STATE;

    #@9
    invoke-direct {p0, v0}, Lcom/lge/ims/BootupController;->setState(Lcom/lge/ims/BootupController$STATE;)V

    #@c
    .line 759
    return-void
.end method

.method private StateUpdatingAC_SettingChanged()V
    .registers 1

    #@0
    .prologue
    .line 689
    return-void
.end method

.method private StateWaitingBootupComplete_ACDone()V
    .registers 1

    #@0
    .prologue
    .line 670
    return-void
.end method

.method private StateWaitingBootupComplete_ACRenew()V
    .registers 1

    #@0
    .prologue
    .line 681
    return-void
.end method

.method private StateWaitingBootupComplete_BootupComplete()V
    .registers 3

    #@0
    .prologue
    .line 657
    const-string v0, "BootupController"

    #@2
    const-string v1, "StateWaitingBootupComplete_BootupComplete()"

    #@4
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 659
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->tempInsertDefaultRcseDB()V

    #@a
    .line 661
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->ForceToStartAC()V

    #@d
    .line 662
    return-void
.end method

.method private StateWaitingBootupComplete_RCSeStop()V
    .registers 3

    #@0
    .prologue
    .line 673
    const-string v0, "BootupController"

    #@2
    const-string v1, "StateWaitingBootupComplete_RCSeStop()"

    #@4
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 676
    sget-object v0, Lcom/lge/ims/BootupController$STATE;->NONE:Lcom/lge/ims/BootupController$STATE;

    #@9
    invoke-direct {p0, v0}, Lcom/lge/ims/BootupController;->setState(Lcom/lge/ims/BootupController$STATE;)V

    #@c
    .line 677
    return-void
.end method

.method private StateWaitingBootupComplete_SettingChanged()V
    .registers 1

    #@0
    .prologue
    .line 666
    return-void
.end method

.method private StateWaitingCleanup_ACDone()V
    .registers 1

    #@0
    .prologue
    .line 849
    return-void
.end method

.method private StateWaitingCleanup_ACRenew()V
    .registers 3

    #@0
    .prologue
    .line 859
    const-string v0, "BootupController"

    #@2
    const-string v1, "StateWaitingCleanup_ACRenew()"

    #@4
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 862
    return-void
.end method

.method private StateWaitingCleanup_BootupCompleted()V
    .registers 1

    #@0
    .prologue
    .line 841
    return-void
.end method

.method private StateWaitingCleanup_RCSeStop()V
    .registers 3

    #@0
    .prologue
    .line 852
    const-string v0, "BootupController"

    #@2
    const-string v1, "StateWaitingCleanup_RCSeStop()"

    #@4
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 855
    sget-object v0, Lcom/lge/ims/BootupController$STATE;->NONE:Lcom/lge/ims/BootupController$STATE;

    #@9
    invoke-direct {p0, v0}, Lcom/lge/ims/BootupController;->setState(Lcom/lge/ims/BootupController$STATE;)V

    #@c
    .line 856
    return-void
.end method

.method private StateWaitingCleanup_SettingChanged()V
    .registers 1

    #@0
    .prologue
    .line 845
    return-void
.end method

.method private StateWaitingNetConnected_BootupComplete()V
    .registers 3

    #@0
    .prologue
    .line 651
    const-string v0, "BootupController"

    #@2
    const-string v1, "StateWaitingNetConnected_BootupComplete()"

    #@4
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 653
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->ForceToStartAC()V

    #@a
    .line 654
    return-void
.end method

.method private StateWaitingUserAction_ACDone()V
    .registers 1

    #@0
    .prologue
    .line 824
    return-void
.end method

.method private StateWaitingUserAction_ACRenew()V
    .registers 3

    #@0
    .prologue
    .line 834
    const-string v0, "BootupController"

    #@2
    const-string v1, "StateWaitingUserAction_ACRenew()"

    #@4
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 837
    return-void
.end method

.method private StateWaitingUserAction_BootupComplete()V
    .registers 1

    #@0
    .prologue
    .line 808
    return-void
.end method

.method private StateWaitingUserAction_RCSeStop()V
    .registers 3

    #@0
    .prologue
    .line 827
    const-string v0, "BootupController"

    #@2
    const-string v1, "StateWaitingUserAction_RCSeStop()"

    #@4
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 830
    sget-object v0, Lcom/lge/ims/BootupController$STATE;->NONE:Lcom/lge/ims/BootupController$STATE;

    #@9
    invoke-direct {p0, v0}, Lcom/lge/ims/BootupController;->setState(Lcom/lge/ims/BootupController$STATE;)V

    #@c
    .line 831
    return-void
.end method

.method private StateWaitingUserAction_SettingChanged()V
    .registers 4

    #@0
    .prologue
    .line 811
    const-string v0, "BootupController"

    #@2
    const-string v1, "StateWaitingUserAction_SettingChanged()"

    #@4
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 813
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->checkRCSSetting()Z

    #@a
    move-result v0

    #@b
    if-eqz v0, :cond_23

    #@d
    .line 814
    sget-object v0, Lcom/lge/ims/BootupController$STATE;->WORKING_RCSE:Lcom/lge/ims/BootupController$STATE;

    #@f
    invoke-direct {p0, v0}, Lcom/lge/ims/BootupController;->setState(Lcom/lge/ims/BootupController$STATE;)V

    #@12
    .line 815
    const v0, 0x8000

    #@15
    const/4 v1, 0x1

    #@16
    const/high16 v2, 0xff

    #@18
    invoke-static {v0, v1, v2}, Lcom/lge/ims/SysMonitor;->sendEventToNative(III)I

    #@1b
    .line 816
    const/16 v0, 0x68

    #@1d
    const-wide/16 v1, 0x7d0

    #@1f
    invoke-direct {p0, v0, v1, v2}, Lcom/lge/ims/BootupController;->sendInternalEvent(IJ)V

    #@22
    .line 820
    :goto_22
    return-void

    #@23
    .line 818
    :cond_23
    const-string v0, "BootupController"

    #@25
    const-string v1, "m_bRCSUserAceept is false !!!!"

    #@27
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@2a
    goto :goto_22
.end method

.method private StateWorkingRCSe_ACDone()V
    .registers 1

    #@0
    .prologue
    .line 784
    return-void
.end method

.method private StateWorkingRCSe_ACRenew()V
    .registers 3

    #@0
    .prologue
    .line 801
    const-string v0, "BootupController"

    #@2
    const-string v1, "StateWorkingRCSe_ACRenew()"

    #@4
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 804
    return-void
.end method

.method private StateWorkingRCSe_BootupComplete()V
    .registers 1

    #@0
    .prologue
    .line 769
    return-void
.end method

.method private StateWorkingRCSe_RCSeStop()V
    .registers 4

    #@0
    .prologue
    .line 787
    const-string v0, "BootupController"

    #@2
    const-string v1, "StateWorkingRCSe_RCSeStop()"

    #@4
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 789
    iget-boolean v0, p0, Lcom/lge/ims/BootupController;->bNetConnected:Z

    #@9
    if-eqz v0, :cond_1f

    #@b
    .line 790
    const v0, 0x8000

    #@e
    const/4 v1, 0x0

    #@f
    const/high16 v2, 0xff

    #@11
    invoke-static {v0, v1, v2}, Lcom/lge/ims/SysMonitor;->sendEventToNative(III)I

    #@14
    .line 792
    sget-object v0, Lcom/lge/ims/BootupController$STATE;->NONE:Lcom/lge/ims/BootupController$STATE;

    #@16
    invoke-direct {p0, v0}, Lcom/lge/ims/BootupController;->setState(Lcom/lge/ims/BootupController$STATE;)V

    #@19
    .line 793
    const/16 v0, 0x6c

    #@1b
    invoke-direct {p0, v0}, Lcom/lge/ims/BootupController;->sendInternalEvent(I)V

    #@1e
    .line 798
    :goto_1e
    return-void

    #@1f
    .line 796
    :cond_1f
    sget-object v0, Lcom/lge/ims/BootupController$STATE;->NONE:Lcom/lge/ims/BootupController$STATE;

    #@21
    invoke-direct {p0, v0}, Lcom/lge/ims/BootupController;->setState(Lcom/lge/ims/BootupController$STATE;)V

    #@24
    goto :goto_1e
.end method

.method private StateWorkingRCSe_SettingChanged()V
    .registers 4

    #@0
    .prologue
    .line 772
    const-string v0, "BootupController"

    #@2
    const-string v1, "StateWorkingRCSe_SettingChanged()"

    #@4
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 774
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->checkRCSSetting()Z

    #@a
    move-result v0

    #@b
    if-nez v0, :cond_1c

    #@d
    .line 775
    sget-object v0, Lcom/lge/ims/BootupController$STATE;->NONE:Lcom/lge/ims/BootupController$STATE;

    #@f
    invoke-direct {p0, v0}, Lcom/lge/ims/BootupController;->setState(Lcom/lge/ims/BootupController$STATE;)V

    #@12
    .line 776
    const v0, 0x8000

    #@15
    const/4 v1, 0x0

    #@16
    const/high16 v2, 0xff

    #@18
    invoke-static {v0, v1, v2}, Lcom/lge/ims/SysMonitor;->sendEventToNative(III)I

    #@1b
    .line 780
    :goto_1b
    return-void

    #@1c
    .line 778
    :cond_1c
    const-string v0, "BootupController"

    #@1e
    const-string v1, "m_bSettingRCSeON is true !!!!"

    #@20
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@23
    goto :goto_1b
.end method

.method static synthetic access$000(Lcom/lge/ims/BootupController;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 33
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->process_bootComplete()V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/lge/ims/BootupController;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 33
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->process_networkConnectionChange()V

    #@3
    return-void
.end method

.method static synthetic access$1000(Lcom/lge/ims/BootupController;)Landroid/net/NetworkInfo$State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 33
    iget-object v0, p0, Lcom/lge/ims/BootupController;->mWifiState:Landroid/net/NetworkInfo$State;

    #@2
    return-object v0
.end method

.method static synthetic access$1002(Lcom/lge/ims/BootupController;Landroid/net/NetworkInfo$State;)Landroid/net/NetworkInfo$State;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 33
    iput-object p1, p0, Lcom/lge/ims/BootupController;->mWifiState:Landroid/net/NetworkInfo$State;

    #@2
    return-object p1
.end method

.method static synthetic access$1100(Lcom/lge/ims/BootupController;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/lge/ims/BootupController;->m_bACResult:Z

    #@2
    return v0
.end method

.method static synthetic access$1102(Lcom/lge/ims/BootupController;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/lge/ims/BootupController;->m_bACResult:Z

    #@2
    return p1
.end method

.method static synthetic access$1200(Lcom/lge/ims/BootupController;IJ)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Lcom/lge/ims/BootupController;->sendInternalEvent(IJ)V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/lge/ims/BootupController;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 33
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->process_acDone()V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/lge/ims/BootupController;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 33
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->process_aosStart()V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/lge/ims/BootupController;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 33
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->process_settingChanged()V

    #@3
    return-void
.end method

.method static synthetic access$500(Lcom/lge/ims/BootupController;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 33
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->process_dbUpdateCompleted()V

    #@3
    return-void
.end method

.method static synthetic access$600(Lcom/lge/ims/BootupController;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 33
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->process_RCSeStop()V

    #@3
    return-void
.end method

.method static synthetic access$700(Lcom/lge/ims/BootupController;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 33
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->process_ACRenew()V

    #@3
    return-void
.end method

.method static synthetic access$800(Lcom/lge/ims/BootupController;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 33
    iget v0, p0, Lcom/lge/ims/BootupController;->mDataState:I

    #@2
    return v0
.end method

.method static synthetic access$802(Lcom/lge/ims/BootupController;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 33
    iput p1, p0, Lcom/lge/ims/BootupController;->mDataState:I

    #@2
    return p1
.end method

.method static synthetic access$900(Lcom/lge/ims/BootupController;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/lge/ims/BootupController;->sendInternalEvent(I)V

    #@3
    return-void
.end method

.method private checkACSetting()Z
    .registers 3

    #@0
    .prologue
    .line 978
    const-string v0, "BootupController"

    #@2
    const-string v1, "checkACSetting()"

    #@4
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 980
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->readACSettingFromDB()Z

    #@a
    move-result v0

    #@b
    if-nez v0, :cond_14

    #@d
    .line 981
    const-string v0, "BootupController"

    #@f
    const-string v1, "readACSettingFromDB FAIL !!!!"

    #@11
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@14
    .line 985
    :cond_14
    iget-object v0, p0, Lcom/lge/ims/BootupController;->m_strACVersion:Ljava/lang/String;

    #@16
    const-string v1, "-1"

    #@18
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b
    move-result v0

    #@1c
    if-nez v0, :cond_3c

    #@1e
    iget-object v0, p0, Lcom/lge/ims/BootupController;->m_strACVersion:Ljava/lang/String;

    #@20
    const-string v1, "0"

    #@22
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25
    move-result v0

    #@26
    if-nez v0, :cond_3c

    #@28
    iget-object v0, p0, Lcom/lge/ims/BootupController;->m_strValidPeriod:Ljava/lang/String;

    #@2a
    const-string v1, "-1"

    #@2c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2f
    move-result v0

    #@30
    if-nez v0, :cond_3c

    #@32
    iget-object v0, p0, Lcom/lge/ims/BootupController;->m_strValidPeriod:Ljava/lang/String;

    #@34
    const-string v1, "0"

    #@36
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@39
    move-result v0

    #@3a
    if-eqz v0, :cond_45

    #@3c
    .line 989
    :cond_3c
    const-string v0, "BootupController"

    #@3e
    const-string v1, "AC Version Or Period is Invalid !!!"

    #@40
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@43
    .line 991
    const/4 v0, 0x0

    #@44
    .line 994
    :goto_44
    return v0

    #@45
    :cond_45
    const/4 v0, 0x1

    #@46
    goto :goto_44
.end method

.method private checkRCSSetting()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 998
    const-string v1, "BootupController"

    #@3
    const-string v2, "checkRCSSetting()"

    #@5
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 1000
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->readRCSSettingFromDB()Z

    #@b
    move-result v1

    #@c
    if-nez v1, :cond_15

    #@e
    .line 1001
    const-string v1, "BootupController"

    #@10
    const-string v2, "readRCSSettingFromDB FAIL !!!!"

    #@12
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@15
    .line 1004
    :cond_15
    iget-boolean v1, p0, Lcom/lge/ims/BootupController;->m_bSettingRCSeON:Z

    #@17
    if-eq v1, v0, :cond_1d

    #@19
    iget-boolean v1, p0, Lcom/lge/ims/BootupController;->m_bRCSUserAccept:Z

    #@1b
    if-ne v0, v1, :cond_1e

    #@1d
    .line 1014
    :cond_1d
    :goto_1d
    return v0

    #@1e
    .line 1007
    :cond_1e
    iget-boolean v1, p0, Lcom/lge/ims/BootupController;->m_bAgainNotification:Z

    #@20
    if-ne v0, v1, :cond_30

    #@22
    iget-boolean v0, p0, Lcom/lge/ims/BootupController;->m_bRCSUserAccept:Z

    #@24
    if-nez v0, :cond_30

    #@26
    .line 1008
    sget-object v0, Lcom/lge/ims/BootupController$STATE;->WAITING_USER_ACTION:Lcom/lge/ims/BootupController$STATE;

    #@28
    invoke-direct {p0, v0}, Lcom/lge/ims/BootupController;->setState(Lcom/lge/ims/BootupController$STATE;)V

    #@2b
    .line 1009
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->startStarter()V

    #@2e
    .line 1014
    :goto_2e
    const/4 v0, 0x0

    #@2f
    goto :goto_1d

    #@30
    .line 1011
    :cond_30
    sget-object v0, Lcom/lge/ims/BootupController$STATE;->NONE:Lcom/lge/ims/BootupController$STATE;

    #@32
    invoke-direct {p0, v0}, Lcom/lge/ims/BootupController;->setState(Lcom/lge/ims/BootupController$STATE;)V

    #@35
    goto :goto_2e
.end method

.method private getAutoCONFIG()Lcom/lge/ims/BootupController$STATE_CONFIG;
    .registers 13

    #@0
    .prologue
    const-wide/16 v10, 0x3e8

    #@2
    .line 1039
    const-string v6, "BootupController"

    #@4
    new-instance v7, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v8, "Current AC Version: "

    #@b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v7

    #@f
    iget-object v8, p0, Lcom/lge/ims/BootupController;->m_strACVersion:Ljava/lang/String;

    #@11
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v7

    #@15
    const-string v8, " / Validity: "

    #@17
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v7

    #@1b
    iget-object v8, p0, Lcom/lge/ims/BootupController;->m_strValidPeriod:Ljava/lang/String;

    #@1d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v7

    #@21
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v7

    #@25
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@28
    .line 1042
    iget-object v6, p0, Lcom/lge/ims/BootupController;->m_strACVersion:Ljava/lang/String;

    #@2a
    const-string v7, "-1"

    #@2c
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2f
    move-result v6

    #@30
    if-nez v6, :cond_3c

    #@32
    iget-object v6, p0, Lcom/lge/ims/BootupController;->m_strValidPeriod:Ljava/lang/String;

    #@34
    const-string v7, "-1"

    #@36
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@39
    move-result v6

    #@3a
    if-eqz v6, :cond_3f

    #@3c
    .line 1043
    :cond_3c
    sget-object v6, Lcom/lge/ims/BootupController$STATE_CONFIG;->INVALID:Lcom/lge/ims/BootupController$STATE_CONFIG;

    #@3e
    .line 1071
    :goto_3e
    return-object v6

    #@3f
    .line 1047
    :cond_3f
    iget-object v6, p0, Lcom/lge/ims/BootupController;->m_strACVersion:Ljava/lang/String;

    #@41
    const-string v7, "0"

    #@43
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@46
    move-result v6

    #@47
    if-eqz v6, :cond_4c

    #@49
    .line 1048
    sget-object v6, Lcom/lge/ims/BootupController$STATE_CONFIG;->DIRTY:Lcom/lge/ims/BootupController$STATE_CONFIG;

    #@4b
    goto :goto_3e

    #@4c
    .line 1052
    :cond_4c
    iget-object v6, p0, Lcom/lge/ims/BootupController;->m_strValidPeriod:Ljava/lang/String;

    #@4e
    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@51
    move-result-wide v6

    #@52
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@55
    move-result-object v1

    #@56
    .line 1053
    .local v1, nExpired:Ljava/lang/Long;
    new-instance v2, Ljava/text/SimpleDateFormat;

    #@58
    const-string v6, "yyyy-MM-dd HH:mm:ss"

    #@5a
    invoke-direct {v2, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    #@5d
    .line 1054
    .local v2, sdfExp:Ljava/text/SimpleDateFormat;
    new-instance v6, Ljava/util/Date;

    #@5f
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    #@62
    move-result-wide v7

    #@63
    mul-long/2addr v7, v10

    #@64
    invoke-direct {v6, v7, v8}, Ljava/util/Date;-><init>(J)V

    #@67
    invoke-virtual {v2, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    #@6a
    move-result-object v4

    #@6b
    .line 1056
    .local v4, strExpired:Ljava/lang/String;
    const-string v6, "BootupController"

    #@6d
    new-instance v7, Ljava/lang/StringBuilder;

    #@6f
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@72
    const-string v8, "nExpired: "

    #@74
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v7

    #@78
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v7

    #@7c
    const-string v8, ": "

    #@7e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v7

    #@82
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v7

    #@86
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@89
    move-result-object v7

    #@8a
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@8d
    .line 1058
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    #@90
    move-result-wide v6

    #@91
    const-wide/16 v8, 0x0

    #@93
    cmp-long v6, v6, v8

    #@95
    if-lez v6, :cond_ee

    #@97
    .line 1059
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@9a
    move-result-wide v6

    #@9b
    div-long/2addr v6, v10

    #@9c
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@9f
    move-result-object v0

    #@a0
    .line 1060
    .local v0, nCur:Ljava/lang/Long;
    new-instance v3, Ljava/text/SimpleDateFormat;

    #@a2
    const-string v6, "yyyy-MM-dd HH:mm:ss"

    #@a4
    invoke-direct {v3, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    #@a7
    .line 1061
    .local v3, sdfNow:Ljava/text/SimpleDateFormat;
    new-instance v6, Ljava/util/Date;

    #@a9
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    #@ac
    move-result-wide v7

    #@ad
    mul-long/2addr v7, v10

    #@ae
    invoke-direct {v6, v7, v8}, Ljava/util/Date;-><init>(J)V

    #@b1
    invoke-virtual {v3, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    #@b4
    move-result-object v5

    #@b5
    .line 1063
    .local v5, strNow:Ljava/lang/String;
    const-string v6, "BootupController"

    #@b7
    new-instance v7, Ljava/lang/StringBuilder;

    #@b9
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@bc
    const-string v8, "nCur: "

    #@be
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v7

    #@c2
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v7

    #@c6
    const-string v8, ": "

    #@c8
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cb
    move-result-object v7

    #@cc
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v7

    #@d0
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d3
    move-result-object v7

    #@d4
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@d7
    .line 1065
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    #@da
    move-result-wide v6

    #@db
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    #@de
    move-result-wide v8

    #@df
    cmp-long v6, v6, v8

    #@e1
    if-gtz v6, :cond_ee

    #@e3
    .line 1066
    const-string v6, "BootupController"

    #@e5
    const-string v7, "Cur < Expired  ---- vaild"

    #@e7
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@ea
    .line 1067
    sget-object v6, Lcom/lge/ims/BootupController$STATE_CONFIG;->VALID:Lcom/lge/ims/BootupController$STATE_CONFIG;

    #@ec
    goto/16 :goto_3e

    #@ee
    .line 1071
    .end local v0           #nCur:Ljava/lang/Long;
    .end local v3           #sdfNow:Ljava/text/SimpleDateFormat;
    .end local v5           #strNow:Ljava/lang/String;
    :cond_ee
    sget-object v6, Lcom/lge/ims/BootupController$STATE_CONFIG;->DIRTY:Lcom/lge/ims/BootupController$STATE_CONFIG;

    #@f0
    goto/16 :goto_3e
.end method

.method private getSubscriberID()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 1019
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getTelephonyManager()Landroid/telephony/TelephonyManager;

    #@3
    move-result-object v0

    #@4
    .line 1021
    .local v0, mTelephonyMngr:Landroid/telephony/TelephonyManager;
    if-nez v0, :cond_10

    #@6
    .line 1022
    const-string v2, "BootupController"

    #@8
    const-string v3, "TelephonyManager interface is null."

    #@a
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    .line 1023
    const-string v1, "450000000000000"

    #@f
    .line 1034
    :goto_f
    return-object v1

    #@10
    .line 1026
    :cond_10
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    .line 1028
    .local v1, strIMSI:Ljava/lang/String;
    if-eqz v1, :cond_1d

    #@16
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    #@19
    move-result v2

    #@1a
    const/4 v3, 0x1

    #@1b
    if-ne v2, v3, :cond_20

    #@1d
    .line 1029
    :cond_1d
    const-string v1, "450000000000000"

    #@1f
    goto :goto_f

    #@20
    .line 1031
    :cond_20
    const-string v2, ":"

    #@22
    const-string v3, "0"

    #@24
    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    goto :goto_f
.end method

.method private isLockOrder()Z
    .registers 2

    #@0
    .prologue
    .line 937
    iget-boolean v0, p0, Lcom/lge/ims/BootupController;->mLockOrder:Z

    #@2
    return v0
.end method

.method private isMobileAvailable()Z
    .registers 2

    #@0
    .prologue
    .line 890
    iget v0, p0, Lcom/lge/ims/BootupController;->mDataState:I

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method private isNetworkAvailable()Z
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 898
    const/4 v0, 0x0

    #@3
    .line 900
    .local v0, bRet:Z
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getConnectivityManager()Landroid/net/ConnectivityManager;

    #@6
    move-result-object v2

    #@7
    .line 902
    .local v2, mConnMgr:Landroid/net/ConnectivityManager;
    if-nez v2, :cond_b

    #@9
    move v1, v0

    #@a
    .line 928
    .end local v0           #bRet:Z
    .local v1, bRet:I
    :goto_a
    return v1

    #@b
    .line 906
    .end local v1           #bRet:I
    .restart local v0       #bRet:Z
    :cond_b
    invoke-virtual {v2, v6}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@e
    move-result-object v3

    #@f
    .line 908
    .local v3, netInfo:Landroid/net/NetworkInfo;
    if-nez v3, :cond_1a

    #@11
    .line 909
    const-string v4, "BootupController"

    #@13
    const-string v5, "network info is null"

    #@15
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    move v1, v0

    #@19
    .line 910
    .restart local v1       #bRet:I
    goto :goto_a

    #@1a
    .line 912
    .end local v1           #bRet:I
    :cond_1a
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z

    #@1d
    move-result v4

    #@1e
    if-ne v4, v7, :cond_28

    #@20
    .line 913
    const/4 v0, 0x1

    #@21
    .line 914
    const-string v4, "BootupController"

    #@23
    const-string v5, "Data is connected"

    #@25
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@28
    .line 918
    :cond_28
    if-eqz v0, :cond_2e

    #@2a
    .line 919
    iput v6, p0, Lcom/lge/ims/BootupController;->mDataState:I

    #@2c
    :cond_2c
    :goto_2c
    move v1, v0

    #@2d
    .line 928
    .restart local v1       #bRet:I
    goto :goto_a

    #@2e
    .line 921
    .end local v1           #bRet:I
    :cond_2e
    iput v7, p0, Lcom/lge/ims/BootupController;->mDataState:I

    #@30
    .line 923
    iget-object v4, p0, Lcom/lge/ims/BootupController;->mWifiState:Landroid/net/NetworkInfo$State;

    #@32
    sget-object v5, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    #@34
    if-ne v4, v5, :cond_2c

    #@36
    .line 924
    const/4 v0, 0x1

    #@37
    goto :goto_2c
.end method

.method private isNetworkRoaming()Z
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 942
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getConnectivityManager()Landroid/net/ConnectivityManager;

    #@4
    move-result-object v0

    #@5
    .line 944
    .local v0, mConnMgr:Landroid/net/ConnectivityManager;
    if-nez v0, :cond_f

    #@7
    .line 945
    const-string v3, "BootupController"

    #@9
    const-string v4, "isNetworkRoaming(). connection mngr is null."

    #@b
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@e
    .line 961
    :goto_e
    return v2

    #@f
    .line 949
    :cond_f
    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@12
    move-result-object v1

    #@13
    .line 951
    .local v1, objNetInfo:Landroid/net/NetworkInfo;
    if-nez v1, :cond_23

    #@15
    .line 952
    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@18
    move-result-object v1

    #@19
    .line 954
    if-nez v1, :cond_23

    #@1b
    .line 955
    const-string v3, "BootupController"

    #@1d
    const-string v4, "Cannot decide Roaming or Not"

    #@1f
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@22
    goto :goto_e

    #@23
    .line 960
    :cond_23
    const-string v2, "BootupController"

    #@25
    new-instance v3, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v4, "Rroaming: "

    #@2c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v3

    #@30
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isRoaming()Z

    #@33
    move-result v4

    #@34
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@37
    move-result-object v3

    #@38
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v3

    #@3c
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@3f
    .line 961
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isRoaming()Z

    #@42
    move-result v2

    #@43
    goto :goto_e
.end method

.method private isSIMAbsent()Z
    .registers 7

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 965
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getTelephonyManager()Landroid/telephony/TelephonyManager;

    #@5
    move-result-object v0

    #@6
    .line 967
    .local v0, mTelephonyManager:Landroid/telephony/TelephonyManager;
    if-nez v0, :cond_10

    #@8
    .line 968
    const-string v1, "BootupController"

    #@a
    const-string v2, "isSIMAbsent(). Telephony manager obj is null."

    #@c
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@f
    .line 974
    :goto_f
    return v3

    #@10
    .line 972
    :cond_10
    const-string v4, "BootupController"

    #@12
    new-instance v1, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v5, "isSIMAbsent(): "

    #@19
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v5

    #@1d
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    #@20
    move-result v1

    #@21
    if-ne v1, v2, :cond_37

    #@23
    move v1, v2

    #@24
    :goto_24
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@27
    move-result-object v1

    #@28
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v1

    #@2c
    invoke-static {v4, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@2f
    .line 974
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    #@32
    move-result v1

    #@33
    if-ne v1, v2, :cond_39

    #@35
    :goto_35
    move v3, v2

    #@36
    goto :goto_f

    #@37
    :cond_37
    move v1, v3

    #@38
    .line 972
    goto :goto_24

    #@39
    :cond_39
    move v2, v3

    #@3a
    .line 974
    goto :goto_35
.end method

.method private isValidVersion()Z
    .registers 3

    #@0
    .prologue
    .line 932
    iget-object v0, p0, Lcom/lge/ims/BootupController;->m_strACVersion:Ljava/lang/String;

    #@2
    const-string v1, "-1"

    #@4
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_16

    #@a
    iget-object v0, p0, Lcom/lge/ims/BootupController;->m_strValidPeriod:Ljava/lang/String;

    #@c
    const-string v1, "-1"

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@11
    move-result v0

    #@12
    if-nez v0, :cond_16

    #@14
    const/4 v0, 0x1

    #@15
    :goto_15
    return v0

    #@16
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_15
.end method

.method private isWifiAvailable()Z
    .registers 3

    #@0
    .prologue
    .line 894
    iget-object v0, p0, Lcom/lge/ims/BootupController;->mWifiState:Landroid/net/NetworkInfo$State;

    #@2
    sget-object v1, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    #@4
    if-ne v0, v1, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method private moveAheadToRCSRun()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 1087
    const-string v1, "BootupController"

    #@3
    const-string v2, "moveAheadToRCSRun()"

    #@5
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 1089
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->getAutoCONFIG()Lcom/lge/ims/BootupController$STATE_CONFIG;

    #@b
    move-result-object v0

    #@c
    .line 1090
    .local v0, eConfig:Lcom/lge/ims/BootupController$STATE_CONFIG;
    const-string v1, "BootupController"

    #@e
    new-instance v2, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v3, "AC Setting : "

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@24
    .line 1092
    sget-object v1, Lcom/lge/ims/BootupController$2;->$SwitchMap$com$lge$ims$BootupController$STATE_CONFIG:[I

    #@26
    invoke-virtual {v0}, Lcom/lge/ims/BootupController$STATE_CONFIG;->ordinal()I

    #@29
    move-result v2

    #@2a
    aget v1, v1, v2

    #@2c
    packed-switch v1, :pswitch_data_60

    #@2f
    .line 1133
    :goto_2f
    return-void

    #@30
    .line 1095
    :pswitch_30
    sget-object v1, Lcom/lge/ims/BootupController$STATE;->NONE:Lcom/lge/ims/BootupController$STATE;

    #@32
    invoke-direct {p0, v1}, Lcom/lge/ims/BootupController;->setState(Lcom/lge/ims/BootupController$STATE;)V

    #@35
    .line 1096
    const v1, 0x8000

    #@38
    const/high16 v2, 0xff

    #@3a
    invoke-static {v1, v4, v2}, Lcom/lge/ims/SysMonitor;->sendEventToNative(III)I

    #@3d
    .line 1097
    const/16 v1, 0x68

    #@3f
    const-wide/16 v2, 0x7d0

    #@41
    invoke-direct {p0, v1, v2, v3}, Lcom/lge/ims/BootupController;->sendInternalEvent(IJ)V

    #@44
    goto :goto_2f

    #@45
    .line 1103
    :pswitch_45
    iget-boolean v1, p0, Lcom/lge/ims/BootupController;->bNetConnected:Z

    #@47
    if-ne v1, v4, :cond_52

    #@49
    .line 1104
    sget-object v1, Lcom/lge/ims/BootupController$STATE;->UPDATING_AUTOCONFIG:Lcom/lge/ims/BootupController$STATE;

    #@4b
    invoke-direct {p0, v1}, Lcom/lge/ims/BootupController;->setState(Lcom/lge/ims/BootupController$STATE;)V

    #@4e
    .line 1105
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->startAC()V

    #@51
    goto :goto_2f

    #@52
    .line 1107
    :cond_52
    const-string v1, "BootupController"

    #@54
    const-string v2, "Network is Roaming.. "

    #@56
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@59
    .line 1108
    sget-object v1, Lcom/lge/ims/BootupController$STATE;->NONE:Lcom/lge/ims/BootupController$STATE;

    #@5b
    invoke-direct {p0, v1}, Lcom/lge/ims/BootupController;->setState(Lcom/lge/ims/BootupController$STATE;)V

    #@5e
    goto :goto_2f

    #@5f
    .line 1092
    nop

    #@60
    :pswitch_data_60
    .packed-switch 0x1
        :pswitch_30
        :pswitch_45
        :pswitch_45
    .end packed-switch
.end method

.method private process_ACRenew()V
    .registers 4

    #@0
    .prologue
    .line 579
    const-string v0, "BootupController"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "process_ACRenew(). old state["

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-object v2, p0, Lcom/lge/ims/BootupController;->mState:Lcom/lge/ims/BootupController$STATE;

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    const-string v2, "]"

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@20
    .line 581
    sget-object v0, Lcom/lge/ims/BootupController$2;->$SwitchMap$com$lge$ims$BootupController$STATE:[I

    #@22
    iget-object v1, p0, Lcom/lge/ims/BootupController;->mState:Lcom/lge/ims/BootupController$STATE;

    #@24
    invoke-virtual {v1}, Lcom/lge/ims/BootupController$STATE;->ordinal()I

    #@27
    move-result v1

    #@28
    aget v0, v0, v1

    #@2a
    packed-switch v0, :pswitch_data_60

    #@2d
    .line 614
    :pswitch_2d
    const-string v0, "BootupController"

    #@2f
    new-instance v1, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v2, "process_settingChanged() invalid State: "

    #@36
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v1

    #@3a
    iget-object v2, p0, Lcom/lge/ims/BootupController;->mState:Lcom/lge/ims/BootupController$STATE;

    #@3c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v1

    #@40
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v1

    #@44
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@47
    .line 618
    :goto_47
    return-void

    #@48
    .line 584
    :pswitch_48
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->StateNone_ACRenew()V

    #@4b
    goto :goto_47

    #@4c
    .line 589
    :pswitch_4c
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->StateWaitingBootupComplete_ACRenew()V

    #@4f
    goto :goto_47

    #@50
    .line 594
    :pswitch_50
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->StateUpdatingAC_ACRenew()V

    #@53
    goto :goto_47

    #@54
    .line 599
    :pswitch_54
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->StateWorkingRCSe_ACRenew()V

    #@57
    goto :goto_47

    #@58
    .line 604
    :pswitch_58
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->StateWaitingUserAction_ACRenew()V

    #@5b
    goto :goto_47

    #@5c
    .line 609
    :pswitch_5c
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->StateWaitingCleanup_ACRenew()V

    #@5f
    goto :goto_47

    #@60
    .line 581
    :pswitch_data_60
    .packed-switch 0x1
        :pswitch_48
        :pswitch_2d
        :pswitch_4c
        :pswitch_50
        :pswitch_54
        :pswitch_58
        :pswitch_5c
    .end packed-switch
.end method

.method private process_RCSeStop()V
    .registers 4

    #@0
    .prologue
    .line 536
    const-string v0, "BootupController"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "process_RCSeStop(). old state["

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-object v2, p0, Lcom/lge/ims/BootupController;->mState:Lcom/lge/ims/BootupController$STATE;

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    const-string v2, "]"

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@20
    .line 538
    sget-object v0, Lcom/lge/ims/BootupController$2;->$SwitchMap$com$lge$ims$BootupController$STATE:[I

    #@22
    iget-object v1, p0, Lcom/lge/ims/BootupController;->mState:Lcom/lge/ims/BootupController$STATE;

    #@24
    invoke-virtual {v1}, Lcom/lge/ims/BootupController$STATE;->ordinal()I

    #@27
    move-result v1

    #@28
    aget v0, v0, v1

    #@2a
    packed-switch v0, :pswitch_data_60

    #@2d
    .line 571
    :pswitch_2d
    const-string v0, "BootupController"

    #@2f
    new-instance v1, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v2, "process_settingChanged() invalid State: "

    #@36
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v1

    #@3a
    iget-object v2, p0, Lcom/lge/ims/BootupController;->mState:Lcom/lge/ims/BootupController$STATE;

    #@3c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v1

    #@40
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v1

    #@44
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@47
    .line 576
    :goto_47
    return-void

    #@48
    .line 541
    :pswitch_48
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->StateNone_RCSeStop()V

    #@4b
    goto :goto_47

    #@4c
    .line 546
    :pswitch_4c
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->StateWaitingBootupComplete_RCSeStop()V

    #@4f
    goto :goto_47

    #@50
    .line 551
    :pswitch_50
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->StateUpdatingAC_RCSeStop()V

    #@53
    goto :goto_47

    #@54
    .line 556
    :pswitch_54
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->StateWorkingRCSe_RCSeStop()V

    #@57
    goto :goto_47

    #@58
    .line 561
    :pswitch_58
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->StateWaitingUserAction_RCSeStop()V

    #@5b
    goto :goto_47

    #@5c
    .line 566
    :pswitch_5c
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->StateWaitingCleanup_RCSeStop()V

    #@5f
    goto :goto_47

    #@60
    .line 538
    :pswitch_data_60
    .packed-switch 0x1
        :pswitch_48
        :pswitch_2d
        :pswitch_4c
        :pswitch_50
        :pswitch_54
        :pswitch_58
        :pswitch_5c
    .end packed-switch
.end method

.method private process_acDone()V
    .registers 4

    #@0
    .prologue
    .line 434
    const-string v0, "BootupController"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "process_acDone(). old state["

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-object v2, p0, Lcom/lge/ims/BootupController;->mState:Lcom/lge/ims/BootupController$STATE;

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    const-string v2, "]"

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@20
    .line 436
    sget-object v0, Lcom/lge/ims/BootupController$2;->$SwitchMap$com$lge$ims$BootupController$STATE:[I

    #@22
    iget-object v1, p0, Lcom/lge/ims/BootupController;->mState:Lcom/lge/ims/BootupController$STATE;

    #@24
    invoke-virtual {v1}, Lcom/lge/ims/BootupController$STATE;->ordinal()I

    #@27
    move-result v1

    #@28
    aget v0, v0, v1

    #@2a
    packed-switch v0, :pswitch_data_60

    #@2d
    .line 469
    :pswitch_2d
    const-string v0, "BootupController"

    #@2f
    new-instance v1, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v2, "process_acDone() invalid State: "

    #@36
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v1

    #@3a
    iget-object v2, p0, Lcom/lge/ims/BootupController;->mState:Lcom/lge/ims/BootupController$STATE;

    #@3c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v1

    #@40
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v1

    #@44
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@47
    .line 473
    :goto_47
    return-void

    #@48
    .line 439
    :pswitch_48
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->StateNone_ACDone()V

    #@4b
    goto :goto_47

    #@4c
    .line 444
    :pswitch_4c
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->StateWaitingBootupComplete_ACDone()V

    #@4f
    goto :goto_47

    #@50
    .line 449
    :pswitch_50
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->StateUpdatingAC_ACDone()V

    #@53
    goto :goto_47

    #@54
    .line 454
    :pswitch_54
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->StateWorkingRCSe_ACDone()V

    #@57
    goto :goto_47

    #@58
    .line 459
    :pswitch_58
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->StateWaitingUserAction_ACDone()V

    #@5b
    goto :goto_47

    #@5c
    .line 464
    :pswitch_5c
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->StateWaitingCleanup_ACDone()V

    #@5f
    goto :goto_47

    #@60
    .line 436
    :pswitch_data_60
    .packed-switch 0x1
        :pswitch_48
        :pswitch_2d
        :pswitch_4c
        :pswitch_50
        :pswitch_54
        :pswitch_58
        :pswitch_5c
    .end packed-switch
.end method

.method private process_aosStart()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 476
    const-string v0, "BootupController"

    #@3
    const-string v1, "process_aosStart()"

    #@5
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 478
    const/high16 v0, 0x1

    #@a
    invoke-static {v0, v2, v2}, Lcom/lge/ims/SysMonitor;->sendEventToNative(III)I

    #@d
    .line 479
    return-void
.end method

.method private process_bootComplete()V
    .registers 4

    #@0
    .prologue
    .line 305
    const-string v0, "BootupController"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "process_bootComplete(). old state["

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-object v2, p0, Lcom/lge/ims/BootupController;->mState:Lcom/lge/ims/BootupController$STATE;

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    const-string v2, "]"

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@20
    .line 307
    sget-object v0, Lcom/lge/ims/BootupController$2;->$SwitchMap$com$lge$ims$BootupController$STATE:[I

    #@22
    iget-object v1, p0, Lcom/lge/ims/BootupController;->mState:Lcom/lge/ims/BootupController$STATE;

    #@24
    invoke-virtual {v1}, Lcom/lge/ims/BootupController$STATE;->ordinal()I

    #@27
    move-result v1

    #@28
    aget v0, v0, v1

    #@2a
    packed-switch v0, :pswitch_data_64

    #@2d
    .line 345
    const-string v0, "BootupController"

    #@2f
    new-instance v1, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v2, "process_bootComplete() invalid State: "

    #@36
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v1

    #@3a
    iget-object v2, p0, Lcom/lge/ims/BootupController;->mState:Lcom/lge/ims/BootupController$STATE;

    #@3c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v1

    #@40
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v1

    #@44
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@47
    .line 349
    :goto_47
    return-void

    #@48
    .line 310
    :pswitch_48
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->StateNone_BootupComplete()V

    #@4b
    goto :goto_47

    #@4c
    .line 315
    :pswitch_4c
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->StateWaitingNetConnected_BootupComplete()V

    #@4f
    goto :goto_47

    #@50
    .line 320
    :pswitch_50
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->StateWaitingBootupComplete_BootupComplete()V

    #@53
    goto :goto_47

    #@54
    .line 325
    :pswitch_54
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->StateUpdatingAC_BootupComplete()V

    #@57
    goto :goto_47

    #@58
    .line 330
    :pswitch_58
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->StateWorkingRCSe_BootupComplete()V

    #@5b
    goto :goto_47

    #@5c
    .line 335
    :pswitch_5c
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->StateWaitingUserAction_BootupComplete()V

    #@5f
    goto :goto_47

    #@60
    .line 340
    :pswitch_60
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->StateWaitingCleanup_BootupCompleted()V

    #@63
    goto :goto_47

    #@64
    .line 307
    :pswitch_data_64
    .packed-switch 0x1
        :pswitch_48
        :pswitch_4c
        :pswitch_50
        :pswitch_54
        :pswitch_58
        :pswitch_5c
        :pswitch_60
    .end packed-switch
.end method

.method private process_dbUpdateCompleted()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 528
    const-string v1, "BootupController"

    #@3
    const-string v2, "process_dbUpdateCompleted()"

    #@5
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 530
    const/high16 v1, 0x8

    #@a
    invoke-static {v1, v3, v3}, Lcom/lge/ims/SysMonitor;->sendEventToNative(III)I

    #@d
    .line 531
    new-instance v0, Landroid/content/Intent;

    #@f
    const-string v1, "com.lge.ims.action.IMS_EVENT_CONFIG_UPDATE_COMPLETE"

    #@11
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@14
    .line 532
    .local v0, intent:Landroid/content/Intent;
    iget-object v1, p0, Lcom/lge/ims/BootupController;->mContext:Landroid/content/Context;

    #@16
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@19
    .line 533
    return-void
.end method

.method private process_networkConnectionChange()V
    .registers 6

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 352
    const-string v1, "BootupController"

    #@4
    const-string v2, "process_networkConnectionChange()"

    #@6
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 354
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->isMobileAvailable()Z

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_88

    #@f
    .line 355
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->isNetworkRoaming()Z

    #@12
    move-result v1

    #@13
    if-nez v1, :cond_79

    #@15
    .line 356
    iput-boolean v4, p0, Lcom/lge/ims/BootupController;->bNetConnected:Z

    #@17
    .line 394
    :goto_17
    const-string v1, "BootupController"

    #@19
    new-instance v2, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v3, "process_networkConnectionChange(). network available : "

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    iget-boolean v3, p0, Lcom/lge/ims/BootupController;->bNetConnected:Z

    #@26
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@29
    move-result-object v2

    #@2a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@31
    .line 395
    const-string v1, "BootupController"

    #@33
    new-instance v2, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v3, "process_acDone(). old state["

    #@3a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    iget-object v3, p0, Lcom/lge/ims/BootupController;->mState:Lcom/lge/ims/BootupController$STATE;

    #@40
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v2

    #@44
    const-string v3, "]"

    #@46
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v2

    #@4a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v2

    #@4e
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@51
    .line 398
    sget-object v1, Lcom/lge/ims/BootupController$2;->$SwitchMap$com$lge$ims$BootupController$STATE:[I

    #@53
    iget-object v2, p0, Lcom/lge/ims/BootupController;->mState:Lcom/lge/ims/BootupController$STATE;

    #@55
    invoke-virtual {v2}, Lcom/lge/ims/BootupController$STATE;->ordinal()I

    #@58
    move-result v2

    #@59
    aget v1, v1, v2

    #@5b
    packed-switch v1, :pswitch_data_d6

    #@5e
    .line 427
    const-string v1, "BootupController"

    #@60
    new-instance v2, Ljava/lang/StringBuilder;

    #@62
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@65
    const-string v3, "process_acDone() invalid State: "

    #@67
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v2

    #@6b
    iget-object v3, p0, Lcom/lge/ims/BootupController;->mState:Lcom/lge/ims/BootupController$STATE;

    #@6d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v2

    #@71
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v2

    #@75
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@78
    .line 431
    :goto_78
    return-void

    #@79
    .line 358
    :cond_79
    const-string v1, "BootupController"

    #@7b
    const-string v2, "Network is Roaming.. "

    #@7d
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@80
    .line 359
    iput-boolean v3, p0, Lcom/lge/ims/BootupController;->bNetConnected:Z

    #@82
    .line 360
    const/16 v1, 0x6b

    #@84
    invoke-direct {p0, v1}, Lcom/lge/ims/BootupController;->sendInternalEvent(I)V

    #@87
    goto :goto_78

    #@88
    .line 363
    :cond_88
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->isWifiAvailable()Z

    #@8b
    move-result v1

    #@8c
    if-eqz v1, :cond_91

    #@8e
    .line 364
    iput-boolean v4, p0, Lcom/lge/ims/BootupController;->bNetConnected:Z

    #@90
    goto :goto_17

    #@91
    .line 366
    :cond_91
    iput-boolean v3, p0, Lcom/lge/ims/BootupController;->bNetConnected:Z

    #@93
    .line 368
    sget-object v1, Lcom/lge/ims/BootupController$2;->$SwitchMap$com$lge$ims$BootupController$STATE:[I

    #@95
    iget-object v2, p0, Lcom/lge/ims/BootupController;->mState:Lcom/lge/ims/BootupController$STATE;

    #@97
    invoke-virtual {v2}, Lcom/lge/ims/BootupController$STATE;->ordinal()I

    #@9a
    move-result v2

    #@9b
    aget v1, v1, v2

    #@9d
    packed-switch v1, :pswitch_data_de

    #@a0
    goto :goto_78

    #@a1
    .line 372
    :pswitch_a1
    sget-object v1, Lcom/lge/ims/BootupController$STATE;->WAITING_NETWORK_CONNECTED:Lcom/lge/ims/BootupController$STATE;

    #@a3
    invoke-direct {p0, v1}, Lcom/lge/ims/BootupController;->setState(Lcom/lge/ims/BootupController$STATE;)V

    #@a6
    goto :goto_78

    #@a7
    .line 381
    :pswitch_a7
    sget-object v1, Lcom/lge/ims/BootupController$STATE;->NONE:Lcom/lge/ims/BootupController$STATE;

    #@a9
    invoke-direct {p0, v1}, Lcom/lge/ims/BootupController;->setState(Lcom/lge/ims/BootupController$STATE;)V

    #@ac
    goto :goto_78

    #@ad
    .line 401
    :pswitch_ad
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->ForceToStartAC()V

    #@b0
    goto :goto_78

    #@b1
    .line 406
    :pswitch_b1
    const/4 v0, 0x0

    #@b2
    .line 408
    .local v0, nAvailableSvc:I
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->checkRCSSetting()Z

    #@b5
    move-result v1

    #@b6
    if-ne v1, v4, :cond_cd

    #@b8
    .line 409
    sget-object v1, Lcom/lge/ims/BootupController$STATE;->WORKING_RCSE:Lcom/lge/ims/BootupController$STATE;

    #@ba
    invoke-direct {p0, v1}, Lcom/lge/ims/BootupController;->setState(Lcom/lge/ims/BootupController$STATE;)V

    #@bd
    .line 410
    const/high16 v0, 0xff

    #@bf
    .line 416
    :goto_bf
    const v1, 0x8000

    #@c2
    invoke-static {v1, v4, v0}, Lcom/lge/ims/SysMonitor;->sendEventToNative(III)I

    #@c5
    .line 417
    const/16 v1, 0x68

    #@c7
    const-wide/16 v2, 0x7d0

    #@c9
    invoke-direct {p0, v1, v2, v3}, Lcom/lge/ims/BootupController;->sendInternalEvent(IJ)V

    #@cc
    goto :goto_78

    #@cd
    .line 412
    :cond_cd
    sget-object v1, Lcom/lge/ims/BootupController$STATE;->NONE:Lcom/lge/ims/BootupController$STATE;

    #@cf
    invoke-direct {p0, v1}, Lcom/lge/ims/BootupController;->setState(Lcom/lge/ims/BootupController$STATE;)V

    #@d2
    .line 413
    const/16 v0, 0x100

    #@d4
    goto :goto_bf

    #@d5
    .line 398
    nop

    #@d6
    :pswitch_data_d6
    .packed-switch 0x1
        :pswitch_b1
        :pswitch_ad
    .end packed-switch

    #@de
    .line 368
    :pswitch_data_de
    .packed-switch 0x3
        :pswitch_a1
        :pswitch_a1
        :pswitch_a7
        :pswitch_a7
        :pswitch_a7
    .end packed-switch
.end method

.method private process_settingChanged()V
    .registers 4

    #@0
    .prologue
    .line 482
    const-string v0, "BootupController"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "process_settingChanged(). old state["

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-object v2, p0, Lcom/lge/ims/BootupController;->mState:Lcom/lge/ims/BootupController$STATE;

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    const-string v2, "]"

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@20
    .line 484
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->readRCSSettingFromDB()Z

    #@23
    move-result v0

    #@24
    if-nez v0, :cond_2d

    #@26
    .line 485
    const-string v0, "BootupController"

    #@28
    const-string v1, "readRCSSettingFromDB FAIL !!!!"

    #@2a
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@2d
    .line 488
    :cond_2d
    sget-object v0, Lcom/lge/ims/BootupController$2;->$SwitchMap$com$lge$ims$BootupController$STATE:[I

    #@2f
    iget-object v1, p0, Lcom/lge/ims/BootupController;->mState:Lcom/lge/ims/BootupController$STATE;

    #@31
    invoke-virtual {v1}, Lcom/lge/ims/BootupController$STATE;->ordinal()I

    #@34
    move-result v1

    #@35
    aget v0, v0, v1

    #@37
    packed-switch v0, :pswitch_data_6e

    #@3a
    .line 521
    :pswitch_3a
    const-string v0, "BootupController"

    #@3c
    new-instance v1, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v2, "process_settingChanged() invalid State: "

    #@43
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v1

    #@47
    iget-object v2, p0, Lcom/lge/ims/BootupController;->mState:Lcom/lge/ims/BootupController$STATE;

    #@49
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v1

    #@4d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v1

    #@51
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@54
    .line 525
    :goto_54
    return-void

    #@55
    .line 491
    :pswitch_55
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->StateNone_SettingChanged()V

    #@58
    goto :goto_54

    #@59
    .line 496
    :pswitch_59
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->StateWaitingBootupComplete_SettingChanged()V

    #@5c
    goto :goto_54

    #@5d
    .line 501
    :pswitch_5d
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->StateUpdatingAC_SettingChanged()V

    #@60
    goto :goto_54

    #@61
    .line 506
    :pswitch_61
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->StateWorkingRCSe_SettingChanged()V

    #@64
    goto :goto_54

    #@65
    .line 511
    :pswitch_65
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->StateWaitingUserAction_SettingChanged()V

    #@68
    goto :goto_54

    #@69
    .line 516
    :pswitch_69
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->StateWaitingCleanup_SettingChanged()V

    #@6c
    goto :goto_54

    #@6d
    .line 488
    nop

    #@6e
    :pswitch_data_6e
    .packed-switch 0x1
        :pswitch_55
        :pswitch_3a
        :pswitch_59
        :pswitch_5d
        :pswitch_61
        :pswitch_65
        :pswitch_69
    .end packed-switch
.end method

.method private readACSettingFromDB()Z
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 1136
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->getSubscriberID()Ljava/lang/String;

    #@5
    move-result-object v7

    #@6
    .line 1137
    .local v7, strIMSI:Ljava/lang/String;
    iget-object v0, p0, Lcom/lge/ims/BootupController;->mContext:Landroid/content/Context;

    #@8
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@b
    move-result-object v0

    #@c
    new-instance v1, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v3, "content://com.lge.ims.provisioning/sims/"

    #@13
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@22
    move-result-object v1

    #@23
    move-object v3, v2

    #@24
    move-object v4, v2

    #@25
    move-object v5, v2

    #@26
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@29
    move-result-object v6

    #@2a
    .line 1139
    .local v6, objCursor:Landroid/database/Cursor;
    if-eqz v6, :cond_32

    #@2c
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    #@2f
    move-result v0

    #@30
    if-nez v0, :cond_84

    #@32
    .line 1140
    :cond_32
    const-string v0, "BootupController"

    #@34
    const-string v1, "Provisioning DB to be init"

    #@36
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@39
    .line 1142
    iget-object v0, p0, Lcom/lge/ims/BootupController;->mContext:Landroid/content/Context;

    #@3b
    invoke-static {v0, v7}, Lcom/lge/ims/provisioning/ProvisioningHelper;->InsertDefaultProvisioningByIMSI(Landroid/content/Context;Ljava/lang/String;)Z

    #@3e
    move-result v0

    #@3f
    if-nez v0, :cond_4a

    #@41
    .line 1143
    const-string v0, "BootupController"

    #@43
    const-string v1, "Cant\' Insert Default Proviosioning "

    #@45
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@48
    move v0, v8

    #@49
    .line 1167
    :goto_49
    return v0

    #@4a
    .line 1147
    :cond_4a
    iget-object v0, p0, Lcom/lge/ims/BootupController;->mContext:Landroid/content/Context;

    #@4c
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4f
    move-result-object v0

    #@50
    new-instance v1, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v3, "content://com.lge.ims.provisioning/sims/"

    #@57
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v1

    #@5b
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v1

    #@5f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v1

    #@63
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@66
    move-result-object v1

    #@67
    move-object v3, v2

    #@68
    move-object v4, v2

    #@69
    move-object v5, v2

    #@6a
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@6d
    move-result-object v6

    #@6e
    .line 1149
    if-eqz v6, :cond_76

    #@70
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    #@73
    move-result v0

    #@74
    if-nez v0, :cond_84

    #@76
    .line 1150
    :cond_76
    const-string v0, "BootupController"

    #@78
    const-string v1, "Can\'t Query Provisioning "

    #@7a
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@7d
    .line 1152
    if-eqz v6, :cond_82

    #@7f
    .line 1153
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@82
    :cond_82
    move v0, v8

    #@83
    .line 1156
    goto :goto_49

    #@84
    .line 1160
    :cond_84
    const-string v0, "validity_period"

    #@86
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@89
    move-result v0

    #@8a
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@8d
    move-result-object v0

    #@8e
    iput-object v0, p0, Lcom/lge/ims/BootupController;->m_strValidPeriod:Ljava/lang/String;

    #@90
    .line 1161
    const-string v0, "ac_version"

    #@92
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@95
    move-result v0

    #@96
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@99
    move-result-object v0

    #@9a
    iput-object v0, p0, Lcom/lge/ims/BootupController;->m_strACVersion:Ljava/lang/String;

    #@9c
    .line 1163
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@9f
    .line 1165
    const-string v0, "BootupController"

    #@a1
    new-instance v1, Ljava/lang/StringBuilder;

    #@a3
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a6
    const-string v2, "Version : ["

    #@a8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v1

    #@ac
    iget-object v2, p0, Lcom/lge/ims/BootupController;->m_strACVersion:Ljava/lang/String;

    #@ae
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v1

    #@b2
    const-string v2, "]   Period: ["

    #@b4
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v1

    #@b8
    iget-object v2, p0, Lcom/lge/ims/BootupController;->m_strValidPeriod:Ljava/lang/String;

    #@ba
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v1

    #@be
    const-string v2, "]"

    #@c0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v1

    #@c4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c7
    move-result-object v1

    #@c8
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@cb
    .line 1167
    const/4 v0, 0x1

    #@cc
    goto/16 :goto_49
.end method

.method private readProvisioningDataFromDB()Z
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 1241
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->getSubscriberID()Ljava/lang/String;

    #@5
    move-result-object v7

    #@6
    .line 1242
    .local v7, strIMSI:Ljava/lang/String;
    iget-object v0, p0, Lcom/lge/ims/BootupController;->mContext:Landroid/content/Context;

    #@8
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@b
    move-result-object v0

    #@c
    new-instance v1, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v3, "content://com.lge.ims.provisioning/sims/"

    #@13
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@22
    move-result-object v1

    #@23
    move-object v3, v2

    #@24
    move-object v4, v2

    #@25
    move-object v5, v2

    #@26
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@29
    move-result-object v6

    #@2a
    .line 1244
    .local v6, objCursor:Landroid/database/Cursor;
    if-eqz v6, :cond_32

    #@2c
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    #@2f
    move-result v0

    #@30
    if-nez v0, :cond_40

    #@32
    .line 1245
    :cond_32
    const-string v0, "BootupController"

    #@34
    const-string v1, "Can\'t Query Provisioning "

    #@36
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@39
    .line 1247
    if-eqz v6, :cond_3e

    #@3b
    .line 1248
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@3e
    :cond_3e
    move v0, v8

    #@3f
    .line 1306
    :goto_3f
    return v0

    #@40
    .line 1254
    :cond_40
    const-string v0, "private_user_identity"

    #@42
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@45
    move-result v0

    #@46
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@49
    move-result-object v0

    #@4a
    iput-object v0, p0, Lcom/lge/ims/BootupController;->mPrivateUri:Ljava/lang/String;

    #@4c
    .line 1255
    const-string v0, "home_network_domain_name"

    #@4e
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@51
    move-result v0

    #@52
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@55
    move-result-object v0

    #@56
    iput-object v0, p0, Lcom/lge/ims/BootupController;->mHomeDomainName:Ljava/lang/String;

    #@58
    .line 1256
    const-string v0, "sip_uri"

    #@5a
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@5d
    move-result v0

    #@5e
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@61
    move-result-object v0

    #@62
    iput-object v0, p0, Lcom/lge/ims/BootupController;->mPublicUri:Ljava/lang/String;

    #@64
    .line 1257
    const-string v0, "realm"

    #@66
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@69
    move-result v0

    #@6a
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@6d
    move-result-object v0

    #@6e
    iput-object v0, p0, Lcom/lge/ims/BootupController;->mRealm:Ljava/lang/String;

    #@70
    .line 1258
    const-string v0, "sip_user"

    #@72
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@75
    move-result v0

    #@76
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@79
    move-result-object v0

    #@7a
    iput-object v0, p0, Lcom/lge/ims/BootupController;->mUserName:Ljava/lang/String;

    #@7c
    .line 1259
    const-string v0, "sip_password"

    #@7e
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@81
    move-result v0

    #@82
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@85
    move-result-object v0

    #@86
    iput-object v0, p0, Lcom/lge/ims/BootupController;->mUserPwd:Ljava/lang/String;

    #@88
    .line 1260
    const-string v0, "timer_t1"

    #@8a
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@8d
    move-result v0

    #@8e
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    #@91
    move-result v0

    #@92
    iput v0, p0, Lcom/lge/ims/BootupController;->mTimerT1:I

    #@94
    .line 1261
    const-string v0, "timer_t2"

    #@96
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@99
    move-result v0

    #@9a
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    #@9d
    move-result v0

    #@9e
    iput v0, p0, Lcom/lge/ims/BootupController;->mTimerT2:I

    #@a0
    .line 1262
    const-string v0, "timer_b"

    #@a2
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@a5
    move-result v0

    #@a6
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    #@a9
    move-result v0

    #@aa
    iput v0, p0, Lcom/lge/ims/BootupController;->mTimerTb:I

    #@ac
    .line 1263
    const-string v0, "timer_d"

    #@ae
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@b1
    move-result v0

    #@b2
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    #@b5
    move-result v0

    #@b6
    iput v0, p0, Lcom/lge/ims/BootupController;->mTimerTd:I

    #@b8
    .line 1264
    const-string v0, "timer_f"

    #@ba
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@bd
    move-result v0

    #@be
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    #@c1
    move-result v0

    #@c2
    iput v0, p0, Lcom/lge/ims/BootupController;->mTimerTf:I

    #@c4
    .line 1265
    const-string v0, "timer_h"

    #@c6
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@c9
    move-result v0

    #@ca
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    #@cd
    move-result v0

    #@ce
    iput v0, p0, Lcom/lge/ims/BootupController;->mTimerTh:I

    #@d0
    .line 1266
    const-string v0, "timer_i"

    #@d2
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@d5
    move-result v0

    #@d6
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    #@d9
    move-result v0

    #@da
    iput v0, p0, Lcom/lge/ims/BootupController;->mTimerTi:I

    #@dc
    .line 1267
    const-string v0, "timer_j"

    #@de
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@e1
    move-result v0

    #@e2
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    #@e5
    move-result v0

    #@e6
    iput v0, p0, Lcom/lge/ims/BootupController;->mTimerTj:I

    #@e8
    .line 1268
    const-string v0, "timer_k"

    #@ea
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@ed
    move-result v0

    #@ee
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    #@f1
    move-result v0

    #@f2
    iput v0, p0, Lcom/lge/ims/BootupController;->mTimerTk:I

    #@f4
    .line 1269
    const-string v0, "rtpport_port1"

    #@f6
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@f9
    move-result v0

    #@fa
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    #@fd
    move-result v0

    #@fe
    iput v0, p0, Lcom/lge/ims/BootupController;->mAudioPort:I

    #@100
    .line 1270
    const-string v0, "rtpport_port2"

    #@102
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@105
    move-result v0

    #@106
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    #@109
    move-result v0

    #@10a
    iput v0, p0, Lcom/lge/ims/BootupController;->mVideoPort:I

    #@10c
    .line 1271
    const-string v0, "lbo_p_cscf_address"

    #@10e
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@111
    move-result v0

    #@112
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@115
    move-result-object v0

    #@116
    iput-object v0, p0, Lcom/lge/ims/BootupController;->mPcscfAddress:Ljava/lang/String;

    #@118
    .line 1272
    iget-object v0, p0, Lcom/lge/ims/BootupController;->mPcscfAddress:Ljava/lang/String;

    #@11a
    iget-object v1, p0, Lcom/lge/ims/BootupController;->mPcscfAddress:Ljava/lang/String;

    #@11c
    const-string v2, ":"

    #@11e
    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@121
    move-result v1

    #@122
    add-int/lit8 v1, v1, 0x1

    #@124
    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@127
    move-result-object v0

    #@128
    iput-object v0, p0, Lcom/lge/ims/BootupController;->mPcscfAddressPort:Ljava/lang/String;

    #@12a
    .line 1273
    iget-object v0, p0, Lcom/lge/ims/BootupController;->mPcscfAddress:Ljava/lang/String;

    #@12c
    iget-object v1, p0, Lcom/lge/ims/BootupController;->mPcscfAddress:Ljava/lang/String;

    #@12e
    const-string v2, ":"

    #@130
    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@133
    move-result v1

    #@134
    invoke-virtual {v0, v8, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@137
    move-result-object v0

    #@138
    iput-object v0, p0, Lcom/lge/ims/BootupController;->mPcscfAddress:Ljava/lang/String;

    #@13a
    .line 1274
    const-string v0, "abcinfo_address"

    #@13c
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@13f
    move-result v0

    #@140
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@143
    move-result-object v0

    #@144
    iput-object v0, p0, Lcom/lge/ims/BootupController;->mAbcAddress:Ljava/lang/String;

    #@146
    .line 1275
    iget-object v0, p0, Lcom/lge/ims/BootupController;->mAbcAddress:Ljava/lang/String;

    #@148
    iget-object v1, p0, Lcom/lge/ims/BootupController;->mAbcAddress:Ljava/lang/String;

    #@14a
    const-string v2, ":"

    #@14c
    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@14f
    move-result v1

    #@150
    add-int/lit8 v1, v1, 0x1

    #@152
    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@155
    move-result-object v0

    #@156
    iput-object v0, p0, Lcom/lge/ims/BootupController;->mAbcAddressPort:Ljava/lang/String;

    #@158
    .line 1276
    iget-object v0, p0, Lcom/lge/ims/BootupController;->mAbcAddress:Ljava/lang/String;

    #@15a
    iget-object v1, p0, Lcom/lge/ims/BootupController;->mAbcAddress:Ljava/lang/String;

    #@15c
    const-string v2, ":"

    #@15e
    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@161
    move-result v1

    #@162
    invoke-virtual {v0, v8, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@165
    move-result-object v0

    #@166
    iput-object v0, p0, Lcom/lge/ims/BootupController;->mAbcAddress:Ljava/lang/String;

    #@168
    .line 1277
    const-string v0, "client_obj_datalimit"

    #@16a
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@16d
    move-result v0

    #@16e
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@171
    move-result-object v0

    #@172
    iput-object v0, p0, Lcom/lge/ims/BootupController;->mClientObjDataLimit:Ljava/lang/String;

    #@174
    .line 1278
    const-string v0, "presence_content_server_uri"

    #@176
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@179
    move-result v0

    #@17a
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@17d
    move-result-object v0

    #@17e
    iput-object v0, p0, Lcom/lge/ims/BootupController;->mPresenceContentServerUri:Ljava/lang/String;

    #@180
    .line 1279
    const-string v0, "im_content_server_uri"

    #@182
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@185
    move-result v0

    #@186
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@189
    move-result-object v0

    #@18a
    iput-object v0, p0, Lcom/lge/ims/BootupController;->mImContentServerUri:Ljava/lang/String;

    #@18c
    .line 1280
    const-string v0, "source_throttlepublish"

    #@18e
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@191
    move-result v0

    #@192
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@195
    move-result-object v0

    #@196
    iput-object v0, p0, Lcom/lge/ims/BootupController;->mSourceThrottlePublish:Ljava/lang/String;

    #@198
    .line 1281
    const-string v0, "max_number_of_subscriptions_inpresence_ist"

    #@19a
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@19d
    move-result v0

    #@19e
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    #@1a1
    move-result v0

    #@1a2
    iput v0, p0, Lcom/lge/ims/BootupController;->mMaxNumberOfSubscriptionsInPresenceList:I

    #@1a4
    .line 1282
    const-string v0, "xcap_root_uri"

    #@1a6
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@1a9
    move-result v0

    #@1aa
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@1ad
    move-result-object v0

    #@1ae
    iput-object v0, p0, Lcom/lge/ims/BootupController;->mXCAPRootURI:Ljava/lang/String;

    #@1b0
    .line 1283
    const-string v0, "max_adhoc_group_size"

    #@1b2
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@1b5
    move-result v0

    #@1b6
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    #@1b9
    move-result v0

    #@1ba
    iput v0, p0, Lcom/lge/ims/BootupController;->mMaxAdhocGroupSize:I

    #@1bc
    .line 1284
    const-string v0, "im_conference_factory_uri"

    #@1be
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@1c1
    move-result v0

    #@1c2
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@1c5
    move-result-object v0

    #@1c6
    iput-object v0, p0, Lcom/lge/ims/BootupController;->mConferenceFactoryUri:Ljava/lang/String;

    #@1c8
    .line 1287
    iget-object v0, p0, Lcom/lge/ims/BootupController;->mConferenceFactoryUri:Ljava/lang/String;

    #@1ca
    const-string v1, "sip:"

    #@1cc
    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@1cf
    move-result v0

    #@1d0
    if-gez v0, :cond_1f3

    #@1d2
    iget-object v0, p0, Lcom/lge/ims/BootupController;->mConferenceFactoryUri:Ljava/lang/String;

    #@1d4
    const-string v1, "tel:"

    #@1d6
    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@1d9
    move-result v0

    #@1da
    if-gez v0, :cond_1f3

    #@1dc
    .line 1288
    new-instance v0, Ljava/lang/StringBuilder;

    #@1de
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1e1
    const-string v1, "sip:"

    #@1e3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e6
    move-result-object v0

    #@1e7
    iget-object v1, p0, Lcom/lge/ims/BootupController;->mConferenceFactoryUri:Ljava/lang/String;

    #@1e9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ec
    move-result-object v0

    #@1ed
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f0
    move-result-object v0

    #@1f1
    iput-object v0, p0, Lcom/lge/ims/BootupController;->mConferenceFactoryUri:Ljava/lang/String;

    #@1f3
    .line 1291
    :cond_1f3
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@1f6
    .line 1294
    const-string v0, "BootupController"

    #@1f8
    new-instance v1, Ljava/lang/StringBuilder;

    #@1fa
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1fd
    const-string v2, "mPrivateUri : ["

    #@1ff
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@202
    move-result-object v1

    #@203
    iget-object v2, p0, Lcom/lge/ims/BootupController;->mPrivateUri:Ljava/lang/String;

    #@205
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@208
    move-result-object v1

    #@209
    const-string v2, "]   mHomeDomainName: ["

    #@20b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20e
    move-result-object v1

    #@20f
    iget-object v2, p0, Lcom/lge/ims/BootupController;->mHomeDomainName:Ljava/lang/String;

    #@211
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@214
    move-result-object v1

    #@215
    const-string v2, "]   mPublicUri: ["

    #@217
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21a
    move-result-object v1

    #@21b
    iget-object v2, p0, Lcom/lge/ims/BootupController;->mPublicUri:Ljava/lang/String;

    #@21d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@220
    move-result-object v1

    #@221
    const-string v2, "] "

    #@223
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@226
    move-result-object v1

    #@227
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22a
    move-result-object v1

    #@22b
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@22e
    .line 1295
    const-string v0, "BootupController"

    #@230
    new-instance v1, Ljava/lang/StringBuilder;

    #@232
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@235
    const-string v2, "mRealm : ["

    #@237
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23a
    move-result-object v1

    #@23b
    iget-object v2, p0, Lcom/lge/ims/BootupController;->mRealm:Ljava/lang/String;

    #@23d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@240
    move-result-object v1

    #@241
    const-string v2, "]   mUserName: ["

    #@243
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@246
    move-result-object v1

    #@247
    iget-object v2, p0, Lcom/lge/ims/BootupController;->mUserName:Ljava/lang/String;

    #@249
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24c
    move-result-object v1

    #@24d
    const-string v2, "]   mUserPwd: ["

    #@24f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@252
    move-result-object v1

    #@253
    iget-object v2, p0, Lcom/lge/ims/BootupController;->mUserPwd:Ljava/lang/String;

    #@255
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@258
    move-result-object v1

    #@259
    const-string v2, "] "

    #@25b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25e
    move-result-object v1

    #@25f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@262
    move-result-object v1

    #@263
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@266
    .line 1296
    const-string v0, "BootupController"

    #@268
    new-instance v1, Ljava/lang/StringBuilder;

    #@26a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@26d
    const-string v2, "mTimerT1 : ["

    #@26f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@272
    move-result-object v1

    #@273
    iget v2, p0, Lcom/lge/ims/BootupController;->mTimerT1:I

    #@275
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@278
    move-result-object v1

    #@279
    const-string v2, "]   mTimerT2: ["

    #@27b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27e
    move-result-object v1

    #@27f
    iget v2, p0, Lcom/lge/ims/BootupController;->mTimerT2:I

    #@281
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@284
    move-result-object v1

    #@285
    const-string v2, "]   mTimerTb: ["

    #@287
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28a
    move-result-object v1

    #@28b
    iget v2, p0, Lcom/lge/ims/BootupController;->mTimerTb:I

    #@28d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@290
    move-result-object v1

    #@291
    const-string v2, "] "

    #@293
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@296
    move-result-object v1

    #@297
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29a
    move-result-object v1

    #@29b
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@29e
    .line 1297
    const-string v0, "BootupController"

    #@2a0
    new-instance v1, Ljava/lang/StringBuilder;

    #@2a2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2a5
    const-string v2, "mTimerTd : ["

    #@2a7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2aa
    move-result-object v1

    #@2ab
    iget v2, p0, Lcom/lge/ims/BootupController;->mTimerTd:I

    #@2ad
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2b0
    move-result-object v1

    #@2b1
    const-string v2, "]   mTimerTf: ["

    #@2b3
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b6
    move-result-object v1

    #@2b7
    iget v2, p0, Lcom/lge/ims/BootupController;->mTimerTf:I

    #@2b9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2bc
    move-result-object v1

    #@2bd
    const-string v2, "]   mTimerTh: ["

    #@2bf
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c2
    move-result-object v1

    #@2c3
    iget v2, p0, Lcom/lge/ims/BootupController;->mTimerTh:I

    #@2c5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c8
    move-result-object v1

    #@2c9
    const-string v2, "] "

    #@2cb
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ce
    move-result-object v1

    #@2cf
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d2
    move-result-object v1

    #@2d3
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@2d6
    .line 1298
    const-string v0, "BootupController"

    #@2d8
    new-instance v1, Ljava/lang/StringBuilder;

    #@2da
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2dd
    const-string v2, "mTimerTi : ["

    #@2df
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e2
    move-result-object v1

    #@2e3
    iget v2, p0, Lcom/lge/ims/BootupController;->mTimerTi:I

    #@2e5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e8
    move-result-object v1

    #@2e9
    const-string v2, "]   mTimerTj: ["

    #@2eb
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ee
    move-result-object v1

    #@2ef
    iget v2, p0, Lcom/lge/ims/BootupController;->mTimerTj:I

    #@2f1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f4
    move-result-object v1

    #@2f5
    const-string v2, "]   mTimerTk: ["

    #@2f7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2fa
    move-result-object v1

    #@2fb
    iget v2, p0, Lcom/lge/ims/BootupController;->mTimerTk:I

    #@2fd
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@300
    move-result-object v1

    #@301
    const-string v2, "] "

    #@303
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@306
    move-result-object v1

    #@307
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30a
    move-result-object v1

    #@30b
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@30e
    .line 1299
    const-string v0, "BootupController"

    #@310
    new-instance v1, Ljava/lang/StringBuilder;

    #@312
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@315
    const-string v2, "mAudioPort : ["

    #@317
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31a
    move-result-object v1

    #@31b
    iget v2, p0, Lcom/lge/ims/BootupController;->mAudioPort:I

    #@31d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@320
    move-result-object v1

    #@321
    const-string v2, "]   mVideoPort: ["

    #@323
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@326
    move-result-object v1

    #@327
    iget v2, p0, Lcom/lge/ims/BootupController;->mVideoPort:I

    #@329
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32c
    move-result-object v1

    #@32d
    const-string v2, "]   mPcscfAddress: ["

    #@32f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@332
    move-result-object v1

    #@333
    iget-object v2, p0, Lcom/lge/ims/BootupController;->mPcscfAddress:Ljava/lang/String;

    #@335
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@338
    move-result-object v1

    #@339
    const-string v2, "] "

    #@33b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33e
    move-result-object v1

    #@33f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@342
    move-result-object v1

    #@343
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@346
    .line 1300
    const-string v0, "BootupController"

    #@348
    new-instance v1, Ljava/lang/StringBuilder;

    #@34a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@34d
    const-string v2, "mPcscfAddressPort : ["

    #@34f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@352
    move-result-object v1

    #@353
    iget-object v2, p0, Lcom/lge/ims/BootupController;->mPcscfAddressPort:Ljava/lang/String;

    #@355
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@358
    move-result-object v1

    #@359
    const-string v2, "]   mAbcAddress: ["

    #@35b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35e
    move-result-object v1

    #@35f
    iget-object v2, p0, Lcom/lge/ims/BootupController;->mAbcAddress:Ljava/lang/String;

    #@361
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@364
    move-result-object v1

    #@365
    const-string v2, "]   mAbcAddressPort: ["

    #@367
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36a
    move-result-object v1

    #@36b
    iget-object v2, p0, Lcom/lge/ims/BootupController;->mAbcAddressPort:Ljava/lang/String;

    #@36d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@370
    move-result-object v1

    #@371
    const-string v2, "] "

    #@373
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@376
    move-result-object v1

    #@377
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37a
    move-result-object v1

    #@37b
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@37e
    .line 1301
    const-string v0, "BootupController"

    #@380
    new-instance v1, Ljava/lang/StringBuilder;

    #@382
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@385
    const-string v2, "mClientObjDataLimit : ["

    #@387
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38a
    move-result-object v1

    #@38b
    iget-object v2, p0, Lcom/lge/ims/BootupController;->mClientObjDataLimit:Ljava/lang/String;

    #@38d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@390
    move-result-object v1

    #@391
    const-string v2, "]   mPresenceContentServerUri: ["

    #@393
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@396
    move-result-object v1

    #@397
    iget-object v2, p0, Lcom/lge/ims/BootupController;->mPresenceContentServerUri:Ljava/lang/String;

    #@399
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39c
    move-result-object v1

    #@39d
    const-string v2, "] "

    #@39f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a2
    move-result-object v1

    #@3a3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a6
    move-result-object v1

    #@3a7
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@3aa
    .line 1302
    const-string v0, "BootupController"

    #@3ac
    new-instance v1, Ljava/lang/StringBuilder;

    #@3ae
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3b1
    const-string v2, "mImContentServerUri : ["

    #@3b3
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b6
    move-result-object v1

    #@3b7
    iget-object v2, p0, Lcom/lge/ims/BootupController;->mImContentServerUri:Ljava/lang/String;

    #@3b9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3bc
    move-result-object v1

    #@3bd
    const-string v2, "]"

    #@3bf
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c2
    move-result-object v1

    #@3c3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c6
    move-result-object v1

    #@3c7
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@3ca
    .line 1303
    const-string v0, "BootupController"

    #@3cc
    new-instance v1, Ljava/lang/StringBuilder;

    #@3ce
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3d1
    const-string v2, "mSourceThrottlePublish : ["

    #@3d3
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d6
    move-result-object v1

    #@3d7
    iget-object v2, p0, Lcom/lge/ims/BootupController;->mSourceThrottlePublish:Ljava/lang/String;

    #@3d9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3dc
    move-result-object v1

    #@3dd
    const-string v2, "]   mMaxNumberOfSubscriptionsInPresenceList: ["

    #@3df
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e2
    move-result-object v1

    #@3e3
    iget v2, p0, Lcom/lge/ims/BootupController;->mMaxNumberOfSubscriptionsInPresenceList:I

    #@3e5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e8
    move-result-object v1

    #@3e9
    const-string v2, "]"

    #@3eb
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3ee
    move-result-object v1

    #@3ef
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f2
    move-result-object v1

    #@3f3
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@3f6
    .line 1304
    const-string v0, "BootupController"

    #@3f8
    new-instance v1, Ljava/lang/StringBuilder;

    #@3fa
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3fd
    const-string v2, "mXCAPRootURI : ["

    #@3ff
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@402
    move-result-object v1

    #@403
    iget-object v2, p0, Lcom/lge/ims/BootupController;->mXCAPRootURI:Ljava/lang/String;

    #@405
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@408
    move-result-object v1

    #@409
    const-string v2, "]   mMaxAdhocGroupSize: ["

    #@40b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40e
    move-result-object v1

    #@40f
    iget v2, p0, Lcom/lge/ims/BootupController;->mMaxAdhocGroupSize:I

    #@411
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@414
    move-result-object v1

    #@415
    const-string v2, "]   mConferenceFactoryUri: ["

    #@417
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41a
    move-result-object v1

    #@41b
    iget-object v2, p0, Lcom/lge/ims/BootupController;->mConferenceFactoryUri:Ljava/lang/String;

    #@41d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@420
    move-result-object v1

    #@421
    const-string v2, "] "

    #@423
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@426
    move-result-object v1

    #@427
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42a
    move-result-object v1

    #@42b
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@42e
    .line 1306
    const/4 v0, 0x1

    #@42f
    goto/16 :goto_3f
.end method

.method private readRCSSettingFromDB()Z
    .registers 15

    #@0
    .prologue
    const/4 v13, 0x1

    #@1
    const/4 v12, 0x0

    #@2
    const/4 v2, 0x0

    #@3
    .line 1171
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->getSubscriberID()Ljava/lang/String;

    #@6
    move-result-object v11

    #@7
    .line 1172
    .local v11, strIMSI:Ljava/lang/String;
    iget-object v0, p0, Lcom/lge/ims/BootupController;->mContext:Landroid/content/Context;

    #@9
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@c
    move-result-object v0

    #@d
    new-instance v1, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v3, "content://com.lge.ims.provisioning/settings/"

    #@14
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@23
    move-result-object v1

    #@24
    move-object v3, v2

    #@25
    move-object v4, v2

    #@26
    move-object v5, v2

    #@27
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@2a
    move-result-object v10

    #@2b
    .line 1174
    .local v10, objCursor:Landroid/database/Cursor;
    if-eqz v10, :cond_33

    #@2d
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    #@30
    move-result v0

    #@31
    if-nez v0, :cond_8c

    #@33
    .line 1175
    :cond_33
    const-string v0, "BootupController"

    #@35
    const-string v1, "Settings DB to be init"

    #@37
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@3a
    .line 1177
    iget-object v0, p0, Lcom/lge/ims/BootupController;->mContext:Landroid/content/Context;

    #@3c
    invoke-static {v0, v11}, Lcom/lge/ims/provisioning/ProvisioningHelper;->InsertDefaultSettingByIMSI(Landroid/content/Context;Ljava/lang/String;)Z

    #@3f
    move-result v0

    #@40
    if-nez v0, :cond_4b

    #@42
    .line 1178
    const-string v0, "BootupController"

    #@44
    const-string v1, "Cant\' Insert Default Setting "

    #@46
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@49
    move v0, v12

    #@4a
    .line 1237
    :goto_4a
    return v0

    #@4b
    .line 1182
    :cond_4b
    if-eqz v10, :cond_50

    #@4d
    .line 1183
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@50
    .line 1186
    :cond_50
    iget-object v0, p0, Lcom/lge/ims/BootupController;->mContext:Landroid/content/Context;

    #@52
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@55
    move-result-object v0

    #@56
    new-instance v1, Ljava/lang/StringBuilder;

    #@58
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5b
    const-string v3, "content://com.lge.ims.provisioning/settings/"

    #@5d
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v1

    #@61
    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v1

    #@65
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v1

    #@69
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@6c
    move-result-object v1

    #@6d
    move-object v3, v2

    #@6e
    move-object v4, v2

    #@6f
    move-object v5, v2

    #@70
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@73
    move-result-object v10

    #@74
    .line 1188
    if-eqz v10, :cond_7c

    #@76
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    #@79
    move-result v0

    #@7a
    if-nez v0, :cond_8a

    #@7c
    .line 1189
    :cond_7c
    const-string v0, "BootupController"

    #@7e
    const-string v1, "Can\'t Query Setttings"

    #@80
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@83
    .line 1191
    if-eqz v10, :cond_88

    #@85
    .line 1192
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@88
    :cond_88
    move v0, v12

    #@89
    .line 1195
    goto :goto_4a

    #@8a
    .line 1198
    :cond_8a
    iput-boolean v12, p0, Lcom/lge/ims/BootupController;->m_bRCSUserAccept:Z

    #@8c
    .line 1201
    :cond_8c
    const-string v0, "rcs_e_service"

    #@8e
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@91
    move-result v0

    #@92
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    #@95
    move-result v8

    #@96
    .line 1202
    .local v8, nService:I
    const-string v0, "rcs_e_roaming"

    #@98
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@9b
    move-result v0

    #@9c
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    #@9f
    move-result v7

    #@a0
    .line 1203
    .local v7, nRoaming:I
    const-string v0, "is_accept"

    #@a2
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@a5
    move-result v0

    #@a6
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    #@a9
    move-result v9

    #@aa
    .line 1204
    .local v9, nTermsAccept:I
    const-string v0, "same_xml_version"

    #@ac
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@af
    move-result v0

    #@b0
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    #@b3
    move-result v0

    #@b4
    iput v0, p0, Lcom/lge/ims/BootupController;->m_intSameXmlVersion:I

    #@b6
    .line 1205
    const-string v0, "again_notification"

    #@b8
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@bb
    move-result v0

    #@bc
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    #@bf
    move-result v6

    #@c0
    .line 1207
    .local v6, nAgainNotification:I
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@c3
    .line 1209
    if-nez v8, :cond_14e

    #@c5
    .line 1210
    iput-boolean v12, p0, Lcom/lge/ims/BootupController;->m_bSettingRCSeON:Z

    #@c7
    .line 1215
    :goto_c7
    if-nez v7, :cond_152

    #@c9
    .line 1216
    iput-boolean v12, p0, Lcom/lge/ims/BootupController;->m_bSettingAllowRoaming:Z

    #@cb
    .line 1221
    :goto_cb
    if-ne v9, v13, :cond_156

    #@cd
    .line 1222
    iput-boolean v13, p0, Lcom/lge/ims/BootupController;->m_bRCSUserAccept:Z

    #@cf
    .line 1227
    :goto_cf
    if-nez v6, :cond_15a

    #@d1
    .line 1228
    iput-boolean v12, p0, Lcom/lge/ims/BootupController;->m_bAgainNotification:Z

    #@d3
    .line 1233
    :goto_d3
    const-string v0, "BootupController"

    #@d5
    new-instance v1, Ljava/lang/StringBuilder;

    #@d7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@da
    const-string v2, "Service : ["

    #@dc
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@df
    move-result-object v1

    #@e0
    iget-boolean v2, p0, Lcom/lge/ims/BootupController;->m_bSettingRCSeON:Z

    #@e2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@e5
    move-result-object v1

    #@e6
    const-string v2, "]    Roaming : ["

    #@e8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@eb
    move-result-object v1

    #@ec
    iget-boolean v2, p0, Lcom/lge/ims/BootupController;->m_bSettingAllowRoaming:Z

    #@ee
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@f1
    move-result-object v1

    #@f2
    const-string v2, "]"

    #@f4
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f7
    move-result-object v1

    #@f8
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fb
    move-result-object v1

    #@fc
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@ff
    .line 1234
    const-string v0, "BootupController"

    #@101
    new-instance v1, Ljava/lang/StringBuilder;

    #@103
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@106
    const-string v2, "SamXmlVersion : ["

    #@108
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10b
    move-result-object v1

    #@10c
    iget v2, p0, Lcom/lge/ims/BootupController;->m_intSameXmlVersion:I

    #@10e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@111
    move-result-object v1

    #@112
    const-string v2, "]    UserAccept : ["

    #@114
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@117
    move-result-object v1

    #@118
    iget-boolean v2, p0, Lcom/lge/ims/BootupController;->m_bRCSUserAccept:Z

    #@11a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@11d
    move-result-object v1

    #@11e
    const-string v2, "]"

    #@120
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@123
    move-result-object v1

    #@124
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@127
    move-result-object v1

    #@128
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@12b
    .line 1235
    const-string v0, "BootupController"

    #@12d
    new-instance v1, Ljava/lang/StringBuilder;

    #@12f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@132
    const-string v2, "AgainNotification : ["

    #@134
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@137
    move-result-object v1

    #@138
    iget-boolean v2, p0, Lcom/lge/ims/BootupController;->m_bAgainNotification:Z

    #@13a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@13d
    move-result-object v1

    #@13e
    const-string v2, "]"

    #@140
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@143
    move-result-object v1

    #@144
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@147
    move-result-object v1

    #@148
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@14b
    move v0, v13

    #@14c
    .line 1237
    goto/16 :goto_4a

    #@14e
    .line 1212
    :cond_14e
    iput-boolean v13, p0, Lcom/lge/ims/BootupController;->m_bSettingRCSeON:Z

    #@150
    goto/16 :goto_c7

    #@152
    .line 1218
    :cond_152
    iput-boolean v13, p0, Lcom/lge/ims/BootupController;->m_bSettingAllowRoaming:Z

    #@154
    goto/16 :goto_cb

    #@156
    .line 1224
    :cond_156
    iput-boolean v12, p0, Lcom/lge/ims/BootupController;->m_bRCSUserAccept:Z

    #@158
    goto/16 :goto_cf

    #@15a
    .line 1230
    :cond_15a
    iput-boolean v13, p0, Lcom/lge/ims/BootupController;->m_bAgainNotification:Z

    #@15c
    goto/16 :goto_d3
.end method

.method private sendEventToUpdateConfig(Z)V
    .registers 11
    .parameter "_bValidity"

    #@0
    .prologue
    const-wide/16 v7, 0x3e8

    #@2
    const v6, 0x640008

    #@5
    const/16 v5, 0x6a

    #@7
    const/16 v4, 0x40

    #@9
    const/4 v3, 0x1

    #@a
    .line 1470
    const-string v0, "BootupController"

    #@c
    new-instance v1, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v2, "sendEventToUpdateConfig() Result: "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@22
    .line 1472
    if-ne p1, v3, :cond_2b

    #@24
    .line 1473
    invoke-static {v4, v6, v3}, Lcom/lge/ims/SysMonitor;->sendEventToNative(III)I

    #@27
    .line 1474
    invoke-direct {p0, v5, v7, v8}, Lcom/lge/ims/BootupController;->sendInternalEvent(IJ)V

    #@2a
    .line 1479
    :goto_2a
    return-void

    #@2b
    .line 1476
    :cond_2b
    const/4 v0, 0x0

    #@2c
    invoke-static {v4, v6, v0}, Lcom/lge/ims/SysMonitor;->sendEventToNative(III)I

    #@2f
    .line 1477
    invoke-direct {p0, v5, v7, v8}, Lcom/lge/ims/BootupController;->sendInternalEvent(IJ)V

    #@32
    goto :goto_2a
.end method

.method private sendInternalEvent(I)V
    .registers 5
    .parameter "event"

    #@0
    .prologue
    .line 866
    iget-object v0, p0, Lcom/lge/ims/BootupController;->mHandler:Landroid/os/Handler;

    #@2
    const-wide/16 v1, 0xa

    #@4
    invoke-virtual {v0, p1, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@7
    .line 867
    return-void
.end method

.method private sendInternalEvent(IJ)V
    .registers 5
    .parameter "event"
    .parameter "duration"

    #@0
    .prologue
    .line 870
    iget-object v0, p0, Lcom/lge/ims/BootupController;->mHandler:Landroid/os/Handler;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@5
    .line 871
    return-void
.end method

.method private setState(Lcom/lge/ims/BootupController$STATE;)V
    .registers 6
    .parameter "_eState"

    #@0
    .prologue
    .line 1669
    iget-object v0, p0, Lcom/lge/ims/BootupController;->mState:Lcom/lge/ims/BootupController$STATE;

    #@2
    .line 1671
    .local v0, oldState:Lcom/lge/ims/BootupController$STATE;
    sget-object v1, Lcom/lge/ims/BootupController$2;->$SwitchMap$com$lge$ims$BootupController$STATE:[I

    #@4
    invoke-virtual {p1}, Lcom/lge/ims/BootupController$STATE;->ordinal()I

    #@7
    move-result v2

    #@8
    aget v1, v1, v2

    #@a
    packed-switch v1, :pswitch_data_188

    #@d
    .line 1716
    const-string v1, "BootupController"

    #@f
    new-instance v2, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v3, "Current State["

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    const-string v3, "]"

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@2b
    .line 1720
    :goto_2b
    return-void

    #@2c
    .line 1674
    :pswitch_2c
    const-string v1, "BootupController"

    #@2e
    new-instance v2, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v3, "oldState["

    #@35
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v2

    #@3d
    const-string v3, "] ---> "

    #@3f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v2

    #@43
    const-string v3, "State["

    #@45
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v2

    #@49
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v2

    #@4d
    const-string v3, " :NONE]"

    #@4f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v2

    #@53
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v2

    #@57
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@5a
    .line 1675
    iput-object p1, p0, Lcom/lge/ims/BootupController;->mState:Lcom/lge/ims/BootupController$STATE;

    #@5c
    goto :goto_2b

    #@5d
    .line 1680
    :pswitch_5d
    const-string v1, "BootupController"

    #@5f
    new-instance v2, Ljava/lang/StringBuilder;

    #@61
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    const-string v3, "oldState["

    #@66
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v2

    #@6a
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v2

    #@6e
    const-string v3, "] ---> "

    #@70
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v2

    #@74
    const-string v3, "State["

    #@76
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v2

    #@7a
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v2

    #@7e
    const-string v3, "] [WAITING_BOOT_COMPLETE]"

    #@80
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v2

    #@84
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@87
    move-result-object v2

    #@88
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@8b
    .line 1681
    iput-object p1, p0, Lcom/lge/ims/BootupController;->mState:Lcom/lge/ims/BootupController$STATE;

    #@8d
    goto :goto_2b

    #@8e
    .line 1686
    :pswitch_8e
    const-string v1, "BootupController"

    #@90
    new-instance v2, Ljava/lang/StringBuilder;

    #@92
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@95
    const-string v3, "oldState["

    #@97
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v2

    #@9b
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v2

    #@9f
    const-string v3, "] ---> "

    #@a1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v2

    #@a5
    const-string v3, "State["

    #@a7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v2

    #@ab
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v2

    #@af
    const-string v3, "] [WAITING_NETWORK_CONNECTED]"

    #@b1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v2

    #@b5
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b8
    move-result-object v2

    #@b9
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@bc
    .line 1687
    iput-object p1, p0, Lcom/lge/ims/BootupController;->mState:Lcom/lge/ims/BootupController$STATE;

    #@be
    goto/16 :goto_2b

    #@c0
    .line 1692
    :pswitch_c0
    const-string v1, "BootupController"

    #@c2
    new-instance v2, Ljava/lang/StringBuilder;

    #@c4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@c7
    const-string v3, "oldState["

    #@c9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cc
    move-result-object v2

    #@cd
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d0
    move-result-object v2

    #@d1
    const-string v3, "] ---> "

    #@d3
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d6
    move-result-object v2

    #@d7
    const-string v3, "State["

    #@d9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v2

    #@dd
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e0
    move-result-object v2

    #@e1
    const-string v3, "] [UPDATING_AUTOCONFIG]"

    #@e3
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e6
    move-result-object v2

    #@e7
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ea
    move-result-object v2

    #@eb
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@ee
    .line 1693
    iput-object p1, p0, Lcom/lge/ims/BootupController;->mState:Lcom/lge/ims/BootupController$STATE;

    #@f0
    goto/16 :goto_2b

    #@f2
    .line 1698
    :pswitch_f2
    const-string v1, "BootupController"

    #@f4
    new-instance v2, Ljava/lang/StringBuilder;

    #@f6
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f9
    const-string v3, "oldState["

    #@fb
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fe
    move-result-object v2

    #@ff
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@102
    move-result-object v2

    #@103
    const-string v3, "] ---> "

    #@105
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@108
    move-result-object v2

    #@109
    const-string v3, "State["

    #@10b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10e
    move-result-object v2

    #@10f
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@112
    move-result-object v2

    #@113
    const-string v3, "] [WORKING_RCSE]"

    #@115
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@118
    move-result-object v2

    #@119
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11c
    move-result-object v2

    #@11d
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@120
    .line 1699
    iput-object p1, p0, Lcom/lge/ims/BootupController;->mState:Lcom/lge/ims/BootupController$STATE;

    #@122
    goto/16 :goto_2b

    #@124
    .line 1704
    :pswitch_124
    const-string v1, "BootupController"

    #@126
    new-instance v2, Ljava/lang/StringBuilder;

    #@128
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12b
    const-string v3, "oldState["

    #@12d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@130
    move-result-object v2

    #@131
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@134
    move-result-object v2

    #@135
    const-string v3, "] ---> "

    #@137
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13a
    move-result-object v2

    #@13b
    const-string v3, "State["

    #@13d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@140
    move-result-object v2

    #@141
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@144
    move-result-object v2

    #@145
    const-string v3, "] [WAITING_USER_ACTION]"

    #@147
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14a
    move-result-object v2

    #@14b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14e
    move-result-object v2

    #@14f
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@152
    .line 1705
    iput-object p1, p0, Lcom/lge/ims/BootupController;->mState:Lcom/lge/ims/BootupController$STATE;

    #@154
    goto/16 :goto_2b

    #@156
    .line 1710
    :pswitch_156
    const-string v1, "BootupController"

    #@158
    new-instance v2, Ljava/lang/StringBuilder;

    #@15a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@15d
    const-string v3, "oldState["

    #@15f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@162
    move-result-object v2

    #@163
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@166
    move-result-object v2

    #@167
    const-string v3, "] ---> "

    #@169
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16c
    move-result-object v2

    #@16d
    const-string v3, "State["

    #@16f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@172
    move-result-object v2

    #@173
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@176
    move-result-object v2

    #@177
    const-string v3, "] [WAITING_CLEANUP]"

    #@179
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17c
    move-result-object v2

    #@17d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@180
    move-result-object v2

    #@181
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@184
    .line 1711
    iput-object p1, p0, Lcom/lge/ims/BootupController;->mState:Lcom/lge/ims/BootupController$STATE;

    #@186
    goto/16 :goto_2b

    #@188
    .line 1671
    :pswitch_data_188
    .packed-switch 0x1
        :pswitch_2c
        :pswitch_8e
        :pswitch_5d
        :pswitch_c0
        :pswitch_f2
        :pswitch_124
        :pswitch_156
    .end packed-switch
.end method

.method private startAC()V
    .registers 4

    #@0
    .prologue
    .line 875
    const-string v1, "BootupController"

    #@2
    const-string v2, "Start AC service"

    #@4
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 877
    new-instance v0, Landroid/content/Intent;

    #@9
    const-string v1, "com.lge.ims.ac.action.AC_START"

    #@b
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@e
    .line 878
    .local v0, intent:Landroid/content/Intent;
    iget-object v1, p0, Lcom/lge/ims/BootupController;->mContext:Landroid/content/Context;

    #@10
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@13
    .line 879
    return-void
.end method

.method private startStarter()V
    .registers 4

    #@0
    .prologue
    .line 882
    const-string v1, "BootupController"

    #@2
    const-string v2, "Start Starter app"

    #@4
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 884
    new-instance v0, Landroid/content/Intent;

    #@9
    const-string v1, "com.lge.ims.action.STARTER"

    #@b
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@e
    .line 885
    .local v0, intent:Landroid/content/Intent;
    iget-object v1, p0, Lcom/lge/ims/BootupController;->mContext:Landroid/content/Context;

    #@10
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@13
    .line 886
    return-void
.end method

.method private tempInsertDefaultRcseDB()V
    .registers 12

    #@0
    .prologue
    .line 1618
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->getSubscriberID()Ljava/lang/String;

    #@3
    move-result-object v9

    #@4
    .line 1619
    .local v9, strIMSI:Ljava/lang/String;
    const/4 v6, 0x0

    #@5
    .line 1622
    .local v6, cursor:Landroid/database/Cursor;
    const/4 v8, 0x0

    #@6
    .line 1626
    .local v8, isTelURIPresent:Z
    :try_start_6
    iget-object v0, p0, Lcom/lge/ims/BootupController;->mContext:Landroid/content/Context;

    #@8
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@b
    move-result-object v0

    #@c
    new-instance v1, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v2, "content://com.lge.ims.provisioning/sims/"

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@22
    move-result-object v1

    #@23
    const/4 v2, 0x0

    #@24
    const/4 v3, 0x0

    #@25
    const/4 v4, 0x0

    #@26
    const/4 v5, 0x0

    #@27
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@2a
    move-result-object v6

    #@2b
    .line 1628
    if-nez v6, :cond_5a

    #@2d
    .line 1629
    const-string v0, "BootupController"

    #@2f
    new-instance v1, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v2, "Set Default Value IMSI: "

    #@36
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v1

    #@3a
    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v1

    #@3e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v1

    #@42
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@45
    .line 1630
    iget-object v0, p0, Lcom/lge/ims/BootupController;->mContext:Landroid/content/Context;

    #@47
    invoke-static {v0, v9}, Lcom/lge/ims/provisioning/ProvisioningHelper;->InsertDefaultProvisioningByIMSI(Landroid/content/Context;Ljava/lang/String;)Z

    #@4a
    .line 1631
    iget-object v0, p0, Lcom/lge/ims/BootupController;->mContext:Landroid/content/Context;

    #@4c
    invoke-static {v0, v9}, Lcom/lge/ims/provisioning/ProvisioningHelper;->InsertDefaultSettingByIMSI(Landroid/content/Context;Ljava/lang/String;)Z
    :try_end_4f
    .catchall {:try_start_6 .. :try_end_4f} :catchall_d0
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_4f} :catch_83

    #@4f
    .line 1656
    :cond_4f
    :goto_4f
    if-eqz v6, :cond_54

    #@51
    .line 1657
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@54
    .line 1662
    :cond_54
    :goto_54
    if-nez v8, :cond_59

    #@56
    .line 1663
    invoke-direct {p0}, Lcom/lge/ims/BootupController;->updateTelURI()V

    #@59
    .line 1666
    :cond_59
    return-void

    #@5a
    .line 1632
    :cond_5a
    :try_start_5a
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    #@5d
    move-result v0

    #@5e
    if-nez v0, :cond_8a

    #@60
    .line 1633
    const-string v0, "BootupController"

    #@62
    new-instance v1, Ljava/lang/StringBuilder;

    #@64
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@67
    const-string v2, "Set Default Value IMSI: "

    #@69
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v1

    #@6d
    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v1

    #@71
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v1

    #@75
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@78
    .line 1634
    iget-object v0, p0, Lcom/lge/ims/BootupController;->mContext:Landroid/content/Context;

    #@7a
    invoke-static {v0, v9}, Lcom/lge/ims/provisioning/ProvisioningHelper;->InsertDefaultProvisioningByIMSI(Landroid/content/Context;Ljava/lang/String;)Z

    #@7d
    .line 1635
    iget-object v0, p0, Lcom/lge/ims/BootupController;->mContext:Landroid/content/Context;

    #@7f
    invoke-static {v0, v9}, Lcom/lge/ims/provisioning/ProvisioningHelper;->InsertDefaultSettingByIMSI(Landroid/content/Context;Ljava/lang/String;)Z
    :try_end_82
    .catchall {:try_start_5a .. :try_end_82} :catchall_d0
    .catch Ljava/lang/Exception; {:try_start_5a .. :try_end_82} :catch_83

    #@82
    goto :goto_4f

    #@83
    .line 1653
    :catch_83
    move-exception v0

    #@84
    .line 1656
    if-eqz v6, :cond_54

    #@86
    .line 1657
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@89
    goto :goto_54

    #@8a
    .line 1637
    :cond_8a
    :try_start_8a
    const-string v0, "BootupController"

    #@8c
    new-instance v1, Ljava/lang/StringBuilder;

    #@8e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@91
    const-string v2, "Already Default value is set"

    #@93
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v1

    #@97
    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v1

    #@9b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9e
    move-result-object v1

    #@9f
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@a2
    .line 1640
    const-string v0, "tel_uri"

    #@a4
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@a7
    move-result v7

    #@a8
    .line 1642
    .local v7, index:I
    if-ltz v7, :cond_4f

    #@aa
    .line 1643
    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@ad
    move-result-object v10

    #@ae
    .line 1645
    .local v10, uri:Ljava/lang/String;
    if-eqz v10, :cond_b7

    #@b0
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    #@b3
    move-result v0

    #@b4
    if-lez v0, :cond_b7

    #@b6
    .line 1646
    const/4 v8, 0x1

    #@b7
    .line 1649
    :cond_b7
    const-string v0, "BootupController"

    #@b9
    new-instance v1, Ljava/lang/StringBuilder;

    #@bb
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@be
    const-string v2, "isTelURIPresent: "

    #@c0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v1

    #@c4
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v1

    #@c8
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cb
    move-result-object v1

    #@cc
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_cf
    .catchall {:try_start_8a .. :try_end_cf} :catchall_d0
    .catch Ljava/lang/Exception; {:try_start_8a .. :try_end_cf} :catch_83

    #@cf
    goto :goto_4f

    #@d0
    .line 1656
    .end local v7           #index:I
    .end local v10           #uri:Ljava/lang/String;
    :catchall_d0
    move-exception v0

    #@d1
    if-eqz v6, :cond_d6

    #@d3
    .line 1657
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@d6
    :cond_d6
    throw v0
.end method

.method private temp_isFakeNumber()Z
    .registers 2

    #@0
    .prologue
    .line 1583
    invoke-static {}, Lcom/lge/ims/Configuration;->isUsimOn()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method private temp_isTestbedServerAddress()Z
    .registers 12

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 1483
    const/4 v4, 0x0

    #@2
    .line 1484
    .local v4, imsDB:Landroid/database/sqlite/SQLiteDatabase;
    const/4 v1, 0x0

    #@3
    .line 1485
    .local v1, cursor:Landroid/database/Cursor;
    const/4 v5, 0x0

    #@4
    .line 1486
    .local v5, pcscfaddress0:Ljava/lang/String;
    const/4 v6, 0x0

    #@5
    .line 1489
    .local v6, pcscfaddress1:Ljava/lang/String;
    :try_start_5
    const-string v8, "/data/data/com.lge.ims/databases/lgims.db"

    #@7
    const/4 v9, 0x0

    #@8
    const/4 v10, 0x0

    #@9
    invoke-static {v8, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->openDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)Landroid/database/sqlite/SQLiteDatabase;
    :try_end_c
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_c} :catch_10

    #@c
    move-result-object v4

    #@d
    .line 1495
    if-nez v4, :cond_15

    #@f
    .line 1528
    :cond_f
    :goto_f
    return v7

    #@10
    .line 1490
    :catch_10
    move-exception v2

    #@11
    .line 1491
    .local v2, e:Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    #@14
    goto :goto_f

    #@15
    .line 1500
    .end local v2           #e:Landroid/database/sqlite/SQLiteException;
    :cond_15
    :try_start_15
    const-string v8, "select * from lgims_subscriber"

    #@17
    const/4 v9, 0x0

    #@18
    invoke-virtual {v4, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1b
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_15 .. :try_end_1b} :catch_3a

    #@1b
    move-result-object v1

    #@1c
    .line 1506
    :goto_1c
    if-eqz v1, :cond_f

    #@1e
    .line 1511
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    #@21
    .line 1513
    invoke-interface {v1}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    #@24
    move-result-object v0

    #@25
    .line 1515
    .local v0, columns:[Ljava/lang/String;
    const/4 v3, 0x0

    #@26
    .local v3, i:I
    :goto_26
    array-length v8, v0

    #@27
    if-ge v3, v8, :cond_4f

    #@29
    .line 1516
    const-string v8, "server_pcscf_0_address"

    #@2b
    aget-object v9, v0, v3

    #@2d
    invoke-virtual {v8, v9}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@30
    move-result v8

    #@31
    if-nez v8, :cond_40

    #@33
    .line 1517
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@36
    move-result-object v5

    #@37
    .line 1515
    :cond_37
    :goto_37
    add-int/lit8 v3, v3, 0x1

    #@39
    goto :goto_26

    #@3a
    .line 1501
    .end local v0           #columns:[Ljava/lang/String;
    .end local v3           #i:I
    :catch_3a
    move-exception v2

    #@3b
    .line 1502
    .restart local v2       #e:Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    #@3e
    .line 1503
    const/4 v1, 0x0

    #@3f
    goto :goto_1c

    #@40
    .line 1518
    .end local v2           #e:Landroid/database/sqlite/SQLiteException;
    .restart local v0       #columns:[Ljava/lang/String;
    .restart local v3       #i:I
    :cond_40
    const-string v8, "server_pcscf_1_address"

    #@42
    aget-object v9, v0, v3

    #@44
    invoke-virtual {v8, v9}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@47
    move-result v8

    #@48
    if-nez v8, :cond_37

    #@4a
    .line 1519
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@4d
    move-result-object v6

    #@4e
    goto :goto_37

    #@4f
    .line 1523
    :cond_4f
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    #@52
    .line 1524
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    #@55
    .line 1526
    const-string v8, "BootupController"

    #@57
    new-instance v9, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    const-string v10, "pcscfaddress0: "

    #@5e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v9

    #@62
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v9

    #@66
    const-string v10, " / pcscfaddress1: "

    #@68
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v9

    #@6c
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v9

    #@70
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@73
    move-result-object v9

    #@74
    invoke-static {v8, v9}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@77
    .line 1528
    const-string v8, "211.115.7.244"

    #@79
    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7c
    move-result v8

    #@7d
    if-nez v8, :cond_87

    #@7f
    const-string v8, "211.115.15.199"

    #@81
    invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@84
    move-result v8

    #@85
    if-eqz v8, :cond_f

    #@87
    :cond_87
    const/4 v7, 0x1

    #@88
    goto :goto_f
.end method

.method private updateTelURI()V
    .registers 13

    #@0
    .prologue
    .line 1723
    const/4 v10, 0x0

    #@1
    .line 1724
    .local v10, impu:Ljava/lang/String;
    const/4 v7, 0x0

    #@2
    .line 1727
    .local v7, c:Landroid/database/Cursor;
    :try_start_2
    iget-object v0, p0, Lcom/lge/ims/BootupController;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v0

    #@8
    sget-object v1, Lcom/lge/ims/provider/IMS$Subscriber;->CONTENT_URI:Landroid/net/Uri;

    #@a
    const/4 v2, 0x0

    #@b
    const/4 v3, 0x0

    #@c
    const/4 v4, 0x0

    #@d
    const/4 v5, 0x0

    #@e
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@11
    move-result-object v7

    #@12
    .line 1729
    if-eqz v7, :cond_26

    #@14
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    #@17
    move-result v0

    #@18
    if-eqz v0, :cond_26

    #@1a
    .line 1730
    const-string v0, "subscriber_0_impu_0"

    #@1c
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@1f
    move-result v11

    #@20
    .line 1732
    .local v11, index:I
    if-ltz v11, :cond_26

    #@22
    .line 1733
    invoke-interface {v7, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_25
    .catchall {:try_start_2 .. :try_end_25} :catchall_6a
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_25} :catch_5a

    #@25
    move-result-object v10

    #@26
    .line 1739
    .end local v11           #index:I
    :cond_26
    if-eqz v7, :cond_2b

    #@28
    .line 1740
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@2b
    .line 1744
    :cond_2b
    :goto_2b
    if-eqz v10, :cond_59

    #@2d
    .line 1745
    const/16 v0, 0x3a

    #@2f
    invoke-virtual {v10, v0}, Ljava/lang/String;->indexOf(I)I

    #@32
    move-result v8

    #@33
    .line 1746
    .local v8, colonIndex:I
    const/16 v0, 0x40

    #@35
    invoke-virtual {v10, v0}, Ljava/lang/String;->indexOf(I)I

    #@38
    move-result v6

    #@39
    .line 1748
    .local v6, atIndex:I
    add-int/lit8 v0, v8, 0x1

    #@3b
    invoke-virtual {v10, v0, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@3e
    move-result-object v10

    #@3f
    .line 1750
    if-eqz v10, :cond_59

    #@41
    .line 1751
    iget-object v0, p0, Lcom/lge/ims/BootupController;->mContext:Landroid/content/Context;

    #@43
    new-instance v1, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v2, "tel:"

    #@4a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v1

    #@4e
    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v1

    #@52
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v1

    #@56
    invoke-static {v0, v1}, Lcom/lge/ims/provisioning/ProvisioningHelper;->setTelUri(Landroid/content/Context;Ljava/lang/String;)Z

    #@59
    .line 1754
    .end local v6           #atIndex:I
    .end local v8           #colonIndex:I
    :cond_59
    return-void

    #@5a
    .line 1736
    :catch_5a
    move-exception v9

    #@5b
    .line 1737
    .local v9, e:Ljava/lang/Exception;
    :try_start_5b
    const-string v0, "BootupController"

    #@5d
    invoke-virtual {v9}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@60
    move-result-object v1

    #@61
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_64
    .catchall {:try_start_5b .. :try_end_64} :catchall_6a

    #@64
    .line 1739
    if-eqz v7, :cond_2b

    #@66
    .line 1740
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@69
    goto :goto_2b

    #@6a
    .line 1739
    .end local v9           #e:Ljava/lang/Exception;
    :catchall_6a
    move-exception v0

    #@6b
    if-eqz v7, :cond_70

    #@6d
    .line 1740
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@70
    :cond_70
    throw v0
.end method

.method private writeProvisioningDataToImsDB()Z
    .registers 16

    #@0
    .prologue
    const/4 v10, 0x1

    #@1
    const/4 v11, 0x0

    #@2
    .line 1311
    invoke-static {}, Lcom/lge/ims/Configuration;->isACOn()Z

    #@5
    move-result v12

    #@6
    if-nez v12, :cond_10

    #@8
    .line 1312
    const-string v11, "BootupController"

    #@a
    const-string v12, "writeProvisioningDataToImsDB :: AC is disabled"

    #@c
    invoke-static {v11, v12}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@f
    .line 1466
    :goto_f
    return v10

    #@10
    .line 1316
    :cond_10
    const/4 v2, 0x0

    #@11
    .line 1319
    .local v2, imsDB:Landroid/database/sqlite/SQLiteDatabase;
    :try_start_11
    const-string v12, "/data/data/com.lge.ims/databases/lgims.db"

    #@13
    const/4 v13, 0x0

    #@14
    const/4 v14, 0x0

    #@15
    invoke-static {v12, v13, v14}, Landroid/database/sqlite/SQLiteDatabase;->openDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)Landroid/database/sqlite/SQLiteDatabase;
    :try_end_18
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_11 .. :try_end_18} :catch_25e

    #@18
    move-result-object v2

    #@19
    .line 1326
    const/4 v0, 0x0

    #@1a
    .line 1327
    .local v0, affectedRows:I
    new-instance v7, Landroid/content/ContentValues;

    #@1c
    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    #@1f
    .line 1329
    .local v7, valuesForSubscriber:Landroid/content/ContentValues;
    const-string v11, "subscriber_0_impi"

    #@21
    iget-object v12, p0, Lcom/lge/ims/BootupController;->mPrivateUri:Ljava/lang/String;

    #@23
    invoke-virtual {v7, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@26
    .line 1330
    const-string v11, "subscriber_0_home_domain_name"

    #@28
    iget-object v12, p0, Lcom/lge/ims/BootupController;->mHomeDomainName:Ljava/lang/String;

    #@2a
    invoke-virtual {v7, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2d
    .line 1331
    const-string v11, "subscriber_0_impu_0"

    #@2f
    iget-object v12, p0, Lcom/lge/ims/BootupController;->mPublicUri:Ljava/lang/String;

    #@31
    invoke-virtual {v7, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@34
    .line 1332
    const-string v11, "subscriber_0_auth_realm"

    #@36
    iget-object v12, p0, Lcom/lge/ims/BootupController;->mRealm:Ljava/lang/String;

    #@38
    invoke-virtual {v7, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@3b
    .line 1333
    const-string v11, "subscriber_0_auth_username"

    #@3d
    iget-object v12, p0, Lcom/lge/ims/BootupController;->mUserName:Ljava/lang/String;

    #@3f
    invoke-virtual {v7, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@42
    .line 1334
    const-string v11, "subscriber_0_auth_password"

    #@44
    iget-object v12, p0, Lcom/lge/ims/BootupController;->mUserPwd:Ljava/lang/String;

    #@46
    invoke-virtual {v7, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@49
    .line 1343
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    #@4c
    .line 1346
    :try_start_4c
    const-string v11, "lgims_subscriber"

    #@4e
    const-string v12, "id = \'1\'"

    #@50
    const/4 v13, 0x0

    #@51
    invoke-virtual {v2, v11, v7, v12, v13}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@54
    move-result v0

    #@55
    .line 1347
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_58
    .catchall {:try_start_4c .. :try_end_58} :catchall_265

    #@58
    .line 1349
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@5b
    .line 1352
    const-string v11, "BootupController"

    #@5d
    new-instance v12, Ljava/lang/StringBuilder;

    #@5f
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@62
    const-string v13, "Subscriber :: updated row count: "

    #@64
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v12

    #@68
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v12

    #@6c
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6f
    move-result-object v12

    #@70
    invoke-static {v11, v12}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@73
    .line 1355
    new-instance v6, Landroid/content/ContentValues;

    #@75
    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    #@78
    .line 1357
    .local v6, valuesForSIP:Landroid/content/ContentValues;
    const-string v11, "timer_tv_t1"

    #@7a
    iget v12, p0, Lcom/lge/ims/BootupController;->mTimerT1:I

    #@7c
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@7f
    move-result-object v12

    #@80
    invoke-virtual {v6, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@83
    .line 1358
    const-string v11, "timer_tv_t2"

    #@85
    iget v12, p0, Lcom/lge/ims/BootupController;->mTimerT2:I

    #@87
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8a
    move-result-object v12

    #@8b
    invoke-virtual {v6, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@8e
    .line 1359
    const-string v11, "timer_tv_tb"

    #@90
    iget v12, p0, Lcom/lge/ims/BootupController;->mTimerTb:I

    #@92
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@95
    move-result-object v12

    #@96
    invoke-virtual {v6, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@99
    .line 1360
    const-string v11, "timer_tv_td"

    #@9b
    iget v12, p0, Lcom/lge/ims/BootupController;->mTimerTd:I

    #@9d
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a0
    move-result-object v12

    #@a1
    invoke-virtual {v6, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@a4
    .line 1361
    const-string v11, "timer_tv_tf"

    #@a6
    iget v12, p0, Lcom/lge/ims/BootupController;->mTimerTf:I

    #@a8
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ab
    move-result-object v12

    #@ac
    invoke-virtual {v6, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@af
    .line 1362
    const-string v11, "timer_tv_th"

    #@b1
    iget v12, p0, Lcom/lge/ims/BootupController;->mTimerTh:I

    #@b3
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@b6
    move-result-object v12

    #@b7
    invoke-virtual {v6, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@ba
    .line 1363
    const-string v11, "timer_tv_ti"

    #@bc
    iget v12, p0, Lcom/lge/ims/BootupController;->mTimerTi:I

    #@be
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c1
    move-result-object v12

    #@c2
    invoke-virtual {v6, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@c5
    .line 1364
    const-string v11, "timer_tv_tj"

    #@c7
    iget v12, p0, Lcom/lge/ims/BootupController;->mTimerTj:I

    #@c9
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@cc
    move-result-object v12

    #@cd
    invoke-virtual {v6, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@d0
    .line 1365
    const-string v11, "timer_tv_tk"

    #@d2
    iget v12, p0, Lcom/lge/ims/BootupController;->mTimerTk:I

    #@d4
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@d7
    move-result-object v12

    #@d8
    invoke-virtual {v6, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@db
    .line 1367
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    #@de
    .line 1370
    :try_start_de
    const-string v11, "lgims_com_kt_sip"

    #@e0
    const-string v12, "id = \'1\'"

    #@e2
    const/4 v13, 0x0

    #@e3
    invoke-virtual {v2, v11, v6, v12, v13}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@e6
    move-result v0

    #@e7
    .line 1371
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_ea
    .catchall {:try_start_de .. :try_end_ea} :catchall_26a

    #@ea
    .line 1373
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@ed
    .line 1376
    const-string v11, "BootupController"

    #@ef
    new-instance v12, Ljava/lang/StringBuilder;

    #@f1
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@f4
    const-string v13, "SIP :: updated row count: "

    #@f6
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f9
    move-result-object v12

    #@fa
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@fd
    move-result-object v12

    #@fe
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@101
    move-result-object v12

    #@102
    invoke-static {v11, v12}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@105
    .line 1379
    new-instance v8, Landroid/content/ContentValues;

    #@107
    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    #@10a
    .line 1381
    .local v8, valuesForVtAudio:Landroid/content/ContentValues;
    const-string v11, "audio_0_port_rtp"

    #@10c
    iget v12, p0, Lcom/lge/ims/BootupController;->mAudioPort:I

    #@10e
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@111
    move-result-object v12

    #@112
    invoke-virtual {v8, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@115
    .line 1382
    const-string v11, "audio_0_port_rtcp"

    #@117
    iget v12, p0, Lcom/lge/ims/BootupController;->mAudioPort:I

    #@119
    add-int/lit8 v12, v12, 0x1

    #@11b
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11e
    move-result-object v12

    #@11f
    invoke-virtual {v8, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@122
    .line 1384
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    #@125
    .line 1387
    :try_start_125
    const-string v11, "lgims_com_kt_media_uc_audio"

    #@127
    const-string v12, "id = \'1\'"

    #@129
    const/4 v13, 0x0

    #@12a
    invoke-virtual {v2, v11, v8, v12, v13}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@12d
    move-result v0

    #@12e
    .line 1388
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_131
    .catchall {:try_start_125 .. :try_end_131} :catchall_26f

    #@131
    .line 1390
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@134
    .line 1393
    const-string v11, "BootupController"

    #@136
    new-instance v12, Ljava/lang/StringBuilder;

    #@138
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@13b
    const-string v13, "VT Audio :: updated row count: "

    #@13d
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@140
    move-result-object v12

    #@141
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@144
    move-result-object v12

    #@145
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@148
    move-result-object v12

    #@149
    invoke-static {v11, v12}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@14c
    .line 1396
    new-instance v9, Landroid/content/ContentValues;

    #@14e
    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    #@151
    .line 1398
    .local v9, valuesForVtVideo:Landroid/content/ContentValues;
    const-string v11, "video_0_port_rtp"

    #@153
    iget v12, p0, Lcom/lge/ims/BootupController;->mVideoPort:I

    #@155
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@158
    move-result-object v12

    #@159
    invoke-virtual {v9, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@15c
    .line 1399
    const-string v11, "video_0_port_rtcp"

    #@15e
    iget v12, p0, Lcom/lge/ims/BootupController;->mVideoPort:I

    #@160
    add-int/lit8 v12, v12, 0x1

    #@162
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@165
    move-result-object v12

    #@166
    invoke-virtual {v9, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@169
    .line 1401
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    #@16c
    .line 1404
    :try_start_16c
    const-string v11, "lgims_com_kt_media_uc_video"

    #@16e
    const-string v12, "id = \'1\'"

    #@170
    const/4 v13, 0x0

    #@171
    invoke-virtual {v2, v11, v9, v12, v13}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@174
    move-result v0

    #@175
    .line 1405
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_178
    .catchall {:try_start_16c .. :try_end_178} :catchall_274

    #@178
    .line 1407
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@17b
    .line 1410
    const-string v11, "BootupController"

    #@17d
    new-instance v12, Ljava/lang/StringBuilder;

    #@17f
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@182
    const-string v13, "VT Video :: updated row count: "

    #@184
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@187
    move-result-object v12

    #@188
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18b
    move-result-object v12

    #@18c
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18f
    move-result-object v12

    #@190
    invoke-static {v11, v12}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@193
    .line 1413
    new-instance v4, Landroid/content/ContentValues;

    #@195
    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    #@198
    .line 1415
    .local v4, valuesForRcsIp:Landroid/content/ContentValues;
    const-string v11, "application_client_object_data_limit"

    #@19a
    iget-object v12, p0, Lcom/lge/ims/BootupController;->mClientObjDataLimit:Ljava/lang/String;

    #@19c
    invoke-virtual {v4, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@19f
    .line 1416
    const-string v11, "application_content_server_uri"

    #@1a1
    iget-object v12, p0, Lcom/lge/ims/BootupController;->mPresenceContentServerUri:Ljava/lang/String;

    #@1a3
    invoke-virtual {v4, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1a6
    .line 1417
    const-string v11, "application_source_throttle_publish"

    #@1a8
    iget-object v12, p0, Lcom/lge/ims/BootupController;->mSourceThrottlePublish:Ljava/lang/String;

    #@1aa
    invoke-virtual {v4, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1ad
    .line 1418
    const-string v11, "application_max_backend_subscription"

    #@1af
    iget v12, p0, Lcom/lge/ims/BootupController;->mMaxNumberOfSubscriptionsInPresenceList:I

    #@1b1
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1b4
    move-result-object v12

    #@1b5
    invoke-virtual {v4, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@1b8
    .line 1420
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    #@1bb
    .line 1423
    :try_start_1bb
    const-string v11, "lgims_com_kt_service_rcse_ip"

    #@1bd
    const-string v12, "id = \'1\'"

    #@1bf
    const/4 v13, 0x0

    #@1c0
    invoke-virtual {v2, v11, v4, v12, v13}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@1c3
    move-result v0

    #@1c4
    .line 1424
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1c7
    .catchall {:try_start_1bb .. :try_end_1c7} :catchall_279

    #@1c7
    .line 1426
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@1ca
    .line 1429
    const-string v11, "BootupController"

    #@1cc
    new-instance v12, Ljava/lang/StringBuilder;

    #@1ce
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@1d1
    const-string v13, "RCS IP :: updated row count: "

    #@1d3
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d6
    move-result-object v12

    #@1d7
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1da
    move-result-object v12

    #@1db
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1de
    move-result-object v12

    #@1df
    invoke-static {v11, v12}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@1e2
    .line 1432
    new-instance v5, Landroid/content/ContentValues;

    #@1e4
    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    #@1e7
    .line 1434
    .local v5, valuesForRcsIpXdm:Landroid/content/ContentValues;
    const-string v11, "resource_root_uri"

    #@1e9
    iget-object v12, p0, Lcom/lge/ims/BootupController;->mXCAPRootURI:Ljava/lang/String;

    #@1eb
    invoke-virtual {v5, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1ee
    .line 1436
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    #@1f1
    .line 1439
    :try_start_1f1
    const-string v11, "lgims_com_kt_service_rcse_ipxdm"

    #@1f3
    const-string v12, "id = \'1\'"

    #@1f5
    const/4 v13, 0x0

    #@1f6
    invoke-virtual {v2, v11, v5, v12, v13}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@1f9
    move-result v0

    #@1fa
    .line 1440
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1fd
    .catchall {:try_start_1f1 .. :try_end_1fd} :catchall_27e

    #@1fd
    .line 1442
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@200
    .line 1445
    const-string v11, "BootupController"

    #@202
    new-instance v12, Ljava/lang/StringBuilder;

    #@204
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@207
    const-string v13, "RCS IP XDM :: updated row count: "

    #@209
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20c
    move-result-object v12

    #@20d
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@210
    move-result-object v12

    #@211
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@214
    move-result-object v12

    #@215
    invoke-static {v11, v12}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@218
    .line 1448
    new-instance v3, Landroid/content/ContentValues;

    #@21a
    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    #@21d
    .line 1450
    .local v3, valuesForRcsIm:Landroid/content/ContentValues;
    const-string v11, "application_max_adhoc_group"

    #@21f
    iget v12, p0, Lcom/lge/ims/BootupController;->mMaxAdhocGroupSize:I

    #@221
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@224
    move-result-object v12

    #@225
    invoke-virtual {v3, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@228
    .line 1451
    const-string v11, "application_conf_factory_uri"

    #@22a
    iget-object v12, p0, Lcom/lge/ims/BootupController;->mConferenceFactoryUri:Ljava/lang/String;

    #@22c
    invoke-virtual {v3, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@22f
    .line 1453
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    #@232
    .line 1456
    :try_start_232
    const-string v11, "lgims_com_kt_service_rcse_im"

    #@234
    const-string v12, "id = \'1\'"

    #@236
    const/4 v13, 0x0

    #@237
    invoke-virtual {v2, v11, v3, v12, v13}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@23a
    move-result v0

    #@23b
    .line 1457
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_23e
    .catchall {:try_start_232 .. :try_end_23e} :catchall_283

    #@23e
    .line 1459
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@241
    .line 1462
    const-string v11, "BootupController"

    #@243
    new-instance v12, Ljava/lang/StringBuilder;

    #@245
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@248
    const-string v13, "RCS IM :: updated row count: "

    #@24a
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24d
    move-result-object v12

    #@24e
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@251
    move-result-object v12

    #@252
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@255
    move-result-object v12

    #@256
    invoke-static {v11, v12}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@259
    .line 1464
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    #@25c
    goto/16 :goto_f

    #@25e
    .line 1320
    .end local v0           #affectedRows:I
    .end local v3           #valuesForRcsIm:Landroid/content/ContentValues;
    .end local v4           #valuesForRcsIp:Landroid/content/ContentValues;
    .end local v5           #valuesForRcsIpXdm:Landroid/content/ContentValues;
    .end local v6           #valuesForSIP:Landroid/content/ContentValues;
    .end local v7           #valuesForSubscriber:Landroid/content/ContentValues;
    .end local v8           #valuesForVtAudio:Landroid/content/ContentValues;
    .end local v9           #valuesForVtVideo:Landroid/content/ContentValues;
    :catch_25e
    move-exception v1

    #@25f
    .line 1321
    .local v1, e:Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    #@262
    move v10, v11

    #@263
    .line 1322
    goto/16 :goto_f

    #@265
    .line 1349
    .end local v1           #e:Landroid/database/sqlite/SQLiteException;
    .restart local v0       #affectedRows:I
    .restart local v7       #valuesForSubscriber:Landroid/content/ContentValues;
    :catchall_265
    move-exception v10

    #@266
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@269
    throw v10

    #@26a
    .line 1373
    .restart local v6       #valuesForSIP:Landroid/content/ContentValues;
    :catchall_26a
    move-exception v10

    #@26b
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@26e
    throw v10

    #@26f
    .line 1390
    .restart local v8       #valuesForVtAudio:Landroid/content/ContentValues;
    :catchall_26f
    move-exception v10

    #@270
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@273
    throw v10

    #@274
    .line 1407
    .restart local v9       #valuesForVtVideo:Landroid/content/ContentValues;
    :catchall_274
    move-exception v10

    #@275
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@278
    throw v10

    #@279
    .line 1426
    .restart local v4       #valuesForRcsIp:Landroid/content/ContentValues;
    :catchall_279
    move-exception v10

    #@27a
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@27d
    throw v10

    #@27e
    .line 1442
    .restart local v5       #valuesForRcsIpXdm:Landroid/content/ContentValues;
    :catchall_27e
    move-exception v10

    #@27f
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@282
    throw v10

    #@283
    .line 1459
    .restart local v3       #valuesForRcsIm:Landroid/content/ContentValues;
    :catchall_283
    move-exception v10

    #@284
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@287
    throw v10
.end method
