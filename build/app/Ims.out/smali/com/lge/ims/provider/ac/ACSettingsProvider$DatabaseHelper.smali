.class Lcom/lge/ims/provider/ac/ACSettingsProvider$DatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "ACSettingsProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/provider/ac/ACSettingsProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DatabaseHelper"
.end annotation


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V
    .registers 5
    .parameter "context"
    .parameter "name"
    .parameter "factory"
    .parameter "version"

    #@0
    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    #@3
    .line 52
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 3
    .parameter "db"

    #@0
    .prologue
    .line 57
    const-string v0, "CREATE TABLE common (availability TEXT,validity INTEGER,update_exception_list TEXT);"

    #@2
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@5
    .line 62
    const-string v0, "INSERT into common (availability,validity,update_exception_list )VALUES (\'true\', \'0\', \'0x00000000\');"

    #@7
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@a
    .line 72
    const-string v0, "CREATE TABLE subscriber (usim TEXT,phone_number TEXT,imsi TEXT);"

    #@c
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@f
    .line 77
    const-string v0, "INSERT into subscriber (usim,phone_number,imsi )VALUES (\'true\', \'\', \'\');"

    #@11
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@14
    .line 87
    const-string v0, "CREATE TABLE connection_server (server_selection INTEGER,commercial_network TEXT,testbed TEXT);"

    #@16
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@19
    .line 93
    const-string v0, "INSERT into connection_server (server_selection,commercial_network,testbed )VALUES (\'0\', \'https://config.rcs.mnc008.mcc450.pub.3gppnetwork.org:9443\', \'https://221.148.247.125:9443\');"

    #@1b
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@1e
    .line 101
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .registers 7
    .parameter "db"
    .parameter "oldVersion"
    .parameter "newVersion"

    #@0
    .prologue
    .line 105
    const-string v0, "ACSettingsProvider"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Upgrading database from version "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, " to "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    const-string v2, ", which will destroy all old data"

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@28
    .line 108
    const-string v0, "DROP TABLE IF EXISTS common"

    #@2a
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@2d
    .line 109
    const-string v0, "DROP TABLE IF EXISTS subscriber"

    #@2f
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@32
    .line 110
    const-string v0, "DROP TABLE IF EXISTS connection_server"

    #@34
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@37
    .line 111
    invoke-virtual {p0, p1}, Lcom/lge/ims/provider/ac/ACSettingsProvider$DatabaseHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    #@3a
    .line 112
    return-void
.end method
