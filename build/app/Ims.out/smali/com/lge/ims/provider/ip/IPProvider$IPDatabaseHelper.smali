.class Lcom/lge/ims/provider/ip/IPProvider$IPDatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "IPProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/provider/ip/IPProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "IPDatabaseHelper"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "objContext"

    #@0
    .prologue
    .line 164
    const-string v0, "ims_ip.db"

    #@2
    const/4 v1, 0x0

    #@3
    const/16 v2, 0xe

    #@5
    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    #@8
    .line 165
    return-void
.end method


# virtual methods
.method CreateTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 3
    .parameter "objDb"

    #@0
    .prologue
    .line 275
    const-string v0, "CREATE TABLE IF NOT EXISTS ip_capa(_id INTEGER PRIMARY KEY AUTOINCREMENT, data_id INTEGER, msisdn TEXT, nor_msisdn TEXT, rcs_status INTEGER,im_status INTEGER,ft_status INTEGER,time_stamp INTEGER,capa_type INTEGER,first_time_rcs INTEGER,mim_status INTEGER,presence_status INTEGER,uri_id INTEGER,http_status INTEGER,response_code INTEGER,rcs_user_list INTEGER,nonrcs_user_list INTEGER,image_uri_id INTEGER,raw_contact_id INTEGER,photo_update INTEGER,display_name TEXT);"

    #@2
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@5
    .line 298
    const-string v0, "CREATE TABLE IF NOT EXISTS ip_presence(_id INTEGER PRIMARY KEY AUTOINCREMENT, nor_msisdn TEXT, homepage TEXT,free_text TEXT,e_mail TEXT,birthday TEXT,status INTEGER,twitter_account TEXT,facebook_account TEXT,cyworld_account TEXT,waggle_account TEXT,status_icon TEXT,status_icon_link TEXT,status_icon_thumb BLOB,status_icon_thumb_link TEXT,status_icon_thumb_etag TEXT,time_stamp INTEGER,statusicon_update INTEGER);"

    #@7
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@a
    .line 318
    const-string v0, "CREATE TABLE IF NOT EXISTS ip_mystatus(homepage TEXT,free_text TEXT,e_mail TEXT,birthday TEXT,status INTEGER,twitter_account TEXT,facebook_account TEXT,cyworld_account TEXT,waggle_account TEXT,status_icon TEXT,status_icon_link TEXT,status_icon_thumb BLOB,status_icon_thumb_link TEXT,status_icon_thumb_etag TEXT,display_name TEXT);"

    #@c
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@f
    .line 335
    const-string v0, "CREATE TABLE IF NOT EXISTS ip_provisioning(latest_polling INTEGER,latest_rlssub_polling INTEGER,excess_max_user INTEGER);"

    #@11
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@14
    .line 340
    const-string v0, "CREATE TABLE IF NOT EXISTS ip_buddyimage(_id INTEGER PRIMARY KEY AUTOINCREMENT, nor_msisdn TEXT, file_path TEXT,time_stamp INTEGER);"

    #@16
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@19
    .line 345
    return-void
.end method

.method UpgradeDatabaseToVersion2(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 3
    .parameter "objDb"

    #@0
    .prologue
    .line 266
    const-string v0, "DROP TABLE IF EXISTS ip_capa"

    #@2
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@5
    .line 267
    const-string v0, "DROP TABLE IF EXISTS ip_presence"

    #@7
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@a
    .line 268
    const-string v0, "DROP TABLE IF EXISTS ip_mystatus"

    #@c
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@f
    .line 269
    const-string v0, "DROP TABLE IF EXISTS ip_provisioning"

    #@11
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@14
    .line 270
    const-string v0, "DROP TABLE IF EXISTS ip_buddyimage"

    #@16
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@19
    .line 271
    invoke-virtual {p0, p1}, Lcom/lge/ims/provider/ip/IPProvider$IPDatabaseHelper;->CreateTable(Landroid/database/sqlite/SQLiteDatabase;)V

    #@1c
    .line 272
    return-void
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 3
    .parameter "objDb"

    #@0
    .prologue
    .line 169
    const-string v0, "CREATE TABLE IF NOT EXISTS ip_capa(_id INTEGER PRIMARY KEY AUTOINCREMENT, data_id INTEGER, msisdn TEXT, nor_msisdn TEXT, rcs_status INTEGER,im_status INTEGER,ft_status INTEGER,time_stamp INTEGER,capa_type INTEGER,first_time_rcs INTEGER,mim_status INTEGER,presence_status INTEGER,uri_id INTEGER,http_status INTEGER,response_code INTEGER,rcs_user_list INTEGER,nonrcs_user_list INTEGER,image_uri_id INTEGER,raw_contact_id INTEGER,photo_update INTEGER,display_name TEXT);"

    #@2
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@5
    .line 192
    const-string v0, "CREATE TABLE IF NOT EXISTS ip_presence(_id INTEGER PRIMARY KEY AUTOINCREMENT, nor_msisdn TEXT, homepage TEXT,free_text TEXT,e_mail TEXT,birthday TEXT,status INTEGER,twitter_account TEXT,facebook_account TEXT,cyworld_account TEXT,waggle_account TEXT,status_icon TEXT,status_icon_link TEXT,status_icon_thumb BLOB,status_icon_thumb_link TEXT,status_icon_thumb_etag TEXT,time_stamp INTEGER,statusicon_update INTEGER);"

    #@7
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@a
    .line 213
    const-string v0, "CREATE TABLE IF NOT EXISTS ip_mystatus(homepage TEXT,free_text TEXT,e_mail TEXT,birthday TEXT,status INTEGER,twitter_account TEXT,facebook_account TEXT,cyworld_account TEXT,waggle_account TEXT,status_icon TEXT,status_icon_link TEXT,status_icon_thumb BLOB,status_icon_thumb_link TEXT,status_icon_thumb_etag TEXT,display_name TEXT);"

    #@c
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@f
    .line 230
    const-string v0, "CREATE TABLE IF NOT EXISTS ip_provisioning(latest_polling INTEGER,latest_rlssub_polling INTEGER,excess_max_user INTEGER);"

    #@11
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@14
    .line 235
    const-string v0, "CREATE TABLE IF NOT EXISTS ip_buddyimage(_id INTEGER PRIMARY KEY AUTOINCREMENT, nor_msisdn TEXT, file_path TEXT,time_stamp INTEGER);"

    #@16
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@19
    .line 240
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .registers 6
    .parameter "objDb"
    .parameter "nOldVersion"
    .parameter "nNewVersion"

    #@0
    .prologue
    .line 244
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "IPDatabaseHelper::onUpgrade : Old : "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    const-string v1, " / New : "

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@20
    .line 246
    if-nez p1, :cond_23

    #@22
    .line 263
    :cond_22
    :goto_22
    return-void

    #@23
    .line 250
    :cond_23
    if-ge p2, p3, :cond_22

    #@25
    .line 260
    invoke-virtual {p0, p1}, Lcom/lge/ims/provider/ip/IPProvider$IPDatabaseHelper;->UpgradeDatabaseToVersion2(Landroid/database/sqlite/SQLiteDatabase;)V

    #@28
    goto :goto_22
.end method
