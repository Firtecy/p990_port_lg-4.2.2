.class public Lcom/lge/ims/provider/im/ConfigIM;
.super Ljava/lang/Object;
.source "ConfigIM.java"

# interfaces
.implements Lcom/lge/ims/provider/IConfiguration;


# static fields
.field private static final APP:[[Ljava/lang/String;

.field private static final HTTP:[[Ljava/lang/String;

.field private static final SERVICE:[[Ljava/lang/String;

.field private static final TABLES:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 9

    #@0
    .prologue
    const/4 v8, 0x4

    #@1
    const/4 v7, 0x3

    #@2
    const/4 v6, 0x2

    #@3
    const/4 v5, 0x1

    #@4
    const/4 v4, 0x0

    #@5
    .line 20
    new-array v0, v7, [Ljava/lang/String;

    #@7
    const-string v1, "lgims_com_kt_app_rcse_im"

    #@9
    aput-object v1, v0, v4

    #@b
    const-string v1, "lgims_com_kt_service_rcse_im"

    #@d
    aput-object v1, v0, v5

    #@f
    const-string v1, "lgims_com_kt_http_rcse_im"

    #@11
    aput-object v1, v0, v6

    #@13
    sput-object v0, Lcom/lge/ims/provider/im/ConfigIM;->TABLES:[Ljava/lang/String;

    #@15
    .line 29
    new-array v0, v7, [[Ljava/lang/String;

    #@17
    new-array v1, v7, [Ljava/lang/String;

    #@19
    const-string v2, "INTEGER"

    #@1b
    aput-object v2, v1, v4

    #@1d
    const-string v2, "id"

    #@1f
    aput-object v2, v1, v5

    #@21
    const-string v2, "1"

    #@23
    aput-object v2, v1, v6

    #@25
    aput-object v1, v0, v4

    #@27
    new-array v1, v7, [Ljava/lang/String;

    #@29
    const-string v2, "INTEGER"

    #@2b
    aput-object v2, v1, v4

    #@2d
    const-string v2, "property"

    #@2f
    aput-object v2, v1, v5

    #@31
    const-string v2, "0"

    #@33
    aput-object v2, v1, v6

    #@35
    aput-object v1, v0, v5

    #@37
    new-array v1, v7, [Ljava/lang/String;

    #@39
    const-string v2, "TEXT"

    #@3b
    aput-object v2, v1, v4

    #@3d
    const-string v2, "conf"

    #@3f
    aput-object v2, v1, v5

    #@41
    const-string v2, "[Uniqueness]\nStream=0\nFramed=1\nBasic=0\nEvent=1\nCoreService=2\nQos=0\nReg=2\nWrite=1\nRead=1\nCap=0\nMprof=1\nConnection=0\nIMService=1\nPresenceService=0\nXDMService=0\n\n[IMSRegistry]\nIMSRegistry=lgims.com.kt.app.rcse.im\n\n[Framed]\nmedia_types=text/plain text/html image/jpeg image/gif image/bmp image/png\nmax_size=4096\n\n[Event]\npackage_names=conference\n\n[CoreService_0]\nservice_id=lgims.com.kt.service.rcse.im\niari=\nicsi=\nfeature_tag=+g.oma.sip-im;require;explicit\n\n[CoreService_1]\nservice_id=lgims.com.kt.service.rcse.im.wifi\niari=\nicsi=\nfeature_tag=+g.oma.sip-im;require;explicit\n\n[Reg_0]\nservice_id=lgims.com.kt.service.rcse.im\nheader_count=1\nheader_0=Supported: path\n\n[Reg_1]\nservice_id=lgims.com.kt.service.rcse.im.wifi\nheader_count=1\nheader_0=Supported: path\n\n[Write]\nheader_names=Accept Content-Disposition Content-Type Refer-Sub Require Subject Supported Contribution-ID Session-Replaces\n\n[Read]\nheader_names=Contact Content-Type From Reason Replaces Server Subject Supported Contribution-ID Session-Replaces\n\n[Mprof_0]\nservice_id=lgims.com.kt.service.rcse.im\nprofile=com.kt.media.im\n\n[IMService]\niari=\nmedia_types=text/plain text/html message/cpim application/im-iscomposing+xml image/jpeg image/gif image/bmp image/png video/mp4\nmax_size=102400\nservice_id=lgims.com.kt.service.rcse.im\n\n"

    #@43
    aput-object v2, v1, v6

    #@45
    aput-object v1, v0, v6

    #@47
    sput-object v0, Lcom/lge/ims/provider/im/ConfigIM;->APP:[[Ljava/lang/String;

    #@49
    .line 104
    const/16 v0, 0x8

    #@4b
    new-array v0, v0, [[Ljava/lang/String;

    #@4d
    new-array v1, v7, [Ljava/lang/String;

    #@4f
    const-string v2, "INTEGER"

    #@51
    aput-object v2, v1, v4

    #@53
    const-string v2, "id"

    #@55
    aput-object v2, v1, v5

    #@57
    const-string v2, "1"

    #@59
    aput-object v2, v1, v6

    #@5b
    aput-object v1, v0, v4

    #@5d
    new-array v1, v7, [Ljava/lang/String;

    #@5f
    const-string v2, "INTEGER"

    #@61
    aput-object v2, v1, v4

    #@63
    const-string v2, "property"

    #@65
    aput-object v2, v1, v5

    #@67
    const-string v2, "1"

    #@69
    aput-object v2, v1, v6

    #@6b
    aput-object v1, v0, v5

    #@6d
    new-array v1, v7, [Ljava/lang/String;

    #@6f
    const-string v2, "INTEGER"

    #@71
    aput-object v2, v1, v4

    #@73
    const-string v2, "application_max_adhoc_group"

    #@75
    aput-object v2, v1, v5

    #@77
    const-string v2, "8"

    #@79
    aput-object v2, v1, v6

    #@7b
    aput-object v1, v0, v6

    #@7d
    new-array v1, v7, [Ljava/lang/String;

    #@7f
    const-string v2, "TEXT"

    #@81
    aput-object v2, v1, v4

    #@83
    const-string v2, "application_conf_factory_uri"

    #@85
    aput-object v2, v1, v5

    #@87
    const-string v2, "sip:conference-factory@ims.kt.com"

    #@89
    aput-object v2, v1, v6

    #@8b
    aput-object v1, v0, v7

    #@8d
    new-array v1, v7, [Ljava/lang/String;

    #@8f
    const-string v2, "TEXT"

    #@91
    aput-object v2, v1, v4

    #@93
    const-string v2, "application_exploder_uri"

    #@95
    aput-object v2, v1, v5

    #@97
    const-string v2, "sip:im-exploder-uri@ims.kt.com"

    #@99
    aput-object v2, v1, v6

    #@9b
    aput-object v1, v0, v8

    #@9d
    const/4 v1, 0x5

    #@9e
    new-array v2, v7, [Ljava/lang/String;

    #@a0
    const-string v3, "TEXT"

    #@a2
    aput-object v3, v2, v4

    #@a4
    const-string v3, "application_conv_history_func_uri"

    #@a6
    aput-object v3, v2, v5

    #@a8
    const-string v3, "sip:im-history-server@ims.kt.com"

    #@aa
    aput-object v3, v2, v6

    #@ac
    aput-object v2, v0, v1

    #@ae
    const/4 v1, 0x6

    #@af
    new-array v2, v7, [Ljava/lang/String;

    #@b1
    const-string v3, "TEXT"

    #@b3
    aput-object v3, v2, v4

    #@b5
    const-string v3, "application_deferred_msg_func_uri"

    #@b7
    aput-object v3, v2, v5

    #@b9
    const-string v3, "sip:deferred-server@ims.kt.com"

    #@bb
    aput-object v3, v2, v6

    #@bd
    aput-object v2, v0, v1

    #@bf
    const/4 v1, 0x7

    #@c0
    new-array v2, v7, [Ljava/lang/String;

    #@c2
    const-string v3, "TEXT"

    #@c4
    aput-object v3, v2, v4

    #@c6
    const-string v3, "ft_http_config"

    #@c8
    aput-object v3, v2, v5

    #@ca
    const-string v3, "lgims.com.kt.http.rcse.im"

    #@cc
    aput-object v3, v2, v6

    #@ce
    aput-object v2, v0, v1

    #@d0
    sput-object v0, Lcom/lge/ims/provider/im/ConfigIM;->SERVICE:[[Ljava/lang/String;

    #@d2
    .line 119
    new-array v0, v8, [[Ljava/lang/String;

    #@d4
    new-array v1, v7, [Ljava/lang/String;

    #@d6
    const-string v2, "INTEGER"

    #@d8
    aput-object v2, v1, v4

    #@da
    const-string v2, "id"

    #@dc
    aput-object v2, v1, v5

    #@de
    const-string v2, "1"

    #@e0
    aput-object v2, v1, v6

    #@e2
    aput-object v1, v0, v4

    #@e4
    new-array v1, v7, [Ljava/lang/String;

    #@e6
    const-string v2, "INTEGER"

    #@e8
    aput-object v2, v1, v4

    #@ea
    const-string v2, "property"

    #@ec
    aput-object v2, v1, v5

    #@ee
    const-string v2, "1"

    #@f0
    aput-object v2, v1, v6

    #@f2
    aput-object v1, v0, v5

    #@f4
    new-array v1, v7, [Ljava/lang/String;

    #@f6
    const-string v2, "TEXT"

    #@f8
    aput-object v2, v1, v4

    #@fa
    const-string v2, "server_info_proxy_address"

    #@fc
    aput-object v2, v1, v5

    #@fe
    const-string v2, "221.148.247.125"

    #@100
    aput-object v2, v1, v6

    #@102
    aput-object v1, v0, v6

    #@104
    new-array v1, v7, [Ljava/lang/String;

    #@106
    const-string v2, "INTEGER"

    #@108
    aput-object v2, v1, v4

    #@10a
    const-string v2, "server_info_proxy_port"

    #@10c
    aput-object v2, v1, v5

    #@10e
    const-string v2, "443"

    #@110
    aput-object v2, v1, v6

    #@112
    aput-object v1, v0, v7

    #@114
    sput-object v0, Lcom/lge/ims/provider/im/ConfigIM;->HTTP:[[Ljava/lang/String;

    #@116
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 18
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public getTableContent(Ljava/lang/String;)[[Ljava/lang/String;
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 140
    const-string v0, "lgims_com_kt_app_rcse_im"

    #@2
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_b

    #@8
    .line 141
    sget-object v0, Lcom/lge/ims/provider/im/ConfigIM;->APP:[[Ljava/lang/String;

    #@a
    .line 147
    :goto_a
    return-object v0

    #@b
    .line 142
    :cond_b
    const-string v0, "lgims_com_kt_service_rcse_im"

    #@d
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@10
    move-result v0

    #@11
    if-eqz v0, :cond_16

    #@13
    .line 143
    sget-object v0, Lcom/lge/ims/provider/im/ConfigIM;->SERVICE:[[Ljava/lang/String;

    #@15
    goto :goto_a

    #@16
    .line 144
    :cond_16
    const-string v0, "lgims_com_kt_http_rcse_im"

    #@18
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1b
    move-result v0

    #@1c
    if-eqz v0, :cond_21

    #@1e
    .line 145
    sget-object v0, Lcom/lge/ims/provider/im/ConfigIM;->HTTP:[[Ljava/lang/String;

    #@20
    goto :goto_a

    #@21
    .line 147
    :cond_21
    const/4 v0, 0x0

    #@22
    check-cast v0, [[Ljava/lang/String;

    #@24
    goto :goto_a
.end method

.method public getTables()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 132
    sget-object v0, Lcom/lge/ims/provider/im/ConfigIM;->TABLES:[Ljava/lang/String;

    #@2
    return-object v0
.end method
