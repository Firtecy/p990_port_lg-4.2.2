.class public final Lcom/lge/ims/provider/IMS$Engine;
.super Ljava/lang/Object;
.source "IMS.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/provider/IMS;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Engine"
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final TABLE_NAME:Ljava/lang/String; = "lgims_engine"

.field public static final TRACE_MODULE:Ljava/lang/String; = "trace_module"

.field public static final TRACE_OPTION:Ljava/lang/String; = "trace_option"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 71
    const-string v0, "content://com.lge.ims.provider.lgims/lgims_engine"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/lge/ims/provider/IMS$Engine;->CONTENT_URI:Landroid/net/Uri;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 65
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
