.class Lcom/lge/ims/provider/uc/UCProvider$DatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "UCProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/provider/uc/UCProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DatabaseHelper"
.end annotation


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V
    .registers 5
    .parameter "context"
    .parameter "name"
    .parameter "factory"
    .parameter "version"

    #@0
    .prologue
    .line 58
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    #@3
    .line 59
    return-void
.end method

.method private createTableWithContent(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[[Ljava/lang/String;)Z
    .registers 13
    .parameter "db"
    .parameter "table"
    .parameter "content"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 80
    if-nez p2, :cond_5

    #@4
    .line 130
    :cond_4
    :goto_4
    return v4

    #@5
    .line 84
    :cond_5
    if-eqz p3, :cond_4

    #@7
    array-length v6, p3

    #@8
    if-eqz v6, :cond_4

    #@a
    .line 89
    new-instance v6, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v7, "CREATE TABLE "

    #@11
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v6

    #@15
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v6

    #@19
    const-string v7, " ("

    #@1b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v6

    #@1f
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    .line 92
    .local v2, query:Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v6

    #@2c
    aget-object v7, p3, v4

    #@2e
    aget-object v7, v7, v5

    #@30
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v6

    #@34
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v2

    #@38
    .line 93
    new-instance v6, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v6

    #@41
    const-string v7, " "

    #@43
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v6

    #@47
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v2

    #@4b
    .line 94
    new-instance v6, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v6

    #@54
    aget-object v7, p3, v4

    #@56
    aget-object v7, v7, v4

    #@58
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v6

    #@5c
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v2

    #@60
    .line 96
    const/4 v1, 0x1

    #@61
    .local v1, i:I
    :goto_61
    array-length v6, p3

    #@62
    if-ge v1, v6, :cond_b7

    #@64
    .line 97
    new-instance v6, Ljava/lang/StringBuilder;

    #@66
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@69
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v6

    #@6d
    const-string v7, ","

    #@6f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v6

    #@73
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v2

    #@77
    .line 99
    new-instance v6, Ljava/lang/StringBuilder;

    #@79
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@7c
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v6

    #@80
    aget-object v7, p3, v1

    #@82
    aget-object v7, v7, v5

    #@84
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v6

    #@88
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8b
    move-result-object v2

    #@8c
    .line 100
    new-instance v6, Ljava/lang/StringBuilder;

    #@8e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@91
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v6

    #@95
    const-string v7, " "

    #@97
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v6

    #@9b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9e
    move-result-object v2

    #@9f
    .line 101
    new-instance v6, Ljava/lang/StringBuilder;

    #@a1
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@a4
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v6

    #@a8
    aget-object v7, p3, v1

    #@aa
    aget-object v7, v7, v4

    #@ac
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af
    move-result-object v6

    #@b0
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b3
    move-result-object v2

    #@b4
    .line 96
    add-int/lit8 v1, v1, 0x1

    #@b6
    goto :goto_61

    #@b7
    .line 104
    :cond_b7
    new-instance v6, Ljava/lang/StringBuilder;

    #@b9
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@bc
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v6

    #@c0
    const-string v7, ");"

    #@c2
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v6

    #@c6
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c9
    move-result-object v2

    #@ca
    .line 108
    :try_start_ca
    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_cd
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_ca .. :try_end_cd} :catch_e5

    #@cd
    .line 115
    new-instance v3, Landroid/content/ContentValues;

    #@cf
    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    #@d2
    .line 117
    .local v3, values:Landroid/content/ContentValues;
    const/4 v1, 0x0

    #@d3
    :goto_d3
    array-length v6, p3

    #@d4
    if-ge v1, v6, :cond_eb

    #@d6
    .line 119
    aget-object v6, p3, v1

    #@d8
    aget-object v6, v6, v5

    #@da
    aget-object v7, p3, v1

    #@dc
    const/4 v8, 0x2

    #@dd
    aget-object v7, v7, v8

    #@df
    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@e2
    .line 117
    add-int/lit8 v1, v1, 0x1

    #@e4
    goto :goto_d3

    #@e5
    .line 109
    .end local v3           #values:Landroid/content/ContentValues;
    :catch_e5
    move-exception v0

    #@e6
    .line 110
    .local v0, e:Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    #@e9
    goto/16 :goto_4

    #@eb
    .line 124
    .end local v0           #e:Landroid/database/sqlite/SQLiteException;
    .restart local v3       #values:Landroid/content/ContentValues;
    :cond_eb
    const/4 v6, 0x0

    #@ec
    :try_start_ec
    invoke-virtual {p1, p2, v6, v3}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_ef
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_ec .. :try_end_ef} :catch_f2

    #@ef
    move v4, v5

    #@f0
    .line 130
    goto/16 :goto_4

    #@f2
    .line 125
    :catch_f2
    move-exception v0

    #@f3
    .line 126
    .restart local v0       #e:Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    #@f6
    goto/16 :goto_4
.end method

.method private createTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 8
    .parameter "db"

    #@0
    .prologue
    .line 134
    invoke-static {}, Lcom/lge/ims/provider/uc/UC;->getInstance()Lcom/lge/ims/provider/uc/UC;

    #@3
    move-result-object v3

    #@4
    invoke-virtual {v3}, Lcom/lge/ims/provider/uc/UC;->getTables()[Ljava/lang/String;

    #@7
    move-result-object v2

    #@8
    .line 136
    .local v2, tables:[Ljava/lang/String;
    if-nez v2, :cond_12

    #@a
    .line 137
    const-string v3, "LGIMS"

    #@c
    const-string v4, "no tables"

    #@e
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 150
    :cond_11
    return-void

    #@12
    .line 141
    :cond_12
    const/4 v1, 0x0

    #@13
    .local v1, j:I
    :goto_13
    array-length v3, v2

    #@14
    if-ge v1, v3, :cond_11

    #@16
    .line 143
    invoke-static {}, Lcom/lge/ims/provider/uc/UC;->getInstance()Lcom/lge/ims/provider/uc/UC;

    #@19
    move-result-object v3

    #@1a
    aget-object v4, v2, v1

    #@1c
    invoke-virtual {v3, v4}, Lcom/lge/ims/provider/uc/UC;->getTableContent(Ljava/lang/String;)[[Ljava/lang/String;

    #@1f
    move-result-object v0

    #@20
    .line 145
    .local v0, content:[[Ljava/lang/String;
    aget-object v3, v2, v1

    #@22
    invoke-direct {p0, p1, v3, v0}, Lcom/lge/ims/provider/uc/UCProvider$DatabaseHelper;->createTableWithContent(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[[Ljava/lang/String;)Z

    #@25
    move-result v3

    #@26
    if-nez v3, :cond_48

    #@28
    .line 146
    const-string v3, "LGIMS"

    #@2a
    new-instance v4, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v5, "Creating a table ("

    #@31
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v4

    #@35
    aget-object v5, v2, v1

    #@37
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v4

    #@3b
    const-string v5, ") failed"

    #@3d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v4

    #@41
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v4

    #@45
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 141
    :cond_48
    add-int/lit8 v1, v1, 0x1

    #@4a
    goto :goto_13
.end method

.method private dropTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 7
    .parameter "db"

    #@0
    .prologue
    .line 153
    invoke-static {}, Lcom/lge/ims/provider/uc/UC;->getInstance()Lcom/lge/ims/provider/uc/UC;

    #@3
    move-result-object v3

    #@4
    invoke-virtual {v3}, Lcom/lge/ims/provider/uc/UC;->getTables()[Ljava/lang/String;

    #@7
    move-result-object v2

    #@8
    .line 155
    .local v2, tables:[Ljava/lang/String;
    if-nez v2, :cond_12

    #@a
    .line 156
    const-string v3, "LGIMS"

    #@c
    const-string v4, "no tables"

    #@e
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 165
    :cond_11
    return-void

    #@12
    .line 160
    :cond_12
    const/4 v1, 0x0

    #@13
    .local v1, j:I
    :goto_13
    array-length v3, v2

    #@14
    if-ge v1, v3, :cond_11

    #@16
    .line 161
    invoke-static {}, Lcom/lge/ims/provider/uc/UC;->getInstance()Lcom/lge/ims/provider/uc/UC;

    #@19
    move-result-object v3

    #@1a
    aget-object v4, v2, v1

    #@1c
    invoke-virtual {v3, v4}, Lcom/lge/ims/provider/uc/UC;->getTableContent(Ljava/lang/String;)[[Ljava/lang/String;

    #@1f
    move-result-object v0

    #@20
    .line 163
    .local v0, content:[[Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v4, "DROP TABLE IF EXISTS "

    #@27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    aget-object v4, v2, v1

    #@2d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@38
    .line 160
    add-int/lit8 v1, v1, 0x1

    #@3a
    goto :goto_13
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 4
    .parameter "db"

    #@0
    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/lge/ims/provider/uc/UCProvider$DatabaseHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    #@3
    .line 66
    const-string v0, "LGIMS"

    #@5
    const-string v1, "ims_uc.db is created ..."

    #@7
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 67
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .registers 7
    .parameter "db"
    .parameter "oldVersion"
    .parameter "newVersion"

    #@0
    .prologue
    .line 71
    const-string v0, "LGIMS"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Upgrading database from version "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, " to "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    const-string v2, ", which will destroy all old data"

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 74
    invoke-direct {p0, p1}, Lcom/lge/ims/provider/uc/UCProvider$DatabaseHelper;->dropTables(Landroid/database/sqlite/SQLiteDatabase;)V

    #@2b
    .line 76
    invoke-virtual {p0, p1}, Lcom/lge/ims/provider/uc/UCProvider$DatabaseHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    #@2e
    .line 77
    return-void
.end method
