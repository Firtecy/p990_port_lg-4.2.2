.class public final Lcom/lge/ims/provider/ip/IPProviderMetaData$IPCapa;
.super Ljava/lang/Object;
.source "IPProviderMetaData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/provider/ip/IPProviderMetaData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "IPCapa"
.end annotation


# static fields
.field public static final CAPA_STATUS_EXPIRED_RCS_USER:I = 0x3

.field public static final CAPA_STATUS_NONE:I = 0x0

.field public static final CAPA_STATUS_NOT_RCS_USER:I = 0x2

.field public static final CAPA_STATUS_RCS_USER:I = 0x1

.field public static final CONTENT_CAPABILITY_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/com.lge.ims.provider.ip.ip_capa"

.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final INDEX_KEY_CAPA_TYPE:I = 0x8

.field public static final INDEX_KEY_DATA_ID:I = 0x1

.field public static final INDEX_KEY_DISPLAY_NAME:I = 0x14

.field public static final INDEX_KEY_FIRSTTIME_RCS:I = 0x9

.field public static final INDEX_KEY_FTSTATUS:I = 0x6

.field public static final INDEX_KEY_HTTPSTATUS:I = 0xd

.field public static final INDEX_KEY_ID:I = 0x0

.field public static final INDEX_KEY_IMAGE_URI_ID:I = 0x11

.field public static final INDEX_KEY_IMSTATUS:I = 0x5

.field public static final INDEX_KEY_MIMSTATUS:I = 0xa

.field public static final INDEX_KEY_MSISDN:I = 0x2

.field public static final INDEX_KEY_NONRCSEUSER_LIST:I = 0x10

.field public static final INDEX_KEY_NORMAL_MSISDN:I = 0x3

.field public static final INDEX_KEY_PHOTO_UPDATE:I = 0x13

.field public static final INDEX_KEY_PRESENCESTATUS:I = 0xb

.field public static final INDEX_KEY_RAW_CONTACT_ID:I = 0x12

.field public static final INDEX_KEY_RCSEUSER_LIST:I = 0xf

.field public static final INDEX_KEY_RCSSTATUS:I = 0x4

.field public static final INDEX_KEY_RESPONSE_CODE:I = 0xe

.field public static final INDEX_KEY_TIME_STAMP:I = 0x7

.field public static final INDEX_KEY_URI_ID:I = 0xc

.field public static final KEY_CAPA_TYPE:Ljava/lang/String; = "capa_type"

.field public static final KEY_DATA_ID:Ljava/lang/String; = "data_id"

.field public static final KEY_DISPLAY_NAME:Ljava/lang/String; = "display_name"

.field public static final KEY_FIRSTTIME_RCS:Ljava/lang/String; = "first_time_rcs"

.field public static final KEY_FTSTATUS:Ljava/lang/String; = "ft_status"

.field public static final KEY_HTTPSTATUS:Ljava/lang/String; = "http_status"

.field public static final KEY_ID:Ljava/lang/String; = "_id"

.field public static final KEY_IMAGE_URI_ID:Ljava/lang/String; = "image_uri_id"

.field public static final KEY_IMSTATUS:Ljava/lang/String; = "im_status"

.field public static final KEY_MIMSTATUS:Ljava/lang/String; = "mim_status"

.field public static final KEY_MSISDN:Ljava/lang/String; = "msisdn"

.field public static final KEY_NONRCSEUSER_LIST:Ljava/lang/String; = "nonrcs_user_list"

.field public static final KEY_NORMAL_MSISDN:Ljava/lang/String; = "nor_msisdn"

.field public static final KEY_PHOTO_UPDATE:Ljava/lang/String; = "photo_update"

.field public static final KEY_PRESENCESTATUS:Ljava/lang/String; = "presence_status"

.field public static final KEY_RAW_CONTACT_ID:Ljava/lang/String; = "raw_contact_id"

.field public static final KEY_RCSEUSER_LIST:Ljava/lang/String; = "rcs_user_list"

.field public static final KEY_RCSSTATUS:Ljava/lang/String; = "rcs_status"

.field public static final KEY_RESPONSE_CODE:Ljava/lang/String; = "response_code"

.field public static final KEY_TIME_STAMP:Ljava/lang/String; = "time_stamp"

.field public static final KEY_URI_ID:Ljava/lang/String; = "uri_id"

.field public static final TABLE_NAME:Ljava/lang/String; = "ip_capa"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 34
    const-string v0, "content://com.lge.ims.provider.ip/ip_capa"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/lge/ims/provider/ip/IPProviderMetaData$IPCapa;->CONTENT_URI:Landroid/net/Uri;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 24
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
