.class public Lcom/lge/ims/provider/ConfigCOM;
.super Ljava/lang/Object;
.source "ConfigCOM.java"

# interfaces
.implements Lcom/lge/ims/provider/IConfiguration;


# static fields
.field private static final MEDIA:[[Ljava/lang/String;

.field private static final SIP:[[Ljava/lang/String;

.field private static final SIP_A1:[[Ljava/lang/String;

.field private static final TABLES:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 10

    #@0
    .prologue
    const/4 v9, 0x4

    #@1
    const/4 v8, 0x3

    #@2
    const/4 v7, 0x2

    #@3
    const/4 v6, 0x1

    #@4
    const/4 v5, 0x0

    #@5
    .line 20
    new-array v0, v7, [Ljava/lang/String;

    #@7
    const-string v1, "lgims_com_kt_media"

    #@9
    aput-object v1, v0, v5

    #@b
    const-string v1, "lgims_com_kt_sip"

    #@d
    aput-object v1, v0, v6

    #@f
    sput-object v0, Lcom/lge/ims/provider/ConfigCOM;->TABLES:[Ljava/lang/String;

    #@11
    .line 29
    new-array v0, v8, [[Ljava/lang/String;

    #@13
    new-array v1, v8, [Ljava/lang/String;

    #@15
    const-string v2, "INTEGER"

    #@17
    aput-object v2, v1, v5

    #@19
    const-string v2, "id"

    #@1b
    aput-object v2, v1, v6

    #@1d
    const-string v2, "1"

    #@1f
    aput-object v2, v1, v7

    #@21
    aput-object v1, v0, v5

    #@23
    new-array v1, v8, [Ljava/lang/String;

    #@25
    const-string v2, "INTEGER"

    #@27
    aput-object v2, v1, v5

    #@29
    const-string v2, "property"

    #@2b
    aput-object v2, v1, v6

    #@2d
    const-string v2, "0"

    #@2f
    aput-object v2, v1, v7

    #@31
    aput-object v1, v0, v6

    #@33
    new-array v1, v8, [Ljava/lang/String;

    #@35
    const-string v2, "TEXT"

    #@37
    aput-object v2, v1, v5

    #@39
    const-string v2, "conf"

    #@3b
    aput-object v2, v1, v6

    #@3d
    const-string v2, "[capabilities]\nstream_audio_count=3\nstream_audio_0=m=audio 0 RTP/AVP 96 100\nstream_audio_1=a=rtpmap:96 AMR/8000/1\nstream_audio_2=a=rtpmap:100 AMR-WB/16000/1\nstream_video_count=5\nstream_video_0=m=video 0 RTP/AVP 102 34\nstream_video_1=a=rtpmap:102 H264/90000\nstream_video_2=a=fmtp:102 profile-level-id=42800D\nstream_video_3=a=rtpmap:34 H263/90000\nstream_video_4=a=fmtp:34 profile=0; level=10\nframed_count=2\nframed_0=m=message 0 TCP/MSRP *\nframed_1=a=file-selector\n\n[com.kt.media.vt]\nstream_audio_count=3\nstream_audio_0=m=audio 0 RTP/AVP 96 100\nstream_audio_1=a=rtpmap:96 AMR/8000/1\nstream_audio_2=a=rtpmap:100 AMR-WB/16000/1\nstream_video_count=5\nstream_video_0=m=video 0 RTP/AVP 102 34\nstream_video_1=a=rtpmap:102 H264/90000\nstream_video_2=a=fmtp:102 profile-level-id=42800D\nstream_video_3=a=rtpmap:34 H263/90000\nstream_video_4=a=fmtp:34 profile=0; level=10\n\n[com.kt.media.is]\nframed_count=4\nframed_0=m=message 0 TCP/MSRP *\nframed_1=a=accept-types:text/plain text/html image/jpeg image/gif image/bmp image/png\nframed_2=a=file-selector\nframed_3=a=max-size:4096\n\n[com.kt.media.vs]\nstream_video_count=3\nstream_video_0=m=video 0 RTP/AVP 34\nstream_video_1=a=rtpmap:34 H263/90000\nstream_video_2=a=fmtp:34 profile=0; level=10\n\n[com.kt.media.im]\nframed_count=4\nframed_0=m=message 0 TCP/MSRP *\nframed_1=a=accept-types:text/plain text/html image/jpeg image/gif image/bmp image/png\nframed_2=a=file-selector\nframed_3=a=max-size:4096\n\n"

    #@3f
    aput-object v2, v1, v7

    #@41
    aput-object v1, v0, v7

    #@43
    sput-object v0, Lcom/lge/ims/provider/ConfigCOM;->MEDIA:[[Ljava/lang/String;

    #@45
    .line 87
    const/16 v0, 0x1f

    #@47
    new-array v0, v0, [[Ljava/lang/String;

    #@49
    new-array v1, v8, [Ljava/lang/String;

    #@4b
    const-string v2, "INTEGER"

    #@4d
    aput-object v2, v1, v5

    #@4f
    const-string v2, "id"

    #@51
    aput-object v2, v1, v6

    #@53
    const-string v2, "1"

    #@55
    aput-object v2, v1, v7

    #@57
    aput-object v1, v0, v5

    #@59
    new-array v1, v8, [Ljava/lang/String;

    #@5b
    const-string v2, "INTEGER"

    #@5d
    aput-object v2, v1, v5

    #@5f
    const-string v2, "property"

    #@61
    aput-object v2, v1, v6

    #@63
    const-string v2, "1"

    #@65
    aput-object v2, v1, v7

    #@67
    aput-object v1, v0, v6

    #@69
    new-array v1, v8, [Ljava/lang/String;

    #@6b
    const-string v2, "TEXT"

    #@6d
    aput-object v2, v1, v5

    #@6f
    const-string v2, "header_info_target_number_format"

    #@71
    aput-object v2, v1, v6

    #@73
    const-string v2, "local"

    #@75
    aput-object v2, v1, v7

    #@77
    aput-object v1, v0, v7

    #@79
    new-array v1, v8, [Ljava/lang/String;

    #@7b
    const-string v2, "TEXT"

    #@7d
    aput-object v2, v1, v5

    #@7f
    const-string v2, "header_info_target_scheme"

    #@81
    aput-object v2, v1, v6

    #@83
    const-string v2, "tel"

    #@85
    aput-object v2, v1, v7

    #@87
    aput-object v1, v0, v8

    #@89
    new-array v1, v8, [Ljava/lang/String;

    #@8b
    const-string v2, "TEXT"

    #@8d
    aput-object v2, v1, v5

    #@8f
    const-string v2, "header_info_preferred_id"

    #@91
    aput-object v2, v1, v6

    #@93
    const-string v2, "sip"

    #@95
    aput-object v2, v1, v7

    #@97
    aput-object v1, v0, v9

    #@99
    const/4 v1, 0x5

    #@9a
    new-array v2, v8, [Ljava/lang/String;

    #@9c
    const-string v3, "TEXT"

    #@9e
    aput-object v3, v2, v5

    #@a0
    const-string v3, "header_info_allow_methods"

    #@a2
    aput-object v3, v2, v6

    #@a4
    const-string v3, "INVITE,BYE,CANCEL,ACK,PRACK,UPDATE,INFO,REFER,NOTIFY,MESSAGE,OPTIONS"

    #@a6
    aput-object v3, v2, v7

    #@a8
    aput-object v2, v0, v1

    #@aa
    const/4 v1, 0x6

    #@ab
    new-array v2, v8, [Ljava/lang/String;

    #@ad
    const-string v3, "TEXT"

    #@af
    aput-object v3, v2, v5

    #@b1
    const-string v3, "header_info_service_version"

    #@b3
    aput-object v3, v2, v6

    #@b5
    new-instance v3, Ljava/lang/StringBuilder;

    #@b7
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@ba
    const-string v4, "KT-client/VoLTE+PSVT;"

    #@bc
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v3

    #@c0
    sget-object v4, Lcom/lge/ims/Configuration;->MODEL:Ljava/lang/String;

    #@c2
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v3

    #@c6
    const-string v4, ";Device_Type=Android_Phone"

    #@c8
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cb
    move-result-object v3

    #@cc
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cf
    move-result-object v3

    #@d0
    aput-object v3, v2, v7

    #@d2
    aput-object v2, v0, v1

    #@d4
    const/4 v1, 0x7

    #@d5
    new-array v2, v8, [Ljava/lang/String;

    #@d7
    const-string v3, "TEXT"

    #@d9
    aput-object v3, v2, v5

    #@db
    const-string v3, "header_info_feature_tags"

    #@dd
    aput-object v3, v2, v6

    #@df
    const-string v3, "0x03000308"

    #@e1
    aput-object v3, v2, v7

    #@e3
    aput-object v2, v0, v1

    #@e5
    const/16 v1, 0x8

    #@e7
    new-array v2, v8, [Ljava/lang/String;

    #@e9
    const-string v3, "TEXT"

    #@eb
    aput-object v3, v2, v5

    #@ed
    const-string v3, "timer_tv_on_runtime"

    #@ef
    aput-object v3, v2, v6

    #@f1
    const-string v3, "true"

    #@f3
    aput-object v3, v2, v7

    #@f5
    aput-object v2, v0, v1

    #@f7
    const/16 v1, 0x9

    #@f9
    new-array v2, v8, [Ljava/lang/String;

    #@fb
    const-string v3, "INTEGER"

    #@fd
    aput-object v3, v2, v5

    #@ff
    const-string v3, "timer_tv_t1"

    #@101
    aput-object v3, v2, v6

    #@103
    const-string v3, "1000"

    #@105
    aput-object v3, v2, v7

    #@107
    aput-object v2, v0, v1

    #@109
    const/16 v1, 0xa

    #@10b
    new-array v2, v8, [Ljava/lang/String;

    #@10d
    const-string v3, "INTEGER"

    #@10f
    aput-object v3, v2, v5

    #@111
    const-string v3, "timer_tv_t2"

    #@113
    aput-object v3, v2, v6

    #@115
    const-string v3, "2000"

    #@117
    aput-object v3, v2, v7

    #@119
    aput-object v2, v0, v1

    #@11b
    const/16 v1, 0xb

    #@11d
    new-array v2, v8, [Ljava/lang/String;

    #@11f
    const-string v3, "INTEGER"

    #@121
    aput-object v3, v2, v5

    #@123
    const-string v3, "timer_tv_tb"

    #@125
    aput-object v3, v2, v6

    #@127
    const-string v3, "4000"

    #@129
    aput-object v3, v2, v7

    #@12b
    aput-object v2, v0, v1

    #@12d
    const/16 v1, 0xc

    #@12f
    new-array v2, v8, [Ljava/lang/String;

    #@131
    const-string v3, "INTEGER"

    #@133
    aput-object v3, v2, v5

    #@135
    const-string v3, "timer_tv_td"

    #@137
    aput-object v3, v2, v6

    #@139
    const-string v3, "64000"

    #@13b
    aput-object v3, v2, v7

    #@13d
    aput-object v2, v0, v1

    #@13f
    const/16 v1, 0xd

    #@141
    new-array v2, v8, [Ljava/lang/String;

    #@143
    const-string v3, "INTEGER"

    #@145
    aput-object v3, v2, v5

    #@147
    const-string v3, "timer_tv_tf"

    #@149
    aput-object v3, v2, v6

    #@14b
    const-string v3, "8000"

    #@14d
    aput-object v3, v2, v7

    #@14f
    aput-object v2, v0, v1

    #@151
    const/16 v1, 0xe

    #@153
    new-array v2, v8, [Ljava/lang/String;

    #@155
    const-string v3, "INTEGER"

    #@157
    aput-object v3, v2, v5

    #@159
    const-string v3, "timer_tv_th"

    #@15b
    aput-object v3, v2, v6

    #@15d
    const-string v3, "64000"

    #@15f
    aput-object v3, v2, v7

    #@161
    aput-object v2, v0, v1

    #@163
    const/16 v1, 0xf

    #@165
    new-array v2, v8, [Ljava/lang/String;

    #@167
    const-string v3, "INTEGER"

    #@169
    aput-object v3, v2, v5

    #@16b
    const-string v3, "timer_tv_ti"

    #@16d
    aput-object v3, v2, v6

    #@16f
    const-string v3, "5000"

    #@171
    aput-object v3, v2, v7

    #@173
    aput-object v2, v0, v1

    #@175
    const/16 v1, 0x10

    #@177
    new-array v2, v8, [Ljava/lang/String;

    #@179
    const-string v3, "INTEGER"

    #@17b
    aput-object v3, v2, v5

    #@17d
    const-string v3, "timer_tv_tj"

    #@17f
    aput-object v3, v2, v6

    #@181
    const-string v3, "64000"

    #@183
    aput-object v3, v2, v7

    #@185
    aput-object v2, v0, v1

    #@187
    const/16 v1, 0x11

    #@189
    new-array v2, v8, [Ljava/lang/String;

    #@18b
    const-string v3, "INTEGER"

    #@18d
    aput-object v3, v2, v5

    #@18f
    const-string v3, "timer_tv_tk"

    #@191
    aput-object v3, v2, v6

    #@193
    const-string v3, "5000"

    #@195
    aput-object v3, v2, v7

    #@197
    aput-object v2, v0, v1

    #@199
    const/16 v1, 0x12

    #@19b
    new-array v2, v8, [Ljava/lang/String;

    #@19d
    const-string v3, "TEXT"

    #@19f
    aput-object v3, v2, v5

    #@1a1
    const-string v3, "session_mode"

    #@1a3
    aput-object v3, v2, v6

    #@1a5
    const-string v3, "IETF"

    #@1a7
    aput-object v3, v2, v7

    #@1a9
    aput-object v2, v0, v1

    #@1ab
    const/16 v1, 0x13

    #@1ad
    new-array v2, v8, [Ljava/lang/String;

    #@1af
    const-string v3, "TEXT"

    #@1b1
    aput-object v3, v2, v5

    #@1b3
    const-string v3, "session_st_supported"

    #@1b5
    aput-object v3, v2, v6

    #@1b7
    const-string v3, "true"

    #@1b9
    aput-object v3, v2, v7

    #@1bb
    aput-object v2, v0, v1

    #@1bd
    const/16 v1, 0x14

    #@1bf
    new-array v2, v8, [Ljava/lang/String;

    #@1c1
    const-string v3, "TEXT"

    #@1c3
    aput-object v3, v2, v5

    #@1c5
    const-string v3, "session_st_refresher"

    #@1c7
    aput-object v3, v2, v6

    #@1c9
    const-string v3, "local"

    #@1cb
    aput-object v3, v2, v7

    #@1cd
    aput-object v2, v0, v1

    #@1cf
    const/16 v1, 0x15

    #@1d1
    new-array v2, v8, [Ljava/lang/String;

    #@1d3
    const-string v3, "TEXT"

    #@1d5
    aput-object v3, v2, v5

    #@1d7
    const-string v3, "session_st_method"

    #@1d9
    aput-object v3, v2, v6

    #@1db
    const-string v3, "UPDATE"

    #@1dd
    aput-object v3, v2, v7

    #@1df
    aput-object v2, v0, v1

    #@1e1
    const/16 v1, 0x16

    #@1e3
    new-array v2, v8, [Ljava/lang/String;

    #@1e5
    const-string v3, "INTEGER"

    #@1e7
    aput-object v3, v2, v5

    #@1e9
    const-string v3, "session_st_minse"

    #@1eb
    aput-object v3, v2, v6

    #@1ed
    const-string v3, "90"

    #@1ef
    aput-object v3, v2, v7

    #@1f1
    aput-object v2, v0, v1

    #@1f3
    const/16 v1, 0x17

    #@1f5
    new-array v2, v8, [Ljava/lang/String;

    #@1f7
    const-string v3, "INTEGER"

    #@1f9
    aput-object v3, v2, v5

    #@1fb
    const-string v3, "session_st_session_expires"

    #@1fd
    aput-object v3, v2, v6

    #@1ff
    const-string v3, "360"

    #@201
    aput-object v3, v2, v7

    #@203
    aput-object v2, v0, v1

    #@205
    const/16 v1, 0x18

    #@207
    new-array v2, v8, [Ljava/lang/String;

    #@209
    const-string v3, "TEXT"

    #@20b
    aput-object v3, v2, v5

    #@20d
    const-string v3, "session_st_headers"

    #@20f
    aput-object v3, v2, v6

    #@211
    const-string v3, "0xF9"

    #@213
    aput-object v3, v2, v7

    #@215
    aput-object v2, v0, v1

    #@217
    const/16 v1, 0x19

    #@219
    new-array v2, v8, [Ljava/lang/String;

    #@21b
    const-string v3, "TEXT"

    #@21d
    aput-object v3, v2, v5

    #@21f
    const-string v3, "session_st_no_refresh_by_reinvite"

    #@221
    aput-object v3, v2, v6

    #@223
    const-string v3, "true"

    #@225
    aput-object v3, v2, v7

    #@227
    aput-object v2, v0, v1

    #@229
    const/16 v1, 0x1a

    #@22b
    new-array v2, v8, [Ljava/lang/String;

    #@22d
    const-string v3, "TEXT"

    #@22f
    aput-object v3, v2, v5

    #@231
    const-string v3, "session_sdp_version_check"

    #@233
    aput-object v3, v2, v6

    #@235
    const-string v3, "true"

    #@237
    aput-object v3, v2, v7

    #@239
    aput-object v2, v0, v1

    #@23b
    const/16 v1, 0x1b

    #@23d
    new-array v2, v8, [Ljava/lang/String;

    #@23f
    const-string v3, "TEXT"

    #@241
    aput-object v3, v2, v5

    #@243
    const-string v3, "session_sdp_non_rpr"

    #@245
    aput-object v3, v2, v6

    #@247
    const-string v3, "true"

    #@249
    aput-object v3, v2, v7

    #@24b
    aput-object v2, v0, v1

    #@24d
    const/16 v1, 0x1c

    #@24f
    new-array v2, v8, [Ljava/lang/String;

    #@251
    const-string v3, "TEXT"

    #@253
    aput-object v3, v2, v5

    #@255
    const-string v3, "capabilities_resp_by_app"

    #@257
    aput-object v3, v2, v6

    #@259
    const-string v3, "true"

    #@25b
    aput-object v3, v2, v7

    #@25d
    aput-object v2, v0, v1

    #@25f
    const/16 v1, 0x1d

    #@261
    new-array v2, v8, [Ljava/lang/String;

    #@263
    const-string v3, "TEXT"

    #@265
    aput-object v3, v2, v5

    #@267
    const-string v3, "pagemessage_resp_by_app"

    #@269
    aput-object v3, v2, v6

    #@26b
    const-string v3, "true"

    #@26d
    aput-object v3, v2, v7

    #@26f
    aput-object v2, v0, v1

    #@271
    const/16 v1, 0x1e

    #@273
    new-array v2, v8, [Ljava/lang/String;

    #@275
    const-string v3, "TEXT"

    #@277
    aput-object v3, v2, v5

    #@279
    const-string v3, "reference_resp_by_app"

    #@27b
    aput-object v3, v2, v6

    #@27d
    const-string v3, "false"

    #@27f
    aput-object v3, v2, v7

    #@281
    aput-object v2, v0, v1

    #@283
    sput-object v0, Lcom/lge/ims/provider/ConfigCOM;->SIP_A1:[[Ljava/lang/String;

    #@285
    .line 135
    const/16 v0, 0x1f

    #@287
    new-array v0, v0, [[Ljava/lang/String;

    #@289
    new-array v1, v8, [Ljava/lang/String;

    #@28b
    const-string v2, "INTEGER"

    #@28d
    aput-object v2, v1, v5

    #@28f
    const-string v2, "id"

    #@291
    aput-object v2, v1, v6

    #@293
    const-string v2, "1"

    #@295
    aput-object v2, v1, v7

    #@297
    aput-object v1, v0, v5

    #@299
    new-array v1, v8, [Ljava/lang/String;

    #@29b
    const-string v2, "INTEGER"

    #@29d
    aput-object v2, v1, v5

    #@29f
    const-string v2, "property"

    #@2a1
    aput-object v2, v1, v6

    #@2a3
    const-string v2, "1"

    #@2a5
    aput-object v2, v1, v7

    #@2a7
    aput-object v1, v0, v6

    #@2a9
    new-array v1, v8, [Ljava/lang/String;

    #@2ab
    const-string v2, "TEXT"

    #@2ad
    aput-object v2, v1, v5

    #@2af
    const-string v2, "header_info_target_number_format"

    #@2b1
    aput-object v2, v1, v6

    #@2b3
    const-string v2, "local"

    #@2b5
    aput-object v2, v1, v7

    #@2b7
    aput-object v1, v0, v7

    #@2b9
    new-array v1, v8, [Ljava/lang/String;

    #@2bb
    const-string v2, "TEXT"

    #@2bd
    aput-object v2, v1, v5

    #@2bf
    const-string v2, "header_info_target_scheme"

    #@2c1
    aput-object v2, v1, v6

    #@2c3
    const-string v2, "tel"

    #@2c5
    aput-object v2, v1, v7

    #@2c7
    aput-object v1, v0, v8

    #@2c9
    new-array v1, v8, [Ljava/lang/String;

    #@2cb
    const-string v2, "TEXT"

    #@2cd
    aput-object v2, v1, v5

    #@2cf
    const-string v2, "header_info_preferred_id"

    #@2d1
    aput-object v2, v1, v6

    #@2d3
    const-string v2, "sip"

    #@2d5
    aput-object v2, v1, v7

    #@2d7
    aput-object v1, v0, v9

    #@2d9
    const/4 v1, 0x5

    #@2da
    new-array v2, v8, [Ljava/lang/String;

    #@2dc
    const-string v3, "TEXT"

    #@2de
    aput-object v3, v2, v5

    #@2e0
    const-string v3, "header_info_allow_methods"

    #@2e2
    aput-object v3, v2, v6

    #@2e4
    const-string v3, "INVITE,BYE,CANCEL,ACK,PRACK,UPDATE,INFO,REFER,NOTIFY,MESSAGE,OPTIONS"

    #@2e6
    aput-object v3, v2, v7

    #@2e8
    aput-object v2, v0, v1

    #@2ea
    const/4 v1, 0x6

    #@2eb
    new-array v2, v8, [Ljava/lang/String;

    #@2ed
    const-string v3, "TEXT"

    #@2ef
    aput-object v3, v2, v5

    #@2f1
    const-string v3, "header_info_service_version"

    #@2f3
    aput-object v3, v2, v6

    #@2f5
    new-instance v3, Ljava/lang/StringBuilder;

    #@2f7
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2fa
    const-string v4, "KT-client/VoLTE+PSVT;"

    #@2fc
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ff
    move-result-object v3

    #@300
    sget-object v4, Lcom/lge/ims/Configuration;->MODEL:Ljava/lang/String;

    #@302
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@305
    move-result-object v3

    #@306
    const-string v4, ";Device_Type=Android_Phone"

    #@308
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30b
    move-result-object v3

    #@30c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30f
    move-result-object v3

    #@310
    aput-object v3, v2, v7

    #@312
    aput-object v2, v0, v1

    #@314
    const/4 v1, 0x7

    #@315
    new-array v2, v8, [Ljava/lang/String;

    #@317
    const-string v3, "TEXT"

    #@319
    aput-object v3, v2, v5

    #@31b
    const-string v3, "header_info_feature_tags"

    #@31d
    aput-object v3, v2, v6

    #@31f
    const-string v3, "0x03000308"

    #@321
    aput-object v3, v2, v7

    #@323
    aput-object v2, v0, v1

    #@325
    const/16 v1, 0x8

    #@327
    new-array v2, v8, [Ljava/lang/String;

    #@329
    const-string v3, "TEXT"

    #@32b
    aput-object v3, v2, v5

    #@32d
    const-string v3, "timer_tv_on_runtime"

    #@32f
    aput-object v3, v2, v6

    #@331
    const-string v3, "true"

    #@333
    aput-object v3, v2, v7

    #@335
    aput-object v2, v0, v1

    #@337
    const/16 v1, 0x9

    #@339
    new-array v2, v8, [Ljava/lang/String;

    #@33b
    const-string v3, "INTEGER"

    #@33d
    aput-object v3, v2, v5

    #@33f
    const-string v3, "timer_tv_t1"

    #@341
    aput-object v3, v2, v6

    #@343
    const-string v3, "2000"

    #@345
    aput-object v3, v2, v7

    #@347
    aput-object v2, v0, v1

    #@349
    const/16 v1, 0xa

    #@34b
    new-array v2, v8, [Ljava/lang/String;

    #@34d
    const-string v3, "INTEGER"

    #@34f
    aput-object v3, v2, v5

    #@351
    const-string v3, "timer_tv_t2"

    #@353
    aput-object v3, v2, v6

    #@355
    const-string v3, "2000"

    #@357
    aput-object v3, v2, v7

    #@359
    aput-object v2, v0, v1

    #@35b
    const/16 v1, 0xb

    #@35d
    new-array v2, v8, [Ljava/lang/String;

    #@35f
    const-string v3, "INTEGER"

    #@361
    aput-object v3, v2, v5

    #@363
    const-string v3, "timer_tv_tb"

    #@365
    aput-object v3, v2, v6

    #@367
    const-string v3, "4000"

    #@369
    aput-object v3, v2, v7

    #@36b
    aput-object v2, v0, v1

    #@36d
    const/16 v1, 0xc

    #@36f
    new-array v2, v8, [Ljava/lang/String;

    #@371
    const-string v3, "INTEGER"

    #@373
    aput-object v3, v2, v5

    #@375
    const-string v3, "timer_tv_td"

    #@377
    aput-object v3, v2, v6

    #@379
    const-string v3, "64000"

    #@37b
    aput-object v3, v2, v7

    #@37d
    aput-object v2, v0, v1

    #@37f
    const/16 v1, 0xd

    #@381
    new-array v2, v8, [Ljava/lang/String;

    #@383
    const-string v3, "INTEGER"

    #@385
    aput-object v3, v2, v5

    #@387
    const-string v3, "timer_tv_tf"

    #@389
    aput-object v3, v2, v6

    #@38b
    const-string v3, "9000"

    #@38d
    aput-object v3, v2, v7

    #@38f
    aput-object v2, v0, v1

    #@391
    const/16 v1, 0xe

    #@393
    new-array v2, v8, [Ljava/lang/String;

    #@395
    const-string v3, "INTEGER"

    #@397
    aput-object v3, v2, v5

    #@399
    const-string v3, "timer_tv_th"

    #@39b
    aput-object v3, v2, v6

    #@39d
    const-string v3, "128000"

    #@39f
    aput-object v3, v2, v7

    #@3a1
    aput-object v2, v0, v1

    #@3a3
    const/16 v1, 0xf

    #@3a5
    new-array v2, v8, [Ljava/lang/String;

    #@3a7
    const-string v3, "INTEGER"

    #@3a9
    aput-object v3, v2, v5

    #@3ab
    const-string v3, "timer_tv_ti"

    #@3ad
    aput-object v3, v2, v6

    #@3af
    const-string v3, "5000"

    #@3b1
    aput-object v3, v2, v7

    #@3b3
    aput-object v2, v0, v1

    #@3b5
    const/16 v1, 0x10

    #@3b7
    new-array v2, v8, [Ljava/lang/String;

    #@3b9
    const-string v3, "INTEGER"

    #@3bb
    aput-object v3, v2, v5

    #@3bd
    const-string v3, "timer_tv_tj"

    #@3bf
    aput-object v3, v2, v6

    #@3c1
    const-string v3, "128000"

    #@3c3
    aput-object v3, v2, v7

    #@3c5
    aput-object v2, v0, v1

    #@3c7
    const/16 v1, 0x11

    #@3c9
    new-array v2, v8, [Ljava/lang/String;

    #@3cb
    const-string v3, "INTEGER"

    #@3cd
    aput-object v3, v2, v5

    #@3cf
    const-string v3, "timer_tv_tk"

    #@3d1
    aput-object v3, v2, v6

    #@3d3
    const-string v3, "5000"

    #@3d5
    aput-object v3, v2, v7

    #@3d7
    aput-object v2, v0, v1

    #@3d9
    const/16 v1, 0x12

    #@3db
    new-array v2, v8, [Ljava/lang/String;

    #@3dd
    const-string v3, "TEXT"

    #@3df
    aput-object v3, v2, v5

    #@3e1
    const-string v3, "session_mode"

    #@3e3
    aput-object v3, v2, v6

    #@3e5
    const-string v3, "IETF"

    #@3e7
    aput-object v3, v2, v7

    #@3e9
    aput-object v2, v0, v1

    #@3eb
    const/16 v1, 0x13

    #@3ed
    new-array v2, v8, [Ljava/lang/String;

    #@3ef
    const-string v3, "TEXT"

    #@3f1
    aput-object v3, v2, v5

    #@3f3
    const-string v3, "session_st_supported"

    #@3f5
    aput-object v3, v2, v6

    #@3f7
    const-string v3, "true"

    #@3f9
    aput-object v3, v2, v7

    #@3fb
    aput-object v2, v0, v1

    #@3fd
    const/16 v1, 0x14

    #@3ff
    new-array v2, v8, [Ljava/lang/String;

    #@401
    const-string v3, "TEXT"

    #@403
    aput-object v3, v2, v5

    #@405
    const-string v3, "session_st_refresher"

    #@407
    aput-object v3, v2, v6

    #@409
    const-string v3, "local"

    #@40b
    aput-object v3, v2, v7

    #@40d
    aput-object v2, v0, v1

    #@40f
    const/16 v1, 0x15

    #@411
    new-array v2, v8, [Ljava/lang/String;

    #@413
    const-string v3, "TEXT"

    #@415
    aput-object v3, v2, v5

    #@417
    const-string v3, "session_st_method"

    #@419
    aput-object v3, v2, v6

    #@41b
    const-string v3, "UPDATE"

    #@41d
    aput-object v3, v2, v7

    #@41f
    aput-object v2, v0, v1

    #@421
    const/16 v1, 0x16

    #@423
    new-array v2, v8, [Ljava/lang/String;

    #@425
    const-string v3, "INTEGER"

    #@427
    aput-object v3, v2, v5

    #@429
    const-string v3, "session_st_minse"

    #@42b
    aput-object v3, v2, v6

    #@42d
    const-string v3, "90"

    #@42f
    aput-object v3, v2, v7

    #@431
    aput-object v2, v0, v1

    #@433
    const/16 v1, 0x17

    #@435
    new-array v2, v8, [Ljava/lang/String;

    #@437
    const-string v3, "INTEGER"

    #@439
    aput-object v3, v2, v5

    #@43b
    const-string v3, "session_st_session_expires"

    #@43d
    aput-object v3, v2, v6

    #@43f
    const-string v3, "360"

    #@441
    aput-object v3, v2, v7

    #@443
    aput-object v2, v0, v1

    #@445
    const/16 v1, 0x18

    #@447
    new-array v2, v8, [Ljava/lang/String;

    #@449
    const-string v3, "TEXT"

    #@44b
    aput-object v3, v2, v5

    #@44d
    const-string v3, "session_st_headers"

    #@44f
    aput-object v3, v2, v6

    #@451
    const-string v3, "0xF9"

    #@453
    aput-object v3, v2, v7

    #@455
    aput-object v2, v0, v1

    #@457
    const/16 v1, 0x19

    #@459
    new-array v2, v8, [Ljava/lang/String;

    #@45b
    const-string v3, "TEXT"

    #@45d
    aput-object v3, v2, v5

    #@45f
    const-string v3, "session_st_no_refresh_by_reinvite"

    #@461
    aput-object v3, v2, v6

    #@463
    const-string v3, "true"

    #@465
    aput-object v3, v2, v7

    #@467
    aput-object v2, v0, v1

    #@469
    const/16 v1, 0x1a

    #@46b
    new-array v2, v8, [Ljava/lang/String;

    #@46d
    const-string v3, "TEXT"

    #@46f
    aput-object v3, v2, v5

    #@471
    const-string v3, "session_sdp_version_check"

    #@473
    aput-object v3, v2, v6

    #@475
    const-string v3, "true"

    #@477
    aput-object v3, v2, v7

    #@479
    aput-object v2, v0, v1

    #@47b
    const/16 v1, 0x1b

    #@47d
    new-array v2, v8, [Ljava/lang/String;

    #@47f
    const-string v3, "TEXT"

    #@481
    aput-object v3, v2, v5

    #@483
    const-string v3, "session_sdp_non_rpr"

    #@485
    aput-object v3, v2, v6

    #@487
    const-string v3, "true"

    #@489
    aput-object v3, v2, v7

    #@48b
    aput-object v2, v0, v1

    #@48d
    const/16 v1, 0x1c

    #@48f
    new-array v2, v8, [Ljava/lang/String;

    #@491
    const-string v3, "TEXT"

    #@493
    aput-object v3, v2, v5

    #@495
    const-string v3, "capabilities_resp_by_app"

    #@497
    aput-object v3, v2, v6

    #@499
    const-string v3, "true"

    #@49b
    aput-object v3, v2, v7

    #@49d
    aput-object v2, v0, v1

    #@49f
    const/16 v1, 0x1d

    #@4a1
    new-array v2, v8, [Ljava/lang/String;

    #@4a3
    const-string v3, "TEXT"

    #@4a5
    aput-object v3, v2, v5

    #@4a7
    const-string v3, "pagemessage_resp_by_app"

    #@4a9
    aput-object v3, v2, v6

    #@4ab
    const-string v3, "true"

    #@4ad
    aput-object v3, v2, v7

    #@4af
    aput-object v2, v0, v1

    #@4b1
    const/16 v1, 0x1e

    #@4b3
    new-array v2, v8, [Ljava/lang/String;

    #@4b5
    const-string v3, "TEXT"

    #@4b7
    aput-object v3, v2, v5

    #@4b9
    const-string v3, "reference_resp_by_app"

    #@4bb
    aput-object v3, v2, v6

    #@4bd
    const-string v3, "false"

    #@4bf
    aput-object v3, v2, v7

    #@4c1
    aput-object v2, v0, v1

    #@4c3
    sput-object v0, Lcom/lge/ims/provider/ConfigCOM;->SIP:[[Ljava/lang/String;

    #@4c5
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 18
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public getTableContent(Ljava/lang/String;)[[Ljava/lang/String;
    .registers 4
    .parameter "name"

    #@0
    .prologue
    .line 191
    const-string v0, "lgims_com_kt_media"

    #@2
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_b

    #@8
    .line 192
    sget-object v0, Lcom/lge/ims/provider/ConfigCOM;->MEDIA:[[Ljava/lang/String;

    #@a
    .line 200
    :goto_a
    return-object v0

    #@b
    .line 193
    :cond_b
    const-string v0, "lgims_com_kt_sip"

    #@d
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@10
    move-result v0

    #@11
    if-eqz v0, :cond_23

    #@13
    .line 194
    sget-object v0, Lcom/lge/ims/Configuration;->MODEL:Ljava/lang/String;

    #@15
    const-string v1, "F320"

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@1a
    move-result v0

    #@1b
    if-eqz v0, :cond_20

    #@1d
    .line 195
    sget-object v0, Lcom/lge/ims/provider/ConfigCOM;->SIP_A1:[[Ljava/lang/String;

    #@1f
    goto :goto_a

    #@20
    .line 197
    :cond_20
    sget-object v0, Lcom/lge/ims/provider/ConfigCOM;->SIP:[[Ljava/lang/String;

    #@22
    goto :goto_a

    #@23
    .line 200
    :cond_23
    const/4 v0, 0x0

    #@24
    check-cast v0, [[Ljava/lang/String;

    #@26
    goto :goto_a
.end method

.method public getTables()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 183
    sget-object v0, Lcom/lge/ims/provider/ConfigCOM;->TABLES:[Ljava/lang/String;

    #@2
    return-object v0
.end method
