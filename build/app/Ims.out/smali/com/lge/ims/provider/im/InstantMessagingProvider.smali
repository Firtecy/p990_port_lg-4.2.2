.class public Lcom/lge/ims/provider/im/InstantMessagingProvider;
.super Landroid/content/ContentProvider;
.source "InstantMessagingProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/provider/im/InstantMessagingProvider$InstantMessagingDatabaseHelper;
    }
.end annotation


# static fields
.field private static final CHAT_INVITATION_ALL:I = 0x1

.field private static final CHAT_INVITATION_COUNT:I = 0x3

.field private static final CHAT_INVITATION_ITEM:I = 0x2

.field private static CHAT_INVITATION_PROJECTION_MAP:Ljava/util/HashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final DATABASE_NAME:Ljava/lang/String; = "lgims_im.db"

.field private static final DATABASE_VERSION:I = 0x1

.field private static final SQL_CREATE_CHAT_INVITATION:Ljava/lang/String; = "CREATE TABLE chat_invitation (_id INTEGER PRIMARY KEY AUTOINCREMENT, message_id TEXT NOT NULL,sender_uri TEXT NOT NULL);"

.field private static final TABLE_NAME_CHAT_INVITATION:Ljava/lang/String; = "chat_invitation"

.field private static URI_MATCHER:Landroid/content/UriMatcher;


# instance fields
.field private mDatabase:Landroid/database/sqlite/SQLiteDatabase;

.field private mInstantMessagingDatabaseHelper:Lcom/lge/ims/provider/im/InstantMessagingProvider$InstantMessagingDatabaseHelper;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    .line 102
    new-instance v0, Landroid/content/UriMatcher;

    #@2
    const/4 v1, -0x1

    #@3
    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    #@6
    sput-object v0, Lcom/lge/ims/provider/im/InstantMessagingProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@8
    .line 104
    new-instance v0, Ljava/util/HashMap;

    #@a
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@d
    sput-object v0, Lcom/lge/ims/provider/im/InstantMessagingProvider;->CHAT_INVITATION_PROJECTION_MAP:Ljava/util/HashMap;

    #@f
    .line 106
    sget-object v0, Lcom/lge/ims/provider/im/InstantMessagingProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@11
    const-string v1, "com.lge.ims.provider.im"

    #@13
    const-string v2, "invitation"

    #@15
    const/4 v3, 0x1

    #@16
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@19
    .line 107
    sget-object v0, Lcom/lge/ims/provider/im/InstantMessagingProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@1b
    const-string v1, "com.lge.ims.provider.im"

    #@1d
    const-string v2, "invitation/#"

    #@1f
    const/4 v3, 0x2

    #@20
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@23
    .line 108
    sget-object v0, Lcom/lge/ims/provider/im/InstantMessagingProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@25
    const-string v1, "com.lge.ims.provider.im"

    #@27
    const-string v2, "invitation/count"

    #@29
    const/4 v3, 0x3

    #@2a
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@2d
    .line 110
    sget-object v0, Lcom/lge/ims/provider/im/InstantMessagingProvider;->CHAT_INVITATION_PROJECTION_MAP:Ljava/util/HashMap;

    #@2f
    const-string v1, "_id"

    #@31
    const-string v2, "_id"

    #@33
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@36
    .line 111
    sget-object v0, Lcom/lge/ims/provider/im/InstantMessagingProvider;->CHAT_INVITATION_PROJECTION_MAP:Ljava/util/HashMap;

    #@38
    const-string v1, "message_id"

    #@3a
    const-string v2, "message_id"

    #@3c
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3f
    .line 112
    sget-object v0, Lcom/lge/ims/provider/im/InstantMessagingProvider;->CHAT_INVITATION_PROJECTION_MAP:Ljava/util/HashMap;

    #@41
    const-string v1, "sender_uri"

    #@43
    const-string v2, "sender_uri"

    #@45
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@48
    .line 113
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 21
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    #@3
    .line 49
    return-void
.end method

.method private deleteChatInvitation(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 6
    .parameter "uri"
    .parameter "selection"
    .parameter "selectionArgs"

    #@0
    .prologue
    .line 257
    iget-object v0, p0, Lcom/lge/ims/provider/im/InstantMessagingProvider;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@2
    const-string v1, "chat_invitation"

    #@4
    invoke-virtual {v0, v1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method private deleteChatInvitationItem(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 8
    .parameter "uri"
    .parameter "selection"
    .parameter "selectionArgs"

    #@0
    .prologue
    .line 261
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@3
    move-result-object v2

    #@4
    const/4 v3, 0x1

    #@5
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Ljava/lang/String;

    #@b
    .line 262
    .local v0, id:Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuffer;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    #@10
    .line 263
    .local v1, sb:Ljava/lang/StringBuffer;
    const-string v2, "_id"

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@15
    move-result-object v2

    #@16
    const/16 v3, 0x3d

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@1f
    .line 264
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@22
    move-result v2

    #@23
    if-nez v2, :cond_2e

    #@25
    .line 265
    const-string v2, " and "

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@2e
    .line 267
    :cond_2e
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@31
    move-result-object v2

    #@32
    invoke-direct {p0, p1, v2, p3}, Lcom/lge/ims/provider/im/InstantMessagingProvider;->deleteChatInvitation(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@35
    move-result v2

    #@36
    return v2
.end method

.method private executeChatInvitationCountQuery(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .registers 7
    .parameter "uri"
    .parameter "selection"
    .parameter "selectionArgs"

    #@0
    .prologue
    .line 165
    const-string v0, "select count(*) from chat_invitation"

    #@2
    .line 166
    .local v0, sql:Ljava/lang/String;
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@5
    move-result v1

    #@6
    if-nez v1, :cond_1f

    #@8
    .line 167
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, " where "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    .line 169
    :cond_1f
    iget-object v1, p0, Lcom/lge/ims/provider/im/InstantMessagingProvider;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@21
    invoke-virtual {v1, v0, p3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    #@24
    move-result-object v1

    #@25
    return-object v1
.end method

.method private executeChatInvitationItemQuery(Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;
    .registers 11
    .parameter "uri"
    .parameter "projection"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 160
    invoke-direct {p0, p1}, Lcom/lge/ims/provider/im/InstantMessagingProvider;->getChatInvitationItemQuery(Landroid/net/Uri;)Landroid/database/sqlite/SQLiteQueryBuilder;

    #@4
    move-result-object v1

    #@5
    .local v1, queryBuilder:Landroid/database/sqlite/SQLiteQueryBuilder;
    move-object v0, p0

    #@6
    move-object v2, p1

    #@7
    move-object v3, p2

    #@8
    move-object v5, v4

    #@9
    move-object v6, v4

    #@a
    move-object v7, v4

    #@b
    .line 161
    invoke-direct/range {v0 .. v7}, Lcom/lge/ims/provider/im/InstantMessagingProvider;->executeQuery(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@e
    move-result-object v0

    #@f
    return-object v0
.end method

.method private executeChatInvitationQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 14
    .parameter "uri"
    .parameter "projection"
    .parameter "selection"
    .parameter "selectionArgs"
    .parameter "sortOrder"

    #@0
    .prologue
    .line 155
    invoke-direct {p0}, Lcom/lge/ims/provider/im/InstantMessagingProvider;->getChatInvitationQuery()Landroid/database/sqlite/SQLiteQueryBuilder;

    #@3
    move-result-object v1

    #@4
    .line 156
    .local v1, queryBuilder:Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v7, 0x0

    #@5
    move-object v0, p0

    #@6
    move-object v2, p1

    #@7
    move-object v3, p2

    #@8
    move-object v4, p3

    #@9
    move-object v5, p4

    #@a
    move-object v6, p5

    #@b
    invoke-direct/range {v0 .. v7}, Lcom/lge/ims/provider/im/InstantMessagingProvider;->executeQuery(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@e
    move-result-object v0

    #@f
    return-object v0
.end method

.method private executeQuery(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 17
    .parameter "queryBuilder"
    .parameter "uri"
    .parameter "projection"
    .parameter "selection"
    .parameter "selectionArgs"
    .parameter "sortOrder"
    .parameter "limit"

    #@0
    .prologue
    .line 175
    iget-object v1, p0, Lcom/lge/ims/provider/im/InstantMessagingProvider;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@2
    const/4 v5, 0x0

    #@3
    const/4 v6, 0x0

    #@4
    move-object v0, p1

    #@5
    move-object v2, p3

    #@6
    move-object v3, p4

    #@7
    move-object v4, p5

    #@8
    move-object v7, p6

    #@9
    move-object/from16 v8, p7

    #@b
    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@e
    move-result-object v0

    #@f
    return-object v0
.end method

.method private getChatInvitationItemQuery(Landroid/net/Uri;)Landroid/database/sqlite/SQLiteQueryBuilder;
    .registers 6
    .parameter "uri"

    #@0
    .prologue
    .line 147
    invoke-direct {p0}, Lcom/lge/ims/provider/im/InstantMessagingProvider;->getChatInvitationQuery()Landroid/database/sqlite/SQLiteQueryBuilder;

    #@3
    move-result-object v0

    #@4
    .line 148
    .local v0, queryBuilder:Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v2, "_id = "

    #@b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@12
    move-result-object v1

    #@13
    const/4 v3, 0x1

    #@14
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@17
    move-result-object v1

    #@18
    check-cast v1, Ljava/lang/String;

    #@1a
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    #@25
    .line 150
    return-object v0
.end method

.method private getChatInvitationQuery()Landroid/database/sqlite/SQLiteQueryBuilder;
    .registers 3

    #@0
    .prologue
    .line 140
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    #@2
    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    #@5
    .line 141
    .local v0, queryBuilder:Landroid/database/sqlite/SQLiteQueryBuilder;
    const-string v1, "chat_invitation"

    #@7
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    #@a
    .line 142
    sget-object v1, Lcom/lge/ims/provider/im/InstantMessagingProvider;->CHAT_INVITATION_PROJECTION_MAP:Ljava/util/HashMap;

    #@c
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    #@f
    .line 143
    return-object v0
.end method

.method private updateChatInvitation(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 7
    .parameter "uri"
    .parameter "values"
    .parameter "selection"
    .parameter "selectionArgs"

    #@0
    .prologue
    .line 227
    iget-object v0, p0, Lcom/lge/ims/provider/im/InstantMessagingProvider;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@2
    const-string v1, "chat_invitation"

    #@4
    invoke-virtual {v0, v1, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method private updateChatInvitationItem(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 9
    .parameter "uri"
    .parameter "values"
    .parameter "selection"
    .parameter "selectionArgs"

    #@0
    .prologue
    .line 231
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@3
    move-result-object v2

    #@4
    const/4 v3, 0x1

    #@5
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Ljava/lang/String;

    #@b
    .line 232
    .local v0, id:Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuffer;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    #@10
    .line 233
    .local v1, sb:Ljava/lang/StringBuffer;
    const-string v2, "_id"

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@15
    move-result-object v2

    #@16
    const/16 v3, 0x3d

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@1f
    .line 234
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@22
    move-result v2

    #@23
    if-nez v2, :cond_2e

    #@25
    .line 235
    const-string v2, " and "

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2, p3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@2e
    .line 237
    :cond_2e
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@31
    move-result-object v2

    #@32
    invoke-direct {p0, p1, p2, v2, p4}, Lcom/lge/ims/provider/im/InstantMessagingProvider;->updateChatInvitation(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@35
    move-result v2

    #@36
    return v2
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 8
    .parameter "uri"
    .parameter "selection"
    .parameter "selectionArgs"

    #@0
    .prologue
    .line 242
    const/4 v0, 0x0

    #@1
    .line 243
    .local v0, count:I
    sget-object v1, Lcom/lge/ims/provider/im/InstantMessagingProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@3
    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@6
    move-result v1

    #@7
    packed-switch v1, :pswitch_data_2e

    #@a
    .line 251
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@c
    new-instance v2, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v3, "Unknown URI "

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@22
    throw v1

    #@23
    .line 245
    :pswitch_23
    invoke-direct {p0, p1, p2, p3}, Lcom/lge/ims/provider/im/InstantMessagingProvider;->deleteChatInvitation(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@26
    move-result v0

    #@27
    .line 253
    :goto_27
    return v0

    #@28
    .line 248
    :pswitch_28
    invoke-direct {p0, p1, p2, p3}, Lcom/lge/ims/provider/im/InstantMessagingProvider;->deleteChatInvitationItem(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@2b
    move-result v0

    #@2c
    .line 249
    goto :goto_27

    #@2d
    .line 243
    nop

    #@2e
    :pswitch_data_2e
    .packed-switch 0x1
        :pswitch_23
        :pswitch_28
    .end packed-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .registers 5
    .parameter "uri"

    #@0
    .prologue
    .line 272
    sget-object v0, Lcom/lge/ims/provider/im/InstantMessagingProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@5
    move-result v0

    #@6
    packed-switch v0, :pswitch_data_28

    #@9
    .line 278
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@b
    new-instance v1, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v2, "Unknown URI "

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@21
    throw v0

    #@22
    .line 274
    :pswitch_22
    const-string v0, "vnd.android.cursor.dir/vnd.lge.ims.service.im.invitation"

    #@24
    .line 276
    :goto_24
    return-object v0

    #@25
    :pswitch_25
    const-string v0, "vnd.android.cursor.item/vnd.lge.ims.service.im.invitation"

    #@27
    goto :goto_24

    #@28
    .line 272
    :pswitch_data_28
    .packed-switch 0x1
        :pswitch_22
        :pswitch_25
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .registers 9
    .parameter "uri"
    .parameter "values"

    #@0
    .prologue
    .line 181
    const-string v3, "message_id"

    #@2
    invoke-virtual {p2, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    #@5
    move-result v3

    #@6
    if-nez v3, :cond_10

    #@8
    .line 182
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@a
    const-string v4, "message_id should not be null"

    #@c
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@f
    throw v3

    #@10
    .line 185
    :cond_10
    const-string v3, "sender_uri"

    #@12
    invoke-virtual {p2, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    #@15
    move-result v3

    #@16
    if-nez v3, :cond_20

    #@18
    .line 186
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@1a
    const-string v4, "sender_uri should not be null"

    #@1c
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v3

    #@20
    .line 189
    :cond_20
    const/4 v0, 0x0

    #@21
    .line 190
    .local v0, insertedUri:Landroid/net/Uri;
    sget-object v3, Lcom/lge/ims/provider/im/InstantMessagingProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@23
    invoke-virtual {v3, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@26
    move-result v3

    #@27
    packed-switch v3, :pswitch_data_72

    #@2a
    .line 198
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@2c
    new-instance v4, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v5, "Unknown URI "

    #@33
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v4

    #@37
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v4

    #@3b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v4

    #@3f
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@42
    throw v3

    #@43
    .line 192
    :pswitch_43
    iget-object v3, p0, Lcom/lge/ims/provider/im/InstantMessagingProvider;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@45
    const-string v4, "chat_invitation"

    #@47
    const/4 v5, 0x0

    #@48
    invoke-virtual {v3, v4, v5, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    #@4b
    move-result-wide v1

    #@4c
    .line 193
    .local v1, rowId:J
    const-wide/16 v3, 0x0

    #@4e
    cmp-long v3, v1, v3

    #@50
    if-lez v3, :cond_56

    #@52
    .line 194
    invoke-static {p1, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    #@55
    move-result-object v0

    #@56
    .line 201
    :cond_56
    if-eqz v0, :cond_59

    #@58
    .line 202
    return-object v0

    #@59
    .line 205
    :cond_59
    new-instance v3, Landroid/database/SQLException;

    #@5b
    new-instance v4, Ljava/lang/StringBuilder;

    #@5d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@60
    const-string v5, "Failed to insert row into "

    #@62
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v4

    #@66
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v4

    #@6a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d
    move-result-object v4

    #@6e
    invoke-direct {v3, v4}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    #@71
    throw v3

    #@72
    .line 190
    :pswitch_data_72
    .packed-switch 0x1
        :pswitch_43
    .end packed-switch
.end method

.method public onCreate()Z
    .registers 6

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 83
    new-instance v1, Lcom/lge/ims/provider/im/InstantMessagingProvider$InstantMessagingDatabaseHelper;

    #@3
    invoke-virtual {p0}, Lcom/lge/ims/provider/im/InstantMessagingProvider;->getContext()Landroid/content/Context;

    #@6
    move-result-object v2

    #@7
    const-string v3, "lgims_im.db"

    #@9
    const/4 v4, 0x0

    #@a
    invoke-direct {v1, v2, v3, v4, v0}, Lcom/lge/ims/provider/im/InstantMessagingProvider$InstantMessagingDatabaseHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    #@d
    iput-object v1, p0, Lcom/lge/ims/provider/im/InstantMessagingProvider;->mInstantMessagingDatabaseHelper:Lcom/lge/ims/provider/im/InstantMessagingProvider$InstantMessagingDatabaseHelper;

    #@f
    .line 89
    iget-object v1, p0, Lcom/lge/ims/provider/im/InstantMessagingProvider;->mInstantMessagingDatabaseHelper:Lcom/lge/ims/provider/im/InstantMessagingProvider$InstantMessagingDatabaseHelper;

    #@11
    invoke-virtual {v1}, Lcom/lge/ims/provider/im/InstantMessagingProvider$InstantMessagingDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@14
    move-result-object v1

    #@15
    iput-object v1, p0, Lcom/lge/ims/provider/im/InstantMessagingProvider;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@17
    .line 91
    iget-object v1, p0, Lcom/lge/ims/provider/im/InstantMessagingProvider;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@19
    if-nez v1, :cond_1c

    #@1b
    .line 92
    const/4 v0, 0x0

    #@1c
    .line 95
    :cond_1c
    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 10
    .parameter "uri"
    .parameter "projection"
    .parameter "selection"
    .parameter "selectionArgs"
    .parameter "sortOrder"

    #@0
    .prologue
    .line 117
    const/4 v0, 0x0

    #@1
    .line 119
    .local v0, cursor:Landroid/database/Cursor;
    sget-object v1, Lcom/lge/ims/provider/im/InstantMessagingProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@3
    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@6
    move-result v1

    #@7
    packed-switch v1, :pswitch_data_40

    #@a
    .line 130
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@c
    new-instance v2, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v3, "Unknown URI "

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@22
    throw v1

    #@23
    .line 121
    :pswitch_23
    invoke-direct/range {p0 .. p5}, Lcom/lge/ims/provider/im/InstantMessagingProvider;->executeChatInvitationQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@26
    move-result-object v0

    #@27
    .line 133
    :goto_27
    if-eqz v0, :cond_34

    #@29
    .line 134
    invoke-virtual {p0}, Lcom/lge/ims/provider/im/InstantMessagingProvider;->getContext()Landroid/content/Context;

    #@2c
    move-result-object v1

    #@2d
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@30
    move-result-object v1

    #@31
    invoke-interface {v0, v1, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    #@34
    .line 136
    :cond_34
    return-object v0

    #@35
    .line 124
    :pswitch_35
    invoke-direct {p0, p1, p2}, Lcom/lge/ims/provider/im/InstantMessagingProvider;->executeChatInvitationItemQuery(Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;

    #@38
    move-result-object v0

    #@39
    .line 125
    goto :goto_27

    #@3a
    .line 127
    :pswitch_3a
    invoke-direct {p0, p1, p3, p4}, Lcom/lge/ims/provider/im/InstantMessagingProvider;->executeChatInvitationCountQuery(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    #@3d
    move-result-object v0

    #@3e
    .line 128
    goto :goto_27

    #@3f
    .line 119
    nop

    #@40
    :pswitch_data_40
    .packed-switch 0x1
        :pswitch_23
        :pswitch_35
        :pswitch_3a
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 9
    .parameter "uri"
    .parameter "values"
    .parameter "selection"
    .parameter "selectionArgs"

    #@0
    .prologue
    .line 210
    const/4 v0, 0x0

    #@1
    .line 212
    .local v0, count:I
    sget-object v1, Lcom/lge/ims/provider/im/InstantMessagingProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@3
    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@6
    move-result v1

    #@7
    packed-switch v1, :pswitch_data_2e

    #@a
    .line 220
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@c
    new-instance v2, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v3, "Unknown URI "

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@22
    throw v1

    #@23
    .line 214
    :pswitch_23
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/lge/ims/provider/im/InstantMessagingProvider;->updateChatInvitation(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@26
    move-result v0

    #@27
    .line 222
    :goto_27
    return v0

    #@28
    .line 217
    :pswitch_28
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/lge/ims/provider/im/InstantMessagingProvider;->updateChatInvitationItem(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@2b
    move-result v0

    #@2c
    .line 218
    goto :goto_27

    #@2d
    .line 212
    nop

    #@2e
    :pswitch_data_2e
    .packed-switch 0x1
        :pswitch_23
        :pswitch_28
    .end packed-switch
.end method
