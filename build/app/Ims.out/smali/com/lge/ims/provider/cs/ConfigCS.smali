.class public Lcom/lge/ims/provider/cs/ConfigCS;
.super Ljava/lang/Object;
.source "ConfigCS.java"

# interfaces
.implements Lcom/lge/ims/provider/IConfiguration;


# static fields
.field private static final APP:[[Ljava/lang/String;

.field private static final MEDIA_CODEC:[[Ljava/lang/String;

.field private static final MEDIA_VIDEO:[[Ljava/lang/String;

.field private static final SERVICE_IS:[[Ljava/lang/String;

.field private static final SERVICE_VS:[[Ljava/lang/String;

.field private static final TABLES:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 9

    #@0
    .prologue
    const/4 v8, 0x4

    #@1
    const/4 v7, 0x3

    #@2
    const/4 v6, 0x2

    #@3
    const/4 v5, 0x1

    #@4
    const/4 v4, 0x0

    #@5
    .line 20
    const/4 v0, 0x5

    #@6
    new-array v0, v0, [Ljava/lang/String;

    #@8
    const-string v1, "lgims_com_kt_app_rcse_cs"

    #@a
    aput-object v1, v0, v4

    #@c
    const-string v1, "lgims_com_kt_service_rcse_vs"

    #@e
    aput-object v1, v0, v5

    #@10
    const-string v1, "lgims_com_kt_service_rcse_is"

    #@12
    aput-object v1, v0, v6

    #@14
    const-string v1, "lgims_com_kt_media_vs_video"

    #@16
    aput-object v1, v0, v7

    #@18
    const-string v1, "lgims_com_kt_media_vs_codec"

    #@1a
    aput-object v1, v0, v8

    #@1c
    sput-object v0, Lcom/lge/ims/provider/cs/ConfigCS;->TABLES:[Ljava/lang/String;

    #@1e
    .line 31
    new-array v0, v7, [[Ljava/lang/String;

    #@20
    new-array v1, v7, [Ljava/lang/String;

    #@22
    const-string v2, "INTEGER"

    #@24
    aput-object v2, v1, v4

    #@26
    const-string v2, "id"

    #@28
    aput-object v2, v1, v5

    #@2a
    const-string v2, "1"

    #@2c
    aput-object v2, v1, v6

    #@2e
    aput-object v1, v0, v4

    #@30
    new-array v1, v7, [Ljava/lang/String;

    #@32
    const-string v2, "INTEGER"

    #@34
    aput-object v2, v1, v4

    #@36
    const-string v2, "property"

    #@38
    aput-object v2, v1, v5

    #@3a
    const-string v2, "0"

    #@3c
    aput-object v2, v1, v6

    #@3e
    aput-object v1, v0, v5

    #@40
    new-array v1, v7, [Ljava/lang/String;

    #@42
    const-string v2, "TEXT"

    #@44
    aput-object v2, v1, v4

    #@46
    const-string v2, "conf"

    #@48
    aput-object v2, v1, v5

    #@4a
    const-string v2, "[Uniqueness]\nStream=1\nFramed=1\nBasic=0\nEvent=0\nCoreService=4\nQos=0\nReg=4\nWrite=1\nRead=1\nCap=0\nMprof=2\nConnection=1\nIMService=0\nPresenceService=0\nXDMService=0\n\n[IMSRegistry]\nIMSRegistry=lgims.com.kt.app.rcse.cs\n\n[Stream]\nmedia_types=Video\n\n[Framed]\nmedia_types=text/plain text/html image/jpeg image/gif image/bmp image/png\nmax_size=4096\n\n[CoreService_0]\nservice_id=lgims.com.kt.service.rcse.vs\niari=\nicsi=\nfeature_tag=+g.3gpp.cs-voice;require;explicit\n\n[CoreService_1]\nservice_id=lgims.com.kt.service.rcse.is\niari=urn:urn-7:3gpp-application.ims.iari.gsma-is\nicsi=\nfeature_tag=+g.3gpp.cs-voice;explicit\n\n[CoreService_2]\nservice_id=lgims.com.kt.service.rcse.vs.wifi\niari=\nicsi=\nfeature_tag=+g.3gpp.cs-voice;require;explicit\n\n[CoreService_3]\nservice_id=lgims.com.kt.service.rcse.is.wifi\niari=urn:urn-7:3gpp-application.ims.iari.gsma-is\nicsi=\nfeature_tag=+g.3gpp.cs-voice;explicit\n\n[Reg_0]\nservice_id=lgims.com.kt.service.rcse.vs\nheader_count=1\nheader_0=Supported: path\n\n[Reg_1]\nservice_id=lgims.com.kt.service.rcse.is\nheader_count=1\nheader_0=Supported: path\n\n[Reg_2]\nservice_id=lgims.com.kt.service.rcse.vs.wifi\nheader_count=1\nheader_0=Supported: path\n\n[Reg_3]\nservice_id=lgims.com.kt.service.rcse.is.wifi\nheader_count=1\nheader_0=Supported: path\n\n[Write]\nheader_names=Subject\n\n[Read]\nheader_names=Subject\n\n[Mprof_0]\nservice_id=lgims.com.kt.service.rcse.vs\nprofile=com.lgu.media.vs\n\n[Mprof_1]\nservice_id=lgims.com.kt.service.rcse.is\nprofile=com.lgu.media.is\n\n[Connection_0]\nservice_id=lgims.com.kt.service.rcse.is\n\n"

    #@4c
    aput-object v2, v1, v6

    #@4e
    aput-object v1, v0, v6

    #@50
    sput-object v0, Lcom/lge/ims/provider/cs/ConfigCS;->APP:[[Ljava/lang/String;

    #@52
    .line 129
    const/16 v0, 0xc

    #@54
    new-array v0, v0, [[Ljava/lang/String;

    #@56
    new-array v1, v7, [Ljava/lang/String;

    #@58
    const-string v2, "INTEGER"

    #@5a
    aput-object v2, v1, v4

    #@5c
    const-string v2, "id"

    #@5e
    aput-object v2, v1, v5

    #@60
    const-string v2, "1"

    #@62
    aput-object v2, v1, v6

    #@64
    aput-object v1, v0, v4

    #@66
    new-array v1, v7, [Ljava/lang/String;

    #@68
    const-string v2, "INTEGER"

    #@6a
    aput-object v2, v1, v4

    #@6c
    const-string v2, "property"

    #@6e
    aput-object v2, v1, v5

    #@70
    const-string v2, "1"

    #@72
    aput-object v2, v1, v6

    #@74
    aput-object v1, v0, v5

    #@76
    new-array v1, v7, [Ljava/lang/String;

    #@78
    const-string v2, "INTEGER"

    #@7a
    aput-object v2, v1, v4

    #@7c
    const-string v2, "session_max_count"

    #@7e
    aput-object v2, v1, v5

    #@80
    const-string v2, "1"

    #@82
    aput-object v2, v1, v6

    #@84
    aput-object v1, v0, v6

    #@86
    new-array v1, v7, [Ljava/lang/String;

    #@88
    const-string v2, "INTEGER"

    #@8a
    aput-object v2, v1, v4

    #@8c
    const-string v2, "session_tv_mo_no_answer"

    #@8e
    aput-object v2, v1, v5

    #@90
    const-string v2, "72"

    #@92
    aput-object v2, v1, v6

    #@94
    aput-object v1, v0, v7

    #@96
    new-array v1, v7, [Ljava/lang/String;

    #@98
    const-string v2, "INTEGER"

    #@9a
    aput-object v2, v1, v4

    #@9c
    const-string v2, "session_tv_mo_1xx_wait"

    #@9e
    aput-object v2, v1, v5

    #@a0
    const-string v2, "44"

    #@a2
    aput-object v2, v1, v6

    #@a4
    aput-object v1, v0, v8

    #@a6
    const/4 v1, 0x5

    #@a7
    new-array v2, v7, [Ljava/lang/String;

    #@a9
    const-string v3, "INTEGER"

    #@ab
    aput-object v3, v2, v4

    #@ad
    const-string v3, "session_tv_mt_alerting"

    #@af
    aput-object v3, v2, v5

    #@b1
    const-string v3, "90"

    #@b3
    aput-object v3, v2, v6

    #@b5
    aput-object v2, v0, v1

    #@b7
    const/4 v1, 0x6

    #@b8
    new-array v2, v7, [Ljava/lang/String;

    #@ba
    const-string v3, "TEXT"

    #@bc
    aput-object v3, v2, v4

    #@be
    const-string v3, "session_conf_factory_uri"

    #@c0
    aput-object v3, v2, v5

    #@c2
    const-string v3, ""

    #@c4
    aput-object v3, v2, v6

    #@c6
    aput-object v2, v0, v1

    #@c8
    const/4 v1, 0x7

    #@c9
    new-array v2, v7, [Ljava/lang/String;

    #@cb
    const-string v3, "TEXT"

    #@cd
    aput-object v3, v2, v4

    #@cf
    const-string v3, "session_rpr_supported"

    #@d1
    aput-object v3, v2, v5

    #@d3
    const-string v3, "false"

    #@d5
    aput-object v3, v2, v6

    #@d7
    aput-object v2, v0, v1

    #@d9
    const/16 v1, 0x8

    #@db
    new-array v2, v7, [Ljava/lang/String;

    #@dd
    const-string v3, "TEXT"

    #@df
    aput-object v3, v2, v4

    #@e1
    const-string v3, "media_loopback"

    #@e3
    aput-object v3, v2, v5

    #@e5
    const-string v3, "false"

    #@e7
    aput-object v3, v2, v6

    #@e9
    aput-object v2, v0, v1

    #@eb
    const/16 v1, 0x9

    #@ed
    new-array v2, v7, [Ljava/lang/String;

    #@ef
    const-string v3, "INTEGER"

    #@f1
    aput-object v3, v2, v4

    #@f3
    const-string v3, "media_tv_heartbeat"

    #@f5
    aput-object v3, v2, v5

    #@f7
    const-string v3, "10"

    #@f9
    aput-object v3, v2, v6

    #@fb
    aput-object v2, v0, v1

    #@fd
    const/16 v1, 0xa

    #@ff
    new-array v2, v7, [Ljava/lang/String;

    #@101
    const-string v3, "INTEGER"

    #@103
    aput-object v3, v2, v4

    #@105
    const-string v3, "media_video_count"

    #@107
    aput-object v3, v2, v5

    #@109
    const-string v3, "1"

    #@10b
    aput-object v3, v2, v6

    #@10d
    aput-object v2, v0, v1

    #@10f
    const/16 v1, 0xb

    #@111
    new-array v2, v7, [Ljava/lang/String;

    #@113
    const-string v3, "TEXT"

    #@115
    aput-object v3, v2, v4

    #@117
    const-string v3, "media_video_ref"

    #@119
    aput-object v3, v2, v5

    #@11b
    const-string v3, "lgims.com.kt.media.vs.video"

    #@11d
    aput-object v3, v2, v6

    #@11f
    aput-object v2, v0, v1

    #@121
    sput-object v0, Lcom/lge/ims/provider/cs/ConfigCS;->SERVICE_VS:[[Ljava/lang/String;

    #@123
    .line 149
    const/16 v0, 0xc

    #@125
    new-array v0, v0, [[Ljava/lang/String;

    #@127
    new-array v1, v7, [Ljava/lang/String;

    #@129
    const-string v2, "INTEGER"

    #@12b
    aput-object v2, v1, v4

    #@12d
    const-string v2, "id"

    #@12f
    aput-object v2, v1, v5

    #@131
    const-string v2, "1"

    #@133
    aput-object v2, v1, v6

    #@135
    aput-object v1, v0, v4

    #@137
    new-array v1, v7, [Ljava/lang/String;

    #@139
    const-string v2, "INTEGER"

    #@13b
    aput-object v2, v1, v4

    #@13d
    const-string v2, "property"

    #@13f
    aput-object v2, v1, v5

    #@141
    const-string v2, "1"

    #@143
    aput-object v2, v1, v6

    #@145
    aput-object v1, v0, v5

    #@147
    new-array v1, v7, [Ljava/lang/String;

    #@149
    const-string v2, "INTEGER"

    #@14b
    aput-object v2, v1, v4

    #@14d
    const-string v2, "session_max_count"

    #@14f
    aput-object v2, v1, v5

    #@151
    const-string v2, "1"

    #@153
    aput-object v2, v1, v6

    #@155
    aput-object v1, v0, v6

    #@157
    new-array v1, v7, [Ljava/lang/String;

    #@159
    const-string v2, "INTEGER"

    #@15b
    aput-object v2, v1, v4

    #@15d
    const-string v2, "session_tv_mo_no_answer"

    #@15f
    aput-object v2, v1, v5

    #@161
    const-string v2, "72"

    #@163
    aput-object v2, v1, v6

    #@165
    aput-object v1, v0, v7

    #@167
    new-array v1, v7, [Ljava/lang/String;

    #@169
    const-string v2, "INTEGER"

    #@16b
    aput-object v2, v1, v4

    #@16d
    const-string v2, "session_tv_mo_1xx_wait"

    #@16f
    aput-object v2, v1, v5

    #@171
    const-string v2, "44"

    #@173
    aput-object v2, v1, v6

    #@175
    aput-object v1, v0, v8

    #@177
    const/4 v1, 0x5

    #@178
    new-array v2, v7, [Ljava/lang/String;

    #@17a
    const-string v3, "INTEGER"

    #@17c
    aput-object v3, v2, v4

    #@17e
    const-string v3, "session_tv_mt_alerting"

    #@180
    aput-object v3, v2, v5

    #@182
    const-string v3, "90"

    #@184
    aput-object v3, v2, v6

    #@186
    aput-object v2, v0, v1

    #@188
    const/4 v1, 0x6

    #@189
    new-array v2, v7, [Ljava/lang/String;

    #@18b
    const-string v3, "TEXT"

    #@18d
    aput-object v3, v2, v4

    #@18f
    const-string v3, "session_conf_factory_uri"

    #@191
    aput-object v3, v2, v5

    #@193
    const-string v3, ""

    #@195
    aput-object v3, v2, v6

    #@197
    aput-object v2, v0, v1

    #@199
    const/4 v1, 0x7

    #@19a
    new-array v2, v7, [Ljava/lang/String;

    #@19c
    const-string v3, "TEXT"

    #@19e
    aput-object v3, v2, v4

    #@1a0
    const-string v3, "session_rpr_supported"

    #@1a2
    aput-object v3, v2, v5

    #@1a4
    const-string v3, "false"

    #@1a6
    aput-object v3, v2, v6

    #@1a8
    aput-object v2, v0, v1

    #@1aa
    const/16 v1, 0x8

    #@1ac
    new-array v2, v7, [Ljava/lang/String;

    #@1ae
    const-string v3, "TEXT"

    #@1b0
    aput-object v3, v2, v4

    #@1b2
    const-string v3, "media_loopback"

    #@1b4
    aput-object v3, v2, v5

    #@1b6
    const-string v3, "false"

    #@1b8
    aput-object v3, v2, v6

    #@1ba
    aput-object v2, v0, v1

    #@1bc
    const/16 v1, 0x9

    #@1be
    new-array v2, v7, [Ljava/lang/String;

    #@1c0
    const-string v3, "INTEGER"

    #@1c2
    aput-object v3, v2, v4

    #@1c4
    const-string v3, "media_tv_heartbeat"

    #@1c6
    aput-object v3, v2, v5

    #@1c8
    const-string v3, "10"

    #@1ca
    aput-object v3, v2, v6

    #@1cc
    aput-object v2, v0, v1

    #@1ce
    const/16 v1, 0xa

    #@1d0
    new-array v2, v7, [Ljava/lang/String;

    #@1d2
    const-string v3, "INTEGER"

    #@1d4
    aput-object v3, v2, v4

    #@1d6
    const-string v3, "media_message_count"

    #@1d8
    aput-object v3, v2, v5

    #@1da
    const-string v3, "0"

    #@1dc
    aput-object v3, v2, v6

    #@1de
    aput-object v2, v0, v1

    #@1e0
    const/16 v1, 0xb

    #@1e2
    new-array v2, v7, [Ljava/lang/String;

    #@1e4
    const-string v3, "TEXT"

    #@1e6
    aput-object v3, v2, v4

    #@1e8
    const-string v3, "media_message_ref"

    #@1ea
    aput-object v3, v2, v5

    #@1ec
    const-string v3, "lgims.com.kt.media.is.message"

    #@1ee
    aput-object v3, v2, v6

    #@1f0
    aput-object v2, v0, v1

    #@1f2
    sput-object v0, Lcom/lge/ims/provider/cs/ConfigCS;->SERVICE_IS:[[Ljava/lang/String;

    #@1f4
    .line 169
    const/16 v0, 0xc

    #@1f6
    new-array v0, v0, [[Ljava/lang/String;

    #@1f8
    new-array v1, v7, [Ljava/lang/String;

    #@1fa
    const-string v2, "INTEGER"

    #@1fc
    aput-object v2, v1, v4

    #@1fe
    const-string v2, "id"

    #@200
    aput-object v2, v1, v5

    #@202
    const-string v2, "1"

    #@204
    aput-object v2, v1, v6

    #@206
    aput-object v1, v0, v4

    #@208
    new-array v1, v7, [Ljava/lang/String;

    #@20a
    const-string v2, "INTEGER"

    #@20c
    aput-object v2, v1, v4

    #@20e
    const-string v2, "property"

    #@210
    aput-object v2, v1, v5

    #@212
    const-string v2, "1"

    #@214
    aput-object v2, v1, v6

    #@216
    aput-object v1, v0, v5

    #@218
    new-array v1, v7, [Ljava/lang/String;

    #@21a
    const-string v2, "INTEGER"

    #@21c
    aput-object v2, v1, v4

    #@21e
    const-string v2, "video_0_port_rtp"

    #@220
    aput-object v2, v1, v5

    #@222
    const-string v2, "30000"

    #@224
    aput-object v2, v1, v6

    #@226
    aput-object v1, v0, v6

    #@228
    new-array v1, v7, [Ljava/lang/String;

    #@22a
    const-string v2, "INTEGER"

    #@22c
    aput-object v2, v1, v4

    #@22e
    const-string v2, "video_0_port_rtcp"

    #@230
    aput-object v2, v1, v5

    #@232
    const-string v2, "0"

    #@234
    aput-object v2, v1, v6

    #@236
    aput-object v1, v0, v7

    #@238
    new-array v1, v7, [Ljava/lang/String;

    #@23a
    const-string v2, "INTEGER"

    #@23c
    aput-object v2, v1, v4

    #@23e
    const-string v2, "video_0_tv_rtp_inactivity"

    #@240
    aput-object v2, v1, v5

    #@242
    const-string v2, "0"

    #@244
    aput-object v2, v1, v6

    #@246
    aput-object v1, v0, v8

    #@248
    const/4 v1, 0x5

    #@249
    new-array v2, v7, [Ljava/lang/String;

    #@24b
    const-string v3, "TEXT"

    #@24d
    aput-object v3, v2, v4

    #@24f
    const-string v3, "video_0_jitter_buffer_size"

    #@251
    aput-object v3, v2, v5

    #@253
    const-string v3, "13,13,19"

    #@255
    aput-object v3, v2, v6

    #@257
    aput-object v2, v0, v1

    #@259
    const/4 v1, 0x6

    #@25a
    new-array v2, v7, [Ljava/lang/String;

    #@25c
    const-string v3, "INTEGER"

    #@25e
    aput-object v3, v2, v4

    #@260
    const-string v3, "video_0_framerate"

    #@262
    aput-object v3, v2, v5

    #@264
    const-string v3, "20"

    #@266
    aput-object v3, v2, v6

    #@268
    aput-object v2, v0, v1

    #@26a
    const/4 v1, 0x7

    #@26b
    new-array v2, v7, [Ljava/lang/String;

    #@26d
    const-string v3, "TEXT"

    #@26f
    aput-object v3, v2, v4

    #@271
    const-string v3, "video_0_framesize"

    #@273
    aput-object v3, v2, v5

    #@275
    const-string v3, "QVGA"

    #@277
    aput-object v3, v2, v6

    #@279
    aput-object v2, v0, v1

    #@27b
    const/16 v1, 0x8

    #@27d
    new-array v2, v7, [Ljava/lang/String;

    #@27f
    const-string v3, "INTEGER"

    #@281
    aput-object v3, v2, v4

    #@283
    const-string v3, "video_0_bitrate"

    #@285
    aput-object v3, v2, v5

    #@287
    const-string v3, "384"

    #@289
    aput-object v3, v2, v6

    #@28b
    aput-object v2, v0, v1

    #@28d
    const/16 v1, 0x9

    #@28f
    new-array v2, v7, [Ljava/lang/String;

    #@291
    const-string v3, "TEXT"

    #@293
    aput-object v3, v2, v4

    #@295
    const-string v3, "video_0_bitrate_control"

    #@297
    aput-object v3, v2, v5

    #@299
    const-string v3, "CBR"

    #@29b
    aput-object v3, v2, v6

    #@29d
    aput-object v2, v0, v1

    #@29f
    const/16 v1, 0xa

    #@2a1
    new-array v2, v7, [Ljava/lang/String;

    #@2a3
    const-string v3, "TEXT"

    #@2a5
    aput-object v3, v2, v4

    #@2a7
    const-string v3, "video_0_codecs"

    #@2a9
    aput-object v3, v2, v5

    #@2ab
    const-string v3, "H264,H263"

    #@2ad
    aput-object v3, v2, v6

    #@2af
    aput-object v2, v0, v1

    #@2b1
    const/16 v1, 0xb

    #@2b3
    new-array v2, v7, [Ljava/lang/String;

    #@2b5
    const-string v3, "TEXT"

    #@2b7
    aput-object v3, v2, v4

    #@2b9
    const-string v3, "video_0_codec_ref"

    #@2bb
    aput-object v3, v2, v5

    #@2bd
    const-string v3, "lgims.com.kt.media.vs.codec"

    #@2bf
    aput-object v3, v2, v6

    #@2c1
    aput-object v2, v0, v1

    #@2c3
    sput-object v0, Lcom/lge/ims/provider/cs/ConfigCS;->MEDIA_VIDEO:[[Ljava/lang/String;

    #@2c5
    .line 189
    const/16 v0, 0xa

    #@2c7
    new-array v0, v0, [[Ljava/lang/String;

    #@2c9
    new-array v1, v7, [Ljava/lang/String;

    #@2cb
    const-string v2, "INTEGER"

    #@2cd
    aput-object v2, v1, v4

    #@2cf
    const-string v2, "id"

    #@2d1
    aput-object v2, v1, v5

    #@2d3
    const-string v2, "1"

    #@2d5
    aput-object v2, v1, v6

    #@2d7
    aput-object v1, v0, v4

    #@2d9
    new-array v1, v7, [Ljava/lang/String;

    #@2db
    const-string v2, "INTEGER"

    #@2dd
    aput-object v2, v1, v4

    #@2df
    const-string v2, "property"

    #@2e1
    aput-object v2, v1, v5

    #@2e3
    const-string v2, "1"

    #@2e5
    aput-object v2, v1, v6

    #@2e7
    aput-object v1, v0, v5

    #@2e9
    new-array v1, v7, [Ljava/lang/String;

    #@2eb
    const-string v2, "INTEGER"

    #@2ed
    aput-object v2, v1, v4

    #@2ef
    const-string v2, "H263_payload_type"

    #@2f1
    aput-object v2, v1, v5

    #@2f3
    const-string v2, "34"

    #@2f5
    aput-object v2, v1, v6

    #@2f7
    aput-object v1, v0, v6

    #@2f9
    new-array v1, v7, [Ljava/lang/String;

    #@2fb
    const-string v2, "INTEGER"

    #@2fd
    aput-object v2, v1, v4

    #@2ff
    const-string v2, "H263_profile"

    #@301
    aput-object v2, v1, v5

    #@303
    const-string v2, "0"

    #@305
    aput-object v2, v1, v6

    #@307
    aput-object v1, v0, v7

    #@309
    new-array v1, v7, [Ljava/lang/String;

    #@30b
    const-string v2, "INTEGER"

    #@30d
    aput-object v2, v1, v4

    #@30f
    const-string v2, "H263_level"

    #@311
    aput-object v2, v1, v5

    #@313
    const-string v2, "10"

    #@315
    aput-object v2, v1, v6

    #@317
    aput-object v1, v0, v8

    #@319
    const/4 v1, 0x5

    #@31a
    new-array v2, v7, [Ljava/lang/String;

    #@31c
    const-string v3, "TEXT"

    #@31e
    aput-object v3, v2, v4

    #@320
    const-string v3, "H263_framesize"

    #@322
    aput-object v3, v2, v5

    #@324
    const-string v3, "176-144"

    #@326
    aput-object v3, v2, v6

    #@328
    aput-object v2, v0, v1

    #@32a
    const/4 v1, 0x6

    #@32b
    new-array v2, v7, [Ljava/lang/String;

    #@32d
    const-string v3, "INTEGER"

    #@32f
    aput-object v3, v2, v4

    #@331
    const-string v3, "H264_payload_type"

    #@333
    aput-object v3, v2, v5

    #@335
    const-string v3, "102"

    #@337
    aput-object v3, v2, v6

    #@339
    aput-object v2, v0, v1

    #@33b
    const/4 v1, 0x7

    #@33c
    new-array v2, v7, [Ljava/lang/String;

    #@33e
    const-string v3, "INTEGER"

    #@340
    aput-object v3, v2, v4

    #@342
    const-string v3, "H264_packetization_mode"

    #@344
    aput-object v3, v2, v5

    #@346
    const-string v3, "1"

    #@348
    aput-object v3, v2, v6

    #@34a
    aput-object v2, v0, v1

    #@34c
    const/16 v1, 0x8

    #@34e
    new-array v2, v7, [Ljava/lang/String;

    #@350
    const-string v3, "TEXT"

    #@352
    aput-object v3, v2, v4

    #@354
    const-string v3, "H264_profile_level_id"

    #@356
    aput-object v3, v2, v5

    #@358
    const-string v3, "42C016"

    #@35a
    aput-object v3, v2, v6

    #@35c
    aput-object v2, v0, v1

    #@35e
    const/16 v1, 0x9

    #@360
    new-array v2, v7, [Ljava/lang/String;

    #@362
    const-string v3, "TEXT"

    #@364
    aput-object v3, v2, v4

    #@366
    const-string v3, "H264_framesize"

    #@368
    aput-object v3, v2, v5

    #@36a
    const-string v3, "480-640"

    #@36c
    aput-object v3, v2, v6

    #@36e
    aput-object v2, v0, v1

    #@370
    sput-object v0, Lcom/lge/ims/provider/cs/ConfigCS;->MEDIA_CODEC:[[Ljava/lang/String;

    #@372
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 18
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public getTableContent(Ljava/lang/String;)[[Ljava/lang/String;
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 216
    const-string v0, "lgims_com_kt_app_rcse_cs"

    #@2
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_b

    #@8
    .line 217
    sget-object v0, Lcom/lge/ims/provider/cs/ConfigCS;->APP:[[Ljava/lang/String;

    #@a
    .line 227
    :goto_a
    return-object v0

    #@b
    .line 218
    :cond_b
    const-string v0, "lgims_com_kt_service_rcse_vs"

    #@d
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@10
    move-result v0

    #@11
    if-eqz v0, :cond_16

    #@13
    .line 219
    sget-object v0, Lcom/lge/ims/provider/cs/ConfigCS;->SERVICE_VS:[[Ljava/lang/String;

    #@15
    goto :goto_a

    #@16
    .line 220
    :cond_16
    const-string v0, "lgims_com_kt_service_rcse_is"

    #@18
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1b
    move-result v0

    #@1c
    if-eqz v0, :cond_21

    #@1e
    .line 221
    sget-object v0, Lcom/lge/ims/provider/cs/ConfigCS;->SERVICE_IS:[[Ljava/lang/String;

    #@20
    goto :goto_a

    #@21
    .line 222
    :cond_21
    const-string v0, "lgims_com_kt_media_vs_video"

    #@23
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@26
    move-result v0

    #@27
    if-eqz v0, :cond_2c

    #@29
    .line 223
    sget-object v0, Lcom/lge/ims/provider/cs/ConfigCS;->MEDIA_VIDEO:[[Ljava/lang/String;

    #@2b
    goto :goto_a

    #@2c
    .line 224
    :cond_2c
    const-string v0, "lgims_com_kt_media_vs_codec"

    #@2e
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@31
    move-result v0

    #@32
    if-eqz v0, :cond_37

    #@34
    .line 225
    sget-object v0, Lcom/lge/ims/provider/cs/ConfigCS;->MEDIA_CODEC:[[Ljava/lang/String;

    #@36
    goto :goto_a

    #@37
    .line 227
    :cond_37
    const/4 v0, 0x0

    #@38
    check-cast v0, [[Ljava/lang/String;

    #@3a
    goto :goto_a
.end method

.method public getTables()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 208
    sget-object v0, Lcom/lge/ims/provider/cs/ConfigCS;->TABLES:[Ljava/lang/String;

    #@2
    return-object v0
.end method
