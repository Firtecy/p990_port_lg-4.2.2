.class public Lcom/lge/ims/provider/uc/ConfigUC;
.super Ljava/lang/Object;
.source "ConfigUC.java"

# interfaces
.implements Lcom/lge/ims/provider/IConfiguration;


# static fields
.field private static final APP:[[Ljava/lang/String;

.field private static final SERVICE_UC:[[Ljava/lang/String;

.field private static final SERVICE_VT:[[Ljava/lang/String;

.field private static final SIP_VT:[[Ljava/lang/String;

.field private static final SIP_VT_A1:[[Ljava/lang/String;

.field private static final TABLES:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 10

    #@0
    .prologue
    const/4 v9, 0x4

    #@1
    const/4 v8, 0x3

    #@2
    const/4 v7, 0x2

    #@3
    const/4 v6, 0x1

    #@4
    const/4 v5, 0x0

    #@5
    .line 21
    new-array v0, v9, [Ljava/lang/String;

    #@7
    const-string v1, "lgims_com_kt_app_uc"

    #@9
    aput-object v1, v0, v5

    #@b
    const-string v1, "lgims_com_kt_service_uc"

    #@d
    aput-object v1, v0, v6

    #@f
    const-string v1, "lgims_com_kt_service_vt"

    #@11
    aput-object v1, v0, v7

    #@13
    const-string v1, "lgims_com_kt_sip_vt"

    #@15
    aput-object v1, v0, v8

    #@17
    sput-object v0, Lcom/lge/ims/provider/uc/ConfigUC;->TABLES:[Ljava/lang/String;

    #@19
    .line 31
    new-array v0, v8, [[Ljava/lang/String;

    #@1b
    new-array v1, v8, [Ljava/lang/String;

    #@1d
    const-string v2, "INTEGER"

    #@1f
    aput-object v2, v1, v5

    #@21
    const-string v2, "id"

    #@23
    aput-object v2, v1, v6

    #@25
    const-string v2, "1"

    #@27
    aput-object v2, v1, v7

    #@29
    aput-object v1, v0, v5

    #@2b
    new-array v1, v8, [Ljava/lang/String;

    #@2d
    const-string v2, "INTEGER"

    #@2f
    aput-object v2, v1, v5

    #@31
    const-string v2, "property"

    #@33
    aput-object v2, v1, v6

    #@35
    const-string v2, "0"

    #@37
    aput-object v2, v1, v7

    #@39
    aput-object v1, v0, v6

    #@3b
    new-array v1, v8, [Ljava/lang/String;

    #@3d
    const-string v2, "TEXT"

    #@3f
    aput-object v2, v1, v5

    #@41
    const-string v2, "conf"

    #@43
    aput-object v2, v1, v6

    #@45
    const-string v2, "[Uniqueness]\nStream=1\nFramed=0\nBasic=0\nEvent=1\nCoreService=2\nQos=0\nReg=2\nWrite=1\nRead=1\nCap=0\nMprof=2\nConnection=0\nIMService=0\nPresenceService=0\nXDMService=0\n\n[IMSRegistry]\nIMSRegistry=lgims.com.kt.app.uc\n\n[Stream]\nmedia_types=audio video\n\n[Event]\npackage_names=conference\n\n[CoreService_0]\nservice_id=lgims.com.kt.service.uc\niari=\nicsi=urn:urn-7:3gpp-service.ims.icsi.mmtel\nfeature_tag=\n\n[CoreService_1]\nservice_id=lgims.com.kt.service.vt\niari=\nicsi=urn:urn-7:3gpp-service.ims.icsi.mmtel\nfeature_tag=\n\n[Reg_0]\nservice_id=lgims.com.kt.service.uc\nheader_count=1\nheader_0=Supported: path\n\n[Reg_1]\nservice_id=lgims.com.kt.service.vt\nheader_count=1\nheader_0=Supported: path\n\n[Write]\nheader_names=Accept Content-Disposition Content-ID Content-Type History-Info Privacy Reason Refer-Sub Require Supported P-Early-Media\n\n[Read]\nheader_names=Contact From History-Info Privacy Reason Server Subscription-State Supported P-Early-Media\n\n[Mprof_0]\nservice_id=lgims.com.kt.service.uc\nprofile=com.kt.media.vt\n\n[Mprof_1]\nservice_id=lgims.com.kt.service.vt\nprofile=com.kt.media.vt\n\n"

    #@47
    aput-object v2, v1, v7

    #@49
    aput-object v1, v0, v7

    #@4b
    sput-object v0, Lcom/lge/ims/provider/uc/ConfigUC;->APP:[[Ljava/lang/String;

    #@4d
    .line 103
    const/16 v0, 0x9

    #@4f
    new-array v0, v0, [[Ljava/lang/String;

    #@51
    new-array v1, v8, [Ljava/lang/String;

    #@53
    const-string v2, "INTEGER"

    #@55
    aput-object v2, v1, v5

    #@57
    const-string v2, "id"

    #@59
    aput-object v2, v1, v6

    #@5b
    const-string v2, "1"

    #@5d
    aput-object v2, v1, v7

    #@5f
    aput-object v1, v0, v5

    #@61
    new-array v1, v8, [Ljava/lang/String;

    #@63
    const-string v2, "INTEGER"

    #@65
    aput-object v2, v1, v5

    #@67
    const-string v2, "property"

    #@69
    aput-object v2, v1, v6

    #@6b
    const-string v2, "1"

    #@6d
    aput-object v2, v1, v7

    #@6f
    aput-object v1, v0, v6

    #@71
    new-array v1, v8, [Ljava/lang/String;

    #@73
    const-string v2, "INTEGER"

    #@75
    aput-object v2, v1, v5

    #@77
    const-string v2, "session_max_count"

    #@79
    aput-object v2, v1, v6

    #@7b
    const-string v2, "2"

    #@7d
    aput-object v2, v1, v7

    #@7f
    aput-object v1, v0, v7

    #@81
    new-array v1, v8, [Ljava/lang/String;

    #@83
    const-string v2, "INTEGER"

    #@85
    aput-object v2, v1, v5

    #@87
    const-string v2, "session_tv_mo_no_answer"

    #@89
    aput-object v2, v1, v6

    #@8b
    const-string v2, "180"

    #@8d
    aput-object v2, v1, v7

    #@8f
    aput-object v1, v0, v8

    #@91
    new-array v1, v8, [Ljava/lang/String;

    #@93
    const-string v2, "INTEGER"

    #@95
    aput-object v2, v1, v5

    #@97
    const-string v2, "session_tv_mo_1xx_wait"

    #@99
    aput-object v2, v1, v6

    #@9b
    const-string v2, "180"

    #@9d
    aput-object v2, v1, v7

    #@9f
    aput-object v1, v0, v9

    #@a1
    const/4 v1, 0x5

    #@a2
    new-array v2, v8, [Ljava/lang/String;

    #@a4
    const-string v3, "INTEGER"

    #@a6
    aput-object v3, v2, v5

    #@a8
    const-string v3, "session_tv_mt_alerting"

    #@aa
    aput-object v3, v2, v6

    #@ac
    const-string v3, "120"

    #@ae
    aput-object v3, v2, v7

    #@b0
    aput-object v2, v0, v1

    #@b2
    const/4 v1, 0x6

    #@b3
    new-array v2, v8, [Ljava/lang/String;

    #@b5
    const-string v3, "TEXT"

    #@b7
    aput-object v3, v2, v5

    #@b9
    const-string v3, "session_conf_factory_uri"

    #@bb
    aput-object v3, v2, v6

    #@bd
    const-string v3, "sip:conference-factory@ims.kt.com"

    #@bf
    aput-object v3, v2, v7

    #@c1
    aput-object v2, v0, v1

    #@c3
    const/4 v1, 0x7

    #@c4
    new-array v2, v8, [Ljava/lang/String;

    #@c6
    const-string v3, "TEXT"

    #@c8
    aput-object v3, v2, v5

    #@ca
    const-string v3, "session_rpr_supported"

    #@cc
    aput-object v3, v2, v6

    #@ce
    const-string v3, "false"

    #@d0
    aput-object v3, v2, v7

    #@d2
    aput-object v2, v0, v1

    #@d4
    const/16 v1, 0x8

    #@d6
    new-array v2, v8, [Ljava/lang/String;

    #@d8
    const-string v3, "TEXT"

    #@da
    aput-object v3, v2, v5

    #@dc
    const-string v3, "media_ref"

    #@de
    aput-object v3, v2, v6

    #@e0
    const-string v3, "lgims.com.media"

    #@e2
    aput-object v3, v2, v7

    #@e4
    aput-object v2, v0, v1

    #@e6
    sput-object v0, Lcom/lge/ims/provider/uc/ConfigUC;->SERVICE_UC:[[Ljava/lang/String;

    #@e8
    .line 120
    const/16 v0, 0x9

    #@ea
    new-array v0, v0, [[Ljava/lang/String;

    #@ec
    new-array v1, v8, [Ljava/lang/String;

    #@ee
    const-string v2, "INTEGER"

    #@f0
    aput-object v2, v1, v5

    #@f2
    const-string v2, "id"

    #@f4
    aput-object v2, v1, v6

    #@f6
    const-string v2, "1"

    #@f8
    aput-object v2, v1, v7

    #@fa
    aput-object v1, v0, v5

    #@fc
    new-array v1, v8, [Ljava/lang/String;

    #@fe
    const-string v2, "INTEGER"

    #@100
    aput-object v2, v1, v5

    #@102
    const-string v2, "property"

    #@104
    aput-object v2, v1, v6

    #@106
    const-string v2, "1"

    #@108
    aput-object v2, v1, v7

    #@10a
    aput-object v1, v0, v6

    #@10c
    new-array v1, v8, [Ljava/lang/String;

    #@10e
    const-string v2, "INTEGER"

    #@110
    aput-object v2, v1, v5

    #@112
    const-string v2, "session_max_count"

    #@114
    aput-object v2, v1, v6

    #@116
    const-string v2, "1"

    #@118
    aput-object v2, v1, v7

    #@11a
    aput-object v1, v0, v7

    #@11c
    new-array v1, v8, [Ljava/lang/String;

    #@11e
    const-string v2, "INTEGER"

    #@120
    aput-object v2, v1, v5

    #@122
    const-string v2, "session_tv_mo_no_answer"

    #@124
    aput-object v2, v1, v6

    #@126
    const-string v2, "180"

    #@128
    aput-object v2, v1, v7

    #@12a
    aput-object v1, v0, v8

    #@12c
    new-array v1, v8, [Ljava/lang/String;

    #@12e
    const-string v2, "INTEGER"

    #@130
    aput-object v2, v1, v5

    #@132
    const-string v2, "session_tv_mo_1xx_wait"

    #@134
    aput-object v2, v1, v6

    #@136
    const-string v2, "180"

    #@138
    aput-object v2, v1, v7

    #@13a
    aput-object v1, v0, v9

    #@13c
    const/4 v1, 0x5

    #@13d
    new-array v2, v8, [Ljava/lang/String;

    #@13f
    const-string v3, "INTEGER"

    #@141
    aput-object v3, v2, v5

    #@143
    const-string v3, "session_tv_mt_alerting"

    #@145
    aput-object v3, v2, v6

    #@147
    const-string v3, "120"

    #@149
    aput-object v3, v2, v7

    #@14b
    aput-object v2, v0, v1

    #@14d
    const/4 v1, 0x6

    #@14e
    new-array v2, v8, [Ljava/lang/String;

    #@150
    const-string v3, "TEXT"

    #@152
    aput-object v3, v2, v5

    #@154
    const-string v3, "session_conf_factory_uri"

    #@156
    aput-object v3, v2, v6

    #@158
    const-string v3, "sip:conference-factory@ims.kt.com"

    #@15a
    aput-object v3, v2, v7

    #@15c
    aput-object v2, v0, v1

    #@15e
    const/4 v1, 0x7

    #@15f
    new-array v2, v8, [Ljava/lang/String;

    #@161
    const-string v3, "TEXT"

    #@163
    aput-object v3, v2, v5

    #@165
    const-string v3, "session_rpr_supported"

    #@167
    aput-object v3, v2, v6

    #@169
    const-string v3, "false"

    #@16b
    aput-object v3, v2, v7

    #@16d
    aput-object v2, v0, v1

    #@16f
    const/16 v1, 0x8

    #@171
    new-array v2, v8, [Ljava/lang/String;

    #@173
    const-string v3, "TEXT"

    #@175
    aput-object v3, v2, v5

    #@177
    const-string v3, "media_ref"

    #@179
    aput-object v3, v2, v6

    #@17b
    const-string v3, "lgims.com.media"

    #@17d
    aput-object v3, v2, v7

    #@17f
    aput-object v2, v0, v1

    #@181
    sput-object v0, Lcom/lge/ims/provider/uc/ConfigUC;->SERVICE_VT:[[Ljava/lang/String;

    #@183
    .line 138
    const/16 v0, 0x1f

    #@185
    new-array v0, v0, [[Ljava/lang/String;

    #@187
    new-array v1, v8, [Ljava/lang/String;

    #@189
    const-string v2, "INTEGER"

    #@18b
    aput-object v2, v1, v5

    #@18d
    const-string v2, "id"

    #@18f
    aput-object v2, v1, v6

    #@191
    const-string v2, "1"

    #@193
    aput-object v2, v1, v7

    #@195
    aput-object v1, v0, v5

    #@197
    new-array v1, v8, [Ljava/lang/String;

    #@199
    const-string v2, "INTEGER"

    #@19b
    aput-object v2, v1, v5

    #@19d
    const-string v2, "property"

    #@19f
    aput-object v2, v1, v6

    #@1a1
    const-string v2, "1"

    #@1a3
    aput-object v2, v1, v7

    #@1a5
    aput-object v1, v0, v6

    #@1a7
    new-array v1, v8, [Ljava/lang/String;

    #@1a9
    const-string v2, "TEXT"

    #@1ab
    aput-object v2, v1, v5

    #@1ad
    const-string v2, "header_info_target_number_format"

    #@1af
    aput-object v2, v1, v6

    #@1b1
    const-string v2, "local"

    #@1b3
    aput-object v2, v1, v7

    #@1b5
    aput-object v1, v0, v7

    #@1b7
    new-array v1, v8, [Ljava/lang/String;

    #@1b9
    const-string v2, "TEXT"

    #@1bb
    aput-object v2, v1, v5

    #@1bd
    const-string v2, "header_info_target_scheme"

    #@1bf
    aput-object v2, v1, v6

    #@1c1
    const-string v2, "tel"

    #@1c3
    aput-object v2, v1, v7

    #@1c5
    aput-object v1, v0, v8

    #@1c7
    new-array v1, v8, [Ljava/lang/String;

    #@1c9
    const-string v2, "TEXT"

    #@1cb
    aput-object v2, v1, v5

    #@1cd
    const-string v2, "header_info_preferred_id"

    #@1cf
    aput-object v2, v1, v6

    #@1d1
    const-string v2, "sip"

    #@1d3
    aput-object v2, v1, v7

    #@1d5
    aput-object v1, v0, v9

    #@1d7
    const/4 v1, 0x5

    #@1d8
    new-array v2, v8, [Ljava/lang/String;

    #@1da
    const-string v3, "TEXT"

    #@1dc
    aput-object v3, v2, v5

    #@1de
    const-string v3, "header_info_allow_methods"

    #@1e0
    aput-object v3, v2, v6

    #@1e2
    const-string v3, "INVITE,BYE,CANCEL,ACK,PRACK,UPDATE,INFO,REFER,NOTIFY,MESSAGE,OPTIONS"

    #@1e4
    aput-object v3, v2, v7

    #@1e6
    aput-object v2, v0, v1

    #@1e8
    const/4 v1, 0x6

    #@1e9
    new-array v2, v8, [Ljava/lang/String;

    #@1eb
    const-string v3, "TEXT"

    #@1ed
    aput-object v3, v2, v5

    #@1ef
    const-string v3, "header_info_service_version"

    #@1f1
    aput-object v3, v2, v6

    #@1f3
    new-instance v3, Ljava/lang/StringBuilder;

    #@1f5
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1f8
    const-string v4, "KT-client/VoLTE+PSVT;"

    #@1fa
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1fd
    move-result-object v3

    #@1fe
    sget-object v4, Lcom/lge/ims/Configuration;->MODEL:Ljava/lang/String;

    #@200
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@203
    move-result-object v3

    #@204
    const-string v4, ";Device_Type=Android_Phone"

    #@206
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@209
    move-result-object v3

    #@20a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20d
    move-result-object v3

    #@20e
    aput-object v3, v2, v7

    #@210
    aput-object v2, v0, v1

    #@212
    const/4 v1, 0x7

    #@213
    new-array v2, v8, [Ljava/lang/String;

    #@215
    const-string v3, "TEXT"

    #@217
    aput-object v3, v2, v5

    #@219
    const-string v3, "header_info_feature_tags"

    #@21b
    aput-object v3, v2, v6

    #@21d
    const-string v3, "0x03000208"

    #@21f
    aput-object v3, v2, v7

    #@221
    aput-object v2, v0, v1

    #@223
    const/16 v1, 0x8

    #@225
    new-array v2, v8, [Ljava/lang/String;

    #@227
    const-string v3, "TEXT"

    #@229
    aput-object v3, v2, v5

    #@22b
    const-string v3, "timer_tv_on_runtime"

    #@22d
    aput-object v3, v2, v6

    #@22f
    const-string v3, "true"

    #@231
    aput-object v3, v2, v7

    #@233
    aput-object v2, v0, v1

    #@235
    const/16 v1, 0x9

    #@237
    new-array v2, v8, [Ljava/lang/String;

    #@239
    const-string v3, "INTEGER"

    #@23b
    aput-object v3, v2, v5

    #@23d
    const-string v3, "timer_tv_t1"

    #@23f
    aput-object v3, v2, v6

    #@241
    const-string v3, "1000"

    #@243
    aput-object v3, v2, v7

    #@245
    aput-object v2, v0, v1

    #@247
    const/16 v1, 0xa

    #@249
    new-array v2, v8, [Ljava/lang/String;

    #@24b
    const-string v3, "INTEGER"

    #@24d
    aput-object v3, v2, v5

    #@24f
    const-string v3, "timer_tv_t2"

    #@251
    aput-object v3, v2, v6

    #@253
    const-string v3, "2000"

    #@255
    aput-object v3, v2, v7

    #@257
    aput-object v2, v0, v1

    #@259
    const/16 v1, 0xb

    #@25b
    new-array v2, v8, [Ljava/lang/String;

    #@25d
    const-string v3, "INTEGER"

    #@25f
    aput-object v3, v2, v5

    #@261
    const-string v3, "timer_tv_tb"

    #@263
    aput-object v3, v2, v6

    #@265
    const-string v3, "4000"

    #@267
    aput-object v3, v2, v7

    #@269
    aput-object v2, v0, v1

    #@26b
    const/16 v1, 0xc

    #@26d
    new-array v2, v8, [Ljava/lang/String;

    #@26f
    const-string v3, "INTEGER"

    #@271
    aput-object v3, v2, v5

    #@273
    const-string v3, "timer_tv_td"

    #@275
    aput-object v3, v2, v6

    #@277
    const-string v3, "64000"

    #@279
    aput-object v3, v2, v7

    #@27b
    aput-object v2, v0, v1

    #@27d
    const/16 v1, 0xd

    #@27f
    new-array v2, v8, [Ljava/lang/String;

    #@281
    const-string v3, "INTEGER"

    #@283
    aput-object v3, v2, v5

    #@285
    const-string v3, "timer_tv_tf"

    #@287
    aput-object v3, v2, v6

    #@289
    const-string v3, "8000"

    #@28b
    aput-object v3, v2, v7

    #@28d
    aput-object v2, v0, v1

    #@28f
    const/16 v1, 0xe

    #@291
    new-array v2, v8, [Ljava/lang/String;

    #@293
    const-string v3, "INTEGER"

    #@295
    aput-object v3, v2, v5

    #@297
    const-string v3, "timer_tv_th"

    #@299
    aput-object v3, v2, v6

    #@29b
    const-string v3, "64000"

    #@29d
    aput-object v3, v2, v7

    #@29f
    aput-object v2, v0, v1

    #@2a1
    const/16 v1, 0xf

    #@2a3
    new-array v2, v8, [Ljava/lang/String;

    #@2a5
    const-string v3, "INTEGER"

    #@2a7
    aput-object v3, v2, v5

    #@2a9
    const-string v3, "timer_tv_ti"

    #@2ab
    aput-object v3, v2, v6

    #@2ad
    const-string v3, "5000"

    #@2af
    aput-object v3, v2, v7

    #@2b1
    aput-object v2, v0, v1

    #@2b3
    const/16 v1, 0x10

    #@2b5
    new-array v2, v8, [Ljava/lang/String;

    #@2b7
    const-string v3, "INTEGER"

    #@2b9
    aput-object v3, v2, v5

    #@2bb
    const-string v3, "timer_tv_tj"

    #@2bd
    aput-object v3, v2, v6

    #@2bf
    const-string v3, "64000"

    #@2c1
    aput-object v3, v2, v7

    #@2c3
    aput-object v2, v0, v1

    #@2c5
    const/16 v1, 0x11

    #@2c7
    new-array v2, v8, [Ljava/lang/String;

    #@2c9
    const-string v3, "INTEGER"

    #@2cb
    aput-object v3, v2, v5

    #@2cd
    const-string v3, "timer_tv_tk"

    #@2cf
    aput-object v3, v2, v6

    #@2d1
    const-string v3, "5000"

    #@2d3
    aput-object v3, v2, v7

    #@2d5
    aput-object v2, v0, v1

    #@2d7
    const/16 v1, 0x12

    #@2d9
    new-array v2, v8, [Ljava/lang/String;

    #@2db
    const-string v3, "TEXT"

    #@2dd
    aput-object v3, v2, v5

    #@2df
    const-string v3, "session_mode"

    #@2e1
    aput-object v3, v2, v6

    #@2e3
    const-string v3, "IETF"

    #@2e5
    aput-object v3, v2, v7

    #@2e7
    aput-object v2, v0, v1

    #@2e9
    const/16 v1, 0x13

    #@2eb
    new-array v2, v8, [Ljava/lang/String;

    #@2ed
    const-string v3, "TEXT"

    #@2ef
    aput-object v3, v2, v5

    #@2f1
    const-string v3, "session_st_supported"

    #@2f3
    aput-object v3, v2, v6

    #@2f5
    const-string v3, "true"

    #@2f7
    aput-object v3, v2, v7

    #@2f9
    aput-object v2, v0, v1

    #@2fb
    const/16 v1, 0x14

    #@2fd
    new-array v2, v8, [Ljava/lang/String;

    #@2ff
    const-string v3, "TEXT"

    #@301
    aput-object v3, v2, v5

    #@303
    const-string v3, "session_st_refresher"

    #@305
    aput-object v3, v2, v6

    #@307
    const-string v3, "local"

    #@309
    aput-object v3, v2, v7

    #@30b
    aput-object v2, v0, v1

    #@30d
    const/16 v1, 0x15

    #@30f
    new-array v2, v8, [Ljava/lang/String;

    #@311
    const-string v3, "TEXT"

    #@313
    aput-object v3, v2, v5

    #@315
    const-string v3, "session_st_method"

    #@317
    aput-object v3, v2, v6

    #@319
    const-string v3, "UPDATE"

    #@31b
    aput-object v3, v2, v7

    #@31d
    aput-object v2, v0, v1

    #@31f
    const/16 v1, 0x16

    #@321
    new-array v2, v8, [Ljava/lang/String;

    #@323
    const-string v3, "INTEGER"

    #@325
    aput-object v3, v2, v5

    #@327
    const-string v3, "session_st_minse"

    #@329
    aput-object v3, v2, v6

    #@32b
    const-string v3, "90"

    #@32d
    aput-object v3, v2, v7

    #@32f
    aput-object v2, v0, v1

    #@331
    const/16 v1, 0x17

    #@333
    new-array v2, v8, [Ljava/lang/String;

    #@335
    const-string v3, "INTEGER"

    #@337
    aput-object v3, v2, v5

    #@339
    const-string v3, "session_st_session_expires"

    #@33b
    aput-object v3, v2, v6

    #@33d
    const-string v3, "360"

    #@33f
    aput-object v3, v2, v7

    #@341
    aput-object v2, v0, v1

    #@343
    const/16 v1, 0x18

    #@345
    new-array v2, v8, [Ljava/lang/String;

    #@347
    const-string v3, "TEXT"

    #@349
    aput-object v3, v2, v5

    #@34b
    const-string v3, "session_st_headers"

    #@34d
    aput-object v3, v2, v6

    #@34f
    const-string v3, "0xF9"

    #@351
    aput-object v3, v2, v7

    #@353
    aput-object v2, v0, v1

    #@355
    const/16 v1, 0x19

    #@357
    new-array v2, v8, [Ljava/lang/String;

    #@359
    const-string v3, "TEXT"

    #@35b
    aput-object v3, v2, v5

    #@35d
    const-string v3, "session_st_no_refresh_by_reinvite"

    #@35f
    aput-object v3, v2, v6

    #@361
    const-string v3, "true"

    #@363
    aput-object v3, v2, v7

    #@365
    aput-object v2, v0, v1

    #@367
    const/16 v1, 0x1a

    #@369
    new-array v2, v8, [Ljava/lang/String;

    #@36b
    const-string v3, "TEXT"

    #@36d
    aput-object v3, v2, v5

    #@36f
    const-string v3, "session_sdp_version_check"

    #@371
    aput-object v3, v2, v6

    #@373
    const-string v3, "true"

    #@375
    aput-object v3, v2, v7

    #@377
    aput-object v2, v0, v1

    #@379
    const/16 v1, 0x1b

    #@37b
    new-array v2, v8, [Ljava/lang/String;

    #@37d
    const-string v3, "TEXT"

    #@37f
    aput-object v3, v2, v5

    #@381
    const-string v3, "session_sdp_non_rpr"

    #@383
    aput-object v3, v2, v6

    #@385
    const-string v3, "true"

    #@387
    aput-object v3, v2, v7

    #@389
    aput-object v2, v0, v1

    #@38b
    const/16 v1, 0x1c

    #@38d
    new-array v2, v8, [Ljava/lang/String;

    #@38f
    const-string v3, "TEXT"

    #@391
    aput-object v3, v2, v5

    #@393
    const-string v3, "capabilities_resp_by_app"

    #@395
    aput-object v3, v2, v6

    #@397
    const-string v3, "true"

    #@399
    aput-object v3, v2, v7

    #@39b
    aput-object v2, v0, v1

    #@39d
    const/16 v1, 0x1d

    #@39f
    new-array v2, v8, [Ljava/lang/String;

    #@3a1
    const-string v3, "TEXT"

    #@3a3
    aput-object v3, v2, v5

    #@3a5
    const-string v3, "pagemessage_resp_by_app"

    #@3a7
    aput-object v3, v2, v6

    #@3a9
    const-string v3, "true"

    #@3ab
    aput-object v3, v2, v7

    #@3ad
    aput-object v2, v0, v1

    #@3af
    const/16 v1, 0x1e

    #@3b1
    new-array v2, v8, [Ljava/lang/String;

    #@3b3
    const-string v3, "TEXT"

    #@3b5
    aput-object v3, v2, v5

    #@3b7
    const-string v3, "reference_resp_by_app"

    #@3b9
    aput-object v3, v2, v6

    #@3bb
    const-string v3, "false"

    #@3bd
    aput-object v3, v2, v7

    #@3bf
    aput-object v2, v0, v1

    #@3c1
    sput-object v0, Lcom/lge/ims/provider/uc/ConfigUC;->SIP_VT_A1:[[Ljava/lang/String;

    #@3c3
    .line 186
    const/16 v0, 0x1f

    #@3c5
    new-array v0, v0, [[Ljava/lang/String;

    #@3c7
    new-array v1, v8, [Ljava/lang/String;

    #@3c9
    const-string v2, "INTEGER"

    #@3cb
    aput-object v2, v1, v5

    #@3cd
    const-string v2, "id"

    #@3cf
    aput-object v2, v1, v6

    #@3d1
    const-string v2, "1"

    #@3d3
    aput-object v2, v1, v7

    #@3d5
    aput-object v1, v0, v5

    #@3d7
    new-array v1, v8, [Ljava/lang/String;

    #@3d9
    const-string v2, "INTEGER"

    #@3db
    aput-object v2, v1, v5

    #@3dd
    const-string v2, "property"

    #@3df
    aput-object v2, v1, v6

    #@3e1
    const-string v2, "1"

    #@3e3
    aput-object v2, v1, v7

    #@3e5
    aput-object v1, v0, v6

    #@3e7
    new-array v1, v8, [Ljava/lang/String;

    #@3e9
    const-string v2, "TEXT"

    #@3eb
    aput-object v2, v1, v5

    #@3ed
    const-string v2, "header_info_target_number_format"

    #@3ef
    aput-object v2, v1, v6

    #@3f1
    const-string v2, "local"

    #@3f3
    aput-object v2, v1, v7

    #@3f5
    aput-object v1, v0, v7

    #@3f7
    new-array v1, v8, [Ljava/lang/String;

    #@3f9
    const-string v2, "TEXT"

    #@3fb
    aput-object v2, v1, v5

    #@3fd
    const-string v2, "header_info_target_scheme"

    #@3ff
    aput-object v2, v1, v6

    #@401
    const-string v2, "tel"

    #@403
    aput-object v2, v1, v7

    #@405
    aput-object v1, v0, v8

    #@407
    new-array v1, v8, [Ljava/lang/String;

    #@409
    const-string v2, "TEXT"

    #@40b
    aput-object v2, v1, v5

    #@40d
    const-string v2, "header_info_preferred_id"

    #@40f
    aput-object v2, v1, v6

    #@411
    const-string v2, "sip"

    #@413
    aput-object v2, v1, v7

    #@415
    aput-object v1, v0, v9

    #@417
    const/4 v1, 0x5

    #@418
    new-array v2, v8, [Ljava/lang/String;

    #@41a
    const-string v3, "TEXT"

    #@41c
    aput-object v3, v2, v5

    #@41e
    const-string v3, "header_info_allow_methods"

    #@420
    aput-object v3, v2, v6

    #@422
    const-string v3, "INVITE,BYE,CANCEL,ACK,PRACK,UPDATE,INFO,REFER,NOTIFY,MESSAGE,OPTIONS"

    #@424
    aput-object v3, v2, v7

    #@426
    aput-object v2, v0, v1

    #@428
    const/4 v1, 0x6

    #@429
    new-array v2, v8, [Ljava/lang/String;

    #@42b
    const-string v3, "TEXT"

    #@42d
    aput-object v3, v2, v5

    #@42f
    const-string v3, "header_info_service_version"

    #@431
    aput-object v3, v2, v6

    #@433
    new-instance v3, Ljava/lang/StringBuilder;

    #@435
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@438
    const-string v4, "KT-client/VoLTE+PSVT;"

    #@43a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43d
    move-result-object v3

    #@43e
    sget-object v4, Lcom/lge/ims/Configuration;->MODEL:Ljava/lang/String;

    #@440
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@443
    move-result-object v3

    #@444
    const-string v4, ";Device_Type=Android_Phone"

    #@446
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@449
    move-result-object v3

    #@44a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44d
    move-result-object v3

    #@44e
    aput-object v3, v2, v7

    #@450
    aput-object v2, v0, v1

    #@452
    const/4 v1, 0x7

    #@453
    new-array v2, v8, [Ljava/lang/String;

    #@455
    const-string v3, "TEXT"

    #@457
    aput-object v3, v2, v5

    #@459
    const-string v3, "header_info_feature_tags"

    #@45b
    aput-object v3, v2, v6

    #@45d
    const-string v3, "0x03000208"

    #@45f
    aput-object v3, v2, v7

    #@461
    aput-object v2, v0, v1

    #@463
    const/16 v1, 0x8

    #@465
    new-array v2, v8, [Ljava/lang/String;

    #@467
    const-string v3, "TEXT"

    #@469
    aput-object v3, v2, v5

    #@46b
    const-string v3, "timer_tv_on_runtime"

    #@46d
    aput-object v3, v2, v6

    #@46f
    const-string v3, "true"

    #@471
    aput-object v3, v2, v7

    #@473
    aput-object v2, v0, v1

    #@475
    const/16 v1, 0x9

    #@477
    new-array v2, v8, [Ljava/lang/String;

    #@479
    const-string v3, "INTEGER"

    #@47b
    aput-object v3, v2, v5

    #@47d
    const-string v3, "timer_tv_t1"

    #@47f
    aput-object v3, v2, v6

    #@481
    const-string v3, "2000"

    #@483
    aput-object v3, v2, v7

    #@485
    aput-object v2, v0, v1

    #@487
    const/16 v1, 0xa

    #@489
    new-array v2, v8, [Ljava/lang/String;

    #@48b
    const-string v3, "INTEGER"

    #@48d
    aput-object v3, v2, v5

    #@48f
    const-string v3, "timer_tv_t2"

    #@491
    aput-object v3, v2, v6

    #@493
    const-string v3, "2000"

    #@495
    aput-object v3, v2, v7

    #@497
    aput-object v2, v0, v1

    #@499
    const/16 v1, 0xb

    #@49b
    new-array v2, v8, [Ljava/lang/String;

    #@49d
    const-string v3, "INTEGER"

    #@49f
    aput-object v3, v2, v5

    #@4a1
    const-string v3, "timer_tv_tb"

    #@4a3
    aput-object v3, v2, v6

    #@4a5
    const-string v3, "4000"

    #@4a7
    aput-object v3, v2, v7

    #@4a9
    aput-object v2, v0, v1

    #@4ab
    const/16 v1, 0xc

    #@4ad
    new-array v2, v8, [Ljava/lang/String;

    #@4af
    const-string v3, "INTEGER"

    #@4b1
    aput-object v3, v2, v5

    #@4b3
    const-string v3, "timer_tv_td"

    #@4b5
    aput-object v3, v2, v6

    #@4b7
    const-string v3, "64000"

    #@4b9
    aput-object v3, v2, v7

    #@4bb
    aput-object v2, v0, v1

    #@4bd
    const/16 v1, 0xd

    #@4bf
    new-array v2, v8, [Ljava/lang/String;

    #@4c1
    const-string v3, "INTEGER"

    #@4c3
    aput-object v3, v2, v5

    #@4c5
    const-string v3, "timer_tv_tf"

    #@4c7
    aput-object v3, v2, v6

    #@4c9
    const-string v3, "9000"

    #@4cb
    aput-object v3, v2, v7

    #@4cd
    aput-object v2, v0, v1

    #@4cf
    const/16 v1, 0xe

    #@4d1
    new-array v2, v8, [Ljava/lang/String;

    #@4d3
    const-string v3, "INTEGER"

    #@4d5
    aput-object v3, v2, v5

    #@4d7
    const-string v3, "timer_tv_th"

    #@4d9
    aput-object v3, v2, v6

    #@4db
    const-string v3, "128000"

    #@4dd
    aput-object v3, v2, v7

    #@4df
    aput-object v2, v0, v1

    #@4e1
    const/16 v1, 0xf

    #@4e3
    new-array v2, v8, [Ljava/lang/String;

    #@4e5
    const-string v3, "INTEGER"

    #@4e7
    aput-object v3, v2, v5

    #@4e9
    const-string v3, "timer_tv_ti"

    #@4eb
    aput-object v3, v2, v6

    #@4ed
    const-string v3, "5000"

    #@4ef
    aput-object v3, v2, v7

    #@4f1
    aput-object v2, v0, v1

    #@4f3
    const/16 v1, 0x10

    #@4f5
    new-array v2, v8, [Ljava/lang/String;

    #@4f7
    const-string v3, "INTEGER"

    #@4f9
    aput-object v3, v2, v5

    #@4fb
    const-string v3, "timer_tv_tj"

    #@4fd
    aput-object v3, v2, v6

    #@4ff
    const-string v3, "128000"

    #@501
    aput-object v3, v2, v7

    #@503
    aput-object v2, v0, v1

    #@505
    const/16 v1, 0x11

    #@507
    new-array v2, v8, [Ljava/lang/String;

    #@509
    const-string v3, "INTEGER"

    #@50b
    aput-object v3, v2, v5

    #@50d
    const-string v3, "timer_tv_tk"

    #@50f
    aput-object v3, v2, v6

    #@511
    const-string v3, "5000"

    #@513
    aput-object v3, v2, v7

    #@515
    aput-object v2, v0, v1

    #@517
    const/16 v1, 0x12

    #@519
    new-array v2, v8, [Ljava/lang/String;

    #@51b
    const-string v3, "TEXT"

    #@51d
    aput-object v3, v2, v5

    #@51f
    const-string v3, "session_mode"

    #@521
    aput-object v3, v2, v6

    #@523
    const-string v3, "IETF"

    #@525
    aput-object v3, v2, v7

    #@527
    aput-object v2, v0, v1

    #@529
    const/16 v1, 0x13

    #@52b
    new-array v2, v8, [Ljava/lang/String;

    #@52d
    const-string v3, "TEXT"

    #@52f
    aput-object v3, v2, v5

    #@531
    const-string v3, "session_st_supported"

    #@533
    aput-object v3, v2, v6

    #@535
    const-string v3, "true"

    #@537
    aput-object v3, v2, v7

    #@539
    aput-object v2, v0, v1

    #@53b
    const/16 v1, 0x14

    #@53d
    new-array v2, v8, [Ljava/lang/String;

    #@53f
    const-string v3, "TEXT"

    #@541
    aput-object v3, v2, v5

    #@543
    const-string v3, "session_st_refresher"

    #@545
    aput-object v3, v2, v6

    #@547
    const-string v3, "local"

    #@549
    aput-object v3, v2, v7

    #@54b
    aput-object v2, v0, v1

    #@54d
    const/16 v1, 0x15

    #@54f
    new-array v2, v8, [Ljava/lang/String;

    #@551
    const-string v3, "TEXT"

    #@553
    aput-object v3, v2, v5

    #@555
    const-string v3, "session_st_method"

    #@557
    aput-object v3, v2, v6

    #@559
    const-string v3, "UPDATE"

    #@55b
    aput-object v3, v2, v7

    #@55d
    aput-object v2, v0, v1

    #@55f
    const/16 v1, 0x16

    #@561
    new-array v2, v8, [Ljava/lang/String;

    #@563
    const-string v3, "INTEGER"

    #@565
    aput-object v3, v2, v5

    #@567
    const-string v3, "session_st_minse"

    #@569
    aput-object v3, v2, v6

    #@56b
    const-string v3, "90"

    #@56d
    aput-object v3, v2, v7

    #@56f
    aput-object v2, v0, v1

    #@571
    const/16 v1, 0x17

    #@573
    new-array v2, v8, [Ljava/lang/String;

    #@575
    const-string v3, "INTEGER"

    #@577
    aput-object v3, v2, v5

    #@579
    const-string v3, "session_st_session_expires"

    #@57b
    aput-object v3, v2, v6

    #@57d
    const-string v3, "360"

    #@57f
    aput-object v3, v2, v7

    #@581
    aput-object v2, v0, v1

    #@583
    const/16 v1, 0x18

    #@585
    new-array v2, v8, [Ljava/lang/String;

    #@587
    const-string v3, "TEXT"

    #@589
    aput-object v3, v2, v5

    #@58b
    const-string v3, "session_st_headers"

    #@58d
    aput-object v3, v2, v6

    #@58f
    const-string v3, "0xF9"

    #@591
    aput-object v3, v2, v7

    #@593
    aput-object v2, v0, v1

    #@595
    const/16 v1, 0x19

    #@597
    new-array v2, v8, [Ljava/lang/String;

    #@599
    const-string v3, "TEXT"

    #@59b
    aput-object v3, v2, v5

    #@59d
    const-string v3, "session_st_no_refresh_by_reinvite"

    #@59f
    aput-object v3, v2, v6

    #@5a1
    const-string v3, "true"

    #@5a3
    aput-object v3, v2, v7

    #@5a5
    aput-object v2, v0, v1

    #@5a7
    const/16 v1, 0x1a

    #@5a9
    new-array v2, v8, [Ljava/lang/String;

    #@5ab
    const-string v3, "TEXT"

    #@5ad
    aput-object v3, v2, v5

    #@5af
    const-string v3, "session_sdp_version_check"

    #@5b1
    aput-object v3, v2, v6

    #@5b3
    const-string v3, "true"

    #@5b5
    aput-object v3, v2, v7

    #@5b7
    aput-object v2, v0, v1

    #@5b9
    const/16 v1, 0x1b

    #@5bb
    new-array v2, v8, [Ljava/lang/String;

    #@5bd
    const-string v3, "TEXT"

    #@5bf
    aput-object v3, v2, v5

    #@5c1
    const-string v3, "session_sdp_non_rpr"

    #@5c3
    aput-object v3, v2, v6

    #@5c5
    const-string v3, "true"

    #@5c7
    aput-object v3, v2, v7

    #@5c9
    aput-object v2, v0, v1

    #@5cb
    const/16 v1, 0x1c

    #@5cd
    new-array v2, v8, [Ljava/lang/String;

    #@5cf
    const-string v3, "TEXT"

    #@5d1
    aput-object v3, v2, v5

    #@5d3
    const-string v3, "capabilities_resp_by_app"

    #@5d5
    aput-object v3, v2, v6

    #@5d7
    const-string v3, "true"

    #@5d9
    aput-object v3, v2, v7

    #@5db
    aput-object v2, v0, v1

    #@5dd
    const/16 v1, 0x1d

    #@5df
    new-array v2, v8, [Ljava/lang/String;

    #@5e1
    const-string v3, "TEXT"

    #@5e3
    aput-object v3, v2, v5

    #@5e5
    const-string v3, "pagemessage_resp_by_app"

    #@5e7
    aput-object v3, v2, v6

    #@5e9
    const-string v3, "true"

    #@5eb
    aput-object v3, v2, v7

    #@5ed
    aput-object v2, v0, v1

    #@5ef
    const/16 v1, 0x1e

    #@5f1
    new-array v2, v8, [Ljava/lang/String;

    #@5f3
    const-string v3, "TEXT"

    #@5f5
    aput-object v3, v2, v5

    #@5f7
    const-string v3, "reference_resp_by_app"

    #@5f9
    aput-object v3, v2, v6

    #@5fb
    const-string v3, "false"

    #@5fd
    aput-object v3, v2, v7

    #@5ff
    aput-object v2, v0, v1

    #@601
    sput-object v0, Lcom/lge/ims/provider/uc/ConfigUC;->SIP_VT:[[Ljava/lang/String;

    #@603
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 19
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public getTableContent(Ljava/lang/String;)[[Ljava/lang/String;
    .registers 4
    .parameter "name"

    #@0
    .prologue
    .line 242
    const-string v0, "lgims_com_kt_app_uc"

    #@2
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_b

    #@8
    .line 243
    sget-object v0, Lcom/lge/ims/provider/uc/ConfigUC;->APP:[[Ljava/lang/String;

    #@a
    .line 255
    :goto_a
    return-object v0

    #@b
    .line 244
    :cond_b
    const-string v0, "lgims_com_kt_service_uc"

    #@d
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@10
    move-result v0

    #@11
    if-eqz v0, :cond_16

    #@13
    .line 245
    sget-object v0, Lcom/lge/ims/provider/uc/ConfigUC;->SERVICE_UC:[[Ljava/lang/String;

    #@15
    goto :goto_a

    #@16
    .line 246
    :cond_16
    const-string v0, "lgims_com_kt_service_vt"

    #@18
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1b
    move-result v0

    #@1c
    if-eqz v0, :cond_21

    #@1e
    .line 247
    sget-object v0, Lcom/lge/ims/provider/uc/ConfigUC;->SERVICE_VT:[[Ljava/lang/String;

    #@20
    goto :goto_a

    #@21
    .line 248
    :cond_21
    const-string v0, "lgims_com_kt_sip_vt"

    #@23
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@26
    move-result v0

    #@27
    if-eqz v0, :cond_39

    #@29
    .line 249
    sget-object v0, Lcom/lge/ims/Configuration;->MODEL:Ljava/lang/String;

    #@2b
    const-string v1, "F320"

    #@2d
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@30
    move-result v0

    #@31
    if-eqz v0, :cond_36

    #@33
    .line 250
    sget-object v0, Lcom/lge/ims/provider/uc/ConfigUC;->SIP_VT_A1:[[Ljava/lang/String;

    #@35
    goto :goto_a

    #@36
    .line 252
    :cond_36
    sget-object v0, Lcom/lge/ims/provider/uc/ConfigUC;->SIP_VT:[[Ljava/lang/String;

    #@38
    goto :goto_a

    #@39
    .line 255
    :cond_39
    const/4 v0, 0x0

    #@3a
    check-cast v0, [[Ljava/lang/String;

    #@3c
    goto :goto_a
.end method

.method public getTables()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 234
    sget-object v0, Lcom/lge/ims/provider/uc/ConfigUC;->TABLES:[Ljava/lang/String;

    #@2
    return-object v0
.end method
