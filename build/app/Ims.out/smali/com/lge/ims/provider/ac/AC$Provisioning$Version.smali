.class public final Lcom/lge/ims/provider/ac/AC$Provisioning$Version;
.super Ljava/lang/Object;
.source "AC.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/provider/ac/AC$Provisioning;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Version"
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final ID:Ljava/lang/String; = "id"

.field public static final TABLE_NAME:Ljava/lang/String; = "version"

.field public static final VALIDITY:Ljava/lang/String; = "validity"

.field public static final VALIDITY_PERIOD:Ljava/lang/String; = "validity_period"

.field public static final VERSION:Ljava/lang/String; = "version"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 85
    const-string v0, "content://com.lge.ims.provider.ac_provisioning/version"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/lge/ims/provider/ac/AC$Provisioning$Version;->CONTENT_URI:Landroid/net/Uri;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 80
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
