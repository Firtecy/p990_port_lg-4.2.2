.class public Lcom/lge/ims/provider/ip/IPProvider;
.super Landroid/content/ContentProvider;
.source "IPProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/provider/ip/IPProvider$IPDatabaseHelper;
    }
.end annotation


# static fields
.field private static final IP_BUDDYIMAGE_TBL_ID:I = 0x9

.field private static final IP_CAPA_MSISDN:I = 0x2

.field private static final IP_CAPA_TBL:I = 0x1

.field private static final IP_MYSTATUS_SERVER_TBL:I = 0x6

.field private static final IP_MYSTATUS_TBL:I = 0x5

.field private static final IP_PRESENCE_MSISDN:I = 0x4

.field private static final IP_PRESENCE_TBL:I = 0x3

.field private static final IP_PRESENCE_TBL_ID:I = 0x8

.field private static final IP_PROVISIONING_TBL:I = 0x7

.field private static objBuddyImageMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static objIPCapaProjectionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static objIPMyStatusProjectionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static objIPPresenceProjectionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static objProvisioningMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final objUriMatcher:Landroid/content/UriMatcher;


# instance fields
.field private objIPDatabaseHelper:Lcom/lge/ims/provider/ip/IPProvider$IPDatabaseHelper;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    .line 49
    new-instance v0, Ljava/util/HashMap;

    #@2
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@5
    sput-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPCapaProjectionMap:Ljava/util/HashMap;

    #@7
    .line 50
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPCapaProjectionMap:Ljava/util/HashMap;

    #@9
    const-string v1, "_id"

    #@b
    const-string v2, "_id"

    #@d
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@10
    .line 51
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPCapaProjectionMap:Ljava/util/HashMap;

    #@12
    const-string v1, "data_id"

    #@14
    const-string v2, "data_id"

    #@16
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@19
    .line 52
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPCapaProjectionMap:Ljava/util/HashMap;

    #@1b
    const-string v1, "msisdn"

    #@1d
    const-string v2, "msisdn"

    #@1f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@22
    .line 53
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPCapaProjectionMap:Ljava/util/HashMap;

    #@24
    const-string v1, "nor_msisdn"

    #@26
    const-string v2, "nor_msisdn"

    #@28
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2b
    .line 54
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPCapaProjectionMap:Ljava/util/HashMap;

    #@2d
    const-string v1, "rcs_status"

    #@2f
    const-string v2, "rcs_status"

    #@31
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@34
    .line 55
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPCapaProjectionMap:Ljava/util/HashMap;

    #@36
    const-string v1, "im_status"

    #@38
    const-string v2, "im_status"

    #@3a
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3d
    .line 56
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPCapaProjectionMap:Ljava/util/HashMap;

    #@3f
    const-string v1, "ft_status"

    #@41
    const-string v2, "ft_status"

    #@43
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@46
    .line 57
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPCapaProjectionMap:Ljava/util/HashMap;

    #@48
    const-string v1, "time_stamp"

    #@4a
    const-string v2, "time_stamp"

    #@4c
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4f
    .line 58
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPCapaProjectionMap:Ljava/util/HashMap;

    #@51
    const-string v1, "capa_type"

    #@53
    const-string v2, "capa_type"

    #@55
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@58
    .line 59
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPCapaProjectionMap:Ljava/util/HashMap;

    #@5a
    const-string v1, "first_time_rcs"

    #@5c
    const-string v2, "first_time_rcs"

    #@5e
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@61
    .line 60
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPCapaProjectionMap:Ljava/util/HashMap;

    #@63
    const-string v1, "mim_status"

    #@65
    const-string v2, "mim_status"

    #@67
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@6a
    .line 61
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPCapaProjectionMap:Ljava/util/HashMap;

    #@6c
    const-string v1, "presence_status"

    #@6e
    const-string v2, "presence_status"

    #@70
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@73
    .line 62
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPCapaProjectionMap:Ljava/util/HashMap;

    #@75
    const-string v1, "uri_id"

    #@77
    const-string v2, "uri_id"

    #@79
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@7c
    .line 63
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPCapaProjectionMap:Ljava/util/HashMap;

    #@7e
    const-string v1, "http_status"

    #@80
    const-string v2, "http_status"

    #@82
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@85
    .line 64
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPCapaProjectionMap:Ljava/util/HashMap;

    #@87
    const-string v1, "response_code"

    #@89
    const-string v2, "response_code"

    #@8b
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@8e
    .line 65
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPCapaProjectionMap:Ljava/util/HashMap;

    #@90
    const-string v1, "rcs_user_list"

    #@92
    const-string v2, "rcs_user_list"

    #@94
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@97
    .line 66
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPCapaProjectionMap:Ljava/util/HashMap;

    #@99
    const-string v1, "nonrcs_user_list"

    #@9b
    const-string v2, "nonrcs_user_list"

    #@9d
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@a0
    .line 67
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPCapaProjectionMap:Ljava/util/HashMap;

    #@a2
    const-string v1, "image_uri_id"

    #@a4
    const-string v2, "image_uri_id"

    #@a6
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@a9
    .line 68
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPCapaProjectionMap:Ljava/util/HashMap;

    #@ab
    const-string v1, "raw_contact_id"

    #@ad
    const-string v2, "raw_contact_id"

    #@af
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@b2
    .line 69
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPCapaProjectionMap:Ljava/util/HashMap;

    #@b4
    const-string v1, "photo_update"

    #@b6
    const-string v2, "photo_update"

    #@b8
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@bb
    .line 70
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPCapaProjectionMap:Ljava/util/HashMap;

    #@bd
    const-string v1, "display_name"

    #@bf
    const-string v2, "display_name"

    #@c1
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@c4
    .line 75
    new-instance v0, Ljava/util/HashMap;

    #@c6
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@c9
    sput-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPPresenceProjectionMap:Ljava/util/HashMap;

    #@cb
    .line 76
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPPresenceProjectionMap:Ljava/util/HashMap;

    #@cd
    const-string v1, "_id"

    #@cf
    const-string v2, "_id"

    #@d1
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@d4
    .line 77
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPPresenceProjectionMap:Ljava/util/HashMap;

    #@d6
    const-string v1, "nor_msisdn"

    #@d8
    const-string v2, "nor_msisdn"

    #@da
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@dd
    .line 78
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPPresenceProjectionMap:Ljava/util/HashMap;

    #@df
    const-string v1, "homepage"

    #@e1
    const-string v2, "homepage"

    #@e3
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@e6
    .line 79
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPPresenceProjectionMap:Ljava/util/HashMap;

    #@e8
    const-string v1, "free_text"

    #@ea
    const-string v2, "free_text"

    #@ec
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@ef
    .line 80
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPPresenceProjectionMap:Ljava/util/HashMap;

    #@f1
    const-string v1, "e_mail"

    #@f3
    const-string v2, "e_mail"

    #@f5
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@f8
    .line 81
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPPresenceProjectionMap:Ljava/util/HashMap;

    #@fa
    const-string v1, "birthday"

    #@fc
    const-string v2, "birthday"

    #@fe
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@101
    .line 82
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPPresenceProjectionMap:Ljava/util/HashMap;

    #@103
    const-string v1, "status"

    #@105
    const-string v2, "status"

    #@107
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@10a
    .line 83
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPPresenceProjectionMap:Ljava/util/HashMap;

    #@10c
    const-string v1, "twitter_account"

    #@10e
    const-string v2, "twitter_account"

    #@110
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@113
    .line 84
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPPresenceProjectionMap:Ljava/util/HashMap;

    #@115
    const-string v1, "facebook_account"

    #@117
    const-string v2, "facebook_account"

    #@119
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@11c
    .line 85
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPPresenceProjectionMap:Ljava/util/HashMap;

    #@11e
    const-string v1, "cyworld_account"

    #@120
    const-string v2, "cyworld_account"

    #@122
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@125
    .line 86
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPPresenceProjectionMap:Ljava/util/HashMap;

    #@127
    const-string v1, "waggle_account"

    #@129
    const-string v2, "waggle_account"

    #@12b
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@12e
    .line 87
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPPresenceProjectionMap:Ljava/util/HashMap;

    #@130
    const-string v1, "status_icon"

    #@132
    const-string v2, "status_icon"

    #@134
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@137
    .line 88
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPPresenceProjectionMap:Ljava/util/HashMap;

    #@139
    const-string v1, "status_icon_link"

    #@13b
    const-string v2, "status_icon_link"

    #@13d
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@140
    .line 89
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPPresenceProjectionMap:Ljava/util/HashMap;

    #@142
    const-string v1, "status_icon_thumb"

    #@144
    const-string v2, "status_icon_thumb"

    #@146
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@149
    .line 90
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPPresenceProjectionMap:Ljava/util/HashMap;

    #@14b
    const-string v1, "status_icon_thumb_link"

    #@14d
    const-string v2, "status_icon_thumb_link"

    #@14f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@152
    .line 91
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPPresenceProjectionMap:Ljava/util/HashMap;

    #@154
    const-string v1, "status_icon_thumb_etag"

    #@156
    const-string v2, "status_icon_thumb_etag"

    #@158
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@15b
    .line 92
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPPresenceProjectionMap:Ljava/util/HashMap;

    #@15d
    const-string v1, "time_stamp"

    #@15f
    const-string v2, "time_stamp"

    #@161
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@164
    .line 93
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPPresenceProjectionMap:Ljava/util/HashMap;

    #@166
    const-string v1, "statusicon_update"

    #@168
    const-string v2, "statusicon_update"

    #@16a
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@16d
    .line 99
    new-instance v0, Ljava/util/HashMap;

    #@16f
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@172
    sput-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPMyStatusProjectionMap:Ljava/util/HashMap;

    #@174
    .line 100
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPMyStatusProjectionMap:Ljava/util/HashMap;

    #@176
    const-string v1, "homepage"

    #@178
    const-string v2, "homepage"

    #@17a
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@17d
    .line 101
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPMyStatusProjectionMap:Ljava/util/HashMap;

    #@17f
    const-string v1, "free_text"

    #@181
    const-string v2, "free_text"

    #@183
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@186
    .line 102
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPMyStatusProjectionMap:Ljava/util/HashMap;

    #@188
    const-string v1, "e_mail"

    #@18a
    const-string v2, "e_mail"

    #@18c
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@18f
    .line 103
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPMyStatusProjectionMap:Ljava/util/HashMap;

    #@191
    const-string v1, "birthday"

    #@193
    const-string v2, "birthday"

    #@195
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@198
    .line 104
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPMyStatusProjectionMap:Ljava/util/HashMap;

    #@19a
    const-string v1, "status"

    #@19c
    const-string v2, "status"

    #@19e
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1a1
    .line 105
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPMyStatusProjectionMap:Ljava/util/HashMap;

    #@1a3
    const-string v1, "twitter_account"

    #@1a5
    const-string v2, "twitter_account"

    #@1a7
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1aa
    .line 106
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPMyStatusProjectionMap:Ljava/util/HashMap;

    #@1ac
    const-string v1, "facebook_account"

    #@1ae
    const-string v2, "facebook_account"

    #@1b0
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1b3
    .line 107
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPMyStatusProjectionMap:Ljava/util/HashMap;

    #@1b5
    const-string v1, "cyworld_account"

    #@1b7
    const-string v2, "cyworld_account"

    #@1b9
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1bc
    .line 108
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPMyStatusProjectionMap:Ljava/util/HashMap;

    #@1be
    const-string v1, "waggle_account"

    #@1c0
    const-string v2, "waggle_account"

    #@1c2
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1c5
    .line 109
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPMyStatusProjectionMap:Ljava/util/HashMap;

    #@1c7
    const-string v1, "status_icon"

    #@1c9
    const-string v2, "status_icon"

    #@1cb
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1ce
    .line 110
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPMyStatusProjectionMap:Ljava/util/HashMap;

    #@1d0
    const-string v1, "status_icon_link"

    #@1d2
    const-string v2, "status_icon_link"

    #@1d4
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1d7
    .line 111
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPMyStatusProjectionMap:Ljava/util/HashMap;

    #@1d9
    const-string v1, "status_icon_thumb"

    #@1db
    const-string v2, "status_icon_thumb"

    #@1dd
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1e0
    .line 112
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPMyStatusProjectionMap:Ljava/util/HashMap;

    #@1e2
    const-string v1, "status_icon_thumb_link"

    #@1e4
    const-string v2, "status_icon_thumb_link"

    #@1e6
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1e9
    .line 113
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPMyStatusProjectionMap:Ljava/util/HashMap;

    #@1eb
    const-string v1, "status_icon_thumb_etag"

    #@1ed
    const-string v2, "status_icon_thumb_etag"

    #@1ef
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1f2
    .line 114
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objIPMyStatusProjectionMap:Ljava/util/HashMap;

    #@1f4
    const-string v1, "display_name"

    #@1f6
    const-string v2, "display_name"

    #@1f8
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1fb
    .line 118
    new-instance v0, Ljava/util/HashMap;

    #@1fd
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@200
    sput-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@202
    .line 119
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@204
    const-string v1, "latest_polling"

    #@206
    const-string v2, "latest_polling"

    #@208
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@20b
    .line 120
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@20d
    const-string v1, "latest_rlssub_polling"

    #@20f
    const-string v2, "latest_rlssub_polling"

    #@211
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@214
    .line 121
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@216
    const-string v1, "excess_max_user"

    #@218
    const-string v2, "excess_max_user"

    #@21a
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@21d
    .line 126
    new-instance v0, Ljava/util/HashMap;

    #@21f
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@222
    sput-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objBuddyImageMap:Ljava/util/HashMap;

    #@224
    .line 127
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objBuddyImageMap:Ljava/util/HashMap;

    #@226
    const-string v1, "_id"

    #@228
    const-string v2, "_id"

    #@22a
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@22d
    .line 128
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objBuddyImageMap:Ljava/util/HashMap;

    #@22f
    const-string v1, "nor_msisdn"

    #@231
    const-string v2, "nor_msisdn"

    #@233
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@236
    .line 129
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objBuddyImageMap:Ljava/util/HashMap;

    #@238
    const-string v1, "file_path"

    #@23a
    const-string v2, "file_path"

    #@23c
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@23f
    .line 130
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objBuddyImageMap:Ljava/util/HashMap;

    #@241
    const-string v1, "time_stamp"

    #@243
    const-string v2, "time_stamp"

    #@245
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@248
    .line 146
    new-instance v0, Landroid/content/UriMatcher;

    #@24a
    const/4 v1, -0x1

    #@24b
    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    #@24e
    sput-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objUriMatcher:Landroid/content/UriMatcher;

    #@250
    .line 147
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objUriMatcher:Landroid/content/UriMatcher;

    #@252
    const-string v1, "com.lge.ims.provider.ip"

    #@254
    const-string v2, "ip_capa"

    #@256
    const/4 v3, 0x1

    #@257
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@25a
    .line 148
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objUriMatcher:Landroid/content/UriMatcher;

    #@25c
    const-string v1, "com.lge.ims.provider.ip"

    #@25e
    const-string v2, "ip_capa/nor_msisdn/*"

    #@260
    const/4 v3, 0x2

    #@261
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@264
    .line 149
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objUriMatcher:Landroid/content/UriMatcher;

    #@266
    const-string v1, "com.lge.ims.provider.ip"

    #@268
    const-string v2, "ip_presence"

    #@26a
    const/4 v3, 0x3

    #@26b
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@26e
    .line 150
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objUriMatcher:Landroid/content/UriMatcher;

    #@270
    const-string v1, "com.lge.ims.provider.ip"

    #@272
    const-string v2, "ip_presence/nor_msisdn/*"

    #@274
    const/4 v3, 0x4

    #@275
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@278
    .line 151
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objUriMatcher:Landroid/content/UriMatcher;

    #@27a
    const-string v1, "com.lge.ims.provider.ip"

    #@27c
    const-string v2, "ip_mystatus"

    #@27e
    const/4 v3, 0x5

    #@27f
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@282
    .line 152
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objUriMatcher:Landroid/content/UriMatcher;

    #@284
    const-string v1, "com.lge.ims.provider.ip"

    #@286
    const-string v2, "ip_mystatus_service"

    #@288
    const/4 v3, 0x6

    #@289
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@28c
    .line 153
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objUriMatcher:Landroid/content/UriMatcher;

    #@28e
    const-string v1, "com.lge.ims.provider.ip"

    #@290
    const-string v2, "ip_provisioning"

    #@292
    const/4 v3, 0x7

    #@293
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@296
    .line 155
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objUriMatcher:Landroid/content/UriMatcher;

    #@298
    const-string v1, "com.lge.ims.provider.ip"

    #@29a
    const-string v2, "ip_presence/id/#"

    #@29c
    const/16 v3, 0x8

    #@29e
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@2a1
    .line 156
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objUriMatcher:Landroid/content/UriMatcher;

    #@2a3
    const-string v1, "com.lge.ims.provider.ip"

    #@2a5
    const-string v2, "ip_buddyimage"

    #@2a7
    const/16 v3, 0x9

    #@2a9
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@2ac
    .line 157
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 44
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    #@3
    .line 162
    return-void
.end method

.method private getCompressedBytes(Landroid/graphics/Bitmap;)[B
    .registers 6
    .parameter "b"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 749
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    #@2
    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    #@5
    .line 750
    .local v0, baos:Ljava/io/ByteArrayOutputStream;
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    #@7
    const/16 v3, 0x5f

    #@9
    invoke-virtual {p1, v2, v3, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    #@c
    move-result v1

    #@d
    .line 751
    .local v1, compressed:Z
    if-nez v1, :cond_17

    #@f
    .line 752
    new-instance v2, Ljava/io/IOException;

    #@11
    const-string v3, "Unable to compress image"

    #@13
    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@16
    throw v2

    #@17
    .line 754
    :cond_17
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->flush()V

    #@1a
    .line 755
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    #@1d
    .line 756
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@20
    move-result-object v2

    #@21
    return-object v2
.end method

.method private getScaledBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .registers 16
    .parameter "mOriginal"
    .parameter "maxDim"

    #@0
    .prologue
    .line 732
    move-object v8, p1

    #@1
    .line 733
    .local v8, scaledBitmap:Landroid/graphics/Bitmap;
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    #@4
    move-result v3

    #@5
    .line 734
    .local v3, width:I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    #@8
    move-result v4

    #@9
    .line 735
    .local v4, height:I
    const/4 v1, 0x0

    #@a
    .line 736
    .local v1, cropLeft:I
    const/4 v2, 0x0

    #@b
    .line 738
    .local v2, cropTop:I
    int-to-float v0, p2

    #@c
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    #@f
    move-result v6

    #@10
    int-to-float v6, v6

    #@11
    div-float v7, v0, v6

    #@13
    .line 739
    .local v7, scaleFactor:F
    float-to-double v9, v7

    #@14
    const-wide/high16 v11, 0x3ff0

    #@16
    cmpg-double v0, v9, v11

    #@18
    if-ltz v0, :cond_1e

    #@1a
    if-nez v1, :cond_1e

    #@1c
    if-eqz v2, :cond_2c

    #@1e
    .line 740
    :cond_1e
    new-instance v5, Landroid/graphics/Matrix;

    #@20
    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    #@23
    .line 741
    .local v5, matrix:Landroid/graphics/Matrix;
    invoke-virtual {v5, v7, v7}, Landroid/graphics/Matrix;->setScale(FF)V

    #@26
    .line 742
    const/4 v6, 0x0

    #@27
    move-object v0, p1

    #@28
    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    #@2b
    move-result-object v8

    #@2c
    .line 745
    .end local v5           #matrix:Landroid/graphics/Matrix;
    :cond_2c
    return-object v8
.end method

.method private processPhoto(Landroid/content/ContentValues;)Z
    .registers 12
    .parameter "objContentValue"

    #@0
    .prologue
    .line 692
    const-string v8, "status_icon_thumb"

    #@2
    invoke-virtual {p1, v8}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    #@5
    move-result v8

    #@6
    if-eqz v8, :cond_55

    #@8
    .line 693
    const-string v8, "status_icon_thumb"

    #@a
    invoke-virtual {p1, v8}, Landroid/content/ContentValues;->getAsByteArray(Ljava/lang/String;)[B

    #@d
    move-result-object v5

    #@e
    .line 695
    .local v5, mOriginal:[B
    if-eqz v5, :cond_55

    #@10
    .line 696
    const/4 v8, 0x0

    #@11
    array-length v9, v5

    #@12
    invoke-static {v5, v8, v9}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    #@15
    move-result-object v6

    #@16
    .line 697
    .local v6, mPhoto:Landroid/graphics/Bitmap;
    const/16 v8, 0x60

    #@18
    invoke-direct {p0, v6, v8}, Lcom/lge/ims/provider/ip/IPProvider;->getScaledBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    #@1b
    move-result-object v7

    #@1c
    .line 700
    .local v7, mThumbnailPhoto:Landroid/graphics/Bitmap;
    :try_start_1c
    const-string v8, "status_icon_thumb"

    #@1e
    invoke-direct {p0, v7}, Lcom/lge/ims/provider/ip/IPProvider;->getCompressedBytes(Landroid/graphics/Bitmap;)[B

    #@21
    move-result-object v9

    #@22
    invoke-virtual {p1, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V
    :try_end_25
    .catchall {:try_start_1c .. :try_end_25} :catchall_6f
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_25} :catch_57

    #@25
    .line 706
    :goto_25
    const/16 v8, 0x1f4

    #@27
    invoke-direct {p0, v6, v8}, Lcom/lge/ims/provider/ip/IPProvider;->getScaledBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    #@2a
    move-result-object v4

    #@2b
    .line 707
    .local v4, mFullPhoto:Landroid/graphics/Bitmap;
    new-instance v2, Ljava/io/File;

    #@2d
    const-string v8, "/data/data/com.lge.ims/image/myfull.jpg"

    #@2f
    invoke-direct {v2, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@32
    .line 708
    .local v2, copyFullFile:Ljava/io/File;
    const/4 v0, 0x0

    #@33
    .line 711
    .local v0, Fullout:Ljava/io/OutputStream;
    :try_start_33
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    #@36
    .line 712
    new-instance v1, Ljava/io/FileOutputStream;

    #@38
    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_3b
    .catchall {:try_start_33 .. :try_end_3b} :catchall_89
    .catch Ljava/io/IOException; {:try_start_33 .. :try_end_3b} :catch_71

    #@3b
    .line 713
    .end local v0           #Fullout:Ljava/io/OutputStream;
    .local v1, Fullout:Ljava/io/OutputStream;
    :try_start_3b
    sget-object v8, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    #@3d
    const/16 v9, 0x64

    #@3f
    invoke-virtual {v4, v8, v9, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    #@42
    move-result v8

    #@43
    if-eqz v8, :cond_91

    #@45
    .line 714
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_48
    .catchall {:try_start_3b .. :try_end_48} :catchall_8b
    .catch Ljava/io/IOException; {:try_start_3b .. :try_end_48} :catch_8e

    #@48
    .line 715
    const/4 v0, 0x0

    #@49
    .line 717
    .end local v1           #Fullout:Ljava/io/OutputStream;
    .restart local v0       #Fullout:Ljava/io/OutputStream;
    :goto_49
    :try_start_49
    const-string v8, "status_icon"

    #@4b
    const-string v9, "/data/data/com.lge.ims/image/myfull.jpg"

    #@4d
    invoke-virtual {p1, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@50
    .line 719
    if-eqz v0, :cond_55

    #@52
    .line 720
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_55
    .catchall {:try_start_49 .. :try_end_55} :catchall_89
    .catch Ljava/io/IOException; {:try_start_49 .. :try_end_55} :catch_71

    #@55
    .line 729
    .end local v0           #Fullout:Ljava/io/OutputStream;
    .end local v2           #copyFullFile:Ljava/io/File;
    .end local v4           #mFullPhoto:Landroid/graphics/Bitmap;
    .end local v5           #mOriginal:[B
    .end local v6           #mPhoto:Landroid/graphics/Bitmap;
    .end local v7           #mThumbnailPhoto:Landroid/graphics/Bitmap;
    :cond_55
    :goto_55
    const/4 v8, 0x1

    #@56
    return v8

    #@57
    .line 701
    .restart local v5       #mOriginal:[B
    .restart local v6       #mPhoto:Landroid/graphics/Bitmap;
    .restart local v7       #mThumbnailPhoto:Landroid/graphics/Bitmap;
    :catch_57
    move-exception v3

    #@58
    .line 702
    .local v3, e:Ljava/io/IOException;
    :try_start_58
    new-instance v8, Ljava/lang/StringBuilder;

    #@5a
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@5d
    const-string v9, "[ERROR]IPProvider::processPhoto : "

    #@5f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v8

    #@63
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v8

    #@67
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6a
    move-result-object v8

    #@6b
    invoke-static {v8}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_6e
    .catchall {:try_start_58 .. :try_end_6e} :catchall_6f

    #@6e
    goto :goto_25

    #@6f
    .line 703
    .end local v3           #e:Ljava/io/IOException;
    :catchall_6f
    move-exception v8

    #@70
    throw v8

    #@71
    .line 722
    .restart local v0       #Fullout:Ljava/io/OutputStream;
    .restart local v2       #copyFullFile:Ljava/io/File;
    .restart local v4       #mFullPhoto:Landroid/graphics/Bitmap;
    :catch_71
    move-exception v3

    #@72
    .line 723
    .restart local v3       #e:Ljava/io/IOException;
    :goto_72
    :try_start_72
    new-instance v8, Ljava/lang/StringBuilder;

    #@74
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@77
    const-string v9, "[ERROR]IPProvider::processPhoto : "

    #@79
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v8

    #@7d
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v8

    #@81
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@84
    move-result-object v8

    #@85
    invoke-static {v8}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_88
    .catchall {:try_start_72 .. :try_end_88} :catchall_89

    #@88
    goto :goto_55

    #@89
    .line 724
    .end local v3           #e:Ljava/io/IOException;
    :catchall_89
    move-exception v8

    #@8a
    :goto_8a
    throw v8

    #@8b
    .end local v0           #Fullout:Ljava/io/OutputStream;
    .restart local v1       #Fullout:Ljava/io/OutputStream;
    :catchall_8b
    move-exception v8

    #@8c
    move-object v0, v1

    #@8d
    .end local v1           #Fullout:Ljava/io/OutputStream;
    .restart local v0       #Fullout:Ljava/io/OutputStream;
    goto :goto_8a

    #@8e
    .line 722
    .end local v0           #Fullout:Ljava/io/OutputStream;
    .restart local v1       #Fullout:Ljava/io/OutputStream;
    :catch_8e
    move-exception v3

    #@8f
    move-object v0, v1

    #@90
    .end local v1           #Fullout:Ljava/io/OutputStream;
    .restart local v0       #Fullout:Ljava/io/OutputStream;
    goto :goto_72

    #@91
    .end local v0           #Fullout:Ljava/io/OutputStream;
    .restart local v1       #Fullout:Ljava/io/OutputStream;
    :cond_91
    move-object v0, v1

    #@92
    .end local v1           #Fullout:Ljava/io/OutputStream;
    .restart local v0       #Fullout:Ljava/io/OutputStream;
    goto :goto_49
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 12
    .parameter "objUri"
    .parameter "strWhere"
    .parameter "astrWhereArgs"

    #@0
    .prologue
    .line 625
    iget-object v5, p0, Lcom/lge/ims/provider/ip/IPProvider;->objIPDatabaseHelper:Lcom/lge/ims/provider/ip/IPProvider$IPDatabaseHelper;

    #@2
    invoke-virtual {v5}, Lcom/lge/ims/provider/ip/IPProvider$IPDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@5
    move-result-object v2

    #@6
    .line 626
    .local v2, objSQLiteDatabase:Landroid/database/sqlite/SQLiteDatabase;
    const/4 v1, -0x1

    #@7
    .line 629
    .local v1, nCount:I
    :try_start_7
    sget-object v5, Lcom/lge/ims/provider/ip/IPProvider;->objUriMatcher:Landroid/content/UriMatcher;

    #@9
    invoke-virtual {v5, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@c
    move-result v5

    #@d
    packed-switch v5, :pswitch_data_110

    #@10
    .line 679
    :pswitch_10
    new-instance v5, Ljava/lang/IllegalArgumentException;

    #@12
    const-string v6, "Uri is invaild"

    #@14
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v5
    :try_end_18
    .catchall {:try_start_7 .. :try_end_18} :catchall_8a
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_18} :catch_18

    #@18
    .line 682
    :catch_18
    move-exception v0

    #@19
    .line 683
    .local v0, e:Landroid/database/sqlite/SQLiteException;
    :try_start_19
    invoke-static {v0}, Landroid/database/sqlite/SqliteWrapper;->isLowMemory(Landroid/database/sqlite/SQLiteException;)Z

    #@1c
    move-result v5

    #@1d
    if-eqz v5, :cond_24

    #@1f
    .line 684
    const-string v5, "[ERROR]IPProvider::delete - intenal memory storage is Full"

    #@21
    invoke-static {v5}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_24
    .catchall {:try_start_19 .. :try_end_24} :catchall_8a

    #@24
    .line 689
    .end local v0           #e:Landroid/database/sqlite/SQLiteException;
    :cond_24
    :goto_24
    return v1

    #@25
    .line 632
    :pswitch_25
    :try_start_25
    const-string v5, "ip_capa"

    #@27
    invoke-virtual {v2, v5, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    #@2a
    move-result v1

    #@2b
    .line 634
    goto :goto_24

    #@2c
    .line 637
    :pswitch_2c
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@2f
    move-result-object v5

    #@30
    const/4 v6, 0x1

    #@31
    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@34
    move-result-object v4

    #@35
    check-cast v4, Ljava/lang/String;

    #@37
    .line 638
    .local v4, strMDN:Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v6, "nor_msisdn="

    #@3e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v5

    #@42
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v6

    #@46
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@49
    move-result v5

    #@4a
    if-nez v5, :cond_74

    #@4c
    new-instance v5, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v7, " AND ("

    #@53
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v5

    #@57
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v5

    #@5b
    const-string v7, ")"

    #@5d
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v5

    #@61
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v5

    #@65
    :goto_65
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v5

    #@69
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v3

    #@6d
    .line 639
    .local v3, strDeletedWhere:Ljava/lang/String;
    const-string v5, "ip_capa"

    #@6f
    invoke-virtual {v2, v5, v3, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    #@72
    move-result v1

    #@73
    .line 641
    goto :goto_24

    #@74
    .line 638
    .end local v3           #strDeletedWhere:Ljava/lang/String;
    :cond_74
    const-string v5, ""

    #@76
    goto :goto_65

    #@77
    .line 644
    .end local v4           #strMDN:Ljava/lang/String;
    :pswitch_77
    const-string v5, "ip_presence"

    #@79
    invoke-virtual {v2, v5, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    #@7c
    move-result v1

    #@7d
    .line 645
    invoke-virtual {p0}, Lcom/lge/ims/provider/ip/IPProvider;->getContext()Landroid/content/Context;

    #@80
    move-result-object v5

    #@81
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@84
    move-result-object v5

    #@85
    const/4 v6, 0x0

    #@86
    invoke-virtual {v5, p1, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_89
    .catchall {:try_start_25 .. :try_end_89} :catchall_8a
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_25 .. :try_end_89} :catch_18

    #@89
    goto :goto_24

    #@8a
    .line 686
    :catchall_8a
    move-exception v5

    #@8b
    throw v5

    #@8c
    .line 650
    :pswitch_8c
    :try_start_8c
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@8f
    move-result-object v5

    #@90
    const/4 v6, 0x1

    #@91
    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@94
    move-result-object v4

    #@95
    check-cast v4, Ljava/lang/String;

    #@97
    .line 651
    .restart local v4       #strMDN:Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    #@99
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@9c
    const-string v6, "nor_msisdn="

    #@9e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v5

    #@a2
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v6

    #@a6
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@a9
    move-result v5

    #@aa
    if-nez v5, :cond_d5

    #@ac
    new-instance v5, Ljava/lang/StringBuilder;

    #@ae
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@b1
    const-string v7, " AND ("

    #@b3
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v5

    #@b7
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v5

    #@bb
    const-string v7, ")"

    #@bd
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v5

    #@c1
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c4
    move-result-object v5

    #@c5
    :goto_c5
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v5

    #@c9
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cc
    move-result-object v3

    #@cd
    .line 652
    .restart local v3       #strDeletedWhere:Ljava/lang/String;
    const-string v5, "ip_presence"

    #@cf
    invoke-virtual {v2, v5, v3, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    #@d2
    move-result v1

    #@d3
    .line 654
    goto/16 :goto_24

    #@d5
    .line 651
    .end local v3           #strDeletedWhere:Ljava/lang/String;
    :cond_d5
    const-string v5, ""

    #@d7
    goto :goto_c5

    #@d8
    .line 657
    .end local v4           #strMDN:Ljava/lang/String;
    :pswitch_d8
    const-string v5, "ip_mystatus"

    #@da
    invoke-virtual {v2, v5, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    #@dd
    move-result v1

    #@de
    .line 658
    invoke-virtual {p0}, Lcom/lge/ims/provider/ip/IPProvider;->getContext()Landroid/content/Context;

    #@e1
    move-result-object v5

    #@e2
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@e5
    move-result-object v5

    #@e6
    const/4 v6, 0x0

    #@e7
    invoke-virtual {v5, p1, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    #@ea
    goto/16 :goto_24

    #@ec
    .line 663
    :pswitch_ec
    const-string v5, "ip_mystatus"

    #@ee
    invoke-virtual {v2, v5, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    #@f1
    move-result v1

    #@f2
    .line 664
    invoke-virtual {p0}, Lcom/lge/ims/provider/ip/IPProvider;->getContext()Landroid/content/Context;

    #@f5
    move-result-object v5

    #@f6
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@f9
    move-result-object v5

    #@fa
    const/4 v6, 0x0

    #@fb
    invoke-virtual {v5, p1, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    #@fe
    goto/16 :goto_24

    #@100
    .line 669
    :pswitch_100
    const-string v5, "ip_provisioning"

    #@102
    invoke-virtual {v2, v5, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    #@105
    move-result v1

    #@106
    .line 671
    goto/16 :goto_24

    #@108
    .line 674
    :pswitch_108
    const-string v5, "ip_buddyimage"

    #@10a
    invoke-virtual {v2, v5, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_10d
    .catchall {:try_start_8c .. :try_end_10d} :catchall_8a
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_8c .. :try_end_10d} :catch_18

    #@10d
    move-result v1

    #@10e
    .line 676
    goto/16 :goto_24

    #@110
    .line 629
    :pswitch_data_110
    .packed-switch 0x1
        :pswitch_25
        :pswitch_2c
        :pswitch_77
        :pswitch_8c
        :pswitch_d8
        :pswitch_ec
        :pswitch_100
        :pswitch_10
        :pswitch_108
    .end packed-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .registers 5
    .parameter "objUri"

    #@0
    .prologue
    .line 356
    sget-object v0, Lcom/lge/ims/provider/ip/IPProvider;->objUriMatcher:Landroid/content/UriMatcher;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@5
    move-result v0

    #@6
    packed-switch v0, :pswitch_data_34

    #@9
    .line 383
    :pswitch_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@b
    new-instance v1, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v2, "Uri is invalid"

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@21
    throw v0

    #@22
    .line 359
    :pswitch_22
    const-string v0, "vnd.android.cursor.dir/com.lge.ims.provider.ip.ip_capa"

    #@24
    .line 379
    :goto_24
    return-object v0

    #@25
    .line 363
    :pswitch_25
    const-string v0, "vnd.android.cursor.item/com.lge.ims.ip.presence"

    #@27
    goto :goto_24

    #@28
    .line 367
    :pswitch_28
    const-string v0, "vnd.android.cursor.item/com.lge.ims.ip.mystatus"

    #@2a
    goto :goto_24

    #@2b
    .line 371
    :pswitch_2b
    const-string v0, "vnd.android.cursor.item/com.lge.ims.ip.mystatus"

    #@2d
    goto :goto_24

    #@2e
    .line 375
    :pswitch_2e
    const-string v0, "vnd.android.cursor.dir/com.lge.ims.provider.ip.ip_provisioning"

    #@30
    goto :goto_24

    #@31
    .line 379
    :pswitch_31
    const-string v0, "vnd.android.cursor.item/com.lge.ims.ip.buddyimage"

    #@33
    goto :goto_24

    #@34
    .line 356
    :pswitch_data_34
    .packed-switch 0x1
        :pswitch_22
        :pswitch_9
        :pswitch_25
        :pswitch_9
        :pswitch_28
        :pswitch_2b
        :pswitch_2e
        :pswitch_9
        :pswitch_31
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .registers 13
    .parameter "objUri"
    .parameter "objContentValue"

    #@0
    .prologue
    const-wide/16 v8, 0x0

    #@2
    .line 475
    sget-object v6, Lcom/lge/ims/provider/ip/IPProvider;->objUriMatcher:Landroid/content/UriMatcher;

    #@4
    invoke-virtual {v6, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@7
    move-result v6

    #@8
    const/4 v7, 0x1

    #@9
    if-eq v6, v7, :cond_41

    #@b
    sget-object v6, Lcom/lge/ims/provider/ip/IPProvider;->objUriMatcher:Landroid/content/UriMatcher;

    #@d
    invoke-virtual {v6, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@10
    move-result v6

    #@11
    const/4 v7, 0x3

    #@12
    if-eq v6, v7, :cond_41

    #@14
    sget-object v6, Lcom/lge/ims/provider/ip/IPProvider;->objUriMatcher:Landroid/content/UriMatcher;

    #@16
    invoke-virtual {v6, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@19
    move-result v6

    #@1a
    const/4 v7, 0x6

    #@1b
    if-eq v6, v7, :cond_41

    #@1d
    sget-object v6, Lcom/lge/ims/provider/ip/IPProvider;->objUriMatcher:Landroid/content/UriMatcher;

    #@1f
    invoke-virtual {v6, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@22
    move-result v6

    #@23
    const/4 v7, 0x5

    #@24
    if-eq v6, v7, :cond_41

    #@26
    sget-object v6, Lcom/lge/ims/provider/ip/IPProvider;->objUriMatcher:Landroid/content/UriMatcher;

    #@28
    invoke-virtual {v6, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@2b
    move-result v6

    #@2c
    const/4 v7, 0x7

    #@2d
    if-eq v6, v7, :cond_41

    #@2f
    sget-object v6, Lcom/lge/ims/provider/ip/IPProvider;->objUriMatcher:Landroid/content/UriMatcher;

    #@31
    invoke-virtual {v6, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@34
    move-result v6

    #@35
    const/16 v7, 0x9

    #@37
    if-eq v6, v7, :cond_41

    #@39
    .line 476
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@3b
    const-string v7, "Uri is invaild"

    #@3d
    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@40
    throw v6

    #@41
    .line 479
    :cond_41
    iget-object v6, p0, Lcom/lge/ims/provider/ip/IPProvider;->objIPDatabaseHelper:Lcom/lge/ims/provider/ip/IPProvider$IPDatabaseHelper;

    #@43
    invoke-virtual {v6}, Lcom/lge/ims/provider/ip/IPProvider$IPDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@46
    move-result-object v5

    #@47
    .line 480
    .local v5, objSQLiteDatabase:Landroid/database/sqlite/SQLiteDatabase;
    const-wide/16 v2, 0x0

    #@49
    .line 481
    .local v2, nRowId:J
    sget-object v6, Lcom/lge/ims/provider/ip/IPProvider;->objUriMatcher:Landroid/content/UriMatcher;

    #@4b
    invoke-virtual {v6, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@4e
    move-result v1

    #@4f
    .line 482
    .local v1, match:I
    const/4 v4, 0x0

    #@50
    .line 485
    .local v4, objInsertedRowUri:Landroid/net/Uri;
    packed-switch v1, :pswitch_data_114

    #@53
    .line 527
    :pswitch_53
    :try_start_53
    new-instance v6, Ljava/lang/UnsupportedOperationException;

    #@55
    new-instance v7, Ljava/lang/StringBuilder;

    #@57
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@5a
    const-string v8, "Cannot insert into URL: "

    #@5c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v7

    #@60
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v7

    #@64
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@67
    move-result-object v7

    #@68
    invoke-direct {v6, v7}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@6b
    throw v6
    :try_end_6c
    .catchall {:try_start_53 .. :try_end_6c} :catchall_ab
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_53 .. :try_end_6c} :catch_6c

    #@6c
    .line 529
    :catch_6c
    move-exception v0

    #@6d
    .line 530
    .local v0, e:Landroid/database/sqlite/SQLiteException;
    :try_start_6d
    invoke-static {v0}, Landroid/database/sqlite/SqliteWrapper;->isLowMemory(Landroid/database/sqlite/SQLiteException;)Z

    #@70
    move-result v6

    #@71
    if-eqz v6, :cond_78

    #@73
    .line 531
    const-string v6, "[ERROR]IPProvider::insert - intenal memory storage is Full"

    #@75
    invoke-static {v6}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_78
    .catchall {:try_start_6d .. :try_end_78} :catchall_ab

    #@78
    .line 536
    .end local v0           #e:Landroid/database/sqlite/SQLiteException;
    :cond_78
    :goto_78
    return-object v4

    #@79
    .line 487
    :pswitch_79
    :try_start_79
    const-string v6, "ip_capa"

    #@7b
    const-string v7, "msisdn"

    #@7d
    invoke-virtual {v5, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    #@80
    move-result-wide v2

    #@81
    .line 488
    cmp-long v6, v2, v8

    #@83
    if-lez v6, :cond_78

    #@85
    .line 489
    sget-object v6, Lcom/lge/ims/provider/ip/IPProviderMetaData$IPCapa;->CONTENT_URI:Landroid/net/Uri;

    #@87
    invoke-static {v6, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    #@8a
    move-result-object v4

    #@8b
    goto :goto_78

    #@8c
    .line 493
    :pswitch_8c
    const-string v6, "ip_presence"

    #@8e
    const-string v7, "nor_msisdn"

    #@90
    invoke-virtual {v5, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    #@93
    move-result-wide v2

    #@94
    .line 494
    cmp-long v6, v2, v8

    #@96
    if-lez v6, :cond_78

    #@98
    .line 495
    sget-object v6, Lcom/lge/ims/provider/ip/IPProviderMetaData$IPPresence;->CONTENT_URI:Landroid/net/Uri;

    #@9a
    invoke-static {v6, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    #@9d
    move-result-object v4

    #@9e
    .line 496
    invoke-virtual {p0}, Lcom/lge/ims/provider/ip/IPProvider;->getContext()Landroid/content/Context;

    #@a1
    move-result-object v6

    #@a2
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@a5
    move-result-object v6

    #@a6
    const/4 v7, 0x0

    #@a7
    invoke-virtual {v6, v4, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_aa
    .catchall {:try_start_79 .. :try_end_aa} :catchall_ab
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_79 .. :try_end_aa} :catch_6c

    #@aa
    goto :goto_78

    #@ab
    .line 533
    :catchall_ab
    move-exception v6

    #@ac
    throw v6

    #@ad
    .line 500
    :pswitch_ad
    :try_start_ad
    invoke-direct {p0, p2}, Lcom/lge/ims/provider/ip/IPProvider;->processPhoto(Landroid/content/ContentValues;)Z

    #@b0
    .line 501
    const-string v6, "ip_mystatus"

    #@b2
    const/4 v7, 0x0

    #@b3
    invoke-virtual {v5, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    #@b6
    move-result-wide v2

    #@b7
    .line 502
    cmp-long v6, v2, v8

    #@b9
    if-lez v6, :cond_78

    #@bb
    .line 503
    sget-object v6, Lcom/lge/ims/provider/ip/IPProviderMetaData$IPMyStatus;->CONTENT_URI:Landroid/net/Uri;

    #@bd
    invoke-static {v6, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    #@c0
    move-result-object v4

    #@c1
    .line 504
    invoke-virtual {p0}, Lcom/lge/ims/provider/ip/IPProvider;->getContext()Landroid/content/Context;

    #@c4
    move-result-object v6

    #@c5
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@c8
    move-result-object v6

    #@c9
    const/4 v7, 0x0

    #@ca
    invoke-virtual {v6, v4, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    #@cd
    goto :goto_78

    #@ce
    .line 508
    :pswitch_ce
    const-string v6, "ip_mystatus"

    #@d0
    const/4 v7, 0x0

    #@d1
    invoke-virtual {v5, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    #@d4
    move-result-wide v2

    #@d5
    .line 509
    cmp-long v6, v2, v8

    #@d7
    if-lez v6, :cond_78

    #@d9
    .line 510
    sget-object v6, Lcom/lge/ims/provider/ip/IPProviderMetaData$IPMyStatus;->CONTENT_URI:Landroid/net/Uri;

    #@db
    invoke-static {v6, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    #@de
    move-result-object v4

    #@df
    .line 511
    invoke-virtual {p0}, Lcom/lge/ims/provider/ip/IPProvider;->getContext()Landroid/content/Context;

    #@e2
    move-result-object v6

    #@e3
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@e6
    move-result-object v6

    #@e7
    const/4 v7, 0x0

    #@e8
    invoke-virtual {v6, v4, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    #@eb
    goto :goto_78

    #@ec
    .line 515
    :pswitch_ec
    const-string v6, "ip_provisioning"

    #@ee
    const/4 v7, 0x0

    #@ef
    invoke-virtual {v5, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    #@f2
    move-result-wide v2

    #@f3
    .line 516
    cmp-long v6, v2, v8

    #@f5
    if-lez v6, :cond_78

    #@f7
    .line 517
    sget-object v6, Lcom/lge/ims/provider/ip/IPProviderMetaData$IPProvisioning;->CONTENT_URI:Landroid/net/Uri;

    #@f9
    invoke-static {v6, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    #@fc
    move-result-object v4

    #@fd
    goto/16 :goto_78

    #@ff
    .line 521
    :pswitch_ff
    const-string v6, "ip_buddyimage"

    #@101
    const-string v7, "nor_msisdn"

    #@103
    invoke-virtual {v5, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    #@106
    move-result-wide v2

    #@107
    .line 522
    cmp-long v6, v2, v8

    #@109
    if-lez v6, :cond_78

    #@10b
    .line 523
    sget-object v6, Lcom/lge/ims/provider/ip/IPProviderMetaData$IPBuddyImage;->CONTENT_URI:Landroid/net/Uri;

    #@10d
    invoke-static {v6, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;
    :try_end_110
    .catchall {:try_start_ad .. :try_end_110} :catchall_ab
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_ad .. :try_end_110} :catch_6c

    #@110
    move-result-object v4

    #@111
    goto/16 :goto_78

    #@113
    .line 485
    nop

    #@114
    :pswitch_data_114
    .packed-switch 0x1
        :pswitch_79
        :pswitch_53
        :pswitch_8c
        :pswitch_53
        :pswitch_ad
        :pswitch_ce
        :pswitch_ec
        :pswitch_53
        :pswitch_ff
    .end packed-switch
.end method

.method public onCreate()Z
    .registers 3

    #@0
    .prologue
    .line 350
    new-instance v0, Lcom/lge/ims/provider/ip/IPProvider$IPDatabaseHelper;

    #@2
    invoke-virtual {p0}, Lcom/lge/ims/provider/ip/IPProvider;->getContext()Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1}, Lcom/lge/ims/provider/ip/IPProvider$IPDatabaseHelper;-><init>(Landroid/content/Context;)V

    #@9
    iput-object v0, p0, Lcom/lge/ims/provider/ip/IPProvider;->objIPDatabaseHelper:Lcom/lge/ims/provider/ip/IPProvider$IPDatabaseHelper;

    #@b
    .line 351
    const/4 v0, 0x1

    #@c
    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 15
    .parameter "objUri"
    .parameter "astrProjection"
    .parameter "strSelection"
    .parameter "astrSelectionArgs"
    .parameter "objSortOrder"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 390
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    #@4
    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    #@7
    .line 392
    .local v0, objSQLiteQueryBuilder:Landroid/database/sqlite/SQLiteQueryBuilder;
    sget-object v2, Lcom/lge/ims/provider/ip/IPProvider;->objUriMatcher:Landroid/content/UriMatcher;

    #@9
    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@c
    move-result v2

    #@d
    packed-switch v2, :pswitch_data_112

    #@10
    .line 448
    :pswitch_10
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@12
    const-string v3, "Uri is invaild"

    #@14
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v2

    #@18
    .line 395
    :pswitch_18
    const-string v2, "ip_capa"

    #@1a
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    #@1d
    .line 396
    sget-object v2, Lcom/lge/ims/provider/ip/IPProvider;->objIPCapaProjectionMap:Ljava/util/HashMap;

    #@1f
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    #@22
    .line 452
    :goto_22
    const/4 v7, 0x0

    #@23
    .line 453
    .local v7, strOrderBy:Ljava/lang/String;
    sget-object v2, Lcom/lge/ims/provider/ip/IPProvider;->objUriMatcher:Landroid/content/UriMatcher;

    #@25
    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@28
    move-result v2

    #@29
    const/4 v3, 0x7

    #@2a
    if-eq v2, v3, :cond_3d

    #@2c
    sget-object v2, Lcom/lge/ims/provider/ip/IPProvider;->objUriMatcher:Landroid/content/UriMatcher;

    #@2e
    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@31
    move-result v2

    #@32
    const/4 v3, 0x5

    #@33
    if-eq v2, v3, :cond_3d

    #@35
    .line 454
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@38
    move-result v2

    #@39
    if-eqz v2, :cond_101

    #@3b
    .line 455
    const-string v7, "_id DESC"

    #@3d
    .line 461
    :cond_3d
    :goto_3d
    iget-object v2, p0, Lcom/lge/ims/provider/ip/IPProvider;->objIPDatabaseHelper:Lcom/lge/ims/provider/ip/IPProvider$IPDatabaseHelper;

    #@3f
    invoke-virtual {v2}, Lcom/lge/ims/provider/ip/IPProvider$IPDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@42
    move-result-object v1

    #@43
    .local v1, objSQLiteDatabase:Landroid/database/sqlite/SQLiteDatabase;
    move-object v2, p2

    #@44
    move-object v3, p3

    #@45
    move-object v4, p4

    #@46
    move-object v6, v5

    #@47
    .line 462
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@4a
    move-result-object v8

    #@4b
    .line 464
    .local v8, objCursor:Landroid/database/Cursor;
    if-nez v8, :cond_104

    #@4d
    .line 470
    :goto_4d
    return-object v5

    #@4e
    .line 401
    .end local v1           #objSQLiteDatabase:Landroid/database/sqlite/SQLiteDatabase;
    .end local v7           #strOrderBy:Ljava/lang/String;
    .end local v8           #objCursor:Landroid/database/Cursor;
    :pswitch_4e
    const-string v2, "ip_capa"

    #@50
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    #@53
    .line 402
    sget-object v2, Lcom/lge/ims/provider/ip/IPProvider;->objIPCapaProjectionMap:Ljava/util/HashMap;

    #@55
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    #@58
    .line 403
    new-instance v2, Ljava/lang/StringBuilder;

    #@5a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5d
    const-string v3, "nor_msisdn="

    #@5f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v3

    #@63
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@66
    move-result-object v2

    #@67
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@6a
    move-result-object v2

    #@6b
    check-cast v2, Ljava/lang/String;

    #@6d
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v2

    #@71
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v2

    #@75
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    #@78
    goto :goto_22

    #@79
    .line 408
    :pswitch_79
    const-string v2, "ip_presence"

    #@7b
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    #@7e
    .line 409
    sget-object v2, Lcom/lge/ims/provider/ip/IPProvider;->objIPPresenceProjectionMap:Ljava/util/HashMap;

    #@80
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    #@83
    goto :goto_22

    #@84
    .line 414
    :pswitch_84
    const-string v2, "ip_presence"

    #@86
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    #@89
    .line 415
    sget-object v2, Lcom/lge/ims/provider/ip/IPProvider;->objIPPresenceProjectionMap:Ljava/util/HashMap;

    #@8b
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    #@8e
    .line 416
    new-instance v2, Ljava/lang/StringBuilder;

    #@90
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@93
    const-string v3, "nor_msisdn="

    #@95
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v3

    #@99
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@9c
    move-result-object v2

    #@9d
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@a0
    move-result-object v2

    #@a1
    check-cast v2, Ljava/lang/String;

    #@a3
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v2

    #@a7
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@aa
    move-result-object v2

    #@ab
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    #@ae
    goto/16 :goto_22

    #@b0
    .line 421
    :pswitch_b0
    const-string v2, "ip_mystatus"

    #@b2
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    #@b5
    .line 422
    sget-object v2, Lcom/lge/ims/provider/ip/IPProvider;->objIPMyStatusProjectionMap:Ljava/util/HashMap;

    #@b7
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    #@ba
    goto/16 :goto_22

    #@bc
    .line 427
    :pswitch_bc
    const-string v2, "ip_provisioning"

    #@be
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    #@c1
    .line 428
    sget-object v2, Lcom/lge/ims/provider/ip/IPProvider;->objProvisioningMap:Ljava/util/HashMap;

    #@c3
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    #@c6
    goto/16 :goto_22

    #@c8
    .line 434
    :pswitch_c8
    const-string v2, "ip_presence"

    #@ca
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    #@cd
    .line 435
    sget-object v2, Lcom/lge/ims/provider/ip/IPProvider;->objIPPresenceProjectionMap:Ljava/util/HashMap;

    #@cf
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    #@d2
    .line 436
    new-instance v2, Ljava/lang/StringBuilder;

    #@d4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d7
    const-string v3, "_id="

    #@d9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v3

    #@dd
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@e0
    move-result-object v2

    #@e1
    const/4 v4, 0x2

    #@e2
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@e5
    move-result-object v2

    #@e6
    check-cast v2, Ljava/lang/String;

    #@e8
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@eb
    move-result-object v2

    #@ec
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ef
    move-result-object v2

    #@f0
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    #@f3
    goto/16 :goto_22

    #@f5
    .line 442
    :pswitch_f5
    const-string v2, "ip_buddyimage"

    #@f7
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    #@fa
    .line 443
    sget-object v2, Lcom/lge/ims/provider/ip/IPProvider;->objBuddyImageMap:Ljava/util/HashMap;

    #@fc
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    #@ff
    goto/16 :goto_22

    #@101
    .line 457
    .restart local v7       #strOrderBy:Ljava/lang/String;
    :cond_101
    move-object v7, p5

    #@102
    goto/16 :goto_3d

    #@104
    .line 468
    .restart local v1       #objSQLiteDatabase:Landroid/database/sqlite/SQLiteDatabase;
    .restart local v8       #objCursor:Landroid/database/Cursor;
    :cond_104
    invoke-virtual {p0}, Lcom/lge/ims/provider/ip/IPProvider;->getContext()Landroid/content/Context;

    #@107
    move-result-object v2

    #@108
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@10b
    move-result-object v2

    #@10c
    invoke-interface {v8, v2, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    #@10f
    move-object v5, v8

    #@110
    .line 470
    goto/16 :goto_4d

    #@112
    .line 392
    :pswitch_data_112
    .packed-switch 0x1
        :pswitch_18
        :pswitch_4e
        :pswitch_79
        :pswitch_84
        :pswitch_b0
        :pswitch_10
        :pswitch_bc
        :pswitch_c8
        :pswitch_f5
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 13
    .parameter "objUri"
    .parameter "objContentValues"
    .parameter "strWhere"
    .parameter "astrWhereArgs"

    #@0
    .prologue
    .line 541
    iget-object v5, p0, Lcom/lge/ims/provider/ip/IPProvider;->objIPDatabaseHelper:Lcom/lge/ims/provider/ip/IPProvider$IPDatabaseHelper;

    #@2
    invoke-virtual {v5}, Lcom/lge/ims/provider/ip/IPProvider$IPDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@5
    move-result-object v2

    #@6
    .line 542
    .local v2, objSQLiteDatabase:Landroid/database/sqlite/SQLiteDatabase;
    const/4 v1, -0x1

    #@7
    .line 545
    .local v1, nCount:I
    :try_start_7
    sget-object v5, Lcom/lge/ims/provider/ip/IPProvider;->objUriMatcher:Landroid/content/UriMatcher;

    #@9
    invoke-virtual {v5, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@c
    move-result v5

    #@d
    packed-switch v5, :pswitch_data_114

    #@10
    .line 610
    :pswitch_10
    new-instance v5, Ljava/lang/IllegalArgumentException;

    #@12
    const-string v6, "Uri is invaild"

    #@14
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v5
    :try_end_18
    .catchall {:try_start_7 .. :try_end_18} :catchall_8a
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_18} :catch_18

    #@18
    .line 613
    :catch_18
    move-exception v0

    #@19
    .line 614
    .local v0, e:Landroid/database/sqlite/SQLiteException;
    :try_start_19
    invoke-static {v0}, Landroid/database/sqlite/SqliteWrapper;->isLowMemory(Landroid/database/sqlite/SQLiteException;)Z

    #@1c
    move-result v5

    #@1d
    if-eqz v5, :cond_24

    #@1f
    .line 615
    const-string v5, "[ERROR]IPProvider::update - intenal memory storage is Full"

    #@21
    invoke-static {v5}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_24
    .catchall {:try_start_19 .. :try_end_24} :catchall_8a

    #@24
    .line 620
    .end local v0           #e:Landroid/database/sqlite/SQLiteException;
    :cond_24
    :goto_24
    return v1

    #@25
    .line 548
    :pswitch_25
    :try_start_25
    const-string v5, "ip_capa"

    #@27
    invoke-virtual {v2, v5, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@2a
    move-result v1

    #@2b
    .line 550
    goto :goto_24

    #@2c
    .line 553
    :pswitch_2c
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@2f
    move-result-object v5

    #@30
    const/4 v6, 0x1

    #@31
    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@34
    move-result-object v3

    #@35
    check-cast v3, Ljava/lang/String;

    #@37
    .line 554
    .local v3, strMDN:Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v6, "nor_msisdn="

    #@3e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v5

    #@42
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v6

    #@46
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@49
    move-result v5

    #@4a
    if-nez v5, :cond_74

    #@4c
    new-instance v5, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v7, " AND ("

    #@53
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v5

    #@57
    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v5

    #@5b
    const-string v7, ")"

    #@5d
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v5

    #@61
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v5

    #@65
    :goto_65
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v5

    #@69
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v4

    #@6d
    .line 555
    .local v4, strUpdatedWhere:Ljava/lang/String;
    const-string v5, "ip_capa"

    #@6f
    invoke-virtual {v2, v5, p2, v4, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@72
    move-result v1

    #@73
    .line 557
    goto :goto_24

    #@74
    .line 554
    .end local v4           #strUpdatedWhere:Ljava/lang/String;
    :cond_74
    const-string v5, ""

    #@76
    goto :goto_65

    #@77
    .line 560
    .end local v3           #strMDN:Ljava/lang/String;
    :pswitch_77
    const-string v5, "ip_presence"

    #@79
    invoke-virtual {v2, v5, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@7c
    move-result v1

    #@7d
    .line 561
    invoke-virtual {p0}, Lcom/lge/ims/provider/ip/IPProvider;->getContext()Landroid/content/Context;

    #@80
    move-result-object v5

    #@81
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@84
    move-result-object v5

    #@85
    const/4 v6, 0x0

    #@86
    invoke-virtual {v5, p1, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_89
    .catchall {:try_start_25 .. :try_end_89} :catchall_8a
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_25 .. :try_end_89} :catch_18

    #@89
    goto :goto_24

    #@8a
    .line 617
    :catchall_8a
    move-exception v5

    #@8b
    throw v5

    #@8c
    .line 566
    :pswitch_8c
    :try_start_8c
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@8f
    move-result-object v5

    #@90
    const/4 v6, 0x1

    #@91
    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@94
    move-result-object v3

    #@95
    check-cast v3, Ljava/lang/String;

    #@97
    .line 567
    .restart local v3       #strMDN:Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    #@99
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@9c
    const-string v6, "nor_msisdn="

    #@9e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v5

    #@a2
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v6

    #@a6
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@a9
    move-result v5

    #@aa
    if-nez v5, :cond_d5

    #@ac
    new-instance v5, Ljava/lang/StringBuilder;

    #@ae
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@b1
    const-string v7, " AND ("

    #@b3
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v5

    #@b7
    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v5

    #@bb
    const-string v7, ")"

    #@bd
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v5

    #@c1
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c4
    move-result-object v5

    #@c5
    :goto_c5
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v5

    #@c9
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cc
    move-result-object v4

    #@cd
    .line 568
    .restart local v4       #strUpdatedWhere:Ljava/lang/String;
    const-string v5, "ip_presence"

    #@cf
    invoke-virtual {v2, v5, p2, v4, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@d2
    move-result v1

    #@d3
    .line 570
    goto/16 :goto_24

    #@d5
    .line 567
    .end local v4           #strUpdatedWhere:Ljava/lang/String;
    :cond_d5
    const-string v5, ""

    #@d7
    goto :goto_c5

    #@d8
    .line 587
    .end local v3           #strMDN:Ljava/lang/String;
    :pswitch_d8
    invoke-direct {p0, p2}, Lcom/lge/ims/provider/ip/IPProvider;->processPhoto(Landroid/content/ContentValues;)Z

    #@db
    .line 588
    const-string v5, "ip_mystatus"

    #@dd
    invoke-virtual {v2, v5, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@e0
    move-result v1

    #@e1
    .line 589
    invoke-virtual {p0}, Lcom/lge/ims/provider/ip/IPProvider;->getContext()Landroid/content/Context;

    #@e4
    move-result-object v5

    #@e5
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@e8
    move-result-object v5

    #@e9
    const/4 v6, 0x0

    #@ea
    invoke-virtual {v5, p1, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    #@ed
    goto/16 :goto_24

    #@ef
    .line 594
    :pswitch_ef
    const-string v5, "ip_mystatus"

    #@f1
    invoke-virtual {v2, v5, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@f4
    move-result v1

    #@f5
    .line 595
    invoke-virtual {p0}, Lcom/lge/ims/provider/ip/IPProvider;->getContext()Landroid/content/Context;

    #@f8
    move-result-object v5

    #@f9
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@fc
    move-result-object v5

    #@fd
    const/4 v6, 0x0

    #@fe
    invoke-virtual {v5, p1, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    #@101
    goto/16 :goto_24

    #@103
    .line 600
    :pswitch_103
    const-string v5, "ip_provisioning"

    #@105
    invoke-virtual {v2, v5, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@108
    move-result v1

    #@109
    .line 602
    goto/16 :goto_24

    #@10b
    .line 605
    :pswitch_10b
    const-string v5, "ip_buddyimage"

    #@10d
    invoke-virtual {v2, v5, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_110
    .catchall {:try_start_8c .. :try_end_110} :catchall_8a
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_8c .. :try_end_110} :catch_18

    #@110
    move-result v1

    #@111
    .line 607
    goto/16 :goto_24

    #@113
    .line 545
    nop

    #@114
    :pswitch_data_114
    .packed-switch 0x1
        :pswitch_25
        :pswitch_2c
        :pswitch_77
        :pswitch_8c
        :pswitch_d8
        :pswitch_ef
        :pswitch_103
        :pswitch_10
        :pswitch_10b
    .end packed-switch
.end method
