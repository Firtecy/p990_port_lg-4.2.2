.class public Lcom/lge/ims/provider/uc/ConfigMedia;
.super Ljava/lang/Object;
.source "ConfigMedia.java"

# interfaces
.implements Lcom/lge/ims/provider/IConfiguration;


# static fields
.field private static final MEDIA:[[Ljava/lang/String;

.field private static final MEDIA_AUDIO:[[Ljava/lang/String;

.field private static final MEDIA_AUDIO_CODEC_VOLTE:[[Ljava/lang/String;

.field private static final MEDIA_AUDIO_CODEC_VT:[[Ljava/lang/String;

.field private static final MEDIA_VIDEO:[[Ljava/lang/String;

.field private static final MEDIA_VIDEO_CODEC_VT:[[Ljava/lang/String;

.field private static final TABLES:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 9

    #@0
    .prologue
    const/4 v8, 0x4

    #@1
    const/4 v7, 0x3

    #@2
    const/4 v6, 0x2

    #@3
    const/4 v5, 0x1

    #@4
    const/4 v4, 0x0

    #@5
    .line 20
    const/4 v0, 0x6

    #@6
    new-array v0, v0, [Ljava/lang/String;

    #@8
    const-string v1, "lgims_com_media"

    #@a
    aput-object v1, v0, v4

    #@c
    const-string v1, "lgims_com_media_audio"

    #@e
    aput-object v1, v0, v5

    #@10
    const-string v1, "lgims_com_media_video"

    #@12
    aput-object v1, v0, v6

    #@14
    const-string v1, "lgims_com_media_audio_codec_volte"

    #@16
    aput-object v1, v0, v7

    #@18
    const-string v1, "lgims_com_media_audio_codec_vt"

    #@1a
    aput-object v1, v0, v8

    #@1c
    const/4 v1, 0x5

    #@1d
    const-string v2, "lgims_com_media_video_codec_vt"

    #@1f
    aput-object v2, v0, v1

    #@21
    sput-object v0, Lcom/lge/ims/provider/uc/ConfigMedia;->TABLES:[Ljava/lang/String;

    #@23
    .line 32
    const/4 v0, 0x7

    #@24
    new-array v0, v0, [[Ljava/lang/String;

    #@26
    new-array v1, v7, [Ljava/lang/String;

    #@28
    const-string v2, "INTEGER"

    #@2a
    aput-object v2, v1, v4

    #@2c
    const-string v2, "id"

    #@2e
    aput-object v2, v1, v5

    #@30
    const-string v2, "1"

    #@32
    aput-object v2, v1, v6

    #@34
    aput-object v1, v0, v4

    #@36
    new-array v1, v7, [Ljava/lang/String;

    #@38
    const-string v2, "INTEGER"

    #@3a
    aput-object v2, v1, v4

    #@3c
    const-string v2, "property"

    #@3e
    aput-object v2, v1, v5

    #@40
    const-string v2, "1"

    #@42
    aput-object v2, v1, v6

    #@44
    aput-object v1, v0, v5

    #@46
    new-array v1, v7, [Ljava/lang/String;

    #@48
    const-string v2, "TEXT"

    #@4a
    aput-object v2, v1, v4

    #@4c
    const-string v2, "media_loopback"

    #@4e
    aput-object v2, v1, v5

    #@50
    const-string v2, "false"

    #@52
    aput-object v2, v1, v6

    #@54
    aput-object v1, v0, v6

    #@56
    new-array v1, v7, [Ljava/lang/String;

    #@58
    const-string v2, "INTEGER"

    #@5a
    aput-object v2, v1, v4

    #@5c
    const-string v2, "media_audio_count"

    #@5e
    aput-object v2, v1, v5

    #@60
    const-string v2, "2"

    #@62
    aput-object v2, v1, v6

    #@64
    aput-object v1, v0, v7

    #@66
    new-array v1, v7, [Ljava/lang/String;

    #@68
    const-string v2, "TEXT"

    #@6a
    aput-object v2, v1, v4

    #@6c
    const-string v2, "media_audio_ref"

    #@6e
    aput-object v2, v1, v5

    #@70
    const-string v2, "lgims.com.media.audio"

    #@72
    aput-object v2, v1, v6

    #@74
    aput-object v1, v0, v8

    #@76
    const/4 v1, 0x5

    #@77
    new-array v2, v7, [Ljava/lang/String;

    #@79
    const-string v3, "INTEGER"

    #@7b
    aput-object v3, v2, v4

    #@7d
    const-string v3, "media_video_count"

    #@7f
    aput-object v3, v2, v5

    #@81
    const-string v3, "1"

    #@83
    aput-object v3, v2, v6

    #@85
    aput-object v2, v0, v1

    #@87
    const/4 v1, 0x6

    #@88
    new-array v2, v7, [Ljava/lang/String;

    #@8a
    const-string v3, "TEXT"

    #@8c
    aput-object v3, v2, v4

    #@8e
    const-string v3, "media_video_ref"

    #@90
    aput-object v3, v2, v5

    #@92
    const-string v3, "lgims.com.media.video"

    #@94
    aput-object v3, v2, v6

    #@96
    aput-object v2, v0, v1

    #@98
    sput-object v0, Lcom/lge/ims/provider/uc/ConfigMedia;->MEDIA:[[Ljava/lang/String;

    #@9a
    .line 48
    const/16 v0, 0x32

    #@9c
    new-array v0, v0, [[Ljava/lang/String;

    #@9e
    new-array v1, v7, [Ljava/lang/String;

    #@a0
    const-string v2, "INTEGER"

    #@a2
    aput-object v2, v1, v4

    #@a4
    const-string v2, "id"

    #@a6
    aput-object v2, v1, v5

    #@a8
    const-string v2, "1"

    #@aa
    aput-object v2, v1, v6

    #@ac
    aput-object v1, v0, v4

    #@ae
    new-array v1, v7, [Ljava/lang/String;

    #@b0
    const-string v2, "INTEGER"

    #@b2
    aput-object v2, v1, v4

    #@b4
    const-string v2, "property"

    #@b6
    aput-object v2, v1, v5

    #@b8
    const-string v2, "1"

    #@ba
    aput-object v2, v1, v6

    #@bc
    aput-object v1, v0, v5

    #@be
    new-array v1, v7, [Ljava/lang/String;

    #@c0
    const-string v2, "TEXT"

    #@c2
    aput-object v2, v1, v4

    #@c4
    const-string v2, "audio_0_session_type"

    #@c6
    aput-object v2, v1, v5

    #@c8
    const-string v2, "volte"

    #@ca
    aput-object v2, v1, v6

    #@cc
    aput-object v1, v0, v6

    #@ce
    new-array v1, v7, [Ljava/lang/String;

    #@d0
    const-string v2, "INTEGER"

    #@d2
    aput-object v2, v1, v4

    #@d4
    const-string v2, "audio_0_pdp_profile_num"

    #@d6
    aput-object v2, v1, v5

    #@d8
    const-string v2, "0"

    #@da
    aput-object v2, v1, v6

    #@dc
    aput-object v1, v0, v7

    #@de
    new-array v1, v7, [Ljava/lang/String;

    #@e0
    const-string v2, "INTEGER"

    #@e2
    aput-object v2, v1, v4

    #@e4
    const-string v2, "audio_0_sdp_answer_fullcapa"

    #@e6
    aput-object v2, v1, v5

    #@e8
    const-string v2, "0"

    #@ea
    aput-object v2, v1, v6

    #@ec
    aput-object v1, v0, v8

    #@ee
    const/4 v1, 0x5

    #@ef
    new-array v2, v7, [Ljava/lang/String;

    #@f1
    const-string v3, "INTEGER"

    #@f3
    aput-object v3, v2, v4

    #@f5
    const-string v3, "audio_0_sdp_reoffer_fullcapa"

    #@f7
    aput-object v3, v2, v5

    #@f9
    const-string v3, "0"

    #@fb
    aput-object v3, v2, v6

    #@fd
    aput-object v2, v0, v1

    #@ff
    const/4 v1, 0x6

    #@100
    new-array v2, v7, [Ljava/lang/String;

    #@102
    const-string v3, "INTEGER"

    #@104
    aput-object v3, v2, v4

    #@106
    const-string v3, "audio_0_sdp_keep_m_line"

    #@108
    aput-object v3, v2, v5

    #@10a
    const-string v3, "1"

    #@10c
    aput-object v3, v2, v6

    #@10e
    aput-object v2, v0, v1

    #@110
    const/4 v1, 0x7

    #@111
    new-array v2, v7, [Ljava/lang/String;

    #@113
    const-string v3, "INTEGER"

    #@115
    aput-object v3, v2, v4

    #@117
    const-string v3, "audio_0_scr_enable"

    #@119
    aput-object v3, v2, v5

    #@11b
    const-string v3, "1"

    #@11d
    aput-object v3, v2, v6

    #@11f
    aput-object v2, v0, v1

    #@121
    const/16 v1, 0x8

    #@123
    new-array v2, v7, [Ljava/lang/String;

    #@125
    const-string v3, "TEXT"

    #@127
    aput-object v3, v2, v4

    #@129
    const-string v3, "audio_0_port_rtp"

    #@12b
    aput-object v3, v2, v5

    #@12d
    const-string v3, "49152,65535,1"

    #@12f
    aput-object v3, v2, v6

    #@131
    aput-object v2, v0, v1

    #@133
    const/16 v1, 0x9

    #@135
    new-array v2, v7, [Ljava/lang/String;

    #@137
    const-string v3, "INTEGER"

    #@139
    aput-object v3, v2, v4

    #@13b
    const-string v3, "audio_0_common_rtp_port_per_service"

    #@13d
    aput-object v3, v2, v5

    #@13f
    const-string v3, "1"

    #@141
    aput-object v3, v2, v6

    #@143
    aput-object v2, v0, v1

    #@145
    const/16 v1, 0xa

    #@147
    new-array v2, v7, [Ljava/lang/String;

    #@149
    const-string v3, "TEXT"

    #@14b
    aput-object v3, v2, v4

    #@14d
    const-string v3, "audio_0_jitter_buffer_size"

    #@14f
    aput-object v3, v2, v5

    #@151
    const-string v3, "8,8,13"

    #@153
    aput-object v3, v2, v6

    #@155
    aput-object v2, v0, v1

    #@157
    const/16 v1, 0xb

    #@159
    new-array v2, v7, [Ljava/lang/String;

    #@15b
    const-string v3, "INTEGER"

    #@15d
    aput-object v3, v2, v4

    #@15f
    const-string v3, "audio_0_rtcp_enable"

    #@161
    aput-object v3, v2, v5

    #@163
    const-string v3, "0"

    #@165
    aput-object v3, v2, v6

    #@167
    aput-object v2, v0, v1

    #@169
    const/16 v1, 0xc

    #@16b
    new-array v2, v7, [Ljava/lang/String;

    #@16d
    const-string v3, "INTEGER"

    #@16f
    aput-object v3, v2, v4

    #@171
    const-string v3, "audio_0_send_rtcp_bye"

    #@173
    aput-object v3, v2, v5

    #@175
    const-string v3, "0"

    #@177
    aput-object v3, v2, v6

    #@179
    aput-object v2, v0, v1

    #@17b
    const/16 v1, 0xd

    #@17d
    new-array v2, v7, [Ljava/lang/String;

    #@17f
    const-string v3, "TEXT"

    #@181
    aput-object v3, v2, v4

    #@183
    const-string v3, "audio_0_tv_rtp_inactivity"

    #@185
    aput-object v3, v2, v5

    #@187
    const-string v3, "20,10,10"

    #@189
    aput-object v3, v2, v6

    #@18b
    aput-object v2, v0, v1

    #@18d
    const/16 v1, 0xe

    #@18f
    new-array v2, v7, [Ljava/lang/String;

    #@191
    const-string v3, "INTEGER"

    #@193
    aput-object v3, v2, v4

    #@195
    const-string v3, "audio_0_hold_tone_mode"

    #@197
    aput-object v3, v2, v5

    #@199
    const-string v3, "1"

    #@19b
    aput-object v3, v2, v6

    #@19d
    aput-object v2, v0, v1

    #@19f
    const/16 v1, 0xf

    #@1a1
    new-array v2, v7, [Ljava/lang/String;

    #@1a3
    const-string v3, "INTEGER"

    #@1a5
    aput-object v3, v2, v4

    #@1a7
    const-string v3, "audio_0_bw_mode"

    #@1a9
    aput-object v3, v2, v5

    #@1ab
    const-string v3, "1"

    #@1ad
    aput-object v3, v2, v6

    #@1af
    aput-object v2, v0, v1

    #@1b1
    const/16 v1, 0x10

    #@1b3
    new-array v2, v7, [Ljava/lang/String;

    #@1b5
    const-string v3, "INTEGER"

    #@1b7
    aput-object v3, v2, v4

    #@1b9
    const-string v3, "audio_0_bw_as"

    #@1bb
    aput-object v3, v2, v5

    #@1bd
    const-string v3, "49"

    #@1bf
    aput-object v3, v2, v6

    #@1c1
    aput-object v2, v0, v1

    #@1c3
    const/16 v1, 0x11

    #@1c5
    new-array v2, v7, [Ljava/lang/String;

    #@1c7
    const-string v3, "INTEGER"

    #@1c9
    aput-object v3, v2, v4

    #@1cb
    const-string v3, "audio_0_bw_rs"

    #@1cd
    aput-object v3, v2, v5

    #@1cf
    const-string v3, "0"

    #@1d1
    aput-object v3, v2, v6

    #@1d3
    aput-object v2, v0, v1

    #@1d5
    const/16 v1, 0x12

    #@1d7
    new-array v2, v7, [Ljava/lang/String;

    #@1d9
    const-string v3, "INTEGER"

    #@1db
    aput-object v3, v2, v4

    #@1dd
    const-string v3, "audio_0_bw_rr"

    #@1df
    aput-object v3, v2, v5

    #@1e1
    const-string v3, "0"

    #@1e3
    aput-object v3, v2, v6

    #@1e5
    aput-object v2, v0, v1

    #@1e7
    const/16 v1, 0x13

    #@1e9
    new-array v2, v7, [Ljava/lang/String;

    #@1eb
    const-string v3, "INTEGER"

    #@1ed
    aput-object v3, v2, v4

    #@1ef
    const-string v3, "audio_0_bw_answer_option"

    #@1f1
    aput-object v3, v2, v5

    #@1f3
    const-string v3, "1"

    #@1f5
    aput-object v3, v2, v6

    #@1f7
    aput-object v2, v0, v1

    #@1f9
    const/16 v1, 0x14

    #@1fb
    new-array v2, v7, [Ljava/lang/String;

    #@1fd
    const-string v3, "INTEGER"

    #@1ff
    aput-object v3, v2, v4

    #@201
    const-string v3, "audio_0_ptime"

    #@203
    aput-object v3, v2, v5

    #@205
    const-string v3, "20"

    #@207
    aput-object v3, v2, v6

    #@209
    aput-object v2, v0, v1

    #@20b
    const/16 v1, 0x15

    #@20d
    new-array v2, v7, [Ljava/lang/String;

    #@20f
    const-string v3, "INTEGER"

    #@211
    aput-object v3, v2, v4

    #@213
    const-string v3, "audio_0_maxptime"

    #@215
    aput-object v3, v2, v5

    #@217
    const-string v3, "240"

    #@219
    aput-object v3, v2, v6

    #@21b
    aput-object v2, v0, v1

    #@21d
    const/16 v1, 0x16

    #@21f
    new-array v2, v7, [Ljava/lang/String;

    #@221
    const-string v3, "INTEGER"

    #@223
    aput-object v3, v2, v4

    #@225
    const-string v3, "audio_0_candidate_priority"

    #@227
    aput-object v3, v2, v5

    #@229
    const-string v3, ""

    #@22b
    aput-object v3, v2, v6

    #@22d
    aput-object v2, v0, v1

    #@22f
    const/16 v1, 0x17

    #@231
    new-array v2, v7, [Ljava/lang/String;

    #@233
    const-string v3, "TEXT"

    #@235
    aput-object v3, v2, v4

    #@237
    const-string v3, "audio_0_telephone_event_duration"

    #@239
    aput-object v3, v2, v5

    #@23b
    const-string v3, "5,2,2"

    #@23d
    aput-object v3, v2, v6

    #@23f
    aput-object v2, v0, v1

    #@241
    const/16 v1, 0x18

    #@243
    new-array v2, v7, [Ljava/lang/String;

    #@245
    const-string v3, "INTEGER"

    #@247
    aput-object v3, v2, v4

    #@249
    const-string v3, "audio_0_codec_list_size"

    #@24b
    aput-object v3, v2, v5

    #@24d
    const-string v3, "6"

    #@24f
    aput-object v3, v2, v6

    #@251
    aput-object v2, v0, v1

    #@253
    const/16 v1, 0x19

    #@255
    new-array v2, v7, [Ljava/lang/String;

    #@257
    const-string v3, "TEXT"

    #@259
    aput-object v3, v2, v4

    #@25b
    const-string v3, "audio_0_codec_ref"

    #@25d
    aput-object v3, v2, v5

    #@25f
    const-string v3, "lgims.com.media.audio.codec.volte"

    #@261
    aput-object v3, v2, v6

    #@263
    aput-object v2, v0, v1

    #@265
    const/16 v1, 0x1a

    #@267
    new-array v2, v7, [Ljava/lang/String;

    #@269
    const-string v3, "TEXT"

    #@26b
    aput-object v3, v2, v4

    #@26d
    const-string v3, "audio_1_session_type"

    #@26f
    aput-object v3, v2, v5

    #@271
    const-string v3, "vt"

    #@273
    aput-object v3, v2, v6

    #@275
    aput-object v2, v0, v1

    #@277
    const/16 v1, 0x1b

    #@279
    new-array v2, v7, [Ljava/lang/String;

    #@27b
    const-string v3, "INTEGER"

    #@27d
    aput-object v3, v2, v4

    #@27f
    const-string v3, "audio_1_pdp_profile_num"

    #@281
    aput-object v3, v2, v5

    #@283
    const-string v3, "0"

    #@285
    aput-object v3, v2, v6

    #@287
    aput-object v2, v0, v1

    #@289
    const/16 v1, 0x1c

    #@28b
    new-array v2, v7, [Ljava/lang/String;

    #@28d
    const-string v3, "INTEGER"

    #@28f
    aput-object v3, v2, v4

    #@291
    const-string v3, "audio_1_sdp_answer_fullcapa"

    #@293
    aput-object v3, v2, v5

    #@295
    const-string v3, "0"

    #@297
    aput-object v3, v2, v6

    #@299
    aput-object v2, v0, v1

    #@29b
    const/16 v1, 0x1d

    #@29d
    new-array v2, v7, [Ljava/lang/String;

    #@29f
    const-string v3, "INTEGER"

    #@2a1
    aput-object v3, v2, v4

    #@2a3
    const-string v3, "audio_1_sdp_reoffer_fullcapa"

    #@2a5
    aput-object v3, v2, v5

    #@2a7
    const-string v3, "0"

    #@2a9
    aput-object v3, v2, v6

    #@2ab
    aput-object v2, v0, v1

    #@2ad
    const/16 v1, 0x1e

    #@2af
    new-array v2, v7, [Ljava/lang/String;

    #@2b1
    const-string v3, "INTEGER"

    #@2b3
    aput-object v3, v2, v4

    #@2b5
    const-string v3, "audio_1_sdp_keep_m_line"

    #@2b7
    aput-object v3, v2, v5

    #@2b9
    const-string v3, "1"

    #@2bb
    aput-object v3, v2, v6

    #@2bd
    aput-object v2, v0, v1

    #@2bf
    const/16 v1, 0x1f

    #@2c1
    new-array v2, v7, [Ljava/lang/String;

    #@2c3
    const-string v3, "INTEGER"

    #@2c5
    aput-object v3, v2, v4

    #@2c7
    const-string v3, "audio_1_scr_enable"

    #@2c9
    aput-object v3, v2, v5

    #@2cb
    const-string v3, "1"

    #@2cd
    aput-object v3, v2, v6

    #@2cf
    aput-object v2, v0, v1

    #@2d1
    const/16 v1, 0x20

    #@2d3
    new-array v2, v7, [Ljava/lang/String;

    #@2d5
    const-string v3, "TEXT"

    #@2d7
    aput-object v3, v2, v4

    #@2d9
    const-string v3, "audio_1_port_rtp"

    #@2db
    aput-object v3, v2, v5

    #@2dd
    const-string v3, "49152,65535,2"

    #@2df
    aput-object v3, v2, v6

    #@2e1
    aput-object v2, v0, v1

    #@2e3
    const/16 v1, 0x21

    #@2e5
    new-array v2, v7, [Ljava/lang/String;

    #@2e7
    const-string v3, "INTEGER"

    #@2e9
    aput-object v3, v2, v4

    #@2eb
    const-string v3, "audio_1_common_rtp_port_per_service"

    #@2ed
    aput-object v3, v2, v5

    #@2ef
    const-string v3, "1"

    #@2f1
    aput-object v3, v2, v6

    #@2f3
    aput-object v2, v0, v1

    #@2f5
    const/16 v1, 0x22

    #@2f7
    new-array v2, v7, [Ljava/lang/String;

    #@2f9
    const-string v3, "TEXT"

    #@2fb
    aput-object v3, v2, v4

    #@2fd
    const-string v3, "audio_1_jitter_buffer_size"

    #@2ff
    aput-object v3, v2, v5

    #@301
    const-string v3, "13,13,19"

    #@303
    aput-object v3, v2, v6

    #@305
    aput-object v2, v0, v1

    #@307
    const/16 v1, 0x23

    #@309
    new-array v2, v7, [Ljava/lang/String;

    #@30b
    const-string v3, "INTEGER"

    #@30d
    aput-object v3, v2, v4

    #@30f
    const-string v3, "audio_1_rtcp_enable"

    #@311
    aput-object v3, v2, v5

    #@313
    const-string v3, "1"

    #@315
    aput-object v3, v2, v6

    #@317
    aput-object v2, v0, v1

    #@319
    const/16 v1, 0x24

    #@31b
    new-array v2, v7, [Ljava/lang/String;

    #@31d
    const-string v3, "INTEGER"

    #@31f
    aput-object v3, v2, v4

    #@321
    const-string v3, "audio_1_send_rtcp_bye"

    #@323
    aput-object v3, v2, v5

    #@325
    const-string v3, "0"

    #@327
    aput-object v3, v2, v6

    #@329
    aput-object v2, v0, v1

    #@32b
    const/16 v1, 0x25

    #@32d
    new-array v2, v7, [Ljava/lang/String;

    #@32f
    const-string v3, "TEXT"

    #@331
    aput-object v3, v2, v4

    #@333
    const-string v3, "audio_1_tv_rtp_inactivity"

    #@335
    aput-object v3, v2, v5

    #@337
    const-string v3, "20,10,10"

    #@339
    aput-object v3, v2, v6

    #@33b
    aput-object v2, v0, v1

    #@33d
    const/16 v1, 0x26

    #@33f
    new-array v2, v7, [Ljava/lang/String;

    #@341
    const-string v3, "INTEGER"

    #@343
    aput-object v3, v2, v4

    #@345
    const-string v3, "audio_1_hold_tone_mode"

    #@347
    aput-object v3, v2, v5

    #@349
    const-string v3, "1"

    #@34b
    aput-object v3, v2, v6

    #@34d
    aput-object v2, v0, v1

    #@34f
    const/16 v1, 0x27

    #@351
    new-array v2, v7, [Ljava/lang/String;

    #@353
    const-string v3, "INTEGER"

    #@355
    aput-object v3, v2, v4

    #@357
    const-string v3, "audio_1_bw_mode"

    #@359
    aput-object v3, v2, v5

    #@35b
    const-string v3, "1"

    #@35d
    aput-object v3, v2, v6

    #@35f
    aput-object v2, v0, v1

    #@361
    const/16 v1, 0x28

    #@363
    new-array v2, v7, [Ljava/lang/String;

    #@365
    const-string v3, "INTEGER"

    #@367
    aput-object v3, v2, v4

    #@369
    const-string v3, "audio_1_bw_as"

    #@36b
    aput-object v3, v2, v5

    #@36d
    const-string v3, "49"

    #@36f
    aput-object v3, v2, v6

    #@371
    aput-object v2, v0, v1

    #@373
    const/16 v1, 0x29

    #@375
    new-array v2, v7, [Ljava/lang/String;

    #@377
    const-string v3, "INTEGER"

    #@379
    aput-object v3, v2, v4

    #@37b
    const-string v3, "audio_1_bw_rs"

    #@37d
    aput-object v3, v2, v5

    #@37f
    const-string v3, "0"

    #@381
    aput-object v3, v2, v6

    #@383
    aput-object v2, v0, v1

    #@385
    const/16 v1, 0x2a

    #@387
    new-array v2, v7, [Ljava/lang/String;

    #@389
    const-string v3, "INTEGER"

    #@38b
    aput-object v3, v2, v4

    #@38d
    const-string v3, "audio_1_bw_rr"

    #@38f
    aput-object v3, v2, v5

    #@391
    const-string v3, "0"

    #@393
    aput-object v3, v2, v6

    #@395
    aput-object v2, v0, v1

    #@397
    const/16 v1, 0x2b

    #@399
    new-array v2, v7, [Ljava/lang/String;

    #@39b
    const-string v3, "INTEGER"

    #@39d
    aput-object v3, v2, v4

    #@39f
    const-string v3, "audio_1_bw_answer_option"

    #@3a1
    aput-object v3, v2, v5

    #@3a3
    const-string v3, "1"

    #@3a5
    aput-object v3, v2, v6

    #@3a7
    aput-object v2, v0, v1

    #@3a9
    const/16 v1, 0x2c

    #@3ab
    new-array v2, v7, [Ljava/lang/String;

    #@3ad
    const-string v3, "INTEGER"

    #@3af
    aput-object v3, v2, v4

    #@3b1
    const-string v3, "audio_1_ptime"

    #@3b3
    aput-object v3, v2, v5

    #@3b5
    const-string v3, "20"

    #@3b7
    aput-object v3, v2, v6

    #@3b9
    aput-object v2, v0, v1

    #@3bb
    const/16 v1, 0x2d

    #@3bd
    new-array v2, v7, [Ljava/lang/String;

    #@3bf
    const-string v3, "INTEGER"

    #@3c1
    aput-object v3, v2, v4

    #@3c3
    const-string v3, "audio_1_maxptime"

    #@3c5
    aput-object v3, v2, v5

    #@3c7
    const-string v3, "240"

    #@3c9
    aput-object v3, v2, v6

    #@3cb
    aput-object v2, v0, v1

    #@3cd
    const/16 v1, 0x2e

    #@3cf
    new-array v2, v7, [Ljava/lang/String;

    #@3d1
    const-string v3, "INTEGER"

    #@3d3
    aput-object v3, v2, v4

    #@3d5
    const-string v3, "audio_1_candidate_priority"

    #@3d7
    aput-object v3, v2, v5

    #@3d9
    const-string v3, ""

    #@3db
    aput-object v3, v2, v6

    #@3dd
    aput-object v2, v0, v1

    #@3df
    const/16 v1, 0x2f

    #@3e1
    new-array v2, v7, [Ljava/lang/String;

    #@3e3
    const-string v3, "TEXT"

    #@3e5
    aput-object v3, v2, v4

    #@3e7
    const-string v3, "audio_1_telephone_event_duration"

    #@3e9
    aput-object v3, v2, v5

    #@3eb
    const-string v3, "5,2,2"

    #@3ed
    aput-object v3, v2, v6

    #@3ef
    aput-object v2, v0, v1

    #@3f1
    const/16 v1, 0x30

    #@3f3
    new-array v2, v7, [Ljava/lang/String;

    #@3f5
    const-string v3, "INTEGER"

    #@3f7
    aput-object v3, v2, v4

    #@3f9
    const-string v3, "audio_1_codec_list_size"

    #@3fb
    aput-object v3, v2, v5

    #@3fd
    const-string v3, "6"

    #@3ff
    aput-object v3, v2, v6

    #@401
    aput-object v2, v0, v1

    #@403
    const/16 v1, 0x31

    #@405
    new-array v2, v7, [Ljava/lang/String;

    #@407
    const-string v3, "TEXT"

    #@409
    aput-object v3, v2, v4

    #@40b
    const-string v3, "audio_1_codec_ref"

    #@40d
    aput-object v3, v2, v5

    #@40f
    const-string v3, "lgims.com.media.audio.codec.vt"

    #@411
    aput-object v3, v2, v6

    #@413
    aput-object v2, v0, v1

    #@415
    sput-object v0, Lcom/lge/ims/provider/uc/ConfigMedia;->MEDIA_AUDIO:[[Ljava/lang/String;

    #@417
    .line 118
    const/16 v0, 0x17

    #@419
    new-array v0, v0, [[Ljava/lang/String;

    #@41b
    new-array v1, v7, [Ljava/lang/String;

    #@41d
    const-string v2, "INTEGER"

    #@41f
    aput-object v2, v1, v4

    #@421
    const-string v2, "id"

    #@423
    aput-object v2, v1, v5

    #@425
    const-string v2, "1"

    #@427
    aput-object v2, v1, v6

    #@429
    aput-object v1, v0, v4

    #@42b
    new-array v1, v7, [Ljava/lang/String;

    #@42d
    const-string v2, "INTEGER"

    #@42f
    aput-object v2, v1, v4

    #@431
    const-string v2, "property"

    #@433
    aput-object v2, v1, v5

    #@435
    const-string v2, "1"

    #@437
    aput-object v2, v1, v6

    #@439
    aput-object v1, v0, v5

    #@43b
    new-array v1, v7, [Ljava/lang/String;

    #@43d
    const-string v2, "TEXT"

    #@43f
    aput-object v2, v1, v4

    #@441
    const-string v2, "video_0_session_type"

    #@443
    aput-object v2, v1, v5

    #@445
    const-string v2, "vt"

    #@447
    aput-object v2, v1, v6

    #@449
    aput-object v1, v0, v6

    #@44b
    new-array v1, v7, [Ljava/lang/String;

    #@44d
    const-string v2, "INTEGER"

    #@44f
    aput-object v2, v1, v4

    #@451
    const-string v2, "video_0_sdp_answer_fullcapa"

    #@453
    aput-object v2, v1, v5

    #@455
    const-string v2, "0"

    #@457
    aput-object v2, v1, v6

    #@459
    aput-object v1, v0, v7

    #@45b
    new-array v1, v7, [Ljava/lang/String;

    #@45d
    const-string v2, "INTEGER"

    #@45f
    aput-object v2, v1, v4

    #@461
    const-string v2, "video_0_sdp_reoffer_fullcapa"

    #@463
    aput-object v2, v1, v5

    #@465
    const-string v2, "0"

    #@467
    aput-object v2, v1, v6

    #@469
    aput-object v1, v0, v8

    #@46b
    const/4 v1, 0x5

    #@46c
    new-array v2, v7, [Ljava/lang/String;

    #@46e
    const-string v3, "INTEGER"

    #@470
    aput-object v3, v2, v4

    #@472
    const-string v3, "video_0_sdp_keep_m_line"

    #@474
    aput-object v3, v2, v5

    #@476
    const-string v3, "1"

    #@478
    aput-object v3, v2, v6

    #@47a
    aput-object v2, v0, v1

    #@47c
    const/4 v1, 0x6

    #@47d
    new-array v2, v7, [Ljava/lang/String;

    #@47f
    const-string v3, "TEXT"

    #@481
    aput-object v3, v2, v4

    #@483
    const-string v3, "video_0_port_rtp"

    #@485
    aput-object v3, v2, v5

    #@487
    const-string v3, "49154,65535,2"

    #@489
    aput-object v3, v2, v6

    #@48b
    aput-object v2, v0, v1

    #@48d
    const/4 v1, 0x7

    #@48e
    new-array v2, v7, [Ljava/lang/String;

    #@490
    const-string v3, "INTEGER"

    #@492
    aput-object v3, v2, v4

    #@494
    const-string v3, "video_0_rtcp_enable"

    #@496
    aput-object v3, v2, v5

    #@498
    const-string v3, "1"

    #@49a
    aput-object v3, v2, v6

    #@49c
    aput-object v2, v0, v1

    #@49e
    const/16 v1, 0x8

    #@4a0
    new-array v2, v7, [Ljava/lang/String;

    #@4a2
    const-string v3, "INTEGER"

    #@4a4
    aput-object v3, v2, v4

    #@4a6
    const-string v3, "video_0_send_rtcp_bye"

    #@4a8
    aput-object v3, v2, v5

    #@4aa
    const-string v3, "0"

    #@4ac
    aput-object v3, v2, v6

    #@4ae
    aput-object v2, v0, v1

    #@4b0
    const/16 v1, 0x9

    #@4b2
    new-array v2, v7, [Ljava/lang/String;

    #@4b4
    const-string v3, "TEXT"

    #@4b6
    aput-object v3, v2, v4

    #@4b8
    const-string v3, "video_0_tv_rtp_inactivity"

    #@4ba
    aput-object v3, v2, v5

    #@4bc
    const-string v3, "20,10,10"

    #@4be
    aput-object v3, v2, v6

    #@4c0
    aput-object v2, v0, v1

    #@4c2
    const/16 v1, 0xa

    #@4c4
    new-array v2, v7, [Ljava/lang/String;

    #@4c6
    const-string v3, "INTEGER"

    #@4c8
    aput-object v3, v2, v4

    #@4ca
    const-string v3, "video_0_bw_mode"

    #@4cc
    aput-object v3, v2, v5

    #@4ce
    const-string v3, "1"

    #@4d0
    aput-object v3, v2, v6

    #@4d2
    aput-object v2, v0, v1

    #@4d4
    const/16 v1, 0xb

    #@4d6
    new-array v2, v7, [Ljava/lang/String;

    #@4d8
    const-string v3, "INTEGER"

    #@4da
    aput-object v3, v2, v4

    #@4dc
    const-string v3, "video_0_bw_as"

    #@4de
    aput-object v3, v2, v5

    #@4e0
    const-string v3, "0"

    #@4e2
    aput-object v3, v2, v6

    #@4e4
    aput-object v2, v0, v1

    #@4e6
    const/16 v1, 0xc

    #@4e8
    new-array v2, v7, [Ljava/lang/String;

    #@4ea
    const-string v3, "INTEGER"

    #@4ec
    aput-object v3, v2, v4

    #@4ee
    const-string v3, "video_0_bw_rs"

    #@4f0
    aput-object v3, v2, v5

    #@4f2
    const-string v3, "0"

    #@4f4
    aput-object v3, v2, v6

    #@4f6
    aput-object v2, v0, v1

    #@4f8
    const/16 v1, 0xd

    #@4fa
    new-array v2, v7, [Ljava/lang/String;

    #@4fc
    const-string v3, "INTEGER"

    #@4fe
    aput-object v3, v2, v4

    #@500
    const-string v3, "video_0_bw_rr"

    #@502
    aput-object v3, v2, v5

    #@504
    const-string v3, "0"

    #@506
    aput-object v3, v2, v6

    #@508
    aput-object v2, v0, v1

    #@50a
    const/16 v1, 0xe

    #@50c
    new-array v2, v7, [Ljava/lang/String;

    #@50e
    const-string v3, "INTEGER"

    #@510
    aput-object v3, v2, v4

    #@512
    const-string v3, "video_0_framerate_mode"

    #@514
    aput-object v3, v2, v5

    #@516
    const-string v3, "1"

    #@518
    aput-object v3, v2, v6

    #@51a
    aput-object v2, v0, v1

    #@51c
    const/16 v1, 0xf

    #@51e
    new-array v2, v7, [Ljava/lang/String;

    #@520
    const-string v3, "INTEGER"

    #@522
    aput-object v3, v2, v4

    #@524
    const-string v3, "video_0_framerate"

    #@526
    aput-object v3, v2, v5

    #@528
    const-string v3, "15"

    #@52a
    aput-object v3, v2, v6

    #@52c
    aput-object v2, v0, v1

    #@52e
    const/16 v1, 0x10

    #@530
    new-array v2, v7, [Ljava/lang/String;

    #@532
    const-string v3, "INTEGER"

    #@534
    aput-object v3, v2, v4

    #@536
    const-string v3, "video_0_enable_onscreen_debug_info_video"

    #@538
    aput-object v3, v2, v5

    #@53a
    const-string v3, "0"

    #@53c
    aput-object v3, v2, v6

    #@53e
    aput-object v2, v0, v1

    #@540
    const/16 v1, 0x11

    #@542
    new-array v2, v7, [Ljava/lang/String;

    #@544
    const-string v3, "INTEGER"

    #@546
    aput-object v3, v2, v4

    #@548
    const-string v3, "video_0_send_periodic_sps_pps"

    #@54a
    aput-object v3, v2, v5

    #@54c
    const-string v3, "0"

    #@54e
    aput-object v3, v2, v6

    #@550
    aput-object v2, v0, v1

    #@552
    const/16 v1, 0x12

    #@554
    new-array v2, v7, [Ljava/lang/String;

    #@556
    const-string v3, "INTEGER"

    #@558
    aput-object v3, v2, v4

    #@55a
    const-string v3, "video_0_candidate_priority"

    #@55c
    aput-object v3, v2, v5

    #@55e
    const-string v3, ""

    #@560
    aput-object v3, v2, v6

    #@562
    aput-object v2, v0, v1

    #@564
    const/16 v1, 0x13

    #@566
    new-array v2, v7, [Ljava/lang/String;

    #@568
    const-string v3, "INTEGER"

    #@56a
    aput-object v3, v2, v4

    #@56c
    const-string v3, "video_0_conf_disable_tx_view"

    #@56e
    aput-object v3, v2, v5

    #@570
    const-string v3, "0"

    #@572
    aput-object v3, v2, v6

    #@574
    aput-object v2, v0, v1

    #@576
    const/16 v1, 0x14

    #@578
    new-array v2, v7, [Ljava/lang/String;

    #@57a
    const-string v3, "INTEGER"

    #@57c
    aput-object v3, v2, v4

    #@57e
    const-string v3, "video_0_conf_use_landscape"

    #@580
    aput-object v3, v2, v5

    #@582
    const-string v3, "0"

    #@584
    aput-object v3, v2, v6

    #@586
    aput-object v2, v0, v1

    #@588
    const/16 v1, 0x15

    #@58a
    new-array v2, v7, [Ljava/lang/String;

    #@58c
    const-string v3, "INTEGER"

    #@58e
    aput-object v3, v2, v4

    #@590
    const-string v3, "video_0_codec_list_size"

    #@592
    aput-object v3, v2, v5

    #@594
    const-string v3, "6"

    #@596
    aput-object v3, v2, v6

    #@598
    aput-object v2, v0, v1

    #@59a
    const/16 v1, 0x16

    #@59c
    new-array v2, v7, [Ljava/lang/String;

    #@59e
    const-string v3, "TEXT"

    #@5a0
    aput-object v3, v2, v4

    #@5a2
    const-string v3, "video_0_codec_ref"

    #@5a4
    aput-object v3, v2, v5

    #@5a6
    const-string v3, "lgims.com.media.video.codec.vt"

    #@5a8
    aput-object v3, v2, v6

    #@5aa
    aput-object v2, v0, v1

    #@5ac
    sput-object v0, Lcom/lge/ims/provider/uc/ConfigMedia;->MEDIA_VIDEO:[[Ljava/lang/String;

    #@5ae
    .line 165
    const/16 v0, 0x5c

    #@5b0
    new-array v0, v0, [[Ljava/lang/String;

    #@5b2
    new-array v1, v7, [Ljava/lang/String;

    #@5b4
    const-string v2, "INTEGER"

    #@5b6
    aput-object v2, v1, v4

    #@5b8
    const-string v2, "id"

    #@5ba
    aput-object v2, v1, v5

    #@5bc
    const-string v2, "1"

    #@5be
    aput-object v2, v1, v6

    #@5c0
    aput-object v1, v0, v4

    #@5c2
    new-array v1, v7, [Ljava/lang/String;

    #@5c4
    const-string v2, "INTEGER"

    #@5c6
    aput-object v2, v1, v4

    #@5c8
    const-string v2, "property"

    #@5ca
    aput-object v2, v1, v5

    #@5cc
    const-string v2, "1"

    #@5ce
    aput-object v2, v1, v6

    #@5d0
    aput-object v1, v0, v5

    #@5d2
    new-array v1, v7, [Ljava/lang/String;

    #@5d4
    const-string v2, "TEXT"

    #@5d6
    aput-object v2, v1, v4

    #@5d8
    const-string v2, "audiocodec_0_codec_type"

    #@5da
    aput-object v2, v1, v5

    #@5dc
    const-string v2, "AMR"

    #@5de
    aput-object v2, v1, v6

    #@5e0
    aput-object v1, v0, v6

    #@5e2
    new-array v1, v7, [Ljava/lang/String;

    #@5e4
    const-string v2, "TEXT"

    #@5e6
    aput-object v2, v1, v4

    #@5e8
    const-string v2, "audiocodec_0_network_type"

    #@5ea
    aput-object v2, v1, v5

    #@5ec
    const-string v2, "lte"

    #@5ee
    aput-object v2, v1, v6

    #@5f0
    aput-object v1, v0, v7

    #@5f2
    new-array v1, v7, [Ljava/lang/String;

    #@5f4
    const-string v2, "INTEGER"

    #@5f6
    aput-object v2, v1, v4

    #@5f8
    const-string v2, "audiocodec_0_payload_type"

    #@5fa
    aput-object v2, v1, v5

    #@5fc
    const-string v2, "97"

    #@5fe
    aput-object v2, v1, v6

    #@600
    aput-object v1, v0, v8

    #@602
    const/4 v1, 0x5

    #@603
    new-array v2, v7, [Ljava/lang/String;

    #@605
    const-string v3, "INTEGER"

    #@607
    aput-object v3, v2, v4

    #@609
    const-string v3, "audiocodec_0_sampling_rate"

    #@60b
    aput-object v3, v2, v5

    #@60d
    const-string v3, "16000"

    #@60f
    aput-object v3, v2, v6

    #@611
    aput-object v2, v0, v1

    #@613
    const/4 v1, 0x6

    #@614
    new-array v2, v7, [Ljava/lang/String;

    #@616
    const-string v3, "INTEGER"

    #@618
    aput-object v3, v2, v4

    #@61a
    const-string v3, "AMR_0_channel"

    #@61c
    aput-object v3, v2, v5

    #@61e
    const-string v3, "1"

    #@620
    aput-object v3, v2, v6

    #@622
    aput-object v2, v0, v1

    #@624
    const/4 v1, 0x7

    #@625
    new-array v2, v7, [Ljava/lang/String;

    #@627
    const-string v3, "INTEGER"

    #@629
    aput-object v3, v2, v4

    #@62b
    const-string v3, "AMR_0_octet_align"

    #@62d
    aput-object v3, v2, v5

    #@62f
    const-string v3, ""

    #@631
    aput-object v3, v2, v6

    #@633
    aput-object v2, v0, v1

    #@635
    const/16 v1, 0x8

    #@637
    new-array v2, v7, [Ljava/lang/String;

    #@639
    const-string v3, "INTEGER"

    #@63b
    aput-object v3, v2, v4

    #@63d
    const-string v3, "AMR_0_mode_change_capability"

    #@63f
    aput-object v3, v2, v5

    #@641
    const-string v3, "2"

    #@643
    aput-object v3, v2, v6

    #@645
    aput-object v2, v0, v1

    #@647
    const/16 v1, 0x9

    #@649
    new-array v2, v7, [Ljava/lang/String;

    #@64b
    const-string v3, "TEXT"

    #@64d
    aput-object v3, v2, v4

    #@64f
    const-string v3, "AMR_0_mode_set"

    #@651
    aput-object v3, v2, v5

    #@653
    const-string v3, ""

    #@655
    aput-object v3, v2, v6

    #@657
    aput-object v2, v0, v1

    #@659
    const/16 v1, 0xa

    #@65b
    new-array v2, v7, [Ljava/lang/String;

    #@65d
    const-string v3, "INTEGER"

    #@65f
    aput-object v3, v2, v4

    #@661
    const-string v3, "AMR_0_mode_change_period"

    #@663
    aput-object v3, v2, v5

    #@665
    const-string v3, ""

    #@667
    aput-object v3, v2, v6

    #@669
    aput-object v2, v0, v1

    #@66b
    const/16 v1, 0xb

    #@66d
    new-array v2, v7, [Ljava/lang/String;

    #@66f
    const-string v3, "INTEGER"

    #@671
    aput-object v3, v2, v4

    #@673
    const-string v3, "AMR_0_mode_change_neighbor"

    #@675
    aput-object v3, v2, v5

    #@677
    const-string v3, ""

    #@679
    aput-object v3, v2, v6

    #@67b
    aput-object v2, v0, v1

    #@67d
    const/16 v1, 0xc

    #@67f
    new-array v2, v7, [Ljava/lang/String;

    #@681
    const-string v3, "INTEGER"

    #@683
    aput-object v3, v2, v4

    #@685
    const-string v3, "AMR_0_max_red"

    #@687
    aput-object v3, v2, v5

    #@689
    const-string v3, "220"

    #@68b
    aput-object v3, v2, v6

    #@68d
    aput-object v2, v0, v1

    #@68f
    const/16 v1, 0xd

    #@691
    new-array v2, v7, [Ljava/lang/String;

    #@693
    const-string v3, "INTEGER"

    #@695
    aput-object v3, v2, v4

    #@697
    const-string v3, "AMR_0_robust_sorting"

    #@699
    aput-object v3, v2, v5

    #@69b
    const-string v3, ""

    #@69d
    aput-object v3, v2, v6

    #@69f
    aput-object v2, v0, v1

    #@6a1
    const/16 v1, 0xe

    #@6a3
    new-array v2, v7, [Ljava/lang/String;

    #@6a5
    const-string v3, "INTEGER"

    #@6a7
    aput-object v3, v2, v4

    #@6a9
    const-string v3, "AMR_0_ptime"

    #@6ab
    aput-object v3, v2, v5

    #@6ad
    const-string v3, ""

    #@6af
    aput-object v3, v2, v6

    #@6b1
    aput-object v2, v0, v1

    #@6b3
    const/16 v1, 0xf

    #@6b5
    new-array v2, v7, [Ljava/lang/String;

    #@6b7
    const-string v3, "INTEGER"

    #@6b9
    aput-object v3, v2, v4

    #@6bb
    const-string v3, "AMR_0_max_ptime"

    #@6bd
    aput-object v3, v2, v5

    #@6bf
    const-string v3, ""

    #@6c1
    aput-object v3, v2, v6

    #@6c3
    aput-object v2, v0, v1

    #@6c5
    const/16 v1, 0x10

    #@6c7
    new-array v2, v7, [Ljava/lang/String;

    #@6c9
    const-string v3, "TEXT"

    #@6cb
    aput-object v3, v2, v4

    #@6cd
    const-string v3, "telephone_event_0_events"

    #@6cf
    aput-object v3, v2, v5

    #@6d1
    const-string v3, "0-15"

    #@6d3
    aput-object v3, v2, v6

    #@6d5
    aput-object v2, v0, v1

    #@6d7
    const/16 v1, 0x11

    #@6d9
    new-array v2, v7, [Ljava/lang/String;

    #@6db
    const-string v3, "TEXT"

    #@6dd
    aput-object v3, v2, v4

    #@6df
    const-string v3, "audiocodec_1_codec_type"

    #@6e1
    aput-object v3, v2, v5

    #@6e3
    const-string v3, "AMR"

    #@6e5
    aput-object v3, v2, v6

    #@6e7
    aput-object v2, v0, v1

    #@6e9
    const/16 v1, 0x12

    #@6eb
    new-array v2, v7, [Ljava/lang/String;

    #@6ed
    const-string v3, "TEXT"

    #@6ef
    aput-object v3, v2, v4

    #@6f1
    const-string v3, "audiocodec_1_network_type"

    #@6f3
    aput-object v3, v2, v5

    #@6f5
    const-string v3, "lte"

    #@6f7
    aput-object v3, v2, v6

    #@6f9
    aput-object v2, v0, v1

    #@6fb
    const/16 v1, 0x13

    #@6fd
    new-array v2, v7, [Ljava/lang/String;

    #@6ff
    const-string v3, "INTEGER"

    #@701
    aput-object v3, v2, v4

    #@703
    const-string v3, "audiocodec_1_payload_type"

    #@705
    aput-object v3, v2, v5

    #@707
    const-string v3, "98"

    #@709
    aput-object v3, v2, v6

    #@70b
    aput-object v2, v0, v1

    #@70d
    const/16 v1, 0x14

    #@70f
    new-array v2, v7, [Ljava/lang/String;

    #@711
    const-string v3, "INTEGER"

    #@713
    aput-object v3, v2, v4

    #@715
    const-string v3, "audiocodec_1_sampling_rate"

    #@717
    aput-object v3, v2, v5

    #@719
    const-string v3, "16000"

    #@71b
    aput-object v3, v2, v6

    #@71d
    aput-object v2, v0, v1

    #@71f
    const/16 v1, 0x15

    #@721
    new-array v2, v7, [Ljava/lang/String;

    #@723
    const-string v3, "INTEGER"

    #@725
    aput-object v3, v2, v4

    #@727
    const-string v3, "AMR_1_channel"

    #@729
    aput-object v3, v2, v5

    #@72b
    const-string v3, "1"

    #@72d
    aput-object v3, v2, v6

    #@72f
    aput-object v2, v0, v1

    #@731
    const/16 v1, 0x16

    #@733
    new-array v2, v7, [Ljava/lang/String;

    #@735
    const-string v3, "INTEGER"

    #@737
    aput-object v3, v2, v4

    #@739
    const-string v3, "AMR_1_octet_align"

    #@73b
    aput-object v3, v2, v5

    #@73d
    const-string v3, "1"

    #@73f
    aput-object v3, v2, v6

    #@741
    aput-object v2, v0, v1

    #@743
    const/16 v1, 0x17

    #@745
    new-array v2, v7, [Ljava/lang/String;

    #@747
    const-string v3, "INTEGER"

    #@749
    aput-object v3, v2, v4

    #@74b
    const-string v3, "AMR_1_mode_change_capability"

    #@74d
    aput-object v3, v2, v5

    #@74f
    const-string v3, "2"

    #@751
    aput-object v3, v2, v6

    #@753
    aput-object v2, v0, v1

    #@755
    const/16 v1, 0x18

    #@757
    new-array v2, v7, [Ljava/lang/String;

    #@759
    const-string v3, "TEXT"

    #@75b
    aput-object v3, v2, v4

    #@75d
    const-string v3, "AMR_1_mode_set"

    #@75f
    aput-object v3, v2, v5

    #@761
    const-string v3, ""

    #@763
    aput-object v3, v2, v6

    #@765
    aput-object v2, v0, v1

    #@767
    const/16 v1, 0x19

    #@769
    new-array v2, v7, [Ljava/lang/String;

    #@76b
    const-string v3, "INTEGER"

    #@76d
    aput-object v3, v2, v4

    #@76f
    const-string v3, "AMR_1_mode_change_period"

    #@771
    aput-object v3, v2, v5

    #@773
    const-string v3, ""

    #@775
    aput-object v3, v2, v6

    #@777
    aput-object v2, v0, v1

    #@779
    const/16 v1, 0x1a

    #@77b
    new-array v2, v7, [Ljava/lang/String;

    #@77d
    const-string v3, "INTEGER"

    #@77f
    aput-object v3, v2, v4

    #@781
    const-string v3, "AMR_1_mode_change_neighbor"

    #@783
    aput-object v3, v2, v5

    #@785
    const-string v3, ""

    #@787
    aput-object v3, v2, v6

    #@789
    aput-object v2, v0, v1

    #@78b
    const/16 v1, 0x1b

    #@78d
    new-array v2, v7, [Ljava/lang/String;

    #@78f
    const-string v3, "INTEGER"

    #@791
    aput-object v3, v2, v4

    #@793
    const-string v3, "AMR_1_max_red"

    #@795
    aput-object v3, v2, v5

    #@797
    const-string v3, "220"

    #@799
    aput-object v3, v2, v6

    #@79b
    aput-object v2, v0, v1

    #@79d
    const/16 v1, 0x1c

    #@79f
    new-array v2, v7, [Ljava/lang/String;

    #@7a1
    const-string v3, "INTEGER"

    #@7a3
    aput-object v3, v2, v4

    #@7a5
    const-string v3, "AMR_1_robust_sorting"

    #@7a7
    aput-object v3, v2, v5

    #@7a9
    const-string v3, ""

    #@7ab
    aput-object v3, v2, v6

    #@7ad
    aput-object v2, v0, v1

    #@7af
    const/16 v1, 0x1d

    #@7b1
    new-array v2, v7, [Ljava/lang/String;

    #@7b3
    const-string v3, "INTEGER"

    #@7b5
    aput-object v3, v2, v4

    #@7b7
    const-string v3, "AMR_1_ptime"

    #@7b9
    aput-object v3, v2, v5

    #@7bb
    const-string v3, ""

    #@7bd
    aput-object v3, v2, v6

    #@7bf
    aput-object v2, v0, v1

    #@7c1
    const/16 v1, 0x1e

    #@7c3
    new-array v2, v7, [Ljava/lang/String;

    #@7c5
    const-string v3, "INTEGER"

    #@7c7
    aput-object v3, v2, v4

    #@7c9
    const-string v3, "AMR_1_max_ptime"

    #@7cb
    aput-object v3, v2, v5

    #@7cd
    const-string v3, ""

    #@7cf
    aput-object v3, v2, v6

    #@7d1
    aput-object v2, v0, v1

    #@7d3
    const/16 v1, 0x1f

    #@7d5
    new-array v2, v7, [Ljava/lang/String;

    #@7d7
    const-string v3, "TEXT"

    #@7d9
    aput-object v3, v2, v4

    #@7db
    const-string v3, "telephone_event_1_events"

    #@7dd
    aput-object v3, v2, v5

    #@7df
    const-string v3, "0-15"

    #@7e1
    aput-object v3, v2, v6

    #@7e3
    aput-object v2, v0, v1

    #@7e5
    const/16 v1, 0x20

    #@7e7
    new-array v2, v7, [Ljava/lang/String;

    #@7e9
    const-string v3, "TEXT"

    #@7eb
    aput-object v3, v2, v4

    #@7ed
    const-string v3, "audiocodec_2_codec_type"

    #@7ef
    aput-object v3, v2, v5

    #@7f1
    const-string v3, "telephone-event"

    #@7f3
    aput-object v3, v2, v6

    #@7f5
    aput-object v2, v0, v1

    #@7f7
    const/16 v1, 0x21

    #@7f9
    new-array v2, v7, [Ljava/lang/String;

    #@7fb
    const-string v3, "TEXT"

    #@7fd
    aput-object v3, v2, v4

    #@7ff
    const-string v3, "audiocodec_2_network_type"

    #@801
    aput-object v3, v2, v5

    #@803
    const-string v3, "lte"

    #@805
    aput-object v3, v2, v6

    #@807
    aput-object v2, v0, v1

    #@809
    const/16 v1, 0x22

    #@80b
    new-array v2, v7, [Ljava/lang/String;

    #@80d
    const-string v3, "INTEGER"

    #@80f
    aput-object v3, v2, v4

    #@811
    const-string v3, "audiocodec_2_payload_type"

    #@813
    aput-object v3, v2, v5

    #@815
    const-string v3, "99"

    #@817
    aput-object v3, v2, v6

    #@819
    aput-object v2, v0, v1

    #@81b
    const/16 v1, 0x23

    #@81d
    new-array v2, v7, [Ljava/lang/String;

    #@81f
    const-string v3, "INTEGER"

    #@821
    aput-object v3, v2, v4

    #@823
    const-string v3, "audiocodec_2_sampling_rate"

    #@825
    aput-object v3, v2, v5

    #@827
    const-string v3, "16000"

    #@829
    aput-object v3, v2, v6

    #@82b
    aput-object v2, v0, v1

    #@82d
    const/16 v1, 0x24

    #@82f
    new-array v2, v7, [Ljava/lang/String;

    #@831
    const-string v3, "INTEGER"

    #@833
    aput-object v3, v2, v4

    #@835
    const-string v3, "AMR_2_channel"

    #@837
    aput-object v3, v2, v5

    #@839
    const-string v3, ""

    #@83b
    aput-object v3, v2, v6

    #@83d
    aput-object v2, v0, v1

    #@83f
    const/16 v1, 0x25

    #@841
    new-array v2, v7, [Ljava/lang/String;

    #@843
    const-string v3, "INTEGER"

    #@845
    aput-object v3, v2, v4

    #@847
    const-string v3, "AMR_2_octet_align"

    #@849
    aput-object v3, v2, v5

    #@84b
    const-string v3, ""

    #@84d
    aput-object v3, v2, v6

    #@84f
    aput-object v2, v0, v1

    #@851
    const/16 v1, 0x26

    #@853
    new-array v2, v7, [Ljava/lang/String;

    #@855
    const-string v3, "INTEGER"

    #@857
    aput-object v3, v2, v4

    #@859
    const-string v3, "AMR_2_mode_change_capability"

    #@85b
    aput-object v3, v2, v5

    #@85d
    const-string v3, ""

    #@85f
    aput-object v3, v2, v6

    #@861
    aput-object v2, v0, v1

    #@863
    const/16 v1, 0x27

    #@865
    new-array v2, v7, [Ljava/lang/String;

    #@867
    const-string v3, "TEXT"

    #@869
    aput-object v3, v2, v4

    #@86b
    const-string v3, "AMR_2_mode_set"

    #@86d
    aput-object v3, v2, v5

    #@86f
    const-string v3, ""

    #@871
    aput-object v3, v2, v6

    #@873
    aput-object v2, v0, v1

    #@875
    const/16 v1, 0x28

    #@877
    new-array v2, v7, [Ljava/lang/String;

    #@879
    const-string v3, "INTEGER"

    #@87b
    aput-object v3, v2, v4

    #@87d
    const-string v3, "AMR_2_mode_change_period"

    #@87f
    aput-object v3, v2, v5

    #@881
    const-string v3, ""

    #@883
    aput-object v3, v2, v6

    #@885
    aput-object v2, v0, v1

    #@887
    const/16 v1, 0x29

    #@889
    new-array v2, v7, [Ljava/lang/String;

    #@88b
    const-string v3, "INTEGER"

    #@88d
    aput-object v3, v2, v4

    #@88f
    const-string v3, "AMR_2_mode_change_neighbor"

    #@891
    aput-object v3, v2, v5

    #@893
    const-string v3, ""

    #@895
    aput-object v3, v2, v6

    #@897
    aput-object v2, v0, v1

    #@899
    const/16 v1, 0x2a

    #@89b
    new-array v2, v7, [Ljava/lang/String;

    #@89d
    const-string v3, "INTEGER"

    #@89f
    aput-object v3, v2, v4

    #@8a1
    const-string v3, "AMR_2_max_red"

    #@8a3
    aput-object v3, v2, v5

    #@8a5
    const-string v3, ""

    #@8a7
    aput-object v3, v2, v6

    #@8a9
    aput-object v2, v0, v1

    #@8ab
    const/16 v1, 0x2b

    #@8ad
    new-array v2, v7, [Ljava/lang/String;

    #@8af
    const-string v3, "INTEGER"

    #@8b1
    aput-object v3, v2, v4

    #@8b3
    const-string v3, "AMR_2_robust_sorting"

    #@8b5
    aput-object v3, v2, v5

    #@8b7
    const-string v3, ""

    #@8b9
    aput-object v3, v2, v6

    #@8bb
    aput-object v2, v0, v1

    #@8bd
    const/16 v1, 0x2c

    #@8bf
    new-array v2, v7, [Ljava/lang/String;

    #@8c1
    const-string v3, "INTEGER"

    #@8c3
    aput-object v3, v2, v4

    #@8c5
    const-string v3, "AMR_2_ptime"

    #@8c7
    aput-object v3, v2, v5

    #@8c9
    const-string v3, ""

    #@8cb
    aput-object v3, v2, v6

    #@8cd
    aput-object v2, v0, v1

    #@8cf
    const/16 v1, 0x2d

    #@8d1
    new-array v2, v7, [Ljava/lang/String;

    #@8d3
    const-string v3, "INTEGER"

    #@8d5
    aput-object v3, v2, v4

    #@8d7
    const-string v3, "AMR_2_max_ptime"

    #@8d9
    aput-object v3, v2, v5

    #@8db
    const-string v3, ""

    #@8dd
    aput-object v3, v2, v6

    #@8df
    aput-object v2, v0, v1

    #@8e1
    const/16 v1, 0x2e

    #@8e3
    new-array v2, v7, [Ljava/lang/String;

    #@8e5
    const-string v3, "TEXT"

    #@8e7
    aput-object v3, v2, v4

    #@8e9
    const-string v3, "telephone_event_2_events"

    #@8eb
    aput-object v3, v2, v5

    #@8ed
    const-string v3, "0-15"

    #@8ef
    aput-object v3, v2, v6

    #@8f1
    aput-object v2, v0, v1

    #@8f3
    const/16 v1, 0x2f

    #@8f5
    new-array v2, v7, [Ljava/lang/String;

    #@8f7
    const-string v3, "TEXT"

    #@8f9
    aput-object v3, v2, v4

    #@8fb
    const-string v3, "audiocodec_3_codec_type"

    #@8fd
    aput-object v3, v2, v5

    #@8ff
    const-string v3, "AMR"

    #@901
    aput-object v3, v2, v6

    #@903
    aput-object v2, v0, v1

    #@905
    const/16 v1, 0x30

    #@907
    new-array v2, v7, [Ljava/lang/String;

    #@909
    const-string v3, "TEXT"

    #@90b
    aput-object v3, v2, v4

    #@90d
    const-string v3, "audiocodec_3_network_type"

    #@90f
    aput-object v3, v2, v5

    #@911
    const-string v3, "lte,hspa,3g"

    #@913
    aput-object v3, v2, v6

    #@915
    aput-object v2, v0, v1

    #@917
    const/16 v1, 0x31

    #@919
    new-array v2, v7, [Ljava/lang/String;

    #@91b
    const-string v3, "INTEGER"

    #@91d
    aput-object v3, v2, v4

    #@91f
    const-string v3, "audiocodec_3_payload_type"

    #@921
    aput-object v3, v2, v5

    #@923
    const-string v3, "100"

    #@925
    aput-object v3, v2, v6

    #@927
    aput-object v2, v0, v1

    #@929
    const/16 v1, 0x32

    #@92b
    new-array v2, v7, [Ljava/lang/String;

    #@92d
    const-string v3, "INTEGER"

    #@92f
    aput-object v3, v2, v4

    #@931
    const-string v3, "audiocodec_3_sampling_rate"

    #@933
    aput-object v3, v2, v5

    #@935
    const-string v3, "8000"

    #@937
    aput-object v3, v2, v6

    #@939
    aput-object v2, v0, v1

    #@93b
    const/16 v1, 0x33

    #@93d
    new-array v2, v7, [Ljava/lang/String;

    #@93f
    const-string v3, "INTEGER"

    #@941
    aput-object v3, v2, v4

    #@943
    const-string v3, "AMR_3_channel"

    #@945
    aput-object v3, v2, v5

    #@947
    const-string v3, "1"

    #@949
    aput-object v3, v2, v6

    #@94b
    aput-object v2, v0, v1

    #@94d
    const/16 v1, 0x34

    #@94f
    new-array v2, v7, [Ljava/lang/String;

    #@951
    const-string v3, "INTEGER"

    #@953
    aput-object v3, v2, v4

    #@955
    const-string v3, "AMR_3_octet_align"

    #@957
    aput-object v3, v2, v5

    #@959
    const-string v3, ""

    #@95b
    aput-object v3, v2, v6

    #@95d
    aput-object v2, v0, v1

    #@95f
    const/16 v1, 0x35

    #@961
    new-array v2, v7, [Ljava/lang/String;

    #@963
    const-string v3, "INTEGER"

    #@965
    aput-object v3, v2, v4

    #@967
    const-string v3, "AMR_3_mode_change_capability"

    #@969
    aput-object v3, v2, v5

    #@96b
    const-string v3, "2"

    #@96d
    aput-object v3, v2, v6

    #@96f
    aput-object v2, v0, v1

    #@971
    const/16 v1, 0x36

    #@973
    new-array v2, v7, [Ljava/lang/String;

    #@975
    const-string v3, "TEXT"

    #@977
    aput-object v3, v2, v4

    #@979
    const-string v3, "AMR_3_mode_set"

    #@97b
    aput-object v3, v2, v5

    #@97d
    const-string v3, ""

    #@97f
    aput-object v3, v2, v6

    #@981
    aput-object v2, v0, v1

    #@983
    const/16 v1, 0x37

    #@985
    new-array v2, v7, [Ljava/lang/String;

    #@987
    const-string v3, "INTEGER"

    #@989
    aput-object v3, v2, v4

    #@98b
    const-string v3, "AMR_3_mode_change_period"

    #@98d
    aput-object v3, v2, v5

    #@98f
    const-string v3, ""

    #@991
    aput-object v3, v2, v6

    #@993
    aput-object v2, v0, v1

    #@995
    const/16 v1, 0x38

    #@997
    new-array v2, v7, [Ljava/lang/String;

    #@999
    const-string v3, "INTEGER"

    #@99b
    aput-object v3, v2, v4

    #@99d
    const-string v3, "AMR_3_mode_change_neighbor"

    #@99f
    aput-object v3, v2, v5

    #@9a1
    const-string v3, ""

    #@9a3
    aput-object v3, v2, v6

    #@9a5
    aput-object v2, v0, v1

    #@9a7
    const/16 v1, 0x39

    #@9a9
    new-array v2, v7, [Ljava/lang/String;

    #@9ab
    const-string v3, "INTEGER"

    #@9ad
    aput-object v3, v2, v4

    #@9af
    const-string v3, "AMR_3_max_red"

    #@9b1
    aput-object v3, v2, v5

    #@9b3
    const-string v3, "220"

    #@9b5
    aput-object v3, v2, v6

    #@9b7
    aput-object v2, v0, v1

    #@9b9
    const/16 v1, 0x3a

    #@9bb
    new-array v2, v7, [Ljava/lang/String;

    #@9bd
    const-string v3, "INTEGER"

    #@9bf
    aput-object v3, v2, v4

    #@9c1
    const-string v3, "AMR_3_robust_sorting"

    #@9c3
    aput-object v3, v2, v5

    #@9c5
    const-string v3, ""

    #@9c7
    aput-object v3, v2, v6

    #@9c9
    aput-object v2, v0, v1

    #@9cb
    const/16 v1, 0x3b

    #@9cd
    new-array v2, v7, [Ljava/lang/String;

    #@9cf
    const-string v3, "INTEGER"

    #@9d1
    aput-object v3, v2, v4

    #@9d3
    const-string v3, "AMR_3_ptime"

    #@9d5
    aput-object v3, v2, v5

    #@9d7
    const-string v3, ""

    #@9d9
    aput-object v3, v2, v6

    #@9db
    aput-object v2, v0, v1

    #@9dd
    const/16 v1, 0x3c

    #@9df
    new-array v2, v7, [Ljava/lang/String;

    #@9e1
    const-string v3, "INTEGER"

    #@9e3
    aput-object v3, v2, v4

    #@9e5
    const-string v3, "AMR_3_max_ptime"

    #@9e7
    aput-object v3, v2, v5

    #@9e9
    const-string v3, ""

    #@9eb
    aput-object v3, v2, v6

    #@9ed
    aput-object v2, v0, v1

    #@9ef
    const/16 v1, 0x3d

    #@9f1
    new-array v2, v7, [Ljava/lang/String;

    #@9f3
    const-string v3, "TEXT"

    #@9f5
    aput-object v3, v2, v4

    #@9f7
    const-string v3, "telephone_event_3_events"

    #@9f9
    aput-object v3, v2, v5

    #@9fb
    const-string v3, "0-15"

    #@9fd
    aput-object v3, v2, v6

    #@9ff
    aput-object v2, v0, v1

    #@a01
    const/16 v1, 0x3e

    #@a03
    new-array v2, v7, [Ljava/lang/String;

    #@a05
    const-string v3, "TEXT"

    #@a07
    aput-object v3, v2, v4

    #@a09
    const-string v3, "audiocodec_4_codec_type"

    #@a0b
    aput-object v3, v2, v5

    #@a0d
    const-string v3, "AMR"

    #@a0f
    aput-object v3, v2, v6

    #@a11
    aput-object v2, v0, v1

    #@a13
    const/16 v1, 0x3f

    #@a15
    new-array v2, v7, [Ljava/lang/String;

    #@a17
    const-string v3, "TEXT"

    #@a19
    aput-object v3, v2, v4

    #@a1b
    const-string v3, "audiocodec_4_network_type"

    #@a1d
    aput-object v3, v2, v5

    #@a1f
    const-string v3, "lte,hspa,3g"

    #@a21
    aput-object v3, v2, v6

    #@a23
    aput-object v2, v0, v1

    #@a25
    const/16 v1, 0x40

    #@a27
    new-array v2, v7, [Ljava/lang/String;

    #@a29
    const-string v3, "INTEGER"

    #@a2b
    aput-object v3, v2, v4

    #@a2d
    const-string v3, "audiocodec_4_payload_type"

    #@a2f
    aput-object v3, v2, v5

    #@a31
    const-string v3, "101"

    #@a33
    aput-object v3, v2, v6

    #@a35
    aput-object v2, v0, v1

    #@a37
    const/16 v1, 0x41

    #@a39
    new-array v2, v7, [Ljava/lang/String;

    #@a3b
    const-string v3, "INTEGER"

    #@a3d
    aput-object v3, v2, v4

    #@a3f
    const-string v3, "audiocodec_4_sampling_rate"

    #@a41
    aput-object v3, v2, v5

    #@a43
    const-string v3, "8000"

    #@a45
    aput-object v3, v2, v6

    #@a47
    aput-object v2, v0, v1

    #@a49
    const/16 v1, 0x42

    #@a4b
    new-array v2, v7, [Ljava/lang/String;

    #@a4d
    const-string v3, "INTEGER"

    #@a4f
    aput-object v3, v2, v4

    #@a51
    const-string v3, "AMR_4_channel"

    #@a53
    aput-object v3, v2, v5

    #@a55
    const-string v3, "1"

    #@a57
    aput-object v3, v2, v6

    #@a59
    aput-object v2, v0, v1

    #@a5b
    const/16 v1, 0x43

    #@a5d
    new-array v2, v7, [Ljava/lang/String;

    #@a5f
    const-string v3, "INTEGER"

    #@a61
    aput-object v3, v2, v4

    #@a63
    const-string v3, "AMR_4_octet_align"

    #@a65
    aput-object v3, v2, v5

    #@a67
    const-string v3, "1"

    #@a69
    aput-object v3, v2, v6

    #@a6b
    aput-object v2, v0, v1

    #@a6d
    const/16 v1, 0x44

    #@a6f
    new-array v2, v7, [Ljava/lang/String;

    #@a71
    const-string v3, "INTEGER"

    #@a73
    aput-object v3, v2, v4

    #@a75
    const-string v3, "AMR_4_mode_change_capability"

    #@a77
    aput-object v3, v2, v5

    #@a79
    const-string v3, "2"

    #@a7b
    aput-object v3, v2, v6

    #@a7d
    aput-object v2, v0, v1

    #@a7f
    const/16 v1, 0x45

    #@a81
    new-array v2, v7, [Ljava/lang/String;

    #@a83
    const-string v3, "TEXT"

    #@a85
    aput-object v3, v2, v4

    #@a87
    const-string v3, "AMR_4_mode_set"

    #@a89
    aput-object v3, v2, v5

    #@a8b
    const-string v3, ""

    #@a8d
    aput-object v3, v2, v6

    #@a8f
    aput-object v2, v0, v1

    #@a91
    const/16 v1, 0x46

    #@a93
    new-array v2, v7, [Ljava/lang/String;

    #@a95
    const-string v3, "INTEGER"

    #@a97
    aput-object v3, v2, v4

    #@a99
    const-string v3, "AMR_4_mode_change_period"

    #@a9b
    aput-object v3, v2, v5

    #@a9d
    const-string v3, ""

    #@a9f
    aput-object v3, v2, v6

    #@aa1
    aput-object v2, v0, v1

    #@aa3
    const/16 v1, 0x47

    #@aa5
    new-array v2, v7, [Ljava/lang/String;

    #@aa7
    const-string v3, "INTEGER"

    #@aa9
    aput-object v3, v2, v4

    #@aab
    const-string v3, "AMR_4_mode_change_neighbor"

    #@aad
    aput-object v3, v2, v5

    #@aaf
    const-string v3, ""

    #@ab1
    aput-object v3, v2, v6

    #@ab3
    aput-object v2, v0, v1

    #@ab5
    const/16 v1, 0x48

    #@ab7
    new-array v2, v7, [Ljava/lang/String;

    #@ab9
    const-string v3, "INTEGER"

    #@abb
    aput-object v3, v2, v4

    #@abd
    const-string v3, "AMR_4_max_red"

    #@abf
    aput-object v3, v2, v5

    #@ac1
    const-string v3, "220"

    #@ac3
    aput-object v3, v2, v6

    #@ac5
    aput-object v2, v0, v1

    #@ac7
    const/16 v1, 0x49

    #@ac9
    new-array v2, v7, [Ljava/lang/String;

    #@acb
    const-string v3, "INTEGER"

    #@acd
    aput-object v3, v2, v4

    #@acf
    const-string v3, "AMR_4_robust_sorting"

    #@ad1
    aput-object v3, v2, v5

    #@ad3
    const-string v3, ""

    #@ad5
    aput-object v3, v2, v6

    #@ad7
    aput-object v2, v0, v1

    #@ad9
    const/16 v1, 0x4a

    #@adb
    new-array v2, v7, [Ljava/lang/String;

    #@add
    const-string v3, "INTEGER"

    #@adf
    aput-object v3, v2, v4

    #@ae1
    const-string v3, "AMR_4_ptime"

    #@ae3
    aput-object v3, v2, v5

    #@ae5
    const-string v3, ""

    #@ae7
    aput-object v3, v2, v6

    #@ae9
    aput-object v2, v0, v1

    #@aeb
    const/16 v1, 0x4b

    #@aed
    new-array v2, v7, [Ljava/lang/String;

    #@aef
    const-string v3, "INTEGER"

    #@af1
    aput-object v3, v2, v4

    #@af3
    const-string v3, "AMR_4_max_ptime"

    #@af5
    aput-object v3, v2, v5

    #@af7
    const-string v3, ""

    #@af9
    aput-object v3, v2, v6

    #@afb
    aput-object v2, v0, v1

    #@afd
    const/16 v1, 0x4c

    #@aff
    new-array v2, v7, [Ljava/lang/String;

    #@b01
    const-string v3, "TEXT"

    #@b03
    aput-object v3, v2, v4

    #@b05
    const-string v3, "telephone_event_4_events"

    #@b07
    aput-object v3, v2, v5

    #@b09
    const-string v3, "0-15"

    #@b0b
    aput-object v3, v2, v6

    #@b0d
    aput-object v2, v0, v1

    #@b0f
    const/16 v1, 0x4d

    #@b11
    new-array v2, v7, [Ljava/lang/String;

    #@b13
    const-string v3, "TEXT"

    #@b15
    aput-object v3, v2, v4

    #@b17
    const-string v3, "audiocodec_5_codec_type"

    #@b19
    aput-object v3, v2, v5

    #@b1b
    const-string v3, "telephone-event"

    #@b1d
    aput-object v3, v2, v6

    #@b1f
    aput-object v2, v0, v1

    #@b21
    const/16 v1, 0x4e

    #@b23
    new-array v2, v7, [Ljava/lang/String;

    #@b25
    const-string v3, "TEXT"

    #@b27
    aput-object v3, v2, v4

    #@b29
    const-string v3, "audiocodec_5_network_type"

    #@b2b
    aput-object v3, v2, v5

    #@b2d
    const-string v3, "lte,hspa,3g"

    #@b2f
    aput-object v3, v2, v6

    #@b31
    aput-object v2, v0, v1

    #@b33
    const/16 v1, 0x4f

    #@b35
    new-array v2, v7, [Ljava/lang/String;

    #@b37
    const-string v3, "INTEGER"

    #@b39
    aput-object v3, v2, v4

    #@b3b
    const-string v3, "audiocodec_5_payload_type"

    #@b3d
    aput-object v3, v2, v5

    #@b3f
    const-string v3, "102"

    #@b41
    aput-object v3, v2, v6

    #@b43
    aput-object v2, v0, v1

    #@b45
    const/16 v1, 0x50

    #@b47
    new-array v2, v7, [Ljava/lang/String;

    #@b49
    const-string v3, "INTEGER"

    #@b4b
    aput-object v3, v2, v4

    #@b4d
    const-string v3, "audiocodec_5_sampling_rate"

    #@b4f
    aput-object v3, v2, v5

    #@b51
    const-string v3, "8000"

    #@b53
    aput-object v3, v2, v6

    #@b55
    aput-object v2, v0, v1

    #@b57
    const/16 v1, 0x51

    #@b59
    new-array v2, v7, [Ljava/lang/String;

    #@b5b
    const-string v3, "INTEGER"

    #@b5d
    aput-object v3, v2, v4

    #@b5f
    const-string v3, "AMR_5_channel"

    #@b61
    aput-object v3, v2, v5

    #@b63
    const-string v3, ""

    #@b65
    aput-object v3, v2, v6

    #@b67
    aput-object v2, v0, v1

    #@b69
    const/16 v1, 0x52

    #@b6b
    new-array v2, v7, [Ljava/lang/String;

    #@b6d
    const-string v3, "INTEGER"

    #@b6f
    aput-object v3, v2, v4

    #@b71
    const-string v3, "AMR_5_octet_align"

    #@b73
    aput-object v3, v2, v5

    #@b75
    const-string v3, ""

    #@b77
    aput-object v3, v2, v6

    #@b79
    aput-object v2, v0, v1

    #@b7b
    const/16 v1, 0x53

    #@b7d
    new-array v2, v7, [Ljava/lang/String;

    #@b7f
    const-string v3, "INTEGER"

    #@b81
    aput-object v3, v2, v4

    #@b83
    const-string v3, "AMR_5_mode_change_capability"

    #@b85
    aput-object v3, v2, v5

    #@b87
    const-string v3, ""

    #@b89
    aput-object v3, v2, v6

    #@b8b
    aput-object v2, v0, v1

    #@b8d
    const/16 v1, 0x54

    #@b8f
    new-array v2, v7, [Ljava/lang/String;

    #@b91
    const-string v3, "TEXT"

    #@b93
    aput-object v3, v2, v4

    #@b95
    const-string v3, "AMR_5_mode_set"

    #@b97
    aput-object v3, v2, v5

    #@b99
    const-string v3, ""

    #@b9b
    aput-object v3, v2, v6

    #@b9d
    aput-object v2, v0, v1

    #@b9f
    const/16 v1, 0x55

    #@ba1
    new-array v2, v7, [Ljava/lang/String;

    #@ba3
    const-string v3, "INTEGER"

    #@ba5
    aput-object v3, v2, v4

    #@ba7
    const-string v3, "AMR_5_mode_change_period"

    #@ba9
    aput-object v3, v2, v5

    #@bab
    const-string v3, ""

    #@bad
    aput-object v3, v2, v6

    #@baf
    aput-object v2, v0, v1

    #@bb1
    const/16 v1, 0x56

    #@bb3
    new-array v2, v7, [Ljava/lang/String;

    #@bb5
    const-string v3, "INTEGER"

    #@bb7
    aput-object v3, v2, v4

    #@bb9
    const-string v3, "AMR_5_mode_change_neighbor"

    #@bbb
    aput-object v3, v2, v5

    #@bbd
    const-string v3, ""

    #@bbf
    aput-object v3, v2, v6

    #@bc1
    aput-object v2, v0, v1

    #@bc3
    const/16 v1, 0x57

    #@bc5
    new-array v2, v7, [Ljava/lang/String;

    #@bc7
    const-string v3, "INTEGER"

    #@bc9
    aput-object v3, v2, v4

    #@bcb
    const-string v3, "AMR_5_max_red"

    #@bcd
    aput-object v3, v2, v5

    #@bcf
    const-string v3, ""

    #@bd1
    aput-object v3, v2, v6

    #@bd3
    aput-object v2, v0, v1

    #@bd5
    const/16 v1, 0x58

    #@bd7
    new-array v2, v7, [Ljava/lang/String;

    #@bd9
    const-string v3, "INTEGER"

    #@bdb
    aput-object v3, v2, v4

    #@bdd
    const-string v3, "AMR_5_robust_sorting"

    #@bdf
    aput-object v3, v2, v5

    #@be1
    const-string v3, ""

    #@be3
    aput-object v3, v2, v6

    #@be5
    aput-object v2, v0, v1

    #@be7
    const/16 v1, 0x59

    #@be9
    new-array v2, v7, [Ljava/lang/String;

    #@beb
    const-string v3, "INTEGER"

    #@bed
    aput-object v3, v2, v4

    #@bef
    const-string v3, "AMR_5_ptime"

    #@bf1
    aput-object v3, v2, v5

    #@bf3
    const-string v3, ""

    #@bf5
    aput-object v3, v2, v6

    #@bf7
    aput-object v2, v0, v1

    #@bf9
    const/16 v1, 0x5a

    #@bfb
    new-array v2, v7, [Ljava/lang/String;

    #@bfd
    const-string v3, "INTEGER"

    #@bff
    aput-object v3, v2, v4

    #@c01
    const-string v3, "AMR_5_max_ptime"

    #@c03
    aput-object v3, v2, v5

    #@c05
    const-string v3, ""

    #@c07
    aput-object v3, v2, v6

    #@c09
    aput-object v2, v0, v1

    #@c0b
    const/16 v1, 0x5b

    #@c0d
    new-array v2, v7, [Ljava/lang/String;

    #@c0f
    const-string v3, "TEXT"

    #@c11
    aput-object v3, v2, v4

    #@c13
    const-string v3, "telephone_event_5_events"

    #@c15
    aput-object v3, v2, v5

    #@c17
    const-string v3, "0-15"

    #@c19
    aput-object v3, v2, v6

    #@c1b
    aput-object v2, v0, v1

    #@c1d
    sput-object v0, Lcom/lge/ims/provider/uc/ConfigMedia;->MEDIA_AUDIO_CODEC_VOLTE:[[Ljava/lang/String;

    #@c1f
    .line 270
    const/16 v0, 0x5c

    #@c21
    new-array v0, v0, [[Ljava/lang/String;

    #@c23
    new-array v1, v7, [Ljava/lang/String;

    #@c25
    const-string v2, "INTEGER"

    #@c27
    aput-object v2, v1, v4

    #@c29
    const-string v2, "id"

    #@c2b
    aput-object v2, v1, v5

    #@c2d
    const-string v2, "1"

    #@c2f
    aput-object v2, v1, v6

    #@c31
    aput-object v1, v0, v4

    #@c33
    new-array v1, v7, [Ljava/lang/String;

    #@c35
    const-string v2, "INTEGER"

    #@c37
    aput-object v2, v1, v4

    #@c39
    const-string v2, "property"

    #@c3b
    aput-object v2, v1, v5

    #@c3d
    const-string v2, "1"

    #@c3f
    aput-object v2, v1, v6

    #@c41
    aput-object v1, v0, v5

    #@c43
    new-array v1, v7, [Ljava/lang/String;

    #@c45
    const-string v2, "TEXT"

    #@c47
    aput-object v2, v1, v4

    #@c49
    const-string v2, "audiocodec_0_codec_type"

    #@c4b
    aput-object v2, v1, v5

    #@c4d
    const-string v2, "AMR"

    #@c4f
    aput-object v2, v1, v6

    #@c51
    aput-object v1, v0, v6

    #@c53
    new-array v1, v7, [Ljava/lang/String;

    #@c55
    const-string v2, "TEXT"

    #@c57
    aput-object v2, v1, v4

    #@c59
    const-string v2, "audiocodec_0_network_type"

    #@c5b
    aput-object v2, v1, v5

    #@c5d
    const-string v2, "lte"

    #@c5f
    aput-object v2, v1, v6

    #@c61
    aput-object v1, v0, v7

    #@c63
    new-array v1, v7, [Ljava/lang/String;

    #@c65
    const-string v2, "INTEGER"

    #@c67
    aput-object v2, v1, v4

    #@c69
    const-string v2, "audiocodec_0_payload_type"

    #@c6b
    aput-object v2, v1, v5

    #@c6d
    const-string v2, "97"

    #@c6f
    aput-object v2, v1, v6

    #@c71
    aput-object v1, v0, v8

    #@c73
    const/4 v1, 0x5

    #@c74
    new-array v2, v7, [Ljava/lang/String;

    #@c76
    const-string v3, "INTEGER"

    #@c78
    aput-object v3, v2, v4

    #@c7a
    const-string v3, "audiocodec_0_sampling_rate"

    #@c7c
    aput-object v3, v2, v5

    #@c7e
    const-string v3, "16000"

    #@c80
    aput-object v3, v2, v6

    #@c82
    aput-object v2, v0, v1

    #@c84
    const/4 v1, 0x6

    #@c85
    new-array v2, v7, [Ljava/lang/String;

    #@c87
    const-string v3, "INTEGER"

    #@c89
    aput-object v3, v2, v4

    #@c8b
    const-string v3, "AMR_0_channel"

    #@c8d
    aput-object v3, v2, v5

    #@c8f
    const-string v3, "1"

    #@c91
    aput-object v3, v2, v6

    #@c93
    aput-object v2, v0, v1

    #@c95
    const/4 v1, 0x7

    #@c96
    new-array v2, v7, [Ljava/lang/String;

    #@c98
    const-string v3, "INTEGER"

    #@c9a
    aput-object v3, v2, v4

    #@c9c
    const-string v3, "AMR_0_octet_align"

    #@c9e
    aput-object v3, v2, v5

    #@ca0
    const-string v3, ""

    #@ca2
    aput-object v3, v2, v6

    #@ca4
    aput-object v2, v0, v1

    #@ca6
    const/16 v1, 0x8

    #@ca8
    new-array v2, v7, [Ljava/lang/String;

    #@caa
    const-string v3, "INTEGER"

    #@cac
    aput-object v3, v2, v4

    #@cae
    const-string v3, "AMR_0_mode_change_capability"

    #@cb0
    aput-object v3, v2, v5

    #@cb2
    const-string v3, "2"

    #@cb4
    aput-object v3, v2, v6

    #@cb6
    aput-object v2, v0, v1

    #@cb8
    const/16 v1, 0x9

    #@cba
    new-array v2, v7, [Ljava/lang/String;

    #@cbc
    const-string v3, "TEXT"

    #@cbe
    aput-object v3, v2, v4

    #@cc0
    const-string v3, "AMR_0_mode_set"

    #@cc2
    aput-object v3, v2, v5

    #@cc4
    const-string v3, ""

    #@cc6
    aput-object v3, v2, v6

    #@cc8
    aput-object v2, v0, v1

    #@cca
    const/16 v1, 0xa

    #@ccc
    new-array v2, v7, [Ljava/lang/String;

    #@cce
    const-string v3, "INTEGER"

    #@cd0
    aput-object v3, v2, v4

    #@cd2
    const-string v3, "AMR_0_mode_change_period"

    #@cd4
    aput-object v3, v2, v5

    #@cd6
    const-string v3, ""

    #@cd8
    aput-object v3, v2, v6

    #@cda
    aput-object v2, v0, v1

    #@cdc
    const/16 v1, 0xb

    #@cde
    new-array v2, v7, [Ljava/lang/String;

    #@ce0
    const-string v3, "INTEGER"

    #@ce2
    aput-object v3, v2, v4

    #@ce4
    const-string v3, "AMR_0_mode_change_neighbor"

    #@ce6
    aput-object v3, v2, v5

    #@ce8
    const-string v3, ""

    #@cea
    aput-object v3, v2, v6

    #@cec
    aput-object v2, v0, v1

    #@cee
    const/16 v1, 0xc

    #@cf0
    new-array v2, v7, [Ljava/lang/String;

    #@cf2
    const-string v3, "INTEGER"

    #@cf4
    aput-object v3, v2, v4

    #@cf6
    const-string v3, "AMR_0_max_red"

    #@cf8
    aput-object v3, v2, v5

    #@cfa
    const-string v3, "220"

    #@cfc
    aput-object v3, v2, v6

    #@cfe
    aput-object v2, v0, v1

    #@d00
    const/16 v1, 0xd

    #@d02
    new-array v2, v7, [Ljava/lang/String;

    #@d04
    const-string v3, "INTEGER"

    #@d06
    aput-object v3, v2, v4

    #@d08
    const-string v3, "AMR_0_robust_sorting"

    #@d0a
    aput-object v3, v2, v5

    #@d0c
    const-string v3, ""

    #@d0e
    aput-object v3, v2, v6

    #@d10
    aput-object v2, v0, v1

    #@d12
    const/16 v1, 0xe

    #@d14
    new-array v2, v7, [Ljava/lang/String;

    #@d16
    const-string v3, "INTEGER"

    #@d18
    aput-object v3, v2, v4

    #@d1a
    const-string v3, "AMR_0_ptime"

    #@d1c
    aput-object v3, v2, v5

    #@d1e
    const-string v3, ""

    #@d20
    aput-object v3, v2, v6

    #@d22
    aput-object v2, v0, v1

    #@d24
    const/16 v1, 0xf

    #@d26
    new-array v2, v7, [Ljava/lang/String;

    #@d28
    const-string v3, "INTEGER"

    #@d2a
    aput-object v3, v2, v4

    #@d2c
    const-string v3, "AMR_0_max_ptime"

    #@d2e
    aput-object v3, v2, v5

    #@d30
    const-string v3, ""

    #@d32
    aput-object v3, v2, v6

    #@d34
    aput-object v2, v0, v1

    #@d36
    const/16 v1, 0x10

    #@d38
    new-array v2, v7, [Ljava/lang/String;

    #@d3a
    const-string v3, "TEXT"

    #@d3c
    aput-object v3, v2, v4

    #@d3e
    const-string v3, "telephone_event_0_events"

    #@d40
    aput-object v3, v2, v5

    #@d42
    const-string v3, "0-15"

    #@d44
    aput-object v3, v2, v6

    #@d46
    aput-object v2, v0, v1

    #@d48
    const/16 v1, 0x11

    #@d4a
    new-array v2, v7, [Ljava/lang/String;

    #@d4c
    const-string v3, "TEXT"

    #@d4e
    aput-object v3, v2, v4

    #@d50
    const-string v3, "audiocodec_1_codec_type"

    #@d52
    aput-object v3, v2, v5

    #@d54
    const-string v3, "AMR"

    #@d56
    aput-object v3, v2, v6

    #@d58
    aput-object v2, v0, v1

    #@d5a
    const/16 v1, 0x12

    #@d5c
    new-array v2, v7, [Ljava/lang/String;

    #@d5e
    const-string v3, "TEXT"

    #@d60
    aput-object v3, v2, v4

    #@d62
    const-string v3, "audiocodec_1_network_type"

    #@d64
    aput-object v3, v2, v5

    #@d66
    const-string v3, "lte"

    #@d68
    aput-object v3, v2, v6

    #@d6a
    aput-object v2, v0, v1

    #@d6c
    const/16 v1, 0x13

    #@d6e
    new-array v2, v7, [Ljava/lang/String;

    #@d70
    const-string v3, "INTEGER"

    #@d72
    aput-object v3, v2, v4

    #@d74
    const-string v3, "audiocodec_1_payload_type"

    #@d76
    aput-object v3, v2, v5

    #@d78
    const-string v3, "98"

    #@d7a
    aput-object v3, v2, v6

    #@d7c
    aput-object v2, v0, v1

    #@d7e
    const/16 v1, 0x14

    #@d80
    new-array v2, v7, [Ljava/lang/String;

    #@d82
    const-string v3, "INTEGER"

    #@d84
    aput-object v3, v2, v4

    #@d86
    const-string v3, "audiocodec_1_sampling_rate"

    #@d88
    aput-object v3, v2, v5

    #@d8a
    const-string v3, "16000"

    #@d8c
    aput-object v3, v2, v6

    #@d8e
    aput-object v2, v0, v1

    #@d90
    const/16 v1, 0x15

    #@d92
    new-array v2, v7, [Ljava/lang/String;

    #@d94
    const-string v3, "INTEGER"

    #@d96
    aput-object v3, v2, v4

    #@d98
    const-string v3, "AMR_1_channel"

    #@d9a
    aput-object v3, v2, v5

    #@d9c
    const-string v3, "1"

    #@d9e
    aput-object v3, v2, v6

    #@da0
    aput-object v2, v0, v1

    #@da2
    const/16 v1, 0x16

    #@da4
    new-array v2, v7, [Ljava/lang/String;

    #@da6
    const-string v3, "INTEGER"

    #@da8
    aput-object v3, v2, v4

    #@daa
    const-string v3, "AMR_1_octet_align"

    #@dac
    aput-object v3, v2, v5

    #@dae
    const-string v3, "1"

    #@db0
    aput-object v3, v2, v6

    #@db2
    aput-object v2, v0, v1

    #@db4
    const/16 v1, 0x17

    #@db6
    new-array v2, v7, [Ljava/lang/String;

    #@db8
    const-string v3, "INTEGER"

    #@dba
    aput-object v3, v2, v4

    #@dbc
    const-string v3, "AMR_1_mode_change_capability"

    #@dbe
    aput-object v3, v2, v5

    #@dc0
    const-string v3, "2"

    #@dc2
    aput-object v3, v2, v6

    #@dc4
    aput-object v2, v0, v1

    #@dc6
    const/16 v1, 0x18

    #@dc8
    new-array v2, v7, [Ljava/lang/String;

    #@dca
    const-string v3, "TEXT"

    #@dcc
    aput-object v3, v2, v4

    #@dce
    const-string v3, "AMR_1_mode_set"

    #@dd0
    aput-object v3, v2, v5

    #@dd2
    const-string v3, ""

    #@dd4
    aput-object v3, v2, v6

    #@dd6
    aput-object v2, v0, v1

    #@dd8
    const/16 v1, 0x19

    #@dda
    new-array v2, v7, [Ljava/lang/String;

    #@ddc
    const-string v3, "INTEGER"

    #@dde
    aput-object v3, v2, v4

    #@de0
    const-string v3, "AMR_1_mode_change_period"

    #@de2
    aput-object v3, v2, v5

    #@de4
    const-string v3, ""

    #@de6
    aput-object v3, v2, v6

    #@de8
    aput-object v2, v0, v1

    #@dea
    const/16 v1, 0x1a

    #@dec
    new-array v2, v7, [Ljava/lang/String;

    #@dee
    const-string v3, "INTEGER"

    #@df0
    aput-object v3, v2, v4

    #@df2
    const-string v3, "AMR_1_mode_change_neighbor"

    #@df4
    aput-object v3, v2, v5

    #@df6
    const-string v3, ""

    #@df8
    aput-object v3, v2, v6

    #@dfa
    aput-object v2, v0, v1

    #@dfc
    const/16 v1, 0x1b

    #@dfe
    new-array v2, v7, [Ljava/lang/String;

    #@e00
    const-string v3, "INTEGER"

    #@e02
    aput-object v3, v2, v4

    #@e04
    const-string v3, "AMR_1_max_red"

    #@e06
    aput-object v3, v2, v5

    #@e08
    const-string v3, "220"

    #@e0a
    aput-object v3, v2, v6

    #@e0c
    aput-object v2, v0, v1

    #@e0e
    const/16 v1, 0x1c

    #@e10
    new-array v2, v7, [Ljava/lang/String;

    #@e12
    const-string v3, "INTEGER"

    #@e14
    aput-object v3, v2, v4

    #@e16
    const-string v3, "AMR_1_robust_sorting"

    #@e18
    aput-object v3, v2, v5

    #@e1a
    const-string v3, ""

    #@e1c
    aput-object v3, v2, v6

    #@e1e
    aput-object v2, v0, v1

    #@e20
    const/16 v1, 0x1d

    #@e22
    new-array v2, v7, [Ljava/lang/String;

    #@e24
    const-string v3, "INTEGER"

    #@e26
    aput-object v3, v2, v4

    #@e28
    const-string v3, "AMR_1_ptime"

    #@e2a
    aput-object v3, v2, v5

    #@e2c
    const-string v3, ""

    #@e2e
    aput-object v3, v2, v6

    #@e30
    aput-object v2, v0, v1

    #@e32
    const/16 v1, 0x1e

    #@e34
    new-array v2, v7, [Ljava/lang/String;

    #@e36
    const-string v3, "INTEGER"

    #@e38
    aput-object v3, v2, v4

    #@e3a
    const-string v3, "AMR_1_max_ptime"

    #@e3c
    aput-object v3, v2, v5

    #@e3e
    const-string v3, ""

    #@e40
    aput-object v3, v2, v6

    #@e42
    aput-object v2, v0, v1

    #@e44
    const/16 v1, 0x1f

    #@e46
    new-array v2, v7, [Ljava/lang/String;

    #@e48
    const-string v3, "TEXT"

    #@e4a
    aput-object v3, v2, v4

    #@e4c
    const-string v3, "telephone_event_1_events"

    #@e4e
    aput-object v3, v2, v5

    #@e50
    const-string v3, "0-15"

    #@e52
    aput-object v3, v2, v6

    #@e54
    aput-object v2, v0, v1

    #@e56
    const/16 v1, 0x20

    #@e58
    new-array v2, v7, [Ljava/lang/String;

    #@e5a
    const-string v3, "TEXT"

    #@e5c
    aput-object v3, v2, v4

    #@e5e
    const-string v3, "audiocodec_2_codec_type"

    #@e60
    aput-object v3, v2, v5

    #@e62
    const-string v3, "telephone-event"

    #@e64
    aput-object v3, v2, v6

    #@e66
    aput-object v2, v0, v1

    #@e68
    const/16 v1, 0x21

    #@e6a
    new-array v2, v7, [Ljava/lang/String;

    #@e6c
    const-string v3, "TEXT"

    #@e6e
    aput-object v3, v2, v4

    #@e70
    const-string v3, "audiocodec_2_network_type"

    #@e72
    aput-object v3, v2, v5

    #@e74
    const-string v3, "lte"

    #@e76
    aput-object v3, v2, v6

    #@e78
    aput-object v2, v0, v1

    #@e7a
    const/16 v1, 0x22

    #@e7c
    new-array v2, v7, [Ljava/lang/String;

    #@e7e
    const-string v3, "INTEGER"

    #@e80
    aput-object v3, v2, v4

    #@e82
    const-string v3, "audiocodec_2_payload_type"

    #@e84
    aput-object v3, v2, v5

    #@e86
    const-string v3, "99"

    #@e88
    aput-object v3, v2, v6

    #@e8a
    aput-object v2, v0, v1

    #@e8c
    const/16 v1, 0x23

    #@e8e
    new-array v2, v7, [Ljava/lang/String;

    #@e90
    const-string v3, "INTEGER"

    #@e92
    aput-object v3, v2, v4

    #@e94
    const-string v3, "audiocodec_2_sampling_rate"

    #@e96
    aput-object v3, v2, v5

    #@e98
    const-string v3, "16000"

    #@e9a
    aput-object v3, v2, v6

    #@e9c
    aput-object v2, v0, v1

    #@e9e
    const/16 v1, 0x24

    #@ea0
    new-array v2, v7, [Ljava/lang/String;

    #@ea2
    const-string v3, "INTEGER"

    #@ea4
    aput-object v3, v2, v4

    #@ea6
    const-string v3, "AMR_2_channel"

    #@ea8
    aput-object v3, v2, v5

    #@eaa
    const-string v3, ""

    #@eac
    aput-object v3, v2, v6

    #@eae
    aput-object v2, v0, v1

    #@eb0
    const/16 v1, 0x25

    #@eb2
    new-array v2, v7, [Ljava/lang/String;

    #@eb4
    const-string v3, "INTEGER"

    #@eb6
    aput-object v3, v2, v4

    #@eb8
    const-string v3, "AMR_2_octet_align"

    #@eba
    aput-object v3, v2, v5

    #@ebc
    const-string v3, ""

    #@ebe
    aput-object v3, v2, v6

    #@ec0
    aput-object v2, v0, v1

    #@ec2
    const/16 v1, 0x26

    #@ec4
    new-array v2, v7, [Ljava/lang/String;

    #@ec6
    const-string v3, "INTEGER"

    #@ec8
    aput-object v3, v2, v4

    #@eca
    const-string v3, "AMR_2_mode_change_capability"

    #@ecc
    aput-object v3, v2, v5

    #@ece
    const-string v3, ""

    #@ed0
    aput-object v3, v2, v6

    #@ed2
    aput-object v2, v0, v1

    #@ed4
    const/16 v1, 0x27

    #@ed6
    new-array v2, v7, [Ljava/lang/String;

    #@ed8
    const-string v3, "TEXT"

    #@eda
    aput-object v3, v2, v4

    #@edc
    const-string v3, "AMR_2_mode_set"

    #@ede
    aput-object v3, v2, v5

    #@ee0
    const-string v3, ""

    #@ee2
    aput-object v3, v2, v6

    #@ee4
    aput-object v2, v0, v1

    #@ee6
    const/16 v1, 0x28

    #@ee8
    new-array v2, v7, [Ljava/lang/String;

    #@eea
    const-string v3, "INTEGER"

    #@eec
    aput-object v3, v2, v4

    #@eee
    const-string v3, "AMR_2_mode_change_period"

    #@ef0
    aput-object v3, v2, v5

    #@ef2
    const-string v3, ""

    #@ef4
    aput-object v3, v2, v6

    #@ef6
    aput-object v2, v0, v1

    #@ef8
    const/16 v1, 0x29

    #@efa
    new-array v2, v7, [Ljava/lang/String;

    #@efc
    const-string v3, "INTEGER"

    #@efe
    aput-object v3, v2, v4

    #@f00
    const-string v3, "AMR_2_mode_change_neighbor"

    #@f02
    aput-object v3, v2, v5

    #@f04
    const-string v3, ""

    #@f06
    aput-object v3, v2, v6

    #@f08
    aput-object v2, v0, v1

    #@f0a
    const/16 v1, 0x2a

    #@f0c
    new-array v2, v7, [Ljava/lang/String;

    #@f0e
    const-string v3, "INTEGER"

    #@f10
    aput-object v3, v2, v4

    #@f12
    const-string v3, "AMR_2_max_red"

    #@f14
    aput-object v3, v2, v5

    #@f16
    const-string v3, ""

    #@f18
    aput-object v3, v2, v6

    #@f1a
    aput-object v2, v0, v1

    #@f1c
    const/16 v1, 0x2b

    #@f1e
    new-array v2, v7, [Ljava/lang/String;

    #@f20
    const-string v3, "INTEGER"

    #@f22
    aput-object v3, v2, v4

    #@f24
    const-string v3, "AMR_2_robust_sorting"

    #@f26
    aput-object v3, v2, v5

    #@f28
    const-string v3, ""

    #@f2a
    aput-object v3, v2, v6

    #@f2c
    aput-object v2, v0, v1

    #@f2e
    const/16 v1, 0x2c

    #@f30
    new-array v2, v7, [Ljava/lang/String;

    #@f32
    const-string v3, "INTEGER"

    #@f34
    aput-object v3, v2, v4

    #@f36
    const-string v3, "AMR_2_ptime"

    #@f38
    aput-object v3, v2, v5

    #@f3a
    const-string v3, ""

    #@f3c
    aput-object v3, v2, v6

    #@f3e
    aput-object v2, v0, v1

    #@f40
    const/16 v1, 0x2d

    #@f42
    new-array v2, v7, [Ljava/lang/String;

    #@f44
    const-string v3, "INTEGER"

    #@f46
    aput-object v3, v2, v4

    #@f48
    const-string v3, "AMR_2_max_ptime"

    #@f4a
    aput-object v3, v2, v5

    #@f4c
    const-string v3, ""

    #@f4e
    aput-object v3, v2, v6

    #@f50
    aput-object v2, v0, v1

    #@f52
    const/16 v1, 0x2e

    #@f54
    new-array v2, v7, [Ljava/lang/String;

    #@f56
    const-string v3, "TEXT"

    #@f58
    aput-object v3, v2, v4

    #@f5a
    const-string v3, "telephone_event_2_events"

    #@f5c
    aput-object v3, v2, v5

    #@f5e
    const-string v3, "0-15"

    #@f60
    aput-object v3, v2, v6

    #@f62
    aput-object v2, v0, v1

    #@f64
    const/16 v1, 0x2f

    #@f66
    new-array v2, v7, [Ljava/lang/String;

    #@f68
    const-string v3, "TEXT"

    #@f6a
    aput-object v3, v2, v4

    #@f6c
    const-string v3, "audiocodec_3_codec_type"

    #@f6e
    aput-object v3, v2, v5

    #@f70
    const-string v3, "AMR"

    #@f72
    aput-object v3, v2, v6

    #@f74
    aput-object v2, v0, v1

    #@f76
    const/16 v1, 0x30

    #@f78
    new-array v2, v7, [Ljava/lang/String;

    #@f7a
    const-string v3, "TEXT"

    #@f7c
    aput-object v3, v2, v4

    #@f7e
    const-string v3, "audiocodec_3_network_type"

    #@f80
    aput-object v3, v2, v5

    #@f82
    const-string v3, "lte,hspa,3g"

    #@f84
    aput-object v3, v2, v6

    #@f86
    aput-object v2, v0, v1

    #@f88
    const/16 v1, 0x31

    #@f8a
    new-array v2, v7, [Ljava/lang/String;

    #@f8c
    const-string v3, "INTEGER"

    #@f8e
    aput-object v3, v2, v4

    #@f90
    const-string v3, "audiocodec_3_payload_type"

    #@f92
    aput-object v3, v2, v5

    #@f94
    const-string v3, "100"

    #@f96
    aput-object v3, v2, v6

    #@f98
    aput-object v2, v0, v1

    #@f9a
    const/16 v1, 0x32

    #@f9c
    new-array v2, v7, [Ljava/lang/String;

    #@f9e
    const-string v3, "INTEGER"

    #@fa0
    aput-object v3, v2, v4

    #@fa2
    const-string v3, "audiocodec_3_sampling_rate"

    #@fa4
    aput-object v3, v2, v5

    #@fa6
    const-string v3, "8000"

    #@fa8
    aput-object v3, v2, v6

    #@faa
    aput-object v2, v0, v1

    #@fac
    const/16 v1, 0x33

    #@fae
    new-array v2, v7, [Ljava/lang/String;

    #@fb0
    const-string v3, "INTEGER"

    #@fb2
    aput-object v3, v2, v4

    #@fb4
    const-string v3, "AMR_3_channel"

    #@fb6
    aput-object v3, v2, v5

    #@fb8
    const-string v3, "1"

    #@fba
    aput-object v3, v2, v6

    #@fbc
    aput-object v2, v0, v1

    #@fbe
    const/16 v1, 0x34

    #@fc0
    new-array v2, v7, [Ljava/lang/String;

    #@fc2
    const-string v3, "INTEGER"

    #@fc4
    aput-object v3, v2, v4

    #@fc6
    const-string v3, "AMR_3_octet_align"

    #@fc8
    aput-object v3, v2, v5

    #@fca
    const-string v3, ""

    #@fcc
    aput-object v3, v2, v6

    #@fce
    aput-object v2, v0, v1

    #@fd0
    const/16 v1, 0x35

    #@fd2
    new-array v2, v7, [Ljava/lang/String;

    #@fd4
    const-string v3, "INTEGER"

    #@fd6
    aput-object v3, v2, v4

    #@fd8
    const-string v3, "AMR_3_mode_change_capability"

    #@fda
    aput-object v3, v2, v5

    #@fdc
    const-string v3, "2"

    #@fde
    aput-object v3, v2, v6

    #@fe0
    aput-object v2, v0, v1

    #@fe2
    const/16 v1, 0x36

    #@fe4
    new-array v2, v7, [Ljava/lang/String;

    #@fe6
    const-string v3, "TEXT"

    #@fe8
    aput-object v3, v2, v4

    #@fea
    const-string v3, "AMR_3_mode_set"

    #@fec
    aput-object v3, v2, v5

    #@fee
    const-string v3, ""

    #@ff0
    aput-object v3, v2, v6

    #@ff2
    aput-object v2, v0, v1

    #@ff4
    const/16 v1, 0x37

    #@ff6
    new-array v2, v7, [Ljava/lang/String;

    #@ff8
    const-string v3, "INTEGER"

    #@ffa
    aput-object v3, v2, v4

    #@ffc
    const-string v3, "AMR_3_mode_change_period"

    #@ffe
    aput-object v3, v2, v5

    #@1000
    const-string v3, ""

    #@1002
    aput-object v3, v2, v6

    #@1004
    aput-object v2, v0, v1

    #@1006
    const/16 v1, 0x38

    #@1008
    new-array v2, v7, [Ljava/lang/String;

    #@100a
    const-string v3, "INTEGER"

    #@100c
    aput-object v3, v2, v4

    #@100e
    const-string v3, "AMR_3_mode_change_neighbor"

    #@1010
    aput-object v3, v2, v5

    #@1012
    const-string v3, ""

    #@1014
    aput-object v3, v2, v6

    #@1016
    aput-object v2, v0, v1

    #@1018
    const/16 v1, 0x39

    #@101a
    new-array v2, v7, [Ljava/lang/String;

    #@101c
    const-string v3, "INTEGER"

    #@101e
    aput-object v3, v2, v4

    #@1020
    const-string v3, "AMR_3_max_red"

    #@1022
    aput-object v3, v2, v5

    #@1024
    const-string v3, "220"

    #@1026
    aput-object v3, v2, v6

    #@1028
    aput-object v2, v0, v1

    #@102a
    const/16 v1, 0x3a

    #@102c
    new-array v2, v7, [Ljava/lang/String;

    #@102e
    const-string v3, "INTEGER"

    #@1030
    aput-object v3, v2, v4

    #@1032
    const-string v3, "AMR_3_robust_sorting"

    #@1034
    aput-object v3, v2, v5

    #@1036
    const-string v3, ""

    #@1038
    aput-object v3, v2, v6

    #@103a
    aput-object v2, v0, v1

    #@103c
    const/16 v1, 0x3b

    #@103e
    new-array v2, v7, [Ljava/lang/String;

    #@1040
    const-string v3, "INTEGER"

    #@1042
    aput-object v3, v2, v4

    #@1044
    const-string v3, "AMR_3_ptime"

    #@1046
    aput-object v3, v2, v5

    #@1048
    const-string v3, ""

    #@104a
    aput-object v3, v2, v6

    #@104c
    aput-object v2, v0, v1

    #@104e
    const/16 v1, 0x3c

    #@1050
    new-array v2, v7, [Ljava/lang/String;

    #@1052
    const-string v3, "INTEGER"

    #@1054
    aput-object v3, v2, v4

    #@1056
    const-string v3, "AMR_3_max_ptime"

    #@1058
    aput-object v3, v2, v5

    #@105a
    const-string v3, ""

    #@105c
    aput-object v3, v2, v6

    #@105e
    aput-object v2, v0, v1

    #@1060
    const/16 v1, 0x3d

    #@1062
    new-array v2, v7, [Ljava/lang/String;

    #@1064
    const-string v3, "TEXT"

    #@1066
    aput-object v3, v2, v4

    #@1068
    const-string v3, "telephone_event_3_events"

    #@106a
    aput-object v3, v2, v5

    #@106c
    const-string v3, "0-15"

    #@106e
    aput-object v3, v2, v6

    #@1070
    aput-object v2, v0, v1

    #@1072
    const/16 v1, 0x3e

    #@1074
    new-array v2, v7, [Ljava/lang/String;

    #@1076
    const-string v3, "TEXT"

    #@1078
    aput-object v3, v2, v4

    #@107a
    const-string v3, "audiocodec_4_codec_type"

    #@107c
    aput-object v3, v2, v5

    #@107e
    const-string v3, "AMR"

    #@1080
    aput-object v3, v2, v6

    #@1082
    aput-object v2, v0, v1

    #@1084
    const/16 v1, 0x3f

    #@1086
    new-array v2, v7, [Ljava/lang/String;

    #@1088
    const-string v3, "TEXT"

    #@108a
    aput-object v3, v2, v4

    #@108c
    const-string v3, "audiocodec_4_network_type"

    #@108e
    aput-object v3, v2, v5

    #@1090
    const-string v3, "lte,hspa,3g"

    #@1092
    aput-object v3, v2, v6

    #@1094
    aput-object v2, v0, v1

    #@1096
    const/16 v1, 0x40

    #@1098
    new-array v2, v7, [Ljava/lang/String;

    #@109a
    const-string v3, "INTEGER"

    #@109c
    aput-object v3, v2, v4

    #@109e
    const-string v3, "audiocodec_4_payload_type"

    #@10a0
    aput-object v3, v2, v5

    #@10a2
    const-string v3, "101"

    #@10a4
    aput-object v3, v2, v6

    #@10a6
    aput-object v2, v0, v1

    #@10a8
    const/16 v1, 0x41

    #@10aa
    new-array v2, v7, [Ljava/lang/String;

    #@10ac
    const-string v3, "INTEGER"

    #@10ae
    aput-object v3, v2, v4

    #@10b0
    const-string v3, "audiocodec_4_sampling_rate"

    #@10b2
    aput-object v3, v2, v5

    #@10b4
    const-string v3, "8000"

    #@10b6
    aput-object v3, v2, v6

    #@10b8
    aput-object v2, v0, v1

    #@10ba
    const/16 v1, 0x42

    #@10bc
    new-array v2, v7, [Ljava/lang/String;

    #@10be
    const-string v3, "INTEGER"

    #@10c0
    aput-object v3, v2, v4

    #@10c2
    const-string v3, "AMR_4_channel"

    #@10c4
    aput-object v3, v2, v5

    #@10c6
    const-string v3, "1"

    #@10c8
    aput-object v3, v2, v6

    #@10ca
    aput-object v2, v0, v1

    #@10cc
    const/16 v1, 0x43

    #@10ce
    new-array v2, v7, [Ljava/lang/String;

    #@10d0
    const-string v3, "INTEGER"

    #@10d2
    aput-object v3, v2, v4

    #@10d4
    const-string v3, "AMR_4_octet_align"

    #@10d6
    aput-object v3, v2, v5

    #@10d8
    const-string v3, "1"

    #@10da
    aput-object v3, v2, v6

    #@10dc
    aput-object v2, v0, v1

    #@10de
    const/16 v1, 0x44

    #@10e0
    new-array v2, v7, [Ljava/lang/String;

    #@10e2
    const-string v3, "INTEGER"

    #@10e4
    aput-object v3, v2, v4

    #@10e6
    const-string v3, "AMR_4_mode_change_capability"

    #@10e8
    aput-object v3, v2, v5

    #@10ea
    const-string v3, "2"

    #@10ec
    aput-object v3, v2, v6

    #@10ee
    aput-object v2, v0, v1

    #@10f0
    const/16 v1, 0x45

    #@10f2
    new-array v2, v7, [Ljava/lang/String;

    #@10f4
    const-string v3, "TEXT"

    #@10f6
    aput-object v3, v2, v4

    #@10f8
    const-string v3, "AMR_4_mode_set"

    #@10fa
    aput-object v3, v2, v5

    #@10fc
    const-string v3, ""

    #@10fe
    aput-object v3, v2, v6

    #@1100
    aput-object v2, v0, v1

    #@1102
    const/16 v1, 0x46

    #@1104
    new-array v2, v7, [Ljava/lang/String;

    #@1106
    const-string v3, "INTEGER"

    #@1108
    aput-object v3, v2, v4

    #@110a
    const-string v3, "AMR_4_mode_change_period"

    #@110c
    aput-object v3, v2, v5

    #@110e
    const-string v3, ""

    #@1110
    aput-object v3, v2, v6

    #@1112
    aput-object v2, v0, v1

    #@1114
    const/16 v1, 0x47

    #@1116
    new-array v2, v7, [Ljava/lang/String;

    #@1118
    const-string v3, "INTEGER"

    #@111a
    aput-object v3, v2, v4

    #@111c
    const-string v3, "AMR_4_mode_change_neighbor"

    #@111e
    aput-object v3, v2, v5

    #@1120
    const-string v3, ""

    #@1122
    aput-object v3, v2, v6

    #@1124
    aput-object v2, v0, v1

    #@1126
    const/16 v1, 0x48

    #@1128
    new-array v2, v7, [Ljava/lang/String;

    #@112a
    const-string v3, "INTEGER"

    #@112c
    aput-object v3, v2, v4

    #@112e
    const-string v3, "AMR_4_max_red"

    #@1130
    aput-object v3, v2, v5

    #@1132
    const-string v3, "220"

    #@1134
    aput-object v3, v2, v6

    #@1136
    aput-object v2, v0, v1

    #@1138
    const/16 v1, 0x49

    #@113a
    new-array v2, v7, [Ljava/lang/String;

    #@113c
    const-string v3, "INTEGER"

    #@113e
    aput-object v3, v2, v4

    #@1140
    const-string v3, "AMR_4_robust_sorting"

    #@1142
    aput-object v3, v2, v5

    #@1144
    const-string v3, ""

    #@1146
    aput-object v3, v2, v6

    #@1148
    aput-object v2, v0, v1

    #@114a
    const/16 v1, 0x4a

    #@114c
    new-array v2, v7, [Ljava/lang/String;

    #@114e
    const-string v3, "INTEGER"

    #@1150
    aput-object v3, v2, v4

    #@1152
    const-string v3, "AMR_4_ptime"

    #@1154
    aput-object v3, v2, v5

    #@1156
    const-string v3, ""

    #@1158
    aput-object v3, v2, v6

    #@115a
    aput-object v2, v0, v1

    #@115c
    const/16 v1, 0x4b

    #@115e
    new-array v2, v7, [Ljava/lang/String;

    #@1160
    const-string v3, "INTEGER"

    #@1162
    aput-object v3, v2, v4

    #@1164
    const-string v3, "AMR_4_max_ptime"

    #@1166
    aput-object v3, v2, v5

    #@1168
    const-string v3, ""

    #@116a
    aput-object v3, v2, v6

    #@116c
    aput-object v2, v0, v1

    #@116e
    const/16 v1, 0x4c

    #@1170
    new-array v2, v7, [Ljava/lang/String;

    #@1172
    const-string v3, "TEXT"

    #@1174
    aput-object v3, v2, v4

    #@1176
    const-string v3, "telephone_event_4_events"

    #@1178
    aput-object v3, v2, v5

    #@117a
    const-string v3, "0-15"

    #@117c
    aput-object v3, v2, v6

    #@117e
    aput-object v2, v0, v1

    #@1180
    const/16 v1, 0x4d

    #@1182
    new-array v2, v7, [Ljava/lang/String;

    #@1184
    const-string v3, "TEXT"

    #@1186
    aput-object v3, v2, v4

    #@1188
    const-string v3, "audiocodec_5_codec_type"

    #@118a
    aput-object v3, v2, v5

    #@118c
    const-string v3, "telephone-event"

    #@118e
    aput-object v3, v2, v6

    #@1190
    aput-object v2, v0, v1

    #@1192
    const/16 v1, 0x4e

    #@1194
    new-array v2, v7, [Ljava/lang/String;

    #@1196
    const-string v3, "TEXT"

    #@1198
    aput-object v3, v2, v4

    #@119a
    const-string v3, "audiocodec_5_network_type"

    #@119c
    aput-object v3, v2, v5

    #@119e
    const-string v3, "lte,hspa,3g"

    #@11a0
    aput-object v3, v2, v6

    #@11a2
    aput-object v2, v0, v1

    #@11a4
    const/16 v1, 0x4f

    #@11a6
    new-array v2, v7, [Ljava/lang/String;

    #@11a8
    const-string v3, "INTEGER"

    #@11aa
    aput-object v3, v2, v4

    #@11ac
    const-string v3, "audiocodec_5_payload_type"

    #@11ae
    aput-object v3, v2, v5

    #@11b0
    const-string v3, "102"

    #@11b2
    aput-object v3, v2, v6

    #@11b4
    aput-object v2, v0, v1

    #@11b6
    const/16 v1, 0x50

    #@11b8
    new-array v2, v7, [Ljava/lang/String;

    #@11ba
    const-string v3, "INTEGER"

    #@11bc
    aput-object v3, v2, v4

    #@11be
    const-string v3, "audiocodec_5_sampling_rate"

    #@11c0
    aput-object v3, v2, v5

    #@11c2
    const-string v3, "8000"

    #@11c4
    aput-object v3, v2, v6

    #@11c6
    aput-object v2, v0, v1

    #@11c8
    const/16 v1, 0x51

    #@11ca
    new-array v2, v7, [Ljava/lang/String;

    #@11cc
    const-string v3, "INTEGER"

    #@11ce
    aput-object v3, v2, v4

    #@11d0
    const-string v3, "AMR_5_channel"

    #@11d2
    aput-object v3, v2, v5

    #@11d4
    const-string v3, ""

    #@11d6
    aput-object v3, v2, v6

    #@11d8
    aput-object v2, v0, v1

    #@11da
    const/16 v1, 0x52

    #@11dc
    new-array v2, v7, [Ljava/lang/String;

    #@11de
    const-string v3, "INTEGER"

    #@11e0
    aput-object v3, v2, v4

    #@11e2
    const-string v3, "AMR_5_octet_align"

    #@11e4
    aput-object v3, v2, v5

    #@11e6
    const-string v3, ""

    #@11e8
    aput-object v3, v2, v6

    #@11ea
    aput-object v2, v0, v1

    #@11ec
    const/16 v1, 0x53

    #@11ee
    new-array v2, v7, [Ljava/lang/String;

    #@11f0
    const-string v3, "INTEGER"

    #@11f2
    aput-object v3, v2, v4

    #@11f4
    const-string v3, "AMR_5_mode_change_capability"

    #@11f6
    aput-object v3, v2, v5

    #@11f8
    const-string v3, ""

    #@11fa
    aput-object v3, v2, v6

    #@11fc
    aput-object v2, v0, v1

    #@11fe
    const/16 v1, 0x54

    #@1200
    new-array v2, v7, [Ljava/lang/String;

    #@1202
    const-string v3, "TEXT"

    #@1204
    aput-object v3, v2, v4

    #@1206
    const-string v3, "AMR_5_mode_set"

    #@1208
    aput-object v3, v2, v5

    #@120a
    const-string v3, ""

    #@120c
    aput-object v3, v2, v6

    #@120e
    aput-object v2, v0, v1

    #@1210
    const/16 v1, 0x55

    #@1212
    new-array v2, v7, [Ljava/lang/String;

    #@1214
    const-string v3, "INTEGER"

    #@1216
    aput-object v3, v2, v4

    #@1218
    const-string v3, "AMR_5_mode_change_period"

    #@121a
    aput-object v3, v2, v5

    #@121c
    const-string v3, ""

    #@121e
    aput-object v3, v2, v6

    #@1220
    aput-object v2, v0, v1

    #@1222
    const/16 v1, 0x56

    #@1224
    new-array v2, v7, [Ljava/lang/String;

    #@1226
    const-string v3, "INTEGER"

    #@1228
    aput-object v3, v2, v4

    #@122a
    const-string v3, "AMR_5_mode_change_neighbor"

    #@122c
    aput-object v3, v2, v5

    #@122e
    const-string v3, ""

    #@1230
    aput-object v3, v2, v6

    #@1232
    aput-object v2, v0, v1

    #@1234
    const/16 v1, 0x57

    #@1236
    new-array v2, v7, [Ljava/lang/String;

    #@1238
    const-string v3, "INTEGER"

    #@123a
    aput-object v3, v2, v4

    #@123c
    const-string v3, "AMR_5_max_red"

    #@123e
    aput-object v3, v2, v5

    #@1240
    const-string v3, ""

    #@1242
    aput-object v3, v2, v6

    #@1244
    aput-object v2, v0, v1

    #@1246
    const/16 v1, 0x58

    #@1248
    new-array v2, v7, [Ljava/lang/String;

    #@124a
    const-string v3, "INTEGER"

    #@124c
    aput-object v3, v2, v4

    #@124e
    const-string v3, "AMR_5_robust_sorting"

    #@1250
    aput-object v3, v2, v5

    #@1252
    const-string v3, ""

    #@1254
    aput-object v3, v2, v6

    #@1256
    aput-object v2, v0, v1

    #@1258
    const/16 v1, 0x59

    #@125a
    new-array v2, v7, [Ljava/lang/String;

    #@125c
    const-string v3, "INTEGER"

    #@125e
    aput-object v3, v2, v4

    #@1260
    const-string v3, "AMR_5_ptime"

    #@1262
    aput-object v3, v2, v5

    #@1264
    const-string v3, ""

    #@1266
    aput-object v3, v2, v6

    #@1268
    aput-object v2, v0, v1

    #@126a
    const/16 v1, 0x5a

    #@126c
    new-array v2, v7, [Ljava/lang/String;

    #@126e
    const-string v3, "INTEGER"

    #@1270
    aput-object v3, v2, v4

    #@1272
    const-string v3, "AMR_5_max_ptime"

    #@1274
    aput-object v3, v2, v5

    #@1276
    const-string v3, ""

    #@1278
    aput-object v3, v2, v6

    #@127a
    aput-object v2, v0, v1

    #@127c
    const/16 v1, 0x5b

    #@127e
    new-array v2, v7, [Ljava/lang/String;

    #@1280
    const-string v3, "TEXT"

    #@1282
    aput-object v3, v2, v4

    #@1284
    const-string v3, "telephone_event_5_events"

    #@1286
    aput-object v3, v2, v5

    #@1288
    const-string v3, "0-15"

    #@128a
    aput-object v3, v2, v6

    #@128c
    aput-object v2, v0, v1

    #@128e
    sput-object v0, Lcom/lge/ims/provider/uc/ConfigMedia;->MEDIA_AUDIO_CODEC_VT:[[Ljava/lang/String;

    #@1290
    .line 375
    const/16 v0, 0x74

    #@1292
    new-array v0, v0, [[Ljava/lang/String;

    #@1294
    new-array v1, v7, [Ljava/lang/String;

    #@1296
    const-string v2, "INTEGER"

    #@1298
    aput-object v2, v1, v4

    #@129a
    const-string v2, "id"

    #@129c
    aput-object v2, v1, v5

    #@129e
    const-string v2, "1"

    #@12a0
    aput-object v2, v1, v6

    #@12a2
    aput-object v1, v0, v4

    #@12a4
    new-array v1, v7, [Ljava/lang/String;

    #@12a6
    const-string v2, "INTEGER"

    #@12a8
    aput-object v2, v1, v4

    #@12aa
    const-string v2, "property"

    #@12ac
    aput-object v2, v1, v5

    #@12ae
    const-string v2, "1"

    #@12b0
    aput-object v2, v1, v6

    #@12b2
    aput-object v1, v0, v5

    #@12b4
    new-array v1, v7, [Ljava/lang/String;

    #@12b6
    const-string v2, "TEXT"

    #@12b8
    aput-object v2, v1, v4

    #@12ba
    const-string v2, "videocodec_0_codec_type"

    #@12bc
    aput-object v2, v1, v5

    #@12be
    const-string v2, "H264"

    #@12c0
    aput-object v2, v1, v6

    #@12c2
    aput-object v1, v0, v6

    #@12c4
    new-array v1, v7, [Ljava/lang/String;

    #@12c6
    const-string v2, "TEXT"

    #@12c8
    aput-object v2, v1, v4

    #@12ca
    const-string v2, "videocodec_0_network_type"

    #@12cc
    aput-object v2, v1, v5

    #@12ce
    const-string v2, "lte"

    #@12d0
    aput-object v2, v1, v6

    #@12d2
    aput-object v1, v0, v7

    #@12d4
    new-array v1, v7, [Ljava/lang/String;

    #@12d6
    const-string v2, "INTEGER"

    #@12d8
    aput-object v2, v1, v4

    #@12da
    const-string v2, "videocodec_0_payload_type"

    #@12dc
    aput-object v2, v1, v5

    #@12de
    const-string v2, "103"

    #@12e0
    aput-object v2, v1, v6

    #@12e2
    aput-object v1, v0, v8

    #@12e4
    const/4 v1, 0x5

    #@12e5
    new-array v2, v7, [Ljava/lang/String;

    #@12e7
    const-string v3, "INTEGER"

    #@12e9
    aput-object v3, v2, v4

    #@12eb
    const-string v3, "videocodec_0_resolution"

    #@12ed
    aput-object v3, v2, v5

    #@12ef
    const-string v3, "4"

    #@12f1
    aput-object v3, v2, v6

    #@12f3
    aput-object v2, v0, v1

    #@12f5
    const/4 v1, 0x6

    #@12f6
    new-array v2, v7, [Ljava/lang/String;

    #@12f8
    const-string v3, "INTEGER"

    #@12fa
    aput-object v3, v2, v4

    #@12fc
    const-string v3, "videocodec_0_framerate"

    #@12fe
    aput-object v3, v2, v5

    #@1300
    const-string v3, "15"

    #@1302
    aput-object v3, v2, v6

    #@1304
    aput-object v2, v0, v1

    #@1306
    const/4 v1, 0x7

    #@1307
    new-array v2, v7, [Ljava/lang/String;

    #@1309
    const-string v3, "INTEGER"

    #@130b
    aput-object v3, v2, v4

    #@130d
    const-string v3, "videocodec_0_bitrate"

    #@130f
    aput-object v3, v2, v5

    #@1311
    const-string v3, "512"

    #@1313
    aput-object v3, v2, v6

    #@1315
    aput-object v2, v0, v1

    #@1317
    const/16 v1, 0x8

    #@1319
    new-array v2, v7, [Ljava/lang/String;

    #@131b
    const-string v3, "INTEGER"

    #@131d
    aput-object v3, v2, v4

    #@131f
    const-string v3, "videocodec_0_AS"

    #@1321
    aput-object v3, v2, v5

    #@1323
    const-string v3, "768"

    #@1325
    aput-object v3, v2, v6

    #@1327
    aput-object v2, v0, v1

    #@1329
    const/16 v1, 0x9

    #@132b
    new-array v2, v7, [Ljava/lang/String;

    #@132d
    const-string v3, "TEXT"

    #@132f
    aput-object v3, v2, v4

    #@1331
    const-string v3, "videocodec_0_profile_level_id"

    #@1333
    aput-object v3, v2, v5

    #@1335
    const-string v3, "42C016"

    #@1337
    aput-object v3, v2, v6

    #@1339
    aput-object v2, v0, v1

    #@133b
    const/16 v1, 0xa

    #@133d
    new-array v2, v7, [Ljava/lang/String;

    #@133f
    const-string v3, "TEXT"

    #@1341
    aput-object v3, v2, v4

    #@1343
    const-string v3, "videocodec_0_framesize"

    #@1345
    aput-object v3, v2, v5

    #@1347
    const-string v3, "true"

    #@1349
    aput-object v3, v2, v6

    #@134b
    aput-object v2, v0, v1

    #@134d
    const/16 v1, 0xb

    #@134f
    new-array v2, v7, [Ljava/lang/String;

    #@1351
    const-string v3, "INTEGER"

    #@1353
    aput-object v3, v2, v4

    #@1355
    const-string v3, "H263_0_profile"

    #@1357
    aput-object v3, v2, v5

    #@1359
    const-string v3, "0"

    #@135b
    aput-object v3, v2, v6

    #@135d
    aput-object v2, v0, v1

    #@135f
    const/16 v1, 0xc

    #@1361
    new-array v2, v7, [Ljava/lang/String;

    #@1363
    const-string v3, "INTEGER"

    #@1365
    aput-object v3, v2, v4

    #@1367
    const-string v3, "H263_0_level"

    #@1369
    aput-object v3, v2, v5

    #@136b
    const-string v3, ""

    #@136d
    aput-object v3, v2, v6

    #@136f
    aput-object v2, v0, v1

    #@1371
    const/16 v1, 0xd

    #@1373
    new-array v2, v7, [Ljava/lang/String;

    #@1375
    const-string v3, "TEXT"

    #@1377
    aput-object v3, v2, v4

    #@1379
    const-string v3, "H263_0_QCIF"

    #@137b
    aput-object v3, v2, v5

    #@137d
    const-string v3, ""

    #@137f
    aput-object v3, v2, v6

    #@1381
    aput-object v2, v0, v1

    #@1383
    const/16 v1, 0xe

    #@1385
    new-array v2, v7, [Ljava/lang/String;

    #@1387
    const-string v3, "INTEGER"

    #@1389
    aput-object v3, v2, v4

    #@138b
    const-string v3, "H264_0_packetization_mode"

    #@138d
    aput-object v3, v2, v5

    #@138f
    const-string v3, "1"

    #@1391
    aput-object v3, v2, v6

    #@1393
    aput-object v2, v0, v1

    #@1395
    const/16 v1, 0xf

    #@1397
    new-array v2, v7, [Ljava/lang/String;

    #@1399
    const-string v3, "TEXT"

    #@139b
    aput-object v3, v2, v4

    #@139d
    const-string v3, "H264_0_sprop_parameter_sets"

    #@139f
    aput-object v3, v2, v5

    #@13a1
    const-string v3, "true"

    #@13a3
    aput-object v3, v2, v6

    #@13a5
    aput-object v2, v0, v1

    #@13a7
    const/16 v1, 0x10

    #@13a9
    new-array v2, v7, [Ljava/lang/String;

    #@13ab
    const-string v3, "INTEGER"

    #@13ad
    aput-object v3, v2, v4

    #@13af
    const-string v3, "H264_0_max_mbps"

    #@13b1
    aput-object v3, v2, v5

    #@13b3
    const-string v3, ""

    #@13b5
    aput-object v3, v2, v6

    #@13b7
    aput-object v2, v0, v1

    #@13b9
    const/16 v1, 0x11

    #@13bb
    new-array v2, v7, [Ljava/lang/String;

    #@13bd
    const-string v3, "INTEGER"

    #@13bf
    aput-object v3, v2, v4

    #@13c1
    const-string v3, "H264_0_max_fs"

    #@13c3
    aput-object v3, v2, v5

    #@13c5
    const-string v3, ""

    #@13c7
    aput-object v3, v2, v6

    #@13c9
    aput-object v2, v0, v1

    #@13cb
    const/16 v1, 0x12

    #@13cd
    new-array v2, v7, [Ljava/lang/String;

    #@13cf
    const-string v3, "INTEGER"

    #@13d1
    aput-object v3, v2, v4

    #@13d3
    const-string v3, "H264_0_max_cpb"

    #@13d5
    aput-object v3, v2, v5

    #@13d7
    const-string v3, ""

    #@13d9
    aput-object v3, v2, v6

    #@13db
    aput-object v2, v0, v1

    #@13dd
    const/16 v1, 0x13

    #@13df
    new-array v2, v7, [Ljava/lang/String;

    #@13e1
    const-string v3, "INTEGER"

    #@13e3
    aput-object v3, v2, v4

    #@13e5
    const-string v3, "H264_0_max_dpb"

    #@13e7
    aput-object v3, v2, v5

    #@13e9
    const-string v3, ""

    #@13eb
    aput-object v3, v2, v6

    #@13ed
    aput-object v2, v0, v1

    #@13ef
    const/16 v1, 0x14

    #@13f1
    new-array v2, v7, [Ljava/lang/String;

    #@13f3
    const-string v3, "INTEGER"

    #@13f5
    aput-object v3, v2, v4

    #@13f7
    const-string v3, "H264_0_max_br"

    #@13f9
    aput-object v3, v2, v5

    #@13fb
    const-string v3, ""

    #@13fd
    aput-object v3, v2, v6

    #@13ff
    aput-object v2, v0, v1

    #@1401
    const/16 v1, 0x15

    #@1403
    new-array v2, v7, [Ljava/lang/String;

    #@1405
    const-string v3, "TEXT"

    #@1407
    aput-object v3, v2, v4

    #@1409
    const-string v3, "videocodec_1_codec_type"

    #@140b
    aput-object v3, v2, v5

    #@140d
    const-string v3, "H264"

    #@140f
    aput-object v3, v2, v6

    #@1411
    aput-object v2, v0, v1

    #@1413
    const/16 v1, 0x16

    #@1415
    new-array v2, v7, [Ljava/lang/String;

    #@1417
    const-string v3, "TEXT"

    #@1419
    aput-object v3, v2, v4

    #@141b
    const-string v3, "videocodec_1_network_type"

    #@141d
    aput-object v3, v2, v5

    #@141f
    const-string v3, "lte"

    #@1421
    aput-object v3, v2, v6

    #@1423
    aput-object v2, v0, v1

    #@1425
    const/16 v1, 0x17

    #@1427
    new-array v2, v7, [Ljava/lang/String;

    #@1429
    const-string v3, "INTEGER"

    #@142b
    aput-object v3, v2, v4

    #@142d
    const-string v3, "videocodec_1_payload_type"

    #@142f
    aput-object v3, v2, v5

    #@1431
    const-string v3, "104"

    #@1433
    aput-object v3, v2, v6

    #@1435
    aput-object v2, v0, v1

    #@1437
    const/16 v1, 0x18

    #@1439
    new-array v2, v7, [Ljava/lang/String;

    #@143b
    const-string v3, "INTEGER"

    #@143d
    aput-object v3, v2, v4

    #@143f
    const-string v3, "videocodec_1_resolution"

    #@1441
    aput-object v3, v2, v5

    #@1443
    const-string v3, "1"

    #@1445
    aput-object v3, v2, v6

    #@1447
    aput-object v2, v0, v1

    #@1449
    const/16 v1, 0x19

    #@144b
    new-array v2, v7, [Ljava/lang/String;

    #@144d
    const-string v3, "INTEGER"

    #@144f
    aput-object v3, v2, v4

    #@1451
    const-string v3, "videocodec_1_framerate"

    #@1453
    aput-object v3, v2, v5

    #@1455
    const-string v3, "15"

    #@1457
    aput-object v3, v2, v6

    #@1459
    aput-object v2, v0, v1

    #@145b
    const/16 v1, 0x1a

    #@145d
    new-array v2, v7, [Ljava/lang/String;

    #@145f
    const-string v3, "INTEGER"

    #@1461
    aput-object v3, v2, v4

    #@1463
    const-string v3, "videocodec_1_bitrate"

    #@1465
    aput-object v3, v2, v5

    #@1467
    const-string v3, "384"

    #@1469
    aput-object v3, v2, v6

    #@146b
    aput-object v2, v0, v1

    #@146d
    const/16 v1, 0x1b

    #@146f
    new-array v2, v7, [Ljava/lang/String;

    #@1471
    const-string v3, "INTEGER"

    #@1473
    aput-object v3, v2, v4

    #@1475
    const-string v3, "videocodec_1_AS"

    #@1477
    aput-object v3, v2, v5

    #@1479
    const-string v3, "512"

    #@147b
    aput-object v3, v2, v6

    #@147d
    aput-object v2, v0, v1

    #@147f
    const/16 v1, 0x1c

    #@1481
    new-array v2, v7, [Ljava/lang/String;

    #@1483
    const-string v3, "TEXT"

    #@1485
    aput-object v3, v2, v4

    #@1487
    const-string v3, "videocodec_1_profile_level_id"

    #@1489
    aput-object v3, v2, v5

    #@148b
    const-string v3, "42C00D"

    #@148d
    aput-object v3, v2, v6

    #@148f
    aput-object v2, v0, v1

    #@1491
    const/16 v1, 0x1d

    #@1493
    new-array v2, v7, [Ljava/lang/String;

    #@1495
    const-string v3, "TEXT"

    #@1497
    aput-object v3, v2, v4

    #@1499
    const-string v3, "videocodec_1_framesize"

    #@149b
    aput-object v3, v2, v5

    #@149d
    const-string v3, "true"

    #@149f
    aput-object v3, v2, v6

    #@14a1
    aput-object v2, v0, v1

    #@14a3
    const/16 v1, 0x1e

    #@14a5
    new-array v2, v7, [Ljava/lang/String;

    #@14a7
    const-string v3, "INTEGER"

    #@14a9
    aput-object v3, v2, v4

    #@14ab
    const-string v3, "H263_1_profile"

    #@14ad
    aput-object v3, v2, v5

    #@14af
    const-string v3, "0"

    #@14b1
    aput-object v3, v2, v6

    #@14b3
    aput-object v2, v0, v1

    #@14b5
    const/16 v1, 0x1f

    #@14b7
    new-array v2, v7, [Ljava/lang/String;

    #@14b9
    const-string v3, "INTEGER"

    #@14bb
    aput-object v3, v2, v4

    #@14bd
    const-string v3, "H263_1_level"

    #@14bf
    aput-object v3, v2, v5

    #@14c1
    const-string v3, ""

    #@14c3
    aput-object v3, v2, v6

    #@14c5
    aput-object v2, v0, v1

    #@14c7
    const/16 v1, 0x20

    #@14c9
    new-array v2, v7, [Ljava/lang/String;

    #@14cb
    const-string v3, "TEXT"

    #@14cd
    aput-object v3, v2, v4

    #@14cf
    const-string v3, "H263_1_QCIF"

    #@14d1
    aput-object v3, v2, v5

    #@14d3
    const-string v3, ""

    #@14d5
    aput-object v3, v2, v6

    #@14d7
    aput-object v2, v0, v1

    #@14d9
    const/16 v1, 0x21

    #@14db
    new-array v2, v7, [Ljava/lang/String;

    #@14dd
    const-string v3, "INTEGER"

    #@14df
    aput-object v3, v2, v4

    #@14e1
    const-string v3, "H264_1_packetization_mode"

    #@14e3
    aput-object v3, v2, v5

    #@14e5
    const-string v3, "1"

    #@14e7
    aput-object v3, v2, v6

    #@14e9
    aput-object v2, v0, v1

    #@14eb
    const/16 v1, 0x22

    #@14ed
    new-array v2, v7, [Ljava/lang/String;

    #@14ef
    const-string v3, "TEXT"

    #@14f1
    aput-object v3, v2, v4

    #@14f3
    const-string v3, "H264_1_sprop_parameter_sets"

    #@14f5
    aput-object v3, v2, v5

    #@14f7
    const-string v3, "true"

    #@14f9
    aput-object v3, v2, v6

    #@14fb
    aput-object v2, v0, v1

    #@14fd
    const/16 v1, 0x23

    #@14ff
    new-array v2, v7, [Ljava/lang/String;

    #@1501
    const-string v3, "INTEGER"

    #@1503
    aput-object v3, v2, v4

    #@1505
    const-string v3, "H264_1_max_mbps"

    #@1507
    aput-object v3, v2, v5

    #@1509
    const-string v3, ""

    #@150b
    aput-object v3, v2, v6

    #@150d
    aput-object v2, v0, v1

    #@150f
    const/16 v1, 0x24

    #@1511
    new-array v2, v7, [Ljava/lang/String;

    #@1513
    const-string v3, "INTEGER"

    #@1515
    aput-object v3, v2, v4

    #@1517
    const-string v3, "H264_1_max_fs"

    #@1519
    aput-object v3, v2, v5

    #@151b
    const-string v3, ""

    #@151d
    aput-object v3, v2, v6

    #@151f
    aput-object v2, v0, v1

    #@1521
    const/16 v1, 0x25

    #@1523
    new-array v2, v7, [Ljava/lang/String;

    #@1525
    const-string v3, "INTEGER"

    #@1527
    aput-object v3, v2, v4

    #@1529
    const-string v3, "H264_1_max_cpb"

    #@152b
    aput-object v3, v2, v5

    #@152d
    const-string v3, ""

    #@152f
    aput-object v3, v2, v6

    #@1531
    aput-object v2, v0, v1

    #@1533
    const/16 v1, 0x26

    #@1535
    new-array v2, v7, [Ljava/lang/String;

    #@1537
    const-string v3, "INTEGER"

    #@1539
    aput-object v3, v2, v4

    #@153b
    const-string v3, "H264_1_max_dpb"

    #@153d
    aput-object v3, v2, v5

    #@153f
    const-string v3, ""

    #@1541
    aput-object v3, v2, v6

    #@1543
    aput-object v2, v0, v1

    #@1545
    const/16 v1, 0x27

    #@1547
    new-array v2, v7, [Ljava/lang/String;

    #@1549
    const-string v3, "INTEGER"

    #@154b
    aput-object v3, v2, v4

    #@154d
    const-string v3, "H264_1_max_br"

    #@154f
    aput-object v3, v2, v5

    #@1551
    const-string v3, ""

    #@1553
    aput-object v3, v2, v6

    #@1555
    aput-object v2, v0, v1

    #@1557
    const/16 v1, 0x28

    #@1559
    new-array v2, v7, [Ljava/lang/String;

    #@155b
    const-string v3, "TEXT"

    #@155d
    aput-object v3, v2, v4

    #@155f
    const-string v3, "videocodec_2_codec_type"

    #@1561
    aput-object v3, v2, v5

    #@1563
    const-string v3, "H264"

    #@1565
    aput-object v3, v2, v6

    #@1567
    aput-object v2, v0, v1

    #@1569
    const/16 v1, 0x29

    #@156b
    new-array v2, v7, [Ljava/lang/String;

    #@156d
    const-string v3, "TEXT"

    #@156f
    aput-object v3, v2, v4

    #@1571
    const-string v3, "videocodec_2_network_type"

    #@1573
    aput-object v3, v2, v5

    #@1575
    const-string v3, "lte"

    #@1577
    aput-object v3, v2, v6

    #@1579
    aput-object v2, v0, v1

    #@157b
    const/16 v1, 0x2a

    #@157d
    new-array v2, v7, [Ljava/lang/String;

    #@157f
    const-string v3, "INTEGER"

    #@1581
    aput-object v3, v2, v4

    #@1583
    const-string v3, "videocodec_2_payload_type"

    #@1585
    aput-object v3, v2, v5

    #@1587
    const-string v3, "105"

    #@1589
    aput-object v3, v2, v6

    #@158b
    aput-object v2, v0, v1

    #@158d
    const/16 v1, 0x2b

    #@158f
    new-array v2, v7, [Ljava/lang/String;

    #@1591
    const-string v3, "INTEGER"

    #@1593
    aput-object v3, v2, v4

    #@1595
    const-string v3, "videocodec_2_resolution"

    #@1597
    aput-object v3, v2, v5

    #@1599
    const-string v3, "0"

    #@159b
    aput-object v3, v2, v6

    #@159d
    aput-object v2, v0, v1

    #@159f
    const/16 v1, 0x2c

    #@15a1
    new-array v2, v7, [Ljava/lang/String;

    #@15a3
    const-string v3, "INTEGER"

    #@15a5
    aput-object v3, v2, v4

    #@15a7
    const-string v3, "videocodec_2_framerate"

    #@15a9
    aput-object v3, v2, v5

    #@15ab
    const-string v3, "10"

    #@15ad
    aput-object v3, v2, v6

    #@15af
    aput-object v2, v0, v1

    #@15b1
    const/16 v1, 0x2d

    #@15b3
    new-array v2, v7, [Ljava/lang/String;

    #@15b5
    const-string v3, "INTEGER"

    #@15b7
    aput-object v3, v2, v4

    #@15b9
    const-string v3, "videocodec_2_bitrate"

    #@15bb
    aput-object v3, v2, v5

    #@15bd
    const-string v3, "48"

    #@15bf
    aput-object v3, v2, v6

    #@15c1
    aput-object v2, v0, v1

    #@15c3
    const/16 v1, 0x2e

    #@15c5
    new-array v2, v7, [Ljava/lang/String;

    #@15c7
    const-string v3, "INTEGER"

    #@15c9
    aput-object v3, v2, v4

    #@15cb
    const-string v3, "videocodec_2_AS"

    #@15cd
    aput-object v3, v2, v5

    #@15cf
    const-string v3, "57"

    #@15d1
    aput-object v3, v2, v6

    #@15d3
    aput-object v2, v0, v1

    #@15d5
    const/16 v1, 0x2f

    #@15d7
    new-array v2, v7, [Ljava/lang/String;

    #@15d9
    const-string v3, "TEXT"

    #@15db
    aput-object v3, v2, v4

    #@15dd
    const-string v3, "videocodec_2_profile_level_id"

    #@15df
    aput-object v3, v2, v5

    #@15e1
    const-string v3, "42C00A"

    #@15e3
    aput-object v3, v2, v6

    #@15e5
    aput-object v2, v0, v1

    #@15e7
    const/16 v1, 0x30

    #@15e9
    new-array v2, v7, [Ljava/lang/String;

    #@15eb
    const-string v3, "TEXT"

    #@15ed
    aput-object v3, v2, v4

    #@15ef
    const-string v3, "videocodec_2_framesize"

    #@15f1
    aput-object v3, v2, v5

    #@15f3
    const-string v3, "true"

    #@15f5
    aput-object v3, v2, v6

    #@15f7
    aput-object v2, v0, v1

    #@15f9
    const/16 v1, 0x31

    #@15fb
    new-array v2, v7, [Ljava/lang/String;

    #@15fd
    const-string v3, "INTEGER"

    #@15ff
    aput-object v3, v2, v4

    #@1601
    const-string v3, "H263_2_profile"

    #@1603
    aput-object v3, v2, v5

    #@1605
    const-string v3, ""

    #@1607
    aput-object v3, v2, v6

    #@1609
    aput-object v2, v0, v1

    #@160b
    const/16 v1, 0x32

    #@160d
    new-array v2, v7, [Ljava/lang/String;

    #@160f
    const-string v3, "INTEGER"

    #@1611
    aput-object v3, v2, v4

    #@1613
    const-string v3, "H263_2_level"

    #@1615
    aput-object v3, v2, v5

    #@1617
    const-string v3, ""

    #@1619
    aput-object v3, v2, v6

    #@161b
    aput-object v2, v0, v1

    #@161d
    const/16 v1, 0x33

    #@161f
    new-array v2, v7, [Ljava/lang/String;

    #@1621
    const-string v3, "TEXT"

    #@1623
    aput-object v3, v2, v4

    #@1625
    const-string v3, "H263_2_QCIF"

    #@1627
    aput-object v3, v2, v5

    #@1629
    const-string v3, ""

    #@162b
    aput-object v3, v2, v6

    #@162d
    aput-object v2, v0, v1

    #@162f
    const/16 v1, 0x34

    #@1631
    new-array v2, v7, [Ljava/lang/String;

    #@1633
    const-string v3, "INTEGER"

    #@1635
    aput-object v3, v2, v4

    #@1637
    const-string v3, "H264_2_packetization_mode"

    #@1639
    aput-object v3, v2, v5

    #@163b
    const-string v3, "1"

    #@163d
    aput-object v3, v2, v6

    #@163f
    aput-object v2, v0, v1

    #@1641
    const/16 v1, 0x35

    #@1643
    new-array v2, v7, [Ljava/lang/String;

    #@1645
    const-string v3, "TEXT"

    #@1647
    aput-object v3, v2, v4

    #@1649
    const-string v3, "H264_2_sprop_parameter_sets"

    #@164b
    aput-object v3, v2, v5

    #@164d
    const-string v3, "true"

    #@164f
    aput-object v3, v2, v6

    #@1651
    aput-object v2, v0, v1

    #@1653
    const/16 v1, 0x36

    #@1655
    new-array v2, v7, [Ljava/lang/String;

    #@1657
    const-string v3, "INTEGER"

    #@1659
    aput-object v3, v2, v4

    #@165b
    const-string v3, "H264_2_max_mbps"

    #@165d
    aput-object v3, v2, v5

    #@165f
    const-string v3, ""

    #@1661
    aput-object v3, v2, v6

    #@1663
    aput-object v2, v0, v1

    #@1665
    const/16 v1, 0x37

    #@1667
    new-array v2, v7, [Ljava/lang/String;

    #@1669
    const-string v3, "INTEGER"

    #@166b
    aput-object v3, v2, v4

    #@166d
    const-string v3, "H264_2_max_fs"

    #@166f
    aput-object v3, v2, v5

    #@1671
    const-string v3, ""

    #@1673
    aput-object v3, v2, v6

    #@1675
    aput-object v2, v0, v1

    #@1677
    const/16 v1, 0x38

    #@1679
    new-array v2, v7, [Ljava/lang/String;

    #@167b
    const-string v3, "INTEGER"

    #@167d
    aput-object v3, v2, v4

    #@167f
    const-string v3, "H264_2_max_cpb"

    #@1681
    aput-object v3, v2, v5

    #@1683
    const-string v3, ""

    #@1685
    aput-object v3, v2, v6

    #@1687
    aput-object v2, v0, v1

    #@1689
    const/16 v1, 0x39

    #@168b
    new-array v2, v7, [Ljava/lang/String;

    #@168d
    const-string v3, "INTEGER"

    #@168f
    aput-object v3, v2, v4

    #@1691
    const-string v3, "H264_2_max_dpb"

    #@1693
    aput-object v3, v2, v5

    #@1695
    const-string v3, ""

    #@1697
    aput-object v3, v2, v6

    #@1699
    aput-object v2, v0, v1

    #@169b
    const/16 v1, 0x3a

    #@169d
    new-array v2, v7, [Ljava/lang/String;

    #@169f
    const-string v3, "INTEGER"

    #@16a1
    aput-object v3, v2, v4

    #@16a3
    const-string v3, "H264_2_max_br"

    #@16a5
    aput-object v3, v2, v5

    #@16a7
    const-string v3, ""

    #@16a9
    aput-object v3, v2, v6

    #@16ab
    aput-object v2, v0, v1

    #@16ad
    const/16 v1, 0x3b

    #@16af
    new-array v2, v7, [Ljava/lang/String;

    #@16b1
    const-string v3, "TEXT"

    #@16b3
    aput-object v3, v2, v4

    #@16b5
    const-string v3, "videocodec_3_codec_type"

    #@16b7
    aput-object v3, v2, v5

    #@16b9
    const-string v3, "H263"

    #@16bb
    aput-object v3, v2, v6

    #@16bd
    aput-object v2, v0, v1

    #@16bf
    const/16 v1, 0x3c

    #@16c1
    new-array v2, v7, [Ljava/lang/String;

    #@16c3
    const-string v3, "TEXT"

    #@16c5
    aput-object v3, v2, v4

    #@16c7
    const-string v3, "videocodec_3_network_type"

    #@16c9
    aput-object v3, v2, v5

    #@16cb
    const-string v3, "lte,hspa,3g"

    #@16cd
    aput-object v3, v2, v6

    #@16cf
    aput-object v2, v0, v1

    #@16d1
    const/16 v1, 0x3d

    #@16d3
    new-array v2, v7, [Ljava/lang/String;

    #@16d5
    const-string v3, "INTEGER"

    #@16d7
    aput-object v3, v2, v4

    #@16d9
    const-string v3, "videocodec_3_payload_type"

    #@16db
    aput-object v3, v2, v5

    #@16dd
    const-string v3, "34"

    #@16df
    aput-object v3, v2, v6

    #@16e1
    aput-object v2, v0, v1

    #@16e3
    const/16 v1, 0x3e

    #@16e5
    new-array v2, v7, [Ljava/lang/String;

    #@16e7
    const-string v3, "INTEGER"

    #@16e9
    aput-object v3, v2, v4

    #@16eb
    const-string v3, "videocodec_3_resolution"

    #@16ed
    aput-object v3, v2, v5

    #@16ef
    const-string v3, "0"

    #@16f1
    aput-object v3, v2, v6

    #@16f3
    aput-object v2, v0, v1

    #@16f5
    const/16 v1, 0x3f

    #@16f7
    new-array v2, v7, [Ljava/lang/String;

    #@16f9
    const-string v3, "INTEGER"

    #@16fb
    aput-object v3, v2, v4

    #@16fd
    const-string v3, "videocodec_3_framerate"

    #@16ff
    aput-object v3, v2, v5

    #@1701
    const-string v3, "10"

    #@1703
    aput-object v3, v2, v6

    #@1705
    aput-object v2, v0, v1

    #@1707
    const/16 v1, 0x40

    #@1709
    new-array v2, v7, [Ljava/lang/String;

    #@170b
    const-string v3, "INTEGER"

    #@170d
    aput-object v3, v2, v4

    #@170f
    const-string v3, "videocodec_3_bitrate"

    #@1711
    aput-object v3, v2, v5

    #@1713
    const-string v3, "48"

    #@1715
    aput-object v3, v2, v6

    #@1717
    aput-object v2, v0, v1

    #@1719
    const/16 v1, 0x41

    #@171b
    new-array v2, v7, [Ljava/lang/String;

    #@171d
    const-string v3, "INTEGER"

    #@171f
    aput-object v3, v2, v4

    #@1721
    const-string v3, "videocodec_3_AS"

    #@1723
    aput-object v3, v2, v5

    #@1725
    const-string v3, "57"

    #@1727
    aput-object v3, v2, v6

    #@1729
    aput-object v2, v0, v1

    #@172b
    const/16 v1, 0x42

    #@172d
    new-array v2, v7, [Ljava/lang/String;

    #@172f
    const-string v3, "TEXT"

    #@1731
    aput-object v3, v2, v4

    #@1733
    const-string v3, "videocodec_3_profile_level_id"

    #@1735
    aput-object v3, v2, v5

    #@1737
    const-string v3, ""

    #@1739
    aput-object v3, v2, v6

    #@173b
    aput-object v2, v0, v1

    #@173d
    const/16 v1, 0x43

    #@173f
    new-array v2, v7, [Ljava/lang/String;

    #@1741
    const-string v3, "TEXT"

    #@1743
    aput-object v3, v2, v4

    #@1745
    const-string v3, "videocodec_3_framesize"

    #@1747
    aput-object v3, v2, v5

    #@1749
    const-string v3, "true"

    #@174b
    aput-object v3, v2, v6

    #@174d
    aput-object v2, v0, v1

    #@174f
    const/16 v1, 0x44

    #@1751
    new-array v2, v7, [Ljava/lang/String;

    #@1753
    const-string v3, "INTEGER"

    #@1755
    aput-object v3, v2, v4

    #@1757
    const-string v3, "H263_3_profile"

    #@1759
    aput-object v3, v2, v5

    #@175b
    const-string v3, "0"

    #@175d
    aput-object v3, v2, v6

    #@175f
    aput-object v2, v0, v1

    #@1761
    const/16 v1, 0x45

    #@1763
    new-array v2, v7, [Ljava/lang/String;

    #@1765
    const-string v3, "INTEGER"

    #@1767
    aput-object v3, v2, v4

    #@1769
    const-string v3, "H263_3_level"

    #@176b
    aput-object v3, v2, v5

    #@176d
    const-string v3, "10"

    #@176f
    aput-object v3, v2, v6

    #@1771
    aput-object v2, v0, v1

    #@1773
    const/16 v1, 0x46

    #@1775
    new-array v2, v7, [Ljava/lang/String;

    #@1777
    const-string v3, "TEXT"

    #@1779
    aput-object v3, v2, v4

    #@177b
    const-string v3, "H263_3_QCIF"

    #@177d
    aput-object v3, v2, v5

    #@177f
    const-string v3, "true"

    #@1781
    aput-object v3, v2, v6

    #@1783
    aput-object v2, v0, v1

    #@1785
    const/16 v1, 0x47

    #@1787
    new-array v2, v7, [Ljava/lang/String;

    #@1789
    const-string v3, "INTEGER"

    #@178b
    aput-object v3, v2, v4

    #@178d
    const-string v3, "H264_3_packetization_mode"

    #@178f
    aput-object v3, v2, v5

    #@1791
    const-string v3, ""

    #@1793
    aput-object v3, v2, v6

    #@1795
    aput-object v2, v0, v1

    #@1797
    const/16 v1, 0x48

    #@1799
    new-array v2, v7, [Ljava/lang/String;

    #@179b
    const-string v3, "TEXT"

    #@179d
    aput-object v3, v2, v4

    #@179f
    const-string v3, "H264_3_sprop_parameter_sets"

    #@17a1
    aput-object v3, v2, v5

    #@17a3
    const-string v3, ""

    #@17a5
    aput-object v3, v2, v6

    #@17a7
    aput-object v2, v0, v1

    #@17a9
    const/16 v1, 0x49

    #@17ab
    new-array v2, v7, [Ljava/lang/String;

    #@17ad
    const-string v3, "INTEGER"

    #@17af
    aput-object v3, v2, v4

    #@17b1
    const-string v3, "H264_3_max_mbps"

    #@17b3
    aput-object v3, v2, v5

    #@17b5
    const-string v3, ""

    #@17b7
    aput-object v3, v2, v6

    #@17b9
    aput-object v2, v0, v1

    #@17bb
    const/16 v1, 0x4a

    #@17bd
    new-array v2, v7, [Ljava/lang/String;

    #@17bf
    const-string v3, "INTEGER"

    #@17c1
    aput-object v3, v2, v4

    #@17c3
    const-string v3, "H264_3_max_fs"

    #@17c5
    aput-object v3, v2, v5

    #@17c7
    const-string v3, ""

    #@17c9
    aput-object v3, v2, v6

    #@17cb
    aput-object v2, v0, v1

    #@17cd
    const/16 v1, 0x4b

    #@17cf
    new-array v2, v7, [Ljava/lang/String;

    #@17d1
    const-string v3, "INTEGER"

    #@17d3
    aput-object v3, v2, v4

    #@17d5
    const-string v3, "H264_3_max_cpb"

    #@17d7
    aput-object v3, v2, v5

    #@17d9
    const-string v3, ""

    #@17db
    aput-object v3, v2, v6

    #@17dd
    aput-object v2, v0, v1

    #@17df
    const/16 v1, 0x4c

    #@17e1
    new-array v2, v7, [Ljava/lang/String;

    #@17e3
    const-string v3, "INTEGER"

    #@17e5
    aput-object v3, v2, v4

    #@17e7
    const-string v3, "H264_3_max_dpb"

    #@17e9
    aput-object v3, v2, v5

    #@17eb
    const-string v3, ""

    #@17ed
    aput-object v3, v2, v6

    #@17ef
    aput-object v2, v0, v1

    #@17f1
    const/16 v1, 0x4d

    #@17f3
    new-array v2, v7, [Ljava/lang/String;

    #@17f5
    const-string v3, "INTEGER"

    #@17f7
    aput-object v3, v2, v4

    #@17f9
    const-string v3, "H264_3_max_br"

    #@17fb
    aput-object v3, v2, v5

    #@17fd
    const-string v3, ""

    #@17ff
    aput-object v3, v2, v6

    #@1801
    aput-object v2, v0, v1

    #@1803
    const/16 v1, 0x4e

    #@1805
    new-array v2, v7, [Ljava/lang/String;

    #@1807
    const-string v3, "TEXT"

    #@1809
    aput-object v3, v2, v4

    #@180b
    const-string v3, "videocodec_4_codec_type"

    #@180d
    aput-object v3, v2, v5

    #@180f
    const-string v3, "None"

    #@1811
    aput-object v3, v2, v6

    #@1813
    aput-object v2, v0, v1

    #@1815
    const/16 v1, 0x4f

    #@1817
    new-array v2, v7, [Ljava/lang/String;

    #@1819
    const-string v3, "TEXT"

    #@181b
    aput-object v3, v2, v4

    #@181d
    const-string v3, "videocodec_4_network_type"

    #@181f
    aput-object v3, v2, v5

    #@1821
    const-string v3, ""

    #@1823
    aput-object v3, v2, v6

    #@1825
    aput-object v2, v0, v1

    #@1827
    const/16 v1, 0x50

    #@1829
    new-array v2, v7, [Ljava/lang/String;

    #@182b
    const-string v3, "INTEGER"

    #@182d
    aput-object v3, v2, v4

    #@182f
    const-string v3, "videocodec_4_payload_type"

    #@1831
    aput-object v3, v2, v5

    #@1833
    const-string v3, ""

    #@1835
    aput-object v3, v2, v6

    #@1837
    aput-object v2, v0, v1

    #@1839
    const/16 v1, 0x51

    #@183b
    new-array v2, v7, [Ljava/lang/String;

    #@183d
    const-string v3, "INTEGER"

    #@183f
    aput-object v3, v2, v4

    #@1841
    const-string v3, "videocodec_4_resolution"

    #@1843
    aput-object v3, v2, v5

    #@1845
    const-string v3, ""

    #@1847
    aput-object v3, v2, v6

    #@1849
    aput-object v2, v0, v1

    #@184b
    const/16 v1, 0x52

    #@184d
    new-array v2, v7, [Ljava/lang/String;

    #@184f
    const-string v3, "INTEGER"

    #@1851
    aput-object v3, v2, v4

    #@1853
    const-string v3, "videocodec_4_framerate"

    #@1855
    aput-object v3, v2, v5

    #@1857
    const-string v3, ""

    #@1859
    aput-object v3, v2, v6

    #@185b
    aput-object v2, v0, v1

    #@185d
    const/16 v1, 0x53

    #@185f
    new-array v2, v7, [Ljava/lang/String;

    #@1861
    const-string v3, "INTEGER"

    #@1863
    aput-object v3, v2, v4

    #@1865
    const-string v3, "videocodec_4_bitrate"

    #@1867
    aput-object v3, v2, v5

    #@1869
    const-string v3, ""

    #@186b
    aput-object v3, v2, v6

    #@186d
    aput-object v2, v0, v1

    #@186f
    const/16 v1, 0x54

    #@1871
    new-array v2, v7, [Ljava/lang/String;

    #@1873
    const-string v3, "INTEGER"

    #@1875
    aput-object v3, v2, v4

    #@1877
    const-string v3, "videocodec_4_AS"

    #@1879
    aput-object v3, v2, v5

    #@187b
    const-string v3, ""

    #@187d
    aput-object v3, v2, v6

    #@187f
    aput-object v2, v0, v1

    #@1881
    const/16 v1, 0x55

    #@1883
    new-array v2, v7, [Ljava/lang/String;

    #@1885
    const-string v3, "TEXT"

    #@1887
    aput-object v3, v2, v4

    #@1889
    const-string v3, "videocodec_4_profile_level_id"

    #@188b
    aput-object v3, v2, v5

    #@188d
    const-string v3, ""

    #@188f
    aput-object v3, v2, v6

    #@1891
    aput-object v2, v0, v1

    #@1893
    const/16 v1, 0x56

    #@1895
    new-array v2, v7, [Ljava/lang/String;

    #@1897
    const-string v3, "TEXT"

    #@1899
    aput-object v3, v2, v4

    #@189b
    const-string v3, "videocodec_4_framesize"

    #@189d
    aput-object v3, v2, v5

    #@189f
    const-string v3, ""

    #@18a1
    aput-object v3, v2, v6

    #@18a3
    aput-object v2, v0, v1

    #@18a5
    const/16 v1, 0x57

    #@18a7
    new-array v2, v7, [Ljava/lang/String;

    #@18a9
    const-string v3, "INTEGER"

    #@18ab
    aput-object v3, v2, v4

    #@18ad
    const-string v3, "H263_4_profile"

    #@18af
    aput-object v3, v2, v5

    #@18b1
    const-string v3, ""

    #@18b3
    aput-object v3, v2, v6

    #@18b5
    aput-object v2, v0, v1

    #@18b7
    const/16 v1, 0x58

    #@18b9
    new-array v2, v7, [Ljava/lang/String;

    #@18bb
    const-string v3, "INTEGER"

    #@18bd
    aput-object v3, v2, v4

    #@18bf
    const-string v3, "H263_4_level"

    #@18c1
    aput-object v3, v2, v5

    #@18c3
    const-string v3, ""

    #@18c5
    aput-object v3, v2, v6

    #@18c7
    aput-object v2, v0, v1

    #@18c9
    const/16 v1, 0x59

    #@18cb
    new-array v2, v7, [Ljava/lang/String;

    #@18cd
    const-string v3, "TEXT"

    #@18cf
    aput-object v3, v2, v4

    #@18d1
    const-string v3, "H263_4_QCIF"

    #@18d3
    aput-object v3, v2, v5

    #@18d5
    const-string v3, "true"

    #@18d7
    aput-object v3, v2, v6

    #@18d9
    aput-object v2, v0, v1

    #@18db
    const/16 v1, 0x5a

    #@18dd
    new-array v2, v7, [Ljava/lang/String;

    #@18df
    const-string v3, "INTEGER"

    #@18e1
    aput-object v3, v2, v4

    #@18e3
    const-string v3, "H264_4_packetization_mode"

    #@18e5
    aput-object v3, v2, v5

    #@18e7
    const-string v3, ""

    #@18e9
    aput-object v3, v2, v6

    #@18eb
    aput-object v2, v0, v1

    #@18ed
    const/16 v1, 0x5b

    #@18ef
    new-array v2, v7, [Ljava/lang/String;

    #@18f1
    const-string v3, "TEXT"

    #@18f3
    aput-object v3, v2, v4

    #@18f5
    const-string v3, "H264_4_sprop_parameter_sets"

    #@18f7
    aput-object v3, v2, v5

    #@18f9
    const-string v3, ""

    #@18fb
    aput-object v3, v2, v6

    #@18fd
    aput-object v2, v0, v1

    #@18ff
    const/16 v1, 0x5c

    #@1901
    new-array v2, v7, [Ljava/lang/String;

    #@1903
    const-string v3, "INTEGER"

    #@1905
    aput-object v3, v2, v4

    #@1907
    const-string v3, "H264_4_max_mbps"

    #@1909
    aput-object v3, v2, v5

    #@190b
    const-string v3, ""

    #@190d
    aput-object v3, v2, v6

    #@190f
    aput-object v2, v0, v1

    #@1911
    const/16 v1, 0x5d

    #@1913
    new-array v2, v7, [Ljava/lang/String;

    #@1915
    const-string v3, "INTEGER"

    #@1917
    aput-object v3, v2, v4

    #@1919
    const-string v3, "H264_4_max_fs"

    #@191b
    aput-object v3, v2, v5

    #@191d
    const-string v3, ""

    #@191f
    aput-object v3, v2, v6

    #@1921
    aput-object v2, v0, v1

    #@1923
    const/16 v1, 0x5e

    #@1925
    new-array v2, v7, [Ljava/lang/String;

    #@1927
    const-string v3, "INTEGER"

    #@1929
    aput-object v3, v2, v4

    #@192b
    const-string v3, "H264_4_max_cpb"

    #@192d
    aput-object v3, v2, v5

    #@192f
    const-string v3, ""

    #@1931
    aput-object v3, v2, v6

    #@1933
    aput-object v2, v0, v1

    #@1935
    const/16 v1, 0x5f

    #@1937
    new-array v2, v7, [Ljava/lang/String;

    #@1939
    const-string v3, "INTEGER"

    #@193b
    aput-object v3, v2, v4

    #@193d
    const-string v3, "H264_4_max_dpb"

    #@193f
    aput-object v3, v2, v5

    #@1941
    const-string v3, ""

    #@1943
    aput-object v3, v2, v6

    #@1945
    aput-object v2, v0, v1

    #@1947
    const/16 v1, 0x60

    #@1949
    new-array v2, v7, [Ljava/lang/String;

    #@194b
    const-string v3, "INTEGER"

    #@194d
    aput-object v3, v2, v4

    #@194f
    const-string v3, "H264_4_max_br"

    #@1951
    aput-object v3, v2, v5

    #@1953
    const-string v3, ""

    #@1955
    aput-object v3, v2, v6

    #@1957
    aput-object v2, v0, v1

    #@1959
    const/16 v1, 0x61

    #@195b
    new-array v2, v7, [Ljava/lang/String;

    #@195d
    const-string v3, "TEXT"

    #@195f
    aput-object v3, v2, v4

    #@1961
    const-string v3, "videocodec_5_codec_type"

    #@1963
    aput-object v3, v2, v5

    #@1965
    const-string v3, "None"

    #@1967
    aput-object v3, v2, v6

    #@1969
    aput-object v2, v0, v1

    #@196b
    const/16 v1, 0x62

    #@196d
    new-array v2, v7, [Ljava/lang/String;

    #@196f
    const-string v3, "TEXT"

    #@1971
    aput-object v3, v2, v4

    #@1973
    const-string v3, "videocodec_5_network_type"

    #@1975
    aput-object v3, v2, v5

    #@1977
    const-string v3, ""

    #@1979
    aput-object v3, v2, v6

    #@197b
    aput-object v2, v0, v1

    #@197d
    const/16 v1, 0x63

    #@197f
    new-array v2, v7, [Ljava/lang/String;

    #@1981
    const-string v3, "INTEGER"

    #@1983
    aput-object v3, v2, v4

    #@1985
    const-string v3, "videocodec_5_payload_type"

    #@1987
    aput-object v3, v2, v5

    #@1989
    const-string v3, ""

    #@198b
    aput-object v3, v2, v6

    #@198d
    aput-object v2, v0, v1

    #@198f
    const/16 v1, 0x64

    #@1991
    new-array v2, v7, [Ljava/lang/String;

    #@1993
    const-string v3, "INTEGER"

    #@1995
    aput-object v3, v2, v4

    #@1997
    const-string v3, "videocodec_5_resolution"

    #@1999
    aput-object v3, v2, v5

    #@199b
    const-string v3, ""

    #@199d
    aput-object v3, v2, v6

    #@199f
    aput-object v2, v0, v1

    #@19a1
    const/16 v1, 0x65

    #@19a3
    new-array v2, v7, [Ljava/lang/String;

    #@19a5
    const-string v3, "INTEGER"

    #@19a7
    aput-object v3, v2, v4

    #@19a9
    const-string v3, "videocodec_5_framerate"

    #@19ab
    aput-object v3, v2, v5

    #@19ad
    const-string v3, ""

    #@19af
    aput-object v3, v2, v6

    #@19b1
    aput-object v2, v0, v1

    #@19b3
    const/16 v1, 0x66

    #@19b5
    new-array v2, v7, [Ljava/lang/String;

    #@19b7
    const-string v3, "INTEGER"

    #@19b9
    aput-object v3, v2, v4

    #@19bb
    const-string v3, "videocodec_5_bitrate"

    #@19bd
    aput-object v3, v2, v5

    #@19bf
    const-string v3, ""

    #@19c1
    aput-object v3, v2, v6

    #@19c3
    aput-object v2, v0, v1

    #@19c5
    const/16 v1, 0x67

    #@19c7
    new-array v2, v7, [Ljava/lang/String;

    #@19c9
    const-string v3, "INTEGER"

    #@19cb
    aput-object v3, v2, v4

    #@19cd
    const-string v3, "videocodec_5_AS"

    #@19cf
    aput-object v3, v2, v5

    #@19d1
    const-string v3, ""

    #@19d3
    aput-object v3, v2, v6

    #@19d5
    aput-object v2, v0, v1

    #@19d7
    const/16 v1, 0x68

    #@19d9
    new-array v2, v7, [Ljava/lang/String;

    #@19db
    const-string v3, "TEXT"

    #@19dd
    aput-object v3, v2, v4

    #@19df
    const-string v3, "videocodec_5_profile_level_id"

    #@19e1
    aput-object v3, v2, v5

    #@19e3
    const-string v3, ""

    #@19e5
    aput-object v3, v2, v6

    #@19e7
    aput-object v2, v0, v1

    #@19e9
    const/16 v1, 0x69

    #@19eb
    new-array v2, v7, [Ljava/lang/String;

    #@19ed
    const-string v3, "TEXT"

    #@19ef
    aput-object v3, v2, v4

    #@19f1
    const-string v3, "videocodec_5_framesize"

    #@19f3
    aput-object v3, v2, v5

    #@19f5
    const-string v3, ""

    #@19f7
    aput-object v3, v2, v6

    #@19f9
    aput-object v2, v0, v1

    #@19fb
    const/16 v1, 0x6a

    #@19fd
    new-array v2, v7, [Ljava/lang/String;

    #@19ff
    const-string v3, "INTEGER"

    #@1a01
    aput-object v3, v2, v4

    #@1a03
    const-string v3, "H263_5_profile"

    #@1a05
    aput-object v3, v2, v5

    #@1a07
    const-string v3, ""

    #@1a09
    aput-object v3, v2, v6

    #@1a0b
    aput-object v2, v0, v1

    #@1a0d
    const/16 v1, 0x6b

    #@1a0f
    new-array v2, v7, [Ljava/lang/String;

    #@1a11
    const-string v3, "INTEGER"

    #@1a13
    aput-object v3, v2, v4

    #@1a15
    const-string v3, "H263_5_level"

    #@1a17
    aput-object v3, v2, v5

    #@1a19
    const-string v3, ""

    #@1a1b
    aput-object v3, v2, v6

    #@1a1d
    aput-object v2, v0, v1

    #@1a1f
    const/16 v1, 0x6c

    #@1a21
    new-array v2, v7, [Ljava/lang/String;

    #@1a23
    const-string v3, "TEXT"

    #@1a25
    aput-object v3, v2, v4

    #@1a27
    const-string v3, "H263_5_QCIF"

    #@1a29
    aput-object v3, v2, v5

    #@1a2b
    const-string v3, ""

    #@1a2d
    aput-object v3, v2, v6

    #@1a2f
    aput-object v2, v0, v1

    #@1a31
    const/16 v1, 0x6d

    #@1a33
    new-array v2, v7, [Ljava/lang/String;

    #@1a35
    const-string v3, "INTEGER"

    #@1a37
    aput-object v3, v2, v4

    #@1a39
    const-string v3, "H264_5_packetization_mode"

    #@1a3b
    aput-object v3, v2, v5

    #@1a3d
    const-string v3, ""

    #@1a3f
    aput-object v3, v2, v6

    #@1a41
    aput-object v2, v0, v1

    #@1a43
    const/16 v1, 0x6e

    #@1a45
    new-array v2, v7, [Ljava/lang/String;

    #@1a47
    const-string v3, "TEXT"

    #@1a49
    aput-object v3, v2, v4

    #@1a4b
    const-string v3, "H264_5_sprop_parameter_sets"

    #@1a4d
    aput-object v3, v2, v5

    #@1a4f
    const-string v3, ""

    #@1a51
    aput-object v3, v2, v6

    #@1a53
    aput-object v2, v0, v1

    #@1a55
    const/16 v1, 0x6f

    #@1a57
    new-array v2, v7, [Ljava/lang/String;

    #@1a59
    const-string v3, "INTEGER"

    #@1a5b
    aput-object v3, v2, v4

    #@1a5d
    const-string v3, "H264_5_max_mbps"

    #@1a5f
    aput-object v3, v2, v5

    #@1a61
    const-string v3, ""

    #@1a63
    aput-object v3, v2, v6

    #@1a65
    aput-object v2, v0, v1

    #@1a67
    const/16 v1, 0x70

    #@1a69
    new-array v2, v7, [Ljava/lang/String;

    #@1a6b
    const-string v3, "INTEGER"

    #@1a6d
    aput-object v3, v2, v4

    #@1a6f
    const-string v3, "H264_5_max_fs"

    #@1a71
    aput-object v3, v2, v5

    #@1a73
    const-string v3, ""

    #@1a75
    aput-object v3, v2, v6

    #@1a77
    aput-object v2, v0, v1

    #@1a79
    const/16 v1, 0x71

    #@1a7b
    new-array v2, v7, [Ljava/lang/String;

    #@1a7d
    const-string v3, "INTEGER"

    #@1a7f
    aput-object v3, v2, v4

    #@1a81
    const-string v3, "H264_5_max_cpb"

    #@1a83
    aput-object v3, v2, v5

    #@1a85
    const-string v3, ""

    #@1a87
    aput-object v3, v2, v6

    #@1a89
    aput-object v2, v0, v1

    #@1a8b
    const/16 v1, 0x72

    #@1a8d
    new-array v2, v7, [Ljava/lang/String;

    #@1a8f
    const-string v3, "INTEGER"

    #@1a91
    aput-object v3, v2, v4

    #@1a93
    const-string v3, "H264_5_max_dpb"

    #@1a95
    aput-object v3, v2, v5

    #@1a97
    const-string v3, ""

    #@1a99
    aput-object v3, v2, v6

    #@1a9b
    aput-object v2, v0, v1

    #@1a9d
    const/16 v1, 0x73

    #@1a9f
    new-array v2, v7, [Ljava/lang/String;

    #@1aa1
    const-string v3, "INTEGER"

    #@1aa3
    aput-object v3, v2, v4

    #@1aa5
    const-string v3, "H264_5_max_br"

    #@1aa7
    aput-object v3, v2, v5

    #@1aa9
    const-string v3, ""

    #@1aab
    aput-object v3, v2, v6

    #@1aad
    aput-object v2, v0, v1

    #@1aaf
    sput-object v0, Lcom/lge/ims/provider/uc/ConfigMedia;->MEDIA_VIDEO_CODEC_VT:[[Ljava/lang/String;

    #@1ab1
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 18
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public getTableContent(Ljava/lang/String;)[[Ljava/lang/String;
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 513
    const-string v0, "lgims_com_media"

    #@2
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_b

    #@8
    .line 514
    sget-object v0, Lcom/lge/ims/provider/uc/ConfigMedia;->MEDIA:[[Ljava/lang/String;

    #@a
    .line 532
    :goto_a
    return-object v0

    #@b
    .line 516
    :cond_b
    const-string v0, "lgims_com_media_audio"

    #@d
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@10
    move-result v0

    #@11
    if-eqz v0, :cond_16

    #@13
    .line 517
    sget-object v0, Lcom/lge/ims/provider/uc/ConfigMedia;->MEDIA_AUDIO:[[Ljava/lang/String;

    #@15
    goto :goto_a

    #@16
    .line 519
    :cond_16
    const-string v0, "lgims_com_media_video"

    #@18
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1b
    move-result v0

    #@1c
    if-eqz v0, :cond_21

    #@1e
    .line 520
    sget-object v0, Lcom/lge/ims/provider/uc/ConfigMedia;->MEDIA_VIDEO:[[Ljava/lang/String;

    #@20
    goto :goto_a

    #@21
    .line 522
    :cond_21
    const-string v0, "lgims_com_media_audio_codec_volte"

    #@23
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@26
    move-result v0

    #@27
    if-eqz v0, :cond_2c

    #@29
    .line 523
    sget-object v0, Lcom/lge/ims/provider/uc/ConfigMedia;->MEDIA_AUDIO_CODEC_VOLTE:[[Ljava/lang/String;

    #@2b
    goto :goto_a

    #@2c
    .line 525
    :cond_2c
    const-string v0, "lgims_com_media_audio_codec_vt"

    #@2e
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@31
    move-result v0

    #@32
    if-eqz v0, :cond_37

    #@34
    .line 526
    sget-object v0, Lcom/lge/ims/provider/uc/ConfigMedia;->MEDIA_AUDIO_CODEC_VT:[[Ljava/lang/String;

    #@36
    goto :goto_a

    #@37
    .line 528
    :cond_37
    const-string v0, "lgims_com_media_video_codec_vt"

    #@39
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@3c
    move-result v0

    #@3d
    if-eqz v0, :cond_42

    #@3f
    .line 529
    sget-object v0, Lcom/lge/ims/provider/uc/ConfigMedia;->MEDIA_VIDEO_CODEC_VT:[[Ljava/lang/String;

    #@41
    goto :goto_a

    #@42
    .line 532
    :cond_42
    const/4 v0, 0x0

    #@43
    check-cast v0, [[Ljava/lang/String;

    #@45
    goto :goto_a
.end method

.method public getTables()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 505
    sget-object v0, Lcom/lge/ims/provider/uc/ConfigMedia;->TABLES:[Ljava/lang/String;

    #@2
    return-object v0
.end method
