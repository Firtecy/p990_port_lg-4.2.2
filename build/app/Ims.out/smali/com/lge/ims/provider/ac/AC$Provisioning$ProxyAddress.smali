.class public final Lcom/lge/ims/provider/ac/AC$Provisioning$ProxyAddress;
.super Ljava/lang/Object;
.source "AC.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/provider/ac/AC$Provisioning;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ProxyAddress"
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final ID:Ljava/lang/String; = "id"

.field public static final LBO_PCSCF_ADDRESS:Ljava/lang/String; = "lbo_pcscf_address"

.field public static final LBO_PCSCF_ADDRESS_TYPE:Ljava/lang/String; = "lbo_pcscf_address_type"

.field public static final SBC_ADDRESS:Ljava/lang/String; = "sbc_address"

.field public static final SBC_ADDRESS_TYPE:Ljava/lang/String; = "sbc_address_type"

.field public static final SBC_TLS_ADDRESS:Ljava/lang/String; = "sbc_tls_address"

.field public static final SBC_TLS_ADDRESS_TYPE:Ljava/lang/String; = "sbc_tls_address_type"

.field public static final TABLE_NAME:Ljava/lang/String; = "proxy_address"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 117
    const-string v0, "content://com.lge.ims.provider.ac_provisioning/proxy_address"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/lge/ims/provider/ac/AC$Provisioning$ProxyAddress;->CONTENT_URI:Landroid/net/Uri;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 112
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
