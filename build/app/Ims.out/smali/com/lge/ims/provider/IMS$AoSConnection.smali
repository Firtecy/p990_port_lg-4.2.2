.class public final Lcom/lge/ims/provider/IMS$AoSConnection;
.super Ljava/lang/Object;
.source "IMS.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/provider/IMS;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AoSConnection"
.end annotation


# static fields
.field public static final ACCESS_POLICY_0:Ljava/lang/String; = "aos_connection_0_access_policy"

.field public static final ACCESS_POLICY_1:Ljava/lang/String; = "aos_connection_1_access_policy"

.field public static final ACCESS_POLICY_2:Ljava/lang/String; = "aos_connection_2_access_policy"

.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final IP_VERSION_0:Ljava/lang/String; = "aos_connection_0_ip_version"

.field public static final IP_VERSION_1:Ljava/lang/String; = "aos_connection_1_ip_version"

.field public static final IP_VERSION_2:Ljava/lang/String; = "aos_connection_2_ip_version"

.field public static final PROFILE_NAME_0:Ljava/lang/String; = "aos_connection_0_profile_name"

.field public static final PROFILE_NAME_1:Ljava/lang/String; = "aos_connection_1_profile_name"

.field public static final PROFILE_NAME_2:Ljava/lang/String; = "aos_connection_2_profile_name"

.field public static final TABLE_NAME:Ljava/lang/String; = "lgims_aosconnection"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 129
    const-string v0, "content://com.lge.ims.provider.lgims/lgims_aosconnection"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/lge/ims/provider/IMS$AoSConnection;->CONTENT_URI:Landroid/net/Uri;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 124
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
