.class public Lcom/lge/ims/provider/Config;
.super Ljava/lang/Object;
.source "Config.java"

# interfaces
.implements Lcom/lge/ims/provider/IConfiguration;


# static fields
.field private static final AOS:[[Ljava/lang/String;

.field private static final AOSCONNECTION:[[Ljava/lang/String;

.field private static final AOSREG:[[Ljava/lang/String;

.field private static final CONFIG:[[Ljava/lang/String;

.field private static final ENGINE:[[Ljava/lang/String;

.field private static final ENGINE_DEBUG:[[Ljava/lang/String;

.field private static final MEDIA:[[Ljava/lang/String;

.field private static final NETPOLICY:[[Ljava/lang/String;

.field private static final SIP:[[Ljava/lang/String;

.field private static final SUBSCRIBER:[[Ljava/lang/String;

.field private static final SUBSCRIBER_AKA:[[Ljava/lang/String;

.field private static final TABLES:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 9

    #@0
    .prologue
    const/4 v8, 0x4

    #@1
    const/4 v7, 0x3

    #@2
    const/4 v6, 0x2

    #@3
    const/4 v5, 0x1

    #@4
    const/4 v4, 0x0

    #@5
    .line 20
    const/16 v0, 0x9

    #@7
    new-array v0, v0, [Ljava/lang/String;

    #@9
    const-string v1, "lgims_aos"

    #@b
    aput-object v1, v0, v4

    #@d
    const-string v1, "lgims_aosconnection"

    #@f
    aput-object v1, v0, v5

    #@11
    const-string v1, "lgims_aosreg"

    #@13
    aput-object v1, v0, v6

    #@15
    const-string v1, "lgims_config"

    #@17
    aput-object v1, v0, v7

    #@19
    const-string v1, "lgims_engine"

    #@1b
    aput-object v1, v0, v8

    #@1d
    const/4 v1, 0x5

    #@1e
    const-string v2, "lgims_media"

    #@20
    aput-object v2, v0, v1

    #@22
    const/4 v1, 0x6

    #@23
    const-string v2, "lgims_netpolicy"

    #@25
    aput-object v2, v0, v1

    #@27
    const/4 v1, 0x7

    #@28
    const-string v2, "lgims_sip"

    #@2a
    aput-object v2, v0, v1

    #@2c
    const/16 v1, 0x8

    #@2e
    const-string v2, "lgims_subscriber"

    #@30
    aput-object v2, v0, v1

    #@32
    sput-object v0, Lcom/lge/ims/provider/Config;->TABLES:[Ljava/lang/String;

    #@34
    .line 35
    new-array v0, v7, [[Ljava/lang/String;

    #@36
    new-array v1, v7, [Ljava/lang/String;

    #@38
    const-string v2, "INTEGER"

    #@3a
    aput-object v2, v1, v4

    #@3c
    const-string v2, "id"

    #@3e
    aput-object v2, v1, v5

    #@40
    const-string v2, "1"

    #@42
    aput-object v2, v1, v6

    #@44
    aput-object v1, v0, v4

    #@46
    new-array v1, v7, [Ljava/lang/String;

    #@48
    const-string v2, "INTEGER"

    #@4a
    aput-object v2, v1, v4

    #@4c
    const-string v2, "property"

    #@4e
    aput-object v2, v1, v5

    #@50
    const-string v2, "0"

    #@52
    aput-object v2, v1, v6

    #@54
    aput-object v1, v0, v5

    #@56
    new-array v1, v7, [Ljava/lang/String;

    #@58
    const-string v2, "TEXT"

    #@5a
    aput-object v2, v1, v4

    #@5c
    const-string v2, "conf"

    #@5e
    aput-object v2, v1, v5

    #@60
    const-string v2, "[Uniqueness]\naos_profile=2\n\n[aos_profile_0]\nid=aos_app_0\nconnection=aos_connection_0\nreg=aos_reg_0\nservice_count=8\napp_id_0=lgims.com.kt.app.uc\nservice_id_0=lgims.com.kt.service.uc\napp_id_1=lgims.com.kt.app.uc\nservice_id_1=lgims.com.kt.service.vt\napp_id_2=lgims.com.kt.app.rcse.cd\nservice_id_2=lgims.com.kt.service.rcse.cd\napp_id_3=lgims.com.kt.app.rcse.cs\nservice_id_3=lgims.com.kt.service.rcse.vs\napp_id_4=lgims.com.kt.app.rcse.cs\nservice_id_4=lgims.com.kt.service.rcse.is\napp_id_5=lgims.com.kt.app.rcse.ip\nservice_id_5=lgims.com.kt.service.rcse.ip\napp_id_6=lgims.com.kt.app.rcse.ip\nservice_id_6=lgims.com.kt.service.rcse.ipxdm\napp_id_7=lgims.com.kt.app.rcse.im\nservice_id_7=lgims.com.kt.service.rcse.im\n\n[aos_profile_1]\nid=aos_app_1\nconnection=aos_connection_1\nreg=aos_reg_1\nservice_count=6\napp_id_0=lgims.com.kt.app.rcse.cd\nservice_id_0=lgims.com.kt.service.rcse.cd.wifi\napp_id_1=lgims.com.kt.app.rcse.cs\nservice_id_1=lgims.com.kt.service.rcse.vs.wifi\napp_id_2=lgims.com.kt.app.rcse.cs\nservice_id_2=lgims.com.kt.service.rcse.is.wifi\napp_id_3=lgims.com.kt.app.rcse.ip\nservice_id_3=lgims.com.kt.service.rcse.ip.wifi\napp_id_4=lgims.com.kt.app.rcse.ip\nservice_id_4=lgims.com.kt.service.rcse.ipxdm.wifi\napp_id_5=lgims.com.kt.app.rcse.im\nservice_id_5=lgims.com.kt.service.rcse.im.wifi\n\n"

    #@62
    aput-object v2, v1, v6

    #@64
    aput-object v1, v0, v6

    #@66
    sput-object v0, Lcom/lge/ims/provider/Config;->AOS:[[Ljava/lang/String;

    #@68
    .line 88
    const/16 v0, 0xf

    #@6a
    new-array v0, v0, [[Ljava/lang/String;

    #@6c
    new-array v1, v7, [Ljava/lang/String;

    #@6e
    const-string v2, "INTEGER"

    #@70
    aput-object v2, v1, v4

    #@72
    const-string v2, "id"

    #@74
    aput-object v2, v1, v5

    #@76
    const-string v2, "1"

    #@78
    aput-object v2, v1, v6

    #@7a
    aput-object v1, v0, v4

    #@7c
    new-array v1, v7, [Ljava/lang/String;

    #@7e
    const-string v2, "INTEGER"

    #@80
    aput-object v2, v1, v4

    #@82
    const-string v2, "property"

    #@84
    aput-object v2, v1, v5

    #@86
    const-string v2, "1"

    #@88
    aput-object v2, v1, v6

    #@8a
    aput-object v1, v0, v5

    #@8c
    new-array v1, v7, [Ljava/lang/String;

    #@8e
    const-string v2, "INTEGER"

    #@90
    aput-object v2, v1, v4

    #@92
    const-string v2, "Uniqueness_aos_connection"

    #@94
    aput-object v2, v1, v5

    #@96
    const-string v2, "2"

    #@98
    aput-object v2, v1, v6

    #@9a
    aput-object v1, v0, v6

    #@9c
    new-array v1, v7, [Ljava/lang/String;

    #@9e
    const-string v2, "TEXT"

    #@a0
    aput-object v2, v1, v4

    #@a2
    const-string v2, "aos_connection_0_id"

    #@a4
    aput-object v2, v1, v5

    #@a6
    const-string v2, "aos_connection_0"

    #@a8
    aput-object v2, v1, v6

    #@aa
    aput-object v1, v0, v7

    #@ac
    new-array v1, v7, [Ljava/lang/String;

    #@ae
    const-string v2, "TEXT"

    #@b0
    aput-object v2, v1, v4

    #@b2
    const-string v2, "aos_connection_0_profile_name"

    #@b4
    aput-object v2, v1, v5

    #@b6
    const-string v2, "mobile_ims"

    #@b8
    aput-object v2, v1, v6

    #@ba
    aput-object v1, v0, v8

    #@bc
    const/4 v1, 0x5

    #@bd
    new-array v2, v7, [Ljava/lang/String;

    #@bf
    const-string v3, "INTEGER"

    #@c1
    aput-object v3, v2, v4

    #@c3
    const-string v3, "aos_connection_0_ip_version"

    #@c5
    aput-object v3, v2, v5

    #@c7
    const-string v3, "4"

    #@c9
    aput-object v3, v2, v6

    #@cb
    aput-object v2, v0, v1

    #@cd
    const/4 v1, 0x6

    #@ce
    new-array v2, v7, [Ljava/lang/String;

    #@d0
    const-string v3, "TEXT"

    #@d2
    aput-object v3, v2, v4

    #@d4
    const-string v3, "aos_connection_0_access_policy"

    #@d6
    aput-object v3, v2, v5

    #@d8
    const-string v3, "0x20200004"

    #@da
    aput-object v3, v2, v6

    #@dc
    aput-object v2, v0, v1

    #@de
    const/4 v1, 0x7

    #@df
    new-array v2, v7, [Ljava/lang/String;

    #@e1
    const-string v3, "INTEGER"

    #@e3
    aput-object v3, v2, v4

    #@e5
    const-string v3, "aos_connection_0_service_in_time"

    #@e7
    aput-object v3, v2, v5

    #@e9
    const-string v3, "2000"

    #@eb
    aput-object v3, v2, v6

    #@ed
    aput-object v2, v0, v1

    #@ef
    const/16 v1, 0x8

    #@f1
    new-array v2, v7, [Ljava/lang/String;

    #@f3
    const-string v3, "INTEGER"

    #@f5
    aput-object v3, v2, v4

    #@f7
    const-string v3, "aos_connection_0_service_out_time"

    #@f9
    aput-object v3, v2, v5

    #@fb
    const-string v3, "1000"

    #@fd
    aput-object v3, v2, v6

    #@ff
    aput-object v2, v0, v1

    #@101
    const/16 v1, 0x9

    #@103
    new-array v2, v7, [Ljava/lang/String;

    #@105
    const-string v3, "TEXT"

    #@107
    aput-object v3, v2, v4

    #@109
    const-string v3, "aos_connection_1_id"

    #@10b
    aput-object v3, v2, v5

    #@10d
    const-string v3, "aos_connection_1"

    #@10f
    aput-object v3, v2, v6

    #@111
    aput-object v2, v0, v1

    #@113
    const/16 v1, 0xa

    #@115
    new-array v2, v7, [Ljava/lang/String;

    #@117
    const-string v3, "TEXT"

    #@119
    aput-object v3, v2, v4

    #@11b
    const-string v3, "aos_connection_1_profile_name"

    #@11d
    aput-object v3, v2, v5

    #@11f
    const-string v3, "wifi"

    #@121
    aput-object v3, v2, v6

    #@123
    aput-object v2, v0, v1

    #@125
    const/16 v1, 0xb

    #@127
    new-array v2, v7, [Ljava/lang/String;

    #@129
    const-string v3, "INTEGER"

    #@12b
    aput-object v3, v2, v4

    #@12d
    const-string v3, "aos_connection_1_ip_version"

    #@12f
    aput-object v3, v2, v5

    #@131
    const-string v3, "4"

    #@133
    aput-object v3, v2, v6

    #@135
    aput-object v2, v0, v1

    #@137
    const/16 v1, 0xc

    #@139
    new-array v2, v7, [Ljava/lang/String;

    #@13b
    const-string v3, "TEXT"

    #@13d
    aput-object v3, v2, v4

    #@13f
    const-string v3, "aos_connection_1_access_policy"

    #@141
    aput-object v3, v2, v5

    #@143
    const-string v3, "0x0100FFFF"

    #@145
    aput-object v3, v2, v6

    #@147
    aput-object v2, v0, v1

    #@149
    const/16 v1, 0xd

    #@14b
    new-array v2, v7, [Ljava/lang/String;

    #@14d
    const-string v3, "INTEGER"

    #@14f
    aput-object v3, v2, v4

    #@151
    const-string v3, "aos_connection_1_service_in_time"

    #@153
    aput-object v3, v2, v5

    #@155
    const-string v3, "500"

    #@157
    aput-object v3, v2, v6

    #@159
    aput-object v2, v0, v1

    #@15b
    const/16 v1, 0xe

    #@15d
    new-array v2, v7, [Ljava/lang/String;

    #@15f
    const-string v3, "INTEGER"

    #@161
    aput-object v3, v2, v4

    #@163
    const-string v3, "aos_connection_1_service_out_time"

    #@165
    aput-object v3, v2, v5

    #@167
    const-string v3, "500"

    #@169
    aput-object v3, v2, v6

    #@16b
    aput-object v2, v0, v1

    #@16d
    sput-object v0, Lcom/lge/ims/provider/Config;->AOSCONNECTION:[[Ljava/lang/String;

    #@16f
    .line 109
    const/16 v0, 0xf

    #@171
    new-array v0, v0, [[Ljava/lang/String;

    #@173
    new-array v1, v7, [Ljava/lang/String;

    #@175
    const-string v2, "INTEGER"

    #@177
    aput-object v2, v1, v4

    #@179
    const-string v2, "id"

    #@17b
    aput-object v2, v1, v5

    #@17d
    const-string v2, "1"

    #@17f
    aput-object v2, v1, v6

    #@181
    aput-object v1, v0, v4

    #@183
    new-array v1, v7, [Ljava/lang/String;

    #@185
    const-string v2, "INTEGER"

    #@187
    aput-object v2, v1, v4

    #@189
    const-string v2, "property"

    #@18b
    aput-object v2, v1, v5

    #@18d
    const-string v2, "1"

    #@18f
    aput-object v2, v1, v6

    #@191
    aput-object v1, v0, v5

    #@193
    new-array v1, v7, [Ljava/lang/String;

    #@195
    const-string v2, "INTEGER"

    #@197
    aput-object v2, v1, v4

    #@199
    const-string v2, "Uniqueness_aos_reg"

    #@19b
    aput-object v2, v1, v5

    #@19d
    const-string v2, "2"

    #@19f
    aput-object v2, v1, v6

    #@1a1
    aput-object v1, v0, v6

    #@1a3
    new-array v1, v7, [Ljava/lang/String;

    #@1a5
    const-string v2, "TEXT"

    #@1a7
    aput-object v2, v1, v4

    #@1a9
    const-string v2, "aos_reg_0_id"

    #@1ab
    aput-object v2, v1, v5

    #@1ad
    const-string v2, "aos_reg_0"

    #@1af
    aput-object v2, v1, v6

    #@1b1
    aput-object v1, v0, v7

    #@1b3
    new-array v1, v7, [Ljava/lang/String;

    #@1b5
    const-string v2, "TEXT"

    #@1b7
    aput-object v2, v1, v4

    #@1b9
    const-string v2, "aos_reg_0_type"

    #@1bb
    aput-object v2, v1, v5

    #@1bd
    const-string v2, "normal"

    #@1bf
    aput-object v2, v1, v6

    #@1c1
    aput-object v1, v0, v8

    #@1c3
    const/4 v1, 0x5

    #@1c4
    new-array v2, v7, [Ljava/lang/String;

    #@1c6
    const-string v3, "INTEGER"

    #@1c8
    aput-object v3, v2, v4

    #@1ca
    const-string v3, "aos_reg_0_flow_id"

    #@1cc
    aput-object v3, v2, v5

    #@1ce
    const-string v3, "1"

    #@1d0
    aput-object v3, v2, v6

    #@1d2
    aput-object v2, v0, v1

    #@1d4
    const/4 v1, 0x6

    #@1d5
    new-array v2, v7, [Ljava/lang/String;

    #@1d7
    const-string v3, "TEXT"

    #@1d9
    aput-object v3, v2, v4

    #@1db
    const-string v3, "aos_reg_0_retry_interval"

    #@1dd
    aput-object v3, v2, v5

    #@1df
    const-string v3, "180,300,600"

    #@1e1
    aput-object v3, v2, v6

    #@1e3
    aput-object v2, v0, v1

    #@1e5
    const/4 v1, 0x7

    #@1e6
    new-array v2, v7, [Ljava/lang/String;

    #@1e8
    const-string v3, "INTEGER"

    #@1ea
    aput-object v3, v2, v4

    #@1ec
    const-string v3, "aos_reg_0_retry_repeat_interval"

    #@1ee
    aput-object v3, v2, v5

    #@1f0
    const-string v3, "-1"

    #@1f2
    aput-object v3, v2, v6

    #@1f4
    aput-object v2, v0, v1

    #@1f6
    const/16 v1, 0x8

    #@1f8
    new-array v2, v7, [Ljava/lang/String;

    #@1fa
    const-string v3, "TEXT"

    #@1fc
    aput-object v3, v2, v4

    #@1fe
    const-string v3, "aos_reg_0_ipsec"

    #@200
    aput-object v3, v2, v5

    #@202
    const-string v3, "false"

    #@204
    aput-object v3, v2, v6

    #@206
    aput-object v2, v0, v1

    #@208
    const/16 v1, 0x9

    #@20a
    new-array v2, v7, [Ljava/lang/String;

    #@20c
    const-string v3, "TEXT"

    #@20e
    aput-object v3, v2, v4

    #@210
    const-string v3, "aos_reg_1_id"

    #@212
    aput-object v3, v2, v5

    #@214
    const-string v3, "aos_reg_1"

    #@216
    aput-object v3, v2, v6

    #@218
    aput-object v2, v0, v1

    #@21a
    const/16 v1, 0xa

    #@21c
    new-array v2, v7, [Ljava/lang/String;

    #@21e
    const-string v3, "TEXT"

    #@220
    aput-object v3, v2, v4

    #@222
    const-string v3, "aos_reg_1_type"

    #@224
    aput-object v3, v2, v5

    #@226
    const-string v3, "normal"

    #@228
    aput-object v3, v2, v6

    #@22a
    aput-object v2, v0, v1

    #@22c
    const/16 v1, 0xb

    #@22e
    new-array v2, v7, [Ljava/lang/String;

    #@230
    const-string v3, "INTEGER"

    #@232
    aput-object v3, v2, v4

    #@234
    const-string v3, "aos_reg_1_flow_id"

    #@236
    aput-object v3, v2, v5

    #@238
    const-string v3, "2"

    #@23a
    aput-object v3, v2, v6

    #@23c
    aput-object v2, v0, v1

    #@23e
    const/16 v1, 0xc

    #@240
    new-array v2, v7, [Ljava/lang/String;

    #@242
    const-string v3, "TEXT"

    #@244
    aput-object v3, v2, v4

    #@246
    const-string v3, "aos_reg_1_retry_interval"

    #@248
    aput-object v3, v2, v5

    #@24a
    const-string v3, "180,300,600"

    #@24c
    aput-object v3, v2, v6

    #@24e
    aput-object v2, v0, v1

    #@250
    const/16 v1, 0xd

    #@252
    new-array v2, v7, [Ljava/lang/String;

    #@254
    const-string v3, "INTEGER"

    #@256
    aput-object v3, v2, v4

    #@258
    const-string v3, "aos_reg_1_retry_repeat_interval"

    #@25a
    aput-object v3, v2, v5

    #@25c
    const-string v3, "-1"

    #@25e
    aput-object v3, v2, v6

    #@260
    aput-object v2, v0, v1

    #@262
    const/16 v1, 0xe

    #@264
    new-array v2, v7, [Ljava/lang/String;

    #@266
    const-string v3, "TEXT"

    #@268
    aput-object v3, v2, v4

    #@26a
    const-string v3, "aos_reg_1_ipsec"

    #@26c
    aput-object v3, v2, v5

    #@26e
    const-string v3, "false"

    #@270
    aput-object v3, v2, v6

    #@272
    aput-object v2, v0, v1

    #@274
    sput-object v0, Lcom/lge/ims/provider/Config;->AOSREG:[[Ljava/lang/String;

    #@276
    .line 132
    new-array v0, v7, [[Ljava/lang/String;

    #@278
    new-array v1, v7, [Ljava/lang/String;

    #@27a
    const-string v2, "INTEGER"

    #@27c
    aput-object v2, v1, v4

    #@27e
    const-string v2, "id"

    #@280
    aput-object v2, v1, v5

    #@282
    const-string v2, "1"

    #@284
    aput-object v2, v1, v6

    #@286
    aput-object v1, v0, v4

    #@288
    new-array v1, v7, [Ljava/lang/String;

    #@28a
    const-string v2, "INTEGER"

    #@28c
    aput-object v2, v1, v4

    #@28e
    const-string v2, "property"

    #@290
    aput-object v2, v1, v5

    #@292
    const-string v2, "0"

    #@294
    aput-object v2, v1, v6

    #@296
    aput-object v1, v0, v5

    #@298
    new-array v1, v7, [Ljava/lang/String;

    #@29a
    const-string v2, "TEXT"

    #@29c
    aput-object v2, v1, v4

    #@29e
    const-string v2, "conf"

    #@2a0
    aput-object v2, v1, v5

    #@2a2
    const-string v2, "[config]\nsubscriber=lgims.subscriber\nnetpolicy=lgims.netpolicy\nengine=lgims.engine\nsip=lgims.sip\nmedia=lgims.media\naos=lgims.aos\naosconnection=lgims.aosconnection\naosreg=lgims.aosreg\n\n"

    #@2a4
    aput-object v2, v1, v6

    #@2a6
    aput-object v1, v0, v6

    #@2a8
    sput-object v0, Lcom/lge/ims/provider/Config;->CONFIG:[[Ljava/lang/String;

    #@2aa
    .line 152
    new-array v0, v8, [[Ljava/lang/String;

    #@2ac
    new-array v1, v7, [Ljava/lang/String;

    #@2ae
    const-string v2, "INTEGER"

    #@2b0
    aput-object v2, v1, v4

    #@2b2
    const-string v2, "id"

    #@2b4
    aput-object v2, v1, v5

    #@2b6
    const-string v2, "1"

    #@2b8
    aput-object v2, v1, v6

    #@2ba
    aput-object v1, v0, v4

    #@2bc
    new-array v1, v7, [Ljava/lang/String;

    #@2be
    const-string v2, "INTEGER"

    #@2c0
    aput-object v2, v1, v4

    #@2c2
    const-string v2, "property"

    #@2c4
    aput-object v2, v1, v5

    #@2c6
    const-string v2, "1"

    #@2c8
    aput-object v2, v1, v6

    #@2ca
    aput-object v1, v0, v5

    #@2cc
    new-array v1, v7, [Ljava/lang/String;

    #@2ce
    const-string v2, "TEXT"

    #@2d0
    aput-object v2, v1, v4

    #@2d2
    const-string v2, "trace_option"

    #@2d4
    aput-object v2, v1, v5

    #@2d6
    const-string v2, "0x00040000"

    #@2d8
    aput-object v2, v1, v6

    #@2da
    aput-object v1, v0, v6

    #@2dc
    new-array v1, v7, [Ljava/lang/String;

    #@2de
    const-string v2, "TEXT"

    #@2e0
    aput-object v2, v1, v4

    #@2e2
    const-string v2, "trace_module"

    #@2e4
    aput-object v2, v1, v5

    #@2e6
    const-string v2, "0xFFFFFFFF"

    #@2e8
    aput-object v2, v1, v6

    #@2ea
    aput-object v1, v0, v7

    #@2ec
    sput-object v0, Lcom/lge/ims/provider/Config;->ENGINE:[[Ljava/lang/String;

    #@2ee
    .line 162
    new-array v0, v8, [[Ljava/lang/String;

    #@2f0
    new-array v1, v7, [Ljava/lang/String;

    #@2f2
    const-string v2, "INTEGER"

    #@2f4
    aput-object v2, v1, v4

    #@2f6
    const-string v2, "id"

    #@2f8
    aput-object v2, v1, v5

    #@2fa
    const-string v2, "1"

    #@2fc
    aput-object v2, v1, v6

    #@2fe
    aput-object v1, v0, v4

    #@300
    new-array v1, v7, [Ljava/lang/String;

    #@302
    const-string v2, "INTEGER"

    #@304
    aput-object v2, v1, v4

    #@306
    const-string v2, "property"

    #@308
    aput-object v2, v1, v5

    #@30a
    const-string v2, "1"

    #@30c
    aput-object v2, v1, v6

    #@30e
    aput-object v1, v0, v5

    #@310
    new-array v1, v7, [Ljava/lang/String;

    #@312
    const-string v2, "TEXT"

    #@314
    aput-object v2, v1, v4

    #@316
    const-string v2, "trace_option"

    #@318
    aput-object v2, v1, v5

    #@31a
    const-string v2, "0x0004000F"

    #@31c
    aput-object v2, v1, v6

    #@31e
    aput-object v1, v0, v6

    #@320
    new-array v1, v7, [Ljava/lang/String;

    #@322
    const-string v2, "TEXT"

    #@324
    aput-object v2, v1, v4

    #@326
    const-string v2, "trace_module"

    #@328
    aput-object v2, v1, v5

    #@32a
    const-string v2, "0xFFFFFFFF"

    #@32c
    aput-object v2, v1, v6

    #@32e
    aput-object v1, v0, v7

    #@330
    sput-object v0, Lcom/lge/ims/provider/Config;->ENGINE_DEBUG:[[Ljava/lang/String;

    #@332
    .line 178
    new-array v0, v7, [[Ljava/lang/String;

    #@334
    new-array v1, v7, [Ljava/lang/String;

    #@336
    const-string v2, "INTEGER"

    #@338
    aput-object v2, v1, v4

    #@33a
    const-string v2, "id"

    #@33c
    aput-object v2, v1, v5

    #@33e
    const-string v2, "1"

    #@340
    aput-object v2, v1, v6

    #@342
    aput-object v1, v0, v4

    #@344
    new-array v1, v7, [Ljava/lang/String;

    #@346
    const-string v2, "INTEGER"

    #@348
    aput-object v2, v1, v4

    #@34a
    const-string v2, "property"

    #@34c
    aput-object v2, v1, v5

    #@34e
    const-string v2, "0"

    #@350
    aput-object v2, v1, v6

    #@352
    aput-object v1, v0, v5

    #@354
    new-array v1, v7, [Ljava/lang/String;

    #@356
    const-string v2, "TEXT"

    #@358
    aput-object v2, v1, v4

    #@35a
    const-string v2, "conf"

    #@35c
    aput-object v2, v1, v5

    #@35e
    const-string v2, "[profiles]\nids=capabilities,com.kt.media.vt,com.kt.media.is,com.kt.media.vs,com.kt.media.im\ncapabilities=lgims.com.kt.media\ncom.kt.media.vt=lgims.com.kt.media\ncom.kt.media.is=lgims.com.kt.media\ncom.kt.media.vs=lgims.com.kt.media\ncom.kt.media.im=lgims.com.kt.media\n\n"

    #@360
    aput-object v2, v1, v6

    #@362
    aput-object v1, v0, v6

    #@364
    sput-object v0, Lcom/lge/ims/provider/Config;->MEDIA:[[Ljava/lang/String;

    #@366
    .line 196
    new-array v0, v7, [[Ljava/lang/String;

    #@368
    new-array v1, v7, [Ljava/lang/String;

    #@36a
    const-string v2, "INTEGER"

    #@36c
    aput-object v2, v1, v4

    #@36e
    const-string v2, "id"

    #@370
    aput-object v2, v1, v5

    #@372
    const-string v2, "1"

    #@374
    aput-object v2, v1, v6

    #@376
    aput-object v1, v0, v4

    #@378
    new-array v1, v7, [Ljava/lang/String;

    #@37a
    const-string v2, "INTEGER"

    #@37c
    aput-object v2, v1, v4

    #@37e
    const-string v2, "property"

    #@380
    aput-object v2, v1, v5

    #@382
    const-string v2, "0"

    #@384
    aput-object v2, v1, v6

    #@386
    aput-object v1, v0, v5

    #@388
    new-array v1, v7, [Ljava/lang/String;

    #@38a
    const-string v2, "TEXT"

    #@38c
    aput-object v2, v1, v4

    #@38e
    const-string v2, "conf"

    #@390
    aput-object v2, v1, v5

    #@392
    const-string v2, "[netpolicy]\nprimary=mobile_ims\ntypes=mobile_ims,mobile_internet,wifi\n\n[mobile_ims]\nname=mobile_ims\nprofile_id=1\npco_pcscf=true\n\n[mobile_internet]\nname=mobile_internet\nprofile_id=2\npco_pcscf=false\n\n[wifi]\nname=wifi\nprofile_id=21\npco_pcscf=false\n\n"

    #@394
    aput-object v2, v1, v6

    #@396
    aput-object v1, v0, v6

    #@398
    sput-object v0, Lcom/lge/ims/provider/Config;->NETPOLICY:[[Ljava/lang/String;

    #@39a
    .line 225
    const/16 v0, 0x18

    #@39c
    new-array v0, v0, [[Ljava/lang/String;

    #@39e
    new-array v1, v7, [Ljava/lang/String;

    #@3a0
    const-string v2, "INTEGER"

    #@3a2
    aput-object v2, v1, v4

    #@3a4
    const-string v2, "id"

    #@3a6
    aput-object v2, v1, v5

    #@3a8
    const-string v2, "1"

    #@3aa
    aput-object v2, v1, v6

    #@3ac
    aput-object v1, v0, v4

    #@3ae
    new-array v1, v7, [Ljava/lang/String;

    #@3b0
    const-string v2, "INTEGER"

    #@3b2
    aput-object v2, v1, v4

    #@3b4
    const-string v2, "property"

    #@3b6
    aput-object v2, v1, v5

    #@3b8
    const-string v2, "1"

    #@3ba
    aput-object v2, v1, v6

    #@3bc
    aput-object v1, v0, v5

    #@3be
    new-array v1, v7, [Ljava/lang/String;

    #@3c0
    const-string v2, "TEXT"

    #@3c2
    aput-object v2, v1, v4

    #@3c4
    const-string v2, "common_compact_form"

    #@3c6
    aput-object v2, v1, v5

    #@3c8
    const-string v2, "false"

    #@3ca
    aput-object v2, v1, v6

    #@3cc
    aput-object v1, v0, v6

    #@3ce
    new-array v1, v7, [Ljava/lang/String;

    #@3d0
    const-string v2, "TEXT"

    #@3d2
    aput-object v2, v1, v4

    #@3d4
    const-string v2, "common_sip_features"

    #@3d6
    aput-object v2, v1, v5

    #@3d8
    const-string v2, "0x04010008"

    #@3da
    aput-object v2, v1, v6

    #@3dc
    aput-object v1, v0, v7

    #@3de
    new-array v1, v7, [Ljava/lang/String;

    #@3e0
    const-string v2, "INTEGER"

    #@3e2
    aput-object v2, v1, v4

    #@3e4
    const-string v2, "common_device_id"

    #@3e6
    aput-object v2, v1, v5

    #@3e8
    const-string v2, "0"

    #@3ea
    aput-object v2, v1, v6

    #@3ec
    aput-object v1, v0, v8

    #@3ee
    const/4 v1, 0x5

    #@3ef
    new-array v2, v7, [Ljava/lang/String;

    #@3f1
    const-string v3, "TEXT"

    #@3f3
    aput-object v3, v2, v4

    #@3f5
    const-string v3, "common_tag_prefix"

    #@3f7
    aput-object v3, v2, v5

    #@3f9
    const-string v3, "54467"

    #@3fb
    aput-object v3, v2, v6

    #@3fd
    aput-object v2, v0, v1

    #@3ff
    const/4 v1, 0x6

    #@400
    new-array v2, v7, [Ljava/lang/String;

    #@402
    const-string v3, "INTEGER"

    #@404
    aput-object v3, v2, v4

    #@406
    const-string v3, "common_tcp_criterion_len"

    #@408
    aput-object v3, v2, v5

    #@40a
    const-string v3, "4096"

    #@40c
    aput-object v3, v2, v6

    #@40e
    aput-object v2, v0, v1

    #@410
    const/4 v1, 0x7

    #@411
    new-array v2, v7, [Ljava/lang/String;

    #@413
    const-string v3, "INTEGER"

    #@415
    aput-object v3, v2, v4

    #@417
    const-string v3, "timer_tv_t1"

    #@419
    aput-object v3, v2, v5

    #@41b
    const-string v3, "1000"

    #@41d
    aput-object v3, v2, v6

    #@41f
    aput-object v2, v0, v1

    #@421
    const/16 v1, 0x8

    #@423
    new-array v2, v7, [Ljava/lang/String;

    #@425
    const-string v3, "INTEGER"

    #@427
    aput-object v3, v2, v4

    #@429
    const-string v3, "timer_tv_t2"

    #@42b
    aput-object v3, v2, v5

    #@42d
    const-string v3, "2000"

    #@42f
    aput-object v3, v2, v6

    #@431
    aput-object v2, v0, v1

    #@433
    const/16 v1, 0x9

    #@435
    new-array v2, v7, [Ljava/lang/String;

    #@437
    const-string v3, "INTEGER"

    #@439
    aput-object v3, v2, v4

    #@43b
    const-string v3, "timer_tv_tcp_connection"

    #@43d
    aput-object v3, v2, v5

    #@43f
    const-string v3, "-1"

    #@441
    aput-object v3, v2, v6

    #@443
    aput-object v2, v0, v1

    #@445
    const/16 v1, 0xa

    #@447
    new-array v2, v7, [Ljava/lang/String;

    #@449
    const-string v3, "INTEGER"

    #@44b
    aput-object v3, v2, v4

    #@44d
    const-string v3, "timer_tv_tcp_keepalive"

    #@44f
    aput-object v3, v2, v5

    #@451
    const-string v3, "-1"

    #@453
    aput-object v3, v2, v6

    #@455
    aput-object v2, v0, v1

    #@457
    const/16 v1, 0xb

    #@459
    new-array v2, v7, [Ljava/lang/String;

    #@45b
    const-string v3, "INTEGER"

    #@45d
    aput-object v3, v2, v4

    #@45f
    const-string v3, "timer_tv_tcp_wouldblock"

    #@461
    aput-object v3, v2, v5

    #@463
    const-string v3, "-1"

    #@465
    aput-object v3, v2, v6

    #@467
    aput-object v2, v0, v1

    #@469
    const/16 v1, 0xc

    #@46b
    new-array v2, v7, [Ljava/lang/String;

    #@46d
    const-string v3, "TEXT"

    #@46f
    aput-object v3, v2, v4

    #@471
    const-string v3, "listen_channel_scheme"

    #@473
    aput-object v3, v2, v5

    #@475
    const-string v3, "sip"

    #@477
    aput-object v3, v2, v6

    #@479
    aput-object v2, v0, v1

    #@47b
    const/16 v1, 0xd

    #@47d
    new-array v2, v7, [Ljava/lang/String;

    #@47f
    const-string v3, "INTEGER"

    #@481
    aput-object v3, v2, v4

    #@483
    const-string v3, "listen_channel_port"

    #@485
    aput-object v3, v2, v5

    #@487
    const-string v3, "5060"

    #@489
    aput-object v3, v2, v6

    #@48b
    aput-object v2, v0, v1

    #@48d
    const/16 v1, 0xe

    #@48f
    new-array v2, v7, [Ljava/lang/String;

    #@491
    const-string v3, "TEXT"

    #@493
    aput-object v3, v2, v4

    #@495
    const-string v3, "listen_channel_transport"

    #@497
    aput-object v3, v2, v5

    #@499
    const-string v3, "udp"

    #@49b
    aput-object v3, v2, v6

    #@49d
    aput-object v2, v0, v1

    #@49f
    const/16 v1, 0xf

    #@4a1
    new-array v2, v7, [Ljava/lang/String;

    #@4a3
    const-string v3, "TEXT"

    #@4a5
    aput-object v3, v2, v4

    #@4a7
    const-string v3, "ua_version_sw_version"

    #@4a9
    aput-object v3, v2, v5

    #@4ab
    const-string v3, ""

    #@4ad
    aput-object v3, v2, v6

    #@4af
    aput-object v2, v0, v1

    #@4b1
    const/16 v1, 0x10

    #@4b3
    new-array v2, v7, [Ljava/lang/String;

    #@4b5
    const-string v3, "INTEGER"

    #@4b7
    aput-object v3, v2, v4

    #@4b9
    const-string v3, "reg_expiration"

    #@4bb
    aput-object v3, v2, v5

    #@4bd
    const-string v3, "28800"

    #@4bf
    aput-object v3, v2, v6

    #@4c1
    aput-object v2, v0, v1

    #@4c3
    const/16 v1, 0x11

    #@4c5
    new-array v2, v7, [Ljava/lang/String;

    #@4c7
    const-string v3, "TEXT"

    #@4c9
    aput-object v3, v2, v4

    #@4cb
    const-string v3, "reg_methods"

    #@4cd
    aput-object v3, v2, v5

    #@4cf
    const-string v3, "INVITE,BYE,CANCEL,ACK,NOTIFY,UPDATE,REFER,PRACK,INFO,MESSAGE,OPTIONS"

    #@4d1
    aput-object v3, v2, v6

    #@4d3
    aput-object v2, v0, v1

    #@4d5
    const/16 v1, 0x12

    #@4d7
    new-array v2, v7, [Ljava/lang/String;

    #@4d9
    const-string v3, "TEXT"

    #@4db
    aput-object v3, v2, v4

    #@4dd
    const-string v3, "reg_subscription"

    #@4df
    aput-object v3, v2, v5

    #@4e1
    const-string v3, "true"

    #@4e3
    aput-object v3, v2, v6

    #@4e5
    aput-object v2, v0, v1

    #@4e7
    const/16 v1, 0x13

    #@4e9
    new-array v2, v7, [Ljava/lang/String;

    #@4eb
    const-string v3, "INTEGER"

    #@4ed
    aput-object v3, v2, v4

    #@4ef
    const-string v3, "reg_sub_expiration"

    #@4f1
    aput-object v3, v2, v5

    #@4f3
    const-string v3, "8000"

    #@4f5
    aput-object v3, v2, v6

    #@4f7
    aput-object v2, v0, v1

    #@4f9
    const/16 v1, 0x14

    #@4fb
    new-array v2, v7, [Ljava/lang/String;

    #@4fd
    const-string v3, "TEXT"

    #@4ff
    aput-object v3, v2, v4

    #@501
    const-string v3, "service_shared"

    #@503
    aput-object v3, v2, v5

    #@505
    const-string v3, "lgims.com.kt.sip"

    #@507
    aput-object v3, v2, v6

    #@509
    aput-object v2, v0, v1

    #@50b
    const/16 v1, 0x15

    #@50d
    new-array v2, v7, [Ljava/lang/String;

    #@50f
    const-string v3, "INTEGER"

    #@511
    aput-object v3, v2, v4

    #@513
    const-string v3, "service_dedicated_count"

    #@515
    aput-object v3, v2, v5

    #@517
    const-string v3, "1"

    #@519
    aput-object v3, v2, v6

    #@51b
    aput-object v2, v0, v1

    #@51d
    const/16 v1, 0x16

    #@51f
    new-array v2, v7, [Ljava/lang/String;

    #@521
    const-string v3, "TEXT"

    #@523
    aput-object v3, v2, v4

    #@525
    const-string v3, "service_id_0"

    #@527
    aput-object v3, v2, v5

    #@529
    const-string v3, "lgims.com.kt.service.vt"

    #@52b
    aput-object v3, v2, v6

    #@52d
    aput-object v2, v0, v1

    #@52f
    const/16 v1, 0x17

    #@531
    new-array v2, v7, [Ljava/lang/String;

    #@533
    const-string v3, "TEXT"

    #@535
    aput-object v3, v2, v4

    #@537
    const-string v3, "service_config_0"

    #@539
    aput-object v3, v2, v5

    #@53b
    const-string v3, "lgims.com.kt.sip.vt"

    #@53d
    aput-object v3, v2, v6

    #@53f
    aput-object v2, v0, v1

    #@541
    sput-object v0, Lcom/lge/ims/provider/Config;->SIP:[[Ljava/lang/String;

    #@543
    .line 280
    const/16 v0, 0x1f

    #@545
    new-array v0, v0, [[Ljava/lang/String;

    #@547
    new-array v1, v7, [Ljava/lang/String;

    #@549
    const-string v2, "INTEGER"

    #@54b
    aput-object v2, v1, v4

    #@54d
    const-string v2, "id"

    #@54f
    aput-object v2, v1, v5

    #@551
    const-string v2, "1"

    #@553
    aput-object v2, v1, v6

    #@555
    aput-object v1, v0, v4

    #@557
    new-array v1, v7, [Ljava/lang/String;

    #@559
    const-string v2, "INTEGER"

    #@55b
    aput-object v2, v1, v4

    #@55d
    const-string v2, "property"

    #@55f
    aput-object v2, v1, v5

    #@561
    const-string v2, "1"

    #@563
    aput-object v2, v1, v6

    #@565
    aput-object v1, v0, v5

    #@567
    new-array v1, v7, [Ljava/lang/String;

    #@569
    const-string v2, "INTEGER"

    #@56b
    aput-object v2, v1, v4

    #@56d
    const-string v2, "Uniqueness_server_pcscf"

    #@56f
    aput-object v2, v1, v5

    #@571
    const-string v2, "2"

    #@573
    aput-object v2, v1, v6

    #@575
    aput-object v1, v0, v6

    #@577
    new-array v1, v7, [Ljava/lang/String;

    #@579
    const-string v2, "INTEGER"

    #@57b
    aput-object v2, v1, v4

    #@57d
    const-string v2, "Uniqueness_subscriber"

    #@57f
    aput-object v2, v1, v5

    #@581
    const-string v2, "1"

    #@583
    aput-object v2, v1, v6

    #@585
    aput-object v1, v0, v7

    #@587
    new-array v1, v7, [Ljava/lang/String;

    #@589
    const-string v2, "TEXT"

    #@58b
    aput-object v2, v1, v4

    #@58d
    const-string v2, "admin_ims"

    #@58f
    aput-object v2, v1, v5

    #@591
    const-string v2, "true"

    #@593
    aput-object v2, v1, v6

    #@595
    aput-object v1, v0, v8

    #@597
    const/4 v1, 0x5

    #@598
    new-array v2, v7, [Ljava/lang/String;

    #@59a
    const-string v3, "TEXT"

    #@59c
    aput-object v3, v2, v4

    #@59e
    const-string v3, "admin_isim"

    #@5a0
    aput-object v3, v2, v5

    #@5a2
    const-string v3, "false"

    #@5a4
    aput-object v3, v2, v6

    #@5a6
    aput-object v2, v0, v1

    #@5a8
    const/4 v1, 0x6

    #@5a9
    new-array v2, v7, [Ljava/lang/String;

    #@5ab
    const-string v3, "TEXT"

    #@5ad
    aput-object v3, v2, v4

    #@5af
    const-string v3, "admin_usim"

    #@5b1
    aput-object v3, v2, v5

    #@5b3
    const-string v3, "true"

    #@5b5
    aput-object v3, v2, v6

    #@5b7
    aput-object v2, v0, v1

    #@5b9
    const/4 v1, 0x7

    #@5ba
    new-array v2, v7, [Ljava/lang/String;

    #@5bc
    const-string v3, "TEXT"

    #@5be
    aput-object v3, v2, v4

    #@5c0
    const-string v3, "admin_pcscf"

    #@5c2
    aput-object v3, v2, v5

    #@5c4
    const-string v3, "CONF"

    #@5c6
    aput-object v3, v2, v6

    #@5c8
    aput-object v2, v0, v1

    #@5ca
    const/16 v1, 0x8

    #@5cc
    new-array v2, v7, [Ljava/lang/String;

    #@5ce
    const-string v3, "TEXT"

    #@5d0
    aput-object v3, v2, v4

    #@5d2
    const-string v3, "admin_ac"

    #@5d4
    aput-object v3, v2, v5

    #@5d6
    const-string v3, "true"

    #@5d8
    aput-object v3, v2, v6

    #@5da
    aput-object v2, v0, v1

    #@5dc
    const/16 v1, 0x9

    #@5de
    new-array v2, v7, [Ljava/lang/String;

    #@5e0
    const-string v3, "TEXT"

    #@5e2
    aput-object v3, v2, v4

    #@5e4
    const-string v3, "admin_dm"

    #@5e6
    aput-object v3, v2, v5

    #@5e8
    const-string v3, "false"

    #@5ea
    aput-object v3, v2, v6

    #@5ec
    aput-object v2, v0, v1

    #@5ee
    const/16 v1, 0xa

    #@5f0
    new-array v2, v7, [Ljava/lang/String;

    #@5f2
    const-string v3, "TEXT"

    #@5f4
    aput-object v3, v2, v4

    #@5f6
    const-string v3, "admin_debug"

    #@5f8
    aput-object v3, v2, v5

    #@5fa
    const-string v3, "false"

    #@5fc
    aput-object v3, v2, v6

    #@5fe
    aput-object v2, v0, v1

    #@600
    const/16 v1, 0xb

    #@602
    new-array v2, v7, [Ljava/lang/String;

    #@604
    const-string v3, "TEXT"

    #@606
    aput-object v3, v2, v4

    #@608
    const-string v3, "admin_services"

    #@60a
    aput-object v3, v2, v5

    #@60c
    const-string v3, "0x00000001"

    #@60e
    aput-object v3, v2, v6

    #@610
    aput-object v2, v0, v1

    #@612
    const/16 v1, 0xc

    #@614
    new-array v2, v7, [Ljava/lang/String;

    #@616
    const-string v3, "TEXT"

    #@618
    aput-object v3, v2, v4

    #@61a
    const-string v3, "admin_testmode"

    #@61c
    aput-object v3, v2, v5

    #@61e
    const-string v3, "false"

    #@620
    aput-object v3, v2, v6

    #@622
    aput-object v2, v0, v1

    #@624
    const/16 v1, 0xd

    #@626
    new-array v2, v7, [Ljava/lang/String;

    #@628
    const-string v3, "TEXT"

    #@62a
    aput-object v3, v2, v4

    #@62c
    const-string v3, "server_pcscf_0_address"

    #@62e
    aput-object v3, v2, v5

    #@630
    const-string v3, "125.159.63.5"

    #@632
    aput-object v3, v2, v6

    #@634
    aput-object v2, v0, v1

    #@636
    const/16 v1, 0xe

    #@638
    new-array v2, v7, [Ljava/lang/String;

    #@63a
    const-string v3, "INTEGER"

    #@63c
    aput-object v3, v2, v4

    #@63e
    const-string v3, "server_pcscf_0_port"

    #@640
    aput-object v3, v2, v5

    #@642
    const-string v3, "5080"

    #@644
    aput-object v3, v2, v6

    #@646
    aput-object v2, v0, v1

    #@648
    const/16 v1, 0xf

    #@64a
    new-array v2, v7, [Ljava/lang/String;

    #@64c
    const-string v3, "TEXT"

    #@64e
    aput-object v3, v2, v4

    #@650
    const-string v3, "server_pcscf_1_address"

    #@652
    aput-object v3, v2, v5

    #@654
    const-string v3, "125.159.63.5"

    #@656
    aput-object v3, v2, v6

    #@658
    aput-object v2, v0, v1

    #@65a
    const/16 v1, 0x10

    #@65c
    new-array v2, v7, [Ljava/lang/String;

    #@65e
    const-string v3, "INTEGER"

    #@660
    aput-object v3, v2, v4

    #@662
    const-string v3, "server_pcscf_1_port"

    #@664
    aput-object v3, v2, v5

    #@666
    const-string v3, "5080"

    #@668
    aput-object v3, v2, v6

    #@66a
    aput-object v2, v0, v1

    #@66c
    const/16 v1, 0x11

    #@66e
    new-array v2, v7, [Ljava/lang/String;

    #@670
    const-string v3, "TEXT"

    #@672
    aput-object v3, v2, v4

    #@674
    const-string v3, "subscriber_0_home_domain_name"

    #@676
    aput-object v3, v2, v5

    #@678
    const-string v3, "ims.kt.com"

    #@67a
    aput-object v3, v2, v6

    #@67c
    aput-object v2, v0, v1

    #@67e
    const/16 v1, 0x12

    #@680
    new-array v2, v7, [Ljava/lang/String;

    #@682
    const-string v3, "TEXT"

    #@684
    aput-object v3, v2, v4

    #@686
    const-string v3, "subscriber_0_impi"

    #@688
    aput-object v3, v2, v5

    #@68a
    const-string v3, "450086020001592@ims.kt.com"

    #@68c
    aput-object v3, v2, v6

    #@68e
    aput-object v2, v0, v1

    #@690
    const/16 v1, 0x13

    #@692
    new-array v2, v7, [Ljava/lang/String;

    #@694
    const-string v3, "INTEGER"

    #@696
    aput-object v3, v2, v4

    #@698
    const-string v3, "subscriber_0_impu_primary_ref_index"

    #@69a
    aput-object v3, v2, v5

    #@69c
    const-string v3, "0"

    #@69e
    aput-object v3, v2, v6

    #@6a0
    aput-object v2, v0, v1

    #@6a2
    const/16 v1, 0x14

    #@6a4
    new-array v2, v7, [Ljava/lang/String;

    #@6a6
    const-string v3, "INTEGER"

    #@6a8
    aput-object v3, v2, v4

    #@6aa
    const-string v3, "subscriber_0_impu_count"

    #@6ac
    aput-object v3, v2, v5

    #@6ae
    const-string v3, "3"

    #@6b0
    aput-object v3, v2, v6

    #@6b2
    aput-object v2, v0, v1

    #@6b4
    const/16 v1, 0x15

    #@6b6
    new-array v2, v7, [Ljava/lang/String;

    #@6b8
    const-string v3, "TEXT"

    #@6ba
    aput-object v3, v2, v4

    #@6bc
    const-string v3, "subscriber_0_impu_0"

    #@6be
    aput-object v3, v2, v5

    #@6c0
    const-string v3, "sip:01032164404@ims.kt.com"

    #@6c2
    aput-object v3, v2, v6

    #@6c4
    aput-object v2, v0, v1

    #@6c6
    const/16 v1, 0x16

    #@6c8
    new-array v2, v7, [Ljava/lang/String;

    #@6ca
    const-string v3, "TEXT"

    #@6cc
    aput-object v3, v2, v4

    #@6ce
    const-string v3, "subscriber_0_impu_1"

    #@6d0
    aput-object v3, v2, v5

    #@6d2
    const-string v3, "sip:01032164404@ims.kt.com"

    #@6d4
    aput-object v3, v2, v6

    #@6d6
    aput-object v2, v0, v1

    #@6d8
    const/16 v1, 0x17

    #@6da
    new-array v2, v7, [Ljava/lang/String;

    #@6dc
    const-string v3, "TEXT"

    #@6de
    aput-object v3, v2, v4

    #@6e0
    const-string v3, "subscriber_0_impu_2"

    #@6e2
    aput-object v3, v2, v5

    #@6e4
    const-string v3, "tel:*"

    #@6e6
    aput-object v3, v2, v6

    #@6e8
    aput-object v2, v0, v1

    #@6ea
    const/16 v1, 0x18

    #@6ec
    new-array v2, v7, [Ljava/lang/String;

    #@6ee
    const-string v3, "TEXT"

    #@6f0
    aput-object v3, v2, v4

    #@6f2
    const-string v3, "subscriber_0_phone_context"

    #@6f4
    aput-object v3, v2, v5

    #@6f6
    const-string v3, "ims.kt.com"

    #@6f8
    aput-object v3, v2, v6

    #@6fa
    aput-object v2, v0, v1

    #@6fc
    const/16 v1, 0x19

    #@6fe
    new-array v2, v7, [Ljava/lang/String;

    #@700
    const-string v3, "TEXT"

    #@702
    aput-object v3, v2, v4

    #@704
    const-string v3, "subscriber_0_auth_username"

    #@706
    aput-object v3, v2, v5

    #@708
    const-string v3, ""

    #@70a
    aput-object v3, v2, v6

    #@70c
    aput-object v2, v0, v1

    #@70e
    const/16 v1, 0x1a

    #@710
    new-array v2, v7, [Ljava/lang/String;

    #@712
    const-string v3, "TEXT"

    #@714
    aput-object v3, v2, v4

    #@716
    const-string v3, "subscriber_0_auth_password"

    #@718
    aput-object v3, v2, v5

    #@71a
    const-string v3, "827CCB0EEA8A706C4C34A16891F84E7B"

    #@71c
    aput-object v3, v2, v6

    #@71e
    aput-object v2, v0, v1

    #@720
    const/16 v1, 0x1b

    #@722
    new-array v2, v7, [Ljava/lang/String;

    #@724
    const-string v3, "TEXT"

    #@726
    aput-object v3, v2, v4

    #@728
    const-string v3, "subscriber_0_auth_realm"

    #@72a
    aput-object v3, v2, v5

    #@72c
    const-string v3, ""

    #@72e
    aput-object v3, v2, v6

    #@730
    aput-object v2, v0, v1

    #@732
    const/16 v1, 0x1c

    #@734
    new-array v2, v7, [Ljava/lang/String;

    #@736
    const-string v3, "TEXT"

    #@738
    aput-object v3, v2, v4

    #@73a
    const-string v3, "subscriber_0_auth_algorithm"

    #@73c
    aput-object v3, v2, v5

    #@73e
    const-string v3, ""

    #@740
    aput-object v3, v2, v6

    #@742
    aput-object v2, v0, v1

    #@744
    const/16 v1, 0x1d

    #@746
    new-array v2, v7, [Ljava/lang/String;

    #@748
    const-string v3, "TEXT"

    #@74a
    aput-object v3, v2, v4

    #@74c
    const-string v3, "subscriber_0_auth_realm_leniency"

    #@74e
    aput-object v3, v2, v5

    #@750
    const-string v3, "true"

    #@752
    aput-object v3, v2, v6

    #@754
    aput-object v2, v0, v1

    #@756
    const/16 v1, 0x1e

    #@758
    new-array v2, v7, [Ljava/lang/String;

    #@75a
    const-string v3, "TEXT"

    #@75c
    aput-object v3, v2, v4

    #@75e
    const-string v3, "subscriber_0_server_scscf"

    #@760
    aput-object v3, v2, v5

    #@762
    const-string v3, "ims.kt.com"

    #@764
    aput-object v3, v2, v6

    #@766
    aput-object v2, v0, v1

    #@768
    sput-object v0, Lcom/lge/ims/provider/Config;->SUBSCRIBER:[[Ljava/lang/String;

    #@76a
    .line 323
    const/16 v0, 0x1f

    #@76c
    new-array v0, v0, [[Ljava/lang/String;

    #@76e
    new-array v1, v7, [Ljava/lang/String;

    #@770
    const-string v2, "INTEGER"

    #@772
    aput-object v2, v1, v4

    #@774
    const-string v2, "id"

    #@776
    aput-object v2, v1, v5

    #@778
    const-string v2, "1"

    #@77a
    aput-object v2, v1, v6

    #@77c
    aput-object v1, v0, v4

    #@77e
    new-array v1, v7, [Ljava/lang/String;

    #@780
    const-string v2, "INTEGER"

    #@782
    aput-object v2, v1, v4

    #@784
    const-string v2, "property"

    #@786
    aput-object v2, v1, v5

    #@788
    const-string v2, "1"

    #@78a
    aput-object v2, v1, v6

    #@78c
    aput-object v1, v0, v5

    #@78e
    new-array v1, v7, [Ljava/lang/String;

    #@790
    const-string v2, "INTEGER"

    #@792
    aput-object v2, v1, v4

    #@794
    const-string v2, "Uniqueness_server_pcscf"

    #@796
    aput-object v2, v1, v5

    #@798
    const-string v2, "2"

    #@79a
    aput-object v2, v1, v6

    #@79c
    aput-object v1, v0, v6

    #@79e
    new-array v1, v7, [Ljava/lang/String;

    #@7a0
    const-string v2, "INTEGER"

    #@7a2
    aput-object v2, v1, v4

    #@7a4
    const-string v2, "Uniqueness_subscriber"

    #@7a6
    aput-object v2, v1, v5

    #@7a8
    const-string v2, "1"

    #@7aa
    aput-object v2, v1, v6

    #@7ac
    aput-object v1, v0, v7

    #@7ae
    new-array v1, v7, [Ljava/lang/String;

    #@7b0
    const-string v2, "TEXT"

    #@7b2
    aput-object v2, v1, v4

    #@7b4
    const-string v2, "admin_ims"

    #@7b6
    aput-object v2, v1, v5

    #@7b8
    const-string v2, "true"

    #@7ba
    aput-object v2, v1, v6

    #@7bc
    aput-object v1, v0, v8

    #@7be
    const/4 v1, 0x5

    #@7bf
    new-array v2, v7, [Ljava/lang/String;

    #@7c1
    const-string v3, "TEXT"

    #@7c3
    aput-object v3, v2, v4

    #@7c5
    const-string v3, "admin_isim"

    #@7c7
    aput-object v3, v2, v5

    #@7c9
    const-string v3, "false"

    #@7cb
    aput-object v3, v2, v6

    #@7cd
    aput-object v2, v0, v1

    #@7cf
    const/4 v1, 0x6

    #@7d0
    new-array v2, v7, [Ljava/lang/String;

    #@7d2
    const-string v3, "TEXT"

    #@7d4
    aput-object v3, v2, v4

    #@7d6
    const-string v3, "admin_usim"

    #@7d8
    aput-object v3, v2, v5

    #@7da
    const-string v3, "true"

    #@7dc
    aput-object v3, v2, v6

    #@7de
    aput-object v2, v0, v1

    #@7e0
    const/4 v1, 0x7

    #@7e1
    new-array v2, v7, [Ljava/lang/String;

    #@7e3
    const-string v3, "TEXT"

    #@7e5
    aput-object v3, v2, v4

    #@7e7
    const-string v3, "admin_pcscf"

    #@7e9
    aput-object v3, v2, v5

    #@7eb
    const-string v3, "CONF"

    #@7ed
    aput-object v3, v2, v6

    #@7ef
    aput-object v2, v0, v1

    #@7f1
    const/16 v1, 0x8

    #@7f3
    new-array v2, v7, [Ljava/lang/String;

    #@7f5
    const-string v3, "TEXT"

    #@7f7
    aput-object v3, v2, v4

    #@7f9
    const-string v3, "admin_ac"

    #@7fb
    aput-object v3, v2, v5

    #@7fd
    const-string v3, "true"

    #@7ff
    aput-object v3, v2, v6

    #@801
    aput-object v2, v0, v1

    #@803
    const/16 v1, 0x9

    #@805
    new-array v2, v7, [Ljava/lang/String;

    #@807
    const-string v3, "TEXT"

    #@809
    aput-object v3, v2, v4

    #@80b
    const-string v3, "admin_dm"

    #@80d
    aput-object v3, v2, v5

    #@80f
    const-string v3, "false"

    #@811
    aput-object v3, v2, v6

    #@813
    aput-object v2, v0, v1

    #@815
    const/16 v1, 0xa

    #@817
    new-array v2, v7, [Ljava/lang/String;

    #@819
    const-string v3, "TEXT"

    #@81b
    aput-object v3, v2, v4

    #@81d
    const-string v3, "admin_debug"

    #@81f
    aput-object v3, v2, v5

    #@821
    const-string v3, "false"

    #@823
    aput-object v3, v2, v6

    #@825
    aput-object v2, v0, v1

    #@827
    const/16 v1, 0xb

    #@829
    new-array v2, v7, [Ljava/lang/String;

    #@82b
    const-string v3, "TEXT"

    #@82d
    aput-object v3, v2, v4

    #@82f
    const-string v3, "admin_services"

    #@831
    aput-object v3, v2, v5

    #@833
    const-string v3, "0x00000001"

    #@835
    aput-object v3, v2, v6

    #@837
    aput-object v2, v0, v1

    #@839
    const/16 v1, 0xc

    #@83b
    new-array v2, v7, [Ljava/lang/String;

    #@83d
    const-string v3, "TEXT"

    #@83f
    aput-object v3, v2, v4

    #@841
    const-string v3, "admin_testmode"

    #@843
    aput-object v3, v2, v5

    #@845
    const-string v3, "false"

    #@847
    aput-object v3, v2, v6

    #@849
    aput-object v2, v0, v1

    #@84b
    const/16 v1, 0xd

    #@84d
    new-array v2, v7, [Ljava/lang/String;

    #@84f
    const-string v3, "TEXT"

    #@851
    aput-object v3, v2, v4

    #@853
    const-string v3, "server_pcscf_0_address"

    #@855
    aput-object v3, v2, v5

    #@857
    const-string v3, "125.159.63.5"

    #@859
    aput-object v3, v2, v6

    #@85b
    aput-object v2, v0, v1

    #@85d
    const/16 v1, 0xe

    #@85f
    new-array v2, v7, [Ljava/lang/String;

    #@861
    const-string v3, "INTEGER"

    #@863
    aput-object v3, v2, v4

    #@865
    const-string v3, "server_pcscf_0_port"

    #@867
    aput-object v3, v2, v5

    #@869
    const-string v3, "5080"

    #@86b
    aput-object v3, v2, v6

    #@86d
    aput-object v2, v0, v1

    #@86f
    const/16 v1, 0xf

    #@871
    new-array v2, v7, [Ljava/lang/String;

    #@873
    const-string v3, "TEXT"

    #@875
    aput-object v3, v2, v4

    #@877
    const-string v3, "server_pcscf_1_address"

    #@879
    aput-object v3, v2, v5

    #@87b
    const-string v3, "125.159.63.5"

    #@87d
    aput-object v3, v2, v6

    #@87f
    aput-object v2, v0, v1

    #@881
    const/16 v1, 0x10

    #@883
    new-array v2, v7, [Ljava/lang/String;

    #@885
    const-string v3, "INTEGER"

    #@887
    aput-object v3, v2, v4

    #@889
    const-string v3, "server_pcscf_1_port"

    #@88b
    aput-object v3, v2, v5

    #@88d
    const-string v3, "5080"

    #@88f
    aput-object v3, v2, v6

    #@891
    aput-object v2, v0, v1

    #@893
    const/16 v1, 0x11

    #@895
    new-array v2, v7, [Ljava/lang/String;

    #@897
    const-string v3, "TEXT"

    #@899
    aput-object v3, v2, v4

    #@89b
    const-string v3, "subscriber_0_home_domain_name"

    #@89d
    aput-object v3, v2, v5

    #@89f
    const-string v3, "ims.kt.com"

    #@8a1
    aput-object v3, v2, v6

    #@8a3
    aput-object v2, v0, v1

    #@8a5
    const/16 v1, 0x12

    #@8a7
    new-array v2, v7, [Ljava/lang/String;

    #@8a9
    const-string v3, "TEXT"

    #@8ab
    aput-object v3, v2, v4

    #@8ad
    const-string v3, "subscriber_0_impi"

    #@8af
    aput-object v3, v2, v5

    #@8b1
    const-string v3, "450086020001592@ims.kt.com"

    #@8b3
    aput-object v3, v2, v6

    #@8b5
    aput-object v2, v0, v1

    #@8b7
    const/16 v1, 0x13

    #@8b9
    new-array v2, v7, [Ljava/lang/String;

    #@8bb
    const-string v3, "INTEGER"

    #@8bd
    aput-object v3, v2, v4

    #@8bf
    const-string v3, "subscriber_0_impu_primary_ref_index"

    #@8c1
    aput-object v3, v2, v5

    #@8c3
    const-string v3, "0"

    #@8c5
    aput-object v3, v2, v6

    #@8c7
    aput-object v2, v0, v1

    #@8c9
    const/16 v1, 0x14

    #@8cb
    new-array v2, v7, [Ljava/lang/String;

    #@8cd
    const-string v3, "INTEGER"

    #@8cf
    aput-object v3, v2, v4

    #@8d1
    const-string v3, "subscriber_0_impu_count"

    #@8d3
    aput-object v3, v2, v5

    #@8d5
    const-string v3, "3"

    #@8d7
    aput-object v3, v2, v6

    #@8d9
    aput-object v2, v0, v1

    #@8db
    const/16 v1, 0x15

    #@8dd
    new-array v2, v7, [Ljava/lang/String;

    #@8df
    const-string v3, "TEXT"

    #@8e1
    aput-object v3, v2, v4

    #@8e3
    const-string v3, "subscriber_0_impu_0"

    #@8e5
    aput-object v3, v2, v5

    #@8e7
    const-string v3, "sip:01032164404@ims.kt.com"

    #@8e9
    aput-object v3, v2, v6

    #@8eb
    aput-object v2, v0, v1

    #@8ed
    const/16 v1, 0x16

    #@8ef
    new-array v2, v7, [Ljava/lang/String;

    #@8f1
    const-string v3, "TEXT"

    #@8f3
    aput-object v3, v2, v4

    #@8f5
    const-string v3, "subscriber_0_impu_1"

    #@8f7
    aput-object v3, v2, v5

    #@8f9
    const-string v3, "sip:01032164404@ims.kt.com"

    #@8fb
    aput-object v3, v2, v6

    #@8fd
    aput-object v2, v0, v1

    #@8ff
    const/16 v1, 0x17

    #@901
    new-array v2, v7, [Ljava/lang/String;

    #@903
    const-string v3, "TEXT"

    #@905
    aput-object v3, v2, v4

    #@907
    const-string v3, "subscriber_0_impu_2"

    #@909
    aput-object v3, v2, v5

    #@90b
    const-string v3, "tel:*"

    #@90d
    aput-object v3, v2, v6

    #@90f
    aput-object v2, v0, v1

    #@911
    const/16 v1, 0x18

    #@913
    new-array v2, v7, [Ljava/lang/String;

    #@915
    const-string v3, "TEXT"

    #@917
    aput-object v3, v2, v4

    #@919
    const-string v3, "subscriber_0_phone_context"

    #@91b
    aput-object v3, v2, v5

    #@91d
    const-string v3, "ims.kt.com"

    #@91f
    aput-object v3, v2, v6

    #@921
    aput-object v2, v0, v1

    #@923
    const/16 v1, 0x19

    #@925
    new-array v2, v7, [Ljava/lang/String;

    #@927
    const-string v3, "TEXT"

    #@929
    aput-object v3, v2, v4

    #@92b
    const-string v3, "subscriber_0_auth_username"

    #@92d
    aput-object v3, v2, v5

    #@92f
    const-string v3, ""

    #@931
    aput-object v3, v2, v6

    #@933
    aput-object v2, v0, v1

    #@935
    const/16 v1, 0x1a

    #@937
    new-array v2, v7, [Ljava/lang/String;

    #@939
    const-string v3, "TEXT"

    #@93b
    aput-object v3, v2, v4

    #@93d
    const-string v3, "subscriber_0_auth_password"

    #@93f
    aput-object v3, v2, v5

    #@941
    const-string v3, "827CCB0EEA8A706C4C34A16891F84E7B"

    #@943
    aput-object v3, v2, v6

    #@945
    aput-object v2, v0, v1

    #@947
    const/16 v1, 0x1b

    #@949
    new-array v2, v7, [Ljava/lang/String;

    #@94b
    const-string v3, "TEXT"

    #@94d
    aput-object v3, v2, v4

    #@94f
    const-string v3, "subscriber_0_auth_realm"

    #@951
    aput-object v3, v2, v5

    #@953
    const-string v3, ""

    #@955
    aput-object v3, v2, v6

    #@957
    aput-object v2, v0, v1

    #@959
    const/16 v1, 0x1c

    #@95b
    new-array v2, v7, [Ljava/lang/String;

    #@95d
    const-string v3, "TEXT"

    #@95f
    aput-object v3, v2, v4

    #@961
    const-string v3, "subscriber_0_auth_algorithm"

    #@963
    aput-object v3, v2, v5

    #@965
    const-string v3, "AKAv1-MD5"

    #@967
    aput-object v3, v2, v6

    #@969
    aput-object v2, v0, v1

    #@96b
    const/16 v1, 0x1d

    #@96d
    new-array v2, v7, [Ljava/lang/String;

    #@96f
    const-string v3, "TEXT"

    #@971
    aput-object v3, v2, v4

    #@973
    const-string v3, "subscriber_0_auth_realm_leniency"

    #@975
    aput-object v3, v2, v5

    #@977
    const-string v3, "true"

    #@979
    aput-object v3, v2, v6

    #@97b
    aput-object v2, v0, v1

    #@97d
    const/16 v1, 0x1e

    #@97f
    new-array v2, v7, [Ljava/lang/String;

    #@981
    const-string v3, "TEXT"

    #@983
    aput-object v3, v2, v4

    #@985
    const-string v3, "subscriber_0_server_scscf"

    #@987
    aput-object v3, v2, v5

    #@989
    const-string v3, "ims.kt.com"

    #@98b
    aput-object v3, v2, v6

    #@98d
    aput-object v2, v0, v1

    #@98f
    sput-object v0, Lcom/lge/ims/provider/Config;->SUBSCRIBER_AKA:[[Ljava/lang/String;

    #@991
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 18
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public getTableContent(Ljava/lang/String;)[[Ljava/lang/String;
    .registers 4
    .parameter "name"

    #@0
    .prologue
    .line 377
    const-string v0, "lgims_aos"

    #@2
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_b

    #@8
    .line 378
    sget-object v0, Lcom/lge/ims/provider/Config;->AOS:[[Ljava/lang/String;

    #@a
    .line 404
    :goto_a
    return-object v0

    #@b
    .line 379
    :cond_b
    const-string v0, "lgims_aosconnection"

    #@d
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@10
    move-result v0

    #@11
    if-eqz v0, :cond_16

    #@13
    .line 380
    sget-object v0, Lcom/lge/ims/provider/Config;->AOSCONNECTION:[[Ljava/lang/String;

    #@15
    goto :goto_a

    #@16
    .line 381
    :cond_16
    const-string v0, "lgims_aosreg"

    #@18
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1b
    move-result v0

    #@1c
    if-eqz v0, :cond_21

    #@1e
    .line 382
    sget-object v0, Lcom/lge/ims/provider/Config;->AOSREG:[[Ljava/lang/String;

    #@20
    goto :goto_a

    #@21
    .line 383
    :cond_21
    const-string v0, "lgims_config"

    #@23
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@26
    move-result v0

    #@27
    if-eqz v0, :cond_2c

    #@29
    .line 384
    sget-object v0, Lcom/lge/ims/provider/Config;->CONFIG:[[Ljava/lang/String;

    #@2b
    goto :goto_a

    #@2c
    .line 385
    :cond_2c
    const-string v0, "lgims_engine"

    #@2e
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@31
    move-result v0

    #@32
    if-eqz v0, :cond_44

    #@34
    .line 386
    const-string v0, "user"

    #@36
    sget-object v1, Lcom/lge/ims/Configuration;->BUILD_TYPE:Ljava/lang/String;

    #@38
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@3b
    move-result v0

    #@3c
    if-eqz v0, :cond_41

    #@3e
    .line 387
    sget-object v0, Lcom/lge/ims/provider/Config;->ENGINE_DEBUG:[[Ljava/lang/String;

    #@40
    goto :goto_a

    #@41
    .line 389
    :cond_41
    sget-object v0, Lcom/lge/ims/provider/Config;->ENGINE_DEBUG:[[Ljava/lang/String;

    #@43
    goto :goto_a

    #@44
    .line 391
    :cond_44
    const-string v0, "lgims_media"

    #@46
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@49
    move-result v0

    #@4a
    if-eqz v0, :cond_4f

    #@4c
    .line 392
    sget-object v0, Lcom/lge/ims/provider/Config;->MEDIA:[[Ljava/lang/String;

    #@4e
    goto :goto_a

    #@4f
    .line 393
    :cond_4f
    const-string v0, "lgims_netpolicy"

    #@51
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@54
    move-result v0

    #@55
    if-eqz v0, :cond_5a

    #@57
    .line 394
    sget-object v0, Lcom/lge/ims/provider/Config;->NETPOLICY:[[Ljava/lang/String;

    #@59
    goto :goto_a

    #@5a
    .line 395
    :cond_5a
    const-string v0, "lgims_sip"

    #@5c
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@5f
    move-result v0

    #@60
    if-eqz v0, :cond_65

    #@62
    .line 396
    sget-object v0, Lcom/lge/ims/provider/Config;->SIP:[[Ljava/lang/String;

    #@64
    goto :goto_a

    #@65
    .line 397
    :cond_65
    const-string v0, "lgims_subscriber"

    #@67
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@6a
    move-result v0

    #@6b
    if-eqz v0, :cond_87

    #@6d
    .line 398
    sget-object v0, Lcom/lge/ims/Configuration;->MODEL:Ljava/lang/String;

    #@6f
    const-string v1, "F180"

    #@71
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@74
    move-result v0

    #@75
    if-nez v0, :cond_81

    #@77
    sget-object v0, Lcom/lge/ims/Configuration;->MODEL:Ljava/lang/String;

    #@79
    const-string v1, "F200"

    #@7b
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@7e
    move-result v0

    #@7f
    if-eqz v0, :cond_84

    #@81
    .line 399
    :cond_81
    sget-object v0, Lcom/lge/ims/provider/Config;->SUBSCRIBER:[[Ljava/lang/String;

    #@83
    goto :goto_a

    #@84
    .line 401
    :cond_84
    sget-object v0, Lcom/lge/ims/provider/Config;->SUBSCRIBER_AKA:[[Ljava/lang/String;

    #@86
    goto :goto_a

    #@87
    .line 404
    :cond_87
    const/4 v0, 0x0

    #@88
    check-cast v0, [[Ljava/lang/String;

    #@8a
    goto :goto_a
.end method

.method public getTables()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 369
    sget-object v0, Lcom/lge/ims/provider/Config;->TABLES:[Ljava/lang/String;

    #@2
    return-object v0
.end method
