.class public Lcom/lge/ims/provider/ip/ConfigIP;
.super Ljava/lang/Object;
.source "ConfigIP.java"

# interfaces
.implements Lcom/lge/ims/provider/IConfiguration;


# static fields
.field private static final APP:[[Ljava/lang/String;

.field private static final HTTP_IPXDM:[[Ljava/lang/String;

.field private static final SERVICE_IP:[[Ljava/lang/String;

.field private static final SERVICE_IPXDM:[[Ljava/lang/String;

.field private static final TABLES:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 9

    #@0
    .prologue
    const/4 v8, 0x4

    #@1
    const/4 v7, 0x3

    #@2
    const/4 v6, 0x2

    #@3
    const/4 v5, 0x1

    #@4
    const/4 v4, 0x0

    #@5
    .line 20
    new-array v0, v8, [Ljava/lang/String;

    #@7
    const-string v1, "lgims_com_kt_app_rcse_ip"

    #@9
    aput-object v1, v0, v4

    #@b
    const-string v1, "lgims_com_kt_service_rcse_ip"

    #@d
    aput-object v1, v0, v5

    #@f
    const-string v1, "lgims_com_kt_service_rcse_ipxdm"

    #@11
    aput-object v1, v0, v6

    #@13
    const-string v1, "lgims_com_kt_http_rcse_ipxdm"

    #@15
    aput-object v1, v0, v7

    #@17
    sput-object v0, Lcom/lge/ims/provider/ip/ConfigIP;->TABLES:[Ljava/lang/String;

    #@19
    .line 30
    new-array v0, v7, [[Ljava/lang/String;

    #@1b
    new-array v1, v7, [Ljava/lang/String;

    #@1d
    const-string v2, "INTEGER"

    #@1f
    aput-object v2, v1, v4

    #@21
    const-string v2, "id"

    #@23
    aput-object v2, v1, v5

    #@25
    const-string v2, "1"

    #@27
    aput-object v2, v1, v6

    #@29
    aput-object v1, v0, v4

    #@2b
    new-array v1, v7, [Ljava/lang/String;

    #@2d
    const-string v2, "INTEGER"

    #@2f
    aput-object v2, v1, v4

    #@31
    const-string v2, "property"

    #@33
    aput-object v2, v1, v5

    #@35
    const-string v2, "0"

    #@37
    aput-object v2, v1, v6

    #@39
    aput-object v1, v0, v5

    #@3b
    new-array v1, v7, [Ljava/lang/String;

    #@3d
    const-string v2, "TEXT"

    #@3f
    aput-object v2, v1, v4

    #@41
    const-string v2, "conf"

    #@43
    aput-object v2, v1, v5

    #@45
    const-string v2, "[Uniqueness]\nStream=0\nFramed=0\nBasic=0\nEvent=1\nCoreService=4\nQos=0\nReg=4\nWrite=1\nRead=1\nCap=0\nMprof=0\nConnection=0\nIMService=0\nPresenceService=1\nXDMService=1\n\n[IMSRegistry]\nIMSRegistry=lgims.com.kt.app.rcse.ip\n\n[Event]\npackage_names=presence\n\n[CoreService_0]\nservice_id=lgims.com.kt.service.rcse.ip\niari=\nicsi=\nfeature_tag=\n\n[CoreService_1]\nservice_id=lgims.com.kt.service.rcse.ipxdm\niari=\nicsi=\nfeature_tag=\n\n[CoreService_2]\nservice_id=lgims.com.kt.service.rcse.ip.wifi\niari=\nicsi=\nfeature_tag=\n\n[CoreService_3]\nservice_id=lgims.com.kt.service.rcse.ipxdm.wifi\niari=\nicsi=\nfeature_tag=\n\n[Reg_0]\nservice_id=lgims.com.kt.service.rcse.ip\nheader_count=1\nheader_0=Supported: path\n\n[Reg_1]\nservice_id=lgims.com.kt.service.rcse.ipxdm\nheader_count=1\nheader_0=Supported: path\n\n[Reg_2]\nservice_id=lgims.com.kt.service.rcse.ip.wifi\nheader_count=1\nheader_0=Supported: path\n\n[Reg_3]\nservice_id=lgims.com.kt.service.rcse.ipxdm.wifi\nheader_count=1\nheader_0=Supported: path\n\n[Write]\nheader_names=\n\n[Read]\nheader_names=\n\n[PresenceService]\nservice_id=lgims.com.kt.service.rcse.ip\n\n[XDMService]\nservice_id=lgims.com.kt.service.rcse.ipxdm\n\n"

    #@47
    aput-object v2, v1, v6

    #@49
    aput-object v1, v0, v6

    #@4b
    sput-object v0, Lcom/lge/ims/provider/ip/ConfigIP;->APP:[[Ljava/lang/String;

    #@4d
    .line 119
    const/16 v0, 0x8

    #@4f
    new-array v0, v0, [[Ljava/lang/String;

    #@51
    new-array v1, v7, [Ljava/lang/String;

    #@53
    const-string v2, "INTEGER"

    #@55
    aput-object v2, v1, v4

    #@57
    const-string v2, "id"

    #@59
    aput-object v2, v1, v5

    #@5b
    const-string v2, "1"

    #@5d
    aput-object v2, v1, v6

    #@5f
    aput-object v1, v0, v4

    #@61
    new-array v1, v7, [Ljava/lang/String;

    #@63
    const-string v2, "INTEGER"

    #@65
    aput-object v2, v1, v4

    #@67
    const-string v2, "property"

    #@69
    aput-object v2, v1, v5

    #@6b
    const-string v2, "1"

    #@6d
    aput-object v2, v1, v6

    #@6f
    aput-object v1, v0, v5

    #@71
    new-array v1, v7, [Ljava/lang/String;

    #@73
    const-string v2, "INTEGER"

    #@75
    aput-object v2, v1, v4

    #@77
    const-string v2, "application_client_object_data_limit"

    #@79
    aput-object v2, v1, v5

    #@7b
    const-string v2, "8"

    #@7d
    aput-object v2, v1, v6

    #@7f
    aput-object v1, v0, v6

    #@81
    new-array v1, v7, [Ljava/lang/String;

    #@83
    const-string v2, "TEXT"

    #@85
    aput-object v2, v1, v4

    #@87
    const-string v2, "application_content_server_uri"

    #@89
    aput-object v2, v1, v5

    #@8b
    const-string v2, "http://221.148.247.125:443"

    #@8d
    aput-object v2, v1, v6

    #@8f
    aput-object v1, v0, v7

    #@91
    new-array v1, v7, [Ljava/lang/String;

    #@93
    const-string v2, "INTEGER"

    #@95
    aput-object v2, v1, v4

    #@97
    const-string v2, "application_source_throttle_publish"

    #@99
    aput-object v2, v1, v5

    #@9b
    const-string v2, "30"

    #@9d
    aput-object v2, v1, v6

    #@9f
    aput-object v1, v0, v8

    #@a1
    const/4 v1, 0x5

    #@a2
    new-array v2, v7, [Ljava/lang/String;

    #@a4
    const-string v3, "INTEGER"

    #@a6
    aput-object v3, v2, v4

    #@a8
    const-string v3, "application_max_backend_subscription"

    #@aa
    aput-object v3, v2, v5

    #@ac
    const-string v3, "5"

    #@ae
    aput-object v3, v2, v6

    #@b0
    aput-object v2, v0, v1

    #@b2
    const/4 v1, 0x6

    #@b3
    new-array v2, v7, [Ljava/lang/String;

    #@b5
    const-string v3, "TEXT"

    #@b7
    aput-object v3, v2, v4

    #@b9
    const-string v3, "application_service_uri_template"

    #@bb
    aput-object v3, v2, v5

    #@bd
    const-string v3, "sip:uri@ims.kt.com"

    #@bf
    aput-object v3, v2, v6

    #@c1
    aput-object v2, v0, v1

    #@c3
    const/4 v1, 0x7

    #@c4
    new-array v2, v7, [Ljava/lang/String;

    #@c6
    const-string v3, "TEXT"

    #@c8
    aput-object v3, v2, v4

    #@ca
    const-string v3, "application_rls_list"

    #@cc
    aput-object v3, v2, v5

    #@ce
    const-string v3, "sip:rls@ims.kt.com"

    #@d0
    aput-object v3, v2, v6

    #@d2
    aput-object v2, v0, v1

    #@d4
    sput-object v0, Lcom/lge/ims/provider/ip/ConfigIP;->SERVICE_IP:[[Ljava/lang/String;

    #@d6
    .line 134
    const/16 v0, 0x8

    #@d8
    new-array v0, v0, [[Ljava/lang/String;

    #@da
    new-array v1, v7, [Ljava/lang/String;

    #@dc
    const-string v2, "INTEGER"

    #@de
    aput-object v2, v1, v4

    #@e0
    const-string v2, "id"

    #@e2
    aput-object v2, v1, v5

    #@e4
    const-string v2, "1"

    #@e6
    aput-object v2, v1, v6

    #@e8
    aput-object v1, v0, v4

    #@ea
    new-array v1, v7, [Ljava/lang/String;

    #@ec
    const-string v2, "INTEGER"

    #@ee
    aput-object v2, v1, v4

    #@f0
    const-string v2, "property"

    #@f2
    aput-object v2, v1, v5

    #@f4
    const-string v2, "1"

    #@f6
    aput-object v2, v1, v6

    #@f8
    aput-object v1, v0, v5

    #@fa
    new-array v1, v7, [Ljava/lang/String;

    #@fc
    const-string v2, "TEXT"

    #@fe
    aput-object v2, v1, v4

    #@100
    const-string v2, "resource_root_uri"

    #@102
    aput-object v2, v1, v5

    #@104
    const-string v2, "221.148.247.125:9110"

    #@106
    aput-object v2, v1, v6

    #@108
    aput-object v1, v0, v6

    #@10a
    new-array v1, v7, [Ljava/lang/String;

    #@10c
    const-string v2, "TEXT"

    #@10e
    aput-object v2, v1, v4

    #@110
    const-string v2, "resource_auth_name"

    #@112
    aput-object v2, v1, v5

    #@114
    const-string v2, "01032164404"

    #@116
    aput-object v2, v1, v6

    #@118
    aput-object v1, v0, v7

    #@11a
    new-array v1, v7, [Ljava/lang/String;

    #@11c
    const-string v2, "TEXT"

    #@11e
    aput-object v2, v1, v4

    #@120
    const-string v2, "resource_auth_secret"

    #@122
    aput-object v2, v1, v5

    #@124
    const-string v2, "827CCB0EEA8A706C4C34A16891F84E7B"

    #@126
    aput-object v2, v1, v6

    #@128
    aput-object v1, v0, v8

    #@12a
    const/4 v1, 0x5

    #@12b
    new-array v2, v7, [Ljava/lang/String;

    #@12d
    const-string v3, "TEXT"

    #@12f
    aput-object v3, v2, v4

    #@131
    const-string v3, "resource_auth_type"

    #@133
    aput-object v3, v2, v5

    #@135
    const-string v3, "HTTP-DIGEST"

    #@137
    aput-object v3, v2, v6

    #@139
    aput-object v2, v0, v1

    #@13b
    const/4 v1, 0x6

    #@13c
    new-array v2, v7, [Ljava/lang/String;

    #@13e
    const-string v3, "TEXT"

    #@140
    aput-object v3, v2, v4

    #@142
    const-string v3, "resource_http_config"

    #@144
    aput-object v3, v2, v5

    #@146
    const-string v3, "lgims.com.kt.http.rcse.ipxdm"

    #@148
    aput-object v3, v2, v6

    #@14a
    aput-object v2, v0, v1

    #@14c
    const/4 v1, 0x7

    #@14d
    new-array v2, v7, [Ljava/lang/String;

    #@14f
    const-string v3, "TEXT"

    #@151
    aput-object v3, v2, v4

    #@153
    const-string v3, "application_conf_uri_template"

    #@155
    aput-object v3, v2, v5

    #@157
    const-string v3, "sip:uri@ims.kt.com"

    #@159
    aput-object v3, v2, v6

    #@15b
    aput-object v2, v0, v1

    #@15d
    sput-object v0, Lcom/lge/ims/provider/ip/ConfigIP;->SERVICE_IPXDM:[[Ljava/lang/String;

    #@15f
    .line 150
    new-array v0, v8, [[Ljava/lang/String;

    #@161
    new-array v1, v7, [Ljava/lang/String;

    #@163
    const-string v2, "INTEGER"

    #@165
    aput-object v2, v1, v4

    #@167
    const-string v2, "id"

    #@169
    aput-object v2, v1, v5

    #@16b
    const-string v2, "1"

    #@16d
    aput-object v2, v1, v6

    #@16f
    aput-object v1, v0, v4

    #@171
    new-array v1, v7, [Ljava/lang/String;

    #@173
    const-string v2, "INTEGER"

    #@175
    aput-object v2, v1, v4

    #@177
    const-string v2, "property"

    #@179
    aput-object v2, v1, v5

    #@17b
    const-string v2, "1"

    #@17d
    aput-object v2, v1, v6

    #@17f
    aput-object v1, v0, v5

    #@181
    new-array v1, v7, [Ljava/lang/String;

    #@183
    const-string v2, "TEXT"

    #@185
    aput-object v2, v1, v4

    #@187
    const-string v2, "server_info_proxy_address"

    #@189
    aput-object v2, v1, v5

    #@18b
    const-string v2, "221.148.247.125"

    #@18d
    aput-object v2, v1, v6

    #@18f
    aput-object v1, v0, v6

    #@191
    new-array v1, v7, [Ljava/lang/String;

    #@193
    const-string v2, "INTEGER"

    #@195
    aput-object v2, v1, v4

    #@197
    const-string v2, "server_info_proxy_port"

    #@199
    aput-object v2, v1, v5

    #@19b
    const-string v2, "9110"

    #@19d
    aput-object v2, v1, v6

    #@19f
    aput-object v1, v0, v7

    #@1a1
    sput-object v0, Lcom/lge/ims/provider/ip/ConfigIP;->HTTP_IPXDM:[[Ljava/lang/String;

    #@1a3
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 18
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public getTableContent(Ljava/lang/String;)[[Ljava/lang/String;
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 171
    const-string v0, "lgims_com_kt_app_rcse_ip"

    #@2
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_b

    #@8
    .line 172
    sget-object v0, Lcom/lge/ims/provider/ip/ConfigIP;->APP:[[Ljava/lang/String;

    #@a
    .line 180
    :goto_a
    return-object v0

    #@b
    .line 173
    :cond_b
    const-string v0, "lgims_com_kt_service_rcse_ip"

    #@d
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@10
    move-result v0

    #@11
    if-eqz v0, :cond_16

    #@13
    .line 174
    sget-object v0, Lcom/lge/ims/provider/ip/ConfigIP;->SERVICE_IP:[[Ljava/lang/String;

    #@15
    goto :goto_a

    #@16
    .line 175
    :cond_16
    const-string v0, "lgims_com_kt_service_rcse_ipxdm"

    #@18
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1b
    move-result v0

    #@1c
    if-eqz v0, :cond_21

    #@1e
    .line 176
    sget-object v0, Lcom/lge/ims/provider/ip/ConfigIP;->SERVICE_IPXDM:[[Ljava/lang/String;

    #@20
    goto :goto_a

    #@21
    .line 177
    :cond_21
    const-string v0, "lgims_com_kt_http_rcse_ipxdm"

    #@23
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@26
    move-result v0

    #@27
    if-eqz v0, :cond_2c

    #@29
    .line 178
    sget-object v0, Lcom/lge/ims/provider/ip/ConfigIP;->HTTP_IPXDM:[[Ljava/lang/String;

    #@2b
    goto :goto_a

    #@2c
    .line 180
    :cond_2c
    const/4 v0, 0x0

    #@2d
    check-cast v0, [[Ljava/lang/String;

    #@2f
    goto :goto_a
.end method

.method public getTables()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 163
    sget-object v0, Lcom/lge/ims/provider/ip/ConfigIP;->TABLES:[Ljava/lang/String;

    #@2
    return-object v0
.end method
