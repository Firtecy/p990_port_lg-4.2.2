.class Lcom/lge/ims/provider/xdm/XDMProvider$XDMDatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "XDMProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/provider/xdm/XDMProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "XDMDatabaseHelper"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "objContext"

    #@0
    .prologue
    .line 62
    const-string v0, "ims_xdm.db"

    #@2
    const/4 v1, 0x0

    #@3
    const/4 v2, 0x1

    #@4
    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    #@7
    .line 63
    const-string v0, "XDMDatabaseHelper::XDMDatabaseHelper"

    #@9
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@c
    .line 64
    return-void
.end method


# virtual methods
.method CreateTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 3
    .parameter "objDb"

    #@0
    .prologue
    .line 104
    const-string v0, "CREATE TABLE IF NOT EXISTS ims_xdm(list_etag TEXT, rls_etag TEXT, rule_etag TEXT,pidf_etag TEXT,rls_uri TEXT);"

    #@2
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@5
    .line 110
    return-void
.end method

.method UpgradeDatabaseToVersion2(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 3
    .parameter "objDb"

    #@0
    .prologue
    .line 99
    const-string v0, "DROP TABLE IF EXISTS ims_xdm"

    #@2
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@5
    .line 100
    invoke-virtual {p0, p1}, Lcom/lge/ims/provider/xdm/XDMProvider$XDMDatabaseHelper;->CreateTable(Landroid/database/sqlite/SQLiteDatabase;)V

    #@8
    .line 101
    return-void
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 3
    .parameter "objDb"

    #@0
    .prologue
    .line 68
    const-string v0, "XDMDatabaseHelper::onCreate"

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 69
    const-string v0, "CREATE TABLE IF NOT EXISTS ims_xdm(list_etag TEXT, rls_etag TEXT, rule_etag TEXT,pidf_etag TEXT,rls_uri TEXT);"

    #@7
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@a
    .line 75
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .registers 6
    .parameter "objDb"
    .parameter "nOldVersion"
    .parameter "nNewVersion"

    #@0
    .prologue
    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "XDMDatabaseHelper::onUpgrade : Old"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    const-string v1, " / New : "

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@20
    .line 80
    if-nez p1, :cond_23

    #@22
    .line 96
    :cond_22
    :goto_22
    return-void

    #@23
    .line 84
    :cond_23
    if-ge p2, p3, :cond_22

    #@25
    .line 88
    packed-switch p2, :pswitch_data_2e

    #@28
    goto :goto_22

    #@29
    .line 91
    :pswitch_29
    invoke-virtual {p0, p1}, Lcom/lge/ims/provider/xdm/XDMProvider$XDMDatabaseHelper;->UpgradeDatabaseToVersion2(Landroid/database/sqlite/SQLiteDatabase;)V

    #@2c
    goto :goto_22

    #@2d
    .line 88
    nop

    #@2e
    :pswitch_data_2e
    .packed-switch 0x1
        :pswitch_29
    .end packed-switch
.end method
