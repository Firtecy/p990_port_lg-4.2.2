.class public Lcom/lge/ims/provider/ConfigurationManager;
.super Ljava/lang/Object;
.source "ConfigurationManager.java"


# static fields
.field private static mConfigMngr:Lcom/lge/ims/provider/ConfigurationManager;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 24
    const/4 v0, 0x0

    #@1
    sput-object v0, Lcom/lge/ims/provider/ConfigurationManager;->mConfigMngr:Lcom/lge/ims/provider/ConfigurationManager;

    #@3
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 23
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static getInstance()Lcom/lge/ims/provider/ConfigurationManager;
    .registers 1

    #@0
    .prologue
    .line 32
    sget-object v0, Lcom/lge/ims/provider/ConfigurationManager;->mConfigMngr:Lcom/lge/ims/provider/ConfigurationManager;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 33
    new-instance v0, Lcom/lge/ims/provider/ConfigurationManager;

    #@6
    invoke-direct {v0}, Lcom/lge/ims/provider/ConfigurationManager;-><init>()V

    #@9
    sput-object v0, Lcom/lge/ims/provider/ConfigurationManager;->mConfigMngr:Lcom/lge/ims/provider/ConfigurationManager;

    #@b
    .line 36
    :cond_b
    sget-object v0, Lcom/lge/ims/provider/ConfigurationManager;->mConfigMngr:Lcom/lge/ims/provider/ConfigurationManager;

    #@d
    return-object v0
.end method


# virtual methods
.method public getConfigurations()Ljava/util/ArrayList;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/lge/ims/provider/IConfiguration;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 43
    new-instance v0, Ljava/util/ArrayList;

    #@2
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 46
    .local v0, configs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/lge/ims/provider/IConfiguration;>;"
    new-instance v1, Lcom/lge/ims/provider/Config;

    #@7
    invoke-direct {v1}, Lcom/lge/ims/provider/Config;-><init>()V

    #@a
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@d
    .line 49
    new-instance v1, Lcom/lge/ims/provider/ConfigCOM;

    #@f
    invoke-direct {v1}, Lcom/lge/ims/provider/ConfigCOM;-><init>()V

    #@12
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@15
    .line 54
    new-instance v1, Lcom/lge/ims/provider/uc/ConfigUC;

    #@17
    invoke-direct {v1}, Lcom/lge/ims/provider/uc/ConfigUC;-><init>()V

    #@1a
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1d
    .line 57
    new-instance v1, Lcom/lge/ims/provider/uc/ConfigMedia;

    #@1f
    invoke-direct {v1}, Lcom/lge/ims/provider/uc/ConfigMedia;-><init>()V

    #@22
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@25
    .line 75
    return-object v0
.end method
