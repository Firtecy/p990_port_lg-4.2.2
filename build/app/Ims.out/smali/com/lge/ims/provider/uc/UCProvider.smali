.class public Lcom/lge/ims/provider/uc/UCProvider;
.super Landroid/content/ContentProvider;
.source "UCProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/provider/uc/UCProvider$DatabaseHelper;
    }
.end annotation


# static fields
.field private static final DATABASE_NAME:Ljava/lang/String; = "ims_uc.db"

.field private static final DATABASE_VERSION:I = 0x3

.field protected static final PM_UC:Ljava/util/HashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "LGIMS"

.field protected static final URI_MATCHER:Landroid/content/UriMatcher; = null

.field protected static final URI_UC:I = 0x1


# instance fields
.field protected mDB:Landroid/database/sqlite/SQLiteDatabase;

.field protected mOpenHelper:Lcom/lge/ims/provider/uc/UCProvider$DatabaseHelper;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    #@0
    .prologue
    .line 267
    new-instance v2, Landroid/content/UriMatcher;

    #@2
    const/4 v3, -0x1

    #@3
    invoke-direct {v2, v3}, Landroid/content/UriMatcher;-><init>(I)V

    #@6
    sput-object v2, Lcom/lge/ims/provider/uc/UCProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@8
    .line 268
    sget-object v2, Lcom/lge/ims/provider/uc/UCProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@a
    const-string v3, "com.lge.ims.provider.uc"

    #@c
    const-string v4, "ucstate"

    #@e
    const/4 v5, 0x1

    #@f
    invoke-virtual {v2, v3, v4, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@12
    .line 270
    new-instance v2, Ljava/util/HashMap;

    #@14
    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    #@17
    sput-object v2, Lcom/lge/ims/provider/uc/UCProvider;->PM_UC:Ljava/util/HashMap;

    #@19
    .line 272
    invoke-static {}, Lcom/lge/ims/provider/uc/UC$Data;->getUCStr()[Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    .line 274
    .local v1, strUc:[Ljava/lang/String;
    const/4 v0, 0x0

    #@1e
    .local v0, i:I
    :goto_1e
    array-length v2, v1

    #@1f
    if-ge v0, v2, :cond_2d

    #@21
    .line 275
    sget-object v2, Lcom/lge/ims/provider/uc/UCProvider;->PM_UC:Ljava/util/HashMap;

    #@23
    aget-object v3, v1, v0

    #@25
    aget-object v4, v1, v0

    #@27
    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2a
    .line 274
    add-int/lit8 v0, v0, 0x1

    #@2c
    goto :goto_1e

    #@2d
    .line 277
    :cond_2d
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 32
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    #@3
    .line 55
    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 6
    .parameter "arg0"
    .parameter "arg1"
    .parameter "arg2"

    #@0
    .prologue
    .line 171
    const-string v0, "LGIMS"

    #@2
    const-string v1, "DELETE query is not implemented"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 172
    const/4 v0, 0x0

    #@8
    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .registers 5
    .parameter "uri"

    #@0
    .prologue
    .line 177
    sget-object v0, Lcom/lge/ims/provider/uc/UCProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@5
    move-result v0

    #@6
    packed-switch v0, :pswitch_data_26

    #@9
    .line 181
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@b
    new-instance v1, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v2, "Unsupported URI: "

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@21
    throw v0

    #@22
    .line 179
    :pswitch_22
    const-string v0, "vnd.android.cursor.dir/com.lge.ims.provider.uc.ucstate"

    #@24
    return-object v0

    #@25
    .line 177
    nop

    #@26
    :pswitch_data_26
    .packed-switch 0x1
        :pswitch_22
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .registers 4
    .parameter "uri"
    .parameter "values"

    #@0
    .prologue
    .line 187
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public onCreate()Z
    .registers 6

    #@0
    .prologue
    .line 193
    const-string v0, "LGIMS"

    #@2
    const-string v1, "UCProvider is created ..."

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 196
    new-instance v0, Lcom/lge/ims/provider/uc/UCProvider$DatabaseHelper;

    #@9
    invoke-virtual {p0}, Lcom/lge/ims/provider/uc/UCProvider;->getContext()Landroid/content/Context;

    #@c
    move-result-object v1

    #@d
    const-string v2, "ims_uc.db"

    #@f
    const/4 v3, 0x0

    #@10
    const/4 v4, 0x3

    #@11
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/lge/ims/provider/uc/UCProvider$DatabaseHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    #@14
    iput-object v0, p0, Lcom/lge/ims/provider/uc/UCProvider;->mOpenHelper:Lcom/lge/ims/provider/uc/UCProvider$DatabaseHelper;

    #@16
    .line 198
    iget-object v0, p0, Lcom/lge/ims/provider/uc/UCProvider;->mOpenHelper:Lcom/lge/ims/provider/uc/UCProvider$DatabaseHelper;

    #@18
    invoke-virtual {v0}, Lcom/lge/ims/provider/uc/UCProvider$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@1b
    move-result-object v0

    #@1c
    iput-object v0, p0, Lcom/lge/ims/provider/uc/UCProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    #@1e
    .line 200
    iget-object v0, p0, Lcom/lge/ims/provider/uc/UCProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    #@20
    if-nez v0, :cond_24

    #@22
    .line 201
    const/4 v0, 0x0

    #@23
    .line 204
    :goto_23
    return v0

    #@24
    :cond_24
    const/4 v0, 0x1

    #@25
    goto :goto_23
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 15
    .parameter "uri"
    .parameter "projection"
    .parameter "selection"
    .parameter "selectionArgs"
    .parameter "sortOrder"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 210
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    #@3
    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    #@6
    .line 212
    .local v0, queryBuilder:Landroid/database/sqlite/SQLiteQueryBuilder;
    sget-object v1, Lcom/lge/ims/provider/uc/UCProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@8
    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@b
    move-result v1

    #@c
    packed-switch v1, :pswitch_data_4e

    #@f
    .line 219
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@11
    new-instance v2, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v3, "Unknown URI "

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@27
    throw v1

    #@28
    .line 214
    :pswitch_28
    const-string v1, "ucstate"

    #@2a
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    #@2d
    .line 215
    sget-object v1, Lcom/lge/ims/provider/uc/UCProvider;->PM_UC:Ljava/util/HashMap;

    #@2f
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    #@32
    .line 222
    iget-object v1, p0, Lcom/lge/ims/provider/uc/UCProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    #@34
    move-object v2, p2

    #@35
    move-object v3, p3

    #@36
    move-object v4, p4

    #@37
    move-object v6, v5

    #@38
    move-object v7, v5

    #@39
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@3c
    move-result-object v8

    #@3d
    .line 223
    .local v8, cursor:Landroid/database/Cursor;
    const-string v1, "LGIMS"

    #@3f
    const-string v2, "Querying ..."

    #@41
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 225
    if-nez v8, :cond_4d

    #@46
    .line 226
    const-string v1, "LGIMS"

    #@48
    const-string v2, "Cursor is null"

    #@4a
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    .line 229
    :cond_4d
    return-object v8

    #@4e
    .line 212
    :pswitch_data_4e
    .packed-switch 0x1
        :pswitch_28
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 11
    .parameter "uri"
    .parameter "values"
    .parameter "where"
    .parameter "whereArgs"

    #@0
    .prologue
    .line 234
    const/4 v2, 0x0

    #@1
    .line 236
    .local v2, tableName:Ljava/lang/String;
    sget-object v3, Lcom/lge/ims/provider/uc/UCProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@3
    invoke-virtual {v3, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@6
    move-result v3

    #@7
    packed-switch v3, :pswitch_data_4e

    #@a
    .line 242
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@c
    new-instance v4, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v5, "Unknown URI "

    #@13
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v4

    #@1f
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@22
    throw v3

    #@23
    .line 238
    :pswitch_23
    const-string v2, "ucstate"

    #@25
    .line 245
    if-nez v2, :cond_29

    #@27
    .line 246
    const/4 v0, 0x0

    #@28
    .line 263
    :cond_28
    :goto_28
    return v0

    #@29
    .line 249
    :cond_29
    const/4 v0, -0x1

    #@2a
    .line 252
    .local v0, count:I
    :try_start_2a
    iget-object v3, p0, Lcom/lge/ims/provider/uc/UCProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    #@2c
    invoke-virtual {v3, v2, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@2f
    move-result v0

    #@30
    .line 253
    invoke-virtual {p0}, Lcom/lge/ims/provider/uc/UCProvider;->getContext()Landroid/content/Context;

    #@33
    move-result-object v3

    #@34
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@37
    move-result-object v3

    #@38
    const/4 v4, 0x0

    #@39
    invoke-virtual {v3, p1, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_3c
    .catchall {:try_start_2a .. :try_end_3c} :catchall_4c
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2a .. :try_end_3c} :catch_3d

    #@3c
    goto :goto_28

    #@3d
    .line 254
    :catch_3d
    move-exception v1

    #@3e
    .line 255
    .local v1, e:Landroid/database/sqlite/SQLiteException;
    :try_start_3e
    invoke-static {v1}, Landroid/database/sqlite/SqliteWrapper;->isLowMemory(Landroid/database/sqlite/SQLiteException;)Z

    #@41
    move-result v3

    #@42
    if-eqz v3, :cond_28

    #@44
    .line 256
    const-string v3, "LGIMS"

    #@46
    const-string v4, "intenal memory storage is Full"

    #@48
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4b
    .catchall {:try_start_3e .. :try_end_4b} :catchall_4c

    #@4b
    goto :goto_28

    #@4c
    .line 258
    .end local v1           #e:Landroid/database/sqlite/SQLiteException;
    :catchall_4c
    move-exception v3

    #@4d
    throw v3

    #@4e
    .line 236
    :pswitch_data_4e
    .packed-switch 0x1
        :pswitch_23
    .end packed-switch
.end method
