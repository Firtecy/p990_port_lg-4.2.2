.class public final Lcom/lge/ims/provider/ip/IPProviderMetaData$IPPresence;
.super Ljava/lang/Object;
.source "IPProviderMetaData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/provider/ip/IPProviderMetaData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "IPPresence"
.end annotation


# static fields
.field public static final CAPA_STATUS_EXPIRED_RCS_USER:I = 0x3

.field public static final CAPA_STATUS_NONE:I = 0x0

.field public static final CAPA_STATUS_NOT_RCS_USER:I = 0x2

.field public static final CAPA_STATUS_RCS_USER:I = 0x1

.field public static final CONTENT_CAPABILITY_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/com.lge.ims.ip.presence"

.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final INDEX_KEY_BIRTHDAY:I = 0x5

.field public static final INDEX_KEY_CYWORLDACCOUNT:I = 0x9

.field public static final INDEX_KEY_EMAIL:I = 0x4

.field public static final INDEX_KEY_FACEBOOKACCOUNT:I = 0x8

.field public static final INDEX_KEY_FREETEXT:I = 0x3

.field public static final INDEX_KEY_HOMEPAGE:I = 0x2

.field public static final INDEX_KEY_ID:I = 0x0

.field public static final INDEX_KEY_NORMAL_MSISDN:I = 0x1

.field public static final INDEX_KEY_STATUS:I = 0x6

.field public static final INDEX_KEY_STATUSICON:I = 0xb

.field public static final INDEX_KEY_STATUSICON_LINK:I = 0xc

.field public static final INDEX_KEY_STATUSICON_THUMB:I = 0xd

.field public static final INDEX_KEY_STATUSICON_THUMB_ETAG:I = 0xf

.field public static final INDEX_KEY_STATUSICON_THUMB_LINK:I = 0xe

.field public static final INDEX_KEY_STATUSICON_UPDATE:I = 0x11

.field public static final INDEX_KEY_TIME_STAMP:I = 0x10

.field public static final INDEX_KEY_TWITTERACCOUNT:I = 0x7

.field public static final INDEX_KEY_WAGGLEACCOUNT:I = 0xa

.field public static final KEY_BIRTHDAY:Ljava/lang/String; = "birthday"

.field public static final KEY_CYWORLDACCOUNT:Ljava/lang/String; = "cyworld_account"

.field public static final KEY_EMAIL:Ljava/lang/String; = "e_mail"

.field public static final KEY_FACEBOOKACCOUNT:Ljava/lang/String; = "facebook_account"

.field public static final KEY_FREETEXT:Ljava/lang/String; = "free_text"

.field public static final KEY_HOMEPAGE:Ljava/lang/String; = "homepage"

.field public static final KEY_ID:Ljava/lang/String; = "_id"

.field public static final KEY_NORMAL_MSISDN:Ljava/lang/String; = "nor_msisdn"

.field public static final KEY_STATUS:Ljava/lang/String; = "status"

.field public static final KEY_STATUSICON:Ljava/lang/String; = "status_icon"

.field public static final KEY_STATUSICON_LINK:Ljava/lang/String; = "status_icon_link"

.field public static final KEY_STATUSICON_THUMB:Ljava/lang/String; = "status_icon_thumb"

.field public static final KEY_STATUSICON_THUMB_ETAG:Ljava/lang/String; = "status_icon_thumb_etag"

.field public static final KEY_STATUSICON_THUMB_LINK:Ljava/lang/String; = "status_icon_thumb_link"

.field public static final KEY_STATUSICON_UPDATE:Ljava/lang/String; = "statusicon_update"

.field public static final KEY_TIME_STAMP:Ljava/lang/String; = "time_stamp"

.field public static final KEY_TWITTERACCOUNT:Ljava/lang/String; = "twitter_account"

.field public static final KEY_WAGGLEACCOUNT:Ljava/lang/String; = "waggle_account"

.field public static final TABLE_NAME:Ljava/lang/String; = "ip_presence"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 94
    const-string v0, "content://com.lge.ims.provider.ip/ip_presence"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/lge/ims/provider/ip/IPProviderMetaData$IPPresence;->CONTENT_URI:Landroid/net/Uri;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 84
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
