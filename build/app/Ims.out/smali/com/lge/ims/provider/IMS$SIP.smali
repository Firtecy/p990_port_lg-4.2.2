.class public final Lcom/lge/ims/provider/IMS$SIP;
.super Ljava/lang/Object;
.source "IMS.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/provider/IMS;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SIP"
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final FEATURE_TAGS:Ljava/lang/String; = "header_info_feature_tags"

.field public static final PORT:Ljava/lang/String; = "listen_channel_port"

.field public static final PREFERRED_ID:Ljava/lang/String; = "header_info_preferred_id"

.field public static final REG_EXPIRATION:Ljava/lang/String; = "reg_expiration"

.field public static final REG_SUBSCRIPTION:Ljava/lang/String; = "reg_subscription"

.field public static final REG_SUB_EXPIRATION:Ljava/lang/String; = "reg_sub_expiration"

.field public static final SERVICE_SHARED:Ljava/lang/String; = "service_shared"

.field public static final SERVICE_VERSION:Ljava/lang/String; = "header_info_service_version"

.field public static final ST_METHOD:Ljava/lang/String; = "session_st_method"

.field public static final ST_MINSE:Ljava/lang/String; = "session_st_minse"

.field public static final ST_REFRESHER:Ljava/lang/String; = "session_st_refresher"

.field public static final ST_SESSION_EXPIRES:Ljava/lang/String; = "session_st_session_expires"

.field public static final TABLE_NAME:Ljava/lang/String; = "lgims_sip"

.field public static final TARGET_SCHEME:Ljava/lang/String; = "header_info_target_scheme"

.field public static final TV_T1:Ljava/lang/String; = "timer_tv_t1"

.field public static final TV_T2:Ljava/lang/String; = "timer_tv_t2"

.field public static final TV_TB:Ljava/lang/String; = "timer_tv_tb"

.field public static final TV_TD:Ljava/lang/String; = "timer_tv_td"

.field public static final TV_TF:Ljava/lang/String; = "timer_tv_tf"

.field public static final TV_TH:Ljava/lang/String; = "timer_tv_th"

.field public static final TV_TI:Ljava/lang/String; = "timer_tv_ti"

.field public static final TV_TJ:Ljava/lang/String; = "timer_tv_tj"

.field public static final TV_TK:Ljava/lang/String; = "timer_tv_tk"

.field public static final UA_VERSION_SW_VERSION:Ljava/lang/String; = "ua_version_sw_version"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 84
    const-string v0, "content://com.lge.ims.provider.lgims/lgims_sip"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/lge/ims/provider/IMS$SIP;->CONTENT_URI:Landroid/net/Uri;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 78
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
