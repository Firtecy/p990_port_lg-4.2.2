.class Lcom/lge/ims/provider/ConfigurationProvider$DatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "ConfigurationProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/provider/ConfigurationProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DatabaseHelper"
.end annotation


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V
    .registers 5
    .parameter "context"
    .parameter "name"
    .parameter "factory"
    .parameter "version"

    #@0
    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    #@3
    .line 65
    return-void
.end method

.method private createTableWithContent(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[[Ljava/lang/String;)Z
    .registers 13
    .parameter "db"
    .parameter "table"
    .parameter "content"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 96
    if-nez p2, :cond_5

    #@4
    .line 146
    :cond_4
    :goto_4
    return v4

    #@5
    .line 100
    :cond_5
    if-eqz p3, :cond_4

    #@7
    array-length v6, p3

    #@8
    if-eqz v6, :cond_4

    #@a
    .line 105
    new-instance v6, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v7, "CREATE TABLE "

    #@11
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v6

    #@15
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v6

    #@19
    const-string v7, " ("

    #@1b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v6

    #@1f
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    .line 108
    .local v2, query:Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v6

    #@2c
    aget-object v7, p3, v4

    #@2e
    aget-object v7, v7, v5

    #@30
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v6

    #@34
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v2

    #@38
    .line 109
    new-instance v6, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v6

    #@41
    const-string v7, " "

    #@43
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v6

    #@47
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v2

    #@4b
    .line 110
    new-instance v6, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v6

    #@54
    aget-object v7, p3, v4

    #@56
    aget-object v7, v7, v4

    #@58
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v6

    #@5c
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v2

    #@60
    .line 112
    const/4 v1, 0x1

    #@61
    .local v1, i:I
    :goto_61
    array-length v6, p3

    #@62
    if-ge v1, v6, :cond_b7

    #@64
    .line 113
    new-instance v6, Ljava/lang/StringBuilder;

    #@66
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@69
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v6

    #@6d
    const-string v7, ","

    #@6f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v6

    #@73
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v2

    #@77
    .line 115
    new-instance v6, Ljava/lang/StringBuilder;

    #@79
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@7c
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v6

    #@80
    aget-object v7, p3, v1

    #@82
    aget-object v7, v7, v5

    #@84
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v6

    #@88
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8b
    move-result-object v2

    #@8c
    .line 116
    new-instance v6, Ljava/lang/StringBuilder;

    #@8e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@91
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v6

    #@95
    const-string v7, " "

    #@97
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v6

    #@9b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9e
    move-result-object v2

    #@9f
    .line 117
    new-instance v6, Ljava/lang/StringBuilder;

    #@a1
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@a4
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v6

    #@a8
    aget-object v7, p3, v1

    #@aa
    aget-object v7, v7, v4

    #@ac
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af
    move-result-object v6

    #@b0
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b3
    move-result-object v2

    #@b4
    .line 112
    add-int/lit8 v1, v1, 0x1

    #@b6
    goto :goto_61

    #@b7
    .line 120
    :cond_b7
    new-instance v6, Ljava/lang/StringBuilder;

    #@b9
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@bc
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v6

    #@c0
    const-string v7, ");"

    #@c2
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v6

    #@c6
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c9
    move-result-object v2

    #@ca
    .line 124
    :try_start_ca
    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_cd
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_ca .. :try_end_cd} :catch_e5

    #@cd
    .line 131
    new-instance v3, Landroid/content/ContentValues;

    #@cf
    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    #@d2
    .line 133
    .local v3, values:Landroid/content/ContentValues;
    const/4 v1, 0x0

    #@d3
    :goto_d3
    array-length v6, p3

    #@d4
    if-ge v1, v6, :cond_eb

    #@d6
    .line 135
    aget-object v6, p3, v1

    #@d8
    aget-object v6, v6, v5

    #@da
    aget-object v7, p3, v1

    #@dc
    const/4 v8, 0x2

    #@dd
    aget-object v7, v7, v8

    #@df
    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@e2
    .line 133
    add-int/lit8 v1, v1, 0x1

    #@e4
    goto :goto_d3

    #@e5
    .line 125
    .end local v3           #values:Landroid/content/ContentValues;
    :catch_e5
    move-exception v0

    #@e6
    .line 126
    .local v0, e:Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    #@e9
    goto/16 :goto_4

    #@eb
    .line 140
    .end local v0           #e:Landroid/database/sqlite/SQLiteException;
    .restart local v3       #values:Landroid/content/ContentValues;
    :cond_eb
    const/4 v6, 0x0

    #@ec
    :try_start_ec
    invoke-virtual {p1, p2, v6, v3}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_ef
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_ec .. :try_end_ef} :catch_f2

    #@ef
    move v4, v5

    #@f0
    .line 146
    goto/16 :goto_4

    #@f2
    .line 141
    :catch_f2
    move-exception v0

    #@f3
    .line 142
    .restart local v0       #e:Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    #@f6
    goto/16 :goto_4
.end method

.method private createTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 11
    .parameter "db"

    #@0
    .prologue
    .line 150
    invoke-static {}, Lcom/lge/ims/provider/ConfigurationManager;->getInstance()Lcom/lge/ims/provider/ConfigurationManager;

    #@3
    move-result-object v6

    #@4
    invoke-virtual {v6}, Lcom/lge/ims/provider/ConfigurationManager;->getConfigurations()Ljava/util/ArrayList;

    #@7
    move-result-object v1

    #@8
    .line 152
    .local v1, configs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/lge/ims/provider/IConfiguration;>;"
    if-nez v1, :cond_12

    #@a
    .line 153
    const-string v6, "LGIMS"

    #@c
    const-string v7, "IConfiguration is null"

    #@e
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 181
    :cond_11
    return-void

    #@12
    .line 157
    :cond_12
    const/4 v3, 0x0

    #@13
    .local v3, i:I
    :goto_13
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@16
    move-result v6

    #@17
    if-ge v3, v6, :cond_11

    #@19
    .line 158
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1c
    move-result-object v0

    #@1d
    check-cast v0, Lcom/lge/ims/provider/IConfiguration;

    #@1f
    .line 160
    .local v0, config:Lcom/lge/ims/provider/IConfiguration;
    if-nez v0, :cond_2b

    #@21
    .line 161
    const-string v6, "LGIMS"

    #@23
    const-string v7, "IConfiguration is null"

    #@25
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 157
    :cond_28
    :goto_28
    add-int/lit8 v3, v3, 0x1

    #@2a
    goto :goto_13

    #@2b
    .line 165
    :cond_2b
    invoke-interface {v0}, Lcom/lge/ims/provider/IConfiguration;->getTables()[Ljava/lang/String;

    #@2e
    move-result-object v5

    #@2f
    .line 167
    .local v5, tables:[Ljava/lang/String;
    if-nez v5, :cond_39

    #@31
    .line 168
    const-string v6, "LGIMS"

    #@33
    const-string v7, "no tables"

    #@35
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    goto :goto_28

    #@39
    .line 172
    :cond_39
    const/4 v4, 0x0

    #@3a
    .local v4, j:I
    :goto_3a
    array-length v6, v5

    #@3b
    if-ge v4, v6, :cond_28

    #@3d
    .line 173
    aget-object v6, v5, v4

    #@3f
    invoke-interface {v0, v6}, Lcom/lge/ims/provider/IConfiguration;->getTableContent(Ljava/lang/String;)[[Ljava/lang/String;

    #@42
    move-result-object v2

    #@43
    .line 175
    .local v2, content:[[Ljava/lang/String;
    aget-object v6, v5, v4

    #@45
    invoke-direct {p0, p1, v6, v2}, Lcom/lge/ims/provider/ConfigurationProvider$DatabaseHelper;->createTableWithContent(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[[Ljava/lang/String;)Z

    #@48
    move-result v6

    #@49
    if-nez v6, :cond_6b

    #@4b
    .line 176
    const-string v6, "LGIMS"

    #@4d
    new-instance v7, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v8, "Creating a table ("

    #@54
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v7

    #@58
    aget-object v8, v5, v4

    #@5a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v7

    #@5e
    const-string v8, ") failed"

    #@60
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v7

    #@64
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@67
    move-result-object v7

    #@68
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6b
    .line 172
    :cond_6b
    add-int/lit8 v4, v4, 0x1

    #@6d
    goto :goto_3a
.end method

.method private dropTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 9
    .parameter "db"

    #@0
    .prologue
    .line 184
    invoke-static {}, Lcom/lge/ims/provider/ConfigurationManager;->getInstance()Lcom/lge/ims/provider/ConfigurationManager;

    #@3
    move-result-object v5

    #@4
    invoke-virtual {v5}, Lcom/lge/ims/provider/ConfigurationManager;->getConfigurations()Ljava/util/ArrayList;

    #@7
    move-result-object v1

    #@8
    .line 186
    .local v1, configs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/lge/ims/provider/IConfiguration;>;"
    if-nez v1, :cond_12

    #@a
    .line 187
    const-string v5, "LGIMS"

    #@c
    const-string v6, "IConfiguration is null"

    #@e
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 210
    :cond_11
    return-void

    #@12
    .line 191
    :cond_12
    const/4 v2, 0x0

    #@13
    .local v2, i:I
    :goto_13
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@16
    move-result v5

    #@17
    if-ge v2, v5, :cond_11

    #@19
    .line 192
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1c
    move-result-object v0

    #@1d
    check-cast v0, Lcom/lge/ims/provider/IConfiguration;

    #@1f
    .line 194
    .local v0, config:Lcom/lge/ims/provider/IConfiguration;
    if-nez v0, :cond_2b

    #@21
    .line 195
    const-string v5, "LGIMS"

    #@23
    const-string v6, "IConfiguration is null"

    #@25
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 191
    :cond_28
    :goto_28
    add-int/lit8 v2, v2, 0x1

    #@2a
    goto :goto_13

    #@2b
    .line 199
    :cond_2b
    invoke-interface {v0}, Lcom/lge/ims/provider/IConfiguration;->getTables()[Ljava/lang/String;

    #@2e
    move-result-object v4

    #@2f
    .line 201
    .local v4, tables:[Ljava/lang/String;
    if-nez v4, :cond_39

    #@31
    .line 202
    const-string v5, "LGIMS"

    #@33
    const-string v6, "no tables"

    #@35
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    goto :goto_28

    #@39
    .line 206
    :cond_39
    const/4 v3, 0x0

    #@3a
    .local v3, j:I
    :goto_3a
    array-length v5, v4

    #@3b
    if-ge v3, v5, :cond_28

    #@3d
    .line 207
    new-instance v5, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    const-string v6, "DROP TABLE IF EXISTS "

    #@44
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v5

    #@48
    aget-object v6, v4, v3

    #@4a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v5

    #@4e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v5

    #@52
    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@55
    .line 206
    add-int/lit8 v3, v3, 0x1

    #@57
    goto :goto_3a
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 4
    .parameter "db"

    #@0
    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/lge/ims/provider/ConfigurationProvider$DatabaseHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    #@3
    .line 71
    const-string v0, "LGIMS"

    #@5
    const-string v1, "lgims.db is created ..."

    #@7
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 72
    return-void
.end method

.method public onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .registers 7
    .parameter "db"
    .parameter "oldVersion"
    .parameter "newVersion"

    #@0
    .prologue
    .line 88
    const-string v0, "LGIMS"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Downgrading database from version "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, " to "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    const-string v2, ", which will destroy all old data"

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 91
    invoke-direct {p0, p1}, Lcom/lge/ims/provider/ConfigurationProvider$DatabaseHelper;->dropTables(Landroid/database/sqlite/SQLiteDatabase;)V

    #@2b
    .line 92
    invoke-virtual {p0, p1}, Lcom/lge/ims/provider/ConfigurationProvider$DatabaseHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    #@2e
    .line 93
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .registers 7
    .parameter "db"
    .parameter "oldVersion"
    .parameter "newVersion"

    #@0
    .prologue
    .line 76
    const-string v0, "LGIMS"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Upgrading database from version "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, " to "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    const-string v2, ", which will destroy all old data"

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 79
    invoke-direct {p0, p1}, Lcom/lge/ims/provider/ConfigurationProvider$DatabaseHelper;->dropTables(Landroid/database/sqlite/SQLiteDatabase;)V

    #@2b
    .line 80
    invoke-virtual {p0, p1}, Lcom/lge/ims/provider/ConfigurationProvider$DatabaseHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    #@2e
    .line 81
    return-void
.end method
