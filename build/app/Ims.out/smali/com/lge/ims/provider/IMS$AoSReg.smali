.class public final Lcom/lge/ims/provider/IMS$AoSReg;
.super Ljava/lang/Object;
.source "IMS.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/provider/IMS;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AoSReg"
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final IPSEC_0:Ljava/lang/String; = "aos_reg_0_ipsec"

.field public static final IPSEC_1:Ljava/lang/String; = "aos_reg_1_ipsec"

.field public static final RETRY_INTERVAL_0:Ljava/lang/String; = "aos_reg_0_retry_interval"

.field public static final RETRY_INTERVAL_1:Ljava/lang/String; = "aos_reg_1_retry_interval"

.field public static final RETRY_REPEAT_INTERVAL_0:Ljava/lang/String; = "aos_reg_0_retry_repeat_interval"

.field public static final RETRY_REPEAT_INTERVAL_1:Ljava/lang/String; = "aos_reg_1_retry_repeat_interval"

.field public static final TABLE_NAME:Ljava/lang/String; = "lgims_aosreg"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 150
    const-string v0, "content://com.lge.ims.provider.lgims/lgims_aosreg"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/lge/ims/provider/IMS$AoSReg;->CONTENT_URI:Landroid/net/Uri;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 145
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
