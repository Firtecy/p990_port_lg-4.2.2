.class public Lcom/lge/ims/provider/ConfigurationProvider;
.super Landroid/content/ContentProvider;
.source "ConfigurationProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/provider/ConfigurationProvider$DatabaseHelper;
    }
.end annotation


# static fields
.field private static final DATABASE_NAME:Ljava/lang/String; = "lgims.db"

.field private static final DATABASE_VERSION:I = 0x10

.field protected static final PM_AOS_CONNECTION:Ljava/util/HashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static final PM_AOS_REG:Ljava/util/HashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static final PM_ENGINE:Ljava/util/HashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static final PM_SERVICE_SIP:Ljava/util/HashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static final PM_SERVICE_VT:Ljava/util/HashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static final PM_SIP:Ljava/util/HashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static final PM_SUBSCRIBER:Ljava/util/HashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final TABLE_SERVICE_VT:Ljava/lang/String; = "lgims_com_kt_service_vt"

.field private static final TAG:Ljava/lang/String; = "LGIMS"

.field protected static final URI_AOS_CONNECTION:I = 0x16

.field protected static final URI_AOS_REG:I = 0x17

.field protected static final URI_ENGINE:I = 0x2

.field protected static final URI_MATCHER:Landroid/content/UriMatcher; = null

.field protected static final URI_SERVICE_SIP:I = 0x15

.field protected static final URI_SERVICE_VT:I = 0x18

.field protected static final URI_SIP:I = 0x3

.field protected static final URI_SUBSCRIBER:I = 0x1


# instance fields
.field protected mDB:Landroid/database/sqlite/SQLiteDatabase;

.field protected mOpenHelper:Lcom/lge/ims/provider/ConfigurationProvider$DatabaseHelper;

.field protected mServiceSIPTableName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    .line 429
    new-instance v0, Landroid/content/UriMatcher;

    #@2
    const/4 v1, -0x1

    #@3
    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    #@6
    sput-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@8
    .line 430
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@a
    const-string v1, "com.lge.ims.provider.lgims"

    #@c
    const-string v2, "lgims_subscriber"

    #@e
    const/4 v3, 0x1

    #@f
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@12
    .line 431
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@14
    const-string v1, "com.lge.ims.provider.lgims"

    #@16
    const-string v2, "lgims_engine"

    #@18
    const/4 v3, 0x2

    #@19
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@1c
    .line 432
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@1e
    const-string v1, "com.lge.ims.provider.lgims"

    #@20
    const-string v2, "lgims_sip"

    #@22
    const/4 v3, 0x3

    #@23
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@26
    .line 433
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@28
    const-string v1, "com.lge.ims.provider.lgims"

    #@2a
    const-string v2, "lgims_aosconnection"

    #@2c
    const/16 v3, 0x16

    #@2e
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@31
    .line 434
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@33
    const-string v1, "com.lge.ims.provider.lgims"

    #@35
    const-string v2, "lgims_aosreg"

    #@37
    const/16 v3, 0x17

    #@39
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@3c
    .line 435
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@3e
    const-string v1, "com.lge.ims.provider.lgims"

    #@40
    const-string v2, "lgims_com_kt_service_vt"

    #@42
    const/16 v3, 0x18

    #@44
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@47
    .line 437
    new-instance v0, Ljava/util/HashMap;

    #@49
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@4c
    sput-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@4e
    .line 438
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@50
    const-string v1, "admin_ims"

    #@52
    const-string v2, "admin_ims"

    #@54
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@57
    .line 439
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@59
    const-string v1, "admin_isim"

    #@5b
    const-string v2, "admin_isim"

    #@5d
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@60
    .line 440
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@62
    const-string v1, "admin_usim"

    #@64
    const-string v2, "admin_usim"

    #@66
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@69
    .line 441
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@6b
    const-string v1, "admin_debug"

    #@6d
    const-string v2, "admin_debug"

    #@6f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@72
    .line 442
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@74
    const-string v1, "admin_pcscf"

    #@76
    const-string v2, "admin_pcscf"

    #@78
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@7b
    .line 443
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@7d
    const-string v1, "admin_ac"

    #@7f
    const-string v2, "admin_ac"

    #@81
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@84
    .line 444
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@86
    const-string v1, "admin_dm"

    #@88
    const-string v2, "admin_dm"

    #@8a
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@8d
    .line 445
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@8f
    const-string v1, "admin_services"

    #@91
    const-string v2, "admin_services"

    #@93
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@96
    .line 446
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@98
    const-string v1, "admin_testmode"

    #@9a
    const-string v2, "admin_testmode"

    #@9c
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@9f
    .line 447
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@a1
    const-string v1, "server_pcscf_0_address"

    #@a3
    const-string v2, "server_pcscf_0_address"

    #@a5
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@a8
    .line 448
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@aa
    const-string v1, "server_pcscf_0_port"

    #@ac
    const-string v2, "server_pcscf_0_port"

    #@ae
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@b1
    .line 449
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@b3
    const-string v1, "server_pcscf_1_address"

    #@b5
    const-string v2, "server_pcscf_1_address"

    #@b7
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@ba
    .line 450
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@bc
    const-string v1, "server_pcscf_1_port"

    #@be
    const-string v2, "server_pcscf_1_port"

    #@c0
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@c3
    .line 451
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@c5
    const-string v1, "subscriber_0_server_scscf"

    #@c7
    const-string v2, "subscriber_0_server_scscf"

    #@c9
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@cc
    .line 452
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@ce
    const-string v1, "subscriber_0_home_domain_name"

    #@d0
    const-string v2, "subscriber_0_home_domain_name"

    #@d2
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@d5
    .line 453
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@d7
    const-string v1, "subscriber_0_impi"

    #@d9
    const-string v2, "subscriber_0_impi"

    #@db
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@de
    .line 454
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@e0
    const-string v1, "subscriber_0_impu_0"

    #@e2
    const-string v2, "subscriber_0_impu_0"

    #@e4
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@e7
    .line 455
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@e9
    const-string v1, "subscriber_0_impu_1"

    #@eb
    const-string v2, "subscriber_0_impu_1"

    #@ed
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@f0
    .line 456
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@f2
    const-string v1, "subscriber_0_impu_2"

    #@f4
    const-string v2, "subscriber_0_impu_2"

    #@f6
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@f9
    .line 457
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@fb
    const-string v1, "subscriber_0_phone_context"

    #@fd
    const-string v2, "subscriber_0_phone_context"

    #@ff
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@102
    .line 458
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@104
    const-string v1, "subscriber_0_auth_username"

    #@106
    const-string v2, "subscriber_0_auth_username"

    #@108
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@10b
    .line 459
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@10d
    const-string v1, "subscriber_0_auth_password"

    #@10f
    const-string v2, "subscriber_0_auth_password"

    #@111
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@114
    .line 460
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@116
    const-string v1, "subscriber_0_auth_realm"

    #@118
    const-string v2, "subscriber_0_auth_realm"

    #@11a
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@11d
    .line 461
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@11f
    const-string v1, "subscriber_0_auth_algorithm"

    #@121
    const-string v2, "subscriber_0_auth_algorithm"

    #@123
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@126
    .line 463
    new-instance v0, Ljava/util/HashMap;

    #@128
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@12b
    sput-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_ENGINE:Ljava/util/HashMap;

    #@12d
    .line 464
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_ENGINE:Ljava/util/HashMap;

    #@12f
    const-string v1, "trace_option"

    #@131
    const-string v2, "trace_option"

    #@133
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@136
    .line 465
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_ENGINE:Ljava/util/HashMap;

    #@138
    const-string v1, "trace_module"

    #@13a
    const-string v2, "trace_module"

    #@13c
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@13f
    .line 467
    new-instance v0, Ljava/util/HashMap;

    #@141
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@144
    sput-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SIP:Ljava/util/HashMap;

    #@146
    .line 468
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SIP:Ljava/util/HashMap;

    #@148
    const-string v1, "timer_tv_t1"

    #@14a
    const-string v2, "timer_tv_t1"

    #@14c
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@14f
    .line 469
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SIP:Ljava/util/HashMap;

    #@151
    const-string v1, "timer_tv_t2"

    #@153
    const-string v2, "timer_tv_t2"

    #@155
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@158
    .line 470
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SIP:Ljava/util/HashMap;

    #@15a
    const-string v1, "listen_channel_port"

    #@15c
    const-string v2, "listen_channel_port"

    #@15e
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@161
    .line 471
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SIP:Ljava/util/HashMap;

    #@163
    const-string v1, "ua_version_sw_version"

    #@165
    const-string v2, "ua_version_sw_version"

    #@167
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@16a
    .line 472
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SIP:Ljava/util/HashMap;

    #@16c
    const-string v1, "reg_expiration"

    #@16e
    const-string v2, "reg_expiration"

    #@170
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@173
    .line 473
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SIP:Ljava/util/HashMap;

    #@175
    const-string v1, "reg_subscription"

    #@177
    const-string v2, "reg_subscription"

    #@179
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@17c
    .line 474
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SIP:Ljava/util/HashMap;

    #@17e
    const-string v1, "reg_sub_expiration"

    #@180
    const-string v2, "reg_sub_expiration"

    #@182
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@185
    .line 476
    new-instance v0, Ljava/util/HashMap;

    #@187
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@18a
    sput-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SERVICE_SIP:Ljava/util/HashMap;

    #@18c
    .line 477
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SERVICE_SIP:Ljava/util/HashMap;

    #@18e
    const-string v1, "timer_tv_t1"

    #@190
    const-string v2, "timer_tv_t1"

    #@192
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@195
    .line 478
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SERVICE_SIP:Ljava/util/HashMap;

    #@197
    const-string v1, "timer_tv_t2"

    #@199
    const-string v2, "timer_tv_t2"

    #@19b
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@19e
    .line 479
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SERVICE_SIP:Ljava/util/HashMap;

    #@1a0
    const-string v1, "timer_tv_tb"

    #@1a2
    const-string v2, "timer_tv_tb"

    #@1a4
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1a7
    .line 480
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SERVICE_SIP:Ljava/util/HashMap;

    #@1a9
    const-string v1, "timer_tv_td"

    #@1ab
    const-string v2, "timer_tv_td"

    #@1ad
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1b0
    .line 481
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SERVICE_SIP:Ljava/util/HashMap;

    #@1b2
    const-string v1, "timer_tv_tf"

    #@1b4
    const-string v2, "timer_tv_tf"

    #@1b6
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1b9
    .line 482
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SERVICE_SIP:Ljava/util/HashMap;

    #@1bb
    const-string v1, "timer_tv_th"

    #@1bd
    const-string v2, "timer_tv_th"

    #@1bf
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1c2
    .line 483
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SERVICE_SIP:Ljava/util/HashMap;

    #@1c4
    const-string v1, "timer_tv_ti"

    #@1c6
    const-string v2, "timer_tv_ti"

    #@1c8
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1cb
    .line 484
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SERVICE_SIP:Ljava/util/HashMap;

    #@1cd
    const-string v1, "timer_tv_tj"

    #@1cf
    const-string v2, "timer_tv_tj"

    #@1d1
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1d4
    .line 485
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SERVICE_SIP:Ljava/util/HashMap;

    #@1d6
    const-string v1, "timer_tv_tk"

    #@1d8
    const-string v2, "timer_tv_tk"

    #@1da
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1dd
    .line 486
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SERVICE_SIP:Ljava/util/HashMap;

    #@1df
    const-string v1, "header_info_target_scheme"

    #@1e1
    const-string v2, "header_info_target_scheme"

    #@1e3
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1e6
    .line 487
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SERVICE_SIP:Ljava/util/HashMap;

    #@1e8
    const-string v1, "header_info_preferred_id"

    #@1ea
    const-string v2, "header_info_preferred_id"

    #@1ec
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1ef
    .line 488
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SERVICE_SIP:Ljava/util/HashMap;

    #@1f1
    const-string v1, "header_info_service_version"

    #@1f3
    const-string v2, "header_info_service_version"

    #@1f5
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1f8
    .line 489
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SERVICE_SIP:Ljava/util/HashMap;

    #@1fa
    const-string v1, "header_info_feature_tags"

    #@1fc
    const-string v2, "header_info_feature_tags"

    #@1fe
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@201
    .line 490
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SERVICE_SIP:Ljava/util/HashMap;

    #@203
    const-string v1, "session_st_refresher"

    #@205
    const-string v2, "session_st_refresher"

    #@207
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@20a
    .line 491
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SERVICE_SIP:Ljava/util/HashMap;

    #@20c
    const-string v1, "session_st_method"

    #@20e
    const-string v2, "session_st_method"

    #@210
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@213
    .line 492
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SERVICE_SIP:Ljava/util/HashMap;

    #@215
    const-string v1, "session_st_minse"

    #@217
    const-string v2, "session_st_minse"

    #@219
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@21c
    .line 493
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SERVICE_SIP:Ljava/util/HashMap;

    #@21e
    const-string v1, "session_st_session_expires"

    #@220
    const-string v2, "session_st_session_expires"

    #@222
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@225
    .line 495
    new-instance v0, Ljava/util/HashMap;

    #@227
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@22a
    sput-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_AOS_CONNECTION:Ljava/util/HashMap;

    #@22c
    .line 496
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_AOS_CONNECTION:Ljava/util/HashMap;

    #@22e
    const-string v1, "aos_connection_0_profile_name"

    #@230
    const-string v2, "aos_connection_0_profile_name"

    #@232
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@235
    .line 497
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_AOS_CONNECTION:Ljava/util/HashMap;

    #@237
    const-string v1, "aos_connection_1_profile_name"

    #@239
    const-string v2, "aos_connection_1_profile_name"

    #@23b
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@23e
    .line 499
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_AOS_CONNECTION:Ljava/util/HashMap;

    #@240
    const-string v1, "aos_connection_0_access_policy"

    #@242
    const-string v2, "aos_connection_0_access_policy"

    #@244
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@247
    .line 500
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_AOS_CONNECTION:Ljava/util/HashMap;

    #@249
    const-string v1, "aos_connection_1_access_policy"

    #@24b
    const-string v2, "aos_connection_1_access_policy"

    #@24d
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@250
    .line 502
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_AOS_CONNECTION:Ljava/util/HashMap;

    #@252
    const-string v1, "aos_connection_0_ip_version"

    #@254
    const-string v2, "aos_connection_0_ip_version"

    #@256
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@259
    .line 503
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_AOS_CONNECTION:Ljava/util/HashMap;

    #@25b
    const-string v1, "aos_connection_1_ip_version"

    #@25d
    const-string v2, "aos_connection_1_ip_version"

    #@25f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@262
    .line 506
    new-instance v0, Ljava/util/HashMap;

    #@264
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@267
    sput-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_AOS_REG:Ljava/util/HashMap;

    #@269
    .line 507
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_AOS_REG:Ljava/util/HashMap;

    #@26b
    const-string v1, "aos_reg_0_retry_interval"

    #@26d
    const-string v2, "aos_reg_0_retry_interval"

    #@26f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@272
    .line 508
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_AOS_REG:Ljava/util/HashMap;

    #@274
    const-string v1, "aos_reg_1_retry_interval"

    #@276
    const-string v2, "aos_reg_1_retry_interval"

    #@278
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@27b
    .line 509
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_AOS_REG:Ljava/util/HashMap;

    #@27d
    const-string v1, "aos_reg_0_retry_repeat_interval"

    #@27f
    const-string v2, "aos_reg_0_retry_repeat_interval"

    #@281
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@284
    .line 510
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_AOS_REG:Ljava/util/HashMap;

    #@286
    const-string v1, "aos_reg_1_retry_repeat_interval"

    #@288
    const-string v2, "aos_reg_1_retry_repeat_interval"

    #@28a
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@28d
    .line 511
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_AOS_REG:Ljava/util/HashMap;

    #@28f
    const-string v1, "aos_reg_0_ipsec"

    #@291
    const-string v2, "aos_reg_0_ipsec"

    #@293
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@296
    .line 512
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_AOS_REG:Ljava/util/HashMap;

    #@298
    const-string v1, "aos_reg_1_ipsec"

    #@29a
    const-string v2, "aos_reg_1_ipsec"

    #@29c
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@29f
    .line 514
    new-instance v0, Ljava/util/HashMap;

    #@2a1
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@2a4
    sput-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SERVICE_VT:Ljava/util/HashMap;

    #@2a6
    .line 515
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SERVICE_VT:Ljava/util/HashMap;

    #@2a8
    const-string v1, "media_loopback"

    #@2aa
    const-string v2, "media_loopback"

    #@2ac
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2af
    .line 516
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 24
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    #@3
    .line 61
    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 6
    .parameter "arg0"
    .parameter "arg1"
    .parameter "arg2"

    #@0
    .prologue
    .line 216
    const-string v0, "LGIMS"

    #@2
    const-string v1, "DELETE query is not implemented"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 217
    const/4 v0, 0x0

    #@8
    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .registers 5
    .parameter "uri"

    #@0
    .prologue
    .line 222
    sget-object v0, Lcom/lge/ims/provider/ConfigurationProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@5
    move-result v0

    #@6
    sparse-switch v0, :sswitch_data_4a

    #@9
    .line 238
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@b
    new-instance v1, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v2, "Unsupported URI: "

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@21
    throw v0

    #@22
    .line 224
    :sswitch_22
    const-string v0, "vnd.android.cursor.dir/com.lge.ims.provider.lgims.lgims_subscriber"

    #@24
    .line 236
    :goto_24
    return-object v0

    #@25
    .line 226
    :sswitch_25
    const-string v0, "vnd.android.cursor.dir/com.lge.ims.provider.lgims.lgims_engine"

    #@27
    goto :goto_24

    #@28
    .line 228
    :sswitch_28
    const-string v0, "vnd.android.cursor.dir/com.lge.ims.provider.lgims.lgims_sip"

    #@2a
    goto :goto_24

    #@2b
    .line 230
    :sswitch_2b
    new-instance v0, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v1, "vnd.android.cursor.dir/com.lge.ims.provider.lgims."

    #@32
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v0

    #@36
    iget-object v1, p0, Lcom/lge/ims/provider/ConfigurationProvider;->mServiceSIPTableName:Ljava/lang/String;

    #@38
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v0

    #@3c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v0

    #@40
    goto :goto_24

    #@41
    .line 232
    :sswitch_41
    const-string v0, "vnd.android.cursor.dir/com.lge.ims.provider.lgims.lgims_aosconnection"

    #@43
    goto :goto_24

    #@44
    .line 234
    :sswitch_44
    const-string v0, "vnd.android.cursor.dir/com.lge.ims.provider.lgims.lgims_aosreg"

    #@46
    goto :goto_24

    #@47
    .line 236
    :sswitch_47
    const-string v0, "vnd.android.cursor.dir/com.lge.ims.provider.lgims.lgims_com_kt_service_vt"

    #@49
    goto :goto_24

    #@4a
    .line 222
    :sswitch_data_4a
    .sparse-switch
        0x1 -> :sswitch_22
        0x2 -> :sswitch_25
        0x3 -> :sswitch_28
        0x15 -> :sswitch_2b
        0x16 -> :sswitch_41
        0x17 -> :sswitch_44
        0x18 -> :sswitch_47
    .end sparse-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .registers 4
    .parameter "uri"
    .parameter "values"

    #@0
    .prologue
    .line 244
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public onCreate()Z
    .registers 13

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v11, 0x0

    #@2
    .line 249
    const-string v7, "LGIMS"

    #@4
    const-string v8, "ConfigurationProvider is created ..."

    #@6
    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 252
    new-instance v7, Lcom/lge/ims/provider/ConfigurationProvider$DatabaseHelper;

    #@b
    invoke-virtual {p0}, Lcom/lge/ims/provider/ConfigurationProvider;->getContext()Landroid/content/Context;

    #@e
    move-result-object v8

    #@f
    const-string v9, "lgims.db"

    #@11
    const/16 v10, 0x10

    #@13
    invoke-direct {v7, v8, v9, v11, v10}, Lcom/lge/ims/provider/ConfigurationProvider$DatabaseHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    #@16
    iput-object v7, p0, Lcom/lge/ims/provider/ConfigurationProvider;->mOpenHelper:Lcom/lge/ims/provider/ConfigurationProvider$DatabaseHelper;

    #@18
    .line 254
    iget-object v7, p0, Lcom/lge/ims/provider/ConfigurationProvider;->mOpenHelper:Lcom/lge/ims/provider/ConfigurationProvider$DatabaseHelper;

    #@1a
    invoke-virtual {v7}, Lcom/lge/ims/provider/ConfigurationProvider$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@1d
    move-result-object v7

    #@1e
    iput-object v7, p0, Lcom/lge/ims/provider/ConfigurationProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    #@20
    .line 256
    iget-object v7, p0, Lcom/lge/ims/provider/ConfigurationProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    #@22
    if-nez v7, :cond_25

    #@24
    .line 328
    :goto_24
    return v6

    #@25
    .line 261
    :cond_25
    const/4 v1, 0x0

    #@26
    .line 264
    .local v1, cursor:Landroid/database/Cursor;
    :try_start_26
    iget-object v6, p0, Lcom/lge/ims/provider/ConfigurationProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    #@28
    const-string v7, "select * from lgims_sip"

    #@2a
    const/4 v8, 0x0

    #@2b
    invoke-virtual {v6, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    #@2e
    move-result-object v1

    #@2f
    .line 267
    if-eqz v1, :cond_94

    #@31
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    #@34
    move-result v6

    #@35
    if-eqz v6, :cond_94

    #@37
    .line 268
    const-string v6, "service_shared"

    #@39
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@3c
    move-result v4

    #@3d
    .line 270
    .local v4, index:I
    if-ltz v4, :cond_11e

    #@3f
    .line 271
    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@42
    move-result-object v6

    #@43
    iput-object v6, p0, Lcom/lge/ims/provider/ConfigurationProvider;->mServiceSIPTableName:Ljava/lang/String;

    #@45
    .line 273
    const-string v6, "LGIMS"

    #@47
    new-instance v7, Ljava/lang/StringBuilder;

    #@49
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    const-string v8, "Service SIP table :: "

    #@4e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v7

    #@52
    iget-object v8, p0, Lcom/lge/ims/provider/ConfigurationProvider;->mServiceSIPTableName:Ljava/lang/String;

    #@54
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v7

    #@58
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v7

    #@5c
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5f
    .line 275
    iget-object v6, p0, Lcom/lge/ims/provider/ConfigurationProvider;->mServiceSIPTableName:Ljava/lang/String;

    #@61
    if-eqz v6, :cond_94

    #@63
    .line 276
    iget-object v6, p0, Lcom/lge/ims/provider/ConfigurationProvider;->mServiceSIPTableName:Ljava/lang/String;

    #@65
    const/16 v7, 0x2e

    #@67
    const/16 v8, 0x5f

    #@69
    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    #@6c
    move-result-object v6

    #@6d
    iput-object v6, p0, Lcom/lge/ims/provider/ConfigurationProvider;->mServiceSIPTableName:Ljava/lang/String;

    #@6f
    .line 277
    const-string v6, "LGIMS"

    #@71
    new-instance v7, Ljava/lang/StringBuilder;

    #@73
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    const-string v8, "Service SIP table :: "

    #@78
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v7

    #@7c
    iget-object v8, p0, Lcom/lge/ims/provider/ConfigurationProvider;->mServiceSIPTableName:Ljava/lang/String;

    #@7e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v7

    #@82
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v7

    #@86
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@89
    .line 278
    sget-object v6, Lcom/lge/ims/provider/ConfigurationProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@8b
    const-string v7, "com.lge.ims.provider.lgims"

    #@8d
    iget-object v8, p0, Lcom/lge/ims/provider/ConfigurationProvider;->mServiceSIPTableName:Ljava/lang/String;

    #@8f
    const/16 v9, 0x15

    #@91
    invoke-virtual {v6, v7, v8, v9}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_94
    .catchall {:try_start_26 .. :try_end_94} :catchall_13d
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_26 .. :try_end_94} :catch_127
    .catch Ljava/lang/Exception; {:try_start_26 .. :try_end_94} :catch_132

    #@94
    .line 289
    .end local v4           #index:I
    :cond_94
    :goto_94
    if-eqz v1, :cond_99

    #@96
    .line 290
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    #@99
    .line 295
    :cond_99
    :goto_99
    const-string v6, "user"

    #@9b
    sget-object v7, Lcom/lge/ims/Configuration;->BUILD_TYPE:Ljava/lang/String;

    #@9d
    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@a0
    move-result v6

    #@a1
    if-nez v6, :cond_11b

    #@a3
    .line 296
    const/4 v0, 0x0

    #@a4
    .line 299
    .local v0, c:Landroid/database/Cursor;
    :try_start_a4
    iget-object v6, p0, Lcom/lge/ims/provider/ConfigurationProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    #@a6
    const-string v7, "select * from lgims_engine"

    #@a8
    const/4 v8, 0x0

    #@a9
    invoke-virtual {v6, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    #@ac
    move-result-object v0

    #@ad
    .line 302
    if-eqz v0, :cond_116

    #@af
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    #@b2
    move-result v6

    #@b3
    if-eqz v6, :cond_116

    #@b5
    .line 303
    const-string v6, "trace_option"

    #@b7
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@ba
    move-result v6

    #@bb
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@be
    move-result-object v5

    #@bf
    .line 305
    .local v5, option:Ljava/lang/String;
    if-eqz v5, :cond_116

    #@c1
    const-string v6, "0000"

    #@c3
    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@c6
    move-result v6

    #@c7
    if-eqz v6, :cond_116

    #@c9
    .line 306
    new-instance v2, Landroid/content/ContentValues;

    #@cb
    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    #@ce
    .line 308
    .local v2, cvs:Landroid/content/ContentValues;
    const/4 v6, 0x0

    #@cf
    const/4 v7, 0x6

    #@d0
    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@d3
    move-result-object v5

    #@d4
    .line 309
    new-instance v6, Ljava/lang/StringBuilder;

    #@d6
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@d9
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v6

    #@dd
    const-string v7, "000F"

    #@df
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v6

    #@e3
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e6
    move-result-object v5

    #@e7
    .line 310
    const-string v6, "trace_option"

    #@e9
    invoke-virtual {v2, v6, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@ec
    .line 312
    iget-object v6, p0, Lcom/lge/ims/provider/ConfigurationProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    #@ee
    const-string v7, "lgims_engine"

    #@f0
    const/4 v8, 0x0

    #@f1
    const/4 v9, 0x0

    #@f2
    invoke-virtual {v6, v7, v2, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@f5
    move-result v6

    #@f6
    if-lez v6, :cond_116

    #@f8
    .line 313
    const-string v6, "LGIMS"

    #@fa
    new-instance v7, Ljava/lang/StringBuilder;

    #@fc
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@ff
    const-string v8, "LOG :: option ("

    #@101
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@104
    move-result-object v7

    #@105
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@108
    move-result-object v7

    #@109
    const-string v8, ") is updated"

    #@10b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10e
    move-result-object v7

    #@10f
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@112
    move-result-object v7

    #@113
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_116
    .catchall {:try_start_a4 .. :try_end_116} :catchall_158
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_a4 .. :try_end_116} :catch_144
    .catch Ljava/lang/Exception; {:try_start_a4 .. :try_end_116} :catch_14e

    #@116
    .line 322
    .end local v2           #cvs:Landroid/content/ContentValues;
    .end local v5           #option:Ljava/lang/String;
    :cond_116
    if-eqz v0, :cond_11b

    #@118
    .line 323
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    #@11b
    .line 328
    .end local v0           #c:Landroid/database/Cursor;
    :cond_11b
    :goto_11b
    const/4 v6, 0x1

    #@11c
    goto/16 :goto_24

    #@11e
    .line 281
    .restart local v4       #index:I
    :cond_11e
    :try_start_11e
    const-string v6, "LGIMS"

    #@120
    const-string v7, "Getting Service SIP table failed"

    #@122
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_125
    .catchall {:try_start_11e .. :try_end_125} :catchall_13d
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_11e .. :try_end_125} :catch_127
    .catch Ljava/lang/Exception; {:try_start_11e .. :try_end_125} :catch_132

    #@125
    goto/16 :goto_94

    #@127
    .line 284
    .end local v4           #index:I
    :catch_127
    move-exception v3

    #@128
    .line 285
    .local v3, e:Landroid/database/sqlite/SQLiteException;
    :try_start_128
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_12b
    .catchall {:try_start_128 .. :try_end_12b} :catchall_13d

    #@12b
    .line 289
    if-eqz v1, :cond_99

    #@12d
    .line 290
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    #@130
    goto/16 :goto_99

    #@132
    .line 286
    .end local v3           #e:Landroid/database/sqlite/SQLiteException;
    :catch_132
    move-exception v3

    #@133
    .line 287
    .local v3, e:Ljava/lang/Exception;
    :try_start_133
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_136
    .catchall {:try_start_133 .. :try_end_136} :catchall_13d

    #@136
    .line 289
    if-eqz v1, :cond_99

    #@138
    .line 290
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    #@13b
    goto/16 :goto_99

    #@13d
    .line 289
    .end local v3           #e:Ljava/lang/Exception;
    :catchall_13d
    move-exception v6

    #@13e
    if-eqz v1, :cond_143

    #@140
    .line 290
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    #@143
    :cond_143
    throw v6

    #@144
    .line 317
    .restart local v0       #c:Landroid/database/Cursor;
    :catch_144
    move-exception v3

    #@145
    .line 318
    .local v3, e:Landroid/database/sqlite/SQLiteException;
    :try_start_145
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_148
    .catchall {:try_start_145 .. :try_end_148} :catchall_158

    #@148
    .line 322
    if-eqz v0, :cond_11b

    #@14a
    .line 323
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    #@14d
    goto :goto_11b

    #@14e
    .line 319
    .end local v3           #e:Landroid/database/sqlite/SQLiteException;
    :catch_14e
    move-exception v3

    #@14f
    .line 320
    .local v3, e:Ljava/lang/Exception;
    :try_start_14f
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_152
    .catchall {:try_start_14f .. :try_end_152} :catchall_158

    #@152
    .line 322
    if-eqz v0, :cond_11b

    #@154
    .line 323
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    #@157
    goto :goto_11b

    #@158
    .line 322
    .end local v3           #e:Ljava/lang/Exception;
    :catchall_158
    move-exception v6

    #@159
    if-eqz v0, :cond_15e

    #@15b
    .line 323
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    #@15e
    :cond_15e
    throw v6
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 15
    .parameter "uri"
    .parameter "projection"
    .parameter "selection"
    .parameter "selectionArgs"
    .parameter "sortOrder"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 333
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    #@3
    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    #@6
    .line 335
    .local v0, queryBuilder:Landroid/database/sqlite/SQLiteQueryBuilder;
    sget-object v1, Lcom/lge/ims/provider/ConfigurationProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@8
    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@b
    move-result v1

    #@c
    sparse-switch v1, :sswitch_data_ba

    #@f
    .line 365
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@11
    new-instance v2, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v3, "Unknown URI "

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@27
    throw v1

    #@28
    .line 337
    :sswitch_28
    const-string v1, "lgims_subscriber"

    #@2a
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    #@2d
    .line 338
    sget-object v1, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@2f
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    #@32
    .line 368
    :goto_32
    iget-object v1, p0, Lcom/lge/ims/provider/ConfigurationProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    #@34
    move-object v2, p2

    #@35
    move-object v3, p3

    #@36
    move-object v4, p4

    #@37
    move-object v6, v5

    #@38
    move-object v7, v5

    #@39
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@3c
    move-result-object v8

    #@3d
    .line 370
    .local v8, cursor:Landroid/database/Cursor;
    const-string v1, "user"

    #@3f
    sget-object v2, Lcom/lge/ims/Configuration;->BUILD_TYPE:Ljava/lang/String;

    #@41
    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@44
    move-result v1

    #@45
    if-nez v1, :cond_65

    #@47
    .line 371
    if-eqz p1, :cond_b1

    #@49
    .line 372
    const-string v1, "LGIMS"

    #@4b
    new-instance v2, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    const-string v3, "[ConfigurationProvider] query :: "

    #@52
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v2

    #@56
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    #@59
    move-result-object v3

    #@5a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v2

    #@5e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v2

    #@62
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@65
    .line 378
    :cond_65
    :goto_65
    if-nez v8, :cond_6e

    #@67
    .line 379
    const-string v1, "LGIMS"

    #@69
    const-string v2, "Cursor is null"

    #@6b
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6e
    .line 382
    :cond_6e
    return-object v8

    #@6f
    .line 341
    .end local v8           #cursor:Landroid/database/Cursor;
    :sswitch_6f
    const-string v1, "lgims_engine"

    #@71
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    #@74
    .line 342
    sget-object v1, Lcom/lge/ims/provider/ConfigurationProvider;->PM_ENGINE:Ljava/util/HashMap;

    #@76
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    #@79
    goto :goto_32

    #@7a
    .line 345
    :sswitch_7a
    const-string v1, "lgims_sip"

    #@7c
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    #@7f
    .line 346
    sget-object v1, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SIP:Ljava/util/HashMap;

    #@81
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    #@84
    goto :goto_32

    #@85
    .line 349
    :sswitch_85
    iget-object v1, p0, Lcom/lge/ims/provider/ConfigurationProvider;->mServiceSIPTableName:Ljava/lang/String;

    #@87
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    #@8a
    .line 350
    sget-object v1, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SERVICE_SIP:Ljava/util/HashMap;

    #@8c
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    #@8f
    goto :goto_32

    #@90
    .line 353
    :sswitch_90
    const-string v1, "lgims_aosconnection"

    #@92
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    #@95
    .line 354
    sget-object v1, Lcom/lge/ims/provider/ConfigurationProvider;->PM_AOS_CONNECTION:Ljava/util/HashMap;

    #@97
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    #@9a
    goto :goto_32

    #@9b
    .line 357
    :sswitch_9b
    const-string v1, "lgims_aosreg"

    #@9d
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    #@a0
    .line 358
    sget-object v1, Lcom/lge/ims/provider/ConfigurationProvider;->PM_AOS_REG:Ljava/util/HashMap;

    #@a2
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    #@a5
    goto :goto_32

    #@a6
    .line 361
    :sswitch_a6
    const-string v1, "lgims_com_kt_service_vt"

    #@a8
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    #@ab
    .line 362
    sget-object v1, Lcom/lge/ims/provider/ConfigurationProvider;->PM_SERVICE_VT:Ljava/util/HashMap;

    #@ad
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    #@b0
    goto :goto_32

    #@b1
    .line 374
    .restart local v8       #cursor:Landroid/database/Cursor;
    :cond_b1
    const-string v1, "LGIMS"

    #@b3
    const-string v2, "[ConfigurationProvider] query"

    #@b5
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b8
    goto :goto_65

    #@b9
    .line 335
    nop

    #@ba
    :sswitch_data_ba
    .sparse-switch
        0x1 -> :sswitch_28
        0x2 -> :sswitch_6f
        0x3 -> :sswitch_7a
        0x15 -> :sswitch_85
        0x16 -> :sswitch_90
        0x17 -> :sswitch_9b
        0x18 -> :sswitch_a6
    .end sparse-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 10
    .parameter "uri"
    .parameter "values"
    .parameter "where"
    .parameter "whereArgs"

    #@0
    .prologue
    .line 387
    const/4 v1, 0x0

    #@1
    .line 389
    .local v1, tableName:Ljava/lang/String;
    sget-object v2, Lcom/lge/ims/provider/ConfigurationProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@3
    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@6
    move-result v2

    #@7
    sparse-switch v2, :sswitch_data_76

    #@a
    .line 412
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@c
    new-instance v3, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v4, "Unknown URI "

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@22
    throw v2

    #@23
    .line 391
    :sswitch_23
    const-string v1, "lgims_subscriber"

    #@25
    .line 415
    :goto_25
    :sswitch_25
    if-nez v1, :cond_38

    #@27
    .line 416
    const/4 v0, 0x0

    #@28
    .line 425
    :cond_28
    :goto_28
    return v0

    #@29
    .line 394
    :sswitch_29
    const-string v1, "lgims_engine"

    #@2b
    .line 395
    goto :goto_25

    #@2c
    .line 397
    :sswitch_2c
    const-string v1, "lgims_sip"

    #@2e
    .line 398
    goto :goto_25

    #@2f
    .line 400
    :sswitch_2f
    iget-object v1, p0, Lcom/lge/ims/provider/ConfigurationProvider;->mServiceSIPTableName:Ljava/lang/String;

    #@31
    .line 401
    goto :goto_25

    #@32
    .line 403
    :sswitch_32
    const-string v1, "lgims_aosconnection"

    #@34
    .line 404
    goto :goto_25

    #@35
    .line 406
    :sswitch_35
    const-string v1, "lgims_aosreg"

    #@37
    .line 407
    goto :goto_25

    #@38
    .line 419
    :cond_38
    iget-object v2, p0, Lcom/lge/ims/provider/ConfigurationProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    #@3a
    invoke-virtual {v2, v1, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@3d
    move-result v0

    #@3e
    .line 421
    .local v0, count:I
    const-string v2, "user"

    #@40
    sget-object v3, Lcom/lge/ims/Configuration;->BUILD_TYPE:Ljava/lang/String;

    #@42
    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@45
    move-result v2

    #@46
    if-nez v2, :cond_28

    #@48
    .line 422
    const-string v2, "LGIMS"

    #@4a
    new-instance v3, Ljava/lang/StringBuilder;

    #@4c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@4f
    const-string v4, "update :: "

    #@51
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v3

    #@55
    invoke-virtual {p2}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    #@58
    move-result-object v4

    #@59
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v3

    #@5d
    const-string v4, "; count ("

    #@5f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v3

    #@63
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@66
    move-result-object v3

    #@67
    const-string v4, ")"

    #@69
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v3

    #@6d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@70
    move-result-object v3

    #@71
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@74
    goto :goto_28

    #@75
    .line 389
    nop

    #@76
    :sswitch_data_76
    .sparse-switch
        0x1 -> :sswitch_23
        0x2 -> :sswitch_29
        0x3 -> :sswitch_2c
        0x15 -> :sswitch_2f
        0x16 -> :sswitch_32
        0x17 -> :sswitch_35
        0x18 -> :sswitch_25
    .end sparse-switch
.end method
