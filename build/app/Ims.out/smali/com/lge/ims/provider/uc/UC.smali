.class public Lcom/lge/ims/provider/uc/UC;
.super Ljava/lang/Object;
.source "UC.java"

# interfaces
.implements Lcom/lge/ims/provider/IConfiguration;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/provider/uc/UC$Data;
    }
.end annotation


# static fields
.field private static final TABLES:[Ljava/lang/String;

.field private static final UCSTATE:[[Ljava/lang/String;

.field private static mUC:Lcom/lge/ims/provider/uc/UC;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x3

    #@1
    const/4 v6, 0x2

    #@2
    const/4 v5, 0x1

    #@3
    const/4 v4, 0x0

    #@4
    .line 83
    new-array v0, v5, [Ljava/lang/String;

    #@6
    const-string v1, "ucstate"

    #@8
    aput-object v1, v0, v4

    #@a
    sput-object v0, Lcom/lge/ims/provider/uc/UC;->TABLES:[Ljava/lang/String;

    #@c
    .line 89
    const/4 v0, 0x6

    #@d
    new-array v0, v0, [[Ljava/lang/String;

    #@f
    new-array v1, v7, [Ljava/lang/String;

    #@11
    const-string v2, "TEXT"

    #@13
    aput-object v2, v1, v4

    #@15
    const-string v2, "vt_state"

    #@17
    aput-object v2, v1, v5

    #@19
    const-string v2, "0"

    #@1b
    aput-object v2, v1, v6

    #@1d
    aput-object v1, v0, v4

    #@1f
    new-array v1, v7, [Ljava/lang/String;

    #@21
    const-string v2, "TEXT"

    #@23
    aput-object v2, v1, v4

    #@25
    const-string v2, "voip_state"

    #@27
    aput-object v2, v1, v5

    #@29
    const-string v2, "0"

    #@2b
    aput-object v2, v1, v6

    #@2d
    aput-object v1, v0, v5

    #@2f
    new-array v1, v7, [Ljava/lang/String;

    #@31
    const-string v2, "TEXT"

    #@33
    aput-object v2, v1, v4

    #@35
    const-string v2, "uc_reg_state"

    #@37
    aput-object v2, v1, v5

    #@39
    const-string v2, "0"

    #@3b
    aput-object v2, v1, v6

    #@3d
    aput-object v1, v0, v6

    #@3f
    new-array v1, v7, [Ljava/lang/String;

    #@41
    const-string v2, "TEXT"

    #@43
    aput-object v2, v1, v4

    #@45
    const-string v2, "voip_enabled"

    #@47
    aput-object v2, v1, v5

    #@49
    const-string v2, "0"

    #@4b
    aput-object v2, v1, v6

    #@4d
    aput-object v1, v0, v7

    #@4f
    const/4 v1, 0x4

    #@50
    new-array v2, v7, [Ljava/lang/String;

    #@52
    const-string v3, "TEXT"

    #@54
    aput-object v3, v2, v4

    #@56
    const-string v3, "vt_enabled"

    #@58
    aput-object v3, v2, v5

    #@5a
    const-string v3, "0"

    #@5c
    aput-object v3, v2, v6

    #@5e
    aput-object v2, v0, v1

    #@60
    const/4 v1, 0x5

    #@61
    new-array v2, v7, [Ljava/lang/String;

    #@63
    const-string v3, "TEXT"

    #@65
    aput-object v3, v2, v4

    #@67
    const-string v3, "feature_smartcall"

    #@69
    aput-object v3, v2, v5

    #@6b
    const-string v3, "0"

    #@6d
    aput-object v3, v2, v6

    #@6f
    aput-object v2, v0, v1

    #@71
    sput-object v0, Lcom/lge/ims/provider/uc/UC;->UCSTATE:[[Ljava/lang/String;

    #@73
    .line 100
    const/4 v0, 0x0

    #@74
    sput-object v0, Lcom/lge/ims/provider/uc/UC;->mUC:Lcom/lge/ims/provider/uc/UC;

    #@76
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 17
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 18
    return-void
.end method

.method public static getInstance()Lcom/lge/ims/provider/uc/UC;
    .registers 1

    #@0
    .prologue
    .line 106
    sget-object v0, Lcom/lge/ims/provider/uc/UC;->mUC:Lcom/lge/ims/provider/uc/UC;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 107
    new-instance v0, Lcom/lge/ims/provider/uc/UC;

    #@6
    invoke-direct {v0}, Lcom/lge/ims/provider/uc/UC;-><init>()V

    #@9
    sput-object v0, Lcom/lge/ims/provider/uc/UC;->mUC:Lcom/lge/ims/provider/uc/UC;

    #@b
    .line 110
    :cond_b
    sget-object v0, Lcom/lge/ims/provider/uc/UC;->mUC:Lcom/lge/ims/provider/uc/UC;

    #@d
    return-object v0
.end method


# virtual methods
.method public getTableContent(Ljava/lang/String;)[[Ljava/lang/String;
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 126
    const-string v0, "ucstate"

    #@2
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_b

    #@8
    .line 127
    sget-object v0, Lcom/lge/ims/provider/uc/UC;->UCSTATE:[[Ljava/lang/String;

    #@a
    .line 129
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    check-cast v0, [[Ljava/lang/String;

    #@e
    goto :goto_a
.end method

.method public getTables()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 118
    sget-object v0, Lcom/lge/ims/provider/uc/UC;->TABLES:[Ljava/lang/String;

    #@2
    return-object v0
.end method
