.class public Lcom/lge/ims/provider/ac/ACSettingsProvider;
.super Landroid/content/ContentProvider;
.source "ACSettingsProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/provider/ac/ACSettingsProvider$DatabaseHelper;
    }
.end annotation


# static fields
.field private static final DATABASE_NAME:Ljava/lang/String; = "ac_settings.db"

.field private static final DATABASE_VERSION:I = 0x4

.field protected static final PM_COMMON:Ljava/util/HashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static final PM_CONNECTION_SERVER:Ljava/util/HashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static final PM_SUBSCRIBER:Ljava/util/HashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "ACSettingsProvider"

.field protected static final URI_COMMON:I = 0x1

.field protected static final URI_CONNECTION_SERVER:I = 0x3

.field protected static final URI_MATCHER:Landroid/content/UriMatcher; = null

.field protected static final URI_SUBSCRIBER:I = 0x2


# instance fields
.field private mDB:Landroid/database/sqlite/SQLiteDatabase;

.field private mOpenHelper:Lcom/lge/ims/provider/ac/ACSettingsProvider$DatabaseHelper;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    .line 232
    new-instance v0, Landroid/content/UriMatcher;

    #@2
    const/4 v1, -0x1

    #@3
    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    #@6
    sput-object v0, Lcom/lge/ims/provider/ac/ACSettingsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@8
    .line 233
    sget-object v0, Lcom/lge/ims/provider/ac/ACSettingsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@a
    const-string v1, "com.lge.ims.provider.ac_settings"

    #@c
    const-string v2, "common"

    #@e
    const/4 v3, 0x1

    #@f
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@12
    .line 234
    sget-object v0, Lcom/lge/ims/provider/ac/ACSettingsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@14
    const-string v1, "com.lge.ims.provider.ac_settings"

    #@16
    const-string v2, "subscriber"

    #@18
    const/4 v3, 0x2

    #@19
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@1c
    .line 235
    sget-object v0, Lcom/lge/ims/provider/ac/ACSettingsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@1e
    const-string v1, "com.lge.ims.provider.ac_settings"

    #@20
    const-string v2, "connection_server"

    #@22
    const/4 v3, 0x3

    #@23
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@26
    .line 237
    new-instance v0, Ljava/util/HashMap;

    #@28
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@2b
    sput-object v0, Lcom/lge/ims/provider/ac/ACSettingsProvider;->PM_COMMON:Ljava/util/HashMap;

    #@2d
    .line 238
    sget-object v0, Lcom/lge/ims/provider/ac/ACSettingsProvider;->PM_COMMON:Ljava/util/HashMap;

    #@2f
    const-string v1, "availability"

    #@31
    const-string v2, "availability"

    #@33
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@36
    .line 239
    sget-object v0, Lcom/lge/ims/provider/ac/ACSettingsProvider;->PM_COMMON:Ljava/util/HashMap;

    #@38
    const-string v1, "validity"

    #@3a
    const-string v2, "validity"

    #@3c
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3f
    .line 240
    sget-object v0, Lcom/lge/ims/provider/ac/ACSettingsProvider;->PM_COMMON:Ljava/util/HashMap;

    #@41
    const-string v1, "update_exception_list"

    #@43
    const-string v2, "update_exception_list"

    #@45
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@48
    .line 242
    new-instance v0, Ljava/util/HashMap;

    #@4a
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@4d
    sput-object v0, Lcom/lge/ims/provider/ac/ACSettingsProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@4f
    .line 243
    sget-object v0, Lcom/lge/ims/provider/ac/ACSettingsProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@51
    const-string v1, "usim"

    #@53
    const-string v2, "usim"

    #@55
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@58
    .line 244
    sget-object v0, Lcom/lge/ims/provider/ac/ACSettingsProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@5a
    const-string v1, "phone_number"

    #@5c
    const-string v2, "phone_number"

    #@5e
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@61
    .line 245
    sget-object v0, Lcom/lge/ims/provider/ac/ACSettingsProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@63
    const-string v1, "imsi"

    #@65
    const-string v2, "imsi"

    #@67
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@6a
    .line 247
    new-instance v0, Ljava/util/HashMap;

    #@6c
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@6f
    sput-object v0, Lcom/lge/ims/provider/ac/ACSettingsProvider;->PM_CONNECTION_SERVER:Ljava/util/HashMap;

    #@71
    .line 248
    sget-object v0, Lcom/lge/ims/provider/ac/ACSettingsProvider;->PM_CONNECTION_SERVER:Ljava/util/HashMap;

    #@73
    const-string v1, "server_selection"

    #@75
    const-string v2, "server_selection"

    #@77
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@7a
    .line 249
    sget-object v0, Lcom/lge/ims/provider/ac/ACSettingsProvider;->PM_CONNECTION_SERVER:Ljava/util/HashMap;

    #@7c
    const-string v1, "commercial_network"

    #@7e
    const-string v2, "commercial_network"

    #@80
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@83
    .line 250
    sget-object v0, Lcom/lge/ims/provider/ac/ACSettingsProvider;->PM_CONNECTION_SERVER:Ljava/util/HashMap;

    #@85
    const-string v1, "testbed"

    #@87
    const-string v2, "testbed"

    #@89
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@8c
    .line 251
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 21
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    #@4
    .line 42
    iput-object v0, p0, Lcom/lge/ims/provider/ac/ACSettingsProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    #@6
    .line 43
    iput-object v0, p0, Lcom/lge/ims/provider/ac/ACSettingsProvider;->mOpenHelper:Lcom/lge/ims/provider/ac/ACSettingsProvider$DatabaseHelper;

    #@8
    .line 48
    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 6
    .parameter "arg0"
    .parameter "arg1"
    .parameter "arg2"

    #@0
    .prologue
    .line 118
    const-string v0, "ACSettingsProvider"

    #@2
    const-string v1, "DELETE query is not implemented"

    #@4
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 119
    const/4 v0, 0x0

    #@8
    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .registers 5
    .parameter "uri"

    #@0
    .prologue
    .line 124
    sget-object v0, Lcom/lge/ims/provider/ac/ACSettingsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@5
    move-result v0

    #@6
    packed-switch v0, :pswitch_data_2c

    #@9
    .line 132
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@b
    new-instance v1, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v2, "Unsupported URI: "

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@21
    throw v0

    #@22
    .line 126
    :pswitch_22
    const-string v0, "vnd.android.cursor.dir/com.lge.ims.provider.ac_settings.common"

    #@24
    .line 130
    :goto_24
    return-object v0

    #@25
    .line 128
    :pswitch_25
    const-string v0, "vnd.android.cursor.dir/com.lge.ims.provider.ac_settings.subscriber"

    #@27
    goto :goto_24

    #@28
    .line 130
    :pswitch_28
    const-string v0, "vnd.android.cursor.dir/com.lge.ims.provider.ac_settings.connection_server"

    #@2a
    goto :goto_24

    #@2b
    .line 124
    nop

    #@2c
    :pswitch_data_2c
    .packed-switch 0x1
        :pswitch_22
        :pswitch_25
        :pswitch_28
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .registers 4
    .parameter "uri"
    .parameter "values"

    #@0
    .prologue
    .line 138
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public onCreate()Z
    .registers 7

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 143
    const-string v1, "ACSettingsProvider"

    #@3
    const-string v2, "ACSettingsProvider is created ..."

    #@5
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 145
    new-instance v1, Lcom/lge/ims/provider/ac/ACSettingsProvider$DatabaseHelper;

    #@a
    invoke-virtual {p0}, Lcom/lge/ims/provider/ac/ACSettingsProvider;->getContext()Landroid/content/Context;

    #@d
    move-result-object v2

    #@e
    const-string v3, "ac_settings.db"

    #@10
    const/4 v4, 0x0

    #@11
    const/4 v5, 0x4

    #@12
    invoke-direct {v1, v2, v3, v4, v5}, Lcom/lge/ims/provider/ac/ACSettingsProvider$DatabaseHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    #@15
    iput-object v1, p0, Lcom/lge/ims/provider/ac/ACSettingsProvider;->mOpenHelper:Lcom/lge/ims/provider/ac/ACSettingsProvider$DatabaseHelper;

    #@17
    .line 147
    iget-object v1, p0, Lcom/lge/ims/provider/ac/ACSettingsProvider;->mOpenHelper:Lcom/lge/ims/provider/ac/ACSettingsProvider$DatabaseHelper;

    #@19
    if-nez v1, :cond_1c

    #@1b
    .line 157
    :cond_1b
    :goto_1b
    return v0

    #@1c
    .line 151
    :cond_1c
    iget-object v1, p0, Lcom/lge/ims/provider/ac/ACSettingsProvider;->mOpenHelper:Lcom/lge/ims/provider/ac/ACSettingsProvider$DatabaseHelper;

    #@1e
    invoke-virtual {v1}, Lcom/lge/ims/provider/ac/ACSettingsProvider$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@21
    move-result-object v1

    #@22
    iput-object v1, p0, Lcom/lge/ims/provider/ac/ACSettingsProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    #@24
    .line 153
    iget-object v1, p0, Lcom/lge/ims/provider/ac/ACSettingsProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    #@26
    if-eqz v1, :cond_1b

    #@28
    .line 157
    const/4 v0, 0x1

    #@29
    goto :goto_1b
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 15
    .parameter "uri"
    .parameter "projection"
    .parameter "selection"
    .parameter "selectionArgs"
    .parameter "sortOrder"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 162
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    #@3
    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    #@6
    .line 164
    .local v0, queryBuilder:Landroid/database/sqlite/SQLiteQueryBuilder;
    sget-object v1, Lcom/lge/ims/provider/ac/ACSettingsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@8
    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@b
    move-result v1

    #@c
    packed-switch v1, :pswitch_data_84

    #@f
    .line 181
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@11
    new-instance v2, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v3, "Unknown URI "

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@27
    throw v1

    #@28
    .line 166
    :pswitch_28
    const-string v1, "common"

    #@2a
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    #@2d
    .line 167
    sget-object v1, Lcom/lge/ims/provider/ac/ACSettingsProvider;->PM_COMMON:Ljava/util/HashMap;

    #@2f
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    #@32
    .line 184
    :goto_32
    iget-object v1, p0, Lcom/lge/ims/provider/ac/ACSettingsProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    #@34
    move-object v2, p2

    #@35
    move-object v3, p3

    #@36
    move-object v4, p4

    #@37
    move-object v6, v5

    #@38
    move-object v7, v5

    #@39
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@3c
    move-result-object v8

    #@3d
    .line 186
    .local v8, cursor:Landroid/database/Cursor;
    if-eqz p1, :cond_7b

    #@3f
    .line 187
    const-string v1, "ACSettingsProvider"

    #@41
    new-instance v2, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v3, "query :: "

    #@48
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v2

    #@4c
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    #@4f
    move-result-object v3

    #@50
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v2

    #@54
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v2

    #@58
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@5b
    .line 192
    :goto_5b
    if-nez v8, :cond_64

    #@5d
    .line 193
    const-string v1, "ACSettingsProvider"

    #@5f
    const-string v2, "Cursor is null"

    #@61
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@64
    .line 196
    :cond_64
    return-object v8

    #@65
    .line 171
    .end local v8           #cursor:Landroid/database/Cursor;
    :pswitch_65
    const-string v1, "subscriber"

    #@67
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    #@6a
    .line 172
    sget-object v1, Lcom/lge/ims/provider/ac/ACSettingsProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@6c
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    #@6f
    goto :goto_32

    #@70
    .line 176
    :pswitch_70
    const-string v1, "connection_server"

    #@72
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    #@75
    .line 177
    sget-object v1, Lcom/lge/ims/provider/ac/ACSettingsProvider;->PM_CONNECTION_SERVER:Ljava/util/HashMap;

    #@77
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    #@7a
    goto :goto_32

    #@7b
    .line 189
    .restart local v8       #cursor:Landroid/database/Cursor;
    :cond_7b
    const-string v1, "ACSettingsProvider"

    #@7d
    const-string v2, "query"

    #@7f
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@82
    goto :goto_5b

    #@83
    .line 164
    nop

    #@84
    :pswitch_data_84
    .packed-switch 0x1
        :pswitch_28
        :pswitch_65
        :pswitch_70
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 10
    .parameter "uri"
    .parameter "values"
    .parameter "where"
    .parameter "whereArgs"

    #@0
    .prologue
    .line 201
    const/4 v1, 0x0

    #@1
    .line 203
    .local v1, tableName:Ljava/lang/String;
    sget-object v2, Lcom/lge/ims/provider/ac/ACSettingsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@3
    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@6
    move-result v2

    #@7
    packed-switch v2, :pswitch_data_62

    #@a
    .line 217
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@c
    new-instance v3, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v4, "Unknown URI "

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@22
    throw v2

    #@23
    .line 205
    :pswitch_23
    const-string v1, "common"

    #@25
    .line 220
    :goto_25
    if-nez v1, :cond_2f

    #@27
    .line 221
    const/4 v0, 0x0

    #@28
    .line 228
    :goto_28
    return v0

    #@29
    .line 209
    :pswitch_29
    const-string v1, "subscriber"

    #@2b
    .line 210
    goto :goto_25

    #@2c
    .line 213
    :pswitch_2c
    const-string v1, "connection_server"

    #@2e
    .line 214
    goto :goto_25

    #@2f
    .line 224
    :cond_2f
    iget-object v2, p0, Lcom/lge/ims/provider/ac/ACSettingsProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    #@31
    invoke-virtual {v2, v1, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@34
    move-result v0

    #@35
    .line 226
    .local v0, count:I
    const-string v2, "ACSettingsProvider"

    #@37
    new-instance v3, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v4, "update :: "

    #@3e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v3

    #@42
    invoke-virtual {p2}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    #@45
    move-result-object v4

    #@46
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v3

    #@4a
    const-string v4, "; count ("

    #@4c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v3

    #@50
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@53
    move-result-object v3

    #@54
    const-string v4, ")"

    #@56
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v3

    #@5a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object v3

    #@5e
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@61
    goto :goto_28

    #@62
    .line 203
    :pswitch_data_62
    .packed-switch 0x1
        :pswitch_23
        :pswitch_29
        :pswitch_2c
    .end packed-switch
.end method
