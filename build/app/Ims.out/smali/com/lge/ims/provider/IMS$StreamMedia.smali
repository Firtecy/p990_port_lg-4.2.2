.class public final Lcom/lge/ims/provider/IMS$StreamMedia;
.super Ljava/lang/Object;
.source "IMS.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/provider/IMS;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StreamMedia"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/provider/IMS$StreamMedia$Codec;,
        Lcom/lge/ims/provider/IMS$StreamMedia$Video;,
        Lcom/lge/ims/provider/IMS$StreamMedia$Audio;
    }
.end annotation


# static fields
.field public static final AUDIO_COUNT:Ljava/lang/String; = "media_audio_count"

.field public static final AUDIO_REF:Ljava/lang/String; = "media_audio_ref"

.field public static final CODECS:Ljava/lang/String; = "codecs"

.field public static final CODEC_REF:Ljava/lang/String; = "codec_ref"

.field public static final JITTER_BUFFER_SIZE:Ljava/lang/String; = "jitter_buffer_size"

.field public static final LOOPBACK:Ljava/lang/String; = "media_loopback"

.field public static final PORT_RTCP:Ljava/lang/String; = "port_rtcp"

.field public static final PORT_RTP:Ljava/lang/String; = "port_rtp"

.field public static final TV_HEARTBEAT:Ljava/lang/String; = "media_tv_heartbeat"

.field public static final VIDEO_COUNT:Ljava/lang/String; = "media_video_count"

.field public static final VIDEO_REF:Ljava/lang/String; = "media_video_ref"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 173
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 200
    return-void
.end method
