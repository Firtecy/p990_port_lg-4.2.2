.class public final Lcom/lge/ims/provider/im/InstantMessagingProvider$InstantMessagingDatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "InstantMessagingProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/provider/im/InstantMessagingProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1c
    name = "InstantMessagingDatabaseHelper"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V
    .registers 5
    .parameter "context"
    .parameter "name"
    .parameter "factory"
    .parameter "version"

    #@0
    .prologue
    .line 57
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    #@3
    .line 58
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 3
    .parameter "db"

    #@0
    .prologue
    .line 67
    const-string v0, "CREATE TABLE chat_invitation (_id INTEGER PRIMARY KEY AUTOINCREMENT, message_id TEXT NOT NULL,sender_uri TEXT NOT NULL);"

    #@2
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@5
    .line 68
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .registers 4
    .parameter "db"
    .parameter "oldVersion"
    .parameter "newVersion"

    #@0
    .prologue
    .line 73
    return-void
.end method
