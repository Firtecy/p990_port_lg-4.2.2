.class public Lcom/lge/ims/provider/im/InstantMessaging$ChatInvitation;
.super Ljava/lang/Object;
.source "InstantMessaging.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/provider/im/InstantMessaging;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ChatInvitation"
.end annotation


# static fields
.field public static final CONTENT_COUNT_PATH:Ljava/lang/String; = "invitation/count"

.field public static final CONTENT_ITEM_PATH:Ljava/lang/String; = "invitation/#"

.field public static final CONTENT_ITEM_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/vnd.lge.ims.service.im.invitation"

.field public static final CONTENT_PATH:Ljava/lang/String; = "invitation"

.field public static final CONTENT_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/vnd.lge.ims.service.im.invitation"

.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final MESSAGE_ID:Ljava/lang/String; = "message_id"

.field public static final SENDER_URI:Ljava/lang/String; = "sender_uri"

.field public static final _ID:Ljava/lang/String; = "_id"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 23
    const-string v0, "content://com.lge.ims.provider.im/invitation"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/lge/ims/provider/im/InstantMessaging$ChatInvitation;->CONTENT_URI:Landroid/net/Uri;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 19
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
