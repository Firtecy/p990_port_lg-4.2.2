.class public Lcom/lge/ims/provider/ip/IPProviderMetaData;
.super Ljava/lang/Object;
.source "IPProviderMetaData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/provider/ip/IPProviderMetaData$IPBuddyImage;,
        Lcom/lge/ims/provider/ip/IPProviderMetaData$IPProvisioning;,
        Lcom/lge/ims/provider/ip/IPProviderMetaData$IPMyStatus;,
        Lcom/lge/ims/provider/ip/IPProviderMetaData$IPPresence;,
        Lcom/lge/ims/provider/ip/IPProviderMetaData$IPCapa;
    }
.end annotation


# static fields
.field public static final DEFAULT_SORT_ORDER:Ljava/lang/String; = "_id DESC"

.field public static final IPDATABASE_NAME:Ljava/lang/String; = "ims_ip.db"

.field public static final IPDATABASE_VERSION:I = 0xe

.field public static final IPPROVIDER_AUTHORITY:Ljava/lang/String; = "com.lge.ims.provider.ip"

.field public static final IPPROVIDER_SERVERAUTHORITY:Ljava/lang/String; = "com.lge.ims.provider.server.ip"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 17
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 198
    return-void
.end method
