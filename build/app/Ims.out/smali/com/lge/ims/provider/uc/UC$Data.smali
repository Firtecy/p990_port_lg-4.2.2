.class public final Lcom/lge/ims/provider/uc/UC$Data;
.super Ljava/lang/Object;
.source "UC.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/provider/uc/UC;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Data"
.end annotation


# static fields
.field public static final AUTHORITY:Ljava/lang/String; = "com.lge.ims.provider.uc"

.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final REG_STATE_OFF:I = 0x0

.field public static final REG_STATE_ON:I = 0x1

.field public static final REG_STATE_UNSUPPORTED:I = 0x2

.field public static final TABLE_NAME:Ljava/lang/String; = "ucstate"

.field private static final UC:[Ljava/lang/String;

.field private static final UC_STATE:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    #@0
    .prologue
    const/4 v6, 0x3

    #@1
    const/4 v5, 0x2

    #@2
    const/4 v4, 0x1

    #@3
    const/4 v3, 0x0

    #@4
    .line 26
    const-string v0, "content://com.lge.ims.provider.uc/ucstate"

    #@6
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@9
    move-result-object v0

    #@a
    sput-object v0, Lcom/lge/ims/provider/uc/UC$Data;->CONTENT_URI:Landroid/net/Uri;

    #@c
    .line 33
    const/4 v0, 0x6

    #@d
    new-array v0, v0, [Ljava/lang/String;

    #@f
    const-string v1, "vt_state"

    #@11
    aput-object v1, v0, v3

    #@13
    const-string v1, "voip_state"

    #@15
    aput-object v1, v0, v4

    #@17
    const-string v1, "uc_reg_state"

    #@19
    aput-object v1, v0, v5

    #@1b
    const-string v1, "voip_enabled"

    #@1d
    aput-object v1, v0, v6

    #@1f
    const/4 v1, 0x4

    #@20
    const-string v2, "vt_enabled"

    #@22
    aput-object v2, v0, v1

    #@24
    const/4 v1, 0x5

    #@25
    const-string v2, "feature_smartcall"

    #@27
    aput-object v2, v0, v1

    #@29
    sput-object v0, Lcom/lge/ims/provider/uc/UC$Data;->UC:[Ljava/lang/String;

    #@2b
    .line 44
    new-array v0, v6, [Ljava/lang/String;

    #@2d
    const-string v1, "vt_state"

    #@2f
    aput-object v1, v0, v3

    #@31
    const-string v1, "voip_state"

    #@33
    aput-object v1, v0, v4

    #@35
    const-string v1, "uc_reg_state"

    #@37
    aput-object v1, v0, v5

    #@39
    sput-object v0, Lcom/lge/ims/provider/uc/UC$Data;->UC_STATE:[Ljava/lang/String;

    #@3b
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 18
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static getUCStateStr()[Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 56
    sget-object v0, Lcom/lge/ims/provider/uc/UC$Data;->UC_STATE:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public static getUCStr()[Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 52
    sget-object v0, Lcom/lge/ims/provider/uc/UC$Data;->UC:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public static getUcRegStateStr()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 68
    const-string v0, "uc_reg_state"

    #@2
    return-object v0
.end method

.method public static getVTEnabledStr()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 76
    const-string v0, "vt_enabled"

    #@2
    return-object v0
.end method

.method public static getVoIPEnabledStr()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 72
    const-string v0, "voip_enabled"

    #@2
    return-object v0
.end method

.method public static getVoIPStateStr()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 64
    const-string v0, "voip_state"

    #@2
    return-object v0
.end method

.method public static getVtStateStr()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 60
    const-string v0, "vt_state"

    #@2
    return-object v0
.end method
