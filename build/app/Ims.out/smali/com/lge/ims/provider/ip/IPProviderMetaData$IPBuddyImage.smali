.class public final Lcom/lge/ims/provider/ip/IPProviderMetaData$IPBuddyImage;
.super Ljava/lang/Object;
.source "IPProviderMetaData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/provider/ip/IPProviderMetaData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "IPBuddyImage"
.end annotation


# static fields
.field public static final CONTENT_CAPABILITY_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/com.lge.ims.ip.buddyimage"

.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final INDEX_KEY_FILE_PATH:I = 0x2

.field public static final INDEX_KEY_ID:I = 0x0

.field public static final INDEX_KEY_NORMAL_MSISDN:I = 0x1

.field public static final INDEX_KEY_TIME_STAMP:I = 0x3

.field public static final KEY_FILE_PATH:Ljava/lang/String; = "file_path"

.field public static final KEY_ID:Ljava/lang/String; = "_id"

.field public static final KEY_NORMAL_MSISDN:Ljava/lang/String; = "nor_msisdn"

.field public static final KEY_TIME_STAMP:Ljava/lang/String; = "time_stamp"

.field public static final TABLE_NAME:Ljava/lang/String; = "ip_buddyimage"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 202
    const-string v0, "content://com.lge.ims.provider.ip/ip_buddyimage"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/lge/ims/provider/ip/IPProviderMetaData$IPBuddyImage;->CONTENT_URI:Landroid/net/Uri;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 198
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
