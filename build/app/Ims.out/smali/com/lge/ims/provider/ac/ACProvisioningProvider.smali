.class public Lcom/lge/ims/provider/ac/ACProvisioningProvider;
.super Landroid/content/ContentProvider;
.source "ACProvisioningProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/provider/ac/ACProvisioningProvider$DatabaseHelper;
    }
.end annotation


# static fields
.field private static final DATABASE_NAME:Ljava/lang/String; = "ac_provisioning.db"

.field private static final DATABASE_VERSION:I = 0x2

.field protected static final PM_IMSI:Ljava/util/HashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static final PM_PROXY_ADDRESS:Ljava/util/HashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static final PM_SERVICE_AVAILABILITY:Ljava/util/HashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static final PM_SUBSCRIBER:Ljava/util/HashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static final PM_VERSION:Ljava/util/HashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "ACProvisioningProvider"

.field protected static final URI_IMSI:I = 0x1

.field protected static final URI_MATCHER:Landroid/content/UriMatcher; = null

.field protected static final URI_PROXY_ADDRESS:I = 0x4

.field protected static final URI_SERVICE_AVAILABILITY:I = 0x5

.field protected static final URI_SUBSCRIBER:I = 0x3

.field protected static final URI_VERSION:I = 0x2


# instance fields
.field private mDB:Landroid/database/sqlite/SQLiteDatabase;

.field private mOpenHelper:Lcom/lge/ims/provider/ac/ACProvisioningProvider$DatabaseHelper;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    .line 346
    new-instance v0, Landroid/content/UriMatcher;

    #@2
    const/4 v1, -0x1

    #@3
    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    #@6
    sput-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@8
    .line 347
    sget-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@a
    const-string v1, "com.lge.ims.provider.ac_provisioning"

    #@c
    const-string v2, "imsi"

    #@e
    const/4 v3, 0x1

    #@f
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@12
    .line 348
    sget-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@14
    const-string v1, "com.lge.ims.provider.ac_provisioning"

    #@16
    const-string v2, "version"

    #@18
    const/4 v3, 0x2

    #@19
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@1c
    .line 349
    sget-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@1e
    const-string v1, "com.lge.ims.provider.ac_provisioning"

    #@20
    const-string v2, "subscriber"

    #@22
    const/4 v3, 0x3

    #@23
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@26
    .line 350
    sget-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@28
    const-string v1, "com.lge.ims.provider.ac_provisioning"

    #@2a
    const-string v2, "proxy_address"

    #@2c
    const/4 v3, 0x4

    #@2d
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@30
    .line 351
    sget-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@32
    const-string v1, "com.lge.ims.provider.ac_provisioning"

    #@34
    const-string v2, "service_availability"

    #@36
    const/4 v3, 0x5

    #@37
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@3a
    .line 353
    new-instance v0, Ljava/util/HashMap;

    #@3c
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@3f
    sput-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_IMSI:Ljava/util/HashMap;

    #@41
    .line 354
    sget-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_IMSI:Ljava/util/HashMap;

    #@43
    const-string v1, "id"

    #@45
    const-string v2, "id"

    #@47
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4a
    .line 355
    sget-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_IMSI:Ljava/util/HashMap;

    #@4c
    const-string v1, "msisdn"

    #@4e
    const-string v2, "msisdn"

    #@50
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@53
    .line 356
    sget-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_IMSI:Ljava/util/HashMap;

    #@55
    const-string v1, "ref_id"

    #@57
    const-string v2, "ref_id"

    #@59
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5c
    .line 357
    sget-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_IMSI:Ljava/util/HashMap;

    #@5e
    const-string v1, "next_ref_id"

    #@60
    const-string v2, "next_ref_id"

    #@62
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@65
    .line 359
    new-instance v0, Ljava/util/HashMap;

    #@67
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@6a
    sput-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_VERSION:Ljava/util/HashMap;

    #@6c
    .line 360
    sget-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_VERSION:Ljava/util/HashMap;

    #@6e
    const-string v1, "id"

    #@70
    const-string v2, "id"

    #@72
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@75
    .line 361
    sget-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_VERSION:Ljava/util/HashMap;

    #@77
    const-string v1, "version"

    #@79
    const-string v2, "version"

    #@7b
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@7e
    .line 362
    sget-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_VERSION:Ljava/util/HashMap;

    #@80
    const-string v1, "validity"

    #@82
    const-string v2, "validity"

    #@84
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@87
    .line 363
    sget-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_VERSION:Ljava/util/HashMap;

    #@89
    const-string v1, "validity_period"

    #@8b
    const-string v2, "validity_period"

    #@8d
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@90
    .line 365
    new-instance v0, Ljava/util/HashMap;

    #@92
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@95
    sput-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@97
    .line 366
    sget-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@99
    const-string v1, "id"

    #@9b
    const-string v2, "id"

    #@9d
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@a0
    .line 367
    sget-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@a2
    const-string v1, "impi"

    #@a4
    const-string v2, "impi"

    #@a6
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@a9
    .line 368
    sget-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@ab
    const-string v1, "impu_0"

    #@ad
    const-string v2, "impu_0"

    #@af
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@b2
    .line 369
    sget-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@b4
    const-string v1, "impu_1"

    #@b6
    const-string v2, "impu_1"

    #@b8
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@bb
    .line 370
    sget-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@bd
    const-string v1, "impu_2"

    #@bf
    const-string v2, "impu_2"

    #@c1
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@c4
    .line 371
    sget-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@c6
    const-string v1, "home_domain_name"

    #@c8
    const-string v2, "home_domain_name"

    #@ca
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@cd
    .line 372
    sget-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@cf
    const-string v1, "auth_type"

    #@d1
    const-string v2, "auth_type"

    #@d3
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@d6
    .line 373
    sget-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@d8
    const-string v1, "auth_realm"

    #@da
    const-string v2, "auth_realm"

    #@dc
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@df
    .line 374
    sget-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@e1
    const-string v1, "auth_username"

    #@e3
    const-string v2, "auth_username"

    #@e5
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@e8
    .line 375
    sget-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@ea
    const-string v1, "auth_password"

    #@ec
    const-string v2, "auth_password"

    #@ee
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@f1
    .line 377
    new-instance v0, Ljava/util/HashMap;

    #@f3
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@f6
    sput-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_PROXY_ADDRESS:Ljava/util/HashMap;

    #@f8
    .line 378
    sget-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_PROXY_ADDRESS:Ljava/util/HashMap;

    #@fa
    const-string v1, "id"

    #@fc
    const-string v2, "id"

    #@fe
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@101
    .line 379
    sget-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_PROXY_ADDRESS:Ljava/util/HashMap;

    #@103
    const-string v1, "sbc_address"

    #@105
    const-string v2, "sbc_address"

    #@107
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@10a
    .line 380
    sget-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_PROXY_ADDRESS:Ljava/util/HashMap;

    #@10c
    const-string v1, "sbc_address_type"

    #@10e
    const-string v2, "sbc_address_type"

    #@110
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@113
    .line 381
    sget-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_PROXY_ADDRESS:Ljava/util/HashMap;

    #@115
    const-string v1, "sbc_tls_address"

    #@117
    const-string v2, "sbc_tls_address"

    #@119
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@11c
    .line 382
    sget-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_PROXY_ADDRESS:Ljava/util/HashMap;

    #@11e
    const-string v1, "sbc_tls_address_type"

    #@120
    const-string v2, "sbc_tls_address_type"

    #@122
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@125
    .line 383
    sget-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_PROXY_ADDRESS:Ljava/util/HashMap;

    #@127
    const-string v1, "lbo_pcscf_address"

    #@129
    const-string v2, "lbo_pcscf_address"

    #@12b
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@12e
    .line 384
    sget-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_PROXY_ADDRESS:Ljava/util/HashMap;

    #@130
    const-string v1, "lbo_pcscf_address_type"

    #@132
    const-string v2, "lbo_pcscf_address_type"

    #@134
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@137
    .line 386
    new-instance v0, Ljava/util/HashMap;

    #@139
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@13c
    sput-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_SERVICE_AVAILABILITY:Ljava/util/HashMap;

    #@13e
    .line 387
    sget-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_SERVICE_AVAILABILITY:Ljava/util/HashMap;

    #@140
    const-string v1, "id"

    #@142
    const-string v2, "id"

    #@144
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@147
    .line 388
    sget-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_SERVICE_AVAILABILITY:Ljava/util/HashMap;

    #@149
    const-string v1, "rcs_oem"

    #@14b
    const-string v2, "rcs_oem"

    #@14d
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@150
    .line 389
    sget-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_SERVICE_AVAILABILITY:Ljava/util/HashMap;

    #@152
    const-string v1, "volte"

    #@154
    const-string v2, "volte"

    #@156
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@159
    .line 390
    sget-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_SERVICE_AVAILABILITY:Ljava/util/HashMap;

    #@15b
    const-string v1, "vt"

    #@15d
    const-string v2, "vt"

    #@15f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@162
    .line 391
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 24
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    #@4
    .line 49
    iput-object v0, p0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    #@6
    .line 50
    iput-object v0, p0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->mOpenHelper:Lcom/lge/ims/provider/ac/ACProvisioningProvider$DatabaseHelper;

    #@8
    .line 55
    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 10
    .parameter "uri"
    .parameter "selection"
    .parameter "selectionArgs"

    #@0
    .prologue
    .line 149
    sget-object v3, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@2
    invoke-virtual {v3, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@5
    move-result v3

    #@6
    packed-switch v3, :pswitch_data_68

    #@9
    .line 166
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@b
    new-instance v4, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v5, "Unknown URI "

    #@12
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v4

    #@16
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v4

    #@1a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v4

    #@1e
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@21
    throw v3

    #@22
    .line 151
    :pswitch_22
    const-string v2, "imsi"

    #@24
    .line 169
    .local v2, tableName:Ljava/lang/String;
    :goto_24
    const/4 v0, 0x0

    #@25
    .line 172
    .local v0, count:I
    :try_start_25
    iget-object v3, p0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    #@27
    invoke-virtual {v3, v2, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2a
    .catch Ljava/lang/Exception; {:try_start_25 .. :try_end_2a} :catch_46

    #@2a
    move-result v0

    #@2b
    .line 178
    :goto_2b
    if-lez v0, :cond_39

    #@2d
    .line 179
    invoke-virtual {p0}, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->getContext()Landroid/content/Context;

    #@30
    move-result-object v3

    #@31
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@34
    move-result-object v3

    #@35
    const/4 v4, 0x0

    #@36
    invoke-virtual {v3, p1, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    #@39
    .line 182
    :cond_39
    return v0

    #@3a
    .line 154
    .end local v0           #count:I
    .end local v2           #tableName:Ljava/lang/String;
    :pswitch_3a
    const-string v2, "version"

    #@3c
    .line 155
    .restart local v2       #tableName:Ljava/lang/String;
    goto :goto_24

    #@3d
    .line 157
    .end local v2           #tableName:Ljava/lang/String;
    :pswitch_3d
    const-string v2, "subscriber"

    #@3f
    .line 158
    .restart local v2       #tableName:Ljava/lang/String;
    goto :goto_24

    #@40
    .line 160
    .end local v2           #tableName:Ljava/lang/String;
    :pswitch_40
    const-string v2, "proxy_address"

    #@42
    .line 161
    .restart local v2       #tableName:Ljava/lang/String;
    goto :goto_24

    #@43
    .line 163
    .end local v2           #tableName:Ljava/lang/String;
    :pswitch_43
    const-string v2, "service_availability"

    #@45
    .line 164
    .restart local v2       #tableName:Ljava/lang/String;
    goto :goto_24

    #@46
    .line 173
    .restart local v0       #count:I
    :catch_46
    move-exception v1

    #@47
    .line 174
    .local v1, e:Ljava/lang/Exception;
    const-string v3, "ACProvisioningProvider"

    #@49
    new-instance v4, Ljava/lang/StringBuilder;

    #@4b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4e
    const-string v5, "Exception :: "

    #@50
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v4

    #@54
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@57
    move-result-object v5

    #@58
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v4

    #@5c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v4

    #@60
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@63
    .line 175
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    #@66
    goto :goto_2b

    #@67
    .line 149
    nop

    #@68
    :pswitch_data_68
    .packed-switch 0x1
        :pswitch_22
        :pswitch_3a
        :pswitch_3d
        :pswitch_40
        :pswitch_43
    .end packed-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .registers 5
    .parameter "uri"

    #@0
    .prologue
    .line 187
    sget-object v0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@5
    move-result v0

    #@6
    packed-switch v0, :pswitch_data_32

    #@9
    .line 199
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@b
    new-instance v1, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v2, "Unsupported URI: "

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@21
    throw v0

    #@22
    .line 189
    :pswitch_22
    const-string v0, "vnd.android.cursor.dir/com.lge.ims.provider.ac_provisioning.imsi"

    #@24
    .line 197
    :goto_24
    return-object v0

    #@25
    .line 191
    :pswitch_25
    const-string v0, "vnd.android.cursor.dir/com.lge.ims.provider.ac_provisioning.version"

    #@27
    goto :goto_24

    #@28
    .line 193
    :pswitch_28
    const-string v0, "vnd.android.cursor.dir/com.lge.ims.provider.ac_provisioning.subscriber"

    #@2a
    goto :goto_24

    #@2b
    .line 195
    :pswitch_2b
    const-string v0, "vnd.android.cursor.dir/com.lge.ims.provider.ac_provisioning.proxy_address"

    #@2d
    goto :goto_24

    #@2e
    .line 197
    :pswitch_2e
    const-string v0, "vnd.android.cursor.dir/com.lge.ims.provider.ac_provisioning.service_availability"

    #@30
    goto :goto_24

    #@31
    .line 187
    nop

    #@32
    :pswitch_data_32
    .packed-switch 0x1
        :pswitch_22
        :pswitch_25
        :pswitch_28
        :pswitch_2b
        :pswitch_2e
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .registers 13
    .parameter "uri"
    .parameter "values"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 208
    sget-object v6, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@3
    invoke-virtual {v6, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@6
    move-result v6

    #@7
    packed-switch v6, :pswitch_data_94

    #@a
    .line 230
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@c
    new-instance v7, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v8, "Unknown URI "

    #@13
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v7

    #@17
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v7

    #@1b
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v7

    #@1f
    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@22
    throw v6

    #@23
    .line 210
    :pswitch_23
    const-string v5, "imsi"

    #@25
    .line 211
    .local v5, tableName:Ljava/lang/String;
    sget-object v0, Lcom/lge/ims/provider/ac/AC$Provisioning$IMSI;->CONTENT_URI:Landroid/net/Uri;

    #@27
    .line 233
    .local v0, contentUri:Landroid/net/Uri;
    :goto_27
    const-wide/16 v3, 0x0

    #@29
    .line 236
    .local v3, rowId:J
    :try_start_29
    iget-object v6, p0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    #@2b
    const/4 v7, 0x0

    #@2c
    invoke-virtual {v6, v5, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_2f
    .catch Ljava/lang/Exception; {:try_start_29 .. :try_end_2f} :catch_5a

    #@2f
    move-result-wide v3

    #@30
    .line 242
    :goto_30
    const-wide/16 v6, 0x0

    #@32
    cmp-long v6, v3, v6

    #@34
    if-lez v6, :cond_7b

    #@36
    .line 243
    invoke-static {v0, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    #@39
    move-result-object v2

    #@3a
    .line 244
    .local v2, newUri:Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->getContext()Landroid/content/Context;

    #@3d
    move-result-object v6

    #@3e
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@41
    move-result-object v6

    #@42
    invoke-virtual {v6, v2, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    #@45
    .line 245
    return-object v2

    #@46
    .line 214
    .end local v0           #contentUri:Landroid/net/Uri;
    .end local v2           #newUri:Landroid/net/Uri;
    .end local v3           #rowId:J
    .end local v5           #tableName:Ljava/lang/String;
    :pswitch_46
    const-string v5, "version"

    #@48
    .line 215
    .restart local v5       #tableName:Ljava/lang/String;
    sget-object v0, Lcom/lge/ims/provider/ac/AC$Provisioning$Version;->CONTENT_URI:Landroid/net/Uri;

    #@4a
    .line 216
    .restart local v0       #contentUri:Landroid/net/Uri;
    goto :goto_27

    #@4b
    .line 218
    .end local v0           #contentUri:Landroid/net/Uri;
    .end local v5           #tableName:Ljava/lang/String;
    :pswitch_4b
    const-string v5, "subscriber"

    #@4d
    .line 219
    .restart local v5       #tableName:Ljava/lang/String;
    sget-object v0, Lcom/lge/ims/provider/ac/AC$Provisioning$Subscriber;->CONTENT_URI:Landroid/net/Uri;

    #@4f
    .line 220
    .restart local v0       #contentUri:Landroid/net/Uri;
    goto :goto_27

    #@50
    .line 222
    .end local v0           #contentUri:Landroid/net/Uri;
    .end local v5           #tableName:Ljava/lang/String;
    :pswitch_50
    const-string v5, "proxy_address"

    #@52
    .line 223
    .restart local v5       #tableName:Ljava/lang/String;
    sget-object v0, Lcom/lge/ims/provider/ac/AC$Provisioning$ProxyAddress;->CONTENT_URI:Landroid/net/Uri;

    #@54
    .line 224
    .restart local v0       #contentUri:Landroid/net/Uri;
    goto :goto_27

    #@55
    .line 226
    .end local v0           #contentUri:Landroid/net/Uri;
    .end local v5           #tableName:Ljava/lang/String;
    :pswitch_55
    const-string v5, "service_availability"

    #@57
    .line 227
    .restart local v5       #tableName:Ljava/lang/String;
    sget-object v0, Lcom/lge/ims/provider/ac/AC$Provisioning$ServiceAvailability;->CONTENT_URI:Landroid/net/Uri;

    #@59
    .line 228
    .restart local v0       #contentUri:Landroid/net/Uri;
    goto :goto_27

    #@5a
    .line 237
    .restart local v3       #rowId:J
    :catch_5a
    move-exception v1

    #@5b
    .line 238
    .local v1, e:Ljava/lang/Exception;
    const-string v6, "ACProvisioningProvider"

    #@5d
    new-instance v7, Ljava/lang/StringBuilder;

    #@5f
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@62
    const-string v8, "Exception :: "

    #@64
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v7

    #@68
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@6b
    move-result-object v8

    #@6c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v7

    #@70
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@73
    move-result-object v7

    #@74
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@77
    .line 239
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    #@7a
    goto :goto_30

    #@7b
    .line 248
    .end local v1           #e:Ljava/lang/Exception;
    :cond_7b
    new-instance v6, Landroid/database/SQLException;

    #@7d
    new-instance v7, Ljava/lang/StringBuilder;

    #@7f
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@82
    const-string v8, "Failed to insert row into "

    #@84
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v7

    #@88
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v7

    #@8c
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8f
    move-result-object v7

    #@90
    invoke-direct {v6, v7}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    #@93
    throw v6

    #@94
    .line 208
    :pswitch_data_94
    .packed-switch 0x1
        :pswitch_23
        :pswitch_46
        :pswitch_4b
        :pswitch_50
        :pswitch_55
    .end packed-switch
.end method

.method public onCreate()Z
    .registers 7

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 253
    const-string v1, "ACProvisioningProvider"

    #@3
    const-string v2, "ACProvisioningProvider is created ..."

    #@5
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 255
    new-instance v1, Lcom/lge/ims/provider/ac/ACProvisioningProvider$DatabaseHelper;

    #@a
    invoke-virtual {p0}, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->getContext()Landroid/content/Context;

    #@d
    move-result-object v2

    #@e
    const-string v3, "ac_provisioning.db"

    #@10
    const/4 v4, 0x0

    #@11
    const/4 v5, 0x2

    #@12
    invoke-direct {v1, v2, v3, v4, v5}, Lcom/lge/ims/provider/ac/ACProvisioningProvider$DatabaseHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    #@15
    iput-object v1, p0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->mOpenHelper:Lcom/lge/ims/provider/ac/ACProvisioningProvider$DatabaseHelper;

    #@17
    .line 257
    iget-object v1, p0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->mOpenHelper:Lcom/lge/ims/provider/ac/ACProvisioningProvider$DatabaseHelper;

    #@19
    if-nez v1, :cond_1c

    #@1b
    .line 267
    :cond_1b
    :goto_1b
    return v0

    #@1c
    .line 261
    :cond_1c
    iget-object v1, p0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->mOpenHelper:Lcom/lge/ims/provider/ac/ACProvisioningProvider$DatabaseHelper;

    #@1e
    invoke-virtual {v1}, Lcom/lge/ims/provider/ac/ACProvisioningProvider$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@21
    move-result-object v1

    #@22
    iput-object v1, p0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    #@24
    .line 263
    iget-object v1, p0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    #@26
    if-eqz v1, :cond_1b

    #@28
    .line 267
    const/4 v0, 0x1

    #@29
    goto :goto_1b
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 15
    .parameter "uri"
    .parameter "projection"
    .parameter "selection"
    .parameter "selectionArgs"
    .parameter "sortOrder"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 272
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    #@3
    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    #@6
    .line 274
    .local v0, queryBuilder:Landroid/database/sqlite/SQLiteQueryBuilder;
    sget-object v1, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@8
    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@b
    move-result v1

    #@c
    packed-switch v1, :pswitch_data_74

    #@f
    .line 296
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@11
    new-instance v2, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v3, "Unknown URI "

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@27
    throw v1

    #@28
    .line 276
    :pswitch_28
    const-string v1, "imsi"

    #@2a
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    #@2d
    .line 277
    sget-object v1, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_IMSI:Ljava/util/HashMap;

    #@2f
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    #@32
    .line 299
    :goto_32
    iget-object v1, p0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    #@34
    move-object v2, p2

    #@35
    move-object v3, p3

    #@36
    move-object v4, p4

    #@37
    move-object v6, v5

    #@38
    move-object v7, v5

    #@39
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@3c
    move-result-object v8

    #@3d
    .line 301
    .local v8, cursor:Landroid/database/Cursor;
    if-nez v8, :cond_46

    #@3f
    .line 302
    const-string v1, "ACProvisioningProvider"

    #@41
    const-string v2, "Cursor is null"

    #@43
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@46
    .line 305
    :cond_46
    return-object v8

    #@47
    .line 280
    .end local v8           #cursor:Landroid/database/Cursor;
    :pswitch_47
    const-string v1, "version"

    #@49
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    #@4c
    .line 281
    sget-object v1, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_VERSION:Ljava/util/HashMap;

    #@4e
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    #@51
    goto :goto_32

    #@52
    .line 284
    :pswitch_52
    const-string v1, "subscriber"

    #@54
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    #@57
    .line 285
    sget-object v1, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_SUBSCRIBER:Ljava/util/HashMap;

    #@59
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    #@5c
    goto :goto_32

    #@5d
    .line 288
    :pswitch_5d
    const-string v1, "proxy_address"

    #@5f
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    #@62
    .line 289
    sget-object v1, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_PROXY_ADDRESS:Ljava/util/HashMap;

    #@64
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    #@67
    goto :goto_32

    #@68
    .line 292
    :pswitch_68
    const-string v1, "service_availability"

    #@6a
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    #@6d
    .line 293
    sget-object v1, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->PM_SERVICE_AVAILABILITY:Ljava/util/HashMap;

    #@6f
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    #@72
    goto :goto_32

    #@73
    .line 274
    nop

    #@74
    :pswitch_data_74
    .packed-switch 0x1
        :pswitch_28
        :pswitch_47
        :pswitch_52
        :pswitch_5d
        :pswitch_68
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 10
    .parameter "uri"
    .parameter "values"
    .parameter "where"
    .parameter "whereArgs"

    #@0
    .prologue
    .line 310
    const/4 v1, 0x0

    #@1
    .line 312
    .local v1, tableName:Ljava/lang/String;
    sget-object v2, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@3
    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@6
    move-result v2

    #@7
    packed-switch v2, :pswitch_data_72

    #@a
    .line 329
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@c
    new-instance v3, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v4, "Unknown URI "

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@22
    throw v2

    #@23
    .line 314
    :pswitch_23
    const-string v1, "imsi"

    #@25
    .line 332
    :goto_25
    if-nez v1, :cond_35

    #@27
    .line 333
    const/4 v0, 0x0

    #@28
    .line 342
    :cond_28
    :goto_28
    return v0

    #@29
    .line 317
    :pswitch_29
    const-string v1, "version"

    #@2b
    .line 318
    goto :goto_25

    #@2c
    .line 320
    :pswitch_2c
    const-string v1, "subscriber"

    #@2e
    .line 321
    goto :goto_25

    #@2f
    .line 323
    :pswitch_2f
    const-string v1, "proxy_address"

    #@31
    .line 324
    goto :goto_25

    #@32
    .line 326
    :pswitch_32
    const-string v1, "service_availability"

    #@34
    .line 327
    goto :goto_25

    #@35
    .line 336
    :cond_35
    iget-object v2, p0, Lcom/lge/ims/provider/ac/ACProvisioningProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    #@37
    invoke-virtual {v2, v1, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@3a
    move-result v0

    #@3b
    .line 338
    .local v0, count:I
    const-string v2, "user"

    #@3d
    sget-object v3, Lcom/lge/ims/Configuration;->BUILD_TYPE:Ljava/lang/String;

    #@3f
    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@42
    move-result v2

    #@43
    if-nez v2, :cond_28

    #@45
    .line 339
    const-string v2, "ACProvisioningProvider"

    #@47
    new-instance v3, Ljava/lang/StringBuilder;

    #@49
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    const-string v4, "update :: "

    #@4e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v3

    #@52
    invoke-virtual {p2}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    #@55
    move-result-object v4

    #@56
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v3

    #@5a
    const-string v4, "; count ("

    #@5c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v3

    #@60
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@63
    move-result-object v3

    #@64
    const-string v4, ")"

    #@66
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v3

    #@6a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d
    move-result-object v3

    #@6e
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@71
    goto :goto_28

    #@72
    .line 312
    :pswitch_data_72
    .packed-switch 0x1
        :pswitch_23
        :pswitch_29
        :pswitch_2c
        :pswitch_2f
        :pswitch_32
    .end packed-switch
.end method
