.class public final Lcom/lge/ims/provider/ac/AC$Settings$ConnectionServer;
.super Ljava/lang/Object;
.source "AC.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/provider/ac/AC$Settings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ConnectionServer"
.end annotation


# static fields
.field public static final COMMERCIAL_NETWORK:Ljava/lang/String; = "commercial_network"

.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final SERVER_SELECTION:Ljava/lang/String; = "server_selection"

.field public static final TABLE_NAME:Ljava/lang/String; = "connection_server"

.field public static final TESTBED:Ljava/lang/String; = "testbed"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 54
    const-string v0, "content://com.lge.ims.provider.ac_settings/connection_server"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/lge/ims/provider/ac/AC$Settings$ConnectionServer;->CONTENT_URI:Landroid/net/Uri;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 48
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
