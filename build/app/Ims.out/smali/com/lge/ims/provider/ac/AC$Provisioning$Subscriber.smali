.class public final Lcom/lge/ims/provider/ac/AC$Provisioning$Subscriber;
.super Ljava/lang/Object;
.source "AC.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/provider/ac/AC$Provisioning;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Subscriber"
.end annotation


# static fields
.field public static final AUTH_PASSWORD:Ljava/lang/String; = "auth_password"

.field public static final AUTH_REALM:Ljava/lang/String; = "auth_realm"

.field public static final AUTH_TYPE:Ljava/lang/String; = "auth_type"

.field public static final AUTH_USERNAME:Ljava/lang/String; = "auth_username"

.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final HOME_DOMAIN_NAME:Ljava/lang/String; = "home_domain_name"

.field public static final ID:Ljava/lang/String; = "id"

.field public static final IMPI:Ljava/lang/String; = "impi"

.field public static final IMPU_0:Ljava/lang/String; = "impu_0"

.field public static final IMPU_1:Ljava/lang/String; = "impu_1"

.field public static final IMPU_2:Ljava/lang/String; = "impu_2"

.field public static final TABLE_NAME:Ljava/lang/String; = "subscriber"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 98
    const-string v0, "content://com.lge.ims.provider.ac_provisioning/subscriber"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/lge/ims/provider/ac/AC$Provisioning$Subscriber;->CONTENT_URI:Landroid/net/Uri;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 93
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
