.class public Lcom/lge/ims/provider/xdm/XDMProvider;
.super Landroid/content/ContentProvider;
.source "XDMProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/provider/xdm/XDMProvider$XDMDatabaseHelper;
    }
.end annotation


# static fields
.field private static final URI_MATCHER:Landroid/content/UriMatcher; = null

.field private static final XDM_ETAG_TBL:I = 0x1

.field private static objXDMEtagProjectionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private objXDMDatabaseHelper:Lcom/lge/ims/provider/xdm/XDMProvider$XDMDatabaseHelper;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    .line 40
    new-instance v0, Ljava/util/HashMap;

    #@2
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@5
    sput-object v0, Lcom/lge/ims/provider/xdm/XDMProvider;->objXDMEtagProjectionMap:Ljava/util/HashMap;

    #@7
    .line 41
    sget-object v0, Lcom/lge/ims/provider/xdm/XDMProvider;->objXDMEtagProjectionMap:Ljava/util/HashMap;

    #@9
    const-string v1, "list_etag"

    #@b
    const-string v2, "list_etag"

    #@d
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@10
    .line 42
    sget-object v0, Lcom/lge/ims/provider/xdm/XDMProvider;->objXDMEtagProjectionMap:Ljava/util/HashMap;

    #@12
    const-string v1, "rls_etag"

    #@14
    const-string v2, "rls_etag"

    #@16
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@19
    .line 43
    sget-object v0, Lcom/lge/ims/provider/xdm/XDMProvider;->objXDMEtagProjectionMap:Ljava/util/HashMap;

    #@1b
    const-string v1, "rule_etag"

    #@1d
    const-string v2, "rule_etag"

    #@1f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@22
    .line 44
    sget-object v0, Lcom/lge/ims/provider/xdm/XDMProvider;->objXDMEtagProjectionMap:Ljava/util/HashMap;

    #@24
    const-string v1, "pidf_etag"

    #@26
    const-string v2, "pidf_etag"

    #@28
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2b
    .line 45
    sget-object v0, Lcom/lge/ims/provider/xdm/XDMProvider;->objXDMEtagProjectionMap:Ljava/util/HashMap;

    #@2d
    const-string v1, "rls_uri"

    #@2f
    const-string v2, "rls_uri"

    #@31
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@34
    .line 53
    new-instance v0, Landroid/content/UriMatcher;

    #@36
    const/4 v1, -0x1

    #@37
    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    #@3a
    sput-object v0, Lcom/lge/ims/provider/xdm/XDMProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@3c
    .line 54
    sget-object v0, Lcom/lge/ims/provider/xdm/XDMProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@3e
    const-string v1, "com.lge.ims.provider.xdm"

    #@40
    const-string v2, "ims_xdm"

    #@42
    const/4 v3, 0x1

    #@43
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@46
    .line 55
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 35
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    #@3
    .line 60
    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 9
    .parameter "objUri"
    .parameter "strWhere"
    .parameter "astrWhereArgs"

    #@0
    .prologue
    .line 226
    iget-object v3, p0, Lcom/lge/ims/provider/xdm/XDMProvider;->objXDMDatabaseHelper:Lcom/lge/ims/provider/xdm/XDMProvider$XDMDatabaseHelper;

    #@2
    invoke-virtual {v3}, Lcom/lge/ims/provider/xdm/XDMProvider$XDMDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@5
    move-result-object v2

    #@6
    .line 227
    .local v2, objSQLiteDatabase:Landroid/database/sqlite/SQLiteDatabase;
    const/4 v1, -0x1

    #@7
    .line 230
    .local v1, nCount:I
    :try_start_7
    sget-object v3, Lcom/lge/ims/provider/xdm/XDMProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@9
    invoke-virtual {v3, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@c
    move-result v3

    #@d
    packed-switch v3, :pswitch_data_2e

    #@10
    .line 238
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@12
    const-string v4, "Uri is invaild"

    #@14
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v3
    :try_end_18
    .catchall {:try_start_7 .. :try_end_18} :catchall_2c
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_18} :catch_18

    #@18
    .line 241
    :catch_18
    move-exception v0

    #@19
    .line 242
    .local v0, e:Landroid/database/sqlite/SQLiteException;
    :try_start_19
    invoke-static {v0}, Landroid/database/sqlite/SqliteWrapper;->isLowMemory(Landroid/database/sqlite/SQLiteException;)Z

    #@1c
    move-result v3

    #@1d
    if-eqz v3, :cond_24

    #@1f
    .line 243
    const-string v3, "[ERROR]XDMProvider::delete - intenal memory storage is Full"

    #@21
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_24
    .catchall {:try_start_19 .. :try_end_24} :catchall_2c

    #@24
    .line 248
    .end local v0           #e:Landroid/database/sqlite/SQLiteException;
    :cond_24
    :goto_24
    return v1

    #@25
    .line 233
    :pswitch_25
    :try_start_25
    const-string v3, "ims_xdm"

    #@27
    invoke-virtual {v2, v3, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2a
    .catchall {:try_start_25 .. :try_end_2a} :catchall_2c
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_25 .. :try_end_2a} :catch_18

    #@2a
    move-result v1

    #@2b
    .line 235
    goto :goto_24

    #@2c
    .line 245
    :catchall_2c
    move-exception v3

    #@2d
    throw v3

    #@2e
    .line 230
    :pswitch_data_2e
    .packed-switch 0x1
        :pswitch_25
    .end packed-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .registers 5
    .parameter "objUri"

    #@0
    .prologue
    .line 122
    sget-object v0, Lcom/lge/ims/provider/xdm/XDMProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@5
    move-result v0

    #@6
    packed-switch v0, :pswitch_data_26

    #@9
    .line 129
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@b
    new-instance v1, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v2, "Uri is invalid"

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@21
    throw v0

    #@22
    .line 125
    :pswitch_22
    const-string v0, "vnd.android.cursor.item/com.lge.ims.xdm.etag"

    #@24
    return-object v0

    #@25
    .line 122
    nop

    #@26
    :pswitch_data_26
    .packed-switch 0x1
        :pswitch_22
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .registers 12
    .parameter "objUri"
    .parameter "objContentValue"

    #@0
    .prologue
    .line 166
    sget-object v6, Lcom/lge/ims/provider/xdm/XDMProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@2
    invoke-virtual {v6, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@5
    move-result v6

    #@6
    const/4 v7, 0x1

    #@7
    if-eq v6, v7, :cond_11

    #@9
    .line 167
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@b
    const-string v7, "Uri is invaild"

    #@d
    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@10
    throw v6

    #@11
    .line 170
    :cond_11
    iget-object v6, p0, Lcom/lge/ims/provider/xdm/XDMProvider;->objXDMDatabaseHelper:Lcom/lge/ims/provider/xdm/XDMProvider$XDMDatabaseHelper;

    #@13
    invoke-virtual {v6}, Lcom/lge/ims/provider/xdm/XDMProvider$XDMDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@16
    move-result-object v5

    #@17
    .line 171
    .local v5, objSQLiteDatabase:Landroid/database/sqlite/SQLiteDatabase;
    const-wide/16 v2, 0x0

    #@19
    .line 172
    .local v2, nRowId:J
    sget-object v6, Lcom/lge/ims/provider/xdm/XDMProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@1b
    invoke-virtual {v6, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@1e
    move-result v1

    #@1f
    .line 173
    .local v1, match:I
    const/4 v4, 0x0

    #@20
    .line 176
    .local v4, objInsertedRowUri:Landroid/net/Uri;
    packed-switch v1, :pswitch_data_76

    #@23
    .line 185
    :try_start_23
    new-instance v6, Ljava/lang/UnsupportedOperationException;

    #@25
    new-instance v7, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v8, "Cannot insert into URL: "

    #@2c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v7

    #@30
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v7

    #@34
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v7

    #@38
    invoke-direct {v6, v7}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@3b
    throw v6
    :try_end_3c
    .catchall {:try_start_23 .. :try_end_3c} :catchall_73
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_23 .. :try_end_3c} :catch_3c

    #@3c
    .line 187
    :catch_3c
    move-exception v0

    #@3d
    .line 188
    .local v0, e:Landroid/database/sqlite/SQLiteException;
    :try_start_3d
    invoke-static {v0}, Landroid/database/sqlite/SqliteWrapper;->isLowMemory(Landroid/database/sqlite/SQLiteException;)Z

    #@40
    move-result v6

    #@41
    if-eqz v6, :cond_48

    #@43
    .line 189
    const-string v6, "[ERROR]XDMProvider::insert - intenal memory storage is Full"

    #@45
    invoke-static {v6}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_48
    .catchall {:try_start_3d .. :try_end_48} :catchall_73

    #@48
    .line 194
    .end local v0           #e:Landroid/database/sqlite/SQLiteException;
    :cond_48
    :goto_48
    return-object v4

    #@49
    .line 178
    :pswitch_49
    :try_start_49
    const-string v6, "ims_xdm"

    #@4b
    const/4 v7, 0x0

    #@4c
    invoke-virtual {v5, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    #@4f
    move-result-wide v2

    #@50
    .line 179
    new-instance v6, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v7, "IPProvider::insert - nRowId : "

    #@57
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v6

    #@5b
    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v6

    #@5f
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v6

    #@63
    invoke-static {v6}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@66
    .line 180
    const-wide/16 v6, 0x0

    #@68
    cmp-long v6, v2, v6

    #@6a
    if-lez v6, :cond_48

    #@6c
    .line 181
    sget-object v6, Lcom/lge/ims/provider/xdm/XDMProviderMetaData$XDMEtag;->CONTENT_URI:Landroid/net/Uri;

    #@6e
    invoke-static {v6, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;
    :try_end_71
    .catchall {:try_start_49 .. :try_end_71} :catchall_73
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_49 .. :try_end_71} :catch_3c

    #@71
    move-result-object v4

    #@72
    goto :goto_48

    #@73
    .line 191
    :catchall_73
    move-exception v6

    #@74
    throw v6

    #@75
    .line 176
    nop

    #@76
    :pswitch_data_76
    .packed-switch 0x1
        :pswitch_49
    .end packed-switch
.end method

.method public onCreate()Z
    .registers 3

    #@0
    .prologue
    .line 115
    const-string v0, "XDMProvider::onCreate"

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 116
    new-instance v0, Lcom/lge/ims/provider/xdm/XDMProvider$XDMDatabaseHelper;

    #@7
    invoke-virtual {p0}, Lcom/lge/ims/provider/xdm/XDMProvider;->getContext()Landroid/content/Context;

    #@a
    move-result-object v1

    #@b
    invoke-direct {v0, v1}, Lcom/lge/ims/provider/xdm/XDMProvider$XDMDatabaseHelper;-><init>(Landroid/content/Context;)V

    #@e
    iput-object v0, p0, Lcom/lge/ims/provider/xdm/XDMProvider;->objXDMDatabaseHelper:Lcom/lge/ims/provider/xdm/XDMProvider$XDMDatabaseHelper;

    #@10
    .line 117
    const/4 v0, 0x1

    #@11
    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 15
    .parameter "objUri"
    .parameter "astrProjection"
    .parameter "strSelection"
    .parameter "astrSelectionArgs"
    .parameter "objSortOrder"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 136
    new-instance v2, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v3, "XDMProvider::query - Uri : "

    #@8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v2

    #@c
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@f
    move-result-object v3

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    const-string v3, ", Selection : "

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@25
    .line 137
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    #@27
    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    #@2a
    .line 139
    .local v0, objSQLiteQueryBuilder:Landroid/database/sqlite/SQLiteQueryBuilder;
    sget-object v2, Lcom/lge/ims/provider/xdm/XDMProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@2c
    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@2f
    move-result v2

    #@30
    packed-switch v2, :pswitch_data_64

    #@33
    .line 148
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@35
    const-string v3, "Uri is invaild"

    #@37
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@3a
    throw v2

    #@3b
    .line 142
    :pswitch_3b
    const-string v2, "ims_xdm"

    #@3d
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    #@40
    .line 143
    sget-object v2, Lcom/lge/ims/provider/xdm/XDMProvider;->objXDMEtagProjectionMap:Ljava/util/HashMap;

    #@42
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    #@45
    .line 152
    iget-object v2, p0, Lcom/lge/ims/provider/xdm/XDMProvider;->objXDMDatabaseHelper:Lcom/lge/ims/provider/xdm/XDMProvider$XDMDatabaseHelper;

    #@47
    invoke-virtual {v2}, Lcom/lge/ims/provider/xdm/XDMProvider$XDMDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@4a
    move-result-object v1

    #@4b
    .local v1, objSQLiteDatabase:Landroid/database/sqlite/SQLiteDatabase;
    move-object v2, p2

    #@4c
    move-object v3, p3

    #@4d
    move-object v4, p4

    #@4e
    move-object v6, v5

    #@4f
    move-object v7, v5

    #@50
    .line 153
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@53
    move-result-object v8

    #@54
    .line 155
    .local v8, objCursor:Landroid/database/Cursor;
    if-nez v8, :cond_57

    #@56
    .line 161
    :goto_56
    return-object v5

    #@57
    .line 159
    :cond_57
    invoke-virtual {p0}, Lcom/lge/ims/provider/xdm/XDMProvider;->getContext()Landroid/content/Context;

    #@5a
    move-result-object v2

    #@5b
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5e
    move-result-object v2

    #@5f
    invoke-interface {v8, v2, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    #@62
    move-object v5, v8

    #@63
    .line 161
    goto :goto_56

    #@64
    .line 139
    :pswitch_data_64
    .packed-switch 0x1
        :pswitch_3b
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 10
    .parameter "objUri"
    .parameter "objContentValues"
    .parameter "strWhere"
    .parameter "astrWhereArgs"

    #@0
    .prologue
    .line 199
    iget-object v3, p0, Lcom/lge/ims/provider/xdm/XDMProvider;->objXDMDatabaseHelper:Lcom/lge/ims/provider/xdm/XDMProvider$XDMDatabaseHelper;

    #@2
    invoke-virtual {v3}, Lcom/lge/ims/provider/xdm/XDMProvider$XDMDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@5
    move-result-object v2

    #@6
    .line 200
    .local v2, objSQLiteDatabase:Landroid/database/sqlite/SQLiteDatabase;
    const/4 v1, -0x1

    #@7
    .line 203
    .local v1, nCount:I
    :try_start_7
    sget-object v3, Lcom/lge/ims/provider/xdm/XDMProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    #@9
    invoke-virtual {v3, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@c
    move-result v3

    #@d
    packed-switch v3, :pswitch_data_2e

    #@10
    .line 211
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@12
    const-string v4, "Uri is invaild"

    #@14
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v3
    :try_end_18
    .catchall {:try_start_7 .. :try_end_18} :catchall_2c
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_18} :catch_18

    #@18
    .line 214
    :catch_18
    move-exception v0

    #@19
    .line 215
    .local v0, e:Landroid/database/sqlite/SQLiteException;
    :try_start_19
    invoke-static {v0}, Landroid/database/sqlite/SqliteWrapper;->isLowMemory(Landroid/database/sqlite/SQLiteException;)Z

    #@1c
    move-result v3

    #@1d
    if-eqz v3, :cond_24

    #@1f
    .line 216
    const-string v3, "[ERROR]XDMProvider::update - intenal memory storage is Full"

    #@21
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_24
    .catchall {:try_start_19 .. :try_end_24} :catchall_2c

    #@24
    .line 221
    .end local v0           #e:Landroid/database/sqlite/SQLiteException;
    :cond_24
    :goto_24
    return v1

    #@25
    .line 206
    :pswitch_25
    :try_start_25
    const-string v3, "ims_xdm"

    #@27
    invoke-virtual {v2, v3, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2a
    .catchall {:try_start_25 .. :try_end_2a} :catchall_2c
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_25 .. :try_end_2a} :catch_18

    #@2a
    move-result v1

    #@2b
    .line 208
    goto :goto_24

    #@2c
    .line 218
    :catchall_2c
    move-exception v3

    #@2d
    throw v3

    #@2e
    .line 203
    :pswitch_data_2e
    .packed-switch 0x1
        :pswitch_25
    .end packed-switch
.end method
