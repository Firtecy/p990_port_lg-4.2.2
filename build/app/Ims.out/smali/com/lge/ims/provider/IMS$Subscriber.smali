.class public final Lcom/lge/ims/provider/IMS$Subscriber;
.super Ljava/lang/Object;
.source "IMS.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/provider/IMS;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Subscriber"
.end annotation


# static fields
.field public static final AC:Ljava/lang/String; = "admin_ac"

.field public static final AUTH_ALGORITHM:Ljava/lang/String; = "subscriber_0_auth_algorithm"

.field public static final AUTH_PASSWORD:Ljava/lang/String; = "subscriber_0_auth_password"

.field public static final AUTH_REALM:Ljava/lang/String; = "subscriber_0_auth_realm"

.field public static final AUTH_USERNAME:Ljava/lang/String; = "subscriber_0_auth_username"

.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final DEBUG:Ljava/lang/String; = "admin_debug"

.field public static final DM:Ljava/lang/String; = "admin_dm"

.field public static final HOME_DOMAIN_NAME:Ljava/lang/String; = "subscriber_0_home_domain_name"

.field public static final IMPI:Ljava/lang/String; = "subscriber_0_impi"

.field public static final IMPU_0:Ljava/lang/String; = "subscriber_0_impu_0"

.field public static final IMPU_1:Ljava/lang/String; = "subscriber_0_impu_1"

.field public static final IMPU_2:Ljava/lang/String; = "subscriber_0_impu_2"

.field public static final IMS:Ljava/lang/String; = "admin_ims"

.field public static final ISIM:Ljava/lang/String; = "admin_isim"

.field public static final PCSCF:Ljava/lang/String; = "admin_pcscf"

.field public static final PCSCF_ADDRESS_0:Ljava/lang/String; = "server_pcscf_0_address"

.field public static final PCSCF_ADDRESS_1:Ljava/lang/String; = "server_pcscf_1_address"

.field public static final PCSCF_ADDRESS_2:Ljava/lang/String; = "server_pcscf_2_address"

.field public static final PCSCF_PORT_0:Ljava/lang/String; = "server_pcscf_0_port"

.field public static final PCSCF_PORT_1:Ljava/lang/String; = "server_pcscf_1_port"

.field public static final PCSCF_PORT_2:Ljava/lang/String; = "server_pcscf_2_port"

.field public static final PHONE_CONTEXT:Ljava/lang/String; = "subscriber_0_phone_context"

.field public static final SCSCF_ADDRESS:Ljava/lang/String; = "subscriber_0_server_scscf"

.field public static final SERVICES:Ljava/lang/String; = "admin_services"

.field public static final TABLE_NAME:Ljava/lang/String; = "lgims_subscriber"

.field public static final TESTMODE:Ljava/lang/String; = "admin_testmode"

.field public static final USIM:Ljava/lang/String; = "admin_usim"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 30
    const-string v0, "content://com.lge.ims.provider.lgims/lgims_subscriber"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/lge/ims/provider/IMS$Subscriber;->CONTENT_URI:Landroid/net/Uri;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 24
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
