.class public final Lcom/lge/ims/provider/IMS$Session;
.super Ljava/lang/Object;
.source "IMS.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/provider/IMS;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Session"
.end annotation


# static fields
.field public static final CONF_FACTORY_URI:Ljava/lang/String; = "session_conf_factory_uri"

.field public static final MAX_COUNT:Ljava/lang/String; = "session_max_count"

.field public static final RPR_SUPPORTED:Ljava/lang/String; = "session_rpr_supported"

.field public static final TV_MO_1XX_WAIT:Ljava/lang/String; = "session_tv_mo_1xx_wait"

.field public static final TV_MO_NO_ANSWER:Ljava/lang/String; = "session_tv_mo_no_answer"

.field public static final TV_MT_ALERTING:Ljava/lang/String; = "session_tv_mt_alerting"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 163
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
