.class public final Lcom/lge/ims/provider/ip/IPProviderMetaData$IPProvisioning;
.super Ljava/lang/Object;
.source "IPProviderMetaData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/provider/ip/IPProviderMetaData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "IPProvisioning"
.end annotation


# static fields
.field public static final CONTENT_CAPABILITY_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/com.lge.ims.provider.ip.ip_provisioning"

.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final INDEX_KEY_EXCESS_MAX_USER:I = 0x2

.field public static final INDEX_KEY_LATEST_POLLING:I = 0x0

.field public static final INDEX_KEY_LATEST_RLSSUB_POLLING:I = 0x1

.field public static final KEY_EXCESS_MAX_USER:Ljava/lang/String; = "excess_max_user"

.field public static final KEY_LATEST_POLLING:Ljava/lang/String; = "latest_polling"

.field public static final KEY_LATEST_RLSSUB_POLLING:Ljava/lang/String; = "latest_rlssub_polling"

.field public static final TABLE_NAME:Ljava/lang/String; = "ip_provisioning"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 184
    const-string v0, "content://com.lge.ims.provider.ip/ip_provisioning"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/lge/ims/provider/ip/IPProviderMetaData$IPProvisioning;->CONTENT_URI:Landroid/net/Uri;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 180
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
