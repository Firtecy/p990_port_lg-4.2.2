.class public Lcom/lge/ims/provider/cd/ConfigCD;
.super Ljava/lang/Object;
.source "ConfigCD.java"

# interfaces
.implements Lcom/lge/ims/provider/IConfiguration;


# static fields
.field private static final APP:[[Ljava/lang/String;

.field private static final TABLES:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    #@0
    .prologue
    const/4 v6, 0x3

    #@1
    const/4 v5, 0x2

    #@2
    const/4 v4, 0x1

    #@3
    const/4 v3, 0x0

    #@4
    .line 20
    new-array v0, v4, [Ljava/lang/String;

    #@6
    const-string v1, "lgims_com_kt_app_rcse_cd"

    #@8
    aput-object v1, v0, v3

    #@a
    sput-object v0, Lcom/lge/ims/provider/cd/ConfigCD;->TABLES:[Ljava/lang/String;

    #@c
    .line 28
    new-array v0, v6, [[Ljava/lang/String;

    #@e
    new-array v1, v6, [Ljava/lang/String;

    #@10
    const-string v2, "INTEGER"

    #@12
    aput-object v2, v1, v3

    #@14
    const-string v2, "id"

    #@16
    aput-object v2, v1, v4

    #@18
    const-string v2, "1"

    #@1a
    aput-object v2, v1, v5

    #@1c
    aput-object v1, v0, v3

    #@1e
    new-array v1, v6, [Ljava/lang/String;

    #@20
    const-string v2, "INTEGER"

    #@22
    aput-object v2, v1, v3

    #@24
    const-string v2, "property"

    #@26
    aput-object v2, v1, v4

    #@28
    const-string v2, "0"

    #@2a
    aput-object v2, v1, v5

    #@2c
    aput-object v1, v0, v4

    #@2e
    new-array v1, v6, [Ljava/lang/String;

    #@30
    const-string v2, "TEXT"

    #@32
    aput-object v2, v1, v3

    #@34
    const-string v2, "conf"

    #@36
    aput-object v2, v1, v4

    #@38
    const-string v2, "[Uniqueness]\nStream=0\nFramed=0\nBasic=0\nEvent=0\nCoreService=2\nQos=0\nReg=2\nWrite=1\nRead=1\nCap=0\nMprof=0\nConnection=0\nIMService=0\nPresenceService=0\nXDMService=0\n\n[IMSRegistry]\nIMSRegistry=lgims.com.kt.app.rcse.cd\n\n[CoreService_0]\nservice_id=lgims.com.kt.service.rcse.cd\niari=\nicsi=\nfeature_tag=\n\n[CoreService_1]\nservice_id=lgims.com.kt.service.rcse.cd.wifi\niari=\nicsi=\nfeature_tag=\n\n[Reg_0]\nservice_id=lgims.com.kt.service.rcse.cd\nheader_count=1\nheader_0=Supported: path\n\n[Reg_1]\nservice_id=lgims.com.kt.service.rcse.cd.wifi\nheader_count=1\nheader_0=Supported: path\n\n[Write]\nheader_names=Accept Accept-Encoding Accept-Language\n\n[Read]\nheader_names=Accept Accept-Encoding Accept-Language Contact Retry-After\n\n"

    #@3a
    aput-object v2, v1, v5

    #@3c
    aput-object v1, v0, v5

    #@3e
    sput-object v0, Lcom/lge/ims/provider/cd/ConfigCD;->APP:[[Ljava/lang/String;

    #@40
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 18
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public getTableContent(Ljava/lang/String;)[[Ljava/lang/String;
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 112
    const-string v0, "lgims_com_kt_app_rcse_cd"

    #@2
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_b

    #@8
    .line 113
    sget-object v0, Lcom/lge/ims/provider/cd/ConfigCD;->APP:[[Ljava/lang/String;

    #@a
    .line 117
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    check-cast v0, [[Ljava/lang/String;

    #@e
    goto :goto_a
.end method

.method public getTables()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 104
    sget-object v0, Lcom/lge/ims/provider/cd/ConfigCD;->TABLES:[Ljava/lang/String;

    #@2
    return-object v0
.end method
