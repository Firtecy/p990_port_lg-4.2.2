.class public Lcom/lge/ims/SysMonitor;
.super Lcom/android/internal/telephony/ISysMonitor$Stub;
.source "SysMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/SysMonitor$1;,
        Lcom/lge/ims/SysMonitor$SysMonitorHandler;,
        Lcom/lge/ims/SysMonitor$WiFiReceiver;,
        Lcom/lge/ims/SysMonitor$SysMonitorReceiver;
    }
.end annotation


# static fields
.field private static final ACTION_DATA_DISABLE:Ljava/lang/String; = "android.intent.action.DATA_DISABLE"

.field private static final ACTION_DATA_IP_CHANGED:Ljava/lang/String; = "lge.intent.action.DATA_IP_CHANGED"

.field private static final ACTION_LOG_SERVICE:Ljava/lang/String; = "com.lge.ims.action.LOG_SERVICE"

.field private static final ACTION_MEM_DEBUG:Ljava/lang/String; = "com.lge.ims.action.MEM_DEBUG"

.field private static final EVENT_AC_CONFIGURATION_UPDATE_COMPLETED:I = 0x835

.field private static final EVENT_AIRPLANE_MODE_CHANGED:I = 0x454

.field private static final EVENT_AOS_START:I = 0x65

.field private static final EVENT_BATTERY_LOW:I = 0x3ea

.field private static final EVENT_BATTERY_OKAY:I = 0x3eb

.field private static final EVENT_CALL_STATE_CHANGED:I = 0x44d

.field private static final EVENT_DATA_SERVICE_STATE_CHANGED:I = 0x44f

.field private static final EVENT_DATA_STATE_CHANGED:I = 0x4b1

.field private static final EVENT_IMS_PHONE_RESTARTED:I = 0x3e9

.field private static final EVENT_RAT_CHANGED:I = 0x450

.field private static final EVENT_ROAMING_STATE_CHANGED:I = 0x451

.field private static final EVENT_SIM_STATE_CHANGED:I = 0x455

.field private static final EVENT_VOLTE_SETTING_CHANGED:I = 0x515

.field private static final MEM_DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "SysMonitor"

.field private static mContext:Landroid/content/Context;


# instance fields
.field private mLowBattery:Z

.field private mSysMonitorHandler:Lcom/lge/ims/SysMonitor$SysMonitorHandler;

.field private mSysMonitorReceiver:Lcom/lge/ims/SysMonitor$SysMonitorReceiver;

.field private mWiFiReceiver:Lcom/lge/ims/SysMonitor$WiFiReceiver;

.field private mWiFiServiceEnabled:Z

.field public mWifiDetailedStatus:I

.field public mWifiState:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 66
    const/4 v0, 0x0

    #@1
    sput-object v0, Lcom/lge/ims/SysMonitor;->mContext:Landroid/content/Context;

    #@3
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 11
    .parameter "context"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v8, 0x0

    #@2
    .line 78
    invoke-direct {p0}, Lcom/android/internal/telephony/ISysMonitor$Stub;-><init>()V

    #@5
    .line 67
    iput-object v8, p0, Lcom/lge/ims/SysMonitor;->mWiFiReceiver:Lcom/lge/ims/SysMonitor$WiFiReceiver;

    #@7
    .line 68
    iput-object v8, p0, Lcom/lge/ims/SysMonitor;->mSysMonitorReceiver:Lcom/lge/ims/SysMonitor$SysMonitorReceiver;

    #@9
    .line 69
    iput-object v8, p0, Lcom/lge/ims/SysMonitor;->mSysMonitorHandler:Lcom/lge/ims/SysMonitor$SysMonitorHandler;

    #@b
    .line 71
    const/4 v6, 0x4

    #@c
    iput v6, p0, Lcom/lge/ims/SysMonitor;->mWifiState:I

    #@e
    .line 72
    const/16 v6, 0x9

    #@10
    iput v6, p0, Lcom/lge/ims/SysMonitor;->mWifiDetailedStatus:I

    #@12
    .line 73
    iput-boolean v7, p0, Lcom/lge/ims/SysMonitor;->mLowBattery:Z

    #@14
    .line 74
    iput-boolean v7, p0, Lcom/lge/ims/SysMonitor;->mWiFiServiceEnabled:Z

    #@16
    .line 79
    sput-object p1, Lcom/lge/ims/SysMonitor;->mContext:Landroid/content/Context;

    #@18
    .line 81
    new-instance v6, Lcom/lge/ims/SysMonitor$SysMonitorReceiver;

    #@1a
    invoke-direct {v6, p0, p1}, Lcom/lge/ims/SysMonitor$SysMonitorReceiver;-><init>(Lcom/lge/ims/SysMonitor;Landroid/content/Context;)V

    #@1d
    iput-object v6, p0, Lcom/lge/ims/SysMonitor;->mSysMonitorReceiver:Lcom/lge/ims/SysMonitor$SysMonitorReceiver;

    #@1f
    .line 82
    new-instance v6, Lcom/lge/ims/SysMonitor$SysMonitorHandler;

    #@21
    invoke-direct {v6, p0, v8}, Lcom/lge/ims/SysMonitor$SysMonitorHandler;-><init>(Lcom/lge/ims/SysMonitor;Lcom/lge/ims/SysMonitor$1;)V

    #@24
    iput-object v6, p0, Lcom/lge/ims/SysMonitor;->mSysMonitorHandler:Lcom/lge/ims/SysMonitor$SysMonitorHandler;

    #@26
    .line 84
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getInstance()Lcom/lge/ims/SystemServiceManager;

    #@29
    move-result-object v5

    #@2a
    .line 86
    .local v5, ssm:Lcom/lge/ims/SystemServiceManager;
    if-eqz v5, :cond_36

    #@2c
    .line 87
    invoke-virtual {v5, p0}, Lcom/lge/ims/SystemServiceManager;->setSysMonitor(Lcom/android/internal/telephony/ISysMonitor;)V

    #@2f
    .line 88
    iget-object v6, p0, Lcom/lge/ims/SysMonitor;->mSysMonitorHandler:Lcom/lge/ims/SysMonitor$SysMonitorHandler;

    #@31
    const/16 v7, 0x3e9

    #@33
    invoke-virtual {v5, v6, v7, v8}, Lcom/lge/ims/SystemServiceManager;->registerForSystemServiceRestarted(Landroid/os/Handler;ILjava/lang/Object;)V

    #@36
    .line 91
    :cond_36
    invoke-static {}, Lcom/lge/ims/BatteryStateTracker;->getInstance()Lcom/lge/ims/BatteryStateTracker;

    #@39
    move-result-object v1

    #@3a
    .line 93
    .local v1, bsTracker:Lcom/lge/ims/BatteryStateTracker;
    if-eqz v1, :cond_4a

    #@3c
    .line 94
    iget-object v6, p0, Lcom/lge/ims/SysMonitor;->mSysMonitorHandler:Lcom/lge/ims/SysMonitor$SysMonitorHandler;

    #@3e
    const/16 v7, 0x3ea

    #@40
    invoke-virtual {v1, v6, v7, v8}, Lcom/lge/ims/BatteryStateTracker;->registerForBatteryLow(Landroid/os/Handler;ILjava/lang/Object;)V

    #@43
    .line 95
    iget-object v6, p0, Lcom/lge/ims/SysMonitor;->mSysMonitorHandler:Lcom/lge/ims/SysMonitor$SysMonitorHandler;

    #@45
    const/16 v7, 0x3eb

    #@47
    invoke-virtual {v1, v6, v7, v8}, Lcom/lge/ims/BatteryStateTracker;->registerForBatteryOkay(Landroid/os/Handler;ILjava/lang/Object;)V

    #@4a
    .line 98
    :cond_4a
    invoke-static {}, Lcom/lge/ims/PhoneStateTracker;->getInstance()Lcom/lge/ims/PhoneStateTracker;

    #@4d
    move-result-object v4

    #@4e
    .line 100
    .local v4, pst:Lcom/lge/ims/PhoneStateTracker;
    if-eqz v4, :cond_7a

    #@50
    .line 101
    iget-object v6, p0, Lcom/lge/ims/SysMonitor;->mSysMonitorHandler:Lcom/lge/ims/SysMonitor$SysMonitorHandler;

    #@52
    const/16 v7, 0x44d

    #@54
    invoke-virtual {v4, v6, v7, v8}, Lcom/lge/ims/PhoneStateTracker;->registerForCallStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@57
    .line 103
    iget-object v6, p0, Lcom/lge/ims/SysMonitor;->mSysMonitorHandler:Lcom/lge/ims/SysMonitor$SysMonitorHandler;

    #@59
    const/16 v7, 0x44f

    #@5b
    invoke-virtual {v4, v6, v7, v8}, Lcom/lge/ims/PhoneStateTracker;->registerForDataServiceStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5e
    .line 104
    iget-object v6, p0, Lcom/lge/ims/SysMonitor;->mSysMonitorHandler:Lcom/lge/ims/SysMonitor$SysMonitorHandler;

    #@60
    const/16 v7, 0x450

    #@62
    invoke-virtual {v4, v6, v7, v8}, Lcom/lge/ims/PhoneStateTracker;->registerForRATChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@65
    .line 105
    iget-object v6, p0, Lcom/lge/ims/SysMonitor;->mSysMonitorHandler:Lcom/lge/ims/SysMonitor$SysMonitorHandler;

    #@67
    const/16 v7, 0x451

    #@69
    invoke-virtual {v4, v6, v7, v8}, Lcom/lge/ims/PhoneStateTracker;->registerForRoamingStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@6c
    .line 108
    iget-object v6, p0, Lcom/lge/ims/SysMonitor;->mSysMonitorHandler:Lcom/lge/ims/SysMonitor$SysMonitorHandler;

    #@6e
    const/16 v7, 0x454

    #@70
    invoke-virtual {v4, v6, v7, v8}, Lcom/lge/ims/PhoneStateTracker;->registerForAirplaneModeChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@73
    .line 109
    iget-object v6, p0, Lcom/lge/ims/SysMonitor;->mSysMonitorHandler:Lcom/lge/ims/SysMonitor$SysMonitorHandler;

    #@75
    const/16 v7, 0x455

    #@77
    invoke-virtual {v4, v6, v7, v8}, Lcom/lge/ims/PhoneStateTracker;->registerForSimStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@7a
    .line 112
    :cond_7a
    invoke-static {}, Lcom/lge/ims/DataConnectionManager;->getInstance()Lcom/lge/ims/DataConnectionManager;

    #@7d
    move-result-object v2

    #@7e
    .line 114
    .local v2, dcm:Lcom/lge/ims/DataConnectionManager;
    if-eqz v2, :cond_87

    #@80
    .line 115
    iget-object v6, p0, Lcom/lge/ims/SysMonitor;->mSysMonitorHandler:Lcom/lge/ims/SysMonitor$SysMonitorHandler;

    #@82
    const/16 v7, 0x4b1

    #@84
    invoke-virtual {v2, v6, v7, v8}, Lcom/lge/ims/DataConnectionManager;->registerForDataStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@87
    .line 118
    :cond_87
    invoke-static {}, Lcom/lge/ims/PhoneSettingsTracker;->getInstance()Lcom/lge/ims/PhoneSettingsTracker;

    #@8a
    move-result-object v3

    #@8b
    .line 120
    .local v3, psTracker:Lcom/lge/ims/PhoneSettingsTracker;
    if-eqz v3, :cond_94

    #@8d
    .line 121
    iget-object v6, p0, Lcom/lge/ims/SysMonitor;->mSysMonitorHandler:Lcom/lge/ims/SysMonitor$SysMonitorHandler;

    #@8f
    const/16 v7, 0x515

    #@91
    invoke-virtual {v3, v6, v7, v8}, Lcom/lge/ims/PhoneSettingsTracker;->registerForVoLTESettingsChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@94
    .line 124
    :cond_94
    invoke-static {}, Lcom/lge/ims/Configuration;->isACOn()Z

    #@97
    move-result v6

    #@98
    if-eqz v6, :cond_a7

    #@9a
    .line 125
    invoke-static {}, Lcom/lge/ims/service/ac/ACService;->getInstance()Lcom/lge/ims/service/ac/ACService;

    #@9d
    move-result-object v0

    #@9e
    .line 127
    .local v0, acs:Lcom/lge/ims/service/ac/ACService;
    if-eqz v0, :cond_a7

    #@a0
    .line 128
    iget-object v6, p0, Lcom/lge/ims/SysMonitor;->mSysMonitorHandler:Lcom/lge/ims/SysMonitor$SysMonitorHandler;

    #@a2
    const/16 v7, 0x835

    #@a4
    invoke-virtual {v0, v6, v7, v8}, Lcom/lge/ims/service/ac/ACService;->registerForConfigurationUpdateCompleted(Landroid/os/Handler;ILjava/lang/Object;)V

    #@a7
    .line 135
    .end local v0           #acs:Lcom/lge/ims/service/ac/ACService;
    :cond_a7
    const-string v6, "SysMonitor"

    #@a9
    const-string v7, "SysMonitor created"

    #@ab
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@ae
    .line 136
    return-void
.end method

.method static synthetic access$100(Lcom/lge/ims/SysMonitor;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/lge/ims/SysMonitor;->mLowBattery:Z

    #@2
    return v0
.end method

.method static synthetic access$102(Lcom/lge/ims/SysMonitor;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 31
    iput-boolean p1, p0, Lcom/lge/ims/SysMonitor;->mLowBattery:Z

    #@2
    return p1
.end method

.method static synthetic access$200()Landroid/content/Context;
    .registers 1

    #@0
    .prologue
    .line 31
    sget-object v0, Lcom/lge/ims/SysMonitor;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/lge/ims/SysMonitor;)Lcom/lge/ims/SysMonitor$SysMonitorHandler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 31
    iget-object v0, p0, Lcom/lge/ims/SysMonitor;->mSysMonitorHandler:Lcom/lge/ims/SysMonitor$SysMonitorHandler;

    #@2
    return-object v0
.end method

.method public static getNetworkInfoDetailedState(Landroid/net/NetworkInfo$DetailedState;)I
    .registers 2
    .parameter "state"

    #@0
    .prologue
    .line 442
    sget-object v0, Landroid/net/NetworkInfo$DetailedState;->IDLE:Landroid/net/NetworkInfo$DetailedState;

    #@2
    if-ne p0, v0, :cond_6

    #@4
    .line 443
    const/4 v0, 0x0

    #@5
    .line 464
    :goto_5
    return v0

    #@6
    .line 444
    :cond_6
    sget-object v0, Landroid/net/NetworkInfo$DetailedState;->SCANNING:Landroid/net/NetworkInfo$DetailedState;

    #@8
    if-ne p0, v0, :cond_c

    #@a
    .line 445
    const/4 v0, 0x1

    #@b
    goto :goto_5

    #@c
    .line 446
    :cond_c
    sget-object v0, Landroid/net/NetworkInfo$DetailedState;->CONNECTING:Landroid/net/NetworkInfo$DetailedState;

    #@e
    if-ne p0, v0, :cond_12

    #@10
    .line 447
    const/4 v0, 0x2

    #@11
    goto :goto_5

    #@12
    .line 448
    :cond_12
    sget-object v0, Landroid/net/NetworkInfo$DetailedState;->AUTHENTICATING:Landroid/net/NetworkInfo$DetailedState;

    #@14
    if-ne p0, v0, :cond_18

    #@16
    .line 449
    const/4 v0, 0x3

    #@17
    goto :goto_5

    #@18
    .line 450
    :cond_18
    sget-object v0, Landroid/net/NetworkInfo$DetailedState;->OBTAINING_IPADDR:Landroid/net/NetworkInfo$DetailedState;

    #@1a
    if-ne p0, v0, :cond_1e

    #@1c
    .line 451
    const/4 v0, 0x4

    #@1d
    goto :goto_5

    #@1e
    .line 452
    :cond_1e
    sget-object v0, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@20
    if-ne p0, v0, :cond_24

    #@22
    .line 453
    const/4 v0, 0x5

    #@23
    goto :goto_5

    #@24
    .line 454
    :cond_24
    sget-object v0, Landroid/net/NetworkInfo$DetailedState;->SUSPENDED:Landroid/net/NetworkInfo$DetailedState;

    #@26
    if-ne p0, v0, :cond_2a

    #@28
    .line 455
    const/4 v0, 0x6

    #@29
    goto :goto_5

    #@2a
    .line 456
    :cond_2a
    sget-object v0, Landroid/net/NetworkInfo$DetailedState;->DISCONNECTING:Landroid/net/NetworkInfo$DetailedState;

    #@2c
    if-ne p0, v0, :cond_30

    #@2e
    .line 457
    const/4 v0, 0x7

    #@2f
    goto :goto_5

    #@30
    .line 458
    :cond_30
    sget-object v0, Landroid/net/NetworkInfo$DetailedState;->DISCONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@32
    if-ne p0, v0, :cond_37

    #@34
    .line 459
    const/16 v0, 0x8

    #@36
    goto :goto_5

    #@37
    .line 460
    :cond_37
    sget-object v0, Landroid/net/NetworkInfo$DetailedState;->FAILED:Landroid/net/NetworkInfo$DetailedState;

    #@39
    if-ne p0, v0, :cond_3e

    #@3b
    .line 461
    const/16 v0, 0x9

    #@3d
    goto :goto_5

    #@3e
    .line 464
    :cond_3e
    const/4 v0, -0x1

    #@3f
    goto :goto_5
.end method

.method public static getNetworkInfoState(Landroid/net/NetworkInfo$State;)I
    .registers 2
    .parameter "state"

    #@0
    .prologue
    .line 468
    sget-object v0, Landroid/net/NetworkInfo$State;->CONNECTING:Landroid/net/NetworkInfo$State;

    #@2
    if-ne p0, v0, :cond_6

    #@4
    .line 469
    const/4 v0, 0x0

    #@5
    .line 482
    :goto_5
    return v0

    #@6
    .line 470
    :cond_6
    sget-object v0, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    #@8
    if-ne p0, v0, :cond_c

    #@a
    .line 471
    const/4 v0, 0x1

    #@b
    goto :goto_5

    #@c
    .line 472
    :cond_c
    sget-object v0, Landroid/net/NetworkInfo$State;->SUSPENDED:Landroid/net/NetworkInfo$State;

    #@e
    if-ne p0, v0, :cond_12

    #@10
    .line 473
    const/4 v0, 0x2

    #@11
    goto :goto_5

    #@12
    .line 474
    :cond_12
    sget-object v0, Landroid/net/NetworkInfo$State;->DISCONNECTING:Landroid/net/NetworkInfo$State;

    #@14
    if-ne p0, v0, :cond_18

    #@16
    .line 475
    const/4 v0, 0x3

    #@17
    goto :goto_5

    #@18
    .line 476
    :cond_18
    sget-object v0, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    #@1a
    if-ne p0, v0, :cond_1e

    #@1c
    .line 477
    const/4 v0, 0x4

    #@1d
    goto :goto_5

    #@1e
    .line 478
    :cond_1e
    sget-object v0, Landroid/net/NetworkInfo$State;->UNKNOWN:Landroid/net/NetworkInfo$State;

    #@20
    if-ne p0, v0, :cond_24

    #@22
    .line 479
    const/4 v0, 0x5

    #@23
    goto :goto_5

    #@24
    .line 482
    :cond_24
    const/4 v0, -0x1

    #@25
    goto :goto_5
.end method

.method public static native nativeAirplaneModeChanged(Z)V
.end method

.method public static native nativeBatteryChanged(I)V
.end method

.method public static native nativeBatteryLevelLow()V
.end method

.method public static native nativeBootCompleted()V
.end method

.method public static native nativeDataConnectionFailed(I)V
.end method

.method public static native nativeOnCallStateChanged(ILjava/lang/String;)V
.end method

.method public static native nativeOnDataActivity(I)V
.end method

.method public static native nativeOnDataConnectionStateChanged(II)V
.end method

.method public static native nativeOnEventReceived(III)I
.end method

.method public static native nativeOnPhoneStateChanged(I)V
.end method

.method public static native nativeOnRadioTechStateChanged(I)V
.end method

.method public static native nativeOnServiceStateChanged(IIZ)V
.end method

.method public static native nativePowerOff()V
.end method

.method public static native nativeProcessAlarmTimer(I)V
.end method

.method public static native nativeWifiNetworkStateChanged(II)V
.end method

.method public static native nativeWifiStateChanged(I)V
.end method

.method public static sendEventToNative(III)I
    .registers 4
    .parameter "nEvent"
    .parameter "nWParam"
    .parameter "nLParam"

    #@0
    .prologue
    .line 486
    invoke-static {p0, p1, p2}, Lcom/lge/ims/SysMonitor;->nativeOnEventReceived(III)I

    #@3
    move-result v0

    #@4
    return v0
.end method


# virtual methods
.method public destroy()V
    .registers 9

    #@0
    .prologue
    .line 139
    sget-object v6, Lcom/lge/ims/SysMonitor;->mContext:Landroid/content/Context;

    #@2
    if-eqz v6, :cond_19

    #@4
    .line 140
    const-string v6, "SysMonitor"

    #@6
    const-string v7, "unregister receiver"

    #@8
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@b
    .line 141
    sget-object v6, Lcom/lge/ims/SysMonitor;->mContext:Landroid/content/Context;

    #@d
    iget-object v7, p0, Lcom/lge/ims/SysMonitor;->mSysMonitorReceiver:Lcom/lge/ims/SysMonitor$SysMonitorReceiver;

    #@f
    invoke-virtual {v6, v7}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@12
    .line 142
    sget-object v6, Lcom/lge/ims/SysMonitor;->mContext:Landroid/content/Context;

    #@14
    iget-object v7, p0, Lcom/lge/ims/SysMonitor;->mWiFiReceiver:Lcom/lge/ims/SysMonitor$WiFiReceiver;

    #@16
    invoke-virtual {v6, v7}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@19
    .line 145
    :cond_19
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getInstance()Lcom/lge/ims/SystemServiceManager;

    #@1c
    move-result-object v5

    #@1d
    .line 147
    .local v5, ssm:Lcom/lge/ims/SystemServiceManager;
    if-eqz v5, :cond_24

    #@1f
    .line 148
    iget-object v6, p0, Lcom/lge/ims/SysMonitor;->mSysMonitorHandler:Lcom/lge/ims/SysMonitor$SysMonitorHandler;

    #@21
    invoke-virtual {v5, v6}, Lcom/lge/ims/SystemServiceManager;->unregisterForSystemServiceRestarted(Landroid/os/Handler;)V

    #@24
    .line 151
    :cond_24
    invoke-static {}, Lcom/lge/ims/BatteryStateTracker;->getInstance()Lcom/lge/ims/BatteryStateTracker;

    #@27
    move-result-object v1

    #@28
    .line 153
    .local v1, bsTracker:Lcom/lge/ims/BatteryStateTracker;
    if-eqz v1, :cond_34

    #@2a
    .line 154
    iget-object v6, p0, Lcom/lge/ims/SysMonitor;->mSysMonitorHandler:Lcom/lge/ims/SysMonitor$SysMonitorHandler;

    #@2c
    invoke-virtual {v1, v6}, Lcom/lge/ims/BatteryStateTracker;->unregisterForBatteryLow(Landroid/os/Handler;)V

    #@2f
    .line 155
    iget-object v6, p0, Lcom/lge/ims/SysMonitor;->mSysMonitorHandler:Lcom/lge/ims/SysMonitor$SysMonitorHandler;

    #@31
    invoke-virtual {v1, v6}, Lcom/lge/ims/BatteryStateTracker;->unregisterForBatteryOkay(Landroid/os/Handler;)V

    #@34
    .line 158
    :cond_34
    invoke-static {}, Lcom/lge/ims/PhoneStateTracker;->getInstance()Lcom/lge/ims/PhoneStateTracker;

    #@37
    move-result-object v4

    #@38
    .line 160
    .local v4, pst:Lcom/lge/ims/PhoneStateTracker;
    if-eqz v4, :cond_58

    #@3a
    .line 161
    iget-object v6, p0, Lcom/lge/ims/SysMonitor;->mSysMonitorHandler:Lcom/lge/ims/SysMonitor$SysMonitorHandler;

    #@3c
    invoke-virtual {v4, v6}, Lcom/lge/ims/PhoneStateTracker;->unregisterForCallStateChanged(Landroid/os/Handler;)V

    #@3f
    .line 163
    iget-object v6, p0, Lcom/lge/ims/SysMonitor;->mSysMonitorHandler:Lcom/lge/ims/SysMonitor$SysMonitorHandler;

    #@41
    invoke-virtual {v4, v6}, Lcom/lge/ims/PhoneStateTracker;->unregisterForDataServiceStateChanged(Landroid/os/Handler;)V

    #@44
    .line 164
    iget-object v6, p0, Lcom/lge/ims/SysMonitor;->mSysMonitorHandler:Lcom/lge/ims/SysMonitor$SysMonitorHandler;

    #@46
    invoke-virtual {v4, v6}, Lcom/lge/ims/PhoneStateTracker;->unregisterForRATChanged(Landroid/os/Handler;)V

    #@49
    .line 165
    iget-object v6, p0, Lcom/lge/ims/SysMonitor;->mSysMonitorHandler:Lcom/lge/ims/SysMonitor$SysMonitorHandler;

    #@4b
    invoke-virtual {v4, v6}, Lcom/lge/ims/PhoneStateTracker;->unregisterForRoamingStateChanged(Landroid/os/Handler;)V

    #@4e
    .line 168
    iget-object v6, p0, Lcom/lge/ims/SysMonitor;->mSysMonitorHandler:Lcom/lge/ims/SysMonitor$SysMonitorHandler;

    #@50
    invoke-virtual {v4, v6}, Lcom/lge/ims/PhoneStateTracker;->unregisterForAirplaneModeChanged(Landroid/os/Handler;)V

    #@53
    .line 169
    iget-object v6, p0, Lcom/lge/ims/SysMonitor;->mSysMonitorHandler:Lcom/lge/ims/SysMonitor$SysMonitorHandler;

    #@55
    invoke-virtual {v4, v6}, Lcom/lge/ims/PhoneStateTracker;->unregisterForSimStateChanged(Landroid/os/Handler;)V

    #@58
    .line 172
    :cond_58
    invoke-static {}, Lcom/lge/ims/DataConnectionManager;->getInstance()Lcom/lge/ims/DataConnectionManager;

    #@5b
    move-result-object v2

    #@5c
    .line 174
    .local v2, dcm:Lcom/lge/ims/DataConnectionManager;
    if-eqz v2, :cond_63

    #@5e
    .line 175
    iget-object v6, p0, Lcom/lge/ims/SysMonitor;->mSysMonitorHandler:Lcom/lge/ims/SysMonitor$SysMonitorHandler;

    #@60
    invoke-virtual {v2, v6}, Lcom/lge/ims/DataConnectionManager;->unregisterForDataStateChanged(Landroid/os/Handler;)V

    #@63
    .line 178
    :cond_63
    invoke-static {}, Lcom/lge/ims/PhoneSettingsTracker;->getInstance()Lcom/lge/ims/PhoneSettingsTracker;

    #@66
    move-result-object v3

    #@67
    .line 180
    .local v3, psTracker:Lcom/lge/ims/PhoneSettingsTracker;
    if-eqz v3, :cond_6e

    #@69
    .line 181
    iget-object v6, p0, Lcom/lge/ims/SysMonitor;->mSysMonitorHandler:Lcom/lge/ims/SysMonitor$SysMonitorHandler;

    #@6b
    invoke-virtual {v3, v6}, Lcom/lge/ims/PhoneSettingsTracker;->unregisterVoLTESettingsChanged(Landroid/os/Handler;)V

    #@6e
    .line 184
    :cond_6e
    invoke-static {}, Lcom/lge/ims/Configuration;->isACOn()Z

    #@71
    move-result v6

    #@72
    if-eqz v6, :cond_7f

    #@74
    .line 185
    invoke-static {}, Lcom/lge/ims/service/ac/ACService;->getInstance()Lcom/lge/ims/service/ac/ACService;

    #@77
    move-result-object v0

    #@78
    .line 187
    .local v0, acs:Lcom/lge/ims/service/ac/ACService;
    if-eqz v0, :cond_7f

    #@7a
    .line 188
    iget-object v6, p0, Lcom/lge/ims/SysMonitor;->mSysMonitorHandler:Lcom/lge/ims/SysMonitor$SysMonitorHandler;

    #@7c
    invoke-virtual {v0, v6}, Lcom/lge/ims/service/ac/ACService;->unregisterForConfigurationUpdateCompleted(Landroid/os/Handler;)V

    #@7f
    .line 191
    .end local v0           #acs:Lcom/lge/ims/service/ac/ACService;
    :cond_7f
    return-void
.end method

.method public disableWiFiService()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 417
    iget-object v0, p0, Lcom/lge/ims/SysMonitor;->mWiFiReceiver:Lcom/lge/ims/SysMonitor$WiFiReceiver;

    #@3
    if-nez v0, :cond_8

    #@5
    .line 418
    iput-boolean v1, p0, Lcom/lge/ims/SysMonitor;->mWiFiServiceEnabled:Z

    #@7
    .line 425
    :goto_7
    return-void

    #@8
    .line 422
    :cond_8
    iget-object v0, p0, Lcom/lge/ims/SysMonitor;->mWiFiReceiver:Lcom/lge/ims/SysMonitor$WiFiReceiver;

    #@a
    invoke-virtual {v0}, Lcom/lge/ims/SysMonitor$WiFiReceiver;->unregisterFilters()V

    #@d
    .line 423
    const/4 v0, 0x0

    #@e
    iput-object v0, p0, Lcom/lge/ims/SysMonitor;->mWiFiReceiver:Lcom/lge/ims/SysMonitor$WiFiReceiver;

    #@10
    .line 424
    iput-boolean v1, p0, Lcom/lge/ims/SysMonitor;->mWiFiServiceEnabled:Z

    #@12
    goto :goto_7
.end method

.method public enableWiFiService()V
    .registers 3

    #@0
    .prologue
    .line 428
    iget-object v0, p0, Lcom/lge/ims/SysMonitor;->mWiFiReceiver:Lcom/lge/ims/SysMonitor$WiFiReceiver;

    #@2
    if-eqz v0, :cond_5

    #@4
    .line 439
    :goto_4
    return-void

    #@5
    .line 432
    :cond_5
    new-instance v0, Lcom/lge/ims/SysMonitor$WiFiReceiver;

    #@7
    sget-object v1, Lcom/lge/ims/SysMonitor;->mContext:Landroid/content/Context;

    #@9
    invoke-direct {v0, p0, v1}, Lcom/lge/ims/SysMonitor$WiFiReceiver;-><init>(Lcom/lge/ims/SysMonitor;Landroid/content/Context;)V

    #@c
    iput-object v0, p0, Lcom/lge/ims/SysMonitor;->mWiFiReceiver:Lcom/lge/ims/SysMonitor$WiFiReceiver;

    #@e
    .line 434
    iget-object v0, p0, Lcom/lge/ims/SysMonitor;->mWiFiReceiver:Lcom/lge/ims/SysMonitor$WiFiReceiver;

    #@10
    if-eqz v0, :cond_17

    #@12
    .line 435
    iget-object v0, p0, Lcom/lge/ims/SysMonitor;->mWiFiReceiver:Lcom/lge/ims/SysMonitor$WiFiReceiver;

    #@14
    invoke-virtual {v0}, Lcom/lge/ims/SysMonitor$WiFiReceiver;->registerFilters()V

    #@17
    .line 438
    :cond_17
    const/4 v0, 0x1

    #@18
    iput-boolean v0, p0, Lcom/lge/ims/SysMonitor;->mWiFiServiceEnabled:Z

    #@1a
    goto :goto_4
.end method

.method public onPhoneStateChanged(IIILjava/lang/String;)V
    .registers 8
    .parameter "item"
    .parameter "param1"
    .parameter "param2"
    .parameter "paramEx"

    #@0
    .prologue
    .line 413
    const-string v0, "SysMonitor"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "item = "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 414
    return-void
.end method
