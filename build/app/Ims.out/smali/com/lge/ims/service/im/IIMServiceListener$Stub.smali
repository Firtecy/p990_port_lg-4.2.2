.class public abstract Lcom/lge/ims/service/im/IIMServiceListener$Stub;
.super Landroid/os/Binder;
.source "IIMServiceListener.java"

# interfaces
.implements Lcom/lge/ims/service/im/IIMServiceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/im/IIMServiceListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/im/IIMServiceListener$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.lge.ims.service.im.IIMServiceListener"

.field static final TRANSACTION_capabilityNotify:I = 0x5

.field static final TRANSACTION_messageDelivered:I = 0x3

.field static final TRANSACTION_messageDisplayed:I = 0x4

.field static final TRANSACTION_serviceClosed:I = 0x1

.field static final TRANSACTION_serviceStarted:I = 0x2


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 15
    const-string v0, "com.lge.ims.service.im.IIMServiceListener"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/lge/ims/service/im/IIMServiceListener$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/im/IIMServiceListener;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 23
    if-nez p0, :cond_4

    #@2
    .line 24
    const/4 v0, 0x0

    #@3
    .line 30
    :goto_3
    return-object v0

    #@4
    .line 26
    :cond_4
    const-string v1, "com.lge.ims.service.im.IIMServiceListener"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 27
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/lge/ims/service/im/IIMServiceListener;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 28
    check-cast v0, Lcom/lge/ims/service/im/IIMServiceListener;

    #@12
    goto :goto_3

    #@13
    .line 30
    :cond_13
    new-instance v0, Lcom/lge/ims/service/im/IIMServiceListener$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/lge/ims/service/im/IIMServiceListener$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 9
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 38
    sparse-switch p1, :sswitch_data_6c

    #@4
    .line 96
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v2

    #@8
    :goto_8
    return v2

    #@9
    .line 42
    :sswitch_9
    const-string v3, "com.lge.ims.service.im.IIMServiceListener"

    #@b
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 47
    :sswitch_f
    const-string v3, "com.lge.ims.service.im.IIMServiceListener"

    #@11
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 48
    invoke-virtual {p0}, Lcom/lge/ims/service/im/IIMServiceListener$Stub;->serviceClosed()V

    #@17
    .line 49
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1a
    goto :goto_8

    #@1b
    .line 54
    :sswitch_1b
    const-string v3, "com.lge.ims.service.im.IIMServiceListener"

    #@1d
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@20
    .line 55
    invoke-virtual {p0}, Lcom/lge/ims/service/im/IIMServiceListener$Stub;->serviceStarted()V

    #@23
    .line 56
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@26
    goto :goto_8

    #@27
    .line 61
    :sswitch_27
    const-string v3, "com.lge.ims.service.im.IIMServiceListener"

    #@29
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2c
    .line 63
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2f
    move-result-object v0

    #@30
    .line 65
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@33
    move-result-object v1

    #@34
    .line 66
    .local v1, _arg1:Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/service/im/IIMServiceListener$Stub;->messageDelivered(Ljava/lang/String;Ljava/lang/String;)V

    #@37
    .line 67
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3a
    goto :goto_8

    #@3b
    .line 72
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:Ljava/lang/String;
    :sswitch_3b
    const-string v3, "com.lge.ims.service.im.IIMServiceListener"

    #@3d
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@40
    .line 74
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@43
    move-result-object v0

    #@44
    .line 76
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@47
    move-result-object v1

    #@48
    .line 77
    .restart local v1       #_arg1:Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/service/im/IIMServiceListener$Stub;->messageDisplayed(Ljava/lang/String;Ljava/lang/String;)V

    #@4b
    .line 78
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4e
    goto :goto_8

    #@4f
    .line 83
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:Ljava/lang/String;
    :sswitch_4f
    const-string v3, "com.lge.ims.service.im.IIMServiceListener"

    #@51
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@54
    .line 85
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@57
    move-result v3

    #@58
    if-eqz v3, :cond_69

    #@5a
    .line 86
    sget-object v3, Lcom/lge/ims/service/cd/Capabilities;->CREATOR:Landroid/os/Parcelable$Creator;

    #@5c
    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@5f
    move-result-object v0

    #@60
    check-cast v0, Lcom/lge/ims/service/cd/Capabilities;

    #@62
    .line 91
    .local v0, _arg0:Lcom/lge/ims/service/cd/Capabilities;
    :goto_62
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/im/IIMServiceListener$Stub;->capabilityNotify(Lcom/lge/ims/service/cd/Capabilities;)V

    #@65
    .line 92
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@68
    goto :goto_8

    #@69
    .line 89
    .end local v0           #_arg0:Lcom/lge/ims/service/cd/Capabilities;
    :cond_69
    const/4 v0, 0x0

    #@6a
    .restart local v0       #_arg0:Lcom/lge/ims/service/cd/Capabilities;
    goto :goto_62

    #@6b
    .line 38
    nop

    #@6c
    :sswitch_data_6c
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_1b
        0x3 -> :sswitch_27
        0x4 -> :sswitch_3b
        0x5 -> :sswitch_4f
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
