.class Lcom/lge/ims/service/cs/ContentShareServiceImpl;
.super Lcom/lge/ims/service/cs/IContentShare$Stub;
.source "ContentShareService.java"

# interfaces
.implements Lcom/lge/ims/JNICoreListener;
.implements Lcom/lge/ims/service/CallManager$CallInfoListener;


# instance fields
.field private listener:Lcom/lge/ims/service/cs/IContentShareListener;

.field private final mCM:Lcom/lge/ims/service/CallManager;

.field private nativeObj:I

.field private networkType:I


# direct methods
.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 62
    invoke-direct {p0}, Lcom/lge/ims/service/cs/IContentShare$Stub;-><init>()V

    #@4
    .line 57
    iput v1, p0, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->nativeObj:I

    #@6
    .line 58
    iput v1, p0, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->networkType:I

    #@8
    .line 59
    const/4 v1, 0x0

    #@9
    iput-object v1, p0, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->listener:Lcom/lge/ims/service/cs/IContentShareListener;

    #@b
    .line 60
    invoke-static {}, Lcom/lge/ims/service/CallManager;->getInstance()Lcom/lge/ims/service/CallManager;

    #@e
    move-result-object v1

    #@f
    iput-object v1, p0, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->mCM:Lcom/lge/ims/service/CallManager;

    #@11
    .line 63
    const-string v1, ""

    #@13
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@16
    .line 65
    const/16 v1, 0xb

    #@18
    invoke-static {v1}, Lcom/lge/ims/JNICore;->getInterface(I)I

    #@1b
    move-result v1

    #@1c
    iput v1, p0, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->nativeObj:I

    #@1e
    .line 66
    iget v1, p0, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->nativeObj:I

    #@20
    invoke-static {v1, p0}, Lcom/lge/ims/JNICore;->setListener(ILcom/lge/ims/JNICoreListener;)I

    #@23
    .line 69
    :try_start_23
    invoke-virtual {p0}, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->getServiceNetworkType()I

    #@26
    move-result v1

    #@27
    iput v1, p0, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->networkType:I
    :try_end_29
    .catch Landroid/os/RemoteException; {:try_start_23 .. :try_end_29} :catch_2a

    #@29
    .line 73
    :goto_29
    return-void

    #@2a
    .line 70
    :catch_2a
    move-exception v0

    #@2b
    .line 71
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->getStackTrace()[Ljava/lang/StackTraceElement;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@36
    goto :goto_29
.end method


# virtual methods
.method public addCall(Ljava/lang/String;I)V
    .registers 5
    .parameter "number"
    .parameter "callMode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 178
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "addCall = "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@16
    .line 180
    iget-object v0, p0, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->mCM:Lcom/lge/ims/service/CallManager;

    #@18
    if-eqz v0, :cond_20

    #@1a
    .line 181
    iget-object v0, p0, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->mCM:Lcom/lge/ims/service/CallManager;

    #@1c
    const/4 v1, 0x1

    #@1d
    invoke-virtual {v0, p1, v1}, Lcom/lge/ims/service/CallManager;->addCall(Ljava/lang/String;I)V

    #@20
    .line 183
    :cond_20
    return-void
.end method

.method public addCapabilityObserver(Ljava/lang/String;)Z
    .registers 3
    .parameter "peerPhoneNumber"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 143
    const-string v0, "To be deleted"

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->w(Ljava/lang/String;)V

    #@5
    .line 149
    const/4 v0, 0x1

    #@6
    return v0
.end method

.method public closeImageSession(Lcom/lge/ims/service/cs/IImageSession;)V
    .registers 3
    .parameter "iImageSession"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 232
    const-string v0, ""

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 234
    if-eqz p1, :cond_d

    #@7
    .line 235
    check-cast p1, Lcom/lge/ims/service/cs/ImageSessionImpl;

    #@9
    .end local p1
    invoke-virtual {p1}, Lcom/lge/ims/service/cs/ImageSessionImpl;->terminate()V

    #@c
    .line 236
    const/4 p1, 0x0

    #@d
    .line 238
    .restart local p1
    :cond_d
    return-void
.end method

.method public closeVideoSession(Lcom/lge/ims/service/cs/IVideoSession;)V
    .registers 3
    .parameter "iVideoSession"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 217
    const-string v0, ""

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 219
    if-eqz p1, :cond_d

    #@7
    .line 220
    check-cast p1, Lcom/lge/ims/service/cs/VideoSessionImpl;

    #@9
    .end local p1
    invoke-virtual {p1}, Lcom/lge/ims/service/cs/VideoSessionImpl;->terminate()V

    #@c
    .line 221
    const/4 p1, 0x0

    #@d
    .line 223
    .restart local p1
    :cond_d
    return-void
.end method

.method public getServiceNetworkType()I
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v4, 0xc

    #@2
    const/4 v3, 0x1

    #@3
    .line 241
    const-string v2, ""

    #@5
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@8
    .line 244
    invoke-static {}, Lcom/lge/ims/service/ServiceMngr;->getInstance()Lcom/lge/ims/service/ServiceMngr;

    #@b
    move-result-object v1

    #@c
    .line 245
    .local v1, serviceMngr:Lcom/lge/ims/service/ServiceMngr;
    const/4 v0, 0x0

    #@d
    .line 247
    .local v0, result:I
    if-eqz v1, :cond_16

    #@f
    .line 248
    invoke-virtual {v1, v4}, Lcom/lge/ims/service/ServiceMngr;->isWifiConnected(I)Z

    #@12
    move-result v2

    #@13
    if-ne v2, v3, :cond_2d

    #@15
    .line 249
    const/4 v0, 0x2

    #@16
    .line 255
    :cond_16
    :goto_16
    new-instance v2, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v3, "network type = "

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@2c
    .line 257
    return v0

    #@2d
    .line 250
    :cond_2d
    invoke-virtual {v1, v4}, Lcom/lge/ims/service/ServiceMngr;->isLTEConnected(I)Z

    #@30
    move-result v2

    #@31
    if-ne v2, v3, :cond_16

    #@33
    .line 251
    const/4 v0, 0x1

    #@34
    goto :goto_16
.end method

.method public holdCall(Ljava/lang/String;)V
    .registers 4
    .parameter "number"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 194
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "holdCall = "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@16
    .line 196
    iget-object v0, p0, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->mCM:Lcom/lge/ims/service/CallManager;

    #@18
    if-eqz v0, :cond_1f

    #@1a
    .line 197
    iget-object v0, p0, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->mCM:Lcom/lge/ims/service/CallManager;

    #@1c
    invoke-virtual {v0, p1}, Lcom/lge/ims/service/CallManager;->holdCall(Ljava/lang/String;)V

    #@1f
    .line 199
    :cond_1f
    return-void
.end method

.method public notifyCapabilities(Lcom/lge/ims/service/cd/Capabilities;)V
    .registers 4
    .parameter "capabilities"

    #@0
    .prologue
    .line 276
    iget-object v1, p0, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->listener:Lcom/lge/ims/service/cs/IContentShareListener;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 285
    :goto_4
    return-void

    #@5
    .line 281
    :cond_5
    :try_start_5
    iget-object v1, p0, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->listener:Lcom/lge/ims/service/cs/IContentShareListener;

    #@7
    invoke-interface {v1, p1}, Lcom/lge/ims/service/cs/IContentShareListener;->onUpdateCapability(Lcom/lge/ims/service/cd/Capabilities;)V
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_a} :catch_b

    #@a
    goto :goto_4

    #@b
    .line 282
    :catch_b
    move-exception v0

    #@c
    .line 283
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->getStackTrace()[Ljava/lang/StackTraceElement;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@17
    goto :goto_4
.end method

.method public onAvailable(I)V
    .registers 4
    .parameter "networkType"

    #@0
    .prologue
    .line 261
    const-string v1, ""

    #@2
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 263
    iget-object v1, p0, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->listener:Lcom/lge/ims/service/cs/IContentShareListener;

    #@7
    if-eqz v1, :cond_1c

    #@9
    .line 265
    :try_start_9
    iget-object v1, p0, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->listener:Lcom/lge/ims/service/cs/IContentShareListener;

    #@b
    invoke-interface {v1, p1}, Lcom/lge/ims/service/cs/IContentShareListener;->onReconfigured(I)V
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_e} :catch_f

    #@e
    .line 272
    :goto_e
    return-void

    #@f
    .line 266
    :catch_f
    move-exception v0

    #@10
    .line 267
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->getStackTrace()[Ljava/lang/StackTraceElement;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@1b
    goto :goto_e

    #@1c
    .line 270
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_1c
    const-string v1, "content share listener is null"

    #@1e
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@21
    goto :goto_e
.end method

.method public onMessage(Landroid/os/Parcel;)V
    .registers 10
    .parameter "parcel"

    #@0
    .prologue
    .line 92
    const-string v3, ""

    #@2
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 94
    iget-object v3, p0, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->listener:Lcom/lge/ims/service/cs/IContentShareListener;

    #@7
    if-nez v3, :cond_f

    #@9
    .line 95
    const-string v3, "called but listener is null"

    #@b
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@e
    .line 124
    :cond_e
    :goto_e
    return-void

    #@f
    .line 99
    :cond_f
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@12
    move-result v1

    #@13
    .line 102
    .local v1, msg:I
    packed-switch v1, :pswitch_data_74

    #@16
    .line 118
    :pswitch_16
    :try_start_16
    new-instance v3, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v4, "unknown message received from native msg : "

    #@1d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v3

    #@29
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V
    :try_end_2c
    .catch Landroid/os/RemoteException; {:try_start_16 .. :try_end_2c} :catch_2d

    #@2c
    goto :goto_e

    #@2d
    .line 121
    :catch_2d
    move-exception v0

    #@2e
    .line 122
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->getStackTrace()[Ljava/lang/StackTraceElement;

    #@31
    move-result-object v3

    #@32
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@35
    move-result-object v3

    #@36
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@39
    goto :goto_e

    #@3a
    .line 104
    .end local v0           #e:Landroid/os/RemoteException;
    :pswitch_3a
    :try_start_3a
    iget-object v3, p0, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->listener:Lcom/lge/ims/service/cs/IContentShareListener;

    #@3c
    new-instance v4, Lcom/lge/ims/service/cs/VideoSessionImpl;

    #@3e
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@41
    move-result v5

    #@42
    invoke-direct {v4, v5}, Lcom/lge/ims/service/cs/VideoSessionImpl;-><init>(I)V

    #@45
    invoke-interface {v3, v4}, Lcom/lge/ims/service/cs/IContentShareListener;->onReceivedVideoSession(Lcom/lge/ims/service/cs/IVideoSession;)V

    #@48
    goto :goto_e

    #@49
    .line 107
    :pswitch_49
    iget-object v3, p0, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->listener:Lcom/lge/ims/service/cs/IContentShareListener;

    #@4b
    new-instance v4, Lcom/lge/ims/service/cs/ImageSessionImpl;

    #@4d
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@50
    move-result v5

    #@51
    invoke-direct {v4, v5}, Lcom/lge/ims/service/cs/ImageSessionImpl;-><init>(I)V

    #@54
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@57
    move-result-object v5

    #@58
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@5b
    move-result-object v6

    #@5c
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@5f
    move-result v7

    #@60
    invoke-interface {v3, v4, v5, v6, v7}, Lcom/lge/ims/service/cs/IContentShareListener;->onReceivedImageSession(Lcom/lge/ims/service/cs/IImageSession;Ljava/lang/String;Ljava/lang/String;I)V

    #@63
    goto :goto_e

    #@64
    .line 110
    :pswitch_64
    invoke-virtual {p0}, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->getServiceNetworkType()I

    #@67
    move-result v2

    #@68
    .line 112
    .local v2, type:I
    iget v3, p0, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->networkType:I

    #@6a
    if-eq v3, v2, :cond_e

    #@6c
    .line 113
    iput v2, p0, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->networkType:I

    #@6e
    .line 114
    iget v3, p0, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->networkType:I

    #@70
    invoke-virtual {p0, v3}, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->onAvailable(I)V
    :try_end_73
    .catch Landroid/os/RemoteException; {:try_start_3a .. :try_end_73} :catch_2d

    #@73
    goto :goto_e

    #@74
    .line 102
    :pswitch_data_74
    .packed-switch 0x28a3
        :pswitch_3a
        :pswitch_16
        :pswitch_16
        :pswitch_49
        :pswitch_64
    .end packed-switch
.end method

.method public openImageSession()Lcom/lge/ims/service/cs/IImageSession;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 226
    const-string v0, ""

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 228
    new-instance v0, Lcom/lge/ims/service/cs/ImageSessionImpl;

    #@7
    invoke-direct {v0}, Lcom/lge/ims/service/cs/ImageSessionImpl;-><init>()V

    #@a
    return-object v0
.end method

.method public openVideoSession()Lcom/lge/ims/service/cs/IVideoSession;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 211
    const-string v0, ""

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 213
    new-instance v0, Lcom/lge/ims/service/cs/VideoSessionImpl;

    #@7
    invoke-direct {v0}, Lcom/lge/ims/service/cs/VideoSessionImpl;-><init>()V

    #@a
    return-object v0
.end method

.method public queryCapability(Ljava/lang/String;)Z
    .registers 3
    .parameter "peerPhoneNumber"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 168
    const-string v0, "To be deleted"

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->w(Ljava/lang/String;)V

    #@5
    .line 173
    const/4 v0, 0x1

    #@6
    return v0
.end method

.method public removeCall(Ljava/lang/String;)V
    .registers 4
    .parameter "number"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 186
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "removeCall = "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@16
    .line 188
    iget-object v0, p0, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->mCM:Lcom/lge/ims/service/CallManager;

    #@18
    if-eqz v0, :cond_1f

    #@1a
    .line 189
    iget-object v0, p0, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->mCM:Lcom/lge/ims/service/CallManager;

    #@1c
    invoke-virtual {v0, p1}, Lcom/lge/ims/service/CallManager;->removeCall(Ljava/lang/String;)V

    #@1f
    .line 191
    :cond_1f
    return-void
.end method

.method public removeCapabilityObserver(Ljava/lang/String;)V
    .registers 3
    .parameter "peerPhoneNumber"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 153
    const-string v0, "To be deleted"

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->w(Ljava/lang/String;)V

    #@5
    .line 158
    return-void
.end method

.method public setCapability(ZZ)Z
    .registers 4
    .parameter "myVideoCapa"
    .parameter "myImageCapa"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 161
    const-string v0, "To be deleted"

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->w(Ljava/lang/String;)V

    #@5
    .line 163
    const/4 v0, 0x1

    #@6
    return v0
.end method

.method public setListener(Lcom/lge/ims/service/cs/IContentShareListener;)Z
    .registers 4
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 127
    const-string v0, ""

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 129
    iput-object p1, p0, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->listener:Lcom/lge/ims/service/cs/IContentShareListener;

    #@7
    .line 131
    iget-object v0, p0, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->mCM:Lcom/lge/ims/service/CallManager;

    #@9
    if-eqz v0, :cond_14

    #@b
    .line 132
    iget-object v0, p0, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->listener:Lcom/lge/ims/service/cs/IContentShareListener;

    #@d
    if-eqz v0, :cond_16

    #@f
    .line 133
    iget-object v0, p0, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->mCM:Lcom/lge/ims/service/CallManager;

    #@11
    invoke-virtual {v0, p0}, Lcom/lge/ims/service/CallManager;->setListener(Lcom/lge/ims/service/CallManager$CallInfoListener;)V

    #@14
    .line 139
    :cond_14
    :goto_14
    const/4 v0, 0x1

    #@15
    return v0

    #@16
    .line 135
    :cond_16
    iget-object v0, p0, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->mCM:Lcom/lge/ims/service/CallManager;

    #@18
    const/4 v1, 0x0

    #@19
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/CallManager;->setListener(Lcom/lge/ims/service/CallManager$CallInfoListener;)V

    #@1c
    goto :goto_14
.end method

.method public terminate()V
    .registers 2

    #@0
    .prologue
    .line 76
    const-string v0, ""

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 78
    iget v0, p0, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->nativeObj:I

    #@7
    invoke-static {v0, p0}, Lcom/lge/ims/JNICore;->removeListener(ILcom/lge/ims/JNICoreListener;)I

    #@a
    .line 79
    iget v0, p0, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->nativeObj:I

    #@c
    invoke-static {v0}, Lcom/lge/ims/JNICore;->releaseInterface(I)I

    #@f
    .line 81
    iget-object v0, p0, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->mCM:Lcom/lge/ims/service/CallManager;

    #@11
    if-eqz v0, :cond_18

    #@13
    .line 82
    iget-object v0, p0, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->mCM:Lcom/lge/ims/service/CallManager;

    #@15
    invoke-virtual {v0}, Lcom/lge/ims/service/CallManager;->removeAllCalls()V

    #@18
    .line 86
    :cond_18
    const/4 v0, 0x0

    #@19
    iput v0, p0, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->nativeObj:I

    #@1b
    .line 88
    return-void
.end method

.method public unholdCall(Ljava/lang/String;)V
    .registers 4
    .parameter "number"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 202
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "unholdCall = "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@16
    .line 204
    iget-object v0, p0, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->mCM:Lcom/lge/ims/service/CallManager;

    #@18
    if-eqz v0, :cond_1f

    #@1a
    .line 205
    iget-object v0, p0, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->mCM:Lcom/lge/ims/service/CallManager;

    #@1c
    invoke-virtual {v0, p1}, Lcom/lge/ims/service/CallManager;->unholdCall(Ljava/lang/String;)V

    #@1f
    .line 207
    :cond_1f
    return-void
.end method
