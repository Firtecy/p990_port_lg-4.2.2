.class public Lcom/lge/ims/service/ac/ACContentParser;
.super Ljava/lang/Object;
.source "ACContentParser.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ACContentParser"

.field private static mContentParser:Lcom/lge/ims/service/ac/ACContentParser;


# instance fields
.field private mCache:Lcom/lge/ims/service/ac/ACContentCache;

.field private mCharacteristic:Ljava/lang/String;

.field private mValidContent:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 25
    new-instance v0, Lcom/lge/ims/service/ac/ACContentParser;

    #@2
    invoke-direct {v0}, Lcom/lge/ims/service/ac/ACContentParser;-><init>()V

    #@5
    sput-object v0, Lcom/lge/ims/service/ac/ACContentParser;->mContentParser:Lcom/lge/ims/service/ac/ACContentParser;

    #@7
    return-void
.end method

.method private constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 31
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 27
    new-instance v0, Lcom/lge/ims/service/ac/ACContentCache;

    #@5
    invoke-direct {v0}, Lcom/lge/ims/service/ac/ACContentCache;-><init>()V

    #@8
    iput-object v0, p0, Lcom/lge/ims/service/ac/ACContentParser;->mCache:Lcom/lge/ims/service/ac/ACContentCache;

    #@a
    .line 29
    const/4 v0, 0x0

    #@b
    iput-boolean v0, p0, Lcom/lge/ims/service/ac/ACContentParser;->mValidContent:Z

    #@d
    .line 32
    return-void
.end method

.method private getIndexOfAttributeName(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)I
    .registers 7
    .parameter "xpp"
    .parameter "name"

    #@0
    .prologue
    .line 108
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    #@3
    move-result v0

    #@4
    .line 111
    .local v0, count:I
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    if-ge v1, v0, :cond_17

    #@7
    .line 112
    invoke-interface {p1, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    #@a
    move-result-object v2

    #@b
    .line 114
    .local v2, tmp:Ljava/lang/String;
    if-nez v2, :cond_10

    #@d
    .line 111
    :cond_d
    add-int/lit8 v1, v1, 0x1

    #@f
    goto :goto_5

    #@10
    .line 118
    :cond_10
    invoke-virtual {v2, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@13
    move-result v3

    #@14
    if-eqz v3, :cond_d

    #@16
    .line 123
    .end local v1           #i:I
    .end local v2           #tmp:Ljava/lang/String;
    :goto_16
    return v1

    #@17
    .restart local v1       #i:I
    :cond_17
    const/4 v1, -0x1

    #@18
    goto :goto_16
.end method

.method public static getInstance()Lcom/lge/ims/service/ac/ACContentParser;
    .registers 1

    #@0
    .prologue
    .line 35
    sget-object v0, Lcom/lge/ims/service/ac/ACContentParser;->mContentParser:Lcom/lge/ims/service/ac/ACContentParser;

    #@2
    return-object v0
.end method

.method private parseCharacteristicOnStartTag(Lorg/xmlpull/v1/XmlPullParser;)V
    .registers 4
    .parameter "xpp"

    #@0
    .prologue
    .line 127
    if-nez p1, :cond_3

    #@2
    .line 139
    :goto_2
    return-void

    #@3
    .line 131
    :cond_3
    const-string v1, "type"

    #@5
    invoke-direct {p0, p1, v1}, Lcom/lge/ims/service/ac/ACContentParser;->getIndexOfAttributeName(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)I

    #@8
    move-result v0

    #@9
    .line 133
    .local v0, indexOfType:I
    const/4 v1, -0x1

    #@a
    if-ne v0, v1, :cond_10

    #@c
    .line 134
    const/4 v1, 0x0

    #@d
    iput-object v1, p0, Lcom/lge/ims/service/ac/ACContentParser;->mCharacteristic:Ljava/lang/String;

    #@f
    goto :goto_2

    #@10
    .line 138
    :cond_10
    invoke-interface {p1, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    iput-object v1, p0, Lcom/lge/ims/service/ac/ACContentParser;->mCharacteristic:Ljava/lang/String;

    #@16
    goto :goto_2
.end method

.method private parseParmOnStartTag(Lorg/xmlpull/v1/XmlPullParser;)V
    .registers 8
    .parameter "xpp"

    #@0
    .prologue
    const/4 v4, -0x1

    #@1
    .line 142
    if-nez p1, :cond_4

    #@3
    .line 210
    :cond_3
    :goto_3
    return-void

    #@4
    .line 146
    :cond_4
    const-string v3, "name"

    #@6
    invoke-direct {p0, p1, v3}, Lcom/lge/ims/service/ac/ACContentParser;->getIndexOfAttributeName(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)I

    #@9
    move-result v0

    #@a
    .line 147
    .local v0, indexOfName:I
    const-string v3, "value"

    #@c
    invoke-direct {p0, p1, v3}, Lcom/lge/ims/service/ac/ACContentParser;->getIndexOfAttributeName(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)I

    #@f
    move-result v1

    #@10
    .line 149
    .local v1, indexOfValue:I
    if-eq v0, v4, :cond_14

    #@12
    if-ne v1, v4, :cond_37

    #@14
    .line 150
    :cond_14
    const-string v3, "ACContentParser"

    #@16
    new-instance v4, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v5, "indexOfName="

    #@1d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v4

    #@21
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v4

    #@25
    const-string v5, ", indexOfValue="

    #@27
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v4

    #@2b
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v4

    #@2f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v4

    #@33
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@36
    goto :goto_3

    #@37
    .line 154
    :cond_37
    invoke-interface {p1, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    #@3a
    move-result-object v2

    #@3b
    .line 157
    .local v2, value:Ljava/lang/String;
    const-string v3, "version"

    #@3d
    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@40
    move-result v3

    #@41
    if-eqz v3, :cond_4e

    #@43
    .line 158
    iget-object v3, p0, Lcom/lge/ims/service/ac/ACContentParser;->mCache:Lcom/lge/ims/service/ac/ACContentCache;

    #@45
    iget-object v3, v3, Lcom/lge/ims/service/ac/ACContentCache;->mVersion:Lcom/lge/ims/service/ac/content/ACVersion;

    #@47
    invoke-interface {p1, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    #@4a
    move-result-object v4

    #@4b
    iput-object v4, v3, Lcom/lge/ims/service/ac/content/ACVersion;->mVersion:Ljava/lang/String;

    #@4d
    goto :goto_3

    #@4e
    .line 159
    :cond_4e
    const-string v3, "validity"

    #@50
    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@53
    move-result v3

    #@54
    if-eqz v3, :cond_61

    #@56
    .line 160
    iget-object v3, p0, Lcom/lge/ims/service/ac/ACContentParser;->mCache:Lcom/lge/ims/service/ac/ACContentCache;

    #@58
    iget-object v3, v3, Lcom/lge/ims/service/ac/ACContentCache;->mVersion:Lcom/lge/ims/service/ac/content/ACVersion;

    #@5a
    invoke-interface {p1, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    #@5d
    move-result-object v4

    #@5e
    iput-object v4, v3, Lcom/lge/ims/service/ac/content/ACVersion;->mValidity:Ljava/lang/String;

    #@60
    goto :goto_3

    #@61
    .line 163
    :cond_61
    const-string v3, "Home_network_domain_name"

    #@63
    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@66
    move-result v3

    #@67
    if-eqz v3, :cond_74

    #@69
    .line 164
    iget-object v3, p0, Lcom/lge/ims/service/ac/ACContentParser;->mCache:Lcom/lge/ims/service/ac/ACContentCache;

    #@6b
    iget-object v3, v3, Lcom/lge/ims/service/ac/ACContentCache;->mSubscriber:Lcom/lge/ims/service/ac/content/ACSubscriber;

    #@6d
    invoke-interface {p1, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    #@70
    move-result-object v4

    #@71
    iput-object v4, v3, Lcom/lge/ims/service/ac/content/ACSubscriber;->mHomeDomainName:Ljava/lang/String;

    #@73
    goto :goto_3

    #@74
    .line 167
    :cond_74
    const-string v3, "Private_User_Identity"

    #@76
    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@79
    move-result v3

    #@7a
    if-eqz v3, :cond_88

    #@7c
    .line 168
    iget-object v3, p0, Lcom/lge/ims/service/ac/ACContentParser;->mCache:Lcom/lge/ims/service/ac/ACContentCache;

    #@7e
    iget-object v3, v3, Lcom/lge/ims/service/ac/ACContentCache;->mSubscriber:Lcom/lge/ims/service/ac/content/ACSubscriber;

    #@80
    invoke-interface {p1, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    #@83
    move-result-object v4

    #@84
    iput-object v4, v3, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPI:Ljava/lang/String;

    #@86
    goto/16 :goto_3

    #@88
    .line 171
    :cond_88
    const-string v3, "Public_User_Identity"

    #@8a
    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@8d
    move-result v3

    #@8e
    if-eqz v3, :cond_9f

    #@90
    .line 172
    iget-object v3, p0, Lcom/lge/ims/service/ac/ACContentParser;->mCache:Lcom/lge/ims/service/ac/ACContentCache;

    #@92
    iget-object v3, v3, Lcom/lge/ims/service/ac/ACContentCache;->mSubscriber:Lcom/lge/ims/service/ac/content/ACSubscriber;

    #@94
    iget-object v3, v3, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPUs:Ljava/util/ArrayList;

    #@96
    invoke-interface {p1, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    #@99
    move-result-object v4

    #@9a
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@9d
    goto/16 :goto_3

    #@9f
    .line 175
    :cond_9f
    const-string v3, "AuthType"

    #@a1
    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@a4
    move-result v3

    #@a5
    if-eqz v3, :cond_b3

    #@a7
    .line 176
    iget-object v3, p0, Lcom/lge/ims/service/ac/ACContentParser;->mCache:Lcom/lge/ims/service/ac/ACContentCache;

    #@a9
    iget-object v3, v3, Lcom/lge/ims/service/ac/ACContentCache;->mSubscriber:Lcom/lge/ims/service/ac/content/ACSubscriber;

    #@ab
    invoke-interface {p1, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    #@ae
    move-result-object v4

    #@af
    iput-object v4, v3, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthType:Ljava/lang/String;

    #@b1
    goto/16 :goto_3

    #@b3
    .line 177
    :cond_b3
    const-string v3, "Realm"

    #@b5
    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@b8
    move-result v3

    #@b9
    if-eqz v3, :cond_c7

    #@bb
    .line 178
    iget-object v3, p0, Lcom/lge/ims/service/ac/ACContentParser;->mCache:Lcom/lge/ims/service/ac/ACContentCache;

    #@bd
    iget-object v3, v3, Lcom/lge/ims/service/ac/ACContentCache;->mSubscriber:Lcom/lge/ims/service/ac/content/ACSubscriber;

    #@bf
    invoke-interface {p1, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    #@c2
    move-result-object v4

    #@c3
    iput-object v4, v3, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthRealm:Ljava/lang/String;

    #@c5
    goto/16 :goto_3

    #@c7
    .line 179
    :cond_c7
    const-string v3, "UserName"

    #@c9
    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@cc
    move-result v3

    #@cd
    if-eqz v3, :cond_db

    #@cf
    .line 180
    iget-object v3, p0, Lcom/lge/ims/service/ac/ACContentParser;->mCache:Lcom/lge/ims/service/ac/ACContentCache;

    #@d1
    iget-object v3, v3, Lcom/lge/ims/service/ac/ACContentCache;->mSubscriber:Lcom/lge/ims/service/ac/content/ACSubscriber;

    #@d3
    invoke-interface {p1, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    #@d6
    move-result-object v4

    #@d7
    iput-object v4, v3, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthUsername:Ljava/lang/String;

    #@d9
    goto/16 :goto_3

    #@db
    .line 181
    :cond_db
    const-string v3, "UserPwd"

    #@dd
    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@e0
    move-result v3

    #@e1
    if-eqz v3, :cond_ef

    #@e3
    .line 182
    iget-object v3, p0, Lcom/lge/ims/service/ac/ACContentParser;->mCache:Lcom/lge/ims/service/ac/ACContentCache;

    #@e5
    iget-object v3, v3, Lcom/lge/ims/service/ac/ACContentCache;->mSubscriber:Lcom/lge/ims/service/ac/content/ACSubscriber;

    #@e7
    invoke-interface {p1, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    #@ea
    move-result-object v4

    #@eb
    iput-object v4, v3, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthPassword:Ljava/lang/String;

    #@ed
    goto/16 :goto_3

    #@ef
    .line 185
    :cond_ef
    const-string v3, "Address"

    #@f1
    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@f4
    move-result v3

    #@f5
    if-eqz v3, :cond_145

    #@f7
    .line 186
    iget-object v3, p0, Lcom/lge/ims/service/ac/ACContentParser;->mCharacteristic:Ljava/lang/String;

    #@f9
    if-eqz v3, :cond_111

    #@fb
    iget-object v3, p0, Lcom/lge/ims/service/ac/ACContentParser;->mCharacteristic:Ljava/lang/String;

    #@fd
    const-string v4, "SBC_Address"

    #@ff
    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@102
    move-result v3

    #@103
    if-eqz v3, :cond_111

    #@105
    .line 187
    iget-object v3, p0, Lcom/lge/ims/service/ac/ACContentParser;->mCache:Lcom/lge/ims/service/ac/ACContentCache;

    #@107
    iget-object v3, v3, Lcom/lge/ims/service/ac/ACContentCache;->mProxyAddress:Lcom/lge/ims/service/ac/content/ACProxyAddress;

    #@109
    invoke-interface {p1, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    #@10c
    move-result-object v4

    #@10d
    iput-object v4, v3, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCAddress:Ljava/lang/String;

    #@10f
    goto/16 :goto_3

    #@111
    .line 188
    :cond_111
    iget-object v3, p0, Lcom/lge/ims/service/ac/ACContentParser;->mCharacteristic:Ljava/lang/String;

    #@113
    if-eqz v3, :cond_12b

    #@115
    iget-object v3, p0, Lcom/lge/ims/service/ac/ACContentParser;->mCharacteristic:Ljava/lang/String;

    #@117
    const-string v4, "SBC_TLS_Address"

    #@119
    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@11c
    move-result v3

    #@11d
    if-eqz v3, :cond_12b

    #@11f
    .line 189
    iget-object v3, p0, Lcom/lge/ims/service/ac/ACContentParser;->mCache:Lcom/lge/ims/service/ac/ACContentCache;

    #@121
    iget-object v3, v3, Lcom/lge/ims/service/ac/ACContentCache;->mProxyAddress:Lcom/lge/ims/service/ac/content/ACProxyAddress;

    #@123
    invoke-interface {p1, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    #@126
    move-result-object v4

    #@127
    iput-object v4, v3, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCTLSAddress:Ljava/lang/String;

    #@129
    goto/16 :goto_3

    #@12b
    .line 190
    :cond_12b
    iget-object v3, p0, Lcom/lge/ims/service/ac/ACContentParser;->mCharacteristic:Ljava/lang/String;

    #@12d
    if-eqz v3, :cond_3

    #@12f
    iget-object v3, p0, Lcom/lge/ims/service/ac/ACContentParser;->mCharacteristic:Ljava/lang/String;

    #@131
    const-string v4, "LBO_P-CSCF_Address"

    #@133
    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@136
    move-result v3

    #@137
    if-eqz v3, :cond_3

    #@139
    .line 191
    iget-object v3, p0, Lcom/lge/ims/service/ac/ACContentParser;->mCache:Lcom/lge/ims/service/ac/ACContentCache;

    #@13b
    iget-object v3, v3, Lcom/lge/ims/service/ac/ACContentCache;->mProxyAddress:Lcom/lge/ims/service/ac/content/ACProxyAddress;

    #@13d
    invoke-interface {p1, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    #@140
    move-result-object v4

    #@141
    iput-object v4, v3, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mLBOPCSCFAddress:Ljava/lang/String;

    #@143
    goto/16 :goto_3

    #@145
    .line 193
    :cond_145
    const-string v3, "AddressType"

    #@147
    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@14a
    move-result v3

    #@14b
    if-eqz v3, :cond_19b

    #@14d
    .line 194
    iget-object v3, p0, Lcom/lge/ims/service/ac/ACContentParser;->mCharacteristic:Ljava/lang/String;

    #@14f
    if-eqz v3, :cond_167

    #@151
    iget-object v3, p0, Lcom/lge/ims/service/ac/ACContentParser;->mCharacteristic:Ljava/lang/String;

    #@153
    const-string v4, "SBC_Address"

    #@155
    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@158
    move-result v3

    #@159
    if-eqz v3, :cond_167

    #@15b
    .line 195
    iget-object v3, p0, Lcom/lge/ims/service/ac/ACContentParser;->mCache:Lcom/lge/ims/service/ac/ACContentCache;

    #@15d
    iget-object v3, v3, Lcom/lge/ims/service/ac/ACContentCache;->mProxyAddress:Lcom/lge/ims/service/ac/content/ACProxyAddress;

    #@15f
    invoke-interface {p1, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    #@162
    move-result-object v4

    #@163
    iput-object v4, v3, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCAddressType:Ljava/lang/String;

    #@165
    goto/16 :goto_3

    #@167
    .line 196
    :cond_167
    iget-object v3, p0, Lcom/lge/ims/service/ac/ACContentParser;->mCharacteristic:Ljava/lang/String;

    #@169
    if-eqz v3, :cond_181

    #@16b
    iget-object v3, p0, Lcom/lge/ims/service/ac/ACContentParser;->mCharacteristic:Ljava/lang/String;

    #@16d
    const-string v4, "SBC_TLS_Address"

    #@16f
    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@172
    move-result v3

    #@173
    if-eqz v3, :cond_181

    #@175
    .line 197
    iget-object v3, p0, Lcom/lge/ims/service/ac/ACContentParser;->mCache:Lcom/lge/ims/service/ac/ACContentCache;

    #@177
    iget-object v3, v3, Lcom/lge/ims/service/ac/ACContentCache;->mProxyAddress:Lcom/lge/ims/service/ac/content/ACProxyAddress;

    #@179
    invoke-interface {p1, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    #@17c
    move-result-object v4

    #@17d
    iput-object v4, v3, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCTLSAddressType:Ljava/lang/String;

    #@17f
    goto/16 :goto_3

    #@181
    .line 198
    :cond_181
    iget-object v3, p0, Lcom/lge/ims/service/ac/ACContentParser;->mCharacteristic:Ljava/lang/String;

    #@183
    if-eqz v3, :cond_3

    #@185
    iget-object v3, p0, Lcom/lge/ims/service/ac/ACContentParser;->mCharacteristic:Ljava/lang/String;

    #@187
    const-string v4, "LBO_P-CSCF_Address"

    #@189
    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@18c
    move-result v3

    #@18d
    if-eqz v3, :cond_3

    #@18f
    .line 199
    iget-object v3, p0, Lcom/lge/ims/service/ac/ACContentParser;->mCache:Lcom/lge/ims/service/ac/ACContentCache;

    #@191
    iget-object v3, v3, Lcom/lge/ims/service/ac/ACContentCache;->mProxyAddress:Lcom/lge/ims/service/ac/content/ACProxyAddress;

    #@193
    invoke-interface {p1, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    #@196
    move-result-object v4

    #@197
    iput-object v4, v3, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mLBOPCSCFAddressType:Ljava/lang/String;

    #@199
    goto/16 :goto_3

    #@19b
    .line 203
    :cond_19b
    const-string v3, "RCS_OEM"

    #@19d
    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1a0
    move-result v3

    #@1a1
    if-eqz v3, :cond_1af

    #@1a3
    .line 204
    iget-object v3, p0, Lcom/lge/ims/service/ac/ACContentParser;->mCache:Lcom/lge/ims/service/ac/ACContentCache;

    #@1a5
    iget-object v3, v3, Lcom/lge/ims/service/ac/ACContentCache;->mServiceAvailability:Lcom/lge/ims/service/ac/content/ACServiceAvailability;

    #@1a7
    invoke-interface {p1, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    #@1aa
    move-result-object v4

    #@1ab
    iput-object v4, v3, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->mRCSOEM:Ljava/lang/String;

    #@1ad
    goto/16 :goto_3

    #@1af
    .line 205
    :cond_1af
    const-string v3, "VOLTE"

    #@1b1
    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1b4
    move-result v3

    #@1b5
    if-eqz v3, :cond_1c3

    #@1b7
    .line 206
    iget-object v3, p0, Lcom/lge/ims/service/ac/ACContentParser;->mCache:Lcom/lge/ims/service/ac/ACContentCache;

    #@1b9
    iget-object v3, v3, Lcom/lge/ims/service/ac/ACContentCache;->mServiceAvailability:Lcom/lge/ims/service/ac/content/ACServiceAvailability;

    #@1bb
    invoke-interface {p1, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    #@1be
    move-result-object v4

    #@1bf
    iput-object v4, v3, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->mVOLTE:Ljava/lang/String;

    #@1c1
    goto/16 :goto_3

    #@1c3
    .line 207
    :cond_1c3
    const-string v3, "PSVT"

    #@1c5
    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1c8
    move-result v3

    #@1c9
    if-eqz v3, :cond_3

    #@1cb
    .line 208
    iget-object v3, p0, Lcom/lge/ims/service/ac/ACContentParser;->mCache:Lcom/lge/ims/service/ac/ACContentCache;

    #@1cd
    iget-object v3, v3, Lcom/lge/ims/service/ac/ACContentCache;->mServiceAvailability:Lcom/lge/ims/service/ac/content/ACServiceAvailability;

    #@1cf
    invoke-interface {p1, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    #@1d2
    move-result-object v4

    #@1d3
    iput-object v4, v3, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->mPSVT:Ljava/lang/String;

    #@1d5
    goto/16 :goto_3
.end method


# virtual methods
.method public getContentCache()Lcom/lge/ims/service/ac/ACContentCache;
    .registers 2

    #@0
    .prologue
    .line 100
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACContentParser;->mCache:Lcom/lge/ims/service/ac/ACContentCache;

    #@2
    return-object v0
.end method

.method public hasValidContent()Z
    .registers 2

    #@0
    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/lge/ims/service/ac/ACContentParser;->mValidContent:Z

    #@2
    return v0
.end method

.method public invalidate()V
    .registers 2

    #@0
    .prologue
    .line 96
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Lcom/lge/ims/service/ac/ACContentParser;->mValidContent:Z

    #@3
    .line 97
    return-void
.end method

.method public parse(Ljava/io/InputStream;)V
    .registers 9
    .parameter "is"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 39
    if-nez p1, :cond_4

    #@3
    .line 93
    :cond_3
    :goto_3
    return-void

    #@4
    .line 44
    :cond_4
    const/4 v4, 0x0

    #@5
    iput-boolean v4, p0, Lcom/lge/ims/service/ac/ACContentParser;->mValidContent:Z

    #@7
    .line 45
    const/4 v4, 0x0

    #@8
    iput-object v4, p0, Lcom/lge/ims/service/ac/ACContentParser;->mCharacteristic:Ljava/lang/String;

    #@a
    .line 47
    iget-object v4, p0, Lcom/lge/ims/service/ac/ACContentParser;->mCache:Lcom/lge/ims/service/ac/ACContentCache;

    #@c
    invoke-virtual {v4}, Lcom/lge/ims/service/ac/ACContentCache;->clear()V

    #@f
    .line 49
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    #@12
    move-result-object v3

    #@13
    .line 50
    .local v3, xppFactory:Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual {v3, v6}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    #@16
    .line 52
    invoke-virtual {v3}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@19
    move-result-object v2

    #@1a
    .line 53
    .local v2, xpp:Lorg/xmlpull/v1/XmlPullParser;
    new-instance v1, Ljava/io/InputStreamReader;

    #@1c
    invoke-direct {v1, p1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    #@1f
    .line 55
    .local v1, isr:Ljava/io/InputStreamReader;
    invoke-interface {v2, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    #@22
    .line 57
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    #@25
    move-result v0

    #@26
    .line 60
    .local v0, event:I
    :goto_26
    packed-switch v0, :pswitch_data_78

    #@29
    .line 83
    :cond_29
    :goto_29
    :pswitch_29
    if-ne v0, v6, :cond_72

    #@2b
    .line 90
    iget-object v4, p0, Lcom/lge/ims/service/ac/ACContentParser;->mCache:Lcom/lge/ims/service/ac/ACContentCache;

    #@2d
    invoke-virtual {v4}, Lcom/lge/ims/service/ac/ACContentCache;->hasValidContent()Z

    #@30
    move-result v4

    #@31
    if-eqz v4, :cond_3

    #@33
    .line 91
    iput-boolean v6, p0, Lcom/lge/ims/service/ac/ACContentParser;->mValidContent:Z

    #@35
    goto :goto_3

    #@36
    .line 62
    :pswitch_36
    const-string v4, "ACContentParser"

    #@38
    const-string v5, "START_DOCUMENT"

    #@3a
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@3d
    goto :goto_29

    #@3e
    .line 65
    :pswitch_3e
    const-string v4, "ACContentParser"

    #@40
    const-string v5, "END_DOCUMENT"

    #@42
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@45
    goto :goto_29

    #@46
    .line 72
    :pswitch_46
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@49
    move-result-object v4

    #@4a
    const-string v5, "characteristic"

    #@4c
    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@4f
    move-result v4

    #@50
    if-eqz v4, :cond_56

    #@52
    .line 73
    invoke-direct {p0, v2}, Lcom/lge/ims/service/ac/ACContentParser;->parseCharacteristicOnStartTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@55
    goto :goto_29

    #@56
    .line 74
    :cond_56
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@59
    move-result-object v4

    #@5a
    const-string v5, "parm"

    #@5c
    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@5f
    move-result v4

    #@60
    if-nez v4, :cond_6e

    #@62
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@65
    move-result-object v4

    #@66
    const-string v5, "param"

    #@68
    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@6b
    move-result v4

    #@6c
    if-eqz v4, :cond_29

    #@6e
    .line 76
    :cond_6e
    invoke-direct {p0, v2}, Lcom/lge/ims/service/ac/ACContentParser;->parseParmOnStartTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@71
    goto :goto_29

    #@72
    .line 87
    :cond_72
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@75
    move-result v0

    #@76
    goto :goto_26

    #@77
    .line 60
    nop

    #@78
    :pswitch_data_78
    .packed-switch 0x0
        :pswitch_36
        :pswitch_3e
        :pswitch_46
        :pswitch_29
        :pswitch_29
    .end packed-switch
.end method
