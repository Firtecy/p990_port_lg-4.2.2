.class public Lcom/lge/ims/service/uc/ConfInfo;
.super Ljava/lang/Object;
.source "ConfInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/uc/ConfInfo$Participant;
    }
.end annotation


# static fields
.field public static final CCTYPE_BCC:I = 0x2

.field public static final CCTYPE_CC:I = 0x1

.field public static final CCTYPE_TO:I = 0x0

.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/lge/ims/service/uc/ConfInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "LGIMS_ConfInfo"


# instance fields
.field public participants:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/lge/ims/service/uc/ConfInfo$Participant;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 120
    new-instance v0, Lcom/lge/ims/service/uc/ConfInfo$1;

    #@2
    invoke-direct {v0}, Lcom/lge/ims/service/uc/ConfInfo$1;-><init>()V

    #@5
    sput-object v0, Lcom/lge/ims/service/uc/ConfInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    .line 30
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 19
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Lcom/lge/ims/service/uc/ConfInfo;->participants:Ljava/util/ArrayList;

    #@a
    .line 31
    const-string v0, "LGIMS_ConfInfo"

    #@c
    const-string v1, "ConfInfo()"

    #@e
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 4
    .parameter "source"

    #@0
    .prologue
    .line 34
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 19
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Lcom/lge/ims/service/uc/ConfInfo;->participants:Ljava/util/ArrayList;

    #@a
    .line 35
    const-string v0, "LGIMS_ConfInfo"

    #@c
    const-string v1, "ConfInfo()"

    #@e
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 36
    invoke-virtual {p0, p1}, Lcom/lge/ims/service/uc/ConfInfo;->readFromParcel(Landroid/os/Parcel;)V

    #@14
    .line 37
    return-void
.end method


# virtual methods
.method public addParticipant(Ljava/lang/String;IZ)V
    .registers 8
    .parameter "_target"
    .parameter "_ccType"
    .parameter "_anonymize"

    #@0
    .prologue
    .line 64
    const-string v1, "LGIMS_ConfInfo"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "ConfInfo() :addParticipant ["

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    iget-object v3, p0, Lcom/lge/ims/service/uc/ConfInfo;->participants:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@12
    move-result v3

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    const-string v3, "]"

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    const-string v3, "["

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    const-string v3, "]"

    #@29
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    const-string v3, "["

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    const-string v3, "]"

    #@39
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v2

    #@3d
    const-string v3, "["

    #@3f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v2

    #@43
    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    const-string v3, "]"

    #@49
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v2

    #@4d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v2

    #@51
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    .line 66
    new-instance v0, Lcom/lge/ims/service/uc/ConfInfo$Participant;

    #@56
    invoke-direct {v0, p0}, Lcom/lge/ims/service/uc/ConfInfo$Participant;-><init>(Lcom/lge/ims/service/uc/ConfInfo;)V

    #@59
    .line 67
    .local v0, participant:Lcom/lge/ims/service/uc/ConfInfo$Participant;
    iput-object p1, v0, Lcom/lge/ims/service/uc/ConfInfo$Participant;->target:Ljava/lang/String;

    #@5b
    .line 68
    iput p2, v0, Lcom/lge/ims/service/uc/ConfInfo$Participant;->ccType:I

    #@5d
    .line 69
    iput-boolean p3, v0, Lcom/lge/ims/service/uc/ConfInfo$Participant;->anonymize:Z

    #@5f
    .line 71
    iget-object v1, p0, Lcom/lge/ims/service/uc/ConfInfo;->participants:Ljava/util/ArrayList;

    #@61
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@64
    .line 73
    return-void
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 117
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getSize()I
    .registers 4

    #@0
    .prologue
    .line 57
    const-string v0, "LGIMS_ConfInfo"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "getSize : ["

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-object v2, p0, Lcom/lge/ims/service/uc/ConfInfo;->participants:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@12
    move-result v2

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, "]"

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 58
    iget-object v0, p0, Lcom/lge/ims/service/uc/ConfInfo;->participants:Ljava/util/ArrayList;

    #@26
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@29
    move-result v0

    #@2a
    return v0
.end method

.method public logIn()V
    .registers 6

    #@0
    .prologue
    .line 41
    const-string v2, "LGIMS_ConfInfo"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "logIn() : size["

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    iget-object v4, p0, Lcom/lge/ims/service/uc/ConfInfo;->participants:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@12
    move-result v4

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    const-string v4, "]"

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v3

    #@21
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 43
    const/4 v0, 0x0

    #@25
    .local v0, index:I
    :goto_25
    iget-object v2, p0, Lcom/lge/ims/service/uc/ConfInfo;->participants:Ljava/util/ArrayList;

    #@27
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@2a
    move-result v2

    #@2b
    if-ge v0, v2, :cond_7a

    #@2d
    .line 45
    iget-object v2, p0, Lcom/lge/ims/service/uc/ConfInfo;->participants:Ljava/util/ArrayList;

    #@2f
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@32
    move-result-object v1

    #@33
    check-cast v1, Lcom/lge/ims/service/uc/ConfInfo$Participant;

    #@35
    .line 47
    .local v1, participant:Lcom/lge/ims/service/uc/ConfInfo$Participant;
    const-string v2, "LGIMS_ConfInfo"

    #@37
    new-instance v3, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v4, "["

    #@3e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v3

    #@42
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@45
    move-result-object v3

    #@46
    const-string v4, "]"

    #@48
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v3

    #@4c
    const-string v4, " target : "

    #@4e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v3

    #@52
    iget-object v4, v1, Lcom/lge/ims/service/uc/ConfInfo$Participant;->target:Ljava/lang/String;

    #@54
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v3

    #@58
    const-string v4, " ccType : "

    #@5a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v3

    #@5e
    iget v4, v1, Lcom/lge/ims/service/uc/ConfInfo$Participant;->ccType:I

    #@60
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@63
    move-result-object v3

    #@64
    const-string v4, " anonymize : "

    #@66
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v3

    #@6a
    iget-boolean v4, v1, Lcom/lge/ims/service/uc/ConfInfo$Participant;->anonymize:Z

    #@6c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v3

    #@70
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@73
    move-result-object v3

    #@74
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@77
    .line 43
    add-int/lit8 v0, v0, 0x1

    #@79
    goto :goto_25

    #@7a
    .line 53
    .end local v1           #participant:Lcom/lge/ims/service/uc/ConfInfo$Participant;
    :cond_7a
    return-void
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .registers 9
    .parameter "source"

    #@0
    .prologue
    .line 77
    const-string v5, "LGIMS_ConfInfo"

    #@2
    const-string v6, "readFromParcel()"

    #@4
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 79
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@a
    move-result v3

    #@b
    .line 81
    .local v3, participant_num:I
    const/4 v2, 0x0

    #@c
    .local v2, index:I
    :goto_c
    if-ge v2, v3, :cond_26

    #@e
    .line 83
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@11
    move-result-object v4

    #@12
    .line 84
    .local v4, target:Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@15
    move-result v1

    #@16
    .line 86
    .local v1, ccType:I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@19
    move-result v5

    #@1a
    const/4 v6, 0x1

    #@1b
    if-ne v5, v6, :cond_24

    #@1d
    .line 87
    const/4 v0, 0x1

    #@1e
    .line 93
    .local v0, anonymize:Z
    :goto_1e
    invoke-virtual {p0, v4, v1, v0}, Lcom/lge/ims/service/uc/ConfInfo;->addParticipant(Ljava/lang/String;IZ)V

    #@21
    .line 81
    add-int/lit8 v2, v2, 0x1

    #@23
    goto :goto_c

    #@24
    .line 90
    .end local v0           #anonymize:Z
    :cond_24
    const/4 v0, 0x0

    #@25
    .restart local v0       #anonymize:Z
    goto :goto_1e

    #@26
    .line 96
    .end local v0           #anonymize:Z
    .end local v1           #ccType:I
    .end local v4           #target:Ljava/lang/String;
    :cond_26
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 100
    iget-object v2, p0, Lcom/lge/ims/service/uc/ConfInfo;->participants:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v2

    #@6
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@9
    .line 102
    const/4 v0, 0x0

    #@a
    .local v0, index:I
    :goto_a
    iget-object v2, p0, Lcom/lge/ims/service/uc/ConfInfo;->participants:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@f
    move-result v2

    #@10
    if-ge v0, v2, :cond_34

    #@12
    .line 103
    iget-object v2, p0, Lcom/lge/ims/service/uc/ConfInfo;->participants:Ljava/util/ArrayList;

    #@14
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@17
    move-result-object v1

    #@18
    check-cast v1, Lcom/lge/ims/service/uc/ConfInfo$Participant;

    #@1a
    .line 105
    .local v1, participant:Lcom/lge/ims/service/uc/ConfInfo$Participant;
    iget-object v2, v1, Lcom/lge/ims/service/uc/ConfInfo$Participant;->target:Ljava/lang/String;

    #@1c
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1f
    .line 106
    iget v2, v1, Lcom/lge/ims/service/uc/ConfInfo$Participant;->ccType:I

    #@21
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@24
    .line 108
    iget-boolean v2, v1, Lcom/lge/ims/service/uc/ConfInfo$Participant;->anonymize:Z

    #@26
    if-eqz v2, :cond_2f

    #@28
    .line 109
    const/4 v2, 0x1

    #@29
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@2c
    .line 102
    :goto_2c
    add-int/lit8 v0, v0, 0x1

    #@2e
    goto :goto_a

    #@2f
    .line 111
    :cond_2f
    const/4 v2, 0x0

    #@30
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@33
    goto :goto_2c

    #@34
    .line 114
    .end local v1           #participant:Lcom/lge/ims/service/uc/ConfInfo$Participant;
    :cond_34
    return-void
.end method
