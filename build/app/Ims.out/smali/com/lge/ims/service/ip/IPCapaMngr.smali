.class public Lcom/lge/ims/service/ip/IPCapaMngr;
.super Ljava/lang/Object;
.source "IPCapaMngr.java"


# instance fields
.field private bCurrentFTAvailable:Z

.field private bDeviceFTAvailable:Z

.field private bUse:Z

.field private nCallState:I

.field private nDataConnectionState:I

.field private objContext:Landroid/content/Context;

.field private objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

.field private phoneStateListener:Landroid/telephony/PhoneStateListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;)V
    .registers 4
    .parameter "_objContext"
    .parameter "_objIPAgentImpl"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 57
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 36
    iput v0, p0, Lcom/lge/ims/service/ip/IPCapaMngr;->nCallState:I

    #@6
    .line 37
    iput v0, p0, Lcom/lge/ims/service/ip/IPCapaMngr;->nDataConnectionState:I

    #@8
    .line 238
    new-instance v0, Lcom/lge/ims/service/ip/IPCapaMngr$1;

    #@a
    invoke-direct {v0, p0}, Lcom/lge/ims/service/ip/IPCapaMngr$1;-><init>(Lcom/lge/ims/service/ip/IPCapaMngr;)V

    #@d
    iput-object v0, p0, Lcom/lge/ims/service/ip/IPCapaMngr;->phoneStateListener:Landroid/telephony/PhoneStateListener;

    #@f
    .line 58
    if-eqz p1, :cond_13

    #@11
    if-nez p2, :cond_19

    #@13
    .line 59
    :cond_13
    const-string v0, "[IP][ERROR]Context or JNI Impl is null"

    #@15
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@18
    .line 65
    :goto_18
    return-void

    #@19
    .line 62
    :cond_19
    iput-object p1, p0, Lcom/lge/ims/service/ip/IPCapaMngr;->objContext:Landroid/content/Context;

    #@1b
    .line 63
    iput-object p2, p0, Lcom/lge/ims/service/ip/IPCapaMngr;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@1d
    .line 64
    invoke-direct {p0}, Lcom/lge/ims/service/ip/IPCapaMngr;->Initialize()V

    #@20
    goto :goto_18
.end method

.method private CheckCapability()V
    .registers 4

    #@0
    .prologue
    .line 147
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, ""

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    invoke-direct {p0}, Lcom/lge/ims/service/ip/IPCapaMngr;->GetNetworkType()I

    #@e
    move-result v2

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@1a
    .line 148
    const/4 v0, 0x1

    #@1b
    .line 151
    .local v0, bTempFTAvailable:Z
    invoke-direct {p0}, Lcom/lge/ims/service/ip/IPCapaMngr;->GetNetworkType()I

    #@1e
    move-result v1

    #@1f
    packed-switch v1, :pswitch_data_3e

    #@22
    .line 174
    :pswitch_22
    const-string v1, "[IP][ERROR]Invalid Network Type"

    #@24
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@27
    .line 175
    const/4 v0, 0x0

    #@28
    .line 181
    :goto_28
    iget-boolean v1, p0, Lcom/lge/ims/service/ip/IPCapaMngr;->bDeviceFTAvailable:Z

    #@2a
    if-nez v1, :cond_2d

    #@2c
    .line 182
    const/4 v0, 0x0

    #@2d
    .line 185
    :cond_2d
    iget-boolean v1, p0, Lcom/lge/ims/service/ip/IPCapaMngr;->bCurrentFTAvailable:Z

    #@2f
    if-eq v0, v1, :cond_33

    #@31
    .line 186
    iput-boolean v0, p0, Lcom/lge/ims/service/ip/IPCapaMngr;->bCurrentFTAvailable:Z

    #@33
    .line 188
    :cond_33
    return-void

    #@34
    .line 158
    :pswitch_34
    const/4 v0, 0x1

    #@35
    .line 160
    goto :goto_28

    #@36
    .line 164
    :pswitch_36
    iget v1, p0, Lcom/lge/ims/service/ip/IPCapaMngr;->nCallState:I

    #@38
    if-nez v1, :cond_3c

    #@3a
    .line 165
    const/4 v0, 0x1

    #@3b
    goto :goto_28

    #@3c
    .line 167
    :cond_3c
    const/4 v0, 0x0

    #@3d
    .line 170
    goto :goto_28

    #@3e
    .line 151
    :pswitch_data_3e
    .packed-switch 0x2
        :pswitch_36
        :pswitch_34
        :pswitch_22
        :pswitch_22
        :pswitch_22
        :pswitch_22
        :pswitch_34
        :pswitch_34
        :pswitch_34
    .end packed-switch
.end method

.method private Deinitialize()V
    .registers 4

    #@0
    .prologue
    .line 137
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPCapaMngr;->objContext:Landroid/content/Context;

    #@2
    const-string v2, "phone"

    #@4
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Landroid/telephony/TelephonyManager;

    #@a
    .line 138
    .local v0, telephonyManager:Landroid/telephony/TelephonyManager;
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPCapaMngr;->phoneStateListener:Landroid/telephony/PhoneStateListener;

    #@c
    const/4 v2, 0x0

    #@d
    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    #@10
    .line 139
    return-void
.end method

.method private GetNetworkType()I
    .registers 4

    #@0
    .prologue
    .line 230
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPCapaMngr;->objContext:Landroid/content/Context;

    #@2
    const-string v2, "phone"

    #@4
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Landroid/telephony/TelephonyManager;

    #@a
    .line 231
    .local v0, telephonyManager:Landroid/telephony/TelephonyManager;
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    #@d
    move-result v1

    #@e
    return v1
.end method

.method private Initialize()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 118
    const/4 v2, 0x0

    #@2
    iput-boolean v2, p0, Lcom/lge/ims/service/ip/IPCapaMngr;->bUse:Z

    #@4
    .line 120
    iput-boolean v3, p0, Lcom/lge/ims/service/ip/IPCapaMngr;->bDeviceFTAvailable:Z

    #@6
    .line 122
    iput-boolean v3, p0, Lcom/lge/ims/service/ip/IPCapaMngr;->bCurrentFTAvailable:Z

    #@8
    .line 123
    invoke-direct {p0}, Lcom/lge/ims/service/ip/IPCapaMngr;->CheckCapability()V

    #@b
    .line 125
    const/16 v0, 0x60

    #@d
    .line 127
    .local v0, nEvents:I
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPCapaMngr;->objContext:Landroid/content/Context;

    #@f
    const-string v3, "phone"

    #@11
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@14
    move-result-object v1

    #@15
    check-cast v1, Landroid/telephony/TelephonyManager;

    #@17
    .line 128
    .local v1, telephonyManager:Landroid/telephony/TelephonyManager;
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPCapaMngr;->phoneStateListener:Landroid/telephony/PhoneStateListener;

    #@19
    invoke-virtual {v1, v2, v0}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    #@1c
    .line 129
    return-void
.end method

.method static synthetic access$002(Lcom/lge/ims/service/ip/IPCapaMngr;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 28
    iput p1, p0, Lcom/lge/ims/service/ip/IPCapaMngr;->nCallState:I

    #@2
    return p1
.end method

.method static synthetic access$100(Lcom/lge/ims/service/ip/IPCapaMngr;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 28
    invoke-direct {p0}, Lcom/lge/ims/service/ip/IPCapaMngr;->CheckCapability()V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/lge/ims/service/ip/IPCapaMngr;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 28
    iget v0, p0, Lcom/lge/ims/service/ip/IPCapaMngr;->nDataConnectionState:I

    #@2
    return v0
.end method

.method static synthetic access$202(Lcom/lge/ims/service/ip/IPCapaMngr;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 28
    iput p1, p0, Lcom/lge/ims/service/ip/IPCapaMngr;->nDataConnectionState:I

    #@2
    return p1
.end method


# virtual methods
.method public Destroy()V
    .registers 1

    #@0
    .prologue
    .line 73
    invoke-direct {p0}, Lcom/lge/ims/service/ip/IPCapaMngr;->Deinitialize()V

    #@3
    .line 74
    return-void
.end method

.method public SetCapa()V
    .registers 1

    #@0
    .prologue
    .line 81
    invoke-direct {p0}, Lcom/lge/ims/service/ip/IPCapaMngr;->CheckCapability()V

    #@3
    .line 82
    return-void
.end method

.method public SetDeviceCapa(ZZZ)V
    .registers 4
    .parameter "_bDeviceVSAvailable"
    .parameter "_bDeviceISAvailable"
    .parameter "_bDeviceFTAvailable"

    #@0
    .prologue
    .line 110
    iput-boolean p3, p0, Lcom/lge/ims/service/ip/IPCapaMngr;->bDeviceFTAvailable:Z

    #@2
    .line 111
    return-void
.end method
