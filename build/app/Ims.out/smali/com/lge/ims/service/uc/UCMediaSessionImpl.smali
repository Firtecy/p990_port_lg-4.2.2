.class public Lcom/lge/ims/service/uc/UCMediaSessionImpl;
.super Lcom/lge/ims/service/uc/IUCMediaSession$Stub;
.source "UCMediaSessionImpl.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "_UCMediaSessionImpl"


# instance fields
.field private mListener:Lcom/lge/ims/service/uc/IUCMediaSessionListener;

.field private m_nSessionID:I


# direct methods
.method public constructor <init>(I)V
    .registers 3
    .parameter "sessionID"

    #@0
    .prologue
    .line 20
    invoke-direct {p0}, Lcom/lge/ims/service/uc/IUCMediaSession$Stub;-><init>()V

    #@3
    .line 17
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCMediaSessionListener;

    #@6
    .line 21
    iput p1, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->m_nSessionID:I

    #@8
    .line 22
    return-void
.end method


# virtual methods
.method public audioStop()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 35
    const-string v2, "_UCMediaSessionImpl"

    #@2
    const-string v3, "audioStop()"

    #@4
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 37
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@a
    move-result-object v1

    #@b
    .line 38
    .local v1, parcel:Landroid/os/Parcel;
    if-nez v1, :cond_e

    #@d
    .line 49
    :goto_d
    return-void

    #@e
    .line 42
    :cond_e
    const/16 v2, 0x58f

    #@10
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 44
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@16
    move-result-object v0

    #@17
    .line 45
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 46
    const/4 v1, 0x0

    #@1b
    .line 48
    iget v2, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->m_nSessionID:I

    #@1d
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@20
    goto :goto_d
.end method

.method public capture(Ljava/lang/String;I)V
    .registers 8
    .parameter "file"
    .parameter "view"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 142
    const-string v2, "_UCMediaSessionImpl"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "capture() :: file = "

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    const-string v4, ", view = "

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@22
    .line 144
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@25
    move-result-object v1

    #@26
    .line 145
    .local v1, parcel:Landroid/os/Parcel;
    if-nez v1, :cond_29

    #@28
    .line 158
    :goto_28
    return-void

    #@29
    .line 149
    :cond_29
    const/16 v2, 0x585

    #@2b
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@2e
    .line 150
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@31
    .line 151
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@34
    .line 153
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@37
    move-result-object v0

    #@38
    .line 154
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3b
    .line 155
    const/4 v1, 0x0

    #@3c
    .line 157
    iget v2, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->m_nSessionID:I

    #@3e
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@41
    goto :goto_28
.end method

.method public changeCameraBrightness(I)V
    .registers 7
    .parameter "bright"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 88
    const-string v2, "_UCMediaSessionImpl"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "changeCameraBrightness() :: bright ="

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 90
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@1b
    move-result-object v1

    #@1c
    .line 91
    .local v1, parcel:Landroid/os/Parcel;
    if-nez v1, :cond_1f

    #@1e
    .line 103
    :goto_1e
    return-void

    #@1f
    .line 95
    :cond_1f
    const/16 v2, 0x583

    #@21
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@24
    .line 96
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@27
    .line 98
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@2a
    move-result-object v0

    #@2b
    .line 99
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 100
    const/4 v1, 0x0

    #@2f
    .line 102
    iget v2, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->m_nSessionID:I

    #@31
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@34
    goto :goto_1e
.end method

.method public changeCameraZoom(I)V
    .registers 7
    .parameter "zoom"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 70
    const-string v2, "_UCMediaSessionImpl"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "changeCameraZoom() :: zoom ="

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 72
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@1b
    move-result-object v1

    #@1c
    .line 73
    .local v1, parcel:Landroid/os/Parcel;
    if-nez v1, :cond_1f

    #@1e
    .line 85
    :goto_1e
    return-void

    #@1f
    .line 77
    :cond_1f
    const/16 v2, 0x581

    #@21
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@24
    .line 78
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@27
    .line 80
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@2a
    move-result-object v0

    #@2b
    .line 81
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 82
    const/4 v1, 0x0

    #@2f
    .line 84
    iget v2, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->m_nSessionID:I

    #@31
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@34
    goto :goto_1e
.end method

.method public changeOrientation(II)V
    .registers 8
    .parameter "view"
    .parameter "orientation"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 288
    const-string v2, "_UCMediaSessionImpl"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "changeOrientation(), view="

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    const-string v4, ", orientation="

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@22
    .line 290
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@25
    move-result-object v1

    #@26
    .line 291
    .local v1, parcel:Landroid/os/Parcel;
    if-nez v1, :cond_29

    #@28
    .line 304
    :goto_28
    return-void

    #@29
    .line 295
    :cond_29
    const/16 v2, 0x59a

    #@2b
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@2e
    .line 296
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@31
    .line 297
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@34
    .line 299
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@37
    move-result-object v0

    #@38
    .line 300
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3b
    .line 301
    const/4 v1, 0x0

    #@3c
    .line 303
    iget v2, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->m_nSessionID:I

    #@3e
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@41
    goto :goto_28
.end method

.method public changeViewSize(II)V
    .registers 7
    .parameter "view"
    .parameter "size"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 269
    const-string v2, "_UCMediaSessionImpl"

    #@2
    const-string v3, "changeViewSize()"

    #@4
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 271
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@a
    move-result-object v1

    #@b
    .line 272
    .local v1, parcel:Landroid/os/Parcel;
    if-nez v1, :cond_e

    #@d
    .line 285
    :goto_d
    return-void

    #@e
    .line 276
    :cond_e
    const/16 v2, 0x594

    #@10
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 277
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 278
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 280
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@1c
    move-result-object v0

    #@1d
    .line 281
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@20
    .line 282
    const/4 v1, 0x0

    #@21
    .line 284
    iget v2, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->m_nSessionID:I

    #@23
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@26
    goto :goto_d
.end method

.method public getSessionId()I
    .registers 4

    #@0
    .prologue
    .line 25
    const-string v0, "_UCMediaSessionImpl"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "getSessionId : m_nSessionID = "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget v2, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->m_nSessionID:I

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@1a
    .line 26
    iget v0, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->m_nSessionID:I

    #@1c
    return v0
.end method

.method public onMessage(Landroid/os/Parcel;)V
    .registers 9
    .parameter "parcel"

    #@0
    .prologue
    .line 307
    iget-object v4, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCMediaSessionListener;

    #@2
    if-nez v4, :cond_c

    #@4
    .line 308
    const-string v4, "_UCMediaSessionImpl"

    #@6
    const-string v5, "listener is null"

    #@8
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@b
    .line 473
    :goto_b
    return-void

    #@c
    .line 312
    :cond_c
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@f
    move-result v2

    #@10
    .line 314
    .local v2, msg:I
    const-string v4, "_UCMediaSessionImpl"

    #@12
    new-instance v5, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v6, "onMessage() :: msg = "

    #@19
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v5

    #@1d
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v5

    #@21
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v5

    #@25
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@28
    .line 317
    packed-switch v2, :pswitch_data_27c

    #@2b
    :pswitch_2b
    goto :goto_b

    #@2c
    .line 320
    :pswitch_2c
    :try_start_2c
    const-string v4, "_UCMediaSessionImpl"

    #@2e
    const-string v5, "FARFRAME_IND"

    #@30
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@33
    .line 321
    iget-object v4, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCMediaSessionListener;

    #@35
    invoke-interface {v4}, Lcom/lge/ims/service/uc/IUCMediaSessionListener;->firstPeerVideoReceived()V
    :try_end_38
    .catch Landroid/os/RemoteException; {:try_start_2c .. :try_end_38} :catch_39

    #@38
    goto :goto_b

    #@39
    .line 469
    :catch_39
    move-exception v1

    #@3a
    .line 470
    .local v1, e:Landroid/os/RemoteException;
    const-string v4, "_UCMediaSessionImpl"

    #@3c
    new-instance v5, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v6, "RemoteException :: "

    #@43
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v5

    #@47
    invoke-virtual {v1}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    #@4a
    move-result-object v6

    #@4b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v5

    #@4f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v5

    #@53
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@56
    .line 471
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    #@59
    goto :goto_b

    #@5a
    .line 327
    .end local v1           #e:Landroid/os/RemoteException;
    :pswitch_5a
    :try_start_5a
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@5d
    move-result v3

    #@5e
    .line 328
    .local v3, result:I
    const-string v4, "_UCMediaSessionImpl"

    #@60
    new-instance v5, Ljava/lang/StringBuilder;

    #@62
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@65
    const-string v6, "BACKGROUND_STARTED_IND :: result"

    #@67
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v5

    #@6b
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v5

    #@6f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@72
    move-result-object v5

    #@73
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@76
    .line 329
    iget-object v4, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCMediaSessionListener;

    #@78
    invoke-interface {v4, v3}, Lcom/lge/ims/service/uc/IUCMediaSessionListener;->backgroundStarted(I)V

    #@7b
    goto :goto_b

    #@7c
    .line 335
    .end local v3           #result:I
    :pswitch_7c
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@7f
    move-result v3

    #@80
    .line 336
    .restart local v3       #result:I
    const-string v4, "_UCMediaSessionImpl"

    #@82
    new-instance v5, Ljava/lang/StringBuilder;

    #@84
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@87
    const-string v6, "BACKGROUND_STOPPED_IND :: result"

    #@89
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v5

    #@8d
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@90
    move-result-object v5

    #@91
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v5

    #@95
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@98
    .line 337
    iget-object v4, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCMediaSessionListener;

    #@9a
    invoke-interface {v4, v3}, Lcom/lge/ims/service/uc/IUCMediaSessionListener;->backgroundStopped(I)V

    #@9d
    goto/16 :goto_b

    #@9f
    .line 343
    .end local v3           #result:I
    :pswitch_9f
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@a2
    move-result v3

    #@a3
    .line 344
    .restart local v3       #result:I
    const-string v4, "_UCMediaSessionImpl"

    #@a5
    new-instance v5, Ljava/lang/StringBuilder;

    #@a7
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@aa
    const-string v6, "CAMERA_SELECTED_IND :: result"

    #@ac
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af
    move-result-object v5

    #@b0
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v5

    #@b4
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b7
    move-result-object v5

    #@b8
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@bb
    .line 345
    iget-object v4, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCMediaSessionListener;

    #@bd
    invoke-interface {v4, v3}, Lcom/lge/ims/service/uc/IUCMediaSessionListener;->cameraSelected(I)V

    #@c0
    goto/16 :goto_b

    #@c2
    .line 351
    .end local v3           #result:I
    :pswitch_c2
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@c5
    move-result v3

    #@c6
    .line 352
    .restart local v3       #result:I
    const-string v4, "_UCMediaSessionImpl"

    #@c8
    new-instance v5, Ljava/lang/StringBuilder;

    #@ca
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@cd
    const-string v6, "CAMERA_ZOOM_CHANGED_IND :: result"

    #@cf
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v5

    #@d3
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d6
    move-result-object v5

    #@d7
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@da
    move-result-object v5

    #@db
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@de
    .line 353
    iget-object v4, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCMediaSessionListener;

    #@e0
    invoke-interface {v4, v3}, Lcom/lge/ims/service/uc/IUCMediaSessionListener;->cameraZoomChanged(I)V

    #@e3
    goto/16 :goto_b

    #@e5
    .line 359
    .end local v3           #result:I
    :pswitch_e5
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@e8
    move-result v3

    #@e9
    .line 360
    .restart local v3       #result:I
    const-string v4, "_UCMediaSessionImpl"

    #@eb
    new-instance v5, Ljava/lang/StringBuilder;

    #@ed
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@f0
    const-string v6, "CAMERA_BRIGHTNESS_CHANGED_IND :: result"

    #@f2
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f5
    move-result-object v5

    #@f6
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f9
    move-result-object v5

    #@fa
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fd
    move-result-object v5

    #@fe
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@101
    .line 361
    iget-object v4, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCMediaSessionListener;

    #@103
    invoke-interface {v4, v3}, Lcom/lge/ims/service/uc/IUCMediaSessionListener;->cameraBrightnessChanged(I)V

    #@106
    goto/16 :goto_b

    #@108
    .line 367
    .end local v3           #result:I
    :pswitch_108
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@10b
    move-result v3

    #@10c
    .line 368
    .restart local v3       #result:I
    const-string v4, "_UCMediaSessionImpl"

    #@10e
    new-instance v5, Ljava/lang/StringBuilder;

    #@110
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@113
    const-string v6, "CAPTURED_IND :: result"

    #@115
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@118
    move-result-object v5

    #@119
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11c
    move-result-object v5

    #@11d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@120
    move-result-object v5

    #@121
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@124
    .line 369
    iget-object v4, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCMediaSessionListener;

    #@126
    invoke-interface {v4, v3}, Lcom/lge/ims/service/uc/IUCMediaSessionListener;->captured(I)V

    #@129
    goto/16 :goto_b

    #@12b
    .line 375
    .end local v3           #result:I
    :pswitch_12b
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@12e
    move-result v3

    #@12f
    .line 376
    .restart local v3       #result:I
    const-string v4, "_UCMediaSessionImpl"

    #@131
    new-instance v5, Ljava/lang/StringBuilder;

    #@133
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@136
    const-string v6, "RECODING_STARTED_IND :: result"

    #@138
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13b
    move-result-object v5

    #@13c
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13f
    move-result-object v5

    #@140
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@143
    move-result-object v5

    #@144
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@147
    .line 377
    iget-object v4, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCMediaSessionListener;

    #@149
    invoke-interface {v4, v3}, Lcom/lge/ims/service/uc/IUCMediaSessionListener;->recordingStarted(I)V

    #@14c
    goto/16 :goto_b

    #@14e
    .line 383
    .end local v3           #result:I
    :pswitch_14e
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@151
    move-result v3

    #@152
    .line 384
    .restart local v3       #result:I
    const-string v4, "_UCMediaSessionImpl"

    #@154
    new-instance v5, Ljava/lang/StringBuilder;

    #@156
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@159
    const-string v6, "RECODING_STOPPED_IND :: result"

    #@15b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15e
    move-result-object v5

    #@15f
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@162
    move-result-object v5

    #@163
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@166
    move-result-object v5

    #@167
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@16a
    .line 385
    iget-object v4, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCMediaSessionListener;

    #@16c
    invoke-interface {v4, v3}, Lcom/lge/ims/service/uc/IUCMediaSessionListener;->recordingStopped(I)V

    #@16f
    goto/16 :goto_b

    #@171
    .line 391
    .end local v3           #result:I
    :pswitch_171
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@174
    move-result v3

    #@175
    .line 392
    .restart local v3       #result:I
    const-string v4, "_UCMediaSessionImpl"

    #@177
    new-instance v5, Ljava/lang/StringBuilder;

    #@179
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@17c
    const-string v6, "ALTERNATE_IMAGE_STARTED_IND :: result"

    #@17e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@181
    move-result-object v5

    #@182
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@185
    move-result-object v5

    #@186
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@189
    move-result-object v5

    #@18a
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@18d
    .line 393
    iget-object v4, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCMediaSessionListener;

    #@18f
    invoke-interface {v4, v3}, Lcom/lge/ims/service/uc/IUCMediaSessionListener;->alternateImageStarted(I)V

    #@192
    goto/16 :goto_b

    #@194
    .line 399
    .end local v3           #result:I
    :pswitch_194
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@197
    move-result v3

    #@198
    .line 400
    .restart local v3       #result:I
    const-string v4, "_UCMediaSessionImpl"

    #@19a
    new-instance v5, Ljava/lang/StringBuilder;

    #@19c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@19f
    const-string v6, "ALTERNATE_IMAGE_STOPPED_IND :: result"

    #@1a1
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a4
    move-result-object v5

    #@1a5
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a8
    move-result-object v5

    #@1a9
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ac
    move-result-object v5

    #@1ad
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@1b0
    .line 401
    iget-object v4, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCMediaSessionListener;

    #@1b2
    invoke-interface {v4, v3}, Lcom/lge/ims/service/uc/IUCMediaSessionListener;->alternateImageStopped(I)V

    #@1b5
    goto/16 :goto_b

    #@1b7
    .line 407
    .end local v3           #result:I
    :pswitch_1b7
    const-string v4, "_UCMediaSessionImpl"

    #@1b9
    const-string v5, "AUDIO_STARTED_IND"

    #@1bb
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@1be
    .line 408
    iget-object v4, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCMediaSessionListener;

    #@1c0
    invoke-interface {v4}, Lcom/lge/ims/service/uc/IUCMediaSessionListener;->audioStarted()V

    #@1c3
    goto/16 :goto_b

    #@1c5
    .line 414
    :pswitch_1c5
    const-string v4, "_UCMediaSessionImpl"

    #@1c7
    const-string v5, "MEDIA_STARTED_IND"

    #@1c9
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@1cc
    .line 415
    iget-object v4, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCMediaSessionListener;

    #@1ce
    invoke-interface {v4}, Lcom/lge/ims/service/uc/IUCMediaSessionListener;->mediaStarted()V

    #@1d1
    goto/16 :goto_b

    #@1d3
    .line 421
    :pswitch_1d3
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1d6
    move-result v3

    #@1d7
    .line 422
    .restart local v3       #result:I
    const-string v4, "_UCMediaSessionImpl"

    #@1d9
    new-instance v5, Ljava/lang/StringBuilder;

    #@1db
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1de
    const-string v6, "DISPLAY_UPDATED_IND :: result"

    #@1e0
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e3
    move-result-object v5

    #@1e4
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e7
    move-result-object v5

    #@1e8
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1eb
    move-result-object v5

    #@1ec
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@1ef
    .line 423
    iget-object v4, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCMediaSessionListener;

    #@1f1
    invoke-interface {v4, v3}, Lcom/lge/ims/service/uc/IUCMediaSessionListener;->displayUpdated(I)V

    #@1f4
    goto/16 :goto_b

    #@1f6
    .line 429
    .end local v3           #result:I
    :pswitch_1f6
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1f9
    move-result v3

    #@1fa
    .line 430
    .restart local v3       #result:I
    const-string v4, "_UCMediaSessionImpl"

    #@1fc
    new-instance v5, Ljava/lang/StringBuilder;

    #@1fe
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@201
    const-string v6, "VIEW_SIZE_CHANGED_IND :: result"

    #@203
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@206
    move-result-object v5

    #@207
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20a
    move-result-object v5

    #@20b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20e
    move-result-object v5

    #@20f
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@212
    .line 431
    iget-object v4, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCMediaSessionListener;

    #@214
    invoke-interface {v4, v3}, Lcom/lge/ims/service/uc/IUCMediaSessionListener;->viewSizeChanged(I)V

    #@217
    goto/16 :goto_b

    #@219
    .line 437
    .end local v3           #result:I
    :pswitch_219
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@21c
    move-result v3

    #@21d
    .line 438
    .restart local v3       #result:I
    const-string v4, "_UCMediaSessionImpl"

    #@21f
    new-instance v5, Ljava/lang/StringBuilder;

    #@221
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@224
    const-string v6, "DISPLAY_SWAPPED_IND :: result"

    #@226
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@229
    move-result-object v5

    #@22a
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22d
    move-result-object v5

    #@22e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@231
    move-result-object v5

    #@232
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@235
    .line 439
    iget-object v4, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCMediaSessionListener;

    #@237
    invoke-interface {v4, v3}, Lcom/lge/ims/service/uc/IUCMediaSessionListener;->displaySwapped(I)V

    #@23a
    goto/16 :goto_b

    #@23c
    .line 445
    .end local v3           #result:I
    :pswitch_23c
    const-string v4, "_UCMediaSessionImpl"

    #@23e
    const-string v5, "AUDIO_STOPPED_IND"

    #@240
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@243
    .line 446
    iget-object v4, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCMediaSessionListener;

    #@245
    invoke-interface {v4}, Lcom/lge/ims/service/uc/IUCMediaSessionListener;->audioStopped()V

    #@248
    goto/16 :goto_b

    #@24a
    .line 452
    :pswitch_24a
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@24d
    move-result v3

    #@24e
    .line 453
    .restart local v3       #result:I
    const-string v4, "_UCMediaSessionImpl"

    #@250
    new-instance v5, Ljava/lang/StringBuilder;

    #@252
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@255
    const-string v6, "ORIENTATION_CHANGED_IND :: result"

    #@257
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25a
    move-result-object v5

    #@25b
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25e
    move-result-object v5

    #@25f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@262
    move-result-object v5

    #@263
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@266
    .line 454
    iget-object v4, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCMediaSessionListener;

    #@268
    invoke-interface {v4, v3}, Lcom/lge/ims/service/uc/IUCMediaSessionListener;->orientationChanged(I)V

    #@26b
    goto/16 :goto_b

    #@26d
    .line 460
    .end local v3           #result:I
    :pswitch_26d
    new-instance v0, Lcom/lge/ims/service/uc/OnScreenDebugInfoVideo;

    #@26f
    invoke-direct {v0, p1}, Lcom/lge/ims/service/uc/OnScreenDebugInfoVideo;-><init>(Landroid/os/Parcel;)V

    #@272
    .line 461
    .local v0, DebugInfoVideo:Lcom/lge/ims/service/uc/OnScreenDebugInfoVideo;
    invoke-virtual {v0}, Lcom/lge/ims/service/uc/OnScreenDebugInfoVideo;->logIn()V

    #@275
    .line 462
    iget-object v4, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCMediaSessionListener;

    #@277
    invoke-interface {v4, v0}, Lcom/lge/ims/service/uc/IUCMediaSessionListener;->DebugInfoUpdated(Lcom/lge/ims/service/uc/OnScreenDebugInfoVideo;)V
    :try_end_27a
    .catch Landroid/os/RemoteException; {:try_start_5a .. :try_end_27a} :catch_39

    #@27a
    goto/16 :goto_b

    #@27c
    .line 317
    :pswitch_data_27c
    .packed-switch 0x57a
        :pswitch_2c
        :pswitch_2b
        :pswitch_5a
        :pswitch_2b
        :pswitch_7c
        :pswitch_2b
        :pswitch_9f
        :pswitch_2b
        :pswitch_c2
        :pswitch_2b
        :pswitch_e5
        :pswitch_2b
        :pswitch_108
        :pswitch_2b
        :pswitch_12b
        :pswitch_2b
        :pswitch_14e
        :pswitch_2b
        :pswitch_171
        :pswitch_2b
        :pswitch_194
        :pswitch_2b
        :pswitch_1b7
        :pswitch_1c5
        :pswitch_2b
        :pswitch_1d3
        :pswitch_2b
        :pswitch_1f6
        :pswitch_2b
        :pswitch_219
        :pswitch_2b
        :pswitch_23c
        :pswitch_2b
        :pswitch_24a
        :pswitch_26d
    .end packed-switch
.end method

.method public selectCamera(I)V
    .registers 7
    .parameter "camera"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 52
    const-string v2, "_UCMediaSessionImpl"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "selectCamera() :: camera ="

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 54
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@1b
    move-result-object v1

    #@1c
    .line 55
    .local v1, parcel:Landroid/os/Parcel;
    if-nez v1, :cond_1f

    #@1e
    .line 67
    :goto_1e
    return-void

    #@1f
    .line 59
    :cond_1f
    const/16 v2, 0x57f

    #@21
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@24
    .line 60
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@27
    .line 62
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@2a
    move-result-object v0

    #@2b
    .line 63
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 64
    const/4 v1, 0x0

    #@2f
    .line 66
    iget v2, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->m_nSessionID:I

    #@31
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@34
    goto :goto_1e
.end method

.method public setListenerThread(Lcom/lge/ims/service/uc/IUCMediaSessionListener;)V
    .registers 2
    .parameter "_listener"

    #@0
    .prologue
    .line 30
    iput-object p1, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCMediaSessionListener;

    #@2
    .line 31
    return-void
.end method

.method public startAlternateImage(Ljava/lang/String;)V
    .registers 7
    .parameter "file"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 199
    const-string v2, "_UCMediaSessionImpl"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "startAlternateImage() :: file = "

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 201
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@1b
    move-result-object v1

    #@1c
    .line 202
    .local v1, parcel:Landroid/os/Parcel;
    if-nez v1, :cond_1f

    #@1e
    .line 214
    :goto_1e
    return-void

    #@1f
    .line 206
    :cond_1f
    const/16 v2, 0x58b

    #@21
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@24
    .line 207
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@27
    .line 209
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@2a
    move-result-object v0

    #@2b
    .line 210
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 211
    const/4 v1, 0x0

    #@2f
    .line 213
    iget v2, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->m_nSessionID:I

    #@31
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@34
    goto :goto_1e
.end method

.method public startBackground()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 107
    const-string v2, "_UCMediaSessionImpl"

    #@2
    const-string v3, "startBackground()"

    #@4
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 109
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@a
    move-result-object v1

    #@b
    .line 110
    .local v1, parcel:Landroid/os/Parcel;
    if-nez v1, :cond_e

    #@d
    .line 121
    :goto_d
    return-void

    #@e
    .line 114
    :cond_e
    const/16 v2, 0x57b

    #@10
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 116
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@16
    move-result-object v0

    #@17
    .line 117
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 118
    const/4 v1, 0x0

    #@1b
    .line 120
    iget v2, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->m_nSessionID:I

    #@1d
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@20
    goto :goto_d
.end method

.method public startRecording(Ljava/lang/String;I)V
    .registers 8
    .parameter "file"
    .parameter "view"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 162
    const-string v2, "_UCMediaSessionImpl"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "startRecording() :: file = "

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    const-string v4, ", view = "

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@22
    .line 164
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@25
    move-result-object v1

    #@26
    .line 165
    .local v1, parcel:Landroid/os/Parcel;
    if-nez v1, :cond_29

    #@28
    .line 178
    :goto_28
    return-void

    #@29
    .line 169
    :cond_29
    const/16 v2, 0x587

    #@2b
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@2e
    .line 170
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@31
    .line 171
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@34
    .line 173
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@37
    move-result-object v0

    #@38
    .line 174
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3b
    .line 175
    const/4 v1, 0x0

    #@3c
    .line 177
    iget v2, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->m_nSessionID:I

    #@3e
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@41
    goto :goto_28
.end method

.method public stopAlternateImage()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 217
    const-string v2, "_UCMediaSessionImpl"

    #@2
    const-string v3, "stopAlternateImage()"

    #@4
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 219
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@a
    move-result-object v1

    #@b
    .line 220
    .local v1, parcel:Landroid/os/Parcel;
    if-nez v1, :cond_e

    #@d
    .line 231
    :goto_d
    return-void

    #@e
    .line 224
    :cond_e
    const/16 v2, 0x58d

    #@10
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 226
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@16
    move-result-object v0

    #@17
    .line 227
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 228
    const/4 v1, 0x0

    #@1b
    .line 230
    iget v2, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->m_nSessionID:I

    #@1d
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@20
    goto :goto_d
.end method

.method public stopBackground()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 124
    const-string v2, "_UCMediaSessionImpl"

    #@2
    const-string v3, "stopBackground()"

    #@4
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 126
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@a
    move-result-object v1

    #@b
    .line 127
    .local v1, parcel:Landroid/os/Parcel;
    if-nez v1, :cond_e

    #@d
    .line 138
    :goto_d
    return-void

    #@e
    .line 131
    :cond_e
    const/16 v2, 0x57d

    #@10
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 133
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@16
    move-result-object v0

    #@17
    .line 134
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 135
    const/4 v1, 0x0

    #@1b
    .line 137
    iget v2, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->m_nSessionID:I

    #@1d
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@20
    goto :goto_d
.end method

.method public stopRecording()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 181
    const-string v2, "_UCMediaSessionImpl"

    #@2
    const-string v3, "stopRecording()"

    #@4
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 183
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@a
    move-result-object v1

    #@b
    .line 184
    .local v1, parcel:Landroid/os/Parcel;
    if-nez v1, :cond_e

    #@d
    .line 195
    :goto_d
    return-void

    #@e
    .line 188
    :cond_e
    const/16 v2, 0x589

    #@10
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 190
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@16
    move-result-object v0

    #@17
    .line 191
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 192
    const/4 v1, 0x0

    #@1b
    .line 194
    iget v2, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->m_nSessionID:I

    #@1d
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@20
    goto :goto_d
.end method

.method public swapDisplay()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 235
    const-string v2, "_UCMediaSessionImpl"

    #@2
    const-string v3, "swapDisplay()"

    #@4
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 237
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@a
    move-result-object v1

    #@b
    .line 238
    .local v1, parcel:Landroid/os/Parcel;
    if-nez v1, :cond_e

    #@d
    .line 249
    :goto_d
    return-void

    #@e
    .line 242
    :cond_e
    const/16 v2, 0x596

    #@10
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 244
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@16
    move-result-object v0

    #@17
    .line 245
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 246
    const/4 v1, 0x0

    #@1b
    .line 248
    iget v2, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->m_nSessionID:I

    #@1d
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@20
    goto :goto_d
.end method

.method public updateDisplay(I)V
    .registers 6
    .parameter "view"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 252
    const-string v2, "_UCMediaSessionImpl"

    #@2
    const-string v3, "updateDisplay()"

    #@4
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 254
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@a
    move-result-object v1

    #@b
    .line 255
    .local v1, parcel:Landroid/os/Parcel;
    if-nez v1, :cond_e

    #@d
    .line 266
    :goto_d
    return-void

    #@e
    .line 259
    :cond_e
    const/16 v2, 0x592

    #@10
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 261
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@16
    move-result-object v0

    #@17
    .line 262
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 263
    const/4 v1, 0x0

    #@1b
    .line 265
    iget v2, p0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->m_nSessionID:I

    #@1d
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@20
    goto :goto_d
.end method
