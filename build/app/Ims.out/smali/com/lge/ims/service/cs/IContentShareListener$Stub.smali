.class public abstract Lcom/lge/ims/service/cs/IContentShareListener$Stub;
.super Landroid/os/Binder;
.source "IContentShareListener.java"

# interfaces
.implements Lcom/lge/ims/service/cs/IContentShareListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/cs/IContentShareListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/cs/IContentShareListener$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.lge.ims.service.cs.IContentShareListener"

.field static final TRANSACTION_onReceivedImageSession:I = 0x4

.field static final TRANSACTION_onReceivedVideoSession:I = 0x3

.field static final TRANSACTION_onReconfigured:I = 0x1

.field static final TRANSACTION_onUpdateCapability:I = 0x2


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 15
    const-string v0, "com.lge.ims.service.cs.IContentShareListener"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/lge/ims/service/cs/IContentShareListener$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/cs/IContentShareListener;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 23
    if-nez p0, :cond_4

    #@2
    .line 24
    const/4 v0, 0x0

    #@3
    .line 30
    :goto_3
    return-object v0

    #@4
    .line 26
    :cond_4
    const-string v1, "com.lge.ims.service.cs.IContentShareListener"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 27
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/lge/ims/service/cs/IContentShareListener;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 28
    check-cast v0, Lcom/lge/ims/service/cs/IContentShareListener;

    #@12
    goto :goto_3

    #@13
    .line 30
    :cond_13
    new-instance v0, Lcom/lge/ims/service/cs/IContentShareListener$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/lge/ims/service/cs/IContentShareListener$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 11
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 38
    sparse-switch p1, :sswitch_data_70

    #@4
    .line 93
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v4

    #@8
    :goto_8
    return v4

    #@9
    .line 42
    :sswitch_9
    const-string v5, "com.lge.ims.service.cs.IContentShareListener"

    #@b
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 47
    :sswitch_f
    const-string v5, "com.lge.ims.service.cs.IContentShareListener"

    #@11
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v0

    #@18
    .line 50
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/cs/IContentShareListener$Stub;->onReconfigured(I)V

    #@1b
    .line 51
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1e
    goto :goto_8

    #@1f
    .line 56
    .end local v0           #_arg0:I
    :sswitch_1f
    const-string v5, "com.lge.ims.service.cs.IContentShareListener"

    #@21
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@24
    .line 58
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@27
    move-result v5

    #@28
    if-eqz v5, :cond_39

    #@2a
    .line 59
    sget-object v5, Lcom/lge/ims/service/cd/Capabilities;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2c
    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2f
    move-result-object v0

    #@30
    check-cast v0, Lcom/lge/ims/service/cd/Capabilities;

    #@32
    .line 64
    .local v0, _arg0:Lcom/lge/ims/service/cd/Capabilities;
    :goto_32
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/cs/IContentShareListener$Stub;->onUpdateCapability(Lcom/lge/ims/service/cd/Capabilities;)V

    #@35
    .line 65
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@38
    goto :goto_8

    #@39
    .line 62
    .end local v0           #_arg0:Lcom/lge/ims/service/cd/Capabilities;
    :cond_39
    const/4 v0, 0x0

    #@3a
    .restart local v0       #_arg0:Lcom/lge/ims/service/cd/Capabilities;
    goto :goto_32

    #@3b
    .line 70
    .end local v0           #_arg0:Lcom/lge/ims/service/cd/Capabilities;
    :sswitch_3b
    const-string v5, "com.lge.ims.service.cs.IContentShareListener"

    #@3d
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@40
    .line 72
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@43
    move-result-object v5

    #@44
    invoke-static {v5}, Lcom/lge/ims/service/cs/IVideoSession$Stub;->asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/cs/IVideoSession;

    #@47
    move-result-object v0

    #@48
    .line 73
    .local v0, _arg0:Lcom/lge/ims/service/cs/IVideoSession;
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/cs/IContentShareListener$Stub;->onReceivedVideoSession(Lcom/lge/ims/service/cs/IVideoSession;)V

    #@4b
    .line 74
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4e
    goto :goto_8

    #@4f
    .line 79
    .end local v0           #_arg0:Lcom/lge/ims/service/cs/IVideoSession;
    :sswitch_4f
    const-string v5, "com.lge.ims.service.cs.IContentShareListener"

    #@51
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@54
    .line 81
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@57
    move-result-object v5

    #@58
    invoke-static {v5}, Lcom/lge/ims/service/cs/IImageSession$Stub;->asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/cs/IImageSession;

    #@5b
    move-result-object v0

    #@5c
    .line 83
    .local v0, _arg0:Lcom/lge/ims/service/cs/IImageSession;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@5f
    move-result-object v1

    #@60
    .line 85
    .local v1, _arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@63
    move-result-object v2

    #@64
    .line 87
    .local v2, _arg2:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@67
    move-result v3

    #@68
    .line 88
    .local v3, _arg3:I
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/lge/ims/service/cs/IContentShareListener$Stub;->onReceivedImageSession(Lcom/lge/ims/service/cs/IImageSession;Ljava/lang/String;Ljava/lang/String;I)V

    #@6b
    .line 89
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@6e
    goto :goto_8

    #@6f
    .line 38
    nop

    #@70
    :sswitch_data_70
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_1f
        0x3 -> :sswitch_3b
        0x4 -> :sswitch_4f
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
