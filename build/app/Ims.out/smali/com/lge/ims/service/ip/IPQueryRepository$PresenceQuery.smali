.class Lcom/lge/ims/service/ip/IPQueryRepository$PresenceQuery;
.super Ljava/lang/Object;
.source "IPQueryRepository.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/ip/IPQueryRepository;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PresenceQuery"
.end annotation


# instance fields
.field private strMSISDN:Ljava/lang/String;

.field final synthetic this$0:Lcom/lge/ims/service/ip/IPQueryRepository;


# direct methods
.method public constructor <init>(Lcom/lge/ims/service/ip/IPQueryRepository;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter "_strMSISDN"

    #@0
    .prologue
    .line 149
    iput-object p1, p0, Lcom/lge/ims/service/ip/IPQueryRepository$PresenceQuery;->this$0:Lcom/lge/ims/service/ip/IPQueryRepository;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 150
    if-eqz p2, :cond_d

    #@7
    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    #@a
    move-result v0

    #@b
    if-eqz v0, :cond_13

    #@d
    .line 151
    :cond_d
    const-string v0, "[IP][ERROR] MSISDN is null"

    #@f
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@12
    .line 155
    :goto_12
    return-void

    #@13
    .line 154
    :cond_13
    iput-object p2, p0, Lcom/lge/ims/service/ip/IPQueryRepository$PresenceQuery;->strMSISDN:Ljava/lang/String;

    #@15
    goto :goto_12
.end method


# virtual methods
.method public GetMSISDN()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 158
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPQueryRepository$PresenceQuery;->strMSISDN:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public SetMSISDN(Ljava/lang/String;)V
    .registers 2
    .parameter "_strMSISDN"

    #@0
    .prologue
    .line 162
    iput-object p1, p0, Lcom/lge/ims/service/ip/IPQueryRepository$PresenceQuery;->strMSISDN:Ljava/lang/String;

    #@2
    .line 163
    return-void
.end method
