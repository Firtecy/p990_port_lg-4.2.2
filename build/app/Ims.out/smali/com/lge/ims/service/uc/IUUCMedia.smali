.class public Lcom/lge/ims/service/uc/IUUCMedia;
.super Ljava/lang/Object;
.source "IUUCMedia.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/uc/IUUCMedia$Reason;
    }
.end annotation


# static fields
.field public static final ALTERNATE_IMAGE_STARTED_IND:I = 0x58c

.field public static final ALTERNATE_IMAGE_STOPPED_IND:I = 0x58e

.field public static final AUDIO_STARTED_IND:I = 0x590

.field public static final AUDIO_START_CMD:I = 0x598

.field public static final AUDIO_STOPPED_IND:I = 0x599

.field public static final AUDIO_STOP_CMD:I = 0x58f

.field public static final BACKGROUND_STARTED_IND:I = 0x57c

.field public static final BACKGROUND_STOPPED_IND:I = 0x57e

.field public static final CAMERA_BRIGHTNESS_CHANGED_IND:I = 0x584

.field public static final CAMERA_SELECTED_IND:I = 0x580

.field public static final CAMERA_ZOOM_CHANGED_IND:I = 0x582

.field public static final CAPTURED_IND:I = 0x586

.field public static final CAPTURE_CMD:I = 0x585

.field public static final CHANGE_CAMERA_BRIGHTNESS_CMD:I = 0x583

.field public static final CHANGE_CAMERA_ZOOM_CMD:I = 0x581

.field public static final CHANGE_ORIENTATION_CMD:I = 0x59a

.field public static final CHANGE_VIEW_SIZE_CMD:I = 0x594

.field public static final DISPLAY_SWAPPED_IND:I = 0x597

.field public static final DISPLAY_UPDATED_IND:I = 0x593

.field public static final FARFRAME_IND:I = 0x57a

.field public static final IMS_MEDIA_MSG_REASON:I = 0x12c

.field public static final IMS_MSG_BASE_MEDIA:I = 0x578

.field public static final MEDIA_STARTED_IND:I = 0x591

.field public static final ONSCREEN_DEBUG_INFO_VIDEO:I = 0x59c

.field public static final ORIENTATION_CHANGED_IND:I = 0x59b

.field public static final RECODING_STARTED_IND:I = 0x588

.field public static final RECODING_STOPPED_IND:I = 0x58a

.field public static final SELECT_CAMERA_CMD:I = 0x57f

.field public static final SETSURFACE_CMD:I = 0x579

.field public static final START_ALTERNATE_IMAGE_CMD:I = 0x58b

.field public static final START_BACKGROUND_CMD:I = 0x57b

.field public static final START_RECODING_CMD:I = 0x587

.field public static final STOP_ALTERNATE_IMAGE_CMD:I = 0x58d

.field public static final STOP_BACKGROUND_CMD:I = 0x57d

.field public static final STOP_RECODING_CMD:I = 0x589

.field public static final SWAP_DISPLAY_CMD:I = 0x596

.field public static final UPDATE_DISPLAY_CMD:I = 0x592

.field public static final VIEW_SIZE_CHANGED_IND:I = 0x595


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 3
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
