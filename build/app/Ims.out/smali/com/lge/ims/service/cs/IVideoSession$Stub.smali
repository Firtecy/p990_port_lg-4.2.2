.class public abstract Lcom/lge/ims/service/cs/IVideoSession$Stub;
.super Landroid/os/Binder;
.source "IVideoSession.java"

# interfaces
.implements Lcom/lge/ims/service/cs/IVideoSession;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/cs/IVideoSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/cs/IVideoSession$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.lge.ims.service.cs.IVideoSession"

.field static final TRANSACTION_accept:I = 0x5

.field static final TRANSACTION_changeViewSize:I = 0xc

.field static final TRANSACTION_changeViewSizeEx:I = 0xd

.field static final TRANSACTION_getMediaQuality:I = 0xb

.field static final TRANSACTION_getSessionType:I = 0x2

.field static final TRANSACTION_reject:I = 0x6

.field static final TRANSACTION_setListener:I = 0x1

.field static final TRANSACTION_start:I = 0x3

.field static final TRANSACTION_startMediaRecord:I = 0x7

.field static final TRANSACTION_stop:I = 0x4

.field static final TRANSACTION_stopMediaRecord:I = 0x8

.field static final TRANSACTION_swapCamera:I = 0x9

.field static final TRANSACTION_updateDiaplay:I = 0xa


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 15
    const-string v0, "com.lge.ims.service.cs.IVideoSession"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/lge/ims/service/cs/IVideoSession$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/cs/IVideoSession;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 23
    if-nez p0, :cond_4

    #@2
    .line 24
    const/4 v0, 0x0

    #@3
    .line 30
    :goto_3
    return-object v0

    #@4
    .line 26
    :cond_4
    const-string v1, "com.lge.ims.service.cs.IVideoSession"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 27
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/lge/ims/service/cs/IVideoSession;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 28
    check-cast v0, Lcom/lge/ims/service/cs/IVideoSession;

    #@12
    goto :goto_3

    #@13
    .line 30
    :cond_13
    new-instance v0, Lcom/lge/ims/service/cs/IVideoSession$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/lge/ims/service/cs/IVideoSession$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 9
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 38
    sparse-switch p1, :sswitch_data_108

    #@4
    .line 162
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v3

    #@8
    :goto_8
    return v3

    #@9
    .line 42
    :sswitch_9
    const-string v2, "com.lge.ims.service.cs.IVideoSession"

    #@b
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 47
    :sswitch_f
    const-string v2, "com.lge.ims.service.cs.IVideoSession"

    #@11
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@17
    move-result-object v2

    #@18
    invoke-static {v2}, Lcom/lge/ims/service/cs/IVideoSessionListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/cs/IVideoSessionListener;

    #@1b
    move-result-object v0

    #@1c
    .line 50
    .local v0, _arg0:Lcom/lge/ims/service/cs/IVideoSessionListener;
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/cs/IVideoSession$Stub;->setListener(Lcom/lge/ims/service/cs/IVideoSessionListener;)Z

    #@1f
    move-result v1

    #@20
    .line 51
    .local v1, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@23
    .line 52
    if-eqz v1, :cond_2a

    #@25
    move v2, v3

    #@26
    :goto_26
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@29
    goto :goto_8

    #@2a
    :cond_2a
    const/4 v2, 0x0

    #@2b
    goto :goto_26

    #@2c
    .line 57
    .end local v0           #_arg0:Lcom/lge/ims/service/cs/IVideoSessionListener;
    .end local v1           #_result:Z
    :sswitch_2c
    const-string v2, "com.lge.ims.service.cs.IVideoSession"

    #@2e
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@31
    .line 58
    invoke-virtual {p0}, Lcom/lge/ims/service/cs/IVideoSession$Stub;->getSessionType()I

    #@34
    move-result v1

    #@35
    .line 59
    .local v1, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@38
    .line 60
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@3b
    goto :goto_8

    #@3c
    .line 65
    .end local v1           #_result:I
    :sswitch_3c
    const-string v2, "com.lge.ims.service.cs.IVideoSession"

    #@3e
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@41
    .line 67
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@44
    move-result-object v0

    #@45
    .line 68
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/cs/IVideoSession$Stub;->start(Ljava/lang/String;)I

    #@48
    move-result v1

    #@49
    .line 69
    .restart local v1       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4c
    .line 70
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@4f
    goto :goto_8

    #@50
    .line 75
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_result:I
    :sswitch_50
    const-string v2, "com.lge.ims.service.cs.IVideoSession"

    #@52
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@55
    .line 76
    invoke-virtual {p0}, Lcom/lge/ims/service/cs/IVideoSession$Stub;->stop()I

    #@58
    move-result v1

    #@59
    .line 77
    .restart local v1       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5c
    .line 78
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@5f
    goto :goto_8

    #@60
    .line 83
    .end local v1           #_result:I
    :sswitch_60
    const-string v2, "com.lge.ims.service.cs.IVideoSession"

    #@62
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@65
    .line 84
    invoke-virtual {p0}, Lcom/lge/ims/service/cs/IVideoSession$Stub;->accept()I

    #@68
    move-result v1

    #@69
    .line 85
    .restart local v1       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@6c
    .line 86
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@6f
    goto :goto_8

    #@70
    .line 91
    .end local v1           #_result:I
    :sswitch_70
    const-string v2, "com.lge.ims.service.cs.IVideoSession"

    #@72
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@75
    .line 92
    invoke-virtual {p0}, Lcom/lge/ims/service/cs/IVideoSession$Stub;->reject()I

    #@78
    move-result v1

    #@79
    .line 93
    .restart local v1       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@7c
    .line 94
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@7f
    goto :goto_8

    #@80
    .line 99
    .end local v1           #_result:I
    :sswitch_80
    const-string v2, "com.lge.ims.service.cs.IVideoSession"

    #@82
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@85
    .line 101
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@88
    move-result-object v0

    #@89
    .line 102
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/cs/IVideoSession$Stub;->startMediaRecord(Ljava/lang/String;)I

    #@8c
    move-result v1

    #@8d
    .line 103
    .restart local v1       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@90
    .line 104
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@93
    goto/16 :goto_8

    #@95
    .line 109
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_result:I
    :sswitch_95
    const-string v2, "com.lge.ims.service.cs.IVideoSession"

    #@97
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9a
    .line 110
    invoke-virtual {p0}, Lcom/lge/ims/service/cs/IVideoSession$Stub;->stopMediaRecord()I

    #@9d
    move-result v1

    #@9e
    .line 111
    .restart local v1       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@a1
    .line 112
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@a4
    goto/16 :goto_8

    #@a6
    .line 117
    .end local v1           #_result:I
    :sswitch_a6
    const-string v2, "com.lge.ims.service.cs.IVideoSession"

    #@a8
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ab
    .line 119
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@ae
    move-result v0

    #@af
    .line 120
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/cs/IVideoSession$Stub;->swapCamera(I)I

    #@b2
    move-result v1

    #@b3
    .line 121
    .restart local v1       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@b6
    .line 122
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@b9
    goto/16 :goto_8

    #@bb
    .line 127
    .end local v0           #_arg0:I
    .end local v1           #_result:I
    :sswitch_bb
    const-string v2, "com.lge.ims.service.cs.IVideoSession"

    #@bd
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c0
    .line 128
    invoke-virtual {p0}, Lcom/lge/ims/service/cs/IVideoSession$Stub;->updateDiaplay()I

    #@c3
    move-result v1

    #@c4
    .line 129
    .restart local v1       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@c7
    .line 130
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@ca
    goto/16 :goto_8

    #@cc
    .line 135
    .end local v1           #_result:I
    :sswitch_cc
    const-string v2, "com.lge.ims.service.cs.IVideoSession"

    #@ce
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d1
    .line 136
    invoke-virtual {p0}, Lcom/lge/ims/service/cs/IVideoSession$Stub;->getMediaQuality()I

    #@d4
    move-result v1

    #@d5
    .line 137
    .restart local v1       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@d8
    .line 138
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@db
    goto/16 :goto_8

    #@dd
    .line 143
    .end local v1           #_result:I
    :sswitch_dd
    const-string v2, "com.lge.ims.service.cs.IVideoSession"

    #@df
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@e2
    .line 145
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@e5
    move-result v0

    #@e6
    .line 146
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/cs/IVideoSession$Stub;->changeViewSize(I)I

    #@e9
    move-result v1

    #@ea
    .line 147
    .restart local v1       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@ed
    .line 148
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@f0
    goto/16 :goto_8

    #@f2
    .line 153
    .end local v0           #_arg0:I
    .end local v1           #_result:I
    :sswitch_f2
    const-string v2, "com.lge.ims.service.cs.IVideoSession"

    #@f4
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f7
    .line 155
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@fa
    move-result v0

    #@fb
    .line 156
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/cs/IVideoSession$Stub;->changeViewSizeEx(I)I

    #@fe
    move-result v1

    #@ff
    .line 157
    .restart local v1       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@102
    .line 158
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@105
    goto/16 :goto_8

    #@107
    .line 38
    nop

    #@108
    :sswitch_data_108
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_2c
        0x3 -> :sswitch_3c
        0x4 -> :sswitch_50
        0x5 -> :sswitch_60
        0x6 -> :sswitch_70
        0x7 -> :sswitch_80
        0x8 -> :sswitch_95
        0x9 -> :sswitch_a6
        0xa -> :sswitch_bb
        0xb -> :sswitch_cc
        0xc -> :sswitch_dd
        0xd -> :sswitch_f2
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
