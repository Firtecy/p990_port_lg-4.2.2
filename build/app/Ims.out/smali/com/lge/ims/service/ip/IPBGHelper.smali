.class public Lcom/lge/ims/service/ip/IPBGHelper;
.super Ljava/lang/Object;
.source "IPBGHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/ip/IPBGHelper$1;,
        Lcom/lge/ims/service/ip/IPBGHelper$BatteryMonitorHandler;
    }
.end annotation


# static fields
.field private static final EVENT_BATTERY_LOW:I = 0x3eb

.field private static final EVENT_BATTERY_OKAY:I = 0x3ec


# instance fields
.field private bBGServiceOn:Z

.field private bIsScreenOn:Z

.field private mBatteryMonitorHandler:Lcom/lge/ims/service/ip/IPBGHelper$BatteryMonitorHandler;

.field public mLowBattery:Z

.field private objContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 11
    .parameter "_objContext"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/4 v7, 0x1

    #@2
    const/4 v6, 0x0

    #@3
    .line 50
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 32
    iput-boolean v8, p0, Lcom/lge/ims/service/ip/IPBGHelper;->mLowBattery:Z

    #@8
    .line 34
    iput-object v6, p0, Lcom/lge/ims/service/ip/IPBGHelper;->mBatteryMonitorHandler:Lcom/lge/ims/service/ip/IPBGHelper$BatteryMonitorHandler;

    #@a
    .line 51
    if-nez p1, :cond_12

    #@c
    .line 52
    const-string v4, "--- parameter is invalid"

    #@e
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@11
    .line 90
    :cond_11
    :goto_11
    return-void

    #@12
    .line 56
    :cond_12
    iput-object p1, p0, Lcom/lge/ims/service/ip/IPBGHelper;->objContext:Landroid/content/Context;

    #@14
    .line 59
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPBGHelper;->objContext:Landroid/content/Context;

    #@16
    const-string v5, "power"

    #@18
    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1b
    move-result-object v2

    #@1c
    check-cast v2, Landroid/os/PowerManager;

    #@1e
    .line 60
    .local v2, objPowerManger:Landroid/os/PowerManager;
    invoke-virtual {v2}, Landroid/os/PowerManager;->isScreenOn()Z

    #@21
    move-result v4

    #@22
    iput-boolean v4, p0, Lcom/lge/ims/service/ip/IPBGHelper;->bIsScreenOn:Z

    #@24
    .line 63
    const/4 v3, 0x0

    #@25
    .line 64
    .local v3, stickyIntent:Landroid/content/Intent;
    new-instance v4, Landroid/content/IntentFilter;

    #@27
    const-string v5, "android.intent.action.BATTERY_CHANGED"

    #@29
    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@2c
    invoke-virtual {p1, v6, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@2f
    move-result-object v3

    #@30
    .line 66
    if-eqz v3, :cond_59

    #@32
    .line 67
    const-string v4, "level"

    #@34
    const/4 v5, -0x1

    #@35
    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@38
    move-result v1

    #@39
    .line 68
    .local v1, level:I
    new-instance v4, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v5, "[IP]battery level : "

    #@40
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v4

    #@44
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@47
    move-result-object v4

    #@48
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v4

    #@4c
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@4f
    .line 69
    const/4 v4, 0x5

    #@50
    if-gt v1, v4, :cond_59

    #@52
    .line 70
    iput-boolean v7, p0, Lcom/lge/ims/service/ip/IPBGHelper;->mLowBattery:Z

    #@54
    .line 72
    const-string v4, "[IP]low battery"

    #@56
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@59
    .line 75
    .end local v1           #level:I
    :cond_59
    iget-boolean v4, p0, Lcom/lge/ims/service/ip/IPBGHelper;->bIsScreenOn:Z

    #@5b
    if-ne v4, v7, :cond_84

    #@5d
    iget-boolean v4, p0, Lcom/lge/ims/service/ip/IPBGHelper;->mLowBattery:Z

    #@5f
    if-nez v4, :cond_84

    #@61
    .line 76
    iput-boolean v7, p0, Lcom/lge/ims/service/ip/IPBGHelper;->bBGServiceOn:Z

    #@63
    .line 77
    const-string v4, "[IP]bBGServiceOn is true"

    #@65
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@68
    .line 83
    :goto_68
    new-instance v4, Lcom/lge/ims/service/ip/IPBGHelper$BatteryMonitorHandler;

    #@6a
    invoke-direct {v4, p0, v6}, Lcom/lge/ims/service/ip/IPBGHelper$BatteryMonitorHandler;-><init>(Lcom/lge/ims/service/ip/IPBGHelper;Lcom/lge/ims/service/ip/IPBGHelper$1;)V

    #@6d
    iput-object v4, p0, Lcom/lge/ims/service/ip/IPBGHelper;->mBatteryMonitorHandler:Lcom/lge/ims/service/ip/IPBGHelper$BatteryMonitorHandler;

    #@6f
    .line 84
    invoke-static {}, Lcom/lge/ims/BatteryStateTracker;->getInstance()Lcom/lge/ims/BatteryStateTracker;

    #@72
    move-result-object v0

    #@73
    .line 86
    .local v0, bsTracker:Lcom/lge/ims/BatteryStateTracker;
    if-eqz v0, :cond_11

    #@75
    .line 87
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPBGHelper;->mBatteryMonitorHandler:Lcom/lge/ims/service/ip/IPBGHelper$BatteryMonitorHandler;

    #@77
    const/16 v5, 0x3eb

    #@79
    invoke-virtual {v0, v4, v5, v6}, Lcom/lge/ims/BatteryStateTracker;->registerForBatteryLow(Landroid/os/Handler;ILjava/lang/Object;)V

    #@7c
    .line 88
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPBGHelper;->mBatteryMonitorHandler:Lcom/lge/ims/service/ip/IPBGHelper$BatteryMonitorHandler;

    #@7e
    const/16 v5, 0x3ec

    #@80
    invoke-virtual {v0, v4, v5, v6}, Lcom/lge/ims/BatteryStateTracker;->registerForBatteryOkay(Landroid/os/Handler;ILjava/lang/Object;)V

    #@83
    goto :goto_11

    #@84
    .line 79
    .end local v0           #bsTracker:Lcom/lge/ims/BatteryStateTracker;
    :cond_84
    iput-boolean v8, p0, Lcom/lge/ims/service/ip/IPBGHelper;->bBGServiceOn:Z

    #@86
    .line 80
    const-string v4, "[IP]bBGServiceOn is false"

    #@88
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@8b
    goto :goto_68
.end method

.method private CheckBGServiceState()V
    .registers 5

    #@0
    .prologue
    .line 189
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@3
    move-result-object v1

    #@4
    .line 190
    .local v1, objIPAgent:Lcom/lge/ims/service/ip/IPAgent;
    if-nez v1, :cond_c

    #@6
    .line 191
    const-string v2, "objIPAgent is null"

    #@8
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@b
    .line 217
    :goto_b
    return-void

    #@c
    .line 194
    :cond_c
    const/4 v0, 0x0

    #@d
    .line 196
    .local v0, bBGCurrentServiceOn:Z
    new-instance v2, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v3, "[IP]CheckBGServiceState : bIsScreenOn [ "

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    iget-boolean v3, p0, Lcom/lge/ims/service/ip/IPBGHelper;->bIsScreenOn:Z

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    const-string v3, " ], mLowBattery [ "

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    iget-boolean v3, p0, Lcom/lge/ims/service/ip/IPBGHelper;->mLowBattery:Z

    #@26
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@29
    move-result-object v2

    #@2a
    const-string v3, " ], bBGServiceOn [ "

    #@2c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    iget-boolean v3, p0, Lcom/lge/ims/service/ip/IPBGHelper;->bBGServiceOn:Z

    #@32
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@35
    move-result-object v2

    #@36
    const-string v3, " ]"

    #@38
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v2

    #@3c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v2

    #@40
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@43
    .line 197
    iget-boolean v2, p0, Lcom/lge/ims/service/ip/IPBGHelper;->bIsScreenOn:Z

    #@45
    const/4 v3, 0x1

    #@46
    if-ne v2, v3, :cond_7d

    #@48
    iget-boolean v2, p0, Lcom/lge/ims/service/ip/IPBGHelper;->mLowBattery:Z

    #@4a
    if-nez v2, :cond_7d

    #@4c
    .line 198
    const/4 v0, 0x1

    #@4d
    .line 203
    :goto_4d
    iget-boolean v2, p0, Lcom/lge/ims/service/ip/IPBGHelper;->bBGServiceOn:Z

    #@4f
    if-eq v0, v2, :cond_62

    #@51
    .line 204
    if-eqz v0, :cond_7f

    #@53
    .line 205
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@56
    move-result-object v2

    #@57
    if-eqz v2, :cond_62

    #@59
    .line 206
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@5c
    move-result-object v2

    #@5d
    const/16 v3, 0xf

    #@5f
    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@62
    .line 215
    :cond_62
    :goto_62
    iput-boolean v0, p0, Lcom/lge/ims/service/ip/IPBGHelper;->bBGServiceOn:Z

    #@64
    .line 216
    new-instance v2, Ljava/lang/StringBuilder;

    #@66
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@69
    const-string v3, "[IP]CheckBGServiceState : bBGServiceOn - "

    #@6b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v2

    #@6f
    iget-boolean v3, p0, Lcom/lge/ims/service/ip/IPBGHelper;->bBGServiceOn:Z

    #@71
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@74
    move-result-object v2

    #@75
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@78
    move-result-object v2

    #@79
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@7c
    goto :goto_b

    #@7d
    .line 200
    :cond_7d
    const/4 v0, 0x0

    #@7e
    goto :goto_4d

    #@7f
    .line 209
    :cond_7f
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@82
    move-result-object v2

    #@83
    if-eqz v2, :cond_62

    #@85
    .line 210
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@88
    move-result-object v2

    #@89
    const/16 v3, 0x10

    #@8b
    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@8e
    goto :goto_62
.end method

.method static synthetic access$100(Lcom/lge/ims/service/ip/IPBGHelper;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/lge/ims/service/ip/IPBGHelper;->bIsScreenOn:Z

    #@2
    return v0
.end method

.method static synthetic access$202(Lcom/lge/ims/service/ip/IPBGHelper;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 28
    iput-boolean p1, p0, Lcom/lge/ims/service/ip/IPBGHelper;->bBGServiceOn:Z

    #@2
    return p1
.end method


# virtual methods
.method public GetBGServiceState()Z
    .registers 2

    #@0
    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/lge/ims/service/ip/IPBGHelper;->bBGServiceOn:Z

    #@2
    return v0
.end method

.method public SetScreenState(Z)V
    .registers 2
    .parameter "_bIsScreenOn"

    #@0
    .prologue
    .line 160
    iput-boolean p1, p0, Lcom/lge/ims/service/ip/IPBGHelper;->bIsScreenOn:Z

    #@2
    .line 161
    invoke-direct {p0}, Lcom/lge/ims/service/ip/IPBGHelper;->CheckBGServiceState()V

    #@5
    .line 162
    return-void
.end method
