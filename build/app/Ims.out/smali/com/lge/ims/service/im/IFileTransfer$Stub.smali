.class public abstract Lcom/lge/ims/service/im/IFileTransfer$Stub;
.super Landroid/os/Binder;
.source "IFileTransfer.java"

# interfaces
.implements Lcom/lge/ims/service/im/IFileTransfer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/im/IFileTransfer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/im/IFileTransfer$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.lge.ims.service.im.IFileTransfer"

.field static final TRANSACTION_accept:I = 0x2

.field static final TRANSACTION_cancel:I = 0x4

.field static final TRANSACTION_download:I = 0x5

.field static final TRANSACTION_reject:I = 0x3

.field static final TRANSACTION_sendFile:I = 0x1

.field static final TRANSACTION_setListener:I = 0x6


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 15
    const-string v0, "com.lge.ims.service.im.IFileTransfer"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/lge/ims/service/im/IFileTransfer$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/im/IFileTransfer;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 23
    if-nez p0, :cond_4

    #@2
    .line 24
    const/4 v0, 0x0

    #@3
    .line 30
    :goto_3
    return-object v0

    #@4
    .line 26
    :cond_4
    const-string v1, "com.lge.ims.service.im.IFileTransfer"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 27
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/lge/ims/service/im/IFileTransfer;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 28
    check-cast v0, Lcom/lge/ims/service/im/IFileTransfer;

    #@12
    goto :goto_3

    #@13
    .line 30
    :cond_13
    new-instance v0, Lcom/lge/ims/service/im/IFileTransfer$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/lge/ims/service/im/IFileTransfer$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 12
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 38
    sparse-switch p1, :sswitch_data_94

    #@4
    .line 111
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v0

    #@8
    :goto_8
    return v0

    #@9
    .line 42
    :sswitch_9
    const-string v0, "com.lge.ims.service.im.IFileTransfer"

    #@b
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    move v0, v6

    #@f
    .line 43
    goto :goto_8

    #@10
    .line 47
    :sswitch_10
    const-string v0, "com.lge.ims.service.im.IFileTransfer"

    #@12
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@18
    move-result v0

    #@19
    if-eqz v0, :cond_2f

    #@1b
    .line 50
    sget-object v0, Lcom/lge/ims/service/im/IMFileInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1d
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@20
    move-result-object v1

    #@21
    check-cast v1, Lcom/lge/ims/service/im/IMFileInfo;

    #@23
    .line 56
    .local v1, _arg0:Lcom/lge/ims/service/im/IMFileInfo;
    :goto_23
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    .line 57
    .local v2, _arg1:Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/lge/ims/service/im/IFileTransfer$Stub;->sendFile(Lcom/lge/ims/service/im/IMFileInfo;Ljava/lang/String;)V

    #@2a
    .line 58
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2d
    move v0, v6

    #@2e
    .line 59
    goto :goto_8

    #@2f
    .line 53
    .end local v1           #_arg0:Lcom/lge/ims/service/im/IMFileInfo;
    .end local v2           #_arg1:Ljava/lang/String;
    :cond_2f
    const/4 v1, 0x0

    #@30
    .restart local v1       #_arg0:Lcom/lge/ims/service/im/IMFileInfo;
    goto :goto_23

    #@31
    .line 63
    .end local v1           #_arg0:Lcom/lge/ims/service/im/IMFileInfo;
    :sswitch_31
    const-string v0, "com.lge.ims.service.im.IFileTransfer"

    #@33
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@36
    .line 65
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@39
    move-result-object v1

    #@3a
    .line 66
    .local v1, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/lge/ims/service/im/IFileTransfer$Stub;->accept(Ljava/lang/String;)V

    #@3d
    .line 67
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@40
    move v0, v6

    #@41
    .line 68
    goto :goto_8

    #@42
    .line 72
    .end local v1           #_arg0:Ljava/lang/String;
    :sswitch_42
    const-string v0, "com.lge.ims.service.im.IFileTransfer"

    #@44
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@47
    .line 73
    invoke-virtual {p0}, Lcom/lge/ims/service/im/IFileTransfer$Stub;->reject()V

    #@4a
    .line 74
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4d
    move v0, v6

    #@4e
    .line 75
    goto :goto_8

    #@4f
    .line 79
    :sswitch_4f
    const-string v0, "com.lge.ims.service.im.IFileTransfer"

    #@51
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@54
    .line 80
    invoke-virtual {p0}, Lcom/lge/ims/service/im/IFileTransfer$Stub;->cancel()V

    #@57
    .line 81
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5a
    move v0, v6

    #@5b
    .line 82
    goto :goto_8

    #@5c
    .line 86
    :sswitch_5c
    const-string v0, "com.lge.ims.service.im.IFileTransfer"

    #@5e
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@61
    .line 88
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@64
    move-result-object v1

    #@65
    .line 90
    .restart local v1       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@68
    move-result-object v2

    #@69
    .line 92
    .restart local v2       #_arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@6c
    move-result-object v3

    #@6d
    .line 94
    .local v3, _arg2:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@70
    move-result v4

    #@71
    .line 96
    .local v4, _arg3:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@74
    move-result-object v5

    #@75
    .local v5, _arg4:Ljava/lang/String;
    move-object v0, p0

    #@76
    .line 97
    invoke-virtual/range {v0 .. v5}, Lcom/lge/ims/service/im/IFileTransfer$Stub;->download(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    #@79
    .line 98
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@7c
    move v0, v6

    #@7d
    .line 99
    goto :goto_8

    #@7e
    .line 103
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v3           #_arg2:Ljava/lang/String;
    .end local v4           #_arg3:I
    .end local v5           #_arg4:Ljava/lang/String;
    :sswitch_7e
    const-string v0, "com.lge.ims.service.im.IFileTransfer"

    #@80
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@83
    .line 105
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@86
    move-result-object v0

    #@87
    invoke-static {v0}, Lcom/lge/ims/service/im/IFileTransferListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/im/IFileTransferListener;

    #@8a
    move-result-object v1

    #@8b
    .line 106
    .local v1, _arg0:Lcom/lge/ims/service/im/IFileTransferListener;
    invoke-virtual {p0, v1}, Lcom/lge/ims/service/im/IFileTransfer$Stub;->setListener(Lcom/lge/ims/service/im/IFileTransferListener;)V

    #@8e
    .line 107
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@91
    move v0, v6

    #@92
    .line 108
    goto/16 :goto_8

    #@94
    .line 38
    :sswitch_data_94
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_31
        0x3 -> :sswitch_42
        0x4 -> :sswitch_4f
        0x5 -> :sswitch_5c
        0x6 -> :sswitch_7e
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
