.class final Lcom/lge/ims/service/ac/ACService$ACServiceHandler;
.super Landroid/os/Handler;
.source "ACService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/ac/ACService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ACServiceHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/service/ac/ACService;


# direct methods
.method private constructor <init>(Lcom/lge/ims/service/ac/ACService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 915
    iput-object p1, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/lge/ims/service/ac/ACService;Lcom/lge/ims/service/ac/ACService$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 915
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;-><init>(Lcom/lge/ims/service/ac/ACService;)V

    #@3
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 16
    .parameter "msg"

    #@0
    .prologue
    .line 919
    if-nez p1, :cond_3

    #@2
    .line 1130
    :cond_2
    :goto_2
    return-void

    #@3
    .line 923
    :cond_3
    const-string v9, "ACService"

    #@5
    new-instance v10, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v11, "ACServiceHandler :: handleMessage - "

    #@c
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v10

    #@10
    iget v11, p1, Landroid/os/Message;->what:I

    #@12
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v10

    #@16
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v10

    #@1a
    invoke-static {v9, v10}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@1d
    .line 925
    iget v9, p1, Landroid/os/Message;->what:I

    #@1f
    sparse-switch v9, :sswitch_data_3f8

    #@22
    .line 1127
    const-string v9, "ACService"

    #@24
    new-instance v10, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v11, "Unhandled Message :: "

    #@2b
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v10

    #@2f
    iget v11, p1, Landroid/os/Message;->what:I

    #@31
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v10

    #@35
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v10

    #@39
    invoke-static {v9, v10}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@3c
    goto :goto_2

    #@3d
    .line 928
    :sswitch_3d
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@3f
    check-cast v1, Landroid/os/AsyncResult;

    #@41
    .line 930
    .local v1, ar:Landroid/os/AsyncResult;
    if-eqz v1, :cond_2

    #@43
    .line 934
    iget-object v2, v1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@45
    check-cast v2, Ljava/lang/Integer;

    #@47
    .line 936
    .local v2, dataState:Ljava/lang/Integer;
    if-eqz v2, :cond_2

    #@49
    .line 940
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@4c
    move-result v9

    #@4d
    invoke-static {v9}, Lcom/lge/ims/DataConnectionManager;->getApnTypeFromDataStateChanged(I)I

    #@50
    move-result v0

    #@51
    .line 941
    .local v0, apnType:I
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@54
    move-result v9

    #@55
    invoke-static {v9}, Lcom/lge/ims/DataConnectionManager;->getDataStateFromDataStateChanged(I)I

    #@58
    move-result v7

    #@59
    .line 943
    .local v7, state:I
    const-string v9, "ACService"

    #@5b
    new-instance v10, Ljava/lang/StringBuilder;

    #@5d
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@60
    const-string v11, "DataStateChanged :: apnType="

    #@62
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v10

    #@66
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@69
    move-result-object v10

    #@6a
    const-string v11, ", state="

    #@6c
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v10

    #@70
    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@73
    move-result-object v10

    #@74
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@77
    move-result-object v10

    #@78
    invoke-static {v9, v10}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@7b
    .line 945
    const/4 v9, 0x2

    #@7c
    if-ne v0, v9, :cond_91

    #@7e
    .line 946
    invoke-static {}, Lcom/lge/ims/DataConnectionManager;->getInstance()Lcom/lge/ims/DataConnectionManager;

    #@81
    move-result-object v9

    #@82
    invoke-virtual {v9}, Lcom/lge/ims/DataConnectionManager;->isMultipleApnSupported()Z

    #@85
    move-result v9

    #@86
    if-nez v9, :cond_2

    #@88
    .line 947
    if-nez v7, :cond_2

    #@8a
    .line 948
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@8c
    invoke-static {v9}, Lcom/lge/ims/service/ac/ACService;->access$000(Lcom/lge/ims/service/ac/ACService;)V

    #@8f
    goto/16 :goto_2

    #@91
    .line 951
    :cond_91
    const/4 v9, 0x1

    #@92
    if-ne v0, v9, :cond_2

    #@94
    .line 952
    if-nez v7, :cond_2

    #@96
    .line 953
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@98
    invoke-static {v9}, Lcom/lge/ims/service/ac/ACService;->access$000(Lcom/lge/ims/service/ac/ACService;)V

    #@9b
    goto/16 :goto_2

    #@9d
    .line 959
    .end local v0           #apnType:I
    .end local v1           #ar:Landroid/os/AsyncResult;
    .end local v2           #dataState:Ljava/lang/Integer;
    .end local v7           #state:I
    :sswitch_9d
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@9f
    invoke-static {v9}, Lcom/lge/ims/service/ac/ACService;->access$100(Lcom/lge/ims/service/ac/ACService;)V

    #@a2
    goto/16 :goto_2

    #@a4
    .line 963
    :sswitch_a4
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@a6
    const/4 v10, 0x0

    #@a7
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACService;->access$202(Lcom/lge/ims/service/ac/ACService;I)I

    #@aa
    .line 964
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@ac
    const/4 v10, 0x0

    #@ad
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACService;->access$302(Lcom/lge/ims/service/ac/ACService;Z)Z

    #@b0
    .line 965
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@b2
    iget-object v10, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@b4
    invoke-static {v10}, Lcom/lge/ims/service/ac/ACService;->access$500(Lcom/lge/ims/service/ac/ACService;)Ljava/util/ArrayList;

    #@b7
    move-result-object v10

    #@b8
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    #@bb
    move-result v10

    #@bc
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACService;->access$402(Lcom/lge/ims/service/ac/ACService;I)I

    #@bf
    .line 966
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@c1
    const/4 v10, 0x0

    #@c2
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACService;->access$600(Lcom/lge/ims/service/ac/ACService;Z)V

    #@c5
    .line 967
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@c7
    invoke-static {v9}, Lcom/lge/ims/service/ac/ACService;->access$700(Lcom/lge/ims/service/ac/ACService;)I

    #@ca
    move-result v9

    #@cb
    const/16 v10, 0x1f7

    #@cd
    if-eq v9, v10, :cond_dc

    #@cf
    .line 968
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@d1
    const/4 v10, 0x5

    #@d2
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACService;->access$802(Lcom/lge/ims/service/ac/ACService;I)I

    #@d5
    .line 973
    :goto_d5
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@d7
    invoke-static {v9}, Lcom/lge/ims/service/ac/ACService;->access$100(Lcom/lge/ims/service/ac/ACService;)V

    #@da
    goto/16 :goto_2

    #@dc
    .line 970
    :cond_dc
    const-string v9, "ACService"

    #@de
    new-instance v10, Ljava/lang/StringBuilder;

    #@e0
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@e3
    const-string v11, "mResultCodeForJoin="

    #@e5
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e8
    move-result-object v10

    #@e9
    iget-object v11, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@eb
    invoke-static {v11}, Lcom/lge/ims/service/ac/ACService;->access$800(Lcom/lge/ims/service/ac/ACService;)I

    #@ee
    move-result v11

    #@ef
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f2
    move-result-object v10

    #@f3
    const-string v11, ", Retry count for join is not changed.."

    #@f5
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f8
    move-result-object v10

    #@f9
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fc
    move-result-object v10

    #@fd
    invoke-static {v9, v10}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@100
    goto :goto_d5

    #@101
    .line 979
    :sswitch_101
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@103
    invoke-static {v9}, Lcom/lge/ims/service/ac/ACService;->access$300(Lcom/lge/ims/service/ac/ACService;)Z

    #@106
    move-result v9

    #@107
    if-eqz v9, :cond_141

    #@109
    .line 980
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@10b
    const/4 v10, 0x1

    #@10c
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACService;->access$202(Lcom/lge/ims/service/ac/ACService;I)I

    #@10f
    .line 985
    :goto_10f
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@111
    const/4 v10, 0x0

    #@112
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACService;->access$302(Lcom/lge/ims/service/ac/ACService;Z)Z

    #@115
    .line 986
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@117
    iget-object v10, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@119
    invoke-static {v10}, Lcom/lge/ims/service/ac/ACService;->access$500(Lcom/lge/ims/service/ac/ACService;)Ljava/util/ArrayList;

    #@11c
    move-result-object v10

    #@11d
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    #@120
    move-result v10

    #@121
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACService;->access$402(Lcom/lge/ims/service/ac/ACService;I)I

    #@124
    .line 987
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@126
    const/4 v10, 0x0

    #@127
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACService;->access$900(Lcom/lge/ims/service/ac/ACService;Z)V

    #@12a
    .line 988
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@12c
    invoke-static {v9}, Lcom/lge/ims/service/ac/ACService;->access$700(Lcom/lge/ims/service/ac/ACService;)I

    #@12f
    move-result v9

    #@130
    const/16 v10, 0x1f7

    #@132
    if-eq v9, v10, :cond_148

    #@134
    .line 989
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@136
    const/4 v10, 0x5

    #@137
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACService;->access$802(Lcom/lge/ims/service/ac/ACService;I)I

    #@13a
    .line 994
    :goto_13a
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@13c
    invoke-static {v9}, Lcom/lge/ims/service/ac/ACService;->access$100(Lcom/lge/ims/service/ac/ACService;)V

    #@13f
    goto/16 :goto_2

    #@141
    .line 982
    :cond_141
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@143
    const/4 v10, 0x0

    #@144
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACService;->access$202(Lcom/lge/ims/service/ac/ACService;I)I

    #@147
    goto :goto_10f

    #@148
    .line 991
    :cond_148
    const-string v9, "ACService"

    #@14a
    new-instance v10, Ljava/lang/StringBuilder;

    #@14c
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@14f
    const-string v11, "mResultCodeForJoin="

    #@151
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@154
    move-result-object v10

    #@155
    iget-object v11, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@157
    invoke-static {v11}, Lcom/lge/ims/service/ac/ACService;->access$800(Lcom/lge/ims/service/ac/ACService;)I

    #@15a
    move-result v11

    #@15b
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15e
    move-result-object v10

    #@15f
    const-string v11, ", Retry count for join is not changed.."

    #@161
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@164
    move-result-object v10

    #@165
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@168
    move-result-object v10

    #@169
    invoke-static {v9, v10}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@16c
    goto :goto_13a

    #@16d
    .line 998
    :sswitch_16d
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@16f
    const/4 v10, 0x0

    #@170
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACService;->access$202(Lcom/lge/ims/service/ac/ACService;I)I

    #@173
    .line 999
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@175
    const/4 v10, 0x0

    #@176
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACService;->access$302(Lcom/lge/ims/service/ac/ACService;Z)Z

    #@179
    .line 1000
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@17b
    iget-object v10, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@17d
    invoke-static {v10}, Lcom/lge/ims/service/ac/ACService;->access$500(Lcom/lge/ims/service/ac/ACService;)Ljava/util/ArrayList;

    #@180
    move-result-object v10

    #@181
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    #@184
    move-result v10

    #@185
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACService;->access$402(Lcom/lge/ims/service/ac/ACService;I)I

    #@188
    .line 1001
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@18a
    const/4 v10, 0x1

    #@18b
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACService;->access$600(Lcom/lge/ims/service/ac/ACService;Z)V

    #@18e
    .line 1002
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@190
    const/4 v10, 0x1

    #@191
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACService;->access$900(Lcom/lge/ims/service/ac/ACService;Z)V

    #@194
    .line 1003
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@196
    invoke-static {v9}, Lcom/lge/ims/service/ac/ACService;->access$700(Lcom/lge/ims/service/ac/ACService;)I

    #@199
    move-result v9

    #@19a
    const/16 v10, 0x1f7

    #@19c
    if-eq v9, v10, :cond_1ab

    #@19e
    .line 1004
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@1a0
    const/4 v10, 0x5

    #@1a1
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACService;->access$802(Lcom/lge/ims/service/ac/ACService;I)I

    #@1a4
    .line 1009
    :goto_1a4
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@1a6
    invoke-static {v9}, Lcom/lge/ims/service/ac/ACService;->access$100(Lcom/lge/ims/service/ac/ACService;)V

    #@1a9
    goto/16 :goto_2

    #@1ab
    .line 1006
    :cond_1ab
    const-string v9, "ACService"

    #@1ad
    new-instance v10, Ljava/lang/StringBuilder;

    #@1af
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@1b2
    const-string v11, "mResultCodeForJoin="

    #@1b4
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b7
    move-result-object v10

    #@1b8
    iget-object v11, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@1ba
    invoke-static {v11}, Lcom/lge/ims/service/ac/ACService;->access$800(Lcom/lge/ims/service/ac/ACService;)I

    #@1bd
    move-result v11

    #@1be
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c1
    move-result-object v10

    #@1c2
    const-string v11, ", Retry count for join is not changed.."

    #@1c4
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c7
    move-result-object v10

    #@1c8
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1cb
    move-result-object v10

    #@1cc
    invoke-static {v9, v10}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@1cf
    goto :goto_1a4

    #@1d0
    .line 1014
    :sswitch_1d0
    invoke-static {}, Lcom/lge/ims/PhoneStateTracker;->getInstance()Lcom/lge/ims/PhoneStateTracker;

    #@1d3
    move-result-object v5

    #@1d4
    .line 1016
    .local v5, pst:Lcom/lge/ims/PhoneStateTracker;
    if-eqz v5, :cond_2

    #@1d6
    .line 1017
    invoke-virtual {v5}, Lcom/lge/ims/PhoneStateTracker;->getIccState()Ljava/lang/String;

    #@1d9
    move-result-object v3

    #@1da
    .line 1019
    .local v3, iccState:Ljava/lang/String;
    if-eqz v3, :cond_2

    #@1dc
    const-string v9, "LOADED"

    #@1de
    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e1
    move-result v9

    #@1e2
    if-eqz v9, :cond_2

    #@1e4
    .line 1020
    const-string v9, "ACService"

    #@1e6
    const-string v10, "SIM LOADED :: AC configuration access is allowed"

    #@1e8
    invoke-static {v9, v10}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@1eb
    .line 1023
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@1ed
    invoke-static {v9}, Lcom/lge/ims/service/ac/ACService;->access$1000(Lcom/lge/ims/service/ac/ACService;)Landroid/content/Context;

    #@1f0
    move-result-object v9

    #@1f1
    invoke-static {v9}, Lcom/lge/ims/service/ac/ACContentHelper;->deleteUnknownContentForAC(Landroid/content/Context;)V

    #@1f4
    .line 1024
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@1f6
    invoke-static {v9}, Lcom/lge/ims/service/ac/ACService;->access$1000(Lcom/lge/ims/service/ac/ACService;)Landroid/content/Context;

    #@1f9
    move-result-object v9

    #@1fa
    invoke-static {v9}, Lcom/lge/ims/service/ac/ACContentHelper;->deleteUnknownContentWithMSISDNForAC(Landroid/content/Context;)V

    #@1fd
    .line 1026
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@1ff
    const/4 v10, 0x1

    #@200
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACService;->access$1102(Lcom/lge/ims/service/ac/ACService;Z)Z

    #@203
    .line 1027
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@205
    invoke-static {v9}, Lcom/lge/ims/service/ac/ACService;->access$1200(Lcom/lge/ims/service/ac/ACService;)Landroid/os/RegistrantList;

    #@208
    move-result-object v9

    #@209
    invoke-virtual {v9}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@20c
    goto/16 :goto_2

    #@20e
    .line 1035
    .end local v3           #iccState:Ljava/lang/String;
    .end local v5           #pst:Lcom/lge/ims/PhoneStateTracker;
    :sswitch_20e
    invoke-static {}, Lcom/lge/ims/Configuration;->isACOn()Z

    #@211
    move-result v9

    #@212
    if-nez v9, :cond_21d

    #@214
    .line 1036
    const-string v9, "ACService"

    #@216
    const-string v10, "AC is off, configuration retrieval is not allowed on airplane mode changed"

    #@218
    invoke-static {v9, v10}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@21b
    goto/16 :goto_2

    #@21d
    .line 1040
    :cond_21d
    invoke-static {}, Lcom/lge/ims/PhoneStateTracker;->getInstance()Lcom/lge/ims/PhoneStateTracker;

    #@220
    move-result-object v5

    #@221
    .line 1042
    .restart local v5       #pst:Lcom/lge/ims/PhoneStateTracker;
    if-eqz v5, :cond_2

    #@223
    .line 1043
    invoke-virtual {v5}, Lcom/lge/ims/PhoneStateTracker;->isAirplaneMode()Z

    #@226
    move-result v9

    #@227
    if-nez v9, :cond_2

    #@229
    .line 1044
    const-string v9, "ACService"

    #@22b
    const-string v10, "ACS interworking flag is set by the airplane mode"

    #@22d
    invoke-static {v9, v10}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@230
    .line 1045
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@232
    const/4 v10, 0x1

    #@233
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACService;->access$1302(Lcom/lge/ims/service/ac/ACService;Z)Z

    #@236
    goto/16 :goto_2

    #@238
    .line 1052
    .end local v5           #pst:Lcom/lge/ims/PhoneStateTracker;
    :sswitch_238
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@23a
    const/4 v10, 0x2

    #@23b
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACService;->access$202(Lcom/lge/ims/service/ac/ACService;I)I

    #@23e
    .line 1053
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@240
    const/4 v10, 0x0

    #@241
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACService;->access$302(Lcom/lge/ims/service/ac/ACService;Z)Z

    #@244
    .line 1054
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@246
    iget-object v10, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@248
    invoke-static {v10}, Lcom/lge/ims/service/ac/ACService;->access$500(Lcom/lge/ims/service/ac/ACService;)Ljava/util/ArrayList;

    #@24b
    move-result-object v10

    #@24c
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    #@24f
    move-result v10

    #@250
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACService;->access$402(Lcom/lge/ims/service/ac/ACService;I)I

    #@253
    .line 1055
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@255
    const/4 v10, 0x1

    #@256
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACService;->access$600(Lcom/lge/ims/service/ac/ACService;Z)V

    #@259
    .line 1056
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@25b
    const/4 v10, 0x1

    #@25c
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACService;->access$900(Lcom/lge/ims/service/ac/ACService;Z)V

    #@25f
    .line 1057
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@261
    invoke-static {v9}, Lcom/lge/ims/service/ac/ACService;->access$100(Lcom/lge/ims/service/ac/ACService;)V

    #@264
    goto/16 :goto_2

    #@266
    .line 1062
    :sswitch_266
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@268
    const/4 v10, 0x0

    #@269
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACService;->access$202(Lcom/lge/ims/service/ac/ACService;I)I

    #@26c
    .line 1063
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@26e
    const/4 v10, 0x0

    #@26f
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACService;->access$302(Lcom/lge/ims/service/ac/ACService;Z)Z

    #@272
    .line 1064
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@274
    iget-object v10, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@276
    invoke-static {v10}, Lcom/lge/ims/service/ac/ACService;->access$500(Lcom/lge/ims/service/ac/ACService;)Ljava/util/ArrayList;

    #@279
    move-result-object v10

    #@27a
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    #@27d
    move-result v10

    #@27e
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACService;->access$402(Lcom/lge/ims/service/ac/ACService;I)I

    #@281
    .line 1065
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@283
    const/4 v10, 0x0

    #@284
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACService;->access$600(Lcom/lge/ims/service/ac/ACService;Z)V

    #@287
    .line 1067
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@289
    invoke-static {v9}, Lcom/lge/ims/service/ac/ACService;->access$810(Lcom/lge/ims/service/ac/ACService;)I

    #@28c
    .line 1069
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@28e
    const/4 v10, 0x1

    #@28f
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACService;->access$1400(Lcom/lge/ims/service/ac/ACService;I)Z

    #@292
    move-result v6

    #@293
    .line 1071
    .local v6, resultOfVoLTE:Z
    if-eqz v6, :cond_2ab

    #@295
    .line 1072
    const-string v9, "ACService"

    #@297
    const-string v10, "Retrieve the configuration from ACS after 1s"

    #@299
    invoke-static {v9, v10}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@29c
    .line 1073
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@29e
    invoke-static {v9}, Lcom/lge/ims/service/ac/ACService;->access$1500(Lcom/lge/ims/service/ac/ACService;)Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@2a1
    move-result-object v9

    #@2a2
    const/16 v10, 0x66

    #@2a4
    const-wide/16 v11, 0x3e8

    #@2a6
    invoke-virtual {v9, v10, v11, v12}, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->sendEmptyMessageDelayed(IJ)Z

    #@2a9
    goto/16 :goto_2

    #@2ab
    .line 1075
    :cond_2ab
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@2ad
    invoke-static {v9}, Lcom/lge/ims/service/ac/ACService;->access$700(Lcom/lge/ims/service/ac/ACService;)I

    #@2b0
    move-result v9

    #@2b1
    const/16 v10, 0x1f7

    #@2b3
    if-ne v9, v10, :cond_2df

    #@2b5
    .line 1076
    const-string v9, "ACService"

    #@2b7
    const-string v10, "Received 503 response, stop joining the services until power cycle."

    #@2b9
    invoke-static {v9, v10}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@2bc
    .line 1078
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@2be
    invoke-static {v9}, Lcom/lge/ims/service/ac/ACService;->access$1000(Lcom/lge/ims/service/ac/ACService;)Landroid/content/Context;

    #@2c1
    move-result-object v9

    #@2c2
    const v10, 0x7f0603a9

    #@2c5
    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@2c8
    move-result-object v8

    #@2c9
    .line 1079
    .local v8, text:Ljava/lang/String;
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@2cb
    invoke-static {v9}, Lcom/lge/ims/service/ac/ACService;->access$1000(Lcom/lge/ims/service/ac/ACService;)Landroid/content/Context;

    #@2ce
    move-result-object v9

    #@2cf
    const/4 v10, 0x0

    #@2d0
    invoke-static {v9, v8, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@2d3
    move-result-object v9

    #@2d4
    invoke-virtual {v9}, Landroid/widget/Toast;->show()V

    #@2d7
    .line 1081
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@2d9
    const/4 v10, -0x1

    #@2da
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACService;->access$802(Lcom/lge/ims/service/ac/ACService;I)I

    #@2dd
    goto/16 :goto_2

    #@2df
    .line 1083
    .end local v8           #text:Ljava/lang/String;
    :cond_2df
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@2e1
    invoke-static {v9}, Lcom/lge/ims/service/ac/ACService;->access$800(Lcom/lge/ims/service/ac/ACService;)I

    #@2e4
    move-result v9

    #@2e5
    if-lez v9, :cond_31f

    #@2e7
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@2e9
    invoke-static {v9}, Lcom/lge/ims/service/ac/ACService;->access$800(Lcom/lge/ims/service/ac/ACService;)I

    #@2ec
    move-result v9

    #@2ed
    iget-object v10, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@2ef
    invoke-static {v10}, Lcom/lge/ims/service/ac/ACService;->access$500(Lcom/lge/ims/service/ac/ACService;)Ljava/util/ArrayList;

    #@2f2
    move-result-object v10

    #@2f3
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    #@2f6
    move-result v10

    #@2f7
    if-gt v9, v10, :cond_31f

    #@2f9
    .line 1084
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@2fb
    invoke-static {v9}, Lcom/lge/ims/service/ac/ACService;->access$1500(Lcom/lge/ims/service/ac/ACService;)Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@2fe
    move-result-object v10

    #@2ff
    const/16 v11, 0x79

    #@301
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@303
    invoke-static {v9}, Lcom/lge/ims/service/ac/ACService;->access$500(Lcom/lge/ims/service/ac/ACService;)Ljava/util/ArrayList;

    #@306
    move-result-object v9

    #@307
    iget-object v12, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@309
    invoke-static {v12}, Lcom/lge/ims/service/ac/ACService;->access$800(Lcom/lge/ims/service/ac/ACService;)I

    #@30c
    move-result v12

    #@30d
    add-int/lit8 v12, v12, -0x1

    #@30f
    invoke-virtual {v9, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@312
    move-result-object v9

    #@313
    check-cast v9, Ljava/lang/Integer;

    #@315
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    #@318
    move-result v9

    #@319
    int-to-long v12, v9

    #@31a
    invoke-virtual {v10, v11, v12, v13}, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->sendEmptyMessageDelayed(IJ)Z

    #@31d
    goto/16 :goto_2

    #@31f
    .line 1086
    :cond_31f
    const-string v9, "ACService"

    #@321
    const-string v10, "All the retries for joining service is consumed"

    #@323
    invoke-static {v9, v10}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@326
    goto/16 :goto_2

    #@328
    .line 1095
    .end local v6           #resultOfVoLTE:Z
    :sswitch_328
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@32a
    const/4 v10, 0x0

    #@32b
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACService;->access$202(Lcom/lge/ims/service/ac/ACService;I)I

    #@32e
    .line 1096
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@330
    const/4 v10, 0x0

    #@331
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACService;->access$302(Lcom/lge/ims/service/ac/ACService;Z)Z

    #@334
    .line 1097
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@336
    iget-object v10, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@338
    invoke-static {v10}, Lcom/lge/ims/service/ac/ACService;->access$500(Lcom/lge/ims/service/ac/ACService;)Ljava/util/ArrayList;

    #@33b
    move-result-object v10

    #@33c
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    #@33f
    move-result v10

    #@340
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACService;->access$402(Lcom/lge/ims/service/ac/ACService;I)I

    #@343
    .line 1098
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@345
    const/4 v10, 0x0

    #@346
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACService;->access$600(Lcom/lge/ims/service/ac/ACService;Z)V

    #@349
    .line 1100
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@34b
    invoke-static {v9}, Lcom/lge/ims/service/ac/ACService;->access$810(Lcom/lge/ims/service/ac/ACService;)I

    #@34e
    .line 1103
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@350
    iget v10, p1, Landroid/os/Message;->arg1:I

    #@352
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACService;->access$1400(Lcom/lge/ims/service/ac/ACService;I)Z

    #@355
    move-result v9

    #@356
    if-eqz v9, :cond_36e

    #@358
    .line 1104
    const-string v9, "ACService"

    #@35a
    const-string v10, "Retrieve the configuration from ACS after 1s"

    #@35c
    invoke-static {v9, v10}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@35f
    .line 1105
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@361
    invoke-static {v9}, Lcom/lge/ims/service/ac/ACService;->access$1500(Lcom/lge/ims/service/ac/ACService;)Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@364
    move-result-object v9

    #@365
    const/16 v10, 0x66

    #@367
    const-wide/16 v11, 0x3e8

    #@369
    invoke-virtual {v9, v10, v11, v12}, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->sendEmptyMessageDelayed(IJ)Z

    #@36c
    goto/16 :goto_2

    #@36e
    .line 1107
    :cond_36e
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@370
    invoke-static {v9}, Lcom/lge/ims/service/ac/ACService;->access$700(Lcom/lge/ims/service/ac/ACService;)I

    #@373
    move-result v9

    #@374
    const/16 v10, 0x1f7

    #@376
    if-ne v9, v10, :cond_3a2

    #@378
    .line 1108
    const-string v9, "ACService"

    #@37a
    const-string v10, "Received 503 response, stop joining the services until power cycle."

    #@37c
    invoke-static {v9, v10}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@37f
    .line 1110
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@381
    invoke-static {v9}, Lcom/lge/ims/service/ac/ACService;->access$1000(Lcom/lge/ims/service/ac/ACService;)Landroid/content/Context;

    #@384
    move-result-object v9

    #@385
    const v10, 0x7f0603a9

    #@388
    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@38b
    move-result-object v8

    #@38c
    .line 1111
    .restart local v8       #text:Ljava/lang/String;
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@38e
    invoke-static {v9}, Lcom/lge/ims/service/ac/ACService;->access$1000(Lcom/lge/ims/service/ac/ACService;)Landroid/content/Context;

    #@391
    move-result-object v9

    #@392
    const/4 v10, 0x0

    #@393
    invoke-static {v9, v8, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@396
    move-result-object v9

    #@397
    invoke-virtual {v9}, Landroid/widget/Toast;->show()V

    #@39a
    .line 1113
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@39c
    const/4 v10, -0x1

    #@39d
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACService;->access$802(Lcom/lge/ims/service/ac/ACService;I)I

    #@3a0
    goto/16 :goto_2

    #@3a2
    .line 1115
    .end local v8           #text:Ljava/lang/String;
    :cond_3a2
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@3a4
    invoke-static {v9}, Lcom/lge/ims/service/ac/ACService;->access$800(Lcom/lge/ims/service/ac/ACService;)I

    #@3a7
    move-result v9

    #@3a8
    if-lez v9, :cond_3ef

    #@3aa
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@3ac
    invoke-static {v9}, Lcom/lge/ims/service/ac/ACService;->access$800(Lcom/lge/ims/service/ac/ACService;)I

    #@3af
    move-result v9

    #@3b0
    iget-object v10, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@3b2
    invoke-static {v10}, Lcom/lge/ims/service/ac/ACService;->access$500(Lcom/lge/ims/service/ac/ACService;)Ljava/util/ArrayList;

    #@3b5
    move-result-object v10

    #@3b6
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    #@3b9
    move-result v10

    #@3ba
    if-gt v9, v10, :cond_3ef

    #@3bc
    .line 1116
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@3be
    invoke-static {v9}, Lcom/lge/ims/service/ac/ACService;->access$1500(Lcom/lge/ims/service/ac/ACService;)Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@3c1
    move-result-object v9

    #@3c2
    const/16 v10, 0x7a

    #@3c4
    iget v11, p1, Landroid/os/Message;->arg1:I

    #@3c6
    const/4 v12, 0x0

    #@3c7
    invoke-static {v9, v10, v11, v12}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    #@3ca
    move-result-object v4

    #@3cb
    .line 1117
    .local v4, msgForRetry:Landroid/os/Message;
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@3cd
    invoke-static {v9}, Lcom/lge/ims/service/ac/ACService;->access$1500(Lcom/lge/ims/service/ac/ACService;)Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@3d0
    move-result-object v10

    #@3d1
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@3d3
    invoke-static {v9}, Lcom/lge/ims/service/ac/ACService;->access$500(Lcom/lge/ims/service/ac/ACService;)Ljava/util/ArrayList;

    #@3d6
    move-result-object v9

    #@3d7
    iget-object v11, p0, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@3d9
    invoke-static {v11}, Lcom/lge/ims/service/ac/ACService;->access$800(Lcom/lge/ims/service/ac/ACService;)I

    #@3dc
    move-result v11

    #@3dd
    add-int/lit8 v11, v11, -0x1

    #@3df
    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@3e2
    move-result-object v9

    #@3e3
    check-cast v9, Ljava/lang/Integer;

    #@3e5
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    #@3e8
    move-result v9

    #@3e9
    int-to-long v11, v9

    #@3ea
    invoke-virtual {v10, v4, v11, v12}, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@3ed
    goto/16 :goto_2

    #@3ef
    .line 1119
    .end local v4           #msgForRetry:Landroid/os/Message;
    :cond_3ef
    const-string v9, "ACService"

    #@3f1
    const-string v10, "All the retries for joining service is consumed"

    #@3f3
    invoke-static {v9, v10}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@3f6
    goto/16 :goto_2

    #@3f8
    .line 925
    :sswitch_data_3f8
    .sparse-switch
        0x65 -> :sswitch_3d
        0x66 -> :sswitch_9d
        0x67 -> :sswitch_a4
        0x68 -> :sswitch_101
        0x69 -> :sswitch_1d0
        0x6a -> :sswitch_20e
        0x6b -> :sswitch_238
        0x79 -> :sswitch_266
        0x7a -> :sswitch_328
        0xc9 -> :sswitch_16d
    .end sparse-switch
.end method
