.class Lcom/lge/ims/service/cd/CapabilityStateTracker$1;
.super Landroid/content/BroadcastReceiver;
.source "CapabilityStateTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lge/ims/service/cd/CapabilityStateTracker;->startWatchingExternalStorage()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/service/cd/CapabilityStateTracker;


# direct methods
.method constructor <init>(Lcom/lge/ims/service/cd/CapabilityStateTracker;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 102
    iput-object p1, p0, Lcom/lge/ims/service/cd/CapabilityStateTracker$1;->this$0:Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 6
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 107
    const-string v0, "CapService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Storage: "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    const-string v2, ", Action: "

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@2a
    .line 109
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityStateTracker$1;->this$0:Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@2c
    invoke-static {v0}, Lcom/lge/ims/service/cd/CapabilityStateTracker;->access$000(Lcom/lge/ims/service/cd/CapabilityStateTracker;)V

    #@2f
    .line 110
    return-void
.end method
