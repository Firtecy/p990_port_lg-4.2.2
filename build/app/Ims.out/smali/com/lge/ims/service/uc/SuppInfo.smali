.class public Lcom/lge/ims/service/uc/SuppInfo;
.super Ljava/lang/Object;
.source "SuppInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/uc/SuppInfo$SuppService;
    }
.end annotation


# static fields
.field public static final CALLERID_IDENTITY:I = 0x3

.field public static final CALLERID_NETWORK:I = 0x1

.field public static final CALLERID_NONE:I = 0x0

.field public static final CALLERID_RESTICTED:I = 0x2

.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/lge/ims/service/uc/SuppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "LGIMS_SuppInfo"

.field public static final TYPE_CALLERID:I = 0x0

.field public static final TYPE_CDIV_CAUSE:I = 0x5

.field public static final TYPE_CDIV_HISTORY:I = 0x6

.field public static final TYPE_CNAP:I = 0x1

.field public static final TYPE_CNAPEX:I = 0x2

.field public static final TYPE_GTT:I = 0x4

.field public static final TYPE_MMC:I = 0x3


# instance fields
.field public objSuppService:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/lge/ims/service/uc/SuppInfo$SuppService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 203
    new-instance v0, Lcom/lge/ims/service/uc/SuppInfo$1;

    #@2
    invoke-direct {v0}, Lcom/lge/ims/service/uc/SuppInfo$1;-><init>()V

    #@5
    sput-object v0, Lcom/lge/ims/service/uc/SuppInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    .line 42
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 21
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Lcom/lge/ims/service/uc/SuppInfo;->objSuppService:Ljava/util/ArrayList;

    #@a
    .line 44
    const-string v0, "LGIMS_SuppInfo"

    #@c
    const-string v1, "SuppInfo()"

    #@e
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 4
    .parameter "source"

    #@0
    .prologue
    .line 47
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 21
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Lcom/lge/ims/service/uc/SuppInfo;->objSuppService:Ljava/util/ArrayList;

    #@a
    .line 49
    const-string v0, "LGIMS_SuppInfo"

    #@c
    const-string v1, "SuppInfo()"

    #@e
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 50
    invoke-virtual {p0, p1}, Lcom/lge/ims/service/uc/SuppInfo;->readFromParcel(Landroid/os/Parcel;)V

    #@14
    .line 51
    return-void
.end method


# virtual methods
.method public addService_bool(IZ)V
    .registers 7
    .parameter "_type"
    .parameter "_value"

    #@0
    .prologue
    .line 100
    const-string v1, "LGIMS_SuppInfo"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "addService_str() ["

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    iget-object v3, p0, Lcom/lge/ims/service/uc/SuppInfo;->objSuppService:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@12
    move-result v3

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    const-string v3, "]"

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    const-string v3, "["

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    const-string v3, "]"

    #@29
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    const-string v3, "["

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    const-string v3, "]"

    #@39
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v2

    #@3d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v2

    #@41
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 102
    new-instance v0, Lcom/lge/ims/service/uc/SuppInfo$SuppService;

    #@46
    invoke-direct {v0, p0}, Lcom/lge/ims/service/uc/SuppInfo$SuppService;-><init>(Lcom/lge/ims/service/uc/SuppInfo;)V

    #@49
    .line 103
    .local v0, service:Lcom/lge/ims/service/uc/SuppInfo$SuppService;
    iput p1, v0, Lcom/lge/ims/service/uc/SuppInfo$SuppService;->type:I

    #@4b
    .line 104
    const/4 v1, 0x0

    #@4c
    iput-object v1, v0, Lcom/lge/ims/service/uc/SuppInfo$SuppService;->strValue:Ljava/lang/String;

    #@4e
    .line 105
    const/4 v1, 0x0

    #@4f
    iput v1, v0, Lcom/lge/ims/service/uc/SuppInfo$SuppService;->intValue:I

    #@51
    .line 106
    iput-boolean p2, v0, Lcom/lge/ims/service/uc/SuppInfo$SuppService;->boolValue:Z

    #@53
    .line 108
    iget-object v1, p0, Lcom/lge/ims/service/uc/SuppInfo;->objSuppService:Ljava/util/ArrayList;

    #@55
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@58
    .line 110
    return-void
.end method

.method public addService_int(II)V
    .registers 7
    .parameter "_type"
    .parameter "_value"

    #@0
    .prologue
    .line 86
    const-string v1, "LGIMS_SuppInfo"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "addService_int() ["

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    iget-object v3, p0, Lcom/lge/ims/service/uc/SuppInfo;->objSuppService:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@12
    move-result v3

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    const-string v3, "]"

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    const-string v3, "["

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    const-string v3, "]"

    #@29
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    const-string v3, "["

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    const-string v3, "]"

    #@39
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v2

    #@3d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v2

    #@41
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 88
    new-instance v0, Lcom/lge/ims/service/uc/SuppInfo$SuppService;

    #@46
    invoke-direct {v0, p0}, Lcom/lge/ims/service/uc/SuppInfo$SuppService;-><init>(Lcom/lge/ims/service/uc/SuppInfo;)V

    #@49
    .line 89
    .local v0, service:Lcom/lge/ims/service/uc/SuppInfo$SuppService;
    iput p1, v0, Lcom/lge/ims/service/uc/SuppInfo$SuppService;->type:I

    #@4b
    .line 90
    const/4 v1, 0x0

    #@4c
    iput-object v1, v0, Lcom/lge/ims/service/uc/SuppInfo$SuppService;->strValue:Ljava/lang/String;

    #@4e
    .line 91
    iput p2, v0, Lcom/lge/ims/service/uc/SuppInfo$SuppService;->intValue:I

    #@50
    .line 92
    const/4 v1, 0x0

    #@51
    iput-boolean v1, v0, Lcom/lge/ims/service/uc/SuppInfo$SuppService;->boolValue:Z

    #@53
    .line 94
    iget-object v1, p0, Lcom/lge/ims/service/uc/SuppInfo;->objSuppService:Ljava/util/ArrayList;

    #@55
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@58
    .line 96
    return-void
.end method

.method public addService_str(ILjava/lang/String;)V
    .registers 8
    .parameter "_type"
    .parameter "_value"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 72
    const-string v1, "LGIMS_SuppInfo"

    #@3
    new-instance v2, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v3, "addService_str() ["

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    iget-object v3, p0, Lcom/lge/ims/service/uc/SuppInfo;->objSuppService:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@13
    move-result v3

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    const-string v3, "]"

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    const-string v3, "["

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    const-string v3, "]"

    #@2a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v2

    #@2e
    const-string v3, "["

    #@30
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v2

    #@38
    const-string v3, "]"

    #@3a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v2

    #@42
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 74
    new-instance v0, Lcom/lge/ims/service/uc/SuppInfo$SuppService;

    #@47
    invoke-direct {v0, p0}, Lcom/lge/ims/service/uc/SuppInfo$SuppService;-><init>(Lcom/lge/ims/service/uc/SuppInfo;)V

    #@4a
    .line 75
    .local v0, service:Lcom/lge/ims/service/uc/SuppInfo$SuppService;
    iput p1, v0, Lcom/lge/ims/service/uc/SuppInfo$SuppService;->type:I

    #@4c
    .line 76
    iput-object p2, v0, Lcom/lge/ims/service/uc/SuppInfo$SuppService;->strValue:Ljava/lang/String;

    #@4e
    .line 77
    iput v4, v0, Lcom/lge/ims/service/uc/SuppInfo$SuppService;->intValue:I

    #@50
    .line 78
    iput-boolean v4, v0, Lcom/lge/ims/service/uc/SuppInfo$SuppService;->boolValue:Z

    #@52
    .line 80
    iget-object v1, p0, Lcom/lge/ims/service/uc/SuppInfo;->objSuppService:Ljava/util/ArrayList;

    #@54
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@57
    .line 82
    return-void
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 200
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getService(I)Lcom/lge/ims/service/uc/SuppInfo$SuppService;
    .registers 7
    .parameter "type"

    #@0
    .prologue
    .line 138
    const-string v2, "LGIMS_SuppInfo"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "getService : ["

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    const-string v4, "]"

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 140
    const/4 v0, 0x0

    #@1f
    .local v0, index:I
    :goto_1f
    iget-object v2, p0, Lcom/lge/ims/service/uc/SuppInfo;->objSuppService:Ljava/util/ArrayList;

    #@21
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@24
    move-result v2

    #@25
    if-ge v0, v2, :cond_37

    #@27
    .line 142
    iget-object v2, p0, Lcom/lge/ims/service/uc/SuppInfo;->objSuppService:Ljava/util/ArrayList;

    #@29
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2c
    move-result-object v1

    #@2d
    check-cast v1, Lcom/lge/ims/service/uc/SuppInfo$SuppService;

    #@2f
    .line 144
    .local v1, service:Lcom/lge/ims/service/uc/SuppInfo$SuppService;
    iget v2, v1, Lcom/lge/ims/service/uc/SuppInfo$SuppService;->type:I

    #@31
    if-ne v2, p1, :cond_34

    #@33
    .line 149
    .end local v1           #service:Lcom/lge/ims/service/uc/SuppInfo$SuppService;
    :goto_33
    return-object v1

    #@34
    .line 140
    .restart local v1       #service:Lcom/lge/ims/service/uc/SuppInfo$SuppService;
    :cond_34
    add-int/lit8 v0, v0, 0x1

    #@36
    goto :goto_1f

    #@37
    .line 149
    .end local v1           #service:Lcom/lge/ims/service/uc/SuppInfo$SuppService;
    :cond_37
    const/4 v1, 0x0

    #@38
    goto :goto_33
.end method

.method public getServiceSize()I
    .registers 4

    #@0
    .prologue
    .line 114
    const-string v0, "LGIMS_SuppInfo"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "getServiceSize : ["

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-object v2, p0, Lcom/lge/ims/service/uc/SuppInfo;->objSuppService:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@12
    move-result v2

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, "]"

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 115
    iget-object v0, p0, Lcom/lge/ims/service/uc/SuppInfo;->objSuppService:Ljava/util/ArrayList;

    #@26
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@29
    move-result v0

    #@2a
    return v0
.end method

.method public isService(I)Z
    .registers 8
    .parameter "type"

    #@0
    .prologue
    .line 121
    const/4 v0, 0x0

    #@1
    .line 123
    .local v0, bIs:Z
    const/4 v1, 0x0

    #@2
    .local v1, index:I
    :goto_2
    iget-object v3, p0, Lcom/lge/ims/service/uc/SuppInfo;->objSuppService:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v3

    #@8
    if-ge v1, v3, :cond_1a

    #@a
    .line 125
    iget-object v3, p0, Lcom/lge/ims/service/uc/SuppInfo;->objSuppService:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v2

    #@10
    check-cast v2, Lcom/lge/ims/service/uc/SuppInfo$SuppService;

    #@12
    .line 127
    .local v2, service:Lcom/lge/ims/service/uc/SuppInfo$SuppService;
    iget v3, v2, Lcom/lge/ims/service/uc/SuppInfo$SuppService;->type:I

    #@14
    if-ne v3, p1, :cond_17

    #@16
    .line 128
    const/4 v0, 0x1

    #@17
    .line 123
    :cond_17
    add-int/lit8 v1, v1, 0x1

    #@19
    goto :goto_2

    #@1a
    .line 132
    .end local v2           #service:Lcom/lge/ims/service/uc/SuppInfo$SuppService;
    :cond_1a
    const-string v3, "LGIMS_SuppInfo"

    #@1c
    new-instance v4, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v5, "isService : ["

    #@23
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v4

    #@27
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v4

    #@2b
    const-string v5, "]"

    #@2d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v4

    #@31
    const-string v5, "["

    #@33
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v4

    #@37
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v4

    #@3b
    const-string v5, "]"

    #@3d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v4

    #@41
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v4

    #@45
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 133
    return v0
.end method

.method public logIn()V
    .registers 6

    #@0
    .prologue
    .line 55
    const-string v2, "LGIMS_SuppInfo"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "logIn() : size["

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    iget-object v4, p0, Lcom/lge/ims/service/uc/SuppInfo;->objSuppService:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@12
    move-result v4

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    const-string v4, "]"

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v3

    #@21
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 57
    const/4 v0, 0x0

    #@25
    .local v0, index:I
    :goto_25
    iget-object v2, p0, Lcom/lge/ims/service/uc/SuppInfo;->objSuppService:Ljava/util/ArrayList;

    #@27
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@2a
    move-result v2

    #@2b
    if-ge v0, v2, :cond_86

    #@2d
    .line 59
    iget-object v2, p0, Lcom/lge/ims/service/uc/SuppInfo;->objSuppService:Ljava/util/ArrayList;

    #@2f
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@32
    move-result-object v1

    #@33
    check-cast v1, Lcom/lge/ims/service/uc/SuppInfo$SuppService;

    #@35
    .line 61
    .local v1, service:Lcom/lge/ims/service/uc/SuppInfo$SuppService;
    const-string v2, "LGIMS_SuppInfo"

    #@37
    new-instance v3, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v4, "["

    #@3e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v3

    #@42
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@45
    move-result-object v3

    #@46
    const-string v4, "]"

    #@48
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v3

    #@4c
    const-string v4, " type : "

    #@4e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v3

    #@52
    iget v4, v1, Lcom/lge/ims/service/uc/SuppInfo$SuppService;->type:I

    #@54
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@57
    move-result-object v3

    #@58
    const-string v4, " strValue : "

    #@5a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v3

    #@5e
    iget-object v4, v1, Lcom/lge/ims/service/uc/SuppInfo$SuppService;->strValue:Ljava/lang/String;

    #@60
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v3

    #@64
    const-string v4, " intValue : "

    #@66
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v3

    #@6a
    iget v4, v1, Lcom/lge/ims/service/uc/SuppInfo$SuppService;->intValue:I

    #@6c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v3

    #@70
    const-string v4, " boolValue : "

    #@72
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v3

    #@76
    iget-boolean v4, v1, Lcom/lge/ims/service/uc/SuppInfo$SuppService;->boolValue:Z

    #@78
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v3

    #@7c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7f
    move-result-object v3

    #@80
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@83
    .line 57
    add-int/lit8 v0, v0, 0x1

    #@85
    goto :goto_25

    #@86
    .line 68
    .end local v1           #service:Lcom/lge/ims/service/uc/SuppInfo$SuppService;
    :cond_86
    return-void
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .registers 8
    .parameter "source"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 154
    const-string v3, "LGIMS_SuppInfo"

    #@3
    const-string v4, "readFromParcel()"

    #@5
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 156
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@b
    move-result v2

    #@c
    .line 158
    .local v2, service_num:I
    const/4 v0, 0x0

    #@d
    .local v0, index:I
    :goto_d
    if-ge v0, v2, :cond_3a

    #@f
    .line 160
    new-instance v1, Lcom/lge/ims/service/uc/SuppInfo$SuppService;

    #@11
    invoke-direct {v1, p0}, Lcom/lge/ims/service/uc/SuppInfo$SuppService;-><init>(Lcom/lge/ims/service/uc/SuppInfo;)V

    #@14
    .line 161
    .local v1, service:Lcom/lge/ims/service/uc/SuppInfo$SuppService;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v3

    #@18
    iput v3, v1, Lcom/lge/ims/service/uc/SuppInfo$SuppService;->type:I

    #@1a
    .line 162
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1d
    move-result-object v3

    #@1e
    iput-object v3, v1, Lcom/lge/ims/service/uc/SuppInfo$SuppService;->strValue:Ljava/lang/String;

    #@20
    .line 163
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@23
    move-result v3

    #@24
    iput v3, v1, Lcom/lge/ims/service/uc/SuppInfo$SuppService;->intValue:I

    #@26
    .line 164
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@29
    move-result v3

    #@2a
    if-ne v3, v5, :cond_36

    #@2c
    .line 165
    iput-boolean v5, v1, Lcom/lge/ims/service/uc/SuppInfo$SuppService;->boolValue:Z

    #@2e
    .line 171
    :goto_2e
    iget-object v3, p0, Lcom/lge/ims/service/uc/SuppInfo;->objSuppService:Ljava/util/ArrayList;

    #@30
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@33
    .line 158
    add-int/lit8 v0, v0, 0x1

    #@35
    goto :goto_d

    #@36
    .line 168
    :cond_36
    const/4 v3, 0x0

    #@37
    iput-boolean v3, v1, Lcom/lge/ims/service/uc/SuppInfo$SuppService;->boolValue:Z

    #@39
    goto :goto_2e

    #@3a
    .line 175
    .end local v1           #service:Lcom/lge/ims/service/uc/SuppInfo$SuppService;
    :cond_3a
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 179
    iget-object v2, p0, Lcom/lge/ims/service/uc/SuppInfo;->objSuppService:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v2

    #@6
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@9
    .line 181
    const/4 v0, 0x0

    #@a
    .local v0, index:I
    :goto_a
    iget-object v2, p0, Lcom/lge/ims/service/uc/SuppInfo;->objSuppService:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@f
    move-result v2

    #@10
    if-ge v0, v2, :cond_39

    #@12
    .line 183
    iget-object v2, p0, Lcom/lge/ims/service/uc/SuppInfo;->objSuppService:Ljava/util/ArrayList;

    #@14
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@17
    move-result-object v1

    #@18
    check-cast v1, Lcom/lge/ims/service/uc/SuppInfo$SuppService;

    #@1a
    .line 185
    .local v1, service:Lcom/lge/ims/service/uc/SuppInfo$SuppService;
    iget v2, v1, Lcom/lge/ims/service/uc/SuppInfo$SuppService;->type:I

    #@1c
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 187
    iget-object v2, v1, Lcom/lge/ims/service/uc/SuppInfo$SuppService;->strValue:Ljava/lang/String;

    #@21
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@24
    .line 188
    iget v2, v1, Lcom/lge/ims/service/uc/SuppInfo$SuppService;->intValue:I

    #@26
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@29
    .line 190
    iget-boolean v2, v1, Lcom/lge/ims/service/uc/SuppInfo$SuppService;->boolValue:Z

    #@2b
    if-eqz v2, :cond_34

    #@2d
    .line 191
    const/4 v2, 0x1

    #@2e
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@31
    .line 181
    :goto_31
    add-int/lit8 v0, v0, 0x1

    #@33
    goto :goto_a

    #@34
    .line 193
    :cond_34
    const/4 v2, 0x0

    #@35
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@38
    goto :goto_31

    #@39
    .line 197
    .end local v1           #service:Lcom/lge/ims/service/uc/SuppInfo$SuppService;
    :cond_39
    return-void
.end method
