.class final Lcom/lge/ims/service/ip/IPBGHelper$BatteryMonitorHandler;
.super Landroid/os/Handler;
.source "IPBGHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/ip/IPBGHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "BatteryMonitorHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/service/ip/IPBGHelper;


# direct methods
.method private constructor <init>(Lcom/lge/ims/service/ip/IPBGHelper;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 105
    iput-object p1, p0, Lcom/lge/ims/service/ip/IPBGHelper$BatteryMonitorHandler;->this$0:Lcom/lge/ims/service/ip/IPBGHelper;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/lge/ims/service/ip/IPBGHelper;Lcom/lge/ims/service/ip/IPBGHelper$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 105
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPBGHelper$BatteryMonitorHandler;-><init>(Lcom/lge/ims/service/ip/IPBGHelper;)V

    #@3
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 7
    .parameter "msg"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 107
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[IP]battery handle message : "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget v2, p1, Landroid/os/Message;->what:I

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@1a
    .line 108
    iget v1, p1, Landroid/os/Message;->what:I

    #@1c
    packed-switch v1, :pswitch_data_82

    #@1f
    .line 140
    :cond_1f
    :goto_1f
    return-void

    #@20
    .line 110
    :pswitch_20
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPBGHelper$BatteryMonitorHandler;->this$0:Lcom/lge/ims/service/ip/IPBGHelper;

    #@22
    iget-boolean v1, v1, Lcom/lge/ims/service/ip/IPBGHelper;->mLowBattery:Z

    #@24
    if-eqz v1, :cond_1f

    #@26
    .line 111
    const-string v1, "[IP]battery okay"

    #@28
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@2b
    .line 112
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPBGHelper$BatteryMonitorHandler;->this$0:Lcom/lge/ims/service/ip/IPBGHelper;

    #@2d
    iput-boolean v4, v1, Lcom/lge/ims/service/ip/IPBGHelper;->mLowBattery:Z

    #@2f
    .line 113
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPBGHelper$BatteryMonitorHandler;->this$0:Lcom/lge/ims/service/ip/IPBGHelper;

    #@31
    invoke-static {v1}, Lcom/lge/ims/service/ip/IPBGHelper;->access$100(Lcom/lge/ims/service/ip/IPBGHelper;)Z

    #@34
    move-result v1

    #@35
    if-ne v1, v3, :cond_1f

    #@37
    .line 114
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@3a
    move-result-object v0

    #@3b
    .line 115
    .local v0, objIPAgent:Lcom/lge/ims/service/ip/IPAgent;
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@3e
    move-result-object v1

    #@3f
    if-eqz v1, :cond_4a

    #@41
    .line 116
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@44
    move-result-object v1

    #@45
    const/16 v2, 0xf

    #@47
    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@4a
    .line 118
    :cond_4a
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPBGHelper$BatteryMonitorHandler;->this$0:Lcom/lge/ims/service/ip/IPBGHelper;

    #@4c
    invoke-static {v1, v3}, Lcom/lge/ims/service/ip/IPBGHelper;->access$202(Lcom/lge/ims/service/ip/IPBGHelper;Z)Z

    #@4f
    .line 119
    const-string v1, "[IP]bBGServiceOn is true"

    #@51
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@54
    goto :goto_1f

    #@55
    .line 125
    .end local v0           #objIPAgent:Lcom/lge/ims/service/ip/IPAgent;
    :pswitch_55
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPBGHelper$BatteryMonitorHandler;->this$0:Lcom/lge/ims/service/ip/IPBGHelper;

    #@57
    iget-boolean v1, v1, Lcom/lge/ims/service/ip/IPBGHelper;->mLowBattery:Z

    #@59
    if-nez v1, :cond_1f

    #@5b
    .line 126
    const-string v1, "[IP]low battery"

    #@5d
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@60
    .line 127
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@63
    move-result-object v0

    #@64
    .line 128
    .restart local v0       #objIPAgent:Lcom/lge/ims/service/ip/IPAgent;
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPBGHelper$BatteryMonitorHandler;->this$0:Lcom/lge/ims/service/ip/IPBGHelper;

    #@66
    iput-boolean v3, v1, Lcom/lge/ims/service/ip/IPBGHelper;->mLowBattery:Z

    #@68
    .line 129
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@6b
    move-result-object v1

    #@6c
    if-eqz v1, :cond_77

    #@6e
    .line 130
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@71
    move-result-object v1

    #@72
    const/16 v2, 0x10

    #@74
    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@77
    .line 132
    :cond_77
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPBGHelper$BatteryMonitorHandler;->this$0:Lcom/lge/ims/service/ip/IPBGHelper;

    #@79
    invoke-static {v1, v4}, Lcom/lge/ims/service/ip/IPBGHelper;->access$202(Lcom/lge/ims/service/ip/IPBGHelper;Z)Z

    #@7c
    .line 133
    const-string v1, "[IP]bBGServiceOn is false"

    #@7e
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@81
    goto :goto_1f

    #@82
    .line 108
    :pswitch_data_82
    .packed-switch 0x3eb
        :pswitch_55
        :pswitch_20
    .end packed-switch
.end method
