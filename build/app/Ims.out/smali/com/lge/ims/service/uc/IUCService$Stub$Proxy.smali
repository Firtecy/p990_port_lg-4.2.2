.class Lcom/lge/ims/service/uc/IUCService$Stub$Proxy;
.super Ljava/lang/Object;
.source "IUCService.java"

# interfaces
.implements Lcom/lge/ims/service/uc/IUCService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/uc/IUCService$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 95
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 96
    iput-object p1, p0, Lcom/lge/ims/service/uc/IUCService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 97
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 100
    iget-object v0, p0, Lcom/lge/ims/service/uc/IUCService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public attachSession(I)Lcom/lge/ims/service/uc/IUCSession;
    .registers 8
    .parameter "sessionKey"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 126
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 127
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 130
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.ims.service.uc.IUCService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 131
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 132
    iget-object v3, p0, Lcom/lge/ims/service/uc/IUCService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v4, 0x2

    #@13
    const/4 v5, 0x0

    #@14
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 133
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1a
    .line 134
    invoke-virtual {v1}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1d
    move-result-object v3

    #@1e
    invoke-static {v3}, Lcom/lge/ims/service/uc/IUCSession$Stub;->asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/uc/IUCSession;
    :try_end_21
    .catchall {:try_start_8 .. :try_end_21} :catchall_29

    #@21
    move-result-object v2

    #@22
    .line 137
    .local v2, _result:Lcom/lge/ims/service/uc/IUCSession;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 138
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 140
    return-object v2

    #@29
    .line 137
    .end local v2           #_result:Lcom/lge/ims/service/uc/IUCSession;
    :catchall_29
    move-exception v3

    #@2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 138
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    throw v3
.end method

.method public closeSession(Lcom/lge/ims/service/uc/IUCSession;)V
    .registers 7
    .parameter "session"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 144
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 145
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 147
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCService"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 148
    if-eqz p1, :cond_27

    #@f
    invoke-interface {p1}, Lcom/lge/ims/service/uc/IUCSession;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 149
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/4 v3, 0x3

    #@19
    const/4 v4, 0x0

    #@1a
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1d
    .line 150
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_20
    .catchall {:try_start_8 .. :try_end_20} :catchall_29

    #@20
    .line 153
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 154
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 156
    return-void

    #@27
    .line 148
    :cond_27
    const/4 v2, 0x0

    #@28
    goto :goto_13

    #@29
    .line 153
    :catchall_29
    move-exception v2

    #@2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 154
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    throw v2
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 104
    const-string v0, "com.lge.ims.service.uc.IUCService"

    #@2
    return-object v0
.end method

.method public openSession(I)Lcom/lge/ims/service/uc/IUCSession;
    .registers 8
    .parameter "callType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 108
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 109
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 112
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.ims.service.uc.IUCService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 113
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 114
    iget-object v3, p0, Lcom/lge/ims/service/uc/IUCService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v4, 0x1

    #@13
    const/4 v5, 0x0

    #@14
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 115
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1a
    .line 116
    invoke-virtual {v1}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1d
    move-result-object v3

    #@1e
    invoke-static {v3}, Lcom/lge/ims/service/uc/IUCSession$Stub;->asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/uc/IUCSession;
    :try_end_21
    .catchall {:try_start_8 .. :try_end_21} :catchall_29

    #@21
    move-result-object v2

    #@22
    .line 119
    .local v2, _result:Lcom/lge/ims/service/uc/IUCSession;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 120
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 122
    return-object v2

    #@29
    .line 119
    .end local v2           #_result:Lcom/lge/ims/service/uc/IUCSession;
    :catchall_29
    move-exception v3

    #@2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 120
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    throw v3
.end method

.method public setListener(Lcom/lge/ims/service/uc/IUCServiceListener;)V
    .registers 7
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 159
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 160
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 162
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCService"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 163
    if-eqz p1, :cond_27

    #@f
    invoke-interface {p1}, Lcom/lge/ims/service/uc/IUCServiceListener;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 164
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/4 v3, 0x4

    #@19
    const/4 v4, 0x0

    #@1a
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1d
    .line 165
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_20
    .catchall {:try_start_8 .. :try_end_20} :catchall_29

    #@20
    .line 168
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 169
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 171
    return-void

    #@27
    .line 163
    :cond_27
    const/4 v2, 0x0

    #@28
    goto :goto_13

    #@29
    .line 168
    :catchall_29
    move-exception v2

    #@2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 169
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    throw v2
.end method
