.class Lcom/lge/ims/service/ip/IPAgent$IPBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "IPAgent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/ip/IPAgent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "IPBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/service/ip/IPAgent;


# direct methods
.method constructor <init>(Lcom/lge/ims/service/ip/IPAgent;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1905
    iput-object p1, p0, Lcom/lge/ims/service/ip/IPAgent$IPBroadcastReceiver;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public declared-synchronized onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 15
    .parameter "objContext"
    .parameter "objIntent"

    #@0
    .prologue
    const/4 v10, 0x2

    #@1
    const/4 v11, 0x1

    #@2
    .line 1908
    monitor-enter p0

    #@3
    :try_start_3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@6
    move-result-object v5

    #@7
    .line 1909
    .local v5, strAction:Ljava/lang/String;
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@a
    move-result-object v9

    #@b
    if-eqz v9, :cond_17

    #@d
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@10
    move-result-object v9

    #@11
    invoke-virtual {v9}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@14
    move-result-object v9

    #@15
    if-nez v9, :cond_1e

    #@17
    .line 1910
    :cond_17
    const-string v9, "[IP][ERROR]onReceive : IPAgent.GetIPAgent() is NULL"

    #@19
    invoke-static {v9}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_1c
    .catchall {:try_start_3 .. :try_end_1c} :catchall_49

    #@1c
    .line 2015
    :cond_1c
    :goto_1c
    monitor-exit p0

    #@1d
    return-void

    #@1e
    .line 1914
    :cond_1e
    :try_start_1e
    const-string v9, "com.lge.ims.rcs.query_capa"

    #@20
    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v9

    #@24
    if-eqz v9, :cond_4c

    #@26
    .line 1915
    const-string v9, "[IP]onReceive : IP_ACTION_QUERY_CAPA"

    #@28
    invoke-static {v9}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@2b
    .line 1916
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@2e
    move-result-object v9

    #@2f
    invoke-static {v9}, Lcom/lge/ims/service/ip/IPAgent;->access$1000(Lcom/lge/ims/service/ip/IPAgent;)I

    #@32
    move-result v9

    #@33
    if-lt v9, v10, :cond_1c

    #@35
    .line 1917
    const-string v9, "msisdn"

    #@37
    invoke-virtual {p2, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@3a
    move-result-object v6

    #@3b
    .line 1918
    .local v6, strMSISDN:Ljava/lang/String;
    if-eqz v6, :cond_1c

    #@3d
    .line 1919
    invoke-static {v6}, Lcom/lge/ims/service/ip/IPUtil;->NomalizeMSISDN(Ljava/lang/String;)Ljava/lang/String;

    #@40
    move-result-object v8

    #@41
    .line 1920
    .local v8, strNormalizeMSISDN:Ljava/lang/String;
    iget-object v9, p0, Lcom/lge/ims/service/ip/IPAgent$IPBroadcastReceiver;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@43
    iget-object v9, v9, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@45
    invoke-virtual {v9, v8}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->ContactCapaQuery(Ljava/lang/String;)V
    :try_end_48
    .catchall {:try_start_1e .. :try_end_48} :catchall_49

    #@48
    goto :goto_1c

    #@49
    .line 1908
    .end local v5           #strAction:Ljava/lang/String;
    .end local v6           #strMSISDN:Ljava/lang/String;
    .end local v8           #strNormalizeMSISDN:Ljava/lang/String;
    :catchall_49
    move-exception v9

    #@4a
    monitor-exit p0

    #@4b
    throw v9

    #@4c
    .line 1923
    .restart local v5       #strAction:Ljava/lang/String;
    :cond_4c
    :try_start_4c
    const-string v9, "com.lge.ims.rcs.query_capa_id"

    #@4e
    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@51
    move-result v9

    #@52
    if-eqz v9, :cond_79

    #@54
    .line 1924
    const-string v9, "[IP]onReceive : IP_ACTION_QUERY_CAPA_ID"

    #@56
    invoke-static {v9}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@59
    .line 1925
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@5c
    move-result-object v9

    #@5d
    invoke-static {v9}, Lcom/lge/ims/service/ip/IPAgent;->access$1000(Lcom/lge/ims/service/ip/IPAgent;)I

    #@60
    move-result v9

    #@61
    if-lt v9, v10, :cond_1c

    #@63
    .line 1926
    const-string v9, "contact_id"

    #@65
    const/4 v10, 0x0

    #@66
    invoke-virtual {p2, v9, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@69
    move-result v9

    #@6a
    int-to-long v2, v9

    #@6b
    .line 1927
    .local v2, nContactId:J
    const-wide/16 v9, 0x0

    #@6d
    cmp-long v9, v2, v9

    #@6f
    if-eqz v9, :cond_1c

    #@71
    .line 1928
    iget-object v9, p0, Lcom/lge/ims/service/ip/IPAgent$IPBroadcastReceiver;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@73
    iget-object v9, v9, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@75
    invoke-virtual {v9, v2, v3}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->ContactCapaQuery(J)V

    #@78
    goto :goto_1c

    #@79
    .line 1931
    .end local v2           #nContactId:J
    :cond_79
    const-string v9, "com.lge.ims.rcs.query_list"

    #@7b
    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7e
    move-result v9

    #@7f
    if-eqz v9, :cond_e9

    #@81
    .line 1932
    const-string v9, "[IP]onReceive : IP_ACTION_QUERY_LIST"

    #@83
    invoke-static {v9}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@86
    .line 1933
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@89
    move-result-object v9

    #@8a
    invoke-static {v9}, Lcom/lge/ims/service/ip/IPAgent;->access$1000(Lcom/lge/ims/service/ip/IPAgent;)I

    #@8d
    move-result v9

    #@8e
    if-lt v9, v10, :cond_b1

    #@90
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@93
    move-result-object v9

    #@94
    invoke-static {v9}, Lcom/lge/ims/service/ip/IPAgent;->access$1100(Lcom/lge/ims/service/ip/IPAgent;)Z

    #@97
    move-result v9

    #@98
    if-ne v9, v11, :cond_b1

    #@9a
    .line 1934
    iget-object v9, p0, Lcom/lge/ims/service/ip/IPAgent$IPBroadcastReceiver;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@9c
    iget-object v9, v9, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@9e
    invoke-virtual {v9}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->SendRLSListQuery()Z

    #@a1
    move-result v9

    #@a2
    if-ne v9, v11, :cond_1c

    #@a4
    .line 1935
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@a7
    move-result-object v9

    #@a8
    invoke-static {v9}, Lcom/lge/ims/service/ip/IPAgent;->access$300(Lcom/lge/ims/service/ip/IPAgent;)Lcom/lge/ims/service/ip/IPContactHelper;

    #@ab
    move-result-object v9

    #@ac
    invoke-virtual {v9}, Lcom/lge/ims/service/ip/IPContactHelper;->UpdateRLSSubPollingLatestTime()Z

    #@af
    goto/16 :goto_1c

    #@b1
    .line 1938
    :cond_b1
    new-instance v9, Ljava/lang/StringBuilder;

    #@b3
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@b6
    const-string v10, "[IP][ERROR]onReceive : state [ "

    #@b8
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v9

    #@bc
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@bf
    move-result-object v10

    #@c0
    invoke-static {v10}, Lcom/lge/ims/service/ip/IPAgent;->access$1000(Lcom/lge/ims/service/ip/IPAgent;)I

    #@c3
    move-result v10

    #@c4
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v9

    #@c8
    const-string v10, " ], bXDMConnected [ "

    #@ca
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v9

    #@ce
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@d1
    move-result-object v10

    #@d2
    invoke-static {v10}, Lcom/lge/ims/service/ip/IPAgent;->access$1100(Lcom/lge/ims/service/ip/IPAgent;)Z

    #@d5
    move-result v10

    #@d6
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v9

    #@da
    const-string v10, " ]"

    #@dc
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@df
    move-result-object v9

    #@e0
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e3
    move-result-object v9

    #@e4
    invoke-static {v9}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@e7
    goto/16 :goto_1c

    #@e9
    .line 1940
    :cond_e9
    const-string v9, "com.lge.ims.rcs.my_status_change"

    #@eb
    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ee
    move-result v9

    #@ef
    if-eqz v9, :cond_14b

    #@f1
    .line 1941
    const-string v9, "[IP]onReceive : IP_ACTION_MY_STATUS_CHANGE"

    #@f3
    invoke-static {v9}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@f6
    .line 1942
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@f9
    move-result-object v9

    #@fa
    invoke-static {v9}, Lcom/lge/ims/service/ip/IPAgent;->access$1000(Lcom/lge/ims/service/ip/IPAgent;)I

    #@fd
    move-result v9

    #@fe
    if-lt v9, v10, :cond_113

    #@100
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@103
    move-result-object v9

    #@104
    invoke-static {v9}, Lcom/lge/ims/service/ip/IPAgent;->access$1100(Lcom/lge/ims/service/ip/IPAgent;)Z

    #@107
    move-result v9

    #@108
    if-ne v9, v11, :cond_113

    #@10a
    .line 1943
    iget-object v9, p0, Lcom/lge/ims/service/ip/IPAgent$IPBroadcastReceiver;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@10c
    iget-object v9, v9, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@10e
    invoke-virtual {v9}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->UpdateMyStatus()V

    #@111
    goto/16 :goto_1c

    #@113
    .line 1945
    :cond_113
    new-instance v9, Ljava/lang/StringBuilder;

    #@115
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@118
    const-string v10, "[IP][ERROR]onReceive : state [ "

    #@11a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11d
    move-result-object v9

    #@11e
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@121
    move-result-object v10

    #@122
    invoke-static {v10}, Lcom/lge/ims/service/ip/IPAgent;->access$1000(Lcom/lge/ims/service/ip/IPAgent;)I

    #@125
    move-result v10

    #@126
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@129
    move-result-object v9

    #@12a
    const-string v10, " ], bXDMConnected [ "

    #@12c
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12f
    move-result-object v9

    #@130
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@133
    move-result-object v10

    #@134
    invoke-static {v10}, Lcom/lge/ims/service/ip/IPAgent;->access$1100(Lcom/lge/ims/service/ip/IPAgent;)Z

    #@137
    move-result v10

    #@138
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@13b
    move-result-object v9

    #@13c
    const-string v10, " ]"

    #@13e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@141
    move-result-object v9

    #@142
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@145
    move-result-object v9

    #@146
    invoke-static {v9}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@149
    goto/16 :goto_1c

    #@14b
    .line 1947
    :cond_14b
    const-string v9, "com.lge.ims.rcs.my_icon_change"

    #@14d
    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@150
    move-result v9

    #@151
    if-eqz v9, :cond_1ad

    #@153
    .line 1948
    const-string v9, "[IP]onReceive : IP_ACTION_MY_ICON_CHANGE"

    #@155
    invoke-static {v9}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@158
    .line 1949
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@15b
    move-result-object v9

    #@15c
    invoke-static {v9}, Lcom/lge/ims/service/ip/IPAgent;->access$1000(Lcom/lge/ims/service/ip/IPAgent;)I

    #@15f
    move-result v9

    #@160
    if-lt v9, v10, :cond_175

    #@162
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@165
    move-result-object v9

    #@166
    invoke-static {v9}, Lcom/lge/ims/service/ip/IPAgent;->access$1100(Lcom/lge/ims/service/ip/IPAgent;)Z

    #@169
    move-result v9

    #@16a
    if-ne v9, v11, :cond_175

    #@16c
    .line 1950
    iget-object v9, p0, Lcom/lge/ims/service/ip/IPAgent$IPBroadcastReceiver;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@16e
    iget-object v9, v9, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@170
    invoke-virtual {v9}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->UpdateMyStatusIcon()V

    #@173
    goto/16 :goto_1c

    #@175
    .line 1952
    :cond_175
    new-instance v9, Ljava/lang/StringBuilder;

    #@177
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@17a
    const-string v10, "[IP][ERROR]onReceive : state [ "

    #@17c
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17f
    move-result-object v9

    #@180
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@183
    move-result-object v10

    #@184
    invoke-static {v10}, Lcom/lge/ims/service/ip/IPAgent;->access$1000(Lcom/lge/ims/service/ip/IPAgent;)I

    #@187
    move-result v10

    #@188
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18b
    move-result-object v9

    #@18c
    const-string v10, " ], bXDMConnected [ "

    #@18e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@191
    move-result-object v9

    #@192
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@195
    move-result-object v10

    #@196
    invoke-static {v10}, Lcom/lge/ims/service/ip/IPAgent;->access$1100(Lcom/lge/ims/service/ip/IPAgent;)Z

    #@199
    move-result v10

    #@19a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@19d
    move-result-object v9

    #@19e
    const-string v10, " ]"

    #@1a0
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a3
    move-result-object v9

    #@1a4
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a7
    move-result-object v9

    #@1a8
    invoke-static {v9}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@1ab
    goto/16 :goto_1c

    #@1ad
    .line 1954
    :cond_1ad
    const-string v9, "com.lge.ims.rcs.my_icon_delete"

    #@1af
    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b2
    move-result v9

    #@1b3
    if-eqz v9, :cond_20f

    #@1b5
    .line 1955
    const-string v9, "[IP]onReceive : IP_ACTION_MY_ICON_DELETE"

    #@1b7
    invoke-static {v9}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@1ba
    .line 1956
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@1bd
    move-result-object v9

    #@1be
    invoke-static {v9}, Lcom/lge/ims/service/ip/IPAgent;->access$1000(Lcom/lge/ims/service/ip/IPAgent;)I

    #@1c1
    move-result v9

    #@1c2
    if-lt v9, v10, :cond_1d7

    #@1c4
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@1c7
    move-result-object v9

    #@1c8
    invoke-static {v9}, Lcom/lge/ims/service/ip/IPAgent;->access$1100(Lcom/lge/ims/service/ip/IPAgent;)Z

    #@1cb
    move-result v9

    #@1cc
    if-ne v9, v11, :cond_1d7

    #@1ce
    .line 1957
    iget-object v9, p0, Lcom/lge/ims/service/ip/IPAgent$IPBroadcastReceiver;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@1d0
    iget-object v9, v9, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@1d2
    invoke-virtual {v9}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->DeleteMyStatusIcon()V

    #@1d5
    goto/16 :goto_1c

    #@1d7
    .line 1959
    :cond_1d7
    new-instance v9, Ljava/lang/StringBuilder;

    #@1d9
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@1dc
    const-string v10, "[IP][ERROR]onReceive : state [ "

    #@1de
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e1
    move-result-object v9

    #@1e2
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@1e5
    move-result-object v10

    #@1e6
    invoke-static {v10}, Lcom/lge/ims/service/ip/IPAgent;->access$1000(Lcom/lge/ims/service/ip/IPAgent;)I

    #@1e9
    move-result v10

    #@1ea
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1ed
    move-result-object v9

    #@1ee
    const-string v10, " ], bXDMConnected [ "

    #@1f0
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f3
    move-result-object v9

    #@1f4
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@1f7
    move-result-object v10

    #@1f8
    invoke-static {v10}, Lcom/lge/ims/service/ip/IPAgent;->access$1100(Lcom/lge/ims/service/ip/IPAgent;)Z

    #@1fb
    move-result v10

    #@1fc
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1ff
    move-result-object v9

    #@200
    const-string v10, " ]"

    #@202
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@205
    move-result-object v9

    #@206
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@209
    move-result-object v9

    #@20a
    invoke-static {v9}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@20d
    goto/16 :goto_1c

    #@20f
    .line 1961
    :cond_20f
    const-string v9, "com.lge.ims.rcs.invite_user"

    #@211
    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@214
    move-result v9

    #@215
    if-eqz v9, :cond_281

    #@217
    .line 1962
    const-string v9, "msisdn"

    #@219
    invoke-virtual {p2, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@21c
    move-result-object v6

    #@21d
    .line 1963
    .restart local v6       #strMSISDN:Ljava/lang/String;
    if-eqz v6, :cond_1c

    #@21f
    .line 1964
    new-instance v9, Ljava/lang/StringBuilder;

    #@221
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@224
    const-string v10, "[IP]onReceive : IP_ACTION_INVITE_USER - strMSISDN [ "

    #@226
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@229
    move-result-object v9

    #@22a
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22d
    move-result-object v9

    #@22e
    const-string v10, " ]"

    #@230
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@233
    move-result-object v9

    #@234
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@237
    move-result-object v9

    #@238
    invoke-static {v9}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@23b
    .line 1965
    const-string v9, ";"

    #@23d
    invoke-virtual {v6, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@240
    move-result-object v7

    #@241
    .line 1966
    .local v7, strMSISDNTemp:[Ljava/lang/String;
    const/4 v0, 0x0

    #@242
    .local v0, i:I
    :goto_242
    array-length v9, v7

    #@243
    if-ge v0, v9, :cond_1c

    #@245
    .line 1967
    aget-object v9, v7, v0

    #@247
    if-eqz v9, :cond_27e

    #@249
    .line 1968
    new-instance v9, Ljava/lang/StringBuilder;

    #@24b
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@24e
    const-string v10, "[IP]onReceive : IP_ACTION_INVITE_USER - index [ "

    #@250
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@253
    move-result-object v9

    #@254
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@257
    move-result-object v9

    #@258
    const-string v10, " ], normalize msisdn ["

    #@25a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25d
    move-result-object v9

    #@25e
    aget-object v10, v7, v0

    #@260
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@263
    move-result-object v9

    #@264
    const-string v10, "]"

    #@266
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@269
    move-result-object v9

    #@26a
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26d
    move-result-object v9

    #@26e
    invoke-static {v9}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@271
    .line 1969
    iget-object v9, p0, Lcom/lge/ims/service/ip/IPAgent$IPBroadcastReceiver;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@273
    iget-object v9, v9, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@275
    aget-object v10, v7, v0

    #@277
    invoke-static {v10}, Lcom/lge/ims/service/ip/IPUtil;->NomalizeMSISDN(Ljava/lang/String;)Ljava/lang/String;

    #@27a
    move-result-object v10

    #@27b
    invoke-virtual {v9, v10}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->InviteFriendToRCSService(Ljava/lang/String;)V

    #@27e
    .line 1966
    :cond_27e
    add-int/lit8 v0, v0, 0x1

    #@280
    goto :goto_242

    #@281
    .line 1973
    .end local v0           #i:I
    .end local v6           #strMSISDN:Ljava/lang/String;
    .end local v7           #strMSISDNTemp:[Ljava/lang/String;
    :cond_281
    const-string v9, "com.lge.ims.rcs.get_buddy_icon"

    #@283
    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@286
    move-result v9

    #@287
    if-eqz v9, :cond_2ef

    #@289
    .line 1974
    const-string v9, "[IP]onReceive : IP_ACTION_GET_BUDDY_ICON"

    #@28b
    invoke-static {v9}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@28e
    .line 1975
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@291
    move-result-object v9

    #@292
    invoke-static {v9}, Lcom/lge/ims/service/ip/IPAgent;->access$1000(Lcom/lge/ims/service/ip/IPAgent;)I

    #@295
    move-result v9

    #@296
    if-lt v9, v10, :cond_2b7

    #@298
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@29b
    move-result-object v9

    #@29c
    invoke-static {v9}, Lcom/lge/ims/service/ip/IPAgent;->access$1100(Lcom/lge/ims/service/ip/IPAgent;)Z

    #@29f
    move-result v9

    #@2a0
    if-ne v9, v11, :cond_2b7

    #@2a2
    .line 1976
    const-string v9, "msisdn"

    #@2a4
    invoke-virtual {p2, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@2a7
    move-result-object v6

    #@2a8
    .line 1977
    .restart local v6       #strMSISDN:Ljava/lang/String;
    if-eqz v6, :cond_1c

    #@2aa
    .line 1978
    invoke-static {v6}, Lcom/lge/ims/service/ip/IPUtil;->NomalizeMSISDN(Ljava/lang/String;)Ljava/lang/String;

    #@2ad
    move-result-object v8

    #@2ae
    .line 1979
    .restart local v8       #strNormalizeMSISDN:Ljava/lang/String;
    iget-object v9, p0, Lcom/lge/ims/service/ip/IPAgent$IPBroadcastReceiver;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@2b0
    iget-object v9, v9, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@2b2
    invoke-virtual {v9, v8}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->GetBuddyStatusIcon(Ljava/lang/String;)V

    #@2b5
    goto/16 :goto_1c

    #@2b7
    .line 1982
    .end local v6           #strMSISDN:Ljava/lang/String;
    .end local v8           #strNormalizeMSISDN:Ljava/lang/String;
    :cond_2b7
    new-instance v9, Ljava/lang/StringBuilder;

    #@2b9
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@2bc
    const-string v10, "[IP][ERROR]onReceive : state [ "

    #@2be
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c1
    move-result-object v9

    #@2c2
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@2c5
    move-result-object v10

    #@2c6
    invoke-static {v10}, Lcom/lge/ims/service/ip/IPAgent;->access$1000(Lcom/lge/ims/service/ip/IPAgent;)I

    #@2c9
    move-result v10

    #@2ca
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2cd
    move-result-object v9

    #@2ce
    const-string v10, " ], bXDMConnected [ "

    #@2d0
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d3
    move-result-object v9

    #@2d4
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@2d7
    move-result-object v10

    #@2d8
    invoke-static {v10}, Lcom/lge/ims/service/ip/IPAgent;->access$1100(Lcom/lge/ims/service/ip/IPAgent;)Z

    #@2db
    move-result v10

    #@2dc
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2df
    move-result-object v9

    #@2e0
    const-string v10, " ]"

    #@2e2
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e5
    move-result-object v9

    #@2e6
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e9
    move-result-object v9

    #@2ea
    invoke-static {v9}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@2ed
    goto/16 :goto_1c

    #@2ef
    .line 1984
    :cond_2ef
    const-string v9, "com.lge.ims.rcs.photo_update"

    #@2f1
    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2f4
    move-result v9

    #@2f5
    if-eqz v9, :cond_338

    #@2f7
    .line 1985
    const-string v9, "photo_overwrite"

    #@2f9
    const/4 v10, 0x0

    #@2fa
    invoke-virtual {p2, v9, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@2fd
    move-result v4

    #@2fe
    .line 1986
    .local v4, nPhotoOverwrite:I
    new-instance v9, Ljava/lang/StringBuilder;

    #@300
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@303
    const-string v10, "[IP]onReceive : IP_ACTION_PHOTO_UPDATE - nPhotoOverwrite [ "

    #@305
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@308
    move-result-object v9

    #@309
    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30c
    move-result-object v9

    #@30d
    const-string v10, " ]"

    #@30f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@312
    move-result-object v9

    #@313
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@316
    move-result-object v9

    #@317
    invoke-static {v9}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@31a
    .line 1987
    if-ne v4, v11, :cond_32a

    #@31c
    .line 1988
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@31f
    move-result-object v9

    #@320
    invoke-static {v9}, Lcom/lge/ims/service/ip/IPAgent;->access$300(Lcom/lge/ims/service/ip/IPAgent;)Lcom/lge/ims/service/ip/IPContactHelper;

    #@323
    move-result-object v9

    #@324
    const/4 v10, 0x1

    #@325
    invoke-virtual {v9, v10}, Lcom/lge/ims/service/ip/IPContactHelper;->SyncPresenceFullImageToContact(Z)Z

    #@328
    goto/16 :goto_1c

    #@32a
    .line 1990
    :cond_32a
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@32d
    move-result-object v9

    #@32e
    invoke-static {v9}, Lcom/lge/ims/service/ip/IPAgent;->access$300(Lcom/lge/ims/service/ip/IPAgent;)Lcom/lge/ims/service/ip/IPContactHelper;

    #@331
    move-result-object v9

    #@332
    const/4 v10, 0x0

    #@333
    invoke-virtual {v9, v10}, Lcom/lge/ims/service/ip/IPContactHelper;->SyncPresenceFullImageToContact(Z)Z

    #@336
    goto/16 :goto_1c

    #@338
    .line 1995
    .end local v4           #nPhotoOverwrite:I
    :cond_338
    const-string v9, "com.lge.ims.action.IMS_EVENT_CONFIG_UPDATE_COMPLETE"

    #@33a
    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@33d
    move-result v9

    #@33e
    if-eqz v9, :cond_34e

    #@340
    .line 1996
    const-string v9, "[IP]onReceive : IP_ACTION_AC_UPDATE_COMPLETE"

    #@342
    invoke-static {v9}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@345
    .line 1997
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@348
    move-result-object v9

    #@349
    invoke-static {v9}, Lcom/lge/ims/service/ip/IPAgent;->access$1200(Lcom/lge/ims/service/ip/IPAgent;)V

    #@34c
    goto/16 :goto_1c

    #@34e
    .line 1998
    :cond_34e
    const-string v9, "com.lge.ims.ip.polling"

    #@350
    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@353
    move-result v9

    #@354
    if-eqz v9, :cond_399

    #@356
    .line 1999
    const-string v9, "ip_alarm_id"

    #@358
    const/4 v10, 0x0

    #@359
    invoke-virtual {p2, v9, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@35c
    move-result v1

    #@35d
    .line 2000
    .local v1, nAlarmId:I
    new-instance v9, Ljava/lang/StringBuilder;

    #@35f
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@362
    const-string v10, "[IP]onReceive : ACTION_IP_ALARM_TIME [ "

    #@364
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@367
    move-result-object v9

    #@368
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36b
    move-result-object v9

    #@36c
    const-string v10, " ]"

    #@36e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@371
    move-result-object v9

    #@372
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@375
    move-result-object v9

    #@376
    invoke-static {v9}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@379
    .line 2001
    const/16 v9, 0x5015

    #@37b
    if-ne v1, v9, :cond_38b

    #@37d
    .line 2002
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@380
    move-result-object v9

    #@381
    invoke-virtual {v9}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@384
    move-result-object v9

    #@385
    const/4 v10, 0x4

    #@386
    invoke-virtual {v9, v10}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@389
    goto/16 :goto_1c

    #@38b
    .line 2004
    :cond_38b
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@38e
    move-result-object v9

    #@38f
    invoke-virtual {v9}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@392
    move-result-object v9

    #@393
    const/4 v10, 0x5

    #@394
    invoke-virtual {v9, v10}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@397
    goto/16 :goto_1c

    #@399
    .line 2006
    .end local v1           #nAlarmId:I
    :cond_399
    const-string v9, "android.intent.action.SCREEN_OFF"

    #@39b
    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@39e
    move-result v9

    #@39f
    if-eqz v9, :cond_3ad

    #@3a1
    .line 2007
    iget-object v9, p0, Lcom/lge/ims/service/ip/IPAgent$IPBroadcastReceiver;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@3a3
    invoke-static {v9}, Lcom/lge/ims/service/ip/IPAgent;->access$1300(Lcom/lge/ims/service/ip/IPAgent;)Lcom/lge/ims/service/ip/IPBGHelper;

    #@3a6
    move-result-object v9

    #@3a7
    const/4 v10, 0x0

    #@3a8
    invoke-virtual {v9, v10}, Lcom/lge/ims/service/ip/IPBGHelper;->SetScreenState(Z)V

    #@3ab
    goto/16 :goto_1c

    #@3ad
    .line 2008
    :cond_3ad
    const-string v9, "android.intent.action.SCREEN_ON"

    #@3af
    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3b2
    move-result v9

    #@3b3
    if-eqz v9, :cond_3c1

    #@3b5
    .line 2009
    iget-object v9, p0, Lcom/lge/ims/service/ip/IPAgent$IPBroadcastReceiver;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@3b7
    invoke-static {v9}, Lcom/lge/ims/service/ip/IPAgent;->access$1300(Lcom/lge/ims/service/ip/IPAgent;)Lcom/lge/ims/service/ip/IPBGHelper;

    #@3ba
    move-result-object v9

    #@3bb
    const/4 v10, 0x1

    #@3bc
    invoke-virtual {v9, v10}, Lcom/lge/ims/service/ip/IPBGHelper;->SetScreenState(Z)V

    #@3bf
    goto/16 :goto_1c

    #@3c1
    .line 2013
    :cond_3c1
    const-string v9, "[IP][ERROR]onReceive : Invalid Broadcast"

    #@3c3
    invoke-static {v9}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_3c6
    .catchall {:try_start_4c .. :try_end_3c6} :catchall_49

    #@3c6
    goto/16 :goto_1c
.end method
