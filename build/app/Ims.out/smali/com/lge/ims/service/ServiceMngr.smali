.class public Lcom/lge/ims/service/ServiceMngr;
.super Ljava/lang/Object;
.source "ServiceMngr.java"


# static fields
.field public static final SERVICE_ALL_M:I = 0xf

.field public static final SERVICE_CS:I = 0xc

.field public static final SERVICE_CS_IS:I = 0x4

.field public static final SERVICE_CS_VS:I = 0x8

.field public static final SERVICE_IM:I = 0x3

.field public static final SERVICE_IM_CHAT:I = 0x2

.field public static final SERVICE_IM_FT:I = 0x1

.field private static final TAG:Ljava/lang/String; = "ServiceMngr"

.field private static serviceMngr:Lcom/lge/ims/service/ServiceMngr;


# instance fields
.field private serviceLte:I

.field private serviceWifi:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 16
    const/4 v0, 0x0

    #@1
    sput-object v0, Lcom/lge/ims/service/ServiceMngr;->serviceMngr:Lcom/lge/ims/service/ServiceMngr;

    #@3
    return-void
.end method

.method public constructor <init>()V
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 55
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 38
    iput v0, p0, Lcom/lge/ims/service/ServiceMngr;->serviceWifi:I

    #@6
    .line 39
    iput v0, p0, Lcom/lge/ims/service/ServiceMngr;->serviceLte:I

    #@8
    .line 56
    const-string v0, "ServiceMngr"

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "ServiceMngr() "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@20
    .line 57
    return-void
.end method

.method public static getInstance()Lcom/lge/ims/service/ServiceMngr;
    .registers 2

    #@0
    .prologue
    .line 42
    sget-object v0, Lcom/lge/ims/service/ServiceMngr;->serviceMngr:Lcom/lge/ims/service/ServiceMngr;

    #@2
    if-nez v0, :cond_18

    #@4
    .line 43
    new-instance v0, Lcom/lge/ims/service/ServiceMngr;

    #@6
    invoke-direct {v0}, Lcom/lge/ims/service/ServiceMngr;-><init>()V

    #@9
    sput-object v0, Lcom/lge/ims/service/ServiceMngr;->serviceMngr:Lcom/lge/ims/service/ServiceMngr;

    #@b
    .line 45
    sget-object v0, Lcom/lge/ims/service/ServiceMngr;->serviceMngr:Lcom/lge/ims/service/ServiceMngr;

    #@d
    if-nez v0, :cond_18

    #@f
    .line 46
    const-string v0, "ServiceMngr"

    #@11
    const-string v1, "serviceMngr is failed"

    #@13
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@16
    .line 47
    const/4 v0, 0x0

    #@17
    .line 51
    :goto_17
    return-object v0

    #@18
    :cond_18
    sget-object v0, Lcom/lge/ims/service/ServiceMngr;->serviceMngr:Lcom/lge/ims/service/ServiceMngr;

    #@1a
    goto :goto_17
.end method

.method private updateState(III)V
    .registers 7
    .parameter "netType"
    .parameter "regState"
    .parameter "value"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 103
    if-ne p1, v0, :cond_2d

    #@3
    .line 104
    if-ne p2, v0, :cond_25

    #@5
    .line 105
    iget v0, p0, Lcom/lge/ims/service/ServiceMngr;->serviceLte:I

    #@7
    or-int/2addr v0, p3

    #@8
    iput v0, p0, Lcom/lge/ims/service/ServiceMngr;->serviceLte:I

    #@a
    .line 110
    :goto_a
    const-string v0, "ServiceMngr"

    #@c
    new-instance v1, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v2, "LTE service update = "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    iget v2, p0, Lcom/lge/ims/service/ServiceMngr;->serviceLte:I

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@24
    .line 120
    :goto_24
    return-void

    #@25
    .line 107
    :cond_25
    iget v0, p0, Lcom/lge/ims/service/ServiceMngr;->serviceLte:I

    #@27
    xor-int/lit8 v1, p3, -0x1

    #@29
    and-int/2addr v0, v1

    #@2a
    iput v0, p0, Lcom/lge/ims/service/ServiceMngr;->serviceLte:I

    #@2c
    goto :goto_a

    #@2d
    .line 112
    :cond_2d
    if-ne p2, v0, :cond_4f

    #@2f
    .line 113
    iget v0, p0, Lcom/lge/ims/service/ServiceMngr;->serviceWifi:I

    #@31
    or-int/2addr v0, p3

    #@32
    iput v0, p0, Lcom/lge/ims/service/ServiceMngr;->serviceWifi:I

    #@34
    .line 118
    :goto_34
    const-string v0, "ServiceMngr"

    #@36
    new-instance v1, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v2, "WIFI service update = "

    #@3d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v1

    #@41
    iget v2, p0, Lcom/lge/ims/service/ServiceMngr;->serviceWifi:I

    #@43
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v1

    #@47
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v1

    #@4b
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@4e
    goto :goto_24

    #@4f
    .line 115
    :cond_4f
    iget v0, p0, Lcom/lge/ims/service/ServiceMngr;->serviceWifi:I

    #@51
    xor-int/lit8 v1, p3, -0x1

    #@53
    and-int/2addr v0, v1

    #@54
    iput v0, p0, Lcom/lge/ims/service/ServiceMngr;->serviceWifi:I

    #@56
    goto :goto_34
.end method


# virtual methods
.method public isLTEConnected(I)Z
    .registers 5
    .parameter "serviceType"

    #@0
    .prologue
    .line 81
    const-string v0, "ServiceMngr"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "LTE service = "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget v2, p0, Lcom/lge/ims/service/ServiceMngr;->serviceLte:I

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    const-string v2, " , type = "

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@24
    .line 83
    iget v0, p0, Lcom/lge/ims/service/ServiceMngr;->serviceLte:I

    #@26
    and-int/2addr v0, p1

    #@27
    if-lez v0, :cond_32

    #@29
    .line 84
    const-string v0, "ServiceMngr"

    #@2b
    const-string v1, "service is connected in LTE"

    #@2d
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@30
    .line 85
    const/4 v0, 0x1

    #@31
    .line 88
    :goto_31
    return v0

    #@32
    :cond_32
    const/4 v0, 0x0

    #@33
    goto :goto_31
.end method

.method public isWifiConnected(I)Z
    .registers 5
    .parameter "serviceType"

    #@0
    .prologue
    .line 92
    const-string v0, "ServiceMngr"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "WIFI service = "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget v2, p0, Lcom/lge/ims/service/ServiceMngr;->serviceWifi:I

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    const-string v2, " , type = "

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@24
    .line 94
    iget v0, p0, Lcom/lge/ims/service/ServiceMngr;->serviceWifi:I

    #@26
    and-int/2addr v0, p1

    #@27
    if-lez v0, :cond_32

    #@29
    .line 95
    const-string v0, "ServiceMngr"

    #@2b
    const-string v1, "service is connected in WIFI"

    #@2d
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@30
    .line 96
    const/4 v0, 0x1

    #@31
    .line 99
    :goto_31
    return v0

    #@32
    :cond_32
    const/4 v0, 0x0

    #@33
    goto :goto_31
.end method

.method public updateServiceState(II)V
    .registers 8
    .parameter "wParam"
    .parameter "lParam"

    #@0
    .prologue
    const v4, 0xffff

    #@3
    const/16 v3, 0xc

    #@5
    .line 60
    and-int v0, p1, v4

    #@7
    .line 61
    .local v0, appType:I
    shr-int/lit8 v2, p1, 0x10

    #@9
    and-int v1, v2, v4

    #@b
    .line 63
    .local v1, networkType:I
    const/16 v2, 0x15

    #@d
    if-ne v0, v2, :cond_14

    #@f
    .line 64
    const/4 v2, 0x3

    #@10
    invoke-direct {p0, v1, p2, v2}, Lcom/lge/ims/service/ServiceMngr;->updateState(III)V

    #@13
    .line 78
    :goto_13
    return-void

    #@14
    .line 65
    :cond_14
    const/16 v2, 0x16

    #@16
    if-ne v0, v2, :cond_1d

    #@18
    .line 66
    const/4 v2, 0x2

    #@19
    invoke-direct {p0, v1, p2, v2}, Lcom/lge/ims/service/ServiceMngr;->updateState(III)V

    #@1c
    goto :goto_13

    #@1d
    .line 67
    :cond_1d
    const/16 v2, 0x18

    #@1f
    if-ne v0, v2, :cond_26

    #@21
    .line 68
    const/4 v2, 0x1

    #@22
    invoke-direct {p0, v1, p2, v2}, Lcom/lge/ims/service/ServiceMngr;->updateState(III)V

    #@25
    goto :goto_13

    #@26
    .line 69
    :cond_26
    const/16 v2, 0xb

    #@28
    if-ne v0, v2, :cond_2e

    #@2a
    .line 70
    invoke-direct {p0, v1, p2, v3}, Lcom/lge/ims/service/ServiceMngr;->updateState(III)V

    #@2d
    goto :goto_13

    #@2e
    .line 71
    :cond_2e
    if-ne v0, v3, :cond_36

    #@30
    .line 72
    const/16 v2, 0x8

    #@32
    invoke-direct {p0, v1, p2, v2}, Lcom/lge/ims/service/ServiceMngr;->updateState(III)V

    #@35
    goto :goto_13

    #@36
    .line 73
    :cond_36
    const/16 v2, 0xd

    #@38
    if-ne v0, v2, :cond_3f

    #@3a
    .line 74
    const/4 v2, 0x4

    #@3b
    invoke-direct {p0, v1, p2, v2}, Lcom/lge/ims/service/ServiceMngr;->updateState(III)V

    #@3e
    goto :goto_13

    #@3f
    .line 76
    :cond_3f
    const-string v2, "ServiceMngr"

    #@41
    const-string v3, "invlid app type"

    #@43
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@46
    goto :goto_13
.end method
