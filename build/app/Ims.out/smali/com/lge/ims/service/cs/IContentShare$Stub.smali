.class public abstract Lcom/lge/ims/service/cs/IContentShare$Stub;
.super Landroid/os/Binder;
.source "IContentShare.java"

# interfaces
.implements Lcom/lge/ims/service/cs/IContentShare;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/cs/IContentShare;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/cs/IContentShare$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.lge.ims.service.cs.IContentShare"

.field static final TRANSACTION_addCall:I = 0x2

.field static final TRANSACTION_closeImageSession:I = 0x9

.field static final TRANSACTION_closeVideoSession:I = 0x7

.field static final TRANSACTION_getServiceNetworkType:I = 0xa

.field static final TRANSACTION_holdCall:I = 0x4

.field static final TRANSACTION_openImageSession:I = 0x8

.field static final TRANSACTION_openVideoSession:I = 0x6

.field static final TRANSACTION_removeCall:I = 0x3

.field static final TRANSACTION_setListener:I = 0x1

.field static final TRANSACTION_unholdCall:I = 0x5


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 15
    const-string v0, "com.lge.ims.service.cs.IContentShare"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/lge/ims/service/cs/IContentShare$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/cs/IContentShare;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 23
    if-nez p0, :cond_4

    #@2
    .line 24
    const/4 v0, 0x0

    #@3
    .line 30
    :goto_3
    return-object v0

    #@4
    .line 26
    :cond_4
    const-string v1, "com.lge.ims.service.cs.IContentShare"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 27
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/lge/ims/service/cs/IContentShare;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 28
    check-cast v0, Lcom/lge/ims/service/cs/IContentShare;

    #@12
    goto :goto_3

    #@13
    .line 30
    :cond_13
    new-instance v0, Lcom/lge/ims/service/cs/IContentShare$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/lge/ims/service/cs/IContentShare$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 11
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 38
    sparse-switch p1, :sswitch_data_da

    #@5
    .line 136
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v4

    #@9
    :goto_9
    return v4

    #@a
    .line 42
    :sswitch_a
    const-string v3, "com.lge.ims.service.cs.IContentShare"

    #@c
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 47
    :sswitch_10
    const-string v3, "com.lge.ims.service.cs.IContentShare"

    #@12
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@18
    move-result-object v3

    #@19
    invoke-static {v3}, Lcom/lge/ims/service/cs/IContentShareListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/cs/IContentShareListener;

    #@1c
    move-result-object v0

    #@1d
    .line 50
    .local v0, _arg0:Lcom/lge/ims/service/cs/IContentShareListener;
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/cs/IContentShare$Stub;->setListener(Lcom/lge/ims/service/cs/IContentShareListener;)Z

    #@20
    move-result v2

    #@21
    .line 51
    .local v2, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@24
    .line 52
    if-eqz v2, :cond_2b

    #@26
    move v3, v4

    #@27
    :goto_27
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@2a
    goto :goto_9

    #@2b
    :cond_2b
    const/4 v3, 0x0

    #@2c
    goto :goto_27

    #@2d
    .line 57
    .end local v0           #_arg0:Lcom/lge/ims/service/cs/IContentShareListener;
    .end local v2           #_result:Z
    :sswitch_2d
    const-string v3, "com.lge.ims.service.cs.IContentShare"

    #@2f
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@32
    .line 59
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@35
    move-result-object v0

    #@36
    .line 61
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@39
    move-result v1

    #@3a
    .line 62
    .local v1, _arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/service/cs/IContentShare$Stub;->addCall(Ljava/lang/String;I)V

    #@3d
    .line 63
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@40
    goto :goto_9

    #@41
    .line 68
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:I
    :sswitch_41
    const-string v3, "com.lge.ims.service.cs.IContentShare"

    #@43
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@46
    .line 70
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@49
    move-result-object v0

    #@4a
    .line 71
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/cs/IContentShare$Stub;->removeCall(Ljava/lang/String;)V

    #@4d
    .line 72
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@50
    goto :goto_9

    #@51
    .line 77
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_51
    const-string v3, "com.lge.ims.service.cs.IContentShare"

    #@53
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@56
    .line 79
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@59
    move-result-object v0

    #@5a
    .line 80
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/cs/IContentShare$Stub;->holdCall(Ljava/lang/String;)V

    #@5d
    .line 81
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@60
    goto :goto_9

    #@61
    .line 86
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_61
    const-string v3, "com.lge.ims.service.cs.IContentShare"

    #@63
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@66
    .line 88
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@69
    move-result-object v0

    #@6a
    .line 89
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/cs/IContentShare$Stub;->unholdCall(Ljava/lang/String;)V

    #@6d
    .line 90
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@70
    goto :goto_9

    #@71
    .line 95
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_71
    const-string v5, "com.lge.ims.service.cs.IContentShare"

    #@73
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@76
    .line 96
    invoke-virtual {p0}, Lcom/lge/ims/service/cs/IContentShare$Stub;->openVideoSession()Lcom/lge/ims/service/cs/IVideoSession;

    #@79
    move-result-object v2

    #@7a
    .line 97
    .local v2, _result:Lcom/lge/ims/service/cs/IVideoSession;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@7d
    .line 98
    if-eqz v2, :cond_83

    #@7f
    invoke-interface {v2}, Lcom/lge/ims/service/cs/IVideoSession;->asBinder()Landroid/os/IBinder;

    #@82
    move-result-object v3

    #@83
    :cond_83
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@86
    goto :goto_9

    #@87
    .line 103
    .end local v2           #_result:Lcom/lge/ims/service/cs/IVideoSession;
    :sswitch_87
    const-string v3, "com.lge.ims.service.cs.IContentShare"

    #@89
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@8c
    .line 105
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@8f
    move-result-object v3

    #@90
    invoke-static {v3}, Lcom/lge/ims/service/cs/IVideoSession$Stub;->asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/cs/IVideoSession;

    #@93
    move-result-object v0

    #@94
    .line 106
    .local v0, _arg0:Lcom/lge/ims/service/cs/IVideoSession;
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/cs/IContentShare$Stub;->closeVideoSession(Lcom/lge/ims/service/cs/IVideoSession;)V

    #@97
    .line 107
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@9a
    goto/16 :goto_9

    #@9c
    .line 112
    .end local v0           #_arg0:Lcom/lge/ims/service/cs/IVideoSession;
    :sswitch_9c
    const-string v5, "com.lge.ims.service.cs.IContentShare"

    #@9e
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a1
    .line 113
    invoke-virtual {p0}, Lcom/lge/ims/service/cs/IContentShare$Stub;->openImageSession()Lcom/lge/ims/service/cs/IImageSession;

    #@a4
    move-result-object v2

    #@a5
    .line 114
    .local v2, _result:Lcom/lge/ims/service/cs/IImageSession;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@a8
    .line 115
    if-eqz v2, :cond_ae

    #@aa
    invoke-interface {v2}, Lcom/lge/ims/service/cs/IImageSession;->asBinder()Landroid/os/IBinder;

    #@ad
    move-result-object v3

    #@ae
    :cond_ae
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@b1
    goto/16 :goto_9

    #@b3
    .line 120
    .end local v2           #_result:Lcom/lge/ims/service/cs/IImageSession;
    :sswitch_b3
    const-string v3, "com.lge.ims.service.cs.IContentShare"

    #@b5
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b8
    .line 122
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@bb
    move-result-object v3

    #@bc
    invoke-static {v3}, Lcom/lge/ims/service/cs/IImageSession$Stub;->asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/cs/IImageSession;

    #@bf
    move-result-object v0

    #@c0
    .line 123
    .local v0, _arg0:Lcom/lge/ims/service/cs/IImageSession;
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/cs/IContentShare$Stub;->closeImageSession(Lcom/lge/ims/service/cs/IImageSession;)V

    #@c3
    .line 124
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@c6
    goto/16 :goto_9

    #@c8
    .line 129
    .end local v0           #_arg0:Lcom/lge/ims/service/cs/IImageSession;
    :sswitch_c8
    const-string v3, "com.lge.ims.service.cs.IContentShare"

    #@ca
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@cd
    .line 130
    invoke-virtual {p0}, Lcom/lge/ims/service/cs/IContentShare$Stub;->getServiceNetworkType()I

    #@d0
    move-result v2

    #@d1
    .line 131
    .local v2, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@d4
    .line 132
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@d7
    goto/16 :goto_9

    #@d9
    .line 38
    nop

    #@da
    :sswitch_data_da
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_2d
        0x3 -> :sswitch_41
        0x4 -> :sswitch_51
        0x5 -> :sswitch_61
        0x6 -> :sswitch_71
        0x7 -> :sswitch_87
        0x8 -> :sswitch_9c
        0x9 -> :sswitch_b3
        0xa -> :sswitch_c8
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
