.class public Lcom/lge/ims/service/ac/ACHttpConnection;
.super Ljava/lang/Object;
.source "ACHttpConnection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = true

.field private static final DO_NOT_VERIFY:Ljavax/net/ssl/HostnameVerifier; = null

.field public static final RESULT_PERMANENTLY_UNAVAILABLE:I = -0x2

.field public static final RESULT_SUCCESS:I = 0x0

.field public static final RESULT_TEMPORARILY_UNAVAILABLE:I = -0x1

.field private static final SW_VERSION:Ljava/lang/String; = null

.field private static final TAG:Ljava/lang/String; = "ACHttpConnection"


# instance fields
.field private mAKASupportedForAuthType:Z

.field private mConnection:Ljava/net/HttpURLConnection;

.field private mCredentials:Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;

.field private mHost:Ljava/lang/String;

.field private mRetryAfter:I

.field private mUrl:Ljava/net/URL;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 43
    const-string v0, "ro.lge.swversion"

    #@2
    const-string v1, "unknown"

    #@4
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    sput-object v0, Lcom/lge/ims/service/ac/ACHttpConnection;->SW_VERSION:Ljava/lang/String;

    #@a
    .line 558
    new-instance v0, Lcom/lge/ims/service/ac/ACHttpConnection$2;

    #@c
    invoke-direct {v0}, Lcom/lge/ims/service/ac/ACHttpConnection$2;-><init>()V

    #@f
    sput-object v0, Lcom/lge/ims/service/ac/ACHttpConnection;->DO_NOT_VERIFY:Ljavax/net/ssl/HostnameVerifier;

    #@11
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 6
    .parameter "url"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 54
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 45
    iput-object v1, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mUrl:Ljava/net/URL;

    #@7
    .line 46
    iput-object v1, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@9
    .line 47
    iput-object v1, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mCredentials:Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;

    #@b
    .line 48
    iput v2, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mRetryAfter:I

    #@d
    .line 49
    iput-object v1, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mHost:Ljava/lang/String;

    #@f
    .line 50
    iput-boolean v2, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mAKASupportedForAuthType:Z

    #@11
    .line 56
    :try_start_11
    new-instance v1, Ljava/net/URL;

    #@13
    invoke-direct {v1, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    #@16
    iput-object v1, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mUrl:Ljava/net/URL;
    :try_end_18
    .catch Ljava/net/MalformedURLException; {:try_start_11 .. :try_end_18} :catch_19

    #@18
    .line 61
    :goto_18
    return-void

    #@19
    .line 57
    :catch_19
    move-exception v0

    #@1a
    .line 58
    .local v0, e:Ljava/net/MalformedURLException;
    const-string v1, "ACHttpConnection"

    #@1c
    new-instance v2, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v3, "MalformedURLException :: "

    #@23
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v0}, Ljava/net/MalformedURLException;->toString()Ljava/lang/String;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v2

    #@33
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@36
    .line 59
    invoke-virtual {v0}, Ljava/net/MalformedURLException;->printStackTrace()V

    #@39
    goto :goto_18
.end method

.method private static displayHeaders(Ljava/net/HttpURLConnection;Z)V
    .registers 12
    .parameter "connection"
    .parameter "isRequest"

    #@0
    .prologue
    .line 483
    if-nez p0, :cond_3

    #@2
    .line 527
    :goto_2
    return-void

    #@3
    .line 487
    :cond_3
    if-eqz p1, :cond_5c

    #@5
    .line 488
    const-string v7, "ACHttpConnection"

    #@7
    const-string v8, "REQUEST HEADERS -- starts"

    #@9
    invoke-static {v7, v8}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@c
    .line 490
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getRequestProperties()Ljava/util/Map;

    #@f
    move-result-object v4

    #@10
    .line 492
    .local v4, properties:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    if-eqz v4, :cond_54

    #@12
    .line 493
    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    #@15
    move-result-object v2

    #@16
    .line 495
    .local v2, names:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v2, :cond_54

    #@18
    .line 496
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@1b
    move-result-object v0

    #@1c
    .line 498
    .local v0, iterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_1c
    :goto_1c
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@1f
    move-result v7

    #@20
    if-eqz v7, :cond_54

    #@22
    .line 499
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@25
    move-result-object v1

    #@26
    check-cast v1, Ljava/lang/String;

    #@28
    .line 500
    .local v1, name:Ljava/lang/String;
    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@2b
    move-result-object v6

    #@2c
    check-cast v6, Ljava/util/List;

    #@2e
    .line 502
    .local v6, values:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v6, :cond_1c

    #@30
    .line 506
    const-string v8, "ACHttpConnection"

    #@32
    new-instance v7, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v7

    #@3b
    const-string v9, ": "

    #@3d
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v9

    #@41
    const/4 v7, 0x0

    #@42
    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@45
    move-result-object v7

    #@46
    check-cast v7, Ljava/lang/String;

    #@48
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v7

    #@4c
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v7

    #@50
    invoke-static {v8, v7}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@53
    goto :goto_1c

    #@54
    .line 511
    .end local v0           #iterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v1           #name:Ljava/lang/String;
    .end local v2           #names:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    .end local v6           #values:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :cond_54
    const-string v7, "ACHttpConnection"

    #@56
    const-string v8, "REQUEST HEADERS -- ends"

    #@58
    invoke-static {v7, v8}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@5b
    goto :goto_2

    #@5c
    .line 513
    .end local v4           #properties:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    :cond_5c
    const-string v7, "ACHttpConnection"

    #@5e
    const-string v8, "RESPONSE HEADERS -- starts"

    #@60
    invoke-static {v7, v8}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@63
    .line 515
    const/4 v3, 0x0

    #@64
    .line 516
    .local v3, pos:I
    const/4 v1, 0x0

    #@65
    .line 517
    .restart local v1       #name:Ljava/lang/String;
    const/4 v5, 0x0

    #@66
    .line 520
    .local v5, value:Ljava/lang/String;
    :goto_66
    invoke-virtual {p0, v3}, Ljava/net/HttpURLConnection;->getHeaderFieldKey(I)Ljava/lang/String;

    #@69
    move-result-object v1

    #@6a
    if-eqz v1, :cond_91

    #@6c
    invoke-virtual {p0, v3}, Ljava/net/HttpURLConnection;->getHeaderField(I)Ljava/lang/String;

    #@6f
    move-result-object v5

    #@70
    if-eqz v5, :cond_91

    #@72
    .line 521
    const-string v7, "ACHttpConnection"

    #@74
    new-instance v8, Ljava/lang/StringBuilder;

    #@76
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@79
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v8

    #@7d
    const-string v9, ": "

    #@7f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v8

    #@83
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v8

    #@87
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8a
    move-result-object v8

    #@8b
    invoke-static {v7, v8}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@8e
    .line 522
    add-int/lit8 v3, v3, 0x1

    #@90
    goto :goto_66

    #@91
    .line 525
    :cond_91
    const-string v7, "ACHttpConnection"

    #@93
    const-string v8, "RESPONSE HEADERS -- ends"

    #@95
    invoke-static {v7, v8}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@98
    goto/16 :goto_2
.end method

.method private static isValidContentType(Ljava/lang/String;)Z
    .registers 7
    .parameter "contentType"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 449
    if-nez p0, :cond_5

    #@4
    .line 479
    :goto_4
    return v2

    #@5
    .line 453
    :cond_5
    const-string v4, "\\p{Space}"

    #@7
    const-string v5, ""

    #@9
    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@c
    move-result-object v1

    #@d
    .line 454
    .local v1, value:Ljava/lang/String;
    const-string v4, ";"

    #@f
    invoke-virtual {v1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    .line 456
    .local v0, tokens:[Ljava/lang/String;
    if-nez v0, :cond_2e

    #@15
    .line 457
    const-string v3, "ACHttpConnection"

    #@17
    new-instance v4, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v5, "Content-Type is invalid - "

    #@1e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v4

    #@22
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v4

    #@26
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v4

    #@2a
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@2d
    goto :goto_4

    #@2e
    .line 461
    :cond_2e
    array-length v4, v0

    #@2f
    if-nez v4, :cond_4a

    #@31
    .line 462
    const-string v3, "ACHttpConnection"

    #@33
    new-instance v4, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v5, "Content-Type is invalid - "

    #@3a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v4

    #@3e
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v4

    #@42
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v4

    #@46
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@49
    goto :goto_4

    #@4a
    .line 466
    :cond_4a
    sget-object v4, Lcom/lge/ims/Configuration;->OPERATOR:Ljava/lang/String;

    #@4c
    const-string v5, "KT"

    #@4e
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@51
    move-result v4

    #@52
    if-eqz v4, :cond_60

    #@54
    .line 467
    aget-object v4, v0, v2

    #@56
    const-string v5, "application/wap-provisioningdoc+xml"

    #@58
    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@5b
    move-result v4

    #@5c
    if-eqz v4, :cond_80

    #@5e
    move v2, v3

    #@5f
    .line 468
    goto :goto_4

    #@60
    .line 470
    :cond_60
    sget-object v4, Lcom/lge/ims/Configuration;->OPERATOR:Ljava/lang/String;

    #@62
    const-string v5, "SKT"

    #@64
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@67
    move-result v4

    #@68
    if-eqz v4, :cond_80

    #@6a
    .line 471
    aget-object v4, v0, v2

    #@6c
    const-string v5, "application/volte-config-info+xml"

    #@6e
    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@71
    move-result v4

    #@72
    if-nez v4, :cond_7e

    #@74
    aget-object v4, v0, v2

    #@76
    const-string v5, "application/wap-provisioningdoc+xml"

    #@78
    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@7b
    move-result v4

    #@7c
    if-eqz v4, :cond_80

    #@7e
    :cond_7e
    move v2, v3

    #@7f
    .line 473
    goto :goto_4

    #@80
    .line 477
    :cond_80
    const-string v3, "ACHttpConnection"

    #@82
    new-instance v4, Ljava/lang/StringBuilder;

    #@84
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@87
    const-string v5, "Content-Type is invalid - "

    #@89
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v4

    #@8d
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v4

    #@91
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v4

    #@95
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@98
    goto/16 :goto_4
.end method

.method private processContent(Ljava/io/InputStream;)Z
    .registers 7
    .parameter "is"

    #@0
    .prologue
    .line 305
    invoke-static {}, Lcom/lge/ims/service/ac/ACContentParser;->getInstance()Lcom/lge/ims/service/ac/ACContentParser;

    #@3
    move-result-object v0

    #@4
    .line 308
    .local v0, contentParser:Lcom/lge/ims/service/ac/ACContentParser;
    :try_start_4
    invoke-virtual {v0, p1}, Lcom/lge/ims/service/ac/ACContentParser;->parse(Ljava/io/InputStream;)V
    :try_end_7
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4 .. :try_end_7} :catch_f
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_7} :catch_30

    #@7
    .line 317
    :goto_7
    invoke-virtual {v0}, Lcom/lge/ims/service/ac/ACContentParser;->hasValidContent()Z

    #@a
    move-result v2

    #@b
    if-eqz v2, :cond_51

    #@d
    .line 318
    const/4 v2, 0x1

    #@e
    .line 321
    :goto_e
    return v2

    #@f
    .line 309
    :catch_f
    move-exception v1

    #@10
    .line 310
    .local v1, e:Lorg/xmlpull/v1/XmlPullParserException;
    const-string v2, "ACHttpConnection"

    #@12
    new-instance v3, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v4, "XmlPullParserException :: "

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserException;->toString()Ljava/lang/String;

    #@20
    move-result-object v4

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v3

    #@29
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@2c
    .line 311
    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    #@2f
    goto :goto_7

    #@30
    .line 312
    .end local v1           #e:Lorg/xmlpull/v1/XmlPullParserException;
    :catch_30
    move-exception v1

    #@31
    .line 313
    .local v1, e:Ljava/lang/Exception;
    const-string v2, "ACHttpConnection"

    #@33
    new-instance v3, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v4, "Exception :: "

    #@3a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v3

    #@3e
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@41
    move-result-object v4

    #@42
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v3

    #@46
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v3

    #@4a
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@4d
    .line 314
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    #@50
    goto :goto_7

    #@51
    .line 321
    .end local v1           #e:Ljava/lang/Exception;
    :cond_51
    const/4 v2, 0x0

    #@52
    goto :goto_e
.end method

.method private setExtraHeaders(Ljava/lang/String;)V
    .registers 12
    .parameter "serviceType"

    #@0
    .prologue
    .line 325
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@2
    if-nez v6, :cond_5

    #@4
    .line 421
    :cond_4
    :goto_4
    return-void

    #@5
    .line 329
    :cond_5
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mCredentials:Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;

    #@7
    if-eqz v6, :cond_34

    #@9
    .line 331
    const-string v6, "ACHttpConnection"

    #@b
    new-instance v7, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v8, "Credential :: "

    #@12
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v7

    #@16
    iget-object v8, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mCredentials:Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;

    #@18
    invoke-virtual {v8}, Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;->toString()Ljava/lang/String;

    #@1b
    move-result-object v8

    #@1c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v7

    #@20
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v7

    #@24
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@27
    .line 334
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@29
    const-string v7, "Authorization"

    #@2b
    iget-object v8, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mCredentials:Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;

    #@2d
    invoke-virtual {v8}, Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;->toString()Ljava/lang/String;

    #@30
    move-result-object v8

    #@31
    invoke-virtual {v6, v7, v8}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@34
    .line 337
    :cond_34
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mHost:Ljava/lang/String;

    #@36
    if-eqz v6, :cond_e9

    #@38
    .line 338
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@3a
    const-string v7, "Host"

    #@3c
    iget-object v8, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mHost:Ljava/lang/String;

    #@3e
    invoke-virtual {v6, v7, v8}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@41
    .line 343
    :goto_41
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@43
    const-string v7, "Connection"

    #@45
    const-string v8, "close"

    #@47
    invoke-virtual {v6, v7, v8}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@4a
    .line 344
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@4c
    const-string v7, "Content-Length"

    #@4e
    const-string v8, "0"

    #@50
    invoke-virtual {v6, v7, v8}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@53
    .line 346
    sget-object v6, Lcom/lge/ims/Configuration;->OPERATOR:Ljava/lang/String;

    #@55
    const-string v7, "KT"

    #@57
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5a
    move-result v6

    #@5b
    if-eqz v6, :cond_135

    #@5d
    .line 347
    new-instance v6, Ljava/lang/StringBuilder;

    #@5f
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@62
    const-string v7, "KT-client/VoLTE+PSVT;"

    #@64
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v6

    #@68
    sget-object v7, Lcom/lge/ims/Configuration;->MODEL:Ljava/lang/String;

    #@6a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v6

    #@6e
    const-string v7, ";Device_Type=Android_Phone"

    #@70
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v6

    #@74
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@77
    move-result-object v5

    #@78
    .line 348
    .local v5, userAgent:Ljava/lang/String;
    invoke-static {}, Lcom/lge/ims/PhoneStateTracker;->getInstance()Lcom/lge/ims/PhoneStateTracker;

    #@7b
    move-result-object v4

    #@7c
    .line 350
    .local v4, pst:Lcom/lge/ims/PhoneStateTracker;
    if-eqz v4, :cond_8a

    #@7e
    .line 351
    invoke-virtual {v4}, Lcom/lge/ims/PhoneStateTracker;->is3G()Z

    #@81
    move-result v6

    #@82
    if-eqz v6, :cond_f8

    #@84
    .line 352
    const-string v6, ";Net_Type=3G"

    #@86
    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@89
    move-result-object v5

    #@8a
    .line 358
    :cond_8a
    :goto_8a
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@8c
    const-string v7, "User-Agent"

    #@8e
    invoke-virtual {v6, v7, v5}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@91
    .line 360
    invoke-static {}, Lcom/lge/ims/PhoneInfo;->getPhoneNumber()Ljava/lang/String;

    #@94
    move-result-object v2

    #@95
    .line 363
    .local v2, msisdn:Ljava/lang/String;
    :try_start_95
    invoke-static {}, Lcom/lge/ims/service/ac/ACConfiguration;->isUsimOn()Z

    #@98
    move-result v6

    #@99
    if-nez v6, :cond_b9

    #@9b
    .line 364
    invoke-static {}, Lcom/lge/ims/service/ac/ACConfiguration;->getPhoneNumber()Ljava/lang/String;

    #@9e
    move-result-object v2

    #@9f
    .line 366
    if-eqz v2, :cond_b9

    #@a1
    .line 367
    const/4 v6, 0x1

    #@a2
    invoke-virtual {v2, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@a5
    move-result-object v2

    #@a6
    .line 368
    new-instance v6, Ljava/lang/StringBuilder;

    #@a8
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@ab
    const-string v7, "82"

    #@ad
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v6

    #@b1
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v6

    #@b5
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b8
    move-result-object v2

    #@b9
    .line 372
    :cond_b9
    if-eqz v2, :cond_c8

    #@bb
    .line 373
    const-string v6, "+"

    #@bd
    invoke-virtual {v2, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@c0
    move-result v6

    #@c1
    if-eqz v6, :cond_c8

    #@c3
    .line 374
    const/4 v6, 0x1

    #@c4
    invoke-virtual {v2, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;
    :try_end_c7
    .catch Ljava/lang/Exception; {:try_start_95 .. :try_end_c7} :catch_ff

    #@c7
    move-result-object v2

    #@c8
    .line 383
    :cond_c8
    :goto_c8
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@ca
    const-string v7, "X-RCS-USER-ID"

    #@cc
    invoke-virtual {v6, v7, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@cf
    .line 385
    if-nez p1, :cond_122

    #@d1
    .line 386
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@d3
    const-string v7, "X-RCS-SERVICE-TYPE"

    #@d5
    const-string v8, "VOLTE"

    #@d7
    invoke-virtual {v6, v7, v8}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@da
    .line 391
    :goto_da
    invoke-static {}, Lcom/lge/ims/PhoneInfo;->getDeviceId()Ljava/lang/String;

    #@dd
    move-result-object v1

    #@de
    .line 393
    .local v1, imei:Ljava/lang/String;
    if-eqz v1, :cond_12a

    #@e0
    .line 394
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@e2
    const-string v7, "X-RCS-SERVICE-IMEI"

    #@e4
    invoke-virtual {v6, v7, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@e7
    goto/16 :goto_4

    #@e9
    .line 340
    .end local v1           #imei:Ljava/lang/String;
    .end local v2           #msisdn:Ljava/lang/String;
    .end local v4           #pst:Lcom/lge/ims/PhoneStateTracker;
    .end local v5           #userAgent:Ljava/lang/String;
    :cond_e9
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@eb
    const-string v7, "Host"

    #@ed
    iget-object v8, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mUrl:Ljava/net/URL;

    #@ef
    invoke-virtual {v8}, Ljava/net/URL;->getHost()Ljava/lang/String;

    #@f2
    move-result-object v8

    #@f3
    invoke-virtual {v6, v7, v8}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@f6
    goto/16 :goto_41

    #@f8
    .line 354
    .restart local v4       #pst:Lcom/lge/ims/PhoneStateTracker;
    .restart local v5       #userAgent:Ljava/lang/String;
    :cond_f8
    const-string v6, ";Net_Type=LTE"

    #@fa
    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@fd
    move-result-object v5

    #@fe
    goto :goto_8a

    #@ff
    .line 377
    .restart local v2       #msisdn:Ljava/lang/String;
    :catch_ff
    move-exception v0

    #@100
    .line 378
    .local v0, e:Ljava/lang/Exception;
    const-string v6, "ACHttpConnection"

    #@102
    new-instance v7, Ljava/lang/StringBuilder;

    #@104
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@107
    const-string v8, "Exception :: "

    #@109
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10c
    move-result-object v7

    #@10d
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@110
    move-result-object v8

    #@111
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@114
    move-result-object v7

    #@115
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@118
    move-result-object v7

    #@119
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@11c
    .line 379
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@11f
    .line 380
    const-string v2, ""

    #@121
    goto :goto_c8

    #@122
    .line 388
    .end local v0           #e:Ljava/lang/Exception;
    :cond_122
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@124
    const-string v7, "X-RCS-SERVICE-TYPE"

    #@126
    invoke-virtual {v6, v7, p1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@129
    goto :goto_da

    #@12a
    .line 396
    .restart local v1       #imei:Ljava/lang/String;
    :cond_12a
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@12c
    const-string v7, "X-RCS-SERVICE-IMEI"

    #@12e
    const-string v8, "000000000000000"

    #@130
    invoke-virtual {v6, v7, v8}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@133
    goto/16 :goto_4

    #@135
    .line 398
    .end local v1           #imei:Ljava/lang/String;
    .end local v2           #msisdn:Ljava/lang/String;
    .end local v4           #pst:Lcom/lge/ims/PhoneStateTracker;
    .end local v5           #userAgent:Ljava/lang/String;
    :cond_135
    sget-object v6, Lcom/lge/ims/Configuration;->OPERATOR:Ljava/lang/String;

    #@137
    const-string v7, "SKT"

    #@139
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13c
    move-result v6

    #@13d
    if-eqz v6, :cond_4

    #@13f
    .line 399
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@141
    const-string v7, "User-Agent"

    #@143
    new-instance v8, Ljava/lang/StringBuilder;

    #@145
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@148
    sget-object v9, Lcom/lge/ims/Configuration;->MODEL:Ljava/lang/String;

    #@14a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14d
    move-result-object v8

    #@14e
    const-string v9, "/"

    #@150
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@153
    move-result-object v8

    #@154
    sget-object v9, Lcom/lge/ims/service/ac/ACHttpConnection;->SW_VERSION:Ljava/lang/String;

    #@156
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@159
    move-result-object v8

    #@15a
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15d
    move-result-object v8

    #@15e
    invoke-virtual {v6, v7, v8}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@161
    .line 401
    invoke-static {}, Lcom/lge/ims/PhoneInfo;->getPhoneNumberExcludingNationalPrefix()Ljava/lang/String;

    #@164
    move-result-object v3

    #@165
    .line 403
    .local v3, phoneNumber:Ljava/lang/String;
    invoke-static {}, Lcom/lge/ims/service/ac/ACConfiguration;->isUsimOn()Z

    #@168
    move-result v6

    #@169
    if-nez v6, :cond_16f

    #@16b
    .line 404
    invoke-static {}, Lcom/lge/ims/service/ac/ACConfiguration;->getPhoneNumber()Ljava/lang/String;

    #@16e
    move-result-object v3

    #@16f
    .line 407
    :cond_16f
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@171
    const-string v7, "X-VOLTE-USER-ID"

    #@173
    invoke-virtual {v6, v7, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@176
    .line 409
    if-nez p1, :cond_190

    #@178
    .line 410
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@17a
    const-string v7, "X-SERVICE-ID"

    #@17c
    const-string v8, "VOLTE"

    #@17e
    invoke-virtual {v6, v7, v8}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@181
    .line 415
    :goto_181
    iget-boolean v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mAKASupportedForAuthType:Z

    #@183
    if-eqz v6, :cond_198

    #@185
    .line 416
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@187
    const-string v7, "X-IMS-AUTH-TYPE"

    #@189
    const-string v8, "AKA"

    #@18b
    invoke-virtual {v6, v7, v8}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@18e
    goto/16 :goto_4

    #@190
    .line 412
    :cond_190
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@192
    const-string v7, "X-SERVICE-ID"

    #@194
    invoke-virtual {v6, v7, p1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@197
    goto :goto_181

    #@198
    .line 418
    :cond_198
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@19a
    const-string v7, "X-IMS-AUTH-TYPE"

    #@19c
    const-string v8, "Digest"

    #@19e
    invoke-virtual {v6, v7, v8}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@1a1
    goto/16 :goto_4
.end method

.method private static trustAllHosts()V
    .registers 5

    #@0
    .prologue
    .line 531
    const/4 v3, 0x1

    #@1
    new-array v2, v3, [Ljavax/net/ssl/TrustManager;

    #@3
    const/4 v3, 0x0

    #@4
    new-instance v4, Lcom/lge/ims/service/ac/ACHttpConnection$1;

    #@6
    invoke-direct {v4}, Lcom/lge/ims/service/ac/ACHttpConnection$1;-><init>()V

    #@9
    aput-object v4, v2, v3

    #@b
    .line 549
    .local v2, trustAllCerts:[Ljavax/net/ssl/TrustManager;
    :try_start_b
    const-string v3, "TLS"

    #@d
    invoke-static {v3}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    #@10
    move-result-object v1

    #@11
    .line 550
    .local v1, sc:Ljavax/net/ssl/SSLContext;
    const/4 v3, 0x0

    #@12
    new-instance v4, Ljava/security/SecureRandom;

    #@14
    invoke-direct {v4}, Ljava/security/SecureRandom;-><init>()V

    #@17
    invoke-virtual {v1, v3, v2, v4}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    #@1a
    .line 551
    invoke-virtual {v1}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    #@1d
    move-result-object v3

    #@1e
    invoke-static {v3}, Ljavax/net/ssl/HttpsURLConnection;->setDefaultSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V
    :try_end_21
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_21} :catch_22

    #@21
    .line 556
    .end local v1           #sc:Ljavax/net/ssl/SSLContext;
    :goto_21
    return-void

    #@22
    .line 552
    :catch_22
    move-exception v0

    #@23
    .line 553
    .local v0, e:Ljava/lang/Exception;
    const-string v3, "ACHttpConnection"

    #@25
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@28
    move-result-object v4

    #@29
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@2c
    .line 554
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@2f
    goto :goto_21
.end method

.method private updateRouteTable()V
    .registers 7

    #@0
    .prologue
    .line 425
    const-string v3, "ACHttpConnection"

    #@2
    new-instance v4, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v5, "updateRouteTable :: host="

    #@9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v4

    #@d
    iget-object v5, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mUrl:Ljava/net/URL;

    #@f
    invoke-virtual {v5}, Ljava/net/URL;->getHost()Ljava/lang/String;

    #@12
    move-result-object v5

    #@13
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v4

    #@1b
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@1e
    .line 428
    const/4 v0, 0x0

    #@1f
    .line 431
    .local v0, address:Ljava/net/InetAddress;
    :try_start_1f
    iget-object v3, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mUrl:Ljava/net/URL;

    #@21
    invoke-virtual {v3}, Ljava/net/URL;->getHost()Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    invoke-static {v3}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;
    :try_end_28
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_28} :catch_40

    #@28
    move-result-object v0

    #@29
    .line 437
    :goto_29
    if-eqz v0, :cond_3f

    #@2b
    .line 438
    invoke-static {}, Lcom/lge/ims/DataConnectionManager;->getInstance()Lcom/lge/ims/DataConnectionManager;

    #@2e
    move-result-object v1

    #@2f
    .line 440
    .local v1, dcm:Lcom/lge/ims/DataConnectionManager;
    if-eqz v1, :cond_39

    #@31
    .line 441
    const/4 v3, 0x1

    #@32
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@35
    move-result-object v4

    #@36
    invoke-virtual {v1, v3, v4}, Lcom/lge/ims/DataConnectionManager;->requestRouteToHostAddress(ILjava/lang/String;)Z

    #@39
    .line 444
    :cond_39
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@3c
    move-result-object v3

    #@3d
    iput-object v3, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mHost:Ljava/lang/String;

    #@3f
    .line 446
    .end local v1           #dcm:Lcom/lge/ims/DataConnectionManager;
    :cond_3f
    return-void

    #@40
    .line 432
    :catch_40
    move-exception v2

    #@41
    .line 433
    .local v2, e:Ljava/lang/Exception;
    const-string v3, "ACHttpConnection"

    #@43
    new-instance v4, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v5, "Exception :: "

    #@4a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v4

    #@4e
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@51
    move-result-object v5

    #@52
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v4

    #@56
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v4

    #@5a
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@5d
    .line 434
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    #@60
    goto :goto_29
.end method


# virtual methods
.method public close()V
    .registers 2

    #@0
    .prologue
    .line 64
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 65
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@6
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    #@9
    .line 66
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@c
    .line 68
    :cond_c
    return-void
.end method

.method public getNonce()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mCredentials:Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 72
    const/4 v0, 0x0

    #@5
    .line 75
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mCredentials:Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;

    #@8
    invoke-virtual {v0}, Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;->getNonce()Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method public getRetryAfter()I
    .registers 2

    #@0
    .prologue
    .line 79
    iget v0, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mRetryAfter:I

    #@2
    return v0
.end method

.method public join(Ljava/lang/String;)I
    .registers 13
    .parameter "serviceType"

    #@0
    .prologue
    const/4 v7, -0x1

    #@1
    const/4 v10, 0x0

    #@2
    .line 83
    const-string v6, "ACHttpConnection"

    #@4
    new-instance v8, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v9, "join() - "

    #@b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v8

    #@f
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v8

    #@13
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v8

    #@17
    invoke-static {v6, v8}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@1a
    .line 85
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mUrl:Ljava/net/URL;

    #@1c
    if-nez v6, :cond_27

    #@1e
    .line 86
    const-string v6, "ACHttpConnection"

    #@20
    const-string v8, "URL is null"

    #@22
    invoke-static {v6, v8}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@25
    move v3, v7

    #@26
    .line 173
    :cond_26
    :goto_26
    return v3

    #@27
    .line 91
    :cond_27
    invoke-direct {p0}, Lcom/lge/ims/service/ac/ACHttpConnection;->updateRouteTable()V

    #@2a
    .line 93
    const/4 v3, 0x0

    #@2b
    .line 94
    .local v3, resultCode:I
    const-string v6, "https"

    #@2d
    iget-object v8, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mUrl:Ljava/net/URL;

    #@2f
    invoke-virtual {v8}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    #@32
    move-result-object v8

    #@33
    invoke-virtual {v6, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@36
    move-result v2

    #@37
    .line 97
    .local v2, isSecureProtocol:Z
    if-eqz v2, :cond_40

    #@39
    .line 98
    const-string v6, "ACHttpConnection"

    #@3b
    const-string v8, "Connection requires a secure protocol"

    #@3d
    invoke-static {v6, v8}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@40
    .line 104
    :cond_40
    if-eqz v2, :cond_45

    #@42
    .line 105
    :try_start_42
    invoke-static {}, Lcom/lge/ims/service/ac/ACHttpConnection;->trustAllHosts()V

    #@45
    .line 108
    :cond_45
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mUrl:Ljava/net/URL;

    #@47
    invoke-virtual {v6}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    #@4a
    move-result-object v6

    #@4b
    check-cast v6, Ljava/net/HttpURLConnection;

    #@4d
    iput-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@4f
    .line 110
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;
    :try_end_51
    .catchall {:try_start_42 .. :try_end_51} :catchall_183
    .catch Ljava/io/IOException; {:try_start_42 .. :try_end_51} :catch_11d
    .catch Ljava/lang/Exception; {:try_start_42 .. :try_end_51} :catch_155

    #@51
    if-nez v6, :cond_60

    #@53
    .line 167
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@55
    if-eqz v6, :cond_5e

    #@57
    .line 168
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@59
    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->disconnect()V

    #@5c
    .line 169
    iput-object v10, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@5e
    :cond_5e
    move v3, v7

    #@5f
    goto :goto_26

    #@60
    .line 115
    :cond_60
    if-eqz v2, :cond_6b

    #@62
    .line 116
    :try_start_62
    iget-object v1, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@64
    check-cast v1, Ljavax/net/ssl/HttpsURLConnection;

    #@66
    .line 117
    .local v1, httpsConnection:Ljavax/net/ssl/HttpsURLConnection;
    sget-object v6, Lcom/lge/ims/service/ac/ACHttpConnection;->DO_NOT_VERIFY:Ljavax/net/ssl/HostnameVerifier;

    #@68
    invoke-virtual {v1, v6}, Ljavax/net/ssl/HttpsURLConnection;->setHostnameVerifier(Ljavax/net/ssl/HostnameVerifier;)V

    #@6b
    .line 120
    .end local v1           #httpsConnection:Ljavax/net/ssl/HttpsURLConnection;
    :cond_6b
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@6d
    const-string v7, "PUT"

    #@6f
    invoke-virtual {v6, v7}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    #@72
    .line 123
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@74
    const/16 v7, 0x2710

    #@76
    invoke-virtual {v6, v7}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    #@79
    .line 124
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@7b
    const/16 v7, 0x4e20

    #@7d
    invoke-virtual {v6, v7}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    #@80
    .line 127
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ac/ACHttpConnection;->setExtraHeaders(Ljava/lang/String;)V

    #@83
    .line 130
    const-string v6, "ACHttpConnection"

    #@85
    new-instance v7, Ljava/lang/StringBuilder;

    #@87
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@8a
    const-string v8, "HttpURLConnection :: "

    #@8c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v7

    #@90
    iget-object v8, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@92
    invoke-virtual {v8}, Ljava/net/HttpURLConnection;->toString()Ljava/lang/String;

    #@95
    move-result-object v8

    #@96
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v7

    #@9a
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9d
    move-result-object v7

    #@9e
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@a1
    .line 131
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@a3
    const/4 v7, 0x1

    #@a4
    invoke-static {v6, v7}, Lcom/lge/ims/service/ac/ACHttpConnection;->displayHeaders(Ljava/net/HttpURLConnection;Z)V

    #@a7
    .line 135
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@a9
    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->connect()V

    #@ac
    .line 138
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@ae
    const/4 v7, 0x0

    #@af
    invoke-static {v6, v7}, Lcom/lge/ims/service/ac/ACHttpConnection;->displayHeaders(Ljava/net/HttpURLConnection;Z)V

    #@b2
    .line 141
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@b4
    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->getResponseCode()I

    #@b7
    move-result v3

    #@b8
    .line 143
    const-string v6, "ACHttpConnection"

    #@ba
    new-instance v7, Ljava/lang/StringBuilder;

    #@bc
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@bf
    const-string v8, "Result="

    #@c1
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v7

    #@c5
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v7

    #@c9
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cc
    move-result-object v7

    #@cd
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@d0
    .line 145
    const/16 v6, 0x1f7

    #@d2
    if-ne v3, v6, :cond_fe

    #@d4
    .line 146
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@d6
    const-string v7, "Retry-After"

    #@d8
    invoke-virtual {v6, v7}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    #@db
    move-result-object v4

    #@dc
    .line 148
    .local v4, retryAfter:Ljava/lang/String;
    if-eqz v4, :cond_f1

    #@de
    .line 149
    const-string v6, ";"

    #@e0
    invoke-virtual {v4, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@e3
    move-result-object v5

    #@e4
    .line 150
    .local v5, tokens:[Ljava/lang/String;
    const/4 v6, 0x0

    #@e5
    aget-object v6, v5, v6

    #@e7
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    #@ea
    move-result-object v6

    #@eb
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    #@ee
    move-result v6

    #@ef
    iput v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mRetryAfter:I
    :try_end_f1
    .catchall {:try_start_62 .. :try_end_f1} :catchall_183
    .catch Ljava/io/IOException; {:try_start_62 .. :try_end_f1} :catch_11d
    .catch Ljava/lang/Exception; {:try_start_62 .. :try_end_f1} :catch_155

    #@f1
    .line 167
    .end local v4           #retryAfter:Ljava/lang/String;
    .end local v5           #tokens:[Ljava/lang/String;
    :cond_f1
    :goto_f1
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@f3
    if-eqz v6, :cond_26

    #@f5
    .line 168
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@f7
    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->disconnect()V

    #@fa
    .line 169
    iput-object v10, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@fc
    goto/16 :goto_26

    #@fe
    .line 152
    :cond_fe
    const/16 v6, 0x191

    #@100
    if-ne v3, v6, :cond_14b

    #@102
    .line 153
    :try_start_102
    new-instance v6, Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;

    #@104
    iget-object v7, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mUrl:Ljava/net/URL;

    #@106
    invoke-virtual {v7}, Ljava/net/URL;->getPath()Ljava/lang/String;

    #@109
    move-result-object v7

    #@10a
    invoke-direct {v6, p0, v7}, Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;-><init>(Lcom/lge/ims/service/ac/ACHttpConnection;Ljava/lang/String;)V

    #@10d
    iput-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mCredentials:Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;

    #@10f
    .line 154
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mCredentials:Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;

    #@111
    iget-object v7, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@113
    const-string v8, "WWW-Authenticate"

    #@115
    invoke-virtual {v7, v8}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    #@118
    move-result-object v7

    #@119
    invoke-virtual {v6, v7}, Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;->parse(Ljava/lang/String;)V
    :try_end_11c
    .catchall {:try_start_102 .. :try_end_11c} :catchall_183
    .catch Ljava/io/IOException; {:try_start_102 .. :try_end_11c} :catch_11d
    .catch Ljava/lang/Exception; {:try_start_102 .. :try_end_11c} :catch_155

    #@11c
    goto :goto_f1

    #@11d
    .line 158
    :catch_11d
    move-exception v0

    #@11e
    .line 159
    .local v0, e:Ljava/io/IOException;
    :try_start_11e
    const-string v6, "ACHttpConnection"

    #@120
    new-instance v7, Ljava/lang/StringBuilder;

    #@122
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@125
    const-string v8, "IOException :: "

    #@127
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12a
    move-result-object v7

    #@12b
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    #@12e
    move-result-object v8

    #@12f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@132
    move-result-object v7

    #@133
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@136
    move-result-object v7

    #@137
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@13a
    .line 160
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_13d
    .catchall {:try_start_11e .. :try_end_13d} :catchall_183

    #@13d
    .line 161
    const/4 v3, -0x1

    #@13e
    .line 167
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@140
    if-eqz v6, :cond_26

    #@142
    .line 168
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@144
    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->disconnect()V

    #@147
    .line 169
    iput-object v10, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@149
    goto/16 :goto_26

    #@14b
    .line 155
    .end local v0           #e:Ljava/io/IOException;
    :cond_14b
    const/16 v6, 0xc8

    #@14d
    if-lt v3, v6, :cond_f1

    #@14f
    const/16 v6, 0x12c

    #@151
    if-ge v3, v6, :cond_f1

    #@153
    .line 156
    const/4 v3, 0x0

    #@154
    goto :goto_f1

    #@155
    .line 162
    :catch_155
    move-exception v0

    #@156
    .line 163
    .local v0, e:Ljava/lang/Exception;
    :try_start_156
    const-string v6, "ACHttpConnection"

    #@158
    new-instance v7, Ljava/lang/StringBuilder;

    #@15a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@15d
    const-string v8, "Exception :: "

    #@15f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@162
    move-result-object v7

    #@163
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@166
    move-result-object v8

    #@167
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16a
    move-result-object v7

    #@16b
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16e
    move-result-object v7

    #@16f
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@172
    .line 164
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_175
    .catchall {:try_start_156 .. :try_end_175} :catchall_183

    #@175
    .line 165
    const/4 v3, -0x1

    #@176
    .line 167
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@178
    if-eqz v6, :cond_26

    #@17a
    .line 168
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@17c
    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->disconnect()V

    #@17f
    .line 169
    iput-object v10, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@181
    goto/16 :goto_26

    #@183
    .line 167
    .end local v0           #e:Ljava/lang/Exception;
    :catchall_183
    move-exception v6

    #@184
    iget-object v7, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@186
    if-eqz v7, :cond_18f

    #@188
    .line 168
    iget-object v7, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@18a
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->disconnect()V

    #@18d
    .line 169
    iput-object v10, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@18f
    :cond_18f
    throw v6
.end method

.method public send()I
    .registers 15

    #@0
    .prologue
    const/4 v10, -0x1

    #@1
    const/4 v13, 0x0

    #@2
    const/4 v12, 0x1

    #@3
    .line 177
    const-string v9, "ACHttpConnection"

    #@5
    const-string v11, "send()"

    #@7
    invoke-static {v9, v11}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 179
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mUrl:Ljava/net/URL;

    #@c
    if-nez v9, :cond_17

    #@e
    .line 180
    const-string v9, "ACHttpConnection"

    #@10
    const-string v11, "URL is null"

    #@12
    invoke-static {v9, v11}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@15
    move v6, v10

    #@16
    .line 288
    :cond_16
    :goto_16
    return v6

    #@17
    .line 185
    :cond_17
    invoke-direct {p0}, Lcom/lge/ims/service/ac/ACHttpConnection;->updateRouteTable()V

    #@1a
    .line 187
    const/4 v6, 0x0

    #@1b
    .line 188
    .local v6, resultCode:I
    const-string v9, "https"

    #@1d
    iget-object v11, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mUrl:Ljava/net/URL;

    #@1f
    invoke-virtual {v11}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    #@22
    move-result-object v11

    #@23
    invoke-virtual {v9, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@26
    move-result v5

    #@27
    .line 191
    .local v5, isSecureProtocol:Z
    if-eqz v5, :cond_30

    #@29
    .line 192
    const-string v9, "ACHttpConnection"

    #@2b
    const-string v11, "Connection requires a secure protocol"

    #@2d
    invoke-static {v9, v11}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@30
    .line 198
    :cond_30
    if-eqz v5, :cond_35

    #@32
    .line 199
    :try_start_32
    invoke-static {}, Lcom/lge/ims/service/ac/ACHttpConnection;->trustAllHosts()V

    #@35
    .line 202
    :cond_35
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mUrl:Ljava/net/URL;

    #@37
    invoke-virtual {v9}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    #@3a
    move-result-object v9

    #@3b
    check-cast v9, Ljava/net/HttpURLConnection;

    #@3d
    iput-object v9, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@3f
    .line 204
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;
    :try_end_41
    .catchall {:try_start_32 .. :try_end_41} :catchall_1ce
    .catch Ljava/io/IOException; {:try_start_32 .. :try_end_41} :catch_12c
    .catch Ljava/lang/Exception; {:try_start_32 .. :try_end_41} :catch_191

    #@41
    if-nez v9, :cond_5f

    #@43
    .line 273
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@45
    if-eqz v9, :cond_4e

    #@47
    .line 274
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@49
    invoke-virtual {v9}, Ljava/net/HttpURLConnection;->disconnect()V

    #@4c
    .line 275
    iput-object v13, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@4e
    .line 279
    :cond_4e
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mHost:Ljava/lang/String;

    #@50
    if-eqz v9, :cond_5d

    #@52
    .line 280
    invoke-static {}, Lcom/lge/ims/DataConnectionManager;->getInstance()Lcom/lge/ims/DataConnectionManager;

    #@55
    move-result-object v1

    #@56
    .line 282
    .local v1, dcm:Lcom/lge/ims/DataConnectionManager;
    if-eqz v1, :cond_5d

    #@58
    .line 283
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mHost:Ljava/lang/String;

    #@5a
    invoke-virtual {v1, v12, v9}, Lcom/lge/ims/DataConnectionManager;->removeRouteToHostAddress(ILjava/lang/String;)Z

    #@5d
    .end local v1           #dcm:Lcom/lge/ims/DataConnectionManager;
    :cond_5d
    move v6, v10

    #@5e
    .line 285
    goto :goto_16

    #@5f
    .line 209
    :cond_5f
    if-eqz v5, :cond_6a

    #@61
    .line 210
    :try_start_61
    iget-object v3, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@63
    check-cast v3, Ljavax/net/ssl/HttpsURLConnection;

    #@65
    .line 211
    .local v3, httpsConnection:Ljavax/net/ssl/HttpsURLConnection;
    sget-object v9, Lcom/lge/ims/service/ac/ACHttpConnection;->DO_NOT_VERIFY:Ljavax/net/ssl/HostnameVerifier;

    #@67
    invoke-virtual {v3, v9}, Ljavax/net/ssl/HttpsURLConnection;->setHostnameVerifier(Ljavax/net/ssl/HostnameVerifier;)V

    #@6a
    .line 214
    .end local v3           #httpsConnection:Ljavax/net/ssl/HttpsURLConnection;
    :cond_6a
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@6c
    const-string v10, "GET"

    #@6e
    invoke-virtual {v9, v10}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    #@71
    .line 217
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@73
    const/16 v10, 0x2710

    #@75
    invoke-virtual {v9, v10}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    #@78
    .line 218
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@7a
    const/16 v10, 0x4e20

    #@7c
    invoke-virtual {v9, v10}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    #@7f
    .line 221
    const/4 v9, 0x0

    #@80
    invoke-direct {p0, v9}, Lcom/lge/ims/service/ac/ACHttpConnection;->setExtraHeaders(Ljava/lang/String;)V

    #@83
    .line 224
    const-string v9, "ACHttpConnection"

    #@85
    new-instance v10, Ljava/lang/StringBuilder;

    #@87
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@8a
    const-string v11, "HttpURLConnection :: "

    #@8c
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v10

    #@90
    iget-object v11, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@92
    invoke-virtual {v11}, Ljava/net/HttpURLConnection;->toString()Ljava/lang/String;

    #@95
    move-result-object v11

    #@96
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v10

    #@9a
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9d
    move-result-object v10

    #@9e
    invoke-static {v9, v10}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@a1
    .line 225
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@a3
    const/4 v10, 0x1

    #@a4
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACHttpConnection;->displayHeaders(Ljava/net/HttpURLConnection;Z)V

    #@a7
    .line 229
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@a9
    invoke-virtual {v9}, Ljava/net/HttpURLConnection;->connect()V

    #@ac
    .line 232
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@ae
    const/4 v10, 0x0

    #@af
    invoke-static {v9, v10}, Lcom/lge/ims/service/ac/ACHttpConnection;->displayHeaders(Ljava/net/HttpURLConnection;Z)V

    #@b2
    .line 235
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@b4
    invoke-virtual {v9}, Ljava/net/HttpURLConnection;->getResponseCode()I

    #@b7
    move-result v6

    #@b8
    .line 237
    const-string v9, "ACHttpConnection"

    #@ba
    new-instance v10, Ljava/lang/StringBuilder;

    #@bc
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@bf
    const-string v11, "Result="

    #@c1
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v10

    #@c5
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v10

    #@c9
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cc
    move-result-object v10

    #@cd
    invoke-static {v9, v10}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@d0
    .line 239
    const/16 v9, 0x1f7

    #@d2
    if-ne v6, v9, :cond_10d

    #@d4
    .line 240
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@d6
    const-string v10, "Retry-After"

    #@d8
    invoke-virtual {v9, v10}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    #@db
    move-result-object v7

    #@dc
    .line 242
    .local v7, retryAfter:Ljava/lang/String;
    if-eqz v7, :cond_f1

    #@de
    .line 243
    const-string v9, ";"

    #@e0
    invoke-virtual {v7, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@e3
    move-result-object v8

    #@e4
    .line 244
    .local v8, tokens:[Ljava/lang/String;
    const/4 v9, 0x0

    #@e5
    aget-object v9, v8, v9

    #@e7
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    #@ea
    move-result-object v9

    #@eb
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    #@ee
    move-result v9

    #@ef
    iput v9, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mRetryAfter:I
    :try_end_f1
    .catchall {:try_start_61 .. :try_end_f1} :catchall_1ce
    .catch Ljava/io/IOException; {:try_start_61 .. :try_end_f1} :catch_12c
    .catch Ljava/lang/Exception; {:try_start_61 .. :try_end_f1} :catch_191

    #@f1
    .line 273
    .end local v7           #retryAfter:Ljava/lang/String;
    .end local v8           #tokens:[Ljava/lang/String;
    :cond_f1
    :goto_f1
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@f3
    if-eqz v9, :cond_fc

    #@f5
    .line 274
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@f7
    invoke-virtual {v9}, Ljava/net/HttpURLConnection;->disconnect()V

    #@fa
    .line 275
    iput-object v13, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@fc
    .line 279
    :cond_fc
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mHost:Ljava/lang/String;

    #@fe
    if-eqz v9, :cond_16

    #@100
    .line 280
    invoke-static {}, Lcom/lge/ims/DataConnectionManager;->getInstance()Lcom/lge/ims/DataConnectionManager;

    #@103
    move-result-object v1

    #@104
    .line 282
    .restart local v1       #dcm:Lcom/lge/ims/DataConnectionManager;
    if-eqz v1, :cond_16

    #@106
    .line 283
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mHost:Ljava/lang/String;

    #@108
    invoke-virtual {v1, v12, v9}, Lcom/lge/ims/DataConnectionManager;->removeRouteToHostAddress(ILjava/lang/String;)Z

    #@10b
    goto/16 :goto_16

    #@10d
    .line 246
    .end local v1           #dcm:Lcom/lge/ims/DataConnectionManager;
    :cond_10d
    const/16 v9, 0x191

    #@10f
    if-ne v6, v9, :cond_169

    #@111
    .line 247
    :try_start_111
    new-instance v9, Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;

    #@113
    iget-object v10, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mUrl:Ljava/net/URL;

    #@115
    invoke-virtual {v10}, Ljava/net/URL;->getPath()Ljava/lang/String;

    #@118
    move-result-object v10

    #@119
    invoke-direct {v9, p0, v10}, Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;-><init>(Lcom/lge/ims/service/ac/ACHttpConnection;Ljava/lang/String;)V

    #@11c
    iput-object v9, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mCredentials:Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;

    #@11e
    .line 248
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mCredentials:Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;

    #@120
    iget-object v10, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@122
    const-string v11, "WWW-Authenticate"

    #@124
    invoke-virtual {v10, v11}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    #@127
    move-result-object v10

    #@128
    invoke-virtual {v9, v10}, Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;->parse(Ljava/lang/String;)V
    :try_end_12b
    .catchall {:try_start_111 .. :try_end_12b} :catchall_1ce
    .catch Ljava/io/IOException; {:try_start_111 .. :try_end_12b} :catch_12c
    .catch Ljava/lang/Exception; {:try_start_111 .. :try_end_12b} :catch_191

    #@12b
    goto :goto_f1

    #@12c
    .line 264
    :catch_12c
    move-exception v2

    #@12d
    .line 265
    .local v2, e:Ljava/io/IOException;
    :try_start_12d
    const-string v9, "ACHttpConnection"

    #@12f
    new-instance v10, Ljava/lang/StringBuilder;

    #@131
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@134
    const-string v11, "IOException :: "

    #@136
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@139
    move-result-object v10

    #@13a
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    #@13d
    move-result-object v11

    #@13e
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@141
    move-result-object v10

    #@142
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@145
    move-result-object v10

    #@146
    invoke-static {v9, v10}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@149
    .line 266
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_14c
    .catchall {:try_start_12d .. :try_end_14c} :catchall_1ce

    #@14c
    .line 267
    const/4 v6, -0x1

    #@14d
    .line 273
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@14f
    if-eqz v9, :cond_158

    #@151
    .line 274
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@153
    invoke-virtual {v9}, Ljava/net/HttpURLConnection;->disconnect()V

    #@156
    .line 275
    iput-object v13, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@158
    .line 279
    :cond_158
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mHost:Ljava/lang/String;

    #@15a
    if-eqz v9, :cond_16

    #@15c
    .line 280
    invoke-static {}, Lcom/lge/ims/DataConnectionManager;->getInstance()Lcom/lge/ims/DataConnectionManager;

    #@15f
    move-result-object v1

    #@160
    .line 282
    .restart local v1       #dcm:Lcom/lge/ims/DataConnectionManager;
    if-eqz v1, :cond_16

    #@162
    .line 283
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mHost:Ljava/lang/String;

    #@164
    invoke-virtual {v1, v12, v9}, Lcom/lge/ims/DataConnectionManager;->removeRouteToHostAddress(ILjava/lang/String;)Z

    #@167
    goto/16 :goto_16

    #@169
    .line 249
    .end local v1           #dcm:Lcom/lge/ims/DataConnectionManager;
    .end local v2           #e:Ljava/io/IOException;
    :cond_169
    const/16 v9, 0xc8

    #@16b
    if-lt v6, v9, :cond_f1

    #@16d
    const/16 v9, 0x12c

    #@16f
    if-ge v6, v9, :cond_f1

    #@171
    .line 250
    :try_start_171
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@173
    invoke-virtual {v9}, Ljava/net/HttpURLConnection;->getContentType()Ljava/lang/String;

    #@176
    move-result-object v0

    #@177
    .line 252
    .local v0, contentType:Ljava/lang/String;
    invoke-static {v0}, Lcom/lge/ims/service/ac/ACHttpConnection;->isValidContentType(Ljava/lang/String;)Z

    #@17a
    move-result v9

    #@17b
    if-eqz v9, :cond_f1

    #@17d
    .line 253
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@17f
    invoke-virtual {v9}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    #@182
    move-result-object v4

    #@183
    .line 255
    .local v4, is:Ljava/io/InputStream;
    invoke-direct {p0, v4}, Lcom/lge/ims/service/ac/ACHttpConnection;->processContent(Ljava/io/InputStream;)Z

    #@186
    move-result v9

    #@187
    if-eqz v9, :cond_18a

    #@189
    .line 256
    const/4 v6, 0x0

    #@18a
    .line 259
    :cond_18a
    if-eqz v4, :cond_f1

    #@18c
    .line 260
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_18f
    .catchall {:try_start_171 .. :try_end_18f} :catchall_1ce
    .catch Ljava/io/IOException; {:try_start_171 .. :try_end_18f} :catch_12c
    .catch Ljava/lang/Exception; {:try_start_171 .. :try_end_18f} :catch_191

    #@18f
    goto/16 :goto_f1

    #@191
    .line 268
    .end local v0           #contentType:Ljava/lang/String;
    .end local v4           #is:Ljava/io/InputStream;
    :catch_191
    move-exception v2

    #@192
    .line 269
    .local v2, e:Ljava/lang/Exception;
    :try_start_192
    const-string v9, "ACHttpConnection"

    #@194
    new-instance v10, Ljava/lang/StringBuilder;

    #@196
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@199
    const-string v11, "Exception :: "

    #@19b
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19e
    move-result-object v10

    #@19f
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@1a2
    move-result-object v11

    #@1a3
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a6
    move-result-object v10

    #@1a7
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1aa
    move-result-object v10

    #@1ab
    invoke-static {v9, v10}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@1ae
    .line 270
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1b1
    .catchall {:try_start_192 .. :try_end_1b1} :catchall_1ce

    #@1b1
    .line 271
    const/4 v6, -0x1

    #@1b2
    .line 273
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@1b4
    if-eqz v9, :cond_1bd

    #@1b6
    .line 274
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@1b8
    invoke-virtual {v9}, Ljava/net/HttpURLConnection;->disconnect()V

    #@1bb
    .line 275
    iput-object v13, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@1bd
    .line 279
    :cond_1bd
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mHost:Ljava/lang/String;

    #@1bf
    if-eqz v9, :cond_16

    #@1c1
    .line 280
    invoke-static {}, Lcom/lge/ims/DataConnectionManager;->getInstance()Lcom/lge/ims/DataConnectionManager;

    #@1c4
    move-result-object v1

    #@1c5
    .line 282
    .restart local v1       #dcm:Lcom/lge/ims/DataConnectionManager;
    if-eqz v1, :cond_16

    #@1c7
    .line 283
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mHost:Ljava/lang/String;

    #@1c9
    invoke-virtual {v1, v12, v9}, Lcom/lge/ims/DataConnectionManager;->removeRouteToHostAddress(ILjava/lang/String;)Z

    #@1cc
    goto/16 :goto_16

    #@1ce
    .line 273
    .end local v1           #dcm:Lcom/lge/ims/DataConnectionManager;
    .end local v2           #e:Ljava/lang/Exception;
    :catchall_1ce
    move-exception v9

    #@1cf
    iget-object v10, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@1d1
    if-eqz v10, :cond_1da

    #@1d3
    .line 274
    iget-object v10, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@1d5
    invoke-virtual {v10}, Ljava/net/HttpURLConnection;->disconnect()V

    #@1d8
    .line 275
    iput-object v13, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mConnection:Ljava/net/HttpURLConnection;

    #@1da
    .line 279
    :cond_1da
    iget-object v10, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mHost:Ljava/lang/String;

    #@1dc
    if-eqz v10, :cond_1e9

    #@1de
    .line 280
    invoke-static {}, Lcom/lge/ims/DataConnectionManager;->getInstance()Lcom/lge/ims/DataConnectionManager;

    #@1e1
    move-result-object v1

    #@1e2
    .line 282
    .restart local v1       #dcm:Lcom/lge/ims/DataConnectionManager;
    if-eqz v1, :cond_1e9

    #@1e4
    .line 283
    iget-object v10, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mHost:Ljava/lang/String;

    #@1e6
    invoke-virtual {v1, v12, v10}, Lcom/lge/ims/DataConnectionManager;->removeRouteToHostAddress(ILjava/lang/String;)Z

    #@1e9
    .line 285
    .end local v1           #dcm:Lcom/lge/ims/DataConnectionManager;
    :cond_1e9
    throw v9
.end method

.method public setAuthType(Z)V
    .registers 2
    .parameter "aka"

    #@0
    .prologue
    .line 292
    iput-boolean p1, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mAKASupportedForAuthType:Z

    #@2
    .line 293
    return-void
.end method

.method public setCredential(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "username"
    .parameter "response"

    #@0
    .prologue
    .line 296
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mCredentials:Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 302
    :goto_4
    return-void

    #@5
    .line 300
    :cond_5
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mCredentials:Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;

    #@7
    invoke-virtual {v0, p1}, Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;->setUsername(Ljava/lang/String;)V

    #@a
    .line 301
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACHttpConnection;->mCredentials:Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;

    #@c
    invoke-virtual {v0, p2}, Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;->setResponse(Ljava/lang/String;)V

    #@f
    goto :goto_4
.end method
