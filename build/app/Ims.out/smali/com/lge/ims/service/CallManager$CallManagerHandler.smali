.class final Lcom/lge/ims/service/CallManager$CallManagerHandler;
.super Landroid/os/Handler;
.source "CallManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/CallManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "CallManagerHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/service/CallManager;


# direct methods
.method public constructor <init>(Lcom/lge/ims/service/CallManager;Landroid/os/Looper;)V
    .registers 3
    .parameter
    .parameter "looper"

    #@0
    .prologue
    .line 508
    iput-object p1, p0, Lcom/lge/ims/service/CallManager$CallManagerHandler;->this$0:Lcom/lge/ims/service/CallManager;

    #@2
    .line 509
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 510
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 6
    .parameter "msg"

    #@0
    .prologue
    .line 514
    if-nez p1, :cond_3

    #@2
    .line 545
    :cond_2
    :goto_2
    return-void

    #@3
    .line 518
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v3, "handleMessage :: "

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    iget v3, p1, Landroid/os/Message;->what:I

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v2

    #@18
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@1b
    .line 520
    iget v2, p1, Landroid/os/Message;->what:I

    #@1d
    const/4 v3, 0x1

    #@1e
    if-ne v2, v3, :cond_30

    #@20
    .line 521
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@23
    move-result-object v0

    #@24
    .line 523
    .local v0, looper:Landroid/os/Looper;
    if-eqz v0, :cond_29

    #@26
    .line 524
    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    #@29
    .line 526
    :cond_29
    iget-object v2, p0, Lcom/lge/ims/service/CallManager$CallManagerHandler;->this$0:Lcom/lge/ims/service/CallManager;

    #@2b
    const/4 v3, 0x0

    #@2c
    invoke-static {v2, v3}, Lcom/lge/ims/service/CallManager;->access$002(Lcom/lge/ims/service/CallManager;Landroid/os/Handler;)Landroid/os/Handler;

    #@2f
    goto :goto_2

    #@30
    .line 530
    .end local v0           #looper:Landroid/os/Looper;
    :cond_30
    iget v2, p1, Landroid/os/Message;->what:I

    #@32
    packed-switch v2, :pswitch_data_5e

    #@35
    .line 542
    new-instance v2, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v3, "Unhandled Message :: "

    #@3c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v2

    #@40
    iget v3, p1, Landroid/os/Message;->what:I

    #@42
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@45
    move-result-object v2

    #@46
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v2

    #@4a
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@4d
    goto :goto_2

    #@4e
    .line 533
    :pswitch_4e
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@50
    check-cast v1, Lcom/lge/ims/service/cd/ServiceCapabilities;

    #@52
    .line 535
    .local v1, sc:Lcom/lge/ims/service/cd/ServiceCapabilities;
    if-eqz v1, :cond_2

    #@54
    .line 536
    iget-object v2, p0, Lcom/lge/ims/service/CallManager$CallManagerHandler;->this$0:Lcom/lge/ims/service/CallManager;

    #@56
    invoke-virtual {v1}, Lcom/lge/ims/service/cd/ServiceCapabilities;->toCapabilities()Lcom/lge/ims/service/cd/Capabilities;

    #@59
    move-result-object v3

    #@5a
    invoke-virtual {v2, v3}, Lcom/lge/ims/service/CallManager;->notifyCapabilities(Lcom/lge/ims/service/cd/Capabilities;)V

    #@5d
    goto :goto_2

    #@5e
    .line 530
    :pswitch_data_5e
    .packed-switch 0x32
        :pswitch_4e
    .end packed-switch
.end method
