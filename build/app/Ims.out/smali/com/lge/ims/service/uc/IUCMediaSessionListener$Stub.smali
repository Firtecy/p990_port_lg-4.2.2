.class public abstract Lcom/lge/ims/service/uc/IUCMediaSessionListener$Stub;
.super Landroid/os/Binder;
.source "IUCMediaSessionListener.java"

# interfaces
.implements Lcom/lge/ims/service/uc/IUCMediaSessionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/uc/IUCMediaSessionListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/uc/IUCMediaSessionListener$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.lge.ims.service.uc.IUCMediaSessionListener"

.field static final TRANSACTION_DebugInfoUpdated:I = 0x13

.field static final TRANSACTION_alternateImageStarted:I = 0xd

.field static final TRANSACTION_alternateImageStopped:I = 0xe

.field static final TRANSACTION_audioStarted:I = 0x1

.field static final TRANSACTION_audioStopped:I = 0x2

.field static final TRANSACTION_backgroundStarted:I = 0x8

.field static final TRANSACTION_backgroundStopped:I = 0x9

.field static final TRANSACTION_cameraBrightnessChanged:I = 0x7

.field static final TRANSACTION_cameraSelected:I = 0x5

.field static final TRANSACTION_cameraZoomChanged:I = 0x6

.field static final TRANSACTION_captured:I = 0xa

.field static final TRANSACTION_displaySwapped:I = 0xf

.field static final TRANSACTION_displayUpdated:I = 0x10

.field static final TRANSACTION_firstPeerVideoReceived:I = 0x4

.field static final TRANSACTION_mediaStarted:I = 0x3

.field static final TRANSACTION_orientationChanged:I = 0x12

.field static final TRANSACTION_recordingStarted:I = 0xb

.field static final TRANSACTION_recordingStopped:I = 0xc

.field static final TRANSACTION_viewSizeChanged:I = 0x11


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 20
    const-string v0, "com.lge.ims.service.uc.IUCMediaSessionListener"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/lge/ims/service/uc/IUCMediaSessionListener$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 21
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/uc/IUCMediaSessionListener;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 28
    if-nez p0, :cond_4

    #@2
    .line 29
    const/4 v0, 0x0

    #@3
    .line 35
    :goto_3
    return-object v0

    #@4
    .line 31
    :cond_4
    const-string v1, "com.lge.ims.service.uc.IUCMediaSessionListener"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 32
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/lge/ims/service/uc/IUCMediaSessionListener;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 33
    check-cast v0, Lcom/lge/ims/service/uc/IUCMediaSessionListener;

    #@12
    goto :goto_3

    #@13
    .line 35
    :cond_13
    new-instance v0, Lcom/lge/ims/service/uc/IUCMediaSessionListener$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/lge/ims/service/uc/IUCMediaSessionListener$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 8
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 43
    sparse-switch p1, :sswitch_data_146

    #@4
    .line 219
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v1

    #@8
    :goto_8
    return v1

    #@9
    .line 47
    :sswitch_9
    const-string v2, "com.lge.ims.service.uc.IUCMediaSessionListener"

    #@b
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 52
    :sswitch_f
    const-string v2, "com.lge.ims.service.uc.IUCMediaSessionListener"

    #@11
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 53
    invoke-virtual {p0}, Lcom/lge/ims/service/uc/IUCMediaSessionListener$Stub;->audioStarted()V

    #@17
    .line 54
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1a
    goto :goto_8

    #@1b
    .line 59
    :sswitch_1b
    const-string v2, "com.lge.ims.service.uc.IUCMediaSessionListener"

    #@1d
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@20
    .line 60
    invoke-virtual {p0}, Lcom/lge/ims/service/uc/IUCMediaSessionListener$Stub;->audioStopped()V

    #@23
    .line 61
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@26
    goto :goto_8

    #@27
    .line 66
    :sswitch_27
    const-string v2, "com.lge.ims.service.uc.IUCMediaSessionListener"

    #@29
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2c
    .line 67
    invoke-virtual {p0}, Lcom/lge/ims/service/uc/IUCMediaSessionListener$Stub;->mediaStarted()V

    #@2f
    .line 68
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@32
    goto :goto_8

    #@33
    .line 73
    :sswitch_33
    const-string v2, "com.lge.ims.service.uc.IUCMediaSessionListener"

    #@35
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@38
    .line 74
    invoke-virtual {p0}, Lcom/lge/ims/service/uc/IUCMediaSessionListener$Stub;->firstPeerVideoReceived()V

    #@3b
    .line 75
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3e
    goto :goto_8

    #@3f
    .line 80
    :sswitch_3f
    const-string v2, "com.lge.ims.service.uc.IUCMediaSessionListener"

    #@41
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@44
    .line 82
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@47
    move-result v0

    #@48
    .line 83
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCMediaSessionListener$Stub;->cameraSelected(I)V

    #@4b
    .line 84
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4e
    goto :goto_8

    #@4f
    .line 89
    .end local v0           #_arg0:I
    :sswitch_4f
    const-string v2, "com.lge.ims.service.uc.IUCMediaSessionListener"

    #@51
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@54
    .line 91
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@57
    move-result v0

    #@58
    .line 92
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCMediaSessionListener$Stub;->cameraZoomChanged(I)V

    #@5b
    .line 93
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5e
    goto :goto_8

    #@5f
    .line 98
    .end local v0           #_arg0:I
    :sswitch_5f
    const-string v2, "com.lge.ims.service.uc.IUCMediaSessionListener"

    #@61
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@64
    .line 100
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@67
    move-result v0

    #@68
    .line 101
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCMediaSessionListener$Stub;->cameraBrightnessChanged(I)V

    #@6b
    .line 102
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@6e
    goto :goto_8

    #@6f
    .line 107
    .end local v0           #_arg0:I
    :sswitch_6f
    const-string v2, "com.lge.ims.service.uc.IUCMediaSessionListener"

    #@71
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@74
    .line 109
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@77
    move-result v0

    #@78
    .line 110
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCMediaSessionListener$Stub;->backgroundStarted(I)V

    #@7b
    .line 111
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@7e
    goto :goto_8

    #@7f
    .line 116
    .end local v0           #_arg0:I
    :sswitch_7f
    const-string v2, "com.lge.ims.service.uc.IUCMediaSessionListener"

    #@81
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@84
    .line 118
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@87
    move-result v0

    #@88
    .line 119
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCMediaSessionListener$Stub;->backgroundStopped(I)V

    #@8b
    .line 120
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@8e
    goto/16 :goto_8

    #@90
    .line 125
    .end local v0           #_arg0:I
    :sswitch_90
    const-string v2, "com.lge.ims.service.uc.IUCMediaSessionListener"

    #@92
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@95
    .line 127
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@98
    move-result v0

    #@99
    .line 128
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCMediaSessionListener$Stub;->captured(I)V

    #@9c
    .line 129
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@9f
    goto/16 :goto_8

    #@a1
    .line 134
    .end local v0           #_arg0:I
    :sswitch_a1
    const-string v2, "com.lge.ims.service.uc.IUCMediaSessionListener"

    #@a3
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a6
    .line 136
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@a9
    move-result v0

    #@aa
    .line 137
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCMediaSessionListener$Stub;->recordingStarted(I)V

    #@ad
    .line 138
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@b0
    goto/16 :goto_8

    #@b2
    .line 143
    .end local v0           #_arg0:I
    :sswitch_b2
    const-string v2, "com.lge.ims.service.uc.IUCMediaSessionListener"

    #@b4
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b7
    .line 145
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@ba
    move-result v0

    #@bb
    .line 146
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCMediaSessionListener$Stub;->recordingStopped(I)V

    #@be
    .line 147
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@c1
    goto/16 :goto_8

    #@c3
    .line 152
    .end local v0           #_arg0:I
    :sswitch_c3
    const-string v2, "com.lge.ims.service.uc.IUCMediaSessionListener"

    #@c5
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c8
    .line 154
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@cb
    move-result v0

    #@cc
    .line 155
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCMediaSessionListener$Stub;->alternateImageStarted(I)V

    #@cf
    .line 156
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@d2
    goto/16 :goto_8

    #@d4
    .line 161
    .end local v0           #_arg0:I
    :sswitch_d4
    const-string v2, "com.lge.ims.service.uc.IUCMediaSessionListener"

    #@d6
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d9
    .line 163
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@dc
    move-result v0

    #@dd
    .line 164
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCMediaSessionListener$Stub;->alternateImageStopped(I)V

    #@e0
    .line 165
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@e3
    goto/16 :goto_8

    #@e5
    .line 170
    .end local v0           #_arg0:I
    :sswitch_e5
    const-string v2, "com.lge.ims.service.uc.IUCMediaSessionListener"

    #@e7
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ea
    .line 172
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@ed
    move-result v0

    #@ee
    .line 173
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCMediaSessionListener$Stub;->displaySwapped(I)V

    #@f1
    .line 174
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@f4
    goto/16 :goto_8

    #@f6
    .line 179
    .end local v0           #_arg0:I
    :sswitch_f6
    const-string v2, "com.lge.ims.service.uc.IUCMediaSessionListener"

    #@f8
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@fb
    .line 181
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@fe
    move-result v0

    #@ff
    .line 182
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCMediaSessionListener$Stub;->displayUpdated(I)V

    #@102
    .line 183
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@105
    goto/16 :goto_8

    #@107
    .line 188
    .end local v0           #_arg0:I
    :sswitch_107
    const-string v2, "com.lge.ims.service.uc.IUCMediaSessionListener"

    #@109
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@10c
    .line 190
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@10f
    move-result v0

    #@110
    .line 191
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCMediaSessionListener$Stub;->viewSizeChanged(I)V

    #@113
    .line 192
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@116
    goto/16 :goto_8

    #@118
    .line 197
    .end local v0           #_arg0:I
    :sswitch_118
    const-string v2, "com.lge.ims.service.uc.IUCMediaSessionListener"

    #@11a
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@11d
    .line 199
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@120
    move-result v0

    #@121
    .line 200
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCMediaSessionListener$Stub;->orientationChanged(I)V

    #@124
    .line 201
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@127
    goto/16 :goto_8

    #@129
    .line 206
    .end local v0           #_arg0:I
    :sswitch_129
    const-string v2, "com.lge.ims.service.uc.IUCMediaSessionListener"

    #@12b
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@12e
    .line 208
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@131
    move-result v2

    #@132
    if-eqz v2, :cond_144

    #@134
    .line 209
    sget-object v2, Lcom/lge/ims/service/uc/OnScreenDebugInfoVideo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@136
    invoke-interface {v2, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@139
    move-result-object v0

    #@13a
    check-cast v0, Lcom/lge/ims/service/uc/OnScreenDebugInfoVideo;

    #@13c
    .line 214
    .local v0, _arg0:Lcom/lge/ims/service/uc/OnScreenDebugInfoVideo;
    :goto_13c
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCMediaSessionListener$Stub;->DebugInfoUpdated(Lcom/lge/ims/service/uc/OnScreenDebugInfoVideo;)V

    #@13f
    .line 215
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@142
    goto/16 :goto_8

    #@144
    .line 212
    .end local v0           #_arg0:Lcom/lge/ims/service/uc/OnScreenDebugInfoVideo;
    :cond_144
    const/4 v0, 0x0

    #@145
    .restart local v0       #_arg0:Lcom/lge/ims/service/uc/OnScreenDebugInfoVideo;
    goto :goto_13c

    #@146
    .line 43
    :sswitch_data_146
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_1b
        0x3 -> :sswitch_27
        0x4 -> :sswitch_33
        0x5 -> :sswitch_3f
        0x6 -> :sswitch_4f
        0x7 -> :sswitch_5f
        0x8 -> :sswitch_6f
        0x9 -> :sswitch_7f
        0xa -> :sswitch_90
        0xb -> :sswitch_a1
        0xc -> :sswitch_b2
        0xd -> :sswitch_c3
        0xe -> :sswitch_d4
        0xf -> :sswitch_e5
        0x10 -> :sswitch_f6
        0x11 -> :sswitch_107
        0x12 -> :sswitch_118
        0x13 -> :sswitch_129
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
