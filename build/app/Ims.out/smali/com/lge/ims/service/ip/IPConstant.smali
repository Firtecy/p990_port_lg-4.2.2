.class public Lcom/lge/ims/service/ip/IPConstant;
.super Ljava/lang/Object;
.source "IPConstant.java"


# static fields
.field public static final ACCOUNT_NAME:Ljava/lang/String; = "lgeims"

.field public static final ACCOUNT_TYPE:Ljava/lang/String; = "com.lge.ims"

.field public static final ACTION_IP_ALARM_TIME:Ljava/lang/String; = "com.lge.ims.ip.polling"

.field public static final BATTERY_THRESHOLD:I = 0x5

.field public static final CAPA_STATUS_NONE:I = 0x0

.field public static final CAPA_STATUS_NOT_RCS_USER:I = 0x2

.field public static final CAPA_STATUS_RCS_USER:I = 0x1

.field public static final EXCESS_MAX_USER:Ljava/lang/String; = "excess_max_user"

.field public static final INDEX_COM_KEY_DATA_ID:I = 0x1

.field public static final INDEX_COM_KEY_ID:I = 0x0

.field public static final INDEX_COM_KEY_MSISDN:I = 0x2

.field public static final INDEX_COM_NORMAL_MSISDN:I = 0x3

.field public static final INDEX_KEY_BUDDY_IMAGE_FILE_PATH:I = 0x2

.field public static final INDEX_KEY_BUDDY_IMAGE_NORMAL_MSISDN:I = 0x1

.field public static final INDEX_KEY_BUDDY_IMAGE_TIME_STAMP:I = 0x3

.field public static final INDEX_KEY_CAPA_DISPLAY_NAME:I = 0x14

.field public static final INDEX_KEY_CAPA_FIRSTTIME_RCS:I = 0x9

.field public static final INDEX_KEY_CAPA_FTSTATUS:I = 0x6

.field public static final INDEX_KEY_CAPA_HTTPSTATUS:I = 0xd

.field public static final INDEX_KEY_CAPA_IMAGE_URI_ID:I = 0x11

.field public static final INDEX_KEY_CAPA_IMSTATUS:I = 0x5

.field public static final INDEX_KEY_CAPA_MIMSTATUS:I = 0xa

.field public static final INDEX_KEY_CAPA_NONRCSEUSER_LIST:I = 0x10

.field public static final INDEX_KEY_CAPA_PHOTO_UPDATE:I = 0x13

.field public static final INDEX_KEY_CAPA_PRESENCESTATUS:I = 0xb

.field public static final INDEX_KEY_CAPA_RAW_CONTACT_ID:I = 0x12

.field public static final INDEX_KEY_CAPA_RCSEUSER_LIST:I = 0xf

.field public static final INDEX_KEY_CAPA_RCSSTATUS:I = 0x4

.field public static final INDEX_KEY_CAPA_RESPONSE_CODE:I = 0xe

.field public static final INDEX_KEY_CAPA_TIME_STAMP:I = 0x7

.field public static final INDEX_KEY_CAPA_TYPE:I = 0x8

.field public static final INDEX_KEY_CAPA_URI_ID:I = 0xc

.field public static final INDEX_KEY_EXCESS_MAX_USER:I = 0x2

.field public static final INDEX_KEY_LATEST_POLLING:I = 0x0

.field public static final INDEX_KEY_LATEST_RLSSUB_POLLING:I = 0x1

.field public static final INDEX_KEY_MY_BIRTHDAY:I = 0x3

.field public static final INDEX_KEY_MY_CYWORLDACCOUNT:I = 0x7

.field public static final INDEX_KEY_MY_DISPLAY_NAME:I = 0xe

.field public static final INDEX_KEY_MY_EMAIL:I = 0x2

.field public static final INDEX_KEY_MY_FACEBOOKACCOUNT:I = 0x6

.field public static final INDEX_KEY_MY_FREETEXT:I = 0x1

.field public static final INDEX_KEY_MY_HOMEPAGE:I = 0x0

.field public static final INDEX_KEY_MY_STATUS:I = 0x4

.field public static final INDEX_KEY_MY_STATUSICON:I = 0x9

.field public static final INDEX_KEY_MY_STATUSICON_LINK:I = 0xa

.field public static final INDEX_KEY_MY_STATUSICON_THUMB:I = 0xb

.field public static final INDEX_KEY_MY_STATUSICON_THUMB_ETAG:I = 0xd

.field public static final INDEX_KEY_MY_STATUSICON_THUMB_LINK:I = 0xc

.field public static final INDEX_KEY_MY_TWITTERACCOUNT:I = 0x5

.field public static final INDEX_KEY_MY_WAGGLEACCOUNT:I = 0x8

.field public static final INDEX_KEY_PRES_BIRTHDAY:I = 0x5

.field public static final INDEX_KEY_PRES_CYWORLDACCOUNT:I = 0x9

.field public static final INDEX_KEY_PRES_EMAIL:I = 0x4

.field public static final INDEX_KEY_PRES_FACEBOOKACCOUNT:I = 0x8

.field public static final INDEX_KEY_PRES_FREETEXT:I = 0x3

.field public static final INDEX_KEY_PRES_HOMEPAGE:I = 0x2

.field public static final INDEX_KEY_PRES_NORMAL_MSISDN:I = 0x1

.field public static final INDEX_KEY_PRES_STATUS:I = 0x6

.field public static final INDEX_KEY_PRES_STATUSICON:I = 0xb

.field public static final INDEX_KEY_PRES_STATUSICON_LINK:I = 0xc

.field public static final INDEX_KEY_PRES_STATUSICON_THUMB:I = 0xd

.field public static final INDEX_KEY_PRES_STATUSICON_THUMB_ETAG:I = 0xf

.field public static final INDEX_KEY_PRES_STATUSICON_THUMB_LINK:I = 0xe

.field public static final INDEX_KEY_PRES_STATUSICON_UPDATE:I = 0x11

.field public static final INDEX_KEY_PRES_TIME_STAMP:I = 0x10

.field public static final INDEX_KEY_PRES_TWITTERACCOUNT:I = 0x7

.field public static final INDEX_KEY_PRES_WAGGLEACCOUNT:I = 0xa

.field public static final INTERFACETYPE_IMSPEOPLE:I = 0x29

.field public static final IP_ACTION_AC_UPDATE_COMPLETE:Ljava/lang/String; = "com.lge.ims.action.IMS_EVENT_CONFIG_UPDATE_COMPLETE"

.field public static final IP_ACTION_DOWNLOAD_BUDDYICON:Ljava/lang/String; = "com.lge.ims.rcs.buddy_icon_downloaded"

.field public static final IP_ACTION_EXCESS_MAX_USER:Ljava/lang/String; = "com.lge.ims.rcs.excess_max_user"

.field public static final IP_ACTION_GET_BUDDY_ICON:Ljava/lang/String; = "com.lge.ims.rcs.get_buddy_icon"

.field public static final IP_ACTION_INVITE_USER:Ljava/lang/String; = "com.lge.ims.rcs.invite_user"

.field public static final IP_ACTION_MY_ICON_CHANGE:Ljava/lang/String; = "com.lge.ims.rcs.my_icon_change"

.field public static final IP_ACTION_MY_ICON_DELETE:Ljava/lang/String; = "com.lge.ims.rcs.my_icon_delete"

.field public static final IP_ACTION_MY_STATUS_CHANGE:Ljava/lang/String; = "com.lge.ims.rcs.my_status_change"

.field public static final IP_ACTION_PHOTO_UPDATE:Ljava/lang/String; = "com.lge.ims.rcs.photo_update"

.field public static final IP_ACTION_QUERY_CAPA:Ljava/lang/String; = "com.lge.ims.rcs.query_capa"

.field public static final IP_ACTION_QUERY_CAPA_ID:Ljava/lang/String; = "com.lge.ims.rcs.query_capa_id"

.field public static final IP_ACTION_QUERY_LIST:Ljava/lang/String; = "com.lge.ims.rcs.query_list"

.field public static final IP_ALARM_ID:Ljava/lang/String; = "ip_alarm_id"

.field public static IP_BUDDY_IMAGE_CONTENT_URI:Landroid/net/Uri; = null

.field public static IP_CAPA_CONTENT:Ljava/lang/String; = null

.field public static IP_CAPA_CONTENT_URI:Landroid/net/Uri; = null

.field public static IP_CAPA_MIMETYPE:Ljava/lang/String; = null

.field public static final IP_CAPA_TYPE_PBSYNC:I = 0x0

.field public static final IP_CAPA_TYPE_POLLING:I = 0x1

.field public static IP_MYSTAUS_CONTENT_URI:Landroid/net/Uri; = null

.field public static IP_MYSTAUS_SERVER_CONTENT_URI:Landroid/net/Uri; = null

.field public static final IP_NOT_RESOURCE_LIST_USER:I = 0x0

.field public static final IP_NOT_SUPPORT_DISCOVERYPRESENCE:I = 0x0

.field public static final IP_NOT_SUPPORT_FT:I = 0x0

.field public static final IP_NOT_SUPPORT_HTTP:I = 0x0

.field public static final IP_NOT_SUPPORT_IM:I = 0x0

.field public static final IP_NOT_SUPPORT_MIM:I = 0x0

.field public static final IP_NOT_SUPPORT_PRESENCE:I = 0x0

.field public static IP_PRESENCE_CONTENT_URI:Landroid/net/Uri; = null

.field public static final IP_PROVIDER_AUTHORITY:Ljava/lang/String; = "com.lge.ims.provider.ip"

.field public static IP_PROVISIONING_CONTENT_URI:Landroid/net/Uri; = null

.field public static final IP_RESOURCE_LIST_USER:I = 0x1

.field public static final IP_RLSSUBTIMER_ID:I = 0x5016

.field public static final IP_SUPPORT_DISCOVERYPRESENCE:I = 0x1

.field public static final IP_SUPPORT_FT:I = 0x1

.field public static final IP_SUPPORT_HTTP:I = 0x1

.field public static final IP_SUPPORT_IM:I = 0x1

.field public static final IP_SUPPORT_MIM:I = 0x1

.field public static final IP_SUPPORT_PRESENCE:I = 0x1

.field public static final IP_TIMER_ID:I = 0x5015

.field public static final IP_USER_INVALID:I = -0x1

.field public static final IP_USER_MAX:I = 0x6

.field public static final IP_USER_RCSE:I = 0x1

.field public static final IP_USER_TEMP:I = 0x4

.field public static final IP_USER_UNKNOWN:I = 0x0

.field public static final KEY_BUDDY_IMAGE_FILE_PATH:Ljava/lang/String; = "file_path"

.field public static final KEY_BUDDY_IMAGE_TIME_STAMP:Ljava/lang/String; = "time_stamp"

.field public static final KEY_CAPA_DISPLAY_NAME:Ljava/lang/String; = "display_name"

.field public static final KEY_CAPA_FIRSTTIME_RCS:Ljava/lang/String; = "first_time_rcs"

.field public static final KEY_CAPA_FTSTATUS:Ljava/lang/String; = "ft_status"

.field public static final KEY_CAPA_HTTPSTATUS:Ljava/lang/String; = "http_status"

.field public static final KEY_CAPA_IMAGE_URI_ID:Ljava/lang/String; = "image_uri_id"

.field public static final KEY_CAPA_IMSTATUS:Ljava/lang/String; = "im_status"

.field public static final KEY_CAPA_MIMSTATUS:Ljava/lang/String; = "mim_status"

.field public static final KEY_CAPA_NONRCSEUSER_LIST:Ljava/lang/String; = "nonrcs_user_list"

.field public static final KEY_CAPA_PHOTO_UPDATE:Ljava/lang/String; = "photo_update"

.field public static final KEY_CAPA_PRESENCESTATUS:Ljava/lang/String; = "presence_status"

.field public static final KEY_CAPA_RAW_CONTACT_ID:Ljava/lang/String; = "raw_contact_id"

.field public static final KEY_CAPA_RCSEUSER_LIST:Ljava/lang/String; = "rcs_user_list"

.field public static final KEY_CAPA_RCSSTATUS:Ljava/lang/String; = "rcs_status"

.field public static final KEY_CAPA_RESPONSE_CODE:Ljava/lang/String; = "response_code"

.field public static final KEY_CAPA_TIME_STAMP:Ljava/lang/String; = "time_stamp"

.field public static final KEY_CAPA_TYPE:Ljava/lang/String; = "capa_type"

.field public static final KEY_CAPA_URI_ID:Ljava/lang/String; = "uri_id"

.field public static final KEY_COM_DATA_ID:Ljava/lang/String; = "data_id"

.field public static final KEY_COM_ID:Ljava/lang/String; = "_id"

.field public static final KEY_COM_MSISDN:Ljava/lang/String; = "msisdn"

.field public static final KEY_COM_NORMAL_MSISDN:Ljava/lang/String; = "nor_msisdn"

.field public static final KEY_MY_BIRTHDAY:Ljava/lang/String; = "birthday"

.field public static final KEY_MY_CYWORLDACCOUNT:Ljava/lang/String; = "cyworld_account"

.field public static final KEY_MY_DISPLAY_NAME:Ljava/lang/String; = "display_name"

.field public static final KEY_MY_EMAIL:Ljava/lang/String; = "e_mail"

.field public static final KEY_MY_FACEBOOKACCOUNT:Ljava/lang/String; = "facebook_account"

.field public static final KEY_MY_FREETEXT:Ljava/lang/String; = "free_text"

.field public static final KEY_MY_HOMEPAGE:Ljava/lang/String; = "homepage"

.field public static final KEY_MY_STATUS:Ljava/lang/String; = "status"

.field public static final KEY_MY_STATUSICON:Ljava/lang/String; = "status_icon"

.field public static final KEY_MY_STATUSICON_LINK:Ljava/lang/String; = "status_icon_link"

.field public static final KEY_MY_STATUSICON_THUMB:Ljava/lang/String; = "status_icon_thumb"

.field public static final KEY_MY_STATUSICON_THUMB_ETAG:Ljava/lang/String; = "status_icon_thumb_etag"

.field public static final KEY_MY_STATUSICON_THUMB_LINK:Ljava/lang/String; = "status_icon_thumb_link"

.field public static final KEY_MY_TWITTERACCOUNT:Ljava/lang/String; = "twitter_account"

.field public static final KEY_MY_WAGGLEACCOUNT:Ljava/lang/String; = "waggle_account"

.field public static final KEY_PRES_BIRTHDAY:Ljava/lang/String; = "birthday"

.field public static final KEY_PRES_CYWORLDACCOUNT:Ljava/lang/String; = "cyworld_account"

.field public static final KEY_PRES_EMAIL:Ljava/lang/String; = "e_mail"

.field public static final KEY_PRES_FACEBOOKACCOUNT:Ljava/lang/String; = "facebook_account"

.field public static final KEY_PRES_FREETEXT:Ljava/lang/String; = "free_text"

.field public static final KEY_PRES_HOMEPAGE:Ljava/lang/String; = "homepage"

.field public static final KEY_PRES_STATUS:Ljava/lang/String; = "status"

.field public static final KEY_PRES_STATUSICON:Ljava/lang/String; = "status_icon"

.field public static final KEY_PRES_STATUSICON_LINK:Ljava/lang/String; = "status_icon_link"

.field public static final KEY_PRES_STATUSICON_THUMB:Ljava/lang/String; = "status_icon_thumb"

.field public static final KEY_PRES_STATUSICON_THUMB_ETAG:Ljava/lang/String; = "status_icon_thumb_etag"

.field public static final KEY_PRES_STATUSICON_THUMB_LINK:Ljava/lang/String; = "status_icon_thumb_link"

.field public static final KEY_PRES_STATUSICON_UPDATE:Ljava/lang/String; = "statusicon_update"

.field public static final KEY_PRES_TIME_STAMP:Ljava/lang/String; = "time_stamp"

.field public static final KEY_PRES_TWITTERACCOUNT:Ljava/lang/String; = "twitter_account"

.field public static final KEY_PRES_WAGGLEACCOUNT:Ljava/lang/String; = "waggle_account"

.field public static final LATEST_POLLING:Ljava/lang/String; = "latest_polling"

.field public static final LATEST_RLSSUB_POLLING:Ljava/lang/String; = "latest_rlssub_polling"

.field public static final MAX_CAPA_QUERY:I = 0xa

.field public static final MAX_SELECTED_CAPA:I = 0xa

.field public static final SETTING_CONTENT_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 86
    const-string v0, "content://com.lge.ims.provider.ip/ip_capa"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/lge/ims/service/ip/IPConstant;->IP_CAPA_CONTENT_URI:Landroid/net/Uri;

    #@8
    .line 87
    const-string v0, "content://com.lge.ims.provider.ip/ip_presence"

    #@a
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@d
    move-result-object v0

    #@e
    sput-object v0, Lcom/lge/ims/service/ip/IPConstant;->IP_PRESENCE_CONTENT_URI:Landroid/net/Uri;

    #@10
    .line 88
    const-string v0, "content://com.lge.ims.provider.ip/ip_mystatus"

    #@12
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@15
    move-result-object v0

    #@16
    sput-object v0, Lcom/lge/ims/service/ip/IPConstant;->IP_MYSTAUS_CONTENT_URI:Landroid/net/Uri;

    #@18
    .line 89
    const-string v0, "content://com.lge.ims.provider.ip/ip_mystatus_service"

    #@1a
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@1d
    move-result-object v0

    #@1e
    sput-object v0, Lcom/lge/ims/service/ip/IPConstant;->IP_MYSTAUS_SERVER_CONTENT_URI:Landroid/net/Uri;

    #@20
    .line 90
    const-string v0, "content://com.lge.ims.provider.ip/ip_provisioning"

    #@22
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@25
    move-result-object v0

    #@26
    sput-object v0, Lcom/lge/ims/service/ip/IPConstant;->IP_PROVISIONING_CONTENT_URI:Landroid/net/Uri;

    #@28
    .line 91
    const-string v0, "content://com.lge.ims.provider.ip/ip_buddyimage"

    #@2a
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@2d
    move-result-object v0

    #@2e
    sput-object v0, Lcom/lge/ims/service/ip/IPConstant;->IP_BUDDY_IMAGE_CONTENT_URI:Landroid/net/Uri;

    #@30
    .line 94
    const-string v0, "content://com.lge.ims.provider.ip/ip_capa"

    #@32
    sput-object v0, Lcom/lge/ims/service/ip/IPConstant;->IP_CAPA_CONTENT:Ljava/lang/String;

    #@34
    .line 95
    const-string v0, "vnd.android.cursor.item/com.lge.ims.rcs.status"

    #@36
    sput-object v0, Lcom/lge/ims/service/ip/IPConstant;->IP_CAPA_MIMETYPE:Ljava/lang/String;

    #@38
    .line 225
    const-string v0, "content://com.lge.ims.provisioning/settings"

    #@3a
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@3d
    move-result-object v0

    #@3e
    sput-object v0, Lcom/lge/ims/service/ip/IPConstant;->SETTING_CONTENT_URI:Landroid/net/Uri;

    #@40
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 18
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
