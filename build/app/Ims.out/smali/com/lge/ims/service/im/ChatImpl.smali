.class public Lcom/lge/ims/service/im/ChatImpl;
.super Lcom/lge/ims/service/im/IChat$Stub;
.source "ChatImpl.java"

# interfaces
.implements Lcom/lge/ims/JNICoreListener;
.implements Ljava/io/Serializable;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mContributionId:Ljava/lang/String;

.field private mIsInviteForBye:Z

.field private mIsRejoin:Z

.field private mListener:Lcom/lge/ims/service/im/IChatListener;

.field private mNativeObj:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 30
    invoke-direct {p0}, Lcom/lge/ims/service/im/IChat$Stub;-><init>()V

    #@4
    .line 27
    iput-boolean v0, p0, Lcom/lge/ims/service/im/ChatImpl;->mIsRejoin:Z

    #@6
    .line 28
    iput-boolean v0, p0, Lcom/lge/ims/service/im/ChatImpl;->mIsInviteForBye:Z

    #@8
    .line 31
    const-string v0, "ChatImpl()"

    #@a
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@d
    .line 33
    iput-object p1, p0, Lcom/lge/ims/service/im/ChatImpl;->mContext:Landroid/content/Context;

    #@f
    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;)V
    .registers 6
    .parameter "context"
    .parameter "nativeObj"
    .parameter "contributionId"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 36
    invoke-direct {p0}, Lcom/lge/ims/service/im/IChat$Stub;-><init>()V

    #@4
    .line 27
    iput-boolean v0, p0, Lcom/lge/ims/service/im/ChatImpl;->mIsRejoin:Z

    #@6
    .line 28
    iput-boolean v0, p0, Lcom/lge/ims/service/im/ChatImpl;->mIsInviteForBye:Z

    #@8
    .line 37
    new-instance v0, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v1, "ChatImpl() : nativeObj = "

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    const-string v1, "\n"

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, "contribution id = "

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v0

    #@27
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v0

    #@2b
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@2e
    .line 40
    iput-object p1, p0, Lcom/lge/ims/service/im/ChatImpl;->mContext:Landroid/content/Context;

    #@30
    .line 41
    iput p2, p0, Lcom/lge/ims/service/im/ChatImpl;->mNativeObj:I

    #@32
    .line 42
    iput-object p3, p0, Lcom/lge/ims/service/im/ChatImpl;->mContributionId:Ljava/lang/String;

    #@34
    .line 43
    invoke-static {p2, p0}, Lcom/lge/ims/JNICore;->setListener(ILcom/lge/ims/JNICoreListener;)I

    #@37
    .line 44
    return-void
.end method

.method private sendMessageToJNI(Landroid/os/Parcel;)V
    .registers 4
    .parameter "parcel"

    #@0
    .prologue
    .line 340
    if-nez p1, :cond_3

    #@2
    .line 351
    :cond_2
    :goto_2
    return-void

    #@3
    .line 344
    :cond_3
    invoke-virtual {p1}, Landroid/os/Parcel;->marshall()[B

    #@6
    move-result-object v0

    #@7
    .line 345
    .local v0, baData:[B
    invoke-virtual {p1}, Landroid/os/Parcel;->recycle()V

    #@a
    .line 346
    const/4 p1, 0x0

    #@b
    .line 348
    iget v1, p0, Lcom/lge/ims/service/im/ChatImpl;->mNativeObj:I

    #@d
    if-eqz v1, :cond_2

    #@f
    .line 349
    iget v1, p0, Lcom/lge/ims/service/im/ChatImpl;->mNativeObj:I

    #@11
    invoke-static {v1, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@14
    goto :goto_2
.end method


# virtual methods
.method public accept()V
    .registers 3

    #@0
    .prologue
    .line 182
    const-string v1, "accept()"

    #@2
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@5
    .line 184
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v0

    #@9
    .line 186
    .local v0, parcel:Landroid/os/Parcel;
    const/16 v1, 0x298c

    #@b
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@e
    .line 188
    invoke-direct {p0, v0}, Lcom/lge/ims/service/im/ChatImpl;->sendMessageToJNI(Landroid/os/Parcel;)V

    #@11
    .line 189
    return-void
.end method

.method public addParticipants([Ljava/lang/String;)V
    .registers 9
    .parameter "participants"

    #@0
    .prologue
    .line 226
    new-instance v5, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v6, "addParticipants() : participants = "

    #@7
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v5

    #@b
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v5

    #@f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v5

    #@13
    invoke-static {v5}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@16
    .line 228
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@19
    move-result-object v3

    #@1a
    .line 229
    .local v3, parcel:Landroid/os/Parcel;
    const/16 v5, 0x298e

    #@1c
    invoke-virtual {v3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 230
    array-length v5, p1

    #@20
    invoke-virtual {v3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@23
    .line 231
    move-object v0, p1

    #@24
    .local v0, arr$:[Ljava/lang/String;
    array-length v2, v0

    #@25
    .local v2, len$:I
    const/4 v1, 0x0

    #@26
    .local v1, i$:I
    :goto_26
    if-ge v1, v2, :cond_30

    #@28
    aget-object v4, v0, v1

    #@2a
    .line 232
    .local v4, participant:Ljava/lang/String;
    invoke-virtual {v3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@2d
    .line 231
    add-int/lit8 v1, v1, 0x1

    #@2f
    goto :goto_26

    #@30
    .line 235
    .end local v4           #participant:Ljava/lang/String;
    :cond_30
    invoke-direct {p0, v3}, Lcom/lge/ims/service/im/ChatImpl;->sendMessageToJNI(Landroid/os/Parcel;)V

    #@33
    .line 236
    return-void
.end method

.method public close(Z)V
    .registers 5
    .parameter "isExplicitly"

    #@0
    .prologue
    .line 211
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "close() : isExplicitly = "

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@16
    .line 213
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@19
    move-result-object v0

    #@1a
    .line 215
    .local v0, parcel:Landroid/os/Parcel;
    const/16 v1, 0x298b

    #@1c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 216
    if-eqz p1, :cond_29

    #@21
    .line 217
    const/4 v1, 0x1

    #@22
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@25
    .line 222
    :goto_25
    invoke-direct {p0, v0}, Lcom/lge/ims/service/im/ChatImpl;->sendMessageToJNI(Landroid/os/Parcel;)V

    #@28
    .line 223
    return-void

    #@29
    .line 219
    :cond_29
    const/4 v1, 0x0

    #@2a
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@2d
    goto :goto_25
.end method

.method public destroy()V
    .registers 2

    #@0
    .prologue
    .line 330
    const-string v0, "destroy()"

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@5
    .line 332
    iget v0, p0, Lcom/lge/ims/service/im/ChatImpl;->mNativeObj:I

    #@7
    if-eqz v0, :cond_16

    #@9
    .line 333
    iget v0, p0, Lcom/lge/ims/service/im/ChatImpl;->mNativeObj:I

    #@b
    invoke-static {v0, p0}, Lcom/lge/ims/JNICore;->removeListener(ILcom/lge/ims/JNICoreListener;)I

    #@e
    .line 334
    iget v0, p0, Lcom/lge/ims/service/im/ChatImpl;->mNativeObj:I

    #@10
    invoke-static {v0}, Lcom/lge/ims/JNICore;->releaseInterface(I)I

    #@13
    .line 335
    const/4 v0, 0x0

    #@14
    iput v0, p0, Lcom/lge/ims/service/im/ChatImpl;->mNativeObj:I

    #@16
    .line 337
    :cond_16
    return-void
.end method

.method public extendToConference([Ljava/lang/String;Lcom/lge/ims/service/im/IMMessage;Ljava/lang/String;Ljava/lang/String;)V
    .registers 13
    .parameter "additionalParticipants"
    .parameter "message"
    .parameter "contributionId"
    .parameter "sessionReplaces"

    #@0
    .prologue
    .line 240
    new-instance v6, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v7, "extendToConference() : additionalParticipants = "

    #@7
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v6

    #@b
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v6

    #@f
    const-string v7, ", message = "

    #@11
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v6

    #@15
    invoke-virtual {p2}, Lcom/lge/ims/service/im/IMMessage;->getContent()Ljava/lang/String;

    #@18
    move-result-object v7

    #@19
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v6

    #@1d
    const-string v7, ", messageId = "

    #@1f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v6

    #@23
    invoke-virtual {p2}, Lcom/lge/ims/service/im/IMMessage;->getMessageId()Ljava/lang/String;

    #@26
    move-result-object v7

    #@27
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v6

    #@2b
    const-string v7, ", contributionId = "

    #@2d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v6

    #@31
    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v6

    #@35
    const-string v7, ", sessionReplaces = "

    #@37
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v6

    #@3b
    invoke-virtual {v6, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v6

    #@3f
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v6

    #@43
    invoke-static {v6}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@46
    .line 246
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@49
    move-result-object v4

    #@4a
    .line 247
    .local v4, parcel:Landroid/os/Parcel;
    const/16 v6, 0x298f

    #@4c
    invoke-virtual {v4, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@4f
    .line 248
    array-length v6, p1

    #@50
    invoke-virtual {v4, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@53
    .line 249
    move-object v0, p1

    #@54
    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    #@55
    .local v3, len$:I
    const/4 v2, 0x0

    #@56
    .local v2, i$:I
    :goto_56
    if-ge v2, v3, :cond_60

    #@58
    aget-object v5, v0, v2

    #@5a
    .line 250
    .local v5, participant:Ljava/lang/String;
    invoke-virtual {v4, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5d
    .line 249
    add-int/lit8 v2, v2, 0x1

    #@5f
    goto :goto_56

    #@60
    .line 253
    .end local v5           #participant:Ljava/lang/String;
    :cond_60
    invoke-virtual {p2}, Lcom/lge/ims/service/im/IMMessage;->getContentType()Ljava/lang/String;

    #@63
    move-result-object v1

    #@64
    .line 255
    .local v1, contentType:Ljava/lang/String;
    const-string v6, "application/xml"

    #@66
    invoke-virtual {v1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@69
    move-result v6

    #@6a
    if-eqz v6, :cond_6e

    #@6c
    .line 256
    const-string v1, "application/http-ft+xml"

    #@6e
    .line 261
    :cond_6e
    invoke-virtual {v4, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@71
    .line 262
    invoke-virtual {p2}, Lcom/lge/ims/service/im/IMMessage;->getContent()Ljava/lang/String;

    #@74
    move-result-object v6

    #@75
    invoke-virtual {v4, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@78
    .line 263
    invoke-virtual {p2}, Lcom/lge/ims/service/im/IMMessage;->getMessageId()Ljava/lang/String;

    #@7b
    move-result-object v6

    #@7c
    invoke-virtual {v4, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@7f
    .line 264
    invoke-virtual {v4, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@82
    .line 265
    invoke-virtual {v4, p4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@85
    .line 267
    invoke-direct {p0, v4}, Lcom/lge/ims/service/im/ChatImpl;->sendMessageToJNI(Landroid/os/Parcel;)V

    #@88
    .line 268
    return-void
.end method

.method public leaveConference([Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "recipients"
    .parameter "contributionId"

    #@0
    .prologue
    .line 158
    if-eqz p1, :cond_4

    #@2
    if-nez p2, :cond_a

    #@4
    .line 160
    :cond_4
    const-string v5, "leaveConference() : Invalid Parameter"

    #@6
    invoke-static {v5}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@9
    .line 179
    :goto_9
    return-void

    #@a
    .line 164
    :cond_a
    const/16 v5, 0x17

    #@c
    invoke-static {v5}, Lcom/lge/ims/JNICore;->getInterface(I)I

    #@f
    move-result v5

    #@10
    iput v5, p0, Lcom/lge/ims/service/im/ChatImpl;->mNativeObj:I

    #@12
    .line 165
    iget v5, p0, Lcom/lge/ims/service/im/ChatImpl;->mNativeObj:I

    #@14
    invoke-static {v5, p0}, Lcom/lge/ims/JNICore;->setListener(ILcom/lge/ims/JNICoreListener;)I

    #@17
    .line 167
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@1a
    move-result-object v3

    #@1b
    .line 169
    .local v3, parcel:Landroid/os/Parcel;
    const/16 v5, 0x298a

    #@1d
    invoke-virtual {v3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@20
    .line 170
    invoke-virtual {v3, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@23
    .line 171
    array-length v5, p1

    #@24
    invoke-virtual {v3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@27
    .line 172
    move-object v0, p1

    #@28
    .local v0, arr$:[Ljava/lang/String;
    array-length v2, v0

    #@29
    .local v2, len$:I
    const/4 v1, 0x0

    #@2a
    .local v1, i$:I
    :goto_2a
    if-ge v1, v2, :cond_34

    #@2c
    aget-object v4, v0, v1

    #@2e
    .line 173
    .local v4, recipient:Ljava/lang/String;
    invoke-virtual {v3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@31
    .line 172
    add-int/lit8 v1, v1, 0x1

    #@33
    goto :goto_2a

    #@34
    .line 176
    .end local v4           #recipient:Ljava/lang/String;
    :cond_34
    invoke-direct {p0, v3}, Lcom/lge/ims/service/im/ChatImpl;->sendMessageToJNI(Landroid/os/Parcel;)V

    #@37
    .line 178
    const/4 v5, 0x1

    #@38
    iput-boolean v5, p0, Lcom/lge/ims/service/im/ChatImpl;->mIsInviteForBye:Z

    #@3a
    goto :goto_9
.end method

.method public onMessage(Landroid/os/Parcel;)V
    .registers 47
    .parameter "parcel"

    #@0
    .prologue
    .line 354
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v34

    #@4
    .line 356
    .local v34, msg:I
    move-object/from16 v0, p0

    #@6
    iget-object v0, v0, Lcom/lge/ims/service/im/ChatImpl;->mListener:Lcom/lge/ims/service/im/IChatListener;

    #@8
    move-object/from16 v41, v0

    #@a
    if-nez v41, :cond_12

    #@c
    .line 357
    const-string v41, "NO LISTENER!!!"

    #@e
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@11
    .line 593
    :goto_11
    return-void

    #@12
    .line 362
    :cond_12
    packed-switch v34, :pswitch_data_5a6

    #@15
    .line 577
    :try_start_15
    const-string v41, "NOT HANDLED!!!"

    #@17
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_1a} :catch_1b

    #@1a
    goto :goto_11

    #@1b
    .line 580
    :catch_1b
    move-exception v29

    #@1c
    .line 581
    .local v29, e:Ljava/lang/Exception;
    invoke-virtual/range {v29 .. v29}, Ljava/lang/Exception;->printStackTrace()V

    #@1f
    .line 582
    const-string v41, "RemoteException!!!"

    #@21
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@24
    .line 584
    new-instance v31, Landroid/content/Intent;

    #@26
    invoke-direct/range {v31 .. v31}, Landroid/content/Intent;-><init>()V

    #@29
    .line 585
    .local v31, intent:Landroid/content/Intent;
    const-string v41, "com.lge.ims.service.im.IIMService"

    #@2b
    move-object/from16 v0, v31

    #@2d
    move-object/from16 v1, v41

    #@2f
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@32
    .line 586
    const-string v41, "deleteChat"

    #@34
    const/16 v42, 0x1

    #@36
    move-object/from16 v0, v31

    #@38
    move-object/from16 v1, v41

    #@3a
    move/from16 v2, v42

    #@3c
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@3f
    .line 587
    new-instance v30, Landroid/os/Bundle;

    #@41
    invoke-direct/range {v30 .. v30}, Landroid/os/Bundle;-><init>()V

    #@44
    .line 588
    .local v30, extra:Landroid/os/Bundle;
    const-string v41, "ChatImpl"

    #@46
    move-object/from16 v0, v30

    #@48
    move-object/from16 v1, v41

    #@4a
    move-object/from16 v2, p0

    #@4c
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    #@4f
    .line 589
    move-object/from16 v0, v31

    #@51
    move-object/from16 v1, v30

    #@53
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    #@56
    .line 591
    move-object/from16 v0, p0

    #@58
    iget-object v0, v0, Lcom/lge/ims/service/im/ChatImpl;->mContext:Landroid/content/Context;

    #@5a
    move-object/from16 v41, v0

    #@5c
    move-object/from16 v0, v41

    #@5e
    move-object/from16 v1, v31

    #@60
    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    #@63
    goto :goto_11

    #@64
    .line 364
    .end local v29           #e:Ljava/lang/Exception;
    .end local v30           #extra:Landroid/os/Bundle;
    .end local v31           #intent:Landroid/content/Intent;
    :pswitch_64
    :try_start_64
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@67
    move-result v37

    #@68
    .line 365
    .local v37, result:I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@6b
    move-result-object v39

    #@6c
    .line 366
    .local v39, sessionId:Ljava/lang/String;
    new-instance v41, Ljava/lang/StringBuilder;

    #@6e
    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    #@71
    const-string v42, "\tESTABLISHED_IND : result = "

    #@73
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v41

    #@77
    move-object/from16 v0, v41

    #@79
    move/from16 v1, v37

    #@7b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v41

    #@7f
    const-string v42, ", sessionId = "

    #@81
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v41

    #@85
    move-object/from16 v0, v41

    #@87
    move-object/from16 v1, v39

    #@89
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v41

    #@8d
    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@90
    move-result-object v41

    #@91
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@94
    .line 368
    move-object/from16 v0, p0

    #@96
    iget-boolean v0, v0, Lcom/lge/ims/service/im/ChatImpl;->mIsRejoin:Z

    #@98
    move/from16 v41, v0

    #@9a
    if-eqz v41, :cond_b8

    #@9c
    .line 369
    if-nez v37, :cond_a9

    #@9e
    .line 370
    move-object/from16 v0, p0

    #@a0
    iget-object v0, v0, Lcom/lge/ims/service/im/ChatImpl;->mListener:Lcom/lge/ims/service/im/IChatListener;

    #@a2
    move-object/from16 v41, v0

    #@a4
    invoke-interface/range {v41 .. v41}, Lcom/lge/ims/service/im/IChatListener;->chatRejoined()V

    #@a7
    goto/16 :goto_11

    #@a9
    .line 372
    :cond_a9
    move-object/from16 v0, p0

    #@ab
    iget-object v0, v0, Lcom/lge/ims/service/im/ChatImpl;->mListener:Lcom/lge/ims/service/im/IChatListener;

    #@ad
    move-object/from16 v41, v0

    #@af
    move-object/from16 v0, v41

    #@b1
    move/from16 v1, v37

    #@b3
    invoke-interface {v0, v1}, Lcom/lge/ims/service/im/IChatListener;->chatRejoinFailed(I)V

    #@b6
    goto/16 :goto_11

    #@b8
    .line 374
    :cond_b8
    move-object/from16 v0, p0

    #@ba
    iget-boolean v0, v0, Lcom/lge/ims/service/im/ChatImpl;->mIsInviteForBye:Z

    #@bc
    move/from16 v41, v0

    #@be
    if-eqz v41, :cond_e4

    #@c0
    .line 375
    if-eqz v37, :cond_ca

    #@c2
    const/16 v41, -0x6

    #@c4
    move/from16 v0, v37

    #@c6
    move/from16 v1, v41

    #@c8
    if-ne v0, v1, :cond_d5

    #@ca
    .line 377
    :cond_ca
    move-object/from16 v0, p0

    #@cc
    iget-object v0, v0, Lcom/lge/ims/service/im/ChatImpl;->mListener:Lcom/lge/ims/service/im/IChatListener;

    #@ce
    move-object/from16 v41, v0

    #@d0
    invoke-interface/range {v41 .. v41}, Lcom/lge/ims/service/im/IChatListener;->chatLeft()V

    #@d3
    goto/16 :goto_11

    #@d5
    .line 379
    :cond_d5
    move-object/from16 v0, p0

    #@d7
    iget-object v0, v0, Lcom/lge/ims/service/im/ChatImpl;->mListener:Lcom/lge/ims/service/im/IChatListener;

    #@d9
    move-object/from16 v41, v0

    #@db
    move-object/from16 v0, v41

    #@dd
    move/from16 v1, v37

    #@df
    invoke-interface {v0, v1}, Lcom/lge/ims/service/im/IChatListener;->chatLeaveFailed(I)V

    #@e2
    goto/16 :goto_11

    #@e4
    .line 382
    :cond_e4
    if-nez v37, :cond_f5

    #@e6
    .line 383
    move-object/from16 v0, p0

    #@e8
    iget-object v0, v0, Lcom/lge/ims/service/im/ChatImpl;->mListener:Lcom/lge/ims/service/im/IChatListener;

    #@ea
    move-object/from16 v41, v0

    #@ec
    move-object/from16 v0, v41

    #@ee
    move-object/from16 v1, v39

    #@f0
    invoke-interface {v0, v1}, Lcom/lge/ims/service/im/IChatListener;->chatStarted(Ljava/lang/String;)V

    #@f3
    goto/16 :goto_11

    #@f5
    .line 385
    :cond_f5
    move-object/from16 v0, p0

    #@f7
    iget-object v0, v0, Lcom/lge/ims/service/im/ChatImpl;->mListener:Lcom/lge/ims/service/im/IChatListener;

    #@f9
    move-object/from16 v41, v0

    #@fb
    move-object/from16 v0, v41

    #@fd
    move/from16 v1, v37

    #@ff
    invoke-interface {v0, v1}, Lcom/lge/ims/service/im/IChatListener;->chatStartFailed(I)V

    #@102
    goto/16 :goto_11

    #@104
    .line 392
    .end local v37           #result:I
    .end local v39           #sessionId:Ljava/lang/String;
    :pswitch_104
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@107
    move-result v36

    #@108
    .line 393
    .local v36, reason:I
    new-instance v41, Ljava/lang/StringBuilder;

    #@10a
    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    #@10d
    const-string v42, "\tTERMINATED_IND : reason = "

    #@10f
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@112
    move-result-object v41

    #@113
    move-object/from16 v0, v41

    #@115
    move/from16 v1, v36

    #@117
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11a
    move-result-object v41

    #@11b
    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11e
    move-result-object v41

    #@11f
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@122
    .line 395
    move-object/from16 v0, p0

    #@124
    iget-object v0, v0, Lcom/lge/ims/service/im/ChatImpl;->mListener:Lcom/lge/ims/service/im/IChatListener;

    #@126
    move-object/from16 v41, v0

    #@128
    move-object/from16 v0, v41

    #@12a
    move/from16 v1, v36

    #@12c
    invoke-interface {v0, v1}, Lcom/lge/ims/service/im/IChatListener;->chatTerminated(I)V

    #@12f
    goto/16 :goto_11

    #@131
    .line 400
    .end local v36           #reason:I
    :pswitch_131
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@134
    move-result v37

    #@135
    .line 401
    .restart local v37       #result:I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@138
    move-result-object v39

    #@139
    .line 402
    .restart local v39       #sessionId:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@13c
    move-result-object v20

    #@13d
    .line 404
    .local v20, contributionId:Ljava/lang/String;
    new-instance v41, Ljava/lang/StringBuilder;

    #@13f
    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    #@142
    const-string v42, "\tEXTENDEDCONFERENCE_IND: result = "

    #@144
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@147
    move-result-object v41

    #@148
    move-object/from16 v0, v41

    #@14a
    move/from16 v1, v37

    #@14c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14f
    move-result-object v41

    #@150
    const-string v42, ", sessionId = "

    #@152
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@155
    move-result-object v41

    #@156
    move-object/from16 v0, v41

    #@158
    move-object/from16 v1, v39

    #@15a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15d
    move-result-object v41

    #@15e
    const-string v42, ", contributionId = "

    #@160
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@163
    move-result-object v41

    #@164
    move-object/from16 v0, v41

    #@166
    move-object/from16 v1, v20

    #@168
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16b
    move-result-object v41

    #@16c
    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16f
    move-result-object v41

    #@170
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@173
    .line 406
    if-nez v37, :cond_18c

    #@175
    .line 407
    move-object/from16 v0, v20

    #@177
    move-object/from16 v1, p0

    #@179
    iput-object v0, v1, Lcom/lge/ims/service/im/ChatImpl;->mContributionId:Ljava/lang/String;

    #@17b
    .line 408
    move-object/from16 v0, p0

    #@17d
    iget-object v0, v0, Lcom/lge/ims/service/im/ChatImpl;->mListener:Lcom/lge/ims/service/im/IChatListener;

    #@17f
    move-object/from16 v41, v0

    #@181
    move-object/from16 v0, v41

    #@183
    move-object/from16 v1, v39

    #@185
    move-object/from16 v2, v20

    #@187
    invoke-interface {v0, v1, v2}, Lcom/lge/ims/service/im/IChatListener;->chatExtended(Ljava/lang/String;Ljava/lang/String;)V

    #@18a
    goto/16 :goto_11

    #@18c
    .line 410
    :cond_18c
    move-object/from16 v0, p0

    #@18e
    iget-object v0, v0, Lcom/lge/ims/service/im/ChatImpl;->mListener:Lcom/lge/ims/service/im/IChatListener;

    #@190
    move-object/from16 v41, v0

    #@192
    move-object/from16 v0, v41

    #@194
    move/from16 v1, v37

    #@196
    invoke-interface {v0, v1}, Lcom/lge/ims/service/im/IChatListener;->chatExtensionFailed(I)V

    #@199
    goto/16 :goto_11

    #@19b
    .line 416
    .end local v20           #contributionId:Ljava/lang/String;
    .end local v37           #result:I
    .end local v39           #sessionId:Ljava/lang/String;
    :pswitch_19b
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@19e
    move-result v37

    #@19f
    .line 417
    .restart local v37       #result:I
    new-instance v41, Ljava/lang/StringBuilder;

    #@1a1
    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    #@1a4
    const-string v42, "\tADDEDPARTICIPANTS_IND : result = "

    #@1a6
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a9
    move-result-object v41

    #@1aa
    move-object/from16 v0, v41

    #@1ac
    move/from16 v1, v37

    #@1ae
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b1
    move-result-object v41

    #@1b2
    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b5
    move-result-object v41

    #@1b6
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@1b9
    .line 419
    if-nez v37, :cond_1c6

    #@1bb
    .line 420
    move-object/from16 v0, p0

    #@1bd
    iget-object v0, v0, Lcom/lge/ims/service/im/ChatImpl;->mListener:Lcom/lge/ims/service/im/IChatListener;

    #@1bf
    move-object/from16 v41, v0

    #@1c1
    invoke-interface/range {v41 .. v41}, Lcom/lge/ims/service/im/IChatListener;->addParticipantsRequestDelivered()V

    #@1c4
    goto/16 :goto_11

    #@1c6
    .line 422
    :cond_1c6
    move-object/from16 v0, p0

    #@1c8
    iget-object v0, v0, Lcom/lge/ims/service/im/ChatImpl;->mListener:Lcom/lge/ims/service/im/IChatListener;

    #@1ca
    move-object/from16 v41, v0

    #@1cc
    move-object/from16 v0, v41

    #@1ce
    move/from16 v1, v37

    #@1d0
    invoke-interface {v0, v1}, Lcom/lge/ims/service/im/IChatListener;->addParticipantsRequestFailed(I)V

    #@1d3
    goto/16 :goto_11

    #@1d5
    .line 428
    .end local v37           #result:I
    :pswitch_1d5
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1d8
    move-result-object v35

    #@1d9
    .line 429
    .local v35, participant:Ljava/lang/String;
    new-instance v41, Ljava/lang/StringBuilder;

    #@1db
    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    #@1de
    const-string v42, "\tJOINEDPARTICIPANT_IND : participant = "

    #@1e0
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e3
    move-result-object v41

    #@1e4
    move-object/from16 v0, v41

    #@1e6
    move-object/from16 v1, v35

    #@1e8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1eb
    move-result-object v41

    #@1ec
    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ef
    move-result-object v41

    #@1f0
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@1f3
    .line 431
    move-object/from16 v0, p0

    #@1f5
    iget-object v0, v0, Lcom/lge/ims/service/im/ChatImpl;->mListener:Lcom/lge/ims/service/im/IChatListener;

    #@1f7
    move-object/from16 v41, v0

    #@1f9
    move-object/from16 v0, v41

    #@1fb
    move-object/from16 v1, v35

    #@1fd
    invoke-interface {v0, v1}, Lcom/lge/ims/service/im/IChatListener;->participantJoined(Ljava/lang/String;)V

    #@200
    goto/16 :goto_11

    #@202
    .line 436
    .end local v35           #participant:Ljava/lang/String;
    :pswitch_202
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@205
    move-result-object v35

    #@206
    .line 437
    .restart local v35       #participant:Ljava/lang/String;
    new-instance v41, Ljava/lang/StringBuilder;

    #@208
    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    #@20b
    const-string v42, "\tLEFTPARTICIPANTS_IND : participant = "

    #@20d
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@210
    move-result-object v41

    #@211
    move-object/from16 v0, v41

    #@213
    move-object/from16 v1, v35

    #@215
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@218
    move-result-object v41

    #@219
    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21c
    move-result-object v41

    #@21d
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@220
    .line 439
    move-object/from16 v0, p0

    #@222
    iget-object v0, v0, Lcom/lge/ims/service/im/ChatImpl;->mListener:Lcom/lge/ims/service/im/IChatListener;

    #@224
    move-object/from16 v41, v0

    #@226
    move-object/from16 v0, v41

    #@228
    move-object/from16 v1, v35

    #@22a
    invoke-interface {v0, v1}, Lcom/lge/ims/service/im/IChatListener;->participantLeft(Ljava/lang/String;)V

    #@22d
    goto/16 :goto_11

    #@22f
    .line 444
    .end local v35           #participant:Ljava/lang/String;
    :pswitch_22f
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@232
    move-result v37

    #@233
    .line 445
    .restart local v37       #result:I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@236
    move-result-object v6

    #@237
    .line 446
    .local v6, messageId:Ljava/lang/String;
    new-instance v41, Ljava/lang/StringBuilder;

    #@239
    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    #@23c
    const-string v42, "\tSENTMESSAGE_IND : messageId = "

    #@23e
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@241
    move-result-object v41

    #@242
    move-object/from16 v0, v41

    #@244
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@247
    move-result-object v41

    #@248
    const-string v42, ", result= "

    #@24a
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24d
    move-result-object v41

    #@24e
    move-object/from16 v0, v41

    #@250
    move/from16 v1, v37

    #@252
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@255
    move-result-object v41

    #@256
    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@259
    move-result-object v41

    #@25a
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@25d
    .line 448
    if-nez v37, :cond_26c

    #@25f
    .line 449
    move-object/from16 v0, p0

    #@261
    iget-object v0, v0, Lcom/lge/ims/service/im/ChatImpl;->mListener:Lcom/lge/ims/service/im/IChatListener;

    #@263
    move-object/from16 v41, v0

    #@265
    move-object/from16 v0, v41

    #@267
    invoke-interface {v0, v6}, Lcom/lge/ims/service/im/IChatListener;->messageSent(Ljava/lang/String;)V

    #@26a
    goto/16 :goto_11

    #@26c
    .line 451
    :cond_26c
    move-object/from16 v0, p0

    #@26e
    iget-object v0, v0, Lcom/lge/ims/service/im/ChatImpl;->mListener:Lcom/lge/ims/service/im/IChatListener;

    #@270
    move-object/from16 v41, v0

    #@272
    move-object/from16 v0, v41

    #@274
    move/from16 v1, v37

    #@276
    invoke-interface {v0, v6, v1}, Lcom/lge/ims/service/im/IChatListener;->messageSendFailed(Ljava/lang/String;I)V

    #@279
    goto/16 :goto_11

    #@27b
    .line 457
    .end local v6           #messageId:Ljava/lang/String;
    .end local v37           #result:I
    :pswitch_27b
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@27e
    move-result-object v38

    #@27f
    .line 458
    .local v38, sender:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@282
    move-result-object v27

    #@283
    .line 459
    .local v27, displayName:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@286
    move-result-object v33

    #@287
    .line 460
    .local v33, mimeType:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@28a
    move-result-object v19

    #@28b
    .line 461
    .local v19, content:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@28e
    move-result-object v6

    #@28f
    .line 462
    .restart local v6       #messageId:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@292
    move-result-object v25

    #@293
    .line 463
    .local v25, dateTime:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@296
    move-result v28

    #@297
    .line 464
    .local v28, displayNotification:I
    new-instance v41, Ljava/lang/StringBuilder;

    #@299
    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    #@29c
    const-string v42, "\tRECEIVEDMESSAGE_IND : sender = "

    #@29e
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a1
    move-result-object v41

    #@2a2
    move-object/from16 v0, v41

    #@2a4
    move-object/from16 v1, v38

    #@2a6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a9
    move-result-object v41

    #@2aa
    const-string v42, ", displayName = "

    #@2ac
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2af
    move-result-object v41

    #@2b0
    move-object/from16 v0, v41

    #@2b2
    move-object/from16 v1, v27

    #@2b4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b7
    move-result-object v41

    #@2b8
    const-string v42, ", message = "

    #@2ba
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2bd
    move-result-object v41

    #@2be
    move-object/from16 v0, v41

    #@2c0
    move-object/from16 v1, v19

    #@2c2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c5
    move-result-object v41

    #@2c6
    const-string v42, ", messageId = "

    #@2c8
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2cb
    move-result-object v41

    #@2cc
    move-object/from16 v0, v41

    #@2ce
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d1
    move-result-object v41

    #@2d2
    const-string v42, ", dateTime = "

    #@2d4
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d7
    move-result-object v41

    #@2d8
    move-object/from16 v0, v41

    #@2da
    move-object/from16 v1, v25

    #@2dc
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2df
    move-result-object v41

    #@2e0
    const-string v42, ", displayNotification = "

    #@2e2
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e5
    move-result-object v41

    #@2e6
    move-object/from16 v0, v41

    #@2e8
    move/from16 v1, v28

    #@2ea
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2ed
    move-result-object v41

    #@2ee
    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f1
    move-result-object v41

    #@2f2
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@2f5
    .line 470
    new-instance v32, Lcom/lge/ims/service/im/IMMessage;

    #@2f7
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@2fa
    move-result-object v41

    #@2fb
    move-object/from16 v0, v32

    #@2fd
    move-object/from16 v1, v41

    #@2ff
    invoke-direct {v0, v1}, Lcom/lge/ims/service/im/IMMessage;-><init>(Landroid/os/Parcel;)V

    #@302
    .line 471
    .local v32, message:Lcom/lge/ims/service/im/IMMessage;
    invoke-virtual/range {v32 .. v33}, Lcom/lge/ims/service/im/IMMessage;->setContentType(Ljava/lang/String;)V

    #@305
    .line 472
    move-object/from16 v0, v32

    #@307
    move-object/from16 v1, v19

    #@309
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/im/IMMessage;->setContent(Ljava/lang/String;)V

    #@30c
    .line 473
    move-object/from16 v0, v32

    #@30e
    invoke-virtual {v0, v6}, Lcom/lge/ims/service/im/IMMessage;->setMessageId(Ljava/lang/String;)V

    #@311
    .line 474
    move-object/from16 v0, v32

    #@313
    move-object/from16 v1, v25

    #@315
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/im/IMMessage;->setTimeStamp(Ljava/lang/String;)V

    #@318
    .line 476
    move-object/from16 v0, p0

    #@31a
    iget-object v0, v0, Lcom/lge/ims/service/im/ChatImpl;->mListener:Lcom/lge/ims/service/im/IChatListener;

    #@31c
    move-object/from16 v42, v0

    #@31e
    if-lez v28, :cond_331

    #@320
    const/16 v41, 0x1

    #@322
    :goto_322
    move-object/from16 v0, v42

    #@324
    move-object/from16 v1, v38

    #@326
    move-object/from16 v2, v27

    #@328
    move-object/from16 v3, v32

    #@32a
    move/from16 v4, v41

    #@32c
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/lge/ims/service/im/IChatListener;->messageReceived(Ljava/lang/String;Ljava/lang/String;Lcom/lge/ims/service/im/IMMessage;Z)V

    #@32f
    goto/16 :goto_11

    #@331
    :cond_331
    const/16 v41, 0x0

    #@333
    goto :goto_322

    #@334
    .line 481
    .end local v6           #messageId:Ljava/lang/String;
    .end local v19           #content:Ljava/lang/String;
    .end local v25           #dateTime:Ljava/lang/String;
    .end local v27           #displayName:Ljava/lang/String;
    .end local v28           #displayNotification:I
    .end local v32           #message:Lcom/lge/ims/service/im/IMMessage;
    .end local v33           #mimeType:Ljava/lang/String;
    .end local v38           #sender:Ljava/lang/String;
    :pswitch_334
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@337
    move-result-object v38

    #@338
    .line 482
    .restart local v38       #sender:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@33b
    move-result v40

    #@33c
    .line 483
    .local v40, timeOut:I
    new-instance v41, Ljava/lang/StringBuilder;

    #@33e
    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    #@341
    const-string v42, "\tRECEIVEDISCOMPOSING_IND : sender = "

    #@343
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@346
    move-result-object v41

    #@347
    move-object/from16 v0, v41

    #@349
    move-object/from16 v1, v38

    #@34b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34e
    move-result-object v41

    #@34f
    const-string v42, ", timeout = "

    #@351
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@354
    move-result-object v41

    #@355
    move-object/from16 v0, v41

    #@357
    move/from16 v1, v40

    #@359
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35c
    move-result-object v41

    #@35d
    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@360
    move-result-object v41

    #@361
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@364
    .line 485
    move-object/from16 v0, p0

    #@366
    iget-object v0, v0, Lcom/lge/ims/service/im/ChatImpl;->mListener:Lcom/lge/ims/service/im/IChatListener;

    #@368
    move-object/from16 v41, v0

    #@36a
    move-object/from16 v0, v41

    #@36c
    move-object/from16 v1, v38

    #@36e
    move/from16 v2, v40

    #@370
    invoke-interface {v0, v1, v2}, Lcom/lge/ims/service/im/IChatListener;->composingIndicatorReceived(Ljava/lang/String;I)V

    #@373
    goto/16 :goto_11

    #@375
    .line 491
    .end local v38           #sender:Ljava/lang/String;
    .end local v40           #timeOut:I
    :pswitch_375
    const-string v41, "\tIM_RECEIVEDFILELINK_IND"

    #@377
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@37a
    .line 492
    new-instance v41, Ljava/lang/StringBuilder;

    #@37c
    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    #@37f
    const-string v42, "mContributionId = "

    #@381
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@384
    move-result-object v41

    #@385
    move-object/from16 v0, p0

    #@387
    iget-object v0, v0, Lcom/lge/ims/service/im/ChatImpl;->mContributionId:Ljava/lang/String;

    #@389
    move-object/from16 v42, v0

    #@38b
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38e
    move-result-object v41

    #@38f
    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@392
    move-result-object v41

    #@393
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@396
    .line 494
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@399
    move-result-object v38

    #@39a
    .line 495
    .restart local v38       #sender:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@39d
    move-result-object v27

    #@39e
    .line 496
    .restart local v27       #displayName:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3a1
    move-result-object v6

    #@3a2
    .line 497
    .restart local v6       #messageId:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3a5
    move-result-object v25

    #@3a6
    .line 500
    .restart local v25       #dateTime:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@3a9
    move-result v9

    #@3aa
    .line 501
    .local v9, thumbnailSize:I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3ad
    move-result-object v7

    #@3ae
    .line 502
    .local v7, thumbnailName:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3b1
    move-result-object v8

    #@3b2
    .line 503
    .local v8, thumbnailContentType:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3b5
    move-result-object v10

    #@3b6
    .line 506
    .local v10, thumbnailUrl:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@3b9
    move-result v13

    #@3ba
    .line 507
    .local v13, fileSize:I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3bd
    move-result-object v41

    #@3be
    invoke-static/range {v41 .. v41}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    #@3c1
    move-result-object v11

    #@3c2
    .line 508
    .local v11, fileName:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3c5
    move-result-object v12

    #@3c6
    .line 509
    .local v12, fileContentType:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3c9
    move-result-object v14

    #@3ca
    .line 512
    .local v14, fileDownloadUrl:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3cd
    move-result-object v15

    #@3ce
    .line 514
    .local v15, expiryDateTime:Ljava/lang/String;
    new-instance v41, Ljava/lang/StringBuilder;

    #@3d0
    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    #@3d3
    const-string v42, " : TERMINATED_IND: until="

    #@3d5
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d8
    move-result-object v41

    #@3d9
    move-object/from16 v0, v41

    #@3db
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3de
    move-result-object v41

    #@3df
    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e2
    move-result-object v41

    #@3e3
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@3e6
    .line 516
    const-string v41, "GMT"

    #@3e8
    invoke-static/range {v41 .. v41}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    #@3eb
    move-result-object v41

    #@3ec
    invoke-static/range {v41 .. v41}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    #@3ef
    move-result-object v17

    #@3f0
    .line 517
    .local v17, cal:Ljava/util/Calendar;
    invoke-virtual/range {v17 .. v17}, Ljava/util/Calendar;->getTimeInMillis()J

    #@3f3
    move-result-wide v41

    #@3f4
    const-wide/32 v43, 0x5265c00

    #@3f7
    add-long v22, v41, v43

    #@3f9
    .line 518
    .local v22, currentTime:J
    move-object/from16 v0, v17

    #@3fb
    move-wide/from16 v1, v22

    #@3fd
    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@400
    .line 519
    invoke-virtual/range {v17 .. v17}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    #@403
    move-result-object v21

    #@404
    .line 520
    .local v21, currentLocalTime:Ljava/util/Date;
    new-instance v24, Ljava/text/SimpleDateFormat;

    #@406
    const-string v41, "yyyy-MM-dd\'T\'HH:mm:ss\'Z\'"

    #@408
    move-object/from16 v0, v24

    #@40a
    move-object/from16 v1, v41

    #@40c
    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    #@40f
    .line 521
    .local v24, dateFormat:Ljava/text/SimpleDateFormat;
    const-string v41, "GMT"

    #@411
    invoke-static/range {v41 .. v41}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    #@414
    move-result-object v41

    #@415
    move-object/from16 v0, v24

    #@417
    move-object/from16 v1, v41

    #@419
    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    #@41c
    .line 522
    move-object/from16 v0, v24

    #@41e
    move-object/from16 v1, v21

    #@420
    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    #@423
    move-result-object v15

    #@424
    .line 524
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@427
    move-result v26

    #@428
    .line 525
    .local v26, deferredMessages:I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@42b
    move-result v28

    #@42c
    .line 527
    .restart local v28       #displayNotification:I
    invoke-static/range {v38 .. v38}, Lcom/lge/ims/service/im/IMFTUtil;->getCallState(Ljava/lang/String;)I

    #@42f
    move-result v18

    #@430
    .line 529
    .local v18, callState:I
    const/16 v16, 0x0

    #@432
    .line 530
    .local v16, bDeferredMessages:Z
    if-nez v26, :cond_589

    #@434
    .line 531
    const/16 v16, 0x0

    #@436
    .line 536
    :goto_436
    new-instance v41, Ljava/lang/StringBuilder;

    #@438
    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    #@43b
    const-string v42, "\tmessageId = "

    #@43d
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@440
    move-result-object v41

    #@441
    move-object/from16 v0, v41

    #@443
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@446
    move-result-object v41

    #@447
    const-string v42, ", sender = "

    #@449
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44c
    move-result-object v41

    #@44d
    move-object/from16 v0, v41

    #@44f
    move-object/from16 v1, v38

    #@451
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@454
    move-result-object v41

    #@455
    const-string v42, "\n"

    #@457
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45a
    move-result-object v41

    #@45b
    const-string v42, ", dateTime = "

    #@45d
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@460
    move-result-object v41

    #@461
    move-object/from16 v0, v41

    #@463
    move-object/from16 v1, v25

    #@465
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@468
    move-result-object v41

    #@469
    const-string v42, "\n"

    #@46b
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46e
    move-result-object v41

    #@46f
    const-string v42, ", fileName = "

    #@471
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@474
    move-result-object v41

    #@475
    move-object/from16 v0, v41

    #@477
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47a
    move-result-object v41

    #@47b
    const-string v42, ", fileContentType="

    #@47d
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@480
    move-result-object v41

    #@481
    move-object/from16 v0, v41

    #@483
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@486
    move-result-object v41

    #@487
    const-string v42, "\n"

    #@489
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48c
    move-result-object v41

    #@48d
    const-string v42, ", thumbnailUrl = "

    #@48f
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@492
    move-result-object v41

    #@493
    move-object/from16 v0, v41

    #@495
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@498
    move-result-object v41

    #@499
    const-string v42, "\n"

    #@49b
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49e
    move-result-object v41

    #@49f
    const-string v42, ", fileDownloadUrl = "

    #@4a1
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a4
    move-result-object v41

    #@4a5
    move-object/from16 v0, v41

    #@4a7
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4aa
    move-result-object v41

    #@4ab
    const-string v42, "\n"

    #@4ad
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b0
    move-result-object v41

    #@4b1
    const-string v42, ", expiryDateTime = "

    #@4b3
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b6
    move-result-object v41

    #@4b7
    move-object/from16 v0, v41

    #@4b9
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4bc
    move-result-object v41

    #@4bd
    const-string v42, "\n"

    #@4bf
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c2
    move-result-object v41

    #@4c3
    const-string v42, ", deferredMessages = "

    #@4c5
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c8
    move-result-object v41

    #@4c9
    move-object/from16 v0, v41

    #@4cb
    move/from16 v1, v26

    #@4cd
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4d0
    move-result-object v41

    #@4d1
    const-string v42, "\n"

    #@4d3
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d6
    move-result-object v41

    #@4d7
    const-string v42, ", displayNotification = "

    #@4d9
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4dc
    move-result-object v41

    #@4dd
    move-object/from16 v0, v41

    #@4df
    move/from16 v1, v28

    #@4e1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4e4
    move-result-object v41

    #@4e5
    const-string v42, "\n"

    #@4e7
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4ea
    move-result-object v41

    #@4eb
    const-string v42, ", call state = "

    #@4ed
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f0
    move-result-object v41

    #@4f1
    move-object/from16 v0, v41

    #@4f3
    move/from16 v1, v18

    #@4f5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4f8
    move-result-object v41

    #@4f9
    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4fc
    move-result-object v41

    #@4fd
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@500
    .line 546
    new-instance v31, Landroid/content/Intent;

    #@502
    invoke-direct/range {v31 .. v31}, Landroid/content/Intent;-><init>()V

    #@505
    .line 548
    .restart local v31       #intent:Landroid/content/Intent;
    const/16 v41, 0x299d

    #@507
    move/from16 v0, v34

    #@509
    move/from16 v1, v41

    #@50b
    if-ne v0, v1, :cond_58d

    #@50d
    .line 549
    const-string v41, "com.lge.ims.rcsim.INVITE_RECEIVED_WITH_FILELINK"

    #@50f
    move-object/from16 v0, v31

    #@511
    move-object/from16 v1, v41

    #@513
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@516
    .line 554
    :goto_516
    const-string v41, "callState"

    #@518
    move-object/from16 v0, v31

    #@51a
    move-object/from16 v1, v41

    #@51c
    move/from16 v2, v18

    #@51e
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@521
    .line 555
    const-string v41, "remoteUser"

    #@523
    move-object/from16 v0, v31

    #@525
    move-object/from16 v1, v41

    #@527
    move-object/from16 v2, v38

    #@529
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@52c
    .line 556
    const-string v41, "displayName"

    #@52e
    move-object/from16 v0, v31

    #@530
    move-object/from16 v1, v41

    #@532
    move-object/from16 v2, v27

    #@534
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@537
    .line 557
    const-string v41, "dateTime"

    #@539
    move-object/from16 v0, v31

    #@53b
    move-object/from16 v1, v41

    #@53d
    move-object/from16 v2, v25

    #@53f
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@542
    .line 558
    const-string v41, "displayNotification"

    #@544
    move-object/from16 v0, v31

    #@546
    move-object/from16 v1, v41

    #@548
    move/from16 v2, v28

    #@54a
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@54d
    .line 559
    const-string v41, "deferredMessages"

    #@54f
    move-object/from16 v0, v31

    #@551
    move-object/from16 v1, v41

    #@553
    move/from16 v2, v16

    #@555
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@558
    .line 560
    const-string v41, "contributionId"

    #@55a
    move-object/from16 v0, p0

    #@55c
    iget-object v0, v0, Lcom/lge/ims/service/im/ChatImpl;->mContributionId:Ljava/lang/String;

    #@55e
    move-object/from16 v42, v0

    #@560
    move-object/from16 v0, v31

    #@562
    move-object/from16 v1, v41

    #@564
    move-object/from16 v2, v42

    #@566
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@569
    .line 563
    new-instance v5, Lcom/lge/ims/service/im/IMFileInfo;

    #@56b
    invoke-direct {v5}, Lcom/lge/ims/service/im/IMFileInfo;-><init>()V

    #@56e
    .line 564
    .local v5, imFileInfo:Lcom/lge/ims/service/im/IMFileInfo;
    invoke-virtual/range {v5 .. v15}, Lcom/lge/ims/service/im/IMFileInfo;->setParamOfFileLinkIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    #@571
    .line 567
    const-string v41, "FileInfo"

    #@573
    move-object/from16 v0, v31

    #@575
    move-object/from16 v1, v41

    #@577
    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@57a
    .line 569
    move-object/from16 v0, p0

    #@57c
    iget-object v0, v0, Lcom/lge/ims/service/im/ChatImpl;->mContext:Landroid/content/Context;

    #@57e
    move-object/from16 v41, v0

    #@580
    if-nez v41, :cond_597

    #@582
    .line 570
    const-string v41, "context is null"

    #@584
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@587
    goto/16 :goto_11

    #@589
    .line 533
    .end local v5           #imFileInfo:Lcom/lge/ims/service/im/IMFileInfo;
    .end local v31           #intent:Landroid/content/Intent;
    :cond_589
    const/16 v16, 0x1

    #@58b
    goto/16 :goto_436

    #@58d
    .line 551
    .restart local v31       #intent:Landroid/content/Intent;
    :cond_58d
    const-string v41, "com.lge.ims.rcsim.CONFERENCE_INVITE_RECEIVED_WITH_FILELINK"

    #@58f
    move-object/from16 v0, v31

    #@591
    move-object/from16 v1, v41

    #@593
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@596
    goto :goto_516

    #@597
    .line 572
    .restart local v5       #imFileInfo:Lcom/lge/ims/service/im/IMFileInfo;
    :cond_597
    move-object/from16 v0, p0

    #@599
    iget-object v0, v0, Lcom/lge/ims/service/im/ChatImpl;->mContext:Landroid/content/Context;

    #@59b
    move-object/from16 v41, v0

    #@59d
    move-object/from16 v0, v41

    #@59f
    move-object/from16 v1, v31

    #@5a1
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_5a4
    .catch Ljava/lang/Exception; {:try_start_64 .. :try_end_5a4} :catch_1b

    #@5a4
    goto/16 :goto_11

    #@5a6
    .line 362
    :pswitch_data_5a6
    .packed-switch 0x2994
        :pswitch_64
        :pswitch_104
        :pswitch_131
        :pswitch_19b
        :pswitch_1d5
        :pswitch_202
        :pswitch_22f
        :pswitch_27b
        :pswitch_334
        :pswitch_375
        :pswitch_375
    .end packed-switch
.end method

.method public reject()V
    .registers 3

    #@0
    .prologue
    .line 192
    const-string v1, "reject()"

    #@2
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@5
    .line 194
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v0

    #@9
    .line 196
    .local v0, parcel:Landroid/os/Parcel;
    const/16 v1, 0x298d

    #@b
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@e
    .line 198
    invoke-direct {p0, v0}, Lcom/lge/ims/service/im/ChatImpl;->sendMessageToJNI(Landroid/os/Parcel;)V

    #@11
    .line 199
    return-void
.end method

.method public rejectDeniedInvitation()V
    .registers 3

    #@0
    .prologue
    .line 202
    const-string v1, "rejectDeniedInvitation()"

    #@2
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@5
    .line 204
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v0

    #@9
    .line 205
    .local v0, parcel:Landroid/os/Parcel;
    const/16 v1, 0x2993

    #@b
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@e
    .line 207
    invoke-direct {p0, v0}, Lcom/lge/ims/service/im/ChatImpl;->sendMessageToJNI(Landroid/os/Parcel;)V

    #@11
    .line 208
    return-void
.end method

.method public rejoinConference(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "sessionId"
    .parameter "contributionId"

    #@0
    .prologue
    .line 134
    if-eqz p1, :cond_4

    #@2
    if-nez p2, :cond_a

    #@4
    .line 136
    :cond_4
    const-string v1, "rejoinConference() : Invalid Parameter"

    #@6
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@9
    .line 155
    :goto_9
    return-void

    #@a
    .line 140
    :cond_a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "rejoinConference() : sessionId = "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    const-string v2, ", contributionId = "

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@2a
    .line 143
    const/16 v1, 0x17

    #@2c
    invoke-static {v1}, Lcom/lge/ims/JNICore;->getInterface(I)I

    #@2f
    move-result v1

    #@30
    iput v1, p0, Lcom/lge/ims/service/im/ChatImpl;->mNativeObj:I

    #@32
    .line 144
    iget v1, p0, Lcom/lge/ims/service/im/ChatImpl;->mNativeObj:I

    #@34
    invoke-static {v1, p0}, Lcom/lge/ims/JNICore;->setListener(ILcom/lge/ims/JNICoreListener;)I

    #@37
    .line 146
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3a
    move-result-object v0

    #@3b
    .line 148
    .local v0, parcel:Landroid/os/Parcel;
    const/16 v1, 0x2989

    #@3d
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@40
    .line 149
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@43
    .line 150
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@46
    .line 152
    invoke-direct {p0, v0}, Lcom/lge/ims/service/im/ChatImpl;->sendMessageToJNI(Landroid/os/Parcel;)V

    #@49
    .line 154
    const/4 v1, 0x1

    #@4a
    iput-boolean v1, p0, Lcom/lge/ims/service/im/ChatImpl;->mIsRejoin:Z

    #@4c
    goto :goto_9
.end method

.method public sendChatInvitation(Ljava/lang/String;Lcom/lge/ims/service/im/IMMessage;Ljava/lang/String;)V
    .registers 8
    .parameter "recipient"
    .parameter "message"
    .parameter "contributionId"

    #@0
    .prologue
    .line 54
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v3, "sendChatInvitation() : recipient = "

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    const-string v3, ", content = "

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {p2}, Lcom/lge/ims/service/im/IMMessage;->getContent()Ljava/lang/String;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    const-string v3, ", messageId = "

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {p2}, Lcom/lge/ims/service/im/IMMessage;->getMessageId()Ljava/lang/String;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    const-string v3, ", contributionId = "

    #@2d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v2

    #@39
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@3c
    .line 59
    invoke-virtual {p2}, Lcom/lge/ims/service/im/IMMessage;->getContentType()Ljava/lang/String;

    #@3f
    move-result-object v0

    #@40
    .line 61
    .local v0, contentType:Ljava/lang/String;
    const-string v2, "application/xml"

    #@42
    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@45
    move-result v2

    #@46
    if-eqz v2, :cond_4a

    #@48
    .line 62
    const-string v0, "application/http-ft+xml"

    #@4a
    .line 66
    :cond_4a
    if-eqz v0, :cond_95

    #@4c
    .line 67
    new-instance v2, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v3, "contentType = "

    #@53
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v2

    #@57
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v2

    #@5b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v2

    #@5f
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@62
    .line 72
    :goto_62
    const/16 v2, 0x16

    #@64
    invoke-static {v2}, Lcom/lge/ims/JNICore;->getInterface(I)I

    #@67
    move-result v2

    #@68
    iput v2, p0, Lcom/lge/ims/service/im/ChatImpl;->mNativeObj:I

    #@6a
    .line 73
    iget v2, p0, Lcom/lge/ims/service/im/ChatImpl;->mNativeObj:I

    #@6c
    invoke-static {v2, p0}, Lcom/lge/ims/JNICore;->setListener(ILcom/lge/ims/JNICoreListener;)I

    #@6f
    .line 75
    iput-object p3, p0, Lcom/lge/ims/service/im/ChatImpl;->mContributionId:Ljava/lang/String;

    #@71
    .line 77
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@74
    move-result-object v1

    #@75
    .line 79
    .local v1, parcel:Landroid/os/Parcel;
    const/16 v2, 0x2987

    #@77
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@7a
    .line 80
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@7d
    .line 81
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@80
    .line 82
    invoke-virtual {p2}, Lcom/lge/ims/service/im/IMMessage;->getContent()Ljava/lang/String;

    #@83
    move-result-object v2

    #@84
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@87
    .line 83
    invoke-virtual {p2}, Lcom/lge/ims/service/im/IMMessage;->getMessageId()Ljava/lang/String;

    #@8a
    move-result-object v2

    #@8b
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@8e
    .line 84
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@91
    .line 86
    invoke-direct {p0, v1}, Lcom/lge/ims/service/im/ChatImpl;->sendMessageToJNI(Landroid/os/Parcel;)V

    #@94
    .line 87
    return-void

    #@95
    .line 69
    .end local v1           #parcel:Landroid/os/Parcel;
    :cond_95
    const-string v0, "text/plain"

    #@97
    goto :goto_62
.end method

.method public sendComposingIndicator(I)V
    .registers 5
    .parameter "timeOut"

    #@0
    .prologue
    .line 298
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "sendComposingIndicator() : timeOut = "

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@16
    .line 300
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@19
    move-result-object v0

    #@1a
    .line 302
    .local v0, parcel:Landroid/os/Parcel;
    const/16 v1, 0x2991

    #@1c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 303
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@22
    .line 305
    invoke-direct {p0, v0}, Lcom/lge/ims/service/im/ChatImpl;->sendMessageToJNI(Landroid/os/Parcel;)V

    #@25
    .line 306
    return-void
.end method

.method public sendConferenceInvitation(Ljava/lang/String;[Ljava/lang/String;Lcom/lge/ims/service/im/IMMessage;Ljava/lang/String;Ljava/lang/String;)V
    .registers 14
    .parameter "remoteUser"
    .parameter "recipients"
    .parameter "message"
    .parameter "contributionId"
    .parameter "sessionReplaces"

    #@0
    .prologue
    .line 90
    new-instance v6, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v7, "sendConferenceInvitation() : numOfParticipants = "

    #@7
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v6

    #@b
    array-length v7, p2

    #@c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f
    move-result-object v6

    #@10
    const-string v7, ", remoteUser = "

    #@12
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v6

    #@16
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v6

    #@1a
    const-string v7, ", content = "

    #@1c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v6

    #@20
    invoke-virtual {p3}, Lcom/lge/ims/service/im/IMMessage;->getContent()Ljava/lang/String;

    #@23
    move-result-object v7

    #@24
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v6

    #@28
    const-string v7, ", messageId = "

    #@2a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v6

    #@2e
    invoke-virtual {p3}, Lcom/lge/ims/service/im/IMMessage;->getMessageId()Ljava/lang/String;

    #@31
    move-result-object v7

    #@32
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v6

    #@36
    const-string v7, ", contributionId = "

    #@38
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v6

    #@3c
    invoke-virtual {v6, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v6

    #@40
    const-string v7, ", sessionReplaces = "

    #@42
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v6

    #@46
    invoke-virtual {v6, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v6

    #@4a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v6

    #@4e
    invoke-static {v6}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@51
    .line 97
    invoke-virtual {p3}, Lcom/lge/ims/service/im/IMMessage;->getContentType()Ljava/lang/String;

    #@54
    move-result-object v1

    #@55
    .line 100
    .local v1, contentType:Ljava/lang/String;
    const-string v6, "application/xml"

    #@57
    invoke-virtual {v1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@5a
    move-result v6

    #@5b
    if-eqz v6, :cond_5f

    #@5d
    .line 101
    const-string v1, "application/http-ft+xml"

    #@5f
    .line 105
    :cond_5f
    if-eqz v1, :cond_a3

    #@61
    .line 106
    new-instance v6, Ljava/lang/StringBuilder;

    #@63
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@66
    const-string v7, "contentType = "

    #@68
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v6

    #@6c
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v6

    #@70
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@73
    move-result-object v6

    #@74
    invoke-static {v6}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@77
    .line 111
    :goto_77
    const/16 v6, 0x17

    #@79
    invoke-static {v6}, Lcom/lge/ims/JNICore;->getInterface(I)I

    #@7c
    move-result v6

    #@7d
    iput v6, p0, Lcom/lge/ims/service/im/ChatImpl;->mNativeObj:I

    #@7f
    .line 112
    iget v6, p0, Lcom/lge/ims/service/im/ChatImpl;->mNativeObj:I

    #@81
    invoke-static {v6, p0}, Lcom/lge/ims/JNICore;->setListener(ILcom/lge/ims/JNICoreListener;)I

    #@84
    .line 114
    iput-object p4, p0, Lcom/lge/ims/service/im/ChatImpl;->mContributionId:Ljava/lang/String;

    #@86
    .line 116
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@89
    move-result-object v4

    #@8a
    .line 118
    .local v4, parcel:Landroid/os/Parcel;
    const/16 v6, 0x2988

    #@8c
    invoke-virtual {v4, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@8f
    .line 119
    invoke-virtual {v4, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@92
    .line 120
    array-length v6, p2

    #@93
    invoke-virtual {v4, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@96
    .line 121
    move-object v0, p2

    #@97
    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    #@98
    .local v3, len$:I
    const/4 v2, 0x0

    #@99
    .local v2, i$:I
    :goto_99
    if-ge v2, v3, :cond_a6

    #@9b
    aget-object v5, v0, v2

    #@9d
    .line 122
    .local v5, recipient:Ljava/lang/String;
    invoke-virtual {v4, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@a0
    .line 121
    add-int/lit8 v2, v2, 0x1

    #@a2
    goto :goto_99

    #@a3
    .line 108
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v2           #i$:I
    .end local v3           #len$:I
    .end local v4           #parcel:Landroid/os/Parcel;
    .end local v5           #recipient:Ljava/lang/String;
    :cond_a3
    const-string v1, "text/plain"

    #@a5
    goto :goto_77

    #@a6
    .line 124
    .restart local v0       #arr$:[Ljava/lang/String;
    .restart local v2       #i$:I
    .restart local v3       #len$:I
    .restart local v4       #parcel:Landroid/os/Parcel;
    :cond_a6
    invoke-virtual {v4, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@a9
    .line 125
    invoke-virtual {p3}, Lcom/lge/ims/service/im/IMMessage;->getContent()Ljava/lang/String;

    #@ac
    move-result-object v6

    #@ad
    invoke-virtual {v4, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@b0
    .line 126
    invoke-virtual {p3}, Lcom/lge/ims/service/im/IMMessage;->getMessageId()Ljava/lang/String;

    #@b3
    move-result-object v6

    #@b4
    invoke-virtual {v4, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@b7
    .line 127
    invoke-virtual {v4, p4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@ba
    .line 128
    invoke-virtual {v4, p5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@bd
    .line 130
    invoke-direct {p0, v4}, Lcom/lge/ims/service/im/ChatImpl;->sendMessageToJNI(Landroid/os/Parcel;)V

    #@c0
    .line 131
    return-void
.end method

.method public sendDisplayNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "recipient"
    .parameter "messageId"
    .parameter "dateTime"

    #@0
    .prologue
    .line 309
    if-eqz p1, :cond_6

    #@2
    if-eqz p2, :cond_6

    #@4
    if-nez p3, :cond_c

    #@6
    .line 312
    :cond_6
    const-string v1, "sendDisplayNotification() : Invalid Parameter"

    #@8
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@b
    .line 327
    :goto_b
    return-void

    #@c
    .line 316
    :cond_c
    new-instance v1, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v2, "sendDisplayNotification() : recipient = "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    const-string v2, ", messageId = "

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    const-string v2, ", dateTime = "

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@36
    .line 319
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@39
    move-result-object v0

    #@3a
    .line 321
    .local v0, parcel:Landroid/os/Parcel;
    const/16 v1, 0x2992

    #@3c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@3f
    .line 322
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@42
    .line 323
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@45
    .line 324
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@48
    .line 326
    invoke-direct {p0, v0}, Lcom/lge/ims/service/im/ChatImpl;->sendMessageToJNI(Landroid/os/Parcel;)V

    #@4b
    goto :goto_b
.end method

.method public sendMessage(Lcom/lge/ims/service/im/IMMessage;ZZ)V
    .registers 8
    .parameter "message"
    .parameter "deliveryReport"
    .parameter "displayReport"

    #@0
    .prologue
    .line 271
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v3, "sendMessage() : message = "

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {p1}, Lcom/lge/ims/service/im/IMMessage;->getContent()Ljava/lang/String;

    #@e
    move-result-object v3

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    const-string v3, ", messageId = "

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {p1}, Lcom/lge/ims/service/im/IMMessage;->getMessageId()Ljava/lang/String;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@28
    .line 274
    invoke-virtual {p1}, Lcom/lge/ims/service/im/IMMessage;->getContentType()Ljava/lang/String;

    #@2b
    move-result-object v0

    #@2c
    .line 277
    .local v0, contentType:Ljava/lang/String;
    const-string v2, "application/xml"

    #@2e
    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@31
    move-result v2

    #@32
    if-eqz v2, :cond_36

    #@34
    .line 278
    const-string v0, "application/http-ft+xml"

    #@36
    .line 281
    :cond_36
    if-eqz v0, :cond_6c

    #@38
    .line 282
    new-instance v2, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v3, "contentType = "

    #@3f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v2

    #@43
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v2

    #@4b
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@4e
    .line 287
    :goto_4e
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@51
    move-result-object v1

    #@52
    .line 289
    .local v1, parcel:Landroid/os/Parcel;
    const/16 v2, 0x2990

    #@54
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@57
    .line 290
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5a
    .line 291
    invoke-virtual {p1}, Lcom/lge/ims/service/im/IMMessage;->getContent()Ljava/lang/String;

    #@5d
    move-result-object v2

    #@5e
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@61
    .line 292
    invoke-virtual {p1}, Lcom/lge/ims/service/im/IMMessage;->getMessageId()Ljava/lang/String;

    #@64
    move-result-object v2

    #@65
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@68
    .line 294
    invoke-direct {p0, v1}, Lcom/lge/ims/service/im/ChatImpl;->sendMessageToJNI(Landroid/os/Parcel;)V

    #@6b
    .line 295
    return-void

    #@6c
    .line 284
    .end local v1           #parcel:Landroid/os/Parcel;
    :cond_6c
    const-string v0, "text/plain"

    #@6e
    goto :goto_4e
.end method

.method public setListener(Lcom/lge/ims/service/im/IChatListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 47
    const-string v0, "setListener()"

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@5
    .line 49
    iput-object p1, p0, Lcom/lge/ims/service/im/ChatImpl;->mListener:Lcom/lge/ims/service/im/IChatListener;

    #@7
    .line 50
    return-void
.end method
