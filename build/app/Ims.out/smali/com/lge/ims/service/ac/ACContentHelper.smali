.class public Lcom/lge/ims/service/ac/ACContentHelper;
.super Ljava/lang/Object;
.source "ACContentHelper.java"


# static fields
.field private static final TABLE_COM_SIP:Ljava/lang/String; = "lgims_com_kt_sip"

.field private static final TAG:Ljava/lang/String; = "ACContentHelper"

.field private static mIMSI:Ljava/lang/String;

.field private static mRefId:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 32
    const/4 v0, 0x0

    #@1
    sput-object v0, Lcom/lge/ims/service/ac/ACContentHelper;->mIMSI:Ljava/lang/String;

    #@3
    .line 33
    const/4 v0, -0x1

    #@4
    sput v0, Lcom/lge/ims/service/ac/ACContentHelper;->mRefId:I

    #@6
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 35
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 36
    return-void
.end method

.method public static checkNUpdateContentForAC(Landroid/content/Context;Lcom/lge/ims/service/ac/ACContentCache;)Z
    .registers 16
    .parameter "context"
    .parameter "contentCache"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v13, 0x1

    #@2
    .line 367
    const-string v10, "ACContentHelper"

    #@4
    const-string v11, "checkNUpdateContentForAC"

    #@6
    invoke-static {v10, v11}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 369
    if-nez p0, :cond_c

    #@b
    .line 458
    :cond_b
    :goto_b
    return v5

    #@c
    .line 373
    :cond_c
    invoke-static {p0}, Lcom/lge/ims/service/ac/ACContentHelper;->getReferenceId(Landroid/content/Context;)I

    #@f
    move-result v7

    #@10
    .line 375
    .local v7, refId:I
    if-gez v7, :cond_2b

    #@12
    .line 376
    const-string v10, "ACContentHelper"

    #@14
    new-instance v11, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v12, "Invalid refId="

    #@1b
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v11

    #@1f
    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v11

    #@23
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v11

    #@27
    invoke-static {v10, v11}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@2a
    goto :goto_b

    #@2b
    .line 380
    :cond_2b
    const/4 v5, 0x0

    #@2c
    .line 383
    .local v5, isUpdated:Z
    invoke-virtual {p1}, Lcom/lge/ims/service/ac/ACContentCache;->getSubscriber()Lcom/lge/ims/service/ac/content/ACSubscriber;

    #@2f
    move-result-object v2

    #@30
    .line 385
    .local v2, cacheSubscriber:Lcom/lge/ims/service/ac/content/ACSubscriber;
    if-eqz v2, :cond_9d

    #@32
    invoke-virtual {v2}, Lcom/lge/ims/service/ac/content/ACSubscriber;->isContentPresent()Z

    #@35
    move-result v10

    #@36
    if-eqz v10, :cond_9d

    #@38
    .line 386
    invoke-static {p0, v7}, Lcom/lge/ims/service/ac/ACContentHelper;->getSubscriberFromAC(Landroid/content/Context;I)Lcom/lge/ims/service/ac/content/ACSubscriber;

    #@3b
    move-result-object v9

    #@3c
    .line 388
    .local v9, subscriber:Lcom/lge/ims/service/ac/content/ACSubscriber;
    if-eqz v9, :cond_9d

    #@3e
    .line 389
    invoke-static {}, Lcom/lge/ims/Configuration;->isDebugOn()Z

    #@41
    move-result v10

    #@42
    if-eqz v10, :cond_60

    #@44
    .line 390
    const-string v10, "ACContentHelper"

    #@46
    new-instance v11, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v12, "Subscriber :: "

    #@4d
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v11

    #@51
    invoke-virtual {v9}, Lcom/lge/ims/service/ac/content/ACSubscriber;->toString()Ljava/lang/String;

    #@54
    move-result-object v12

    #@55
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v11

    #@59
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v11

    #@5d
    invoke-static {v10, v11}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@60
    .line 394
    :cond_60
    iget-object v10, v9, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPUs:Ljava/util/ArrayList;

    #@62
    if-eqz v10, :cond_8c

    #@64
    .line 395
    const/4 v3, 0x0

    #@65
    .local v3, i:I
    :goto_65
    iget-object v10, v9, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPUs:Ljava/util/ArrayList;

    #@67
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    #@6a
    move-result v10

    #@6b
    if-ge v3, v10, :cond_8c

    #@6d
    .line 396
    iget-object v10, v9, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPUs:Ljava/util/ArrayList;

    #@6f
    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@72
    move-result-object v4

    #@73
    check-cast v4, Ljava/lang/String;

    #@75
    .line 398
    .local v4, impu:Ljava/lang/String;
    if-nez v4, :cond_7d

    #@77
    .line 399
    iget-object v10, v9, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPUs:Ljava/util/ArrayList;

    #@79
    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@7c
    goto :goto_65

    #@7d
    .line 403
    :cond_7d
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@80
    move-result v10

    #@81
    if-nez v10, :cond_89

    #@83
    .line 404
    iget-object v10, v9, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPUs:Ljava/util/ArrayList;

    #@85
    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@88
    goto :goto_65

    #@89
    .line 408
    :cond_89
    add-int/lit8 v3, v3, 0x1

    #@8b
    .line 409
    goto :goto_65

    #@8c
    .line 412
    .end local v3           #i:I
    .end local v4           #impu:Ljava/lang/String;
    :cond_8c
    invoke-virtual {v9, v2}, Lcom/lge/ims/service/ac/content/ACSubscriber;->containsContent(Lcom/lge/ims/service/ac/content/ACSubscriber;)Z

    #@8f
    move-result v10

    #@90
    if-nez v10, :cond_9d

    #@92
    .line 413
    const-string v10, "ACContentHelper"

    #@94
    const-string v11, "Subscriber is updated"

    #@96
    invoke-static {v10, v11}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@99
    .line 414
    invoke-static {p0, v2, v7, v13}, Lcom/lge/ims/service/ac/ACContentHelper;->updateSubscriberForAC(Landroid/content/Context;Lcom/lge/ims/service/ac/content/ACSubscriber;IZ)V

    #@9c
    .line 415
    const/4 v5, 0x1

    #@9d
    .line 421
    .end local v9           #subscriber:Lcom/lge/ims/service/ac/content/ACSubscriber;
    :cond_9d
    invoke-virtual {p1}, Lcom/lge/ims/service/ac/ACContentCache;->getProxyAddress()Lcom/lge/ims/service/ac/content/ACProxyAddress;

    #@a0
    move-result-object v0

    #@a1
    .line 423
    .local v0, cacheProxyAddress:Lcom/lge/ims/service/ac/content/ACProxyAddress;
    if-eqz v0, :cond_e2

    #@a3
    invoke-virtual {v0}, Lcom/lge/ims/service/ac/content/ACProxyAddress;->isContentPresent()Z

    #@a6
    move-result v10

    #@a7
    if-eqz v10, :cond_e2

    #@a9
    .line 424
    invoke-static {p0, v7}, Lcom/lge/ims/service/ac/ACContentHelper;->getProxyAddressFromAC(Landroid/content/Context;I)Lcom/lge/ims/service/ac/content/ACProxyAddress;

    #@ac
    move-result-object v6

    #@ad
    .line 426
    .local v6, proxyAddress:Lcom/lge/ims/service/ac/content/ACProxyAddress;
    if-eqz v6, :cond_e2

    #@af
    .line 427
    invoke-static {}, Lcom/lge/ims/Configuration;->isDebugOn()Z

    #@b2
    move-result v10

    #@b3
    if-eqz v10, :cond_d1

    #@b5
    .line 428
    const-string v10, "ACContentHelper"

    #@b7
    new-instance v11, Ljava/lang/StringBuilder;

    #@b9
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@bc
    const-string v12, "ProxyAddress :: "

    #@be
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v11

    #@c2
    invoke-virtual {v6}, Lcom/lge/ims/service/ac/content/ACProxyAddress;->toString()Ljava/lang/String;

    #@c5
    move-result-object v12

    #@c6
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v11

    #@ca
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cd
    move-result-object v11

    #@ce
    invoke-static {v10, v11}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@d1
    .line 431
    :cond_d1
    invoke-virtual {v6, v0}, Lcom/lge/ims/service/ac/content/ACProxyAddress;->containsContent(Lcom/lge/ims/service/ac/content/ACProxyAddress;)Z

    #@d4
    move-result v10

    #@d5
    if-nez v10, :cond_e2

    #@d7
    .line 432
    const-string v10, "ACContentHelper"

    #@d9
    const-string v11, "ProxyAddress is updated"

    #@db
    invoke-static {v10, v11}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@de
    .line 433
    invoke-static {p0, v0, v7, v13}, Lcom/lge/ims/service/ac/ACContentHelper;->updateProxyAddressForAC(Landroid/content/Context;Lcom/lge/ims/service/ac/content/ACProxyAddress;IZ)V

    #@e1
    .line 434
    const/4 v5, 0x1

    #@e2
    .line 440
    .end local v6           #proxyAddress:Lcom/lge/ims/service/ac/content/ACProxyAddress;
    :cond_e2
    invoke-virtual {p1}, Lcom/lge/ims/service/ac/ACContentCache;->getServiceAvailability()Lcom/lge/ims/service/ac/content/ACServiceAvailability;

    #@e5
    move-result-object v1

    #@e6
    .line 442
    .local v1, cacheServiceAvailability:Lcom/lge/ims/service/ac/content/ACServiceAvailability;
    if-eqz v1, :cond_b

    #@e8
    invoke-virtual {v1}, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->isContentPresent()Z

    #@eb
    move-result v10

    #@ec
    if-eqz v10, :cond_b

    #@ee
    .line 443
    invoke-static {p0, v7}, Lcom/lge/ims/service/ac/ACContentHelper;->getServiceAvailabilityFromAC(Landroid/content/Context;I)Lcom/lge/ims/service/ac/content/ACServiceAvailability;

    #@f1
    move-result-object v8

    #@f2
    .line 445
    .local v8, serviceAvailability:Lcom/lge/ims/service/ac/content/ACServiceAvailability;
    if-eqz v8, :cond_b

    #@f4
    .line 446
    invoke-static {}, Lcom/lge/ims/Configuration;->isDebugOn()Z

    #@f7
    move-result v10

    #@f8
    if-eqz v10, :cond_116

    #@fa
    .line 447
    const-string v10, "ACContentHelper"

    #@fc
    new-instance v11, Ljava/lang/StringBuilder;

    #@fe
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@101
    const-string v12, "ServiceAvailability :: "

    #@103
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@106
    move-result-object v11

    #@107
    invoke-virtual {v8}, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->toString()Ljava/lang/String;

    #@10a
    move-result-object v12

    #@10b
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10e
    move-result-object v11

    #@10f
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@112
    move-result-object v11

    #@113
    invoke-static {v10, v11}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@116
    .line 450
    :cond_116
    invoke-virtual {v8, v1}, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->containsContent(Lcom/lge/ims/service/ac/content/ACServiceAvailability;)Z

    #@119
    move-result v10

    #@11a
    if-nez v10, :cond_b

    #@11c
    .line 451
    const-string v10, "ACContentHelper"

    #@11e
    const-string v11, "ServiceAvailability is updated"

    #@120
    invoke-static {v10, v11}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@123
    .line 452
    invoke-static {p0, v1, v7, v13}, Lcom/lge/ims/service/ac/ACContentHelper;->updateServiceAvailabilityForAC(Landroid/content/Context;Lcom/lge/ims/service/ac/content/ACServiceAvailability;IZ)V

    #@126
    .line 453
    const/4 v5, 0x1

    #@127
    goto/16 :goto_b
.end method

.method private static createReferenceId(Landroid/content/Context;)I
    .registers 10
    .parameter "context"

    #@0
    .prologue
    .line 711
    invoke-static {}, Lcom/lge/ims/PhoneInfo;->getSubscriberIdWithDummy()Ljava/lang/String;

    #@3
    move-result-object v3

    #@4
    .line 712
    .local v3, imsi:Ljava/lang/String;
    invoke-static {}, Lcom/lge/ims/PhoneInfo;->getPhoneNumberExcludingNationalPrefix()Ljava/lang/String;

    #@7
    move-result-object v4

    #@8
    .line 714
    .local v4, msisdn:Ljava/lang/String;
    invoke-static {}, Lcom/lge/ims/service/ac/ACConfiguration;->isUsimOn()Z

    #@b
    move-result v6

    #@c
    if-nez v6, :cond_16

    #@e
    .line 715
    invoke-static {}, Lcom/lge/ims/service/ac/ACConfiguration;->getIMSI()Ljava/lang/String;

    #@11
    move-result-object v3

    #@12
    .line 716
    invoke-static {}, Lcom/lge/ims/service/ac/ACConfiguration;->getPhoneNumber()Ljava/lang/String;

    #@15
    move-result-object v4

    #@16
    .line 719
    :cond_16
    invoke-static {p0}, Lcom/lge/ims/service/ac/ACContentHelper;->getNewReferenceId(Landroid/content/Context;)I

    #@19
    move-result v5

    #@1a
    .line 721
    .local v5, refId:I
    if-ltz v5, :cond_73

    #@1c
    .line 722
    const-string v6, "ACContentHelper"

    #@1e
    new-instance v7, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v8, "createReferenceId :: imsi="

    #@25
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v7

    #@29
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v7

    #@2d
    const-string v8, ", msisdn="

    #@2f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v7

    #@33
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v7

    #@37
    const-string v8, ", refId="

    #@39
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v7

    #@3d
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v7

    #@41
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v7

    #@45
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@48
    .line 724
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4b
    move-result-object v0

    #@4c
    .line 725
    .local v0, cr:Landroid/content/ContentResolver;
    new-instance v1, Landroid/content/ContentValues;

    #@4e
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    #@51
    .line 727
    .local v1, cv:Landroid/content/ContentValues;
    const-string v6, "id"

    #@53
    invoke-virtual {v1, v6, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@56
    .line 728
    const-string v6, "msisdn"

    #@58
    invoke-virtual {v1, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@5b
    .line 729
    const-string v6, "ref_id"

    #@5d
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@60
    move-result-object v7

    #@61
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@64
    .line 730
    const-string v6, "next_ref_id"

    #@66
    const/4 v7, 0x0

    #@67
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6a
    move-result-object v7

    #@6b
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@6e
    .line 733
    :try_start_6e
    sget-object v6, Lcom/lge/ims/provider/ac/AC$Provisioning$IMSI;->CONTENT_URI:Landroid/net/Uri;

    #@70
    invoke-virtual {v0, v6, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_73
    .catch Ljava/lang/Exception; {:try_start_6e .. :try_end_73} :catch_74

    #@73
    .line 740
    .end local v0           #cr:Landroid/content/ContentResolver;
    .end local v1           #cv:Landroid/content/ContentValues;
    :cond_73
    :goto_73
    return v5

    #@74
    .line 734
    .restart local v0       #cr:Landroid/content/ContentResolver;
    .restart local v1       #cv:Landroid/content/ContentValues;
    :catch_74
    move-exception v2

    #@75
    .line 735
    .local v2, e:Ljava/lang/Exception;
    const-string v6, "ACContentHelper"

    #@77
    new-instance v7, Ljava/lang/StringBuilder;

    #@79
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@7c
    const-string v8, "Exception :: "

    #@7e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v7

    #@82
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@85
    move-result-object v8

    #@86
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v7

    #@8a
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8d
    move-result-object v7

    #@8e
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@91
    .line 736
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    #@94
    goto :goto_73
.end method

.method public static deleteContentForAC(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 39
    if-nez p0, :cond_3

    #@2
    .line 57
    :cond_2
    :goto_2
    return-void

    #@3
    .line 43
    :cond_3
    invoke-static {p0}, Lcom/lge/ims/service/ac/ACContentHelper;->getReferenceId(Landroid/content/Context;)I

    #@6
    move-result v0

    #@7
    .line 45
    .local v0, refId:I
    if-ltz v0, :cond_2

    #@9
    .line 49
    invoke-static {p0, v0}, Lcom/lge/ims/service/ac/ACContentHelper;->deleteIMSIFromAC(Landroid/content/Context;I)V

    #@c
    .line 50
    invoke-static {p0, v0}, Lcom/lge/ims/service/ac/ACContentHelper;->deleteVersionFromAC(Landroid/content/Context;I)V

    #@f
    .line 51
    invoke-static {p0, v0}, Lcom/lge/ims/service/ac/ACContentHelper;->deleteSubscriberFromAC(Landroid/content/Context;I)V

    #@12
    .line 52
    invoke-static {p0, v0}, Lcom/lge/ims/service/ac/ACContentHelper;->deleteProxyAddressFromAC(Landroid/content/Context;I)V

    #@15
    .line 53
    invoke-static {p0, v0}, Lcom/lge/ims/service/ac/ACContentHelper;->deleteServiceAvailabilityFromAC(Landroid/content/Context;I)V

    #@18
    .line 55
    const/4 v1, 0x0

    #@19
    sput-object v1, Lcom/lge/ims/service/ac/ACContentHelper;->mIMSI:Ljava/lang/String;

    #@1b
    .line 56
    const/4 v1, -0x1

    #@1c
    sput v1, Lcom/lge/ims/service/ac/ACContentHelper;->mRefId:I

    #@1e
    goto :goto_2
.end method

.method private static deleteIMSIFromAC(Landroid/content/Context;I)V
    .registers 8
    .parameter "context"
    .parameter "refId"

    #@0
    .prologue
    .line 744
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3
    move-result-object v1

    #@4
    .line 747
    .local v1, cr:Landroid/content/ContentResolver;
    :try_start_4
    sget-object v3, Lcom/lge/ims/provider/ac/AC$Provisioning$IMSI;->CONTENT_URI:Landroid/net/Uri;

    #@6
    new-instance v4, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v5, "ref_id=\'"

    #@d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v4

    #@11
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@14
    move-result-object v5

    #@15
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v4

    #@19
    const-string v5, "\'"

    #@1b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v4

    #@1f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v4

    #@23
    const/4 v5, 0x0

    #@24
    invoke-virtual {v1, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@27
    move-result v0

    #@28
    .line 748
    .local v0, count:I
    const-string v3, "ACContentHelper"

    #@2a
    new-instance v4, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v5, "IMSI :: row with ref_id="

    #@31
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v4

    #@35
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38
    move-result-object v4

    #@39
    const-string v5, " is successfully deleted; count="

    #@3b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v4

    #@3f
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@42
    move-result-object v4

    #@43
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v4

    #@47
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4a
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4a} :catch_4b

    #@4a
    .line 753
    .end local v0           #count:I
    :goto_4a
    return-void

    #@4b
    .line 749
    :catch_4b
    move-exception v2

    #@4c
    .line 750
    .local v2, e:Ljava/lang/Exception;
    const-string v3, "ACContentHelper"

    #@4e
    new-instance v4, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    const-string v5, "Exception :: "

    #@55
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v4

    #@59
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@5c
    move-result-object v5

    #@5d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v4

    #@61
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v4

    #@65
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@68
    .line 751
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    #@6b
    goto :goto_4a
.end method

.method private static deleteProxyAddressFromAC(Landroid/content/Context;I)V
    .registers 8
    .parameter "context"
    .parameter "refId"

    #@0
    .prologue
    .line 756
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3
    move-result-object v1

    #@4
    .line 759
    .local v1, cr:Landroid/content/ContentResolver;
    :try_start_4
    sget-object v3, Lcom/lge/ims/provider/ac/AC$Provisioning$ProxyAddress;->CONTENT_URI:Landroid/net/Uri;

    #@6
    new-instance v4, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v5, "id=\'"

    #@d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v4

    #@11
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@14
    move-result-object v5

    #@15
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v4

    #@19
    const-string v5, "\'"

    #@1b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v4

    #@1f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v4

    #@23
    const/4 v5, 0x0

    #@24
    invoke-virtual {v1, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@27
    move-result v0

    #@28
    .line 760
    .local v0, count:I
    const-string v3, "ACContentHelper"

    #@2a
    new-instance v4, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v5, "ProxyAddress :: row with id="

    #@31
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v4

    #@35
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38
    move-result-object v4

    #@39
    const-string v5, " is successfully deleted; count="

    #@3b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v4

    #@3f
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@42
    move-result-object v4

    #@43
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v4

    #@47
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4a
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4a} :catch_4b

    #@4a
    .line 765
    .end local v0           #count:I
    :goto_4a
    return-void

    #@4b
    .line 761
    :catch_4b
    move-exception v2

    #@4c
    .line 762
    .local v2, e:Ljava/lang/Exception;
    const-string v3, "ACContentHelper"

    #@4e
    new-instance v4, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    const-string v5, "Exception :: "

    #@55
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v4

    #@59
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@5c
    move-result-object v5

    #@5d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v4

    #@61
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v4

    #@65
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@68
    .line 763
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    #@6b
    goto :goto_4a
.end method

.method private static deleteServiceAvailabilityFromAC(Landroid/content/Context;I)V
    .registers 8
    .parameter "context"
    .parameter "refId"

    #@0
    .prologue
    .line 768
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3
    move-result-object v1

    #@4
    .line 771
    .local v1, cr:Landroid/content/ContentResolver;
    :try_start_4
    sget-object v3, Lcom/lge/ims/provider/ac/AC$Provisioning$ServiceAvailability;->CONTENT_URI:Landroid/net/Uri;

    #@6
    new-instance v4, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v5, "id=\'"

    #@d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v4

    #@11
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@14
    move-result-object v5

    #@15
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v4

    #@19
    const-string v5, "\'"

    #@1b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v4

    #@1f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v4

    #@23
    const/4 v5, 0x0

    #@24
    invoke-virtual {v1, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@27
    move-result v0

    #@28
    .line 772
    .local v0, count:I
    const-string v3, "ACContentHelper"

    #@2a
    new-instance v4, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v5, "ServiceAvailability :: row with id="

    #@31
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v4

    #@35
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38
    move-result-object v4

    #@39
    const-string v5, " is successfully deleted; count="

    #@3b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v4

    #@3f
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@42
    move-result-object v4

    #@43
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v4

    #@47
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4a
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4a} :catch_4b

    #@4a
    .line 777
    .end local v0           #count:I
    :goto_4a
    return-void

    #@4b
    .line 773
    :catch_4b
    move-exception v2

    #@4c
    .line 774
    .local v2, e:Ljava/lang/Exception;
    const-string v3, "ACContentHelper"

    #@4e
    new-instance v4, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    const-string v5, "Exception :: "

    #@55
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v4

    #@59
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@5c
    move-result-object v5

    #@5d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v4

    #@61
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v4

    #@65
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@68
    .line 775
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    #@6b
    goto :goto_4a
.end method

.method private static deleteSubscriberFromAC(Landroid/content/Context;I)V
    .registers 8
    .parameter "context"
    .parameter "refId"

    #@0
    .prologue
    .line 780
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3
    move-result-object v1

    #@4
    .line 783
    .local v1, cr:Landroid/content/ContentResolver;
    :try_start_4
    sget-object v3, Lcom/lge/ims/provider/ac/AC$Provisioning$Subscriber;->CONTENT_URI:Landroid/net/Uri;

    #@6
    new-instance v4, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v5, "id=\'"

    #@d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v4

    #@11
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@14
    move-result-object v5

    #@15
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v4

    #@19
    const-string v5, "\'"

    #@1b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v4

    #@1f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v4

    #@23
    const/4 v5, 0x0

    #@24
    invoke-virtual {v1, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@27
    move-result v0

    #@28
    .line 784
    .local v0, count:I
    const-string v3, "ACContentHelper"

    #@2a
    new-instance v4, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v5, "Subscriber :: row with id="

    #@31
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v4

    #@35
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38
    move-result-object v4

    #@39
    const-string v5, " is successfully deleted; count="

    #@3b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v4

    #@3f
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@42
    move-result-object v4

    #@43
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v4

    #@47
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4a
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4a} :catch_4b

    #@4a
    .line 789
    .end local v0           #count:I
    :goto_4a
    return-void

    #@4b
    .line 785
    :catch_4b
    move-exception v2

    #@4c
    .line 786
    .local v2, e:Ljava/lang/Exception;
    const-string v3, "ACContentHelper"

    #@4e
    new-instance v4, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    const-string v5, "Exception :: "

    #@55
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v4

    #@59
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@5c
    move-result-object v5

    #@5d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v4

    #@61
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v4

    #@65
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@68
    .line 787
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    #@6b
    goto :goto_4a
.end method

.method public static deleteUnknownContentForAC(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    .line 60
    if-nez p0, :cond_3

    #@2
    .line 88
    :cond_2
    :goto_2
    return-void

    #@3
    .line 64
    :cond_3
    invoke-static {p0}, Lcom/lge/ims/service/ac/ACContentHelper;->getReferenceIdsExcludingCurrentIMSI(Landroid/content/Context;)Ljava/util/ArrayList;

    #@6
    move-result-object v2

    #@7
    .line 66
    .local v2, refIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-eqz v2, :cond_2

    #@9
    .line 70
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    #@c
    move-result v3

    #@d
    if-nez v3, :cond_2

    #@f
    .line 74
    const-string v3, "ACContentHelper"

    #@11
    const-string v4, "deleteUnknownContentForAC()"

    #@13
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@16
    .line 76
    const/4 v0, 0x0

    #@17
    .local v0, i:I
    :goto_17
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@1a
    move-result v3

    #@1b
    if-ge v0, v3, :cond_39

    #@1d
    .line 77
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@20
    move-result-object v3

    #@21
    check-cast v3, Ljava/lang/Integer;

    #@23
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    #@26
    move-result v1

    #@27
    .line 79
    .local v1, refId:I
    invoke-static {p0, v1}, Lcom/lge/ims/service/ac/ACContentHelper;->deleteIMSIFromAC(Landroid/content/Context;I)V

    #@2a
    .line 80
    invoke-static {p0, v1}, Lcom/lge/ims/service/ac/ACContentHelper;->deleteVersionFromAC(Landroid/content/Context;I)V

    #@2d
    .line 81
    invoke-static {p0, v1}, Lcom/lge/ims/service/ac/ACContentHelper;->deleteSubscriberFromAC(Landroid/content/Context;I)V

    #@30
    .line 82
    invoke-static {p0, v1}, Lcom/lge/ims/service/ac/ACContentHelper;->deleteProxyAddressFromAC(Landroid/content/Context;I)V

    #@33
    .line 83
    invoke-static {p0, v1}, Lcom/lge/ims/service/ac/ACContentHelper;->deleteServiceAvailabilityFromAC(Landroid/content/Context;I)V

    #@36
    .line 76
    add-int/lit8 v0, v0, 0x1

    #@38
    goto :goto_17

    #@39
    .line 86
    .end local v1           #refId:I
    :cond_39
    const/4 v3, 0x0

    #@3a
    sput-object v3, Lcom/lge/ims/service/ac/ACContentHelper;->mIMSI:Ljava/lang/String;

    #@3c
    .line 87
    const/4 v3, -0x1

    #@3d
    sput v3, Lcom/lge/ims/service/ac/ACContentHelper;->mRefId:I

    #@3f
    goto :goto_2
.end method

.method public static deleteUnknownContentWithMSISDNForAC(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    .line 91
    if-nez p0, :cond_3

    #@2
    .line 119
    :cond_2
    :goto_2
    return-void

    #@3
    .line 95
    :cond_3
    invoke-static {p0}, Lcom/lge/ims/service/ac/ACContentHelper;->getReferenceIdsExcludingCurrentMSISDN(Landroid/content/Context;)Ljava/util/ArrayList;

    #@6
    move-result-object v2

    #@7
    .line 97
    .local v2, refIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-eqz v2, :cond_2

    #@9
    .line 101
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    #@c
    move-result v3

    #@d
    if-nez v3, :cond_2

    #@f
    .line 105
    const-string v3, "ACContentHelper"

    #@11
    const-string v4, "deleteUnknownContentWithMSISDNForAC()"

    #@13
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@16
    .line 107
    const/4 v0, 0x0

    #@17
    .local v0, i:I
    :goto_17
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@1a
    move-result v3

    #@1b
    if-ge v0, v3, :cond_39

    #@1d
    .line 108
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@20
    move-result-object v3

    #@21
    check-cast v3, Ljava/lang/Integer;

    #@23
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    #@26
    move-result v1

    #@27
    .line 110
    .local v1, refId:I
    invoke-static {p0, v1}, Lcom/lge/ims/service/ac/ACContentHelper;->deleteIMSIFromAC(Landroid/content/Context;I)V

    #@2a
    .line 111
    invoke-static {p0, v1}, Lcom/lge/ims/service/ac/ACContentHelper;->deleteVersionFromAC(Landroid/content/Context;I)V

    #@2d
    .line 112
    invoke-static {p0, v1}, Lcom/lge/ims/service/ac/ACContentHelper;->deleteSubscriberFromAC(Landroid/content/Context;I)V

    #@30
    .line 113
    invoke-static {p0, v1}, Lcom/lge/ims/service/ac/ACContentHelper;->deleteProxyAddressFromAC(Landroid/content/Context;I)V

    #@33
    .line 114
    invoke-static {p0, v1}, Lcom/lge/ims/service/ac/ACContentHelper;->deleteServiceAvailabilityFromAC(Landroid/content/Context;I)V

    #@36
    .line 107
    add-int/lit8 v0, v0, 0x1

    #@38
    goto :goto_17

    #@39
    .line 117
    .end local v1           #refId:I
    :cond_39
    const/4 v3, 0x0

    #@3a
    sput-object v3, Lcom/lge/ims/service/ac/ACContentHelper;->mIMSI:Ljava/lang/String;

    #@3c
    .line 118
    const/4 v3, -0x1

    #@3d
    sput v3, Lcom/lge/ims/service/ac/ACContentHelper;->mRefId:I

    #@3f
    goto :goto_2
.end method

.method private static deleteVersionFromAC(Landroid/content/Context;I)V
    .registers 8
    .parameter "context"
    .parameter "refId"

    #@0
    .prologue
    .line 792
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3
    move-result-object v1

    #@4
    .line 795
    .local v1, cr:Landroid/content/ContentResolver;
    :try_start_4
    sget-object v3, Lcom/lge/ims/provider/ac/AC$Provisioning$Version;->CONTENT_URI:Landroid/net/Uri;

    #@6
    new-instance v4, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v5, "id=\'"

    #@d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v4

    #@11
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@14
    move-result-object v5

    #@15
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v4

    #@19
    const-string v5, "\'"

    #@1b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v4

    #@1f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v4

    #@23
    const/4 v5, 0x0

    #@24
    invoke-virtual {v1, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@27
    move-result v0

    #@28
    .line 796
    .local v0, count:I
    const-string v3, "ACContentHelper"

    #@2a
    new-instance v4, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v5, "Version :: row with id="

    #@31
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v4

    #@35
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38
    move-result-object v4

    #@39
    const-string v5, " is successfully deleted; count="

    #@3b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v4

    #@3f
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@42
    move-result-object v4

    #@43
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v4

    #@47
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4a
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4a} :catch_4b

    #@4a
    .line 801
    .end local v0           #count:I
    :goto_4a
    return-void

    #@4b
    .line 797
    :catch_4b
    move-exception v2

    #@4c
    .line 798
    .local v2, e:Ljava/lang/Exception;
    const-string v3, "ACContentHelper"

    #@4e
    new-instance v4, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    const-string v5, "Exception :: "

    #@55
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v4

    #@59
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@5c
    move-result-object v5

    #@5d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v4

    #@61
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v4

    #@65
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@68
    .line 799
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    #@6b
    goto :goto_4a
.end method

.method private static getNewReferenceId(Landroid/content/Context;)I
    .registers 13
    .parameter "context"

    #@0
    .prologue
    .line 894
    const/4 v11, -0x1

    #@1
    .line 895
    .local v11, refId:I
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4
    move-result-object v0

    #@5
    .line 896
    .local v0, cr:Landroid/content/ContentResolver;
    const/4 v6, 0x0

    #@6
    .line 899
    .local v6, c:Landroid/database/Cursor;
    :try_start_6
    sget-object v1, Lcom/lge/ims/provider/ac/AC$Provisioning$IMSI;->CONTENT_URI:Landroid/net/Uri;

    #@8
    const/4 v2, 0x0

    #@9
    const-string v3, "id=\'0\'"

    #@b
    const/4 v4, 0x0

    #@c
    const/4 v5, 0x0

    #@d
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@10
    move-result-object v6

    #@11
    .line 901
    if-eqz v6, :cond_25

    #@13
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@16
    move-result v1

    #@17
    if-eqz v1, :cond_25

    #@19
    .line 903
    const-string v1, "next_ref_id"

    #@1b
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@1e
    move-result v9

    #@1f
    .line 905
    .local v9, index:I
    if-ltz v9, :cond_25

    #@21
    .line 906
    invoke-interface {v6, v9}, Landroid/database/Cursor;->getInt(I)I
    :try_end_24
    .catchall {:try_start_6 .. :try_end_24} :catchall_89
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_24} :catch_63

    #@24
    move-result v11

    #@25
    .line 913
    .end local v9           #index:I
    :cond_25
    if-eqz v6, :cond_2a

    #@27
    .line 914
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@2a
    .line 918
    :cond_2a
    :goto_2a
    if-ltz v11, :cond_62

    #@2c
    .line 919
    add-int/lit8 v10, v11, 0x1

    #@2e
    .line 921
    .local v10, nextRefId:I
    const-string v1, "ACContentHelper"

    #@30
    new-instance v2, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v3, "getNewReferenceId :: next_ref_id="

    #@37
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v2

    #@3b
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v2

    #@43
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@46
    .line 923
    const v1, 0x7fffffff

    #@49
    if-ne v10, v1, :cond_4c

    #@4b
    .line 924
    const/4 v10, 0x0

    #@4c
    .line 927
    :cond_4c
    new-instance v7, Landroid/content/ContentValues;

    #@4e
    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    #@51
    .line 928
    .local v7, cv:Landroid/content/ContentValues;
    const-string v1, "next_ref_id"

    #@53
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@56
    move-result-object v2

    #@57
    invoke-virtual {v7, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@5a
    .line 931
    :try_start_5a
    sget-object v1, Lcom/lge/ims/provider/ac/AC$Provisioning$IMSI;->CONTENT_URI:Landroid/net/Uri;

    #@5c
    const-string v2, "id=\'0\'"

    #@5e
    const/4 v3, 0x0

    #@5f
    invoke-virtual {v0, v1, v7, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_62
    .catch Ljava/lang/Exception; {:try_start_5a .. :try_end_62} :catch_90

    #@62
    .line 938
    .end local v7           #cv:Landroid/content/ContentValues;
    .end local v10           #nextRefId:I
    :cond_62
    :goto_62
    return v11

    #@63
    .line 909
    :catch_63
    move-exception v8

    #@64
    .line 910
    .local v8, e:Ljava/lang/Exception;
    :try_start_64
    const-string v1, "ACContentHelper"

    #@66
    new-instance v2, Ljava/lang/StringBuilder;

    #@68
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@6b
    const-string v3, "Exception :: "

    #@6d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v2

    #@71
    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@74
    move-result-object v3

    #@75
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v2

    #@79
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v2

    #@7d
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@80
    .line 911
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_83
    .catchall {:try_start_64 .. :try_end_83} :catchall_89

    #@83
    .line 913
    if-eqz v6, :cond_2a

    #@85
    .line 914
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@88
    goto :goto_2a

    #@89
    .line 913
    .end local v8           #e:Ljava/lang/Exception;
    :catchall_89
    move-exception v1

    #@8a
    if-eqz v6, :cond_8f

    #@8c
    .line 914
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@8f
    :cond_8f
    throw v1

    #@90
    .line 932
    .restart local v7       #cv:Landroid/content/ContentValues;
    .restart local v10       #nextRefId:I
    :catch_90
    move-exception v8

    #@91
    .line 933
    .restart local v8       #e:Ljava/lang/Exception;
    const-string v1, "ACContentHelper"

    #@93
    new-instance v2, Ljava/lang/StringBuilder;

    #@95
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@98
    const-string v3, "Exception :: "

    #@9a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v2

    #@9e
    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@a1
    move-result-object v3

    #@a2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v2

    #@a6
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a9
    move-result-object v2

    #@aa
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@ad
    .line 934
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    #@b0
    goto :goto_62
.end method

.method public static getPasswordFromSubscriber(Landroid/content/Context;)Ljava/lang/String;
    .registers 12
    .parameter "context"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 122
    if-nez p0, :cond_4

    #@3
    .line 155
    :cond_3
    :goto_3
    return-object v9

    #@4
    .line 126
    :cond_4
    invoke-static {p0}, Lcom/lge/ims/service/ac/ACContentHelper;->getReferenceId(Landroid/content/Context;)I

    #@7
    move-result v10

    #@8
    .line 128
    .local v10, refId:I
    if-ltz v10, :cond_3

    #@a
    .line 132
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@d
    move-result-object v0

    #@e
    .line 133
    .local v0, cr:Landroid/content/ContentResolver;
    const/4 v6, 0x0

    #@f
    .line 134
    .local v6, c:Landroid/database/Cursor;
    const/4 v9, 0x0

    #@10
    .line 137
    .local v9, password:Ljava/lang/String;
    :try_start_10
    sget-object v1, Lcom/lge/ims/provider/ac/AC$Provisioning$Subscriber;->CONTENT_URI:Landroid/net/Uri;

    #@12
    const/4 v2, 0x0

    #@13
    new-instance v3, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v4, "id=\'"

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@21
    move-result-object v4

    #@22
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    const-string v4, "\'"

    #@28
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v3

    #@2c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v3

    #@30
    const/4 v4, 0x0

    #@31
    const/4 v5, 0x0

    #@32
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@35
    move-result-object v6

    #@36
    .line 139
    if-eqz v6, :cond_4a

    #@38
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@3b
    move-result v1

    #@3c
    if-eqz v1, :cond_4a

    #@3e
    .line 140
    const-string v1, "auth_password"

    #@40
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@43
    move-result v8

    #@44
    .line 142
    .local v8, index:I
    if-ltz v8, :cond_4a

    #@46
    .line 143
    invoke-interface {v6, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_49
    .catchall {:try_start_10 .. :try_end_49} :catchall_76
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_49} :catch_50

    #@49
    move-result-object v9

    #@4a
    .line 150
    .end local v8           #index:I
    :cond_4a
    if-eqz v6, :cond_3

    #@4c
    .line 151
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@4f
    goto :goto_3

    #@50
    .line 146
    :catch_50
    move-exception v7

    #@51
    .line 147
    .local v7, e:Ljava/lang/Exception;
    :try_start_51
    const-string v1, "ACContentHelper"

    #@53
    new-instance v2, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    const-string v3, "Exception :: "

    #@5a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v2

    #@5e
    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@61
    move-result-object v3

    #@62
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v2

    #@66
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v2

    #@6a
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@6d
    .line 148
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_70
    .catchall {:try_start_51 .. :try_end_70} :catchall_76

    #@70
    .line 150
    if-eqz v6, :cond_3

    #@72
    .line 151
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@75
    goto :goto_3

    #@76
    .line 150
    .end local v7           #e:Ljava/lang/Exception;
    :catchall_76
    move-exception v1

    #@77
    if-eqz v6, :cond_7c

    #@79
    .line 151
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@7c
    :cond_7c
    throw v1
.end method

.method private static getProxyAddressFromAC(Landroid/content/Context;I)Lcom/lge/ims/service/ac/content/ACProxyAddress;
    .registers 11
    .parameter "context"
    .parameter "refId"

    #@0
    .prologue
    .line 804
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3
    move-result-object v0

    #@4
    .line 805
    .local v0, cr:Landroid/content/ContentResolver;
    const/4 v6, 0x0

    #@5
    .line 806
    .local v6, c:Landroid/database/Cursor;
    new-instance v8, Lcom/lge/ims/service/ac/content/ACProxyAddress;

    #@7
    invoke-direct {v8}, Lcom/lge/ims/service/ac/content/ACProxyAddress;-><init>()V

    #@a
    .line 809
    .local v8, proxyAddress:Lcom/lge/ims/service/ac/content/ACProxyAddress;
    :try_start_a
    sget-object v1, Lcom/lge/ims/provider/ac/AC$Provisioning$ProxyAddress;->CONTENT_URI:Landroid/net/Uri;

    #@c
    const/4 v2, 0x0

    #@d
    new-instance v3, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v4, "id=\'"

    #@14
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    const-string v4, "\'"

    #@22
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    const/4 v4, 0x0

    #@2b
    const/4 v5, 0x0

    #@2c
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@2f
    move-result-object v6

    #@30
    .line 811
    if-eqz v6, :cond_80

    #@32
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@35
    move-result v1

    #@36
    if-eqz v1, :cond_80

    #@38
    .line 812
    const-string v1, "sbc_address"

    #@3a
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@3d
    move-result v1

    #@3e
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@41
    move-result-object v1

    #@42
    iput-object v1, v8, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCAddress:Ljava/lang/String;

    #@44
    .line 813
    const-string v1, "sbc_address_type"

    #@46
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@49
    move-result v1

    #@4a
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@4d
    move-result-object v1

    #@4e
    iput-object v1, v8, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCAddressType:Ljava/lang/String;

    #@50
    .line 814
    const-string v1, "sbc_tls_address"

    #@52
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@55
    move-result v1

    #@56
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@59
    move-result-object v1

    #@5a
    iput-object v1, v8, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCTLSAddress:Ljava/lang/String;

    #@5c
    .line 815
    const-string v1, "sbc_tls_address_type"

    #@5e
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@61
    move-result v1

    #@62
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@65
    move-result-object v1

    #@66
    iput-object v1, v8, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCTLSAddressType:Ljava/lang/String;

    #@68
    .line 816
    const-string v1, "lbo_pcscf_address"

    #@6a
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@6d
    move-result v1

    #@6e
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@71
    move-result-object v1

    #@72
    iput-object v1, v8, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mLBOPCSCFAddress:Ljava/lang/String;

    #@74
    .line 817
    const-string v1, "lbo_pcscf_address_type"

    #@76
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@79
    move-result v1

    #@7a
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@7d
    move-result-object v1

    #@7e
    iput-object v1, v8, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mLBOPCSCFAddressType:Ljava/lang/String;
    :try_end_80
    .catchall {:try_start_a .. :try_end_80} :catchall_af
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_80} :catch_86

    #@80
    .line 825
    :cond_80
    if-eqz v6, :cond_85

    #@82
    .line 826
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@85
    .line 830
    :cond_85
    :goto_85
    return-object v8

    #@86
    .line 819
    :catch_86
    move-exception v7

    #@87
    .line 820
    .local v7, e:Ljava/lang/Exception;
    :try_start_87
    const-string v1, "ACContentHelper"

    #@89
    new-instance v2, Ljava/lang/StringBuilder;

    #@8b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8e
    const-string v3, "Exception :: "

    #@90
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v2

    #@94
    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@97
    move-result-object v3

    #@98
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v2

    #@9c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9f
    move-result-object v2

    #@a0
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@a3
    .line 821
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    #@a6
    .line 823
    invoke-virtual {v8}, Lcom/lge/ims/service/ac/content/ACProxyAddress;->clear()V
    :try_end_a9
    .catchall {:try_start_87 .. :try_end_a9} :catchall_af

    #@a9
    .line 825
    if-eqz v6, :cond_85

    #@ab
    .line 826
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@ae
    goto :goto_85

    #@af
    .line 825
    .end local v7           #e:Ljava/lang/Exception;
    :catchall_af
    move-exception v1

    #@b0
    if-eqz v6, :cond_b5

    #@b2
    .line 826
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@b5
    :cond_b5
    throw v1
.end method

.method private static getReferenceId(Landroid/content/Context;)I
    .registers 12
    .parameter "context"

    #@0
    .prologue
    .line 942
    invoke-static {}, Lcom/lge/ims/PhoneInfo;->getSubscriberIdWithDummy()Ljava/lang/String;

    #@3
    move-result-object v8

    #@4
    .line 944
    .local v8, imsi:Ljava/lang/String;
    invoke-static {}, Lcom/lge/ims/service/ac/ACConfiguration;->isUsimOn()Z

    #@7
    move-result v1

    #@8
    if-nez v1, :cond_e

    #@a
    .line 945
    invoke-static {}, Lcom/lge/ims/service/ac/ACConfiguration;->getIMSI()Ljava/lang/String;

    #@d
    move-result-object v8

    #@e
    .line 949
    :cond_e
    sget-object v1, Lcom/lge/ims/service/ac/ACContentHelper;->mIMSI:Ljava/lang/String;

    #@10
    if-eqz v1, :cond_21

    #@12
    sget-object v1, Lcom/lge/ims/service/ac/ACContentHelper;->mIMSI:Ljava/lang/String;

    #@14
    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v1

    #@18
    if-eqz v1, :cond_21

    #@1a
    .line 950
    sget v1, Lcom/lge/ims/service/ac/ACContentHelper;->mRefId:I

    #@1c
    if-lez v1, :cond_21

    #@1e
    .line 951
    sget v10, Lcom/lge/ims/service/ac/ACContentHelper;->mRefId:I

    #@20
    .line 990
    :cond_20
    :goto_20
    return v10

    #@21
    .line 955
    :cond_21
    if-nez v8, :cond_2c

    #@23
    .line 956
    const-string v1, "ACContentHelper"

    #@25
    const-string v2, "IMSI is null"

    #@27
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@2a
    .line 957
    const/4 v10, -0x1

    #@2b
    goto :goto_20

    #@2c
    .line 960
    :cond_2c
    const/4 v10, -0x1

    #@2d
    .line 961
    .local v10, refId:I
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@30
    move-result-object v0

    #@31
    .line 962
    .local v0, cr:Landroid/content/ContentResolver;
    const/4 v6, 0x0

    #@32
    .line 965
    .local v6, c:Landroid/database/Cursor;
    :try_start_32
    sget-object v1, Lcom/lge/ims/provider/ac/AC$Provisioning$IMSI;->CONTENT_URI:Landroid/net/Uri;

    #@34
    const/4 v2, 0x0

    #@35
    new-instance v3, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v4, "id=\'"

    #@3c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v3

    #@40
    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v3

    #@44
    const-string v4, "\'"

    #@46
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v3

    #@4a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v3

    #@4e
    const/4 v4, 0x0

    #@4f
    const/4 v5, 0x0

    #@50
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@53
    move-result-object v6

    #@54
    .line 967
    if-eqz v6, :cond_68

    #@56
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@59
    move-result v1

    #@5a
    if-eqz v1, :cond_68

    #@5c
    .line 969
    const-string v1, "ref_id"

    #@5e
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@61
    move-result v9

    #@62
    .line 971
    .local v9, index:I
    if-ltz v9, :cond_68

    #@64
    .line 972
    invoke-interface {v6, v9}, Landroid/database/Cursor;->getInt(I)I
    :try_end_67
    .catchall {:try_start_32 .. :try_end_67} :catchall_c0
    .catch Ljava/lang/Exception; {:try_start_32 .. :try_end_67} :catch_9a

    #@67
    move-result v10

    #@68
    .line 979
    .end local v9           #index:I
    :cond_68
    if-eqz v6, :cond_6d

    #@6a
    .line 980
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@6d
    .line 984
    :cond_6d
    :goto_6d
    if-lez v10, :cond_20

    #@6f
    .line 985
    sput-object v8, Lcom/lge/ims/service/ac/ACContentHelper;->mIMSI:Ljava/lang/String;

    #@71
    .line 986
    sput v10, Lcom/lge/ims/service/ac/ACContentHelper;->mRefId:I

    #@73
    .line 987
    const-string v1, "ACContentHelper"

    #@75
    new-instance v2, Ljava/lang/StringBuilder;

    #@77
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7a
    const-string v3, "getReferenceId :: imsi="

    #@7c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v2

    #@80
    sget-object v3, Lcom/lge/ims/service/ac/ACContentHelper;->mIMSI:Ljava/lang/String;

    #@82
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v2

    #@86
    const-string v3, ", refId="

    #@88
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v2

    #@8c
    sget v3, Lcom/lge/ims/service/ac/ACContentHelper;->mRefId:I

    #@8e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@91
    move-result-object v2

    #@92
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@95
    move-result-object v2

    #@96
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@99
    goto :goto_20

    #@9a
    .line 975
    :catch_9a
    move-exception v7

    #@9b
    .line 976
    .local v7, e:Ljava/lang/Exception;
    :try_start_9b
    const-string v1, "ACContentHelper"

    #@9d
    new-instance v2, Ljava/lang/StringBuilder;

    #@9f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a2
    const-string v3, "Exception :: "

    #@a4
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v2

    #@a8
    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@ab
    move-result-object v3

    #@ac
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af
    move-result-object v2

    #@b0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b3
    move-result-object v2

    #@b4
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@b7
    .line 977
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_ba
    .catchall {:try_start_9b .. :try_end_ba} :catchall_c0

    #@ba
    .line 979
    if-eqz v6, :cond_6d

    #@bc
    .line 980
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@bf
    goto :goto_6d

    #@c0
    .line 979
    .end local v7           #e:Ljava/lang/Exception;
    :catchall_c0
    move-exception v1

    #@c1
    if-eqz v6, :cond_c6

    #@c3
    .line 980
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@c6
    :cond_c6
    throw v1
.end method

.method private static getReferenceIdsExcludingCurrentIMSI(Landroid/content/Context;)Ljava/util/ArrayList;
    .registers 13
    .parameter "context"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    .line 994
    invoke-static {}, Lcom/lge/ims/PhoneInfo;->getSubscriberIdWithDummy()Ljava/lang/String;

    #@4
    move-result-object v8

    #@5
    .line 996
    .local v8, imsi:Ljava/lang/String;
    invoke-static {}, Lcom/lge/ims/service/ac/ACConfiguration;->isUsimOn()Z

    #@8
    move-result v1

    #@9
    if-nez v1, :cond_f

    #@b
    .line 997
    invoke-static {}, Lcom/lge/ims/service/ac/ACConfiguration;->getIMSI()Ljava/lang/String;

    #@e
    move-result-object v8

    #@f
    .line 1000
    :cond_f
    if-nez v8, :cond_12

    #@11
    .line 1039
    :goto_11
    return-object v10

    #@12
    .line 1004
    :cond_12
    const/4 v9, -0x1

    #@13
    .line 1005
    .local v9, refId:I
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@16
    move-result-object v0

    #@17
    .line 1006
    .local v0, cr:Landroid/content/ContentResolver;
    const/4 v6, 0x0

    #@18
    .line 1007
    .local v6, c:Landroid/database/Cursor;
    new-instance v10, Ljava/util/ArrayList;

    #@1a
    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    #@1d
    .line 1010
    .local v10, refIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :try_start_1d
    sget-object v1, Lcom/lge/ims/provider/ac/AC$Provisioning$IMSI;->CONTENT_URI:Landroid/net/Uri;

    #@1f
    const/4 v2, 0x0

    #@20
    const/4 v3, 0x0

    #@21
    const/4 v4, 0x0

    #@22
    const/4 v5, 0x0

    #@23
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@26
    move-result-object v6

    #@27
    .line 1012
    if-eqz v6, :cond_9c

    #@29
    .line 1015
    :cond_29
    :goto_29
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    #@2c
    move-result v1

    #@2d
    if-eqz v1, :cond_9c

    #@2f
    .line 1016
    const-string v1, "id"

    #@31
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@34
    move-result v1

    #@35
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@38
    move-result-object v11

    #@39
    .line 1018
    .local v11, tmp:Ljava/lang/String;
    const-string v1, "0"

    #@3b
    invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3e
    move-result v1

    #@3f
    if-nez v1, :cond_29

    #@41
    .line 1023
    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@44
    move-result v1

    #@45
    if-nez v1, :cond_29

    #@47
    .line 1024
    const-string v1, "ref_id"

    #@49
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@4c
    move-result v1

    #@4d
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    #@50
    move-result v1

    #@51
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@54
    move-result-object v1

    #@55
    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_58
    .catchall {:try_start_1d .. :try_end_58} :catchall_a2
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_58} :catch_59

    #@58
    goto :goto_29

    #@59
    .line 1028
    .end local v11           #tmp:Ljava/lang/String;
    :catch_59
    move-exception v7

    #@5a
    .line 1029
    .local v7, e:Ljava/lang/Exception;
    :try_start_5a
    const-string v1, "ACContentHelper"

    #@5c
    new-instance v2, Ljava/lang/StringBuilder;

    #@5e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@61
    const-string v3, "Exception :: "

    #@63
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v2

    #@67
    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@6a
    move-result-object v3

    #@6b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v2

    #@6f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@72
    move-result-object v2

    #@73
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@76
    .line 1030
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_79
    .catchall {:try_start_5a .. :try_end_79} :catchall_a2

    #@79
    .line 1032
    if-eqz v6, :cond_7e

    #@7b
    .line 1033
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@7e
    .line 1037
    .end local v7           #e:Ljava/lang/Exception;
    :cond_7e
    :goto_7e
    const-string v1, "ACContentHelper"

    #@80
    new-instance v2, Ljava/lang/StringBuilder;

    #@82
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@85
    const-string v3, "Count of invalid reference :: "

    #@87
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v2

    #@8b
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    #@8e
    move-result v3

    #@8f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@92
    move-result-object v2

    #@93
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@96
    move-result-object v2

    #@97
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@9a
    goto/16 :goto_11

    #@9c
    .line 1032
    :cond_9c
    if-eqz v6, :cond_7e

    #@9e
    .line 1033
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@a1
    goto :goto_7e

    #@a2
    .line 1032
    :catchall_a2
    move-exception v1

    #@a3
    if-eqz v6, :cond_a8

    #@a5
    .line 1033
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@a8
    :cond_a8
    throw v1
.end method

.method private static getReferenceIdsExcludingCurrentMSISDN(Landroid/content/Context;)Ljava/util/ArrayList;
    .registers 13
    .parameter "context"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    .line 1043
    invoke-static {}, Lcom/lge/ims/PhoneInfo;->getPhoneNumberExcludingNationalPrefix()Ljava/lang/String;

    #@4
    move-result-object v8

    #@5
    .line 1045
    .local v8, msisdn:Ljava/lang/String;
    invoke-static {}, Lcom/lge/ims/service/ac/ACConfiguration;->isUsimOn()Z

    #@8
    move-result v1

    #@9
    if-nez v1, :cond_f

    #@b
    .line 1046
    invoke-static {}, Lcom/lge/ims/service/ac/ACConfiguration;->getPhoneNumber()Ljava/lang/String;

    #@e
    move-result-object v8

    #@f
    .line 1049
    :cond_f
    if-nez v8, :cond_12

    #@11
    .line 1088
    :goto_11
    return-object v10

    #@12
    .line 1053
    :cond_12
    const/4 v9, -0x1

    #@13
    .line 1054
    .local v9, refId:I
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@16
    move-result-object v0

    #@17
    .line 1055
    .local v0, cr:Landroid/content/ContentResolver;
    const/4 v6, 0x0

    #@18
    .line 1056
    .local v6, c:Landroid/database/Cursor;
    new-instance v10, Ljava/util/ArrayList;

    #@1a
    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    #@1d
    .line 1059
    .local v10, refIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :try_start_1d
    sget-object v1, Lcom/lge/ims/provider/ac/AC$Provisioning$IMSI;->CONTENT_URI:Landroid/net/Uri;

    #@1f
    const/4 v2, 0x0

    #@20
    const/4 v3, 0x0

    #@21
    const/4 v4, 0x0

    #@22
    const/4 v5, 0x0

    #@23
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@26
    move-result-object v6

    #@27
    .line 1061
    if-eqz v6, :cond_9c

    #@29
    .line 1064
    :cond_29
    :goto_29
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    #@2c
    move-result v1

    #@2d
    if-eqz v1, :cond_9c

    #@2f
    .line 1065
    const-string v1, "msisdn"

    #@31
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@34
    move-result v1

    #@35
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@38
    move-result-object v11

    #@39
    .line 1067
    .local v11, tmp:Ljava/lang/String;
    const-string v1, "0"

    #@3b
    invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3e
    move-result v1

    #@3f
    if-nez v1, :cond_29

    #@41
    .line 1072
    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@44
    move-result v1

    #@45
    if-nez v1, :cond_29

    #@47
    .line 1073
    const-string v1, "ref_id"

    #@49
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@4c
    move-result v1

    #@4d
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    #@50
    move-result v1

    #@51
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@54
    move-result-object v1

    #@55
    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_58
    .catchall {:try_start_1d .. :try_end_58} :catchall_a2
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_58} :catch_59

    #@58
    goto :goto_29

    #@59
    .line 1077
    .end local v11           #tmp:Ljava/lang/String;
    :catch_59
    move-exception v7

    #@5a
    .line 1078
    .local v7, e:Ljava/lang/Exception;
    :try_start_5a
    const-string v1, "ACContentHelper"

    #@5c
    new-instance v2, Ljava/lang/StringBuilder;

    #@5e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@61
    const-string v3, "Exception :: "

    #@63
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v2

    #@67
    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@6a
    move-result-object v3

    #@6b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v2

    #@6f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@72
    move-result-object v2

    #@73
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@76
    .line 1079
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_79
    .catchall {:try_start_5a .. :try_end_79} :catchall_a2

    #@79
    .line 1081
    if-eqz v6, :cond_7e

    #@7b
    .line 1082
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@7e
    .line 1086
    .end local v7           #e:Ljava/lang/Exception;
    :cond_7e
    :goto_7e
    const-string v1, "ACContentHelper"

    #@80
    new-instance v2, Ljava/lang/StringBuilder;

    #@82
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@85
    const-string v3, "Count of invalid reference :: "

    #@87
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v2

    #@8b
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    #@8e
    move-result v3

    #@8f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@92
    move-result-object v2

    #@93
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@96
    move-result-object v2

    #@97
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@9a
    goto/16 :goto_11

    #@9c
    .line 1081
    :cond_9c
    if-eqz v6, :cond_7e

    #@9e
    .line 1082
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@a1
    goto :goto_7e

    #@a2
    .line 1081
    :catchall_a2
    move-exception v1

    #@a3
    if-eqz v6, :cond_a8

    #@a5
    .line 1082
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@a8
    :cond_a8
    throw v1
.end method

.method public static getServiceAvailability(Landroid/content/Context;)[Ljava/lang/Integer;
    .registers 11
    .parameter "context"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 271
    if-nez p0, :cond_4

    #@3
    .line 302
    :cond_3
    :goto_3
    return-object v6

    #@4
    .line 275
    :cond_4
    invoke-static {p0}, Lcom/lge/ims/service/ac/ACContentHelper;->getReferenceId(Landroid/content/Context;)I

    #@7
    move-result v9

    #@8
    .line 277
    .local v9, refId:I
    if-ltz v9, :cond_3

    #@a
    .line 281
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@d
    move-result-object v0

    #@e
    .line 282
    .local v0, cr:Landroid/content/ContentResolver;
    const/4 v7, 0x0

    #@f
    .line 283
    .local v7, c:Landroid/database/Cursor;
    const/4 v1, 0x3

    #@10
    new-array v6, v1, [Ljava/lang/Integer;

    #@12
    .line 286
    .local v6, availability:[Ljava/lang/Integer;
    :try_start_12
    sget-object v1, Lcom/lge/ims/provider/ac/AC$Provisioning$ServiceAvailability;->CONTENT_URI:Landroid/net/Uri;

    #@14
    const/4 v2, 0x0

    #@15
    new-instance v3, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v4, "id=\'"

    #@1c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@23
    move-result-object v4

    #@24
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v3

    #@28
    const-string v4, "\'"

    #@2a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v3

    #@2e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v3

    #@32
    const/4 v4, 0x0

    #@33
    const/4 v5, 0x0

    #@34
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@37
    move-result-object v7

    #@38
    .line 288
    if-eqz v7, :cond_73

    #@3a
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    #@3d
    move-result v1

    #@3e
    if-eqz v1, :cond_73

    #@40
    .line 289
    const/4 v1, 0x0

    #@41
    const-string v2, "rcs_oem"

    #@43
    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@46
    move-result v2

    #@47
    invoke-interface {v7, v2}, Landroid/database/Cursor;->getInt(I)I

    #@4a
    move-result v2

    #@4b
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4e
    move-result-object v2

    #@4f
    aput-object v2, v6, v1

    #@51
    .line 290
    const/4 v1, 0x1

    #@52
    const-string v2, "volte"

    #@54
    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@57
    move-result v2

    #@58
    invoke-interface {v7, v2}, Landroid/database/Cursor;->getInt(I)I

    #@5b
    move-result v2

    #@5c
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5f
    move-result-object v2

    #@60
    aput-object v2, v6, v1

    #@62
    .line 291
    const/4 v1, 0x2

    #@63
    const-string v2, "vt"

    #@65
    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@68
    move-result v2

    #@69
    invoke-interface {v7, v2}, Landroid/database/Cursor;->getInt(I)I

    #@6c
    move-result v2

    #@6d
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@70
    move-result-object v2

    #@71
    aput-object v2, v6, v1
    :try_end_73
    .catchall {:try_start_12 .. :try_end_73} :catchall_a0
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_73} :catch_79

    #@73
    .line 297
    :cond_73
    if-eqz v7, :cond_3

    #@75
    .line 298
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@78
    goto :goto_3

    #@79
    .line 293
    :catch_79
    move-exception v8

    #@7a
    .line 294
    .local v8, e:Ljava/lang/Exception;
    :try_start_7a
    const-string v1, "ACContentHelper"

    #@7c
    new-instance v2, Ljava/lang/StringBuilder;

    #@7e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@81
    const-string v3, "Exception :: "

    #@83
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v2

    #@87
    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@8a
    move-result-object v3

    #@8b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v2

    #@8f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@92
    move-result-object v2

    #@93
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@96
    .line 295
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_99
    .catchall {:try_start_7a .. :try_end_99} :catchall_a0

    #@99
    .line 297
    if-eqz v7, :cond_3

    #@9b
    .line 298
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@9e
    goto/16 :goto_3

    #@a0
    .line 297
    .end local v8           #e:Ljava/lang/Exception;
    :catchall_a0
    move-exception v1

    #@a1
    if-eqz v7, :cond_a6

    #@a3
    .line 298
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@a6
    :cond_a6
    throw v1
.end method

.method private static getServiceAvailabilityFromAC(Landroid/content/Context;I)Lcom/lge/ims/service/ac/content/ACServiceAvailability;
    .registers 11
    .parameter "context"
    .parameter "refId"

    #@0
    .prologue
    .line 868
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3
    move-result-object v0

    #@4
    .line 869
    .local v0, cr:Landroid/content/ContentResolver;
    const/4 v6, 0x0

    #@5
    .line 870
    .local v6, c:Landroid/database/Cursor;
    new-instance v8, Lcom/lge/ims/service/ac/content/ACServiceAvailability;

    #@7
    invoke-direct {v8}, Lcom/lge/ims/service/ac/content/ACServiceAvailability;-><init>()V

    #@a
    .line 873
    .local v8, serviceAvailability:Lcom/lge/ims/service/ac/content/ACServiceAvailability;
    :try_start_a
    sget-object v1, Lcom/lge/ims/provider/ac/AC$Provisioning$ServiceAvailability;->CONTENT_URI:Landroid/net/Uri;

    #@c
    const/4 v2, 0x0

    #@d
    new-instance v3, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v4, "id=\'"

    #@14
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    const-string v4, "\'"

    #@22
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    const/4 v4, 0x0

    #@2b
    const/4 v5, 0x0

    #@2c
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@2f
    move-result-object v6

    #@30
    .line 875
    if-eqz v6, :cond_50

    #@32
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@35
    move-result v1

    #@36
    if-eqz v1, :cond_50

    #@38
    .line 876
    const-string v1, "volte"

    #@3a
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@3d
    move-result v1

    #@3e
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@41
    move-result-object v1

    #@42
    iput-object v1, v8, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->mVOLTE:Ljava/lang/String;

    #@44
    .line 877
    const-string v1, "vt"

    #@46
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@49
    move-result v1

    #@4a
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@4d
    move-result-object v1

    #@4e
    iput-object v1, v8, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->mPSVT:Ljava/lang/String;
    :try_end_50
    .catchall {:try_start_a .. :try_end_50} :catchall_7f
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_50} :catch_56

    #@50
    .line 885
    :cond_50
    if-eqz v6, :cond_55

    #@52
    .line 886
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@55
    .line 890
    :cond_55
    :goto_55
    return-object v8

    #@56
    .line 879
    :catch_56
    move-exception v7

    #@57
    .line 880
    .local v7, e:Ljava/lang/Exception;
    :try_start_57
    const-string v1, "ACContentHelper"

    #@59
    new-instance v2, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v3, "Exception :: "

    #@60
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v2

    #@64
    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@67
    move-result-object v3

    #@68
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v2

    #@6c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6f
    move-result-object v2

    #@70
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@73
    .line 881
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    #@76
    .line 883
    invoke-virtual {v8}, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->clear()V
    :try_end_79
    .catchall {:try_start_57 .. :try_end_79} :catchall_7f

    #@79
    .line 885
    if-eqz v6, :cond_55

    #@7b
    .line 886
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@7e
    goto :goto_55

    #@7f
    .line 885
    .end local v7           #e:Ljava/lang/Exception;
    :catchall_7f
    move-exception v1

    #@80
    if-eqz v6, :cond_85

    #@82
    .line 886
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@85
    :cond_85
    throw v1
.end method

.method private static getSubscriberFromAC(Landroid/content/Context;I)Lcom/lge/ims/service/ac/content/ACSubscriber;
    .registers 11
    .parameter "context"
    .parameter "refId"

    #@0
    .prologue
    .line 834
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3
    move-result-object v0

    #@4
    .line 835
    .local v0, cr:Landroid/content/ContentResolver;
    const/4 v6, 0x0

    #@5
    .line 836
    .local v6, c:Landroid/database/Cursor;
    new-instance v8, Lcom/lge/ims/service/ac/content/ACSubscriber;

    #@7
    invoke-direct {v8}, Lcom/lge/ims/service/ac/content/ACSubscriber;-><init>()V

    #@a
    .line 839
    .local v8, subscriber:Lcom/lge/ims/service/ac/content/ACSubscriber;
    :try_start_a
    sget-object v1, Lcom/lge/ims/provider/ac/AC$Provisioning$Subscriber;->CONTENT_URI:Landroid/net/Uri;

    #@c
    const/4 v2, 0x0

    #@d
    new-instance v3, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v4, "id=\'"

    #@14
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    const-string v4, "\'"

    #@22
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    const/4 v4, 0x0

    #@2b
    const/4 v5, 0x0

    #@2c
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@2f
    move-result-object v6

    #@30
    .line 841
    if-eqz v6, :cond_ad

    #@32
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@35
    move-result v1

    #@36
    if-eqz v1, :cond_ad

    #@38
    .line 842
    const-string v1, "impi"

    #@3a
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@3d
    move-result v1

    #@3e
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@41
    move-result-object v1

    #@42
    iput-object v1, v8, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPI:Ljava/lang/String;

    #@44
    .line 843
    iget-object v1, v8, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPUs:Ljava/util/ArrayList;

    #@46
    const-string v2, "impu_0"

    #@48
    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@4b
    move-result v2

    #@4c
    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@4f
    move-result-object v2

    #@50
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@53
    .line 844
    iget-object v1, v8, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPUs:Ljava/util/ArrayList;

    #@55
    const-string v2, "impu_1"

    #@57
    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@5a
    move-result v2

    #@5b
    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@5e
    move-result-object v2

    #@5f
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@62
    .line 845
    iget-object v1, v8, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPUs:Ljava/util/ArrayList;

    #@64
    const-string v2, "impu_2"

    #@66
    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@69
    move-result v2

    #@6a
    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@6d
    move-result-object v2

    #@6e
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@71
    .line 846
    const-string v1, "home_domain_name"

    #@73
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@76
    move-result v1

    #@77
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@7a
    move-result-object v1

    #@7b
    iput-object v1, v8, Lcom/lge/ims/service/ac/content/ACSubscriber;->mHomeDomainName:Ljava/lang/String;

    #@7d
    .line 848
    const-string v1, "auth_type"

    #@7f
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@82
    move-result v1

    #@83
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@86
    move-result-object v1

    #@87
    iput-object v1, v8, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthType:Ljava/lang/String;

    #@89
    .line 849
    const-string v1, "auth_realm"

    #@8b
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@8e
    move-result v1

    #@8f
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@92
    move-result-object v1

    #@93
    iput-object v1, v8, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthRealm:Ljava/lang/String;

    #@95
    .line 850
    const-string v1, "auth_username"

    #@97
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@9a
    move-result v1

    #@9b
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@9e
    move-result-object v1

    #@9f
    iput-object v1, v8, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthUsername:Ljava/lang/String;

    #@a1
    .line 851
    const-string v1, "auth_password"

    #@a3
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@a6
    move-result v1

    #@a7
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@aa
    move-result-object v1

    #@ab
    iput-object v1, v8, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthPassword:Ljava/lang/String;
    :try_end_ad
    .catchall {:try_start_a .. :try_end_ad} :catchall_dc
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_ad} :catch_b3

    #@ad
    .line 859
    :cond_ad
    if-eqz v6, :cond_b2

    #@af
    .line 860
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@b2
    .line 864
    :cond_b2
    :goto_b2
    return-object v8

    #@b3
    .line 853
    :catch_b3
    move-exception v7

    #@b4
    .line 854
    .local v7, e:Ljava/lang/Exception;
    :try_start_b4
    const-string v1, "ACContentHelper"

    #@b6
    new-instance v2, Ljava/lang/StringBuilder;

    #@b8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@bb
    const-string v3, "Exception :: "

    #@bd
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v2

    #@c1
    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@c4
    move-result-object v3

    #@c5
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v2

    #@c9
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cc
    move-result-object v2

    #@cd
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@d0
    .line 855
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    #@d3
    .line 857
    invoke-virtual {v8}, Lcom/lge/ims/service/ac/content/ACSubscriber;->clear()V
    :try_end_d6
    .catchall {:try_start_b4 .. :try_end_d6} :catchall_dc

    #@d6
    .line 859
    if-eqz v6, :cond_b2

    #@d8
    .line 860
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@db
    goto :goto_b2

    #@dc
    .line 859
    .end local v7           #e:Ljava/lang/Exception;
    :catchall_dc
    move-exception v1

    #@dd
    if-eqz v6, :cond_e2

    #@df
    .line 860
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@e2
    :cond_e2
    throw v1
.end method

.method public static getValidityFromVersion(Landroid/content/Context;)Ljava/lang/String;
    .registers 12
    .parameter "context"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    .line 159
    if-nez p0, :cond_4

    #@3
    .line 192
    :cond_3
    :goto_3
    return-object v10

    #@4
    .line 163
    :cond_4
    invoke-static {p0}, Lcom/lge/ims/service/ac/ACContentHelper;->getReferenceId(Landroid/content/Context;)I

    #@7
    move-result v9

    #@8
    .line 165
    .local v9, refId:I
    if-ltz v9, :cond_3

    #@a
    .line 169
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@d
    move-result-object v0

    #@e
    .line 170
    .local v0, cr:Landroid/content/ContentResolver;
    const/4 v6, 0x0

    #@f
    .line 171
    .local v6, c:Landroid/database/Cursor;
    const/4 v10, 0x0

    #@10
    .line 174
    .local v10, validity:Ljava/lang/String;
    :try_start_10
    sget-object v1, Lcom/lge/ims/provider/ac/AC$Provisioning$Version;->CONTENT_URI:Landroid/net/Uri;

    #@12
    const/4 v2, 0x0

    #@13
    new-instance v3, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v4, "id=\'"

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@21
    move-result-object v4

    #@22
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    const-string v4, "\'"

    #@28
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v3

    #@2c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v3

    #@30
    const/4 v4, 0x0

    #@31
    const/4 v5, 0x0

    #@32
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@35
    move-result-object v6

    #@36
    .line 176
    if-eqz v6, :cond_4a

    #@38
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@3b
    move-result v1

    #@3c
    if-eqz v1, :cond_4a

    #@3e
    .line 177
    const-string v1, "validity"

    #@40
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@43
    move-result v8

    #@44
    .line 179
    .local v8, index:I
    if-ltz v8, :cond_4a

    #@46
    .line 180
    invoke-interface {v6, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_49
    .catchall {:try_start_10 .. :try_end_49} :catchall_76
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_49} :catch_50

    #@49
    move-result-object v10

    #@4a
    .line 187
    .end local v8           #index:I
    :cond_4a
    if-eqz v6, :cond_3

    #@4c
    .line 188
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@4f
    goto :goto_3

    #@50
    .line 183
    :catch_50
    move-exception v7

    #@51
    .line 184
    .local v7, e:Ljava/lang/Exception;
    :try_start_51
    const-string v1, "ACContentHelper"

    #@53
    new-instance v2, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    const-string v3, "Exception :: "

    #@5a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v2

    #@5e
    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@61
    move-result-object v3

    #@62
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v2

    #@66
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v2

    #@6a
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@6d
    .line 185
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_70
    .catchall {:try_start_51 .. :try_end_70} :catchall_76

    #@70
    .line 187
    if-eqz v6, :cond_3

    #@72
    .line 188
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@75
    goto :goto_3

    #@76
    .line 187
    .end local v7           #e:Ljava/lang/Exception;
    :catchall_76
    move-exception v1

    #@77
    if-eqz v6, :cond_7c

    #@79
    .line 188
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@7c
    :cond_7c
    throw v1
.end method

.method public static getValidityPeriodFromVersion(Landroid/content/Context;)Ljava/lang/String;
    .registers 12
    .parameter "context"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    .line 196
    if-nez p0, :cond_4

    #@3
    .line 229
    :cond_3
    :goto_3
    return-object v10

    #@4
    .line 200
    :cond_4
    invoke-static {p0}, Lcom/lge/ims/service/ac/ACContentHelper;->getReferenceId(Landroid/content/Context;)I

    #@7
    move-result v9

    #@8
    .line 202
    .local v9, refId:I
    if-ltz v9, :cond_3

    #@a
    .line 206
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@d
    move-result-object v0

    #@e
    .line 207
    .local v0, cr:Landroid/content/ContentResolver;
    const/4 v6, 0x0

    #@f
    .line 208
    .local v6, c:Landroid/database/Cursor;
    const/4 v10, 0x0

    #@10
    .line 211
    .local v10, validityPeriod:Ljava/lang/String;
    :try_start_10
    sget-object v1, Lcom/lge/ims/provider/ac/AC$Provisioning$Version;->CONTENT_URI:Landroid/net/Uri;

    #@12
    const/4 v2, 0x0

    #@13
    new-instance v3, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v4, "id=\'"

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@21
    move-result-object v4

    #@22
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    const-string v4, "\'"

    #@28
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v3

    #@2c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v3

    #@30
    const/4 v4, 0x0

    #@31
    const/4 v5, 0x0

    #@32
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@35
    move-result-object v6

    #@36
    .line 213
    if-eqz v6, :cond_4a

    #@38
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@3b
    move-result v1

    #@3c
    if-eqz v1, :cond_4a

    #@3e
    .line 214
    const-string v1, "validity_period"

    #@40
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@43
    move-result v8

    #@44
    .line 216
    .local v8, index:I
    if-ltz v8, :cond_4a

    #@46
    .line 217
    invoke-interface {v6, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_49
    .catchall {:try_start_10 .. :try_end_49} :catchall_76
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_49} :catch_50

    #@49
    move-result-object v10

    #@4a
    .line 224
    .end local v8           #index:I
    :cond_4a
    if-eqz v6, :cond_3

    #@4c
    .line 225
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@4f
    goto :goto_3

    #@50
    .line 220
    :catch_50
    move-exception v7

    #@51
    .line 221
    .local v7, e:Ljava/lang/Exception;
    :try_start_51
    const-string v1, "ACContentHelper"

    #@53
    new-instance v2, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    const-string v3, "Exception :: "

    #@5a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v2

    #@5e
    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@61
    move-result-object v3

    #@62
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v2

    #@66
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v2

    #@6a
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@6d
    .line 222
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_70
    .catchall {:try_start_51 .. :try_end_70} :catchall_76

    #@70
    .line 224
    if-eqz v6, :cond_3

    #@72
    .line 225
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@75
    goto :goto_3

    #@76
    .line 224
    .end local v7           #e:Ljava/lang/Exception;
    :catchall_76
    move-exception v1

    #@77
    if-eqz v6, :cond_7c

    #@79
    .line 225
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@7c
    :cond_7c
    throw v1
.end method

.method public static getVersionFromVersion(Landroid/content/Context;)Ljava/lang/String;
    .registers 12
    .parameter "context"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    .line 233
    if-nez p0, :cond_4

    #@3
    .line 266
    :cond_3
    :goto_3
    return-object v10

    #@4
    .line 237
    :cond_4
    invoke-static {p0}, Lcom/lge/ims/service/ac/ACContentHelper;->getReferenceId(Landroid/content/Context;)I

    #@7
    move-result v9

    #@8
    .line 239
    .local v9, refId:I
    if-ltz v9, :cond_3

    #@a
    .line 243
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@d
    move-result-object v0

    #@e
    .line 244
    .local v0, cr:Landroid/content/ContentResolver;
    const/4 v6, 0x0

    #@f
    .line 245
    .local v6, c:Landroid/database/Cursor;
    const/4 v10, 0x0

    #@10
    .line 248
    .local v10, version:Ljava/lang/String;
    :try_start_10
    sget-object v1, Lcom/lge/ims/provider/ac/AC$Provisioning$Version;->CONTENT_URI:Landroid/net/Uri;

    #@12
    const/4 v2, 0x0

    #@13
    new-instance v3, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v4, "id=\'"

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@21
    move-result-object v4

    #@22
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    const-string v4, "\'"

    #@28
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v3

    #@2c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v3

    #@30
    const/4 v4, 0x0

    #@31
    const/4 v5, 0x0

    #@32
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@35
    move-result-object v6

    #@36
    .line 250
    if-eqz v6, :cond_4a

    #@38
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@3b
    move-result v1

    #@3c
    if-eqz v1, :cond_4a

    #@3e
    .line 251
    const-string v1, "version"

    #@40
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@43
    move-result v8

    #@44
    .line 253
    .local v8, index:I
    if-ltz v8, :cond_4a

    #@46
    .line 254
    invoke-interface {v6, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_49
    .catchall {:try_start_10 .. :try_end_49} :catchall_76
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_49} :catch_50

    #@49
    move-result-object v10

    #@4a
    .line 261
    .end local v8           #index:I
    :cond_4a
    if-eqz v6, :cond_3

    #@4c
    .line 262
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@4f
    goto :goto_3

    #@50
    .line 257
    :catch_50
    move-exception v7

    #@51
    .line 258
    .local v7, e:Ljava/lang/Exception;
    :try_start_51
    const-string v1, "ACContentHelper"

    #@53
    new-instance v2, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    const-string v3, "Exception :: "

    #@5a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v2

    #@5e
    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@61
    move-result-object v3

    #@62
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v2

    #@66
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v2

    #@6a
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@6d
    .line 259
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_70
    .catchall {:try_start_51 .. :try_end_70} :catchall_76

    #@70
    .line 261
    if-eqz v6, :cond_3

    #@72
    .line 262
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@75
    goto :goto_3

    #@76
    .line 261
    .end local v7           #e:Ljava/lang/Exception;
    :catchall_76
    move-exception v1

    #@77
    if-eqz v6, :cond_7c

    #@79
    .line 262
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@7c
    :cond_7c
    throw v1
.end method

.method public static hasCachedConfiguration(Landroid/content/Context;)Z
    .registers 13
    .parameter "context"

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    .line 306
    if-nez p0, :cond_5

    #@3
    move v1, v11

    #@4
    .line 345
    :goto_4
    return v1

    #@5
    .line 310
    :cond_5
    invoke-static {p0}, Lcom/lge/ims/service/ac/ACContentHelper;->getReferenceId(Landroid/content/Context;)I

    #@8
    move-result v8

    #@9
    .line 312
    .local v8, refId:I
    if-gez v8, :cond_d

    #@b
    move v1, v11

    #@c
    .line 313
    goto :goto_4

    #@d
    .line 316
    :cond_d
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@10
    move-result-object v0

    #@11
    .line 317
    .local v0, cr:Landroid/content/ContentResolver;
    const/4 v6, 0x0

    #@12
    .line 318
    .local v6, c:Landroid/database/Cursor;
    const/4 v9, 0x0

    #@13
    .line 319
    .local v9, validity:Ljava/lang/String;
    const/4 v10, 0x0

    #@14
    .line 322
    .local v10, version:Ljava/lang/String;
    :try_start_14
    sget-object v1, Lcom/lge/ims/provider/ac/AC$Provisioning$Version;->CONTENT_URI:Landroid/net/Uri;

    #@16
    const/4 v2, 0x0

    #@17
    new-instance v3, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v4, "id=\'"

    #@1e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@25
    move-result-object v4

    #@26
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v3

    #@2a
    const-string v4, "\'"

    #@2c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v3

    #@30
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v3

    #@34
    const/4 v4, 0x0

    #@35
    const/4 v5, 0x0

    #@36
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@39
    move-result-object v6

    #@3a
    .line 324
    if-eqz v6, :cond_56

    #@3c
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@3f
    move-result v1

    #@40
    if-eqz v1, :cond_56

    #@42
    .line 325
    const-string v1, "validity"

    #@44
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@47
    move-result v1

    #@48
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@4b
    move-result-object v9

    #@4c
    .line 326
    const-string v1, "version"

    #@4e
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@51
    move-result v1

    #@52
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_55
    .catchall {:try_start_14 .. :try_end_55} :catchall_87
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_55} :catch_61

    #@55
    move-result-object v10

    #@56
    .line 332
    :cond_56
    if-eqz v6, :cond_5b

    #@58
    .line 333
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@5b
    .line 337
    :cond_5b
    :goto_5b
    if-eqz v9, :cond_5f

    #@5d
    if-nez v10, :cond_8e

    #@5f
    :cond_5f
    move v1, v11

    #@60
    .line 338
    goto :goto_4

    #@61
    .line 328
    :catch_61
    move-exception v7

    #@62
    .line 329
    .local v7, e:Ljava/lang/Exception;
    :try_start_62
    const-string v1, "ACContentHelper"

    #@64
    new-instance v2, Ljava/lang/StringBuilder;

    #@66
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@69
    const-string v3, "Exception :: "

    #@6b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v2

    #@6f
    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@72
    move-result-object v3

    #@73
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v2

    #@77
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7a
    move-result-object v2

    #@7b
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@7e
    .line 330
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_81
    .catchall {:try_start_62 .. :try_end_81} :catchall_87

    #@81
    .line 332
    if-eqz v6, :cond_5b

    #@83
    .line 333
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@86
    goto :goto_5b

    #@87
    .line 332
    .end local v7           #e:Ljava/lang/Exception;
    :catchall_87
    move-exception v1

    #@88
    if-eqz v6, :cond_8d

    #@8a
    .line 333
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@8d
    :cond_8d
    throw v1

    #@8e
    .line 341
    :cond_8e
    const-string v1, "0"

    #@90
    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@93
    move-result v1

    #@94
    if-nez v1, :cond_9e

    #@96
    const-string v1, "-1"

    #@98
    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9b
    move-result v1

    #@9c
    if-eqz v1, :cond_a1

    #@9e
    :cond_9e
    move v1, v11

    #@9f
    .line 342
    goto/16 :goto_4

    #@a1
    .line 345
    :cond_a1
    const/4 v1, 0x1

    #@a2
    goto/16 :goto_4
.end method

.method public static isSameACVersion(Landroid/content/Context;Lcom/lge/ims/service/ac/ACContentCache;)Z
    .registers 6
    .parameter "context"
    .parameter "contentCache"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 349
    if-nez p1, :cond_4

    #@3
    .line 363
    :cond_3
    :goto_3
    return v2

    #@4
    .line 353
    :cond_4
    invoke-virtual {p1}, Lcom/lge/ims/service/ac/ACContentCache;->getVersion()Lcom/lge/ims/service/ac/content/ACVersion;

    #@7
    move-result-object v1

    #@8
    .line 355
    .local v1, version:Lcom/lge/ims/service/ac/content/ACVersion;
    if-eqz v1, :cond_3

    #@a
    invoke-virtual {v1}, Lcom/lge/ims/service/ac/content/ACVersion;->isContentPresent()Z

    #@d
    move-result v3

    #@e
    if-eqz v3, :cond_3

    #@10
    .line 356
    invoke-static {p0}, Lcom/lge/ims/service/ac/ACContentHelper;->getVersionFromVersion(Landroid/content/Context;)Ljava/lang/String;

    #@13
    move-result-object v0

    #@14
    .line 358
    .local v0, currentVersion:Ljava/lang/String;
    if-eqz v0, :cond_3

    #@16
    iget-object v3, v1, Lcom/lge/ims/service/ac/content/ACVersion;->mVersion:Ljava/lang/String;

    #@18
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_3

    #@1e
    .line 359
    const/4 v2, 0x1

    #@1f
    goto :goto_3
.end method

.method public static updateContentForAC(Landroid/content/Context;Lcom/lge/ims/service/ac/ACContentCache;)V
    .registers 7
    .parameter "context"
    .parameter "contentCache"

    #@0
    .prologue
    .line 462
    const-string v2, "ACContentHelper"

    #@2
    const-string v3, "updateContentForAC"

    #@4
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 464
    if-nez p1, :cond_a

    #@9
    .line 489
    :cond_9
    :goto_9
    return-void

    #@a
    .line 468
    :cond_a
    if-eqz p0, :cond_9

    #@c
    .line 472
    const/4 v0, 0x1

    #@d
    .line 473
    .local v0, isUpdate:Z
    invoke-static {p0}, Lcom/lge/ims/service/ac/ACContentHelper;->getReferenceId(Landroid/content/Context;)I

    #@10
    move-result v1

    #@11
    .line 475
    .local v1, refId:I
    if-gez v1, :cond_18

    #@13
    .line 476
    invoke-static {p0}, Lcom/lge/ims/service/ac/ACContentHelper;->createReferenceId(Landroid/content/Context;)I

    #@16
    move-result v1

    #@17
    .line 477
    const/4 v0, 0x0

    #@18
    .line 480
    :cond_18
    if-gez v1, :cond_33

    #@1a
    .line 481
    const-string v2, "ACContentHelper"

    #@1c
    new-instance v3, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v4, "Invalid refId="

    #@23
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v3

    #@2f
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@32
    goto :goto_9

    #@33
    .line 485
    :cond_33
    invoke-virtual {p1}, Lcom/lge/ims/service/ac/ACContentCache;->getVersion()Lcom/lge/ims/service/ac/content/ACVersion;

    #@36
    move-result-object v2

    #@37
    invoke-static {p0, v2, v1, v0}, Lcom/lge/ims/service/ac/ACContentHelper;->updateVersionForAC(Landroid/content/Context;Lcom/lge/ims/service/ac/content/ACVersion;IZ)V

    #@3a
    .line 486
    invoke-virtual {p1}, Lcom/lge/ims/service/ac/ACContentCache;->getSubscriber()Lcom/lge/ims/service/ac/content/ACSubscriber;

    #@3d
    move-result-object v2

    #@3e
    invoke-static {p0, v2, v1, v0}, Lcom/lge/ims/service/ac/ACContentHelper;->updateSubscriberForAC(Landroid/content/Context;Lcom/lge/ims/service/ac/content/ACSubscriber;IZ)V

    #@41
    .line 487
    invoke-virtual {p1}, Lcom/lge/ims/service/ac/ACContentCache;->getProxyAddress()Lcom/lge/ims/service/ac/content/ACProxyAddress;

    #@44
    move-result-object v2

    #@45
    invoke-static {p0, v2, v1, v0}, Lcom/lge/ims/service/ac/ACContentHelper;->updateProxyAddressForAC(Landroid/content/Context;Lcom/lge/ims/service/ac/content/ACProxyAddress;IZ)V

    #@48
    .line 488
    invoke-virtual {p1}, Lcom/lge/ims/service/ac/ACContentCache;->getServiceAvailability()Lcom/lge/ims/service/ac/content/ACServiceAvailability;

    #@4b
    move-result-object v2

    #@4c
    invoke-static {p0, v2, v1, v0}, Lcom/lge/ims/service/ac/ACContentHelper;->updateServiceAvailabilityForAC(Landroid/content/Context;Lcom/lge/ims/service/ac/content/ACServiceAvailability;IZ)V

    #@4f
    goto :goto_9
.end method

.method public static updateContentForACVersion(Landroid/content/Context;Lcom/lge/ims/service/ac/ACContentCache;)V
    .registers 6
    .parameter "context"
    .parameter "contentCache"

    #@0
    .prologue
    .line 492
    const-string v1, "ACContentHelper"

    #@2
    const-string v2, "updateContentForACVersion"

    #@4
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 494
    if-nez p1, :cond_a

    #@9
    .line 510
    :cond_9
    :goto_9
    return-void

    #@a
    .line 498
    :cond_a
    if-eqz p0, :cond_9

    #@c
    .line 502
    invoke-static {p0}, Lcom/lge/ims/service/ac/ACContentHelper;->getReferenceId(Landroid/content/Context;)I

    #@f
    move-result v0

    #@10
    .line 504
    .local v0, refId:I
    if-gez v0, :cond_2b

    #@12
    .line 505
    const-string v1, "ACContentHelper"

    #@14
    new-instance v2, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v3, "Invalid refId="

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@2a
    goto :goto_9

    #@2b
    .line 509
    :cond_2b
    invoke-virtual {p1}, Lcom/lge/ims/service/ac/ACContentCache;->getVersion()Lcom/lge/ims/service/ac/content/ACVersion;

    #@2e
    move-result-object v1

    #@2f
    const/4 v2, 0x1

    #@30
    invoke-static {p0, v1, v0, v2}, Lcom/lge/ims/service/ac/ACContentHelper;->updateVersionForAC(Landroid/content/Context;Lcom/lge/ims/service/ac/content/ACVersion;IZ)V

    #@33
    goto :goto_9
.end method

.method public static updateContentForIMS(Landroid/content/Context;)I
    .registers 8
    .parameter "context"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 513
    const-string v4, "ACContentHelper"

    #@3
    const-string v5, "updateContentForIMS from cached configuration"

    #@5
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 515
    if-nez p0, :cond_b

    #@a
    .line 537
    :goto_a
    return v3

    #@b
    .line 519
    :cond_b
    invoke-static {p0}, Lcom/lge/ims/service/ac/ACContentHelper;->getReferenceId(Landroid/content/Context;)I

    #@e
    move-result v1

    #@f
    .line 521
    .local v1, refId:I
    if-gez v1, :cond_2a

    #@11
    .line 522
    const-string v4, "ACContentHelper"

    #@13
    new-instance v5, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v6, "Invalid refId="

    #@1a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v5

    #@1e
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v5

    #@22
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v5

    #@26
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@29
    goto :goto_a

    #@2a
    .line 526
    :cond_2a
    const/4 v3, 0x0

    #@2b
    .line 527
    .local v3, updatedTables:I
    invoke-static {p0, v1}, Lcom/lge/ims/service/ac/ACContentHelper;->getSubscriberFromAC(Landroid/content/Context;I)Lcom/lge/ims/service/ac/content/ACSubscriber;

    #@2e
    move-result-object v2

    #@2f
    .line 528
    .local v2, subscriber:Lcom/lge/ims/service/ac/content/ACSubscriber;
    invoke-static {p0, v1}, Lcom/lge/ims/service/ac/ACContentHelper;->getProxyAddressFromAC(Landroid/content/Context;I)Lcom/lge/ims/service/ac/content/ACProxyAddress;

    #@32
    move-result-object v0

    #@33
    .line 530
    .local v0, proxyAddress:Lcom/lge/ims/service/ac/content/ACProxyAddress;
    invoke-static {}, Lcom/lge/ims/Configuration;->isDebugOn()Z

    #@36
    move-result v4

    #@37
    if-eqz v4, :cond_71

    #@39
    .line 531
    const-string v4, "ACContentHelper"

    #@3b
    new-instance v5, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v6, "Subscriber :: "

    #@42
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v5

    #@46
    invoke-virtual {v2}, Lcom/lge/ims/service/ac/content/ACSubscriber;->toString()Ljava/lang/String;

    #@49
    move-result-object v6

    #@4a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v5

    #@4e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v5

    #@52
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@55
    .line 532
    const-string v4, "ACContentHelper"

    #@57
    new-instance v5, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    const-string v6, "ProxyAddress :: "

    #@5e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v5

    #@62
    invoke-virtual {v0}, Lcom/lge/ims/service/ac/content/ACProxyAddress;->toString()Ljava/lang/String;

    #@65
    move-result-object v6

    #@66
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v5

    #@6a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d
    move-result-object v5

    #@6e
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@71
    .line 535
    :cond_71
    invoke-static {p0, v2, v0}, Lcom/lge/ims/service/ac/ACContentHelper;->updateSubscriberForIMS(Landroid/content/Context;Lcom/lge/ims/service/ac/content/ACSubscriber;Lcom/lge/ims/service/ac/content/ACProxyAddress;)I

    #@74
    move-result v4

    #@75
    or-int/2addr v3, v4

    #@76
    .line 537
    goto :goto_a
.end method

.method public static updateContentForIMS(Landroid/content/Context;Lcom/lge/ims/service/ac/ACContentCache;)I
    .registers 5
    .parameter "context"
    .parameter "contentCache"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 541
    const-string v1, "ACContentHelper"

    #@3
    const-string v2, "updateContentForIMS"

    #@5
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 543
    if-nez p1, :cond_b

    #@a
    .line 555
    :cond_a
    :goto_a
    return v0

    #@b
    .line 547
    :cond_b
    if-eqz p0, :cond_a

    #@d
    .line 551
    const/4 v0, 0x0

    #@e
    .line 553
    .local v0, updatedTables:I
    invoke-virtual {p1}, Lcom/lge/ims/service/ac/ACContentCache;->getSubscriber()Lcom/lge/ims/service/ac/content/ACSubscriber;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {p1}, Lcom/lge/ims/service/ac/ACContentCache;->getProxyAddress()Lcom/lge/ims/service/ac/content/ACProxyAddress;

    #@15
    move-result-object v2

    #@16
    invoke-static {p0, v1, v2}, Lcom/lge/ims/service/ac/ACContentHelper;->updateSubscriberForIMS(Landroid/content/Context;Lcom/lge/ims/service/ac/content/ACSubscriber;Lcom/lge/ims/service/ac/content/ACProxyAddress;)I

    #@19
    move-result v1

    #@1a
    or-int/2addr v0, v1

    #@1b
    .line 555
    goto :goto_a
.end method

.method private static updateProxyAddressForAC(Landroid/content/Context;Lcom/lge/ims/service/ac/content/ACProxyAddress;IZ)V
    .registers 10
    .parameter "context"
    .parameter "proxyAddress"
    .parameter "refId"
    .parameter "isUpdate"

    #@0
    .prologue
    .line 1093
    if-nez p1, :cond_3

    #@2
    .line 1141
    :cond_2
    :goto_2
    return-void

    #@3
    .line 1097
    :cond_3
    invoke-virtual {p1}, Lcom/lge/ims/service/ac/content/ACProxyAddress;->isContentPresent()Z

    #@6
    move-result v3

    #@7
    if-eqz v3, :cond_2

    #@9
    .line 1101
    new-instance v1, Landroid/content/ContentValues;

    #@b
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    #@e
    .line 1103
    .local v1, cv:Landroid/content/ContentValues;
    const-string v3, "id"

    #@10
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@13
    move-result-object v4

    #@14
    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@17
    .line 1105
    iget-object v3, p1, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCAddress:Ljava/lang/String;

    #@19
    if-eqz v3, :cond_22

    #@1b
    .line 1106
    const-string v3, "sbc_address"

    #@1d
    iget-object v4, p1, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCAddress:Ljava/lang/String;

    #@1f
    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@22
    .line 1109
    :cond_22
    iget-object v3, p1, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCAddressType:Ljava/lang/String;

    #@24
    if-eqz v3, :cond_2d

    #@26
    .line 1110
    const-string v3, "sbc_address_type"

    #@28
    iget-object v4, p1, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCAddressType:Ljava/lang/String;

    #@2a
    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2d
    .line 1113
    :cond_2d
    iget-object v3, p1, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCTLSAddress:Ljava/lang/String;

    #@2f
    if-eqz v3, :cond_38

    #@31
    .line 1114
    const-string v3, "sbc_tls_address"

    #@33
    iget-object v4, p1, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCTLSAddress:Ljava/lang/String;

    #@35
    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@38
    .line 1117
    :cond_38
    iget-object v3, p1, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCTLSAddressType:Ljava/lang/String;

    #@3a
    if-eqz v3, :cond_43

    #@3c
    .line 1118
    const-string v3, "sbc_tls_address_type"

    #@3e
    iget-object v4, p1, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCTLSAddressType:Ljava/lang/String;

    #@40
    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@43
    .line 1121
    :cond_43
    iget-object v3, p1, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mLBOPCSCFAddress:Ljava/lang/String;

    #@45
    if-eqz v3, :cond_4e

    #@47
    .line 1122
    const-string v3, "lbo_pcscf_address"

    #@49
    iget-object v4, p1, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mLBOPCSCFAddress:Ljava/lang/String;

    #@4b
    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@4e
    .line 1125
    :cond_4e
    iget-object v3, p1, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mLBOPCSCFAddressType:Ljava/lang/String;

    #@50
    if-eqz v3, :cond_59

    #@52
    .line 1126
    const-string v3, "lbo_pcscf_address_type"

    #@54
    iget-object v4, p1, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mLBOPCSCFAddressType:Ljava/lang/String;

    #@56
    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@59
    .line 1129
    :cond_59
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5c
    move-result-object v0

    #@5d
    .line 1132
    .local v0, cr:Landroid/content/ContentResolver;
    if-eqz p3, :cond_a5

    #@5f
    .line 1133
    :try_start_5f
    sget-object v3, Lcom/lge/ims/provider/ac/AC$Provisioning$ProxyAddress;->CONTENT_URI:Landroid/net/Uri;

    #@61
    new-instance v4, Ljava/lang/StringBuilder;

    #@63
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@66
    const-string v5, "id=\'"

    #@68
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v4

    #@6c
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6f
    move-result-object v5

    #@70
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v4

    #@74
    const-string v5, "\'"

    #@76
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v4

    #@7a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v4

    #@7e
    const/4 v5, 0x0

    #@7f
    invoke-virtual {v0, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_82
    .catch Ljava/lang/Exception; {:try_start_5f .. :try_end_82} :catch_83

    #@82
    goto :goto_2

    #@83
    .line 1137
    :catch_83
    move-exception v2

    #@84
    .line 1138
    .local v2, e:Ljava/lang/Exception;
    const-string v3, "ACContentHelper"

    #@86
    new-instance v4, Ljava/lang/StringBuilder;

    #@88
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@8b
    const-string v5, "Exception :: "

    #@8d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v4

    #@91
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@94
    move-result-object v5

    #@95
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v4

    #@99
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9c
    move-result-object v4

    #@9d
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@a0
    .line 1139
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    #@a3
    goto/16 :goto_2

    #@a5
    .line 1135
    .end local v2           #e:Ljava/lang/Exception;
    :cond_a5
    :try_start_a5
    sget-object v3, Lcom/lge/ims/provider/ac/AC$Provisioning$ProxyAddress;->CONTENT_URI:Landroid/net/Uri;

    #@a7
    invoke-virtual {v0, v3, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_aa
    .catch Ljava/lang/Exception; {:try_start_a5 .. :try_end_aa} :catch_83

    #@aa
    goto/16 :goto_2
.end method

.method private static updateServiceAvailabilityForAC(Landroid/content/Context;Lcom/lge/ims/service/ac/content/ACServiceAvailability;IZ)V
    .registers 10
    .parameter "context"
    .parameter "serviceAvailability"
    .parameter "refId"
    .parameter "isUpdate"

    #@0
    .prologue
    .line 1145
    if-nez p1, :cond_3

    #@2
    .line 1181
    :cond_2
    :goto_2
    return-void

    #@3
    .line 1149
    :cond_3
    invoke-virtual {p1}, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->isContentPresent()Z

    #@6
    move-result v3

    #@7
    if-eqz v3, :cond_2

    #@9
    .line 1153
    new-instance v1, Landroid/content/ContentValues;

    #@b
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    #@e
    .line 1155
    .local v1, cv:Landroid/content/ContentValues;
    const-string v3, "id"

    #@10
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@13
    move-result-object v4

    #@14
    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@17
    .line 1157
    iget-object v3, p1, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->mRCSOEM:Ljava/lang/String;

    #@19
    if-eqz v3, :cond_22

    #@1b
    .line 1158
    const-string v3, "rcs_oem"

    #@1d
    iget-object v4, p1, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->mRCSOEM:Ljava/lang/String;

    #@1f
    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@22
    .line 1161
    :cond_22
    iget-object v3, p1, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->mVOLTE:Ljava/lang/String;

    #@24
    if-eqz v3, :cond_2d

    #@26
    .line 1162
    const-string v3, "volte"

    #@28
    iget-object v4, p1, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->mVOLTE:Ljava/lang/String;

    #@2a
    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2d
    .line 1165
    :cond_2d
    iget-object v3, p1, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->mPSVT:Ljava/lang/String;

    #@2f
    if-eqz v3, :cond_38

    #@31
    .line 1166
    const-string v3, "vt"

    #@33
    iget-object v4, p1, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->mPSVT:Ljava/lang/String;

    #@35
    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@38
    .line 1169
    :cond_38
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3b
    move-result-object v0

    #@3c
    .line 1172
    .local v0, cr:Landroid/content/ContentResolver;
    if-eqz p3, :cond_83

    #@3e
    .line 1173
    :try_start_3e
    sget-object v3, Lcom/lge/ims/provider/ac/AC$Provisioning$ServiceAvailability;->CONTENT_URI:Landroid/net/Uri;

    #@40
    new-instance v4, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v5, "id=\'"

    #@47
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v4

    #@4b
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4e
    move-result-object v5

    #@4f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v4

    #@53
    const-string v5, "\'"

    #@55
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v4

    #@59
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v4

    #@5d
    const/4 v5, 0x0

    #@5e
    invoke-virtual {v0, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_61
    .catch Ljava/lang/Exception; {:try_start_3e .. :try_end_61} :catch_62

    #@61
    goto :goto_2

    #@62
    .line 1177
    :catch_62
    move-exception v2

    #@63
    .line 1178
    .local v2, e:Ljava/lang/Exception;
    const-string v3, "ACContentHelper"

    #@65
    new-instance v4, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    const-string v5, "Exception :: "

    #@6c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v4

    #@70
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@73
    move-result-object v5

    #@74
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v4

    #@78
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v4

    #@7c
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@7f
    .line 1179
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    #@82
    goto :goto_2

    #@83
    .line 1175
    .end local v2           #e:Ljava/lang/Exception;
    :cond_83
    :try_start_83
    sget-object v3, Lcom/lge/ims/provider/ac/AC$Provisioning$ServiceAvailability;->CONTENT_URI:Landroid/net/Uri;

    #@85
    invoke-virtual {v0, v3, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_88
    .catch Ljava/lang/Exception; {:try_start_83 .. :try_end_88} :catch_62

    #@88
    goto/16 :goto_2
.end method

.method public static updateServiceAvailabilityForIMS(ZZ)I
    .registers 18
    .parameter "isVoLTEOn"
    .parameter "isVTOn"

    #@0
    .prologue
    .line 561
    const/16 v0, 0x100

    #@2
    .line 562
    .local v0, FEATURE_TAG_MEDIA_STREAM_AUDIO:I
    const/16 v1, 0x200

    #@4
    .line 564
    .local v1, FEATURE_TAG_MEDIA_STREAM_VIDEO:I
    if-nez p0, :cond_f

    #@6
    .line 565
    const-string v13, "ACContentHelper"

    #@8
    const-string v14, "VoLTE service is not provisioned"

    #@a
    invoke-static {v13, v14}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    .line 566
    const/4 v12, 0x0

    #@e
    .line 670
    :goto_e
    return v12

    #@f
    .line 569
    :cond_f
    const/4 v8, 0x0

    #@10
    .line 572
    .local v8, imsdb:Landroid/database/sqlite/SQLiteDatabase;
    :try_start_10
    const-string v13, "/data/data/com.lge.ims/databases/lgims.db"

    #@12
    const/4 v14, 0x0

    #@13
    const/4 v15, 0x0

    #@14
    invoke-static {v13, v14, v15}, Landroid/database/sqlite/SQLiteDatabase;->openDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)Landroid/database/sqlite/SQLiteDatabase;
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_17} :catch_23

    #@17
    move-result-object v8

    #@18
    .line 579
    if-nez v8, :cond_45

    #@1a
    .line 580
    const-string v13, "ACContentHelper"

    #@1c
    const-string v14, "Opening lgims db failed"

    #@1e
    invoke-static {v13, v14}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@21
    .line 581
    const/4 v12, 0x0

    #@22
    goto :goto_e

    #@23
    .line 573
    :catch_23
    move-exception v5

    #@24
    .line 574
    .local v5, e:Ljava/lang/Exception;
    const-string v13, "ACContentHelper"

    #@26
    new-instance v14, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v15, "Exception :: "

    #@2d
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v14

    #@31
    invoke-virtual {v5}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@34
    move-result-object v15

    #@35
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v14

    #@39
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v14

    #@3d
    invoke-static {v13, v14}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@40
    .line 575
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    #@43
    .line 576
    const/4 v12, 0x0

    #@44
    goto :goto_e

    #@45
    .line 584
    .end local v5           #e:Ljava/lang/Exception;
    :cond_45
    const/4 v3, 0x0

    #@46
    .line 585
    .local v3, c:Landroid/database/Cursor;
    const/4 v6, -0x1

    #@47
    .line 588
    .local v6, featureTags:I
    :try_start_47
    const-string v13, "select * from lgims_com_kt_sip"

    #@49
    const/4 v14, 0x0

    #@4a
    invoke-virtual {v8, v13, v14}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    #@4d
    move-result-object v3

    #@4e
    .line 590
    if-eqz v3, :cond_62

    #@50
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    #@53
    move-result v13

    #@54
    if-eqz v13, :cond_62

    #@56
    .line 591
    const-string v13, "header_info_feature_tags"

    #@58
    invoke-interface {v3, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@5b
    move-result v9

    #@5c
    .line 593
    .local v9, index:I
    if-ltz v9, :cond_62

    #@5e
    .line 594
    invoke-interface {v3, v9}, Landroid/database/Cursor;->getInt(I)I
    :try_end_61
    .catchall {:try_start_47 .. :try_end_61} :catchall_9b
    .catch Ljava/lang/Exception; {:try_start_47 .. :try_end_61} :catch_75

    #@61
    move-result v6

    #@62
    .line 601
    .end local v9           #index:I
    :cond_62
    if-eqz v3, :cond_67

    #@64
    .line 602
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    #@67
    .line 606
    :cond_67
    :goto_67
    if-gez v6, :cond_a2

    #@69
    .line 607
    const-string v13, "ACContentHelper"

    #@6b
    const-string v14, "No feature-tags flag"

    #@6d
    invoke-static {v13, v14}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@70
    .line 608
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    #@73
    .line 609
    const/4 v12, 0x0

    #@74
    goto :goto_e

    #@75
    .line 597
    :catch_75
    move-exception v5

    #@76
    .line 598
    .restart local v5       #e:Ljava/lang/Exception;
    :try_start_76
    const-string v13, "ACContentHelper"

    #@78
    new-instance v14, Ljava/lang/StringBuilder;

    #@7a
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@7d
    const-string v15, "Exception :: "

    #@7f
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v14

    #@83
    invoke-virtual {v5}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@86
    move-result-object v15

    #@87
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v14

    #@8b
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8e
    move-result-object v14

    #@8f
    invoke-static {v13, v14}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@92
    .line 599
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_95
    .catchall {:try_start_76 .. :try_end_95} :catchall_9b

    #@95
    .line 601
    if-eqz v3, :cond_67

    #@97
    .line 602
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    #@9a
    goto :goto_67

    #@9b
    .line 601
    .end local v5           #e:Ljava/lang/Exception;
    :catchall_9b
    move-exception v13

    #@9c
    if-eqz v3, :cond_a1

    #@9e
    .line 602
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    #@a1
    :cond_a1
    throw v13

    #@a2
    .line 612
    :cond_a2
    if-eqz p0, :cond_dc

    #@a4
    if-eqz p1, :cond_dc

    #@a6
    .line 613
    and-int/lit16 v13, v6, 0x100

    #@a8
    if-eqz v13, :cond_b4

    #@aa
    and-int/lit16 v13, v6, 0x200

    #@ac
    if-eqz v13, :cond_b4

    #@ae
    .line 615
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    #@b1
    .line 616
    const/4 v12, 0x0

    #@b2
    goto/16 :goto_e

    #@b4
    .line 618
    :cond_b4
    or-int/lit16 v6, v6, 0x100

    #@b6
    .line 619
    or-int/lit16 v6, v6, 0x200

    #@b8
    .line 635
    :goto_b8
    invoke-static {v6}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@bb
    move-result-object v10

    #@bc
    .line 636
    .local v10, newFeatureTags:Ljava/lang/String;
    const-string v11, "0x"

    #@be
    .line 638
    .local v11, prefix:Ljava/lang/String;
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    #@c1
    move-result v7

    #@c2
    .local v7, i:I
    :goto_c2
    const/16 v13, 0x8

    #@c4
    if-ge v7, v13, :cond_f7

    #@c6
    .line 639
    new-instance v13, Ljava/lang/StringBuilder;

    #@c8
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@cb
    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v13

    #@cf
    const-string v14, "0"

    #@d1
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d4
    move-result-object v13

    #@d5
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d8
    move-result-object v11

    #@d9
    .line 638
    add-int/lit8 v7, v7, 0x1

    #@db
    goto :goto_c2

    #@dc
    .line 621
    .end local v7           #i:I
    .end local v10           #newFeatureTags:Ljava/lang/String;
    .end local v11           #prefix:Ljava/lang/String;
    :cond_dc
    if-eqz p0, :cond_f1

    #@de
    .line 622
    and-int/lit16 v13, v6, 0x100

    #@e0
    if-eqz v13, :cond_ec

    #@e2
    and-int/lit16 v13, v6, 0x200

    #@e4
    if-nez v13, :cond_ec

    #@e6
    .line 624
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    #@e9
    .line 625
    const/4 v12, 0x0

    #@ea
    goto/16 :goto_e

    #@ec
    .line 627
    :cond_ec
    or-int/lit16 v6, v6, 0x100

    #@ee
    .line 628
    and-int/lit16 v6, v6, -0x201

    #@f0
    goto :goto_b8

    #@f1
    .line 631
    :cond_f1
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    #@f4
    .line 632
    const/4 v12, 0x0

    #@f5
    goto/16 :goto_e

    #@f7
    .line 642
    .restart local v7       #i:I
    .restart local v10       #newFeatureTags:Ljava/lang/String;
    .restart local v11       #prefix:Ljava/lang/String;
    :cond_f7
    invoke-virtual {v11, v10}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@fa
    move-result-object v10

    #@fb
    .line 644
    new-instance v4, Landroid/content/ContentValues;

    #@fd
    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    #@100
    .line 646
    .local v4, cv:Landroid/content/ContentValues;
    const-string v13, "header_info_feature_tags"

    #@102
    invoke-virtual {v4, v13, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@105
    .line 648
    const/16 v12, 0x100

    #@107
    .line 649
    .local v12, updatedTables:I
    const/4 v2, 0x0

    #@108
    .line 651
    .local v2, affectedRows:I
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    #@10b
    .line 654
    :try_start_10b
    const-string v13, "lgims_com_kt_sip"

    #@10d
    const-string v14, "id = \'1\'"

    #@10f
    const/4 v15, 0x0

    #@110
    invoke-virtual {v8, v13, v4, v14, v15}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@113
    move-result v2

    #@114
    .line 655
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_117
    .catchall {:try_start_10b .. :try_end_117} :catchall_162
    .catch Ljava/lang/Exception; {:try_start_10b .. :try_end_117} :catch_13d

    #@117
    .line 661
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@11a
    .line 664
    :goto_11a
    invoke-static {}, Lcom/lge/ims/Configuration;->isDebugOn()Z

    #@11d
    move-result v13

    #@11e
    if-eqz v13, :cond_138

    #@120
    .line 665
    const-string v13, "ACContentHelper"

    #@122
    new-instance v14, Ljava/lang/StringBuilder;

    #@124
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@127
    const-string v15, "lgims_com_kt_sip :: update="

    #@129
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12c
    move-result-object v14

    #@12d
    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@130
    move-result-object v14

    #@131
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@134
    move-result-object v14

    #@135
    invoke-static {v13, v14}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@138
    .line 668
    :cond_138
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    #@13b
    goto/16 :goto_e

    #@13d
    .line 656
    :catch_13d
    move-exception v5

    #@13e
    .line 657
    .restart local v5       #e:Ljava/lang/Exception;
    :try_start_13e
    const-string v13, "ACContentHelper"

    #@140
    new-instance v14, Ljava/lang/StringBuilder;

    #@142
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@145
    const-string v15, "Exception :: "

    #@147
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14a
    move-result-object v14

    #@14b
    invoke-virtual {v5}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@14e
    move-result-object v15

    #@14f
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@152
    move-result-object v14

    #@153
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@156
    move-result-object v14

    #@157
    invoke-static {v13, v14}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@15a
    .line 658
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_15d
    .catchall {:try_start_13e .. :try_end_15d} :catchall_162

    #@15d
    .line 659
    const/4 v12, 0x0

    #@15e
    .line 661
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@161
    goto :goto_11a

    #@162
    .end local v5           #e:Ljava/lang/Exception;
    :catchall_162
    move-exception v13

    #@163
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@166
    throw v13
.end method

.method private static updateSubscriberForAC(Landroid/content/Context;Lcom/lge/ims/service/ac/content/ACSubscriber;IZ)V
    .registers 12
    .parameter "context"
    .parameter "subscriber"
    .parameter "refId"
    .parameter "isUpdate"

    #@0
    .prologue
    const/4 v7, 0x2

    #@1
    const/4 v6, 0x1

    #@2
    .line 1185
    if-nez p1, :cond_5

    #@4
    .line 1249
    :cond_4
    :goto_4
    return-void

    #@5
    .line 1189
    :cond_5
    invoke-virtual {p1}, Lcom/lge/ims/service/ac/content/ACSubscriber;->isContentPresent()Z

    #@8
    move-result v3

    #@9
    if-eqz v3, :cond_4

    #@b
    .line 1193
    new-instance v1, Landroid/content/ContentValues;

    #@d
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    #@10
    .line 1195
    .local v1, cv:Landroid/content/ContentValues;
    const-string v3, "id"

    #@12
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@15
    move-result-object v4

    #@16
    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@19
    .line 1197
    iget-object v3, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPI:Ljava/lang/String;

    #@1b
    if-eqz v3, :cond_24

    #@1d
    .line 1198
    const-string v3, "impi"

    #@1f
    iget-object v4, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPI:Ljava/lang/String;

    #@21
    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@24
    .line 1201
    :cond_24
    iget-object v3, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPUs:Ljava/util/ArrayList;

    #@26
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    #@29
    move-result v3

    #@2a
    if-nez v3, :cond_64

    #@2c
    .line 1202
    const-string v4, "impu_0"

    #@2e
    iget-object v3, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPUs:Ljava/util/ArrayList;

    #@30
    const/4 v5, 0x0

    #@31
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@34
    move-result-object v3

    #@35
    check-cast v3, Ljava/lang/String;

    #@37
    invoke-virtual {v1, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@3a
    .line 1204
    iget-object v3, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPUs:Ljava/util/ArrayList;

    #@3c
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@3f
    move-result v3

    #@40
    if-le v3, v6, :cond_e8

    #@42
    .line 1205
    const-string v4, "impu_1"

    #@44
    iget-object v3, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPUs:Ljava/util/ArrayList;

    #@46
    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@49
    move-result-object v3

    #@4a
    check-cast v3, Ljava/lang/String;

    #@4c
    invoke-virtual {v1, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@4f
    .line 1210
    :goto_4f
    iget-object v3, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPUs:Ljava/util/ArrayList;

    #@51
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@54
    move-result v3

    #@55
    if-le v3, v7, :cond_f1

    #@57
    .line 1211
    const-string v4, "impu_2"

    #@59
    iget-object v3, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPUs:Ljava/util/ArrayList;

    #@5b
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5e
    move-result-object v3

    #@5f
    check-cast v3, Ljava/lang/String;

    #@61
    invoke-virtual {v1, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@64
    .line 1217
    :cond_64
    :goto_64
    iget-object v3, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mHomeDomainName:Ljava/lang/String;

    #@66
    if-eqz v3, :cond_6f

    #@68
    .line 1218
    const-string v3, "home_domain_name"

    #@6a
    iget-object v4, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mHomeDomainName:Ljava/lang/String;

    #@6c
    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@6f
    .line 1221
    :cond_6f
    iget-object v3, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthType:Ljava/lang/String;

    #@71
    if-eqz v3, :cond_7a

    #@73
    .line 1222
    const-string v3, "auth_type"

    #@75
    iget-object v4, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthType:Ljava/lang/String;

    #@77
    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@7a
    .line 1225
    :cond_7a
    iget-object v3, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthRealm:Ljava/lang/String;

    #@7c
    if-eqz v3, :cond_85

    #@7e
    .line 1226
    const-string v3, "auth_realm"

    #@80
    iget-object v4, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthRealm:Ljava/lang/String;

    #@82
    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@85
    .line 1229
    :cond_85
    iget-object v3, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthUsername:Ljava/lang/String;

    #@87
    if-eqz v3, :cond_90

    #@89
    .line 1230
    const-string v3, "auth_username"

    #@8b
    iget-object v4, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthUsername:Ljava/lang/String;

    #@8d
    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@90
    .line 1233
    :cond_90
    iget-object v3, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthPassword:Ljava/lang/String;

    #@92
    if-eqz v3, :cond_9b

    #@94
    .line 1234
    const-string v3, "auth_password"

    #@96
    iget-object v4, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthPassword:Ljava/lang/String;

    #@98
    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@9b
    .line 1237
    :cond_9b
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@9e
    move-result-object v0

    #@9f
    .line 1240
    .local v0, cr:Landroid/content/ContentResolver;
    if-eqz p3, :cond_fa

    #@a1
    .line 1241
    :try_start_a1
    sget-object v3, Lcom/lge/ims/provider/ac/AC$Provisioning$Subscriber;->CONTENT_URI:Landroid/net/Uri;

    #@a3
    new-instance v4, Ljava/lang/StringBuilder;

    #@a5
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@a8
    const-string v5, "id=\'"

    #@aa
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v4

    #@ae
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@b1
    move-result-object v5

    #@b2
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v4

    #@b6
    const-string v5, "\'"

    #@b8
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v4

    #@bc
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bf
    move-result-object v4

    #@c0
    const/4 v5, 0x0

    #@c1
    invoke-virtual {v0, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_c4
    .catch Ljava/lang/Exception; {:try_start_a1 .. :try_end_c4} :catch_c6

    #@c4
    goto/16 :goto_4

    #@c6
    .line 1245
    :catch_c6
    move-exception v2

    #@c7
    .line 1246
    .local v2, e:Ljava/lang/Exception;
    const-string v3, "ACContentHelper"

    #@c9
    new-instance v4, Ljava/lang/StringBuilder;

    #@cb
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@ce
    const-string v5, "Exception :: "

    #@d0
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d3
    move-result-object v4

    #@d4
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@d7
    move-result-object v5

    #@d8
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@db
    move-result-object v4

    #@dc
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@df
    move-result-object v4

    #@e0
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@e3
    .line 1247
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    #@e6
    goto/16 :goto_4

    #@e8
    .line 1207
    .end local v0           #cr:Landroid/content/ContentResolver;
    .end local v2           #e:Ljava/lang/Exception;
    :cond_e8
    const-string v3, "impu_1"

    #@ea
    const-string v4, ""

    #@ec
    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@ef
    goto/16 :goto_4f

    #@f1
    .line 1213
    :cond_f1
    const-string v3, "impu_2"

    #@f3
    const-string v4, ""

    #@f5
    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@f8
    goto/16 :goto_64

    #@fa
    .line 1243
    .restart local v0       #cr:Landroid/content/ContentResolver;
    :cond_fa
    :try_start_fa
    sget-object v3, Lcom/lge/ims/provider/ac/AC$Provisioning$Subscriber;->CONTENT_URI:Landroid/net/Uri;

    #@fc
    invoke-virtual {v0, v3, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_ff
    .catch Ljava/lang/Exception; {:try_start_fa .. :try_end_ff} :catch_c6

    #@ff
    goto/16 :goto_4
.end method

.method private static updateSubscriberForIMS(Landroid/content/Context;Lcom/lge/ims/service/ac/content/ACSubscriber;Lcom/lge/ims/service/ac/content/ACProxyAddress;)I
    .registers 16
    .parameter "context"
    .parameter "subscriber"
    .parameter "proxyAddress"

    #@0
    .prologue
    const/4 v12, 0x2

    #@1
    const/4 v11, 0x1

    #@2
    const/4 v6, 0x0

    #@3
    .line 1293
    if-nez p1, :cond_8

    #@5
    if-nez p2, :cond_8

    #@7
    .line 1460
    :goto_7
    return v6

    #@8
    .line 1297
    :cond_8
    new-instance v2, Landroid/content/ContentValues;

    #@a
    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    #@d
    .line 1299
    .local v2, cv:Landroid/content/ContentValues;
    if-eqz p1, :cond_101

    #@f
    invoke-virtual {p1}, Lcom/lge/ims/service/ac/content/ACSubscriber;->isContentPresent()Z

    #@12
    move-result v9

    #@13
    if-eqz v9, :cond_101

    #@15
    .line 1301
    iget-object v9, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPI:Ljava/lang/String;

    #@17
    if-eqz v9, :cond_20

    #@19
    .line 1302
    const-string v9, "subscriber_0_impi"

    #@1b
    iget-object v10, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPI:Ljava/lang/String;

    #@1d
    invoke-virtual {v2, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@20
    .line 1306
    :cond_20
    iget-object v9, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPUs:Ljava/util/ArrayList;

    #@22
    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    #@25
    move-result v9

    #@26
    if-nez v9, :cond_5f

    #@28
    .line 1307
    const-string v10, "subscriber_0_impu_0"

    #@2a
    iget-object v9, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPUs:Ljava/util/ArrayList;

    #@2c
    invoke-virtual {v9, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2f
    move-result-object v9

    #@30
    check-cast v9, Ljava/lang/String;

    #@32
    invoke-virtual {v2, v10, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@35
    .line 1309
    iget-object v9, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPUs:Ljava/util/ArrayList;

    #@37
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@3a
    move-result v9

    #@3b
    if-le v9, v11, :cond_1a5

    #@3d
    .line 1310
    const-string v10, "subscriber_0_impu_1"

    #@3f
    iget-object v9, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPUs:Ljava/util/ArrayList;

    #@41
    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@44
    move-result-object v9

    #@45
    check-cast v9, Ljava/lang/String;

    #@47
    invoke-virtual {v2, v10, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@4a
    .line 1315
    :goto_4a
    iget-object v9, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPUs:Ljava/util/ArrayList;

    #@4c
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@4f
    move-result v9

    #@50
    if-le v9, v12, :cond_1ae

    #@52
    .line 1316
    const-string v10, "subscriber_0_impu_2"

    #@54
    iget-object v9, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPUs:Ljava/util/ArrayList;

    #@56
    invoke-virtual {v9, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@59
    move-result-object v9

    #@5a
    check-cast v9, Ljava/lang/String;

    #@5c
    invoke-virtual {v2, v10, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@5f
    .line 1323
    :cond_5f
    :goto_5f
    iget-object v9, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mHomeDomainName:Ljava/lang/String;

    #@61
    if-eqz v9, :cond_71

    #@63
    .line 1324
    const-string v9, "subscriber_0_home_domain_name"

    #@65
    iget-object v10, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mHomeDomainName:Ljava/lang/String;

    #@67
    invoke-virtual {v2, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@6a
    .line 1325
    const-string v9, "subscriber_0_server_scscf"

    #@6c
    iget-object v10, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mHomeDomainName:Ljava/lang/String;

    #@6e
    invoke-virtual {v2, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@71
    .line 1329
    :cond_71
    const/4 v4, 0x1

    #@72
    .line 1331
    .local v4, isDigest:Z
    iget-object v9, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthType:Ljava/lang/String;

    #@74
    if-eqz v9, :cond_81

    #@76
    .line 1332
    iget-object v9, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthType:Ljava/lang/String;

    #@78
    const-string v10, "Digest"

    #@7a
    invoke-virtual {v9, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@7d
    move-result v9

    #@7e
    if-nez v9, :cond_81

    #@80
    .line 1333
    const/4 v4, 0x0

    #@81
    .line 1338
    :cond_81
    sget-object v9, Lcom/lge/ims/Configuration;->MODEL:Ljava/lang/String;

    #@83
    const-string v10, "F180"

    #@85
    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@88
    move-result v9

    #@89
    if-nez v9, :cond_9d

    #@8b
    sget-object v9, Lcom/lge/ims/Configuration;->MODEL:Ljava/lang/String;

    #@8d
    const-string v10, "F200"

    #@8f
    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@92
    move-result v9

    #@93
    if-nez v9, :cond_9d

    #@95
    .line 1339
    const-string v9, "ACContentHelper"

    #@97
    const-string v10, "Digest is off, IMS AKA is on"

    #@99
    invoke-static {v9, v10}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@9c
    .line 1340
    const/4 v4, 0x0

    #@9d
    .line 1343
    :cond_9d
    if-eqz v4, :cond_1c0

    #@9f
    .line 1345
    iget-object v9, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthUsername:Ljava/lang/String;

    #@a1
    if-eqz v9, :cond_aa

    #@a3
    .line 1346
    const-string v9, "subscriber_0_auth_username"

    #@a5
    iget-object v10, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthUsername:Ljava/lang/String;

    #@a7
    invoke-virtual {v2, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@aa
    .line 1350
    :cond_aa
    iget-object v9, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthPassword:Ljava/lang/String;

    #@ac
    if-eqz v9, :cond_ef

    #@ae
    .line 1351
    sget-object v9, Lcom/lge/ims/Configuration;->OPERATOR:Ljava/lang/String;

    #@b0
    const-string v10, "KT"

    #@b2
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b5
    move-result v9

    #@b6
    if-eqz v9, :cond_1b7

    #@b8
    .line 1352
    iget-object v5, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthPassword:Ljava/lang/String;

    #@ba
    .line 1354
    .local v5, pwd:Ljava/lang/String;
    const-string v9, "MD5"

    #@bc
    invoke-static {v9, v5}, Lcom/lge/ims/service/ac/ACAuthHelper;->calculateMessageDigest(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@bf
    move-result-object v5

    #@c0
    .line 1356
    if-eqz v5, :cond_c6

    #@c2
    .line 1357
    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@c5
    move-result-object v5

    #@c6
    .line 1360
    :cond_c6
    const-string v9, "ACContentHelper"

    #@c8
    new-instance v10, Ljava/lang/StringBuilder;

    #@ca
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@cd
    const-string v11, "Password :: "

    #@cf
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v10

    #@d3
    iget-object v11, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthPassword:Ljava/lang/String;

    #@d5
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v10

    #@d9
    const-string v11, " >> "

    #@db
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@de
    move-result-object v10

    #@df
    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v10

    #@e3
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e6
    move-result-object v10

    #@e7
    invoke-static {v9, v10}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@ea
    .line 1363
    const-string v9, "subscriber_0_auth_password"

    #@ec
    invoke-virtual {v2, v9, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@ef
    .line 1370
    .end local v5           #pwd:Ljava/lang/String;
    :cond_ef
    :goto_ef
    iget-object v9, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthRealm:Ljava/lang/String;

    #@f1
    if-eqz v9, :cond_fa

    #@f3
    .line 1371
    const-string v9, "subscriber_0_auth_realm"

    #@f5
    iget-object v10, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthRealm:Ljava/lang/String;

    #@f7
    invoke-virtual {v2, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@fa
    .line 1375
    :cond_fa
    const-string v9, "subscriber_0_auth_algorithm"

    #@fc
    const-string v10, "MD5"

    #@fe
    invoke-virtual {v2, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@101
    .line 1396
    .end local v4           #isDigest:Z
    :cond_101
    :goto_101
    if-eqz p2, :cond_196

    #@103
    invoke-virtual {p2}, Lcom/lge/ims/service/ac/content/ACProxyAddress;->isContentPresent()Z

    #@106
    move-result v9

    #@107
    if-eqz v9, :cond_196

    #@109
    invoke-static {}, Lcom/lge/ims/service/ac/ACConfiguration;->isProxyAddressUpdateRequired()Z

    #@10c
    move-result v9

    #@10d
    if-eqz v9, :cond_196

    #@10f
    .line 1398
    iget-object v9, p2, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCAddress:Ljava/lang/String;

    #@111
    if-eqz v9, :cond_14d

    #@113
    .line 1399
    const/4 v7, 0x0

    #@114
    .line 1400
    .local v7, url:Ljava/net/URL;
    new-instance v9, Ljava/lang/StringBuilder;

    #@116
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@119
    const-string v10, "http://"

    #@11b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11e
    move-result-object v9

    #@11f
    iget-object v10, p2, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCAddress:Ljava/lang/String;

    #@121
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@124
    move-result-object v9

    #@125
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@128
    move-result-object v0

    #@129
    .line 1403
    .local v0, address:Ljava/lang/String;
    :try_start_129
    new-instance v8, Ljava/net/URL;

    #@12b
    invoke-direct {v8, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_12e
    .catch Ljava/net/MalformedURLException; {:try_start_129 .. :try_end_12e} :catch_1fc

    #@12e
    .end local v7           #url:Ljava/net/URL;
    .local v8, url:Ljava/net/URL;
    move-object v7, v8

    #@12f
    .line 1409
    .end local v8           #url:Ljava/net/URL;
    .restart local v7       #url:Ljava/net/URL;
    :goto_12f
    if-eqz v7, :cond_14d

    #@131
    .line 1410
    const-string v9, "server_pcscf_1_address"

    #@133
    invoke-virtual {v7}, Ljava/net/URL;->getHost()Ljava/lang/String;

    #@136
    move-result-object v10

    #@137
    invoke-virtual {v2, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@13a
    .line 1412
    invoke-virtual {v7}, Ljava/net/URL;->getPort()I

    #@13d
    move-result v9

    #@13e
    if-lez v9, :cond_14d

    #@140
    .line 1413
    const-string v9, "server_pcscf_1_port"

    #@142
    invoke-virtual {v7}, Ljava/net/URL;->getPort()I

    #@145
    move-result v10

    #@146
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@149
    move-result-object v10

    #@14a
    invoke-virtual {v2, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@14d
    .line 1418
    .end local v0           #address:Ljava/lang/String;
    .end local v7           #url:Ljava/net/URL;
    :cond_14d
    iget-object v9, p2, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCTLSAddress:Ljava/lang/String;

    #@14f
    if-eqz v9, :cond_158

    #@151
    .line 1420
    const-string v9, "ACContentHelper"

    #@153
    const-string v10, "SBC_TLS_Address is not allowed"

    #@155
    invoke-static {v9, v10}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@158
    .line 1423
    :cond_158
    iget-object v9, p2, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mLBOPCSCFAddress:Ljava/lang/String;

    #@15a
    if-eqz v9, :cond_196

    #@15c
    .line 1424
    const/4 v7, 0x0

    #@15d
    .line 1425
    .restart local v7       #url:Ljava/net/URL;
    new-instance v9, Ljava/lang/StringBuilder;

    #@15f
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@162
    const-string v10, "http://"

    #@164
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@167
    move-result-object v9

    #@168
    iget-object v10, p2, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mLBOPCSCFAddress:Ljava/lang/String;

    #@16a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16d
    move-result-object v9

    #@16e
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@171
    move-result-object v0

    #@172
    .line 1428
    .restart local v0       #address:Ljava/lang/String;
    :try_start_172
    new-instance v8, Ljava/net/URL;

    #@174
    invoke-direct {v8, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_177
    .catch Ljava/net/MalformedURLException; {:try_start_172 .. :try_end_177} :catch_21e

    #@177
    .end local v7           #url:Ljava/net/URL;
    .restart local v8       #url:Ljava/net/URL;
    move-object v7, v8

    #@178
    .line 1434
    .end local v8           #url:Ljava/net/URL;
    .restart local v7       #url:Ljava/net/URL;
    :goto_178
    if-eqz v7, :cond_196

    #@17a
    .line 1435
    const-string v9, "server_pcscf_0_address"

    #@17c
    invoke-virtual {v7}, Ljava/net/URL;->getHost()Ljava/lang/String;

    #@17f
    move-result-object v10

    #@180
    invoke-virtual {v2, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@183
    .line 1437
    invoke-virtual {v7}, Ljava/net/URL;->getPort()I

    #@186
    move-result v9

    #@187
    if-lez v9, :cond_196

    #@189
    .line 1438
    const-string v9, "server_pcscf_0_port"

    #@18b
    invoke-virtual {v7}, Ljava/net/URL;->getPort()I

    #@18e
    move-result v10

    #@18f
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@192
    move-result-object v10

    #@193
    invoke-virtual {v2, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@196
    .line 1444
    .end local v0           #address:Ljava/lang/String;
    .end local v7           #url:Ljava/net/URL;
    :cond_196
    invoke-virtual {v2}, Landroid/content/ContentValues;->size()I

    #@199
    move-result v9

    #@19a
    if-nez v9, :cond_240

    #@19c
    .line 1445
    const-string v9, "ACContentHelper"

    #@19e
    const-string v10, "No contents for IMS subscriber"

    #@1a0
    invoke-static {v9, v10}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@1a3
    goto/16 :goto_7

    #@1a5
    .line 1312
    :cond_1a5
    const-string v9, "subscriber_0_impu_1"

    #@1a7
    const-string v10, ""

    #@1a9
    invoke-virtual {v2, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1ac
    goto/16 :goto_4a

    #@1ae
    .line 1318
    :cond_1ae
    const-string v9, "subscriber_0_impu_2"

    #@1b0
    const-string v10, ""

    #@1b2
    invoke-virtual {v2, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1b5
    goto/16 :goto_5f

    #@1b7
    .line 1365
    .restart local v4       #isDigest:Z
    :cond_1b7
    const-string v9, "subscriber_0_auth_password"

    #@1b9
    iget-object v10, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthPassword:Ljava/lang/String;

    #@1bb
    invoke-virtual {v2, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1be
    goto/16 :goto_ef

    #@1c0
    .line 1378
    :cond_1c0
    const-string v9, "subscriber_0_auth_username"

    #@1c2
    const-string v10, ""

    #@1c4
    invoke-virtual {v2, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1c7
    .line 1380
    const-string v9, "subscriber_0_auth_password"

    #@1c9
    const-string v10, ""

    #@1cb
    invoke-virtual {v2, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1ce
    .line 1382
    sget-object v9, Lcom/lge/ims/Configuration;->OPERATOR:Ljava/lang/String;

    #@1d0
    const-string v10, "KT"

    #@1d2
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d5
    move-result v9

    #@1d6
    if-eqz v9, :cond_1f4

    #@1d8
    .line 1383
    iget-object v9, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthRealm:Ljava/lang/String;

    #@1da
    if-eqz v9, :cond_1ec

    #@1dc
    .line 1384
    const-string v9, "subscriber_0_auth_realm"

    #@1de
    iget-object v10, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthRealm:Ljava/lang/String;

    #@1e0
    invoke-virtual {v2, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1e3
    .line 1392
    :goto_1e3
    const-string v9, "subscriber_0_auth_algorithm"

    #@1e5
    const-string v10, "AKAv1-MD5"

    #@1e7
    invoke-virtual {v2, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1ea
    goto/16 :goto_101

    #@1ec
    .line 1386
    :cond_1ec
    const-string v9, "subscriber_0_auth_realm"

    #@1ee
    const-string v10, ""

    #@1f0
    invoke-virtual {v2, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1f3
    goto :goto_1e3

    #@1f4
    .line 1389
    :cond_1f4
    const-string v9, "subscriber_0_auth_realm"

    #@1f6
    const-string v10, ""

    #@1f8
    invoke-virtual {v2, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1fb
    goto :goto_1e3

    #@1fc
    .line 1404
    .end local v4           #isDigest:Z
    .restart local v0       #address:Ljava/lang/String;
    .restart local v7       #url:Ljava/net/URL;
    :catch_1fc
    move-exception v3

    #@1fd
    .line 1405
    .local v3, e:Ljava/net/MalformedURLException;
    const-string v9, "ACContentHelper"

    #@1ff
    new-instance v10, Ljava/lang/StringBuilder;

    #@201
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@204
    const-string v11, "MalformedURLException :: "

    #@206
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@209
    move-result-object v10

    #@20a
    invoke-virtual {v3}, Ljava/net/MalformedURLException;->toString()Ljava/lang/String;

    #@20d
    move-result-object v11

    #@20e
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@211
    move-result-object v10

    #@212
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@215
    move-result-object v10

    #@216
    invoke-static {v9, v10}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@219
    .line 1406
    invoke-virtual {v3}, Ljava/net/MalformedURLException;->printStackTrace()V

    #@21c
    goto/16 :goto_12f

    #@21e
    .line 1429
    .end local v3           #e:Ljava/net/MalformedURLException;
    :catch_21e
    move-exception v3

    #@21f
    .line 1430
    .restart local v3       #e:Ljava/net/MalformedURLException;
    const-string v9, "ACContentHelper"

    #@221
    new-instance v10, Ljava/lang/StringBuilder;

    #@223
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@226
    const-string v11, "MalformedURLException :: "

    #@228
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22b
    move-result-object v10

    #@22c
    invoke-virtual {v3}, Ljava/net/MalformedURLException;->toString()Ljava/lang/String;

    #@22f
    move-result-object v11

    #@230
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@233
    move-result-object v10

    #@234
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@237
    move-result-object v10

    #@238
    invoke-static {v9, v10}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@23b
    .line 1431
    invoke-virtual {v3}, Ljava/net/MalformedURLException;->printStackTrace()V

    #@23e
    goto/16 :goto_178

    #@240
    .line 1449
    .end local v0           #address:Ljava/lang/String;
    .end local v3           #e:Ljava/net/MalformedURLException;
    .end local v7           #url:Ljava/net/URL;
    :cond_240
    const/4 v6, 0x1

    #@241
    .line 1450
    .local v6, updatedTables:I
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@244
    move-result-object v1

    #@245
    .line 1453
    .local v1, cr:Landroid/content/ContentResolver;
    :try_start_245
    sget-object v9, Lcom/lge/ims/provider/IMS$Subscriber;->CONTENT_URI:Landroid/net/Uri;

    #@247
    const/4 v10, 0x0

    #@248
    const/4 v11, 0x0

    #@249
    invoke-virtual {v1, v9, v2, v10, v11}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_24c
    .catch Ljava/lang/Exception; {:try_start_245 .. :try_end_24c} :catch_24e

    #@24c
    goto/16 :goto_7

    #@24e
    .line 1454
    :catch_24e
    move-exception v3

    #@24f
    .line 1455
    .local v3, e:Ljava/lang/Exception;
    const-string v9, "ACContentHelper"

    #@251
    new-instance v10, Ljava/lang/StringBuilder;

    #@253
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@256
    const-string v11, "Exception :: "

    #@258
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25b
    move-result-object v10

    #@25c
    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@25f
    move-result-object v11

    #@260
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@263
    move-result-object v10

    #@264
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@267
    move-result-object v10

    #@268
    invoke-static {v9, v10}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@26b
    .line 1456
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    #@26e
    .line 1457
    const/4 v6, 0x0

    #@26f
    goto/16 :goto_7
.end method

.method public static updateValidityPeriod(Landroid/content/Context;)V
    .registers 13
    .parameter "context"

    #@0
    .prologue
    .line 674
    if-nez p0, :cond_3

    #@2
    .line 708
    :goto_2
    return-void

    #@3
    .line 678
    :cond_3
    invoke-static {p0}, Lcom/lge/ims/service/ac/ACContentHelper;->getReferenceId(Landroid/content/Context;)I

    #@6
    move-result v4

    #@7
    .line 680
    .local v4, refId:I
    if-gez v4, :cond_22

    #@9
    .line 681
    const-string v6, "ACContentHelper"

    #@b
    new-instance v7, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v8, "Invalid refId="

    #@12
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v7

    #@16
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v7

    #@1a
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v7

    #@1e
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@21
    goto :goto_2

    #@22
    .line 685
    :cond_22
    invoke-static {p0}, Lcom/lge/ims/service/ac/ACContentHelper;->getValidityFromVersion(Landroid/content/Context;)Ljava/lang/String;

    #@25
    move-result-object v5

    #@26
    .line 687
    .local v5, validity:Ljava/lang/String;
    if-nez v5, :cond_30

    #@28
    .line 688
    const-string v6, "ACContentHelper"

    #@2a
    const-string v7, "Validity is null"

    #@2c
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@2f
    goto :goto_2

    #@30
    .line 692
    :cond_30
    new-instance v1, Landroid/content/ContentValues;

    #@32
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    #@35
    .line 694
    .local v1, cv:Landroid/content/ContentValues;
    const-string v6, "id"

    #@37
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3a
    move-result-object v7

    #@3b
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@3e
    .line 697
    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@41
    move-result-wide v6

    #@42
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@45
    move-result-wide v8

    #@46
    const-wide/16 v10, 0x3e8

    #@48
    div-long/2addr v8, v10

    #@49
    add-long/2addr v6, v8

    #@4a
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@4d
    move-result-object v3

    #@4e
    .line 698
    .local v3, period:Ljava/lang/Long;
    const-string v6, "validity_period"

    #@50
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@53
    move-result-object v7

    #@54
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@57
    .line 700
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5a
    move-result-object v0

    #@5b
    .line 703
    .local v0, cr:Landroid/content/ContentResolver;
    :try_start_5b
    sget-object v6, Lcom/lge/ims/provider/ac/AC$Provisioning$Version;->CONTENT_URI:Landroid/net/Uri;

    #@5d
    new-instance v7, Ljava/lang/StringBuilder;

    #@5f
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@62
    const-string v8, "id=\'"

    #@64
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v7

    #@68
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6b
    move-result-object v8

    #@6c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v7

    #@70
    const-string v8, "\'"

    #@72
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v7

    #@76
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object v7

    #@7a
    const/4 v8, 0x0

    #@7b
    invoke-virtual {v0, v6, v1, v7, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_7e
    .catch Ljava/lang/Exception; {:try_start_5b .. :try_end_7e} :catch_7f

    #@7e
    goto :goto_2

    #@7f
    .line 704
    :catch_7f
    move-exception v2

    #@80
    .line 705
    .local v2, e:Ljava/lang/Exception;
    const-string v6, "ACContentHelper"

    #@82
    new-instance v7, Ljava/lang/StringBuilder;

    #@84
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@87
    const-string v8, "Exception :: "

    #@89
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v7

    #@8d
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@90
    move-result-object v8

    #@91
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v7

    #@95
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@98
    move-result-object v7

    #@99
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@9c
    .line 706
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    #@9f
    goto/16 :goto_2
.end method

.method private static updateVersionForAC(Landroid/content/Context;Lcom/lge/ims/service/ac/content/ACVersion;IZ)V
    .registers 14
    .parameter "context"
    .parameter "version"
    .parameter "refId"
    .parameter "isUpdate"

    #@0
    .prologue
    .line 1253
    if-nez p1, :cond_3

    #@2
    .line 1289
    :cond_2
    :goto_2
    return-void

    #@3
    .line 1257
    :cond_3
    invoke-virtual {p1}, Lcom/lge/ims/service/ac/content/ACVersion;->isContentPresent()Z

    #@6
    move-result v4

    #@7
    if-eqz v4, :cond_2

    #@9
    .line 1261
    new-instance v1, Landroid/content/ContentValues;

    #@b
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    #@e
    .line 1263
    .local v1, cv:Landroid/content/ContentValues;
    const-string v4, "id"

    #@10
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@13
    move-result-object v5

    #@14
    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@17
    .line 1265
    iget-object v4, p1, Lcom/lge/ims/service/ac/content/ACVersion;->mVersion:Ljava/lang/String;

    #@19
    if-eqz v4, :cond_22

    #@1b
    .line 1266
    const-string v4, "version"

    #@1d
    iget-object v5, p1, Lcom/lge/ims/service/ac/content/ACVersion;->mVersion:Ljava/lang/String;

    #@1f
    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@22
    .line 1269
    :cond_22
    iget-object v4, p1, Lcom/lge/ims/service/ac/content/ACVersion;->mValidity:Ljava/lang/String;

    #@24
    if-eqz v4, :cond_48

    #@26
    .line 1270
    const-string v4, "validity"

    #@28
    iget-object v5, p1, Lcom/lge/ims/service/ac/content/ACVersion;->mValidity:Ljava/lang/String;

    #@2a
    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2d
    .line 1273
    iget-object v4, p1, Lcom/lge/ims/service/ac/content/ACVersion;->mValidity:Ljava/lang/String;

    #@2f
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@32
    move-result-wide v4

    #@33
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@36
    move-result-wide v6

    #@37
    const-wide/16 v8, 0x3e8

    #@39
    div-long/2addr v6, v8

    #@3a
    add-long/2addr v4, v6

    #@3b
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@3e
    move-result-object v3

    #@3f
    .line 1274
    .local v3, period:Ljava/lang/Long;
    const-string v4, "validity_period"

    #@41
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@44
    move-result-object v5

    #@45
    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@48
    .line 1277
    .end local v3           #period:Ljava/lang/Long;
    :cond_48
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4b
    move-result-object v0

    #@4c
    .line 1280
    .local v0, cr:Landroid/content/ContentResolver;
    if-eqz p3, :cond_94

    #@4e
    .line 1281
    :try_start_4e
    sget-object v4, Lcom/lge/ims/provider/ac/AC$Provisioning$Version;->CONTENT_URI:Landroid/net/Uri;

    #@50
    new-instance v5, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v6, "id=\'"

    #@57
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v5

    #@5b
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5e
    move-result-object v6

    #@5f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v5

    #@63
    const-string v6, "\'"

    #@65
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v5

    #@69
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v5

    #@6d
    const/4 v6, 0x0

    #@6e
    invoke-virtual {v0, v4, v1, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_71
    .catch Ljava/lang/Exception; {:try_start_4e .. :try_end_71} :catch_72

    #@71
    goto :goto_2

    #@72
    .line 1285
    :catch_72
    move-exception v2

    #@73
    .line 1286
    .local v2, e:Ljava/lang/Exception;
    const-string v4, "ACContentHelper"

    #@75
    new-instance v5, Ljava/lang/StringBuilder;

    #@77
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@7a
    const-string v6, "Exception :: "

    #@7c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v5

    #@80
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@83
    move-result-object v6

    #@84
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v5

    #@88
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8b
    move-result-object v5

    #@8c
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@8f
    .line 1287
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    #@92
    goto/16 :goto_2

    #@94
    .line 1283
    .end local v2           #e:Ljava/lang/Exception;
    :cond_94
    :try_start_94
    sget-object v4, Lcom/lge/ims/provider/ac/AC$Provisioning$Version;->CONTENT_URI:Landroid/net/Uri;

    #@96
    invoke-virtual {v0, v4, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_99
    .catch Ljava/lang/Exception; {:try_start_94 .. :try_end_99} :catch_72

    #@99
    goto/16 :goto_2
.end method
