.class public Lcom/lge/ims/service/ip/IPProvisioningInfo;
.super Ljava/lang/Object;
.source "IPProvisioningInfo.java"


# static fields
.field private static objProvisioningInfo:Lcom/lge/ims/service/ip/IPProvisioningInfo;


# instance fields
.field private objContext:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 12
    .parameter "_objContext"

    #@0
    .prologue
    .line 41
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 42
    if-nez p1, :cond_b

    #@5
    .line 43
    const-string v0, "[IP][ERROR]IPProvisioningInfo: _objContext is null"

    #@7
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@a
    .line 85
    :cond_a
    :goto_a
    return-void

    #@b
    .line 47
    :cond_b
    iput-object p1, p0, Lcom/lge/ims/service/ip/IPProvisioningInfo;->objContext:Landroid/content/Context;

    #@d
    .line 48
    const/4 v8, 0x0

    #@e
    .line 51
    .local v8, objCursor:Landroid/database/Cursor;
    :try_start_e
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPProvisioningInfo;->objContext:Landroid/content/Context;

    #@10
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@13
    move-result-object v0

    #@14
    sget-object v1, Lcom/lge/ims/service/ip/IPConstant;->IP_PROVISIONING_CONTENT_URI:Landroid/net/Uri;

    #@16
    const/4 v2, 0x1

    #@17
    new-array v2, v2, [Ljava/lang/String;

    #@19
    const/4 v3, 0x0

    #@1a
    const-string v4, "latest_polling"

    #@1c
    aput-object v4, v2, v3

    #@1e
    const/4 v3, 0x0

    #@1f
    const/4 v4, 0x0

    #@20
    const/4 v5, 0x0

    #@21
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@24
    move-result-object v8

    #@25
    .line 52
    if-nez v8, :cond_33

    #@27
    .line 53
    const-string v0, "[IP][ERROR]IPProvisioningInfo: objCursor is null"

    #@29
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_2c
    .catchall {:try_start_e .. :try_end_2c} :catchall_ba
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_2c} :catch_9b

    #@2c
    .line 80
    if-eqz v8, :cond_a

    #@2e
    .line 81
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@31
    .line 82
    const/4 v8, 0x0

    #@32
    goto :goto_a

    #@33
    .line 55
    :cond_33
    :try_start_33
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    #@36
    move-result v0

    #@37
    if-nez v0, :cond_93

    #@39
    .line 56
    new-instance v7, Landroid/content/ContentValues;

    #@3b
    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    #@3e
    .line 57
    .local v7, objContentValues:Landroid/content/ContentValues;
    const-string v0, "latest_polling"

    #@40
    const/4 v1, 0x0

    #@41
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@44
    move-result-object v1

    #@45
    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_48
    .catchall {:try_start_33 .. :try_end_48} :catchall_ba
    .catch Ljava/lang/Exception; {:try_start_33 .. :try_end_48} :catch_9b

    #@48
    .line 58
    const/4 v9, 0x0

    #@49
    .line 61
    .local v9, objUri:Landroid/net/Uri;
    :try_start_49
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPProvisioningInfo;->objContext:Landroid/content/Context;

    #@4b
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4e
    move-result-object v0

    #@4f
    sget-object v1, Lcom/lge/ims/service/ip/IPConstant;->IP_PROVISIONING_CONTENT_URI:Landroid/net/Uri;

    #@51
    invoke-virtual {v0, v1, v7}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_54
    .catchall {:try_start_49 .. :try_end_54} :catchall_ba
    .catch Landroid/database/SQLException; {:try_start_49 .. :try_end_54} :catch_66
    .catch Ljava/lang/Exception; {:try_start_49 .. :try_end_54} :catch_9b

    #@54
    move-result-object v9

    #@55
    .line 69
    :try_start_55
    invoke-virtual {v7}, Landroid/content/ContentValues;->clear()V

    #@58
    .line 71
    if-nez v9, :cond_79

    #@5a
    .line 72
    const-string v0, "[IP][ERROR]IPProvisioningInfo : objUri is null"

    #@5c
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_5f
    .catchall {:try_start_55 .. :try_end_5f} :catchall_ba
    .catch Ljava/lang/Exception; {:try_start_55 .. :try_end_5f} :catch_9b

    #@5f
    .line 80
    if-eqz v8, :cond_a

    #@61
    .line 81
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@64
    .line 82
    const/4 v8, 0x0

    #@65
    goto :goto_a

    #@66
    .line 62
    :catch_66
    move-exception v6

    #@67
    .line 63
    .local v6, e:Landroid/database/SQLException;
    :try_start_67
    invoke-virtual {v7}, Landroid/content/ContentValues;->clear()V

    #@6a
    .line 64
    const-string v0, "[IP][ERROR]IPProvisioningInfo : Can`t not insert polling timer to provisioning DB"

    #@6c
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@6f
    .line 65
    invoke-virtual {v6}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_72
    .catchall {:try_start_67 .. :try_end_72} :catchall_ba
    .catch Ljava/lang/Exception; {:try_start_67 .. :try_end_72} :catch_9b

    #@72
    .line 80
    if-eqz v8, :cond_a

    #@74
    .line 81
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@77
    .line 82
    const/4 v8, 0x0

    #@78
    goto :goto_a

    #@79
    .line 75
    .end local v6           #e:Landroid/database/SQLException;
    :cond_79
    :try_start_79
    new-instance v0, Ljava/lang/StringBuilder;

    #@7b
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@7e
    const-string v1, "- "

    #@80
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v0

    #@84
    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@87
    move-result-object v1

    #@88
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v0

    #@8c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8f
    move-result-object v0

    #@90
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V
    :try_end_93
    .catchall {:try_start_79 .. :try_end_93} :catchall_ba
    .catch Ljava/lang/Exception; {:try_start_79 .. :try_end_93} :catch_9b

    #@93
    .line 80
    .end local v7           #objContentValues:Landroid/content/ContentValues;
    .end local v9           #objUri:Landroid/net/Uri;
    :cond_93
    if-eqz v8, :cond_a

    #@95
    .line 81
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@98
    .line 82
    const/4 v8, 0x0

    #@99
    goto/16 :goto_a

    #@9b
    .line 77
    :catch_9b
    move-exception v6

    #@9c
    .line 78
    .local v6, e:Ljava/lang/Exception;
    :try_start_9c
    new-instance v0, Ljava/lang/StringBuilder;

    #@9e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@a1
    const-string v1, "[IP][ERROR]exception : "

    #@a3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v0

    #@a7
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v0

    #@ab
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ae
    move-result-object v0

    #@af
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_b2
    .catchall {:try_start_9c .. :try_end_b2} :catchall_ba

    #@b2
    .line 80
    if-eqz v8, :cond_a

    #@b4
    .line 81
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@b7
    .line 82
    const/4 v8, 0x0

    #@b8
    goto/16 :goto_a

    #@ba
    .line 80
    .end local v6           #e:Ljava/lang/Exception;
    :catchall_ba
    move-exception v0

    #@bb
    if-eqz v8, :cond_c1

    #@bd
    .line 81
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@c0
    .line 82
    const/4 v8, 0x0

    #@c1
    :cond_c1
    throw v0
.end method

.method public static CreateProvisioningInfo(Landroid/content/Context;)V
    .registers 2
    .parameter "objContext"

    #@0
    .prologue
    .line 99
    sget-object v0, Lcom/lge/ims/service/ip/IPProvisioningInfo;->objProvisioningInfo:Lcom/lge/ims/service/ip/IPProvisioningInfo;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 100
    new-instance v0, Lcom/lge/ims/service/ip/IPProvisioningInfo;

    #@6
    invoke-direct {v0, p0}, Lcom/lge/ims/service/ip/IPProvisioningInfo;-><init>(Landroid/content/Context;)V

    #@9
    sput-object v0, Lcom/lge/ims/service/ip/IPProvisioningInfo;->objProvisioningInfo:Lcom/lge/ims/service/ip/IPProvisioningInfo;

    #@b
    .line 102
    :cond_b
    return-void
.end method

.method public static DestroyProvisioningInfo()V
    .registers 1

    #@0
    .prologue
    .line 130
    const/4 v0, 0x0

    #@1
    sput-object v0, Lcom/lge/ims/service/ip/IPProvisioningInfo;->objProvisioningInfo:Lcom/lge/ims/service/ip/IPProvisioningInfo;

    #@3
    .line 131
    return-void
.end method

.method public static GetInstance(Landroid/content/Context;)Lcom/lge/ims/service/ip/IPProvisioningInfo;
    .registers 2
    .parameter "objContext"

    #@0
    .prologue
    .line 116
    const-string v0, "[IP]IPProvisioningInfo:GetInstance()"

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 118
    sget-object v0, Lcom/lge/ims/service/ip/IPProvisioningInfo;->objProvisioningInfo:Lcom/lge/ims/service/ip/IPProvisioningInfo;

    #@7
    if-nez v0, :cond_c

    #@9
    .line 119
    invoke-static {p0}, Lcom/lge/ims/service/ip/IPProvisioningInfo;->CreateProvisioningInfo(Landroid/content/Context;)V

    #@c
    .line 122
    :cond_c
    sget-object v0, Lcom/lge/ims/service/ip/IPProvisioningInfo;->objProvisioningInfo:Lcom/lge/ims/service/ip/IPProvisioningInfo;

    #@e
    return-object v0
.end method
