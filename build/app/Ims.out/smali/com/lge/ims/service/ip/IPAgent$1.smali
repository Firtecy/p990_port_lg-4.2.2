.class Lcom/lge/ims/service/ip/IPAgent$1;
.super Landroid/os/Handler;
.source "IPAgent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lge/ims/service/ip/IPAgent;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/service/ip/IPAgent;


# direct methods
.method constructor <init>(Lcom/lge/ims/service/ip/IPAgent;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 202
    iput-object p1, p0, Lcom/lge/ims/service/ip/IPAgent$1;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 12
    .parameter "objMessage"

    #@0
    .prologue
    .line 205
    if-nez p1, :cond_8

    #@2
    .line 206
    const-string v7, "[IP][ERROR]IPAgent : Message is null"

    #@4
    invoke-static {v7}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@7
    .line 325
    :cond_7
    :goto_7
    return-void

    #@8
    .line 209
    :cond_8
    new-instance v7, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v8, "[IP]Message["

    #@f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v7

    #@13
    iget-object v8, p0, Lcom/lge/ims/service/ip/IPAgent$1;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@15
    iget v9, p1, Landroid/os/Message;->what:I

    #@17
    invoke-static {v8, v9}, Lcom/lge/ims/service/ip/IPAgent;->access$000(Lcom/lge/ims/service/ip/IPAgent;I)Ljava/lang/String;

    #@1a
    move-result-object v8

    #@1b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v7

    #@1f
    const-string v8, "]"

    #@21
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v7

    #@25
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v7

    #@29
    invoke-static {v7}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@2c
    .line 210
    new-instance v7, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v8, "[IP]State["

    #@33
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v7

    #@37
    iget-object v8, p0, Lcom/lge/ims/service/ip/IPAgent$1;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@39
    iget-object v9, p0, Lcom/lge/ims/service/ip/IPAgent$1;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@3b
    invoke-static {v9}, Lcom/lge/ims/service/ip/IPAgent;->access$100(Lcom/lge/ims/service/ip/IPAgent;)I

    #@3e
    move-result v9

    #@3f
    invoke-static {v8, v9}, Lcom/lge/ims/service/ip/IPAgent;->access$200(Lcom/lge/ims/service/ip/IPAgent;I)Ljava/lang/String;

    #@42
    move-result-object v8

    #@43
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v7

    #@47
    const-string v8, "]"

    #@49
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v7

    #@4d
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v7

    #@51
    invoke-static {v7}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@54
    .line 212
    iget v7, p1, Landroid/os/Message;->what:I

    #@56
    const/16 v8, 0x28

    #@58
    if-lt v7, v8, :cond_159

    #@5a
    .line 213
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@5d
    move-result-object v2

    #@5e
    .line 214
    .local v2, objBundle:Landroid/os/Bundle;
    iget v7, p1, Landroid/os/Message;->what:I

    #@60
    packed-switch v7, :pswitch_data_19e

    #@63
    :pswitch_63
    goto :goto_7

    #@64
    .line 217
    :pswitch_64
    const-string v7, "rls_list_uri"

    #@66
    invoke-virtual {v2, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@69
    move-result-object v6

    #@6a
    .line 218
    .local v6, szRLSListUri:Ljava/lang/String;
    if-eqz v6, :cond_7

    #@6c
    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    #@6f
    move-result v7

    #@70
    if-nez v7, :cond_7

    #@72
    .line 219
    iget-object v7, p0, Lcom/lge/ims/service/ip/IPAgent$1;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@74
    invoke-static {v7}, Lcom/lge/ims/service/ip/IPAgent;->access$300(Lcom/lge/ims/service/ip/IPAgent;)Lcom/lge/ims/service/ip/IPContactHelper;

    #@77
    move-result-object v7

    #@78
    invoke-virtual {v7, v6}, Lcom/lge/ims/service/ip/IPContactHelper;->UpdateRLSListUri(Ljava/lang/String;)Z

    #@7b
    goto :goto_7

    #@7c
    .line 225
    .end local v6           #szRLSListUri:Ljava/lang/String;
    :pswitch_7c
    const-string v7, "my_status"

    #@7e
    invoke-virtual {v2, v7}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@81
    move-result-object v3

    #@82
    check-cast v3, Lcom/lge/ims/service/ip/MyStatusInfo;

    #@84
    .line 226
    .local v3, objMyStatusInfo:Lcom/lge/ims/service/ip/MyStatusInfo;
    if-eqz v3, :cond_7

    #@86
    .line 227
    iget-object v7, p0, Lcom/lge/ims/service/ip/IPAgent$1;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@88
    invoke-static {v7}, Lcom/lge/ims/service/ip/IPAgent;->access$300(Lcom/lge/ims/service/ip/IPAgent;)Lcom/lge/ims/service/ip/IPContactHelper;

    #@8b
    move-result-object v7

    #@8c
    invoke-virtual {v7, v3}, Lcom/lge/ims/service/ip/IPContactHelper;->UpdateMyStatusInfo(Lcom/lge/ims/service/ip/MyStatusInfo;)Z

    #@8f
    goto/16 :goto_7

    #@91
    .line 233
    .end local v3           #objMyStatusInfo:Lcom/lge/ims/service/ip/MyStatusInfo;
    :pswitch_91
    const-string v7, "my_status"

    #@93
    invoke-virtual {v2, v7}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@96
    move-result-object v3

    #@97
    check-cast v3, Lcom/lge/ims/service/ip/MyStatusInfo;

    #@99
    .line 234
    .restart local v3       #objMyStatusInfo:Lcom/lge/ims/service/ip/MyStatusInfo;
    if-eqz v3, :cond_7

    #@9b
    .line 236
    :try_start_9b
    iget-object v7, p0, Lcom/lge/ims/service/ip/IPAgent$1;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@9d
    invoke-static {v7}, Lcom/lge/ims/service/ip/IPAgent;->access$300(Lcom/lge/ims/service/ip/IPAgent;)Lcom/lge/ims/service/ip/IPContactHelper;

    #@a0
    move-result-object v7

    #@a1
    invoke-virtual {v7, v3}, Lcom/lge/ims/service/ip/IPContactHelper;->UpdateMyStatusIconInfo(Lcom/lge/ims/service/ip/MyStatusInfo;)Z
    :try_end_a4
    .catchall {:try_start_9b .. :try_end_a4} :catchall_bf
    .catch Ljava/lang/Exception; {:try_start_9b .. :try_end_a4} :catch_a6

    #@a4
    goto/16 :goto_7

    #@a6
    .line 237
    :catch_a6
    move-exception v0

    #@a7
    .line 238
    .local v0, e:Ljava/lang/Exception;
    :try_start_a7
    new-instance v7, Ljava/lang/StringBuilder;

    #@a9
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@ac
    const-string v8, "[IP][ERROR]exception : "

    #@ae
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v7

    #@b2
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v7

    #@b6
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b9
    move-result-object v7

    #@ba
    invoke-static {v7}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_bd
    .catchall {:try_start_a7 .. :try_end_bd} :catchall_bf

    #@bd
    goto/16 :goto_7

    #@bf
    .line 239
    .end local v0           #e:Ljava/lang/Exception;
    :catchall_bf
    move-exception v7

    #@c0
    throw v7

    #@c1
    .line 246
    .end local v3           #objMyStatusInfo:Lcom/lge/ims/service/ip/MyStatusInfo;
    :pswitch_c1
    const-string v7, "xdm_doc"

    #@c3
    invoke-virtual {v2, v7}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@c6
    move-result-object v4

    #@c7
    check-cast v4, Lcom/lge/ims/service/ip/XDMEtagInfo;

    #@c9
    .line 247
    .local v4, objXdmEtagInfo:Lcom/lge/ims/service/ip/XDMEtagInfo;
    if-eqz v4, :cond_7

    #@cb
    .line 248
    iget-object v7, p0, Lcom/lge/ims/service/ip/IPAgent$1;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@cd
    invoke-static {v7}, Lcom/lge/ims/service/ip/IPAgent;->access$300(Lcom/lge/ims/service/ip/IPAgent;)Lcom/lge/ims/service/ip/IPContactHelper;

    #@d0
    move-result-object v7

    #@d1
    invoke-virtual {v7, v4}, Lcom/lge/ims/service/ip/IPContactHelper;->UpdateDocumentETag(Lcom/lge/ims/service/ip/XDMEtagInfo;)Z

    #@d4
    goto/16 :goto_7

    #@d6
    .line 254
    .end local v4           #objXdmEtagInfo:Lcom/lge/ims/service/ip/XDMEtagInfo;
    :pswitch_d6
    const-string v7, "msisdn"

    #@d8
    invoke-virtual {v2, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@db
    move-result-object v5

    #@dc
    .line 255
    .local v5, szMsisdn:Ljava/lang/String;
    if-eqz v5, :cond_7

    #@de
    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    #@e1
    move-result v7

    #@e2
    if-nez v7, :cond_7

    #@e4
    .line 256
    iget-object v7, p0, Lcom/lge/ims/service/ip/IPAgent$1;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@e6
    invoke-static {v7}, Lcom/lge/ims/service/ip/IPAgent;->access$300(Lcom/lge/ims/service/ip/IPAgent;)Lcom/lge/ims/service/ip/IPContactHelper;

    #@e9
    move-result-object v7

    #@ea
    invoke-virtual {v7, v5}, Lcom/lge/ims/service/ip/IPContactHelper;->DeleteRCSUserList(Ljava/lang/String;)Z

    #@ed
    goto/16 :goto_7

    #@ef
    .line 262
    .end local v5           #szMsisdn:Ljava/lang/String;
    :pswitch_ef
    const-string v7, "msisdn"

    #@f1
    invoke-virtual {v2, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@f4
    move-result-object v5

    #@f5
    .line 263
    .restart local v5       #szMsisdn:Ljava/lang/String;
    if-eqz v5, :cond_7

    #@f7
    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    #@fa
    move-result v7

    #@fb
    if-nez v7, :cond_7

    #@fd
    .line 264
    iget-object v7, p0, Lcom/lge/ims/service/ip/IPAgent$1;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@ff
    invoke-static {v7}, Lcom/lge/ims/service/ip/IPAgent;->access$300(Lcom/lge/ims/service/ip/IPAgent;)Lcom/lge/ims/service/ip/IPContactHelper;

    #@102
    move-result-object v7

    #@103
    invoke-virtual {v7, v5}, Lcom/lge/ims/service/ip/IPContactHelper;->AddRCSUserList(Ljava/lang/String;)Z

    #@106
    goto/16 :goto_7

    #@108
    .line 270
    .end local v5           #szMsisdn:Ljava/lang/String;
    :pswitch_108
    const-string v7, "msisdn"

    #@10a
    invoke-virtual {v2, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@10d
    move-result-object v5

    #@10e
    .line 271
    .restart local v5       #szMsisdn:Ljava/lang/String;
    if-eqz v5, :cond_7

    #@110
    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    #@113
    move-result v7

    #@114
    if-nez v7, :cond_7

    #@116
    .line 272
    iget-object v7, p0, Lcom/lge/ims/service/ip/IPAgent$1;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@118
    invoke-static {v7}, Lcom/lge/ims/service/ip/IPAgent;->access$300(Lcom/lge/ims/service/ip/IPAgent;)Lcom/lge/ims/service/ip/IPContactHelper;

    #@11b
    move-result-object v7

    #@11c
    invoke-virtual {v7, v5}, Lcom/lge/ims/service/ip/IPContactHelper;->AddNonRCSUserList(Ljava/lang/String;)Z

    #@11f
    goto/16 :goto_7

    #@121
    .line 278
    .end local v5           #szMsisdn:Ljava/lang/String;
    :pswitch_121
    const-string v7, "msisdn"

    #@123
    invoke-virtual {v2, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@126
    move-result-object v5

    #@127
    .line 279
    .restart local v5       #szMsisdn:Ljava/lang/String;
    const-string v7, "responseCode"

    #@129
    invoke-virtual {v2, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@12c
    move-result v1

    #@12d
    .line 280
    .local v1, nResponseCode:I
    if-eqz v5, :cond_7

    #@12f
    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    #@132
    move-result v7

    #@133
    if-nez v7, :cond_7

    #@135
    .line 281
    iget-object v7, p0, Lcom/lge/ims/service/ip/IPAgent$1;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@137
    invoke-static {v7}, Lcom/lge/ims/service/ip/IPAgent;->access$300(Lcom/lge/ims/service/ip/IPAgent;)Lcom/lge/ims/service/ip/IPContactHelper;

    #@13a
    move-result-object v7

    #@13b
    invoke-virtual {v7, v5, v1}, Lcom/lge/ims/service/ip/IPContactHelper;->UpdateOneContactQueryFailed(Ljava/lang/String;I)Z

    #@13e
    goto/16 :goto_7

    #@140
    .line 287
    .end local v1           #nResponseCode:I
    .end local v5           #szMsisdn:Ljava/lang/String;
    :pswitch_140
    const-string v7, "msisdn"

    #@142
    invoke-virtual {v2, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@145
    move-result-object v5

    #@146
    .line 288
    .restart local v5       #szMsisdn:Ljava/lang/String;
    if-eqz v5, :cond_7

    #@148
    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    #@14b
    move-result v7

    #@14c
    if-nez v7, :cond_7

    #@14e
    .line 289
    iget-object v7, p0, Lcom/lge/ims/service/ip/IPAgent$1;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@150
    invoke-static {v7}, Lcom/lge/ims/service/ip/IPAgent;->access$300(Lcom/lge/ims/service/ip/IPAgent;)Lcom/lge/ims/service/ip/IPContactHelper;

    #@153
    move-result-object v7

    #@154
    invoke-virtual {v7, v5}, Lcom/lge/ims/service/ip/IPContactHelper;->UpdateOneContactQuerySuccess(Ljava/lang/String;)Z

    #@157
    goto/16 :goto_7

    #@159
    .line 297
    .end local v2           #objBundle:Landroid/os/Bundle;
    .end local v5           #szMsisdn:Ljava/lang/String;
    :cond_159
    iget-object v7, p0, Lcom/lge/ims/service/ip/IPAgent$1;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@15b
    invoke-static {v7}, Lcom/lge/ims/service/ip/IPAgent;->access$100(Lcom/lge/ims/service/ip/IPAgent;)I

    #@15e
    move-result v7

    #@15f
    packed-switch v7, :pswitch_data_1b6

    #@162
    .line 320
    new-instance v7, Ljava/lang/StringBuilder;

    #@164
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@167
    const-string v8, "[IP] --- invalid Message("

    #@169
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16c
    move-result-object v7

    #@16d
    iget v8, p1, Landroid/os/Message;->what:I

    #@16f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@172
    move-result-object v7

    #@173
    const-string v8, ")"

    #@175
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@178
    move-result-object v7

    #@179
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17c
    move-result-object v7

    #@17d
    invoke-static {v7}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@180
    goto/16 :goto_7

    #@182
    .line 300
    :pswitch_182
    iget-object v7, p0, Lcom/lge/ims/service/ip/IPAgent$1;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@184
    invoke-static {v7, p1}, Lcom/lge/ims/service/ip/IPAgent;->access$400(Lcom/lge/ims/service/ip/IPAgent;Landroid/os/Message;)Z

    #@187
    goto/16 :goto_7

    #@189
    .line 305
    :pswitch_189
    iget-object v7, p0, Lcom/lge/ims/service/ip/IPAgent$1;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@18b
    invoke-static {v7, p1}, Lcom/lge/ims/service/ip/IPAgent;->access$500(Lcom/lge/ims/service/ip/IPAgent;Landroid/os/Message;)Z

    #@18e
    goto/16 :goto_7

    #@190
    .line 310
    :pswitch_190
    iget-object v7, p0, Lcom/lge/ims/service/ip/IPAgent$1;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@192
    invoke-static {v7, p1}, Lcom/lge/ims/service/ip/IPAgent;->access$600(Lcom/lge/ims/service/ip/IPAgent;Landroid/os/Message;)Z

    #@195
    goto/16 :goto_7

    #@197
    .line 315
    :pswitch_197
    iget-object v7, p0, Lcom/lge/ims/service/ip/IPAgent$1;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@199
    invoke-static {v7, p1}, Lcom/lge/ims/service/ip/IPAgent;->access$700(Lcom/lge/ims/service/ip/IPAgent;Landroid/os/Message;)Z

    #@19c
    goto/16 :goto_7

    #@19e
    .line 214
    :pswitch_data_19e
    .packed-switch 0x28
        :pswitch_64
        :pswitch_7c
        :pswitch_91
        :pswitch_c1
        :pswitch_63
        :pswitch_d6
        :pswitch_ef
        :pswitch_108
        :pswitch_121
        :pswitch_140
    .end packed-switch

    #@1b6
    .line 297
    :pswitch_data_1b6
    .packed-switch 0x0
        :pswitch_182
        :pswitch_189
        :pswitch_190
        :pswitch_197
    .end packed-switch
.end method
