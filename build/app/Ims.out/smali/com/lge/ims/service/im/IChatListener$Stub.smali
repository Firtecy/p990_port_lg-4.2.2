.class public abstract Lcom/lge/ims/service/im/IChatListener$Stub;
.super Landroid/os/Binder;
.source "IChatListener.java"

# interfaces
.implements Lcom/lge/ims/service/im/IChatListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/im/IChatListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/im/IChatListener$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.lge.ims.service.im.IChatListener"

.field static final TRANSACTION_addParticipantsRequestDelivered:I = 0x9

.field static final TRANSACTION_addParticipantsRequestFailed:I = 0xa

.field static final TRANSACTION_chatAlerting:I = 0x1

.field static final TRANSACTION_chatExtended:I = 0xb

.field static final TRANSACTION_chatExtensionFailed:I = 0xc

.field static final TRANSACTION_chatLeaveFailed:I = 0x7

.field static final TRANSACTION_chatLeft:I = 0x6

.field static final TRANSACTION_chatRejoinFailed:I = 0x5

.field static final TRANSACTION_chatRejoined:I = 0x4

.field static final TRANSACTION_chatStartFailed:I = 0x3

.field static final TRANSACTION_chatStarted:I = 0x2

.field static final TRANSACTION_chatTerminated:I = 0x8

.field static final TRANSACTION_composingIndicatorReceived:I = 0x12

.field static final TRANSACTION_messageReceived:I = 0x11

.field static final TRANSACTION_messageSendFailed:I = 0x10

.field static final TRANSACTION_messageSent:I = 0xf

.field static final TRANSACTION_participantJoined:I = 0xd

.field static final TRANSACTION_participantLeft:I = 0xe


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 15
    const-string v0, "com.lge.ims.service.im.IChatListener"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/lge/ims/service/im/IChatListener$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/im/IChatListener;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 23
    if-nez p0, :cond_4

    #@2
    .line 24
    const/4 v0, 0x0

    #@3
    .line 30
    :goto_3
    return-object v0

    #@4
    .line 26
    :cond_4
    const-string v1, "com.lge.ims.service.im.IChatListener"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 27
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/lge/ims/service/im/IChatListener;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 28
    check-cast v0, Lcom/lge/ims/service/im/IChatListener;

    #@12
    goto :goto_3

    #@13
    .line 30
    :cond_13
    new-instance v0, Lcom/lge/ims/service/im/IChatListener$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/lge/ims/service/im/IChatListener$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 11
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 38
    sparse-switch p1, :sswitch_data_156

    #@4
    .line 219
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v4

    #@8
    :goto_8
    return v4

    #@9
    .line 42
    :sswitch_9
    const-string v5, "com.lge.ims.service.im.IChatListener"

    #@b
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 47
    :sswitch_f
    const-string v5, "com.lge.ims.service.im.IChatListener"

    #@11
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@17
    move-result-object v0

    #@18
    .line 50
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/im/IChatListener$Stub;->chatAlerting(Ljava/lang/String;)V

    #@1b
    .line 51
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1e
    goto :goto_8

    #@1f
    .line 56
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_1f
    const-string v5, "com.lge.ims.service.im.IChatListener"

    #@21
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@24
    .line 58
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@27
    move-result-object v0

    #@28
    .line 59
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/im/IChatListener$Stub;->chatStarted(Ljava/lang/String;)V

    #@2b
    .line 60
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2e
    goto :goto_8

    #@2f
    .line 65
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_2f
    const-string v5, "com.lge.ims.service.im.IChatListener"

    #@31
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@34
    .line 67
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@37
    move-result v0

    #@38
    .line 68
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/im/IChatListener$Stub;->chatStartFailed(I)V

    #@3b
    .line 69
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3e
    goto :goto_8

    #@3f
    .line 74
    .end local v0           #_arg0:I
    :sswitch_3f
    const-string v5, "com.lge.ims.service.im.IChatListener"

    #@41
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@44
    .line 75
    invoke-virtual {p0}, Lcom/lge/ims/service/im/IChatListener$Stub;->chatRejoined()V

    #@47
    .line 76
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4a
    goto :goto_8

    #@4b
    .line 81
    :sswitch_4b
    const-string v5, "com.lge.ims.service.im.IChatListener"

    #@4d
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@50
    .line 83
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@53
    move-result v0

    #@54
    .line 84
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/im/IChatListener$Stub;->chatRejoinFailed(I)V

    #@57
    .line 85
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5a
    goto :goto_8

    #@5b
    .line 90
    .end local v0           #_arg0:I
    :sswitch_5b
    const-string v5, "com.lge.ims.service.im.IChatListener"

    #@5d
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@60
    .line 91
    invoke-virtual {p0}, Lcom/lge/ims/service/im/IChatListener$Stub;->chatLeft()V

    #@63
    .line 92
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@66
    goto :goto_8

    #@67
    .line 97
    :sswitch_67
    const-string v5, "com.lge.ims.service.im.IChatListener"

    #@69
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6c
    .line 99
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@6f
    move-result v0

    #@70
    .line 100
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/im/IChatListener$Stub;->chatLeaveFailed(I)V

    #@73
    .line 101
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@76
    goto :goto_8

    #@77
    .line 106
    .end local v0           #_arg0:I
    :sswitch_77
    const-string v5, "com.lge.ims.service.im.IChatListener"

    #@79
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@7c
    .line 108
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@7f
    move-result v0

    #@80
    .line 109
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/im/IChatListener$Stub;->chatTerminated(I)V

    #@83
    .line 110
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@86
    goto :goto_8

    #@87
    .line 115
    .end local v0           #_arg0:I
    :sswitch_87
    const-string v5, "com.lge.ims.service.im.IChatListener"

    #@89
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@8c
    .line 116
    invoke-virtual {p0}, Lcom/lge/ims/service/im/IChatListener$Stub;->addParticipantsRequestDelivered()V

    #@8f
    .line 117
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@92
    goto/16 :goto_8

    #@94
    .line 122
    :sswitch_94
    const-string v5, "com.lge.ims.service.im.IChatListener"

    #@96
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@99
    .line 124
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@9c
    move-result v0

    #@9d
    .line 125
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/im/IChatListener$Stub;->addParticipantsRequestFailed(I)V

    #@a0
    .line 126
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@a3
    goto/16 :goto_8

    #@a5
    .line 131
    .end local v0           #_arg0:I
    :sswitch_a5
    const-string v5, "com.lge.ims.service.im.IChatListener"

    #@a7
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@aa
    .line 133
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@ad
    move-result-object v0

    #@ae
    .line 135
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@b1
    move-result-object v1

    #@b2
    .line 136
    .local v1, _arg1:Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/service/im/IChatListener$Stub;->chatExtended(Ljava/lang/String;Ljava/lang/String;)V

    #@b5
    .line 137
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@b8
    goto/16 :goto_8

    #@ba
    .line 142
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:Ljava/lang/String;
    :sswitch_ba
    const-string v5, "com.lge.ims.service.im.IChatListener"

    #@bc
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@bf
    .line 144
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@c2
    move-result v0

    #@c3
    .line 145
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/im/IChatListener$Stub;->chatExtensionFailed(I)V

    #@c6
    .line 146
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@c9
    goto/16 :goto_8

    #@cb
    .line 151
    .end local v0           #_arg0:I
    :sswitch_cb
    const-string v5, "com.lge.ims.service.im.IChatListener"

    #@cd
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d0
    .line 153
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@d3
    move-result-object v0

    #@d4
    .line 154
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/im/IChatListener$Stub;->participantJoined(Ljava/lang/String;)V

    #@d7
    .line 155
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@da
    goto/16 :goto_8

    #@dc
    .line 160
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_dc
    const-string v5, "com.lge.ims.service.im.IChatListener"

    #@de
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@e1
    .line 162
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@e4
    move-result-object v0

    #@e5
    .line 163
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/im/IChatListener$Stub;->participantLeft(Ljava/lang/String;)V

    #@e8
    .line 164
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@eb
    goto/16 :goto_8

    #@ed
    .line 169
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_ed
    const-string v5, "com.lge.ims.service.im.IChatListener"

    #@ef
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f2
    .line 171
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@f5
    move-result-object v0

    #@f6
    .line 172
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/im/IChatListener$Stub;->messageSent(Ljava/lang/String;)V

    #@f9
    .line 173
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@fc
    goto/16 :goto_8

    #@fe
    .line 178
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_fe
    const-string v5, "com.lge.ims.service.im.IChatListener"

    #@100
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@103
    .line 180
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@106
    move-result-object v0

    #@107
    .line 182
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@10a
    move-result v1

    #@10b
    .line 183
    .local v1, _arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/service/im/IChatListener$Stub;->messageSendFailed(Ljava/lang/String;I)V

    #@10e
    .line 184
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@111
    goto/16 :goto_8

    #@113
    .line 189
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:I
    :sswitch_113
    const-string v5, "com.lge.ims.service.im.IChatListener"

    #@115
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@118
    .line 191
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@11b
    move-result-object v0

    #@11c
    .line 193
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@11f
    move-result-object v1

    #@120
    .line 195
    .local v1, _arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@123
    move-result v5

    #@124
    if-eqz v5, :cond_13d

    #@126
    .line 196
    sget-object v5, Lcom/lge/ims/service/im/IMMessage;->CREATOR:Landroid/os/Parcelable$Creator;

    #@128
    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@12b
    move-result-object v2

    #@12c
    check-cast v2, Lcom/lge/ims/service/im/IMMessage;

    #@12e
    .line 202
    .local v2, _arg2:Lcom/lge/ims/service/im/IMMessage;
    :goto_12e
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@131
    move-result v5

    #@132
    if-eqz v5, :cond_13f

    #@134
    move v3, v4

    #@135
    .line 203
    .local v3, _arg3:Z
    :goto_135
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/lge/ims/service/im/IChatListener$Stub;->messageReceived(Ljava/lang/String;Ljava/lang/String;Lcom/lge/ims/service/im/IMMessage;Z)V

    #@138
    .line 204
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@13b
    goto/16 :goto_8

    #@13d
    .line 199
    .end local v2           #_arg2:Lcom/lge/ims/service/im/IMMessage;
    .end local v3           #_arg3:Z
    :cond_13d
    const/4 v2, 0x0

    #@13e
    .restart local v2       #_arg2:Lcom/lge/ims/service/im/IMMessage;
    goto :goto_12e

    #@13f
    .line 202
    :cond_13f
    const/4 v3, 0x0

    #@140
    goto :goto_135

    #@141
    .line 209
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:Ljava/lang/String;
    .end local v2           #_arg2:Lcom/lge/ims/service/im/IMMessage;
    :sswitch_141
    const-string v5, "com.lge.ims.service.im.IChatListener"

    #@143
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@146
    .line 211
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@149
    move-result-object v0

    #@14a
    .line 213
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@14d
    move-result v1

    #@14e
    .line 214
    .local v1, _arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/service/im/IChatListener$Stub;->composingIndicatorReceived(Ljava/lang/String;I)V

    #@151
    .line 215
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@154
    goto/16 :goto_8

    #@156
    .line 38
    :sswitch_data_156
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_1f
        0x3 -> :sswitch_2f
        0x4 -> :sswitch_3f
        0x5 -> :sswitch_4b
        0x6 -> :sswitch_5b
        0x7 -> :sswitch_67
        0x8 -> :sswitch_77
        0x9 -> :sswitch_87
        0xa -> :sswitch_94
        0xb -> :sswitch_a5
        0xc -> :sswitch_ba
        0xd -> :sswitch_cb
        0xe -> :sswitch_dc
        0xf -> :sswitch_ed
        0x10 -> :sswitch_fe
        0x11 -> :sswitch_113
        0x12 -> :sswitch_141
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
