.class public Lcom/lge/ims/service/uc/UCServiceManager;
.super Ljava/lang/Object;
.source "UCServiceManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "UC"

.field private static ucSvcMgr:Lcom/lge/ims/service/uc/UCServiceManager;


# instance fields
.field private ucSvc:Lcom/lge/ims/service/uc/UCServiceImpl;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 9
    const/4 v0, 0x0

    #@1
    sput-object v0, Lcom/lge/ims/service/uc/UCServiceManager;->ucSvcMgr:Lcom/lge/ims/service/uc/UCServiceManager;

    #@3
    return-void
.end method

.method private constructor <init>()V
    .registers 3

    #@0
    .prologue
    .line 12
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 10
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Lcom/lge/ims/service/uc/UCServiceManager;->ucSvc:Lcom/lge/ims/service/uc/UCServiceImpl;

    #@6
    .line 13
    const-string v0, "UC"

    #@8
    const-string v1, "UCServiceManager()"

    #@a
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    .line 14
    return-void
.end method

.method public static getInstance()Lcom/lge/ims/service/uc/UCServiceManager;
    .registers 2

    #@0
    .prologue
    .line 17
    const-string v0, "UC"

    #@2
    const-string v1, "getInstance()"

    #@4
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 19
    sget-object v0, Lcom/lge/ims/service/uc/UCServiceManager;->ucSvcMgr:Lcom/lge/ims/service/uc/UCServiceManager;

    #@9
    if-nez v0, :cond_12

    #@b
    .line 20
    new-instance v0, Lcom/lge/ims/service/uc/UCServiceManager;

    #@d
    invoke-direct {v0}, Lcom/lge/ims/service/uc/UCServiceManager;-><init>()V

    #@10
    sput-object v0, Lcom/lge/ims/service/uc/UCServiceManager;->ucSvcMgr:Lcom/lge/ims/service/uc/UCServiceManager;

    #@12
    .line 23
    :cond_12
    sget-object v0, Lcom/lge/ims/service/uc/UCServiceManager;->ucSvcMgr:Lcom/lge/ims/service/uc/UCServiceManager;

    #@14
    return-object v0
.end method


# virtual methods
.method public getService()Lcom/lge/ims/service/uc/UCServiceImpl;
    .registers 4

    #@0
    .prologue
    .line 46
    const-string v0, "UC"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "getService() :: "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-object v2, p0, Lcom/lge/ims/service/uc/UCServiceManager;->ucSvc:Lcom/lge/ims/service/uc/UCServiceImpl;

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@1a
    .line 48
    iget-object v0, p0, Lcom/lge/ims/service/uc/UCServiceManager;->ucSvc:Lcom/lge/ims/service/uc/UCServiceImpl;

    #@1c
    return-object v0
.end method

.method public resetService()V
    .registers 3

    #@0
    .prologue
    .line 52
    const-string v0, "UC"

    #@2
    const-string v1, "resetService()"

    #@4
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 54
    iget-object v0, p0, Lcom/lge/ims/service/uc/UCServiceManager;->ucSvc:Lcom/lge/ims/service/uc/UCServiceImpl;

    #@9
    if-eqz v0, :cond_11

    #@b
    .line 55
    iget-object v0, p0, Lcom/lge/ims/service/uc/UCServiceManager;->ucSvc:Lcom/lge/ims/service/uc/UCServiceImpl;

    #@d
    const/4 v1, 0x0

    #@e
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/uc/UCServiceImpl;->setListener(Lcom/lge/ims/service/uc/IUCServiceListener;)V

    #@11
    .line 57
    :cond_11
    return-void
.end method

.method public startService(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 27
    const-string v0, "UC"

    #@2
    const-string v1, "startService()"

    #@4
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 29
    iget-object v0, p0, Lcom/lge/ims/service/uc/UCServiceManager;->ucSvc:Lcom/lge/ims/service/uc/UCServiceImpl;

    #@9
    if-nez v0, :cond_17

    #@b
    .line 30
    new-instance v0, Lcom/lge/ims/service/uc/UCServiceImpl;

    #@d
    invoke-direct {v0, p1}, Lcom/lge/ims/service/uc/UCServiceImpl;-><init>(Landroid/content/Context;)V

    #@10
    iput-object v0, p0, Lcom/lge/ims/service/uc/UCServiceManager;->ucSvc:Lcom/lge/ims/service/uc/UCServiceImpl;

    #@12
    .line 31
    iget-object v0, p0, Lcom/lge/ims/service/uc/UCServiceManager;->ucSvc:Lcom/lge/ims/service/uc/UCServiceImpl;

    #@14
    invoke-virtual {v0}, Lcom/lge/ims/service/uc/UCServiceImpl;->create()Z

    #@17
    .line 34
    :cond_17
    return-void
.end method

.method public stopService()V
    .registers 3

    #@0
    .prologue
    .line 37
    const-string v0, "UC"

    #@2
    const-string v1, "stopService()"

    #@4
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 39
    iget-object v0, p0, Lcom/lge/ims/service/uc/UCServiceManager;->ucSvc:Lcom/lge/ims/service/uc/UCServiceImpl;

    #@9
    if-eqz v0, :cond_13

    #@b
    .line 40
    iget-object v0, p0, Lcom/lge/ims/service/uc/UCServiceManager;->ucSvc:Lcom/lge/ims/service/uc/UCServiceImpl;

    #@d
    invoke-virtual {v0}, Lcom/lge/ims/service/uc/UCServiceImpl;->destroy()V

    #@10
    .line 41
    const/4 v0, 0x0

    #@11
    iput-object v0, p0, Lcom/lge/ims/service/uc/UCServiceManager;->ucSvc:Lcom/lge/ims/service/uc/UCServiceImpl;

    #@13
    .line 43
    :cond_13
    return-void
.end method
