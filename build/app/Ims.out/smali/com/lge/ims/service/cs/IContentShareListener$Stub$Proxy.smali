.class Lcom/lge/ims/service/cs/IContentShareListener$Stub$Proxy;
.super Ljava/lang/Object;
.source "IContentShareListener.java"

# interfaces
.implements Lcom/lge/ims/service/cs/IContentShareListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/cs/IContentShareListener$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 99
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 100
    iput-object p1, p0, Lcom/lge/ims/service/cs/IContentShareListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 101
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 104
    iget-object v0, p0, Lcom/lge/ims/service/cs/IContentShareListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 108
    const-string v0, "com.lge.ims.service.cs.IContentShareListener"

    #@2
    return-object v0
.end method

.method public onReceivedImageSession(Lcom/lge/ims/service/cs/IImageSession;Ljava/lang/String;Ljava/lang/String;I)V
    .registers 10
    .parameter "iImageSession"
    .parameter "peerPhoneNumber"
    .parameter "fileName"
    .parameter "fileSize"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 169
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 170
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 172
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.cs.IContentShareListener"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 173
    if-eqz p1, :cond_30

    #@f
    invoke-interface {p1}, Lcom/lge/ims/service/cs/IImageSession;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 174
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@19
    .line 175
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1c
    .line 176
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 177
    iget-object v2, p0, Lcom/lge/ims/service/cs/IContentShareListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@21
    const/4 v3, 0x4

    #@22
    const/4 v4, 0x0

    #@23
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@26
    .line 178
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_29
    .catchall {:try_start_8 .. :try_end_29} :catchall_32

    #@29
    .line 181
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 182
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 184
    return-void

    #@30
    .line 173
    :cond_30
    const/4 v2, 0x0

    #@31
    goto :goto_13

    #@32
    .line 181
    :catchall_32
    move-exception v2

    #@33
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@36
    .line 182
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@39
    throw v2
.end method

.method public onReceivedVideoSession(Lcom/lge/ims/service/cs/IVideoSession;)V
    .registers 7
    .parameter "iVideoSession"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 154
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 155
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 157
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.cs.IContentShareListener"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 158
    if-eqz p1, :cond_27

    #@f
    invoke-interface {p1}, Lcom/lge/ims/service/cs/IVideoSession;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 159
    iget-object v2, p0, Lcom/lge/ims/service/cs/IContentShareListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/4 v3, 0x3

    #@19
    const/4 v4, 0x0

    #@1a
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1d
    .line 160
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_20
    .catchall {:try_start_8 .. :try_end_20} :catchall_29

    #@20
    .line 163
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 164
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 166
    return-void

    #@27
    .line 158
    :cond_27
    const/4 v2, 0x0

    #@28
    goto :goto_13

    #@29
    .line 163
    :catchall_29
    move-exception v2

    #@2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 164
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    throw v2
.end method

.method public onReconfigured(I)V
    .registers 7
    .parameter "serviceNetworkType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 116
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 117
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 119
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.cs.IContentShareListener"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 120
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 121
    iget-object v2, p0, Lcom/lge/ims/service/cs/IContentShareListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v3, 0x1

    #@13
    const/4 v4, 0x0

    #@14
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 122
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_21

    #@1a
    .line 125
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1d
    .line 126
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@20
    .line 128
    return-void

    #@21
    .line 125
    :catchall_21
    move-exception v2

    #@22
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 126
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    throw v2
.end method

.method public onUpdateCapability(Lcom/lge/ims/service/cd/Capabilities;)V
    .registers 7
    .parameter "capabilities"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 133
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 134
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 136
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.cs.IContentShareListener"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 137
    if-eqz p1, :cond_28

    #@f
    .line 138
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 139
    const/4 v2, 0x0

    #@14
    invoke-virtual {p1, v0, v2}, Lcom/lge/ims/service/cd/Capabilities;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 144
    :goto_17
    iget-object v2, p0, Lcom/lge/ims/service/cs/IContentShareListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/4 v3, 0x2

    #@1a
    const/4 v4, 0x0

    #@1b
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 145
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_21
    .catchall {:try_start_8 .. :try_end_21} :catchall_2d

    #@21
    .line 148
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 149
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 151
    return-void

    #@28
    .line 142
    :cond_28
    const/4 v2, 0x0

    #@29
    :try_start_29
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2c
    .catchall {:try_start_29 .. :try_end_2c} :catchall_2d

    #@2c
    goto :goto_17

    #@2d
    .line 148
    :catchall_2d
    move-exception v2

    #@2e
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 149
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@34
    throw v2
.end method
