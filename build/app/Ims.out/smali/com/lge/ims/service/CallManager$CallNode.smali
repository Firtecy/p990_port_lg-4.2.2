.class final Lcom/lge/ims/service/CallManager$CallNode;
.super Ljava/lang/Object;
.source "CallManager.java"

# interfaces
.implements Lcom/lge/ims/service/cd/CapabilityQueryListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/CallManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "CallNode"
.end annotation


# instance fields
.field private final mCM:Lcom/lge/ims/service/CallManager;

.field private mCallType:I

.field private mCapQuery:Lcom/lge/ims/service/cd/CapabilityQuery;

.field private mNumber:Ljava/lang/String;

.field final synthetic this$0:Lcom/lge/ims/service/CallManager;


# direct methods
.method public constructor <init>(Lcom/lge/ims/service/CallManager;Ljava/lang/String;I)V
    .registers 6
    .parameter
    .parameter "number"
    .parameter "callType"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 70
    iput-object p1, p0, Lcom/lge/ims/service/CallManager$CallNode;->this$0:Lcom/lge/ims/service/CallManager;

    #@3
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 65
    invoke-static {}, Lcom/lge/ims/service/CallManager;->getInstance()Lcom/lge/ims/service/CallManager;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Lcom/lge/ims/service/CallManager$CallNode;->mCM:Lcom/lge/ims/service/CallManager;

    #@c
    .line 66
    iput-object v1, p0, Lcom/lge/ims/service/CallManager$CallNode;->mNumber:Ljava/lang/String;

    #@e
    .line 67
    const/4 v0, 0x1

    #@f
    iput v0, p0, Lcom/lge/ims/service/CallManager$CallNode;->mCallType:I

    #@11
    .line 68
    iput-object v1, p0, Lcom/lge/ims/service/CallManager$CallNode;->mCapQuery:Lcom/lge/ims/service/cd/CapabilityQuery;

    #@13
    .line 71
    iput-object p2, p0, Lcom/lge/ims/service/CallManager$CallNode;->mNumber:Ljava/lang/String;

    #@15
    .line 72
    iput p3, p0, Lcom/lge/ims/service/CallManager$CallNode;->mCallType:I

    #@17
    .line 73
    return-void
.end method


# virtual methods
.method public abortCapabilityQuery()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 76
    iget-object v0, p0, Lcom/lge/ims/service/CallManager$CallNode;->mCapQuery:Lcom/lge/ims/service/cd/CapabilityQuery;

    #@3
    if-eqz v0, :cond_11

    #@5
    .line 77
    iget-object v0, p0, Lcom/lge/ims/service/CallManager$CallNode;->mCapQuery:Lcom/lge/ims/service/cd/CapabilityQuery;

    #@7
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/cd/CapabilityQuery;->setListener(Lcom/lge/ims/service/cd/CapabilityQueryListener;)V

    #@a
    .line 78
    iget-object v0, p0, Lcom/lge/ims/service/CallManager$CallNode;->mCapQuery:Lcom/lge/ims/service/cd/CapabilityQuery;

    #@c
    invoke-virtual {v0}, Lcom/lge/ims/service/cd/CapabilityQuery;->close()V

    #@f
    .line 79
    iput-object v1, p0, Lcom/lge/ims/service/CallManager$CallNode;->mCapQuery:Lcom/lge/ims/service/cd/CapabilityQuery;

    #@11
    .line 81
    :cond_11
    return-void
.end method

.method public capabilityQueryDelivered(Lcom/lge/ims/service/cd/CapabilityQuery;Lcom/lge/ims/service/cd/Capabilities;I)V
    .registers 10
    .parameter "cq"
    .parameter "capabilities"
    .parameter "resultCode"

    #@0
    .prologue
    .line 130
    iget-object v1, p0, Lcom/lge/ims/service/CallManager$CallNode;->mCapQuery:Lcom/lge/ims/service/cd/CapabilityQuery;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 154
    :goto_4
    return-void

    #@5
    .line 134
    :cond_5
    iget-object v1, p0, Lcom/lge/ims/service/CallManager$CallNode;->mCapQuery:Lcom/lge/ims/service/cd/CapabilityQuery;

    #@7
    if-eq v1, p1, :cond_2c

    #@9
    .line 135
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v2, "CQ is different ? "

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    iget-object v2, p0, Lcom/lge/ims/service/CallManager$CallNode;->mCapQuery:Lcom/lge/ims/service/cd/CapabilityQuery;

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    const-string v2, ":"

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@2b
    goto :goto_4

    #@2c
    .line 139
    :cond_2c
    const/4 v1, 0x0

    #@2d
    iput-object v1, p0, Lcom/lge/ims/service/CallManager$CallNode;->mCapQuery:Lcom/lge/ims/service/cd/CapabilityQuery;

    #@2f
    .line 142
    :try_start_2f
    iget-object v1, p0, Lcom/lge/ims/service/CallManager$CallNode;->mCM:Lcom/lge/ims/service/CallManager;

    #@31
    invoke-virtual {v1}, Lcom/lge/ims/service/CallManager;->isServiceCapable()Z

    #@34
    move-result v1

    #@35
    if-eqz v1, :cond_57

    #@37
    .line 143
    if-nez p3, :cond_48

    #@39
    .line 144
    iget-object v1, p0, Lcom/lge/ims/service/CallManager$CallNode;->mCM:Lcom/lge/ims/service/CallManager;

    #@3b
    invoke-virtual {v1, p2}, Lcom/lge/ims/service/CallManager;->notifyCapabilities(Lcom/lge/ims/service/cd/Capabilities;)V
    :try_end_3e
    .catch Ljava/lang/Exception; {:try_start_2f .. :try_end_3e} :catch_3f

    #@3e
    goto :goto_4

    #@3f
    .line 151
    :catch_3f
    move-exception v0

    #@40
    .line 152
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@43
    move-result-object v1

    #@44
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@47
    goto :goto_4

    #@48
    .line 146
    .end local v0           #e:Ljava/lang/Exception;
    :cond_48
    :try_start_48
    iget-object v1, p0, Lcom/lge/ims/service/CallManager$CallNode;->mCM:Lcom/lge/ims/service/CallManager;

    #@4a
    new-instance v2, Lcom/lge/ims/service/cd/Capabilities;

    #@4c
    iget-object v3, p0, Lcom/lge/ims/service/CallManager$CallNode;->mNumber:Ljava/lang/String;

    #@4e
    const/4 v4, 0x0

    #@4f
    const/4 v5, 0x0

    #@50
    invoke-direct {v2, v3, v4, v5}, Lcom/lge/ims/service/cd/Capabilities;-><init>(Ljava/lang/String;II)V

    #@53
    invoke-virtual {v1, v2}, Lcom/lge/ims/service/CallManager;->notifyCapabilities(Lcom/lge/ims/service/cd/Capabilities;)V

    #@56
    goto :goto_4

    #@57
    .line 149
    :cond_57
    iget-object v1, p0, Lcom/lge/ims/service/CallManager$CallNode;->mCM:Lcom/lge/ims/service/CallManager;

    #@59
    new-instance v2, Lcom/lge/ims/service/cd/Capabilities;

    #@5b
    iget-object v3, p0, Lcom/lge/ims/service/CallManager$CallNode;->mNumber:Ljava/lang/String;

    #@5d
    const/4 v4, 0x0

    #@5e
    const/4 v5, 0x0

    #@5f
    invoke-direct {v2, v3, v4, v5}, Lcom/lge/ims/service/cd/Capabilities;-><init>(Ljava/lang/String;II)V

    #@62
    invoke-virtual {v1, v2}, Lcom/lge/ims/service/CallManager;->notifyCapabilities(Lcom/lge/ims/service/cd/Capabilities;)V
    :try_end_65
    .catch Ljava/lang/Exception; {:try_start_48 .. :try_end_65} :catch_3f

    #@65
    goto :goto_4
.end method

.method public getCapabilityCallType()I
    .registers 2

    #@0
    .prologue
    .line 85
    iget v0, p0, Lcom/lge/ims/service/CallManager$CallNode;->mCallType:I

    #@2
    return v0
.end method

.method public getNumber()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 89
    iget-object v0, p0, Lcom/lge/ims/service/CallManager$CallNode;->mNumber:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public isCapabilityQuerying()Z
    .registers 2

    #@0
    .prologue
    .line 93
    iget-object v0, p0, Lcom/lge/ims/service/CallManager$CallNode;->mCapQuery:Lcom/lge/ims/service/cd/CapabilityQuery;

    #@2
    if-eqz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public queryCapabilities()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 97
    iget-object v2, p0, Lcom/lge/ims/service/CallManager$CallNode;->mCapQuery:Lcom/lge/ims/service/cd/CapabilityQuery;

    #@3
    if-eqz v2, :cond_6

    #@5
    .line 123
    :goto_5
    return v1

    #@6
    .line 102
    :cond_6
    invoke-static {}, Lcom/lge/ims/service/cd/CapabilityService;->getInstance()Lcom/lge/ims/service/cd/CapabilityService;

    #@9
    move-result-object v0

    #@a
    .line 104
    .local v0, cs:Lcom/lge/ims/service/cd/CapabilityService;
    if-nez v0, :cond_12

    #@c
    .line 105
    const-string v2, "CapabilityService is null"

    #@e
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@11
    goto :goto_5

    #@12
    .line 109
    :cond_12
    invoke-virtual {v0}, Lcom/lge/ims/service/cd/CapabilityService;->createCapabilityQuery()Lcom/lge/ims/service/cd/CapabilityQuery;

    #@15
    move-result-object v2

    #@16
    iput-object v2, p0, Lcom/lge/ims/service/CallManager$CallNode;->mCapQuery:Lcom/lge/ims/service/cd/CapabilityQuery;

    #@18
    .line 111
    iget-object v2, p0, Lcom/lge/ims/service/CallManager$CallNode;->mCapQuery:Lcom/lge/ims/service/cd/CapabilityQuery;

    #@1a
    if-nez v2, :cond_22

    #@1c
    .line 112
    const-string v2, "Instantiating a CapabilityQuery failed"

    #@1e
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@21
    goto :goto_5

    #@22
    .line 116
    :cond_22
    iget-object v2, p0, Lcom/lge/ims/service/CallManager$CallNode;->mCapQuery:Lcom/lge/ims/service/cd/CapabilityQuery;

    #@24
    invoke-virtual {v2, p0}, Lcom/lge/ims/service/cd/CapabilityQuery;->setListener(Lcom/lge/ims/service/cd/CapabilityQueryListener;)V

    #@27
    .line 118
    iget-object v2, p0, Lcom/lge/ims/service/CallManager$CallNode;->mCapQuery:Lcom/lge/ims/service/cd/CapabilityQuery;

    #@29
    iget-object v3, p0, Lcom/lge/ims/service/CallManager$CallNode;->mNumber:Ljava/lang/String;

    #@2b
    invoke-virtual {v2, v3}, Lcom/lge/ims/service/cd/CapabilityQuery;->send(Ljava/lang/String;)Z

    #@2e
    move-result v2

    #@2f
    if-nez v2, :cond_37

    #@31
    .line 119
    const-string v2, "Querying a capabilities failed"

    #@33
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@36
    goto :goto_5

    #@37
    .line 123
    :cond_37
    const/4 v1, 0x1

    #@38
    goto :goto_5
.end method
