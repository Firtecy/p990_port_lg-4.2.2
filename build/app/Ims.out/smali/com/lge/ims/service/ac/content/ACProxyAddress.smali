.class public Lcom/lge/ims/service/ac/content/ACProxyAddress;
.super Ljava/lang/Object;
.source "ACProxyAddress.java"


# instance fields
.field public mLBOPCSCFAddress:Ljava/lang/String;

.field public mLBOPCSCFAddressType:Ljava/lang/String;

.field public mSBCAddress:Ljava/lang/String;

.field public mSBCAddressType:Ljava/lang/String;

.field public mSBCTLSAddress:Ljava/lang/String;

.field public mSBCTLSAddressType:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 16
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 7
    iput-object v0, p0, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCAddress:Ljava/lang/String;

    #@6
    .line 8
    iput-object v0, p0, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCAddressType:Ljava/lang/String;

    #@8
    .line 9
    iput-object v0, p0, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCTLSAddress:Ljava/lang/String;

    #@a
    .line 10
    iput-object v0, p0, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCTLSAddressType:Ljava/lang/String;

    #@c
    .line 11
    iput-object v0, p0, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mLBOPCSCFAddress:Ljava/lang/String;

    #@e
    .line 12
    iput-object v0, p0, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mLBOPCSCFAddressType:Ljava/lang/String;

    #@10
    .line 17
    return-void
.end method


# virtual methods
.method public clear()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 20
    iput-object v0, p0, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCAddress:Ljava/lang/String;

    #@3
    .line 21
    iput-object v0, p0, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCAddressType:Ljava/lang/String;

    #@5
    .line 22
    iput-object v0, p0, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCTLSAddress:Ljava/lang/String;

    #@7
    .line 23
    iput-object v0, p0, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCTLSAddressType:Ljava/lang/String;

    #@9
    .line 24
    iput-object v0, p0, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mLBOPCSCFAddress:Ljava/lang/String;

    #@b
    .line 25
    iput-object v0, p0, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mLBOPCSCFAddressType:Ljava/lang/String;

    #@d
    .line 26
    return-void
.end method

.method public containsContent(Lcom/lge/ims/service/ac/content/ACProxyAddress;)Z
    .registers 6
    .parameter "cache"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 29
    if-nez p1, :cond_5

    #@4
    .line 69
    :cond_4
    :goto_4
    return v0

    #@5
    .line 33
    :cond_5
    iget-object v2, p0, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCAddress:Ljava/lang/String;

    #@7
    if-eqz v2, :cond_19

    #@9
    iget-object v2, p1, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCAddress:Ljava/lang/String;

    #@b
    if-eqz v2, :cond_19

    #@d
    .line 34
    iget-object v2, p0, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCAddress:Ljava/lang/String;

    #@f
    iget-object v3, p1, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCAddress:Ljava/lang/String;

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v2

    #@15
    if-nez v2, :cond_19

    #@17
    move v0, v1

    #@18
    .line 35
    goto :goto_4

    #@19
    .line 39
    :cond_19
    iget-object v2, p0, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCAddressType:Ljava/lang/String;

    #@1b
    if-eqz v2, :cond_2d

    #@1d
    iget-object v2, p1, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCAddressType:Ljava/lang/String;

    #@1f
    if-eqz v2, :cond_2d

    #@21
    .line 40
    iget-object v2, p0, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCAddressType:Ljava/lang/String;

    #@23
    iget-object v3, p1, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCAddressType:Ljava/lang/String;

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28
    move-result v2

    #@29
    if-nez v2, :cond_2d

    #@2b
    move v0, v1

    #@2c
    .line 41
    goto :goto_4

    #@2d
    .line 45
    :cond_2d
    iget-object v2, p0, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCTLSAddress:Ljava/lang/String;

    #@2f
    if-eqz v2, :cond_41

    #@31
    iget-object v2, p1, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCTLSAddress:Ljava/lang/String;

    #@33
    if-eqz v2, :cond_41

    #@35
    .line 46
    iget-object v2, p0, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCTLSAddress:Ljava/lang/String;

    #@37
    iget-object v3, p1, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCTLSAddress:Ljava/lang/String;

    #@39
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3c
    move-result v2

    #@3d
    if-nez v2, :cond_41

    #@3f
    move v0, v1

    #@40
    .line 47
    goto :goto_4

    #@41
    .line 51
    :cond_41
    iget-object v2, p0, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCTLSAddressType:Ljava/lang/String;

    #@43
    if-eqz v2, :cond_55

    #@45
    iget-object v2, p1, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCTLSAddressType:Ljava/lang/String;

    #@47
    if-eqz v2, :cond_55

    #@49
    .line 52
    iget-object v2, p0, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCTLSAddressType:Ljava/lang/String;

    #@4b
    iget-object v3, p1, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCTLSAddressType:Ljava/lang/String;

    #@4d
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@50
    move-result v2

    #@51
    if-nez v2, :cond_55

    #@53
    move v0, v1

    #@54
    .line 53
    goto :goto_4

    #@55
    .line 57
    :cond_55
    iget-object v2, p0, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mLBOPCSCFAddress:Ljava/lang/String;

    #@57
    if-eqz v2, :cond_69

    #@59
    iget-object v2, p1, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mLBOPCSCFAddress:Ljava/lang/String;

    #@5b
    if-eqz v2, :cond_69

    #@5d
    .line 58
    iget-object v2, p0, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mLBOPCSCFAddress:Ljava/lang/String;

    #@5f
    iget-object v3, p1, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mLBOPCSCFAddress:Ljava/lang/String;

    #@61
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@64
    move-result v2

    #@65
    if-nez v2, :cond_69

    #@67
    move v0, v1

    #@68
    .line 59
    goto :goto_4

    #@69
    .line 63
    :cond_69
    iget-object v2, p0, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mLBOPCSCFAddressType:Ljava/lang/String;

    #@6b
    if-eqz v2, :cond_4

    #@6d
    iget-object v2, p1, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mLBOPCSCFAddressType:Ljava/lang/String;

    #@6f
    if-eqz v2, :cond_4

    #@71
    .line 64
    iget-object v2, p0, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mLBOPCSCFAddressType:Ljava/lang/String;

    #@73
    iget-object v3, p1, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mLBOPCSCFAddressType:Ljava/lang/String;

    #@75
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@78
    move-result v2

    #@79
    if-nez v2, :cond_4

    #@7b
    move v0, v1

    #@7c
    .line 65
    goto :goto_4
.end method

.method public isContentPresent()Z
    .registers 2

    #@0
    .prologue
    .line 73
    iget-object v0, p0, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCAddress:Ljava/lang/String;

    #@2
    if-nez v0, :cond_18

    #@4
    iget-object v0, p0, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCAddressType:Ljava/lang/String;

    #@6
    if-nez v0, :cond_18

    #@8
    iget-object v0, p0, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCTLSAddress:Ljava/lang/String;

    #@a
    if-nez v0, :cond_18

    #@c
    iget-object v0, p0, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCTLSAddressType:Ljava/lang/String;

    #@e
    if-nez v0, :cond_18

    #@10
    iget-object v0, p0, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mLBOPCSCFAddress:Ljava/lang/String;

    #@12
    if-nez v0, :cond_18

    #@14
    iget-object v0, p0, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mLBOPCSCFAddressType:Ljava/lang/String;

    #@16
    if-eqz v0, :cond_1a

    #@18
    :cond_18
    const/4 v0, 0x1

    #@19
    :goto_19
    return v0

    #@1a
    :cond_1a
    const/4 v0, 0x0

    #@1b
    goto :goto_19
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "sbc_address="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCAddress:Ljava/lang/String;

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ", sbc_address_type="

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget-object v1, p0, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCAddressType:Ljava/lang/String;

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, ", sbc_tls_address="

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget-object v1, p0, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCTLSAddress:Ljava/lang/String;

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, ", sbc_tls_address_type="

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    iget-object v1, p0, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mSBCTLSAddressType:Ljava/lang/String;

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    const-string v1, ", lbo_pcscf_address="

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    iget-object v1, p0, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mLBOPCSCFAddress:Ljava/lang/String;

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    const-string v1, ", lbo_pcscf_address_type="

    #@43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v0

    #@47
    iget-object v1, p0, Lcom/lge/ims/service/ac/content/ACProxyAddress;->mLBOPCSCFAddressType:Ljava/lang/String;

    #@49
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v0

    #@4d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v0

    #@51
    return-object v0
.end method
