.class public Lcom/lge/ims/service/ac/ACAuthHelper;
.super Ljava/lang/Object;
.source "ACAuthHelper.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ACAuthHelper"


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 16
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 17
    return-void
.end method

.method public static calculateMessageDigest(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "algorithm"
    .parameter "input"

    #@0
    .prologue
    .line 46
    const/4 v1, 0x0

    #@1
    .line 49
    .local v1, md:Ljava/security/MessageDigest;
    :try_start_1
    invoke-static {p0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    #@4
    move-result-object v1

    #@5
    .line 50
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    #@8
    move-result-object v2

    #@9
    invoke-virtual {v1, v2}, Ljava/security/MessageDigest;->update([B)V
    :try_end_c
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_c} :catch_10
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_c} :catch_1f

    #@c
    .line 61
    :goto_c
    if-nez v1, :cond_2e

    #@e
    .line 62
    const/4 v2, 0x0

    #@f
    .line 65
    :goto_f
    return-object v2

    #@10
    .line 51
    :catch_10
    move-exception v0

    #@11
    .line 52
    .local v0, e:Ljava/security/NoSuchAlgorithmException;
    const-string v2, "ACAuthHelper"

    #@13
    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->toString()Ljava/lang/String;

    #@16
    move-result-object v3

    #@17
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@1a
    .line 53
    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    #@1d
    .line 54
    const/4 v1, 0x0

    #@1e
    .line 59
    goto :goto_c

    #@1f
    .line 55
    .end local v0           #e:Ljava/security/NoSuchAlgorithmException;
    :catch_1f
    move-exception v0

    #@20
    .line 56
    .local v0, e:Ljava/lang/Exception;
    const-string v2, "ACAuthHelper"

    #@22
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@25
    move-result-object v3

    #@26
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@29
    .line 57
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@2c
    .line 58
    const/4 v1, 0x0

    #@2d
    goto :goto_c

    #@2e
    .line 65
    .end local v0           #e:Ljava/lang/Exception;
    :cond_2e
    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    #@31
    move-result-object v2

    #@32
    invoke-static {v2}, Lcom/lge/ims/service/ac/ACAuthHelper;->toHexString([B)Ljava/lang/String;

    #@35
    move-result-object v2

    #@36
    goto :goto_f
.end method

.method public static calculateResponse(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 8
    .parameter "nonce"
    .parameter "imsi"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 20
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@4
    move-result v3

    #@5
    if-nez v3, :cond_d

    #@7
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@a
    move-result v3

    #@b
    if-eqz v3, :cond_e

    #@d
    .line 42
    :cond_d
    :goto_d
    return-object v2

    #@e
    .line 24
    :cond_e
    const-string v3, "SHA-256"

    #@10
    invoke-static {v3, p1}, Lcom/lge/ims/service/ac/ACAuthHelper;->calculateMessageDigest(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@13
    move-result-object v0

    #@14
    .line 26
    .local v0, s1:Ljava/lang/String;
    if-nez v0, :cond_1e

    #@16
    .line 27
    const-string v3, "ACAuthHelper"

    #@18
    const-string v4, "s1 is null"

    #@1a
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@1d
    goto :goto_d

    #@1e
    .line 31
    :cond_1e
    new-instance v3, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    const-string v4, ":"

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v1

    #@35
    .line 33
    .local v1, s2:Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@38
    move-result v3

    #@39
    if-eqz v3, :cond_43

    #@3b
    .line 34
    const-string v3, "ACAuthHelper"

    #@3d
    const-string v4, "s2 is empty"

    #@3f
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@42
    goto :goto_d

    #@43
    .line 38
    :cond_43
    const-string v3, "MD5"

    #@45
    invoke-static {v3, v1}, Lcom/lge/ims/service/ac/ACAuthHelper;->calculateMessageDigest(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@48
    move-result-object v2

    #@49
    .line 40
    .local v2, s3:Ljava/lang/String;
    const-string v3, "ACAuthHelper"

    #@4b
    new-instance v4, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    const-string v5, "s3="

    #@52
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v4

    #@56
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v4

    #@5a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object v4

    #@5e
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@61
    goto :goto_d
.end method

.method private static toHexString([B)Ljava/lang/String;
    .registers 6
    .parameter "input"

    #@0
    .prologue
    .line 69
    if-nez p0, :cond_4

    #@2
    .line 70
    const/4 v2, 0x0

    #@3
    .line 86
    :cond_3
    return-object v2

    #@4
    .line 73
    :cond_4
    const-string v2, ""

    #@6
    .line 76
    .local v2, output:Ljava/lang/String;
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    array-length v3, p0

    #@8
    if-ge v1, v3, :cond_3

    #@a
    .line 77
    aget-byte v3, p0, v1

    #@c
    and-int/lit16 v3, v3, 0xff

    #@e
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@11
    move-result-object v0

    #@12
    .line 79
    .local v0, hex:Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@15
    move-result v3

    #@16
    const/4 v4, 0x1

    #@17
    if-ne v3, v4, :cond_1f

    #@19
    .line 80
    const-string v3, "0"

    #@1b
    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    .line 83
    :cond_1f
    new-instance v3, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v3

    #@28
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@2b
    move-result-object v4

    #@2c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v3

    #@30
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v2

    #@34
    .line 76
    add-int/lit8 v1, v1, 0x1

    #@36
    goto :goto_7
.end method
