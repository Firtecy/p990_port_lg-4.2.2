.class public final Lcom/lge/ims/service/ac/ACConfiguration;
.super Ljava/lang/Object;
.source "ACConfiguration.java"


# static fields
.field public static final COMMERCIAL_NETWORK:I = 0x0

.field private static final TAG:Ljava/lang/String; = "ACConfiguration"

.field public static final TESTBED:I = 0x1

.field private static mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;


# instance fields
.field private mCommercialNetwork:Ljava/lang/String;

.field private mConnectionServer:I

.field private mIMSI:Ljava/lang/String;

.field private mPhoneNumber:Ljava/lang/String;

.field private mTestbed:Ljava/lang/String;

.field private mUpdateExceptionList:I

.field private mUsimOn:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 20
    new-instance v0, Lcom/lge/ims/service/ac/ACConfiguration;

    #@2
    invoke-direct {v0}, Lcom/lge/ims/service/ac/ACConfiguration;-><init>()V

    #@5
    sput-object v0, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@7
    return-void
.end method

.method private constructor <init>()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 37
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 23
    iput v2, p0, Lcom/lge/ims/service/ac/ACConfiguration;->mUpdateExceptionList:I

    #@7
    .line 26
    const/4 v0, 0x1

    #@8
    iput-boolean v0, p0, Lcom/lge/ims/service/ac/ACConfiguration;->mUsimOn:Z

    #@a
    .line 27
    iput-object v1, p0, Lcom/lge/ims/service/ac/ACConfiguration;->mPhoneNumber:Ljava/lang/String;

    #@c
    .line 28
    iput-object v1, p0, Lcom/lge/ims/service/ac/ACConfiguration;->mIMSI:Ljava/lang/String;

    #@e
    .line 31
    iput v2, p0, Lcom/lge/ims/service/ac/ACConfiguration;->mConnectionServer:I

    #@10
    .line 32
    iput-object v1, p0, Lcom/lge/ims/service/ac/ACConfiguration;->mCommercialNetwork:Ljava/lang/String;

    #@12
    .line 33
    iput-object v1, p0, Lcom/lge/ims/service/ac/ACConfiguration;->mTestbed:Ljava/lang/String;

    #@14
    .line 38
    const-string v0, "ACConfiguration"

    #@16
    const-string v1, "ACConfiguration is created"

    #@18
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@1b
    .line 39
    return-void
.end method

.method public static getCommercialNetwork()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 72
    sget-object v0, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 73
    const/4 v0, 0x0

    #@5
    .line 76
    :goto_5
    return-object v0

    #@6
    :cond_6
    sget-object v0, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@8
    iget-object v0, v0, Lcom/lge/ims/service/ac/ACConfiguration;->mCommercialNetwork:Ljava/lang/String;

    #@a
    goto :goto_5
.end method

.method public static getConnectionServer()I
    .registers 1

    #@0
    .prologue
    .line 80
    sget-object v0, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 81
    const/4 v0, 0x0

    #@5
    .line 84
    :goto_5
    return v0

    #@6
    :cond_6
    sget-object v0, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@8
    iget v0, v0, Lcom/lge/ims/service/ac/ACConfiguration;->mConnectionServer:I

    #@a
    goto :goto_5
.end method

.method public static getIMSI()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 88
    sget-object v0, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 89
    const/4 v0, 0x0

    #@5
    .line 92
    :goto_5
    return-object v0

    #@6
    :cond_6
    sget-object v0, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@8
    iget-object v0, v0, Lcom/lge/ims/service/ac/ACConfiguration;->mIMSI:Ljava/lang/String;

    #@a
    goto :goto_5
.end method

.method public static getPhoneNumber()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 96
    sget-object v0, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 97
    const/4 v0, 0x0

    #@5
    .line 100
    :goto_5
    return-object v0

    #@6
    :cond_6
    sget-object v0, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@8
    iget-object v0, v0, Lcom/lge/ims/service/ac/ACConfiguration;->mPhoneNumber:Ljava/lang/String;

    #@a
    goto :goto_5
.end method

.method public static getTestbed()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 104
    sget-object v0, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 105
    const/4 v0, 0x0

    #@5
    .line 108
    :goto_5
    return-object v0

    #@6
    :cond_6
    sget-object v0, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@8
    iget-object v0, v0, Lcom/lge/ims/service/ac/ACConfiguration;->mTestbed:Ljava/lang/String;

    #@a
    goto :goto_5
.end method

.method public static hasCommercialNetwork()Z
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 112
    sget-object v1, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@3
    if-nez v1, :cond_6

    #@5
    .line 124
    :cond_5
    :goto_5
    return v0

    #@6
    .line 116
    :cond_6
    sget-object v1, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@8
    iget-object v1, v1, Lcom/lge/ims/service/ac/ACConfiguration;->mCommercialNetwork:Ljava/lang/String;

    #@a
    if-eqz v1, :cond_5

    #@c
    .line 120
    sget-object v1, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@e
    iget-object v1, v1, Lcom/lge/ims/service/ac/ACConfiguration;->mCommercialNetwork:Ljava/lang/String;

    #@10
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@13
    move-result v1

    #@14
    if-eqz v1, :cond_5

    #@16
    .line 124
    const/4 v0, 0x1

    #@17
    goto :goto_5
.end method

.method public static hasTestbed()Z
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 128
    sget-object v1, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@3
    if-nez v1, :cond_6

    #@5
    .line 140
    :cond_5
    :goto_5
    return v0

    #@6
    .line 132
    :cond_6
    sget-object v1, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@8
    iget-object v1, v1, Lcom/lge/ims/service/ac/ACConfiguration;->mTestbed:Ljava/lang/String;

    #@a
    if-eqz v1, :cond_5

    #@c
    .line 136
    sget-object v1, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@e
    iget-object v1, v1, Lcom/lge/ims/service/ac/ACConfiguration;->mTestbed:Ljava/lang/String;

    #@10
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@13
    move-result v1

    #@14
    if-eqz v1, :cond_5

    #@16
    .line 140
    const/4 v0, 0x1

    #@17
    goto :goto_5
.end method

.method public static init(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 42
    if-nez p0, :cond_5

    #@4
    .line 69
    :cond_4
    :goto_4
    return-void

    #@5
    .line 46
    :cond_5
    sget-object v0, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@7
    if-eqz v0, :cond_4

    #@9
    .line 50
    sget-object v0, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@b
    iput v3, v0, Lcom/lge/ims/service/ac/ACConfiguration;->mUpdateExceptionList:I

    #@d
    .line 51
    sget-object v0, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@f
    const/4 v1, 0x1

    #@10
    iput-boolean v1, v0, Lcom/lge/ims/service/ac/ACConfiguration;->mUsimOn:Z

    #@12
    .line 52
    sget-object v0, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@14
    iput-object v2, v0, Lcom/lge/ims/service/ac/ACConfiguration;->mPhoneNumber:Ljava/lang/String;

    #@16
    .line 53
    sget-object v0, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@18
    iput-object v2, v0, Lcom/lge/ims/service/ac/ACConfiguration;->mIMSI:Ljava/lang/String;

    #@1a
    .line 54
    sget-object v0, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@1c
    iput v3, v0, Lcom/lge/ims/service/ac/ACConfiguration;->mConnectionServer:I

    #@1e
    .line 55
    sget-object v0, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@20
    iput-object v2, v0, Lcom/lge/ims/service/ac/ACConfiguration;->mCommercialNetwork:Ljava/lang/String;

    #@22
    .line 56
    sget-object v0, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@24
    iput-object v2, v0, Lcom/lge/ims/service/ac/ACConfiguration;->mTestbed:Ljava/lang/String;

    #@26
    .line 58
    sget-object v0, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@28
    invoke-direct {v0, p0}, Lcom/lge/ims/service/ac/ACConfiguration;->initForCommon(Landroid/content/Context;)V

    #@2b
    .line 59
    sget-object v0, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@2d
    invoke-direct {v0, p0}, Lcom/lge/ims/service/ac/ACConfiguration;->initForSubscriber(Landroid/content/Context;)V

    #@30
    .line 60
    sget-object v0, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@32
    invoke-direct {v0, p0}, Lcom/lge/ims/service/ac/ACConfiguration;->initForConnectionServer(Landroid/content/Context;)V

    #@35
    .line 62
    const-string v0, "ACConfiguration"

    #@37
    new-instance v1, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v2, "UpdateExceptionList="

    #@3e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v1

    #@42
    sget-object v2, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@44
    iget v2, v2, Lcom/lge/ims/service/ac/ACConfiguration;->mUpdateExceptionList:I

    #@46
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@49
    move-result-object v1

    #@4a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v1

    #@4e
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@51
    .line 63
    const-string v0, "ACConfiguration"

    #@53
    new-instance v1, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    const-string v2, "USIM="

    #@5a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v1

    #@5e
    sget-object v2, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@60
    iget-boolean v2, v2, Lcom/lge/ims/service/ac/ACConfiguration;->mUsimOn:Z

    #@62
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@65
    move-result-object v1

    #@66
    const-string v2, ", PhoneNumber="

    #@68
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v1

    #@6c
    sget-object v2, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@6e
    iget-object v2, v2, Lcom/lge/ims/service/ac/ACConfiguration;->mPhoneNumber:Ljava/lang/String;

    #@70
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v1

    #@74
    const-string v2, ", IMSI="

    #@76
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v1

    #@7a
    sget-object v2, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@7c
    iget-object v2, v2, Lcom/lge/ims/service/ac/ACConfiguration;->mIMSI:Ljava/lang/String;

    #@7e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v1

    #@82
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v1

    #@86
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@89
    .line 66
    const-string v0, "ACConfiguration"

    #@8b
    new-instance v1, Ljava/lang/StringBuilder;

    #@8d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@90
    const-string v2, "ConnectionServer="

    #@92
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v1

    #@96
    sget-object v2, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@98
    iget v2, v2, Lcom/lge/ims/service/ac/ACConfiguration;->mConnectionServer:I

    #@9a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v1

    #@9e
    const-string v2, ", CommercialNetwork="

    #@a0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v1

    #@a4
    sget-object v2, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@a6
    iget-object v2, v2, Lcom/lge/ims/service/ac/ACConfiguration;->mCommercialNetwork:Ljava/lang/String;

    #@a8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v1

    #@ac
    const-string v2, ", Testbed="

    #@ae
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v1

    #@b2
    sget-object v2, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@b4
    iget-object v2, v2, Lcom/lge/ims/service/ac/ACConfiguration;->mTestbed:Ljava/lang/String;

    #@b6
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v1

    #@ba
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bd
    move-result-object v1

    #@be
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@c1
    goto/16 :goto_4
.end method

.method private initForCommon(Landroid/content/Context;)V
    .registers 11
    .parameter "context"

    #@0
    .prologue
    .line 165
    const/4 v7, 0x0

    #@1
    .line 168
    .local v7, cursor:Landroid/database/Cursor;
    :try_start_1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4
    move-result-object v0

    #@5
    sget-object v1, Lcom/lge/ims/provider/ac/AC$Settings$Common;->CONTENT_URI:Landroid/net/Uri;

    #@7
    const/4 v2, 0x0

    #@8
    const/4 v3, 0x0

    #@9
    const/4 v4, 0x0

    #@a
    const/4 v5, 0x0

    #@b
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@e
    move-result-object v7

    #@f
    .line 170
    if-eqz v7, :cond_25

    #@11
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    #@14
    move-result v0

    #@15
    if-eqz v0, :cond_25

    #@17
    .line 171
    const-string v0, "update_exception_list"

    #@19
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@1c
    move-result v6

    #@1d
    .line 173
    .local v6, column:I
    if-ltz v6, :cond_25

    #@1f
    .line 174
    invoke-interface {v7, v6}, Landroid/database/Cursor;->getInt(I)I

    #@22
    move-result v0

    #@23
    iput v0, p0, Lcom/lge/ims/service/ac/ACConfiguration;->mUpdateExceptionList:I
    :try_end_25
    .catchall {:try_start_1 .. :try_end_25} :catchall_51
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_25} :catch_2b
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_25} :catch_3e

    #@25
    .line 184
    .end local v6           #column:I
    :cond_25
    if-eqz v7, :cond_2a

    #@27
    .line 185
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@2a
    .line 188
    :cond_2a
    :goto_2a
    return-void

    #@2b
    .line 177
    :catch_2b
    move-exception v8

    #@2c
    .line 178
    .local v8, e:Landroid/database/sqlite/SQLiteException;
    :try_start_2c
    const-string v0, "ACConfiguration"

    #@2e
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    #@31
    move-result-object v1

    #@32
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@35
    .line 179
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_38
    .catchall {:try_start_2c .. :try_end_38} :catchall_51

    #@38
    .line 184
    if-eqz v7, :cond_2a

    #@3a
    .line 185
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@3d
    goto :goto_2a

    #@3e
    .line 180
    .end local v8           #e:Landroid/database/sqlite/SQLiteException;
    :catch_3e
    move-exception v8

    #@3f
    .line 181
    .local v8, e:Ljava/lang/Exception;
    :try_start_3f
    const-string v0, "ACConfiguration"

    #@41
    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@44
    move-result-object v1

    #@45
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@48
    .line 182
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4b
    .catchall {:try_start_3f .. :try_end_4b} :catchall_51

    #@4b
    .line 184
    if-eqz v7, :cond_2a

    #@4d
    .line 185
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@50
    goto :goto_2a

    #@51
    .line 184
    .end local v8           #e:Ljava/lang/Exception;
    :catchall_51
    move-exception v0

    #@52
    if-eqz v7, :cond_57

    #@54
    .line 185
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@57
    :cond_57
    throw v0
.end method

.method private initForConnectionServer(Landroid/content/Context;)V
    .registers 12
    .parameter "context"

    #@0
    .prologue
    .line 235
    const/4 v7, 0x0

    #@1
    .line 238
    .local v7, cursor:Landroid/database/Cursor;
    :try_start_1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4
    move-result-object v0

    #@5
    sget-object v1, Lcom/lge/ims/provider/ac/AC$Settings$ConnectionServer;->CONTENT_URI:Landroid/net/Uri;

    #@7
    const/4 v2, 0x0

    #@8
    const/4 v3, 0x0

    #@9
    const/4 v4, 0x0

    #@a
    const/4 v5, 0x0

    #@b
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@e
    move-result-object v7

    #@f
    .line 240
    if-eqz v7, :cond_4a

    #@11
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    #@14
    move-result v0

    #@15
    if-eqz v0, :cond_4a

    #@17
    .line 241
    const-string v0, "server_selection"

    #@19
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@1c
    move-result v6

    #@1d
    .line 243
    .local v6, column:I
    if-ltz v6, :cond_2e

    #@1f
    .line 244
    invoke-interface {v7, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@22
    move-result-object v9

    #@23
    .line 246
    .local v9, selection:Ljava/lang/String;
    const-string v0, "0"

    #@25
    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28
    move-result v0

    #@29
    if-nez v0, :cond_2e

    #@2b
    .line 247
    const/4 v0, 0x1

    #@2c
    iput v0, p0, Lcom/lge/ims/service/ac/ACConfiguration;->mConnectionServer:I

    #@2e
    .line 252
    .end local v9           #selection:Ljava/lang/String;
    :cond_2e
    const-string v0, "commercial_network"

    #@30
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@33
    move-result v6

    #@34
    .line 254
    if-ltz v6, :cond_3c

    #@36
    .line 255
    invoke-interface {v7, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@39
    move-result-object v0

    #@3a
    iput-object v0, p0, Lcom/lge/ims/service/ac/ACConfiguration;->mCommercialNetwork:Ljava/lang/String;

    #@3c
    .line 259
    :cond_3c
    const-string v0, "testbed"

    #@3e
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@41
    move-result v6

    #@42
    .line 261
    if-ltz v6, :cond_4a

    #@44
    .line 262
    invoke-interface {v7, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@47
    move-result-object v0

    #@48
    iput-object v0, p0, Lcom/lge/ims/service/ac/ACConfiguration;->mTestbed:Ljava/lang/String;
    :try_end_4a
    .catchall {:try_start_1 .. :try_end_4a} :catchall_76
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_4a} :catch_50
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_4a} :catch_63

    #@4a
    .line 272
    .end local v6           #column:I
    :cond_4a
    if-eqz v7, :cond_4f

    #@4c
    .line 273
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@4f
    .line 276
    :cond_4f
    :goto_4f
    return-void

    #@50
    .line 265
    :catch_50
    move-exception v8

    #@51
    .line 266
    .local v8, e:Landroid/database/sqlite/SQLiteException;
    :try_start_51
    const-string v0, "ACConfiguration"

    #@53
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    #@56
    move-result-object v1

    #@57
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@5a
    .line 267
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_5d
    .catchall {:try_start_51 .. :try_end_5d} :catchall_76

    #@5d
    .line 272
    if-eqz v7, :cond_4f

    #@5f
    .line 273
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@62
    goto :goto_4f

    #@63
    .line 268
    .end local v8           #e:Landroid/database/sqlite/SQLiteException;
    :catch_63
    move-exception v8

    #@64
    .line 269
    .local v8, e:Ljava/lang/Exception;
    :try_start_64
    const-string v0, "ACConfiguration"

    #@66
    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@69
    move-result-object v1

    #@6a
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@6d
    .line 270
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_70
    .catchall {:try_start_64 .. :try_end_70} :catchall_76

    #@70
    .line 272
    if-eqz v7, :cond_4f

    #@72
    .line 273
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@75
    goto :goto_4f

    #@76
    .line 272
    .end local v8           #e:Ljava/lang/Exception;
    :catchall_76
    move-exception v0

    #@77
    if-eqz v7, :cond_7c

    #@79
    .line 273
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@7c
    :cond_7c
    throw v0
.end method

.method private initForSubscriber(Landroid/content/Context;)V
    .registers 12
    .parameter "context"

    #@0
    .prologue
    .line 191
    const/4 v7, 0x0

    #@1
    .line 194
    .local v7, cursor:Landroid/database/Cursor;
    :try_start_1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4
    move-result-object v0

    #@5
    sget-object v1, Lcom/lge/ims/provider/ac/AC$Settings$Subscriber;->CONTENT_URI:Landroid/net/Uri;

    #@7
    const/4 v2, 0x0

    #@8
    const/4 v3, 0x0

    #@9
    const/4 v4, 0x0

    #@a
    const/4 v5, 0x0

    #@b
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@e
    move-result-object v7

    #@f
    .line 196
    if-eqz v7, :cond_4a

    #@11
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    #@14
    move-result v0

    #@15
    if-eqz v0, :cond_4a

    #@17
    .line 197
    const-string v0, "usim"

    #@19
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@1c
    move-result v6

    #@1d
    .line 199
    .local v6, column:I
    if-ltz v6, :cond_2e

    #@1f
    .line 200
    invoke-interface {v7, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@22
    move-result-object v9

    #@23
    .line 202
    .local v9, usim:Ljava/lang/String;
    const-string v0, "true"

    #@25
    invoke-virtual {v0, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@28
    move-result v0

    #@29
    if-nez v0, :cond_2e

    #@2b
    .line 203
    const/4 v0, 0x0

    #@2c
    iput-boolean v0, p0, Lcom/lge/ims/service/ac/ACConfiguration;->mUsimOn:Z

    #@2e
    .line 208
    .end local v9           #usim:Ljava/lang/String;
    :cond_2e
    const-string v0, "phone_number"

    #@30
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@33
    move-result v6

    #@34
    .line 210
    if-ltz v6, :cond_3c

    #@36
    .line 211
    invoke-interface {v7, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@39
    move-result-object v0

    #@3a
    iput-object v0, p0, Lcom/lge/ims/service/ac/ACConfiguration;->mPhoneNumber:Ljava/lang/String;

    #@3c
    .line 215
    :cond_3c
    const-string v0, "imsi"

    #@3e
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@41
    move-result v6

    #@42
    .line 217
    if-ltz v6, :cond_4a

    #@44
    .line 218
    invoke-interface {v7, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@47
    move-result-object v0

    #@48
    iput-object v0, p0, Lcom/lge/ims/service/ac/ACConfiguration;->mIMSI:Ljava/lang/String;
    :try_end_4a
    .catchall {:try_start_1 .. :try_end_4a} :catchall_76
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_4a} :catch_50
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_4a} :catch_63

    #@4a
    .line 228
    .end local v6           #column:I
    :cond_4a
    if-eqz v7, :cond_4f

    #@4c
    .line 229
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@4f
    .line 232
    :cond_4f
    :goto_4f
    return-void

    #@50
    .line 221
    :catch_50
    move-exception v8

    #@51
    .line 222
    .local v8, e:Landroid/database/sqlite/SQLiteException;
    :try_start_51
    const-string v0, "ACConfiguration"

    #@53
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    #@56
    move-result-object v1

    #@57
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@5a
    .line 223
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_5d
    .catchall {:try_start_51 .. :try_end_5d} :catchall_76

    #@5d
    .line 228
    if-eqz v7, :cond_4f

    #@5f
    .line 229
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@62
    goto :goto_4f

    #@63
    .line 224
    .end local v8           #e:Landroid/database/sqlite/SQLiteException;
    :catch_63
    move-exception v8

    #@64
    .line 225
    .local v8, e:Ljava/lang/Exception;
    :try_start_64
    const-string v0, "ACConfiguration"

    #@66
    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@69
    move-result-object v1

    #@6a
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@6d
    .line 226
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_70
    .catchall {:try_start_64 .. :try_end_70} :catchall_76

    #@70
    .line 228
    if-eqz v7, :cond_4f

    #@72
    .line 229
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@75
    goto :goto_4f

    #@76
    .line 228
    .end local v8           #e:Ljava/lang/Exception;
    :catchall_76
    move-exception v0

    #@77
    if-eqz v7, :cond_7c

    #@79
    .line 229
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@7c
    :cond_7c
    throw v0
.end method

.method public static isProxyAddressUpdateRequired()Z
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 144
    sget-object v1, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@3
    if-nez v1, :cond_6

    #@5
    .line 153
    :cond_5
    :goto_5
    return v0

    #@6
    .line 149
    :cond_6
    sget-object v1, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@8
    iget v1, v1, Lcom/lge/ims/service/ac/ACConfiguration;->mUpdateExceptionList:I

    #@a
    and-int/lit8 v1, v1, 0x1

    #@c
    if-eqz v1, :cond_5

    #@e
    .line 153
    const/4 v0, 0x0

    #@f
    goto :goto_5
.end method

.method public static isUsimOn()Z
    .registers 1

    #@0
    .prologue
    .line 157
    sget-object v0, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 158
    const/4 v0, 0x1

    #@5
    .line 161
    :goto_5
    return v0

    #@6
    :cond_6
    sget-object v0, Lcom/lge/ims/service/ac/ACConfiguration;->mConfiguration:Lcom/lge/ims/service/ac/ACConfiguration;

    #@8
    iget-boolean v0, v0, Lcom/lge/ims/service/ac/ACConfiguration;->mUsimOn:Z

    #@a
    goto :goto_5
.end method
