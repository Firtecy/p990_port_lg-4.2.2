.class public Lcom/lge/ims/service/ac/ACContentCache;
.super Ljava/lang/Object;
.source "ACContentCache.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ACContentCache"


# instance fields
.field public mProxyAddress:Lcom/lge/ims/service/ac/content/ACProxyAddress;

.field public mServiceAvailability:Lcom/lge/ims/service/ac/content/ACServiceAvailability;

.field public mSubscriber:Lcom/lge/ims/service/ac/content/ACSubscriber;

.field public mVersion:Lcom/lge/ims/service/ac/content/ACVersion;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 22
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 15
    new-instance v0, Lcom/lge/ims/service/ac/content/ACVersion;

    #@5
    invoke-direct {v0}, Lcom/lge/ims/service/ac/content/ACVersion;-><init>()V

    #@8
    iput-object v0, p0, Lcom/lge/ims/service/ac/ACContentCache;->mVersion:Lcom/lge/ims/service/ac/content/ACVersion;

    #@a
    .line 16
    new-instance v0, Lcom/lge/ims/service/ac/content/ACSubscriber;

    #@c
    invoke-direct {v0}, Lcom/lge/ims/service/ac/content/ACSubscriber;-><init>()V

    #@f
    iput-object v0, p0, Lcom/lge/ims/service/ac/ACContentCache;->mSubscriber:Lcom/lge/ims/service/ac/content/ACSubscriber;

    #@11
    .line 17
    new-instance v0, Lcom/lge/ims/service/ac/content/ACProxyAddress;

    #@13
    invoke-direct {v0}, Lcom/lge/ims/service/ac/content/ACProxyAddress;-><init>()V

    #@16
    iput-object v0, p0, Lcom/lge/ims/service/ac/ACContentCache;->mProxyAddress:Lcom/lge/ims/service/ac/content/ACProxyAddress;

    #@18
    .line 18
    new-instance v0, Lcom/lge/ims/service/ac/content/ACServiceAvailability;

    #@1a
    invoke-direct {v0}, Lcom/lge/ims/service/ac/content/ACServiceAvailability;-><init>()V

    #@1d
    iput-object v0, p0, Lcom/lge/ims/service/ac/ACContentCache;->mServiceAvailability:Lcom/lge/ims/service/ac/content/ACServiceAvailability;

    #@1f
    .line 23
    return-void
.end method


# virtual methods
.method public clear()V
    .registers 2

    #@0
    .prologue
    .line 26
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACContentCache;->mVersion:Lcom/lge/ims/service/ac/content/ACVersion;

    #@2
    invoke-virtual {v0}, Lcom/lge/ims/service/ac/content/ACVersion;->clear()V

    #@5
    .line 27
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACContentCache;->mSubscriber:Lcom/lge/ims/service/ac/content/ACSubscriber;

    #@7
    invoke-virtual {v0}, Lcom/lge/ims/service/ac/content/ACSubscriber;->clear()V

    #@a
    .line 28
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACContentCache;->mProxyAddress:Lcom/lge/ims/service/ac/content/ACProxyAddress;

    #@c
    invoke-virtual {v0}, Lcom/lge/ims/service/ac/content/ACProxyAddress;->clear()V

    #@f
    .line 29
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACContentCache;->mServiceAvailability:Lcom/lge/ims/service/ac/content/ACServiceAvailability;

    #@11
    invoke-virtual {v0}, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->clear()V

    #@14
    .line 30
    return-void
.end method

.method public displayContent()V
    .registers 4

    #@0
    .prologue
    .line 33
    const-string v0, "ACContentCache"

    #@2
    const-string v1, "ContentCache -- starts"

    #@4
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 35
    const-string v0, "ACContentCache"

    #@9
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v2, "ACVersion :: "

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    iget-object v2, p0, Lcom/lge/ims/service/ac/ACContentCache;->mVersion:Lcom/lge/ims/service/ac/content/ACVersion;

    #@16
    invoke-virtual {v2}, Lcom/lge/ims/service/ac/content/ACVersion;->toString()Ljava/lang/String;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@25
    .line 36
    const-string v0, "ACContentCache"

    #@27
    new-instance v1, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v2, "ACSubscriber :: "

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    iget-object v2, p0, Lcom/lge/ims/service/ac/ACContentCache;->mSubscriber:Lcom/lge/ims/service/ac/content/ACSubscriber;

    #@34
    invoke-virtual {v2}, Lcom/lge/ims/service/ac/content/ACSubscriber;->toString()Ljava/lang/String;

    #@37
    move-result-object v2

    #@38
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v1

    #@3c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v1

    #@40
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@43
    .line 37
    const-string v0, "ACContentCache"

    #@45
    new-instance v1, Ljava/lang/StringBuilder;

    #@47
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@4a
    const-string v2, "ACProxyAddress :: "

    #@4c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v1

    #@50
    iget-object v2, p0, Lcom/lge/ims/service/ac/ACContentCache;->mProxyAddress:Lcom/lge/ims/service/ac/content/ACProxyAddress;

    #@52
    invoke-virtual {v2}, Lcom/lge/ims/service/ac/content/ACProxyAddress;->toString()Ljava/lang/String;

    #@55
    move-result-object v2

    #@56
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v1

    #@5a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object v1

    #@5e
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@61
    .line 38
    const-string v0, "ACContentCache"

    #@63
    new-instance v1, Ljava/lang/StringBuilder;

    #@65
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@68
    const-string v2, "ACServiceAvailability :: "

    #@6a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v1

    #@6e
    iget-object v2, p0, Lcom/lge/ims/service/ac/ACContentCache;->mServiceAvailability:Lcom/lge/ims/service/ac/content/ACServiceAvailability;

    #@70
    invoke-virtual {v2}, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->toString()Ljava/lang/String;

    #@73
    move-result-object v2

    #@74
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v1

    #@78
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v1

    #@7c
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7f
    .line 40
    const-string v0, "ACContentCache"

    #@81
    const-string v1, "ContentCache -- ends"

    #@83
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@86
    .line 41
    return-void
.end method

.method public getProxyAddress()Lcom/lge/ims/service/ac/content/ACProxyAddress;
    .registers 2

    #@0
    .prologue
    .line 44
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACContentCache;->mProxyAddress:Lcom/lge/ims/service/ac/content/ACProxyAddress;

    #@2
    return-object v0
.end method

.method public getServiceAvailability()Lcom/lge/ims/service/ac/content/ACServiceAvailability;
    .registers 2

    #@0
    .prologue
    .line 48
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACContentCache;->mServiceAvailability:Lcom/lge/ims/service/ac/content/ACServiceAvailability;

    #@2
    return-object v0
.end method

.method public getSubscriber()Lcom/lge/ims/service/ac/content/ACSubscriber;
    .registers 2

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACContentCache;->mSubscriber:Lcom/lge/ims/service/ac/content/ACSubscriber;

    #@2
    return-object v0
.end method

.method public getVersion()Lcom/lge/ims/service/ac/content/ACVersion;
    .registers 2

    #@0
    .prologue
    .line 56
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACContentCache;->mVersion:Lcom/lge/ims/service/ac/content/ACVersion;

    #@2
    return-object v0
.end method

.method public hasValidContent()Z
    .registers 2

    #@0
    .prologue
    .line 60
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACContentCache;->mVersion:Lcom/lge/ims/service/ac/content/ACVersion;

    #@2
    invoke-virtual {v0}, Lcom/lge/ims/service/ac/content/ACVersion;->isContentPresent()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_20

    #@8
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACContentCache;->mSubscriber:Lcom/lge/ims/service/ac/content/ACSubscriber;

    #@a
    invoke-virtual {v0}, Lcom/lge/ims/service/ac/content/ACSubscriber;->isContentPresent()Z

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_20

    #@10
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACContentCache;->mProxyAddress:Lcom/lge/ims/service/ac/content/ACProxyAddress;

    #@12
    invoke-virtual {v0}, Lcom/lge/ims/service/ac/content/ACProxyAddress;->isContentPresent()Z

    #@15
    move-result v0

    #@16
    if-nez v0, :cond_20

    #@18
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACContentCache;->mServiceAvailability:Lcom/lge/ims/service/ac/content/ACServiceAvailability;

    #@1a
    invoke-virtual {v0}, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->isContentPresent()Z

    #@1d
    move-result v0

    #@1e
    if-eqz v0, :cond_22

    #@20
    :cond_20
    const/4 v0, 0x1

    #@21
    :goto_21
    return v0

    #@22
    :cond_22
    const/4 v0, 0x0

    #@23
    goto :goto_21
.end method
