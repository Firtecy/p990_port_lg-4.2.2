.class Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;
.super Ljava/lang/Object;
.source "IPAgent.java"

# interfaces
.implements Lcom/lge/ims/JNICoreListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/ip/IPAgent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "IPAgentImpl"
.end annotation


# instance fields
.field private imServiceMngr:Lcom/lge/ims/service/im/IMServiceManager;

.field private nUsePresence:I

.field private nUserPresenceSocialInformation:I

.field private nativeObj:I

.field private objCapabilityQuery:Lcom/lge/ims/service/cd/CapabilityQuery;

.field private objCapabilityService:Lcom/lge/ims/service/cd/CapabilityService;

.field final synthetic this$0:Lcom/lge/ims/service/ip/IPAgent;


# direct methods
.method public constructor <init>(Lcom/lge/ims/service/ip/IPAgent;)V
    .registers 4
    .parameter

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x0

    #@2
    .line 2035
    iput-object p1, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    .line 2030
    iput-object v0, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->objCapabilityService:Lcom/lge/ims/service/cd/CapabilityService;

    #@9
    .line 2031
    iput-object v0, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->objCapabilityQuery:Lcom/lge/ims/service/cd/CapabilityQuery;

    #@b
    .line 2032
    iput-object v0, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->imServiceMngr:Lcom/lge/ims/service/im/IMServiceManager;

    #@d
    .line 2033
    iput v1, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nUsePresence:I

    #@f
    .line 2034
    iput v1, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nUserPresenceSocialInformation:I

    #@11
    .line 2036
    const-string v0, "[IP]"

    #@13
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@16
    .line 2037
    return-void
.end method

.method private SendAddRcsUserListCmd(Ljava/util/List;)V
    .registers 9
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p1, objList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const/16 v6, 0x32

    #@2
    const/4 v5, 0x0

    #@3
    .line 3090
    new-instance v3, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v4, "[IP] : size["

    #@a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v3

    #@e
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@11
    move-result v4

    #@12
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    const-string v4, "]"

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v3

    #@20
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@23
    .line 3091
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@26
    move-result v3

    #@27
    if-gtz v3, :cond_2a

    #@29
    .line 3121
    :goto_29
    return-void

    #@2a
    .line 3094
    :cond_2a
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@2d
    move-result-object v2

    #@2e
    .line 3095
    .local v2, parcel:Landroid/os/Parcel;
    const/16 v3, 0x2a5e

    #@30
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@33
    .line 3096
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@36
    move-result v3

    #@37
    if-le v3, v6, :cond_63

    #@39
    .line 3097
    const/4 v3, 0x1

    #@3a
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@3d
    .line 3098
    invoke-virtual {v2, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@40
    .line 3099
    const/4 v1, 0x0

    #@41
    .local v1, n:I
    :goto_41
    if-ge v1, v6, :cond_52

    #@43
    .line 3100
    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@46
    move-result-object v3

    #@47
    check-cast v3, Ljava/lang/String;

    #@49
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@4c
    .line 3101
    invoke-interface {p1, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    #@4f
    .line 3099
    add-int/lit8 v1, v1, 0x1

    #@51
    goto :goto_41

    #@52
    .line 3103
    :cond_52
    invoke-virtual {v2}, Landroid/os/Parcel;->marshall()[B

    #@55
    move-result-object v0

    #@56
    .line 3104
    .local v0, baData:[B
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@59
    .line 3105
    const/4 v2, 0x0

    #@5a
    .line 3106
    iget v3, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nativeObj:I

    #@5c
    invoke-static {v3, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@5f
    .line 3107
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->SendAddRcsUserListCmd(Ljava/util/List;)V

    #@62
    goto :goto_29

    #@63
    .line 3109
    .end local v0           #baData:[B
    .end local v1           #n:I
    :cond_63
    invoke-virtual {v2, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@66
    .line 3110
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@69
    move-result v3

    #@6a
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@6d
    .line 3111
    const/4 v1, 0x0

    #@6e
    .restart local v1       #n:I
    :goto_6e
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@71
    move-result v3

    #@72
    if-ge v1, v3, :cond_80

    #@74
    .line 3112
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@77
    move-result-object v3

    #@78
    check-cast v3, Ljava/lang/String;

    #@7a
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@7d
    .line 3111
    add-int/lit8 v1, v1, 0x1

    #@7f
    goto :goto_6e

    #@80
    .line 3114
    :cond_80
    invoke-interface {p1}, Ljava/util/List;->clear()V

    #@83
    .line 3116
    invoke-virtual {v2}, Landroid/os/Parcel;->marshall()[B

    #@86
    move-result-object v0

    #@87
    .line 3117
    .restart local v0       #baData:[B
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@8a
    .line 3118
    const/4 v2, 0x0

    #@8b
    .line 3119
    iget v3, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nativeObj:I

    #@8d
    invoke-static {v3, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@90
    goto :goto_29
.end method

.method private SendDeleteRcsUserListCmd(Ljava/util/List;)V
    .registers 9
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p1, objList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const/4 v6, 0x0

    #@1
    const/16 v5, 0x32

    #@3
    .line 3123
    new-instance v3, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v4, "[IP] : size["

    #@a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v3

    #@e
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@11
    move-result v4

    #@12
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    const-string v4, "]"

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v3

    #@20
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@23
    .line 3124
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@26
    move-result v3

    #@27
    if-gtz v3, :cond_2a

    #@29
    .line 3152
    :goto_29
    return-void

    #@2a
    .line 3127
    :cond_2a
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@2d
    move-result-object v2

    #@2e
    .line 3128
    .local v2, parcel:Landroid/os/Parcel;
    const/16 v3, 0x2a5f

    #@30
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@33
    .line 3129
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@36
    move-result v3

    #@37
    if-le v3, v5, :cond_5f

    #@39
    .line 3130
    invoke-virtual {v2, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@3c
    .line 3131
    const/4 v1, 0x0

    #@3d
    .local v1, n:I
    :goto_3d
    if-ge v1, v5, :cond_4e

    #@3f
    .line 3132
    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@42
    move-result-object v3

    #@43
    check-cast v3, Ljava/lang/String;

    #@45
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@48
    .line 3133
    invoke-interface {p1, v6}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    #@4b
    .line 3131
    add-int/lit8 v1, v1, 0x1

    #@4d
    goto :goto_3d

    #@4e
    .line 3135
    :cond_4e
    invoke-virtual {v2}, Landroid/os/Parcel;->marshall()[B

    #@51
    move-result-object v0

    #@52
    .line 3136
    .local v0, baData:[B
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@55
    .line 3137
    const/4 v2, 0x0

    #@56
    .line 3138
    iget v3, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nativeObj:I

    #@58
    invoke-static {v3, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@5b
    .line 3139
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->SendDeleteRcsUserListCmd(Ljava/util/List;)V

    #@5e
    goto :goto_29

    #@5f
    .line 3141
    .end local v0           #baData:[B
    .end local v1           #n:I
    :cond_5f
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@62
    move-result v3

    #@63
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@66
    .line 3142
    const/4 v1, 0x0

    #@67
    .restart local v1       #n:I
    :goto_67
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@6a
    move-result v3

    #@6b
    if-ge v1, v3, :cond_79

    #@6d
    .line 3143
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@70
    move-result-object v3

    #@71
    check-cast v3, Ljava/lang/String;

    #@73
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@76
    .line 3142
    add-int/lit8 v1, v1, 0x1

    #@78
    goto :goto_67

    #@79
    .line 3145
    :cond_79
    invoke-interface {p1}, Ljava/util/List;->clear()V

    #@7c
    .line 3147
    invoke-virtual {v2}, Landroid/os/Parcel;->marshall()[B

    #@7f
    move-result-object v0

    #@80
    .line 3148
    .restart local v0       #baData:[B
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@83
    .line 3149
    const/4 v2, 0x0

    #@84
    .line 3150
    iget v3, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nativeObj:I

    #@86
    invoke-static {v3, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@89
    goto :goto_29
.end method

.method private SendSyncAddRcsUserListCmd(Ljava/util/List;)V
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p1, objList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const/4 v5, 0x0

    #@1
    .line 3070
    new-instance v3, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v4, "[IP] : size["

    #@8
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v3

    #@c
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@f
    move-result v4

    #@10
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v3

    #@14
    const-string v4, "]"

    #@16
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v3

    #@1e
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@21
    .line 3071
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@24
    move-result v3

    #@25
    if-gtz v3, :cond_28

    #@27
    .line 3088
    :cond_27
    return-void

    #@28
    .line 3074
    :cond_28
    iget v3, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nUsePresence:I

    #@2a
    if-eqz v3, :cond_27

    #@2c
    .line 3077
    const/4 v1, 0x0

    #@2d
    .local v1, i:I
    :goto_2d
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@30
    move-result v3

    #@31
    if-ge v1, v3, :cond_27

    #@33
    .line 3078
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@36
    move-result-object v2

    #@37
    .line 3079
    .local v2, parcel:Landroid/os/Parcel;
    const/16 v3, 0x2a58

    #@39
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@3c
    .line 3080
    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@3f
    move-result-object v3

    #@40
    check-cast v3, Ljava/lang/String;

    #@42
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@45
    .line 3081
    invoke-virtual {v2, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@48
    .line 3082
    invoke-interface {p1, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    #@4b
    .line 3083
    invoke-virtual {v2}, Landroid/os/Parcel;->marshall()[B

    #@4e
    move-result-object v0

    #@4f
    .line 3084
    .local v0, baData:[B
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@52
    .line 3085
    const/4 v2, 0x0

    #@53
    .line 3086
    iget v3, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nativeObj:I

    #@55
    invoke-static {v3, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@58
    .line 3077
    add-int/lit8 v1, v1, 0x1

    #@5a
    goto :goto_2d
.end method


# virtual methods
.method public AddNonRCSFriend(Ljava/lang/String;)V
    .registers 6
    .parameter "strMSISDN"

    #@0
    .prologue
    .line 2940
    iget v2, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nUsePresence:I

    #@2
    if-nez v2, :cond_5

    #@4
    .line 2951
    :goto_4
    return-void

    #@5
    .line 2943
    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v3, "[IP]: strMSISDN ["

    #@c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    const-string v3, "]"

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@21
    .line 2944
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@24
    move-result-object v1

    #@25
    .line 2945
    .local v1, parcel:Landroid/os/Parcel;
    const/16 v2, 0x2a5c

    #@27
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@2a
    .line 2946
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@2d
    .line 2947
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@30
    move-result-object v0

    #@31
    .line 2948
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@34
    .line 2949
    const/4 v1, 0x0

    #@35
    .line 2950
    iget v2, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nativeObj:I

    #@37
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@3a
    goto :goto_4
.end method

.method public AddRCSFriend(Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "strMSISDN"
    .parameter "strDisplayName"

    #@0
    .prologue
    .line 2921
    iget v2, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nUsePresence:I

    #@2
    if-nez v2, :cond_5

    #@4
    .line 2938
    :goto_4
    return-void

    #@5
    .line 2924
    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v3, "[IP]: MSISDN [ "

    #@c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    const-string v3, " ], Display Name [ "

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    const-string v3, " ]"

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@2b
    .line 2925
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@2e
    move-result-object v1

    #@2f
    .line 2926
    .local v1, parcel:Landroid/os/Parcel;
    const/16 v2, 0x2a58

    #@31
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@34
    .line 2927
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@37
    .line 2928
    if-eqz p2, :cond_3f

    #@39
    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    #@3c
    move-result v2

    #@3d
    if-eqz v2, :cond_51

    #@3f
    .line 2929
    :cond_3f
    const/4 v2, 0x0

    #@40
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@43
    .line 2934
    :goto_43
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@46
    move-result-object v0

    #@47
    .line 2935
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@4a
    .line 2936
    const/4 v1, 0x0

    #@4b
    .line 2937
    iget v2, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nativeObj:I

    #@4d
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@50
    goto :goto_4

    #@51
    .line 2931
    .end local v0           #baData:[B
    :cond_51
    const/4 v2, 0x1

    #@52
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@55
    .line 2932
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@58
    goto :goto_43
.end method

.method public AddRCSFriendList()V
    .registers 3

    #@0
    .prologue
    .line 2953
    iget v1, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nUsePresence:I

    #@2
    if-nez v1, :cond_5

    #@4
    .line 2960
    :goto_4
    return-void

    #@5
    .line 2956
    :cond_5
    const-string v1, "[IP]"

    #@7
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@a
    .line 2957
    new-instance v0, Ljava/util/ArrayList;

    #@c
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@f
    .line 2958
    .local v0, objList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@11
    invoke-static {v1}, Lcom/lge/ims/service/ip/IPAgent;->access$300(Lcom/lge/ims/service/ip/IPAgent;)Lcom/lge/ims/service/ip/IPContactHelper;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, v0}, Lcom/lge/ims/service/ip/IPContactHelper;->AddRCSUserToResourceList(Ljava/util/List;)V

    #@18
    .line 2959
    invoke-direct {p0, v0}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->SendAddRcsUserListCmd(Ljava/util/List;)V

    #@1b
    goto :goto_4
.end method

.method public ContactCapaQuery(J)V
    .registers 6
    .parameter "nContactId"

    #@0
    .prologue
    .line 2530
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@2
    invoke-static {v1}, Lcom/lge/ims/service/ip/IPAgent;->access$300(Lcom/lge/ims/service/ip/IPAgent;)Lcom/lge/ims/service/ip/IPContactHelper;

    #@5
    move-result-object v1

    #@6
    const/4 v2, 0x0

    #@7
    invoke-virtual {v1, p1, p2, v2}, Lcom/lge/ims/service/ip/IPContactHelper;->GetRCSContactMSISDNByDataId(JLjava/lang/String;)Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    .line 2531
    .local v0, strMSISDN:Ljava/lang/String;
    iget v1, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nUserPresenceSocialInformation:I

    #@d
    const/4 v2, 0x1

    #@e
    if-ne v1, v2, :cond_24

    #@10
    .line 2532
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@12
    invoke-static {v1}, Lcom/lge/ims/service/ip/IPAgent;->access$300(Lcom/lge/ims/service/ip/IPAgent;)Lcom/lge/ims/service/ip/IPContactHelper;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1, v0}, Lcom/lge/ims/service/ip/IPContactHelper;->IsRCSContact(Ljava/lang/String;)Z

    #@19
    move-result v1

    #@1a
    if-eqz v1, :cond_20

    #@1c
    .line 2533
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->SendPresenceCapaQuery(Ljava/lang/String;)V

    #@1f
    .line 2540
    :goto_1f
    return-void

    #@20
    .line 2535
    :cond_20
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->SendCapaQuery(Ljava/lang/String;)V

    #@23
    goto :goto_1f

    #@24
    .line 2538
    :cond_24
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->SendCapaQuery(Ljava/lang/String;)V

    #@27
    goto :goto_1f
.end method

.method public ContactCapaQuery(Ljava/lang/String;)V
    .registers 4
    .parameter "strMSISDN"

    #@0
    .prologue
    .line 2515
    if-eqz p1, :cond_8

    #@2
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_e

    #@8
    .line 2516
    :cond_8
    const-string v0, "[IP][ERROR]strMSISDN is null"

    #@a
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@d
    .line 2528
    :goto_d
    return-void

    #@e
    .line 2519
    :cond_e
    iget v0, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nUserPresenceSocialInformation:I

    #@10
    const/4 v1, 0x1

    #@11
    if-ne v0, v1, :cond_27

    #@13
    .line 2520
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@15
    invoke-static {v0}, Lcom/lge/ims/service/ip/IPAgent;->access$300(Lcom/lge/ims/service/ip/IPAgent;)Lcom/lge/ims/service/ip/IPContactHelper;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0, p1}, Lcom/lge/ims/service/ip/IPContactHelper;->IsRCSContact(Ljava/lang/String;)Z

    #@1c
    move-result v0

    #@1d
    if-eqz v0, :cond_23

    #@1f
    .line 2521
    invoke-virtual {p0, p1}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->SendPresenceCapaQuery(Ljava/lang/String;)V

    #@22
    goto :goto_d

    #@23
    .line 2523
    :cond_23
    invoke-virtual {p0, p1}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->SendCapaQuery(Ljava/lang/String;)V

    #@26
    goto :goto_d

    #@27
    .line 2526
    :cond_27
    invoke-virtual {p0, p1}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->SendCapaQuery(Ljava/lang/String;)V

    #@2a
    goto :goto_d
.end method

.method public DeleteMyStatusIcon()V
    .registers 4

    #@0
    .prologue
    .line 2801
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "[IP] : nUserPresenceSocialInformation [ "

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    iget v2, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nUserPresenceSocialInformation:I

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, "]"

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@1e
    .line 2802
    iget v1, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nUserPresenceSocialInformation:I

    #@20
    if-nez v1, :cond_23

    #@22
    .line 2812
    :goto_22
    return-void

    #@23
    .line 2805
    :cond_23
    new-instance v0, Lcom/lge/ims/service/ip/MyStatusInfo;

    #@25
    invoke-direct {v0}, Lcom/lge/ims/service/ip/MyStatusInfo;-><init>()V

    #@28
    .line 2807
    .local v0, objMyStatusInfo:Lcom/lge/ims/service/ip/MyStatusInfo;
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@2a
    invoke-static {v1}, Lcom/lge/ims/service/ip/IPAgent;->access$300(Lcom/lge/ims/service/ip/IPAgent;)Lcom/lge/ims/service/ip/IPContactHelper;

    #@2d
    move-result-object v1

    #@2e
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/IPContactHelper;->DeleteMyStatusIconInfo()Z

    #@31
    move-result v1

    #@32
    if-nez v1, :cond_3a

    #@34
    .line 2808
    const-string v1, "[IP][ERROR]UpdateMyStatus : DeleteMyStatusIconInfo is false"

    #@36
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@39
    goto :goto_22

    #@3a
    .line 2811
    :cond_3a
    invoke-virtual {p0}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->UpdateMyStatus()V

    #@3d
    goto :goto_22
.end method

.method public DeleteRCSFriend(Ljava/lang/String;)V
    .registers 6
    .parameter "strMSISDN"

    #@0
    .prologue
    .line 2962
    iget v2, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nUsePresence:I

    #@2
    if-nez v2, :cond_5

    #@4
    .line 2973
    :goto_4
    return-void

    #@5
    .line 2965
    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v3, "[IP]: strMSISDN ["

    #@c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    const-string v3, "]"

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@21
    .line 2966
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@24
    move-result-object v1

    #@25
    .line 2967
    .local v1, parcel:Landroid/os/Parcel;
    const/16 v2, 0x2a5a

    #@27
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@2a
    .line 2968
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@2d
    .line 2969
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@30
    move-result-object v0

    #@31
    .line 2970
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@34
    .line 2971
    const/4 v1, 0x0

    #@35
    .line 2972
    iget v2, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nativeObj:I

    #@37
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@3a
    goto :goto_4
.end method

.method public DeleteRCSFriendList(Lcom/lge/ims/service/ip/ResourceListUserInfo;)V
    .registers 7
    .parameter "objResourceListUserInfo"

    #@0
    .prologue
    .line 2975
    iget v4, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nUsePresence:I

    #@2
    if-nez v4, :cond_5

    #@4
    .line 2990
    :goto_4
    return-void

    #@5
    .line 2978
    :cond_5
    const-string v4, "[IP]"

    #@7
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@a
    .line 2979
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@d
    move-result-object v3

    #@e
    .line 2980
    .local v3, parcel:Landroid/os/Parcel;
    const/16 v4, 0x2a5f

    #@10
    invoke-virtual {v3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 2981
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/ResourceListUserInfo;->GetCurrentResourceListUserSize()I

    #@16
    move-result v2

    #@17
    .line 2982
    .local v2, nCurrentSize:I
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1a
    .line 2983
    const/4 v1, 0x0

    #@1b
    .local v1, n:I
    :goto_1b
    if-ge v1, v2, :cond_27

    #@1d
    .line 2984
    invoke-virtual {p1, v1}, Lcom/lge/ims/service/ip/ResourceListUserInfo;->GetResourceListUser(I)Ljava/lang/String;

    #@20
    move-result-object v4

    #@21
    invoke-virtual {v3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@24
    .line 2983
    add-int/lit8 v1, v1, 0x1

    #@26
    goto :goto_1b

    #@27
    .line 2986
    :cond_27
    invoke-virtual {v3}, Landroid/os/Parcel;->marshall()[B

    #@2a
    move-result-object v0

    #@2b
    .line 2987
    .local v0, baData:[B
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 2988
    const/4 v3, 0x0

    #@2f
    .line 2989
    iget v4, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nativeObj:I

    #@31
    invoke-static {v4, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@34
    goto :goto_4
.end method

.method public GetBuddyStatusIcon(Ljava/lang/String;)V
    .registers 7
    .parameter "szMSISDN"

    #@0
    .prologue
    .line 3046
    iget v3, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nUserPresenceSocialInformation:I

    #@2
    if-nez v3, :cond_5

    #@4
    .line 3067
    :cond_4
    :goto_4
    return-void

    #@5
    .line 3049
    :cond_5
    new-instance v3, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v4, "[IP]: szMSISDN ["

    #@c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v3

    #@10
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v3

    #@14
    const-string v4, "]"

    #@16
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v3

    #@1e
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@21
    .line 3050
    new-instance v1, Lcom/lge/ims/service/ip/PresenceInfo;

    #@23
    invoke-direct {v1}, Lcom/lge/ims/service/ip/PresenceInfo;-><init>()V

    #@26
    .line 3051
    .local v1, objBuddyPresenceInfo:Lcom/lge/ims/service/ip/PresenceInfo;
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@28
    invoke-static {v3}, Lcom/lge/ims/service/ip/IPAgent;->access$300(Lcom/lge/ims/service/ip/IPAgent;)Lcom/lge/ims/service/ip/IPContactHelper;

    #@2b
    move-result-object v3

    #@2c
    invoke-virtual {v3, v1, p1}, Lcom/lge/ims/service/ip/IPContactHelper;->QueryBuddyStatusIcon(Lcom/lge/ims/service/ip/PresenceInfo;Ljava/lang/String;)Z

    #@2f
    move-result v3

    #@30
    if-nez v3, :cond_38

    #@32
    .line 3052
    const-string v3, "[IP][ERROR]UpdateMyStatus : QueryMyStatusInfo is false"

    #@34
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@37
    goto :goto_4

    #@38
    .line 3055
    :cond_38
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIconLink()Ljava/lang/String;

    #@3b
    move-result-object v3

    #@3c
    if-eqz v3, :cond_4

    #@3e
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIconLink()Ljava/lang/String;

    #@41
    move-result-object v3

    #@42
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    #@45
    move-result v3

    #@46
    if-nez v3, :cond_4

    #@48
    .line 3056
    new-instance v3, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v4, "[IP]: StatusIconLink [ "

    #@4f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v3

    #@53
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIconLink()Ljava/lang/String;

    #@56
    move-result-object v4

    #@57
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v3

    #@5b
    const-string v4, " ]"

    #@5d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v3

    #@61
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v3

    #@65
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@68
    .line 3057
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@6b
    move-result-object v2

    #@6c
    .line 3058
    .local v2, parcel:Landroid/os/Parcel;
    const/16 v3, 0x2a62

    #@6e
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@71
    .line 3059
    invoke-virtual {v2, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@74
    .line 3060
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIconLink()Ljava/lang/String;

    #@77
    move-result-object v3

    #@78
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@7b
    .line 3061
    invoke-virtual {v2}, Landroid/os/Parcel;->marshall()[B

    #@7e
    move-result-object v0

    #@7f
    .line 3062
    .local v0, baData:[B
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@82
    .line 3063
    const/4 v2, 0x0

    #@83
    .line 3064
    iget v3, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nativeObj:I

    #@85
    invoke-static {v3, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@88
    .line 3065
    invoke-static {}, Lcom/lge/ims/service/ip/IPQueryRepository;->GetQueryRepository()Lcom/lge/ims/service/ip/IPQueryRepository;

    #@8b
    move-result-object v3

    #@8c
    invoke-virtual {v3, p1}, Lcom/lge/ims/service/ip/IPQueryRepository;->AddBuddyStatusIconQuery(Ljava/lang/String;)V

    #@8f
    goto/16 :goto_4
.end method

.method public GetBuddyStatusIcon(Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "szStatusIconUrl"
    .parameter "szMSISDN"

    #@0
    .prologue
    .line 3032
    iget v2, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nUserPresenceSocialInformation:I

    #@2
    if-nez v2, :cond_5

    #@4
    .line 3044
    :goto_4
    return-void

    #@5
    .line 3035
    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v3, "[IP]: szStatusIconUrl ["

    #@c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    const-string v3, "], szMSISDN ["

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    const-string v3, "]"

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@2b
    .line 3036
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@2e
    move-result-object v1

    #@2f
    .line 3037
    .local v1, parcel:Landroid/os/Parcel;
    const/16 v2, 0x2a62

    #@31
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@34
    .line 3038
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@37
    .line 3039
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@3a
    .line 3040
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@3d
    move-result-object v0

    #@3e
    .line 3041
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@41
    .line 3042
    const/4 v1, 0x0

    #@42
    .line 3043
    iget v2, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nativeObj:I

    #@44
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@47
    goto :goto_4
.end method

.method public GetBuddyStatusIconThumb(Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "szStatusIconThumbUrl"
    .parameter "szMSISDN"

    #@0
    .prologue
    .line 3018
    iget v2, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nUserPresenceSocialInformation:I

    #@2
    if-nez v2, :cond_5

    #@4
    .line 3030
    :goto_4
    return-void

    #@5
    .line 3021
    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v3, "[IP]: szStatusIconThumbUrl ["

    #@c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    const-string v3, "], szMSISDN ["

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    const-string v3, "]"

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@2b
    .line 3022
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@2e
    move-result-object v1

    #@2f
    .line 3023
    .local v1, parcel:Landroid/os/Parcel;
    const/16 v2, 0x2a60

    #@31
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@34
    .line 3024
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@37
    .line 3025
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@3a
    .line 3026
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@3d
    move-result-object v0

    #@3e
    .line 3027
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@41
    .line 3028
    const/4 v1, 0x0

    #@42
    .line 3029
    iget v2, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nativeObj:I

    #@44
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@47
    goto :goto_4
.end method

.method public GetMyStatusIcon(Ljava/lang/String;)V
    .registers 6
    .parameter "szStatusIconUrl"

    #@0
    .prologue
    .line 3005
    iget v2, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nUserPresenceSocialInformation:I

    #@2
    if-nez v2, :cond_5

    #@4
    .line 3016
    :goto_4
    return-void

    #@5
    .line 3008
    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v3, "[IP]: szStatusIconUrl ["

    #@c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    const-string v3, "]"

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@21
    .line 3009
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@24
    move-result-object v1

    #@25
    .line 3010
    .local v1, parcel:Landroid/os/Parcel;
    const/16 v2, 0x2a54

    #@27
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@2a
    .line 3011
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@2d
    .line 3012
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@30
    move-result-object v0

    #@31
    .line 3013
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@34
    .line 3014
    const/4 v1, 0x0

    #@35
    .line 3015
    iget v2, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nativeObj:I

    #@37
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@3a
    goto :goto_4
.end method

.method public GetMyStatusIconThumb(Ljava/lang/String;)V
    .registers 6
    .parameter "szStatusIconThumbUrl"

    #@0
    .prologue
    .line 2992
    iget v2, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nUserPresenceSocialInformation:I

    #@2
    if-nez v2, :cond_5

    #@4
    .line 3003
    :goto_4
    return-void

    #@5
    .line 2995
    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v3, "[IP]: szStatusIconThumbUrl ["

    #@c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    const-string v3, "]"

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@21
    .line 2996
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@24
    move-result-object v1

    #@25
    .line 2997
    .local v1, parcel:Landroid/os/Parcel;
    const/16 v2, 0x2a52

    #@27
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@2a
    .line 2998
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@2d
    .line 2999
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@30
    move-result-object v0

    #@31
    .line 3000
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@34
    .line 3001
    const/4 v1, 0x0

    #@35
    .line 3002
    iget v2, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nativeObj:I

    #@37
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@3a
    goto :goto_4
.end method

.method public InviteFriendToRCSService(Ljava/lang/String;)V
    .registers 5
    .parameter "strMSISDN"

    #@0
    .prologue
    .line 2899
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "[IP]: strMSISDN ["

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    const-string v2, "]"

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@1c
    .line 2900
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->imServiceMngr:Lcom/lge/ims/service/im/IMServiceManager;

    #@1e
    if-nez v1, :cond_26

    #@20
    .line 2901
    invoke-static {}, Lcom/lge/ims/service/im/IMServiceManager;->getInstance()Lcom/lge/ims/service/im/IMServiceManager;

    #@23
    move-result-object v1

    #@24
    iput-object v1, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->imServiceMngr:Lcom/lge/ims/service/im/IMServiceManager;

    #@26
    .line 2903
    :cond_26
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->imServiceMngr:Lcom/lge/ims/service/im/IMServiceManager;

    #@28
    if-eqz v1, :cond_35

    #@2a
    .line 2904
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->imServiceMngr:Lcom/lge/ims/service/im/IMServiceManager;

    #@2c
    invoke-virtual {v1}, Lcom/lge/ims/service/im/IMServiceManager;->getService()Lcom/lge/ims/service/im/IMServiceImpl;

    #@2f
    move-result-object v0

    #@30
    .line 2906
    .local v0, imService:Lcom/lge/ims/service/im/IMServiceImpl;
    if-eqz v0, :cond_35

    #@32
    .line 2907
    invoke-virtual {v0, p1}, Lcom/lge/ims/service/im/IMServiceImpl;->sendInvitationMessage(Ljava/lang/String;)V

    #@35
    .line 2919
    .end local v0           #imService:Lcom/lge/ims/service/im/IMServiceImpl;
    :cond_35
    return-void
.end method

.method public SendCapaQuery(Ljava/lang/String;)V
    .registers 4
    .parameter "strMSISDN"

    #@0
    .prologue
    .line 2542
    if-eqz p1, :cond_8

    #@2
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_e

    #@8
    .line 2543
    :cond_8
    const-string v0, "[IP][ERROR]strMSISDN is null"

    #@a
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@d
    .line 2589
    :cond_d
    :goto_d
    return-void

    #@e
    .line 2547
    :cond_e
    invoke-static {}, Lcom/lge/ims/service/ip/IPQueryRepository;->GetQueryRepository()Lcom/lge/ims/service/ip/IPQueryRepository;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {v0, p1}, Lcom/lge/ims/service/ip/IPQueryRepository;->HasQuery(Ljava/lang/String;)Z

    #@15
    move-result v0

    #@16
    if-eqz v0, :cond_35

    #@18
    .line 2548
    new-instance v0, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v1, "[IP][ERROR]SendCapaQuery : already send options("

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v0

    #@27
    const-string v1, ")"

    #@29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v0

    #@2d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v0

    #@31
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@34
    goto :goto_d

    #@35
    .line 2552
    :cond_35
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@37
    invoke-static {v0}, Lcom/lge/ims/service/ip/IPAgent;->access$300(Lcom/lge/ims/service/ip/IPAgent;)Lcom/lge/ims/service/ip/IPContactHelper;

    #@3a
    move-result-object v0

    #@3b
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@3d
    iget v1, v1, Lcom/lge/ims/service/ip/IPAgent;->nCapaInfoExpireTimer:I

    #@3f
    invoke-virtual {v0, p1, v1}, Lcom/lge/ims/service/ip/IPContactHelper;->IsValidCachingContact(Ljava/lang/String;I)Z

    #@42
    move-result v0

    #@43
    if-nez v0, :cond_d

    #@45
    .line 2579
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->objCapabilityService:Lcom/lge/ims/service/cd/CapabilityService;

    #@47
    if-nez v0, :cond_4f

    #@49
    .line 2580
    invoke-static {}, Lcom/lge/ims/service/cd/CapabilityService;->getInstance()Lcom/lge/ims/service/cd/CapabilityService;

    #@4c
    move-result-object v0

    #@4d
    iput-object v0, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->objCapabilityService:Lcom/lge/ims/service/cd/CapabilityService;

    #@4f
    .line 2582
    :cond_4f
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->objCapabilityService:Lcom/lge/ims/service/cd/CapabilityService;

    #@51
    if-eqz v0, :cond_d

    #@53
    .line 2583
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->objCapabilityService:Lcom/lge/ims/service/cd/CapabilityService;

    #@55
    invoke-virtual {v0}, Lcom/lge/ims/service/cd/CapabilityService;->createCapabilityQuery()Lcom/lge/ims/service/cd/CapabilityQuery;

    #@58
    move-result-object v0

    #@59
    iput-object v0, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->objCapabilityQuery:Lcom/lge/ims/service/cd/CapabilityQuery;

    #@5b
    .line 2584
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->objCapabilityQuery:Lcom/lge/ims/service/cd/CapabilityQuery;

    #@5d
    if-eqz v0, :cond_d

    #@5f
    .line 2585
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->objCapabilityQuery:Lcom/lge/ims/service/cd/CapabilityQuery;

    #@61
    invoke-virtual {v0, p1}, Lcom/lge/ims/service/cd/CapabilityQuery;->send(Ljava/lang/String;)Z

    #@64
    .line 2586
    invoke-static {}, Lcom/lge/ims/service/ip/IPQueryRepository;->GetQueryRepository()Lcom/lge/ims/service/ip/IPQueryRepository;

    #@67
    move-result-object v0

    #@68
    invoke-virtual {v0, p1}, Lcom/lge/ims/service/ip/IPQueryRepository;->AddQuery(Ljava/lang/String;)V

    #@6b
    goto :goto_d
.end method

.method public SendPresenceCapaQuery(Ljava/lang/String;)V
    .registers 7
    .parameter "strMSISDN"

    #@0
    .prologue
    .line 2592
    iget v3, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nUserPresenceSocialInformation:I

    #@2
    if-nez v3, :cond_a

    #@4
    .line 2593
    const-string v3, "[IP][ERROR]nUserPresenceSocialInformation is 0"

    #@6
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@9
    .line 2651
    :cond_9
    :goto_9
    return-void

    #@a
    .line 2596
    :cond_a
    if-eqz p1, :cond_12

    #@c
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    #@f
    move-result v3

    #@10
    if-eqz v3, :cond_18

    #@12
    .line 2597
    :cond_12
    const-string v3, "[IP][ERROR]strMSISDN is null"

    #@14
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@17
    goto :goto_9

    #@18
    .line 2601
    :cond_18
    invoke-static {}, Lcom/lge/ims/service/ip/IPQueryRepository;->GetQueryRepository()Lcom/lge/ims/service/ip/IPQueryRepository;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3, p1}, Lcom/lge/ims/service/ip/IPQueryRepository;->HasPresenceQuery(Ljava/lang/String;)Z

    #@1f
    move-result v3

    #@20
    if-eqz v3, :cond_3f

    #@22
    .line 2602
    new-instance v3, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v4, "[IP][ERROR]SendPresenceCapaQuery : already send one contact subscribe ("

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    const-string v4, ")"

    #@33
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v3

    #@3b
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@3e
    goto :goto_9

    #@3f
    .line 2636
    :cond_3f
    invoke-static {p1}, Lcom/lge/ims/service/ip/IPUtil;->NomalizeMSISDN(Ljava/lang/String;)Ljava/lang/String;

    #@42
    move-result-object v2

    #@43
    .line 2637
    .local v2, strModifiedMSISDN:Ljava/lang/String;
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@45
    invoke-static {v3}, Lcom/lge/ims/service/ip/IPAgent;->access$300(Lcom/lge/ims/service/ip/IPAgent;)Lcom/lge/ims/service/ip/IPContactHelper;

    #@48
    move-result-object v3

    #@49
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@4b
    iget v4, v4, Lcom/lge/ims/service/ip/IPAgent;->nOneContactIntervalTimer:I

    #@4d
    invoke-virtual {v3, v2, v4}, Lcom/lge/ims/service/ip/IPContactHelper;->IsValidPresenceCachingContact(Ljava/lang/String;I)Z

    #@50
    move-result v3

    #@51
    if-nez v3, :cond_9

    #@53
    .line 2640
    new-instance v3, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    const-string v4, "[IP]SendPresenceCapaQuery : "

    #@5a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v3

    #@5e
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v3

    #@62
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65
    move-result-object v3

    #@66
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@69
    .line 2641
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@6c
    move-result-object v1

    #@6d
    .line 2642
    .local v1, parcel:Landroid/os/Parcel;
    const/16 v3, 0x2a44

    #@6f
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@72
    .line 2643
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@75
    .line 2644
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@78
    move-result-object v0

    #@79
    .line 2645
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@7c
    .line 2646
    const/4 v1, 0x0

    #@7d
    .line 2648
    iget v3, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nativeObj:I

    #@7f
    invoke-static {v3, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@82
    .line 2650
    invoke-static {}, Lcom/lge/ims/service/ip/IPQueryRepository;->GetQueryRepository()Lcom/lge/ims/service/ip/IPQueryRepository;

    #@85
    move-result-object v3

    #@86
    invoke-virtual {v3, v2}, Lcom/lge/ims/service/ip/IPQueryRepository;->AddPresenceQuery(Ljava/lang/String;)V

    #@89
    goto :goto_9
.end method

.method public SendRLSListQuery()Z
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2653
    const-string v3, "[IP]"

    #@3
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 2654
    iget v3, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nUserPresenceSocialInformation:I

    #@8
    if-nez v3, :cond_10

    #@a
    .line 2655
    const-string v3, "[IP][ERROR]nUserPresenceSocialInformation is 0"

    #@c
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@f
    .line 2668
    :cond_f
    :goto_f
    return v2

    #@10
    .line 2658
    :cond_10
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@12
    invoke-static {v3}, Lcom/lge/ims/service/ip/IPAgent;->access$300(Lcom/lge/ims/service/ip/IPAgent;)Lcom/lge/ims/service/ip/IPContactHelper;

    #@15
    move-result-object v3

    #@16
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@18
    iget v4, v4, Lcom/lge/ims/service/ip/IPAgent;->nListCapanfoExpireTimer:I

    #@1a
    invoke-virtual {v3, v4}, Lcom/lge/ims/service/ip/IPContactHelper;->IsValidRLSSubCachingContact(I)Z

    #@1d
    move-result v3

    #@1e
    if-nez v3, :cond_f

    #@20
    .line 2662
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@23
    move-result-object v1

    #@24
    .line 2663
    .local v1, parcel:Landroid/os/Parcel;
    const/16 v2, 0x2a46

    #@26
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@29
    .line 2664
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@2c
    move-result-object v0

    #@2d
    .line 2665
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 2666
    const/4 v1, 0x0

    #@31
    .line 2667
    iget v2, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nativeObj:I

    #@33
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@36
    .line 2668
    const/4 v2, 0x1

    #@37
    goto :goto_f
.end method

.method public StartNativeConnection()V
    .registers 2

    #@0
    .prologue
    .line 2044
    const/16 v0, 0x29

    #@2
    invoke-static {v0}, Lcom/lge/ims/JNICore;->getInterface(I)I

    #@5
    move-result v0

    #@6
    iput v0, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nativeObj:I

    #@8
    .line 2045
    iget v0, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nativeObj:I

    #@a
    invoke-static {v0, p0}, Lcom/lge/ims/JNICore;->setListener(ILcom/lge/ims/JNICoreListener;)I

    #@d
    .line 2046
    return-void
.end method

.method public StartXDMAgent()V
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 2814
    iget v4, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nUsePresence:I

    #@4
    if-nez v4, :cond_7

    #@6
    .line 2874
    :goto_6
    return-void

    #@7
    .line 2817
    :cond_7
    new-instance v2, Lcom/lge/ims/service/ip/XDMEtagInfo;

    #@9
    invoke-direct {v2}, Lcom/lge/ims/service/ip/XDMEtagInfo;-><init>()V

    #@c
    .line 2818
    .local v2, objXdmEtagInfo:Lcom/lge/ims/service/ip/XDMEtagInfo;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@f
    move-result-object v3

    #@10
    .line 2819
    .local v3, parcel:Landroid/os/Parcel;
    const/16 v4, 0x2a6c

    #@12
    invoke-virtual {v3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 2820
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@17
    invoke-static {v4}, Lcom/lge/ims/service/ip/IPAgent;->access$300(Lcom/lge/ims/service/ip/IPAgent;)Lcom/lge/ims/service/ip/IPContactHelper;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v4, v2}, Lcom/lge/ims/service/ip/IPContactHelper;->GetDocumentETag(Lcom/lge/ims/service/ip/XDMEtagInfo;)Z

    #@1e
    move-result v4

    #@1f
    if-ne v4, v7, :cond_118

    #@21
    .line 2821
    new-instance v4, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v5, "[IP]StartXDMAgent : listETag:["

    #@28
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v4

    #@2c
    invoke-virtual {v2}, Lcom/lge/ims/service/ip/XDMEtagInfo;->GetListEtag()Ljava/lang/String;

    #@2f
    move-result-object v5

    #@30
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v4

    #@34
    const-string v5, "], rlsETag:["

    #@36
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v4

    #@3a
    invoke-virtual {v2}, Lcom/lge/ims/service/ip/XDMEtagInfo;->GetRLSEtag()Ljava/lang/String;

    #@3d
    move-result-object v5

    #@3e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v4

    #@42
    const-string v5, "], ruleETag:["

    #@44
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v4

    #@48
    invoke-virtual {v2}, Lcom/lge/ims/service/ip/XDMEtagInfo;->GetRuleEtag()Ljava/lang/String;

    #@4b
    move-result-object v5

    #@4c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v4

    #@50
    const-string v5, "],pidfETag:["

    #@52
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v4

    #@56
    invoke-virtual {v2}, Lcom/lge/ims/service/ip/XDMEtagInfo;->GetPidfEtag()Ljava/lang/String;

    #@59
    move-result-object v5

    #@5a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v4

    #@5e
    const-string v5, "]"

    #@60
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v4

    #@64
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@67
    move-result-object v4

    #@68
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6b
    .line 2822
    invoke-virtual {v2}, Lcom/lge/ims/service/ip/XDMEtagInfo;->GetListEtag()Ljava/lang/String;

    #@6e
    move-result-object v4

    #@6f
    if-nez v4, :cond_b3

    #@71
    .line 2823
    invoke-virtual {v3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@74
    .line 2830
    :goto_74
    invoke-virtual {v2}, Lcom/lge/ims/service/ip/XDMEtagInfo;->GetRLSEtag()Ljava/lang/String;

    #@77
    move-result-object v4

    #@78
    if-nez v4, :cond_cc

    #@7a
    .line 2831
    invoke-virtual {v3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@7d
    .line 2838
    :goto_7d
    invoke-virtual {v2}, Lcom/lge/ims/service/ip/XDMEtagInfo;->GetRuleEtag()Ljava/lang/String;

    #@80
    move-result-object v4

    #@81
    if-nez v4, :cond_e5

    #@83
    .line 2839
    invoke-virtual {v3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@86
    .line 2846
    :goto_86
    invoke-virtual {v2}, Lcom/lge/ims/service/ip/XDMEtagInfo;->GetPidfEtag()Ljava/lang/String;

    #@89
    move-result-object v4

    #@8a
    if-nez v4, :cond_fe

    #@8c
    .line 2847
    invoke-virtual {v3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@8f
    .line 2860
    :goto_8f
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@91
    invoke-static {v4}, Lcom/lge/ims/service/ip/IPAgent;->access$300(Lcom/lge/ims/service/ip/IPAgent;)Lcom/lge/ims/service/ip/IPContactHelper;

    #@94
    move-result-object v4

    #@95
    invoke-virtual {v4}, Lcom/lge/ims/service/ip/IPContactHelper;->GetRLSListUri()Ljava/lang/String;

    #@98
    move-result-object v0

    #@99
    .line 2861
    .local v0, RLSListUri:Ljava/lang/String;
    if-eqz v0, :cond_a1

    #@9b
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    #@9e
    move-result v4

    #@9f
    if-eqz v4, :cond_126

    #@a1
    .line 2862
    :cond_a1
    invoke-virtual {v3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@a4
    .line 2869
    :goto_a4
    invoke-virtual {v3}, Landroid/os/Parcel;->marshall()[B

    #@a7
    move-result-object v1

    #@a8
    .line 2870
    .local v1, baData:[B
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    #@ab
    .line 2871
    const/4 v3, 0x0

    #@ac
    .line 2873
    iget v4, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nativeObj:I

    #@ae
    invoke-static {v4, v1}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@b1
    goto/16 :goto_6

    #@b3
    .line 2824
    .end local v0           #RLSListUri:Ljava/lang/String;
    .end local v1           #baData:[B
    :cond_b3
    invoke-virtual {v2}, Lcom/lge/ims/service/ip/XDMEtagInfo;->GetListEtag()Ljava/lang/String;

    #@b6
    move-result-object v4

    #@b7
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    #@ba
    move-result v4

    #@bb
    if-eqz v4, :cond_c1

    #@bd
    .line 2825
    invoke-virtual {v3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@c0
    goto :goto_74

    #@c1
    .line 2827
    :cond_c1
    invoke-virtual {v3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@c4
    .line 2828
    invoke-virtual {v2}, Lcom/lge/ims/service/ip/XDMEtagInfo;->GetListEtag()Ljava/lang/String;

    #@c7
    move-result-object v4

    #@c8
    invoke-virtual {v3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@cb
    goto :goto_74

    #@cc
    .line 2832
    :cond_cc
    invoke-virtual {v2}, Lcom/lge/ims/service/ip/XDMEtagInfo;->GetRLSEtag()Ljava/lang/String;

    #@cf
    move-result-object v4

    #@d0
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    #@d3
    move-result v4

    #@d4
    if-eqz v4, :cond_da

    #@d6
    .line 2833
    invoke-virtual {v3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@d9
    goto :goto_7d

    #@da
    .line 2835
    :cond_da
    invoke-virtual {v3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@dd
    .line 2836
    invoke-virtual {v2}, Lcom/lge/ims/service/ip/XDMEtagInfo;->GetRLSEtag()Ljava/lang/String;

    #@e0
    move-result-object v4

    #@e1
    invoke-virtual {v3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e4
    goto :goto_7d

    #@e5
    .line 2840
    :cond_e5
    invoke-virtual {v2}, Lcom/lge/ims/service/ip/XDMEtagInfo;->GetRuleEtag()Ljava/lang/String;

    #@e8
    move-result-object v4

    #@e9
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    #@ec
    move-result v4

    #@ed
    if-eqz v4, :cond_f3

    #@ef
    .line 2841
    invoke-virtual {v3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@f2
    goto :goto_86

    #@f3
    .line 2843
    :cond_f3
    invoke-virtual {v3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@f6
    .line 2844
    invoke-virtual {v2}, Lcom/lge/ims/service/ip/XDMEtagInfo;->GetRuleEtag()Ljava/lang/String;

    #@f9
    move-result-object v4

    #@fa
    invoke-virtual {v3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@fd
    goto :goto_86

    #@fe
    .line 2848
    :cond_fe
    invoke-virtual {v2}, Lcom/lge/ims/service/ip/XDMEtagInfo;->GetPidfEtag()Ljava/lang/String;

    #@101
    move-result-object v4

    #@102
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    #@105
    move-result v4

    #@106
    if-eqz v4, :cond_10c

    #@108
    .line 2849
    invoke-virtual {v3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@10b
    goto :goto_8f

    #@10c
    .line 2851
    :cond_10c
    invoke-virtual {v3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@10f
    .line 2852
    invoke-virtual {v2}, Lcom/lge/ims/service/ip/XDMEtagInfo;->GetPidfEtag()Ljava/lang/String;

    #@112
    move-result-object v4

    #@113
    invoke-virtual {v3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@116
    goto/16 :goto_8f

    #@118
    .line 2855
    :cond_118
    invoke-virtual {v3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@11b
    .line 2856
    invoke-virtual {v3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@11e
    .line 2857
    invoke-virtual {v3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@121
    .line 2858
    invoke-virtual {v3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@124
    goto/16 :goto_8f

    #@126
    .line 2864
    .restart local v0       #RLSListUri:Ljava/lang/String;
    :cond_126
    new-instance v4, Ljava/lang/StringBuilder;

    #@128
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@12b
    const-string v5, "[IP]StartXDMAgent : RLSListUri:["

    #@12d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@130
    move-result-object v4

    #@131
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@134
    move-result-object v4

    #@135
    const-string v5, "]"

    #@137
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13a
    move-result-object v4

    #@13b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13e
    move-result-object v4

    #@13f
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@142
    .line 2865
    invoke-virtual {v3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@145
    .line 2866
    invoke-virtual {v3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@148
    goto/16 :goto_a4
.end method

.method public StopXDMAgent()V
    .registers 4

    #@0
    .prologue
    .line 2876
    iget v2, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nUsePresence:I

    #@2
    if-nez v2, :cond_5

    #@4
    .line 2885
    :goto_4
    return-void

    #@5
    .line 2879
    :cond_5
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 2880
    .local v1, parcel:Landroid/os/Parcel;
    const/16 v2, 0x2a70

    #@b
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@e
    .line 2881
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@11
    move-result-object v0

    #@12
    .line 2882
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@15
    .line 2883
    const/4 v1, 0x0

    #@16
    .line 2884
    iget v2, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nativeObj:I

    #@18
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@1b
    goto :goto_4
.end method

.method public SyncRCSFriendList()V
    .registers 5

    #@0
    .prologue
    .line 2887
    iget v2, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nUsePresence:I

    #@2
    if-nez v2, :cond_5

    #@4
    .line 2897
    :goto_4
    return-void

    #@5
    .line 2890
    :cond_5
    const-string v2, "[IP]"

    #@7
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@a
    .line 2891
    new-instance v0, Ljava/util/ArrayList;

    #@c
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@f
    .line 2892
    .local v0, objAddList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@11
    invoke-static {v2}, Lcom/lge/ims/service/ip/IPAgent;->access$300(Lcom/lge/ims/service/ip/IPAgent;)Lcom/lge/ims/service/ip/IPContactHelper;

    #@14
    move-result-object v2

    #@15
    const/4 v3, 0x1

    #@16
    invoke-virtual {v2, v0, v3}, Lcom/lge/ims/service/ip/IPContactHelper;->SyncRCSUserToResourceList(Ljava/util/List;Z)V

    #@19
    .line 2893
    invoke-direct {p0, v0}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->SendSyncAddRcsUserListCmd(Ljava/util/List;)V

    #@1c
    .line 2894
    new-instance v1, Ljava/util/ArrayList;

    #@1e
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@21
    .line 2895
    .local v1, objDeleteList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@23
    invoke-static {v2}, Lcom/lge/ims/service/ip/IPAgent;->access$300(Lcom/lge/ims/service/ip/IPAgent;)Lcom/lge/ims/service/ip/IPContactHelper;

    #@26
    move-result-object v2

    #@27
    const/4 v3, 0x0

    #@28
    invoke-virtual {v2, v1, v3}, Lcom/lge/ims/service/ip/IPContactHelper;->SyncRCSUserToResourceList(Ljava/util/List;Z)V

    #@2b
    .line 2896
    invoke-direct {p0, v1}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->SendDeleteRcsUserListCmd(Ljava/util/List;)V

    #@2e
    goto :goto_4
.end method

.method public UpdateMyStatus()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 2671
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "[IP] : nUserPresenceSocialInformation [ "

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    iget v4, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nUserPresenceSocialInformation:I

    #@f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    const-string v4, "]"

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v3

    #@1d
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@20
    .line 2672
    iget v3, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nUserPresenceSocialInformation:I

    #@22
    if-nez v3, :cond_25

    #@24
    .line 2762
    :goto_24
    return-void

    #@25
    .line 2676
    :cond_25
    new-instance v1, Lcom/lge/ims/service/ip/MyStatusInfo;

    #@27
    invoke-direct {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;-><init>()V

    #@2a
    .line 2677
    .local v1, objMyStatusInfo:Lcom/lge/ims/service/ip/MyStatusInfo;
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@2c
    invoke-static {v3}, Lcom/lge/ims/service/ip/IPAgent;->access$300(Lcom/lge/ims/service/ip/IPAgent;)Lcom/lge/ims/service/ip/IPContactHelper;

    #@2f
    move-result-object v3

    #@30
    invoke-virtual {v3, v1}, Lcom/lge/ims/service/ip/IPContactHelper;->QueryMyStatusInfo(Lcom/lge/ims/service/ip/MyStatusInfo;)Z

    #@33
    move-result v3

    #@34
    if-nez v3, :cond_3c

    #@36
    .line 2678
    const-string v3, "[IP][ERROR]UpdateMyStatus : QueryMyStatusInfo is false"

    #@38
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@3b
    goto :goto_24

    #@3c
    .line 2681
    :cond_3c
    new-instance v3, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v4, "[IP] : "

    #@43
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v3

    #@47
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->toString()Ljava/lang/String;

    #@4a
    move-result-object v4

    #@4b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v3

    #@4f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v3

    #@53
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@56
    .line 2682
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@59
    move-result-object v2

    #@5a
    .line 2683
    .local v2, parcel:Landroid/os/Parcel;
    const/16 v3, 0x2a3a

    #@5c
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@5f
    .line 2684
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIconThumbLink()Ljava/lang/String;

    #@62
    move-result-object v3

    #@63
    if-eqz v3, :cond_6f

    #@65
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIconThumbLink()Ljava/lang/String;

    #@68
    move-result-object v3

    #@69
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    #@6c
    move-result v3

    #@6d
    if-eqz v3, :cond_146

    #@6f
    .line 2685
    :cond_6f
    invoke-virtual {v2, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@72
    .line 2695
    :goto_72
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIconLink()Ljava/lang/String;

    #@75
    move-result-object v3

    #@76
    if-eqz v3, :cond_82

    #@78
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIconLink()Ljava/lang/String;

    #@7b
    move-result-object v3

    #@7c
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    #@7f
    move-result v3

    #@80
    if-eqz v3, :cond_16e

    #@82
    .line 2696
    :cond_82
    invoke-virtual {v2, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@85
    .line 2701
    :goto_85
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetHomePage()Ljava/lang/String;

    #@88
    move-result-object v3

    #@89
    if-eqz v3, :cond_95

    #@8b
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetHomePage()Ljava/lang/String;

    #@8e
    move-result-object v3

    #@8f
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    #@92
    move-result v3

    #@93
    if-eqz v3, :cond_17a

    #@95
    .line 2702
    :cond_95
    invoke-virtual {v2, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@98
    .line 2707
    :goto_98
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetFreeText()Ljava/lang/String;

    #@9b
    move-result-object v3

    #@9c
    if-eqz v3, :cond_a8

    #@9e
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetFreeText()Ljava/lang/String;

    #@a1
    move-result-object v3

    #@a2
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    #@a5
    move-result v3

    #@a6
    if-eqz v3, :cond_186

    #@a8
    .line 2708
    :cond_a8
    invoke-virtual {v2, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@ab
    .line 2713
    :goto_ab
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetDisplayName()Ljava/lang/String;

    #@ae
    move-result-object v3

    #@af
    if-eqz v3, :cond_bb

    #@b1
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetDisplayName()Ljava/lang/String;

    #@b4
    move-result-object v3

    #@b5
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    #@b8
    move-result v3

    #@b9
    if-eqz v3, :cond_192

    #@bb
    .line 2714
    :cond_bb
    invoke-virtual {v2, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@be
    .line 2719
    :goto_be
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetEMailAddress()Ljava/lang/String;

    #@c1
    move-result-object v3

    #@c2
    if-eqz v3, :cond_ce

    #@c4
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetEMailAddress()Ljava/lang/String;

    #@c7
    move-result-object v3

    #@c8
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    #@cb
    move-result v3

    #@cc
    if-eqz v3, :cond_19e

    #@ce
    .line 2720
    :cond_ce
    invoke-virtual {v2, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@d1
    .line 2725
    :goto_d1
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetBirthDay()Ljava/lang/String;

    #@d4
    move-result-object v3

    #@d5
    if-eqz v3, :cond_e1

    #@d7
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetBirthDay()Ljava/lang/String;

    #@da
    move-result-object v3

    #@db
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    #@de
    move-result v3

    #@df
    if-eqz v3, :cond_1aa

    #@e1
    .line 2726
    :cond_e1
    invoke-virtual {v2, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@e4
    .line 2731
    :goto_e4
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetTwitterAccount()Ljava/lang/String;

    #@e7
    move-result-object v3

    #@e8
    if-eqz v3, :cond_f4

    #@ea
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetTwitterAccount()Ljava/lang/String;

    #@ed
    move-result-object v3

    #@ee
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    #@f1
    move-result v3

    #@f2
    if-eqz v3, :cond_1b6

    #@f4
    .line 2732
    :cond_f4
    invoke-virtual {v2, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@f7
    .line 2737
    :goto_f7
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetFaceBookAccount()Ljava/lang/String;

    #@fa
    move-result-object v3

    #@fb
    if-eqz v3, :cond_107

    #@fd
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetFaceBookAccount()Ljava/lang/String;

    #@100
    move-result-object v3

    #@101
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    #@104
    move-result v3

    #@105
    if-eqz v3, :cond_1c2

    #@107
    .line 2738
    :cond_107
    invoke-virtual {v2, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@10a
    .line 2743
    :goto_10a
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetCyworldAccount()Ljava/lang/String;

    #@10d
    move-result-object v3

    #@10e
    if-eqz v3, :cond_11a

    #@110
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetCyworldAccount()Ljava/lang/String;

    #@113
    move-result-object v3

    #@114
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    #@117
    move-result v3

    #@118
    if-eqz v3, :cond_1ce

    #@11a
    .line 2744
    :cond_11a
    invoke-virtual {v2, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@11d
    .line 2749
    :goto_11d
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetWaggleAccount()Ljava/lang/String;

    #@120
    move-result-object v3

    #@121
    if-eqz v3, :cond_12d

    #@123
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetWaggleAccount()Ljava/lang/String;

    #@126
    move-result-object v3

    #@127
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    #@12a
    move-result v3

    #@12b
    if-eqz v3, :cond_1da

    #@12d
    .line 2750
    :cond_12d
    invoke-virtual {v2, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@130
    .line 2755
    :goto_130
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatus()I

    #@133
    move-result v3

    #@134
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@137
    .line 2757
    invoke-virtual {v2}, Landroid/os/Parcel;->marshall()[B

    #@13a
    move-result-object v0

    #@13b
    .line 2758
    .local v0, baData:[B
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@13e
    .line 2759
    const/4 v2, 0x0

    #@13f
    .line 2761
    iget v3, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nativeObj:I

    #@141
    invoke-static {v3, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@144
    goto/16 :goto_24

    #@146
    .line 2687
    .end local v0           #baData:[B
    :cond_146
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIconThumbEtag()Ljava/lang/String;

    #@149
    move-result-object v3

    #@14a
    if-eqz v3, :cond_156

    #@14c
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIconThumbEtag()Ljava/lang/String;

    #@14f
    move-result-object v3

    #@150
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    #@153
    move-result v3

    #@154
    if-eqz v3, :cond_15b

    #@156
    .line 2688
    :cond_156
    invoke-virtual {v2, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@159
    goto/16 :goto_72

    #@15b
    .line 2690
    :cond_15b
    invoke-virtual {v2, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@15e
    .line 2691
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIconThumbLink()Ljava/lang/String;

    #@161
    move-result-object v3

    #@162
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@165
    .line 2692
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIconThumbEtag()Ljava/lang/String;

    #@168
    move-result-object v3

    #@169
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@16c
    goto/16 :goto_72

    #@16e
    .line 2698
    :cond_16e
    invoke-virtual {v2, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@171
    .line 2699
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIconLink()Ljava/lang/String;

    #@174
    move-result-object v3

    #@175
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@178
    goto/16 :goto_85

    #@17a
    .line 2704
    :cond_17a
    invoke-virtual {v2, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@17d
    .line 2705
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetHomePage()Ljava/lang/String;

    #@180
    move-result-object v3

    #@181
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@184
    goto/16 :goto_98

    #@186
    .line 2710
    :cond_186
    invoke-virtual {v2, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@189
    .line 2711
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetFreeText()Ljava/lang/String;

    #@18c
    move-result-object v3

    #@18d
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@190
    goto/16 :goto_ab

    #@192
    .line 2716
    :cond_192
    invoke-virtual {v2, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@195
    .line 2717
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetDisplayName()Ljava/lang/String;

    #@198
    move-result-object v3

    #@199
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@19c
    goto/16 :goto_be

    #@19e
    .line 2722
    :cond_19e
    invoke-virtual {v2, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@1a1
    .line 2723
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetEMailAddress()Ljava/lang/String;

    #@1a4
    move-result-object v3

    #@1a5
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1a8
    goto/16 :goto_d1

    #@1aa
    .line 2728
    :cond_1aa
    invoke-virtual {v2, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@1ad
    .line 2729
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetBirthDay()Ljava/lang/String;

    #@1b0
    move-result-object v3

    #@1b1
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1b4
    goto/16 :goto_e4

    #@1b6
    .line 2734
    :cond_1b6
    invoke-virtual {v2, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@1b9
    .line 2735
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetTwitterAccount()Ljava/lang/String;

    #@1bc
    move-result-object v3

    #@1bd
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1c0
    goto/16 :goto_f7

    #@1c2
    .line 2740
    :cond_1c2
    invoke-virtual {v2, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@1c5
    .line 2741
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetFaceBookAccount()Ljava/lang/String;

    #@1c8
    move-result-object v3

    #@1c9
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1cc
    goto/16 :goto_10a

    #@1ce
    .line 2746
    :cond_1ce
    invoke-virtual {v2, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@1d1
    .line 2747
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetCyworldAccount()Ljava/lang/String;

    #@1d4
    move-result-object v3

    #@1d5
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1d8
    goto/16 :goto_11d

    #@1da
    .line 2752
    :cond_1da
    invoke-virtual {v2, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@1dd
    .line 2753
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetWaggleAccount()Ljava/lang/String;

    #@1e0
    move-result-object v3

    #@1e1
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1e4
    goto/16 :goto_130
.end method

.method public UpdateMyStatusIcon()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 2765
    new-instance v3, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v4, "[IP] : nUserPresenceSocialInformation [ "

    #@8
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v3

    #@c
    iget v4, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nUserPresenceSocialInformation:I

    #@e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v3

    #@12
    const-string v4, "]"

    #@14
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v3

    #@1c
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@1f
    .line 2766
    iget v3, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nUserPresenceSocialInformation:I

    #@21
    if-nez v3, :cond_24

    #@23
    .line 2799
    :goto_23
    return-void

    #@24
    .line 2769
    :cond_24
    new-instance v1, Lcom/lge/ims/service/ip/MyStatusInfo;

    #@26
    invoke-direct {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;-><init>()V

    #@29
    .line 2770
    .local v1, objMyStatusInfo:Lcom/lge/ims/service/ip/MyStatusInfo;
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@2b
    invoke-static {v3}, Lcom/lge/ims/service/ip/IPAgent;->access$300(Lcom/lge/ims/service/ip/IPAgent;)Lcom/lge/ims/service/ip/IPContactHelper;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v3, v1}, Lcom/lge/ims/service/ip/IPContactHelper;->QueryMyStatusIcon(Lcom/lge/ims/service/ip/MyStatusInfo;)Z

    #@32
    move-result v3

    #@33
    if-nez v3, :cond_3b

    #@35
    .line 2771
    const-string v3, "[IP][ERROR]UpdateMyStatus : QueryMyStatusInfo is false"

    #@37
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@3a
    goto :goto_23

    #@3b
    .line 2774
    :cond_3b
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIcon()Ljava/lang/String;

    #@3e
    move-result-object v3

    #@3f
    if-eqz v3, :cond_86

    #@41
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIcon()Ljava/lang/String;

    #@44
    move-result-object v3

    #@45
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    #@48
    move-result v3

    #@49
    if-nez v3, :cond_86

    #@4b
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIconThumb()Ljava/lang/String;

    #@4e
    move-result-object v3

    #@4f
    if-eqz v3, :cond_86

    #@51
    .line 2775
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@54
    move-result-object v2

    #@55
    .line 2776
    .local v2, parcel:Landroid/os/Parcel;
    const/16 v3, 0x2a4e

    #@57
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@5a
    .line 2777
    invoke-virtual {v2, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@5d
    .line 2780
    invoke-virtual {v2, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@60
    .line 2781
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIcon()Ljava/lang/String;

    #@63
    move-result-object v3

    #@64
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@67
    .line 2783
    invoke-virtual {v2, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@6a
    .line 2786
    invoke-virtual {v2, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@6d
    .line 2787
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIconThumb()Ljava/lang/String;

    #@70
    move-result-object v3

    #@71
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@74
    .line 2789
    const/4 v3, 0x0

    #@75
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@78
    .line 2791
    invoke-virtual {v2}, Landroid/os/Parcel;->marshall()[B

    #@7b
    move-result-object v0

    #@7c
    .line 2792
    .local v0, baData:[B
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@7f
    .line 2793
    const/4 v2, 0x0

    #@80
    .line 2794
    iget v3, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nativeObj:I

    #@82
    invoke-static {v3, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@85
    goto :goto_23

    #@86
    .line 2796
    .end local v0           #baData:[B
    .end local v2           #parcel:Landroid/os/Parcel;
    :cond_86
    const-string v3, "[IP][ERROR]UpdateMyStatus : GetStatusIcon is not exist"

    #@88
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@8b
    goto :goto_23
.end method

.method public onMessage(Landroid/os/Parcel;)V
    .registers 47
    .parameter "parcel"

    #@0
    .prologue
    .line 2053
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v32

    #@4
    .line 2054
    .local v32, nMsgType:I
    sparse-switch v32, :sswitch_data_922

    #@7
    .line 2508
    new-instance v41, Ljava/lang/StringBuilder;

    #@9
    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v42, "[IP]unknown message type: "

    #@e
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v41

    #@12
    move-object/from16 v0, v41

    #@14
    move/from16 v1, v32

    #@16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v41

    #@1a
    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v41

    #@1e
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@21
    .line 2512
    :goto_21
    return-void

    #@22
    .line 2057
    :sswitch_22
    const-string v41, "[IP]onMessage : CONNECTEDIPSERVICE_IND"

    #@24
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@27
    .line 2058
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@2a
    move-result-object v41

    #@2b
    if-nez v41, :cond_33

    #@2d
    .line 2059
    const-string v41, "[IP][ERROR]onMessage : IPAgent.GetIPAgent() is NULL"

    #@2f
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@32
    goto :goto_21

    #@33
    .line 2061
    :cond_33
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@36
    move-result-object v41

    #@37
    invoke-virtual/range {v41 .. v41}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@3a
    move-result-object v41

    #@3b
    if-nez v41, :cond_43

    #@3d
    .line 2062
    const-string v41, "[IP][ERROR]onMessage : IPAgent.GetIPAgent().GetHandler() is NULL"

    #@3f
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@42
    goto :goto_21

    #@43
    .line 2065
    :cond_43
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@46
    move-result-object v41

    #@47
    invoke-virtual/range {v41 .. v41}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@4a
    move-result-object v41

    #@4b
    const/16 v42, 0x1

    #@4d
    invoke-virtual/range {v41 .. v42}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@50
    goto :goto_21

    #@51
    .line 2070
    :sswitch_51
    const-string v41, "[IP]onMessage : DISCONNECTEDIPSERVICE_IND"

    #@53
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@56
    .line 2071
    invoke-static {}, Lcom/lge/ims/service/ip/IPQueryRepository;->GetQueryRepository()Lcom/lge/ims/service/ip/IPQueryRepository;

    #@59
    move-result-object v41

    #@5a
    invoke-virtual/range {v41 .. v41}, Lcom/lge/ims/service/ip/IPQueryRepository;->ClearQueryList()V

    #@5d
    .line 2072
    invoke-static {}, Lcom/lge/ims/service/ip/IPQueryRepository;->GetQueryRepository()Lcom/lge/ims/service/ip/IPQueryRepository;

    #@60
    move-result-object v41

    #@61
    invoke-virtual/range {v41 .. v41}, Lcom/lge/ims/service/ip/IPQueryRepository;->ClearPresenceQueryList()V

    #@64
    .line 2073
    invoke-static {}, Lcom/lge/ims/service/ip/IPQueryRepository;->GetQueryRepository()Lcom/lge/ims/service/ip/IPQueryRepository;

    #@67
    move-result-object v41

    #@68
    invoke-virtual/range {v41 .. v41}, Lcom/lge/ims/service/ip/IPQueryRepository;->ClearBuddyStatusIconQueryList()V

    #@6b
    .line 2074
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@6e
    move-result-object v41

    #@6f
    if-nez v41, :cond_77

    #@71
    .line 2075
    const-string v41, "[IP][ERROR]onMessage : IPAgent.GetIPAgent() is NULL"

    #@73
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@76
    goto :goto_21

    #@77
    .line 2077
    :cond_77
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@7a
    move-result-object v41

    #@7b
    invoke-virtual/range {v41 .. v41}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@7e
    move-result-object v41

    #@7f
    if-nez v41, :cond_87

    #@81
    .line 2078
    const-string v41, "[IP][ERROR]onMessage : IPAgent.GetIPAgent().GetHandler() is NULL"

    #@83
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@86
    goto :goto_21

    #@87
    .line 2081
    :cond_87
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@8a
    move-result-object v41

    #@8b
    invoke-virtual/range {v41 .. v41}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@8e
    move-result-object v41

    #@8f
    const/16 v42, 0x2

    #@91
    invoke-virtual/range {v41 .. v42}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@94
    goto :goto_21

    #@95
    .line 2086
    :sswitch_95
    const-string v41, "[IP]onMessage : CONNECTEDXDMSERVICE_IND"

    #@97
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@9a
    .line 2087
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@9d
    move-result-object v41

    #@9e
    if-nez v41, :cond_a7

    #@a0
    .line 2088
    const-string v41, "[IP][ERROR]onMessage : IPAgent.GetIPAgent() is NULL"

    #@a2
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@a5
    goto/16 :goto_21

    #@a7
    .line 2090
    :cond_a7
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@aa
    move-result-object v41

    #@ab
    invoke-virtual/range {v41 .. v41}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@ae
    move-result-object v41

    #@af
    if-nez v41, :cond_b8

    #@b1
    .line 2091
    const-string v41, "[IP][ERROR]onMessage : IPAgent.GetIPAgent().GetHandler() is NULL"

    #@b3
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@b6
    goto/16 :goto_21

    #@b8
    .line 2094
    :cond_b8
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@bb
    move-result-object v41

    #@bc
    invoke-virtual/range {v41 .. v41}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@bf
    move-result-object v41

    #@c0
    const/16 v42, 0x3

    #@c2
    invoke-virtual/range {v41 .. v42}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@c5
    goto/16 :goto_21

    #@c7
    .line 2099
    :sswitch_c7
    const-string v41, "[IP]onMessage : UPDATEMYSTATUSINFO_IND"

    #@c9
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@cc
    .line 2100
    new-instance v37, Lcom/lge/ims/service/ip/MyStatusInfo;

    #@ce
    invoke-direct/range {v37 .. v37}, Lcom/lge/ims/service/ip/MyStatusInfo;-><init>()V

    #@d1
    .line 2101
    .local v37, objMyStatusInfo:Lcom/lge/ims/service/ip/MyStatusInfo;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@d4
    move-result v14

    #@d5
    .line 2102
    .local v14, bHasHomePage:I
    const/16 v41, 0x1

    #@d7
    move/from16 v0, v41

    #@d9
    if-ne v14, v0, :cond_e6

    #@db
    .line 2103
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@de
    move-result-object v41

    #@df
    move-object/from16 v0, v37

    #@e1
    move-object/from16 v1, v41

    #@e3
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->SetHomePage(Ljava/lang/String;)V

    #@e6
    .line 2105
    :cond_e6
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@e9
    move-result v13

    #@ea
    .line 2106
    .local v13, bHasFreeText:I
    const/16 v41, 0x1

    #@ec
    move/from16 v0, v41

    #@ee
    if-ne v13, v0, :cond_fb

    #@f0
    .line 2107
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@f3
    move-result-object v41

    #@f4
    move-object/from16 v0, v37

    #@f6
    move-object/from16 v1, v41

    #@f8
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->SetFreeText(Ljava/lang/String;)V

    #@fb
    .line 2109
    :cond_fb
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@fe
    move-result v9

    #@ff
    .line 2110
    .local v9, bHasDisplayName:I
    const/16 v41, 0x1

    #@101
    move/from16 v0, v41

    #@103
    if-ne v9, v0, :cond_110

    #@105
    .line 2111
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@108
    move-result-object v41

    #@109
    move-object/from16 v0, v37

    #@10b
    move-object/from16 v1, v41

    #@10d
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->SetDisplayName(Ljava/lang/String;)V

    #@110
    .line 2113
    :cond_110
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@113
    move-result v10

    #@114
    .line 2114
    .local v10, bHasEMailAddress:I
    const/16 v41, 0x1

    #@116
    move/from16 v0, v41

    #@118
    if-ne v10, v0, :cond_125

    #@11a
    .line 2115
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@11d
    move-result-object v41

    #@11e
    move-object/from16 v0, v37

    #@120
    move-object/from16 v1, v41

    #@122
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->SetEMailAddress(Ljava/lang/String;)V

    #@125
    .line 2117
    :cond_125
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@128
    move-result v5

    #@129
    .line 2118
    .local v5, bHasBirthDay:I
    const/16 v41, 0x1

    #@12b
    move/from16 v0, v41

    #@12d
    if-ne v5, v0, :cond_13a

    #@12f
    .line 2119
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@132
    move-result-object v41

    #@133
    move-object/from16 v0, v37

    #@135
    move-object/from16 v1, v41

    #@137
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->SetBirthDay(Ljava/lang/String;)V

    #@13a
    .line 2121
    :cond_13a
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@13d
    move-result v20

    #@13e
    .line 2122
    .local v20, bHasTwitterAccount:I
    const/16 v41, 0x1

    #@140
    move/from16 v0, v20

    #@142
    move/from16 v1, v41

    #@144
    if-ne v0, v1, :cond_151

    #@146
    .line 2123
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@149
    move-result-object v41

    #@14a
    move-object/from16 v0, v37

    #@14c
    move-object/from16 v1, v41

    #@14e
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->SetTwitterAccount(Ljava/lang/String;)V

    #@151
    .line 2125
    :cond_151
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@154
    move-result v11

    #@155
    .line 2126
    .local v11, bHasFacebookAccount:I
    const/16 v41, 0x1

    #@157
    move/from16 v0, v41

    #@159
    if-ne v11, v0, :cond_166

    #@15b
    .line 2127
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@15e
    move-result-object v41

    #@15f
    move-object/from16 v0, v37

    #@161
    move-object/from16 v1, v41

    #@163
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->SetFaceBookAccount(Ljava/lang/String;)V

    #@166
    .line 2129
    :cond_166
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@169
    move-result v8

    #@16a
    .line 2130
    .local v8, bHasCyworldAccount:I
    const/16 v41, 0x1

    #@16c
    move/from16 v0, v41

    #@16e
    if-ne v8, v0, :cond_17b

    #@170
    .line 2131
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@173
    move-result-object v41

    #@174
    move-object/from16 v0, v37

    #@176
    move-object/from16 v1, v41

    #@178
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->SetCyworldAccount(Ljava/lang/String;)V

    #@17b
    .line 2133
    :cond_17b
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@17e
    move-result v21

    #@17f
    .line 2134
    .local v21, bHasWaggleAccount:I
    const/16 v41, 0x1

    #@181
    move/from16 v0, v21

    #@183
    move/from16 v1, v41

    #@185
    if-ne v0, v1, :cond_192

    #@187
    .line 2135
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@18a
    move-result-object v41

    #@18b
    move-object/from16 v0, v37

    #@18d
    move-object/from16 v1, v41

    #@18f
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->SetWaggleAccount(Ljava/lang/String;)V

    #@192
    .line 2137
    :cond_192
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@195
    move-result v15

    #@196
    .line 2138
    .local v15, bHasStatusIconLink:I
    const/16 v41, 0x1

    #@198
    move/from16 v0, v41

    #@19a
    if-ne v15, v0, :cond_1a7

    #@19c
    .line 2139
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@19f
    move-result-object v41

    #@1a0
    move-object/from16 v0, v37

    #@1a2
    move-object/from16 v1, v41

    #@1a4
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->SetStatusIconLink(Ljava/lang/String;)V

    #@1a7
    .line 2141
    :cond_1a7
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@1aa
    move-result v17

    #@1ab
    .line 2142
    .local v17, bHasStatusIconThumbLink:I
    const/16 v41, 0x1

    #@1ad
    move/from16 v0, v17

    #@1af
    move/from16 v1, v41

    #@1b1
    if-ne v0, v1, :cond_1be

    #@1b3
    .line 2143
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1b6
    move-result-object v41

    #@1b7
    move-object/from16 v0, v37

    #@1b9
    move-object/from16 v1, v41

    #@1bb
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->SetStatusIconThumbLink(Ljava/lang/String;)V

    #@1be
    .line 2145
    :cond_1be
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@1c1
    move-result v16

    #@1c2
    .line 2146
    .local v16, bHasStatusIconThumbEtag:I
    const/16 v41, 0x1

    #@1c4
    move/from16 v0, v16

    #@1c6
    move/from16 v1, v41

    #@1c8
    if-ne v0, v1, :cond_1d5

    #@1ca
    .line 2147
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1cd
    move-result-object v41

    #@1ce
    move-object/from16 v0, v37

    #@1d0
    move-object/from16 v1, v41

    #@1d2
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->SetStatusIconThumbEtag(Ljava/lang/String;)V

    #@1d5
    .line 2149
    :cond_1d5
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@1d8
    move-result v34

    #@1d9
    .line 2150
    .local v34, nStatus:I
    move-object/from16 v0, v37

    #@1db
    move/from16 v1, v34

    #@1dd
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->SetStatus(I)V

    #@1e0
    .line 2151
    new-instance v36, Landroid/os/Message;

    #@1e2
    invoke-direct/range {v36 .. v36}, Landroid/os/Message;-><init>()V

    #@1e5
    .line 2152
    .local v36, objMessage:Landroid/os/Message;
    new-instance v35, Landroid/os/Bundle;

    #@1e7
    invoke-direct/range {v35 .. v35}, Landroid/os/Bundle;-><init>()V

    #@1ea
    .line 2153
    .local v35, objBundle:Landroid/os/Bundle;
    const/16 v41, 0x29

    #@1ec
    move/from16 v0, v41

    #@1ee
    move-object/from16 v1, v36

    #@1f0
    iput v0, v1, Landroid/os/Message;->what:I

    #@1f2
    .line 2154
    const-string v41, "my_status"

    #@1f4
    move-object/from16 v0, v35

    #@1f6
    move-object/from16 v1, v41

    #@1f8
    move-object/from16 v2, v37

    #@1fa
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    #@1fd
    .line 2155
    move-object/from16 v0, v36

    #@1ff
    move-object/from16 v1, v35

    #@201
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@204
    .line 2156
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@207
    move-result-object v41

    #@208
    invoke-virtual/range {v41 .. v41}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@20b
    move-result-object v41

    #@20c
    const-wide/16 v42, 0x64

    #@20e
    move-object/from16 v0, v41

    #@210
    move-object/from16 v1, v36

    #@212
    move-wide/from16 v2, v42

    #@214
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    #@217
    goto/16 :goto_21

    #@219
    .line 2161
    .end local v5           #bHasBirthDay:I
    .end local v8           #bHasCyworldAccount:I
    .end local v9           #bHasDisplayName:I
    .end local v10           #bHasEMailAddress:I
    .end local v11           #bHasFacebookAccount:I
    .end local v13           #bHasFreeText:I
    .end local v14           #bHasHomePage:I
    .end local v15           #bHasStatusIconLink:I
    .end local v16           #bHasStatusIconThumbEtag:I
    .end local v17           #bHasStatusIconThumbLink:I
    .end local v20           #bHasTwitterAccount:I
    .end local v21           #bHasWaggleAccount:I
    .end local v34           #nStatus:I
    .end local v35           #objBundle:Landroid/os/Bundle;
    .end local v36           #objMessage:Landroid/os/Message;
    .end local v37           #objMyStatusInfo:Lcom/lge/ims/service/ip/MyStatusInfo;
    :sswitch_219
    const-string v41, "[IP]onMessage : RECEIVEDPRESENCEQUERY_IND"

    #@21b
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@21e
    .line 2162
    new-instance v38, Lcom/lge/ims/service/ip/PresenceInfo;

    #@220
    invoke-direct/range {v38 .. v38}, Lcom/lge/ims/service/ip/PresenceInfo;-><init>()V

    #@223
    .line 2163
    .local v38, objPresenceInfo:Lcom/lge/ims/service/ip/PresenceInfo;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@226
    move-result-object v41

    #@227
    move-object/from16 v0, v38

    #@229
    move-object/from16 v1, v41

    #@22b
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/PresenceInfo;->SetMSISDN(Ljava/lang/String;)V

    #@22e
    .line 2164
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@231
    move-result v33

    #@232
    .line 2165
    .local v33, nOverridingWillingness:I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@235
    move-result v41

    #@236
    move-object/from16 v0, v38

    #@238
    move/from16 v1, v41

    #@23a
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/PresenceInfo;->SetStatus(I)V

    #@23d
    .line 2166
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@240
    move-result v14

    #@241
    .line 2167
    .restart local v14       #bHasHomePage:I
    const/16 v41, 0x1

    #@243
    move/from16 v0, v41

    #@245
    if-ne v14, v0, :cond_252

    #@247
    .line 2168
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@24a
    move-result-object v41

    #@24b
    move-object/from16 v0, v38

    #@24d
    move-object/from16 v1, v41

    #@24f
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/PresenceInfo;->SetHomePage(Ljava/lang/String;)V

    #@252
    .line 2170
    :cond_252
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@255
    move-result v13

    #@256
    .line 2171
    .restart local v13       #bHasFreeText:I
    const/16 v41, 0x1

    #@258
    move/from16 v0, v41

    #@25a
    if-ne v13, v0, :cond_267

    #@25c
    .line 2172
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@25f
    move-result-object v41

    #@260
    move-object/from16 v0, v38

    #@262
    move-object/from16 v1, v41

    #@264
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/PresenceInfo;->SetFreeText(Ljava/lang/String;)V

    #@267
    .line 2174
    :cond_267
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@26a
    move-result v9

    #@26b
    .line 2175
    .restart local v9       #bHasDisplayName:I
    const/16 v41, 0x1

    #@26d
    move/from16 v0, v41

    #@26f
    if-ne v9, v0, :cond_27c

    #@271
    .line 2176
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@274
    move-result-object v41

    #@275
    move-object/from16 v0, v38

    #@277
    move-object/from16 v1, v41

    #@279
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/PresenceInfo;->SetDisplayName(Ljava/lang/String;)V

    #@27c
    .line 2178
    :cond_27c
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@27f
    move-result v10

    #@280
    .line 2179
    .restart local v10       #bHasEMailAddress:I
    const/16 v41, 0x1

    #@282
    move/from16 v0, v41

    #@284
    if-ne v10, v0, :cond_291

    #@286
    .line 2180
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@289
    move-result-object v41

    #@28a
    move-object/from16 v0, v38

    #@28c
    move-object/from16 v1, v41

    #@28e
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/PresenceInfo;->SetEMailAddress(Ljava/lang/String;)V

    #@291
    .line 2182
    :cond_291
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@294
    move-result v6

    #@295
    .line 2183
    .local v6, bHasBirthday:I
    const/16 v41, 0x1

    #@297
    move/from16 v0, v41

    #@299
    if-ne v6, v0, :cond_2a6

    #@29b
    .line 2184
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@29e
    move-result-object v41

    #@29f
    move-object/from16 v0, v38

    #@2a1
    move-object/from16 v1, v41

    #@2a3
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/PresenceInfo;->SetBirthDay(Ljava/lang/String;)V

    #@2a6
    .line 2186
    :cond_2a6
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@2a9
    move-result v20

    #@2aa
    .line 2187
    .restart local v20       #bHasTwitterAccount:I
    const/16 v41, 0x1

    #@2ac
    move/from16 v0, v20

    #@2ae
    move/from16 v1, v41

    #@2b0
    if-ne v0, v1, :cond_2bd

    #@2b2
    .line 2188
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2b5
    move-result-object v41

    #@2b6
    move-object/from16 v0, v38

    #@2b8
    move-object/from16 v1, v41

    #@2ba
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/PresenceInfo;->SetTwitterAccount(Ljava/lang/String;)V

    #@2bd
    .line 2190
    :cond_2bd
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@2c0
    move-result v11

    #@2c1
    .line 2191
    .restart local v11       #bHasFacebookAccount:I
    const/16 v41, 0x1

    #@2c3
    move/from16 v0, v41

    #@2c5
    if-ne v11, v0, :cond_2d2

    #@2c7
    .line 2192
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2ca
    move-result-object v41

    #@2cb
    move-object/from16 v0, v38

    #@2cd
    move-object/from16 v1, v41

    #@2cf
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/PresenceInfo;->SetFaceBookAccount(Ljava/lang/String;)V

    #@2d2
    .line 2194
    :cond_2d2
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@2d5
    move-result v8

    #@2d6
    .line 2195
    .restart local v8       #bHasCyworldAccount:I
    const/16 v41, 0x1

    #@2d8
    move/from16 v0, v41

    #@2da
    if-ne v8, v0, :cond_2e7

    #@2dc
    .line 2196
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2df
    move-result-object v41

    #@2e0
    move-object/from16 v0, v38

    #@2e2
    move-object/from16 v1, v41

    #@2e4
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/PresenceInfo;->SetCyworldAccount(Ljava/lang/String;)V

    #@2e7
    .line 2198
    :cond_2e7
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@2ea
    move-result v21

    #@2eb
    .line 2199
    .restart local v21       #bHasWaggleAccount:I
    const/16 v41, 0x1

    #@2ed
    move/from16 v0, v21

    #@2ef
    move/from16 v1, v41

    #@2f1
    if-ne v0, v1, :cond_2fe

    #@2f3
    .line 2200
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2f6
    move-result-object v41

    #@2f7
    move-object/from16 v0, v38

    #@2f9
    move-object/from16 v1, v41

    #@2fb
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/PresenceInfo;->SetWaggleAccount(Ljava/lang/String;)V

    #@2fe
    .line 2202
    :cond_2fe
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@301
    move-result v15

    #@302
    .line 2203
    .restart local v15       #bHasStatusIconLink:I
    const/16 v41, 0x1

    #@304
    move/from16 v0, v41

    #@306
    if-ne v15, v0, :cond_313

    #@308
    .line 2204
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@30b
    move-result-object v41

    #@30c
    move-object/from16 v0, v38

    #@30e
    move-object/from16 v1, v41

    #@310
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/PresenceInfo;->SetStatusIconLink(Ljava/lang/String;)V

    #@313
    .line 2206
    :cond_313
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@316
    move-result v17

    #@317
    .line 2207
    .restart local v17       #bHasStatusIconThumbLink:I
    const/16 v41, 0x1

    #@319
    move/from16 v0, v17

    #@31b
    move/from16 v1, v41

    #@31d
    if-ne v0, v1, :cond_32a

    #@31f
    .line 2208
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@322
    move-result-object v41

    #@323
    move-object/from16 v0, v38

    #@325
    move-object/from16 v1, v41

    #@327
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/PresenceInfo;->SetStatusIconThumbLink(Ljava/lang/String;)V

    #@32a
    .line 2210
    :cond_32a
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@32d
    move-result v16

    #@32e
    .line 2211
    .restart local v16       #bHasStatusIconThumbEtag:I
    const/16 v41, 0x1

    #@330
    move/from16 v0, v16

    #@332
    move/from16 v1, v41

    #@334
    if-ne v0, v1, :cond_341

    #@336
    .line 2212
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@339
    move-result-object v41

    #@33a
    move-object/from16 v0, v38

    #@33c
    move-object/from16 v1, v41

    #@33e
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/PresenceInfo;->SetStatusIconThumbEtag(Ljava/lang/String;)V

    #@341
    .line 2214
    :cond_341
    invoke-static {}, Lcom/lge/ims/service/ip/IPListenerThread;->GetIPListenerThread()Lcom/lge/ims/service/ip/IPListenerThread;

    #@344
    move-result-object v41

    #@345
    if-eqz v41, :cond_351

    #@347
    invoke-static {}, Lcom/lge/ims/service/ip/IPListenerThread;->GetIPListenerThread()Lcom/lge/ims/service/ip/IPListenerThread;

    #@34a
    move-result-object v41

    #@34b
    invoke-virtual/range {v41 .. v41}, Lcom/lge/ims/service/ip/IPListenerThread;->GetHandler()Landroid/os/Handler;

    #@34e
    move-result-object v41

    #@34f
    if-nez v41, :cond_358

    #@351
    .line 2215
    :cond_351
    const-string v41, "[IP][ERROR]IPListenerThread is null"

    #@353
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@356
    goto/16 :goto_21

    #@358
    .line 2219
    :cond_358
    new-instance v36, Landroid/os/Message;

    #@35a
    invoke-direct/range {v36 .. v36}, Landroid/os/Message;-><init>()V

    #@35d
    .line 2220
    .restart local v36       #objMessage:Landroid/os/Message;
    new-instance v35, Landroid/os/Bundle;

    #@35f
    invoke-direct/range {v35 .. v35}, Landroid/os/Bundle;-><init>()V

    #@362
    .line 2221
    .restart local v35       #objBundle:Landroid/os/Bundle;
    const/16 v41, 0x14

    #@364
    move/from16 v0, v41

    #@366
    move-object/from16 v1, v36

    #@368
    iput v0, v1, Landroid/os/Message;->what:I

    #@36a
    .line 2222
    const-string v41, "presence"

    #@36c
    move-object/from16 v0, v35

    #@36e
    move-object/from16 v1, v41

    #@370
    move-object/from16 v2, v38

    #@372
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    #@375
    .line 2223
    move-object/from16 v0, v36

    #@377
    move-object/from16 v1, v35

    #@379
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@37c
    .line 2224
    invoke-static {}, Lcom/lge/ims/service/ip/IPListenerThread;->GetIPListenerThread()Lcom/lge/ims/service/ip/IPListenerThread;

    #@37f
    move-result-object v41

    #@380
    invoke-virtual/range {v41 .. v41}, Lcom/lge/ims/service/ip/IPListenerThread;->GetHandler()Landroid/os/Handler;

    #@383
    move-result-object v41

    #@384
    const-wide/16 v42, 0x3e8

    #@386
    move-object/from16 v0, v41

    #@388
    move-object/from16 v1, v36

    #@38a
    move-wide/from16 v2, v42

    #@38c
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    #@38f
    goto/16 :goto_21

    #@391
    .line 2229
    .end local v6           #bHasBirthday:I
    .end local v8           #bHasCyworldAccount:I
    .end local v9           #bHasDisplayName:I
    .end local v10           #bHasEMailAddress:I
    .end local v11           #bHasFacebookAccount:I
    .end local v13           #bHasFreeText:I
    .end local v14           #bHasHomePage:I
    .end local v15           #bHasStatusIconLink:I
    .end local v16           #bHasStatusIconThumbEtag:I
    .end local v17           #bHasStatusIconThumbLink:I
    .end local v20           #bHasTwitterAccount:I
    .end local v21           #bHasWaggleAccount:I
    .end local v33           #nOverridingWillingness:I
    .end local v35           #objBundle:Landroid/os/Bundle;
    .end local v36           #objMessage:Landroid/os/Message;
    .end local v38           #objPresenceInfo:Lcom/lge/ims/service/ip/PresenceInfo;
    :sswitch_391
    const-string v41, "[IP]onMessage : QUERYRLSLISTFAILED_IND"

    #@393
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@396
    goto/16 :goto_21

    #@398
    .line 2236
    :sswitch_398
    const-string v41, "[IP]onMessage : QUERYRLSLIST_IND"

    #@39a
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@39d
    goto/16 :goto_21

    #@39f
    .line 2242
    :sswitch_39f
    const-string v41, "[IP]onMessage : QUERYONECONTACTFAILED_IND"

    #@3a1
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@3a4
    .line 2243
    invoke-static {}, Lcom/lge/ims/service/ip/IPQueryRepository;->GetQueryRepository()Lcom/lge/ims/service/ip/IPQueryRepository;

    #@3a7
    move-result-object v41

    #@3a8
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3ab
    move-result-object v42

    #@3ac
    invoke-virtual/range {v41 .. v42}, Lcom/lge/ims/service/ip/IPQueryRepository;->RemovePresenceQuery(Ljava/lang/String;)V

    #@3af
    goto/16 :goto_21

    #@3b1
    .line 2257
    :sswitch_3b1
    const-string v41, "[IP]onMessage : QUERYONECONTACTSUCCESS_IND"

    #@3b3
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@3b6
    .line 2258
    invoke-static {}, Lcom/lge/ims/service/ip/IPQueryRepository;->GetQueryRepository()Lcom/lge/ims/service/ip/IPQueryRepository;

    #@3b9
    move-result-object v41

    #@3ba
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3bd
    move-result-object v42

    #@3be
    invoke-virtual/range {v41 .. v42}, Lcom/lge/ims/service/ip/IPQueryRepository;->RemovePresenceQuery(Ljava/lang/String;)V

    #@3c1
    goto/16 :goto_21

    #@3c3
    .line 2271
    :sswitch_3c3
    const-string v41, "[IP]onMessage : UPDATEMYSTATUSICON_IND"

    #@3c5
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@3c8
    .line 2272
    new-instance v37, Lcom/lge/ims/service/ip/MyStatusInfo;

    #@3ca
    invoke-direct/range {v37 .. v37}, Lcom/lge/ims/service/ip/MyStatusInfo;-><init>()V

    #@3cd
    .line 2273
    .restart local v37       #objMyStatusInfo:Lcom/lge/ims/service/ip/MyStatusInfo;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@3d0
    move-result v18

    #@3d1
    .line 2274
    .local v18, bHasStatusIconURL:I
    const/16 v41, 0x1

    #@3d3
    move/from16 v0, v18

    #@3d5
    move/from16 v1, v41

    #@3d7
    if-ne v0, v1, :cond_3e4

    #@3d9
    .line 2275
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3dc
    move-result-object v41

    #@3dd
    move-object/from16 v0, v37

    #@3df
    move-object/from16 v1, v41

    #@3e1
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->SetStatusIconLink(Ljava/lang/String;)V

    #@3e4
    .line 2277
    :cond_3e4
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@3e7
    move-result v19

    #@3e8
    .line 2278
    .local v19, bHasStatusThumbIconURL:I
    const/16 v41, 0x1

    #@3ea
    move/from16 v0, v19

    #@3ec
    move/from16 v1, v41

    #@3ee
    if-ne v0, v1, :cond_3fb

    #@3f0
    .line 2279
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3f3
    move-result-object v41

    #@3f4
    move-object/from16 v0, v37

    #@3f6
    move-object/from16 v1, v41

    #@3f8
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->SetStatusIconThumbLink(Ljava/lang/String;)V

    #@3fb
    .line 2281
    :cond_3fb
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@3fe
    move-result v7

    #@3ff
    .line 2282
    .local v7, bHasContentEtag:I
    const/16 v41, 0x1

    #@401
    move/from16 v0, v41

    #@403
    if-ne v7, v0, :cond_410

    #@405
    .line 2283
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@408
    move-result-object v41

    #@409
    move-object/from16 v0, v37

    #@40b
    move-object/from16 v1, v41

    #@40d
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->SetStatusIconThumbEtag(Ljava/lang/String;)V

    #@410
    .line 2285
    :cond_410
    new-instance v36, Landroid/os/Message;

    #@412
    invoke-direct/range {v36 .. v36}, Landroid/os/Message;-><init>()V

    #@415
    .line 2286
    .restart local v36       #objMessage:Landroid/os/Message;
    new-instance v35, Landroid/os/Bundle;

    #@417
    invoke-direct/range {v35 .. v35}, Landroid/os/Bundle;-><init>()V

    #@41a
    .line 2287
    .restart local v35       #objBundle:Landroid/os/Bundle;
    const/16 v41, 0x2a

    #@41c
    move/from16 v0, v41

    #@41e
    move-object/from16 v1, v36

    #@420
    iput v0, v1, Landroid/os/Message;->what:I

    #@422
    .line 2288
    const-string v41, "my_status"

    #@424
    move-object/from16 v0, v35

    #@426
    move-object/from16 v1, v41

    #@428
    move-object/from16 v2, v37

    #@42a
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    #@42d
    .line 2289
    move-object/from16 v0, v36

    #@42f
    move-object/from16 v1, v35

    #@431
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@434
    .line 2290
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@437
    move-result-object v41

    #@438
    invoke-virtual/range {v41 .. v41}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@43b
    move-result-object v41

    #@43c
    const-wide/16 v42, 0x64

    #@43e
    move-object/from16 v0, v41

    #@440
    move-object/from16 v1, v36

    #@442
    move-wide/from16 v2, v42

    #@444
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    #@447
    goto/16 :goto_21

    #@449
    .line 2295
    .end local v7           #bHasContentEtag:I
    .end local v18           #bHasStatusIconURL:I
    .end local v19           #bHasStatusThumbIconURL:I
    .end local v35           #objBundle:Landroid/os/Bundle;
    .end local v36           #objMessage:Landroid/os/Message;
    .end local v37           #objMyStatusInfo:Lcom/lge/ims/service/ip/MyStatusInfo;
    :sswitch_449
    const-string v41, "[IP]onMessage : GETMYSTATUSICONTHUMB_IND"

    #@44b
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@44e
    .line 2296
    new-instance v37, Lcom/lge/ims/service/ip/MyStatusInfo;

    #@450
    invoke-direct/range {v37 .. v37}, Lcom/lge/ims/service/ip/MyStatusInfo;-><init>()V

    #@453
    .line 2297
    .restart local v37       #objMyStatusInfo:Lcom/lge/ims/service/ip/MyStatusInfo;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@456
    move-result v12

    #@457
    .line 2298
    .local v12, bHasFilePath:I
    const/16 v41, 0x1

    #@459
    move/from16 v0, v41

    #@45b
    if-ne v12, v0, :cond_4a1

    #@45d
    .line 2299
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@460
    move-result-object v41

    #@461
    move-object/from16 v0, v37

    #@463
    move-object/from16 v1, v41

    #@465
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->SetStatusIconThumb(Ljava/lang/String;)V

    #@468
    .line 2300
    new-instance v36, Landroid/os/Message;

    #@46a
    invoke-direct/range {v36 .. v36}, Landroid/os/Message;-><init>()V

    #@46d
    .line 2301
    .restart local v36       #objMessage:Landroid/os/Message;
    new-instance v35, Landroid/os/Bundle;

    #@46f
    invoke-direct/range {v35 .. v35}, Landroid/os/Bundle;-><init>()V

    #@472
    .line 2302
    .restart local v35       #objBundle:Landroid/os/Bundle;
    const/16 v41, 0x2a

    #@474
    move/from16 v0, v41

    #@476
    move-object/from16 v1, v36

    #@478
    iput v0, v1, Landroid/os/Message;->what:I

    #@47a
    .line 2303
    const-string v41, "my_status"

    #@47c
    move-object/from16 v0, v35

    #@47e
    move-object/from16 v1, v41

    #@480
    move-object/from16 v2, v37

    #@482
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    #@485
    .line 2304
    move-object/from16 v0, v36

    #@487
    move-object/from16 v1, v35

    #@489
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@48c
    .line 2305
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@48f
    move-result-object v41

    #@490
    invoke-virtual/range {v41 .. v41}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@493
    move-result-object v41

    #@494
    const-wide/16 v42, 0x64

    #@496
    move-object/from16 v0, v41

    #@498
    move-object/from16 v1, v36

    #@49a
    move-wide/from16 v2, v42

    #@49c
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    #@49f
    goto/16 :goto_21

    #@4a1
    .line 2307
    .end local v35           #objBundle:Landroid/os/Bundle;
    .end local v36           #objMessage:Landroid/os/Message;
    :cond_4a1
    const-string v41, "[IP][ERROR]file path is 0"

    #@4a3
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@4a6
    goto/16 :goto_21

    #@4a8
    .line 2313
    .end local v12           #bHasFilePath:I
    .end local v37           #objMyStatusInfo:Lcom/lge/ims/service/ip/MyStatusInfo;
    :sswitch_4a8
    const-string v41, "[IP]onMessage : GETMYSTATUSICON_IND"

    #@4aa
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@4ad
    .line 2314
    new-instance v37, Lcom/lge/ims/service/ip/MyStatusInfo;

    #@4af
    invoke-direct/range {v37 .. v37}, Lcom/lge/ims/service/ip/MyStatusInfo;-><init>()V

    #@4b2
    .line 2315
    .restart local v37       #objMyStatusInfo:Lcom/lge/ims/service/ip/MyStatusInfo;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@4b5
    move-result v25

    #@4b6
    .line 2316
    .local v25, nContentType:I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@4b9
    move-result v12

    #@4ba
    .line 2317
    .restart local v12       #bHasFilePath:I
    const/16 v41, 0x1

    #@4bc
    move/from16 v0, v41

    #@4be
    if-ne v12, v0, :cond_504

    #@4c0
    .line 2318
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@4c3
    move-result-object v41

    #@4c4
    move-object/from16 v0, v37

    #@4c6
    move-object/from16 v1, v41

    #@4c8
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->SetStatusIcon(Ljava/lang/String;)V

    #@4cb
    .line 2319
    new-instance v36, Landroid/os/Message;

    #@4cd
    invoke-direct/range {v36 .. v36}, Landroid/os/Message;-><init>()V

    #@4d0
    .line 2320
    .restart local v36       #objMessage:Landroid/os/Message;
    new-instance v35, Landroid/os/Bundle;

    #@4d2
    invoke-direct/range {v35 .. v35}, Landroid/os/Bundle;-><init>()V

    #@4d5
    .line 2321
    .restart local v35       #objBundle:Landroid/os/Bundle;
    const/16 v41, 0x2a

    #@4d7
    move/from16 v0, v41

    #@4d9
    move-object/from16 v1, v36

    #@4db
    iput v0, v1, Landroid/os/Message;->what:I

    #@4dd
    .line 2322
    const-string v41, "my_status"

    #@4df
    move-object/from16 v0, v35

    #@4e1
    move-object/from16 v1, v41

    #@4e3
    move-object/from16 v2, v37

    #@4e5
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    #@4e8
    .line 2323
    move-object/from16 v0, v36

    #@4ea
    move-object/from16 v1, v35

    #@4ec
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@4ef
    .line 2324
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@4f2
    move-result-object v41

    #@4f3
    invoke-virtual/range {v41 .. v41}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@4f6
    move-result-object v41

    #@4f7
    const-wide/16 v42, 0x64

    #@4f9
    move-object/from16 v0, v41

    #@4fb
    move-object/from16 v1, v36

    #@4fd
    move-wide/from16 v2, v42

    #@4ff
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    #@502
    goto/16 :goto_21

    #@504
    .line 2326
    .end local v35           #objBundle:Landroid/os/Bundle;
    .end local v36           #objMessage:Landroid/os/Message;
    :cond_504
    const-string v41, "[IP][ERROR]file path is 0"

    #@506
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@509
    goto/16 :goto_21

    #@50b
    .line 2332
    .end local v12           #bHasFilePath:I
    .end local v25           #nContentType:I
    .end local v37           #objMyStatusInfo:Lcom/lge/ims/service/ip/MyStatusInfo;
    :sswitch_50b
    const-string v41, "[IP]onMessage : ADDRCSUSER_IND"

    #@50d
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@510
    .line 2333
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@513
    move-result v24

    #@514
    .line 2334
    .local v24, nAddRcsUserResult:I
    const/16 v41, 0x1

    #@516
    move/from16 v0, v24

    #@518
    move/from16 v1, v41

    #@51a
    if-ne v0, v1, :cond_555

    #@51c
    .line 2335
    new-instance v36, Landroid/os/Message;

    #@51e
    invoke-direct/range {v36 .. v36}, Landroid/os/Message;-><init>()V

    #@521
    .line 2336
    .restart local v36       #objMessage:Landroid/os/Message;
    new-instance v35, Landroid/os/Bundle;

    #@523
    invoke-direct/range {v35 .. v35}, Landroid/os/Bundle;-><init>()V

    #@526
    .line 2337
    .restart local v35       #objBundle:Landroid/os/Bundle;
    const/16 v41, 0x2e

    #@528
    move/from16 v0, v41

    #@52a
    move-object/from16 v1, v36

    #@52c
    iput v0, v1, Landroid/os/Message;->what:I

    #@52e
    .line 2338
    const-string v41, "msisdn"

    #@530
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@533
    move-result-object v42

    #@534
    move-object/from16 v0, v35

    #@536
    move-object/from16 v1, v41

    #@538
    move-object/from16 v2, v42

    #@53a
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@53d
    .line 2339
    move-object/from16 v0, v36

    #@53f
    move-object/from16 v1, v35

    #@541
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@544
    .line 2340
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@547
    move-result-object v41

    #@548
    invoke-virtual/range {v41 .. v41}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@54b
    move-result-object v41

    #@54c
    move-object/from16 v0, v41

    #@54e
    move-object/from16 v1, v36

    #@550
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@553
    goto/16 :goto_21

    #@555
    .line 2342
    .end local v35           #objBundle:Landroid/os/Bundle;
    .end local v36           #objMessage:Landroid/os/Message;
    :cond_555
    const-string v41, "[IP][ERROR]onMessage : ADDRCSUSER_IND - failure"

    #@557
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@55a
    .line 2343
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@55d
    goto/16 :goto_21

    #@55f
    .line 2349
    .end local v24           #nAddRcsUserResult:I
    :sswitch_55f
    const-string v41, "[IP]onMessage : DELETERCSUSER_IND"

    #@561
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@564
    .line 2350
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@567
    move-result v26

    #@568
    .line 2351
    .local v26, nDeleteRcsUserResult:I
    const/16 v41, 0x1

    #@56a
    move/from16 v0, v26

    #@56c
    move/from16 v1, v41

    #@56e
    if-ne v0, v1, :cond_5a9

    #@570
    .line 2352
    new-instance v36, Landroid/os/Message;

    #@572
    invoke-direct/range {v36 .. v36}, Landroid/os/Message;-><init>()V

    #@575
    .line 2353
    .restart local v36       #objMessage:Landroid/os/Message;
    new-instance v35, Landroid/os/Bundle;

    #@577
    invoke-direct/range {v35 .. v35}, Landroid/os/Bundle;-><init>()V

    #@57a
    .line 2354
    .restart local v35       #objBundle:Landroid/os/Bundle;
    const/16 v41, 0x2d

    #@57c
    move/from16 v0, v41

    #@57e
    move-object/from16 v1, v36

    #@580
    iput v0, v1, Landroid/os/Message;->what:I

    #@582
    .line 2355
    const-string v41, "msisdn"

    #@584
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@587
    move-result-object v42

    #@588
    move-object/from16 v0, v35

    #@58a
    move-object/from16 v1, v41

    #@58c
    move-object/from16 v2, v42

    #@58e
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@591
    .line 2356
    move-object/from16 v0, v36

    #@593
    move-object/from16 v1, v35

    #@595
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@598
    .line 2357
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@59b
    move-result-object v41

    #@59c
    invoke-virtual/range {v41 .. v41}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@59f
    move-result-object v41

    #@5a0
    move-object/from16 v0, v41

    #@5a2
    move-object/from16 v1, v36

    #@5a4
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@5a7
    goto/16 :goto_21

    #@5a9
    .line 2359
    .end local v35           #objBundle:Landroid/os/Bundle;
    .end local v36           #objMessage:Landroid/os/Message;
    :cond_5a9
    const-string v41, "[IP][ERROR]onMessage : DELETERCSUSER_IND - failure"

    #@5ab
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@5ae
    .line 2360
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@5b1
    goto/16 :goto_21

    #@5b3
    .line 2366
    .end local v26           #nDeleteRcsUserResult:I
    :sswitch_5b3
    const-string v41, "[IP]onMessage : ADDNONRCSUSER_IND"

    #@5b5
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5b8
    .line 2367
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@5bb
    move-result v24

    #@5bc
    .line 2368
    .restart local v24       #nAddRcsUserResult:I
    const/16 v41, 0x1

    #@5be
    move/from16 v0, v24

    #@5c0
    move/from16 v1, v41

    #@5c2
    if-ne v0, v1, :cond_5fd

    #@5c4
    .line 2369
    new-instance v36, Landroid/os/Message;

    #@5c6
    invoke-direct/range {v36 .. v36}, Landroid/os/Message;-><init>()V

    #@5c9
    .line 2370
    .restart local v36       #objMessage:Landroid/os/Message;
    new-instance v35, Landroid/os/Bundle;

    #@5cb
    invoke-direct/range {v35 .. v35}, Landroid/os/Bundle;-><init>()V

    #@5ce
    .line 2371
    .restart local v35       #objBundle:Landroid/os/Bundle;
    const/16 v41, 0x2f

    #@5d0
    move/from16 v0, v41

    #@5d2
    move-object/from16 v1, v36

    #@5d4
    iput v0, v1, Landroid/os/Message;->what:I

    #@5d6
    .line 2372
    const-string v41, "msisdn"

    #@5d8
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@5db
    move-result-object v42

    #@5dc
    move-object/from16 v0, v35

    #@5de
    move-object/from16 v1, v41

    #@5e0
    move-object/from16 v2, v42

    #@5e2
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@5e5
    .line 2373
    move-object/from16 v0, v36

    #@5e7
    move-object/from16 v1, v35

    #@5e9
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@5ec
    .line 2374
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@5ef
    move-result-object v41

    #@5f0
    invoke-virtual/range {v41 .. v41}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@5f3
    move-result-object v41

    #@5f4
    move-object/from16 v0, v41

    #@5f6
    move-object/from16 v1, v36

    #@5f8
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@5fb
    goto/16 :goto_21

    #@5fd
    .line 2376
    .end local v35           #objBundle:Landroid/os/Bundle;
    .end local v36           #objMessage:Landroid/os/Message;
    :cond_5fd
    const-string v41, "[IP][ERROR]onMessage : ADDNONRCSUSER_IND - failure"

    #@5ff
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@602
    .line 2377
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@605
    goto/16 :goto_21

    #@607
    .line 2383
    .end local v24           #nAddRcsUserResult:I
    :sswitch_607
    const-string v41, "[IP]onMessage : GETBUDDYSTATUSICONTHUMB_IND"

    #@609
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@60c
    .line 2384
    new-instance v38, Lcom/lge/ims/service/ip/PresenceInfo;

    #@60e
    invoke-direct/range {v38 .. v38}, Lcom/lge/ims/service/ip/PresenceInfo;-><init>()V

    #@611
    .line 2385
    .restart local v38       #objPresenceInfo:Lcom/lge/ims/service/ip/PresenceInfo;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@614
    move-result v25

    #@615
    .line 2386
    .restart local v25       #nContentType:I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@618
    move-result v12

    #@619
    .line 2387
    .restart local v12       #bHasFilePath:I
    const/16 v41, 0x1

    #@61b
    move/from16 v0, v41

    #@61d
    if-ne v12, v0, :cond_685

    #@61f
    .line 2388
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@622
    move-result-object v41

    #@623
    move-object/from16 v0, v38

    #@625
    move-object/from16 v1, v41

    #@627
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/PresenceInfo;->SetStatusIconThumb(Ljava/lang/String;)V

    #@62a
    .line 2389
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@62d
    move-result-object v41

    #@62e
    move-object/from16 v0, v38

    #@630
    move-object/from16 v1, v41

    #@632
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/PresenceInfo;->SetMSISDN(Ljava/lang/String;)V

    #@635
    .line 2390
    invoke-static {}, Lcom/lge/ims/service/ip/IPListenerThread;->GetIPListenerThread()Lcom/lge/ims/service/ip/IPListenerThread;

    #@638
    move-result-object v41

    #@639
    if-eqz v41, :cond_645

    #@63b
    invoke-static {}, Lcom/lge/ims/service/ip/IPListenerThread;->GetIPListenerThread()Lcom/lge/ims/service/ip/IPListenerThread;

    #@63e
    move-result-object v41

    #@63f
    invoke-virtual/range {v41 .. v41}, Lcom/lge/ims/service/ip/IPListenerThread;->GetHandler()Landroid/os/Handler;

    #@642
    move-result-object v41

    #@643
    if-nez v41, :cond_64c

    #@645
    .line 2391
    :cond_645
    const-string v41, "[IP][ERROR]IPListenerThread is null"

    #@647
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@64a
    goto/16 :goto_21

    #@64c
    .line 2395
    :cond_64c
    new-instance v36, Landroid/os/Message;

    #@64e
    invoke-direct/range {v36 .. v36}, Landroid/os/Message;-><init>()V

    #@651
    .line 2396
    .restart local v36       #objMessage:Landroid/os/Message;
    new-instance v35, Landroid/os/Bundle;

    #@653
    invoke-direct/range {v35 .. v35}, Landroid/os/Bundle;-><init>()V

    #@656
    .line 2397
    .restart local v35       #objBundle:Landroid/os/Bundle;
    const/16 v41, 0x15

    #@658
    move/from16 v0, v41

    #@65a
    move-object/from16 v1, v36

    #@65c
    iput v0, v1, Landroid/os/Message;->what:I

    #@65e
    .line 2398
    const-string v41, "presence"

    #@660
    move-object/from16 v0, v35

    #@662
    move-object/from16 v1, v41

    #@664
    move-object/from16 v2, v38

    #@666
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    #@669
    .line 2399
    move-object/from16 v0, v36

    #@66b
    move-object/from16 v1, v35

    #@66d
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@670
    .line 2400
    invoke-static {}, Lcom/lge/ims/service/ip/IPListenerThread;->GetIPListenerThread()Lcom/lge/ims/service/ip/IPListenerThread;

    #@673
    move-result-object v41

    #@674
    invoke-virtual/range {v41 .. v41}, Lcom/lge/ims/service/ip/IPListenerThread;->GetHandler()Landroid/os/Handler;

    #@677
    move-result-object v41

    #@678
    const-wide/16 v42, 0x3e8

    #@67a
    move-object/from16 v0, v41

    #@67c
    move-object/from16 v1, v36

    #@67e
    move-wide/from16 v2, v42

    #@680
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    #@683
    goto/16 :goto_21

    #@685
    .line 2402
    .end local v35           #objBundle:Landroid/os/Bundle;
    .end local v36           #objMessage:Landroid/os/Message;
    :cond_685
    const-string v41, "[IP][ERROR]file path is 0"

    #@687
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@68a
    goto/16 :goto_21

    #@68c
    .line 2408
    .end local v12           #bHasFilePath:I
    .end local v25           #nContentType:I
    .end local v38           #objPresenceInfo:Lcom/lge/ims/service/ip/PresenceInfo;
    :sswitch_68c
    const-string v41, "[IP]onMessage : GETBUDDYSTATUSICON_IND"

    #@68e
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@691
    .line 2409
    new-instance v38, Lcom/lge/ims/service/ip/PresenceInfo;

    #@693
    invoke-direct/range {v38 .. v38}, Lcom/lge/ims/service/ip/PresenceInfo;-><init>()V

    #@696
    .line 2410
    .restart local v38       #objPresenceInfo:Lcom/lge/ims/service/ip/PresenceInfo;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@699
    move-result v22

    #@69a
    .line 2411
    .local v22, bResult:I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@69d
    move-result v25

    #@69e
    .line 2412
    .restart local v25       #nContentType:I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@6a1
    move-result v12

    #@6a2
    .line 2413
    .restart local v12       #bHasFilePath:I
    const/4 v4, 0x0

    #@6a3
    .line 2414
    .local v4, StatusIcon:Ljava/lang/String;
    const/16 v41, 0x1

    #@6a5
    move/from16 v0, v41

    #@6a7
    if-ne v12, v0, :cond_6ad

    #@6a9
    .line 2415
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@6ac
    move-result-object v4

    #@6ad
    .line 2417
    :cond_6ad
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@6b0
    move-result-object v40

    #@6b1
    .line 2418
    .local v40, szMSISDN:Ljava/lang/String;
    const/16 v41, 0x1

    #@6b3
    move/from16 v0, v22

    #@6b5
    move/from16 v1, v41

    #@6b7
    if-ne v0, v1, :cond_762

    #@6b9
    .line 2419
    const/16 v41, 0x1

    #@6bb
    move/from16 v0, v41

    #@6bd
    if-ne v12, v0, :cond_76f

    #@6bf
    .line 2420
    invoke-static {}, Lcom/lge/ims/service/ip/IPQueryRepository;->GetQueryRepository()Lcom/lge/ims/service/ip/IPQueryRepository;

    #@6c2
    move-result-object v41

    #@6c3
    move-object/from16 v0, v41

    #@6c5
    move-object/from16 v1, v40

    #@6c7
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/IPQueryRepository;->HasBuddyStatusIconQuery(Ljava/lang/String;)Z

    #@6ca
    move-result v41

    #@6cb
    const/16 v42, 0x1

    #@6cd
    move/from16 v0, v41

    #@6cf
    move/from16 v1, v42

    #@6d1
    if-ne v0, v1, :cond_708

    #@6d3
    .line 2421
    new-instance v23, Landroid/content/Intent;

    #@6d5
    const-string v41, "com.lge.ims.rcs.buddy_icon_downloaded"

    #@6d7
    move-object/from16 v0, v23

    #@6d9
    move-object/from16 v1, v41

    #@6db
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@6de
    .line 2422
    .local v23, intent:Landroid/content/Intent;
    const-string v41, "rcs_photo_file_path"

    #@6e0
    move-object/from16 v0, v23

    #@6e2
    move-object/from16 v1, v41

    #@6e4
    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@6e7
    .line 2423
    const-string v41, "msisdn"

    #@6e9
    move-object/from16 v0, v23

    #@6eb
    move-object/from16 v1, v41

    #@6ed
    move-object/from16 v2, v40

    #@6ef
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@6f2
    .line 2424
    move-object/from16 v0, p0

    #@6f4
    iget-object v0, v0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@6f6
    move-object/from16 v41, v0

    #@6f8
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/service/ip/IPAgent;->access$900(Lcom/lge/ims/service/ip/IPAgent;)Landroid/content/Context;

    #@6fb
    move-result-object v41

    #@6fc
    move-object/from16 v0, v41

    #@6fe
    move-object/from16 v1, v23

    #@700
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@703
    .line 2425
    const-string v41, "[IP]send IP_ACTION_DOWNLOAD_BUDDYICON intent"

    #@705
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@708
    .line 2427
    .end local v23           #intent:Landroid/content/Intent;
    :cond_708
    move-object/from16 v0, v38

    #@70a
    invoke-virtual {v0, v4}, Lcom/lge/ims/service/ip/PresenceInfo;->SetStatusIcon(Ljava/lang/String;)V

    #@70d
    .line 2428
    move-object/from16 v0, v38

    #@70f
    move-object/from16 v1, v40

    #@711
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/PresenceInfo;->SetMSISDN(Ljava/lang/String;)V

    #@714
    .line 2429
    invoke-static {}, Lcom/lge/ims/service/ip/IPListenerThread;->GetIPListenerThread()Lcom/lge/ims/service/ip/IPListenerThread;

    #@717
    move-result-object v41

    #@718
    if-eqz v41, :cond_724

    #@71a
    invoke-static {}, Lcom/lge/ims/service/ip/IPListenerThread;->GetIPListenerThread()Lcom/lge/ims/service/ip/IPListenerThread;

    #@71d
    move-result-object v41

    #@71e
    invoke-virtual/range {v41 .. v41}, Lcom/lge/ims/service/ip/IPListenerThread;->GetHandler()Landroid/os/Handler;

    #@721
    move-result-object v41

    #@722
    if-nez v41, :cond_72b

    #@724
    .line 2430
    :cond_724
    const-string v41, "[IP][ERROR]IPListenerThread is null"

    #@726
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@729
    goto/16 :goto_21

    #@72b
    .line 2433
    :cond_72b
    new-instance v36, Landroid/os/Message;

    #@72d
    invoke-direct/range {v36 .. v36}, Landroid/os/Message;-><init>()V

    #@730
    .line 2434
    .restart local v36       #objMessage:Landroid/os/Message;
    new-instance v35, Landroid/os/Bundle;

    #@732
    invoke-direct/range {v35 .. v35}, Landroid/os/Bundle;-><init>()V

    #@735
    .line 2435
    .restart local v35       #objBundle:Landroid/os/Bundle;
    const/16 v41, 0x15

    #@737
    move/from16 v0, v41

    #@739
    move-object/from16 v1, v36

    #@73b
    iput v0, v1, Landroid/os/Message;->what:I

    #@73d
    .line 2436
    const-string v41, "presence"

    #@73f
    move-object/from16 v0, v35

    #@741
    move-object/from16 v1, v41

    #@743
    move-object/from16 v2, v38

    #@745
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    #@748
    .line 2437
    move-object/from16 v0, v36

    #@74a
    move-object/from16 v1, v35

    #@74c
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@74f
    .line 2438
    invoke-static {}, Lcom/lge/ims/service/ip/IPListenerThread;->GetIPListenerThread()Lcom/lge/ims/service/ip/IPListenerThread;

    #@752
    move-result-object v41

    #@753
    invoke-virtual/range {v41 .. v41}, Lcom/lge/ims/service/ip/IPListenerThread;->GetHandler()Landroid/os/Handler;

    #@756
    move-result-object v41

    #@757
    const-wide/16 v42, 0x3e8

    #@759
    move-object/from16 v0, v41

    #@75b
    move-object/from16 v1, v36

    #@75d
    move-wide/from16 v2, v42

    #@75f
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    #@762
    .line 2443
    .end local v35           #objBundle:Landroid/os/Bundle;
    .end local v36           #objMessage:Landroid/os/Message;
    :cond_762
    :goto_762
    invoke-static {}, Lcom/lge/ims/service/ip/IPQueryRepository;->GetQueryRepository()Lcom/lge/ims/service/ip/IPQueryRepository;

    #@765
    move-result-object v41

    #@766
    move-object/from16 v0, v41

    #@768
    move-object/from16 v1, v40

    #@76a
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/IPQueryRepository;->RemoveBuddyStatusIconQuery(Ljava/lang/String;)V

    #@76d
    goto/16 :goto_21

    #@76f
    .line 2440
    :cond_76f
    const-string v41, "[IP][ERROR]file path is 0"

    #@771
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@774
    goto :goto_762

    #@775
    .line 2448
    .end local v4           #StatusIcon:Ljava/lang/String;
    .end local v12           #bHasFilePath:I
    .end local v22           #bResult:I
    .end local v25           #nContentType:I
    .end local v38           #objPresenceInfo:Lcom/lge/ims/service/ip/PresenceInfo;
    .end local v40           #szMSISDN:Ljava/lang/String;
    :sswitch_775
    const-string v41, "[IP]onMessage : GETXDMDOCUMENT_IND"

    #@777
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@77a
    .line 2449
    new-instance v39, Lcom/lge/ims/service/ip/XDMEtagInfo;

    #@77c
    invoke-direct/range {v39 .. v39}, Lcom/lge/ims/service/ip/XDMEtagInfo;-><init>()V

    #@77f
    .line 2450
    .local v39, objXdmEtagInfo:Lcom/lge/ims/service/ip/XDMEtagInfo;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@782
    move-result v27

    #@783
    .line 2451
    .local v27, nHasListETagInfo:I
    const/16 v41, 0x1

    #@785
    move/from16 v0, v27

    #@787
    move/from16 v1, v41

    #@789
    if-ne v0, v1, :cond_7b6

    #@78b
    .line 2452
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@78e
    move-result-object v41

    #@78f
    move-object/from16 v0, v39

    #@791
    move-object/from16 v1, v41

    #@793
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/XDMEtagInfo;->SetListEtag(Ljava/lang/String;)V

    #@796
    .line 2453
    new-instance v41, Ljava/lang/StringBuilder;

    #@798
    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    #@79b
    const-string v42, "[IP]: szListETagInfo ["

    #@79d
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a0
    move-result-object v41

    #@7a1
    invoke-virtual/range {v39 .. v39}, Lcom/lge/ims/service/ip/XDMEtagInfo;->GetListEtag()Ljava/lang/String;

    #@7a4
    move-result-object v42

    #@7a5
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a8
    move-result-object v41

    #@7a9
    const-string v42, "]"

    #@7ab
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7ae
    move-result-object v41

    #@7af
    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b2
    move-result-object v41

    #@7b3
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@7b6
    .line 2455
    :cond_7b6
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@7b9
    move-result v29

    #@7ba
    .line 2456
    .local v29, nHasRLSETagInfo:I
    const/16 v41, 0x1

    #@7bc
    move/from16 v0, v29

    #@7be
    move/from16 v1, v41

    #@7c0
    if-ne v0, v1, :cond_7ed

    #@7c2
    .line 2457
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@7c5
    move-result-object v41

    #@7c6
    move-object/from16 v0, v39

    #@7c8
    move-object/from16 v1, v41

    #@7ca
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/XDMEtagInfo;->SetRLSEtag(Ljava/lang/String;)V

    #@7cd
    .line 2458
    new-instance v41, Ljava/lang/StringBuilder;

    #@7cf
    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    #@7d2
    const-string v42, "[IP]: szRLSETagInfo ["

    #@7d4
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d7
    move-result-object v41

    #@7d8
    invoke-virtual/range {v39 .. v39}, Lcom/lge/ims/service/ip/XDMEtagInfo;->GetRLSEtag()Ljava/lang/String;

    #@7db
    move-result-object v42

    #@7dc
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7df
    move-result-object v41

    #@7e0
    const-string v42, "]"

    #@7e2
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e5
    move-result-object v41

    #@7e6
    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e9
    move-result-object v41

    #@7ea
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@7ed
    .line 2460
    :cond_7ed
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@7f0
    move-result v31

    #@7f1
    .line 2461
    .local v31, nHasRuleETagInfo:I
    const/16 v41, 0x1

    #@7f3
    move/from16 v0, v31

    #@7f5
    move/from16 v1, v41

    #@7f7
    if-ne v0, v1, :cond_824

    #@7f9
    .line 2462
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@7fc
    move-result-object v41

    #@7fd
    move-object/from16 v0, v39

    #@7ff
    move-object/from16 v1, v41

    #@801
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/XDMEtagInfo;->SetRuleEtag(Ljava/lang/String;)V

    #@804
    .line 2463
    new-instance v41, Ljava/lang/StringBuilder;

    #@806
    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    #@809
    const-string v42, "[IP]: szRuleETagInfo ["

    #@80b
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80e
    move-result-object v41

    #@80f
    invoke-virtual/range {v39 .. v39}, Lcom/lge/ims/service/ip/XDMEtagInfo;->GetRuleEtag()Ljava/lang/String;

    #@812
    move-result-object v42

    #@813
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@816
    move-result-object v41

    #@817
    const-string v42, "]"

    #@819
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81c
    move-result-object v41

    #@81d
    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@820
    move-result-object v41

    #@821
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@824
    .line 2465
    :cond_824
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@827
    move-result v28

    #@828
    .line 2466
    .local v28, nHasPIDFETagInfo:I
    const/16 v41, 0x1

    #@82a
    move/from16 v0, v28

    #@82c
    move/from16 v1, v41

    #@82e
    if-ne v0, v1, :cond_85b

    #@830
    .line 2467
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@833
    move-result-object v41

    #@834
    move-object/from16 v0, v39

    #@836
    move-object/from16 v1, v41

    #@838
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/XDMEtagInfo;->SetPidfEtag(Ljava/lang/String;)V

    #@83b
    .line 2468
    new-instance v41, Ljava/lang/StringBuilder;

    #@83d
    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    #@840
    const-string v42, "[IP]: szPIDFETagInfo ["

    #@842
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@845
    move-result-object v41

    #@846
    invoke-virtual/range {v39 .. v39}, Lcom/lge/ims/service/ip/XDMEtagInfo;->GetPidfEtag()Ljava/lang/String;

    #@849
    move-result-object v42

    #@84a
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84d
    move-result-object v41

    #@84e
    const-string v42, "]"

    #@850
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@853
    move-result-object v41

    #@854
    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@857
    move-result-object v41

    #@858
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@85b
    .line 2470
    :cond_85b
    new-instance v36, Landroid/os/Message;

    #@85d
    invoke-direct/range {v36 .. v36}, Landroid/os/Message;-><init>()V

    #@860
    .line 2471
    .restart local v36       #objMessage:Landroid/os/Message;
    new-instance v35, Landroid/os/Bundle;

    #@862
    invoke-direct/range {v35 .. v35}, Landroid/os/Bundle;-><init>()V

    #@865
    .line 2472
    .restart local v35       #objBundle:Landroid/os/Bundle;
    const/16 v41, 0x2b

    #@867
    move/from16 v0, v41

    #@869
    move-object/from16 v1, v36

    #@86b
    iput v0, v1, Landroid/os/Message;->what:I

    #@86d
    .line 2473
    const-string v41, "xdm_doc"

    #@86f
    move-object/from16 v0, v35

    #@871
    move-object/from16 v1, v41

    #@873
    move-object/from16 v2, v39

    #@875
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    #@878
    .line 2474
    move-object/from16 v0, v36

    #@87a
    move-object/from16 v1, v35

    #@87c
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@87f
    .line 2475
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@882
    move-result-object v41

    #@883
    invoke-virtual/range {v41 .. v41}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@886
    move-result-object v41

    #@887
    const-wide/16 v42, 0x64

    #@889
    move-object/from16 v0, v41

    #@88b
    move-object/from16 v1, v36

    #@88d
    move-wide/from16 v2, v42

    #@88f
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    #@892
    goto/16 :goto_21

    #@894
    .line 2480
    .end local v27           #nHasListETagInfo:I
    .end local v28           #nHasPIDFETagInfo:I
    .end local v29           #nHasRLSETagInfo:I
    .end local v31           #nHasRuleETagInfo:I
    .end local v35           #objBundle:Landroid/os/Bundle;
    .end local v36           #objMessage:Landroid/os/Message;
    .end local v39           #objXdmEtagInfo:Lcom/lge/ims/service/ip/XDMEtagInfo;
    :sswitch_894
    const-string v41, "[IP]onMessage : GETRLSLISTURI_IND"

    #@896
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@899
    .line 2481
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@89c
    move-result v30

    #@89d
    .line 2482
    .local v30, nHasRLSListUri:I
    const/16 v41, 0x1

    #@89f
    move/from16 v0, v30

    #@8a1
    move/from16 v1, v41

    #@8a3
    if-ne v0, v1, :cond_8dc

    #@8a5
    .line 2483
    new-instance v36, Landroid/os/Message;

    #@8a7
    invoke-direct/range {v36 .. v36}, Landroid/os/Message;-><init>()V

    #@8aa
    .line 2484
    .restart local v36       #objMessage:Landroid/os/Message;
    new-instance v35, Landroid/os/Bundle;

    #@8ac
    invoke-direct/range {v35 .. v35}, Landroid/os/Bundle;-><init>()V

    #@8af
    .line 2485
    .restart local v35       #objBundle:Landroid/os/Bundle;
    const/16 v41, 0x28

    #@8b1
    move/from16 v0, v41

    #@8b3
    move-object/from16 v1, v36

    #@8b5
    iput v0, v1, Landroid/os/Message;->what:I

    #@8b7
    .line 2486
    const-string v41, "rls_list_uri"

    #@8b9
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@8bc
    move-result-object v42

    #@8bd
    move-object/from16 v0, v35

    #@8bf
    move-object/from16 v1, v41

    #@8c1
    move-object/from16 v2, v42

    #@8c3
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@8c6
    .line 2487
    move-object/from16 v0, v36

    #@8c8
    move-object/from16 v1, v35

    #@8ca
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@8cd
    .line 2488
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@8d0
    move-result-object v41

    #@8d1
    invoke-virtual/range {v41 .. v41}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@8d4
    move-result-object v41

    #@8d5
    move-object/from16 v0, v41

    #@8d7
    move-object/from16 v1, v36

    #@8d9
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@8dc
    .line 2490
    .end local v35           #objBundle:Landroid/os/Bundle;
    .end local v36           #objMessage:Landroid/os/Message;
    :cond_8dc
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@8df
    move-result-object v41

    #@8e0
    invoke-virtual/range {v41 .. v41}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@8e3
    move-result-object v41

    #@8e4
    const/16 v42, 0x17

    #@8e6
    const-wide/16 v43, 0x3e8

    #@8e8
    invoke-virtual/range {v41 .. v44}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    #@8eb
    goto/16 :goto_21

    #@8ed
    .line 2495
    .end local v30           #nHasRLSListUri:I
    :sswitch_8ed
    const-string v41, "[IP]onMessage : RLSSYNCFAILED_IND"

    #@8ef
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@8f2
    .line 2496
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@8f5
    move-result-object v41

    #@8f6
    if-nez v41, :cond_8ff

    #@8f8
    .line 2497
    const-string v41, "[IP][ERROR]onMessage : IPAgent.GetIPAgent() is NULL"

    #@8fa
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@8fd
    goto/16 :goto_21

    #@8ff
    .line 2499
    :cond_8ff
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@902
    move-result-object v41

    #@903
    invoke-virtual/range {v41 .. v41}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@906
    move-result-object v41

    #@907
    if-nez v41, :cond_910

    #@909
    .line 2500
    const-string v41, "[IP][ERROR]onMessage : IPAgent.GetIPAgent().GetHandler() is NULL"

    #@90b
    invoke-static/range {v41 .. v41}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@90e
    goto/16 :goto_21

    #@910
    .line 2503
    :cond_910
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@913
    move-result-object v41

    #@914
    invoke-virtual/range {v41 .. v41}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@917
    move-result-object v41

    #@918
    const/16 v42, 0x18

    #@91a
    const-wide/16 v43, 0x1388

    #@91c
    invoke-virtual/range {v41 .. v44}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    #@91f
    goto/16 :goto_21

    #@921
    .line 2054
    nop

    #@922
    :sswitch_data_922
    .sparse-switch
        0x2a30 -> :sswitch_22
        0x2a31 -> :sswitch_51
        0x2a32 -> :sswitch_95
        0x2a3b -> :sswitch_c7
        0x2a45 -> :sswitch_219
        0x2a47 -> :sswitch_391
        0x2a48 -> :sswitch_398
        0x2a49 -> :sswitch_39f
        0x2a4a -> :sswitch_3b1
        0x2a4f -> :sswitch_3c3
        0x2a53 -> :sswitch_449
        0x2a55 -> :sswitch_4a8
        0x2a59 -> :sswitch_50b
        0x2a5b -> :sswitch_55f
        0x2a5d -> :sswitch_5b3
        0x2a61 -> :sswitch_607
        0x2a63 -> :sswitch_68c
        0x2a6d -> :sswitch_775
        0x2a6e -> :sswitch_894
        0x2a6f -> :sswitch_8ed
    .end sparse-switch
.end method

.method public setValue(II)V
    .registers 5
    .parameter "_nUsePresence"
    .parameter "_nUserPresenceSocialInformation"

    #@0
    .prologue
    .line 2039
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "[IP] : _nUsePresence [ "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    const-string v1, " ], _nUserPresenceSocialInformation [ "

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    const-string v1, "]"

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@26
    .line 2040
    iput p1, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nUsePresence:I

    #@28
    .line 2041
    iput p2, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nUserPresenceSocialInformation:I

    #@2a
    .line 2042
    return-void
.end method

.method public terminate()V
    .registers 2

    #@0
    .prologue
    .line 2048
    iget v0, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nativeObj:I

    #@2
    invoke-static {v0, p0}, Lcom/lge/ims/JNICore;->removeListener(ILcom/lge/ims/JNICoreListener;)I

    #@5
    .line 2049
    iget v0, p0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->nativeObj:I

    #@7
    invoke-static {v0}, Lcom/lge/ims/JNICore;->releaseInterface(I)I

    #@a
    .line 2050
    return-void
.end method
