.class public Lcom/lge/ims/service/ip/IPAgent$IPTimer;
.super Ljava/lang/Object;
.source "IPAgent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/ip/IPAgent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "IPTimer"
.end annotation


# instance fields
.field private nPollingPeriod:J

.field private nRLSSubPollingPeriod:J

.field final synthetic this$0:Lcom/lge/ims/service/ip/IPAgent;


# direct methods
.method public constructor <init>(Lcom/lge/ims/service/ip/IPAgent;)V
    .registers 4
    .parameter

    #@0
    .prologue
    const-wide/16 v0, 0x0

    #@2
    .line 1676
    iput-object p1, p0, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    .line 1677
    iput-wide v0, p0, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->nPollingPeriod:J

    #@9
    .line 1678
    iput-wide v0, p0, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->nRLSSubPollingPeriod:J

    #@b
    .line 1679
    return-void
.end method

.method private KillAlarm()Z
    .registers 6

    #@0
    .prologue
    const/16 v4, 0x5015

    #@2
    const/4 v2, 0x0

    #@3
    .line 1803
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->access$800()Landroid/app/AlarmManager;

    #@6
    move-result-object v3

    #@7
    if-nez v3, :cond_a

    #@9
    .line 1810
    :goto_9
    return v2

    #@a
    .line 1806
    :cond_a
    new-instance v0, Landroid/content/Intent;

    #@c
    const-string v3, "com.lge.ims.ip.polling"

    #@e
    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@11
    .line 1807
    .local v0, objIntent:Landroid/content/Intent;
    const-string v3, "ip_alarm_id"

    #@13
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@16
    .line 1808
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@18
    invoke-static {v3}, Lcom/lge/ims/service/ip/IPAgent;->access$900(Lcom/lge/ims/service/ip/IPAgent;)Landroid/content/Context;

    #@1b
    move-result-object v3

    #@1c
    invoke-static {v3, v4, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@1f
    move-result-object v1

    #@20
    .line 1809
    .local v1, objSender:Landroid/app/PendingIntent;
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->access$800()Landroid/app/AlarmManager;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    #@27
    .line 1810
    const/4 v2, 0x1

    #@28
    goto :goto_9
.end method

.method private KillRLSSubAlarm()Z
    .registers 6

    #@0
    .prologue
    const/16 v4, 0x5016

    #@2
    const/4 v2, 0x0

    #@3
    .line 1830
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->access$800()Landroid/app/AlarmManager;

    #@6
    move-result-object v3

    #@7
    if-nez v3, :cond_a

    #@9
    .line 1837
    :goto_9
    return v2

    #@a
    .line 1833
    :cond_a
    new-instance v0, Landroid/content/Intent;

    #@c
    const-string v3, "com.lge.ims.ip.polling"

    #@e
    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@11
    .line 1834
    .local v0, objIntent:Landroid/content/Intent;
    const-string v3, "ip_alarm_id"

    #@13
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@16
    .line 1835
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@18
    invoke-static {v3}, Lcom/lge/ims/service/ip/IPAgent;->access$900(Lcom/lge/ims/service/ip/IPAgent;)Landroid/content/Context;

    #@1b
    move-result-object v3

    #@1c
    invoke-static {v3, v4, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@1f
    move-result-object v1

    #@20
    .line 1836
    .local v1, objSender:Landroid/app/PendingIntent;
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->access$800()Landroid/app/AlarmManager;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    #@27
    .line 1837
    const/4 v2, 0x1

    #@28
    goto :goto_9
.end method

.method private SetAlarm(J)Z
    .registers 11
    .parameter "nDuration"

    #@0
    .prologue
    const/16 v7, 0x5015

    #@2
    const/4 v6, 0x0

    #@3
    .line 1786
    new-instance v4, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v5, "[IP]SetAlarm : duration ["

    #@a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v4

    #@e
    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@11
    move-result-object v4

    #@12
    const-string v5, "] "

    #@14
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v4

    #@18
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v4

    #@1c
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@1f
    .line 1788
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->access$800()Landroid/app/AlarmManager;

    #@22
    move-result-object v4

    #@23
    if-nez v4, :cond_36

    #@25
    .line 1789
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@27
    invoke-static {v4}, Lcom/lge/ims/service/ip/IPAgent;->access$900(Lcom/lge/ims/service/ip/IPAgent;)Landroid/content/Context;

    #@2a
    move-result-object v4

    #@2b
    const-string v5, "alarm"

    #@2d
    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@30
    move-result-object v4

    #@31
    check-cast v4, Landroid/app/AlarmManager;

    #@33
    invoke-static {v4}, Lcom/lge/ims/service/ip/IPAgent;->access$802(Landroid/app/AlarmManager;)Landroid/app/AlarmManager;

    #@36
    .line 1792
    :cond_36
    new-instance v2, Landroid/content/Intent;

    #@38
    const-string v4, "com.lge.ims.ip.polling"

    #@3a
    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@3d
    .line 1793
    .local v2, objIntent:Landroid/content/Intent;
    const-string v4, "ip_alarm_id"

    #@3f
    invoke-virtual {v2, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@42
    .line 1794
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@44
    invoke-static {v4}, Lcom/lge/ims/service/ip/IPAgent;->access$900(Lcom/lge/ims/service/ip/IPAgent;)Landroid/content/Context;

    #@47
    move-result-object v4

    #@48
    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    #@4b
    move-result-object v4

    #@4c
    invoke-static {v4, v7, v2, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@4f
    move-result-object v3

    #@50
    .line 1796
    .local v3, objSender:Landroid/app/PendingIntent;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@53
    move-result-wide v4

    #@54
    add-long v0, v4, p1

    #@56
    .line 1798
    .local v0, nTriggerTime:J
    new-instance v4, Ljava/lang/StringBuilder;

    #@58
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5b
    const-string v5, "[IP]SetAlarm : Set ["

    #@5d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v4

    #@61
    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@64
    move-result-object v4

    #@65
    const-string v5, "]"

    #@67
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v4

    #@6b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6e
    move-result-object v4

    #@6f
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@72
    .line 1799
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->access$800()Landroid/app/AlarmManager;

    #@75
    move-result-object v4

    #@76
    invoke-virtual {v4, v6, v0, v1, v3}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    #@79
    .line 1800
    const/4 v4, 0x1

    #@7a
    return v4
.end method

.method private SetRLSSubAlarm(J)Z
    .registers 11
    .parameter "nDuration"

    #@0
    .prologue
    const/16 v7, 0x5016

    #@2
    const/4 v6, 0x0

    #@3
    .line 1813
    new-instance v4, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v5, "[IP]SetRLSSubAlarm : duration ["

    #@a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v4

    #@e
    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@11
    move-result-object v4

    #@12
    const-string v5, "] "

    #@14
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v4

    #@18
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v4

    #@1c
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@1f
    .line 1815
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->access$800()Landroid/app/AlarmManager;

    #@22
    move-result-object v4

    #@23
    if-nez v4, :cond_36

    #@25
    .line 1816
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@27
    invoke-static {v4}, Lcom/lge/ims/service/ip/IPAgent;->access$900(Lcom/lge/ims/service/ip/IPAgent;)Landroid/content/Context;

    #@2a
    move-result-object v4

    #@2b
    const-string v5, "alarm"

    #@2d
    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@30
    move-result-object v4

    #@31
    check-cast v4, Landroid/app/AlarmManager;

    #@33
    invoke-static {v4}, Lcom/lge/ims/service/ip/IPAgent;->access$802(Landroid/app/AlarmManager;)Landroid/app/AlarmManager;

    #@36
    .line 1819
    :cond_36
    new-instance v2, Landroid/content/Intent;

    #@38
    const-string v4, "com.lge.ims.ip.polling"

    #@3a
    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@3d
    .line 1820
    .local v2, objIntent:Landroid/content/Intent;
    const-string v4, "ip_alarm_id"

    #@3f
    invoke-virtual {v2, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@42
    .line 1821
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@44
    invoke-static {v4}, Lcom/lge/ims/service/ip/IPAgent;->access$900(Lcom/lge/ims/service/ip/IPAgent;)Landroid/content/Context;

    #@47
    move-result-object v4

    #@48
    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    #@4b
    move-result-object v4

    #@4c
    invoke-static {v4, v7, v2, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@4f
    move-result-object v3

    #@50
    .line 1823
    .local v3, objSender:Landroid/app/PendingIntent;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@53
    move-result-wide v4

    #@54
    add-long v0, v4, p1

    #@56
    .line 1825
    .local v0, nTriggerTime:J
    new-instance v4, Ljava/lang/StringBuilder;

    #@58
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5b
    const-string v5, "[IP]SetRLSSubAlarm : Set ["

    #@5d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v4

    #@61
    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@64
    move-result-object v4

    #@65
    const-string v5, "]"

    #@67
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v4

    #@6b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6e
    move-result-object v4

    #@6f
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@72
    .line 1826
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->access$800()Landroid/app/AlarmManager;

    #@75
    move-result-object v4

    #@76
    invoke-virtual {v4, v6, v0, v1, v3}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    #@79
    .line 1827
    const/4 v4, 0x1

    #@7a
    return v4
.end method


# virtual methods
.method public CheckPollingTimer()V
    .registers 5

    #@0
    .prologue
    const-wide/16 v2, 0x0

    #@2
    .line 1710
    iget-wide v0, p0, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->nPollingPeriod:J

    #@4
    cmp-long v0, v0, v2

    #@6
    if-lez v0, :cond_17

    #@8
    .line 1711
    invoke-virtual {p0}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->GetRestTime()J

    #@b
    move-result-wide v0

    #@c
    cmp-long v0, v0, v2

    #@e
    if-lez v0, :cond_18

    #@10
    .line 1712
    invoke-virtual {p0}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->GetRestTime()J

    #@13
    move-result-wide v0

    #@14
    invoke-direct {p0, v0, v1}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->SetAlarm(J)Z

    #@17
    .line 1724
    :cond_17
    :goto_17
    return-void

    #@18
    .line 1714
    :cond_18
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@1b
    move-result-object v0

    #@1c
    if-nez v0, :cond_24

    #@1e
    .line 1715
    const-string v0, "[IP][ERROR] IPAgent.GetIPAgent() is NULL"

    #@20
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@23
    goto :goto_17

    #@24
    .line 1717
    :cond_24
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@27
    move-result-object v0

    #@28
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@2b
    move-result-object v0

    #@2c
    if-nez v0, :cond_34

    #@2e
    .line 1718
    const-string v0, "[IP][ERROR] IPAgent.GetIPAgent().GetHandler() is NULL"

    #@30
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@33
    goto :goto_17

    #@34
    .line 1721
    :cond_34
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@37
    move-result-object v0

    #@38
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@3b
    move-result-object v0

    #@3c
    const/4 v1, 0x4

    #@3d
    const-wide/16 v2, 0x3e8

    #@3f
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@42
    goto :goto_17
.end method

.method public CheckPollingTimer(J)V
    .registers 7
    .parameter "_resetTimer"

    #@0
    .prologue
    const-wide/16 v2, 0x0

    #@2
    .line 1693
    iget-wide v0, p0, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->nPollingPeriod:J

    #@4
    cmp-long v0, v0, v2

    #@6
    if-lez v0, :cond_f

    #@8
    .line 1694
    cmp-long v0, p1, v2

    #@a
    if-lez v0, :cond_10

    #@c
    .line 1695
    invoke-direct {p0, p1, p2}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->SetAlarm(J)Z

    #@f
    .line 1708
    :cond_f
    :goto_f
    return-void

    #@10
    .line 1697
    :cond_10
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@13
    move-result-object v0

    #@14
    if-nez v0, :cond_1c

    #@16
    .line 1698
    const-string v0, "[IP][ERROR] IPAgent.GetIPAgent() is NULL"

    #@18
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@1b
    goto :goto_f

    #@1c
    .line 1700
    :cond_1c
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@1f
    move-result-object v0

    #@20
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@23
    move-result-object v0

    #@24
    if-nez v0, :cond_2c

    #@26
    .line 1701
    const-string v0, "[IP][ERROR] IPAgent.GetIPAgent().GetHandler() is NULL"

    #@28
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@2b
    goto :goto_f

    #@2c
    .line 1704
    :cond_2c
    const-string v0, "[IP][ERROR] CheckPollingTimer(timer) : reset timer is not > 0. so send expired"

    #@2e
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@31
    .line 1705
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@34
    move-result-object v0

    #@35
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@38
    move-result-object v0

    #@39
    const/4 v1, 0x4

    #@3a
    const-wide/16 v2, 0x3e8

    #@3c
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@3f
    goto :goto_f
.end method

.method public CheckRLSSubPollingTimer()V
    .registers 5

    #@0
    .prologue
    const-wide/16 v2, 0x0

    #@2
    .line 1761
    iget-wide v0, p0, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->nRLSSubPollingPeriod:J

    #@4
    cmp-long v0, v0, v2

    #@6
    if-lez v0, :cond_17

    #@8
    .line 1762
    invoke-virtual {p0}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->GetRLSSubRestTime()J

    #@b
    move-result-wide v0

    #@c
    cmp-long v0, v0, v2

    #@e
    if-lez v0, :cond_18

    #@10
    .line 1763
    invoke-virtual {p0}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->GetRLSSubRestTime()J

    #@13
    move-result-wide v0

    #@14
    invoke-direct {p0, v0, v1}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->SetRLSSubAlarm(J)Z

    #@17
    .line 1775
    :cond_17
    :goto_17
    return-void

    #@18
    .line 1765
    :cond_18
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@1b
    move-result-object v0

    #@1c
    if-nez v0, :cond_24

    #@1e
    .line 1766
    const-string v0, "[IP][ERROR] IPAgent.GetIPAgent() is NULL"

    #@20
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@23
    goto :goto_17

    #@24
    .line 1768
    :cond_24
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@27
    move-result-object v0

    #@28
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@2b
    move-result-object v0

    #@2c
    if-nez v0, :cond_34

    #@2e
    .line 1769
    const-string v0, "[IP][ERROR] IPAgent.GetIPAgent().GetHandler() is NULL"

    #@30
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@33
    goto :goto_17

    #@34
    .line 1772
    :cond_34
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@37
    move-result-object v0

    #@38
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@3b
    move-result-object v0

    #@3c
    const/4 v1, 0x5

    #@3d
    const-wide/16 v2, 0x3e8

    #@3f
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@42
    goto :goto_17
.end method

.method public CheckRLSSubPollingTimer(J)V
    .registers 7
    .parameter "_resetTimer"

    #@0
    .prologue
    const-wide/16 v2, 0x0

    #@2
    .line 1744
    iget-wide v0, p0, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->nRLSSubPollingPeriod:J

    #@4
    cmp-long v0, v0, v2

    #@6
    if-lez v0, :cond_f

    #@8
    .line 1745
    cmp-long v0, p1, v2

    #@a
    if-lez v0, :cond_10

    #@c
    .line 1746
    invoke-direct {p0, p1, p2}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->SetRLSSubAlarm(J)Z

    #@f
    .line 1759
    :cond_f
    :goto_f
    return-void

    #@10
    .line 1748
    :cond_10
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@13
    move-result-object v0

    #@14
    if-nez v0, :cond_1c

    #@16
    .line 1749
    const-string v0, "[IP][ERROR] IPAgent.GetIPAgent() is NULL"

    #@18
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@1b
    goto :goto_f

    #@1c
    .line 1751
    :cond_1c
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@1f
    move-result-object v0

    #@20
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@23
    move-result-object v0

    #@24
    if-nez v0, :cond_2c

    #@26
    .line 1752
    const-string v0, "[IP][ERROR] IPAgent.GetIPAgent().GetHandler() is NULL"

    #@28
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@2b
    goto :goto_f

    #@2c
    .line 1755
    :cond_2c
    const-string v0, "[IP][ERROR] CheckRLSSubPollingTimer(timer) : reset timer is not > 0. so send expired"

    #@2e
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@31
    .line 1756
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@34
    move-result-object v0

    #@35
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@38
    move-result-object v0

    #@39
    const/4 v1, 0x5

    #@3a
    const-wide/16 v2, 0x3e8

    #@3c
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@3f
    goto :goto_f
.end method

.method public GetPollingTime()J
    .registers 3

    #@0
    .prologue
    .line 1726
    iget-wide v0, p0, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->nPollingPeriod:J

    #@2
    return-wide v0
.end method

.method public GetRLSSubPollingTime()J
    .registers 3

    #@0
    .prologue
    .line 1777
    iget-wide v0, p0, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->nRLSSubPollingPeriod:J

    #@2
    return-wide v0
.end method

.method public GetRLSSubRestTime()J
    .registers 8

    #@0
    .prologue
    .line 1780
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@2
    invoke-static {v2}, Lcom/lge/ims/service/ip/IPAgent;->access$300(Lcom/lge/ims/service/ip/IPAgent;)Lcom/lge/ims/service/ip/IPContactHelper;

    #@5
    move-result-object v2

    #@6
    invoke-virtual {v2}, Lcom/lge/ims/service/ip/IPContactHelper;->GetRLSSubPollingLatestTime()J

    #@9
    move-result-wide v0

    #@a
    .line 1781
    .local v0, nRLSSubPollingLatestTime:J
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "[IP]GetRLSSubRestTime : DB ["

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    const-string v3, "], Current ["

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@22
    move-result-wide v3

    #@23
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    const-string v3, "]"

    #@29
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v2

    #@31
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@34
    .line 1782
    new-instance v2, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v3, "[IP]GetRLSSubRestTime : Total ["

    #@3b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v2

    #@3f
    iget-wide v3, p0, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->nRLSSubPollingPeriod:J

    #@41
    add-long/2addr v3, v0

    #@42
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@45
    move-result-wide v5

    #@46
    sub-long/2addr v3, v5

    #@47
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    const-string v3, "]"

    #@4d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v2

    #@51
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v2

    #@55
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@58
    .line 1783
    iget-wide v2, p0, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->nRLSSubPollingPeriod:J

    #@5a
    add-long/2addr v2, v0

    #@5b
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@5e
    move-result-wide v4

    #@5f
    sub-long/2addr v2, v4

    #@60
    return-wide v2
.end method

.method public GetRestTime()J
    .registers 8

    #@0
    .prologue
    .line 1729
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@2
    invoke-static {v2}, Lcom/lge/ims/service/ip/IPAgent;->access$300(Lcom/lge/ims/service/ip/IPAgent;)Lcom/lge/ims/service/ip/IPContactHelper;

    #@5
    move-result-object v2

    #@6
    invoke-virtual {v2}, Lcom/lge/ims/service/ip/IPContactHelper;->GetPollingLatestTime()J

    #@9
    move-result-wide v0

    #@a
    .line 1730
    .local v0, nPollingLatestTime:J
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "[IP]GetRestTime : DB ["

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    const-string v3, "], Current ["

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@22
    move-result-wide v3

    #@23
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    const-string v3, "]"

    #@29
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v2

    #@31
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@34
    .line 1731
    new-instance v2, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v3, "[IP]GetRestTime : Total ["

    #@3b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v2

    #@3f
    iget-wide v3, p0, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->nPollingPeriod:J

    #@41
    add-long/2addr v3, v0

    #@42
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@45
    move-result-wide v5

    #@46
    sub-long/2addr v3, v5

    #@47
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    const-string v3, "]"

    #@4d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v2

    #@51
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v2

    #@55
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@58
    .line 1732
    iget-wide v2, p0, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->nPollingPeriod:J

    #@5a
    add-long/2addr v2, v0

    #@5b
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@5e
    move-result-wide v4

    #@5f
    sub-long/2addr v2, v4

    #@60
    return-wide v2
.end method

.method public StopPollingTimer()V
    .registers 2

    #@0
    .prologue
    .line 1687
    invoke-direct {p0}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->KillAlarm()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_b

    #@6
    .line 1688
    const-string v0, "[IP][ERROR]StopPollingTimer : KillAlarm is null"

    #@8
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@b
    .line 1690
    :cond_b
    return-void
.end method

.method public StopRLSSubPollingTimer()V
    .registers 1

    #@0
    .prologue
    .line 1741
    invoke-direct {p0}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->KillRLSSubAlarm()Z

    #@3
    .line 1742
    return-void
.end method

.method public setPollingPeriod(I)V
    .registers 4
    .parameter "_nPollingPeriod"

    #@0
    .prologue
    .line 1681
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "[IP]IPTimer : PollingPeriod["

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    const-string v1, "]"

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@1c
    .line 1682
    if-lez p1, :cond_23

    #@1e
    .line 1683
    mul-int/lit16 v0, p1, 0x3e8

    #@20
    int-to-long v0, v0

    #@21
    iput-wide v0, p0, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->nPollingPeriod:J

    #@23
    .line 1685
    :cond_23
    return-void
.end method

.method public setRLSSubPeriod(I)V
    .registers 4
    .parameter "_nRLSSubPeriodTimer"

    #@0
    .prologue
    .line 1735
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "[IP]IPTimer : setRLSSubPeriod["

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    const-string v1, "]"

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@1c
    .line 1736
    if-lez p1, :cond_23

    #@1e
    .line 1737
    mul-int/lit16 v0, p1, 0x3e8

    #@20
    int-to-long v0, v0

    #@21
    iput-wide v0, p0, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->nRLSSubPollingPeriod:J

    #@23
    .line 1739
    :cond_23
    return-void
.end method
