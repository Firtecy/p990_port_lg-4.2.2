.class public Lcom/lge/ims/service/im/IMFileInfo;
.super Ljava/lang/Object;
.source "IMFileInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/lge/ims/service/im/IMFileInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private contentType:Ljava/lang/String;

.field private expiryDateTime:Ljava/lang/String;

.field private fileId:Ljava/lang/String;

.field private fileName:Ljava/lang/String;

.field private filePath:Ljava/lang/String;

.field private fileSize:I

.field private fileUrl:Ljava/lang/String;

.field private thumbnailContentType:Ljava/lang/String;

.field private thumbnailName:Ljava/lang/String;

.field private thumbnailPath:Ljava/lang/String;

.field private thumbnailSize:I

.field private thumbnailUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 114
    new-instance v0, Lcom/lge/ims/service/im/IMFileInfo$1;

    #@2
    invoke-direct {v0}, Lcom/lge/ims/service/im/IMFileInfo$1;-><init>()V

    #@5
    sput-object v0, Lcom/lge/ims/service/im/IMFileInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 20
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 23
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 24
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Lcom/lge/ims/service/im/IMFileInfo;->fileId:Ljava/lang/String;

    #@9
    .line 25
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    iput-object v0, p0, Lcom/lge/ims/service/im/IMFileInfo;->fileName:Ljava/lang/String;

    #@f
    .line 26
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    iput-object v0, p0, Lcom/lge/ims/service/im/IMFileInfo;->filePath:Ljava/lang/String;

    #@15
    .line 27
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    iput-object v0, p0, Lcom/lge/ims/service/im/IMFileInfo;->fileUrl:Ljava/lang/String;

    #@1b
    .line 28
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1e
    move-result v0

    #@1f
    iput v0, p0, Lcom/lge/ims/service/im/IMFileInfo;->fileSize:I

    #@21
    .line 29
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@24
    move-result-object v0

    #@25
    iput-object v0, p0, Lcom/lge/ims/service/im/IMFileInfo;->contentType:Ljava/lang/String;

    #@27
    .line 30
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2a
    move-result-object v0

    #@2b
    iput-object v0, p0, Lcom/lge/ims/service/im/IMFileInfo;->expiryDateTime:Ljava/lang/String;

    #@2d
    .line 31
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@30
    move-result-object v0

    #@31
    iput-object v0, p0, Lcom/lge/ims/service/im/IMFileInfo;->thumbnailName:Ljava/lang/String;

    #@33
    .line 32
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@36
    move-result-object v0

    #@37
    iput-object v0, p0, Lcom/lge/ims/service/im/IMFileInfo;->thumbnailPath:Ljava/lang/String;

    #@39
    .line 33
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3c
    move-result-object v0

    #@3d
    iput-object v0, p0, Lcom/lge/ims/service/im/IMFileInfo;->thumbnailUrl:Ljava/lang/String;

    #@3f
    .line 34
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@42
    move-result-object v0

    #@43
    iput-object v0, p0, Lcom/lge/ims/service/im/IMFileInfo;->thumbnailContentType:Ljava/lang/String;

    #@45
    .line 35
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@48
    move-result v0

    #@49
    iput v0, p0, Lcom/lge/ims/service/im/IMFileInfo;->thumbnailSize:I

    #@4b
    .line 36
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 111
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getContentType()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 166
    iget-object v0, p0, Lcom/lge/ims/service/im/IMFileInfo;->contentType:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getExpiryDateTime()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 174
    iget-object v0, p0, Lcom/lge/ims/service/im/IMFileInfo;->expiryDateTime:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getFileId()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 126
    iget-object v0, p0, Lcom/lge/ims/service/im/IMFileInfo;->fileId:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getFileName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 134
    iget-object v0, p0, Lcom/lge/ims/service/im/IMFileInfo;->fileName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getFilePath()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 142
    iget-object v0, p0, Lcom/lge/ims/service/im/IMFileInfo;->filePath:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getFileSize()I
    .registers 2

    #@0
    .prologue
    .line 158
    iget v0, p0, Lcom/lge/ims/service/im/IMFileInfo;->fileSize:I

    #@2
    return v0
.end method

.method public getFileUrl()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 150
    iget-object v0, p0, Lcom/lge/ims/service/im/IMFileInfo;->fileUrl:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getThumbnailContentType()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 190
    iget-object v0, p0, Lcom/lge/ims/service/im/IMFileInfo;->thumbnailContentType:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getThumbnailFilePath()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 206
    iget-object v0, p0, Lcom/lge/ims/service/im/IMFileInfo;->thumbnailPath:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getThumbnailName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 214
    iget-object v0, p0, Lcom/lge/ims/service/im/IMFileInfo;->thumbnailName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getThumbnailSize()I
    .registers 2

    #@0
    .prologue
    .line 198
    iget v0, p0, Lcom/lge/ims/service/im/IMFileInfo;->thumbnailSize:I

    #@2
    return v0
.end method

.method public getThumbnailUrl()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 182
    iget-object v0, p0, Lcom/lge/ims/service/im/IMFileInfo;->thumbnailUrl:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public setContentType(Ljava/lang/String;)V
    .registers 2
    .parameter "contentType"

    #@0
    .prologue
    .line 170
    iput-object p1, p0, Lcom/lge/ims/service/im/IMFileInfo;->contentType:Ljava/lang/String;

    #@2
    .line 171
    return-void
.end method

.method public setExpiryDateTime(Ljava/lang/String;)V
    .registers 2
    .parameter "expiryDateTime"

    #@0
    .prologue
    .line 178
    iput-object p1, p0, Lcom/lge/ims/service/im/IMFileInfo;->expiryDateTime:Ljava/lang/String;

    #@2
    .line 179
    return-void
.end method

.method public setFileId(Ljava/lang/String;)V
    .registers 2
    .parameter "fileId"

    #@0
    .prologue
    .line 130
    iput-object p1, p0, Lcom/lge/ims/service/im/IMFileInfo;->fileId:Ljava/lang/String;

    #@2
    .line 131
    return-void
.end method

.method public setFileName(Ljava/lang/String;)V
    .registers 2
    .parameter "fileName"

    #@0
    .prologue
    .line 138
    iput-object p1, p0, Lcom/lge/ims/service/im/IMFileInfo;->fileName:Ljava/lang/String;

    #@2
    .line 139
    return-void
.end method

.method public setFilePath(Ljava/lang/String;)V
    .registers 2
    .parameter "filePath"

    #@0
    .prologue
    .line 146
    iput-object p1, p0, Lcom/lge/ims/service/im/IMFileInfo;->filePath:Ljava/lang/String;

    #@2
    .line 147
    return-void
.end method

.method public setFileSize(I)V
    .registers 2
    .parameter "fileSize"

    #@0
    .prologue
    .line 162
    iput p1, p0, Lcom/lge/ims/service/im/IMFileInfo;->fileSize:I

    #@2
    .line 163
    return-void
.end method

.method public setFileUrl(Ljava/lang/String;)V
    .registers 2
    .parameter "fileUrl"

    #@0
    .prologue
    .line 154
    iput-object p1, p0, Lcom/lge/ims/service/im/IMFileInfo;->fileUrl:Ljava/lang/String;

    #@2
    .line 155
    return-void
.end method

.method public setParamOfExtendToConferenceWithFileLink(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "thumbnailUrl"
    .parameter "thumbnailContentType"
    .parameter "thumbnailSize"
    .parameter "fileUrl"
    .parameter "contentType"
    .parameter "fileSize"
    .parameter "expiryDateTime"
    .parameter "fileId"

    #@0
    .prologue
    .line 81
    iput-object p1, p0, Lcom/lge/ims/service/im/IMFileInfo;->thumbnailUrl:Ljava/lang/String;

    #@2
    .line 82
    iput-object p2, p0, Lcom/lge/ims/service/im/IMFileInfo;->thumbnailContentType:Ljava/lang/String;

    #@4
    .line 83
    iput p3, p0, Lcom/lge/ims/service/im/IMFileInfo;->thumbnailSize:I

    #@6
    .line 84
    iput-object p4, p0, Lcom/lge/ims/service/im/IMFileInfo;->fileUrl:Ljava/lang/String;

    #@8
    .line 85
    iput-object p5, p0, Lcom/lge/ims/service/im/IMFileInfo;->contentType:Ljava/lang/String;

    #@a
    .line 86
    iput p6, p0, Lcom/lge/ims/service/im/IMFileInfo;->fileSize:I

    #@c
    .line 87
    iput-object p7, p0, Lcom/lge/ims/service/im/IMFileInfo;->expiryDateTime:Ljava/lang/String;

    #@e
    .line 88
    iput-object p8, p0, Lcom/lge/ims/service/im/IMFileInfo;->fileId:Ljava/lang/String;

    #@10
    .line 89
    return-void
.end method

.method public setParamOfFileLinkIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .registers 11
    .parameter "fileId"
    .parameter "thumbnailName"
    .parameter "thumbnailContentType"
    .parameter "thumbnailSize"
    .parameter "thumbnailUrl"
    .parameter "fileName"
    .parameter "contentType"
    .parameter "fileSize"
    .parameter "fileUrl"
    .parameter "expiryDateTime"

    #@0
    .prologue
    .line 96
    iput-object p1, p0, Lcom/lge/ims/service/im/IMFileInfo;->fileId:Ljava/lang/String;

    #@2
    .line 97
    iput-object p2, p0, Lcom/lge/ims/service/im/IMFileInfo;->thumbnailName:Ljava/lang/String;

    #@4
    .line 98
    iput-object p3, p0, Lcom/lge/ims/service/im/IMFileInfo;->thumbnailContentType:Ljava/lang/String;

    #@6
    .line 99
    iput p4, p0, Lcom/lge/ims/service/im/IMFileInfo;->thumbnailSize:I

    #@8
    .line 100
    iput-object p5, p0, Lcom/lge/ims/service/im/IMFileInfo;->thumbnailUrl:Ljava/lang/String;

    #@a
    .line 102
    iput-object p6, p0, Lcom/lge/ims/service/im/IMFileInfo;->fileName:Ljava/lang/String;

    #@c
    .line 103
    iput-object p7, p0, Lcom/lge/ims/service/im/IMFileInfo;->contentType:Ljava/lang/String;

    #@e
    .line 104
    iput p8, p0, Lcom/lge/ims/service/im/IMFileInfo;->fileSize:I

    #@10
    .line 105
    iput-object p9, p0, Lcom/lge/ims/service/im/IMFileInfo;->fileUrl:Ljava/lang/String;

    #@12
    .line 107
    iput-object p10, p0, Lcom/lge/ims/service/im/IMFileInfo;->expiryDateTime:Ljava/lang/String;

    #@14
    .line 108
    return-void
.end method

.method public setParamOfSendFile(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V
    .registers 7
    .parameter "thumbnailPath"
    .parameter "thumbnailContentType"
    .parameter "thumbnailSize"
    .parameter "filePath"
    .parameter "contentType"
    .parameter "fileSize"

    #@0
    .prologue
    .line 55
    iput-object p1, p0, Lcom/lge/ims/service/im/IMFileInfo;->thumbnailPath:Ljava/lang/String;

    #@2
    .line 56
    iput-object p2, p0, Lcom/lge/ims/service/im/IMFileInfo;->thumbnailContentType:Ljava/lang/String;

    #@4
    .line 57
    iput p3, p0, Lcom/lge/ims/service/im/IMFileInfo;->thumbnailSize:I

    #@6
    .line 58
    iput-object p4, p0, Lcom/lge/ims/service/im/IMFileInfo;->filePath:Ljava/lang/String;

    #@8
    .line 59
    iput-object p5, p0, Lcom/lge/ims/service/im/IMFileInfo;->contentType:Ljava/lang/String;

    #@a
    .line 60
    iput p6, p0, Lcom/lge/ims/service/im/IMFileInfo;->fileSize:I

    #@c
    .line 61
    return-void
.end method

.method public setParamOfsendFileLink(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "thumbnailUrl"
    .parameter "thumbnailContentType"
    .parameter "thumbnailSize"
    .parameter "fileUrl"
    .parameter "contentType"
    .parameter "fileSize"
    .parameter "expiryDateTime"
    .parameter "fileId"

    #@0
    .prologue
    .line 66
    iput-object p1, p0, Lcom/lge/ims/service/im/IMFileInfo;->thumbnailUrl:Ljava/lang/String;

    #@2
    .line 67
    iput-object p2, p0, Lcom/lge/ims/service/im/IMFileInfo;->thumbnailContentType:Ljava/lang/String;

    #@4
    .line 68
    iput p3, p0, Lcom/lge/ims/service/im/IMFileInfo;->thumbnailSize:I

    #@6
    .line 69
    iput-object p4, p0, Lcom/lge/ims/service/im/IMFileInfo;->fileUrl:Ljava/lang/String;

    #@8
    .line 70
    iput-object p5, p0, Lcom/lge/ims/service/im/IMFileInfo;->contentType:Ljava/lang/String;

    #@a
    .line 71
    iput p6, p0, Lcom/lge/ims/service/im/IMFileInfo;->fileSize:I

    #@c
    .line 72
    iput-object p7, p0, Lcom/lge/ims/service/im/IMFileInfo;->expiryDateTime:Ljava/lang/String;

    #@e
    .line 73
    iput-object p8, p0, Lcom/lge/ims/service/im/IMFileInfo;->fileId:Ljava/lang/String;

    #@10
    .line 74
    return-void
.end method

.method public setThumbnailContentType(Ljava/lang/String;)V
    .registers 2
    .parameter "thumbnailContentType"

    #@0
    .prologue
    .line 194
    iput-object p1, p0, Lcom/lge/ims/service/im/IMFileInfo;->thumbnailContentType:Ljava/lang/String;

    #@2
    .line 195
    return-void
.end method

.method public setThumbnailFilePath(Ljava/lang/String;)V
    .registers 2
    .parameter "thumbnailFilePath"

    #@0
    .prologue
    .line 210
    iput-object p1, p0, Lcom/lge/ims/service/im/IMFileInfo;->thumbnailPath:Ljava/lang/String;

    #@2
    .line 211
    return-void
.end method

.method public setThumbnailName(Ljava/lang/String;)V
    .registers 2
    .parameter "thumbnailName"

    #@0
    .prologue
    .line 218
    iput-object p1, p0, Lcom/lge/ims/service/im/IMFileInfo;->thumbnailName:Ljava/lang/String;

    #@2
    .line 219
    return-void
.end method

.method public setThumbnailSize(I)V
    .registers 2
    .parameter "thumbnailSize"

    #@0
    .prologue
    .line 202
    iput p1, p0, Lcom/lge/ims/service/im/IMFileInfo;->thumbnailSize:I

    #@2
    .line 203
    return-void
.end method

.method public setThumbnailUrl(Ljava/lang/String;)V
    .registers 2
    .parameter "thumbnailUrl"

    #@0
    .prologue
    .line 186
    iput-object p1, p0, Lcom/lge/ims/service/im/IMFileInfo;->thumbnailUrl:Ljava/lang/String;

    #@2
    .line 187
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "out"
    .parameter "flags"

    #@0
    .prologue
    .line 39
    iget-object v0, p0, Lcom/lge/ims/service/im/IMFileInfo;->fileId:Ljava/lang/String;

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5
    .line 40
    iget-object v0, p0, Lcom/lge/ims/service/im/IMFileInfo;->fileName:Ljava/lang/String;

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@a
    .line 41
    iget-object v0, p0, Lcom/lge/ims/service/im/IMFileInfo;->filePath:Ljava/lang/String;

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    .line 42
    iget-object v0, p0, Lcom/lge/ims/service/im/IMFileInfo;->fileUrl:Ljava/lang/String;

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@14
    .line 43
    iget v0, p0, Lcom/lge/ims/service/im/IMFileInfo;->fileSize:I

    #@16
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 44
    iget-object v0, p0, Lcom/lge/ims/service/im/IMFileInfo;->contentType:Ljava/lang/String;

    #@1b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1e
    .line 45
    iget-object v0, p0, Lcom/lge/ims/service/im/IMFileInfo;->expiryDateTime:Ljava/lang/String;

    #@20
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@23
    .line 46
    iget-object v0, p0, Lcom/lge/ims/service/im/IMFileInfo;->thumbnailName:Ljava/lang/String;

    #@25
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@28
    .line 47
    iget-object v0, p0, Lcom/lge/ims/service/im/IMFileInfo;->thumbnailPath:Ljava/lang/String;

    #@2a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@2d
    .line 48
    iget-object v0, p0, Lcom/lge/ims/service/im/IMFileInfo;->thumbnailUrl:Ljava/lang/String;

    #@2f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@32
    .line 49
    iget-object v0, p0, Lcom/lge/ims/service/im/IMFileInfo;->thumbnailContentType:Ljava/lang/String;

    #@34
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@37
    .line 50
    iget v0, p0, Lcom/lge/ims/service/im/IMFileInfo;->thumbnailSize:I

    #@39
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@3c
    .line 51
    return-void
.end method
