.class Lcom/lge/ims/service/im/IFileTransfer$Stub$Proxy;
.super Ljava/lang/Object;
.source "IFileTransfer.java"

# interfaces
.implements Lcom/lge/ims/service/im/IFileTransfer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/im/IFileTransfer$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 117
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 118
    iput-object p1, p0, Lcom/lge/ims/service/im/IFileTransfer$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 119
    return-void
.end method


# virtual methods
.method public accept(Ljava/lang/String;)V
    .registers 7
    .parameter "folderPath"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 152
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 153
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 155
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.im.IFileTransfer"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 156
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 157
    iget-object v2, p0, Lcom/lge/ims/service/im/IFileTransfer$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v3, 0x2

    #@13
    const/4 v4, 0x0

    #@14
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 158
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_21

    #@1a
    .line 161
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1d
    .line 162
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@20
    .line 164
    return-void

    #@21
    .line 161
    :catchall_21
    move-exception v2

    #@22
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 162
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    throw v2
.end method

.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 122
    iget-object v0, p0, Lcom/lge/ims/service/im/IFileTransfer$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public cancel()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 181
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 182
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 184
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.im.IFileTransfer"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 185
    iget-object v2, p0, Lcom/lge/ims/service/im/IFileTransfer$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v3, 0x4

    #@10
    const/4 v4, 0x0

    #@11
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 186
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_17
    .catchall {:try_start_8 .. :try_end_17} :catchall_1e

    #@17
    .line 189
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 190
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1d
    .line 192
    return-void

    #@1e
    .line 189
    :catchall_1e
    move-exception v2

    #@1f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 190
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    throw v2
.end method

.method public download(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .registers 11
    .parameter "fileName"
    .parameter "url"
    .parameter "contentType"
    .parameter "fileSize"
    .parameter "folderPath"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 195
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 196
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 198
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.im.IFileTransfer"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 199
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 200
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@13
    .line 201
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@16
    .line 202
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 203
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1c
    .line 204
    iget-object v2, p0, Lcom/lge/ims/service/im/IFileTransfer$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1e
    const/4 v3, 0x5

    #@1f
    const/4 v4, 0x0

    #@20
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@23
    .line 205
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_26
    .catchall {:try_start_8 .. :try_end_26} :catchall_2d

    #@26
    .line 208
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 209
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 211
    return-void

    #@2d
    .line 208
    :catchall_2d
    move-exception v2

    #@2e
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 209
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@34
    throw v2
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 126
    const-string v0, "com.lge.ims.service.im.IFileTransfer"

    #@2
    return-object v0
.end method

.method public reject()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 167
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 168
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 170
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.im.IFileTransfer"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 171
    iget-object v2, p0, Lcom/lge/ims/service/im/IFileTransfer$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v3, 0x3

    #@10
    const/4 v4, 0x0

    #@11
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 172
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_17
    .catchall {:try_start_8 .. :try_end_17} :catchall_1e

    #@17
    .line 175
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 176
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1d
    .line 178
    return-void

    #@1e
    .line 175
    :catchall_1e
    move-exception v2

    #@1f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 176
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    throw v2
.end method

.method public sendFile(Lcom/lge/ims/service/im/IMFileInfo;Ljava/lang/String;)V
    .registers 8
    .parameter "fileInfo"
    .parameter "contributionId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 130
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 131
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 133
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.im.IFileTransfer"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 134
    if-eqz p1, :cond_2b

    #@f
    .line 135
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 136
    const/4 v2, 0x0

    #@14
    invoke-virtual {p1, v0, v2}, Lcom/lge/ims/service/im/IMFileInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 141
    :goto_17
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1a
    .line 142
    iget-object v2, p0, Lcom/lge/ims/service/im/IFileTransfer$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1c
    const/4 v3, 0x1

    #@1d
    const/4 v4, 0x0

    #@1e
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@21
    .line 143
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_24
    .catchall {:try_start_8 .. :try_end_24} :catchall_30

    #@24
    .line 146
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 147
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 149
    return-void

    #@2b
    .line 139
    :cond_2b
    const/4 v2, 0x0

    #@2c
    :try_start_2c
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2f
    .catchall {:try_start_2c .. :try_end_2f} :catchall_30

    #@2f
    goto :goto_17

    #@30
    .line 146
    :catchall_30
    move-exception v2

    #@31
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@34
    .line 147
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@37
    throw v2
.end method

.method public setListener(Lcom/lge/ims/service/im/IFileTransferListener;)V
    .registers 7
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 214
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 215
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 217
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.im.IFileTransfer"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 218
    if-eqz p1, :cond_27

    #@f
    invoke-interface {p1}, Lcom/lge/ims/service/im/IFileTransferListener;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 219
    iget-object v2, p0, Lcom/lge/ims/service/im/IFileTransfer$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/4 v3, 0x6

    #@19
    const/4 v4, 0x0

    #@1a
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1d
    .line 220
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_20
    .catchall {:try_start_8 .. :try_end_20} :catchall_29

    #@20
    .line 223
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 224
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 226
    return-void

    #@27
    .line 218
    :cond_27
    const/4 v2, 0x0

    #@28
    goto :goto_13

    #@29
    .line 223
    :catchall_29
    move-exception v2

    #@2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 224
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    throw v2
.end method
