.class Lcom/lge/ims/service/uc/IUCSession$Stub$Proxy;
.super Ljava/lang/Object;
.source "IUCSession.java"

# interfaces
.implements Lcom/lge/ims/service/uc/IUCSession;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/uc/IUCSession$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 290
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 291
    iput-object p1, p0, Lcom/lge/ims/service/uc/IUCSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 292
    return-void
.end method


# virtual methods
.method public accept(I)V
    .registers 7
    .parameter "sessionType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 369
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 370
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 372
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSession"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 373
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 374
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v3, 0x3

    #@13
    const/4 v4, 0x0

    #@14
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 375
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_21

    #@1a
    .line 378
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1d
    .line 379
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@20
    .line 381
    return-void

    #@21
    .line 378
    :catchall_21
    move-exception v2

    #@22
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 379
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    throw v2
.end method

.method public acceptUpdate(ILcom/lge/ims/service/uc/MediaInfo;)V
    .registers 8
    .parameter "sessionType"
    .parameter "mediaInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 564
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 565
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 567
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSession"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 568
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 569
    if-eqz p2, :cond_2c

    #@12
    .line 570
    const/4 v2, 0x1

    #@13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 571
    const/4 v2, 0x0

    #@17
    invoke-virtual {p2, v0, v2}, Lcom/lge/ims/service/uc/MediaInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@1a
    .line 576
    :goto_1a
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1c
    const/16 v3, 0xe

    #@1e
    const/4 v4, 0x0

    #@1f
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@22
    .line 577
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_25
    .catchall {:try_start_8 .. :try_end_25} :catchall_31

    #@25
    .line 580
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 581
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 583
    return-void

    #@2c
    .line 574
    :cond_2c
    const/4 v2, 0x0

    #@2d
    :try_start_2d
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_30
    .catchall {:try_start_2d .. :try_end_30} :catchall_31

    #@30
    goto :goto_1a

    #@31
    .line 580
    :catchall_31
    move-exception v2

    #@32
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 581
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@38
    throw v2
.end method

.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 295
    iget-object v0, p0, Lcom/lge/ims/service/uc/IUCSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public dropConf(Lcom/lge/ims/service/uc/ConfInfo;)V
    .registers 7
    .parameter "confInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 521
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 522
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 524
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSession"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 525
    if-eqz p1, :cond_29

    #@f
    .line 526
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 527
    const/4 v2, 0x0

    #@14
    invoke-virtual {p1, v0, v2}, Lcom/lge/ims/service/uc/ConfInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 532
    :goto_17
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/16 v3, 0xc

    #@1b
    const/4 v4, 0x0

    #@1c
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1f
    .line 533
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_22
    .catchall {:try_start_8 .. :try_end_22} :catchall_2e

    #@22
    .line 536
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 537
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 539
    return-void

    #@29
    .line 530
    :cond_29
    const/4 v2, 0x0

    #@2a
    :try_start_2a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2d
    .catchall {:try_start_2a .. :try_end_2d} :catchall_2e

    #@2d
    goto :goto_17

    #@2e
    .line 536
    :catchall_2e
    move-exception v2

    #@2f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 537
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@35
    throw v2
.end method

.method public expandToConf(Lcom/lge/ims/service/uc/ConfInfo;)V
    .registers 7
    .parameter "confInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 465
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 466
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 468
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSession"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 469
    if-eqz p1, :cond_29

    #@f
    .line 470
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 471
    const/4 v2, 0x0

    #@14
    invoke-virtual {p1, v0, v2}, Lcom/lge/ims/service/uc/ConfInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 476
    :goto_17
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/16 v3, 0x9

    #@1b
    const/4 v4, 0x0

    #@1c
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1f
    .line 477
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_22
    .catchall {:try_start_8 .. :try_end_22} :catchall_2e

    #@22
    .line 480
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 481
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 483
    return-void

    #@29
    .line 474
    :cond_29
    const/4 v2, 0x0

    #@2a
    :try_start_2a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2d
    .catchall {:try_start_2a .. :try_end_2d} :catchall_2e

    #@2d
    goto :goto_17

    #@2e
    .line 480
    :catchall_2e
    move-exception v2

    #@2f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 481
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@35
    throw v2
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 299
    const-string v0, "com.lge.ims.service.uc.IUCSession"

    #@2
    return-object v0
.end method

.method public getMediaSession(Lcom/lge/ims/service/uc/IUCMediaSessionListener;)Lcom/lge/ims/service/uc/IUCMediaSession;
    .registers 8
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 635
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 636
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 639
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.ims.service.uc.IUCSession"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 640
    if-eqz p1, :cond_30

    #@f
    invoke-interface {p1}, Lcom/lge/ims/service/uc/IUCMediaSessionListener;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v3

    #@13
    :goto_13
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 641
    iget-object v3, p0, Lcom/lge/ims/service/uc/IUCSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/16 v4, 0x12

    #@1a
    const/4 v5, 0x0

    #@1b
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 642
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@21
    .line 643
    invoke-virtual {v1}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@24
    move-result-object v3

    #@25
    invoke-static {v3}, Lcom/lge/ims/service/uc/IUCMediaSession$Stub;->asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/uc/IUCMediaSession;
    :try_end_28
    .catchall {:try_start_8 .. :try_end_28} :catchall_32

    #@28
    move-result-object v2

    #@29
    .line 646
    .local v2, _result:Lcom/lge/ims/service/uc/IUCMediaSession;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 647
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 649
    return-object v2

    #@30
    .line 640
    .end local v2           #_result:Lcom/lge/ims/service/uc/IUCMediaSession;
    :cond_30
    const/4 v3, 0x0

    #@31
    goto :goto_13

    #@32
    .line 646
    :catchall_32
    move-exception v3

    #@33
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@36
    .line 647
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@39
    throw v3
.end method

.method public getProperty(I)I
    .registers 8
    .parameter "item"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 616
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 617
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 620
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.ims.service.uc.IUCSession"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 621
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 622
    iget-object v3, p0, Lcom/lge/ims/service/uc/IUCSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v4, 0x11

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 623
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 624
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1e
    .catchall {:try_start_8 .. :try_end_1e} :catchall_26

    #@1e
    move-result v2

    #@1f
    .line 627
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 628
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 630
    return v2

    #@26
    .line 627
    .end local v2           #_result:I
    :catchall_26
    move-exception v3

    #@27
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 628
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    throw v3
.end method

.method public hold(Lcom/lge/ims/service/uc/MediaInfo;)V
    .registers 7
    .parameter "mediaInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 399
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 400
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 402
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSession"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 403
    if-eqz p1, :cond_28

    #@f
    .line 404
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 405
    const/4 v2, 0x0

    #@14
    invoke-virtual {p1, v0, v2}, Lcom/lge/ims/service/uc/MediaInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 410
    :goto_17
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/4 v3, 0x5

    #@1a
    const/4 v4, 0x0

    #@1b
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 411
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_21
    .catchall {:try_start_8 .. :try_end_21} :catchall_2d

    #@21
    .line 414
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 415
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 417
    return-void

    #@28
    .line 408
    :cond_28
    const/4 v2, 0x0

    #@29
    :try_start_29
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2c
    .catchall {:try_start_29 .. :try_end_2c} :catchall_2d

    #@2c
    goto :goto_17

    #@2d
    .line 414
    :catchall_2d
    move-exception v2

    #@2e
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 415
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@34
    throw v2
.end method

.method public joinConf(Lcom/lge/ims/service/uc/ConfInfo;)V
    .registers 7
    .parameter "confInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 500
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 501
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 503
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSession"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 504
    if-eqz p1, :cond_29

    #@f
    .line 505
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 506
    const/4 v2, 0x0

    #@14
    invoke-virtual {p1, v0, v2}, Lcom/lge/ims/service/uc/ConfInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 511
    :goto_17
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/16 v3, 0xb

    #@1b
    const/4 v4, 0x0

    #@1c
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1f
    .line 512
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_22
    .catchall {:try_start_8 .. :try_end_22} :catchall_2e

    #@22
    .line 515
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 516
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 518
    return-void

    #@29
    .line 509
    :cond_29
    const/4 v2, 0x0

    #@2a
    :try_start_2a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2d
    .catchall {:try_start_2a .. :try_end_2d} :catchall_2e

    #@2d
    goto :goto_17

    #@2e
    .line 515
    :catchall_2e
    move-exception v2

    #@2f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 516
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@35
    throw v2
.end method

.method public merge()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 486
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 487
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 489
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSession"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 490
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v3, 0xa

    #@11
    const/4 v4, 0x0

    #@12
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 491
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_18
    .catchall {:try_start_8 .. :try_end_18} :catchall_1f

    #@18
    .line 494
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1b
    .line 495
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 497
    return-void

    #@1f
    .line 494
    :catchall_1f
    move-exception v2

    #@20
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 495
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    throw v2
.end method

.method public reject(I)V
    .registers 7
    .parameter "reason"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 384
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 385
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 387
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSession"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 388
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 389
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v3, 0x4

    #@13
    const/4 v4, 0x0

    #@14
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 390
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_21

    #@1a
    .line 393
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1d
    .line 394
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@20
    .line 396
    return-void

    #@21
    .line 393
    :catchall_21
    move-exception v2

    #@22
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 394
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    throw v2
.end method

.method public rejectUpdate(I)V
    .registers 7
    .parameter "reason"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 586
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 587
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 589
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSession"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 590
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 591
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v3, 0xf

    #@14
    const/4 v4, 0x0

    #@15
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 592
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_22

    #@1b
    .line 595
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 596
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 598
    return-void

    #@22
    .line 595
    :catchall_22
    move-exception v2

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 596
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v2
.end method

.method public resume()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 420
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 421
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 423
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSession"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 424
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v3, 0x6

    #@10
    const/4 v4, 0x0

    #@11
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 425
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_17
    .catchall {:try_start_8 .. :try_end_17} :catchall_1e

    #@17
    .line 428
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 429
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1d
    .line 431
    return-void

    #@1e
    .line 428
    :catchall_1e
    move-exception v2

    #@1f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 429
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    throw v2
.end method

.method public sendDTMF(Ljava/lang/String;I)V
    .registers 8
    .parameter "signal"
    .parameter "duration"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 434
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 435
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 437
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSession"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 438
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 439
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 440
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/4 v3, 0x7

    #@16
    const/4 v4, 0x0

    #@17
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1a
    .line 441
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1d
    .catchall {:try_start_8 .. :try_end_1d} :catchall_24

    #@1d
    .line 444
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@20
    .line 445
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 447
    return-void

    #@24
    .line 444
    :catchall_24
    move-exception v2

    #@25
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 445
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    throw v2
.end method

.method public setListener(Lcom/lge/ims/service/uc/IUCSessionListener;)V
    .registers 7
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 601
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 602
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 604
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSession"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 605
    if-eqz p1, :cond_28

    #@f
    invoke-interface {p1}, Lcom/lge/ims/service/uc/IUCSessionListener;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 606
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/16 v3, 0x10

    #@1a
    const/4 v4, 0x0

    #@1b
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 607
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_21
    .catchall {:try_start_8 .. :try_end_21} :catchall_2a

    #@21
    .line 610
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 611
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 613
    return-void

    #@28
    .line 605
    :cond_28
    const/4 v2, 0x0

    #@29
    goto :goto_13

    #@2a
    .line 610
    :catchall_2a
    move-exception v2

    #@2b
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 611
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    throw v2
.end method

.method public start(ILjava/lang/String;Lcom/lge/ims/service/uc/MediaInfo;Lcom/lge/ims/service/uc/SuppInfo;)V
    .registers 10
    .parameter "sessionType"
    .parameter "target"
    .parameter "mediaInfo"
    .parameter "suppInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 303
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 304
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 306
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSession"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 307
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 308
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@13
    .line 309
    if-eqz p3, :cond_38

    #@15
    .line 310
    const/4 v2, 0x1

    #@16
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 311
    const/4 v2, 0x0

    #@1a
    invoke-virtual {p3, v0, v2}, Lcom/lge/ims/service/uc/MediaInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@1d
    .line 316
    :goto_1d
    if-eqz p4, :cond_45

    #@1f
    .line 317
    const/4 v2, 0x1

    #@20
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@23
    .line 318
    const/4 v2, 0x0

    #@24
    invoke-virtual {p4, v0, v2}, Lcom/lge/ims/service/uc/SuppInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@27
    .line 323
    :goto_27
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@29
    const/4 v3, 0x1

    #@2a
    const/4 v4, 0x0

    #@2b
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@2e
    .line 324
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_31
    .catchall {:try_start_8 .. :try_end_31} :catchall_3d

    #@31
    .line 327
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@34
    .line 328
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@37
    .line 330
    return-void

    #@38
    .line 314
    :cond_38
    const/4 v2, 0x0

    #@39
    :try_start_39
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_3c
    .catchall {:try_start_39 .. :try_end_3c} :catchall_3d

    #@3c
    goto :goto_1d

    #@3d
    .line 327
    :catchall_3d
    move-exception v2

    #@3e
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@41
    .line 328
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@44
    throw v2

    #@45
    .line 321
    :cond_45
    const/4 v2, 0x0

    #@46
    :try_start_46
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_49
    .catchall {:try_start_46 .. :try_end_49} :catchall_3d

    #@49
    goto :goto_27
.end method

.method public startConf(ILcom/lge/ims/service/uc/ConfInfo;Lcom/lge/ims/service/uc/MediaInfo;Lcom/lge/ims/service/uc/SuppInfo;)V
    .registers 10
    .parameter "sessionType"
    .parameter "confInfo"
    .parameter "mediaInfo"
    .parameter "suppInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 333
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 334
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 336
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSession"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 337
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 338
    if-eqz p2, :cond_3f

    #@12
    .line 339
    const/4 v2, 0x1

    #@13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 340
    const/4 v2, 0x0

    #@17
    invoke-virtual {p2, v0, v2}, Lcom/lge/ims/service/uc/ConfInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@1a
    .line 345
    :goto_1a
    if-eqz p3, :cond_4c

    #@1c
    .line 346
    const/4 v2, 0x1

    #@1d
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@20
    .line 347
    const/4 v2, 0x0

    #@21
    invoke-virtual {p3, v0, v2}, Lcom/lge/ims/service/uc/MediaInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@24
    .line 352
    :goto_24
    if-eqz p4, :cond_51

    #@26
    .line 353
    const/4 v2, 0x1

    #@27
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@2a
    .line 354
    const/4 v2, 0x0

    #@2b
    invoke-virtual {p4, v0, v2}, Lcom/lge/ims/service/uc/SuppInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@2e
    .line 359
    :goto_2e
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@30
    const/4 v3, 0x2

    #@31
    const/4 v4, 0x0

    #@32
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@35
    .line 360
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_38
    .catchall {:try_start_8 .. :try_end_38} :catchall_44

    #@38
    .line 363
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3b
    .line 364
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3e
    .line 366
    return-void

    #@3f
    .line 343
    :cond_3f
    const/4 v2, 0x0

    #@40
    :try_start_40
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_43
    .catchall {:try_start_40 .. :try_end_43} :catchall_44

    #@43
    goto :goto_1a

    #@44
    .line 363
    :catchall_44
    move-exception v2

    #@45
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@48
    .line 364
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@4b
    throw v2

    #@4c
    .line 350
    :cond_4c
    const/4 v2, 0x0

    #@4d
    :try_start_4d
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@50
    goto :goto_24

    #@51
    .line 357
    :cond_51
    const/4 v2, 0x0

    #@52
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_55
    .catchall {:try_start_4d .. :try_end_55} :catchall_44

    #@55
    goto :goto_2e
.end method

.method public terminate(I)V
    .registers 7
    .parameter "reason"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 450
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 451
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 453
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSession"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 454
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 455
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v3, 0x8

    #@14
    const/4 v4, 0x0

    #@15
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 456
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_22

    #@1b
    .line 459
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 460
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 462
    return-void

    #@22
    .line 459
    :catchall_22
    move-exception v2

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 460
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v2
.end method

.method public update(ILcom/lge/ims/service/uc/MediaInfo;)V
    .registers 8
    .parameter "sessionType"
    .parameter "mediaInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 542
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 543
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 545
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSession"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 546
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 547
    if-eqz p2, :cond_2c

    #@12
    .line 548
    const/4 v2, 0x1

    #@13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 549
    const/4 v2, 0x0

    #@17
    invoke-virtual {p2, v0, v2}, Lcom/lge/ims/service/uc/MediaInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@1a
    .line 554
    :goto_1a
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1c
    const/16 v3, 0xd

    #@1e
    const/4 v4, 0x0

    #@1f
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@22
    .line 555
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_25
    .catchall {:try_start_8 .. :try_end_25} :catchall_31

    #@25
    .line 558
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 559
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 561
    return-void

    #@2c
    .line 552
    :cond_2c
    const/4 v2, 0x0

    #@2d
    :try_start_2d
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_30
    .catchall {:try_start_2d .. :try_end_30} :catchall_31

    #@30
    goto :goto_1a

    #@31
    .line 558
    :catchall_31
    move-exception v2

    #@32
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 559
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@38
    throw v2
.end method
