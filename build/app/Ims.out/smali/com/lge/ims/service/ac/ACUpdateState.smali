.class public Lcom/lge/ims/service/ac/ACUpdateState;
.super Ljava/lang/Object;
.source "ACUpdateState.java"


# static fields
.field public static final INVALID_UPDATE_STATE:I = -0x1


# instance fields
.field private mProvisionedServices:I

.field private mUpdatedImsTables:I


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 18
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 12
    iput v0, p0, Lcom/lge/ims/service/ac/ACUpdateState;->mProvisionedServices:I

    #@6
    .line 14
    iput v0, p0, Lcom/lge/ims/service/ac/ACUpdateState;->mUpdatedImsTables:I

    #@8
    .line 19
    return-void
.end method

.method public constructor <init>(II)V
    .registers 4
    .parameter "services"
    .parameter "tables"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 21
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 12
    iput v0, p0, Lcom/lge/ims/service/ac/ACUpdateState;->mProvisionedServices:I

    #@6
    .line 14
    iput v0, p0, Lcom/lge/ims/service/ac/ACUpdateState;->mUpdatedImsTables:I

    #@8
    .line 22
    iput p1, p0, Lcom/lge/ims/service/ac/ACUpdateState;->mProvisionedServices:I

    #@a
    .line 23
    iput p2, p0, Lcom/lge/ims/service/ac/ACUpdateState;->mUpdatedImsTables:I

    #@c
    .line 24
    return-void
.end method


# virtual methods
.method public getProvisionedServices()I
    .registers 2

    #@0
    .prologue
    .line 27
    iget v0, p0, Lcom/lge/ims/service/ac/ACUpdateState;->mProvisionedServices:I

    #@2
    return v0
.end method

.method public getUpdatedImsTables()I
    .registers 2

    #@0
    .prologue
    .line 31
    iget v0, p0, Lcom/lge/ims/service/ac/ACUpdateState;->mUpdatedImsTables:I

    #@2
    return v0
.end method

.method public setProvisionedServices(I)V
    .registers 2
    .parameter "services"

    #@0
    .prologue
    .line 35
    iput p1, p0, Lcom/lge/ims/service/ac/ACUpdateState;->mProvisionedServices:I

    #@2
    .line 36
    return-void
.end method

.method public setUpdatedImsTables(I)V
    .registers 2
    .parameter "tables"

    #@0
    .prologue
    .line 39
    iput p1, p0, Lcom/lge/ims/service/ac/ACUpdateState;->mUpdatedImsTables:I

    #@2
    .line 40
    return-void
.end method
