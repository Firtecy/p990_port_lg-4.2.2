.class public Lcom/lge/ims/service/cd/CapabilityQuery;
.super Ljava/lang/Object;
.source "CapabilityQuery.java"

# interfaces
.implements Lcom/lge/ims/JNICoreListener;


# static fields
.field private static final RC_NON_RCS_USER:I = 0x1

.field private static final RC_USER_NOT_REGISTERED:I = 0x2

.field private static final TAG:Ljava/lang/String; = "CapQuery"


# instance fields
.field private final TEST_MODE:Z

.field private mCapNotifier:Lcom/lge/ims/service/cd/CapabilityNotifier;

.field private mCapStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;

.field private mListener:Lcom/lge/ims/service/cd/CapabilityQueryListener;

.field private mNativeObject:I

.field private mRegistrant:Landroid/os/Registrant;


# direct methods
.method public constructor <init>(ILcom/lge/ims/service/cd/CapabilityNotifier;Lcom/lge/ims/service/cd/CapabilityStateTracker;)V
    .registers 6
    .parameter "nativeObject"
    .parameter "notifier"
    .parameter "stateTracker"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x0

    #@2
    .line 35
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 26
    iput-boolean v1, p0, Lcom/lge/ims/service/cd/CapabilityQuery;->TEST_MODE:Z

    #@7
    .line 27
    iput v1, p0, Lcom/lge/ims/service/cd/CapabilityQuery;->mNativeObject:I

    #@9
    .line 28
    iput-object v0, p0, Lcom/lge/ims/service/cd/CapabilityQuery;->mRegistrant:Landroid/os/Registrant;

    #@b
    .line 29
    iput-object v0, p0, Lcom/lge/ims/service/cd/CapabilityQuery;->mCapNotifier:Lcom/lge/ims/service/cd/CapabilityNotifier;

    #@d
    .line 30
    iput-object v0, p0, Lcom/lge/ims/service/cd/CapabilityQuery;->mCapStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@f
    .line 31
    iput-object v0, p0, Lcom/lge/ims/service/cd/CapabilityQuery;->mListener:Lcom/lge/ims/service/cd/CapabilityQueryListener;

    #@11
    .line 36
    iput p1, p0, Lcom/lge/ims/service/cd/CapabilityQuery;->mNativeObject:I

    #@13
    .line 37
    iput-object p2, p0, Lcom/lge/ims/service/cd/CapabilityQuery;->mCapNotifier:Lcom/lge/ims/service/cd/CapabilityNotifier;

    #@15
    .line 38
    iput-object p3, p0, Lcom/lge/ims/service/cd/CapabilityQuery;->mCapStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@17
    .line 44
    iget v0, p0, Lcom/lge/ims/service/cd/CapabilityQuery;->mNativeObject:I

    #@19
    invoke-static {v0, p0}, Lcom/lge/ims/JNICore;->setListener(ILcom/lge/ims/JNICoreListener;)I

    #@1c
    .line 46
    return-void
.end method


# virtual methods
.method public close()V
    .registers 2

    #@0
    .prologue
    .line 49
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityQuery;->mRegistrant:Landroid/os/Registrant;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 55
    :goto_4
    return-void

    #@5
    .line 53
    :cond_5
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityQuery;->mRegistrant:Landroid/os/Registrant;

    #@7
    invoke-virtual {v0, p0}, Landroid/os/Registrant;->notifyResult(Ljava/lang/Object;)V

    #@a
    .line 54
    const/4 v0, 0x0

    #@b
    iput-object v0, p0, Lcom/lge/ims/service/cd/CapabilityQuery;->mRegistrant:Landroid/os/Registrant;

    #@d
    goto :goto_4
.end method

.method public destroy()V
    .registers 2

    #@0
    .prologue
    .line 62
    iget v0, p0, Lcom/lge/ims/service/cd/CapabilityQuery;->mNativeObject:I

    #@2
    if-nez v0, :cond_5

    #@4
    .line 74
    :goto_4
    return-void

    #@5
    .line 69
    :cond_5
    iget v0, p0, Lcom/lge/ims/service/cd/CapabilityQuery;->mNativeObject:I

    #@7
    invoke-static {v0, p0}, Lcom/lge/ims/JNICore;->removeListener(ILcom/lge/ims/JNICoreListener;)I

    #@a
    .line 70
    iget v0, p0, Lcom/lge/ims/service/cd/CapabilityQuery;->mNativeObject:I

    #@c
    invoke-static {v0}, Lcom/lge/ims/JNICore;->releaseInterface(I)I

    #@f
    .line 73
    const/4 v0, 0x0

    #@10
    iput v0, p0, Lcom/lge/ims/service/cd/CapabilityQuery;->mNativeObject:I

    #@12
    goto :goto_4
.end method

.method public onMessage(Landroid/os/Parcel;)V
    .registers 9
    .parameter "parcel"

    #@0
    .prologue
    .line 158
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 160
    .local v0, event:I
    packed-switch v0, :pswitch_data_da

    #@7
    .line 212
    const-string v4, "CapQuery"

    #@9
    new-instance v5, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v6, "Not handled event ("

    #@10
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v5

    #@14
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v5

    #@18
    const-string v6, ")"

    #@1a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v5

    #@1e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v5

    #@22
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@25
    .line 215
    :goto_25
    return-void

    #@26
    .line 163
    :pswitch_26
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@29
    move-result v2

    #@2a
    .line 165
    .local v2, resultCode:I
    new-instance v3, Lcom/lge/ims/service/cd/ServiceCapabilities;

    #@2c
    invoke-direct {v3, p1}, Lcom/lge/ims/service/cd/ServiceCapabilities;-><init>(Landroid/os/Parcel;)V

    #@2f
    .line 167
    .local v3, sc:Lcom/lge/ims/service/cd/ServiceCapabilities;
    if-eqz v3, :cond_c4

    #@31
    .line 169
    if-nez v2, :cond_74

    #@33
    iget-object v4, p0, Lcom/lge/ims/service/cd/CapabilityQuery;->mCapStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@35
    if-eqz v4, :cond_74

    #@37
    iget-object v4, p0, Lcom/lge/ims/service/cd/CapabilityQuery;->mCapStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@39
    invoke-virtual {v4}, Lcom/lge/ims/service/cd/CapabilityStateTracker;->isExternalStorageAvailable()Z

    #@3c
    move-result v4

    #@3d
    if-nez v4, :cond_74

    #@3f
    .line 172
    const-string v4, "CapQuery"

    #@41
    new-instance v5, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v6, "Capabilities (disable features) :: IS = "

    #@48
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v5

    #@4c
    const/4 v6, 0x4

    #@4d
    invoke-virtual {v3, v6}, Lcom/lge/ims/service/cd/ServiceCapabilities;->isServiceSupported(I)Z

    #@50
    move-result v6

    #@51
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@54
    move-result-object v5

    #@55
    const-string v6, ", FT = "

    #@57
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v5

    #@5b
    const/4 v6, 0x1

    #@5c
    invoke-virtual {v3, v6}, Lcom/lge/ims/service/cd/ServiceCapabilities;->isServiceSupported(I)Z

    #@5f
    move-result v6

    #@60
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@63
    move-result-object v5

    #@64
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@67
    move-result-object v5

    #@68
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@6b
    .line 176
    const/4 v4, 0x5

    #@6c
    invoke-virtual {v3, v4}, Lcom/lge/ims/service/cd/ServiceCapabilities;->clearServiceCapabilities(I)V

    #@6f
    .line 177
    const/high16 v4, 0xff

    #@71
    invoke-virtual {v3, v4}, Lcom/lge/ims/service/cd/ServiceCapabilities;->clearMediaCapabilities(I)V

    #@74
    .line 180
    :cond_74
    move v1, v2

    #@75
    .line 182
    .local v1, notifiedResultCode:I
    iget-object v4, p0, Lcom/lge/ims/service/cd/CapabilityQuery;->mListener:Lcom/lge/ims/service/cd/CapabilityQueryListener;

    #@77
    if-eqz v4, :cond_87

    #@79
    .line 183
    const/16 v4, 0x194

    #@7b
    if-ne v2, v4, :cond_c9

    #@7d
    .line 185
    const/4 v1, 0x1

    #@7e
    .line 194
    :cond_7e
    :goto_7e
    iget-object v4, p0, Lcom/lge/ims/service/cd/CapabilityQuery;->mListener:Lcom/lge/ims/service/cd/CapabilityQueryListener;

    #@80
    invoke-virtual {v3}, Lcom/lge/ims/service/cd/ServiceCapabilities;->toCapabilities()Lcom/lge/ims/service/cd/Capabilities;

    #@83
    move-result-object v5

    #@84
    invoke-interface {v4, p0, v5, v1}, Lcom/lge/ims/service/cd/CapabilityQueryListener;->capabilityQueryDelivered(Lcom/lge/ims/service/cd/CapabilityQuery;Lcom/lge/ims/service/cd/Capabilities;I)V

    #@87
    .line 197
    :cond_87
    const-string v4, "CapQuery"

    #@89
    new-instance v5, Ljava/lang/StringBuilder;

    #@8b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@8e
    const-string v6, "Capability query delivered :: "

    #@90
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v5

    #@94
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v5

    #@98
    const-string v6, ", resultCode="

    #@9a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v5

    #@9e
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v5

    #@a2
    const-string v6, ","

    #@a4
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v5

    #@a8
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v5

    #@ac
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@af
    move-result-object v5

    #@b0
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@b3
    .line 200
    iget-object v4, p0, Lcom/lge/ims/service/cd/CapabilityQuery;->mCapNotifier:Lcom/lge/ims/service/cd/CapabilityNotifier;

    #@b5
    if-eqz v4, :cond_c4

    #@b7
    .line 201
    iget-object v4, p0, Lcom/lge/ims/service/cd/CapabilityQuery;->mCapNotifier:Lcom/lge/ims/service/cd/CapabilityNotifier;

    #@b9
    invoke-virtual {v4}, Lcom/lge/ims/service/cd/CapabilityNotifier;->hasListener()Z

    #@bc
    move-result v4

    #@bd
    if-eqz v4, :cond_c4

    #@bf
    .line 202
    iget-object v4, p0, Lcom/lge/ims/service/cd/CapabilityQuery;->mCapNotifier:Lcom/lge/ims/service/cd/CapabilityNotifier;

    #@c1
    invoke-virtual {v4, v3, v2}, Lcom/lge/ims/service/cd/CapabilityNotifier;->updateCapabilities(Lcom/lge/ims/service/cd/ServiceCapabilities;I)V

    #@c4
    .line 207
    .end local v1           #notifiedResultCode:I
    :cond_c4
    invoke-virtual {p0}, Lcom/lge/ims/service/cd/CapabilityQuery;->close()V

    #@c7
    goto/16 :goto_25

    #@c9
    .line 186
    .restart local v1       #notifiedResultCode:I
    :cond_c9
    const/16 v4, 0x1e0

    #@cb
    if-eq v2, v4, :cond_d1

    #@cd
    const/16 v4, 0x198

    #@cf
    if-ne v2, v4, :cond_d3

    #@d1
    .line 188
    :cond_d1
    const/4 v1, 0x2

    #@d2
    goto :goto_7e

    #@d3
    .line 189
    :cond_d3
    const/16 v4, 0x1f7

    #@d5
    if-ne v2, v4, :cond_7e

    #@d7
    .line 191
    const/4 v1, 0x2

    #@d8
    goto :goto_7e

    #@d9
    .line 160
    nop

    #@da
    :pswitch_data_da
    .packed-switch 0x286f
        :pswitch_26
    .end packed-switch
.end method

.method public registerForCapabilityQueryCompleted(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 58
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    iput-object v0, p0, Lcom/lge/ims/service/cd/CapabilityQuery;->mRegistrant:Landroid/os/Registrant;

    #@7
    .line 59
    return-void
.end method

.method public send(Ljava/lang/String;)Z
    .registers 7
    .parameter "target"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 77
    const-string v2, "CapQuery"

    #@3
    new-instance v3, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v4, "send - "

    #@a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v3

    #@e
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v3

    #@12
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v3

    #@16
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@19
    .line 79
    iget v2, p0, Lcom/lge/ims/service/cd/CapabilityQuery;->mNativeObject:I

    #@1b
    if-nez v2, :cond_1e

    #@1d
    .line 99
    :cond_1d
    :goto_1d
    return v1

    #@1e
    .line 83
    :cond_1e
    const/16 v2, 0x283d

    #@20
    invoke-static {v2}, Lcom/lge/ims/JNICore;->obtainParcel(I)Landroid/os/Parcel;

    #@23
    move-result-object v0

    #@24
    .line 85
    .local v0, parcel:Landroid/os/Parcel;
    if-eqz v0, :cond_1d

    #@26
    .line 89
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@29
    .line 94
    iget v2, p0, Lcom/lge/ims/service/cd/CapabilityQuery;->mNativeObject:I

    #@2b
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendEvent(ILandroid/os/Parcel;)I

    #@2e
    move-result v2

    #@2f
    if-lez v2, :cond_1d

    #@31
    .line 99
    const/4 v1, 0x1

    #@32
    goto :goto_1d
.end method

.method public send(Ljava/lang/String;Lcom/lge/ims/service/cd/ServiceCapabilities;)Z
    .registers 10
    .parameter "target"
    .parameter "myCapabilities"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 103
    const-string v4, "CapQuery"

    #@3
    new-instance v5, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v6, "send - "

    #@a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v5

    #@e
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v5

    #@12
    const-string v6, ", "

    #@14
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v5

    #@18
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v5

    #@1c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v5

    #@20
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@23
    .line 105
    iget v4, p0, Lcom/lge/ims/service/cd/CapabilityQuery;->mNativeObject:I

    #@25
    if-nez v4, :cond_28

    #@27
    .line 150
    :cond_27
    :goto_27
    return v3

    #@28
    .line 109
    :cond_28
    const/16 v4, 0x283d

    #@2a
    invoke-static {v4}, Lcom/lge/ims/JNICore;->obtainParcel(I)Landroid/os/Parcel;

    #@2d
    move-result-object v1

    #@2e
    .line 111
    .local v1, parcel:Landroid/os/Parcel;
    if-eqz v1, :cond_27

    #@30
    .line 115
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@33
    .line 117
    const/4 v0, 0x0

    #@34
    .line 118
    .local v0, mediaCapabilities:I
    invoke-virtual {p2}, Lcom/lge/ims/service/cd/ServiceCapabilities;->getServiceCapabilities()I

    #@37
    move-result v2

    #@38
    .line 121
    .local v2, serviceCapabilities:I
    iget-object v4, p0, Lcom/lge/ims/service/cd/CapabilityQuery;->mCapStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@3a
    if-eqz v4, :cond_58

    #@3c
    .line 122
    iget-object v4, p0, Lcom/lge/ims/service/cd/CapabilityQuery;->mCapStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@3e
    invoke-virtual {v4}, Lcom/lge/ims/service/cd/CapabilityStateTracker;->isExternalStorageAvailable()Z

    #@41
    move-result v4

    #@42
    if-eqz v4, :cond_55

    #@44
    .line 123
    or-int/lit8 v2, v2, 0x1

    #@46
    .line 124
    invoke-virtual {p2}, Lcom/lge/ims/service/cd/ServiceCapabilities;->getMediaCapabilities()I

    #@49
    move-result v4

    #@4a
    or-int/2addr v0, v4

    #@4b
    .line 145
    :goto_4b
    iget v4, p0, Lcom/lge/ims/service/cd/CapabilityQuery;->mNativeObject:I

    #@4d
    invoke-static {v4, v1}, Lcom/lge/ims/JNICore;->sendEvent(ILandroid/os/Parcel;)I

    #@50
    move-result v4

    #@51
    if-lez v4, :cond_27

    #@53
    .line 150
    const/4 v3, 0x1

    #@54
    goto :goto_27

    #@55
    .line 126
    :cond_55
    and-int/lit8 v2, v2, -0x6

    #@57
    goto :goto_4b

    #@58
    .line 129
    :cond_58
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    #@5b
    move-result-object v4

    #@5c
    const-string v5, "mounted"

    #@5e
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@61
    move-result v4

    #@62
    if-eqz v4, :cond_6c

    #@64
    .line 130
    or-int/lit8 v2, v2, 0x1

    #@66
    .line 131
    invoke-virtual {p2}, Lcom/lge/ims/service/cd/ServiceCapabilities;->getMediaCapabilities()I

    #@69
    move-result v4

    #@6a
    or-int/2addr v0, v4

    #@6b
    goto :goto_4b

    #@6c
    .line 133
    :cond_6c
    and-int/lit8 v2, v2, -0x6

    #@6e
    goto :goto_4b
.end method

.method public setListener(Lcom/lge/ims/service/cd/CapabilityQueryListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 154
    iput-object p1, p0, Lcom/lge/ims/service/cd/CapabilityQuery;->mListener:Lcom/lge/ims/service/cd/CapabilityQueryListener;

    #@2
    .line 155
    return-void
.end method
