.class Lcom/lge/ims/service/ip/IPQueryRepository;
.super Ljava/lang/Object;
.source "IPQueryRepository.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/ip/IPQueryRepository$BuddyStatusIconQuery;,
        Lcom/lge/ims/service/ip/IPQueryRepository$PresenceQuery;,
        Lcom/lge/ims/service/ip/IPQueryRepository$Query;
    }
.end annotation


# static fields
.field static objQueryRepository:Lcom/lge/ims/service/ip/IPQueryRepository;


# instance fields
.field private objBuddyStatusIconQueryList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/lge/ims/service/ip/IPQueryRepository$BuddyStatusIconQuery;",
            ">;"
        }
    .end annotation
.end field

.field private objPresenceQueryList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/lge/ims/service/ip/IPQueryRepository$PresenceQuery;",
            ">;"
        }
    .end annotation
.end field

.field private objQueryList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/lge/ims/service/ip/IPQueryRepository$Query;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 27
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 29
    new-instance v0, Ljava/util/LinkedList;

    #@5
    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    #@8
    iput-object v0, p0, Lcom/lge/ims/service/ip/IPQueryRepository;->objQueryList:Ljava/util/LinkedList;

    #@a
    .line 30
    new-instance v0, Ljava/util/LinkedList;

    #@c
    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    #@f
    iput-object v0, p0, Lcom/lge/ims/service/ip/IPQueryRepository;->objPresenceQueryList:Ljava/util/LinkedList;

    #@11
    .line 31
    new-instance v0, Ljava/util/LinkedList;

    #@13
    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    #@16
    iput-object v0, p0, Lcom/lge/ims/service/ip/IPQueryRepository;->objBuddyStatusIconQueryList:Ljava/util/LinkedList;

    #@18
    .line 32
    return-void
.end method

.method public static DestroyQueryRepository()V
    .registers 1

    #@0
    .prologue
    .line 60
    sget-object v0, Lcom/lge/ims/service/ip/IPQueryRepository;->objQueryRepository:Lcom/lge/ims/service/ip/IPQueryRepository;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 61
    const/4 v0, 0x0

    #@5
    sput-object v0, Lcom/lge/ims/service/ip/IPQueryRepository;->objQueryRepository:Lcom/lge/ims/service/ip/IPQueryRepository;

    #@7
    .line 63
    :cond_7
    return-void
.end method

.method public static GetQueryRepository()Lcom/lge/ims/service/ip/IPQueryRepository;
    .registers 1

    #@0
    .prologue
    .line 47
    sget-object v0, Lcom/lge/ims/service/ip/IPQueryRepository;->objQueryRepository:Lcom/lge/ims/service/ip/IPQueryRepository;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 48
    new-instance v0, Lcom/lge/ims/service/ip/IPQueryRepository;

    #@6
    invoke-direct {v0}, Lcom/lge/ims/service/ip/IPQueryRepository;-><init>()V

    #@9
    sput-object v0, Lcom/lge/ims/service/ip/IPQueryRepository;->objQueryRepository:Lcom/lge/ims/service/ip/IPQueryRepository;

    #@b
    .line 51
    :cond_b
    sget-object v0, Lcom/lge/ims/service/ip/IPQueryRepository;->objQueryRepository:Lcom/lge/ims/service/ip/IPQueryRepository;

    #@d
    return-object v0
.end method


# virtual methods
.method public AddBuddyStatusIconQuery(Ljava/lang/String;)V
    .registers 7
    .parameter "strMSISDN"

    #@0
    .prologue
    .line 401
    if-eqz p1, :cond_8

    #@2
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    #@5
    move-result v3

    #@6
    if-eqz v3, :cond_e

    #@8
    .line 402
    :cond_8
    const-string v3, "[IP][ERROR]AddBuddyStatusIconQuery : MSISDN is null"

    #@a
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@d
    .line 418
    :goto_d
    return-void

    #@e
    .line 406
    :cond_e
    const/4 v0, 0x0

    #@f
    .local v0, i:I
    :goto_f
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPQueryRepository;->objBuddyStatusIconQueryList:Ljava/util/LinkedList;

    #@11
    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    #@14
    move-result v3

    #@15
    if-ge v0, v3, :cond_53

    #@17
    .line 407
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPQueryRepository;->objBuddyStatusIconQueryList:Ljava/util/LinkedList;

    #@19
    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    #@1c
    move-result-object v2

    #@1d
    check-cast v2, Lcom/lge/ims/service/ip/IPQueryRepository$BuddyStatusIconQuery;

    #@1f
    .line 408
    .local v2, objTempBuddyStatusIconQuery:Lcom/lge/ims/service/ip/IPQueryRepository$BuddyStatusIconQuery;
    invoke-virtual {v2}, Lcom/lge/ims/service/ip/IPQueryRepository$BuddyStatusIconQuery;->GetMSISDN()Ljava/lang/String;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@26
    move-result v3

    #@27
    if-nez v3, :cond_50

    #@29
    .line 409
    new-instance v1, Lcom/lge/ims/service/ip/IPQueryRepository$BuddyStatusIconQuery;

    #@2b
    invoke-direct {v1, p0, p1}, Lcom/lge/ims/service/ip/IPQueryRepository$BuddyStatusIconQuery;-><init>(Lcom/lge/ims/service/ip/IPQueryRepository;Ljava/lang/String;)V

    #@2e
    .line 410
    .local v1, objBuddyStatusIconQuery:Lcom/lge/ims/service/ip/IPQueryRepository$BuddyStatusIconQuery;
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPQueryRepository;->objBuddyStatusIconQueryList:Ljava/util/LinkedList;

    #@30
    invoke-virtual {v3, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    #@33
    .line 411
    new-instance v3, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v4, "[IP]AddBuddyStatusIconQuery [ "

    #@3a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v3

    #@3e
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v3

    #@42
    const-string v4, " ] added"

    #@44
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v3

    #@48
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v3

    #@4c
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@4f
    goto :goto_d

    #@50
    .line 406
    .end local v1           #objBuddyStatusIconQuery:Lcom/lge/ims/service/ip/IPQueryRepository$BuddyStatusIconQuery;
    :cond_50
    add-int/lit8 v0, v0, 0x1

    #@52
    goto :goto_f

    #@53
    .line 415
    .end local v2           #objTempBuddyStatusIconQuery:Lcom/lge/ims/service/ip/IPQueryRepository$BuddyStatusIconQuery;
    :cond_53
    new-instance v1, Lcom/lge/ims/service/ip/IPQueryRepository$BuddyStatusIconQuery;

    #@55
    invoke-direct {v1, p0, p1}, Lcom/lge/ims/service/ip/IPQueryRepository$BuddyStatusIconQuery;-><init>(Lcom/lge/ims/service/ip/IPQueryRepository;Ljava/lang/String;)V

    #@58
    .line 416
    .restart local v1       #objBuddyStatusIconQuery:Lcom/lge/ims/service/ip/IPQueryRepository$BuddyStatusIconQuery;
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPQueryRepository;->objBuddyStatusIconQueryList:Ljava/util/LinkedList;

    #@5a
    invoke-virtual {v3, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    #@5d
    .line 417
    new-instance v3, Ljava/lang/StringBuilder;

    #@5f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@62
    const-string v4, "[IP]AddBuddyStatusIconQuery [ "

    #@64
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v3

    #@68
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v3

    #@6c
    const-string v4, " ] added"

    #@6e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v3

    #@72
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@75
    move-result-object v3

    #@76
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@79
    goto :goto_d
.end method

.method public AddPresenceQuery(Ljava/lang/String;)V
    .registers 4
    .parameter "strMSISDN"

    #@0
    .prologue
    .line 356
    if-eqz p1, :cond_8

    #@2
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_e

    #@8
    .line 357
    :cond_8
    const-string v1, "[IP][ERROR]AddPresenceQuery : MSISDN is null"

    #@a
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@d
    .line 363
    :goto_d
    return-void

    #@e
    .line 361
    :cond_e
    new-instance v0, Lcom/lge/ims/service/ip/IPQueryRepository$PresenceQuery;

    #@10
    invoke-direct {v0, p0, p1}, Lcom/lge/ims/service/ip/IPQueryRepository$PresenceQuery;-><init>(Lcom/lge/ims/service/ip/IPQueryRepository;Ljava/lang/String;)V

    #@13
    .line 362
    .local v0, objPresenceQuery:Lcom/lge/ims/service/ip/IPQueryRepository$PresenceQuery;
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPQueryRepository;->objPresenceQueryList:Ljava/util/LinkedList;

    #@15
    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    #@18
    goto :goto_d
.end method

.method public AddQuery(Ljava/lang/String;)V
    .registers 4
    .parameter "strMSISDN"

    #@0
    .prologue
    .line 273
    if-eqz p1, :cond_8

    #@2
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_e

    #@8
    .line 274
    :cond_8
    const-string v1, "[IP][ERROR]AddQuery : MSISDN is null"

    #@a
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@d
    .line 280
    :goto_d
    return-void

    #@e
    .line 278
    :cond_e
    new-instance v0, Lcom/lge/ims/service/ip/IPQueryRepository$Query;

    #@10
    invoke-direct {v0, p0, p1}, Lcom/lge/ims/service/ip/IPQueryRepository$Query;-><init>(Lcom/lge/ims/service/ip/IPQueryRepository;Ljava/lang/String;)V

    #@13
    .line 279
    .local v0, objQuery:Lcom/lge/ims/service/ip/IPQueryRepository$Query;
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPQueryRepository;->objQueryList:Ljava/util/LinkedList;

    #@15
    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    #@18
    goto :goto_d
.end method

.method public ClearBuddyStatusIconQueryList()V
    .registers 2

    #@0
    .prologue
    .line 436
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPQueryRepository;->objBuddyStatusIconQueryList:Ljava/util/LinkedList;

    #@2
    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    #@5
    .line 437
    return-void
.end method

.method public ClearPresenceQueryList()V
    .registers 2

    #@0
    .prologue
    .line 381
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPQueryRepository;->objPresenceQueryList:Ljava/util/LinkedList;

    #@2
    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    #@5
    .line 382
    return-void
.end method

.method public ClearQueryList()V
    .registers 2

    #@0
    .prologue
    .line 328
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPQueryRepository;->objQueryList:Ljava/util/LinkedList;

    #@2
    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    #@5
    .line 329
    return-void
.end method

.method public HasBuddyStatusIconQuery(Ljava/lang/String;)Z
    .registers 6
    .parameter "strMSISDN"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 385
    if-eqz p1, :cond_9

    #@3
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    #@6
    move-result v3

    #@7
    if-eqz v3, :cond_f

    #@9
    .line 386
    :cond_9
    const-string v3, "[IP][ERROR]HasBuddyStatusIconQuery - MSISDN is null"

    #@b
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@e
    .line 397
    :cond_e
    :goto_e
    return v2

    #@f
    .line 390
    :cond_f
    const/4 v0, 0x0

    #@10
    .local v0, i:I
    :goto_10
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPQueryRepository;->objBuddyStatusIconQueryList:Ljava/util/LinkedList;

    #@12
    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    #@15
    move-result v3

    #@16
    if-ge v0, v3, :cond_e

    #@18
    .line 391
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPQueryRepository;->objBuddyStatusIconQueryList:Ljava/util/LinkedList;

    #@1a
    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    #@1d
    move-result-object v1

    #@1e
    check-cast v1, Lcom/lge/ims/service/ip/IPQueryRepository$BuddyStatusIconQuery;

    #@20
    .line 392
    .local v1, objBuddyStatusIconQuery:Lcom/lge/ims/service/ip/IPQueryRepository$BuddyStatusIconQuery;
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/IPQueryRepository$BuddyStatusIconQuery;->GetMSISDN()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v3, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@27
    move-result v3

    #@28
    if-eqz v3, :cond_2c

    #@2a
    .line 393
    const/4 v2, 0x1

    #@2b
    goto :goto_e

    #@2c
    .line 390
    :cond_2c
    add-int/lit8 v0, v0, 0x1

    #@2e
    goto :goto_10
.end method

.method public HasPresenceQuery(Ljava/lang/String;)Z
    .registers 6
    .parameter "strMSISDN"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 340
    if-eqz p1, :cond_9

    #@3
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    #@6
    move-result v3

    #@7
    if-eqz v3, :cond_f

    #@9
    .line 341
    :cond_9
    const-string v3, "[IP][ERROR]HasPresenceQuery - MSISDN is null"

    #@b
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@e
    .line 352
    :cond_e
    :goto_e
    return v2

    #@f
    .line 345
    :cond_f
    const/4 v0, 0x0

    #@10
    .local v0, i:I
    :goto_10
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPQueryRepository;->objPresenceQueryList:Ljava/util/LinkedList;

    #@12
    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    #@15
    move-result v3

    #@16
    if-ge v0, v3, :cond_e

    #@18
    .line 346
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPQueryRepository;->objPresenceQueryList:Ljava/util/LinkedList;

    #@1a
    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    #@1d
    move-result-object v1

    #@1e
    check-cast v1, Lcom/lge/ims/service/ip/IPQueryRepository$PresenceQuery;

    #@20
    .line 347
    .local v1, objPresenceQuery:Lcom/lge/ims/service/ip/IPQueryRepository$PresenceQuery;
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/IPQueryRepository$PresenceQuery;->GetMSISDN()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v3, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@27
    move-result v3

    #@28
    if-eqz v3, :cond_2c

    #@2a
    .line 348
    const/4 v2, 0x1

    #@2b
    goto :goto_e

    #@2c
    .line 345
    :cond_2c
    add-int/lit8 v0, v0, 0x1

    #@2e
    goto :goto_10
.end method

.method public HasQuery(Ljava/lang/String;)Z
    .registers 6
    .parameter "strMSISDN"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 245
    if-eqz p1, :cond_9

    #@3
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    #@6
    move-result v3

    #@7
    if-eqz v3, :cond_f

    #@9
    .line 246
    :cond_9
    const-string v3, "[IP][ERROR]HasQuery - MSISDN is null"

    #@b
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@e
    .line 257
    :cond_e
    :goto_e
    return v2

    #@f
    .line 250
    :cond_f
    const/4 v0, 0x0

    #@10
    .local v0, i:I
    :goto_10
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPQueryRepository;->objQueryList:Ljava/util/LinkedList;

    #@12
    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    #@15
    move-result v3

    #@16
    if-ge v0, v3, :cond_e

    #@18
    .line 251
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPQueryRepository;->objQueryList:Ljava/util/LinkedList;

    #@1a
    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    #@1d
    move-result-object v1

    #@1e
    check-cast v1, Lcom/lge/ims/service/ip/IPQueryRepository$Query;

    #@20
    .line 252
    .local v1, objQuery:Lcom/lge/ims/service/ip/IPQueryRepository$Query;
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/IPQueryRepository$Query;->GetMSISDN()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v3, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@27
    move-result v3

    #@28
    if-nez v3, :cond_34

    #@2a
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/IPQueryRepository$Query;->GetGlobalNumber()Ljava/lang/String;

    #@2d
    move-result-object v3

    #@2e
    invoke-virtual {v3, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@31
    move-result v3

    #@32
    if-eqz v3, :cond_36

    #@34
    .line 253
    :cond_34
    const/4 v2, 0x1

    #@35
    goto :goto_e

    #@36
    .line 250
    :cond_36
    add-int/lit8 v0, v0, 0x1

    #@38
    goto :goto_10
.end method

.method public IsEmpty()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 218
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPQueryRepository;->objQueryList:Ljava/util/LinkedList;

    #@3
    if-nez v1, :cond_6

    #@5
    .line 222
    :cond_5
    :goto_5
    return v0

    #@6
    :cond_6
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPQueryRepository;->objQueryList:Ljava/util/LinkedList;

    #@8
    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    #@b
    move-result v1

    #@c
    if-nez v1, :cond_5

    #@e
    const/4 v0, 0x1

    #@f
    goto :goto_5
.end method

.method public IsPresenceQueryEmpty()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 332
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPQueryRepository;->objPresenceQueryList:Ljava/util/LinkedList;

    #@3
    if-nez v1, :cond_6

    #@5
    .line 336
    :cond_5
    :goto_5
    return v0

    #@6
    :cond_6
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPQueryRepository;->objPresenceQueryList:Ljava/util/LinkedList;

    #@8
    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    #@b
    move-result v1

    #@c
    if-nez v1, :cond_5

    #@e
    const/4 v0, 0x1

    #@f
    goto :goto_5
.end method

.method public RemoveBuddyStatusIconQuery(Ljava/lang/String;)V
    .registers 6
    .parameter "strNumber"

    #@0
    .prologue
    .line 421
    if-eqz p1, :cond_8

    #@2
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    #@5
    move-result v2

    #@6
    if-eqz v2, :cond_e

    #@8
    .line 422
    :cond_8
    const-string v2, "[IP][ERROR]RemoveBuddyStatusIconQuery : MSISDN is null"

    #@a
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@d
    .line 433
    :cond_d
    return-void

    #@e
    .line 426
    :cond_e
    const/4 v0, 0x0

    #@f
    .local v0, i:I
    :goto_f
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPQueryRepository;->objBuddyStatusIconQueryList:Ljava/util/LinkedList;

    #@11
    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    #@14
    move-result v2

    #@15
    if-ge v0, v2, :cond_d

    #@17
    .line 427
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPQueryRepository;->objBuddyStatusIconQueryList:Ljava/util/LinkedList;

    #@19
    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    #@1c
    move-result-object v1

    #@1d
    check-cast v1, Lcom/lge/ims/service/ip/IPQueryRepository$BuddyStatusIconQuery;

    #@1f
    .line 428
    .local v1, objBuddyStatusIconQuery:Lcom/lge/ims/service/ip/IPQueryRepository$BuddyStatusIconQuery;
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/IPQueryRepository$BuddyStatusIconQuery;->GetMSISDN()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@26
    move-result v2

    #@27
    if-eqz v2, :cond_4a

    #@29
    .line 429
    new-instance v2, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v3, "[IP]RemoveBuddyStatusIconQuery [ "

    #@30
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v2

    #@38
    const-string v3, " ] removed"

    #@3a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v2

    #@42
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@45
    .line 430
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPQueryRepository;->objBuddyStatusIconQueryList:Ljava/util/LinkedList;

    #@47
    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    #@4a
    .line 426
    :cond_4a
    add-int/lit8 v0, v0, 0x1

    #@4c
    goto :goto_f
.end method

.method public RemovePresenceQuery(Ljava/lang/String;)V
    .registers 6
    .parameter "strNumber"

    #@0
    .prologue
    .line 366
    if-eqz p1, :cond_8

    #@2
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    #@5
    move-result v2

    #@6
    if-eqz v2, :cond_e

    #@8
    .line 367
    :cond_8
    const-string v2, "[IP][ERROR]RemovePresenceQuery : MSISDN is null"

    #@a
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@d
    .line 378
    :cond_d
    return-void

    #@e
    .line 371
    :cond_e
    const/4 v0, 0x0

    #@f
    .local v0, i:I
    :goto_f
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPQueryRepository;->objPresenceQueryList:Ljava/util/LinkedList;

    #@11
    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    #@14
    move-result v2

    #@15
    if-ge v0, v2, :cond_d

    #@17
    .line 372
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPQueryRepository;->objPresenceQueryList:Ljava/util/LinkedList;

    #@19
    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    #@1c
    move-result-object v1

    #@1d
    check-cast v1, Lcom/lge/ims/service/ip/IPQueryRepository$PresenceQuery;

    #@1f
    .line 373
    .local v1, objPresenceQuery:Lcom/lge/ims/service/ip/IPQueryRepository$PresenceQuery;
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/IPQueryRepository$PresenceQuery;->GetMSISDN()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@26
    move-result v2

    #@27
    if-eqz v2, :cond_4a

    #@29
    .line 374
    new-instance v2, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v3, "[IP]RemovePresenceQuery [ "

    #@30
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v2

    #@38
    const-string v3, " ] removed"

    #@3a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v2

    #@42
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@45
    .line 375
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPQueryRepository;->objPresenceQueryList:Ljava/util/LinkedList;

    #@47
    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    #@4a
    .line 371
    :cond_4a
    add-int/lit8 v0, v0, 0x1

    #@4c
    goto :goto_f
.end method

.method public RemoveQuery(Ljava/lang/String;)V
    .registers 10
    .parameter "strNumber"

    #@0
    .prologue
    const-wide/16 v6, 0x64

    #@2
    .line 295
    if-eqz p1, :cond_a

    #@4
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    #@7
    move-result v4

    #@8
    if-eqz v4, :cond_10

    #@a
    .line 296
    :cond_a
    const-string v4, "[IP][ERROR] RemoveQuery : MSISDN is null"

    #@c
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@f
    .line 320
    :cond_f
    :goto_f
    return-void

    #@10
    .line 300
    :cond_10
    const/4 v0, 0x0

    #@11
    .local v0, i:I
    :goto_11
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPQueryRepository;->objQueryList:Ljava/util/LinkedList;

    #@13
    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    #@16
    move-result v4

    #@17
    if-ge v0, v4, :cond_59

    #@19
    .line 301
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPQueryRepository;->objQueryList:Ljava/util/LinkedList;

    #@1b
    invoke-virtual {v4, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    #@1e
    move-result-object v3

    #@1f
    check-cast v3, Lcom/lge/ims/service/ip/IPQueryRepository$Query;

    #@21
    .line 302
    .local v3, objQuery:Lcom/lge/ims/service/ip/IPQueryRepository$Query;
    invoke-virtual {v3}, Lcom/lge/ims/service/ip/IPQueryRepository$Query;->GetMSISDN()Ljava/lang/String;

    #@24
    move-result-object v4

    #@25
    invoke-virtual {v4, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@28
    move-result v4

    #@29
    if-nez v4, :cond_35

    #@2b
    invoke-virtual {v3}, Lcom/lge/ims/service/ip/IPQueryRepository$Query;->GetGlobalNumber()Ljava/lang/String;

    #@2e
    move-result-object v4

    #@2f
    invoke-virtual {v4, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@32
    move-result v4

    #@33
    if-eqz v4, :cond_56

    #@35
    .line 303
    :cond_35
    new-instance v4, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v5, "[IP]RemoveQuery [ "

    #@3c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v4

    #@40
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v4

    #@44
    const-string v5, " ]removed"

    #@46
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v4

    #@4a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v4

    #@4e
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@51
    .line 304
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPQueryRepository;->objQueryList:Ljava/util/LinkedList;

    #@53
    invoke-virtual {v4, v0}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    #@56
    .line 300
    :cond_56
    add-int/lit8 v0, v0, 0x1

    #@58
    goto :goto_11

    #@59
    .line 308
    .end local v3           #objQuery:Lcom/lge/ims/service/ip/IPQueryRepository$Query;
    :cond_59
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPQueryRepository;->objQueryList:Ljava/util/LinkedList;

    #@5b
    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    #@5e
    move-result v4

    #@5f
    if-nez v4, :cond_f

    #@61
    .line 309
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@64
    move-result-object v2

    #@65
    .line 310
    .local v2, objIPAgentThread:Lcom/lge/ims/service/ip/IPAgent;
    if-eqz v2, :cond_f

    #@67
    invoke-virtual {v2}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@6a
    move-result-object v4

    #@6b
    if-eqz v4, :cond_f

    #@6d
    .line 311
    invoke-static {}, Lcom/lge/ims/service/ip/IPContactHelper;->GetInstance()Lcom/lge/ims/service/ip/IPContactHelper;

    #@70
    move-result-object v4

    #@71
    invoke-virtual {v4, p1}, Lcom/lge/ims/service/ip/IPContactHelper;->GetCapaType(Ljava/lang/String;)I

    #@74
    move-result v1

    #@75
    .line 312
    .local v1, nCapaType:I
    const/4 v4, 0x1

    #@76
    if-ne v1, v4, :cond_82

    #@78
    .line 313
    invoke-virtual {v2}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@7b
    move-result-object v4

    #@7c
    const/16 v5, 0x12

    #@7e
    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@81
    goto :goto_f

    #@82
    .line 314
    :cond_82
    if-nez v1, :cond_f

    #@84
    .line 315
    invoke-virtual {v2}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@87
    move-result-object v4

    #@88
    const/16 v5, 0x11

    #@8a
    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@8d
    goto :goto_f
.end method
