.class public abstract Lcom/lge/ims/service/im/IIMService$Stub;
.super Landroid/os/Binder;
.source "IIMService.java"

# interfaces
.implements Lcom/lge/ims/service/im/IIMService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/im/IIMService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/im/IIMService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.lge.ims.service.im.IIMService"

.field static final TRANSACTION_closeChat:I = 0x2

.field static final TRANSACTION_closeFileTransfer:I = 0x5

.field static final TRANSACTION_createCapabilityQuery:I = 0x6

.field static final TRANSACTION_getCallState:I = 0xd

.field static final TRANSACTION_getServiceNetworkType:I = 0xb

.field static final TRANSACTION_openChat:I = 0x1

.field static final TRANSACTION_openFileTransfer:I = 0x3

.field static final TRANSACTION_openFileTransferToDownload:I = 0x4

.field static final TRANSACTION_sendMessageDisplayNotification:I = 0x7

.field static final TRANSACTION_setDisplayName:I = 0xa

.field static final TRANSACTION_setListener:I = 0xc

.field static final TRANSACTION_subscribeCapability:I = 0x8

.field static final TRANSACTION_unsubscribeCapability:I = 0x9


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 15
    const-string v0, "com.lge.ims.service.im.IIMService"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/lge/ims/service/im/IIMService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/im/IIMService;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 23
    if-nez p0, :cond_4

    #@2
    .line 24
    const/4 v0, 0x0

    #@3
    .line 30
    :goto_3
    return-object v0

    #@4
    .line 26
    :cond_4
    const-string v1, "com.lge.ims.service.im.IIMService"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 27
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/lge/ims/service/im/IIMService;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 28
    check-cast v0, Lcom/lge/ims/service/im/IIMService;

    #@12
    goto :goto_3

    #@13
    .line 30
    :cond_13
    new-instance v0, Lcom/lge/ims/service/im/IIMService$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/lge/ims/service/im/IIMService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 12
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v4, 0x0

    #@2
    const/4 v5, 0x1

    #@3
    .line 38
    sparse-switch p1, :sswitch_data_12e

    #@6
    .line 167
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@9
    move-result v5

    #@a
    :goto_a
    return v5

    #@b
    .line 42
    :sswitch_b
    const-string v4, "com.lge.ims.service.im.IIMService"

    #@d
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    goto :goto_a

    #@11
    .line 47
    :sswitch_11
    const-string v6, "com.lge.ims.service.im.IIMService"

    #@13
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@16
    .line 48
    invoke-virtual {p0}, Lcom/lge/ims/service/im/IIMService$Stub;->openChat()Lcom/lge/ims/service/im/IChat;

    #@19
    move-result-object v3

    #@1a
    .line 49
    .local v3, _result:Lcom/lge/ims/service/im/IChat;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1d
    .line 50
    if-eqz v3, :cond_23

    #@1f
    invoke-interface {v3}, Lcom/lge/ims/service/im/IChat;->asBinder()Landroid/os/IBinder;

    #@22
    move-result-object v4

    #@23
    :cond_23
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@26
    goto :goto_a

    #@27
    .line 55
    .end local v3           #_result:Lcom/lge/ims/service/im/IChat;
    :sswitch_27
    const-string v4, "com.lge.ims.service.im.IIMService"

    #@29
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2c
    .line 57
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@2f
    move-result-object v4

    #@30
    invoke-static {v4}, Lcom/lge/ims/service/im/IChat$Stub;->asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/im/IChat;

    #@33
    move-result-object v0

    #@34
    .line 58
    .local v0, _arg0:Lcom/lge/ims/service/im/IChat;
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/im/IIMService$Stub;->closeChat(Lcom/lge/ims/service/im/IChat;)Z

    #@37
    move-result v3

    #@38
    .line 59
    .local v3, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3b
    .line 60
    if-eqz v3, :cond_42

    #@3d
    move v4, v5

    #@3e
    :goto_3e
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@41
    goto :goto_a

    #@42
    :cond_42
    move v4, v6

    #@43
    goto :goto_3e

    #@44
    .line 65
    .end local v0           #_arg0:Lcom/lge/ims/service/im/IChat;
    .end local v3           #_result:Z
    :sswitch_44
    const-string v6, "com.lge.ims.service.im.IIMService"

    #@46
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@49
    .line 67
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    #@4c
    move-result-object v0

    #@4d
    .line 68
    .local v0, _arg0:[Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/im/IIMService$Stub;->openFileTransfer([Ljava/lang/String;)Lcom/lge/ims/service/im/IFileTransfer;

    #@50
    move-result-object v3

    #@51
    .line 69
    .local v3, _result:Lcom/lge/ims/service/im/IFileTransfer;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@54
    .line 70
    if-eqz v3, :cond_5a

    #@56
    invoke-interface {v3}, Lcom/lge/ims/service/im/IFileTransfer;->asBinder()Landroid/os/IBinder;

    #@59
    move-result-object v4

    #@5a
    :cond_5a
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@5d
    goto :goto_a

    #@5e
    .line 75
    .end local v0           #_arg0:[Ljava/lang/String;
    .end local v3           #_result:Lcom/lge/ims/service/im/IFileTransfer;
    :sswitch_5e
    const-string v6, "com.lge.ims.service.im.IIMService"

    #@60
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@63
    .line 76
    invoke-virtual {p0}, Lcom/lge/ims/service/im/IIMService$Stub;->openFileTransferToDownload()Lcom/lge/ims/service/im/IFileTransfer;

    #@66
    move-result-object v3

    #@67
    .line 77
    .restart local v3       #_result:Lcom/lge/ims/service/im/IFileTransfer;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@6a
    .line 78
    if-eqz v3, :cond_70

    #@6c
    invoke-interface {v3}, Lcom/lge/ims/service/im/IFileTransfer;->asBinder()Landroid/os/IBinder;

    #@6f
    move-result-object v4

    #@70
    :cond_70
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@73
    goto :goto_a

    #@74
    .line 83
    .end local v3           #_result:Lcom/lge/ims/service/im/IFileTransfer;
    :sswitch_74
    const-string v4, "com.lge.ims.service.im.IIMService"

    #@76
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@79
    .line 85
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@7c
    move-result-object v4

    #@7d
    invoke-static {v4}, Lcom/lge/ims/service/im/IFileTransfer$Stub;->asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/im/IFileTransfer;

    #@80
    move-result-object v0

    #@81
    .line 86
    .local v0, _arg0:Lcom/lge/ims/service/im/IFileTransfer;
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/im/IIMService$Stub;->closeFileTransfer(Lcom/lge/ims/service/im/IFileTransfer;)Z

    #@84
    move-result v3

    #@85
    .line 87
    .local v3, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@88
    .line 88
    if-eqz v3, :cond_8b

    #@8a
    move v6, v5

    #@8b
    :cond_8b
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@8e
    goto/16 :goto_a

    #@90
    .line 93
    .end local v0           #_arg0:Lcom/lge/ims/service/im/IFileTransfer;
    .end local v3           #_result:Z
    :sswitch_90
    const-string v6, "com.lge.ims.service.im.IIMService"

    #@92
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@95
    .line 94
    invoke-virtual {p0}, Lcom/lge/ims/service/im/IIMService$Stub;->createCapabilityQuery()Lcom/lge/ims/service/cd/ICapabilityQuery;

    #@98
    move-result-object v3

    #@99
    .line 95
    .local v3, _result:Lcom/lge/ims/service/cd/ICapabilityQuery;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@9c
    .line 96
    if-eqz v3, :cond_a2

    #@9e
    invoke-interface {v3}, Lcom/lge/ims/service/cd/ICapabilityQuery;->asBinder()Landroid/os/IBinder;

    #@a1
    move-result-object v4

    #@a2
    :cond_a2
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@a5
    goto/16 :goto_a

    #@a7
    .line 101
    .end local v3           #_result:Lcom/lge/ims/service/cd/ICapabilityQuery;
    :sswitch_a7
    const-string v4, "com.lge.ims.service.im.IIMService"

    #@a9
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ac
    .line 103
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@af
    move-result-object v0

    #@b0
    .line 105
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@b3
    move-result-object v1

    #@b4
    .line 107
    .local v1, _arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@b7
    move-result-object v2

    #@b8
    .line 108
    .local v2, _arg2:Ljava/lang/String;
    invoke-virtual {p0, v0, v1, v2}, Lcom/lge/ims/service/im/IIMService$Stub;->sendMessageDisplayNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@bb
    .line 109
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@be
    goto/16 :goto_a

    #@c0
    .line 114
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:Ljava/lang/String;
    .end local v2           #_arg2:Ljava/lang/String;
    :sswitch_c0
    const-string v4, "com.lge.ims.service.im.IIMService"

    #@c2
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c5
    .line 116
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@c8
    move-result-object v0

    #@c9
    .line 117
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/im/IIMService$Stub;->subscribeCapability(Ljava/lang/String;)V

    #@cc
    .line 118
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@cf
    goto/16 :goto_a

    #@d1
    .line 123
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_d1
    const-string v4, "com.lge.ims.service.im.IIMService"

    #@d3
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d6
    .line 125
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@d9
    move-result-object v0

    #@da
    .line 126
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/im/IIMService$Stub;->unsubscribeCapability(Ljava/lang/String;)V

    #@dd
    .line 127
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@e0
    goto/16 :goto_a

    #@e2
    .line 132
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_e2
    const-string v4, "com.lge.ims.service.im.IIMService"

    #@e4
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@e7
    .line 134
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@ea
    move-result-object v0

    #@eb
    .line 135
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/im/IIMService$Stub;->setDisplayName(Ljava/lang/String;)V

    #@ee
    .line 136
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@f1
    goto/16 :goto_a

    #@f3
    .line 141
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_f3
    const-string v4, "com.lge.ims.service.im.IIMService"

    #@f5
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f8
    .line 142
    invoke-virtual {p0}, Lcom/lge/ims/service/im/IIMService$Stub;->getServiceNetworkType()I

    #@fb
    move-result v3

    #@fc
    .line 143
    .local v3, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@ff
    .line 144
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@102
    goto/16 :goto_a

    #@104
    .line 149
    .end local v3           #_result:I
    :sswitch_104
    const-string v4, "com.lge.ims.service.im.IIMService"

    #@106
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@109
    .line 151
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@10c
    move-result-object v4

    #@10d
    invoke-static {v4}, Lcom/lge/ims/service/im/IIMServiceListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/im/IIMServiceListener;

    #@110
    move-result-object v0

    #@111
    .line 152
    .local v0, _arg0:Lcom/lge/ims/service/im/IIMServiceListener;
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/im/IIMService$Stub;->setListener(Lcom/lge/ims/service/im/IIMServiceListener;)V

    #@114
    .line 153
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@117
    goto/16 :goto_a

    #@119
    .line 158
    .end local v0           #_arg0:Lcom/lge/ims/service/im/IIMServiceListener;
    :sswitch_119
    const-string v4, "com.lge.ims.service.im.IIMService"

    #@11b
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@11e
    .line 160
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@121
    move-result-object v0

    #@122
    .line 161
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/im/IIMService$Stub;->getCallState(Ljava/lang/String;)I

    #@125
    move-result v3

    #@126
    .line 162
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@129
    .line 163
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@12c
    goto/16 :goto_a

    #@12e
    .line 38
    :sswitch_data_12e
    .sparse-switch
        0x1 -> :sswitch_11
        0x2 -> :sswitch_27
        0x3 -> :sswitch_44
        0x4 -> :sswitch_5e
        0x5 -> :sswitch_74
        0x6 -> :sswitch_90
        0x7 -> :sswitch_a7
        0x8 -> :sswitch_c0
        0x9 -> :sswitch_d1
        0xa -> :sswitch_e2
        0xb -> :sswitch_f3
        0xc -> :sswitch_104
        0xd -> :sswitch_119
        0x5f4e5446 -> :sswitch_b
    .end sparse-switch
.end method
