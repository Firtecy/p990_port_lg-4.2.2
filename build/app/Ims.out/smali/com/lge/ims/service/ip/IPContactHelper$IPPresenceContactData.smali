.class public final Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;
.super Ljava/lang/Object;
.source "IPContactHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/ip/IPContactHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "IPPresenceContactData"
.end annotation


# instance fields
.field public nId:J

.field public nStatus:I

.field public nStatusIconUpdateToContact:I

.field public nTimeStamp:J

.field public strBirthDay:Ljava/lang/String;

.field public strCyworldAccount:Ljava/lang/String;

.field public strEMailAddress:Ljava/lang/String;

.field public strFaceBookAccount:Ljava/lang/String;

.field public strFreeText:Ljava/lang/String;

.field public strHomepage:Ljava/lang/String;

.field public strNormalizeMSISDN:Ljava/lang/String;

.field public strStatusIcon:Ljava/lang/String;

.field public strStatusIconLink:Ljava/lang/String;

.field public strStatusIconThumb:[B

.field public strStatusIconThumbEtag:Ljava/lang/String;

.field public strStatusIconThumbLink:Ljava/lang/String;

.field public strTwitterAccount:Ljava/lang/String;

.field public strWaggleAccount:Ljava/lang/String;

.field final synthetic this$0:Lcom/lge/ims/service/ip/IPContactHelper;


# direct methods
.method public constructor <init>(Lcom/lge/ims/service/ip/IPContactHelper;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 103
    iput-object p1, p0, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->this$0:Lcom/lge/ims/service/ip/IPContactHelper;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method
