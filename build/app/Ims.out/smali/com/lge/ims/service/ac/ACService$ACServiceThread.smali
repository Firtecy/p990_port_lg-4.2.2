.class final Lcom/lge/ims/service/ac/ACService$ACServiceThread;
.super Ljava/lang/Thread;
.source "ACService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/ac/ACService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ACServiceThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/service/ac/ACService;


# direct methods
.method public constructor <init>(Lcom/lge/ims/service/ac/ACService;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 1135
    iput-object p1, p0, Lcom/lge/ims/service/ac/ACService$ACServiceThread;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@2
    .line 1136
    const-string v0, "ACService"

    #@4
    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    #@7
    .line 1137
    return-void
.end method


# virtual methods
.method public run()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 1140
    invoke-static {}, Landroid/os/Looper;->prepare()V

    #@4
    .line 1142
    const-string v2, "ACService"

    #@6
    new-instance v3, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v4, "ACServiceThread is running ... ("

    #@d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-static {}, Landroid/os/Process;->myTid()I

    #@14
    move-result v4

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    const-string v4, ")"

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v3

    #@23
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@26
    .line 1144
    iget-object v2, p0, Lcom/lge/ims/service/ac/ACService$ACServiceThread;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@28
    new-instance v3, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@2a
    iget-object v4, p0, Lcom/lge/ims/service/ac/ACService$ACServiceThread;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@2c
    invoke-direct {v3, v4, v5}, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;-><init>(Lcom/lge/ims/service/ac/ACService;Lcom/lge/ims/service/ac/ACService$1;)V

    #@2f
    invoke-static {v2, v3}, Lcom/lge/ims/service/ac/ACService;->access$1502(Lcom/lge/ims/service/ac/ACService;Lcom/lge/ims/service/ac/ACService$ACServiceHandler;)Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@32
    .line 1146
    invoke-static {}, Lcom/lge/ims/PhoneStateTracker;->getInstance()Lcom/lge/ims/PhoneStateTracker;

    #@35
    move-result-object v1

    #@36
    .line 1148
    .local v1, pst:Lcom/lge/ims/PhoneStateTracker;
    if-eqz v1, :cond_4e

    #@38
    .line 1149
    iget-object v2, p0, Lcom/lge/ims/service/ac/ACService$ACServiceThread;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@3a
    invoke-static {v2}, Lcom/lge/ims/service/ac/ACService;->access$1500(Lcom/lge/ims/service/ac/ACService;)Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@3d
    move-result-object v2

    #@3e
    const/16 v3, 0x69

    #@40
    invoke-virtual {v1, v2, v3, v5}, Lcom/lge/ims/PhoneStateTracker;->registerForSimStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@43
    .line 1150
    iget-object v2, p0, Lcom/lge/ims/service/ac/ACService$ACServiceThread;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@45
    invoke-static {v2}, Lcom/lge/ims/service/ac/ACService;->access$1500(Lcom/lge/ims/service/ac/ACService;)Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@48
    move-result-object v2

    #@49
    const/16 v3, 0x6a

    #@4b
    invoke-virtual {v1, v2, v3, v5}, Lcom/lge/ims/PhoneStateTracker;->registerForAirplaneModeChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@4e
    .line 1153
    :cond_4e
    invoke-static {}, Lcom/lge/ims/DataConnectionManager;->getInstance()Lcom/lge/ims/DataConnectionManager;

    #@51
    move-result-object v0

    #@52
    .line 1155
    .local v0, dcm:Lcom/lge/ims/DataConnectionManager;
    if-eqz v0, :cond_72

    #@54
    .line 1156
    iget-object v2, p0, Lcom/lge/ims/service/ac/ACService$ACServiceThread;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@56
    invoke-static {v2}, Lcom/lge/ims/service/ac/ACService;->access$1500(Lcom/lge/ims/service/ac/ACService;)Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@59
    move-result-object v2

    #@5a
    const/16 v3, 0x65

    #@5c
    invoke-virtual {v0, v2, v3, v5}, Lcom/lge/ims/DataConnectionManager;->registerForDataStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5f
    .line 1158
    invoke-virtual {v0}, Lcom/lge/ims/DataConnectionManager;->isConnected()Z

    #@62
    move-result v2

    #@63
    if-eqz v2, :cond_72

    #@65
    .line 1159
    iget-object v2, p0, Lcom/lge/ims/service/ac/ACService$ACServiceThread;->this$0:Lcom/lge/ims/service/ac/ACService;

    #@67
    invoke-static {v2}, Lcom/lge/ims/service/ac/ACService;->access$1500(Lcom/lge/ims/service/ac/ACService;)Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@6a
    move-result-object v2

    #@6b
    const/16 v3, 0x66

    #@6d
    const-wide/16 v4, 0xa

    #@6f
    invoke-virtual {v2, v3, v4, v5}, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->sendEmptyMessageDelayed(IJ)Z

    #@72
    .line 1163
    :cond_72
    invoke-static {}, Landroid/os/Looper;->loop()V

    #@75
    .line 1164
    return-void
.end method
