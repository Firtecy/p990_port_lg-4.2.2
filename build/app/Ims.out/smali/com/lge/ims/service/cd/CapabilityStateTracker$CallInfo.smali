.class public final Lcom/lge/ims/service/cd/CapabilityStateTracker$CallInfo;
.super Ljava/lang/Object;
.source "CapabilityStateTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/cd/CapabilityStateTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "CallInfo"
.end annotation


# static fields
.field public static final CALL_TYPE_CS:I = 0x1

.field public static final CALL_TYPE_VOIP:I = 0x3

.field public static final CALL_TYPE_VT:I = 0x2


# instance fields
.field private mCallType:I

.field private mNumber:Ljava/lang/String;

.field final synthetic this$0:Lcom/lge/ims/service/cd/CapabilityStateTracker;


# direct methods
.method public constructor <init>(Lcom/lge/ims/service/cd/CapabilityStateTracker;ILjava/lang/String;)V
    .registers 4
    .parameter
    .parameter "callType"
    .parameter "number"

    #@0
    .prologue
    .line 162
    iput-object p1, p0, Lcom/lge/ims/service/cd/CapabilityStateTracker$CallInfo;->this$0:Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 163
    iput p2, p0, Lcom/lge/ims/service/cd/CapabilityStateTracker$CallInfo;->mCallType:I

    #@7
    .line 164
    iput-object p3, p0, Lcom/lge/ims/service/cd/CapabilityStateTracker$CallInfo;->mNumber:Ljava/lang/String;

    #@9
    .line 165
    return-void
.end method


# virtual methods
.method public getCallType()I
    .registers 2

    #@0
    .prologue
    .line 168
    iget v0, p0, Lcom/lge/ims/service/cd/CapabilityStateTracker$CallInfo;->mCallType:I

    #@2
    return v0
.end method

.method public getNumber()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 172
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityStateTracker$CallInfo;->mNumber:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 176
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "Type="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Lcom/lge/ims/service/cd/CapabilityStateTracker$CallInfo;->mCallType:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ", Number="

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget-object v1, p0, Lcom/lge/ims/service/cd/CapabilityStateTracker$CallInfo;->mNumber:Ljava/lang/String;

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v0

    #@21
    return-object v0
.end method
